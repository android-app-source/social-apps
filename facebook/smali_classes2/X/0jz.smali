.class public final LX/0jz;
.super LX/0gc;
.source ""

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# static fields
.field public static final A:Landroid/view/animation/Interpolator;

.field public static final B:Landroid/view/animation/Interpolator;

.field public static final C:Landroid/view/animation/Interpolator;

.field public static a:Z

.field public static final b:Z

.field public static final z:Landroid/view/animation/Interpolator;


# instance fields
.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public d:[Ljava/lang/Runnable;

.field public e:Z

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0fN;",
            ">;"
        }
    .end annotation
.end field

.field public m:I

.field public mAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0k3;

.field public o:LX/0k1;

.field public p:Landroid/support/v4/app/Fragment;

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Ljava/lang/String;

.field public u:Z

.field public v:Ljava/lang/Exception;

.field public w:Landroid/os/Bundle;

.field public x:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 125969
    sput-boolean v0, LX/0jz;->a:Z

    .line 125970
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, LX/0jz;->b:Z

    .line 125971
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, LX/0jz;->z:Landroid/view/animation/Interpolator;

    .line 125972
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, LX/0jz;->A:Landroid/view/animation/Interpolator;

    .line 125973
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, LX/0jz;->B:Landroid/view/animation/Interpolator;

    .line 125974
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, LX/0jz;->C:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125388
    invoke-direct {p0}, LX/0gc;-><init>()V

    .line 125389
    const/4 v0, 0x0

    iput v0, p0, LX/0jz;->m:I

    .line 125390
    iput-object v1, p0, LX/0jz;->w:Landroid/os/Bundle;

    .line 125391
    iput-object v1, p0, LX/0jz;->x:Landroid/util/SparseArray;

    .line 125392
    new-instance v0, Landroid/support/v4/app/FragmentManagerImpl$1;

    invoke-direct {v0, p0}, Landroid/support/v4/app/FragmentManagerImpl$1;-><init>(LX/0jz;)V

    iput-object v0, p0, LX/0jz;->y:Ljava/lang/Runnable;

    .line 125393
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 125383
    iget-boolean v0, p0, LX/0jz;->r:Z

    if-eqz v0, :cond_0

    .line 125384
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125385
    :cond_0
    iget-object v0, p0, LX/0jz;->t:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 125386
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0jz;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125387
    :cond_1
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 125378
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 125379
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 125380
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fN;

    invoke-interface {v0}, LX/0fN;->fZ_()V

    .line 125381
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125382
    :cond_0
    return-void
.end method

.method private static a(FF)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 125374
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 125375
    sget-object v1, LX/0jz;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 125376
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 125377
    return-object v0
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 125364
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 125365
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 125366
    sget-object v1, LX/0jz;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 125367
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 125368
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 125369
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 125370
    sget-object v1, LX/0jz;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 125371
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 125372
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 125373
    return-object v9
.end method

.method private a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 125341
    iget v0, p1, Landroid/support/v4/app/Fragment;->mNextAnim:I

    invoke-virtual {p1, p2, p3, v0}, Landroid/support/v4/app/Fragment;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 125342
    if-eqz v0, :cond_1

    .line 125343
    :cond_0
    :goto_0
    return-object v0

    .line 125344
    :cond_1
    iget v0, p1, Landroid/support/v4/app/Fragment;->mNextAnim:I

    if-eqz v0, :cond_2

    .line 125345
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    iget v2, p1, Landroid/support/v4/app/Fragment;->mNextAnim:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 125346
    if-nez v0, :cond_0

    .line 125347
    :cond_2
    if-nez p2, :cond_3

    move-object v0, v1

    .line 125348
    goto :goto_0

    .line 125349
    :cond_3
    invoke-static {p2, p3}, LX/0jz;->b(IZ)I

    move-result v0

    .line 125350
    if-gez v0, :cond_4

    move-object v0, v1

    .line 125351
    goto :goto_0

    .line 125352
    :cond_4
    packed-switch v0, :pswitch_data_0

    .line 125353
    if-nez p4, :cond_5

    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->j()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 125354
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->j()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 125355
    :cond_5
    if-nez p4, :cond_6

    move-object v0, v1

    .line 125356
    goto :goto_0

    .line 125357
    :pswitch_0
    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, LX/0jz;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 125358
    :pswitch_1
    invoke-static {v3, v5, v3, v4}, LX/0jz;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 125359
    :pswitch_2
    invoke-static {v5, v3, v4, v3}, LX/0jz;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 125360
    :pswitch_3
    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, LX/0jz;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 125361
    :pswitch_4
    invoke-static {v4, v3}, LX/0jz;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 125362
    :pswitch_5
    invoke-static {v3, v4}, LX/0jz;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 125363
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(ILandroid/support/v4/app/BackStackRecord;)V
    .locals 3

    .prologue
    .line 125323
    monitor-enter p0

    .line 125324
    :try_start_0
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 125325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    .line 125326
    :cond_0
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 125327
    if-ge p1, v0, :cond_2

    .line 125328
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Setting back stack index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125329
    :cond_1
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 125330
    :goto_0
    monitor-exit p0

    return-void

    .line 125331
    :cond_2
    :goto_1
    if-ge v0, p1, :cond_5

    .line 125332
    iget-object v1, p0, LX/0jz;->j:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125333
    iget-object v1, p0, LX/0jz;->k:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 125334
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/0jz;->k:Ljava/util/ArrayList;

    .line 125335
    :cond_3
    sget-boolean v1, LX/0jz;->a:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding available back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125336
    :cond_4
    iget-object v1, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125337
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 125338
    :cond_5
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Adding back stack index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125339
    :cond_6
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125340
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 125321
    invoke-virtual {p0, p1, v0, v0, p2}, LX/0jz;->a(IIIZ)V

    .line 125322
    return-void
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 125183
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125184
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125185
    new-instance v0, LX/0xJ;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, LX/0xJ;-><init>(Ljava/lang/String;)V

    .line 125186
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 125187
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    if-eqz v0, :cond_0

    .line 125188
    :try_start_0
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0k3;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125189
    :goto_0
    throw p1

    .line 125190
    :catch_0
    move-exception v0

    .line 125191
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 125192
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, LX/0jz;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 125193
    :catch_1
    move-exception v0

    .line 125194
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static b(IZ)I
    .locals 1

    .prologue
    .line 125299
    const/4 v0, -0x1

    .line 125300
    sparse-switch p0, :sswitch_data_0

    .line 125301
    :goto_0
    return v0

    .line 125302
    :sswitch_0
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 125303
    :sswitch_1
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 125304
    :sswitch_2
    if-eqz p1, :cond_2

    const/4 v0, 0x5

    goto :goto_0

    :cond_2
    const/4 v0, 0x6

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private static d(Landroid/support/v4/app/Fragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 125297
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/app/Fragment;->mContainerId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 125298
    :goto_0
    return-object v0

    :catch_0
    const-string v0, "not-found"

    goto :goto_0
.end method

.method public static e(I)I
    .locals 1

    .prologue
    .line 125289
    const/4 v0, 0x0

    .line 125290
    sparse-switch p0, :sswitch_data_0

    .line 125291
    :goto_0
    return v0

    .line 125292
    :sswitch_0
    const/16 v0, 0x2002

    .line 125293
    goto :goto_0

    .line 125294
    :sswitch_1
    const/16 v0, 0x1001

    .line 125295
    goto :goto_0

    .line 125296
    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private e(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 125279
    iget v0, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    if-ltz v0, :cond_1

    .line 125280
    :cond_0
    :goto_0
    return-void

    .line 125281
    :cond_1
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_4

    .line 125282
    :cond_2
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 125283
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    .line 125284
    :cond_3
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->setIndex(ILandroid/support/v4/app/Fragment;)V

    .line 125285
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125286
    :goto_1
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Allocated fragment index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 125287
    :cond_4
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    iget-object v1, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->setIndex(ILandroid/support/v4/app/Fragment;)V

    .line 125288
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private f(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 125247
    iget v0, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    if-gez v0, :cond_0

    .line 125248
    :goto_0
    return-void

    .line 125249
    :cond_0
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Freeing fragment index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125250
    :cond_1
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 125251
    const/4 v0, -0x1

    iput v0, p1, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    .line 125252
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 125253
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    .line 125254
    :cond_2
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    iget v1, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125255
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mWho:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0k3;->a(Ljava/lang/String;)V

    .line 125256
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 125257
    const/4 v0, -0x1

    iput v0, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    .line 125258
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mWho:Ljava/lang/String;

    .line 125259
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 125260
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mRemoving:Z

    .line 125261
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mResumed:Z

    .line 125262
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    .line 125263
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 125264
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mRestored:Z

    .line 125265
    iput v1, p1, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    .line 125266
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 125267
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    .line 125268
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    .line 125269
    iput v1, p1, Landroid/support/v4/app/Fragment;->mFragmentId:I

    .line 125270
    iput v1, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 125271
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    .line 125272
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    .line 125273
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    .line 125274
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mRetaining:Z

    .line 125275
    iput-object v2, p1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    .line 125276
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mLoadersStarted:Z

    .line 125277
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mCheckedForLoaderManager:Z

    .line 125278
    goto :goto_0
.end method

.method private g(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 125238
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 125239
    :cond_0
    :goto_0
    return-void

    .line 125240
    :cond_1
    iget-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 125241
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    .line 125242
    :goto_1
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    iget-object v1, p0, LX/0jz;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 125243
    iget-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 125244
    iget-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    .line 125245
    const/4 v0, 0x0

    iput-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    goto :goto_0

    .line 125246
    :cond_2
    iget-object v0, p0, LX/0jz;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method private h(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 125216
    iget-object v0, p0, LX/0jz;->w:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 125217
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/0jz;->w:Landroid/os/Bundle;

    .line 125218
    :cond_0
    iget-object v0, p0, LX/0jz;->w:Landroid/os/Bundle;

    .line 125219
    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 125220
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v2, :cond_1

    .line 125221
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->k()Landroid/os/Parcelable;

    move-result-object v2

    .line 125222
    if-eqz v2, :cond_1

    .line 125223
    const-string v3, "android:support:fragments"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 125224
    :cond_1
    iget-object v0, p0, LX/0jz;->w:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 125225
    iget-object v0, p0, LX/0jz;->w:Landroid/os/Bundle;

    .line 125226
    iput-object v1, p0, LX/0jz;->w:Landroid/os/Bundle;

    .line 125227
    :goto_0
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 125228
    invoke-direct {p0, p1}, LX/0jz;->g(Landroid/support/v4/app/Fragment;)V

    .line 125229
    :cond_2
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    if-eqz v1, :cond_4

    .line 125230
    if-nez v0, :cond_3

    .line 125231
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 125232
    :cond_3
    const-string v1, "android:view_state"

    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 125233
    :cond_4
    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->mUserVisibleHint:Z

    if-nez v1, :cond_6

    .line 125234
    if-nez v0, :cond_5

    .line 125235
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 125236
    :cond_5
    const-string v1, "android:user_visible_hint"

    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->mUserVisibleHint:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 125237
    :cond_6
    return-object v0

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/BackStackRecord;)I
    .locals 3

    .prologue
    .line 125202
    monitor-enter p0

    .line 125203
    :try_start_0
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 125204
    :cond_0
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 125205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    .line 125206
    :cond_1
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 125207
    sget-boolean v1, LX/0jz;->a:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125208
    :cond_2
    iget-object v1, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125209
    monitor-exit p0

    .line 125210
    :goto_0
    return v0

    .line 125211
    :cond_3
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    iget-object v1, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 125212
    sget-boolean v1, LX/0jz;->a:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125213
    :cond_4
    iget-object v1, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 125214
    monitor-exit p0

    goto :goto_0

    .line 125215
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()LX/0hH;
    .locals 1

    .prologue
    .line 125201
    new-instance v0, Landroid/support/v4/app/BackStackRecord;

    invoke-direct {v0, p0}, Landroid/support/v4/app/BackStackRecord;-><init>(LX/0jz;)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 125195
    iget v1, p1, Landroid/support/v4/app/Fragment;->mIndex:I

    if-gez v1, :cond_0

    .line 125196
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not currently in the FragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125197
    :cond_0
    iget v1, p1, Landroid/support/v4/app/Fragment;->mState:I

    if-lez v1, :cond_1

    .line 125198
    invoke-direct {p0, p1}, LX/0jz;->h(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;

    move-result-object v1

    .line 125199
    if-eqz v1, :cond_1

    new-instance v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v4/app/Fragment$SavedState;-><init>(Landroid/os/Bundle;)V

    .line 125200
    :cond_1
    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 125957
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 125958
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 125959
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125960
    if-eqz v0, :cond_1

    iget v2, v0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    if-ne v2, p1, :cond_1

    .line 125961
    :cond_0
    :goto_1
    return-object v0

    .line 125962
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 125963
    :cond_2
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 125964
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 125965
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125966
    if-eqz v0, :cond_3

    iget v2, v0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    if-eq v2, p1, :cond_0

    .line 125967
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 125968
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 125443
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 125444
    if-ne v1, v0, :cond_1

    .line 125445
    const/4 v0, 0x0

    .line 125446
    :cond_0
    :goto_0
    return-object v0

    .line 125447
    :cond_1
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 125448
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125449
    :cond_2
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125450
    if-nez v0, :cond_0

    .line 125451
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 125945
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 125946
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 125947
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125948
    if-eqz v0, :cond_1

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125949
    :cond_0
    :goto_1
    return-object v0

    .line 125950
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 125951
    :cond_2
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 125952
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 125953
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125954
    if-eqz v0, :cond_3

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 125955
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 125956
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 125941
    if-gez p1, :cond_0

    .line 125942
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125943
    :cond_0
    new-instance v0, Landroid/support/v4/app/FragmentManagerImpl$4;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/v4/app/FragmentManagerImpl$4;-><init>(LX/0jz;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0jz;->a(Ljava/lang/Runnable;Z)V

    .line 125944
    return-void
.end method

.method public final a(IIIZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 125923
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 125924
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125925
    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, LX/0jz;->m:I

    if-ne v0, p1, :cond_2

    .line 125926
    :cond_1
    :goto_0
    return-void

    .line 125927
    :cond_2
    iput p1, p0, LX/0jz;->m:I

    .line 125928
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    move v7, v5

    .line 125929
    :goto_1
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 125930
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 125931
    if-eqz v1, :cond_5

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 125932
    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 125933
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    if-eqz v0, :cond_5

    .line 125934
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->a()Z

    move-result v0

    or-int/2addr v7, v0

    move v1, v7

    .line 125935
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1

    .line 125936
    :cond_3
    if-nez v7, :cond_4

    .line 125937
    invoke-virtual {p0}, LX/0jz;->h()V

    .line 125938
    :cond_4
    iget-boolean v0, p0, LX/0jz;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    if-eqz v0, :cond_1

    iget v0, p0, LX/0jz;->m:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 125939
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->l()V

    .line 125940
    iput-boolean v5, p0, LX/0jz;->q:Z

    goto :goto_0

    :cond_5
    move v1, v7

    goto :goto_2
.end method

.method public final a(LX/0fN;)V
    .locals 1

    .prologue
    .line 125919
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 125920
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    .line 125921
    :cond_0
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125922
    return-void
.end method

.method public final a(LX/0k3;LX/0k1;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 125914
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125915
    :cond_0
    iput-object p1, p0, LX/0jz;->n:LX/0k3;

    .line 125916
    iput-object p2, p0, LX/0jz;->o:LX/0k1;

    .line 125917
    iput-object p3, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    .line 125918
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 125905
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 125906
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 125907
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125908
    if-eqz v0, :cond_0

    .line 125909
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 125910
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v2, :cond_0

    .line 125911
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2, p1}, LX/0jz;->a(Landroid/content/res/Configuration;)V

    .line 125912
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125913
    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 125901
    iget v0, p3, Landroid/support/v4/app/Fragment;->mIndex:I

    if-gez v0, :cond_0

    .line 125902
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125903
    :cond_0
    iget v0, p3, Landroid/support/v4/app/Fragment;->mIndex:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125904
    return-void
.end method

.method public final a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 125770
    if-nez p1, :cond_1

    .line 125771
    :cond_0
    :goto_0
    return-void

    .line 125772
    :cond_1
    check-cast p1, Landroid/support/v4/app/FragmentManagerState;

    .line 125773
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    if-eqz v0, :cond_0

    .line 125774
    if-eqz p2, :cond_4

    move v1, v2

    .line 125775
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 125776
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125777
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: re-attaching retained "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125778
    :cond_2
    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    iget v4, v0, Landroid/support/v4/app/Fragment;->mIndex:I

    aget-object v3, v3, v4

    .line 125779
    iput-object v0, v3, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    .line 125780
    iput-object v6, v0, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    .line 125781
    iput v2, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    .line 125782
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 125783
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 125784
    iput-object v6, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    .line 125785
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    if-eqz v4, :cond_3

    .line 125786
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    iget-object v5, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v5}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 125787
    iget-object v4, v3, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    const-string v5, "android:view_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v4

    iput-object v4, v0, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    .line 125788
    iget-object v3, v3, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    iput-object v3, v0, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    .line 125789
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 125790
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    .line 125791
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 125792
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_5
    move v0, v2

    .line 125793
    :goto_2
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 125794
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    aget-object v1, v1, v0

    .line 125795
    if-eqz v1, :cond_7

    .line 125796
    iget-object v3, p0, LX/0jz;->n:LX/0k3;

    iget-object v4, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    .line 125797
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    if-eqz v5, :cond_1b

    .line 125798
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    .line 125799
    :goto_3
    move-object v3, v5

    .line 125800
    sget-boolean v4, LX/0jz;->a:Z

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: active #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125801
    :cond_6
    iget-object v4, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125802
    iput-object v6, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    .line 125803
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 125804
    :cond_7
    iget-object v1, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125805
    iget-object v1, p0, LX/0jz;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_8

    .line 125806
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/0jz;->g:Ljava/util/ArrayList;

    .line 125807
    :cond_8
    sget-boolean v1, LX/0jz;->a:Z

    if-eqz v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "restoreAllState: avail #"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125808
    :cond_9
    iget-object v1, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 125809
    :cond_a
    if-eqz p2, :cond_d

    move v3, v2

    .line 125810
    :goto_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_d

    .line 125811
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125812
    iget v1, v0, Landroid/support/v4/app/Fragment;->mTargetIndex:I

    if-ltz v1, :cond_b

    .line 125813
    iget v1, v0, Landroid/support/v4/app/Fragment;->mTargetIndex:I

    iget-object v4, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    .line 125814
    iget-object v1, p0, LX/0jz;->f:Ljava/util/ArrayList;

    iget v4, v0, Landroid/support/v4/app/Fragment;->mTargetIndex:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    .line 125815
    :cond_b
    :goto_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 125816
    :cond_c
    const-string v1, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Re-attaching retained fragment "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target no longer exists: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/support/v4/app/Fragment;->mTargetIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 125817
    iput-object v6, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    goto :goto_6

    .line 125818
    :cond_d
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    if-eqz v0, :cond_11

    .line 125819
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    move v1, v2

    .line 125820
    :goto_7
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_12

    .line 125821
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125822
    if-nez v0, :cond_e

    .line 125823
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Landroid/support/v4/app/FragmentManagerState;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125824
    :cond_e
    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 125825
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_f

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: added #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125826
    :cond_f
    iget-object v3, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 125827
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125828
    :cond_10
    iget-object v3, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125829
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 125830
    :cond_11
    iput-object v6, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    .line 125831
    :cond_12
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    if-eqz v0, :cond_1a

    .line 125832
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    move v0, v2

    .line 125833
    :goto_8
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 125834
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    aget-object v1, v1, v0

    const/4 p2, 0x1

    const/4 v5, 0x0

    .line 125835
    new-instance v9, Landroid/support/v4/app/BackStackRecord;

    invoke-direct {v9, p0}, Landroid/support/v4/app/BackStackRecord;-><init>(LX/0jz;)V

    move v4, v5

    move v3, v5

    .line 125836
    :goto_9
    iget-object v6, v1, Landroid/support/v4/app/BackStackState;->a:[I

    array-length v6, v6

    if-ge v3, v6, :cond_17

    .line 125837
    new-instance v10, LX/0xN;

    invoke-direct {v10}, LX/0xN;-><init>()V

    .line 125838
    iget-object v6, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v7, v3, 0x1

    aget v3, v6, v3

    iput v3, v10, LX/0xN;->c:I

    .line 125839
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_13

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Instantiate "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " op #"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " base fragment #"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v1, Landroid/support/v4/app/BackStackState;->a:[I

    aget v6, v6, v7

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125840
    :cond_13
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v6, v7, 0x1

    aget v3, v3, v7

    .line 125841
    if-ltz v3, :cond_15

    .line 125842
    iget-object v7, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/Fragment;

    .line 125843
    iput-object v3, v10, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 125844
    :goto_a
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v7, v6, 0x1

    aget v3, v3, v6

    iput v3, v10, LX/0xN;->e:I

    .line 125845
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v6, v7, 0x1

    aget v3, v3, v7

    iput v3, v10, LX/0xN;->f:I

    .line 125846
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v7, v6, 0x1

    aget v3, v3, v6

    iput v3, v10, LX/0xN;->g:I

    .line 125847
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v8, v7, 0x1

    aget v3, v3, v7

    iput v3, v10, LX/0xN;->h:I

    .line 125848
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v6, v8, 0x1

    aget v11, v3, v8

    .line 125849
    if-lez v11, :cond_16

    .line 125850
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v10, LX/0xN;->i:Ljava/util/ArrayList;

    move v7, v5

    .line 125851
    :goto_b
    if-ge v7, v11, :cond_16

    .line 125852
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_14

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Instantiate "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " set remove fragment #"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v8, v1, Landroid/support/v4/app/BackStackState;->a:[I

    aget v8, v8, v6

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125853
    :cond_14
    iget-object v3, p0, LX/0jz;->f:Ljava/util/ArrayList;

    iget-object v12, v1, Landroid/support/v4/app/BackStackState;->a:[I

    add-int/lit8 v8, v6, 0x1

    aget v6, v12, v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/Fragment;

    .line 125854
    iget-object v6, v10, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125855
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v6, v8

    goto :goto_b

    .line 125856
    :cond_15
    const/4 v3, 0x0

    iput-object v3, v10, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    goto :goto_a

    .line 125857
    :cond_16
    invoke-virtual {v9, v10}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 125858
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v6

    .line 125859
    goto/16 :goto_9

    .line 125860
    :cond_17
    iget v3, v1, Landroid/support/v4/app/BackStackState;->b:I

    iput v3, v9, Landroid/support/v4/app/BackStackRecord;->i:I

    .line 125861
    iget v3, v1, Landroid/support/v4/app/BackStackState;->c:I

    iput v3, v9, Landroid/support/v4/app/BackStackRecord;->j:I

    .line 125862
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->d:Ljava/lang/String;

    iput-object v3, v9, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    .line 125863
    iget v3, v1, Landroid/support/v4/app/BackStackState;->e:I

    iput v3, v9, Landroid/support/v4/app/BackStackRecord;->o:I

    .line 125864
    iput-boolean p2, v9, Landroid/support/v4/app/BackStackRecord;->k:Z

    .line 125865
    iget v3, v1, Landroid/support/v4/app/BackStackState;->f:I

    iput v3, v9, Landroid/support/v4/app/BackStackRecord;->p:I

    .line 125866
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->g:Ljava/lang/CharSequence;

    iput-object v3, v9, Landroid/support/v4/app/BackStackRecord;->q:Ljava/lang/CharSequence;

    .line 125867
    iget v3, v1, Landroid/support/v4/app/BackStackState;->h:I

    iput v3, v9, Landroid/support/v4/app/BackStackRecord;->r:I

    .line 125868
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->i:Ljava/lang/CharSequence;

    iput-object v3, v9, Landroid/support/v4/app/BackStackRecord;->s:Ljava/lang/CharSequence;

    .line 125869
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->j:Ljava/util/ArrayList;

    iput-object v3, v9, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    .line 125870
    iget-object v3, v1, Landroid/support/v4/app/BackStackState;->k:Ljava/util/ArrayList;

    iput-object v3, v9, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    .line 125871
    invoke-virtual {v9, p2}, Landroid/support/v4/app/BackStackRecord;->b(I)V

    .line 125872
    move-object v1, v9

    .line 125873
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_18

    .line 125874
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: back stack #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/support/v4/app/BackStackRecord;->o:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125875
    new-instance v3, LX/0xJ;

    const-string v4, "FragmentManager"

    invoke-direct {v3, v4}, LX/0xJ;-><init>(Ljava/lang/String;)V

    .line 125876
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 125877
    const-string v3, "  "

    invoke-virtual {v1, v3, v4, v2}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 125878
    :cond_18
    iget-object v3, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125879
    iget v3, v1, Landroid/support/v4/app/BackStackRecord;->o:I

    if-ltz v3, :cond_19

    .line 125880
    iget v3, v1, Landroid/support/v4/app/BackStackRecord;->o:I

    invoke-direct {p0, v3, v1}, LX/0jz;->a(ILandroid/support/v4/app/BackStackRecord;)V

    .line 125881
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_8

    .line 125882
    :cond_1a
    iput-object v6, p0, LX/0jz;->h:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 125883
    :cond_1b
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v5, :cond_1c

    .line 125884
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    invoke-virtual {v3}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 125885
    :cond_1c
    invoke-virtual {v3}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v5

    iget-object v7, v1, Landroid/support/v4/app/FragmentState;->a:Ljava/lang/String;

    iget-object v8, v1, Landroid/support/v4/app/FragmentState;->j:Landroid/os/Bundle;

    invoke-static {v5, v7, v8}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    iput-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    .line 125886
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    if-eqz v5, :cond_1d

    .line 125887
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    invoke-virtual {v3}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 125888
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget-object v7, v1, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    iput-object v7, v5, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    .line 125889
    :cond_1d
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget v7, v1, Landroid/support/v4/app/FragmentState;->b:I

    invoke-virtual {v5, v7, v4}, Landroid/support/v4/app/Fragment;->setIndex(ILandroid/support/v4/app/Fragment;)V

    .line 125890
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget v7, v1, Landroid/support/v4/app/FragmentState;->c:I

    iput v7, v5, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    .line 125891
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget-boolean v7, v1, Landroid/support/v4/app/FragmentState;->d:Z

    iput-boolean v7, v5, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    .line 125892
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    const/4 v7, 0x1

    iput-boolean v7, v5, Landroid/support/v4/app/Fragment;->mRestored:Z

    .line 125893
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget v7, v1, Landroid/support/v4/app/FragmentState;->e:I

    iput v7, v5, Landroid/support/v4/app/Fragment;->mFragmentId:I

    .line 125894
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget v7, v1, Landroid/support/v4/app/FragmentState;->f:I

    iput v7, v5, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 125895
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget-object v7, v1, Landroid/support/v4/app/FragmentState;->g:Ljava/lang/String;

    iput-object v7, v5, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    .line 125896
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget-boolean v7, v1, Landroid/support/v4/app/FragmentState;->h:Z

    iput-boolean v7, v5, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    .line 125897
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    iget-boolean v7, v1, Landroid/support/v4/app/FragmentState;->i:Z

    iput-boolean v7, v5, Landroid/support/v4/app/Fragment;->mDetached:Z

    .line 125898
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, LX/0k3;->o()LX/0jz;

    move-result-object v7

    iput-object v7, v5, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 125899
    sget-boolean v5, LX/0jz;->a:Z

    if-eqz v5, :cond_1e

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Instantiated fragment "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125900
    :cond_1e
    iget-object v5, v1, Landroid/support/v4/app/FragmentState;->l:Landroid/support/v4/app/Fragment;

    goto/16 :goto_3
.end method

.method public final a(Landroid/support/v4/app/Fragment;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 125756
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "remove: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " nesting="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125757
    :cond_0
    iget v0, p1, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 125758
    if-nez v0, :cond_5

    move v0, v1

    .line 125759
    :goto_1
    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_4

    .line 125760
    :cond_1
    iget-object v2, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 125761
    iget-object v2, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 125762
    :cond_2
    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v2, :cond_3

    .line 125763
    iput-boolean v1, p0, LX/0jz;->q:Z

    .line 125764
    :cond_3
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 125765
    iput-boolean v1, p1, Landroid/support/v4/app/Fragment;->mRemoving:Z

    .line 125766
    if-eqz v0, :cond_6

    move v2, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 125767
    :cond_4
    return-void

    :cond_5
    move v0, v5

    .line 125768
    goto :goto_1

    :cond_6
    move v2, v1

    .line 125769
    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;IIIZ)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 125609
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    .line 125610
    :cond_1
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mRemoving:Z

    if-eqz v0, :cond_2

    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    if-le p2, v0, :cond_2

    .line 125611
    iget p2, p1, Landroid/support/v4/app/Fragment;->mState:I

    .line 125612
    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDeferStart:Z

    if-eqz v0, :cond_3

    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    if-ge v0, v8, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    .line 125613
    :cond_3
    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    if-ge v0, p2, :cond_1e

    .line 125614
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mInLayout:Z

    if-nez v0, :cond_4

    .line 125615
    :goto_0
    return-void

    .line 125616
    :cond_4
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 125617
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    .line 125618
    iget v2, p1, Landroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 125619
    :cond_5
    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 125620
    :cond_6
    :goto_1
    iput p2, p1, Landroid/support/v4/app/Fragment;->mState:I

    goto :goto_0

    .line 125621
    :pswitch_0
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125622
    :cond_7
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    if-eqz v0, :cond_9

    .line 125623
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    iget-object v1, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 125624
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    .line 125625
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-virtual {p0, v0, v1}, LX/0jz;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    .line 125626
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_8

    .line 125627
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/support/v4/app/Fragment;->mTargetRequestCode:I

    .line 125628
    :cond_8
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Landroid/support/v4/app/Fragment;->mUserVisibleHint:Z

    .line 125629
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mUserVisibleHint:Z

    if-nez v0, :cond_9

    .line 125630
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->mDeferStart:Z

    .line 125631
    if-le p2, v6, :cond_9

    move p2, v6

    .line 125632
    :cond_9
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    .line 125633
    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    .line 125634
    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    :goto_2
    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 125635
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    .line 125636
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 125637
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    if-nez v0, :cond_b

    .line 125638
    new-instance v0, LX/3qP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3qP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125639
    :cond_a
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    goto :goto_2

    .line 125640
    :cond_b
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_c

    .line 125641
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    .line 125642
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 125643
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    if-nez v0, :cond_c

    .line 125644
    new-instance v0, LX/3qP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3qP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125645
    :cond_c
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1b

    .line 125646
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0, p1}, LX/0k3;->a(Landroid/support/v4/app/Fragment;)V

    .line 125647
    :goto_3
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mRetaining:Z

    if-nez v0, :cond_d

    .line 125648
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->performCreate(Landroid/os/Bundle;)V

    .line 125649
    :cond_d
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mRetaining:Z

    .line 125650
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    if-eqz v0, :cond_f

    .line 125651
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7, v1}, Landroid/support/v4/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    .line 125652
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_1c

    .line 125653
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    .line 125654
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-static {v0}, LX/18y;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    .line 125655
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-eqz v0, :cond_e

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125656
    :cond_e
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 125657
    :cond_f
    :goto_4
    :pswitch_1
    if-le p2, v5, :cond_17

    .line 125658
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto ACTIVITY_CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125659
    :cond_10
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    if-nez v0, :cond_15

    .line 125660
    iget v0, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    if-eqz v0, :cond_33

    .line 125661
    iget-object v0, p0, LX/0jz;->o:LX/0k1;

    iget v1, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    invoke-interface {v0, v1}, LX/0k1;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 125662
    if-nez v0, :cond_11

    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->mRestored:Z

    if-nez v1, :cond_11

    .line 125663
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No view found for id 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, LX/0jz;->d(Landroid/support/v4/app/Fragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") for fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125664
    :cond_11
    :goto_5
    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    .line 125665
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Landroid/support/v4/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    .line 125666
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v1, :cond_1d

    .line 125667
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    .line 125668
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-static {v1}, LX/18y;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    .line 125669
    if-eqz v0, :cond_13

    .line 125670
    invoke-direct {p0, p1, p3, v5, p4}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 125671
    if-eqz v1, :cond_12

    .line 125672
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 125673
    :cond_12
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 125674
    :cond_13
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-eqz v0, :cond_14

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125675
    :cond_14
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 125676
    :cond_15
    :goto_6
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->performActivityCreated(Landroid/os/Bundle;)V

    .line 125677
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_16

    .line 125678
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->restoreViewState(Landroid/os/Bundle;)V

    .line 125679
    :cond_16
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    .line 125680
    :cond_17
    :pswitch_2
    if-le p2, v6, :cond_19

    .line 125681
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_18

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto STARTED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125682
    :cond_18
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performStart()V

    .line 125683
    :cond_19
    :pswitch_3
    if-le p2, v8, :cond_6

    .line 125684
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto RESUMED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125685
    :cond_1a
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->mResumed:Z

    .line 125686
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performResume()V

    .line 125687
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    .line 125688
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/16 :goto_1

    .line 125689
    :cond_1b
    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_3

    .line 125690
    :cond_1c
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    goto/16 :goto_4

    .line 125691
    :cond_1d
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    goto :goto_6

    .line 125692
    :cond_1e
    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    if-le v0, p2, :cond_6

    .line 125693
    iget v0, p1, Landroid/support/v4/app/Fragment;->mState:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 125694
    :cond_1f
    :goto_7
    :pswitch_4
    if-gtz p2, :cond_6

    .line 125695
    iget-boolean v0, p0, LX/0jz;->s:Z

    if-eqz v0, :cond_20

    .line 125696
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_20

    .line 125697
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    .line 125698
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    .line 125699
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 125700
    :cond_20
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_2d

    .line 125701
    iput p2, p1, Landroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    move p2, v5

    .line 125702
    goto/16 :goto_1

    .line 125703
    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_22

    .line 125704
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_21

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom RESUMED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125705
    :cond_21
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performPause()V

    .line 125706
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mResumed:Z

    .line 125707
    :cond_22
    :pswitch_6
    if-ge p2, v8, :cond_24

    .line 125708
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_23

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom STARTED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125709
    :cond_23
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performStop()V

    .line 125710
    :cond_24
    :pswitch_7
    if-ge p2, v6, :cond_28

    .line 125711
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_25

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom STOPPED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125712
    :cond_25
    const/4 v4, 0x0

    .line 125713
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v0, :cond_26

    .line 125714
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->s()V

    .line 125715
    :cond_26
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mLoadersStarted:Z

    if-eqz v0, :cond_28

    .line 125716
    iput-boolean v4, p1, Landroid/support/v4/app/Fragment;->mLoadersStarted:Z

    .line 125717
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mCheckedForLoaderManager:Z

    if-nez v0, :cond_27

    .line 125718
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v4/app/Fragment;->mCheckedForLoaderManager:Z

    .line 125719
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mWho:Ljava/lang/String;

    iget-boolean v2, p1, Landroid/support/v4/app/Fragment;->mLoadersStarted:Z

    invoke-virtual {v0, v1, v2, v4}, LX/0k3;->a(Ljava/lang/String;ZZ)LX/0k4;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    .line 125720
    :cond_27
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    if-eqz v0, :cond_28

    .line 125721
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->n()Z

    move-result v0

    if-nez v0, :cond_34

    .line 125722
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->c()V

    .line 125723
    :cond_28
    :goto_8
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_1f

    .line 125724
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_29

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom ACTIVITY_CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125725
    :cond_29
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_2a

    .line 125726
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->q()Z

    move-result v0

    if-nez v0, :cond_2a

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    if-nez v0, :cond_2a

    .line 125727
    invoke-direct {p0, p1}, LX/0jz;->g(Landroid/support/v4/app/Fragment;)V

    .line 125728
    :cond_2a
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performDestroyView()V

    .line 125729
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_2c

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2c

    .line 125730
    iget v0, p0, LX/0jz;->m:I

    if-lez v0, :cond_32

    iget-boolean v0, p0, LX/0jz;->s:Z

    if-nez v0, :cond_32

    .line 125731
    invoke-direct {p0, p1, p3, v3, p4}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 125732
    :goto_9
    if-eqz v0, :cond_2b

    .line 125733
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    iput-object v1, p1, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    .line 125734
    iput p2, p1, Landroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    .line 125735
    new-instance v1, LX/3pD;

    invoke-direct {v1, p0, p1}, LX/3pD;-><init>(LX/0jz;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 125736
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 125737
    :cond_2b
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 125738
    :cond_2c
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    .line 125739
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    .line 125740
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mInnerView:Landroid/view/View;

    goto/16 :goto_7

    .line 125741
    :cond_2d
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_2e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125742
    :cond_2e
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mRetaining:Z

    if-nez v0, :cond_2f

    .line 125743
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->performDestroy()V

    .line 125744
    :cond_2f
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    .line 125745
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 125746
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mCalled:Z

    if-nez v0, :cond_30

    .line 125747
    new-instance v0, LX/3qP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3qP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125748
    :cond_30
    if-nez p5, :cond_6

    .line 125749
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mRetaining:Z

    if-nez v0, :cond_31

    .line 125750
    invoke-direct {p0, p1}, LX/0jz;->f(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    .line 125751
    :cond_31
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    .line 125752
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    .line 125753
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 125754
    iput-object v7, p1, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    goto/16 :goto_1

    :cond_32
    move-object v0, v7

    goto :goto_9

    :cond_33
    move-object v0, v7

    goto/16 :goto_5

    .line 125755
    :cond_34
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->d()V

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/app/Fragment;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 125594
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 125595
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    .line 125596
    :cond_0
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "add: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125597
    :cond_1
    invoke-direct {p0, p1}, LX/0jz;->e(Landroid/support/v4/app/Fragment;)V

    .line 125598
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    if-nez v0, :cond_4

    .line 125599
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125600
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125601
    :cond_2
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125602
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 125603
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/Fragment;->mRemoving:Z

    .line 125604
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v0, :cond_3

    .line 125605
    iput-boolean v2, p0, LX/0jz;->q:Z

    .line 125606
    :cond_3
    if-eqz p2, :cond_4

    .line 125607
    invoke-virtual {p0, p1}, LX/0jz;->c(Landroid/support/v4/app/Fragment;)V

    .line 125608
    :cond_4
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Z)V
    .locals 3

    .prologue
    .line 125581
    if-nez p2, :cond_0

    .line 125582
    invoke-direct {p0}, LX/0jz;->A()V

    .line 125583
    :cond_0
    monitor-enter p0

    .line 125584
    :try_start_0
    iget-boolean v0, p0, LX/0jz;->s:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    if-nez v0, :cond_2

    .line 125585
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FragmentHost has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125586
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 125587
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 125588
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    .line 125589
    :cond_3
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125590
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 125591
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/0jz;->y:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 125592
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/0jz;->y:Ljava/lang/Runnable;

    const v2, -0x5f6dce43

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 125593
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 125579
    new-instance v0, Landroid/support/v4/app/FragmentManagerImpl$3;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/v4/app/FragmentManagerImpl$3;-><init>(LX/0jz;Ljava/lang/String;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0jz;->a(Ljava/lang/Runnable;Z)V

    .line 125580
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 125495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 125496
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 125497
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 125498
    if-lez v4, :cond_1

    .line 125499
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125500
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125501
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 125502
    :goto_0
    if-ge v2, v4, :cond_1

    .line 125503
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125504
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 125505
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125506
    if-eqz v0, :cond_0

    .line 125507
    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/Fragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 125508
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 125509
    :cond_1
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 125510
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 125511
    if-lez v4, :cond_2

    .line 125512
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 125513
    :goto_1
    if-ge v2, v4, :cond_2

    .line 125514
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125515
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 125516
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125517
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 125518
    :cond_2
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 125519
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 125520
    if-lez v4, :cond_3

    .line 125521
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 125522
    :goto_2
    if-ge v2, v4, :cond_3

    .line 125523
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125524
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 125525
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125526
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 125527
    :cond_3
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 125528
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 125529
    if-lez v4, :cond_4

    .line 125530
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 125531
    :goto_3
    if-ge v2, v4, :cond_4

    .line 125532
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    .line 125533
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 125534
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/BackStackRecord;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125535
    invoke-virtual {v0, v3, p3}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 125536
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 125537
    :cond_4
    monitor-enter p0

    .line 125538
    :try_start_0
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 125539
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 125540
    if-lez v3, :cond_5

    .line 125541
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 125542
    :goto_4
    if-ge v2, v3, :cond_5

    .line 125543
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    .line 125544
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 125545
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125546
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 125547
    :cond_5
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 125548
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125549
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125550
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125551
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 125552
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 125553
    if-lez v2, :cond_7

    .line 125554
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125555
    :goto_5
    if-ge v1, v2, :cond_7

    .line 125556
    iget-object v0, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 125557
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 125558
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125559
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 125560
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 125561
    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125562
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125563
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0jz;->o:LX/0k1;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125564
    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_8

    .line 125565
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 125566
    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, LX/0jz;->m:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 125567
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0jz;->r:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 125568
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0jz;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 125569
    iget-boolean v0, p0, LX/0jz;->q:Z

    if-eqz v0, :cond_9

    .line 125570
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125571
    iget-boolean v0, p0, LX/0jz;->q:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 125572
    :cond_9
    iget-object v0, p0, LX/0jz;->t:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 125573
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125574
    iget-object v0, p0, LX/0jz;->t:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125575
    :cond_a
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 125576
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 125577
    iget-object v0, p0, LX/0jz;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125578
    :cond_b
    return-void
.end method

.method public final a(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 125479
    iget-object v1, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    move v1, v0

    move v2, v0

    .line 125480
    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 125481
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125482
    if-eqz v0, :cond_2

    .line 125483
    const/4 v3, 0x0

    .line 125484
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v4, :cond_1

    .line 125485
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v4, :cond_0

    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v4, :cond_0

    .line 125486
    const/4 v3, 0x1

    .line 125487
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 125488
    :cond_0
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v4, :cond_1

    .line 125489
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v4, p1}, LX/0jz;->a(Landroid/view/Menu;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 125490
    :cond_1
    move v0, v3

    .line 125491
    if-eqz v0, :cond_2

    .line 125492
    const/4 v2, 0x1

    .line 125493
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v2, v0

    .line 125494
    :cond_4
    return v2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 125452
    const/4 v1, 0x0

    .line 125453
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    move v3, v4

    move v2, v4

    .line 125454
    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 125455
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125456
    if-eqz v0, :cond_3

    .line 125457
    const/4 v5, 0x0

    .line 125458
    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v6, :cond_1

    .line 125459
    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v6, :cond_0

    iget-boolean v6, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v6, :cond_0

    .line 125460
    const/4 v5, 0x1

    .line 125461
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->dispatchOnCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 125462
    :cond_0
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v6, :cond_1

    .line 125463
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v6, p1, p2}, LX/0jz;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v6

    or-int/2addr v5, v6

    .line 125464
    :cond_1
    move v5, v5

    .line 125465
    if-eqz v5, :cond_3

    .line 125466
    const/4 v2, 0x1

    .line 125467
    if-nez v1, :cond_2

    .line 125468
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125469
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move v0, v2

    .line 125470
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v4

    .line 125471
    :cond_5
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    .line 125472
    :goto_1
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 125473
    iget-object v0, p0, LX/0jz;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125474
    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 125475
    :cond_6
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onDestroyOptionsMenu()V

    .line 125476
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 125477
    :cond_8
    iput-object v1, p0, LX/0jz;->i:Ljava/util/ArrayList;

    .line 125478
    return v2
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 125305
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v1, v2

    .line 125306
    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 125307
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125308
    if-eqz v0, :cond_2

    .line 125309
    const/4 v3, 0x1

    .line 125310
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v4, :cond_4

    .line 125311
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v4, :cond_3

    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v4, :cond_3

    .line 125312
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 125313
    :cond_0
    :goto_1
    move v0, v3

    .line 125314
    if-eqz v0, :cond_2

    .line 125315
    const/4 v2, 0x1

    .line 125316
    :cond_1
    return v2

    .line 125317
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125318
    :cond_3
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v4, :cond_4

    .line 125319
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v4, p1}, LX/0jz;->a(Landroid/view/MenuItem;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 125320
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;II)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 125394
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 125395
    :cond_0
    :goto_0
    return v3

    .line 125396
    :cond_1
    if-nez p1, :cond_3

    if-gez p2, :cond_3

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_3

    .line 125397
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 125398
    if-ltz v0, :cond_0

    .line 125399
    iget-object v1, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    .line 125400
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 125401
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 125402
    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 125403
    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/support/v4/app/BackStackRecord;->a(ZLX/0xK;Landroid/util/SparseArray;Landroid/util/SparseArray;)LX/0xK;

    .line 125404
    :cond_2
    invoke-direct {p0}, LX/0jz;->B()V

    move v3, v2

    .line 125405
    goto :goto_0

    .line 125406
    :cond_3
    const/4 v0, -0x1

    .line 125407
    if-nez p1, :cond_4

    if-ltz p2, :cond_b

    .line 125408
    :cond_4
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 125409
    :goto_1
    if-ltz v1, :cond_7

    .line 125410
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    .line 125411
    if-eqz p1, :cond_5

    .line 125412
    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    move-object v5, v5

    .line 125413
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 125414
    :cond_5
    if-ltz p2, :cond_6

    iget v0, v0, Landroid/support/v4/app/BackStackRecord;->o:I

    if-eq p2, v0, :cond_7

    .line 125415
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 125416
    goto :goto_1

    .line 125417
    :cond_7
    if-ltz v1, :cond_0

    .line 125418
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_a

    .line 125419
    add-int/lit8 v1, v1, -0x1

    .line 125420
    :goto_2
    if-ltz v1, :cond_a

    .line 125421
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    .line 125422
    if-eqz p1, :cond_8

    .line 125423
    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    move-object v5, v5

    .line 125424
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_8
    if-ltz p2, :cond_a

    iget v0, v0, Landroid/support/v4/app/BackStackRecord;->o:I

    if-ne p2, v0, :cond_a

    .line 125425
    :cond_9
    add-int/lit8 v1, v1, -0x1

    .line 125426
    goto :goto_2

    :cond_a
    move v0, v1

    .line 125427
    :cond_b
    iget-object v1, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 125428
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 125429
    iget-object v1, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_c

    .line 125430
    iget-object v5, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125431
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 125432
    :cond_c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 125433
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 125434
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    move v1, v3

    .line 125435
    :goto_4
    if-gt v1, v7, :cond_d

    .line 125436
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    invoke-virtual {v0, v8, v9}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 125437
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_d
    move-object v5, v4

    move v4, v3

    .line 125438
    :goto_5
    if-gt v4, v7, :cond_2

    .line 125439
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Popping back stack state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125440
    :cond_e
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    if-ne v4, v7, :cond_f

    move v1, v2

    :goto_6
    invoke-virtual {v0, v1, v5, v8, v9}, Landroid/support/v4/app/BackStackRecord;->a(ZLX/0xK;Landroid/util/SparseArray;Landroid/util/SparseArray;)LX/0xK;

    move-result-object v1

    .line 125441
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v5, v1

    goto :goto_5

    :cond_f
    move v1, v3

    .line 125442
    goto :goto_6
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 124810
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 124811
    :cond_0
    return p1

    .line 124812
    :cond_1
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124813
    if-eqz v0, :cond_2

    .line 124814
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz p0, :cond_3

    .line 124815
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {p0, p1}, LX/0jz;->b(I)I

    move-result p0

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    .line 124816
    :goto_1
    iget p0, v0, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    move p1, p0

    .line 124817
    goto :goto_0

    .line 124818
    :cond_3
    add-int/lit8 p0, p1, 0x1

    iput p0, v0, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    goto :goto_1
.end method

.method public final b(LX/0fN;)V
    .locals 1

    .prologue
    .line 124927
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 124928
    iget-object v0, p0, LX/0jz;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 124929
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v4/app/BackStackRecord;)V
    .locals 1

    .prologue
    .line 124922
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 124923
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    .line 124924
    :cond_0
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124925
    invoke-direct {p0}, LX/0jz;->B()V

    .line 124926
    return-void
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 124916
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDeferStart:Z

    if-eqz v0, :cond_0

    .line 124917
    iget-boolean v0, p0, LX/0jz;->e:Z

    if-eqz v0, :cond_1

    .line 124918
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jz;->u:Z

    .line 124919
    :cond_0
    :goto_0
    return-void

    .line 124920
    :cond_1
    iput-boolean v3, p1, Landroid/support/v4/app/Fragment;->mDeferStart:Z

    .line 124921
    iget v2, p0, LX/0jz;->m:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 124904
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hide: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124905
    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v0, :cond_4

    .line 124906
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    .line 124907
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 124908
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 124909
    if-eqz v0, :cond_1

    .line 124910
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124911
    :cond_1
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124912
    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v0, :cond_3

    .line 124913
    iput-boolean v2, p0, LX/0jz;->q:Z

    .line 124914
    :cond_3
    invoke-virtual {p1, v2}, Landroid/support/v4/app/Fragment;->onHiddenChanged(Z)V

    .line 124915
    :cond_4
    return-void
.end method

.method public final b(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 124893
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 124894
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 124895
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124896
    if-eqz v0, :cond_1

    .line 124897
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v2, :cond_1

    .line 124898
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v2, :cond_0

    .line 124899
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 124900
    :cond_0
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v2, :cond_1

    .line 124901
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2, p1}, LX/0jz;->b(Landroid/view/Menu;)V

    .line 124902
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124903
    :cond_2
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 124892
    invoke-virtual {p0}, LX/0jz;->i()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 124877
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v1, v2

    .line 124878
    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124879
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124880
    if-eqz v0, :cond_2

    .line 124881
    const/4 v3, 0x1

    .line 124882
    iget-boolean v4, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v4, :cond_4

    .line 124883
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 124884
    :cond_0
    :goto_1
    move v0, v3

    .line 124885
    if-eqz v0, :cond_2

    .line 124886
    const/4 v2, 0x1

    .line 124887
    :cond_1
    return v2

    .line 124888
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124889
    :cond_3
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v4, :cond_4

    .line 124890
    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v4, p1}, LX/0jz;->b(Landroid/view/MenuItem;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 124891
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 124874
    invoke-direct {p0}, LX/0jz;->A()V

    .line 124875
    invoke-virtual {p0}, LX/0jz;->b()Z

    .line 124876
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, LX/0jz;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 124863
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 124864
    :goto_0
    return-object v0

    .line 124865
    :cond_0
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124866
    if-eqz v0, :cond_1

    iget v3, v0, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    if-lt v3, p1, :cond_1

    .line 124867
    iget v1, v0, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    if-ne v1, p1, :cond_3

    .line 124868
    :goto_1
    move-object v0, v0

    .line 124869
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 124870
    goto :goto_0

    .line 124871
    :cond_3
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v1, :cond_4

    .line 124872
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v1, p1}, LX/0jz;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_1

    .line 124873
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 124861
    iget v2, p0, LX/0jz;->m:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 124862
    return-void
.end method

.method public final c(Landroid/support/v4/app/Fragment;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124933
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "show: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124934
    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-eqz v0, :cond_4

    .line 124935
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    .line 124936
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 124937
    invoke-direct {p0, p1, p2, v2, p3}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 124938
    if-eqz v0, :cond_1

    .line 124939
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124940
    :cond_1
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 124941
    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v0, :cond_3

    .line 124942
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jz;->q:Z

    .line 124943
    :cond_3
    invoke-virtual {p1, v2}, Landroid/support/v4/app/Fragment;->onHiddenChanged(Z)V

    .line 124944
    :cond_4
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 124860
    iget-boolean v0, p0, LX/0jz;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0jz;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 124858
    new-instance v0, Landroid/support/v4/app/FragmentManagerImpl$2;

    invoke-direct {v0, p0}, Landroid/support/v4/app/FragmentManagerImpl$2;-><init>(LX/0jz;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0jz;->a(Ljava/lang/Runnable;Z)V

    .line 124859
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 124851
    monitor-enter p0

    .line 124852
    :try_start_0
    iget-object v0, p0, LX/0jz;->j:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 124853
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 124854
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    .line 124855
    :cond_0
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Freeing back stack index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124856
    :cond_1
    iget-object v0, p0, LX/0jz;->k:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124857
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Landroid/support/v4/app/Fragment;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 124839
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "detach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124840
    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    if-nez v0, :cond_4

    .line 124841
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    .line 124842
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    if-eqz v0, :cond_4

    .line 124843
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 124844
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "remove from detach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124845
    :cond_1
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 124846
    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v0, :cond_3

    .line 124847
    iput-boolean v2, p0, LX/0jz;->q:Z

    .line 124848
    :cond_3
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 124849
    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 124850
    :cond_4
    return-void
.end method

.method public final e(Landroid/support/v4/app/Fragment;II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 124824
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "attach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124825
    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    if-eqz v0, :cond_5

    .line 124826
    iput-boolean v5, p1, Landroid/support/v4/app/Fragment;->mDetached:Z

    .line 124827
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    if-nez v0, :cond_5

    .line 124828
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 124829
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    .line 124830
    :cond_1
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124831
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124832
    :cond_2
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "add from attach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124833
    :cond_3
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124834
    iput-boolean v2, p1, Landroid/support/v4/app/Fragment;->mAdded:Z

    .line 124835
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v0, :cond_4

    .line 124836
    iput-boolean v2, p0, LX/0jz;->q:Z

    .line 124837
    :cond_4
    iget v2, p0, LX/0jz;->m:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 124838
    :cond_5
    return-void
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 124821
    invoke-direct {p0}, LX/0jz;->A()V

    .line 124822
    invoke-virtual {p0}, LX/0jz;->b()Z

    .line 124823
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/0jz;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 124820
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 124819
    iget-boolean v0, p0, LX/0jz;->s:Z

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 124803
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 124804
    :cond_0
    return-void

    .line 124805
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 124806
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124807
    if-eqz v0, :cond_2

    .line 124808
    invoke-virtual {p0, v0}, LX/0jz;->b(Landroid/support/v4/app/Fragment;)V

    .line 124809
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 125010
    iget-boolean v1, p0, LX/0jz;->e:Z

    if-eqz v1, :cond_1

    .line 125011
    iget-object v0, p0, LX/0jz;->v:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 125012
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Exception thrown in last fragment action was caught, causing invalid state."

    iget-object v2, p0, LX/0jz;->v:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 125013
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125014
    :cond_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v3}, LX/0k3;->i()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v1, v3, :cond_2

    .line 125015
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v1, v2

    .line 125016
    :goto_0
    monitor-enter p0

    .line 125017
    :try_start_0
    iget-object v3, p0, LX/0jz;->c:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 125018
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125019
    iget-boolean v0, p0, LX/0jz;->u:Z

    if-eqz v0, :cond_a

    move v3, v2

    move v4, v2

    .line 125020
    :goto_1
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_9

    .line 125021
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125022
    if-eqz v0, :cond_b

    iget-object v5, v0, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    if-eqz v5, :cond_b

    .line 125023
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mLoaderManager:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->a()Z

    move-result v0

    or-int/2addr v3, v0

    move v0, v3

    .line 125024
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    .line 125025
    :cond_4
    :try_start_1
    iget-object v1, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 125026
    iget-object v1, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    array-length v1, v1

    if-ge v1, v3, :cond_6

    .line 125027
    :cond_5
    new-array v1, v3, [Ljava/lang/Runnable;

    iput-object v1, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    .line 125028
    :cond_6
    iget-object v1, p0, LX/0jz;->c:Ljava/util/ArrayList;

    iget-object v4, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 125029
    iget-object v1, p0, LX/0jz;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 125030
    iget-object v1, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->i()Landroid/os/Handler;

    move-result-object v1

    iget-object v4, p0, LX/0jz;->y:Ljava/lang/Runnable;

    invoke-static {v1, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 125031
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125032
    iput-boolean v0, p0, LX/0jz;->e:Z

    move v1, v2

    .line 125033
    :goto_3
    if-ge v1, v3, :cond_8

    .line 125034
    :try_start_2
    iget-object v4, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 125035
    iget-object v4, p0, LX/0jz;->d:[Ljava/lang/Runnable;

    aput-object v5, v4, v1

    .line 125036
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 125037
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 125038
    :catch_0
    move-exception v0

    .line 125039
    iput-object v0, p0, LX/0jz;->v:Ljava/lang/Exception;

    .line 125040
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Landroid/support/v4/app/FragmentManagerImpl$6;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/app/FragmentManagerImpl$6;-><init>(LX/0jz;Ljava/lang/Exception;)V

    const v3, 0x576aa10b

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 125041
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_7

    .line 125042
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 125043
    :cond_7
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 125044
    :cond_8
    iput-object v5, p0, LX/0jz;->v:Ljava/lang/Exception;

    .line 125045
    iput-boolean v2, p0, LX/0jz;->e:Z

    move v1, v0

    .line 125046
    goto/16 :goto_0

    .line 125047
    :cond_9
    if-nez v3, :cond_a

    .line 125048
    iput-boolean v2, p0, LX/0jz;->u:Z

    .line 125049
    invoke-virtual {p0}, LX/0jz;->h()V

    .line 125050
    :cond_a
    return v1

    :cond_b
    move v0, v3

    goto :goto_2
.end method

.method public final j()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125169
    const/4 v1, 0x0

    .line 125170
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 125171
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 125172
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125173
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    if-eqz v2, :cond_1

    .line 125174
    if-nez v1, :cond_0

    .line 125175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125176
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125177
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->mRetaining:Z

    .line 125178
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_2

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->mIndex:I

    :goto_1
    iput v2, v0, Landroid/support/v4/app/Fragment;->mTargetIndex:I

    .line 125179
    sget-boolean v2, LX/0jz;->a:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "retainNonConfig: keeping retained "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125180
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 125181
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 125182
    :cond_3
    return-object v1
.end method

.method public final k()Landroid/os/Parcelable;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 125118
    invoke-virtual {p0}, LX/0jz;->i()Z

    .line 125119
    sget-boolean v0, LX/0jz;->b:Z

    if-eqz v0, :cond_0

    .line 125120
    iput-boolean v1, p0, LX/0jz;->r:Z

    .line 125121
    :cond_0
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 125122
    :cond_1
    :goto_0
    return-object v3

    .line 125123
    :cond_2
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 125124
    new-array v7, v6, [Landroid/support/v4/app/FragmentState;

    move v5, v4

    move v2, v4

    .line 125125
    :goto_1
    if-ge v5, v6, :cond_9

    .line 125126
    iget-object v0, p0, LX/0jz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 125127
    if-eqz v0, :cond_10

    .line 125128
    iget v2, v0, Landroid/support/v4/app/Fragment;->mIndex:I

    if-gez v2, :cond_3

    .line 125129
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Landroid/support/v4/app/Fragment;->mIndex:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125130
    :cond_3
    new-instance v2, Landroid/support/v4/app/FragmentState;

    invoke-direct {v2, v0}, Landroid/support/v4/app/FragmentState;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 125131
    aput-object v2, v7, v5

    .line 125132
    iget v8, v0, Landroid/support/v4/app/Fragment;->mState:I

    if-lez v8, :cond_8

    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    if-nez v8, :cond_8

    .line 125133
    invoke-direct {p0, v0}, LX/0jz;->h(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    .line 125134
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    if-eqz v8, :cond_6

    .line 125135
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    iget v8, v8, Landroid/support/v4/app/Fragment;->mIndex:I

    if-gez v8, :cond_4

    .line 125136
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125137
    :cond_4
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    if-nez v8, :cond_5

    .line 125138
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    .line 125139
    :cond_5
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Landroid/support/v4/app/Fragment;->mTarget:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v8, v9, v10}, LX/0jz;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 125140
    iget v8, v0, Landroid/support/v4/app/Fragment;->mTargetRequestCode:I

    if-eqz v8, :cond_6

    .line 125141
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    const-string v9, "android:target_req_state"

    iget v10, v0, Landroid/support/v4/app/Fragment;->mTargetRequestCode:I

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125142
    :cond_6
    :goto_2
    sget-boolean v8, LX/0jz;->a:Z

    if-eqz v8, :cond_7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Saved state of "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ": "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    move v0, v1

    .line 125143
    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    .line 125144
    :cond_8
    iget-object v8, v0, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->k:Landroid/os/Bundle;

    goto :goto_2

    .line 125145
    :cond_9
    if-eqz v2, :cond_1

    .line 125146
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    .line 125147
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 125148
    if-lez v5, :cond_c

    .line 125149
    new-array v1, v5, [I

    move v2, v4

    .line 125150
    :goto_4
    if-ge v2, v5, :cond_d

    .line 125151
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iget v0, v0, Landroid/support/v4/app/Fragment;->mIndex:I

    aput v0, v1, v2

    .line 125152
    aget v0, v1, v2

    if-gez v0, :cond_a

    .line 125153
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/0jz;->a(Ljava/lang/RuntimeException;)V

    .line 125154
    :cond_a
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "saveAllState: adding fragment #"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ": "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125155
    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_c
    move-object v1, v3

    .line 125156
    :cond_d
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    .line 125157
    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 125158
    if-lez v5, :cond_f

    .line 125159
    new-array v3, v5, [Landroid/support/v4/app/BackStackState;

    move v2, v4

    .line 125160
    :goto_5
    if-ge v2, v5, :cond_f

    .line 125161
    new-instance v4, Landroid/support/v4/app/BackStackState;

    iget-object v0, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/BackStackRecord;

    invoke-direct {v4, v0}, Landroid/support/v4/app/BackStackState;-><init>(Landroid/support/v4/app/BackStackRecord;)V

    aput-object v4, v3, v2

    .line 125162
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "saveAllState: adding back stack #"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ": "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, LX/0jz;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125163
    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 125164
    :cond_f
    new-instance v0, Landroid/support/v4/app/FragmentManagerState;

    invoke-direct {v0}, Landroid/support/v4/app/FragmentManagerState;-><init>()V

    .line 125165
    iput-object v7, v0, Landroid/support/v4/app/FragmentManagerState;->a:[Landroid/support/v4/app/FragmentState;

    .line 125166
    iput-object v1, v0, Landroid/support/v4/app/FragmentManagerState;->b:[I

    .line 125167
    iput-object v3, v0, Landroid/support/v4/app/FragmentManagerState;->c:[Landroid/support/v4/app/BackStackState;

    move-object v3, v0

    .line 125168
    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_3
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 125116
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0jz;->r:Z

    .line 125117
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125113
    iput-boolean v1, p0, LX/0jz;->r:Z

    .line 125114
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125115
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125110
    iput-boolean v1, p0, LX/0jz;->r:Z

    .line 125111
    const/4 v0, 0x2

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125112
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125107
    iput-boolean v1, p0, LX/0jz;->r:Z

    .line 125108
    const/4 v0, 0x4

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125109
    return-void
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 125056
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 125057
    :goto_0
    return-object v0

    .line 125058
    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125059
    sget-object v4, LX/3pE;->a:[I

    invoke-virtual {p2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 125060
    if-nez v0, :cond_f

    .line 125061
    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 125062
    :goto_1
    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 125063
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 125064
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 125065
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    .line 125066
    :try_start_0
    sget-object v4, Landroid/support/v4/app/Fragment;->sClassMap:LX/01J;

    invoke-virtual {v4, v6}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    .line 125067
    if-nez v4, :cond_1

    .line 125068
    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 125069
    sget-object p1, Landroid/support/v4/app/Fragment;->sClassMap:LX/01J;

    invoke-virtual {p1, v6, v4}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125070
    :cond_1
    const-class p1, Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 125071
    :goto_2
    move v0, v4

    .line 125072
    if-nez v0, :cond_2

    move-object v0, v1

    .line 125073
    goto :goto_0

    .line 125074
    :cond_2
    if-eq v7, v5, :cond_6

    invoke-virtual {p0, v7}, LX/0jz;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 125075
    :goto_3
    if-nez v0, :cond_3

    if-eqz v8, :cond_3

    .line 125076
    invoke-virtual {p0, v8}, LX/0jz;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 125077
    :cond_3
    if-nez v0, :cond_4

    .line 125078
    invoke-virtual {p0, v3}, LX/0jz;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 125079
    :cond_4
    sget-boolean v1, LX/0jz;->a:Z

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "onCreateView: id=0x"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " fname="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " existing="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125080
    :cond_5
    if-nez v0, :cond_8

    .line 125081
    invoke-static {p2, v6}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 125082
    iput-boolean v2, v1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    .line 125083
    if-eqz v7, :cond_7

    move v0, v7

    :goto_4
    iput v0, v1, Landroid/support/v4/app/Fragment;->mFragmentId:I

    .line 125084
    iput v3, v1, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 125085
    iput-object v8, v1, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    .line 125086
    iput-boolean v2, v1, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 125087
    iput-object p0, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 125088
    iget-object v0, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    iget-object v4, v1, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v1, v0, p3, v4}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 125089
    invoke-virtual {p0, v1, v2}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 125090
    :goto_5
    iget v0, p0, LX/0jz;->m:I

    if-gtz v0, :cond_b

    iget-boolean v0, v1, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    if-eqz v0, :cond_b

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 125091
    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 125092
    :goto_6
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-nez v0, :cond_c

    .line 125093
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v0, v1

    .line 125094
    goto/16 :goto_3

    :cond_7
    move v0, v3

    .line 125095
    goto :goto_4

    .line 125096
    :cond_8
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    if-eqz v1, :cond_9

    .line 125097
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Duplicate id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", or parent id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125098
    :cond_9
    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 125099
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mRetaining:Z

    if-nez v1, :cond_a

    .line 125100
    iget-object v1, p0, LX/0jz;->n:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p3, v4}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    :cond_a
    move-object v1, v0

    goto/16 :goto_5

    .line 125101
    :cond_b
    invoke-virtual {p0, v1}, LX/0jz;->c(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_6

    .line 125102
    :cond_c
    if-eqz v7, :cond_d

    .line 125103
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 125104
    :cond_d
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_e

    .line 125105
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 125106
    :cond_e
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    goto/16 :goto_0

    :cond_f
    move-object v6, v0

    goto/16 :goto_1

    :catch_0
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public final p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125053
    iput-boolean v1, p0, LX/0jz;->r:Z

    .line 125054
    const/4 v0, 0x5

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125055
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 125051
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125052
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 124930
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jz;->r:Z

    .line 124931
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 124932
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 125008
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125009
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 125006
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0jz;->a(IZ)V

    .line 125007
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124997
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 124998
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124999
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125000
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125001
    iget-object v1, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    .line 125002
    iget-object v1, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    invoke-static {v1, v0}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 125003
    :goto_0
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125004
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 125005
    :cond_0
    iget-object v1, p0, LX/0jz;->n:LX/0k3;

    invoke-static {v1, v0}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method public final u()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 124990
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jz;->s:Z

    .line 124991
    invoke-virtual {p0}, LX/0jz;->i()Z

    .line 124992
    invoke-direct {p0, v2, v2}, LX/0jz;->a(IZ)V

    .line 124993
    iput-object v1, p0, LX/0jz;->n:LX/0k3;

    .line 124994
    iput-object v1, p0, LX/0jz;->o:LX/0k1;

    .line 124995
    iput-object v1, p0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    .line 124996
    return-void
.end method

.method public final v()V
    .locals 3

    .prologue
    .line 124981
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 124982
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124983
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124984
    if-eqz v0, :cond_0

    .line 124985
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 124986
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v2, :cond_0

    .line 124987
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->v()V

    .line 124988
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124989
    :cond_1
    return-void
.end method

.method public final w()Landroid/view/View;
    .locals 4

    .prologue
    .line 124966
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 124967
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 124968
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124969
    if-eqz v0, :cond_2

    .line 124970
    const/4 v2, 0x0

    .line 124971
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v3, :cond_1

    .line 124972
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v3, :cond_0

    .line 124973
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->dispatchOnCreateOptionsView()Landroid/view/View;

    move-result-object v2

    .line 124974
    :cond_0
    if-nez v2, :cond_1

    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v3, :cond_1

    .line 124975
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->w()Landroid/view/View;

    move-result-object v2

    .line 124976
    :cond_1
    move-object v0, v2

    .line 124977
    if-eqz v0, :cond_2

    .line 124978
    :goto_1
    return-object v0

    .line 124979
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124980
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 124956
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 124957
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124958
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124959
    if-eqz v0, :cond_0

    .line 124960
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v2, :cond_0

    .line 124961
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->dispatchOnInvalidateOptionsMenu()V

    .line 124962
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    if-eqz v2, :cond_0

    .line 124963
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mChildFragmentManager:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->x()V

    .line 124964
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124965
    :cond_1
    return-void
.end method

.method public final y()Landroid/view/MenuInflater;
    .locals 3

    .prologue
    .line 124945
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 124946
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124947
    iget-object v0, p0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 124948
    if-eqz v0, :cond_0

    .line 124949
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-nez v2, :cond_2

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mHasMenu:Z

    if-eqz v2, :cond_2

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mMenuVisible:Z

    if-eqz v2, :cond_2

    .line 124950
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->dispatchGetMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 124951
    :goto_1
    move-object v0, v2

    .line 124952
    if-eqz v0, :cond_0

    .line 124953
    :goto_2
    return-object v0

    .line 124954
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124955
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
