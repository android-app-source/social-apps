.class public final enum LX/1gX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1gX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1gX;

.field public static final enum CONVERSION:LX/1gX;

.field public static final enum EXPOSURE:LX/1gX;

.field public static final enum OBSERVATION:LX/1gX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 294412
    new-instance v0, LX/1gX;

    const-string v1, "EXPOSURE"

    invoke-direct {v0, v1, v2}, LX/1gX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gX;->EXPOSURE:LX/1gX;

    .line 294413
    new-instance v0, LX/1gX;

    const-string v1, "CONVERSION"

    invoke-direct {v0, v1, v3}, LX/1gX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gX;->CONVERSION:LX/1gX;

    .line 294414
    new-instance v0, LX/1gX;

    const-string v1, "OBSERVATION"

    invoke-direct {v0, v1, v4}, LX/1gX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gX;->OBSERVATION:LX/1gX;

    .line 294415
    const/4 v0, 0x3

    new-array v0, v0, [LX/1gX;

    sget-object v1, LX/1gX;->EXPOSURE:LX/1gX;

    aput-object v1, v0, v2

    sget-object v1, LX/1gX;->CONVERSION:LX/1gX;

    aput-object v1, v0, v3

    sget-object v1, LX/1gX;->OBSERVATION:LX/1gX;

    aput-object v1, v0, v4

    sput-object v0, LX/1gX;->$VALUES:[LX/1gX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 294409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1gX;
    .locals 1

    .prologue
    .line 294411
    const-class v0, LX/1gX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1gX;

    return-object v0
.end method

.method public static values()[LX/1gX;
    .locals 1

    .prologue
    .line 294410
    sget-object v0, LX/1gX;->$VALUES:[LX/1gX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1gX;

    return-object v0
.end method
