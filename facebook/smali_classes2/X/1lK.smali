.class public LX/1lK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/nio/ByteBuffer;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 311453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311454
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311455
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 311456
    iput-object p1, p0, LX/1lK;->a:Ljava/nio/ByteBuffer;

    .line 311457
    iput p2, p0, LX/1lK;->b:I

    .line 311458
    return-void

    .line 311459
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0w5;J)[B
    .locals 7
    .param p0    # LX/0w5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 311460
    new-instance v0, LX/186;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 311461
    if-eqz p0, :cond_1

    invoke-virtual {p0}, LX/0w5;->b()Ljava/lang/String;

    move-result-object v1

    .line 311462
    :goto_0
    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 311463
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0w5;->a()Ljava/lang/Integer;

    move-result-object v2

    .line 311464
    :cond_0
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 311465
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 311466
    const/4 v4, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_1
    invoke-virtual {v0, v4, v1, v3}, LX/186;->a(III)V

    .line 311467
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 311468
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 311469
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 311470
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v2

    .line 311471
    goto :goto_0

    :cond_2
    move v1, v3

    .line 311472
    goto :goto_1
.end method
