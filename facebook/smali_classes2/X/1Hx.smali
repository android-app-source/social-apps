.class public LX/1Hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# instance fields
.field public a:[B

.field public b:Z

.field private final c:LX/1Hy;

.field public final d:Landroid/content/SharedPreferences;

.field public final e:LX/1Hd;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Hy;)V
    .locals 2

    .prologue
    .line 227967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227968
    sget-object v0, LX/1Hy;->KEY_128:LX/1Hy;

    if-ne p2, v0, :cond_0

    const-string v0, "crypto"

    :goto_0
    move-object v0, v0

    .line 227969
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/1Hx;->d:Landroid/content/SharedPreferences;

    .line 227970
    new-instance v0, LX/1Hd;

    invoke-direct {v0}, LX/1Hd;-><init>()V

    iput-object v0, p0, LX/1Hx;->e:LX/1Hd;

    .line 227971
    iput-object p2, p0, LX/1Hx;->c:LX/1Hy;

    .line 227972
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "crypto."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()[B
    .locals 5

    .prologue
    .line 227976
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1Hx;->b:Z

    if-nez v0, :cond_0

    .line 227977
    const-string v0, "cipher_key"

    iget-object v1, p0, LX/1Hx;->c:LX/1Hy;

    iget v1, v1, LX/1Hy;->keyLength:I

    .line 227978
    iget-object v2, p0, LX/1Hx;->d:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 227979
    if-nez v2, :cond_1

    .line 227980
    new-array v2, v1, [B

    .line 227981
    iget-object v3, p0, LX/1Hx;->e:LX/1Hd;

    invoke-virtual {v3, v2}, LX/1Hd;->nextBytes([B)V

    .line 227982
    iget-object v3, p0, LX/1Hx;->d:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 227983
    if-nez v2, :cond_2

    .line 227984
    const/4 v4, 0x0

    .line 227985
    :goto_0
    move-object v4, v4

    .line 227986
    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 227987
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 227988
    move-object v2, v2

    .line 227989
    :goto_1
    move-object v0, v2

    .line 227990
    iput-object v0, p0, LX/1Hx;->a:[B

    .line 227991
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Hx;->b:Z

    .line 227992
    iget-object v0, p0, LX/1Hx;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 227993
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 227994
    :cond_1
    if-nez v2, :cond_3

    .line 227995
    const/4 v3, 0x0

    .line 227996
    :goto_2
    move-object v2, v3

    .line 227997
    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    goto :goto_2
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 227973
    iget-object v0, p0, LX/1Hx;->c:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 227974
    iget-object v1, p0, LX/1Hx;->e:LX/1Hd;

    invoke-virtual {v1, v0}, LX/1Hd;->nextBytes([B)V

    .line 227975
    return-object v0
.end method
