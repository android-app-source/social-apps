.class public LX/1WC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/1WB;


# direct methods
.method public constructor <init>(LX/0ad;LX/1WB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267731
    iput-object p1, p0, LX/1WC;->a:LX/0ad;

    .line 267732
    iput-object p2, p0, LX/1WC;->b:LX/1WB;

    .line 267733
    return-void
.end method

.method public static a(LX/0QB;)LX/1WC;
    .locals 5

    .prologue
    .line 267734
    const-class v1, LX/1WC;

    monitor-enter v1

    .line 267735
    :try_start_0
    sget-object v0, LX/1WC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267736
    sput-object v2, LX/1WC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267739
    new-instance p0, LX/1WC;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v4

    check-cast v4, LX/1WB;

    invoke-direct {p0, v3, v4}, LX/1WC;-><init>(LX/0ad;LX/1WB;)V

    .line 267740
    move-object v0, p0

    .line 267741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 267745
    iget-object v0, p0, LX/1WC;->a:LX/0ad;

    sget-short v1, LX/0wk;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 267746
    invoke-virtual {p0}, LX/1WC;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 267747
    :cond_0
    :goto_0
    return v0

    .line 267748
    :cond_1
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 267749
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 267750
    if-eqz v1, :cond_0

    .line 267751
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-gtz v1, :cond_0

    .line 267752
    iget-object v1, p0, LX/1WC;->a:LX/0ad;

    sget-short v2, LX/0wk;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 267753
    :cond_2
    iget-object v1, p0, LX/1WC;->b:LX/1WB;

    invoke-virtual {v1, p1}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
