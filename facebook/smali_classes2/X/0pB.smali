.class public LX/0pB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field private b:J

.field private c:Z

.field private d:Z

.field private e:[J


# direct methods
.method public constructor <init>(J)V
    .locals 3
    .param p1    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 143968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143969
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0pB;->b:J

    .line 143970
    iput-boolean v2, p0, LX/0pB;->c:Z

    .line 143971
    iput-boolean v2, p0, LX/0pB;->d:Z

    .line 143972
    const/4 v0, 0x0

    iput-object v0, p0, LX/0pB;->e:[J

    .line 143973
    iput-wide p1, p0, LX/0pB;->a:J

    .line 143974
    return-void
.end method

.method public static a([J)J
    .locals 6

    .prologue
    .line 143975
    const-wide/16 v2, 0x0

    .line 143976
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 143977
    aget-wide v4, p0, v0

    add-long/2addr v2, v4

    .line 143978
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143979
    :cond_0
    return-wide v2
.end method

.method public static b(ZZ)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 143963
    if-eqz p0, :cond_1

    const/4 v1, 0x2

    .line 143964
    :goto_0
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 143965
    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v1, v0

    .line 143966
    goto :goto_0
.end method

.method private static b()[J
    .locals 1

    .prologue
    .line 143967
    const/4 v0, 0x4

    new-array v0, v0, [J

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 143962
    iget-object v0, p0, LX/0pB;->e:[J

    invoke-static {v0}, LX/0pB;->a([J)J

    move-result-wide v0

    iget-wide v2, p0, LX/0pB;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/util/ArrayList;)[J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[J>;)[J"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 143954
    invoke-static {}, LX/0pB;->b()[J

    move-result-object v4

    .line 143955
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 143956
    array-length v1, v0

    array-length v6, v4

    if-ne v1, v6, :cond_0

    move v1, v2

    .line 143957
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_0

    .line 143958
    aget-wide v6, v4, v1

    aget-wide v8, v0, v1

    add-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 143959
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 143960
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 143961
    :cond_1
    return-object v4
.end method


# virtual methods
.method public final a(ZZ)Z
    .locals 8

    .prologue
    .line 143943
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 143944
    iget-wide v2, p0, LX/0pB;->b:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 143945
    const/4 v2, 0x0

    iput-object v2, p0, LX/0pB;->e:[J

    .line 143946
    :cond_0
    iget-object v2, p0, LX/0pB;->e:[J

    if-nez v2, :cond_1

    .line 143947
    invoke-static {}, LX/0pB;->b()[J

    move-result-object v2

    iput-object v2, p0, LX/0pB;->e:[J

    .line 143948
    :goto_0
    iput-wide v0, p0, LX/0pB;->b:J

    .line 143949
    iput-boolean p1, p0, LX/0pB;->c:Z

    .line 143950
    iput-boolean p2, p0, LX/0pB;->d:Z

    .line 143951
    invoke-direct {p0}, LX/0pB;->c()Z

    move-result v0

    return v0

    .line 143952
    :cond_1
    iget-wide v2, p0, LX/0pB;->b:J

    sub-long v2, v0, v2

    .line 143953
    iget-object v4, p0, LX/0pB;->e:[J

    iget-boolean v5, p0, LX/0pB;->c:Z

    iget-boolean v6, p0, LX/0pB;->d:Z

    invoke-static {v5, v6}, LX/0pB;->b(ZZ)I

    move-result v5

    aget-wide v6, v4, v5

    add-long/2addr v2, v6

    aput-wide v2, v4, v5

    goto :goto_0
.end method

.method public final declared-synchronized a()[J
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 143932
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/0pB;->c:Z

    iget-boolean v2, p0, LX/0pB;->d:Z

    invoke-virtual {p0, v1, v2}, LX/0pB;->a(ZZ)Z

    .line 143933
    invoke-direct {p0}, LX/0pB;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143934
    iget-object v0, p0, LX/0pB;->e:[J

    .line 143935
    invoke-static {v0}, LX/0pB;->a([J)J

    move-result-wide v5

    .line 143936
    const/4 v3, 0x0

    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_0

    .line 143937
    aget-wide v7, v0, v3

    iget-wide v9, p0, LX/0pB;->a:J

    mul-long/2addr v7, v9

    div-long/2addr v7, v5

    aput-wide v7, v0, v3

    .line 143938
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 143939
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/0pB;->e:[J

    .line 143940
    iget-boolean v1, p0, LX/0pB;->c:Z

    iget-boolean v2, p0, LX/0pB;->d:Z

    invoke-virtual {p0, v1, v2}, LX/0pB;->a(ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143941
    :cond_1
    monitor-exit p0

    return-object v0

    .line 143942
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
