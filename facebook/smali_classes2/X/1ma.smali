.class public LX/1ma;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)LX/1mb;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 313904
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "attribution_state.txt"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 313905
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 313906
    new-instance v9, Ljava/io/BufferedReader;

    invoke-direct {v9, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313907
    :try_start_1
    new-instance v0, LX/1mb;

    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, LX/1mb;-><init>(Ljava/lang/String;JJZLjava/lang/String;LX/0lF;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 313908
    invoke-static {v9}, LX/1md;->a(Ljava/io/Reader;)V

    .line 313909
    return-object v0

    .line 313910
    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v9

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/1mb;)V
    .locals 4

    .prologue
    .line 313888
    const/4 v2, 0x0

    .line 313889
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "attribution_state.txt"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 313890
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 313891
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313892
    :try_start_1
    iget-object v0, p1, LX/1mb;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 313893
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 313894
    iget-wide v2, p1, LX/1mb;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 313895
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 313896
    iget-wide v2, p1, LX/1mb;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 313897
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 313898
    iget-boolean v0, p1, LX/1mb;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 313899
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 313900
    iget-object v0, p1, LX/1mb;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 313901
    const/4 v0, 0x0

    invoke-static {v1, v0}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 313902
    return-void

    .line 313903
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
