.class public final enum LX/0rH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rH;

.field public static final enum MODAL:LX/0rH;

.field public static final enum NEWS_FEED:LX/0rH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149169
    new-instance v0, LX/0rH;

    const-string v1, "NEWS_FEED"

    invoke-direct {v0, v1, v2}, LX/0rH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rH;->NEWS_FEED:LX/0rH;

    .line 149170
    new-instance v0, LX/0rH;

    const-string v1, "MODAL"

    invoke-direct {v0, v1, v3}, LX/0rH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rH;->MODAL:LX/0rH;

    .line 149171
    const/4 v0, 0x2

    new-array v0, v0, [LX/0rH;

    sget-object v1, LX/0rH;->NEWS_FEED:LX/0rH;

    aput-object v1, v0, v2

    sget-object v1, LX/0rH;->MODAL:LX/0rH;

    aput-object v1, v0, v3

    sput-object v0, LX/0rH;->$VALUES:[LX/0rH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149172
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rH;
    .locals 1

    .prologue
    .line 149173
    const-class v0, LX/0rH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rH;

    return-object v0
.end method

.method public static values()[LX/0rH;
    .locals 1

    .prologue
    .line 149174
    sget-object v0, LX/0rH;->$VALUES:[LX/0rH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rH;

    return-object v0
.end method
