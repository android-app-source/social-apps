.class public LX/18J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 206237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;
    .locals 2

    .prologue
    .line 206238
    invoke-interface {p0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 206239
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206240
    :goto_0
    return-object v0

    .line 206241
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 206242
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, Lcom/facebook/graphql/model/OrganicImpression;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;

    move-result-object v0

    goto :goto_0

    .line 206243
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;
    .locals 1

    .prologue
    .line 206244
    instance-of v0, p0, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v0, :cond_0

    .line 206245
    check-cast p0, Lcom/facebook/graphql/model/Sponsorable;

    .line 206246
    :goto_0
    return-object p0

    .line 206247
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_1

    .line 206248
    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 206249
    invoke-static {v0}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object p0

    goto :goto_0

    .line 206250
    :cond_1
    instance-of v0, p0, LX/1Rk;

    if-eqz v0, :cond_2

    .line 206251
    check-cast p0, LX/1Rk;

    .line 206252
    iget-object v0, p0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v0, v0

    .line 206253
    invoke-static {v0}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object p0

    goto :goto_0

    .line 206254
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 206255
    instance-of v1, p0, Lcom/facebook/graphql/model/Sponsorable;

    if-nez v1, :cond_1

    .line 206256
    :cond_0
    :goto_0
    return v0

    .line 206257
    :cond_1
    check-cast p0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {p0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 206258
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
