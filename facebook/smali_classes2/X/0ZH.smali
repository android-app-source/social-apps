.class public LX/0ZH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# static fields
.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0ZJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83150
    new-instance v0, LX/0ZI;

    invoke-direct {v0}, LX/0ZI;-><init>()V

    sput-object v0, LX/0ZH;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0ZJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83152
    iput-object p1, p0, LX/0ZH;->a:LX/0ZJ;

    .line 83153
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83154
    const-string v0, "mqtt_client_log"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 83155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 83156
    iget-object v0, p0, LX/0ZH;->a:LX/0ZJ;

    invoke-virtual {v0}, LX/0ZK;->a()Ljava/util/List;

    move-result-object v0

    .line 83157
    sget-object v2, LX/0ZH;->b:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 83158
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ki;

    .line 83159
    const-string v3, "[%s] %s%n"

    invoke-interface {v0}, LX/0ki;->getStartTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0}, LX/0ki;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 83160
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
