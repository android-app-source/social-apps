.class public final LX/1Gw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1BU;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1BU;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 226156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226157
    iput-object p1, p0, LX/1Gw;->a:LX/0QB;

    .line 226158
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226159
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/1Gw;

    invoke-direct {v2, p0}, LX/1Gw;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 226160
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1Gw;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 226161
    packed-switch p2, :pswitch_data_0

    .line 226162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226163
    :pswitch_0
    invoke-static {p1}, LX/1BR;->a(LX/0QB;)LX/1BS;

    move-result-object v0

    .line 226164
    :goto_0
    return-object v0

    .line 226165
    :pswitch_1
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    move-result-object v0

    goto :goto_0

    .line 226166
    :pswitch_2
    invoke-static {p1}, LX/1Ig;->a(LX/0QB;)LX/1Ih;

    move-result-object v0

    goto :goto_0

    .line 226167
    :pswitch_3
    invoke-static {p1}, LX/1Ii;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v0

    goto :goto_0

    .line 226168
    :pswitch_4
    invoke-static {p1}, LX/1Ir;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 226169
    const/4 v0, 0x5

    return v0
.end method
