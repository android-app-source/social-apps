.class public final enum LX/1A0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1A0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1A0;

.field public static final enum DOWNLOAD_ABORTED:LX/1A0;

.field public static final enum DOWNLOAD_COMPLETED:LX/1A0;

.field public static final enum DOWNLOAD_FAILED:LX/1A0;

.field public static final enum DOWNLOAD_IN_PROGRESS:LX/1A0;

.field public static final enum DOWNLOAD_NOT_REQUESTED:LX/1A0;

.field public static final enum DOWNLOAD_NOT_STARTED:LX/1A0;

.field public static final enum DOWNLOAD_PAUSED:LX/1A0;

.field private static final mReverseIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1A0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 209872
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_IN_PROGRESS"

    invoke-direct {v1, v2, v0, v0}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    .line 209873
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_PAUSED"

    invoke-direct {v1, v2, v5, v5}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    .line 209874
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_COMPLETED"

    invoke-direct {v1, v2, v6, v6}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    .line 209875
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_NOT_STARTED"

    invoke-direct {v1, v2, v7, v7}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    .line 209876
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_FAILED"

    invoke-direct {v1, v2, v8, v8}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    .line 209877
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_ABORTED"

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    .line 209878
    new-instance v1, LX/1A0;

    const-string v2, "DOWNLOAD_NOT_REQUESTED"

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4}, LX/1A0;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    .line 209879
    const/4 v1, 0x7

    new-array v1, v1, [LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    aput-object v2, v1, v0

    sget-object v2, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    aput-object v2, v1, v5

    sget-object v2, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    aput-object v2, v1, v6

    sget-object v2, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    aput-object v2, v1, v7

    sget-object v2, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    aput-object v3, v1, v2

    sput-object v1, LX/1A0;->$VALUES:[LX/1A0;

    .line 209880
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LX/1A0;->mReverseIndex:Ljava/util/Map;

    .line 209881
    invoke-static {}, LX/1A0;->values()[LX/1A0;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 209882
    sget-object v4, LX/1A0;->mReverseIndex:Ljava/util/Map;

    iget v5, v3, LX/1A0;->mValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209883
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209884
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 209869
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 209870
    iput p3, p0, LX/1A0;->mValue:I

    .line 209871
    return-void
.end method

.method public static fromVal(I)LX/1A0;
    .locals 2

    .prologue
    .line 209866
    sget-object v0, LX/1A0;->mReverseIndex:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209867
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid download status value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209868
    :cond_0
    sget-object v0, LX/1A0;->mReverseIndex:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1A0;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1A0;
    .locals 1

    .prologue
    .line 209864
    const-class v0, LX/1A0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1A0;

    return-object v0
.end method

.method public static values()[LX/1A0;
    .locals 1

    .prologue
    .line 209865
    sget-object v0, LX/1A0;->$VALUES:[LX/1A0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1A0;

    return-object v0
.end method
