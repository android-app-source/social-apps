.class public final LX/1fx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/1XS;

.field public final b:[I

.field public final c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(LX/1XS;I)V
    .locals 2

    .prologue
    .line 292539
    iput-object p1, p0, LX/1fx;->a:LX/1XS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292540
    new-array v0, p2, [I

    iput-object v0, p0, LX/1fx;->b:[I

    .line 292541
    iget-object v0, p0, LX/1fx;->b:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 292542
    iput p2, p0, LX/1fx;->c:I

    .line 292543
    return-void
.end method

.method public static c(LX/1fx;I)I
    .locals 1

    .prologue
    .line 292547
    iget-object v0, p0, LX/1fx;->b:[I

    aget v0, v0, p1

    return v0
.end method

.method public static c(LX/1fx;)Z
    .locals 2

    .prologue
    .line 292546
    iget v0, p0, LX/1fx;->e:I

    iget v1, p0, LX/1fx;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 292545
    invoke-static {p0}, LX/1fx;->c(LX/1fx;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/1fx;->d:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 292544
    invoke-static {p0, p1}, LX/1fx;->c(LX/1fx;I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
