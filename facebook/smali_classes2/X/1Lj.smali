.class public LX/1Lj;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Lv;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Lv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 234153
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()LX/1Lv;
    .locals 26

    .prologue
    .line 234166
    invoke-static/range {p0 .. p0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v2

    check-cast v2, LX/1Ln;

    invoke-static/range {p0 .. p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v3

    check-cast v3, LX/1AA;

    invoke-static/range {p0 .. p0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v4

    check-cast v4, LX/1Lg;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v7

    check-cast v7, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/1Lr;->a(LX/0QB;)LX/1Lr;

    move-result-object v8

    check-cast v8, LX/1Lr;

    const/16 v9, 0x1488

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v10

    check-cast v10, LX/0yH;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v11

    check-cast v11, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/0kY;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v12

    check-cast v12, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/1Ls;->a(LX/0QB;)LX/1Lt;

    move-result-object v13

    check-cast v13, LX/1Lt;

    invoke-static/range {p0 .. p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v14

    check-cast v14, LX/0Sj;

    const/16 v15, 0x12f4

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v16

    check-cast v16, LX/11i;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v17

    check-cast v17, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v18

    check-cast v18, LX/0TR;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v19

    check-cast v19, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v20

    check-cast v20, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v21

    check-cast v21, LX/19s;

    invoke-static/range {p0 .. p0}, LX/1Lu;->a(LX/0QB;)LX/1Lu;

    move-result-object v22

    check-cast v22, LX/1Lu;

    const/16 v23, 0x132e

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v25

    check-cast v25, LX/0tQ;

    invoke-static/range {v2 .. v25}, LX/19Y;->a(LX/1Ln;LX/1AA;LX/1Lg;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0ka;LX/1Lr;LX/0Or;LX/0yH;Lcom/facebook/http/common/FbHttpRequestProcessor;Landroid/os/Handler;LX/1Lt;LX/0Sj;LX/0Ot;LX/11i;LX/0So;LX/0TR;LX/0WJ;LX/0lC;LX/19s;LX/1Lu;LX/0Ot;LX/0ad;LX/0tQ;)LX/1Lv;

    move-result-object v2

    return-object v2
.end method

.method public static a(LX/0QB;)LX/1Lv;
    .locals 3

    .prologue
    .line 234156
    sget-object v0, LX/1Lj;->a:LX/1Lv;

    if-nez v0, :cond_1

    .line 234157
    const-class v1, LX/1Lj;

    monitor-enter v1

    .line 234158
    :try_start_0
    sget-object v0, LX/1Lj;->a:LX/1Lv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234159
    if-eqz v2, :cond_0

    .line 234160
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Lj;->b(LX/0QB;)LX/1Lv;

    move-result-object v0

    sput-object v0, LX/1Lj;->a:LX/1Lv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234161
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234162
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234163
    :cond_1
    sget-object v0, LX/1Lj;->a:LX/1Lv;

    return-object v0

    .line 234164
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/1Lv;
    .locals 26

    .prologue
    .line 234155
    invoke-static/range {p0 .. p0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v2

    check-cast v2, LX/1Ln;

    invoke-static/range {p0 .. p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v3

    check-cast v3, LX/1AA;

    invoke-static/range {p0 .. p0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v4

    check-cast v4, LX/1Lg;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v7

    check-cast v7, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/1Lr;->a(LX/0QB;)LX/1Lr;

    move-result-object v8

    check-cast v8, LX/1Lr;

    const/16 v9, 0x1488

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v10

    check-cast v10, LX/0yH;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v11

    check-cast v11, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/0kY;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v12

    check-cast v12, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/1Ls;->a(LX/0QB;)LX/1Lt;

    move-result-object v13

    check-cast v13, LX/1Lt;

    invoke-static/range {p0 .. p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v14

    check-cast v14, LX/0Sj;

    const/16 v15, 0x12f4

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v16

    check-cast v16, LX/11i;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v17

    check-cast v17, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v18

    check-cast v18, LX/0TR;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v19

    check-cast v19, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v20

    check-cast v20, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v21

    check-cast v21, LX/19s;

    invoke-static/range {p0 .. p0}, LX/1Lu;->a(LX/0QB;)LX/1Lu;

    move-result-object v22

    check-cast v22, LX/1Lu;

    const/16 v23, 0x132e

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v25

    check-cast v25, LX/0tQ;

    invoke-static/range {v2 .. v25}, LX/19Y;->a(LX/1Ln;LX/1AA;LX/1Lg;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0ka;LX/1Lr;LX/0Or;LX/0yH;Lcom/facebook/http/common/FbHttpRequestProcessor;Landroid/os/Handler;LX/1Lt;LX/0Sj;LX/0Ot;LX/11i;LX/0So;LX/0TR;LX/0WJ;LX/0lC;LX/19s;LX/1Lu;LX/0Ot;LX/0ad;LX/0tQ;)LX/1Lv;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 234154
    invoke-direct {p0}, LX/1Lj;->a()LX/1Lv;

    move-result-object v0

    return-object v0
.end method
