.class public final LX/1uK;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

.field public b:Z

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;

.field public final i:Landroid/graphics/Rect;

.field public j:Ljava/lang/Object;

.field private k:Z

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 340480
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 340481
    iput-boolean v0, p0, LX/1uK;->b:Z

    .line 340482
    iput v0, p0, LX/1uK;->c:I

    .line 340483
    iput v0, p0, LX/1uK;->d:I

    .line 340484
    iput v1, p0, LX/1uK;->e:I

    .line 340485
    iput v1, p0, LX/1uK;->f:I

    .line 340486
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    .line 340487
    return-void
.end method

.method public constructor <init>(LX/1uK;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 340488
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 340489
    iput-boolean v0, p0, LX/1uK;->b:Z

    .line 340490
    iput v0, p0, LX/1uK;->c:I

    .line 340491
    iput v0, p0, LX/1uK;->d:I

    .line 340492
    iput v1, p0, LX/1uK;->e:I

    .line 340493
    iput v1, p0, LX/1uK;->f:I

    .line 340494
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    .line 340495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 340496
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 340497
    iput-boolean v2, p0, LX/1uK;->b:Z

    .line 340498
    iput v2, p0, LX/1uK;->c:I

    .line 340499
    iput v2, p0, LX/1uK;->d:I

    .line 340500
    iput v3, p0, LX/1uK;->e:I

    .line 340501
    iput v3, p0, LX/1uK;->f:I

    .line 340502
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    .line 340503
    sget-object v0, LX/03r;->CoordinatorLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 340504
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/1uK;->c:I

    .line 340505
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/1uK;->f:I

    .line 340506
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/1uK;->d:I

    .line 340507
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/1uK;->e:I

    .line 340508
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, LX/1uK;->b:Z

    .line 340509
    iget-boolean v1, p0, LX/1uK;->b:Z

    if-eqz v1, :cond_0

    .line 340510
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v1

    iput-object v1, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    .line 340511
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 340512
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 340513
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 340514
    iput-boolean v0, p0, LX/1uK;->b:Z

    .line 340515
    iput v0, p0, LX/1uK;->c:I

    .line 340516
    iput v0, p0, LX/1uK;->d:I

    .line 340517
    iput v1, p0, LX/1uK;->e:I

    .line 340518
    iput v1, p0, LX/1uK;->f:I

    .line 340519
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    .line 340520
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 340521
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 340522
    iput-boolean v0, p0, LX/1uK;->b:Z

    .line 340523
    iput v0, p0, LX/1uK;->c:I

    .line 340524
    iput v0, p0, LX/1uK;->d:I

    .line 340525
    iput v1, p0, LX/1uK;->e:I

    .line 340526
    iput v1, p0, LX/1uK;->f:I

    .line 340527
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    .line 340528
    return-void
.end method

.method private a(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 340529
    iget v0, p0, LX/1uK;->f:I

    invoke-virtual {p2, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/1uK;->g:Landroid/view/View;

    .line 340530
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 340531
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    if-ne v0, p2, :cond_1

    .line 340532
    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340533
    iput-object v3, p0, LX/1uK;->h:Landroid/view/View;

    iput-object v3, p0, LX/1uK;->g:Landroid/view/View;

    .line 340534
    :goto_0
    return-void

    .line 340535
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "View can not be anchored to the the parent CoordinatorLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340536
    :cond_1
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    .line 340537
    iget-object v1, p0, LX/1uK;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 340538
    :goto_1
    if-eq v1, p2, :cond_5

    if-eqz v1, :cond_5

    .line 340539
    if-ne v1, p1, :cond_3

    .line 340540
    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 340541
    iput-object v3, p0, LX/1uK;->h:Landroid/view/View;

    iput-object v3, p0, LX/1uK;->g:Landroid/view/View;

    goto :goto_0

    .line 340542
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Anchor must not be a descendant of the anchored view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340543
    :cond_3
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_4

    move-object v0, v1

    .line 340544
    check-cast v0, Landroid/view/View;

    .line 340545
    :cond_4
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 340546
    :cond_5
    iput-object v0, p0, LX/1uK;->h:Landroid/view/View;

    goto :goto_0

    .line 340547
    :cond_6
    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 340548
    iput-object v3, p0, LX/1uK;->h:Landroid/view/View;

    iput-object v3, p0, LX/1uK;->g:Landroid/view/View;

    goto :goto_0

    .line 340549
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find CoordinatorLayout descendant view with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, LX/1uK;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to anchor view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 340550
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, LX/1uK;->f:I

    if-eq v0, v1, :cond_0

    move v0, v2

    .line 340551
    :goto_0
    return v0

    .line 340552
    :cond_0
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    .line 340553
    iget-object v1, p0, LX/1uK;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 340554
    :goto_1
    if-eq v1, p2, :cond_4

    .line 340555
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_2

    .line 340556
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/1uK;->h:Landroid/view/View;

    iput-object v0, p0, LX/1uK;->g:Landroid/view/View;

    move v0, v2

    .line 340557
    goto :goto_0

    .line 340558
    :cond_2
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_3

    move-object v0, v1

    .line 340559
    check-cast v0, Landroid/view/View;

    .line 340560
    :cond_3
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 340561
    :cond_4
    iput-object v0, p0, LX/1uK;->h:Landroid/view/View;

    .line 340562
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 340475
    iget-object v0, p0, LX/1uK;->i:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 340476
    return-void
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V
    .locals 1

    .prologue
    .line 340451
    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    if-eq v0, p1, :cond_0

    .line 340452
    iput-object p1, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    .line 340453
    const/4 v0, 0x0

    iput-object v0, p0, LX/1uK;->j:Ljava/lang/Object;

    .line 340454
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1uK;->b:Z

    .line 340455
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 340477
    iget-boolean v0, p0, LX/1uK;->k:Z

    if-eqz v0, :cond_0

    .line 340478
    const/4 v0, 0x1

    .line 340479
    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, LX/1uK;->k:Z

    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    invoke-virtual {v0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;->blocksInteractionBelow(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Z

    move-result v0

    :goto_1
    or-int/2addr v0, v1

    iput-boolean v0, p0, LX/1uK;->k:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 340474
    iget-object v0, p0, LX/1uK;->h:Landroid/view/View;

    if-eq p3, v0, :cond_0

    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout$Behavior;->layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/support/design/widget/CoordinatorLayout$Behavior;
    .locals 1

    .prologue
    .line 340473
    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    return-object v0
.end method

.method public final b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340467
    iget v1, p0, LX/1uK;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 340468
    iput-object v0, p0, LX/1uK;->h:Landroid/view/View;

    iput-object v0, p0, LX/1uK;->g:Landroid/view/View;

    .line 340469
    :goto_0
    return-object v0

    .line 340470
    :cond_0
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, p1}, LX/1uK;->b(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 340471
    :cond_1
    invoke-direct {p0, p2, p1}, LX/1uK;->a(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)V

    .line 340472
    :cond_2
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 340466
    iget-object v0, p0, LX/1uK;->g:Landroid/view/View;

    if-nez v0, :cond_0

    iget v0, p0, LX/1uK;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 340463
    iget-object v0, p0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    if-nez v0, :cond_0

    .line 340464
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1uK;->k:Z

    .line 340465
    :cond_0
    iget-boolean v0, p0, LX/1uK;->k:Z

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 340461
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1uK;->k:Z

    .line 340462
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 340459
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1uK;->l:Z

    .line 340460
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 340458
    iget-boolean v0, p0, LX/1uK;->l:Z

    return v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 340456
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1uK;->m:Z

    .line 340457
    return-void
.end method
