.class public LX/0cc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0cc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Sh;

.field private final c:Landroid/content/ContentResolver;

.field private final d:LX/0ce;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89124
    const-class v0, LX/0cc;

    sput-object v0, LX/0cc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;Landroid/content/ContentResolver;LX/0ce;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/abtest/qe/annotations/LoggedInUserIdHash;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Landroid/content/ContentResolver;",
            "LX/0ce;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 89118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89119
    iput-object p1, p0, LX/0cc;->b:LX/0Sh;

    .line 89120
    iput-object p2, p0, LX/0cc;->c:Landroid/content/ContentResolver;

    .line 89121
    iput-object p3, p0, LX/0cc;->d:LX/0ce;

    .line 89122
    iput-object p4, p0, LX/0cc;->e:LX/0Or;

    .line 89123
    return-void
.end method

.method public static a(LX/0QB;)LX/0cc;
    .locals 1

    .prologue
    .line 89117
    invoke-static {p0}, LX/0cc;->b(LX/0QB;)LX/0cc;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0cc;
    .locals 5

    .prologue
    .line 89115
    new-instance v3, LX/0cc;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0ce;->a(LX/0QB;)LX/0ce;

    move-result-object v2

    check-cast v2, LX/0ce;

    const/16 v4, 0x15e2

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/0cc;-><init>(LX/0Sh;Landroid/content/ContentResolver;LX/0ce;LX/0Or;)V

    .line 89116
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 89096
    iget-object v0, p0, LX/0cc;->c:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/0cc;->d:LX/0ce;

    iget-object v1, v1, LX/0ce;->c:LX/0cf;

    iget-object v1, v1, LX/0cf;->a:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    sget-object v3, LX/2VT;->b:LX/0U1;

    .line 89097
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 89098
    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2VT;->a:LX/0U1;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    aput-object p1, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 89099
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89100
    sget-object v0, LX/2VT;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 89101
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 89102
    :goto_0
    return-object v5

    .line 89103
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 89104
    iget-object v0, p0, LX/0cc;->c:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/0cc;->d:LX/0ce;

    iget-object v1, v1, LX/0ce;->c:LX/0cf;

    iget-object v1, v1, LX/0cf;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 89105
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 89106
    :try_start_0
    sget-object v2, LX/2VT;->a:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 89107
    sget-object v3, LX/2VT;->b:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 89108
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 89109
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 89110
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 89111
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 89112
    invoke-virtual {v0, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89113
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 89114
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
