.class public LX/1la;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1la;


# instance fields
.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imageformat/ImageFormat$FormatChecker;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/1lb;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 312063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312064
    new-instance v0, LX/1lb;

    invoke-direct {v0}, LX/1lb;-><init>()V

    iput-object v0, p0, LX/1la;->d:LX/1lb;

    .line 312065
    invoke-static {p0}, LX/1la;->b(LX/1la;)V

    .line 312066
    return-void
.end method

.method public static a(ILjava/io/InputStream;[B)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 312067
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312068
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312069
    array-length v0, p2

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 312070
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312071
    :try_start_0
    invoke-virtual {p1, p0}, Ljava/io/InputStream;->mark(I)V

    .line 312072
    const/4 v0, 0x0

    invoke-static {p1, p2, v0, p0}, LX/0aP;->a(Ljava/io/InputStream;[BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 312073
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 312074
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 312075
    goto :goto_0

    .line 312076
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    throw v0

    .line 312077
    :cond_1
    invoke-static {p1, p2, v1, p0}, LX/0aP;->a(Ljava/io/InputStream;[BII)I

    move-result v0

    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;)LX/1lW;
    .locals 6

    .prologue
    .line 312078
    invoke-static {}, LX/1la;->a()LX/1la;

    move-result-object v0

    .line 312079
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312080
    iget v1, v0, LX/1la;->b:I

    new-array v2, v1, [B

    .line 312081
    iget v1, v0, LX/1la;->b:I

    invoke-static {v1, p0, v2}, LX/1la;->a(ILjava/io/InputStream;[B)I

    move-result v3

    .line 312082
    iget-object v1, v0, LX/1la;->c:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 312083
    iget-object v1, v0, LX/1la;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lb;

    .line 312084
    invoke-virtual {v1, v2, v3}, LX/1lb;->a([BI)LX/1lW;

    move-result-object v1

    .line 312085
    if-eqz v1, :cond_0

    sget-object v5, LX/1lW;->a:LX/1lW;

    if-eq v1, v5, :cond_0

    .line 312086
    :cond_1
    :goto_0
    move-object v0, v1

    .line 312087
    return-object v0

    .line 312088
    :cond_2
    iget-object v1, v0, LX/1la;->d:LX/1lb;

    invoke-virtual {v1, v2, v3}, LX/1lb;->a([BI)LX/1lW;

    move-result-object v1

    .line 312089
    if-nez v1, :cond_1

    .line 312090
    sget-object v1, LX/1lW;->a:LX/1lW;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)LX/1lW;
    .locals 3

    .prologue
    .line 312091
    const/4 v0, 0x0

    .line 312092
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312093
    :try_start_1
    invoke-static {v1}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 312094
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    .line 312095
    :catch_0
    move-object v1, v0

    :goto_1
    :try_start_2
    sget-object v0, LX/1lW;->a:LX/1lW;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 312096
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    :goto_2
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 312097
    :catch_1
    goto :goto_1
.end method

.method public static declared-synchronized a()LX/1la;
    .locals 2

    .prologue
    .line 312098
    const-class v1, LX/1la;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1la;->a:LX/1la;

    if-nez v0, :cond_0

    .line 312099
    new-instance v0, LX/1la;

    invoke-direct {v0}, LX/1la;-><init>()V

    sput-object v0, LX/1la;->a:LX/1la;

    .line 312100
    :cond_0
    sget-object v0, LX/1la;->a:LX/1la;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 312101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Ljava/io/InputStream;)LX/1lW;
    .locals 1

    .prologue
    .line 312102
    :try_start_0
    invoke-static {p0}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 312103
    :catch_0
    move-exception v0

    .line 312104
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static b(LX/1la;)V
    .locals 4

    .prologue
    .line 312105
    iget-object v0, p0, LX/1la;->d:LX/1lb;

    .line 312106
    iget v1, v0, LX/1lb;->a:I

    move v0, v1

    .line 312107
    iput v0, p0, LX/1la;->b:I

    .line 312108
    iget-object v0, p0, LX/1la;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 312109
    iget-object v0, p0, LX/1la;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lb;

    .line 312110
    iget v2, p0, LX/1la;->b:I

    .line 312111
    iget v3, v0, LX/1lb;->a:I

    move v0, v3

    .line 312112
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/1la;->b:I

    goto :goto_0

    .line 312113
    :cond_0
    return-void
.end method
