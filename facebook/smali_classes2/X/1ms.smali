.class public LX/1ms;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mr;


# static fields
.field public static b:Ljava/lang/Class;

.field public static c:Z

.field public static d:Z

.field public static e:Ljava/lang/reflect/Constructor;


# instance fields
.field public final a:Landroid/view/DisplayList;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 314225
    sput-boolean v0, LX/1ms;->c:Z

    .line 314226
    sput-boolean v0, LX/1ms;->d:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/DisplayList;)V
    .locals 0

    .prologue
    .line 314242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314243
    iput-object p1, p0, LX/1ms;->a:Landroid/view/DisplayList;

    .line 314244
    return-void
.end method

.method public static b(Ljava/lang/String;)Landroid/view/DisplayList;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 314245
    :try_start_0
    const/4 v6, 0x1

    .line 314246
    sget-boolean v0, LX/1ms;->c:Z

    if-nez v0, :cond_0

    sget-boolean v0, LX/1ms;->d:Z

    if-eqz v0, :cond_2

    .line 314247
    :cond_0
    :goto_0
    sget-boolean v0, LX/1ms;->c:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 314248
    :goto_1
    return-object v0

    .line 314249
    :catch_0
    sput-boolean v2, LX/1ms;->d:Z

    .line 314250
    :cond_1
    :try_start_1
    sget-object v0, LX/1ms;->e:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/DisplayList;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 314251
    :catch_1
    move-object v0, v1

    goto :goto_1

    .line 314252
    :cond_2
    const-string v0, "android.view.GLES20DisplayList"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 314253
    sput-object v0, LX/1ms;->b:Ljava/lang/Class;

    new-array v3, v6, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 314254
    sput-object v0, LX/1ms;->e:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 314255
    sput-boolean v6, LX/1ms;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public a(II)Landroid/graphics/Canvas;
    .locals 3

    .prologue
    .line 314235
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0}, Landroid/view/DisplayList;->start()Landroid/view/HardwareCanvas;

    move-result-object v1

    move-object v0, v1

    .line 314236
    check-cast v0, Landroid/view/HardwareCanvas;

    invoke-virtual {v0, p1, p2}, Landroid/view/HardwareCanvas;->setViewport(II)V

    move-object v0, v1

    .line 314237
    check-cast v0, Landroid/view/HardwareCanvas;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/HardwareCanvas;->onPreDraw(Landroid/graphics/Rect;)I

    .line 314238
    check-cast v1, Landroid/graphics/Canvas;

    return-object v1
.end method

.method public a(IIII)V
    .locals 2

    .prologue
    .line 314239
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    .line 314240
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/DisplayList;->setClipChildren(Z)V

    .line 314241
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 314232
    check-cast p1, Landroid/view/HardwareCanvas;

    invoke-virtual {p1}, Landroid/view/HardwareCanvas;->onPostDraw()V

    .line 314233
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0}, Landroid/view/DisplayList;->end()V

    .line 314234
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 314231
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0}, Landroid/view/DisplayList;->isValid()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 314227
    instance-of v0, p1, Landroid/view/HardwareCanvas;

    if-nez v0, :cond_0

    .line 314228
    new-instance v0, LX/32E;

    new-instance v1, Ljava/lang/ClassCastException;

    invoke-direct {v1}, Ljava/lang/ClassCastException;-><init>()V

    invoke-direct {v0, v1}, LX/32E;-><init>(Ljava/lang/Exception;)V

    throw v0

    .line 314229
    :cond_0
    check-cast p1, Landroid/view/HardwareCanvas;

    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/HardwareCanvas;->drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I

    .line 314230
    return-void
.end method
