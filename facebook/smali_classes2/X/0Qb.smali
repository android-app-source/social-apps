.class public final LX/0Qb;
.super LX/0Qc;
.source ""

# interfaces
.implements LX/0QJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Qc",
        "<TK;TV;>;",
        "LX/0QJ",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0QN;LX/0QM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QN",
            "<-TK;-TV;>;",
            "LX/0QM",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58190
    new-instance v1, LX/0Qd;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QM;

    invoke-direct {v1, p1, v0}, LX/0Qd;-><init>(LX/0QN;LX/0QM;)V

    invoke-direct {p0, v1}, LX/0Qc;-><init>(LX/0Qd;)V

    .line 58191
    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 58199
    invoke-virtual {p0, p1}, LX/0Qb;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 58196
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    .line 58197
    iget-object p0, v0, LX/0Qd;->t:LX/0QM;

    invoke-virtual {v0, p1, p0}, LX/0Qd;->a(Ljava/lang/Object;LX/0QM;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    .line 58198
    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 58193
    :try_start_0
    invoke-virtual {p0, p1}, LX/0Qb;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 58194
    :catch_0
    move-exception v0

    .line 58195
    new-instance v1, LX/52T;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, LX/52T;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 58192
    new-instance v0, LX/4wJ;

    iget-object v1, p0, LX/0Qc;->localCache:LX/0Qd;

    invoke-direct {v0, v1}, LX/4wJ;-><init>(LX/0Qd;)V

    return-object v0
.end method
