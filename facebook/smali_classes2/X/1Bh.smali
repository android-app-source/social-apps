.class public final LX/1Bh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0p3;

.field public final b:I

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(LX/0p3;IZZZ)V
    .locals 7

    .prologue
    .line 213738
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/1Bh;-><init>(LX/0p3;IZZZZ)V

    .line 213739
    return-void
.end method

.method public constructor <init>(LX/0p3;IZZZZ)V
    .locals 0

    .prologue
    .line 213740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213741
    iput-object p1, p0, LX/1Bh;->a:LX/0p3;

    .line 213742
    iput p2, p0, LX/1Bh;->b:I

    .line 213743
    iput-boolean p3, p0, LX/1Bh;->c:Z

    .line 213744
    iput-boolean p4, p0, LX/1Bh;->d:Z

    .line 213745
    iput-boolean p5, p0, LX/1Bh;->e:Z

    .line 213746
    iput-boolean p6, p0, LX/1Bh;->f:Z

    .line 213747
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 213748
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "APSettingCheckerParams"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 213749
    const-string v1, ": connQuality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Bh;->a:LX/0p3;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213750
    const-string v1, ", minBW="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1Bh;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 213751
    const-string v1, ", savedOffline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/1Bh;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213752
    const-string v1, ", shouldAutoplayWhenOffline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/1Bh;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213753
    const-string v1, ", disableAutoplayWhenNotfound="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/1Bh;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213754
    const-string v1, ", minBwSetFromBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/1Bh;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213755
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
