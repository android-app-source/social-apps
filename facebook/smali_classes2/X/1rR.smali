.class public LX/1rR;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/os/Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 332211
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/os/Handler;
    .locals 3

    .prologue
    .line 332199
    sget-object v0, LX/1rR;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 332200
    const-class v1, LX/1rR;

    monitor-enter v1

    .line 332201
    :try_start_0
    sget-object v0, LX/1rR;->a:Landroid/os/Handler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332202
    if-eqz v2, :cond_0

    .line 332203
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332204
    invoke-static {v0}, LX/1rS;->a(LX/0QB;)Landroid/os/Looper;

    move-result-object p0

    check-cast p0, Landroid/os/Looper;

    invoke-static {p0}, LX/1rT;->a(Landroid/os/Looper;)Landroid/os/Handler;

    move-result-object p0

    move-object v0, p0

    .line 332205
    sput-object v0, LX/1rR;->a:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332206
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332208
    :cond_1
    sget-object v0, LX/1rR;->a:Landroid/os/Handler;

    return-object v0

    .line 332209
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 332198
    invoke-static {p0}, LX/1rS;->a(LX/0QB;)Landroid/os/Looper;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    invoke-static {v0}, LX/1rT;->a(Landroid/os/Looper;)Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method
