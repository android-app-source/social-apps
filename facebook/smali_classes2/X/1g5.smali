.class public LX/1g5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1g5;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 293281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293282
    return-void
.end method

.method public static a(LX/0QB;)LX/1g5;
    .locals 3

    .prologue
    .line 293283
    sget-object v0, LX/1g5;->a:LX/1g5;

    if-nez v0, :cond_1

    .line 293284
    const-class v1, LX/1g5;

    monitor-enter v1

    .line 293285
    :try_start_0
    sget-object v0, LX/1g5;->a:LX/1g5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 293286
    if-eqz v2, :cond_0

    .line 293287
    :try_start_1
    new-instance v0, LX/1g5;

    invoke-direct {v0}, LX/1g5;-><init>()V

    .line 293288
    move-object v0, v0

    .line 293289
    sput-object v0, LX/1g5;->a:LX/1g5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293290
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 293291
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 293292
    :cond_1
    sget-object v0, LX/1g5;->a:LX/1g5;

    return-object v0

    .line 293293
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 293294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6VK;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 293295
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 293296
    :cond_0
    const/4 v0, 0x0

    .line 293297
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p0, LX/6VK;->mName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 293298
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293299
    move-object v0, v0

    .line 293300
    goto :goto_0
.end method
