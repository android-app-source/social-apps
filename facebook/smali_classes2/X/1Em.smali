.class public LX/1Em;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1Em;


# instance fields
.field private final a:LX/1En;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/1FZ;


# direct methods
.method public constructor <init>(LX/1En;Ljava/util/concurrent/ExecutorService;LX/1FZ;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 221115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221116
    iput-object p1, p0, LX/1Em;->a:LX/1En;

    .line 221117
    iput-object p2, p0, LX/1Em;->b:Ljava/util/concurrent/ExecutorService;

    .line 221118
    iput-object p3, p0, LX/1Em;->c:LX/1FZ;

    .line 221119
    return-void
.end method

.method public static a(LX/0QB;)LX/1Em;
    .locals 6

    .prologue
    .line 221120
    sget-object v0, LX/1Em;->d:LX/1Em;

    if-nez v0, :cond_1

    .line 221121
    const-class v1, LX/1Em;

    monitor-enter v1

    .line 221122
    :try_start_0
    sget-object v0, LX/1Em;->d:LX/1Em;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221123
    if-eqz v2, :cond_0

    .line 221124
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221125
    new-instance p0, LX/1Em;

    invoke-static {v0}, LX/1En;->a(LX/0QB;)LX/1En;

    move-result-object v3

    check-cast v3, LX/1En;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v5

    check-cast v5, LX/1FZ;

    invoke-direct {p0, v3, v4, v5}, LX/1Em;-><init>(LX/1En;Ljava/util/concurrent/ExecutorService;LX/1FZ;)V

    .line 221126
    move-object v0, p0

    .line 221127
    sput-object v0, LX/1Em;->d:LX/1Em;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221128
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221129
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221130
    :cond_1
    sget-object v0, LX/1Em;->d:LX/1Em;

    return-object v0

    .line 221131
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1Em;II)LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221133
    const/4 v1, 0x0

    .line 221134
    :try_start_0
    iget-object v0, p0, LX/1Em;->c:LX/1FZ;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, p2, p2, v2}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 221135
    new-instance v2, Landroid/graphics/Canvas;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 221136
    invoke-virtual {v2, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 221137
    invoke-static {v1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 221138
    if-eqz v1, :cond_0

    .line 221139
    invoke-virtual {v1}, LX/1FJ;->close()V

    :cond_0
    return-object v0

    .line 221140
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 221141
    invoke-virtual {v1}, LX/1FJ;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;II)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7gj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 221142
    invoke-static {p0, p2, p3}, LX/1Em;->a(LX/1Em;II)LX/1FJ;

    move-result-object v8

    .line 221143
    new-instance v0, LX/7gj;

    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v7}, LX/7gj;-><init>(LX/7gi;Landroid/graphics/Bitmap;Ljava/lang/String;IIZF)V

    .line 221144
    iput-object p1, v0, LX/7gj;->i:Ljava/lang/String;

    .line 221145
    iput-object p1, v0, LX/7gj;->l:Ljava/lang/String;

    .line 221146
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 221147
    iget-object v2, p0, LX/1Em;->a:LX/1En;

    invoke-virtual {v2, v0}, LX/1En;->a(LX/7gj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 221148
    new-instance v3, LX/BaL;

    invoke-direct {v3, p0, v1, v0, v8}, LX/BaL;-><init>(LX/1Em;Lcom/google/common/util/concurrent/SettableFuture;LX/7gj;LX/1FJ;)V

    iget-object v0, p0, LX/1Em;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 221149
    return-object v1
.end method
