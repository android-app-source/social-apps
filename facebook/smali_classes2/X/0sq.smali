.class public final LX/0sq;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 153265
    sget-object v0, LX/0sr;->a:LX/0U1;

    sget-object v1, LX/0sr;->b:LX/0U1;

    sget-object v2, LX/0sr;->c:LX/0U1;

    sget-object v3, LX/0sr;->d:LX/0U1;

    sget-object v4, LX/0sr;->e:LX/0U1;

    sget-object v5, LX/0sr;->f:LX/0U1;

    sget-object v6, LX/0sr;->g:LX/0U1;

    sget-object v7, LX/0sr;->h:LX/0U1;

    sget-object v8, LX/0sr;->i:LX/0U1;

    sget-object v9, LX/0sr;->j:LX/0U1;

    sget-object v10, LX/0sr;->k:LX/0U1;

    sget-object v11, LX/0sr;->l:LX/0U1;

    const/4 v12, 0x1

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/0sr;->m:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0sq;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 153266
    const-string v0, "queries"

    sget-object v1, LX/0sq;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 153267
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 153268
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 153269
    const-string v0, "CREATE UNIQUE INDEX query_user_unique ON queries(query_id, user_id);"

    const v1, 0x6c4e0478

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7449f30e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153270
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 153271
    const/16 v0, 0x12

    if-gt p2, v0, :cond_0

    .line 153272
    const-string v0, "flat"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x90593ec

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6a655970

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153273
    :cond_0
    const/16 v0, 0x3e

    if-lt p2, v0, :cond_1

    .line 153274
    :goto_0
    return-void

    .line 153275
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0
.end method
