.class public LX/11F;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public static b:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/0sO;

.field private final d:LX/03V;

.field public final e:LX/0SG;

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0sO;LX/0SG;LX/03V;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 171371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171372
    iput-object p1, p0, LX/11F;->c:LX/0sO;

    .line 171373
    iput-object p2, p0, LX/11F;->e:LX/0SG;

    .line 171374
    iput-object p3, p0, LX/11F;->d:LX/03V;

    .line 171375
    iput-object p4, p0, LX/11F;->f:LX/0Uh;

    .line 171376
    const/16 v0, 0x91

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 171377
    sput-boolean v0, LX/11G;->d:Z

    .line 171378
    return-void
.end method

.method private a(Ljava/io/DataInputStream;LX/0v6;I)LX/1pP;
    .locals 6

    .prologue
    .line 171339
    invoke-static {p1}, LX/11F;->b(Ljava/io/DataInputStream;)I

    move-result v1

    .line 171340
    new-array v0, v1, [B

    .line 171341
    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 171342
    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 171343
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 171344
    const-string v0, "request_name"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171345
    new-instance v4, LX/1pP;

    invoke-direct {v4}, LX/1pP;-><init>()V

    .line 171346
    iput-object v0, v4, LX/1pP;->c:Ljava/lang/String;

    .line 171347
    iget-object v0, p2, LX/0v6;->a:Ljava/util/Map;

    iget-object v5, v4, LX/1pP;->c:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    iput-object v0, v4, LX/1pP;->g:LX/0zO;

    .line 171348
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, LX/1pP;->e:Ljava/lang/Boolean;

    .line 171349
    iget-object v0, v4, LX/1pP;->g:LX/0zO;

    if-nez v0, :cond_0

    .line 171350
    new-instance v0, LX/4Ub;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request name \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v4, LX/1pP;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4Ub;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171351
    :cond_0
    sub-int v0, p3, v1

    add-int/lit8 v0, v0, -0x4

    .line 171352
    if-lez v0, :cond_1

    .line 171353
    invoke-static {p1, v0}, LX/11F;->a(Ljava/io/DataInputStream;I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 171354
    :try_start_0
    iget-object v1, v4, LX/1pP;->g:LX/0zO;

    .line 171355
    iget-object v2, v1, LX/0zO;->n:LX/0w5;

    move-object v1, v2

    .line 171356
    invoke-virtual {v1, v0}, LX/0w5;->a(Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v1

    .line 171357
    iget-object v2, v4, LX/1pP;->g:LX/0zO;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {p0, v0, v1, v2, v3}, LX/11F;->a(LX/11F;Ljava/nio/ByteBuffer;Ljava/lang/Object;LX/0zO;LX/0ta;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    iput-object v0, v4, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171358
    :goto_0
    return-object v4

    .line 171359
    :catch_0
    move-exception v0

    .line 171360
    iput-object v0, v4, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_0

    .line 171361
    :cond_1
    if-nez v0, :cond_4

    .line 171362
    const-string v0, "error"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171363
    const-string v0, "error"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/error/GraphQLError;->a(Lorg/json/JSONObject;)Lcom/facebook/graphql/error/GraphQLError;

    move-result-object v0

    .line 171364
    if-eqz v0, :cond_2

    .line 171365
    invoke-static {v0}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v0

    iput-object v0, v4, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_0

    .line 171366
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    iput-object v0, v4, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_0

    .line 171367
    :cond_3
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, LX/1pP;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 171368
    :cond_4
    const-string v0, "Response chunk size should be bigger than chunk header size"

    .line 171369
    const-string v1, "FLATBUFFER_FROM_SERVER"

    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 171370
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(LX/0sO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171319
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171320
    :goto_0
    return-object p1

    .line 171321
    :cond_0
    iget-object v0, p0, LX/0sO;->j:LX/0sS;

    move-object v0, v0

    .line 171322
    if-eqz v0, :cond_1

    .line 171323
    iget-object v0, p0, LX/0sO;->j:LX/0sS;

    move-object v0, v0

    .line 171324
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 171325
    new-instance v3, LX/49M;

    invoke-direct {v3, v2}, LX/49M;-><init>(Landroid/util/SparseArray;)V

    .line 171326
    iget-object v8, v0, LX/0sS;->a:LX/0W3;

    sget-wide v10, LX/0X5;->eJ:J

    const/4 v9, 0x5

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JI)I

    move-result v8

    move v2, v8

    .line 171327
    new-instance v4, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;

    iget-object v5, v0, LX/0sS;->b:LX/0Zb;

    invoke-direct {v4, v5, v3, p2}, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;-><init>(LX/0Zb;LX/49M;Ljava/lang/String;)V

    .line 171328
    iget-object v5, v0, LX/0sS;->c:Ljava/util/concurrent/ScheduledExecutorService;

    int-to-long v6, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v4, v6, v7, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 171329
    const/4 v2, 0x1

    .line 171330
    iput-boolean v2, v3, LX/49M;->c:Z

    .line 171331
    move-object v0, v3

    .line 171332
    invoke-static {p1}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v1

    .line 171333
    if-eqz v1, :cond_1

    .line 171334
    iput-object v0, v1, LX/15i;->a:LX/49M;

    .line 171335
    :cond_1
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    .line 171336
    iput-object p2, v0, LX/1lO;->j:Ljava/lang/String;

    .line 171337
    move-object v0, v0

    .line 171338
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    goto :goto_0
.end method

.method private static a(LX/11F;Ljava/nio/ByteBuffer;Ljava/lang/Object;LX/0zO;LX/0ta;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Ljava/lang/Object;",
            "LX/0zO;",
            "LX/0ta;",
            ")",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 171287
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    .line 171288
    iget-object v1, v0, LX/0sO;->i:LX/0sQ;

    move-object v0, v1

    .line 171289
    if-eqz v0, :cond_0

    .line 171290
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    .line 171291
    iget-object v1, v0, LX/0sO;->i:LX/0sQ;

    move-object v0, v1

    .line 171292
    iget-object v1, p3, LX/0zO;->m:LX/0gW;

    move-object v1, v1

    .line 171293
    iget-object v2, v1, LX/0gW;->f:Ljava/lang/String;

    move-object v1, v2

    .line 171294
    iget-object v2, p3, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 171295
    iget-object v3, v2, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v3

    .line 171296
    invoke-virtual {v0, v1, v2, p2}, LX/0sQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171297
    instance-of v0, p2, Lcom/facebook/graphql/modelutil/BaseModel;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 171298
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 171299
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171300
    const-string v1, "GraphQLResponseParser.getQueryResult.isDirect"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 171301
    :goto_0
    invoke-static {v0}, LX/0sR;->b(Ljava/lang/Object;)LX/49J;

    move-result-object v1

    .line 171302
    if-eqz v1, :cond_0

    move-object v0, p2

    .line 171303
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    .line 171304
    invoke-static {v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Lcom/facebook/graphql/modelutil/BaseModel;LX/49J;)LX/49K;

    .line 171305
    :cond_0
    new-instance v0, LX/1lO;

    invoke-direct {v0}, LX/1lO;-><init>()V

    .line 171306
    iput-object p2, v0, LX/1lO;->k:Ljava/lang/Object;

    .line 171307
    move-object v0, v0

    .line 171308
    iput-object p4, v0, LX/1lO;->b:LX/0ta;

    .line 171309
    move-object v0, v0

    .line 171310
    iget-object v1, p0, LX/11F;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 171311
    iput-wide v2, v0, LX/1lO;->c:J

    .line 171312
    move-object v0, v0

    .line 171313
    iget-object v1, p3, LX/0zO;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    move-result-object v0

    .line 171314
    iget-boolean v1, p3, LX/0zO;->p:Z

    move v1, v1

    .line 171315
    iput-boolean v1, v0, LX/1lO;->n:Z

    .line 171316
    move-object v0, v0

    .line 171317
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0

    .line 171318
    :cond_1
    const-string v1, "GraphQLResponseParser.getQueryResult.isNotDirect"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 171280
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 171281
    const/16 v0, 0x800

    new-array v0, v0, [B

    .line 171282
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->read([B)I

    move-result v2

    .line 171283
    if-ltz v2, :cond_0

    .line 171284
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 171285
    :catch_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 171286
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method private static a(Ljava/io/DataInputStream;I)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 171275
    new-array v0, p1, [B

    .line 171276
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 171277
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 171278
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 171279
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 171266
    const-string v0, "GraphQLResponseParser.findTags"

    const v1, -0x724c7d12

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 171267
    :try_start_0
    instance-of v0, p0, LX/0jT;

    if-eqz v0, :cond_0

    .line 171268
    check-cast p0, LX/0jT;

    .line 171269
    new-instance v0, LX/22c;

    invoke-direct {v0}, LX/22c;-><init>()V

    .line 171270
    invoke-virtual {v0, p0}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 171271
    iget-object v0, v0, LX/22c;->a:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171272
    const v1, -0x21bc5c3d

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 171273
    :cond_0
    :try_start_1
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171274
    const v1, 0x23f9e8b6

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x455c538d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static b(Ljava/io/DataInputStream;)I
    .locals 3

    .prologue
    .line 171379
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 171380
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 171381
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/4 v2, 0x3

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    .line 171382
    return v0
.end method

.method public static b(LX/0QB;)LX/11F;
    .locals 5

    .prologue
    .line 171264
    new-instance v4, LX/11F;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/11F;-><init>(LX/0sO;LX/0SG;LX/03V;LX/0Uh;)V

    .line 171265
    return-object v4
.end method

.method private b(LX/15w;LX/0v6;)LX/1pP;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 171221
    new-instance v1, LX/1pP;

    invoke-direct {v1}, LX/1pP;-><init>()V

    .line 171222
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->e:Ljava/lang/Boolean;

    .line 171223
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->c:Ljava/lang/String;

    .line 171224
    iget-object v0, p2, LX/0v6;->a:Ljava/util/Map;

    iget-object v2, v1, LX/1pP;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    iput-object v0, v1, LX/1pP;->g:LX/0zO;

    .line 171225
    iget-object v0, v1, LX/1pP;->g:LX/0zO;

    if-nez v0, :cond_0

    .line 171226
    new-instance v0, LX/4Ub;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request name \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, LX/1pP;->c:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4Ub;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171227
    :cond_0
    invoke-virtual {p1}, LX/15w;->d()LX/15z;

    .line 171228
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_1

    move-object v0, v1

    .line 171229
    :goto_0
    return-object v0

    .line 171230
    :cond_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171231
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_b

    .line 171232
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 171233
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171234
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v2, v3, :cond_2

    .line 171235
    const-string v2, "response"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171236
    :try_start_0
    iget-object v0, v1, LX/1pP;->g:LX/0zO;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/11F;->a(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 171237
    iget-object v0, v1, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 171238
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 171239
    instance-of v2, v0, Ljava/util/HashMap;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171240
    const/4 v0, 0x0

    iput-object v0, v1, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 171241
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->e:Ljava/lang/Boolean;
    :try_end_0
    .catch LX/4Ua; {:try_start_0 .. :try_end_0} :catch_0

    .line 171242
    :cond_2
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 171243
    :catch_0
    move-exception v0

    .line 171244
    iput-object v0, v1, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_2

    .line 171245
    :cond_3
    const-string v2, "error"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 171246
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    invoke-virtual {v0, p1}, LX/0sO;->a(LX/15w;)LX/2Oo;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_2

    .line 171247
    :cond_4
    const-string v2, "query_id"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 171248
    const-string v2, "ref_params"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 171249
    sget-object v0, LX/11F;->b:LX/266;

    if-nez v0, :cond_5

    .line 171250
    new-instance v0, LX/39N;

    invoke-direct {v0}, LX/39N;-><init>()V

    sput-object v0, LX/11F;->b:LX/266;

    .line 171251
    :cond_5
    sget-object v0, LX/11F;->b:LX/266;

    move-object v0, v0

    .line 171252
    invoke-virtual {p1, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, v1, LX/1pP;->a:Ljava/util/Map;

    goto :goto_2

    .line 171253
    :cond_6
    const-string v2, "exports"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 171254
    sget-object v0, LX/11F;->a:LX/266;

    if-nez v0, :cond_7

    .line 171255
    new-instance v0, LX/265;

    invoke-direct {v0}, LX/265;-><init>()V

    sput-object v0, LX/11F;->a:LX/266;

    .line 171256
    :cond_7
    sget-object v0, LX/11F;->a:LX/266;

    move-object v0, v0

    .line 171257
    invoke-virtual {p1, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, v1, LX/1pP;->b:Ljava/util/Map;

    goto :goto_2

    .line 171258
    :cond_8
    const-string v2, "response_id"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 171259
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1pP;->h:Ljava/lang/String;

    goto :goto_2

    .line 171260
    :cond_9
    const-string v2, "GraphQLResponseParser"

    const-string v3, "received unknown response field %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171261
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v2, :cond_2

    .line 171262
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_2

    :cond_b
    move-object v0, v1

    .line 171263
    goto/16 :goto_0
.end method

.method private c(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0zO;",
            "LX/0ta;",
            "Z)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 171166
    invoke-virtual {p2}, LX/0zO;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171167
    const/4 v5, 0x1

    .line 171168
    iget v6, p2, LX/0zO;->A:I

    move v6, v6

    .line 171169
    if-ne v6, v5, :cond_4

    :goto_0
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 171170
    iget-object v5, p2, LX/0zO;->o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    move-object v5, v5

    .line 171171
    if-eqz v5, :cond_5

    .line 171172
    iput-boolean p4, v5, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->d:Z

    .line 171173
    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 171174
    :goto_1
    new-instance v6, LX/1lO;

    invoke-direct {v6}, LX/1lO;-><init>()V

    .line 171175
    iput-object v5, v6, LX/1lO;->k:Ljava/lang/Object;

    .line 171176
    move-object v5, v6

    .line 171177
    iput-object p3, v5, LX/1lO;->b:LX/0ta;

    .line 171178
    move-object v5, v5

    .line 171179
    iget-object v6, p0, LX/11F;->e:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    .line 171180
    iput-wide v7, v5, LX/1lO;->c:J

    .line 171181
    move-object v5, v5

    .line 171182
    iget-object v6, p2, LX/0zO;->d:Ljava/util/Set;

    invoke-virtual {v5, v6}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    move-result-object v5

    .line 171183
    iget-boolean v6, p2, LX/0zO;->p:Z

    move v6, v6

    .line 171184
    iput-boolean v6, v5, LX/1lO;->n:Z

    .line 171185
    move-object v5, v5

    .line 171186
    invoke-virtual {v5}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v5

    move-object v0, v5

    .line 171187
    :goto_2
    return-object v0

    .line 171188
    :cond_0
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    .line 171189
    iget-object v1, p2, LX/0zO;->m:LX/0gW;

    move-object v1, v1

    .line 171190
    iget-object v2, v1, LX/0gW;->f:Ljava/lang/String;

    move-object v1, v2

    .line 171191
    iget v2, p2, LX/0zO;->A:I

    move v2, v2

    .line 171192
    invoke-virtual {v0, v1, v2, p1}, LX/0sO;->a(Ljava/lang/String;ILX/15w;)LX/15w;

    .line 171193
    iget v0, p2, LX/0zO;->A:I

    move v0, v0

    .line 171194
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-ne v1, v2, :cond_1

    .line 171195
    add-int/lit8 v0, v0, -0x1

    .line 171196
    :cond_1
    iget-object v1, p2, LX/0zO;->n:LX/0w5;

    move-object v1, v1

    .line 171197
    invoke-virtual {v1, p1}, LX/0w5;->a(LX/15w;)Ljava/lang/Object;

    move-result-object v2

    .line 171198
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_3

    .line 171199
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    .line 171200
    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v3, v4, :cond_2

    .line 171201
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171202
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 171203
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 171204
    :cond_3
    new-instance v0, LX/1lO;

    invoke-direct {v0}, LX/1lO;-><init>()V

    .line 171205
    iput-object v2, v0, LX/1lO;->k:Ljava/lang/Object;

    .line 171206
    move-object v0, v0

    .line 171207
    iput-object p3, v0, LX/1lO;->b:LX/0ta;

    .line 171208
    move-object v0, v0

    .line 171209
    iget-object v1, p0, LX/11F;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 171210
    iput-wide v2, v0, LX/1lO;->c:J

    .line 171211
    move-object v0, v0

    .line 171212
    iget-object v1, p2, LX/0zO;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    move-result-object v0

    .line 171213
    iget-boolean v1, p2, LX/0zO;->p:Z

    move v1, v1

    .line 171214
    iput-boolean v1, v0, LX/1lO;->n:Z

    .line 171215
    move-object v0, v0

    .line 171216
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    goto :goto_2

    .line 171217
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 171218
    :cond_5
    iget-object v5, p0, LX/11F;->c:LX/0sO;

    .line 171219
    iget-object v6, p2, LX/0zO;->n:LX/0w5;

    move-object v6, v6

    .line 171220
    invoke-virtual {v5, v6, p1}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v5

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0zO;",
            "LX/0ta;",
            "Z)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 171109
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    .line 171110
    iget-object v2, v0, LX/0sO;->i:LX/0sQ;

    move-object v0, v2

    .line 171111
    if-eqz v0, :cond_0

    .line 171112
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    .line 171113
    iget-object v2, v0, LX/0sO;->i:LX/0sQ;

    move-object v0, v2

    .line 171114
    iget-object v2, p2, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 171115
    iget-object v3, v2, LX/0gW;->f:Ljava/lang/String;

    move-object v2, v3

    .line 171116
    iget-object v3, p2, LX/0zO;->m:LX/0gW;

    move-object v3, v3

    .line 171117
    iget-object v4, v3, LX/0gW;->h:Ljava/lang/String;

    move-object v3, v4

    .line 171118
    invoke-virtual {v0, v2, v3, p1}, LX/0sQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171119
    :cond_0
    if-nez p4, :cond_1

    .line 171120
    invoke-direct {p0, p1, p2, p3, v7}, LX/11F;->c(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 171121
    :goto_0
    return-object v0

    .line 171122
    :cond_1
    invoke-static {p1}, LX/261;->a(LX/15w;)LX/15z;

    .line 171123
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 171124
    move-object v2, v1

    .line 171125
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v3, v4, :cond_b

    .line 171126
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 171127
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171128
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    .line 171129
    const-string v4, "data"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 171130
    invoke-direct {p0, p1, p2, p3, v8}, LX/11F;->c(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 171131
    :cond_2
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 171132
    :cond_3
    const-string v4, "errors"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 171133
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    invoke-virtual {v0, p1}, LX/0sO;->b(LX/15w;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 171134
    :cond_4
    const-string v4, "extensions"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 171135
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 171136
    const/4 v1, 0x0

    .line 171137
    new-array v3, v6, [LX/15z;

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    aput-object v4, v3, v5

    invoke-static {p1, v3}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 171138
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171139
    :goto_3
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v3, v4, :cond_8

    .line 171140
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 171141
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171142
    const-string v4, "response_id"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 171143
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 171144
    :cond_5
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_3

    .line 171145
    :cond_6
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_5

    .line 171146
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 171147
    :cond_8
    new-array v3, v6, [LX/15z;

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    aput-object v4, v3, v5

    invoke-static {p1, v3}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 171148
    move-object v1, v1

    .line 171149
    goto :goto_2

    .line 171150
    :cond_9
    const-string v4, "GraphQLResponseParser"

    const-string v5, "received unknown response field %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171151
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_a

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 171152
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_2

    .line 171153
    :cond_b
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_d

    .line 171154
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v5, v3

    :goto_5
    if-ge v5, v6, :cond_f

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Oo;

    .line 171155
    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v4, :cond_e

    .line 171156
    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/error/GraphQLError;

    invoke-virtual {v4}, Lcom/facebook/graphql/error/GraphQLError;->k()LX/4UZ;

    move-result-object v4

    sget-object p2, LX/4UZ;->CRITICAL:LX/4UZ;

    if-ne v4, p2, :cond_e

    .line 171157
    :goto_6
    move-object v3, v3

    .line 171158
    if-eqz v3, :cond_c

    .line 171159
    throw v3

    .line 171160
    :cond_c
    if-nez v2, :cond_d

    .line 171161
    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oo;

    throw v0

    .line 171162
    :cond_d
    new-array v0, v8, [LX/15z;

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    aput-object v3, v0, v7

    invoke-static {p1, v0}, LX/261;->a(LX/15w;[LX/15z;)V

    .line 171163
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    invoke-static {v0, v2, v1}, LX/11F;->a(LX/0sO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    goto/16 :goto_0

    .line 171164
    :cond_e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5

    .line 171165
    :cond_f
    const/4 v3, 0x0

    goto :goto_6
.end method

.method public final a(Ljava/io/InputStream;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 8

    .prologue
    .line 171088
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 171089
    invoke-static {v0}, LX/11F;->b(Ljava/io/DataInputStream;)I

    move-result v1

    .line 171090
    const v2, 0xf4240

    if-le v1, v2, :cond_0

    .line 171091
    const/16 v2, 0x100

    new-array v2, v2, [B

    .line 171092
    :try_start_0
    invoke-virtual {v0, v2}, Ljava/io/DataInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171093
    :goto_0
    iget-object v3, p0, LX/11F;->d:LX/03V;

    const-string v4, "FLATBUFFER_FROM_SERVER"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Server response length exceeds max flatbuffer size:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v6, v2, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171094
    :cond_0
    invoke-static {v0}, LX/11F;->b(Ljava/io/DataInputStream;)I

    move-result v2

    .line 171095
    new-array v3, v2, [B

    .line 171096
    invoke-virtual {v0, v3}, Ljava/io/DataInputStream;->readFully([B)V

    .line 171097
    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v3, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 171098
    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x4

    .line 171099
    if-lez v1, :cond_1

    .line 171100
    invoke-static {v0, v1}, LX/11F;->a(Ljava/io/DataInputStream;I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 171101
    iget-object v1, p2, LX/0zO;->n:LX/0w5;

    move-object v1, v1

    .line 171102
    invoke-virtual {v1, v0}, LX/0w5;->a(Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v1

    .line 171103
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {p0, v0, v1, p2, v2}, LX/11F;->a(LX/11F;Ljava/nio/ByteBuffer;Ljava/lang/Object;LX/0zO;LX/0ta;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0

    .line 171104
    :cond_1
    if-nez v1, :cond_2

    .line 171105
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171106
    :cond_2
    const-string v0, "Response size should be bigger than header size"

    .line 171107
    const-string v1, "FLATBUFFER_FROM_SERVER"

    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 171108
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    goto :goto_0
.end method

.method public final a(LX/15w;LX/0v6;)Ljava/lang/Void;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 171023
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171024
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v5

    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    .line 171025
    :goto_0
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-ne v6, v7, :cond_14

    if-nez v0, :cond_14

    .line 171026
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171027
    const-string v6, "successful_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "error_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "skipped_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 171028
    :cond_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 171029
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v6, :cond_4

    .line 171030
    const-string v0, "successful_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171031
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v3

    .line 171032
    :cond_1
    :goto_2
    invoke-virtual {p1}, LX/15w;->d()LX/15z;

    goto :goto_1

    .line 171033
    :cond_2
    const-string v0, "error_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171034
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v2

    goto :goto_2

    .line 171035
    :cond_3
    const-string v0, "skipped_results"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171036
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v1

    goto :goto_2

    .line 171037
    :cond_4
    const/4 v0, 0x1

    move v10, v1

    move v1, v2

    move v2, v3

    move v3, v0

    move v0, v10

    .line 171038
    :goto_3
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-ne v6, v7, :cond_1a

    .line 171039
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_3

    .line 171040
    :cond_5
    const-string v6, "error"

    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 171041
    invoke-virtual {p1}, LX/15w;->d()LX/15z;

    .line 171042
    iget-object v0, p0, LX/11F;->c:LX/0sO;

    invoke-virtual {v0, p1}, LX/0sO;->a(LX/15w;)LX/2Oo;

    move-result-object v0

    throw v0

    .line 171043
    :cond_6
    invoke-direct {p0, p1, p2}, LX/11F;->b(LX/15w;LX/0v6;)LX/1pP;

    move-result-object v6

    .line 171044
    iget-object v7, v6, LX/1pP;->c:Ljava/lang/String;

    if-eqz v7, :cond_7

    .line 171045
    iget-object v7, v6, LX/1pP;->c:Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171046
    add-int/lit8 v4, v4, 0x1

    .line 171047
    :cond_7
    iget-object v7, v6, LX/1pP;->g:LX/0zO;

    if-nez v7, :cond_8

    .line 171048
    new-instance v7, LX/4Ub;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "No such request "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v6, LX/1pP;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iput-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    .line 171049
    :cond_8
    iget-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    if-nez v7, :cond_a

    iget-object v7, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v7, :cond_a

    iget-object v7, v6, LX/1pP;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_a

    .line 171050
    new-instance v7, LX/4Ub;

    const-string v8, "Received null error and null result"

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iput-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    .line 171051
    :cond_9
    :goto_4
    iget-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    if-eqz v7, :cond_d

    .line 171052
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v7

    iget-object v8, v6, LX/1pP;->d:Ljava/lang/Exception;

    iget-object v6, v6, LX/1pP;->g:LX/0zO;

    invoke-interface {v7, v8, v6}, LX/11X;->a(Ljava/lang/Exception;LX/0zO;)V

    move v10, v1

    move v1, v2

    move v2, v3

    move v3, v0

    move v0, v10

    goto :goto_3

    .line 171053
    :cond_a
    iget-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    if-eqz v7, :cond_b

    iget-object v7, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eqz v7, :cond_b

    .line 171054
    new-instance v7, LX/4Ub;

    const-string v8, "Received both an error and a result"

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iput-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_4

    .line 171055
    :cond_b
    iget-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    if-eqz v7, :cond_c

    iget-object v7, v6, LX/1pP;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 171056
    new-instance v7, LX/4Ub;

    const-string v8, "Received error for skipped query"

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iput-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_4

    .line 171057
    :cond_c
    iget-object v7, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eqz v7, :cond_9

    iget-object v7, v6, LX/1pP;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 171058
    new-instance v7, LX/4Ub;

    const-string v8, "Received result for skipped query"

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iput-object v7, v6, LX/1pP;->d:Ljava/lang/Exception;

    goto :goto_4

    .line 171059
    :cond_d
    iget-object v7, v6, LX/1pP;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_13

    .line 171060
    iget-object v7, v6, LX/1pP;->a:Ljava/util/Map;

    if-nez v7, :cond_e

    iget-object v7, v6, LX/1pP;->b:Ljava/util/Map;

    if-nez v7, :cond_e

    iget-object v7, v6, LX/1pP;->h:Ljava/lang/String;

    if-eqz v7, :cond_12

    .line 171061
    :cond_e
    iget-object v7, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-static {v7}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v7

    .line 171062
    iget-object v8, v6, LX/1pP;->a:Ljava/util/Map;

    if-eqz v8, :cond_f

    .line 171063
    iget-object v8, v6, LX/1pP;->a:Ljava/util/Map;

    iput-object v8, v7, LX/1lO;->e:Ljava/util/Map;

    .line 171064
    :cond_f
    iget-object v8, v6, LX/1pP;->b:Ljava/util/Map;

    if-eqz v8, :cond_10

    .line 171065
    iget-object v8, v6, LX/1pP;->b:Ljava/util/Map;

    iput-object v8, v7, LX/1lO;->f:Ljava/util/Map;

    .line 171066
    :cond_10
    iget-object v8, v6, LX/1pP;->h:Ljava/lang/String;

    if-eqz v8, :cond_11

    .line 171067
    iget-object v8, v6, LX/1pP;->h:Ljava/lang/String;

    iput-object v8, v7, LX/1lO;->j:Ljava/lang/String;

    .line 171068
    :cond_11
    iget-object v8, v6, LX/1pP;->c:Ljava/lang/String;

    .line 171069
    iput-object v8, v7, LX/1lO;->i:Ljava/lang/String;

    .line 171070
    invoke-virtual {v7}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v7

    iput-object v7, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 171071
    :cond_12
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v7

    iget-object v8, v6, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v6, v6, LX/1pP;->g:LX/0zO;

    invoke-interface {v7, v8, v6}, LX/11X;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/0zO;)V

    :cond_13
    move v10, v1

    move v1, v2

    move v2, v3

    move v3, v0

    move v0, v10

    goto/16 :goto_3

    .line 171072
    :cond_14
    if-nez v0, :cond_15

    .line 171073
    iget-object v0, p0, LX/11F;->d:LX/03V;

    const-string v6, "graphql_error"

    const-string v7, "Did not receive an end of message result"

    invoke-virtual {v0, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171074
    :cond_15
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v7

    .line 171075
    iget-object v0, p2, LX/0v6;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_16
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 171076
    iget-object v6, p2, LX/0v6;->a:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0zO;

    .line 171077
    iget-object v9, p2, LX/0v6;->d:Ljava/util/Set;

    invoke-interface {v9, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_16

    .line 171078
    invoke-virtual {v7, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 171079
    :cond_17
    invoke-virtual {v7}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 171080
    invoke-interface {v5, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 171081
    iget-object v0, p2, LX/0v6;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v5}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    .line 171082
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 171083
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v6

    new-instance v7, LX/4Ub;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Did not receive response for "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, LX/4Ub;-><init>(Ljava/lang/String;)V

    iget-object v8, p2, LX/0v6;->a:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    invoke-interface {v6, v7, v0}, LX/11X;->a(Ljava/lang/Exception;LX/0zO;)V

    goto :goto_6

    .line 171084
    :cond_18
    add-int v0, v3, v2

    add-int/2addr v0, v1

    if-eq v4, v0, :cond_19

    .line 171085
    iget-object v0, p0, LX/11F;->d:LX/03V;

    const-string v1, "graphql_error"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Received a different number of results than the server sent ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " vs. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/2addr v2, v3

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " received)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171086
    :cond_19
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v0

    invoke-interface {v0}, LX/11X;->a()V

    .line 171087
    const/4 v0, 0x0

    return-object v0

    :cond_1a
    move v10, v0

    move v0, v3

    move v3, v2

    move v2, v1

    move v1, v10

    goto/16 :goto_0
.end method

.method public final a(Ljava/io/InputStream;LX/0v6;)Ljava/lang/Void;
    .locals 6

    .prologue
    .line 170991
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 170992
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x33

    if-ge v0, v2, :cond_0

    .line 170993
    invoke-static {v1}, LX/11F;->b(Ljava/io/DataInputStream;)I

    move-result v2

    .line 170994
    const v3, 0xf4240

    if-le v2, v3, :cond_2

    .line 170995
    const v0, 0x7265227b

    if-ne v2, v0, :cond_1

    .line 170996
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "{\"er"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/11F;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170997
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 170998
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/error/GraphQLError;->a(Lorg/json/JSONObject;)Lcom/facebook/graphql/error/GraphQLError;

    move-result-object v1

    .line 170999
    if-eqz v1, :cond_0

    .line 171000
    invoke-static {v1}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v1

    throw v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171001
    :catch_0
    iget-object v1, p0, LX/11F;->d:LX/03V;

    const-string v2, "FLATBUFFER_FROM_SERVER"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not able to parse error message:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171002
    :cond_0
    :goto_1
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v0

    invoke-interface {v0}, LX/11X;->a()V

    .line 171003
    const/4 v0, 0x0

    return-object v0

    .line 171004
    :cond_1
    const/16 v0, 0x100

    new-array v0, v0, [B

    .line 171005
    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 171006
    :goto_2
    iget-object v1, p0, LX/11F;->d:LX/03V;

    const-string v3, "FLATBUFFER_FROM_SERVER"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Story length exceeds max story size:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v0, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 171007
    :cond_2
    if-nez v2, :cond_3

    .line 171008
    if-nez v0, :cond_0

    .line 171009
    :try_start_2
    invoke-static {v1}, LX/11F;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v0

    .line 171010
    if-eqz v0, :cond_0

    .line 171011
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 171012
    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/error/GraphQLError;->a(Lorg/json/JSONObject;)Lcom/facebook/graphql/error/GraphQLError;

    move-result-object v0

    .line 171013
    if-eqz v0, :cond_0

    .line 171014
    invoke-static {v0}, LX/261;->a(Lcom/facebook/graphql/error/GraphQLError;)LX/2Oo;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 171015
    :catch_1
    goto :goto_1

    .line 171016
    :cond_3
    const/16 v3, 0x32

    if-ne v0, v3, :cond_4

    .line 171017
    iget-object v0, p0, LX/11F;->d:LX/03V;

    const-string v1, "FLATBUFFER_FROM_SERVER"

    const-string v2, "Number of stories exceeds maximum story limit"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 171018
    :cond_4
    invoke-direct {p0, v1, p2, v2}, LX/11F;->a(Ljava/io/DataInputStream;LX/0v6;I)LX/1pP;

    move-result-object v2

    .line 171019
    iget-object v3, v2, LX/1pP;->d:Ljava/lang/Exception;

    if-eqz v3, :cond_5

    .line 171020
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v3

    iget-object v4, v2, LX/1pP;->d:Ljava/lang/Exception;

    iget-object v2, v2, LX/1pP;->g:LX/0zO;

    invoke-interface {v3, v4, v2}, LX/11X;->a(Ljava/lang/Exception;LX/0zO;)V

    .line 171021
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 171022
    :cond_5
    invoke-virtual {p2}, LX/0v6;->e()LX/11X;

    move-result-object v3

    iget-object v4, v2, LX/1pP;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v2, v2, LX/1pP;->g:LX/0zO;

    invoke-interface {v3, v4, v2}, LX/11X;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/0zO;)V

    goto :goto_3

    :catch_2
    goto :goto_2
.end method

.method public final b(Ljava/io/InputStream;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 170982
    invoke-static {p1}, LX/0hW;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 170983
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 170984
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 170985
    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 170986
    invoke-static {v1, v0, v3}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 170987
    invoke-static {v1, v0, v3}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v6

    .line 170988
    iget-object v0, p2, LX/0zO;->n:LX/0w5;

    move-object v7, v0

    .line 170989
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    invoke-virtual {v7, v0, v6}, LX/0w5;->a(LX/15i;I)Ljava/lang/Object;

    move-result-object v0

    .line 170990
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {p0, v1, v0, p2, v2}, LX/11F;->a(LX/11F;Ljava/nio/ByteBuffer;Ljava/lang/Object;LX/0zO;LX/0ta;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method
