.class public LX/19j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile aX:LX/19j;


# instance fields
.field public final A:F

.field public final B:F

.field public final C:I

.field public final D:I

.field public final E:Z

.field public final F:Z

.field public final G:Z

.field public final H:Z

.field public final I:J

.field public final J:I

.field public final K:Z

.field public final L:I

.field public final M:I

.field public final N:Z

.field public final O:I

.field public final P:I

.field public final Q:Z

.field public final R:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0p3;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final S:Z

.field public final T:Z

.field public final U:I

.field public final V:I

.field public final W:Z

.field public final X:Z

.field public final Y:Z

.field public final Z:Z

.field public final a:Z

.field public final aA:Z

.field public final aB:I

.field public final aC:I

.field public final aD:I

.field public final aE:I

.field public final aF:I

.field public final aG:I

.field public final aH:F

.field public final aI:F

.field public final aJ:Z

.field public final aK:Z

.field public final aL:Z

.field public final aM:Z

.field public final aN:Z

.field public final aO:Z

.field public final aP:Z

.field public final aQ:J

.field public final aR:I

.field public final aS:I

.field public final aT:Z

.field public final aU:I

.field public final aV:Z

.field public final aW:I

.field public final aa:Z

.field public final ab:Z

.field public final ac:Z

.field public final ad:Z

.field public final ae:Z

.field public final af:I

.field public final ag:Z

.field public final ah:Z

.field public final ai:I

.field public final aj:J

.field public final ak:Z

.field public final al:Z

.field public final am:Z

.field public final an:J

.field public final ao:Z

.field public final ap:Z

.field public final aq:Z

.field public final ar:Z

.field public final as:I

.field public final at:I

.field public final au:I

.field public final av:Z

.field public final aw:Z

.field public final ax:Z

.field public final ay:Z

.field public final az:Z

.field public final b:Z

.field public final c:LX/19k;

.field public final d:Z

.field public final e:LX/0p3;

.field public final f:I

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:LX/0p3;

.field public final k:I

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z

.field public final s:I

.field public final t:I

.field public final u:Z

.field public final v:Z

.field public final w:Z

.field public final x:Z

.field public final y:I

.field public final z:I


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 12
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208368
    sget-short v0, LX/0ws;->bP:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aA:Z

    .line 208369
    sget v0, LX/0ws;->cu:I

    const/16 v1, 0x1aa

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aB:I

    .line 208370
    sget v0, LX/0ws;->cv:I

    const/16 v1, 0x3e8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aC:I

    .line 208371
    sget v0, LX/0ws;->cw:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aD:I

    .line 208372
    sget v0, LX/0ws;->cx:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aE:I

    .line 208373
    sget v0, LX/0ws;->cy:I

    const/16 v1, 0x1388

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aF:I

    .line 208374
    sget v0, LX/0ws;->cz:I

    const/16 v1, 0x61a8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aG:I

    .line 208375
    sget v0, LX/0ws;->cq:F

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/19j;->aH:F

    .line 208376
    sget v0, LX/0ws;->ct:F

    const v1, 0x3f666666    # 0.9f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/19j;->aI:F

    .line 208377
    sget-short v0, LX/0ws;->cs:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aJ:Z

    .line 208378
    sget-short v0, LX/0ws;->cB:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aK:Z

    .line 208379
    sget-short v0, LX/0ws;->cC:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aL:Z

    .line 208380
    sget-short v0, LX/0ws;->cr:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aM:Z

    .line 208381
    sget-short v0, LX/0ws;->cA:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aN:Z

    .line 208382
    sget-short v0, LX/0ws;->bt:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aO:Z

    .line 208383
    sget-short v0, LX/0ws;->ch:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aP:Z

    .line 208384
    sget-short v0, LX/0ws;->dt:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->a:Z

    .line 208385
    sget-short v0, LX/0ws;->cl:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->H:Z

    .line 208386
    sget-short v0, LX/0ws;->ci:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->T:Z

    .line 208387
    sget v0, LX/0ws;->by:I

    const/16 v1, 0xfa0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v11

    .line 208388
    new-instance v0, LX/19k;

    sget-short v1, LX/0ws;->cb:S

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    sget-short v2, LX/0ws;->cc:S

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    sget v3, LX/0ws;->bJ:I

    invoke-interface {p1, v3, v11}, LX/0ad;->a(II)I

    move-result v3

    sget v4, LX/0ws;->bK:I

    invoke-interface {p1, v4, v11}, LX/0ad;->a(II)I

    move-result v4

    sget v5, LX/0ws;->bL:I

    invoke-interface {p1, v5, v11}, LX/0ad;->a(II)I

    move-result v5

    sget v6, LX/0ws;->bM:I

    invoke-interface {p1, v6, v11}, LX/0ad;->a(II)I

    move-result v6

    sget v7, LX/0ws;->bN:I

    invoke-interface {p1, v7, v11}, LX/0ad;->a(II)I

    move-result v7

    sget v8, LX/0ws;->bI:I

    invoke-interface {p1, v8, v11}, LX/0ad;->a(II)I

    move-result v8

    sget v9, LX/0ws;->bH:I

    invoke-interface {p1, v9, v11}, LX/0ad;->a(II)I

    move-result v9

    sget v10, LX/0ws;->bG:I

    invoke-interface {p1, v10, v11}, LX/0ad;->a(II)I

    move-result v10

    invoke-direct/range {v0 .. v11}, LX/19k;-><init>(ZZIIIIIIIII)V

    iput-object v0, p0, LX/19j;->c:LX/19k;

    .line 208389
    sget-short v0, LX/0ws;->di:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->d:Z

    .line 208390
    sget-char v0, LX/0ws;->cF:C

    sget-object v1, LX/0p3;->GOOD:LX/0p3;

    invoke-virtual {v1}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208391
    const-class v0, LX/0p3;

    sget-object v2, LX/0p3;->MODERATE:LX/0p3;

    invoke-static {v0, v1, v2}, LX/19j;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0p3;

    iput-object v0, p0, LX/19j;->e:LX/0p3;

    .line 208392
    sget v0, LX/0ws;->cE:I

    const/16 v2, 0x1f4

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->f:I

    .line 208393
    sget-short v0, LX/0ws;->bv:S

    const/4 v2, 0x1

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->h:Z

    .line 208394
    sget-short v0, LX/0ws;->bu:S

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->g:Z

    .line 208395
    sget-short v0, LX/0ws;->du:S

    const/4 v2, 0x1

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->i:Z

    .line 208396
    const-class v0, LX/0p3;

    sget-char v2, LX/0xY;->B:C

    invoke-interface {p1, v2, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/19j;->e:LX/0p3;

    invoke-static {v0, v1, v2}, LX/19j;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0p3;

    iput-object v0, p0, LX/19j;->j:LX/0p3;

    .line 208397
    sget v0, LX/0xY;->A:I

    iget v1, p0, LX/19j;->f:I

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->k:I

    .line 208398
    sget-short v0, LX/0ws;->do:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->o:Z

    .line 208399
    sget-short v0, LX/0ws;->bR:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->l:Z

    .line 208400
    sget-short v0, LX/0ws;->bT:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->m:Z

    .line 208401
    sget-short v0, LX/0ws;->bQ:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->n:Z

    .line 208402
    sget-short v0, LX/0ws;->bS:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->q:Z

    .line 208403
    sget-short v0, LX/0ws;->ck:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->r:Z

    .line 208404
    sget v0, LX/0ws;->dg:I

    const/16 v1, 0x14

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->s:I

    .line 208405
    sget v0, LX/0ws;->bp:I

    const/16 v1, 0x1388

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->t:I

    .line 208406
    sget-short v0, LX/0ws;->dr:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->u:Z

    .line 208407
    sget-short v0, LX/0ws;->cm:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    sget-short v0, LX/0ws;->ag:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/19j;->p:Z

    .line 208408
    sget-short v1, LX/0ws;->bF:S

    iget-boolean v0, p0, LX/19j;->u:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {p1, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->b:Z

    .line 208409
    sget-short v0, LX/0ws;->cg:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->v:Z

    .line 208410
    sget-short v0, LX/0ws;->dq:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->w:Z

    .line 208411
    sget-short v0, LX/0ws;->bW:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->x:Z

    .line 208412
    sget v0, LX/0ws;->bC:I

    const/16 v1, 0x3a98

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->y:I

    .line 208413
    sget v0, LX/0ws;->bx:I

    const/16 v1, 0x7530

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->z:I

    .line 208414
    sget v0, LX/0ws;->bB:F

    const v1, 0x3e4ccccd    # 0.2f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/19j;->A:F

    .line 208415
    sget v0, LX/0ws;->bw:F

    const v1, 0x3f4ccccd    # 0.8f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/19j;->B:F

    .line 208416
    sget v0, LX/0ws;->cK:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->C:I

    .line 208417
    sget v0, LX/0ws;->cL:I

    const/16 v1, 0x1f4

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->D:I

    .line 208418
    sget-short v0, LX/0ws;->da:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->E:Z

    .line 208419
    sget-short v0, LX/0ws;->db:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->F:Z

    .line 208420
    sget-short v0, LX/0ws;->cj:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->G:Z

    .line 208421
    sget v0, LX/0ws;->br:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/19j;->I:J

    .line 208422
    sget v0, LX/0ws;->cD:I

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->J:I

    .line 208423
    sget-short v0, LX/0ws;->dp:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->K:Z

    .line 208424
    sget v0, LX/0ws;->bz:I

    const/16 v1, 0x1388

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->L:I

    .line 208425
    sget v0, LX/0ws;->bA:I

    const/16 v1, 0x1388

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->M:I

    .line 208426
    sget-short v0, LX/0ws;->bV:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->N:Z

    .line 208427
    sget v0, LX/0ws;->dh:I

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->O:I

    .line 208428
    sget v0, LX/0ws;->cM:I

    const/4 v1, 0x4

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->P:I

    .line 208429
    sget-short v0, LX/0ws;->cd:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->Q:Z

    .line 208430
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/0p3;->EXCELLENT:LX/0p3;

    sget v2, LX/0ws;->cN:I

    iget v3, p0, LX/19j;->P:I

    invoke-interface {p1, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0p3;->GOOD:LX/0p3;

    sget v2, LX/0ws;->cO:I

    iget v3, p0, LX/19j;->P:I

    invoke-interface {p1, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0p3;->MODERATE:LX/0p3;

    sget v2, LX/0ws;->cP:I

    iget v3, p0, LX/19j;->P:I

    invoke-interface {p1, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0p3;->POOR:LX/0p3;

    iget v2, p0, LX/19j;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    iget v2, p0, LX/19j;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/19j;->R:Ljava/util/Map;

    .line 208431
    sget-short v0, LX/0ws;->bU:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->S:Z

    .line 208432
    sget v0, LX/0ws;->cJ:I

    const/16 v1, 0xa

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->U:I

    .line 208433
    sget v0, LX/0ws;->cI:I

    const/4 v1, 0x6

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->V:I

    .line 208434
    sget-short v0, LX/0ws;->cp:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->W:Z

    .line 208435
    sget-short v0, LX/0ws;->ca:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->X:Z

    .line 208436
    sget-short v0, LX/0ws;->bE:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->Y:Z

    .line 208437
    sget-short v0, LX/0ws;->dv:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->Z:Z

    .line 208438
    const/16 v0, 0x2cf

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aa:Z

    .line 208439
    sget-short v0, LX/0ws;->cn:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ab:Z

    .line 208440
    sget-short v0, LX/0ws;->bY:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ac:Z

    .line 208441
    sget-short v0, LX/0ws;->ds:S

    iget-boolean v1, p0, LX/19j;->ac:Z

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ad:Z

    .line 208442
    sget v0, LX/0ws;->cV:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->af:I

    .line 208443
    sget-short v0, LX/0ws;->cf:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ag:Z

    .line 208444
    sget-short v0, LX/0ws;->cQ:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ah:Z

    .line 208445
    sget v0, LX/0ws;->cH:I

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->ai:I

    .line 208446
    sget-wide v0, LX/0ws;->df:J

    const-wide/16 v2, 0x0

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/19j;->aj:J

    .line 208447
    sget-short v0, LX/0ws;->dl:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ak:Z

    .line 208448
    sget-short v0, LX/0ws;->dm:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ae:Z

    .line 208449
    sget-short v0, LX/0ws;->bD:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->al:Z

    .line 208450
    sget-short v0, LX/0ws;->cW:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->am:Z

    .line 208451
    sget-wide v0, LX/0ws;->dd:J

    const-wide/32 v2, 0x493e0

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/19j;->an:J

    .line 208452
    sget-short v0, LX/0ws;->dk:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ao:Z

    .line 208453
    sget-short v0, LX/0ws;->dn:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ap:Z

    .line 208454
    sget-short v0, LX/0ws;->cR:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aq:Z

    .line 208455
    sget-short v0, LX/0ws;->cX:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ar:Z

    .line 208456
    sget v0, LX/0ws;->cS:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->as:I

    .line 208457
    sget v0, LX/0ws;->cT:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->at:I

    .line 208458
    sget v0, LX/0ws;->cU:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->au:I

    .line 208459
    sget-short v0, LX/0ws;->em:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->av:Z

    .line 208460
    sget-short v0, LX/0ws;->ce:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aw:Z

    .line 208461
    sget-short v0, LX/0ws;->cZ:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ax:Z

    .line 208462
    sget-short v0, LX/0ws;->bZ:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->ay:Z

    .line 208463
    sget-short v0, LX/0ws;->dc:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->az:Z

    .line 208464
    sget-wide v0, LX/0ws;->dw:J

    const-wide/16 v2, 0x3e8

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/19j;->aQ:J

    .line 208465
    sget v0, LX/0ws;->cG:I

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aR:I

    .line 208466
    sget v0, LX/0ws;->cY:I

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aS:I

    .line 208467
    sget-short v0, LX/0ws;->bs:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aT:Z

    .line 208468
    sget v0, LX/0ws;->bq:I

    const/16 v1, 0xc8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aU:I

    .line 208469
    sget-short v0, LX/0ws;->dx:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19j;->aV:Z

    .line 208470
    sget v0, LX/0ws;->co:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19j;->aW:I

    .line 208471
    return-void

    .line 208472
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 208473
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)LX/19j;
    .locals 5

    .prologue
    .line 208474
    sget-object v0, LX/19j;->aX:LX/19j;

    if-nez v0, :cond_1

    .line 208475
    const-class v1, LX/19j;

    monitor-enter v1

    .line 208476
    :try_start_0
    sget-object v0, LX/19j;->aX:LX/19j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208477
    if-eqz v2, :cond_0

    .line 208478
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208479
    new-instance p0, LX/19j;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/19j;-><init>(LX/0ad;LX/0Uh;)V

    .line 208480
    move-object v0, p0

    .line 208481
    sput-object v0, LX/19j;->aX:LX/19j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208482
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208483
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208484
    :cond_1
    sget-object v0, LX/19j;->aX:LX/19j;

    return-object v0

    .line 208485
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 208487
    if-nez p1, :cond_0

    .line 208488
    :goto_0
    return-object p2

    .line 208489
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 208490
    :catch_0
    goto :goto_0
.end method
