.class public LX/1QR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pq;
.implements LX/1Px;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1R6;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244581
    iput-object p1, p0, LX/1QR;->a:Ljava/lang/Runnable;

    .line 244582
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1QR;->b:Ljava/util/List;

    .line 244583
    return-void
.end method


# virtual methods
.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 244584
    iget-object v0, p0, LX/1QR;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244585
    return-void
.end method

.method public final varargs a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4

    .prologue
    .line 244586
    array-length v0, p1

    new-array v2, v0, [Ljava/lang/Object;

    .line 244587
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 244588
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 244589
    if-nez v1, :cond_0

    aget-object v1, p1, v0

    .line 244590
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 244591
    :cond_0
    aput-object v1, v2, v0

    .line 244592
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244593
    :cond_1
    invoke-virtual {p0, v2}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 244594
    return-void
.end method

.method public final varargs a([Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 244595
    iget-object v0, p0, LX/1QR;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244596
    :cond_0
    :goto_0
    return-void

    .line 244597
    :cond_1
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, p1, v1

    .line 244598
    iget-object v0, p0, LX/1QR;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1R6;

    .line 244599
    invoke-interface {v0, v3}, LX/1R6;->a(Ljava/lang/Object;)V

    goto :goto_2

    .line 244600
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 244601
    :cond_3
    iget-boolean v0, p0, LX/1QR;->c:Z

    if-eqz v0, :cond_0

    .line 244602
    invoke-virtual {p0}, LX/1QR;->iN_()V

    goto :goto_0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 244603
    iget-object v0, p0, LX/1QR;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 244604
    return-void
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 244605
    iget-object v0, p0, LX/1QR;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 244606
    return-void
.end method

.method public final m_(Z)V
    .locals 0

    .prologue
    .line 244607
    iput-boolean p1, p0, LX/1QR;->c:Z

    .line 244608
    return-void
.end method
