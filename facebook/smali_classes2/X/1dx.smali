.class public LX/1dx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1dx;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0Zb;

.field private final c:LX/00G;

.field public d:I


# direct methods
.method public constructor <init>(LX/0SG;LX/0Zb;LX/00G;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287181
    const/4 v0, 0x0

    iput v0, p0, LX/1dx;->d:I

    .line 287182
    iput-object p1, p0, LX/1dx;->a:LX/0SG;

    .line 287183
    iput-object p2, p0, LX/1dx;->b:LX/0Zb;

    .line 287184
    iput-object p3, p0, LX/1dx;->c:LX/00G;

    .line 287185
    return-void
.end method

.method public static a(LX/0QB;)LX/1dx;
    .locals 6

    .prologue
    .line 287167
    sget-object v0, LX/1dx;->e:LX/1dx;

    if-nez v0, :cond_1

    .line 287168
    const-class v1, LX/1dx;

    monitor-enter v1

    .line 287169
    :try_start_0
    sget-object v0, LX/1dx;->e:LX/1dx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 287170
    if-eqz v2, :cond_0

    .line 287171
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 287172
    new-instance p0, LX/1dx;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v5

    check-cast v5, LX/00G;

    invoke-direct {p0, v3, v4, v5}, LX/1dx;-><init>(LX/0SG;LX/0Zb;LX/00G;)V

    .line 287173
    move-object v0, p0

    .line 287174
    sput-object v0, LX/1dx;->e:LX/1dx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 287176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 287177
    :cond_1
    sget-object v0, LX/1dx;->e:LX/1dx;

    return-object v0

    .line 287178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 287179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/Throwable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 287149
    const-class v0, LX/2Oo;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 287150
    :goto_0
    return v0

    .line 287151
    :cond_0
    const-class v0, Ljava/net/UnknownHostException;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 287152
    goto :goto_0

    .line 287153
    :cond_1
    const-class v0, Ljava/net/ConnectException;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 287154
    goto :goto_0

    .line 287155
    :cond_2
    const-class v0, Ljava/net/SocketException;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 287156
    goto :goto_0

    .line 287157
    :cond_3
    const-class v0, LX/4bK;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 287158
    goto :goto_0

    .line 287159
    :cond_4
    const-class v0, Ljava/io/IOException;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 287160
    goto :goto_0

    .line 287161
    :cond_5
    const-class v0, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {p0, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 287162
    if-eqz v0, :cond_6

    sget-object v3, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    .line 287163
    iget-object p0, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, p0

    .line 287164
    invoke-virtual {v3, v0}, LX/1nY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 287165
    goto :goto_0

    :cond_6
    move v0, v1

    .line 287166
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/3G3;)V
    .locals 3

    .prologue
    .line 287120
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "offline_mode_operation_saved"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "offline"

    .line 287121
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 287122
    move-object v0, v0

    .line 287123
    const-string v1, "request_id"

    iget-object v2, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "operation_type"

    invoke-virtual {p1}, LX/3G3;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 287124
    iget-object v1, p0, LX/1dx;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 287125
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3G3;)V
    .locals 6

    .prologue
    .line 287143
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "offline"

    .line 287144
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 287145
    move-object v0, v0

    .line 287146
    const-string v1, "request_id"

    iget-object v2, p2, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "time_spent_pending_ms"

    iget-object v2, p0, LX/1dx;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p2, LX/3G3;->d:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "attempts_number"

    iget v2, p2, LX/3G3;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "operation_type"

    invoke-virtual {p2}, LX/3G3;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 287147
    iget-object v1, p0, LX/1dx;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 287148
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 287186
    iget-object v0, p0, LX/1dx;->c:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 4

    .prologue
    .line 287132
    invoke-virtual {p0}, LX/1dx;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287133
    invoke-static {p1}, LX/1dx;->b(Ljava/lang/Throwable;)Z

    move-result v0

    .line 287134
    iget-object v1, p0, LX/1dx;->b:LX/0Zb;

    const-string v2, "offline_mode_exception"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 287135
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287136
    const-string v2, "is_offline_exception"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 287137
    const-string v2, "exception_class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 287138
    const-string v2, "exception_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 287139
    const-string v2, "stack_trace"

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 287140
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 287141
    :cond_0
    move v0, v0

    .line 287142
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;LX/3Fz;)Z
    .locals 1

    .prologue
    .line 287130
    sget-object v0, LX/3Fz;->a:LX/3Fz;

    if-eq p2, v0, :cond_0

    invoke-virtual {p0, p1}, LX/1dx;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 287131
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/3G3;)V
    .locals 1

    .prologue
    .line 287128
    const-string v0, "offline_mode_operation_retried"

    invoke-virtual {p0, v0, p1}, LX/1dx;->a(Ljava/lang/String;LX/3G3;)V

    .line 287129
    return-void
.end method

.method public final c(LX/3G3;)V
    .locals 1

    .prologue
    .line 287126
    const-string v0, "offline_mode_operation_retry_succeeded"

    invoke-virtual {p0, v0, p1}, LX/1dx;->a(Ljava/lang/String;LX/3G3;)V

    .line 287127
    return-void
.end method
