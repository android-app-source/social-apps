.class public LX/1Bg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213710
    iput-object p1, p0, LX/1Bg;->a:LX/0Ot;

    .line 213711
    iput-object p2, p0, LX/1Bg;->b:LX/0Ot;

    .line 213712
    return-void
.end method

.method public static a(LX/0QB;)LX/1Bg;
    .locals 1

    .prologue
    .line 213713
    invoke-static {p0}, LX/1Bg;->b(LX/0QB;)LX/1Bg;

    move-result-object v0

    return-object v0
.end method

.method public static final a(LX/01T;LX/0WV;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 213714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 213715
    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213716
    const-string v1, "%s/%s;%s/%s;"

    const-string v2, "FB_IAB"

    invoke-virtual {p0}, LX/01T;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1Bg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "FBAV"

    invoke-virtual {p1}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1Bg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213717
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213718
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 213719
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213720
    const-string v0, ""

    .line 213721
    :goto_0
    return-object v0

    .line 213722
    :cond_0
    invoke-static {p0}, LX/0YN;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213723
    const-string v1, "/"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1Bg;
    .locals 3

    .prologue
    .line 213724
    new-instance v0, LX/1Bg;

    const/16 v1, 0x17d

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2ba

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1Bg;-><init>(LX/0Ot;LX/0Ot;)V

    .line 213725
    return-object v0
.end method


# virtual methods
.method public final a(LX/0DW;)V
    .locals 5

    .prologue
    .line 213726
    iget-object v0, p0, LX/1Bg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 213727
    iget-object v0, p0, LX/1Bg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 213728
    :cond_0
    :goto_0
    return-void

    .line 213729
    :cond_1
    iget-object v0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v1, v0

    .line 213730
    iget-object v0, p0, LX/1Bg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {v0, v1}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 213731
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 213732
    if-eqz v2, :cond_2

    .line 213733
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 213734
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213735
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 213737
    :cond_2
    const-string v0, "https://facebook.com/"

    invoke-virtual {p1, v0, v3}, LX/0DW;->a(Ljava/lang/String;Ljava/util/ArrayList;)LX/0DW;

    goto :goto_0
.end method
