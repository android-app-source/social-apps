.class public LX/0Y9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y4;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile o:LX/0Y9;


# instance fields
.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0YA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YC;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YB;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YH;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YF;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field public m:J

.field public n:LX/0Td;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80566
    const-class v0, LX/0Y9;

    sput-object v0, LX/0Y9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0YD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0YC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0YH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0YF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0YB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80676
    iput-object p1, p0, LX/0Y9;->c:LX/0Ot;

    .line 80677
    iput-object p2, p0, LX/0Y9;->d:LX/0Ot;

    .line 80678
    iput-object p3, p0, LX/0Y9;->f:LX/0Ot;

    .line 80679
    iput-object p4, p0, LX/0Y9;->g:LX/0Ot;

    .line 80680
    iput-object p5, p0, LX/0Y9;->e:LX/0Ot;

    .line 80681
    new-instance v0, Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;-><init>(LX/0Y9;)V

    iput-object v0, p0, LX/0Y9;->h:Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;

    .line 80682
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->WAIT:LX/0YA;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 80683
    const/high16 v0, -0x80000000

    iput v0, p0, LX/0Y9;->j:I

    .line 80684
    return-void
.end method

.method public static a(LX/0QB;)LX/0Y9;
    .locals 9

    .prologue
    .line 80662
    sget-object v0, LX/0Y9;->o:LX/0Y9;

    if-nez v0, :cond_1

    .line 80663
    const-class v1, LX/0Y9;

    monitor-enter v1

    .line 80664
    :try_start_0
    sget-object v0, LX/0Y9;->o:LX/0Y9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80665
    if-eqz v2, :cond_0

    .line 80666
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80667
    new-instance v3, LX/0Y9;

    const/16 v4, 0x128c

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x128a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1289

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x272

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1288

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/0Y9;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 80668
    move-object v0, v3

    .line 80669
    sput-object v0, LX/0Y9;->o:LX/0Y9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80670
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80671
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80672
    :cond_1
    sget-object v0, LX/0Y9;->o:LX/0Y9;

    return-object v0

    .line 80673
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80674
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILjava/lang/String;)Z
    .locals 6

    .prologue
    .line 80648
    invoke-static {p0, p1, p2}, LX/0Y9;->b(LX/0Y9;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 80649
    iget-object v0, p0, LX/0Y9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YB;

    invoke-virtual {v0}, LX/0YB;->b()I

    move-result v0

    const/4 p1, 0x3

    if-lt v0, p1, :cond_2

    move v0, v1

    .line 80650
    :goto_0
    move v0, v0

    .line 80651
    if-nez v0, :cond_1

    .line 80652
    :cond_0
    const/4 v0, 0x0

    .line 80653
    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, LX/0Y9;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YC;

    iget-object v1, p0, LX/0Y9;->k:Ljava/lang/String;

    .line 80654
    iget-object v2, v0, LX/0YC;->d:LX/0VP;

    invoke-virtual {v2}, LX/0VP;->c()Ljava/lang/String;

    move-result-object v2

    .line 80655
    if-nez v2, :cond_4

    .line 80656
    const/4 v2, 0x0

    .line 80657
    :goto_2
    move-object v0, v2

    .line 80658
    invoke-static {p0, v0}, LX/0Y9;->a(LX/0Y9;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 80659
    :cond_2
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object p1, LX/0YA;->READY:LX/0YA;

    sget-object p2, LX/0YA;->TRACING:LX/0YA;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 80660
    goto :goto_0

    .line 80661
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, LX/0YC;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".trace"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private static a(LX/0Y9;Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 80627
    iput-object p1, p0, LX/0Y9;->l:Ljava/lang/String;

    .line 80628
    iget-object v0, p0, LX/0Y9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YD;

    iget-object v1, p0, LX/0Y9;->l:Ljava/lang/String;

    const/4 v2, 0x0

    .line 80629
    iget-object v3, v0, LX/0YD;->b:LX/0YE;

    invoke-virtual {v3}, LX/0YE;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 80630
    :goto_0
    move v0, v2

    .line 80631
    if-nez v0, :cond_0

    .line 80632
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->READY:LX/0YA;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 80633
    const/4 v0, 0x0

    .line 80634
    :goto_1
    return v0

    .line 80635
    :cond_0
    iget-object v2, p0, LX/0Y9;->n:LX/0Td;

    if-nez v2, :cond_1

    .line 80636
    iget-object v2, p0, LX/0Y9;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0YF;

    const-string v3, "StopTraceRunnable"

    sget-object v4, LX/0TP;->URGENT:LX/0TP;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/0YF;->a(Ljava/lang/String;LX/0TP;Z)LX/0Te;

    move-result-object v2

    check-cast v2, LX/0Td;

    iput-object v2, p0, LX/0Y9;->n:LX/0Td;

    .line 80637
    :cond_1
    iget-object v2, p0, LX/0Y9;->n:LX/0Td;

    iget-object v3, p0, LX/0Y9;->h:Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;

    iget-wide v4, p0, LX/0Y9;->m:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0Td;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    .line 80638
    iget-object v0, p0, LX/0Y9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YB;

    .line 80639
    iget-object v1, v0, LX/0YB;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DebugTraceRetryKey"

    invoke-virtual {v0}, LX/0YB;->b()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80640
    const/4 v0, 0x1

    goto :goto_1

    .line 80641
    :cond_2
    :try_start_0
    iget v3, v0, LX/0YD;->e:I

    if-nez v3, :cond_3

    .line 80642
    iget v3, v0, LX/0YD;->c:I

    iget v4, v0, LX/0YD;->d:I

    invoke-static {v1, v3, v4}, Landroid/os/Debug;->startMethodTracingSampling(Ljava/lang/String;II)V

    .line 80643
    :goto_2
    const/4 v2, 0x1

    goto :goto_0

    .line 80644
    :cond_3
    iget v3, v0, LX/0YD;->c:I

    invoke-static {v1, v3}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 80645
    :catch_0
    move-exception v3

    .line 80646
    sget-object v4, LX/0YD;->a:Ljava/lang/Class;

    const-string v5, "RuntimeException while starting trace"

    new-array p1, v2, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80647
    iget-object v3, v0, LX/0YD;->b:LX/0YE;

    invoke-virtual {v3}, LX/0YE;->b()V

    goto :goto_0
.end method

.method public static a(LX/0Y9;Z)Z
    .locals 6

    .prologue
    .line 80608
    iget-object v0, p0, LX/0Y9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YD;

    invoke-virtual {v0}, LX/0YD;->a()Z

    .line 80609
    iget-object v0, p0, LX/0Y9;->n:LX/0Td;

    .line 80610
    iget-object v1, v0, LX/0Td;->a:Landroid/os/Handler;

    move-object v0, v1

    .line 80611
    iget-object v1, p0, LX/0Y9;->h:Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 80612
    if-eqz p1, :cond_0

    .line 80613
    iget-object v0, p0, LX/0Y9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YB;

    .line 80614
    iget-object v1, v0, LX/0YB;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DebugTraceFinishedKey"

    const/4 p1, 0x1

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80615
    iget-object v0, p0, LX/0Y9;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YH;

    invoke-virtual {v0}, LX/0YH;->a()Z

    .line 80616
    :goto_0
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->DONE:LX/0YA;

    sget-object v2, LX/0YA;->WAIT:LX/0YA;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 80617
    sget-object v0, LX/0Y9;->a:Ljava/lang/Class;

    const-string v1, "PerfDebugTracer state should be DONE, but isn\'t in internalStopTrace()"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 80618
    const/4 v0, 0x0

    .line 80619
    :goto_1
    return v0

    .line 80620
    :cond_0
    iget-object v0, p0, LX/0Y9;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YC;

    iget-object v1, p0, LX/0Y9;->l:Ljava/lang/String;

    .line 80621
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80622
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 80623
    iget-object v3, v0, LX/0YC;->g:LX/0YI;

    invoke-virtual {v3, v2}, LX/0YI;->a(Ljava/io/File;)V

    .line 80624
    sget-object v3, LX/0YC;->a:Ljava/lang/Class;

    const-string v4, "Error: failed to delete traceFile %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-static {v3, v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80625
    :cond_1
    goto :goto_0

    .line 80626
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static b(LX/0Y9;ILjava/lang/String;)Z
    .locals 4
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80604
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/0Y9;->i:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 80605
    :goto_0
    iget v3, p0, LX/0Y9;->j:I

    if-ne p1, v3, :cond_2

    iget-object v3, p0, LX/0Y9;->k:Ljava/lang/String;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 80606
    goto :goto_0

    :cond_2
    move v2, v1

    .line 80607
    goto :goto_1
.end method

.method public static d(LX/0Y9;)Z
    .locals 3

    .prologue
    .line 80685
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->TRACING:LX/0YA;

    sget-object v2, LX/0YA;->DONE:LX/0YA;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80686
    const/4 v0, 0x0

    .line 80687
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Pn;)V
    .locals 2

    .prologue
    .line 80601
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80602
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0Y9;->a(ILjava/lang/String;)Z

    .line 80603
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;IJII)Z
    .locals 3

    .prologue
    .line 80579
    if-nez p1, :cond_1

    const/high16 v0, -0x80000000

    if-eq p2, v0, :cond_0

    if-nez p3, :cond_1

    .line 80580
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error: passed in null for markerNAme and (null for quicklogEvent or 0 for markerID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80581
    :cond_1
    if-gtz p4, :cond_2

    .line 80582
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error: passed in a non-positive trace file size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80583
    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-gtz v0, :cond_3

    .line 80584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error: passed in a non-positive maximum trace time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80585
    :cond_3
    invoke-static {p8}, LX/0YJ;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 80586
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error: passed in an invalid trace type for trace configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80587
    :cond_4
    if-nez p8, :cond_5

    if-gtz p7, :cond_5

    .line 80588
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error: passed in a non-positive value for sampling interval time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80589
    :cond_5
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->WAIT:LX/0YA;

    sget-object v2, LX/0YA;->READY:LX/0YA;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 80590
    const/4 v0, 0x0

    .line 80591
    :goto_0
    return v0

    .line 80592
    :cond_6
    iput-object p1, p0, LX/0Y9;->i:Ljava/lang/String;

    .line 80593
    iput p2, p0, LX/0Y9;->j:I

    .line 80594
    iput-object p3, p0, LX/0Y9;->k:Ljava/lang/String;

    .line 80595
    iput-wide p5, p0, LX/0Y9;->m:J

    .line 80596
    iget-object v0, p0, LX/0Y9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YD;

    .line 80597
    iput p4, v0, LX/0YD;->c:I

    .line 80598
    iput p7, v0, LX/0YD;->d:I

    .line 80599
    iput p8, v0, LX/0YD;->e:I

    .line 80600
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 80577
    iget-object v0, p0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0YA;->READY:LX/0YA;

    sget-object v2, LX/0YA;->WAIT:LX/0YA;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 80578
    return-void
.end method

.method public final b(LX/0Pn;)V
    .locals 0

    .prologue
    .line 80575
    invoke-virtual {p0, p1}, LX/0Y9;->a(LX/0Pn;)V

    .line 80576
    return-void
.end method

.method public final c(LX/0Pn;)V
    .locals 1

    .prologue
    .line 80572
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80573
    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, LX/0Y9;->b(LX/0Y9;ILjava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {p0}, LX/0Y9;->d(LX/0Y9;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 80574
    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 p1, 0x1

    invoke-static {p0, p1}, LX/0Y9;->a(LX/0Y9;Z)Z

    goto :goto_0
.end method

.method public final d(LX/0Pn;)V
    .locals 2

    .prologue
    .line 80568
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80569
    const/4 v1, 0x0

    .line 80570
    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, LX/0Y9;->b(LX/0Y9;ILjava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {p0}, LX/0Y9;->d(LX/0Y9;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 80571
    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, v1}, LX/0Y9;->a(LX/0Y9;Z)Z

    goto :goto_0
.end method

.method public final e(LX/0Pn;)V
    .locals 0

    .prologue
    .line 80567
    return-void
.end method
