.class public LX/0mo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0mI;


# instance fields
.field private final a:LX/0mp;


# direct methods
.method public constructor <init>(LX/0mp;)V
    .locals 0

    .prologue
    .line 133086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133087
    iput-object p1, p0, LX/0mo;->a:LX/0mp;

    .line 133088
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 133089
    iget-object v0, p0, LX/0mo;->a:LX/0mp;

    const v2, 0x7fffffff

    .line 133090
    iget v1, v0, LX/0mp;->h:I

    if-ne v1, v2, :cond_2

    iget v1, v0, LX/0mp;->e:I

    rem-int v1, v2, v1

    add-int/lit8 v1, v1, 0x1

    :goto_0
    iput v1, v0, LX/0mp;->h:I

    .line 133091
    iget v1, v0, LX/0mp;->h:I

    iget v2, v0, LX/0mp;->e:I

    rem-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 133092
    iget-object v3, v0, LX/0mp;->a:LX/0mh;

    const-string v4, "marauder"

    const-string v5, "pigeon_beacon"

    sget-object v6, LX/0mq;->CLIENT_EVENT:LX/0mq;

    iget-boolean v7, v0, LX/0mp;->f:Z

    invoke-virtual {v3, v4, v5, v6, v7}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;

    move-result-object v3

    .line 133093
    iget-object v4, v0, LX/0mp;->g:LX/1Zu;

    if-nez v4, :cond_0

    .line 133094
    new-instance v4, LX/1Zu;

    iget-object v5, v0, LX/0mp;->b:Landroid/content/Context;

    iget-object v6, v0, LX/0mp;->c:Ljava/lang/String;

    .line 133095
    const-string v7, "analytics_beacon"

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v7

    .line 133096
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v5, v8

    .line 133097
    invoke-direct {v4, v5}, LX/1Zu;-><init>(Ljava/io/File;)V

    iput-object v4, v0, LX/0mp;->g:LX/1Zu;

    .line 133098
    :cond_0
    iget-object v4, v0, LX/0mp;->g:LX/1Zu;

    move-object v4, v4

    .line 133099
    invoke-virtual {v4}, LX/1Zu;->a()J

    move-result-wide v5

    .line 133100
    const-string v4, "tier"

    iget-object v7, v0, LX/0mp;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    move-result-object v3

    const-string v4, "beacon_id"

    invoke-static {v5, v6}, LX/1Zu;->a(J)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    move-result-object v3

    const-string v4, "beacon_session_id"

    invoke-static {v5, v6}, LX/1Zu;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    move-result-object v3

    .line 133101
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/0mj;->g:Z

    .line 133102
    move-object v3, v3

    .line 133103
    invoke-virtual {v3}, LX/0mj;->e()V

    .line 133104
    :cond_1
    return-void

    .line 133105
    :cond_2
    iget v1, v0, LX/0mp;->h:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
