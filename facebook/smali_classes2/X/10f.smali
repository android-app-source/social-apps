.class public final enum LX/10f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/10f;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/10f;

.field public static final enum LOAD_IMMEDIATELY:LX/10f;

.field public static final enum LOAD_WHEN_FOCUSED:LX/10f;

.field public static final enum LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED:LX/10f;

.field public static final enum LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

.field public static final enum LOAD_WHEN_VISIBLE:LX/10f;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169223
    new-instance v0, LX/10f;

    const-string v1, "LOAD_IMMEDIATELY"

    invoke-direct {v0, v1, v2}, LX/10f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10f;->LOAD_IMMEDIATELY:LX/10f;

    .line 169224
    new-instance v0, LX/10f;

    const-string v1, "LOAD_WHEN_VISIBLE"

    invoke-direct {v0, v1, v3}, LX/10f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10f;->LOAD_WHEN_VISIBLE:LX/10f;

    .line 169225
    new-instance v0, LX/10f;

    const-string v1, "LOAD_WHEN_FOCUSED"

    invoke-direct {v0, v1, v4}, LX/10f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10f;->LOAD_WHEN_FOCUSED:LX/10f;

    .line 169226
    new-instance v0, LX/10f;

    const-string v1, "LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE"

    invoke-direct {v0, v1, v5}, LX/10f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

    .line 169227
    new-instance v0, LX/10f;

    const-string v1, "LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED"

    invoke-direct {v0, v1, v6}, LX/10f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED:LX/10f;

    .line 169228
    const/4 v0, 0x5

    new-array v0, v0, [LX/10f;

    sget-object v1, LX/10f;->LOAD_IMMEDIATELY:LX/10f;

    aput-object v1, v0, v2

    sget-object v1, LX/10f;->LOAD_WHEN_VISIBLE:LX/10f;

    aput-object v1, v0, v3

    sget-object v1, LX/10f;->LOAD_WHEN_FOCUSED:LX/10f;

    aput-object v1, v0, v4

    sget-object v1, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

    aput-object v1, v0, v5

    sget-object v1, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED:LX/10f;

    aput-object v1, v0, v6

    sput-object v0, LX/10f;->$VALUES:[LX/10f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169229
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/10f;
    .locals 1

    .prologue
    .line 169230
    const-class v0, LX/10f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/10f;

    return-object v0
.end method

.method public static values()[LX/10f;
    .locals 1

    .prologue
    .line 169231
    sget-object v0, LX/10f;->$VALUES:[LX/10f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/10f;

    return-object v0
.end method


# virtual methods
.method public final shouldLoadImmediately()Z
    .locals 1

    .prologue
    .line 169232
    sget-object v0, LX/10f;->LOAD_IMMEDIATELY:LX/10f;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldLoadWhenFocused()Z
    .locals 1

    .prologue
    .line 169233
    sget-object v0, LX/10f;->LOAD_WHEN_FOCUSED:LX/10f;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED:LX/10f;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldLoadWhenIdle()Z
    .locals 1

    .prologue
    .line 169234
    sget-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_FOCUSED:LX/10f;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldLoadWhenVisible()Z
    .locals 1

    .prologue
    .line 169235
    sget-object v0, LX/10f;->LOAD_WHEN_VISIBLE:LX/10f;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
