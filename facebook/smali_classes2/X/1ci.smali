.class public abstract LX/1ci;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1cj",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 282504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282505
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    .line 282506
    :try_start_0
    invoke-virtual {p0, p1}, LX/1ci;->e(LX/1ca;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282507
    if-eqz v0, :cond_0

    .line 282508
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 282509
    :cond_0
    return-void

    .line 282510
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 282511
    invoke-interface {p1}, LX/1ca;->g()Z

    :cond_1
    throw v1
.end method

.method public final b(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282512
    :try_start_0
    invoke-virtual {p0, p1}, LX/1ci;->f(LX/1ca;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282513
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 282514
    return-void

    .line 282515
    :catchall_0
    move-exception v0

    invoke-interface {p1}, LX/1ca;->g()Z

    throw v0
.end method

.method public c(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282516
    return-void
.end method

.method public d(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282517
    return-void
.end method

.method public abstract e(LX/1ca;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract f(LX/1ca;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation
.end method
