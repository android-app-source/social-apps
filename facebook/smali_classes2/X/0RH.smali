.class public LX/0RH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Q4;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0RN;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4fR;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fl;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0RC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Q4;)V
    .locals 0

    .prologue
    .line 59820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59821
    iput-object p1, p0, LX/0RH;->a:LX/0Q4;

    .line 59822
    return-void
.end method

.method public static f(LX/0RH;LX/0RI;)LX/0RN;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0RN",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59812
    iget-object v0, p0, LX/0RH;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 59813
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0RH;->b:Ljava/util/List;

    .line 59814
    :cond_0
    new-instance v0, LX/0RN;

    invoke-direct {v0}, LX/0RN;-><init>()V

    .line 59815
    iget-object v1, p0, LX/0RH;->a:LX/0Q4;

    .line 59816
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0RN;->a:Ljava/lang/String;

    .line 59817
    iput-object p1, v0, LX/0RN;->b:LX/0RI;

    .line 59818
    iget-object v1, p0, LX/0RH;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59819
    return-object v0
.end method

.method public static h(LX/0RH;LX/0RI;)LX/4fl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/4fl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59805
    iget-object v0, p0, LX/0RH;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 59806
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0RH;->d:Ljava/util/Map;

    .line 59807
    :cond_0
    iget-object v0, p0, LX/0RH;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fl;

    .line 59808
    if-nez v0, :cond_1

    .line 59809
    new-instance v0, LX/4fl;

    invoke-direct {v0, p1}, LX/4fl;-><init>(LX/0RI;)V

    .line 59810
    iget-object v1, p0, LX/0RH;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59811
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/0RO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/facebook/inject/binder/AnnotatedBindingBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59803
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-static {p0, v0}, LX/0RH;->f(LX/0RH;LX/0RI;)LX/0RN;

    move-result-object v0

    .line 59804
    new-instance v1, LX/0RO;

    invoke-direct {v1, v0}, LX/0RO;-><init>(LX/0RN;)V

    return-object v1
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0RN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59800
    iget-object v0, p0, LX/0RH;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->b:Ljava/util/List;

    :goto_0
    return-object v0

    .line 59801
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 59802
    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)LX/0RO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/facebook/inject/binder/AnnotatedBindingBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59797
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-static {p0, v0}, LX/0RH;->f(LX/0RH;LX/0RI;)LX/0RN;

    move-result-object v0

    .line 59798
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0RN;->a(Z)V

    .line 59799
    new-instance v1, LX/0RO;

    invoke-direct {v1, v0}, LX/0RO;-><init>(LX/0RN;)V

    return-object v1
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/4fR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59794
    iget-object v0, p0, LX/0RH;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->c:Ljava/util/List;

    :goto_0
    return-object v0

    .line 59795
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 59796
    goto :goto_0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59791
    iget-object v0, p0, LX/0RH;->f:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->f:Ljava/util/Set;

    :goto_0
    return-object v0

    .line 59792
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 59793
    goto :goto_0
.end method

.method public final c(LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 59764
    iget-object v0, p0, LX/0RH;->f:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 59765
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0RH;->f:Ljava/util/Set;

    .line 59766
    :cond_0
    iget-object v0, p0, LX/0RH;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59767
    return-void
.end method

.method public final d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59788
    iget-object v0, p0, LX/0RH;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->d:Ljava/util/Map;

    :goto_0
    return-object v0

    .line 59789
    :cond_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 59790
    goto :goto_0
.end method

.method public final e()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59785
    iget-object v0, p0, LX/0RH;->e:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->e:Ljava/util/Set;

    :goto_0
    return-object v0

    .line 59786
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 59787
    goto :goto_0
.end method

.method public final e(LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 59781
    iget-object v0, p0, LX/0RH;->e:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 59782
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0RH;->e:Ljava/util/Set;

    .line 59783
    :cond_0
    iget-object v0, p0, LX/0RH;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59784
    return-void
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 59778
    iget-object v0, p0, LX/0RH;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RH;->g:Ljava/util/List;

    :goto_0
    return-object v0

    .line 59779
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 59780
    goto :goto_0
.end method

.method public final h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0RC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59775
    iget-object v0, p0, LX/0RH;->i:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 59776
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 59777
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0RH;->i:Ljava/util/Map;

    goto :goto_0
.end method

.method public final h(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59768
    iget-object v0, p0, LX/0RH;->g:Ljava/util/List;

    if-nez v0, :cond_0

    .line 59769
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0RH;->g:Ljava/util/List;

    .line 59770
    :cond_0
    iget-object v0, p0, LX/0RH;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59771
    iget-object v0, p0, LX/0RH;->h:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 59772
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0RH;->h:Ljava/util/Set;

    .line 59773
    :cond_1
    iget-object v0, p0, LX/0RH;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59774
    return-void
.end method
