.class public LX/0kx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0kx;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/148;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/14B;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/14B;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 128032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128033
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0kx;->a:Ljava/lang/Object;

    .line 128034
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    .line 128035
    iput-object p1, p0, LX/0kx;->c:LX/0Ot;

    .line 128036
    return-void
.end method

.method public static a(LX/0QB;)LX/0kx;
    .locals 5

    .prologue
    .line 128016
    sget-object v0, LX/0kx;->d:LX/0kx;

    if-nez v0, :cond_1

    .line 128017
    const-class v1, LX/0kx;

    monitor-enter v1

    .line 128018
    :try_start_0
    sget-object v0, LX/0kx;->d:LX/0kx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 128019
    if-eqz v2, :cond_0

    .line 128020
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 128021
    new-instance v3, LX/0kx;

    .line 128022
    new-instance v4, LX/0ky;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/0ky;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 128023
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 128024
    invoke-direct {v3, v4}, LX/0kx;-><init>(LX/0Ot;)V

    .line 128025
    move-object v0, v3

    .line 128026
    sput-object v0, LX/0kx;->d:LX/0kx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128027
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 128028
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 128029
    :cond_1
    sget-object v0, LX/0kx;->d:LX/0kx;

    return-object v0

    .line 128030
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 128031
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)LX/149;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/149;"
        }
    .end annotation

    .prologue
    .line 128010
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 128011
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 128012
    iget-object v2, p0, LX/0kx;->b:Ljava/util/Stack;

    new-instance v3, LX/148;

    const-string v0, "dest_module_class"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, p2, v0, p3}, LX/148;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128013
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128014
    new-instance v0, LX/149;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, LX/149;-><init>(LX/0kx;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v0

    .line 128015
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)LX/149;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/149;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128001
    iget-object v3, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 128002
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 128003
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/148;

    .line 128004
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v2, v2

    .line 128005
    :goto_0
    iget-object v4, p0, LX/0kx;->b:Ljava/util/Stack;

    new-instance v5, LX/148;

    if-eqz p2, :cond_0

    const-string v0, "dest_module_class"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-direct {v5, p1, v0, p2}, LX/148;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128006
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128007
    new-instance v0, LX/149;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v4}, LX/149;-><init>(LX/0kx;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v0

    :cond_0
    move-object v0, v1

    .line 128008
    goto :goto_1

    .line 128009
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127965
    invoke-virtual {p0}, LX/0kx;->c()LX/148;

    move-result-object v0

    .line 127966
    if-nez v0, :cond_1

    .line 127967
    :cond_0
    :goto_0
    return-object p1

    .line 127968
    :cond_1
    iget-object p0, v0, LX/148;->a:Ljava/lang/String;

    move-object v0, p0

    .line 127969
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 127970
    goto :goto_0
.end method

.method public final a()Ljava/util/Stack;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack",
            "<",
            "LX/148;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127998
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127999
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    monitor-exit v1

    return-object v0

    .line 128000
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Stack;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "LX/148;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127995
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127996
    :try_start_0
    iput-object p1, p0, LX/0kx;->b:Ljava/util/Stack;

    .line 127997
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)LX/149;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/149;"
        }
    .end annotation

    .prologue
    .line 127994
    new-instance v0, LX/149;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, LX/149;-><init>(LX/0kx;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/util/Map;)LX/149;
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/149;"
        }
    .end annotation

    .prologue
    .line 127980
    const/4 v3, 0x0

    .line 127981
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127982
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/148;

    .line 127983
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v0, v2

    .line 127984
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/148;

    .line 127985
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v0, v2

    .line 127986
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127987
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 127988
    :cond_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127989
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/148;

    .line 127990
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v3, v2

    .line 127991
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127992
    new-instance v0, LX/149;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v4}, LX/149;-><init>(LX/0kx;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v0

    .line 127993
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127979
    const-string v0, "unknown"

    invoke-virtual {p0, v0}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/148;
    .locals 2

    .prologue
    .line 127974
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127975
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127976
    const/4 v0, 0x0

    monitor-exit v1

    .line 127977
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/148;

    monitor-exit v1

    goto :goto_0

    .line 127978
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 127971
    iget-object v1, p0, LX/0kx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127972
    :try_start_0
    iget-object v0, p0, LX/0kx;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 127973
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
