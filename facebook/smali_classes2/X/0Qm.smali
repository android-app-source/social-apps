.class public final enum LX/0Qm;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0Qn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Qm;",
        ">;",
        "LX/0Qn",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Qm;

.field public static final enum INSTANCE:LX/0Qm;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58488
    new-instance v0, LX/0Qm;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/0Qm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qm;->INSTANCE:LX/0Qm;

    .line 58489
    const/4 v0, 0x1

    new-array v0, v0, [LX/0Qm;

    sget-object v1, LX/0Qm;->INSTANCE:LX/0Qm;

    aput-object v1, v0, v2

    sput-object v0, LX/0Qm;->$VALUES:[LX/0Qm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58484
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Qm;
    .locals 1

    .prologue
    .line 58487
    const-class v0, LX/0Qm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Qm;

    return-object v0
.end method

.method public static values()[LX/0Qm;
    .locals 1

    .prologue
    .line 58486
    sget-object v0, LX/0Qm;->$VALUES:[LX/0Qm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Qm;

    return-object v0
.end method


# virtual methods
.method public final onRemoval(LX/4wi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4wi",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58485
    return-void
.end method
