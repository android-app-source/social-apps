.class public LX/1mh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 314065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314066
    return-void
.end method

.method public static a(FLcom/facebook/csslayout/YogaMeasureMode;)I
    .locals 3

    .prologue
    .line 314067
    sget-object v0, LX/1nn;->a:[I

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaMeasureMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 314068
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected YogaMeasureMode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314069
    :pswitch_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 314070
    :goto_0
    invoke-static {p0}, LX/1n0;->a(F)I

    move-result v1

    invoke-static {v1, v0}, LX/1mh;->a(II)I

    move-result v0

    return v0

    .line 314071
    :pswitch_1
    const/4 v0, 0x0

    .line 314072
    goto :goto_0

    .line 314073
    :pswitch_2
    const/high16 v0, -0x80000000

    .line 314074
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 314064
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    return v0
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 314075
    invoke-static {p0, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 314063
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    return v0
.end method

.method public static b(II)I
    .locals 3

    .prologue
    .line 314057
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 314058
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected size mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314059
    :sswitch_0
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result p1

    .line 314060
    :goto_0
    :sswitch_1
    return p1

    .line 314061
    :sswitch_2
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 314062
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
