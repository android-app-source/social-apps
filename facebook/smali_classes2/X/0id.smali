.class public LX/0id;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:[I

.field private static volatile r:LX/0id;


# instance fields
.field public b:LX/03R;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4h1;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final g:LX/0So;

.field private final h:LX/16w;

.field public i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:I

.field private n:Z

.field private final o:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/17B;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mEventList"
    .end annotation
.end field

.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 121496
    const-string v0, "com.facebook.katana.activity.ImmersiveActivity"

    const-string v1, "com.facebook.composer.activity.ComposerActivity"

    const-string v2, "com.facebook.photos.simplepicker.launcher.SimplePickerLauncherActivity"

    const-string v3, "com.facebook.places.checkin.activity.SelectAtTagActivity"

    const-string v4, "com.facebook.katana.FacebookLoginActivity"

    sget-object v5, LX/0cQ;->NATIVE_TIMELINE_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/0cQ;->NATIVE_PERMALINK_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/0cQ;->EVENTS_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v7}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/0cQ;->EVENTS_DASHBOARD_FRAGMENT:LX/0cQ;

    invoke-virtual {v8}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {v9}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v10}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v11, LX/0cQ;->NATIVE_PAGES_FRAGMENT:LX/0cQ;

    invoke-virtual {v11}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v11

    new-array v12, v13, [Ljava/lang/String;

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0id;->d:LX/0Px;

    .line 121497
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100b8

    aput v1, v0, v13

    sput-object v0, LX/0id;->e:[I

    .line 121498
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "show_perf_toast"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0id;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/16w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0So;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/4h1;",
            ">;",
            "LX/16w;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 121341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121342
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/0id;->o:LX/0tf;

    .line 121343
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0id;->p:Ljava/util/LinkedList;

    .line 121344
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0id;->b:LX/03R;

    .line 121345
    iput-object p1, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 121346
    iput-object p2, p0, LX/0id;->g:LX/0So;

    .line 121347
    iput-object p3, p0, LX/0id;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 121348
    iput-object p4, p0, LX/0id;->c:LX/0Ot;

    .line 121349
    iput-object p5, p0, LX/0id;->h:LX/16w;

    .line 121350
    return-void
.end method

.method public static a(LX/0QB;)LX/0id;
    .locals 9

    .prologue
    .line 121351
    sget-object v0, LX/0id;->r:LX/0id;

    if-nez v0, :cond_1

    .line 121352
    const-class v1, LX/0id;

    monitor-enter v1

    .line 121353
    :try_start_0
    sget-object v0, LX/0id;->r:LX/0id;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121354
    if-eqz v2, :cond_0

    .line 121355
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121356
    new-instance v3, LX/0id;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x2ded

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/16w;->a(LX/0QB;)LX/16w;

    move-result-object v8

    check-cast v8, LX/16w;

    invoke-direct/range {v3 .. v8}, LX/0id;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/16w;)V

    .line 121357
    move-object v0, v3

    .line 121358
    sput-object v0, LX/0id;->r:LX/0id;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121359
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121360
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121361
    :cond_1
    sget-object v0, LX/0id;->r:LX/0id;

    return-object v0

    .line 121362
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(IZ)S
    .locals 1

    .prologue
    .line 121364
    packed-switch p0, :pswitch_data_0

    .line 121365
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 121366
    :pswitch_1
    if-eqz p1, :cond_0

    const/16 v0, 0x8e

    goto :goto_0

    :cond_0
    const/16 v0, 0x56

    goto :goto_0

    .line 121367
    :pswitch_2
    if-eqz p1, :cond_1

    const/16 v0, 0x50

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    goto :goto_0

    .line 121368
    :pswitch_3
    if-eqz p1, :cond_2

    const/16 v0, 0x90

    goto :goto_0

    :cond_2
    const/16 v0, 0x4c

    goto :goto_0

    .line 121369
    :pswitch_4
    if-eqz p1, :cond_3

    const/16 v0, 0x8f

    goto :goto_0

    :cond_3
    const/16 v0, 0x4d

    goto :goto_0

    .line 121370
    :pswitch_5
    if-eqz p1, :cond_4

    const/16 v0, 0x91

    goto :goto_0

    :cond_4
    const/16 v0, 0x92

    goto :goto_0

    .line 121371
    :pswitch_6
    if-eqz p1, :cond_5

    const/16 v0, 0x51

    goto :goto_0

    :cond_5
    const/16 v0, 0x49

    goto :goto_0

    .line 121372
    :pswitch_7
    if-eqz p1, :cond_6

    const/16 v0, 0x53

    goto :goto_0

    :cond_6
    const/16 v0, 0x55

    goto :goto_0

    .line 121373
    :pswitch_8
    if-eqz p1, :cond_7

    const/16 v0, 0x52

    goto :goto_0

    :cond_7
    const/16 v0, 0x4f

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4c0003
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private a(IJ)V
    .locals 14

    .prologue
    .line 121374
    iget-object v11, p0, LX/0id;->p:Ljava/util/LinkedList;

    monitor-enter v11

    .line 121375
    :try_start_0
    iget-object v2, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/17B;

    move-object v10, v0

    .line 121376
    iget v2, v10, LX/17B;->c:I

    iget-boolean v3, v10, LX/17B;->a:Z

    invoke-static {v2, v3}, LX/0id;->a(IZ)S

    move-result v5

    .line 121377
    if-lez v5, :cond_0

    .line 121378
    iget-object v3, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v6, "tag_name"

    iget-object v7, v10, LX/17B;->e:Ljava/lang/String;

    iget-wide v8, v10, LX/17B;->b:J

    move v4, p1

    invoke-interface/range {v3 .. v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;J)V

    .line 121379
    :cond_0
    iget-boolean v2, v10, LX/17B;->a:Z

    if-eqz v2, :cond_5

    .line 121380
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v3, v10, LX/17B;->c:I

    const-string v4, "tag_name"

    iget-object v5, v10, LX/17B;->e:Ljava/lang/String;

    iget-wide v6, v10, LX/17B;->b:J

    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121381
    const/4 v3, 0x0

    .line 121382
    iget v2, v10, LX/17B;->c:I

    const v4, 0x4c0009

    if-ne v2, v4, :cond_3

    .line 121383
    const v3, 0x4c001c

    .line 121384
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 121385
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "tag_name"

    iget-object v5, v10, LX/17B;->e:Ljava/lang/String;

    move-wide/from16 v6, p2

    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121386
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 121387
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v4, 0x2

    iget-wide v6, v10, LX/17B;->b:J

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISJ)V

    .line 121388
    :cond_2
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v3, v10, LX/17B;->c:I

    invoke-static {p1}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 121389
    :catchall_0
    move-exception v2

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 121390
    :cond_3
    :try_start_1
    iget v2, v10, LX/17B;->c:I

    const v4, 0x4c0006

    if-ne v2, v4, :cond_4

    .line 121391
    const v3, 0x4c001d

    goto :goto_1

    .line 121392
    :cond_4
    iget v2, v10, LX/17B;->c:I

    const v4, 0x4c000c

    if-ne v2, v4, :cond_1

    .line 121393
    const v3, 0x4c001e

    goto :goto_1

    .line 121394
    :cond_5
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v3, v10, LX/17B;->c:I

    const/4 v4, 0x2

    iget-wide v6, v10, LX/17B;->b:J

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISJ)V

    goto/16 :goto_0

    .line 121395
    :cond_6
    iget-object v2, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 121396
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private a(ILjava/lang/String;J)V
    .locals 9

    .prologue
    .line 121397
    new-instance v1, LX/17B;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move v3, p1

    move-object v4, p2

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, LX/17B;-><init>(ZILjava/lang/String;Ljava/lang/String;J)V

    invoke-direct {p0, v1}, LX/0id;->a(LX/17B;)V

    .line 121398
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 121399
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121400
    const v2, 0x4c0003

    if-eq p1, v2, :cond_0

    const v2, 0x4c000a

    if-eq p1, v2, :cond_0

    const v2, 0x4c0004

    if-eq p1, v2, :cond_0

    const v2, 0x4c0005

    if-ne p1, v2, :cond_3

    :cond_0
    move v0, v1

    .line 121401
    :cond_1
    :goto_0
    move v0, v0

    .line 121402
    if-eqz v0, :cond_2

    .line 121403
    invoke-virtual {p0}, LX/0id;->b()V

    .line 121404
    :goto_1
    return-void

    .line 121405
    :cond_2
    new-instance v1, LX/17B;

    const/4 v2, 0x1

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, LX/17B;-><init>(ZILjava/lang/String;Ljava/lang/String;J)V

    invoke-direct {p0, v1}, LX/0id;->a(LX/17B;)V

    goto :goto_1

    .line 121406
    :cond_3
    const v2, 0x4c000d

    if-ne p1, v2, :cond_4

    const v2, 0x4c0007

    invoke-static {p0, v2}, LX/0id;->c(LX/0id;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121407
    :cond_4
    invoke-static {p0, p1}, LX/0id;->c(LX/0id;I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 121408
    goto :goto_0
.end method

.method public static a(LX/0id;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 121409
    iget-object v0, p0, LX/0id;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, LX/0id;->a(ILjava/lang/String;J)V

    .line 121410
    return-void
.end method

.method public static a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 121411
    iget-object v0, p0, LX/0id;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/0id;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121412
    return-void
.end method

.method private a(LX/17B;)V
    .locals 2

    .prologue
    .line 121413
    iget-object v1, p0, LX/0id;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 121414
    :try_start_0
    iget-object v0, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 121415
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(LX/0id;I)Z
    .locals 3

    .prologue
    .line 121416
    iget-object v1, p0, LX/0id;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 121417
    :try_start_0
    iget-object v0, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17B;

    .line 121418
    iget v0, v0, LX/17B;->c:I

    if-ne p1, v0, :cond_0

    .line 121419
    const/4 v0, 0x1

    monitor-exit v1

    .line 121420
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 121421
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d(I)V
    .locals 6

    .prologue
    .line 121422
    const/4 v0, 0x0

    .line 121423
    iget-object v1, p0, LX/0id;->b:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_0

    .line 121424
    iget-object v1, p0, LX/0id;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0id;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, p0, LX/0id;->b:LX/03R;

    .line 121425
    :cond_0
    iget-object v1, p0, LX/0id;->b:LX/03R;

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v0, v0

    .line 121426
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121427
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v2

    .line 121428
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 121429
    iget-object v0, p0, LX/0id;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4h1;

    invoke-static {p1}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, LX/0id;->g:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 121430
    iget-object v4, v0, LX/4h1;->a:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p0, ": "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p0, " ms"

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 p0, 0x0

    invoke-static {v4, v5, p0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 121431
    :cond_2
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 121432
    iget-object v1, p0, LX/0id;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 121433
    :try_start_0
    iget-object v0, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 121434
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static t(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 121435
    if-eqz p0, :cond_2

    .line 121436
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 121437
    const/16 v0, 0x2f

    add-int/lit8 v2, v1, 0x3

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 121438
    if-ne v1, v3, :cond_0

    .line 121439
    :goto_0
    return-object p0

    .line 121440
    :cond_0
    add-int/lit8 v1, v1, 0x3

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :cond_1
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 121441
    :cond_2
    const-string p0, "unknown"

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 121442
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0id;->l:J

    .line 121443
    invoke-direct {p0}, LX/0id;->e()V

    .line 121444
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x4c0001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 121445
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 121446
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0id;->n:Z

    .line 121447
    iput p1, p0, LX/0id;->m:I

    .line 121448
    return-void
.end method

.method public final a(ILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 121449
    iget-wide v0, p0, LX/0id;->l:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_8

    .line 121450
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-wide v4, p0, LX/0id;->l:J

    invoke-interface {v0, p1, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 121451
    if-eqz p2, :cond_0

    .line 121452
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121453
    iget-object v3, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 121454
    :cond_0
    iget-object v3, p0, LX/0id;->p:Ljava/util/LinkedList;

    monitor-enter v3

    .line 121455
    :try_start_0
    iget-object v0, p0, LX/0id;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17B;

    .line 121456
    iget v1, v0, LX/17B;->c:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    .line 121457
    :goto_2
    if-lez v1, :cond_1

    .line 121458
    iget-object v5, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-wide v6, v0, LX/17B;->b:J

    invoke-interface {v5, p1, v1, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISJ)V

    goto :goto_1

    .line 121459
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 121460
    :pswitch_1
    :try_start_1
    iget-boolean v1, v0, LX/17B;->a:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x51

    goto :goto_2

    :cond_2
    const/16 v1, 0x49

    goto :goto_2

    .line 121461
    :pswitch_2
    iget-boolean v1, v0, LX/17B;->a:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x91

    goto :goto_2

    :cond_3
    const/16 v1, 0x92

    goto :goto_2

    .line 121462
    :pswitch_3
    iget-boolean v1, v0, LX/17B;->a:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x8e

    goto :goto_2

    :cond_4
    const/16 v1, 0x56

    goto :goto_2

    .line 121463
    :pswitch_4
    iget-boolean v1, v0, LX/17B;->a:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x50

    goto :goto_2

    :cond_5
    const/4 v1, 0x7

    goto :goto_2

    .line 121464
    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121465
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x67

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 121466
    :cond_7
    return-void

    .line 121467
    :cond_8
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 121468
    if-eqz p2, :cond_7

    .line 121469
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121470
    iget-object v2, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x4c0003
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const v1, 0x4c000a

    const/4 v2, 0x0

    .line 121471
    iget-object v0, p0, LX/0id;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 121472
    iget-boolean v0, p0, LX/0id;->n:Z

    if-nez v0, :cond_0

    .line 121473
    const/4 v6, 0x0

    .line 121474
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    sget-object v3, LX/0id;->e:[I

    invoke-virtual {p1, v0, v3}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 121475
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 121476
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 121477
    move v0, v3

    .line 121478
    iput v0, p0, LX/0id;->m:I

    .line 121479
    :cond_0
    iget v0, p0, LX/0id;->m:I

    if-lez v0, :cond_2

    .line 121480
    iget-object v0, p0, LX/0id;->o:LX/0tf;

    iget v3, p0, LX/0id;->m:I

    int-to-long v6, v3

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v6, v7, v3}, LX/0tf;->a(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 121481
    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    .line 121482
    :try_start_0
    iget v0, p0, LX/0id;->m:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v6

    .line 121483
    iget-object v0, p0, LX/0id;->o:LX/0tf;

    iget v3, p0, LX/0id;->m:I

    int-to-long v8, v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v8, v9, v3}, LX/0tf;->b(JLjava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121484
    :cond_1
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    move-object v0, p0

    move-object v3, v2

    .line 121485
    invoke-direct/range {v0 .. v5}, LX/0id;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121486
    add-long/2addr v4, v6

    invoke-direct {p0, v1, v2, v4, v5}, LX/0id;->a(ILjava/lang/String;J)V

    .line 121487
    :cond_2
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 121488
    if-nez p1, :cond_1

    .line 121489
    const-string v0, "null"

    .line 121490
    :cond_0
    :goto_0
    move-object v0, v0

    .line 121491
    iget-object v1, p0, LX/0id;->g:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, LX/0id;->a(Ljava/lang/String;J)V

    .line 121492
    return-void

    .line 121493
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121494
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 121495
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121329
    iget-object v0, p0, LX/0id;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, LX/0id;->a(Ljava/lang/String;J)V

    .line 121330
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 121331
    iput-wide p2, p0, LX/0id;->l:J

    .line 121332
    iput-object p1, p0, LX/0id;->j:Ljava/lang/String;

    .line 121333
    iput v0, p0, LX/0id;->m:I

    .line 121334
    iput-boolean v0, p0, LX/0id;->n:Z

    .line 121335
    iput-object v1, p0, LX/0id;->i:Ljava/lang/String;

    .line 121336
    iput-object v1, p0, LX/0id;->k:Ljava/lang/String;

    .line 121337
    invoke-direct {p0}, LX/0id;->e()V

    .line 121338
    iget-object v0, p0, LX/0id;->h:LX/16w;

    invoke-virtual {v0}, LX/16w;->a()V

    .line 121339
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x4c0001

    const-string v2, "tag_name"

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121340
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 121239
    const/4 v0, 0x0

    .line 121240
    if-eqz p2, :cond_0

    .line 121241
    const-string v1, "target_fragment"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 121242
    invoke-static {v1}, LX/0jj;->a(I)LX/0cQ;

    move-result-object v1

    .line 121243
    if-eqz v1, :cond_2

    .line 121244
    invoke-virtual {v1}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121245
    :cond_0
    :goto_0
    move-object v0, v0

    .line 121246
    sget-object v1, LX/0id;->d:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121247
    const v0, 0x4c0005

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121248
    :goto_1
    return-void

    .line 121249
    :cond_1
    invoke-virtual {p0}, LX/0id;->b()V

    goto :goto_1

    .line 121250
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121251
    iget-object v0, p0, LX/0id;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, LX/0id;->a(Ljava/lang/String;J)V

    .line 121252
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const v6, 0x4c0001

    .line 121253
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 121254
    const v0, 0x4c0001

    :goto_1
    move v2, v0

    .line 121255
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(II)V

    .line 121256
    invoke-direct {p0, v2}, LX/0id;->d(I)V

    .line 121257
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121258
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v0

    invoke-direct {p0, v2, v0, v1}, LX/0id;->a(IJ)V

    .line 121259
    :cond_1
    if-eqz p2, :cond_2

    .line 121260
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 121261
    iget-object v4, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 121262
    :cond_2
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "tag_name"

    iget-object v3, p0, LX/0id;->j:Ljava/lang/String;

    invoke-interface {v0, v2, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 121263
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 121264
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 121265
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 121266
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0id;->l:J

    .line 121267
    invoke-direct {p0}, LX/0id;->e()V

    .line 121268
    return-void

    .line 121269
    :sswitch_0
    const-string v1, "LoadTimelineHeader"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "LoadEventPermalink"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0

    :sswitch_2
    const-string v1, "LoadEventsDashboard"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_0

    :sswitch_3
    const-string v1, "LoadGroupsFeed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "LoadPageHeaderNonAdmin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto/16 :goto_0

    :sswitch_5
    const-string v1, "LoadPageHeaderAdmin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "OpenComposer"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "LoadPermalink"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "OpenMediaPicker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto/16 :goto_0

    :sswitch_9
    const-string v1, "OpenPhotoGallery"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "OpenPhotosFeed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "OpenCheckIn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "LoadWebView"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "SearchTypeahead"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    .line 121270
    :pswitch_0
    const v0, 0x4c000e

    goto/16 :goto_1

    .line 121271
    :pswitch_1
    const v0, 0x4c0010

    goto/16 :goto_1

    .line 121272
    :pswitch_2
    const v0, 0x4c001a

    goto/16 :goto_1

    .line 121273
    :pswitch_3
    const v0, 0x4c0011

    goto/16 :goto_1

    .line 121274
    :pswitch_4
    const v0, 0x4c000f

    goto/16 :goto_1

    .line 121275
    :pswitch_5
    const v0, 0x4c0012

    goto/16 :goto_1

    .line 121276
    :pswitch_6
    const v0, 0x4c0014

    goto/16 :goto_1

    .line 121277
    :pswitch_7
    const v0, 0x4c0013

    goto/16 :goto_1

    .line 121278
    :pswitch_8
    const v0, 0x4c0015

    goto/16 :goto_1

    .line 121279
    :pswitch_9
    const v0, 0x4c0016

    goto/16 :goto_1

    .line 121280
    :pswitch_a
    const v0, 0x4c001b

    goto/16 :goto_1

    .line 121281
    :pswitch_b
    const v0, 0x4c0017

    goto/16 :goto_1

    .line 121282
    :pswitch_c
    const v0, 0x4c0018

    goto/16 :goto_1

    .line 121283
    :pswitch_d
    const v0, 0x4c0019

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7c12e17d -> :sswitch_b
        -0x723a03cd -> :sswitch_c
        -0x6471d77b -> :sswitch_7
        -0x426f8f2c -> :sswitch_0
        -0x19c28bb6 -> :sswitch_9
        0xc093817 -> :sswitch_1
        0x212ee795 -> :sswitch_2
        0x252d9244 -> :sswitch_4
        0x3d5530c8 -> :sswitch_8
        0x48e6b8cd -> :sswitch_5
        0x50952f38 -> :sswitch_3
        0x69b4b3c9 -> :sswitch_a
        0x7429e9df -> :sswitch_d
        0x7a62b5aa -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 121284
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0id;->l:J

    .line 121285
    invoke-direct {p0}, LX/0id;->e()V

    .line 121286
    iget-object v0, p0, LX/0id;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x4c0001

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 121287
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121288
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0id;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 121289
    return-void
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const v1, 0x4c0006

    .line 121290
    move-object v0, p0

    move-object v2, p1

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, LX/0id;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 121291
    invoke-static {p0, v1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121292
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121293
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0id;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 121294
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121295
    const v0, 0x4c0007

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121296
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121297
    const v0, 0x4c0007

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121298
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121299
    const v0, 0x4c0008

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121300
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121301
    const v0, 0x4c0008

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121302
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121326
    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121327
    :cond_0
    const v0, 0x4c0009

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121328
    :cond_1
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121303
    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121304
    :cond_0
    const v0, 0x4c0009

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121305
    :cond_1
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121306
    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121307
    :cond_0
    const v0, 0x4c000d

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121308
    :cond_1
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121309
    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0id;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121310
    :cond_0
    const v0, 0x4c000d

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121311
    :cond_1
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121312
    const v0, 0x4c0003

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121313
    return-void
.end method

.method public final o(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121314
    const v0, 0x4c000c

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121315
    return-void
.end method

.method public final p(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121316
    const v0, 0x4c000c

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121317
    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121318
    iput-object p1, p0, LX/0id;->k:Ljava/lang/String;

    .line 121319
    const v0, 0x4c0004

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121320
    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121321
    const v0, 0x4c0004

    invoke-static {p0, v0, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 121322
    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121323
    iput-object p1, p0, LX/0id;->k:Ljava/lang/String;

    .line 121324
    const v0, 0x4c0005

    invoke-static {p0, v0, p1, p1}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    .line 121325
    return-void
.end method
