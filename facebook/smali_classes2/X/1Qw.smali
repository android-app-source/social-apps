.class public LX/1Qw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Qx;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/1Qw;


# instance fields
.field public final b:I

.field private final c:Ljava/lang/String;

.field private final d:[Z

.field public final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final f:Ljava/util/Random;

.field private final g:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245302
    const-class v0, LX/1Qx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Qw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;Ljava/util/Random;LX/0ad;)V
    .locals 2
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245360
    iput-object p1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 245361
    iput-object p2, p0, LX/1Qw;->g:LX/03V;

    .line 245362
    iput-object p3, p0, LX/1Qw;->f:Ljava/util/Random;

    .line 245363
    const/16 v0, 0x8

    new-array v0, v0, [Z

    iput-object v0, p0, LX/1Qw;->d:[Z

    .line 245364
    sget v0, LX/1Qy;->a:I

    const/4 v1, 0x0

    invoke-interface {p4, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1Qw;->b:I

    .line 245365
    iget v0, p0, LX/1Qw;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1Qw;->c:Ljava/lang/String;

    .line 245366
    return-void
.end method

.method public static a(LX/0QB;)LX/1Qw;
    .locals 7

    .prologue
    .line 245346
    sget-object v0, LX/1Qw;->h:LX/1Qw;

    if-nez v0, :cond_1

    .line 245347
    const-class v1, LX/1Qw;

    monitor-enter v1

    .line 245348
    :try_start_0
    sget-object v0, LX/1Qw;->h:LX/1Qw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 245349
    if-eqz v2, :cond_0

    .line 245350
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 245351
    new-instance p0, LX/1Qw;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v5

    check-cast v5, Ljava/util/Random;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Qw;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;Ljava/util/Random;LX/0ad;)V

    .line 245352
    move-object v0, p0

    .line 245353
    sput-object v0, LX/1Qw;->h:LX/1Qw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245354
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 245355
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 245356
    :cond_1
    sget-object v0, LX/1Qw;->h:LX/1Qw;

    return-object v0

    .line 245357
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 245358
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILX/1Po;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 245343
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v0

    .line 245344
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/1Qw;->b(I)I

    move-result v2

    const-string v3, "feed_type"

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 245345
    return-void
.end method

.method private a(ILX/1Pv;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 245339
    invoke-interface {p2}, LX/1Pv;->iO_()Z

    move-result v0

    .line 245340
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/1Qw;->b(I)I

    move-result v2

    const-string v3, "is_async"

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 245341
    return-void

    .line 245342
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    const v0, 0xa008e

    .line 245367
    packed-switch p1, :pswitch_data_0

    .line 245368
    iget-object v1, p0, LX/1Qw;->g:LX/03V;

    sget-object v2, LX/1Qw;->a:Ljava/lang/String;

    const-string v3, "Invalid internal markerId"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245369
    :goto_0
    :pswitch_0
    return v0

    .line 245370
    :pswitch_1
    const v0, 0xa0090

    goto :goto_0

    .line 245371
    :pswitch_2
    const v0, 0xa0091

    goto :goto_0

    .line 245372
    :pswitch_3
    const v0, 0xa0092

    goto :goto_0

    .line 245373
    :pswitch_4
    const v0, 0xa0097

    goto :goto_0

    .line 245374
    :pswitch_5
    const v0, 0xa0098

    goto :goto_0

    .line 245375
    :pswitch_6
    const v0, 0xa0099

    goto :goto_0

    .line 245376
    :pswitch_7
    const v0, 0xa009a

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 245335
    iget-object v0, p0, LX/1Qw;->d:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    .line 245336
    :goto_0
    return-void

    .line 245337
    :cond_0
    iget-object v0, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/1Qw;->b(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 245338
    iget-object v0, p0, LX/1Qw;->d:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    goto :goto_0
.end method

.method public final a(ILX/1PW;)V
    .locals 1

    .prologue
    .line 245329
    iget-object v0, p0, LX/1Qw;->d:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_1

    .line 245330
    :cond_0
    :goto_0
    return-void

    .line 245331
    :cond_1
    instance-of v0, p2, LX/1Pv;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 245332
    check-cast v0, LX/1Pv;

    invoke-direct {p0, p1, v0}, LX/1Qw;->a(ILX/1Pv;)V

    .line 245333
    :cond_2
    instance-of v0, p2, LX/1Po;

    if-eqz v0, :cond_0

    .line 245334
    check-cast p2, LX/1Po;

    invoke-direct {p0, p1, p2}, LX/1Qw;->a(ILX/1Po;)V

    goto :goto_0
.end method

.method public final a(ILjava/util/concurrent/Callable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 245323
    iget-object v0, p0, LX/1Qw;->d:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    .line 245324
    :goto_0
    return-void

    .line 245325
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 245326
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/1Qw;->b(I)I

    move-result v2

    const-string v3, "pd_parents"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 245327
    :catch_0
    move-exception v0

    .line 245328
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 245303
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 245304
    iget v2, p0, LX/1Qw;->b:I

    if-nez v2, :cond_3

    .line 245305
    :cond_0
    :goto_0
    move v0, v0

    .line 245306
    if-nez v0, :cond_2

    .line 245307
    :cond_1
    :goto_1
    return-void

    .line 245308
    :cond_2
    iget-object v0, p0, LX/1Qw;->d:[Z

    aput-boolean v4, v0, p2

    .line 245309
    invoke-direct {p0, p2}, LX/1Qw;->b(I)I

    move-result v0

    .line 245310
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 245311
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 245312
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 245313
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 245314
    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 245315
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "internal_sample_rate"

    iget-object v3, p0, LX/1Qw;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 245316
    if-ne p2, v4, :cond_1

    .line 245317
    instance-of v0, p1, LX/1Rj;

    .line 245318
    iget-object v1, p0, LX/1Qw;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xa0090

    const-string v3, "is_component_pd"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 245319
    goto :goto_1

    .line 245320
    :cond_3
    iget v2, p0, LX/1Qw;->b:I

    if-ne v2, v1, :cond_4

    move v0, v1

    .line 245321
    goto :goto_0

    .line 245322
    :cond_4
    iget-object v2, p0, LX/1Qw;->f:Ljava/util/Random;

    iget v3, p0, LX/1Qw;->b:I

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
