.class public LX/0hB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0hB;


# instance fields
.field private final a:Landroid/view/Display;

.field private final b:Landroid/util/DisplayMetrics;

.field private final c:Landroid/util/DisplayMetrics;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 115180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115181
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    .line 115182
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, LX/0hB;->c:Landroid/util/DisplayMetrics;

    .line 115183
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/0hB;->a:Landroid/view/Display;

    .line 115184
    invoke-direct {p0}, LX/0hB;->h()V

    .line 115185
    return-void
.end method

.method public static a(LX/0QB;)LX/0hB;
    .locals 4

    .prologue
    .line 115191
    sget-object v0, LX/0hB;->e:LX/0hB;

    if-nez v0, :cond_1

    .line 115192
    const-class v1, LX/0hB;

    monitor-enter v1

    .line 115193
    :try_start_0
    sget-object v0, LX/0hB;->e:LX/0hB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 115194
    if-eqz v2, :cond_0

    .line 115195
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 115196
    new-instance p0, LX/0hB;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-direct {p0, v3}, LX/0hB;-><init>(Landroid/view/WindowManager;)V

    .line 115197
    move-object v0, p0

    .line 115198
    sput-object v0, LX/0hB;->e:LX/0hB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115199
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 115200
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 115201
    :cond_1
    sget-object v0, LX/0hB;->e:LX/0hB;

    return-object v0

    .line 115202
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 115203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 115186
    iput p1, p0, LX/0hB;->d:I

    .line 115187
    iget-object v0, p0, LX/0hB;->a:Landroid/view/Display;

    iget-object v1, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 115188
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 115189
    iget-object v0, p0, LX/0hB;->a:Landroid/view/Display;

    iget-object v1, p0, LX/0hB;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 115190
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 115174
    if-eqz p1, :cond_1

    .line 115175
    invoke-direct {p0}, LX/0hB;->h()V

    .line 115176
    :cond_0
    :goto_0
    return-void

    .line 115177
    :cond_1
    iget-object v0, p0, LX/0hB;->a:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 115178
    iget v1, p0, LX/0hB;->d:I

    if-eq v0, v1, :cond_0

    .line 115179
    invoke-direct {p0, v0}, LX/0hB;->a(I)V

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 115172
    iget-object v0, p0, LX/0hB;->a:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    invoke-direct {p0, v0}, LX/0hB;->a(I)V

    .line 115173
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 115204
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0hB;->a(Z)V

    .line 115205
    iget-object v0, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 115170
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0hB;->a(Z)V

    .line 115171
    iget-object v0, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 115168
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0hB;->a(Z)V

    .line 115169
    iget-object v0, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 115166
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0hB;->a(Z)V

    .line 115167
    iget-object v0, p0, LX/0hB;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 115162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0hB;->a(Z)V

    .line 115163
    iget-object v0, p0, LX/0hB;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 115165
    invoke-virtual {p0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 115164
    invoke-virtual {p0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
