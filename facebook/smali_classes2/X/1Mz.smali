.class public LX/1Mz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/1Mz;


# instance fields
.field public final a:LX/0t5;

.field public final b:LX/0tc;

.field private final c:LX/0lp;

.field private final d:LX/11F;

.field public final e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field public final f:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final g:LX/0ti;

.field public final h:LX/0Uh;

.field private final i:LX/1N0;

.field public final j:LX/0Zb;

.field private final k:LX/1BA;

.field public final l:LX/1N7;

.field public final m:LX/03V;


# direct methods
.method public constructor <init>(LX/0t5;LX/0tc;LX/0lp;LX/11F;LX/0TD;LX/0Tf;LX/0ti;LX/0Uh;LX/1N0;LX/0Zb;LX/1BA;LX/1N7;LX/03V;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236620
    iput-object p1, p0, LX/1Mz;->a:LX/0t5;

    .line 236621
    iput-object p2, p0, LX/1Mz;->b:LX/0tc;

    .line 236622
    iput-object p3, p0, LX/1Mz;->c:LX/0lp;

    .line 236623
    iput-object p4, p0, LX/1Mz;->d:LX/11F;

    .line 236624
    iput-object p5, p0, LX/1Mz;->e:LX/0TD;

    .line 236625
    iput-object p6, p0, LX/1Mz;->f:LX/0Tf;

    .line 236626
    iput-object p7, p0, LX/1Mz;->g:LX/0ti;

    .line 236627
    iput-object p8, p0, LX/1Mz;->h:LX/0Uh;

    .line 236628
    iput-object p9, p0, LX/1Mz;->i:LX/1N0;

    .line 236629
    iput-object p10, p0, LX/1Mz;->j:LX/0Zb;

    .line 236630
    iput-object p11, p0, LX/1Mz;->k:LX/1BA;

    .line 236631
    iput-object p12, p0, LX/1Mz;->l:LX/1N7;

    .line 236632
    iput-object p13, p0, LX/1Mz;->m:LX/03V;

    .line 236633
    return-void
.end method

.method public static a(LX/0QB;)LX/1Mz;
    .locals 3

    .prologue
    .line 236609
    sget-object v0, LX/1Mz;->n:LX/1Mz;

    if-nez v0, :cond_1

    .line 236610
    const-class v1, LX/1Mz;

    monitor-enter v1

    .line 236611
    :try_start_0
    sget-object v0, LX/1Mz;->n:LX/1Mz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 236612
    if-eqz v2, :cond_0

    .line 236613
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Mz;->b(LX/0QB;)LX/1Mz;

    move-result-object v0

    sput-object v0, LX/1Mz;->n:LX/1Mz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236614
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 236615
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 236616
    :cond_1
    sget-object v0, LX/1Mz;->n:LX/1Mz;

    return-object v0

    .line 236617
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 236618
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Mz;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 236606
    iget-object v0, p0, LX/1Mz;->c:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 236607
    :try_start_0
    invoke-virtual {v0, p2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 236608
    invoke-virtual {v0}, LX/15w;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/15w;->close()V

    throw v1
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 236597
    :try_start_0
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 236598
    iget-object v1, p0, LX/1Mz;->c:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 236599
    :try_start_1
    invoke-virtual {v1, p1}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 236600
    invoke-virtual {v1}, LX/0nX;->flush()V

    .line 236601
    invoke-virtual {v1}, LX/0nX;->close()V

    .line 236602
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 236603
    :try_start_2
    invoke-virtual {v1}, LX/0nX;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nX;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 236604
    :catch_0
    move-exception v0

    .line 236605
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static a$redex0(LX/1Mz;Ljava/lang/String;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 236594
    iget-object v0, p0, LX/1Mz;->c:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 236595
    :try_start_0
    iget-object v0, p0, LX/1Mz;->d:LX/11F;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p2, v2, v3}, LX/11F;->a(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 236596
    invoke-virtual {v1}, LX/15w;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/15w;->close()V

    throw v0
.end method

.method private static b(LX/0QB;)LX/1Mz;
    .locals 14

    .prologue
    .line 236572
    new-instance v0, LX/1Mz;

    invoke-static {p0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v1

    check-cast v1, LX/0t5;

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v2

    check-cast v2, LX/0tc;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v3

    check-cast v3, LX/0lp;

    invoke-static {p0}, LX/11F;->b(LX/0QB;)LX/11F;

    move-result-object v4

    check-cast v4, LX/11F;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, LX/0Tf;

    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v7

    check-cast v7, LX/0ti;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {p0}, LX/1N0;->a(LX/0QB;)LX/1N0;

    move-result-object v9

    check-cast v9, LX/1N0;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v11

    check-cast v11, LX/1BA;

    const-class v12, LX/1N7;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1N7;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-direct/range {v0 .. v13}, LX/1Mz;-><init>(LX/0t5;LX/0tc;LX/0lp;LX/11F;LX/0TD;LX/0Tf;LX/0ti;LX/0Uh;LX/1N0;LX/0Zb;LX/1BA;LX/1N7;LX/03V;)V

    .line 236573
    return-object v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 236591
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 236592
    :catch_0
    move-exception v0

    .line 236593
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 236589
    iget-object v0, p0, LX/1Mz;->k:LX/1BA;

    invoke-virtual {v0, p1}, LX/1BA;->a(I)V

    .line 236590
    return-void
.end method

.method public final a(LX/0zO;Lcom/facebook/graphql/executor/live/QueryMetadata;ILX/0TF;LX/4VW;)V
    .locals 6
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/graphql/executor/live/QueryMetadata;",
            "I",
            "LX/0TF",
            "<",
            "LX/4Ve",
            "<TT;>;>;",
            "Lcom/facebook/reactivesocket/SubscriptionCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236574
    iget-object v0, p1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 236575
    if-nez v0, :cond_0

    .line 236576
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no viewer context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236577
    :cond_0
    iget-object v1, p1, LX/0zO;->m:LX/0gW;

    move-object v1, v1

    .line 236578
    iget-object v2, v1, LX/0gW;->h:Ljava/lang/String;

    move-object v1, v2

    .line 236579
    iget-object v2, p1, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 236580
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 236581
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v3

    .line 236582
    invoke-virtual {v2}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v2}, LX/1Mz;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1Mz;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 236583
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "doc_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&variables="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&access_token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236584
    invoke-direct {p0, p2}, LX/1Mz;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 236585
    iget-object v0, p0, LX/1Mz;->i:LX/1N0;

    .line 236586
    new-instance v3, LX/4Vc;

    invoke-direct {v3, p0, p1, p4}, LX/4Vc;-><init>(LX/1Mz;LX/0zO;LX/0TF;)V

    move-object v4, v3

    .line 236587
    move v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1N0;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;LX/4VW;)V

    .line 236588
    return-void
.end method
