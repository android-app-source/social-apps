.class public final LX/1US;
.super LX/1UT;
.source ""

# interfaces
.implements LX/0Vf;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1UT;",
        "LX/0Vf;",
        "LX/1OO",
        "<",
        "LX/1a2;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/1Cw;

.field private d:Z


# direct methods
.method public constructor <init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 256037
    invoke-direct {p0, p1, p2}, LX/1UT;-><init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V

    .line 256038
    iput-object p1, p0, LX/1US;->c:LX/1Cw;

    .line 256039
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 256029
    iget-boolean v0, p0, LX/1US;->d:Z

    if-eqz v0, :cond_0

    .line 256030
    :goto_0
    return-void

    .line 256031
    :cond_0
    iget-object v0, p0, LX/1US;->c:LX/1Cw;

    instance-of v0, v0, LX/0Vf;

    if-eqz v0, :cond_1

    .line 256032
    iget-object v0, p0, LX/1US;->c:LX/1Cw;

    check-cast v0, LX/0Vf;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 256033
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1US;->d:Z

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256036
    iget-object v0, p0, LX/1US;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 256035
    iget-object v0, p0, LX/1US;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 256034
    iget-boolean v0, p0, LX/1US;->d:Z

    return v0
.end method
