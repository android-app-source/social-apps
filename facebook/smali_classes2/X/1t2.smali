.class public LX/1t2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05G;


# instance fields
.field private final a:LX/00H;

.field private final b:LX/0dC;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/00H;LX/0dC;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/config/server/AppNameInUserAgent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 335746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335747
    iput-object p1, p0, LX/1t2;->a:LX/00H;

    .line 335748
    iput-object p2, p0, LX/1t2;->b:LX/0dC;

    .line 335749
    iput-object p3, p0, LX/1t2;->c:Ljava/lang/String;

    .line 335750
    return-void
.end method

.method public static a(LX/0QB;)LX/1t2;
    .locals 1

    .prologue
    .line 335745
    invoke-static {p0}, LX/1t2;->b(LX/0QB;)LX/1t2;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/1t2;
    .locals 4

    .prologue
    .line 335743
    new-instance v3, LX/1t2;

    const-class v0, LX/00H;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-static {p0}, LX/0s3;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/1t2;-><init>(LX/00H;LX/0dC;Ljava/lang/String;)V

    .line 335744
    return-object v3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335742
    iget-object v0, p0, LX/1t2;->a:LX/00H;

    invoke-virtual {v0}, LX/00H;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/06y;)Z
    .locals 1

    .prologue
    .line 335751
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335741
    iget-object v0, p0, LX/1t2;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335740
    iget-object v0, p0, LX/1t2;->b:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335739
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 335738
    return-void
.end method
