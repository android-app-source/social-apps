.class public LX/0ja;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/3AL;

.field public static final b:LX/3AL;

.field public static final c:LX/3AM;

.field public static final d:LX/3AM;

.field public static final e:LX/3AM;

.field private static m:LX/0Xm;


# instance fields
.field private final f:LX/0Sg;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/7HJ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Xq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xq",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1TG;",
            ">;>;"
        }
    .end annotation
.end field

.field public final k:Z

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 123969
    new-instance v0, LX/3AL;

    invoke-direct {v0, v3, v1}, LX/3AL;-><init>(II)V

    sput-object v0, LX/0ja;->a:LX/3AL;

    .line 123970
    new-instance v0, LX/3AL;

    invoke-direct {v0, v1, v1}, LX/3AL;-><init>(II)V

    sput-object v0, LX/0ja;->b:LX/3AL;

    .line 123971
    new-instance v0, LX/3AM;

    invoke-direct {v0, v2, v2}, LX/3AM;-><init>(II)V

    .line 123972
    sput-object v0, LX/0ja;->c:LX/3AM;

    sput-object v0, LX/0ja;->d:LX/3AM;

    .line 123973
    new-instance v0, LX/3AM;

    invoke-direct {v0, v3, v1}, LX/3AM;-><init>(II)V

    sput-object v0, LX/0ja;->e:LX/3AM;

    return-void
.end method

.method public constructor <init>(LX/0Sg;LX/0V6;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0V6;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1TG;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 123961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123962
    iput-object p1, p0, LX/0ja;->f:LX/0Sg;

    .line 123963
    iput-object p3, p0, LX/0ja;->j:LX/0Ot;

    .line 123964
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/0ja;->h:LX/0Xq;

    .line 123965
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0ja;->i:Ljava/util/List;

    .line 123966
    invoke-virtual {p2}, LX/0V6;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/0ja;->k:Z

    .line 123967
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0ja;->g:Ljava/util/Map;

    .line 123968
    return-void
.end method

.method public static a(LX/0QB;)LX/0ja;
    .locals 7

    .prologue
    .line 123947
    const-class v1, LX/0ja;

    monitor-enter v1

    .line 123948
    :try_start_0
    sget-object v0, LX/0ja;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 123949
    sput-object v2, LX/0ja;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 123950
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123951
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 123952
    new-instance v5, LX/0ja;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v3

    check-cast v3, LX/0Sg;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v4

    check-cast v4, LX/0V6;

    .line 123953
    new-instance v6, LX/3AN;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v6, p0}, LX/3AN;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 123954
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v6, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v6

    move-object v6, v6

    .line 123955
    invoke-direct {v5, v3, v4, v6}, LX/0ja;-><init>(LX/0Sg;LX/0V6;LX/0Ot;)V

    .line 123956
    move-object v0, v5

    .line 123957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 123958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0ja;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 123960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 123946
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isn\'t configured for recycling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<*>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 123937
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 123938
    iget-object v0, p0, LX/0ja;->h:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xq;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123939
    iget-object v0, p0, LX/0ja;->h:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 123940
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 123941
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 123942
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123943
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 123944
    goto :goto_0

    .line 123945
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 123907
    iget-object v0, p0, LX/0ja;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 123908
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 123909
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 123910
    :cond_1
    iget-object v0, p0, LX/0ja;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 123911
    iget-object v0, p0, LX/0ja;->h:LX/0Xq;

    invoke-virtual {v0}, LX/0Xq;->g()V

    .line 123912
    return-void
.end method

.method public final a(Ljava/lang/Class;LX/3AL;LX/3AM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/3AL;",
            "LX/3AM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123935
    iget-object v0, p0, LX/0ja;->g:Ljava/util/Map;

    new-instance v1, LX/7HJ;

    invoke-direct {v1, p0, p2, p3}, LX/7HJ;-><init>(LX/0ja;LX/3AL;LX/3AM;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123936
    return-void
.end method

.method public final a(Ljava/lang/Class;Landroid/view/View;LX/0h1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/view/View;",
            "LX/0h1;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123913
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 123914
    if-eqz p2, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 123915
    if-eqz p3, :cond_3

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 123916
    invoke-interface {p3, p2}, LX/0h1;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 123917
    const/4 v1, 0x0

    .line 123918
    iget-boolean v0, p0, LX/0ja;->l:Z

    if-eqz v0, :cond_7

    .line 123919
    :goto_3
    iget-object v0, p0, LX/0ja;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 123920
    invoke-static {p1}, LX/0ja;->c(Ljava/lang/Class;)V

    move v0, v1

    .line 123921
    :goto_4
    move v0, v0

    .line 123922
    if-eqz v0, :cond_4

    .line 123923
    iget-object v0, p0, LX/0ja;->h:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123924
    const/4 v0, 0x1

    .line 123925
    :goto_5
    move v0, v0

    .line 123926
    if-nez v0, :cond_0

    .line 123927
    invoke-interface {p3, p2, v2}, LX/0h1;->removeRecyclableViewFromParent(Landroid/view/View;Z)V

    .line 123928
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 123929
    goto :goto_0

    :cond_2
    move v0, v2

    .line 123930
    goto :goto_1

    :cond_3
    move v1, v2

    .line 123931
    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    :cond_5
    iget-object v0, p0, LX/0ja;->h:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    iget-object v0, p0, LX/0ja;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HJ;

    iget v0, v0, LX/7HJ;->b:I

    if-ge v3, v0, :cond_6

    const/4 v0, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_4

    .line 123932
    :cond_7
    iget-object v0, p0, LX/0ja;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1TG;

    .line 123933
    invoke-interface {v0, p0}, LX/1TG;->a(LX/0ja;)V

    goto :goto_6

    .line 123934
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ja;->l:Z

    goto :goto_3
.end method
