.class public LX/0RB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0RC;
.implements LX/0RD;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/0QA;

.field private final c:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Landroid/content/Context;",
            "LX/0S6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 59726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59727
    new-instance v0, LX/0QE;

    new-instance v1, LX/0RE;

    invoke-direct {v1, p0}, LX/0RE;-><init>(LX/0RB;)V

    invoke-direct {v0, v1}, LX/0QE;-><init>(LX/0QM;)V

    iput-object v0, p0, LX/0RB;->c:LX/0QJ;

    .line 59728
    iput-object p1, p0, LX/0RB;->a:Landroid/content/Context;

    .line 59729
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/0Xn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 59711
    const-class v0, LX/0Xn;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xn;

    .line 59712
    if-eqz v0, :cond_0

    .line 59713
    :goto_0
    return-object v0

    .line 59714
    :cond_0
    const-class v0, Landroid/app/Application;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 59715
    if-nez v0, :cond_1

    .line 59716
    new-instance v0, LX/4fr;

    const-string v1, "Context chain contains neither an Application nor a context which implements PropertyBag"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Or;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Or",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59725
    new-instance v0, LX/4fS;

    invoke-direct {v0, p0, p1}, LX/4fS;-><init>(LX/0RB;LX/0Or;)V

    return-object v0
.end method

.method public final a(LX/0QA;)V
    .locals 0

    .prologue
    .line 59723
    iput-object p1, p0, LX/0RB;->b:LX/0QA;

    .line 59724
    return-void
.end method

.method public final a(Landroid/content/Context;LX/0S7;)V
    .locals 1

    .prologue
    .line 59720
    iget-object v0, p0, LX/0RB;->c:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S6;

    .line 59721
    invoke-virtual {p2, v0}, LX/0S7;->a(LX/0R6;)V

    .line 59722
    return-void
.end method

.method public annotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59719
    const-class v0, Lcom/facebook/inject/ContextScoped;

    return-object v0
.end method

.method public getInjectorThreadStack()LX/0S7;
    .locals 1

    .prologue
    .line 59718
    iget-object v0, p0, LX/0RB;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    return-object v0
.end method
