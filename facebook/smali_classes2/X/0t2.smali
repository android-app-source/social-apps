.class public LX/0t2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0lC;


# direct methods
.method public constructor <init>(LX/0Or;LX/0lC;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153425
    iput-object p1, p0, LX/0t2;->a:LX/0Or;

    .line 153426
    iput-object p2, p0, LX/0t2;->b:LX/0lC;

    .line 153427
    return-void
.end method

.method public static a(LX/0QB;)LX/0t2;
    .locals 1

    .prologue
    .line 153348
    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0t2;LX/0w7;Ljava/util/Collection;LX/1l1;)LX/1l6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w7;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1l1;",
            ")",
            "LX/1l6;"
        }
    .end annotation

    .prologue
    .line 153391
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v0

    .line 153392
    invoke-virtual {p1}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v2, v0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 153393
    if-eqz p2, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, LX/1l1;->EXCLUSIVE_PARAMS:LX/1l1;

    if-eq p3, v3, :cond_1

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, LX/1l1;->INCLUSIVE_PARAMS:LX/1l1;

    if-ne p3, v3, :cond_4

    .line 153394
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 153395
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 153396
    instance-of v3, v0, Ljava/util/List;

    if-eqz v3, :cond_5

    .line 153397
    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 153398
    :goto_1
    move-object v2, v2

    .line 153399
    move-object v0, v2

    :goto_2
    move-object v2, v0

    .line 153400
    goto :goto_0

    .line 153401
    :cond_2
    invoke-virtual {p1}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 153402
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 153403
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4a1;

    iget-object v1, v1, LX/4a1;->a:LX/0zO;

    invoke-virtual {v1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 153404
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4a1;

    iget-object v1, v1, LX/4a1;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 153405
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4a1;

    iget-object v0, v0, LX/4a1;->c:LX/4Zz;

    invoke-virtual {v0}, LX/4Zz;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_3

    .line 153406
    :cond_3
    return-object v2

    :cond_4
    move-object v0, v2

    goto :goto_2

    .line 153407
    :cond_5
    instance-of v3, v0, Ljava/lang/reflect/Array;

    if-eqz v3, :cond_6

    .line 153408
    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->deepHashCode([Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/1l6;->a(I)LX/1l6;

    goto :goto_1

    .line 153409
    :cond_6
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 153410
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_1

    .line 153411
    :cond_7
    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_8

    .line 153412
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_1

    .line 153413
    :cond_8
    instance-of v3, v0, Lcom/facebook/graphql/query/JsonPathValue;

    if-eqz v3, :cond_9

    .line 153414
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_1

    .line 153415
    :cond_9
    instance-of v3, v0, Ljava/lang/Boolean;

    if-eqz v3, :cond_a

    .line 153416
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto/16 :goto_1

    .line 153417
    :cond_a
    instance-of v3, v0, Ljava/lang/Enum;

    if-eqz v3, :cond_b

    .line 153418
    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto/16 :goto_1

    .line 153419
    :cond_b
    instance-of v3, v0, LX/0gS;

    if-eqz v3, :cond_c

    .line 153420
    iget-object v3, p0, LX/0t2;->b:LX/0lC;

    invoke-virtual {v3, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto/16 :goto_1

    .line 153421
    :cond_c
    instance-of v3, v0, LX/0gT;

    if-eqz v3, :cond_d

    .line 153422
    iget-object v3, p0, LX/0t2;->b:LX/0lC;

    invoke-virtual {v3, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto/16 :goto_1

    .line 153423
    :cond_d
    invoke-virtual {v2, v0}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto/16 :goto_1
.end method

.method public static b(LX/0QB;)LX/0t2;
    .locals 3

    .prologue
    .line 153389
    new-instance v1, LX/0t2;

    const/16 v0, 0x1617

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-direct {v1, v2, v0}, LX/0t2;-><init>(LX/0Or;LX/0lC;)V

    .line 153390
    return-object v1
.end method


# virtual methods
.method public final a(LX/0w7;Ljava/util/Collection;)LX/1l6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w7;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1l6;"
        }
    .end annotation

    .prologue
    .line 153388
    sget-object v0, LX/1l1;->EXCLUSIVE_PARAMS:LX/1l1;

    invoke-static {p0, p1, p2, v0}, LX/0t2;->a(LX/0t2;LX/0w7;Ljava/util/Collection;LX/1l1;)LX/1l6;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0gW;LX/0w5;LX/0w7;Ljava/util/Collection;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW;",
            "LX/0w5",
            "<*>;",
            "LX/0w7;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 153381
    if-eqz p3, :cond_0

    .line 153382
    :try_start_0
    invoke-virtual {p0, p3, p4}, LX/0t2;->a(LX/0w7;Ljava/util/Collection;)LX/1l6;

    move-result-object v0

    invoke-virtual {v0}, LX/1l6;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 153383
    invoke-virtual {p0, p1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153384
    :goto_0
    return-object v0

    .line 153385
    :catch_0
    move-exception v0

    .line 153386
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 153387
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW;",
            "LX/0w5",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x3a

    .line 153357
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x6e

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 153358
    iget-object v0, p1, LX/0gW;->f:Ljava/lang/String;

    move-object v0, v0

    .line 153359
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153360
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153361
    if-eqz p2, :cond_1

    .line 153362
    invoke-virtual {p2}, LX/0w5;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153363
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153364
    invoke-virtual {p2}, LX/0w5;->a()Ljava/lang/Integer;

    move-result-object v0

    .line 153365
    if-eqz v0, :cond_0

    .line 153366
    const-string v2, "%x"

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153367
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153368
    :cond_0
    const-class v0, LX/0jS;

    invoke-virtual {p2, v0}, LX/0w5;->a(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 153369
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, LX/0gW;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153370
    invoke-virtual {p1}, LX/0gW;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153371
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153372
    :cond_1
    iget-object v0, p1, LX/0gW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 153373
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153374
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153375
    iget-object v0, p0, LX/0t2;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153376
    if-eqz p3, :cond_2

    .line 153377
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153378
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153379
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 153380
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0gW;LX/0w5;LX/0w7;Ljava/util/Collection;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW;",
            "LX/0w5",
            "<*>;",
            "LX/0w7;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 153349
    if-eqz p3, :cond_0

    .line 153350
    :try_start_0
    sget-object v0, LX/1l1;->INCLUSIVE_PARAMS:LX/1l1;

    invoke-static {p0, p3, p4, v0}, LX/0t2;->a(LX/0t2;LX/0w7;Ljava/util/Collection;LX/1l1;)LX/1l6;

    move-result-object v0

    move-object v0, v0

    .line 153351
    invoke-virtual {v0}, LX/1l6;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 153352
    invoke-virtual {p0, p1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153353
    :goto_0
    return-object v0

    .line 153354
    :catch_0
    move-exception v0

    .line 153355
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 153356
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
