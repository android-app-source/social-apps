.class public final LX/0PN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# static fields
.field public static final a:LX/0PO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56181
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    sput-object v0, LX/0PN;->a:LX/0PO;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Ljava/lang/StringBuilder;
    .locals 6

    .prologue
    .line 56178
    const-string v0, "size"

    invoke-static {p0, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 56179
    new-instance v0, Ljava/lang/StringBuilder;

    int-to-long v2, p0

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x40000000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/Collection;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 56177
    check-cast p0, Ljava/util/Collection;

    return-object p0
.end method

.method public static a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TF;>;",
            "LX/0QK",
            "<-TF;TT;>;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 56166
    new-instance v0, LX/26o;

    invoke-direct {v0, p0, p1}, LX/26o;-><init>(Ljava/util/Collection;LX/0QK;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TE;>;",
            "LX/0Rl",
            "<-TE;>;)",
            "Ljava/util/Collection",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 56173
    instance-of v0, p0, LX/303;

    if-eqz v0, :cond_0

    .line 56174
    check-cast p0, LX/303;

    .line 56175
    new-instance v0, LX/303;

    iget-object v1, p0, LX/303;->a:Ljava/util/Collection;

    iget-object v2, p0, LX/303;->b:LX/0Rl;

    invoke-static {v2, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/303;-><init>(Ljava/util/Collection;LX/0Rl;)V

    move-object v0, v0

    .line 56176
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, LX/303;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rl;

    invoke-direct {v2, v0, v1}, LX/303;-><init>(Ljava/util/Collection;LX/0Rl;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 56168
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56169
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 56170
    :goto_0
    return v0

    .line 56171
    :catch_0
    goto :goto_0

    .line 56172
    :catch_1
    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 56167
    invoke-static {p0}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Ph;->e(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method
