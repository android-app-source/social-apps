.class public LX/1Hc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# static fields
.field private static final a:LX/1Hd;


# instance fields
.field private final b:LX/1Hy;

.field private final c:[B

.field private final d:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227267
    new-instance v0, LX/1Hd;

    invoke-direct {v0}, LX/1Hd;-><init>()V

    sput-object v0, LX/1Hc;->a:LX/1Hd;

    return-void
.end method

.method public constructor <init>(LX/1Hy;)V
    .locals 2

    .prologue
    .line 227260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227261
    iput-object p1, p0, LX/1Hc;->b:LX/1Hy;

    .line 227262
    iget-object v0, p0, LX/1Hc;->b:LX/1Hy;

    iget v0, v0, LX/1Hy;->keyLength:I

    new-array v0, v0, [B

    iput-object v0, p0, LX/1Hc;->c:[B

    .line 227263
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, LX/1Hc;->d:[B

    .line 227264
    iget-object v0, p0, LX/1Hc;->c:[B

    const-string v1, "WRITE2shaver@fb.c"

    invoke-static {v0, v1}, LX/1Hc;->a([BLjava/lang/String;)V

    .line 227265
    iget-object v0, p0, LX/1Hc;->d:[B

    const-string v1, "MAC_KEY_4_TEST_EMAIL_SHAVER@FB.COM "

    invoke-static {v0, v1}, LX/1Hc;->a([BLjava/lang/String;)V

    .line 227266
    return-void
.end method

.method private static a([BLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 227255
    invoke-static {p1}, LX/1Hc;->a(Ljava/lang/String;)[B

    move-result-object v2

    move v0, v1

    .line 227256
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_0

    .line 227257
    array-length v3, v2

    array-length v4, p0

    sub-int/2addr v4, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v1, p0, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227258
    array-length v3, v2

    add-int/2addr v0, v3

    goto :goto_0

    .line 227259
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 227253
    :try_start_0
    const-string v0, "ASCII"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 227254
    :goto_0
    return-object v0

    :catch_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 227252
    iget-object v0, p0, LX/1Hc;->c:[B

    return-object v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 227249
    iget-object v0, p0, LX/1Hc;->b:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 227250
    sget-object v1, LX/1Hc;->a:LX/1Hd;

    invoke-virtual {v1, v0}, LX/1Hd;->nextBytes([B)V

    .line 227251
    return-object v0
.end method
