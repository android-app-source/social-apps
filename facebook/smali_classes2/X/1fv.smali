.class public LX/1fv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/1fv;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 292504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292505
    iput-object p1, p0, LX/1fv;->a:LX/0ad;

    .line 292506
    return-void
.end method

.method public static a(LX/0QB;)LX/1fv;
    .locals 4

    .prologue
    .line 292507
    sget-object v0, LX/1fv;->h:LX/1fv;

    if-nez v0, :cond_1

    .line 292508
    const-class v1, LX/1fv;

    monitor-enter v1

    .line 292509
    :try_start_0
    sget-object v0, LX/1fv;->h:LX/1fv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 292510
    if-eqz v2, :cond_0

    .line 292511
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 292512
    new-instance p0, LX/1fv;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/1fv;-><init>(LX/0ad;)V

    .line 292513
    move-object v0, p0

    .line 292514
    sput-object v0, LX/1fv;->h:LX/1fv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292515
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 292516
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292517
    :cond_1
    sget-object v0, LX/1fv;->h:LX/1fv;

    return-object v0

    .line 292518
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 292519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final f()Z
    .locals 3

    .prologue
    .line 292520
    iget-object v0, p0, LX/1fv;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 292521
    iget-object v0, p0, LX/1fv;->a:LX/0ad;

    sget-short v1, LX/1fw;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1fv;->g:Ljava/lang/Boolean;

    .line 292522
    :cond_0
    iget-object v0, p0, LX/1fv;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
