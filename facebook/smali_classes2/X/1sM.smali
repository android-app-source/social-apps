.class public LX/1sM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1sG;

.field public final b:LX/0dC;

.field public final c:LX/0yD;

.field public final d:LX/0SG;


# direct methods
.method public constructor <init>(LX/1sG;LX/0dC;LX/0yD;LX/0SG;)V
    .locals 0
    .param p1    # LX/1sG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334580
    iput-object p1, p0, LX/1sM;->a:LX/1sG;

    .line 334581
    iput-object p2, p0, LX/1sM;->b:LX/0dC;

    .line 334582
    iput-object p3, p0, LX/1sM;->c:LX/0yD;

    .line 334583
    iput-object p4, p0, LX/1sM;->d:LX/0SG;

    .line 334584
    return-void
.end method

.method public static a(I)D
    .locals 4

    .prologue
    .line 334585
    int-to-double v0, p0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v2

    .line 334586
    const-wide v2, 0x40ac200000000000L    # 3600.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/telephony/CellInfoCdma;Lcom/facebook/location/CdmaCellInfo;)LX/2vZ;
    .locals 7
    .param p1    # Lcom/facebook/location/CdmaCellInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const v6, 0x7fffffff

    .line 334587
    new-instance v0, LX/2vZ;

    invoke-direct {v0}, LX/2vZ;-><init>()V

    .line 334588
    invoke-virtual {p0}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v1

    .line 334589
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v2

    if-eq v2, v6, :cond_0

    .line 334590
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->a(Ljava/lang/Integer;)LX/2vZ;

    .line 334591
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v2

    if-eq v2, v6, :cond_5

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v2

    if-eq v2, v6, :cond_5

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v2

    if-eqz v2, :cond_5

    .line 334592
    :cond_1
    new-instance v2, LX/2vb;

    invoke-direct {v2}, LX/2vb;-><init>()V

    .line 334593
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v3

    invoke-static {v3}, LX/1sM;->a(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    .line 334594
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v3

    invoke-static {v3}, LX/1sM;->a(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    .line 334595
    invoke-virtual {v0, v2}, LX/2vZ;->a(LX/2vb;)LX/2vZ;

    .line 334596
    :cond_2
    :goto_0
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v2

    if-eq v2, v6, :cond_3

    .line 334597
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->b(Ljava/lang/Integer;)LX/2vZ;

    .line 334598
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v2

    if-eq v2, v6, :cond_4

    .line 334599
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2vZ;->c(Ljava/lang/Integer;)LX/2vZ;

    .line 334600
    :cond_4
    invoke-virtual {p0}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v1

    .line 334601
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaEcio()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->e(Ljava/lang/Integer;)LX/2vZ;

    .line 334602
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->d(Ljava/lang/Integer;)LX/2vZ;

    .line 334603
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoEcio()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->g(Ljava/lang/Integer;)LX/2vZ;

    .line 334604
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoDbm()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2vZ;->f(Ljava/lang/Integer;)LX/2vZ;

    .line 334605
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoSnr()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2vZ;->h(Ljava/lang/Integer;)LX/2vZ;

    .line 334606
    return-object v0

    .line 334607
    :cond_5
    if-eqz p1, :cond_2

    iget-object v2, p1, Lcom/facebook/location/CdmaCellInfo;->d:Ljava/lang/Double;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/facebook/location/CdmaCellInfo;->e:Ljava/lang/Double;

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/facebook/location/CdmaCellInfo;->c:I

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p1, Lcom/facebook/location/CdmaCellInfo;->b:I

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p1, Lcom/facebook/location/CdmaCellInfo;->a:I

    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 334608
    new-instance v2, LX/2vb;

    invoke-direct {v2}, LX/2vb;-><init>()V

    .line 334609
    iget-object v3, p1, Lcom/facebook/location/CdmaCellInfo;->d:Ljava/lang/Double;

    invoke-virtual {v2, v3}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    .line 334610
    iget-object v3, p1, Lcom/facebook/location/CdmaCellInfo;->e:Ljava/lang/Double;

    invoke-virtual {v2, v3}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    .line 334611
    invoke-virtual {v0, v2}, LX/2vZ;->a(LX/2vb;)LX/2vZ;

    goto/16 :goto_0
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 15
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/2va;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 334612
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334613
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 334614
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_16

    .line 334615
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 334616
    if-eqz v0, :cond_4

    .line 334617
    new-instance v5, LX/2va;

    invoke-direct {v5}, LX/2va;-><init>()V

    .line 334618
    instance-of v6, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v6, :cond_5

    .line 334619
    check-cast v0, Landroid/telephony/CellInfoCdma;

    const v14, 0x7fffffff

    .line 334620
    new-instance v8, LX/2vZ;

    invoke-direct {v8}, LX/2vZ;-><init>()V

    .line 334621
    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v9

    .line 334622
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v10

    if-eq v10, v14, :cond_0

    .line 334623
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->a(Ljava/lang/Integer;)LX/2vZ;

    .line 334624
    :cond_0
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v10

    if-eq v10, v14, :cond_1

    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v10

    if-eq v10, v14, :cond_1

    .line 334625
    new-instance v10, LX/2vb;

    invoke-direct {v10}, LX/2vb;-><init>()V

    .line 334626
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v11

    invoke-static {v11}, LX/1sM;->a(I)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    .line 334627
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v11

    invoke-static {v11}, LX/1sM;->a(I)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    .line 334628
    invoke-virtual {v8, v10}, LX/2vZ;->a(LX/2vb;)LX/2vZ;

    .line 334629
    :cond_1
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v10

    if-eq v10, v14, :cond_2

    .line 334630
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->b(Ljava/lang/Integer;)LX/2vZ;

    .line 334631
    :cond_2
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v10

    if-eq v10, v14, :cond_3

    .line 334632
    invoke-virtual {v9}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/2vZ;->c(Ljava/lang/Integer;)LX/2vZ;

    .line 334633
    :cond_3
    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v9

    .line 334634
    invoke-virtual {v9}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaEcio()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->e(Ljava/lang/Integer;)LX/2vZ;

    .line 334635
    invoke-virtual {v9}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->d(Ljava/lang/Integer;)LX/2vZ;

    .line 334636
    invoke-virtual {v9}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoEcio()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->g(Ljava/lang/Integer;)LX/2vZ;

    .line 334637
    invoke-virtual {v9}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoDbm()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/2vZ;->f(Ljava/lang/Integer;)LX/2vZ;

    .line 334638
    invoke-virtual {v9}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoSnr()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/2vZ;->h(Ljava/lang/Integer;)LX/2vZ;

    .line 334639
    move-object v0, v8

    .line 334640
    const-string v6, "cdma_info"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334641
    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 334642
    const-string v6, "age_ms"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334643
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334644
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 334645
    :cond_5
    instance-of v6, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v6, :cond_a

    .line 334646
    check-cast v0, Landroid/telephony/CellInfoGsm;

    const v9, 0x7fffffff

    .line 334647
    new-instance v6, LX/2vW;

    invoke-direct {v6}, LX/2vW;-><init>()V

    .line 334648
    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v7

    .line 334649
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v8

    if-eq v8, v9, :cond_6

    .line 334650
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vW;->a(Ljava/lang/Integer;)LX/2vW;

    .line 334651
    :cond_6
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v8

    if-eq v8, v9, :cond_7

    .line 334652
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vW;->b(Ljava/lang/Integer;)LX/2vW;

    .line 334653
    :cond_7
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v8

    if-eq v8, v9, :cond_8

    .line 334654
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vW;->c(Ljava/lang/Integer;)LX/2vW;

    .line 334655
    :cond_8
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v8

    if-eq v8, v9, :cond_9

    .line 334656
    invoke-virtual {v7}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vW;->d(Ljava/lang/Integer;)LX/2vW;

    .line 334657
    :cond_9
    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v7

    .line 334658
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vW;->e(Ljava/lang/Integer;)LX/2vW;

    .line 334659
    move-object v0, v6

    .line 334660
    const-string v6, "gsm_info"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334661
    goto :goto_1

    .line 334662
    :cond_a
    instance-of v6, v0, Landroid/telephony/CellInfoLte;

    if-eqz v6, :cond_10

    .line 334663
    check-cast v0, Landroid/telephony/CellInfoLte;

    const v9, 0x7fffffff

    .line 334664
    new-instance v6, LX/2vX;

    invoke-direct {v6}, LX/2vX;-><init>()V

    .line 334665
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v7

    .line 334666
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v8

    if-eq v8, v9, :cond_b

    .line 334667
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vX;->a(Ljava/lang/Integer;)LX/2vX;

    .line 334668
    :cond_b
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v8

    if-eq v8, v9, :cond_c

    .line 334669
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vX;->b(Ljava/lang/Integer;)LX/2vX;

    .line 334670
    :cond_c
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v8

    if-eq v8, v9, :cond_d

    .line 334671
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vX;->c(Ljava/lang/Integer;)LX/2vX;

    .line 334672
    :cond_d
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v8

    if-eq v8, v9, :cond_e

    .line 334673
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vX;->d(Ljava/lang/Integer;)LX/2vX;

    .line 334674
    :cond_e
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v8

    if-eq v8, v9, :cond_f

    .line 334675
    invoke-virtual {v7}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vX;->e(Ljava/lang/Integer;)LX/2vX;

    .line 334676
    :cond_f
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v7

    .line 334677
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vX;->f(Ljava/lang/Integer;)LX/2vX;

    .line 334678
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthLte;->getTimingAdvance()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vX;->g(Ljava/lang/Integer;)LX/2vX;

    .line 334679
    move-object v0, v6

    .line 334680
    const-string v6, "lte_info"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334681
    goto/16 :goto_1

    .line 334682
    :cond_10
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x12

    if-lt v6, v7, :cond_4

    instance-of v6, v0, Landroid/telephony/CellInfoWcdma;

    if-eqz v6, :cond_4

    .line 334683
    check-cast v0, Landroid/telephony/CellInfoWcdma;

    const v9, 0x7fffffff

    .line 334684
    new-instance v6, LX/2vY;

    invoke-direct {v6}, LX/2vY;-><init>()V

    .line 334685
    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v7

    .line 334686
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v8

    if-eq v8, v9, :cond_11

    .line 334687
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vY;->a(Ljava/lang/Integer;)LX/2vY;

    .line 334688
    :cond_11
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v8

    if-eq v8, v9, :cond_12

    .line 334689
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vY;->b(Ljava/lang/Integer;)LX/2vY;

    .line 334690
    :cond_12
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v8

    if-eq v8, v9, :cond_13

    .line 334691
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vY;->c(Ljava/lang/Integer;)LX/2vY;

    .line 334692
    :cond_13
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v8

    if-eq v8, v9, :cond_14

    .line 334693
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2vY;->d(Ljava/lang/Integer;)LX/2vY;

    .line 334694
    :cond_14
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v8

    if-eq v8, v9, :cond_15

    .line 334695
    invoke-virtual {v7}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vY;->e(Ljava/lang/Integer;)LX/2vY;

    .line 334696
    :cond_15
    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v7

    .line 334697
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2vY;->f(Ljava/lang/Integer;)LX/2vY;

    .line 334698
    move-object v0, v6

    .line 334699
    const-string v6, "wcdma_info"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334700
    goto/16 :goto_1

    .line 334701
    :cond_16
    return-object v3
.end method
