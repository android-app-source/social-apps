.class public abstract LX/0To;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0To;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0To;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0To",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0To;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0To",
            "<TT;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63674
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 63675
    if-eqz p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 63676
    iput-object p1, p0, LX/0To;->a:LX/0To;

    .line 63677
    iput-object p2, p0, LX/0To;->b:Ljava/lang/String;

    .line 63678
    return-void

    :cond_0
    move v0, v2

    .line 63679
    goto :goto_0

    :cond_1
    move v1, v2

    .line 63680
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63682
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 63683
    const/4 v0, 0x0

    iput-object v0, p0, LX/0To;->a:LX/0To;

    .line 63684
    iput-object p1, p0, LX/0To;->b:Ljava/lang/String;

    .line 63685
    iput-object p1, p0, LX/0To;->c:Ljava/lang/String;

    .line 63686
    return-void

    .line 63687
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/0To;Ljava/lang/String;)LX/0To;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0To",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/String;)LX/0To;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 63672
    invoke-virtual {p0, p0, p1}, LX/0To;->a(LX/0To;Ljava/lang/String;)LX/0To;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63666
    iget-object v0, p0, LX/0To;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 63667
    iget-object v0, p0, LX/0To;->a:LX/0To;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0To;->a:LX/0To;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 63668
    iget-object v0, p0, LX/0To;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0To;->a:LX/0To;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0To;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/0To;->c:Ljava/lang/String;

    .line 63669
    :cond_0
    :goto_1
    iget-object v0, p0, LX/0To;->c:Ljava/lang/String;

    return-object v0

    .line 63670
    :cond_1
    iget-object v0, p0, LX/0To;->a:LX/0To;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63671
    :cond_2
    iget-object v0, p0, LX/0To;->b:Ljava/lang/String;

    iput-object v0, p0, LX/0To;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(LX/0To;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 63688
    iget-object v0, p0, LX/0To;->a:LX/0To;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0To;->a:LX/0To;

    invoke-virtual {v0, p1}, LX/0To;->a(LX/0To;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0To;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 63664
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 63665
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 63662
    check-cast p1, LX/0To;

    .line 63663
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 63642
    if-ne p0, p1, :cond_1

    .line 63643
    :cond_0
    :goto_0
    return v0

    .line 63644
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 63645
    :cond_3
    check-cast p1, LX/0To;

    .line 63646
    iget-object v2, p0, LX/0To;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, LX/0To;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p0, LX/0To;->a:LX/0To;

    if-nez v2, :cond_5

    iget-object v2, p1, LX/0To;->a:LX/0To;

    if-nez v2, :cond_4

    :goto_1
    iget-object v2, p0, LX/0To;->b:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, LX/0To;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 63647
    :cond_4
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 63648
    :cond_5
    iget-object v2, p0, LX/0To;->a:LX/0To;

    iget-object v3, p1, LX/0To;->a:LX/0To;

    invoke-virtual {v2, v3}, LX/0To;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_6
    iget-object v2, p0, LX/0To;->b:Ljava/lang/String;

    iget-object v3, p1, LX/0To;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 63649
    :cond_7
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 63651
    iget-object v0, p0, LX/0To;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63652
    iget-object v0, p0, LX/0To;->c:Ljava/lang/String;

    move-object v2, v0

    move v0, v1

    .line 63653
    :goto_0
    if-eqz v2, :cond_2

    .line 63654
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 63655
    :goto_1
    if-ge v1, v3, :cond_2

    .line 63656
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/2addr v0, v4

    .line 63657
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 63658
    :cond_0
    iget-object v0, p0, LX/0To;->a:LX/0To;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0To;->a:LX/0To;

    invoke-virtual {v0}, LX/0To;->hashCode()I

    move-result v0

    .line 63659
    :goto_2
    iget-object v2, p0, LX/0To;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 63660
    goto :goto_2

    .line 63661
    :cond_2
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63650
    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
