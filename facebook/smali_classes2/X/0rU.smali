.class public final enum LX/0rU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rU;

.field public static final enum CHUNKED_INITIAL:LX/0rU;

.field public static final enum CHUNKED_REMAINDER:LX/0rU;

.field public static final enum HEAD:LX/0rU;

.field public static final enum TAIL:LX/0rU;

.field public static final enum UNSET:LX/0rU;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149650
    new-instance v0, LX/0rU;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, v2}, LX/0rU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rU;->HEAD:LX/0rU;

    .line 149651
    new-instance v0, LX/0rU;

    const-string v1, "CHUNKED_INITIAL"

    invoke-direct {v0, v1, v3}, LX/0rU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rU;->CHUNKED_INITIAL:LX/0rU;

    .line 149652
    new-instance v0, LX/0rU;

    const-string v1, "CHUNKED_REMAINDER"

    invoke-direct {v0, v1, v4}, LX/0rU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rU;->CHUNKED_REMAINDER:LX/0rU;

    .line 149653
    new-instance v0, LX/0rU;

    const-string v1, "TAIL"

    invoke-direct {v0, v1, v5}, LX/0rU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rU;->TAIL:LX/0rU;

    .line 149654
    new-instance v0, LX/0rU;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v6}, LX/0rU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rU;->UNSET:LX/0rU;

    .line 149655
    const/4 v0, 0x5

    new-array v0, v0, [LX/0rU;

    sget-object v1, LX/0rU;->HEAD:LX/0rU;

    aput-object v1, v0, v2

    sget-object v1, LX/0rU;->CHUNKED_INITIAL:LX/0rU;

    aput-object v1, v0, v3

    sget-object v1, LX/0rU;->CHUNKED_REMAINDER:LX/0rU;

    aput-object v1, v0, v4

    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    aput-object v1, v0, v5

    sget-object v1, LX/0rU;->UNSET:LX/0rU;

    aput-object v1, v0, v6

    sput-object v0, LX/0rU;->$VALUES:[LX/0rU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149656
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rU;
    .locals 1

    .prologue
    .line 149657
    const-class v0, LX/0rU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rU;

    return-object v0
.end method

.method public static values()[LX/0rU;
    .locals 1

    .prologue
    .line 149658
    sget-object v0, LX/0rU;->$VALUES:[LX/0rU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rU;

    return-object v0
.end method
