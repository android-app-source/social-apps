.class public LX/1gZ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/1gi;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 294429
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 294430
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Looper;LX/0fz;LX/1ge;LX/1gf;LX/0g3;LX/1gg;LX/1gh;Lcom/facebook/api/feedtype/FeedType;)LX/1gi;
    .locals 17

    .prologue
    .line 294431
    new-instance v1, LX/1gi;

    const/16 v2, 0x1466

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v11

    check-cast v11, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v13

    check-cast v13, LX/0pV;

    invoke-static/range {p0 .. p0}, LX/0XE;->a(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v14

    check-cast v14, Lcom/facebook/user/model/User;

    invoke-static/range {p0 .. p0}, LX/0pW;->a(LX/0QB;)LX/0pW;

    move-result-object v15

    check-cast v15, LX/0pW;

    invoke-static/range {p0 .. p0}, LX/1gj;->a(LX/0QB;)LX/1gj;

    move-result-object v16

    check-cast v16, LX/1gj;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v16}, LX/1gi;-><init>(Landroid/os/Looper;LX/0fz;LX/1ge;LX/1gf;LX/0g3;LX/1gg;LX/1gh;Lcom/facebook/api/feedtype/FeedType;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/0ad;LX/0pV;Lcom/facebook/user/model/User;LX/0pW;LX/1gj;)V

    .line 294432
    const/16 v2, 0x786

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x66f

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x788

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x605

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, LX/1gi;->a(LX/1gi;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 294433
    return-object v1
.end method
