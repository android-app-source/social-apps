.class public LX/0kv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0kv;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/impression/NewImpressionId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127935
    iput-object p1, p0, LX/0kv;->a:LX/0Or;

    .line 127936
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0kv;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 127937
    return-void
.end method

.method public static a(LX/0QB;)LX/0kv;
    .locals 4

    .prologue
    .line 127938
    sget-object v0, LX/0kv;->c:LX/0kv;

    if-nez v0, :cond_1

    .line 127939
    const-class v1, LX/0kv;

    monitor-enter v1

    .line 127940
    :try_start_0
    sget-object v0, LX/0kv;->c:LX/0kv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127941
    if-eqz v2, :cond_0

    .line 127942
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 127943
    new-instance v3, LX/0kv;

    const/16 p0, 0x15e3

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0kv;-><init>(LX/0Or;)V

    .line 127944
    move-object v0, v3

    .line 127945
    sput-object v0, LX/0kv;->c:LX/0kv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127946
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127947
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127948
    :cond_1
    sget-object v0, LX/0kv;->c:LX/0kv;

    return-object v0

    .line 127949
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127950
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 127951
    iget-object v0, p0, LX/0kv;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127952
    iget-object v1, p0, LX/0kv;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127953
    return-void
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127954
    iget-object v0, p0, LX/0kv;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
