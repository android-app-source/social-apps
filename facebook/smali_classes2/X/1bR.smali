.class public final enum LX/1bR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1bR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1bR;

.field public static final enum DEFAULT:LX/1bR;

.field public static final enum NONE:LX/1bR;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 280545
    new-instance v0, LX/1bR;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/1bR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1bR;->NONE:LX/1bR;

    .line 280546
    new-instance v0, LX/1bR;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v4, v2}, LX/1bR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1bR;->DEFAULT:LX/1bR;

    .line 280547
    const/4 v0, 0x2

    new-array v0, v0, [LX/1bR;

    sget-object v1, LX/1bR;->NONE:LX/1bR;

    aput-object v1, v0, v3

    sget-object v1, LX/1bR;->DEFAULT:LX/1bR;

    aput-object v1, v0, v4

    sput-object v0, LX/1bR;->$VALUES:[LX/1bR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 280548
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 280549
    iput-object p3, p0, LX/1bR;->mValue:Ljava/lang/String;

    .line 280550
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1bR;
    .locals 1

    .prologue
    .line 280551
    const-class v0, LX/1bR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bR;

    return-object v0
.end method

.method public static values()[LX/1bR;
    .locals 1

    .prologue
    .line 280552
    sget-object v0, LX/1bR;->$VALUES:[LX/1bR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bR;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280553
    iget-object v0, p0, LX/1bR;->mValue:Ljava/lang/String;

    return-object v0
.end method
