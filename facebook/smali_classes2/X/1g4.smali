.class public LX/1g4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/1g4;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17Q;

.field private final c:LX/1g5;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/04B;

.field private final f:LX/17S;

.field public final g:LX/1g6;

.field private final h:LX/1g8;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;LX/1g5;LX/0Ot;LX/04B;LX/17S;LX/1g6;LX/1g8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/1g5;",
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;",
            "LX/04B;",
            "LX/17S;",
            "LX/1g6;",
            "LX/1g8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 292721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292722
    iput-object p1, p0, LX/1g4;->a:LX/0Zb;

    .line 292723
    iput-object p2, p0, LX/1g4;->b:LX/17Q;

    .line 292724
    iput-object p3, p0, LX/1g4;->c:LX/1g5;

    .line 292725
    iput-object p4, p0, LX/1g4;->d:LX/0Ot;

    .line 292726
    iput-object p5, p0, LX/1g4;->e:LX/04B;

    .line 292727
    iput-object p6, p0, LX/1g4;->f:LX/17S;

    .line 292728
    iput-object p7, p0, LX/1g4;->g:LX/1g6;

    .line 292729
    iput-object p8, p0, LX/1g4;->h:LX/1g8;

    .line 292730
    return-void
.end method

.method public static a(LX/0QB;)LX/1g4;
    .locals 12

    .prologue
    .line 292731
    sget-object v0, LX/1g4;->i:LX/1g4;

    if-nez v0, :cond_1

    .line 292732
    const-class v1, LX/1g4;

    monitor-enter v1

    .line 292733
    :try_start_0
    sget-object v0, LX/1g4;->i:LX/1g4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 292734
    if-eqz v2, :cond_0

    .line 292735
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 292736
    new-instance v3, LX/1g4;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, LX/1g5;->a(LX/0QB;)LX/1g5;

    move-result-object v6

    check-cast v6, LX/1g5;

    const/16 v7, 0x1335

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/04B;->a(LX/0QB;)LX/04B;

    move-result-object v8

    check-cast v8, LX/04B;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v9

    check-cast v9, LX/17S;

    invoke-static {v0}, LX/1g6;->a(LX/0QB;)LX/1g6;

    move-result-object v10

    check-cast v10, LX/1g6;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v11

    check-cast v11, LX/1g8;

    invoke-direct/range {v3 .. v11}, LX/1g4;-><init>(LX/0Zb;LX/17Q;LX/1g5;LX/0Ot;LX/04B;LX/17S;LX/1g6;LX/1g8;)V

    .line 292737
    move-object v0, v3

    .line 292738
    sput-object v0, LX/1g4;->i:LX/1g4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292739
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 292740
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292741
    :cond_1
    sget-object v0, LX/1g4;->i:LX/1g4;

    return-object v0

    .line 292742
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 292743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V
    .locals 14

    .prologue
    .line 292744
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292745
    :cond_0
    return-void

    .line 292746
    :cond_1
    iget-object v0, p0, LX/1g4;->h:LX/1g8;

    .line 292747
    iget-object v1, v0, LX/1g8;->d:LX/0if;

    sget-object v2, LX/0ig;->aK:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 292748
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->n()LX/0Px;

    move-result-object v5

    .line 292749
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 292750
    const-string v1, ""

    .line 292751
    invoke-static {v0, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 292752
    if-eqz v2, :cond_2

    .line 292753
    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    .line 292754
    :cond_2
    iget-object v7, p0, LX/1g4;->h:LX/1g8;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v0

    .line 292755
    const-string v13, "gpymi_impression"

    move-object v8, v7

    move-object v9, v2

    move-object v10, v3

    move-object v11, v0

    move-object v12, v1

    invoke-static/range {v8 .. v13}, LX/1g8;->b(LX/1g8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292756
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 292757
    :cond_3
    const-string v2, ""

    goto :goto_1

    :cond_4
    const-string v3, ""

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;I)V
    .locals 10

    .prologue
    .line 292758
    instance-of v0, p1, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 292759
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 292760
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292761
    :cond_0
    :goto_0
    return-void

    .line 292762
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 292763
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 292764
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/18M;->c(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 292765
    :cond_2
    :goto_1
    iget-object v0, p0, LX/1g4;->f:LX/17S;

    .line 292766
    sget v1, LX/1TU;->x:I

    invoke-static {v0, v1}, LX/17S;->a(LX/17S;I)Z

    move-result v1

    move v0, v1

    .line 292767
    if-eqz v0, :cond_0

    .line 292768
    invoke-interface {p1}, LX/0jT;->f()I

    move-result v0

    const v1, 0x4c808d5

    if-ne v0, v1, :cond_0

    move-object v0, p1

    .line 292769
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292770
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 292771
    invoke-static {p1}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 292772
    invoke-static {p1, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    .line 292773
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 292774
    const/4 v1, 0x0

    .line 292775
    :goto_2
    move-object v0, v1

    .line 292776
    iget-object v1, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292777
    :cond_3
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;Z)V

    .line 292778
    goto :goto_0

    .line 292779
    :cond_4
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 292780
    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 292781
    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 292782
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;

    move-result-object v1

    .line 292783
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 292784
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4ZX;

    invoke-virtual {v1}, LX/4ZX;->a()LX/162;

    move-result-object v1

    .line 292785
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_3f

    .line 292786
    const/4 v2, 0x0

    .line 292787
    :goto_3
    move-object v1, v2

    .line 292788
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292789
    :cond_5
    const/4 v1, 0x1

    .line 292790
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 292791
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 292792
    :cond_6
    goto :goto_1

    .line 292793
    :cond_7
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v0, :cond_9

    move-object v0, p1

    .line 292794
    check-cast v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 292795
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->I_()I

    move-result v2

    .line 292796
    if-ltz v2, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v2, v1, :cond_40

    .line 292797
    :cond_8
    :goto_4
    goto/16 :goto_1

    .line 292798
    :cond_9
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    if-eqz v0, :cond_b

    move-object v0, p1

    .line 292799
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    const/4 v5, 0x0

    .line 292800
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->I_()I

    move-result v2

    .line 292801
    if-ltz v2, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_42

    .line 292802
    :cond_a
    :goto_5
    goto/16 :goto_1

    .line 292803
    :cond_b
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_c

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/0x1;->a(LX/17w;)Z

    move-result v0

    if-eqz v0, :cond_c

    move-object v0, p1

    .line 292804
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 292805
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v1

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_4a

    .line 292806
    invoke-static {v0}, LX/18M;->c(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v1

    .line 292807
    :goto_6
    move v1, v1

    .line 292808
    if-eqz v1, :cond_48

    .line 292809
    :goto_7
    goto/16 :goto_1

    .line 292810
    :cond_c
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    if-eqz v0, :cond_e

    move-object v0, p1

    .line 292811
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    .line 292812
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 292813
    :cond_d
    :goto_8
    goto/16 :goto_1

    .line 292814
    :cond_e
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    if-eqz v0, :cond_10

    move-object v0, p1

    .line 292815
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 292816
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r()LX/0Px;

    move-result-object v1

    .line 292817
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I_()I

    move-result v2

    .line 292818
    if-eqz v1, :cond_f

    if-ltz v2, :cond_f

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_4f

    .line 292819
    :cond_f
    :goto_9
    goto/16 :goto_1

    .line 292820
    :cond_10
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v0, :cond_12

    move-object v0, p1

    .line 292821
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 292822
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v1

    .line 292823
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->I_()I

    move-result v2

    .line 292824
    if-eqz v1, :cond_11

    if-ltz v2, :cond_11

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_51

    .line 292825
    :cond_11
    :goto_a
    goto/16 :goto_1

    .line 292826
    :cond_12
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    if-eqz v0, :cond_14

    move-object v0, p1

    .line 292827
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    .line 292828
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;)LX/0Px;

    move-result-object v1

    .line 292829
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->I_()I

    move-result v2

    if-ltz v2, :cond_13

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_53

    .line 292830
    :cond_13
    :goto_b
    goto/16 :goto_1

    .line 292831
    :cond_14
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v0, :cond_16

    move-object v0, p1

    .line 292832
    check-cast v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 292833
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)LX/0Px;

    move-result-object v1

    .line 292834
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->I_()I

    move-result v2

    if-ltz v2, :cond_15

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_55

    .line 292835
    :cond_15
    :goto_c
    goto/16 :goto_1

    .line 292836
    :cond_16
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    if-eqz v0, :cond_17

    move-object v0, p1

    .line 292837
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-direct {p0, v0}, LX/1g4;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V

    goto/16 :goto_1

    .line 292838
    :cond_17
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    if-eqz v0, :cond_19

    move-object v0, p1

    .line 292839
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 292840
    if-eqz v0, :cond_18

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_57

    .line 292841
    :cond_18
    :goto_d
    goto/16 :goto_1

    .line 292842
    :cond_19
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_1a

    move-object v0, p1

    .line 292843
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 292844
    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_59

    .line 292845
    :goto_e
    goto/16 :goto_1

    .line 292846
    :cond_1a
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_1c

    move-object v0, p1

    .line 292847
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    .line 292848
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->k()LX/0Px;

    move-result-object v1

    .line 292849
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->I_()I

    move-result v2

    .line 292850
    if-eqz v1, :cond_1b

    if-ltz v2, :cond_1b

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_5b

    .line 292851
    :cond_1b
    :goto_f
    goto/16 :goto_1

    .line 292852
    :cond_1c
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-eqz v0, :cond_1f

    move-object v0, p1

    .line 292853
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    .line 292854
    const/4 v2, 0x0

    .line 292855
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5d

    :cond_1d
    move v1, v2

    .line 292856
    :goto_10
    move v1, v1

    .line 292857
    if-nez v1, :cond_5c

    .line 292858
    :cond_1e
    :goto_11
    goto/16 :goto_1

    .line 292859
    :cond_1f
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    if-eqz v0, :cond_21

    move-object v0, p1

    .line 292860
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 292861
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v1

    .line 292862
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->I_()I

    move-result v2

    .line 292863
    if-eqz v1, :cond_20

    if-ltz v2, :cond_20

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lt v2, v3, :cond_60

    .line 292864
    :cond_20
    :goto_12
    goto/16 :goto_1

    .line 292865
    :cond_21
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v0, :cond_23

    move-object v0, p1

    .line 292866
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 292867
    if-eqz v0, :cond_22

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    if-eqz v1, :cond_22

    if-gez p2, :cond_61

    .line 292868
    :cond_22
    :goto_13
    goto/16 :goto_1

    .line 292869
    :cond_23
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    if-eqz v0, :cond_25

    move-object v0, p1

    .line 292870
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    .line 292871
    if-eqz v0, :cond_24

    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_24

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_63

    .line 292872
    :cond_24
    :goto_14
    goto/16 :goto_1

    .line 292873
    :cond_25
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    if-eqz v0, :cond_27

    move-object v0, p1

    .line 292874
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    const/4 v5, 0x1

    .line 292875
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->I_()I

    move-result v1

    .line 292876
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;

    move-result-object v2

    .line 292877
    if-eqz v2, :cond_26

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_66

    .line 292878
    :cond_26
    :goto_15
    goto/16 :goto_1

    .line 292879
    :cond_27
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    if-eqz v0, :cond_29

    move-object v0, p1

    .line 292880
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 292881
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 292882
    if-eqz v1, :cond_28

    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    if-eqz v2, :cond_28

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_69

    .line 292883
    :cond_28
    :goto_16
    goto/16 :goto_1

    .line 292884
    :cond_29
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    if-eqz v0, :cond_2b

    move-object v0, p1

    .line 292885
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 292886
    if-nez v0, :cond_6a

    .line 292887
    :cond_2a
    :goto_17
    goto/16 :goto_1

    .line 292888
    :cond_2b
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    if-eqz v0, :cond_2d

    move-object v0, p1

    .line 292889
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 292890
    if-eqz v0, :cond_2c

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_2c

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6c

    .line 292891
    :cond_2c
    :goto_18
    goto/16 :goto_1

    .line 292892
    :cond_2d
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    if-eqz v0, :cond_2f

    move-object v0, p1

    .line 292893
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 292894
    if-eqz v0, :cond_2e

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6d

    .line 292895
    :cond_2e
    :goto_19
    goto/16 :goto_1

    .line 292896
    :cond_2f
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    if-eqz v0, :cond_31

    move-object v0, p1

    .line 292897
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 292898
    if-eqz v0, :cond_30

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_6e

    .line 292899
    :cond_30
    :goto_1a
    goto/16 :goto_1

    .line 292900
    :cond_31
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    if-eqz v0, :cond_33

    move-object v0, p1

    .line 292901
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    .line 292902
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_70

    .line 292903
    :cond_32
    :goto_1b
    goto/16 :goto_1

    .line 292904
    :cond_33
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    if-eqz v0, :cond_36

    move-object v0, p1

    .line 292905
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    .line 292906
    const/4 v2, 0x0

    .line 292907
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_34

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_34

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_34

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_72

    :cond_34
    move v1, v2

    .line 292908
    :goto_1c
    move v1, v1

    .line 292909
    if-nez v1, :cond_71

    .line 292910
    :cond_35
    :goto_1d
    goto/16 :goto_1

    .line 292911
    :cond_36
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    if-eqz v0, :cond_38

    move-object v0, p1

    .line 292912
    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    const/4 v5, 0x1

    .line 292913
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_75

    .line 292914
    :cond_37
    :goto_1e
    goto/16 :goto_1

    .line 292915
    :cond_38
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 292916
    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 292917
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_39

    if-ltz p2, :cond_39

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge p2, v1, :cond_39

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_77

    .line 292918
    :cond_39
    :goto_1f
    goto/16 :goto_1

    .line 292919
    :cond_3a
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)LX/0Px;

    move-result-object v1

    .line 292920
    if-eqz v1, :cond_3c

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_3c

    .line 292921
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/25E;

    .line 292922
    :goto_20
    move-object v2, v1

    .line 292923
    if-eqz v2, :cond_2

    .line 292924
    invoke-interface {v2}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    if-eqz v1, :cond_3b

    const/4 v1, 0x1

    :goto_21
    invoke-static {v2, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 292925
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 292926
    const/4 v3, 0x0

    .line 292927
    :goto_22
    move-object v1, v3

    .line 292928
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292929
    invoke-static {v0}, LX/18M;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    goto/16 :goto_1

    .line 292930
    :cond_3b
    const/4 v1, 0x0

    goto :goto_21

    :cond_3c
    const/4 v1, 0x0

    goto :goto_20

    .line 292931
    :cond_3d
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "pyml_imp"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "tracking"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "scroll_index"

    invoke-virtual {v3, v4, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "native_newsfeed"

    .line 292932
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 292933
    move-object v3, v3

    .line 292934
    goto :goto_22

    .line 292935
    :cond_3e
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "ig_text_link_impression_v2"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 292936
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 292937
    move-object v1, v1

    .line 292938
    goto/16 :goto_2

    .line 292939
    :cond_3f
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "ig_pff_imp"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 292940
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 292941
    move-object v2, v2

    .line 292942
    goto/16 :goto_3

    .line 292943
    :cond_40
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 292944
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 292945
    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 292946
    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 292947
    invoke-static {v3}, LX/17Q;->F(LX/0lF;)Z

    move-result p2

    if-eqz p2, :cond_41

    .line 292948
    const/4 p2, 0x0

    .line 292949
    :goto_23
    move-object v2, p2

    .line 292950
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292951
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)V

    goto/16 :goto_4

    :cond_41
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "chained_article_item_impression"

    invoke-direct {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {p2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v0, "URL"

    invoke-virtual {p2, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v0, "scroll_index"

    invoke-virtual {p2, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v0, "native_newsfeed"

    .line 292952
    iput-object v0, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 292953
    move-object p2, p2

    .line 292954
    goto :goto_23

    .line 292955
    :cond_42
    if-eqz v0, :cond_43

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    if-nez v3, :cond_45

    .line 292956
    :cond_43
    :goto_24
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 292957
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 292958
    invoke-static {v8}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 292959
    iget-object v2, p0, LX/1g4;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_44

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    :goto_25
    const/4 v7, 0x0

    move-object v6, v5

    invoke-virtual/range {v2 .. v7}, LX/1C2;->a(LX/0lF;Ljava/lang/String;LX/04D;LX/04G;Z)LX/1C2;

    .line 292960
    const/4 v2, 0x1

    invoke-static {v8, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)V

    goto/16 :goto_5

    :cond_44
    move-object v4, v5

    .line 292961
    goto :goto_25

    .line 292962
    :cond_45
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v6

    .line 292963
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 292964
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v3

    .line 292965
    if-eqz v3, :cond_47

    .line 292966
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;->a()LX/0Px;

    move-result-object v8

    .line 292967
    if-eqz v8, :cond_47

    .line 292968
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v3, 0x0

    move v4, v3

    :goto_26
    if-ge v4, v9, :cond_47

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 292969
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 292970
    if-eqz v3, :cond_46

    .line 292971
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292972
    :cond_46
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_26

    .line 292973
    :cond_47
    iget-object v3, p0, LX/1g4;->e:LX/04B;

    invoke-virtual {v3, v6, v7}, LX/04B;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_24

    .line 292974
    :cond_48
    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 292975
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 292976
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 292977
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_4b

    .line 292978
    const/4 v2, 0x0

    .line 292979
    :goto_27
    move-object v1, v2

    .line 292980
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292981
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v1

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_49

    .line 292982
    invoke-static {v0}, LX/18M;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 292983
    :cond_49
    goto/16 :goto_7

    :cond_4a
    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_4b
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "chained_story_item_impression"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "scroll_index"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 292984
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 292985
    move-object v2, v2

    .line 292986
    goto :goto_27

    .line 292987
    :cond_4c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    .line 292988
    const/4 v1, 0x0

    move v2, v1

    :goto_28
    if-ge v2, v3, :cond_4d

    .line 292989
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    .line 292990
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    .line 292991
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-eqz v4, :cond_4e

    .line 292992
    const/4 v4, 0x0

    .line 292993
    :goto_29
    move-object v1, v4

    .line 292994
    iget-object v4, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v4, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 292995
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_28

    .line 292996
    :cond_4d
    const/4 v1, 0x1

    .line 292997
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 292998
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 292999
    goto/16 :goto_8

    .line 293000
    :cond_4e
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "gifts_imp"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 293001
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293002
    move-object v4, v4

    .line 293003
    goto :goto_29

    .line 293004
    :cond_4f
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 293005
    if-eqz v1, :cond_f

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 293006
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293007
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_50

    .line 293008
    const/4 v3, 0x0

    .line 293009
    :goto_2a
    move-object v2, v3

    .line 293010
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293011
    const/4 v2, 0x1

    .line 293012
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293013
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293014
    goto/16 :goto_9

    .line 293015
    :cond_50
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "pymi_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "native_newsfeed"

    .line 293016
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293017
    move-object v3, v3

    .line 293018
    goto :goto_2a

    .line 293019
    :cond_51
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Fa;

    .line 293020
    if-eqz v1, :cond_11

    invoke-static {v1}, LX/18M;->a(LX/1Fa;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 293021
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293022
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_52

    .line 293023
    const/4 v3, 0x0

    .line 293024
    :goto_2b
    move-object v2, v3

    .line 293025
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293026
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/18M;->a(LX/1Fa;Z)V

    goto/16 :goto_a

    .line 293027
    :cond_52
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "pymk_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "native_newsfeed"

    .line 293028
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293029
    move-object v3, v3

    .line 293030
    goto :goto_2b

    .line 293031
    :cond_53
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 293032
    if-eqz v1, :cond_13

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 293033
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293034
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_54

    .line 293035
    const/4 v3, 0x0

    .line 293036
    :goto_2c
    move-object v2, v3

    .line 293037
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293038
    const/4 v2, 0x1

    .line 293039
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293040
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293041
    goto/16 :goto_b

    .line 293042
    :cond_54
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "pysf_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "native_newsfeed"

    .line 293043
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293044
    move-object v3, v3

    .line 293045
    goto :goto_2c

    .line 293046
    :cond_55
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 293047
    if-eqz v1, :cond_15

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 293048
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293049
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 293050
    const/4 v3, 0x0

    .line 293051
    :goto_2d
    move-object v2, v3

    .line 293052
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293053
    const/4 v2, 0x1

    .line 293054
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293055
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293056
    goto/16 :goto_c

    .line 293057
    :cond_56
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "connect_with_facebook_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "native_newsfeed"

    .line 293058
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293059
    move-object v3, v3

    .line 293060
    goto :goto_2d

    .line 293061
    :cond_57
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v3

    .line 293062
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_2e
    if-ge v2, v4, :cond_58

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 293063
    const-string v5, "gysc_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v5, v1}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 293064
    iget-object v5, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v5, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293065
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2e

    .line 293066
    :cond_58
    const/4 v1, 0x1

    .line 293067
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293068
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293069
    goto/16 :goto_d

    .line 293070
    :cond_59
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v3

    .line 293071
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 293072
    const/4 v1, 0x0

    move v2, v1

    :goto_2f
    if-ge v2, v4, :cond_5a

    .line 293073
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 293074
    const-string v5, "gysj_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v5, v1}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 293075
    iget-object v5, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v5, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293076
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2f

    .line 293077
    :cond_5a
    const/4 v1, 0x1

    .line 293078
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293079
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293080
    goto/16 :goto_e

    .line 293081
    :cond_5b
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/254;

    .line 293082
    if-eqz v1, :cond_1b

    invoke-static {v1}, LX/0x1;->a(LX/254;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 293083
    const-string v2, "gysj_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v2, v3}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293084
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293085
    const/4 v2, 0x1

    .line 293086
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293087
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293088
    goto/16 :goto_f

    .line 293089
    :cond_5c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    .line 293090
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->I_()I

    move-result v2

    .line 293091
    if-ltz v2, :cond_1e

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_1e

    .line 293092
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;

    .line 293093
    if-eqz v1, :cond_1e

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 293094
    const-string v2, "gsym_imp"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-static {v3, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v2, v3}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293095
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293096
    const/4 v2, 0x1

    .line 293097
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293098
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293099
    goto/16 :goto_11

    .line 293100
    :cond_5d
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_30
    if-ge v3, v5, :cond_5f

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;

    .line 293101
    invoke-static {v1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;)Z

    move-result v1

    if-nez v1, :cond_5e

    move v1, v2

    .line 293102
    goto/16 :goto_10

    .line 293103
    :cond_5e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_30

    .line 293104
    :cond_5f
    const/4 v1, 0x1

    goto/16 :goto_10

    .line 293105
    :cond_60
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 293106
    if-eqz v1, :cond_20

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 293107
    const-string v2, "sgny_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v2, v3}, LX/17Q;->c(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293108
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293109
    const/4 v2, 0x1

    .line 293110
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293111
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293112
    goto/16 :goto_12

    .line 293113
    :cond_61
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v1

    .line 293114
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge p2, v2, :cond_22

    .line 293115
    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 293116
    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 293117
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v3

    .line 293118
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 293119
    const/4 v0, 0x0

    .line 293120
    :goto_31
    move-object v2, v0

    .line 293121
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293122
    const/4 v2, 0x1

    .line 293123
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293124
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293125
    goto/16 :goto_13

    .line 293126
    :cond_62
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "saved_collection_ego_imp"

    invoke-direct {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "tracking"

    invoke-virtual {v0, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p2, "collection_id"

    invoke-virtual {v0, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p2, "native_newsfeed"

    .line 293127
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293128
    move-object v0, v0

    .line 293129
    goto :goto_31

    .line 293130
    :cond_63
    const/4 v1, 0x0

    move v2, v1

    :goto_32
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_64

    .line 293131
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;

    .line 293132
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    .line 293133
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_65

    .line 293134
    const/4 v3, 0x0

    .line 293135
    :goto_33
    move-object v1, v3

    .line 293136
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293137
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_32

    .line 293138
    :cond_64
    const/4 v1, 0x1

    .line 293139
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293140
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293141
    goto/16 :goto_14

    .line 293142
    :cond_65
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "social_wifi_ego_imp"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "tracking"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "native_newsfeed"

    .line 293143
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293144
    move-object v3, v3

    .line 293145
    goto :goto_33

    .line 293146
    :cond_66
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 293147
    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 293148
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293149
    sget-object v3, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_ITEM_IMPRESSION:LX/6VK;

    invoke-static {v3, v2}, LX/1g5;->a(LX/6VK;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293150
    if-eqz v2, :cond_67

    .line 293151
    const-string v3, "feed_type"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 293152
    const-string v3, "location_category"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 293153
    :cond_67
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293154
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293155
    iput-boolean v5, v2, LX/0x2;->n:Z

    .line 293156
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eqz v2, :cond_26

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->CANNOT_WAVE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    if-eq v2, v3, :cond_26

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    if-eq v2, v3, :cond_26

    .line 293157
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    const-string v3, "friends_nearby_feedunit_wave"

    invoke-interface {v2, v3, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 293158
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_26

    .line 293159
    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 293160
    const-string v4, "native_newsfeed"

    invoke-virtual {v2, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "target_id"

    if-nez v3, :cond_68

    const-string v2, ""

    :goto_34
    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "impression"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "wave_status"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    goto/16 :goto_15

    :cond_68
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_34

    .line 293161
    :cond_69
    const/4 v2, 0x1

    .line 293162
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293163
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293164
    invoke-static {v0}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)LX/162;

    move-result-object v2

    .line 293165
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "quick_promotion_ego_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "native_newsfeed"

    .line 293166
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293167
    move-object v3, v3

    .line 293168
    move-object v2, v3

    .line 293169
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 293170
    iget-object v1, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_16

    .line 293171
    :cond_6a
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v2

    .line 293172
    if-eqz v2, :cond_2a

    if-ltz p2, :cond_2a

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    if-ge p2, v1, :cond_2a

    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_2a

    .line 293173
    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 293174
    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 293175
    new-instance v2, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/162;-><init>(LX/0mC;)V

    .line 293176
    invoke-static {v1}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/162;->a(LX/162;)LX/162;

    .line 293177
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 293178
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    .line 293179
    invoke-static {v2}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 293180
    const/4 v0, 0x0

    .line 293181
    :goto_35
    move-object v2, v0

    .line 293182
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293183
    const/4 v2, 0x1

    .line 293184
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293185
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293186
    goto/16 :goto_17

    :cond_6b
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "place_review_imp"

    invoke-direct {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "tracking"

    invoke-virtual {v0, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p2, "page_id"

    invoke-virtual {v0, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p2, "native_newsfeed"

    .line 293187
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293188
    move-object v0, v0

    .line 293189
    goto :goto_35

    .line 293190
    :cond_6c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s()Ljava/lang/String;

    move-result-object v1

    .line 293191
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "story_gallery_survey_feed_unit_impression"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 293192
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293193
    move-object v2, v2

    .line 293194
    move-object v1, v2

    .line 293195
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293196
    const/4 v1, 0x1

    .line 293197
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293198
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293199
    goto/16 :goto_18

    .line 293200
    :cond_6d
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;)LX/0Px;

    move-result-object v1

    .line 293201
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->I_()I

    move-result v2

    .line 293202
    if-eqz v1, :cond_2e

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_2e

    .line 293203
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 293204
    if-eqz v1, :cond_2e

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v2

    if-nez v2, :cond_2e

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2e

    .line 293205
    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    .line 293206
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "page_admin_panel_imp"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v0, "page_admin_panel"

    .line 293207
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293208
    move-object v3, v3

    .line 293209
    move-object v2, v3

    .line 293210
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293211
    const/4 v2, 0x1

    .line 293212
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293213
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293214
    goto/16 :goto_19

    .line 293215
    :cond_6e
    invoke-static {v0}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/4Zk;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, LX/4Zk;->b(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v4

    .line 293216
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v5

    if-eqz v5, :cond_6f

    .line 293217
    const/4 v5, 0x0

    .line 293218
    :goto_36
    move-object v1, v5

    .line 293219
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293220
    const/4 v1, 0x1

    .line 293221
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293222
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293223
    goto/16 :goto_1a

    .line 293224
    :cond_6f
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "pyma_imp"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "tracking"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "pyma_category"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "pyma_additional_info"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "native_newsfeed"

    .line 293225
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293226
    move-object v5, v5

    .line 293227
    goto :goto_36

    .line 293228
    :cond_70
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    .line 293229
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->I_()I

    move-result v2

    .line 293230
    if-ltz v2, :cond_32

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_32

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_32

    .line 293231
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 293232
    if-eqz v1, :cond_32

    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)Z

    move-result v2

    if-nez v2, :cond_32

    .line 293233
    const-string v2, "psym_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v2, v1}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 293234
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293235
    const/4 v1, 0x1

    .line 293236
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293237
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293238
    goto/16 :goto_1b

    .line 293239
    :cond_71
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v1

    .line 293240
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->I_()I

    move-result v2

    .line 293241
    if-ltz v2, :cond_35

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_35

    .line 293242
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;

    .line 293243
    if-eqz v1, :cond_35

    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;)Z

    move-result v2

    if-nez v2, :cond_35

    .line 293244
    const-string v2, "ssfy_imp"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-static {v3, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v2, v3}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293245
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293246
    const/4 v2, 0x1

    .line 293247
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 293248
    iput-boolean v2, v3, LX/0x2;->n:Z

    .line 293249
    goto/16 :goto_1d

    .line 293250
    :cond_72
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    .line 293251
    :goto_37
    if-ge v3, v5, :cond_74

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;

    .line 293252
    invoke-static {v1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;)Z

    move-result v1

    if-nez v1, :cond_73

    move v1, v2

    .line 293253
    goto/16 :goto_1c

    .line 293254
    :cond_73
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_37

    .line 293255
    :cond_74
    const/4 v1, 0x1

    goto/16 :goto_1c

    .line 293256
    :cond_75
    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_76

    .line 293257
    iget-object v1, p0, LX/1g4;->g:LX/1g6;

    sget-object v2, LX/7iQ;->PDFY_UNIT_VIEW:LX/7iQ;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/1g6;->a(LX/7iQ;Ljava/lang/String;)V

    .line 293258
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 293259
    iput-boolean v5, v1, LX/0x2;->n:Z

    .line 293260
    :cond_76
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v1

    .line 293261
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->I_()I

    move-result v2

    .line 293262
    if-ltz v2, :cond_37

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_37

    .line 293263
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 293264
    invoke-static {v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)Z

    move-result v2

    if-nez v2, :cond_37

    .line 293265
    iget-object v2, p0, LX/1g4;->g:LX/1g6;

    sget-object v3, LX/7iQ;->PDFY_PRODUCT_VIEW:LX/7iQ;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1g6;->a(LX/7iQ;Ljava/lang/String;)V

    .line 293266
    const-string v2, "pdfy_imp"

    invoke-static {v1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v2, v3}, LX/17Q;->b(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 293267
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293268
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293269
    iput-boolean v5, v2, LX/0x2;->n:Z

    .line 293270
    goto/16 :goto_1e

    .line 293271
    :cond_77
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v1

    .line 293272
    if-eqz v1, :cond_39

    .line 293273
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/17Q;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 293274
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "job_carousel_unit_impression"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "waterfall_session_id"

    sget-object p2, LX/17Q;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v2, v2

    .line 293275
    iget-object v3, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293276
    iget-object v2, p0, LX/1g4;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293277
    const/4 v1, 0x1

    .line 293278
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 293279
    iput-boolean v1, v2, LX/0x2;->n:Z

    .line 293280
    goto/16 :goto_1f
.end method
