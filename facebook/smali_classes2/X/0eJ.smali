.class public LX/0eJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91697
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "logging/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 91698
    sput-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "debug_logs"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->b:LX/0Tn;

    .line 91699
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "logging_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->c:LX/0Tn;

    .line 91700
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_to_logcat"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->d:LX/0Tn;

    .line 91701
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "thread_tracking"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->e:LX/0Tn;

    .line 91702
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "data_connection_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->f:LX/0Tn;

    .line 91703
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "nonemployee_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->g:LX/0Tn;

    .line 91704
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "home_stories_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->h:LX/0Tn;

    .line 91705
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "fresh_feed_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->i:LX/0Tn;

    .line 91706
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "fresh_feed_overlay_statuses"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->j:LX/0Tn;

    .line 91707
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "fresh_feed_overlay_visualization"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->k:LX/0Tn;

    .line 91708
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "fresh_feed_overlay_story_info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->l:LX/0Tn;

    .line 91709
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "components_conversion_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->m:LX/0Tn;

    .line 91710
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "core_dump"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->n:LX/0Tn;

    .line 91711
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "client_value_model_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->o:LX/0Tn;

    .line 91712
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "fresco_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->p:LX/0Tn;

    .line 91713
    sget-object v0, LX/0eJ;->a:LX/0Tn;

    const-string v1, "cameracore_fps_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eJ;->q:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
