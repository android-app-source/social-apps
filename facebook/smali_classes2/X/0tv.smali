.class public LX/0tv;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155805
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 155806
    return-void
.end method

.method public static a(LX/0ad;LX/0W3;LX/0So;)LX/0tw;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 155811
    new-instance v0, LX/0tw;

    invoke-direct {v0, p0, p1, p2}, LX/0tw;-><init>(LX/0ad;LX/0W3;LX/0So;)V

    return-object v0
.end method

.method public static a(Ljava/util/Set;)LX/0u0;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0u4;",
            ">;)",
            "Lcom/facebook/contextual/ContextsProviderRegistry;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 155810
    new-instance v0, LX/0u0;

    invoke-direct {v0, p0}, LX/0u0;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static a(LX/0Zb;)LX/0uc;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 155812
    new-instance v0, LX/0ub;

    invoke-direct {v0, p0}, LX/0ub;-><init>(LX/0Zb;)V

    return-object v0
.end method

.method public static a(LX/0u0;LX/0uc;)LX/0ud;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 155809
    new-instance v0, LX/0ud;

    new-instance v1, LX/0ue;

    invoke-direct {v1}, LX/0ue;-><init>()V

    invoke-direct {v0, v1, p0, p1}, LX/0ud;-><init>(LX/0ue;LX/0u0;LX/0uc;)V

    return-object v0
.end method

.method public static a(LX/0tw;LX/0ud;LX/0uc;LX/0So;)LX/0uf;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 155808
    new-instance v0, LX/0uf;

    invoke-direct {v0, p0, p1, p2, p3}, LX/0uf;-><init>(LX/0tw;LX/0ud;LX/0uc;LX/0So;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 155807
    return-void
.end method
