.class public final LX/0rO;
.super LX/0rP;
.source ""


# instance fields
.field public final a:D


# direct methods
.method private constructor <init>(D)V
    .locals 1

    .prologue
    .line 149538
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput-wide p1, p0, LX/0rO;->a:D

    return-void
.end method

.method public static b(D)LX/0rO;
    .locals 2

    .prologue
    .line 149541
    new-instance v0, LX/0rO;

    invoke-direct {v0, p0, p1}, LX/0rO;-><init>(D)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 149540
    invoke-virtual {p0}, LX/0lF;->z()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 149539
    iget-wide v0, p0, LX/0rO;->a:D

    invoke-static {v0, v1}, LX/13I;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 149522
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    return-object v0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 149537
    sget-object v0, LX/16L;->DOUBLE:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149530
    if-ne p1, p0, :cond_1

    .line 149531
    :cond_0
    :goto_0
    return v0

    .line 149532
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 149533
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 149534
    goto :goto_0

    .line 149535
    :cond_3
    check-cast p1, LX/0rO;

    iget-wide v2, p1, LX/0rO;->a:D

    .line 149536
    iget-wide v4, p0, LX/0rO;->a:D

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 149542
    iget-wide v0, p0, LX/0rO;->a:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 149543
    long-to-int v2, v0

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v0, v0

    xor-int/2addr v0, v2

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 149528
    iget-wide v0, p0, LX/0rO;->a:D

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(D)V

    .line 149529
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 149527
    iget-wide v0, p0, LX/0rO;->a:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 2

    .prologue
    .line 149526
    iget-wide v0, p0, LX/0rO;->a:D

    double-to-int v0, v0

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 149525
    iget-wide v0, p0, LX/0rO;->a:D

    double-to-long v0, v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 149524
    iget-wide v0, p0, LX/0rO;->a:D

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 149523
    iget-wide v0, p0, LX/0rO;->a:D

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method
