.class public LX/0oj;
.super LX/0i8;
.source ""


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 141920
    invoke-direct {p0}, LX/0i8;-><init>()V

    .line 141921
    iput-object p1, p0, LX/0oj;->a:LX/0W3;

    .line 141922
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 141923
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->ju:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 141916
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jj:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    .line 141917
    if-eqz v0, :cond_0

    .line 141918
    iget-object v1, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->fn:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    .line 141919
    :cond_0
    return v0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 141915
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jk:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 4

    .prologue
    .line 141914
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jl:J

    const/16 v1, 0xf

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 141913
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jB:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    .line 141912
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jm:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 141911
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jr:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 141910
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jC:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 141909
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jx:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 4

    .prologue
    .line 141908
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jz:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    .line 141924
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jA:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 4

    .prologue
    .line 141897
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jU:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 4

    .prologue
    .line 141898
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jn:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 4

    .prologue
    .line 141899
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jo:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 4

    .prologue
    .line 141900
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jp:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 4

    .prologue
    .line 141901
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jq:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 4

    .prologue
    .line 141902
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->js:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 4

    .prologue
    .line 141903
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jt:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 141904
    const/4 v0, 0x0

    return v0
.end method

.method public final u()Z
    .locals 4

    .prologue
    .line 141905
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jv:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final v()Z
    .locals 4

    .prologue
    .line 141907
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jy:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 4

    .prologue
    .line 141906
    iget-object v0, p0, LX/0oj;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jw:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method
