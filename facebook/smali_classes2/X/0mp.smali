.class public LX/0mp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0mh;

.field public final b:Landroid/content/Context;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:Z

.field public g:LX/1Zu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I


# direct methods
.method private constructor <init>(LX/0mh;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 133106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133107
    const/4 v0, -0x1

    iput v0, p0, LX/0mp;->h:I

    .line 133108
    iput-object p1, p0, LX/0mp;->a:LX/0mh;

    .line 133109
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0mp;->b:Landroid/content/Context;

    .line 133110
    iput-object p3, p0, LX/0mp;->c:Ljava/lang/String;

    .line 133111
    iput-object p4, p0, LX/0mp;->d:Ljava/lang/String;

    .line 133112
    iput p5, p0, LX/0mp;->e:I

    .line 133113
    iput-boolean p6, p0, LX/0mp;->f:Z

    .line 133114
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0mh;)LX/0mp;
    .locals 7

    .prologue
    .line 133115
    new-instance v0, LX/0mp;

    const-string v3, "normal"

    const-string v4, "normal"

    const/16 v5, 0x61

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, LX/0mp;-><init>(LX/0mh;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;LX/0mh;)LX/0mp;
    .locals 7

    .prologue
    .line 133116
    new-instance v0, LX/0mp;

    const-string v3, "high"

    const-string v4, "hipri"

    const/16 v5, 0xb

    const/4 v6, 0x1

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, LX/0mp;-><init>(LX/0mh;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method
