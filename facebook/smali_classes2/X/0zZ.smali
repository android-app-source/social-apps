.class public abstract LX/0zZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vA;
.implements LX/0za;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0vA",
        "<TT;>;",
        "LX/0za;"
    }
.end annotation


# instance fields
.field private final a:LX/0zb;

.field private final b:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<*>;"
        }
    .end annotation
.end field

.field private c:LX/52p;

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 167404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167405
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0zZ;->d:J

    .line 167406
    const/4 v0, 0x0

    iput-object v0, p0, LX/0zZ;->b:LX/0zZ;

    .line 167407
    new-instance v0, LX/0zb;

    invoke-direct {v0}, LX/0zb;-><init>()V

    iput-object v0, p0, LX/0zZ;->a:LX/0zb;

    .line 167408
    return-void
.end method

.method public constructor <init>(LX/0zZ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 167409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167410
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0zZ;->d:J

    .line 167411
    iput-object p1, p0, LX/0zZ;->b:LX/0zZ;

    .line 167412
    iget-object v0, p1, LX/0zZ;->a:LX/0zb;

    iput-object v0, p0, LX/0zZ;->a:LX/0zb;

    .line 167413
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 167414
    const/4 v0, 0x0

    .line 167415
    monitor-enter p0

    .line 167416
    :try_start_0
    iget-object v1, p0, LX/0zZ;->c:LX/52p;

    if-eqz v1, :cond_1

    .line 167417
    iget-object v0, p0, LX/0zZ;->c:LX/52p;

    .line 167418
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167419
    if-eqz v0, :cond_0

    .line 167420
    invoke-interface {v0, p1, p2}, LX/52p;->a(J)V

    .line 167421
    :cond_0
    return-void

    .line 167422
    :cond_1
    :try_start_1
    iput-wide p1, p0, LX/0zZ;->d:J

    goto :goto_0

    .line 167423
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/0za;)V
    .locals 1

    .prologue
    .line 167383
    iget-object v0, p0, LX/0zZ;->a:LX/0zb;

    invoke-virtual {v0, p1}, LX/0zb;->a(LX/0za;)V

    .line 167384
    return-void
.end method

.method public a(LX/52p;)V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    .line 167389
    const/4 v0, 0x0

    .line 167390
    monitor-enter p0

    .line 167391
    :try_start_0
    iget-wide v2, p0, LX/0zZ;->d:J

    .line 167392
    iput-object p1, p0, LX/0zZ;->c:LX/52p;

    .line 167393
    iget-object v1, p0, LX/0zZ;->b:LX/0zZ;

    if-eqz v1, :cond_0

    .line 167394
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 167395
    const/4 v0, 0x1

    .line 167396
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167397
    if-eqz v0, :cond_1

    .line 167398
    iget-object v0, p0, LX/0zZ;->b:LX/0zZ;

    iget-object v1, p0, LX/0zZ;->c:LX/52p;

    invoke-virtual {v0, v1}, LX/0zZ;->a(LX/52p;)V

    .line 167399
    :goto_0
    return-void

    .line 167400
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 167401
    :cond_1
    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 167402
    iget-object v0, p0, LX/0zZ;->c:LX/52p;

    const-wide v2, 0x7fffffffffffffffL

    invoke-interface {v0, v2, v3}, LX/52p;->a(J)V

    goto :goto_0

    .line 167403
    :cond_2
    iget-object v0, p0, LX/0zZ;->c:LX/52p;

    invoke-interface {v0, v2, v3}, LX/52p;->a(J)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 167387
    iget-object v0, p0, LX/0zZ;->a:LX/0zb;

    invoke-virtual {v0}, LX/0zb;->b()V

    .line 167388
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 167386
    iget-object v0, p0, LX/0zZ;->a:LX/0zb;

    invoke-virtual {v0}, LX/0zb;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 167385
    return-void
.end method
