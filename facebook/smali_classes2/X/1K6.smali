.class public LX/1K6;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1K5;
.implements LX/0hk;
.implements LX/0fm;
.implements LX/0fy;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0fs;",
        ":",
        "LX/1KU;",
        ">",
        "LX/0hD;",
        "LX/1K5;",
        "LX/0hk;",
        "LX/0fm;",
        "LX/0fy;"
    }
.end annotation


# instance fields
.field private final a:LX/1K7;

.field private final b:LX/1Jl;

.field public final c:Landroid/database/DataSetObserver;

.field public final d:LX/1KR;

.field public e:LX/1DK;

.field private f:LX/1Pf;

.field public g:LX/0fs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field

.field private h:LX/0fx;

.field public i:LX/1Qr;


# direct methods
.method public constructor <init>(LX/1Jl;LX/1K7;)V
    .locals 1
    .param p1    # LX/1Jl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230892
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 230893
    new-instance v0, LX/1KP;

    invoke-direct {v0, p0}, LX/1KP;-><init>(LX/1K6;)V

    iput-object v0, p0, LX/1K6;->c:Landroid/database/DataSetObserver;

    .line 230894
    new-instance v0, LX/1KQ;

    invoke-direct {v0, p0}, LX/1KQ;-><init>(LX/1K6;)V

    iput-object v0, p0, LX/1K6;->d:LX/1KR;

    .line 230895
    iput-object p1, p0, LX/1K6;->b:LX/1Jl;

    .line 230896
    iput-object p2, p0, LX/1K6;->a:LX/1K7;

    .line 230897
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 230900
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    if-eqz v0, :cond_0

    .line 230901
    :goto_0
    return-void

    .line 230902
    :cond_0
    iget-object v0, p0, LX/1K6;->e:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    .line 230903
    if-nez v0, :cond_1

    .line 230904
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must have Scrolling View to create an adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230905
    :cond_1
    iget-object v0, p0, LX/1K6;->b:LX/1Jl;

    invoke-virtual {v0}, LX/1Jl;->a()LX/1Qr;

    move-result-object v0

    iput-object v0, p0, LX/1K6;->i:LX/1Qr;

    .line 230906
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    instance-of v0, v0, LX/1Rr;

    if-eqz v0, :cond_3

    .line 230907
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    check-cast v0, LX/1Rr;

    iget-object v1, p0, LX/1K6;->d:LX/1KR;

    invoke-interface {v0, v1}, LX/1Rr;->a(LX/1KR;)V

    .line 230908
    :cond_2
    :goto_1
    iget-object v0, p0, LX/1K6;->g:LX/0fs;

    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    iget-object v2, p0, LX/1K6;->e:LX/1DK;

    invoke-interface {v2}, LX/1DK;->a()LX/0g8;

    move-result-object v2

    iget-object v3, p0, LX/1K6;->f:LX/1Pf;

    invoke-interface {v0, v1, v2, v3}, LX/0fs;->a(LX/1Qr;LX/0g8;LX/1Pf;)V

    goto :goto_0

    .line 230909
    :cond_3
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    instance-of v0, v0, Landroid/widget/ListAdapter;

    if-eqz v0, :cond_2

    .line 230910
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v1, p0, LX/1K6;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 230898
    iget-object v0, p0, LX/1K6;->h:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 230899
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 230887
    iget-object v0, p0, LX/1K6;->h:LX/0fx;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 230888
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 230889
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    if-eqz v0, :cond_0

    .line 230890
    iget-object v0, p0, LX/1K6;->i:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 230891
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 230868
    iget-object v0, p0, LX/1K6;->a:LX/1K7;

    .line 230869
    new-instance p1, LX/1PR;

    invoke-direct {p1, v0}, LX/1PR;-><init>(LX/1K7;)V

    move-object v0, p1

    .line 230870
    iput-object v0, p0, LX/1K6;->h:LX/0fx;

    .line 230871
    iget-object v0, p0, LX/1K6;->b:LX/1Jl;

    .line 230872
    iget-object v1, v0, LX/1Jl;->l:LX/1Jm;

    const-string v2, "native_newsfeed"

    iget-object v3, v0, LX/1Jl;->x:Landroid/content/Context;

    .line 230873
    iget-object v4, v0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 230874
    iget-object v5, v4, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v4, v5

    .line 230875
    sget-object v5, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    if-ne v4, v5, :cond_0

    .line 230876
    sget-object v4, LX/DBy;->a:LX/DBy;

    move-object v4, v4

    .line 230877
    :goto_0
    move-object v4, v4

    .line 230878
    new-instance v5, Lcom/facebook/feed/fragment/controllercallbacks/NewsFeedAdapterConfiguration$1;

    invoke-direct {v5, v0}, Lcom/facebook/feed/fragment/controllercallbacks/NewsFeedAdapterConfiguration$1;-><init>(LX/1Jl;)V

    iget-object v6, v0, LX/1Jl;->p:LX/1Jg;

    iget-object v7, v0, LX/1Jl;->m:LX/1DJ;

    .line 230879
    iget-object v8, v7, LX/1DJ;->c:LX/0g5;

    move-object v7, v8

    .line 230880
    invoke-static {v7}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v7

    new-instance v8, Lcom/facebook/feed/fragment/controllercallbacks/NewsFeedAdapterConfiguration$2;

    invoke-direct {v8, v0}, Lcom/facebook/feed/fragment/controllercallbacks/NewsFeedAdapterConfiguration$2;-><init>(LX/1Jl;)V

    invoke-virtual/range {v1 .. v8}, LX/1Jm;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Ljava/lang/Runnable;)LX/1Pa;

    move-result-object v1

    iput-object v1, v0, LX/1Jl;->y:LX/1Pa;

    .line 230881
    iget-object v1, v0, LX/1Jl;->y:LX/1Pa;

    move-object v0, v1

    .line 230882
    iput-object v0, p0, LX/1K6;->f:LX/1Pf;

    .line 230883
    invoke-direct {p0}, LX/1K6;->b()V

    .line 230884
    return-void

    .line 230885
    :cond_0
    sget-object v4, LX/1PS;->a:LX/1PS;

    move-object v4, v4

    .line 230886
    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 230866
    invoke-direct {p0}, LX/1K6;->b()V

    .line 230867
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 230862
    iget-object v0, p0, LX/1K6;->a:LX/1K7;

    .line 230863
    iget-object v1, v0, LX/1K7;->d:LX/1KA;

    iget p0, v0, LX/1K7;->g:I

    invoke-virtual {v1, p0}, LX/1KA;->a(I)V

    .line 230864
    iget-object v1, v0, LX/1K7;->e:LX/1KN;

    invoke-virtual {v1}, LX/1KN;->b()V

    .line 230865
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230851
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    if-nez v1, :cond_0

    .line 230852
    :goto_0
    iput-object v0, p0, LX/1K6;->f:LX/1Pf;

    .line 230853
    iput-object v0, p0, LX/1K6;->h:LX/0fx;

    .line 230854
    return-void

    .line 230855
    :cond_0
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 230856
    iget-object v1, p0, LX/1K6;->g:LX/0fs;

    invoke-interface {v1}, LX/0fs;->n()V

    .line 230857
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    instance-of v1, v1, LX/1Rr;

    if-eqz v1, :cond_2

    .line 230858
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    check-cast v1, LX/1Rr;

    iget-object v2, p0, LX/1K6;->d:LX/1KR;

    invoke-interface {v1, v2}, LX/1Rr;->b(LX/1KR;)V

    .line 230859
    :cond_1
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, LX/1K6;->i:LX/1Qr;

    goto :goto_0

    .line 230860
    :cond_2
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    instance-of v1, v1, Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    .line 230861
    iget-object v1, p0, LX/1K6;->i:LX/1Qr;

    check-cast v1, Landroid/widget/ListAdapter;

    iget-object v2, p0, LX/1K6;->c:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_1
.end method
