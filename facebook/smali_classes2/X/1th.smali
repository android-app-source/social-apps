.class public LX/1th;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/6mp;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field

.field private final c:LX/05n;

.field public final d:LX/05h;

.field public final e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final f:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 336743
    const-class v0, LX/1th;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1th;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/05n;LX/05h;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V
    .locals 2

    .prologue
    .line 336744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336745
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/1th;->b:Landroid/util/SparseArray;

    .line 336746
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/1th;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 336747
    iput-object p1, p0, LX/1th;->c:LX/05n;

    .line 336748
    iput-object p2, p0, LX/1th;->d:LX/05h;

    .line 336749
    iput-object p3, p0, LX/1th;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 336750
    return-void
.end method

.method private b()I
    .locals 1

    .prologue
    .line 336751
    iget-object v0, p0, LX/1th;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/6mp;
    .locals 10
    .param p4    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 336752
    new-instance v0, LX/6mp;

    invoke-direct {p0}, LX/1th;->b()I

    move-result v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LX/6mp;-><init>(Ljava/lang/String;[BLX/0AL;LX/0AM;IJILjava/lang/String;)V

    .line 336753
    iget-object v1, p0, LX/1th;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 336754
    :try_start_0
    iget-object v2, p0, LX/1th;->b:Landroid/util/SparseArray;

    iget v3, v0, LX/6mp;->g:I

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 336755
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336756
    iget-object v1, p0, LX/1th;->c:LX/05n;

    new-instance v2, Lcom/facebook/mqttlite/MqttPublishQueue$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/mqttlite/MqttPublishQueue$1;-><init>(LX/1th;LX/6mp;)V

    iget v3, v0, LX/6mp;->e:I

    int-to-long v4, v3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v3}, LX/05n;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0Hh;

    move-result-object v1

    .line 336757
    invoke-virtual {v0, v1}, LX/6mp;->a(LX/05u;)V

    .line 336758
    new-instance v2, Lcom/facebook/mqttlite/MqttPublishQueue$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/facebook/mqttlite/MqttPublishQueue$2;-><init>(LX/1th;LX/0Hh;LX/6mp;)V

    .line 336759
    iget-object v3, p0, LX/1th;->c:LX/05n;

    invoke-interface {v1, v2, v3}, LX/05u;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 336760
    return-object v0

    .line 336761
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/6mp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336762
    iget-object v1, p0, LX/1th;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 336763
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/1th;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 336764
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, LX/1th;->b:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 336765
    iget-object v3, p0, LX/1th;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 336766
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 336767
    :cond_0
    monitor-exit v1

    return-object v2

    .line 336768
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 336769
    iget-object v1, p0, LX/1th;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 336770
    :try_start_0
    iget-object v0, p0, LX/1th;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mp;

    .line 336771
    if-eqz v0, :cond_0

    .line 336772
    iget-object v2, p0, LX/1th;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 336773
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336774
    if-eqz v0, :cond_1

    .line 336775
    iget-object v1, v0, LX/6mp;->i:LX/05u;

    if-eqz v1, :cond_1

    .line 336776
    iget-object v1, v0, LX/6mp;->i:LX/05u;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/05u;->cancel(Z)Z

    .line 336777
    const/4 v1, 0x0

    iput-object v1, v0, LX/6mp;->i:LX/05u;

    .line 336778
    :cond_1
    return-void

    .line 336779
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/6mq;ILjava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 336780
    iget-object v0, p0, LX/1th;->c:LX/05n;

    int-to-long v2, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p3, v2, v3, v1}, LX/05n;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0Hh;

    move-result-object v0

    .line 336781
    iput-object v0, p1, LX/6mq;->e:LX/05u;

    .line 336782
    return-void
.end method
