.class public abstract LX/0Ou;
.super Landroid/content/ContentProvider;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54471
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/282;
.end method

.method public abstract b()LX/4hA;
.end method

.method public abstract c()LX/27z;
.end method

.method public abstract d()V
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54470
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 54469
    const/4 v0, 0x1

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 54431
    const/4 v0, 0x1

    return v0
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54468
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54467
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final onCreate()Z
    .locals 1

    .prologue
    .line 54466
    const/4 v0, 0x1

    return v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 54433
    invoke-virtual {p0}, LX/0Ou;->d()V

    .line 54434
    :try_start_0
    invoke-virtual {p0}, LX/0Ou;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/4h6;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54435
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 54436
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 54437
    invoke-virtual {p0}, LX/0Ou;->getContext()Landroid/content/Context;

    invoke-virtual {p0}, LX/0Ou;->a()LX/282;

    move-result-object v5

    .line 54438
    if-eqz v5, :cond_0

    invoke-virtual {p0}, LX/0Ou;->getContext()Landroid/content/Context;

    invoke-virtual {p0}, LX/0Ou;->e()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 54439
    const-string v7, "COL_PHONE_ID"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54440
    const-string v7, "COL_TIMESTAMP"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54441
    iget-object v7, v5, LX/282;->a:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54442
    iget-wide v8, v5, LX/282;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54443
    :cond_0
    invoke-virtual {p0}, LX/0Ou;->getContext()Landroid/content/Context;

    invoke-virtual {p0}, LX/0Ou;->b()LX/4hA;

    move-result-object v5

    .line 54444
    if-eqz v5, :cond_1

    invoke-virtual {p0}, LX/0Ou;->getContext()Landroid/content/Context;

    invoke-virtual {p0}, LX/0Ou;->f()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 54445
    const-string v7, "COL_SFDID"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54446
    const-string v7, "COL_SFDID_CREATION_TS"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54447
    const-string v7, "COL_SFDID_GP"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54448
    const-string v7, "COL_SFDID_GA"

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54449
    iget-object v7, v5, LX/4hA;->a:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54450
    iget-wide v8, v5, LX/4hA;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54451
    iget-object v7, v5, LX/4hA;->c:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54452
    iget-object v5, v5, LX/4hA;->d:Ljava/lang/String;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54453
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 54454
    const/4 v4, 0x0

    .line 54455
    :goto_0
    move-object v0, v4

    .line 54456
    :goto_1
    return-object v0

    .line 54457
    :catch_0
    move-exception v0

    .line 54458
    invoke-virtual {p0}, LX/0Ou;->c()LX/27z;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 54459
    invoke-virtual {p0}, LX/0Ou;->c()LX/27z;

    move-result-object v1

    const-string v2, "AbstractPhoneIdProvider"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/27z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54460
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 54461
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v7, v5, [Ljava/lang/String;

    .line 54462
    new-instance v5, Landroid/database/MatrixCursor;

    invoke-interface {v4, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-direct {v5, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 54463
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    .line 54464
    invoke-interface {v6, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object v4, v5

    .line 54465
    goto :goto_0
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54432
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
