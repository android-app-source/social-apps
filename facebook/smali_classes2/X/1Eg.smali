.class public abstract LX/1Eg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Eh;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private c:LX/2GJ;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220754
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1Eg;-><init>(Ljava/lang/String;I)V

    .line 220755
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 220772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220773
    iput-object p1, p0, LX/1Eg;->a:Ljava/lang/String;

    .line 220774
    iput p2, p0, LX/1Eg;->b:I

    .line 220775
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220771
    iget-object v0, p0, LX/1Eg;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LX/2GJ;)V
    .locals 0

    .prologue
    .line 220769
    iput-object p1, p0, LX/1Eg;->c:LX/2GJ;

    .line 220770
    return-void
.end method

.method public b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 220767
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 220768
    return-object v0
.end method

.method public c()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220765
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 220766
    return-object v0
.end method

.method public d()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 220763
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 220764
    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 220762
    iget v0, p0, LX/1Eg;->b:I

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 220761
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 220757
    iget-object v0, p0, LX/1Eg;->c:LX/2GJ;

    if-eqz v0, :cond_0

    .line 220758
    iget-object v0, p0, LX/1Eg;->c:LX/2GJ;

    .line 220759
    invoke-static {v0}, LX/2GJ;->e(LX/2GJ;)V

    .line 220760
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220756
    invoke-virtual {p0}, LX/1Eg;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
