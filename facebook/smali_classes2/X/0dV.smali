.class public final LX/0dV;
.super LX/0Rf;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rf",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final transient a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private transient b:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 90422
    invoke-direct {p0}, LX/0Rf;-><init>()V

    .line 90423
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    .line 90424
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)V"
        }
    .end annotation

    .prologue
    .line 90418
    invoke-direct {p0}, LX/0Rf;-><init>()V

    .line 90419
    iput-object p1, p0, LX/0dV;->a:Ljava/lang/Object;

    .line 90420
    iput p2, p0, LX/0dV;->b:I

    .line 90421
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 90416
    iget-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 90414
    iget-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    aput-object v0, p1, p2

    .line 90415
    add-int/lit8 v0, p2, 0x1

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 90410
    iget v0, p0, LX/0dV;->b:I

    .line 90411
    if-nez v0, :cond_0

    .line 90412
    iget-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, LX/0dV;->b:I

    .line 90413
    :cond_0
    return v0
.end method

.method public final isHashCodeFast()Z
    .locals 1

    .prologue
    .line 90417
    iget v0, p0, LX/0dV;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 90409
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90408
    iget-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/0RZ;->a(Ljava/lang/Object;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 90407
    invoke-virtual {p0}, LX/0dV;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 90404
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 90405
    iget-object v0, p0, LX/0dV;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90406
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
