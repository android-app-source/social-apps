.class public LX/1Hk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ha;
.implements LX/0po;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final c:J

.field private static final d:J


# instance fields
.field public final a:Ljava/util/Set;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public final e:J

.field public final f:J

.field public final g:Ljava/util/concurrent/CountDownLatch;

.field public h:J

.field private final i:LX/1GE;

.field private j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final k:J

.field public final l:LX/0V8;

.field public final m:LX/1Hh;

.field private final n:LX/1GU;

.field private final o:LX/1GQ;

.field private final p:Z

.field public final q:LX/1Hl;

.field private final r:LX/0SG;

.field public final s:Ljava/lang/Object;

.field private t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 227647
    const-class v0, LX/1Hk;

    sput-object v0, LX/1Hk;->b:Ljava/lang/Class;

    .line 227648
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/1Hk;->c:J

    .line 227649
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/1Hk;->d:J

    return-void
.end method

.method public constructor <init>(LX/1Hh;LX/1GU;LX/1Hj;LX/1GE;LX/1GQ;LX/0pr;Landroid/content/Context;Ljava/util/concurrent/Executor;Z)V
    .locals 2
    .param p6    # LX/0pr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 227622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227623
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1Hk;->s:Ljava/lang/Object;

    .line 227624
    iget-wide v0, p3, LX/1Hj;->b:J

    iput-wide v0, p0, LX/1Hk;->e:J

    .line 227625
    iget-wide v0, p3, LX/1Hj;->c:J

    iput-wide v0, p0, LX/1Hk;->f:J

    .line 227626
    iget-wide v0, p3, LX/1Hj;->c:J

    iput-wide v0, p0, LX/1Hk;->h:J

    .line 227627
    invoke-static {}, LX/0V8;->a()LX/0V8;

    move-result-object v0

    iput-object v0, p0, LX/1Hk;->l:LX/0V8;

    .line 227628
    iput-object p1, p0, LX/1Hk;->m:LX/1Hh;

    .line 227629
    iput-object p2, p0, LX/1Hk;->n:LX/1GU;

    .line 227630
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1Hk;->j:J

    .line 227631
    iput-object p4, p0, LX/1Hk;->i:LX/1GE;

    .line 227632
    iget-wide v0, p3, LX/1Hj;->a:J

    iput-wide v0, p0, LX/1Hk;->k:J

    .line 227633
    iput-object p5, p0, LX/1Hk;->o:LX/1GQ;

    .line 227634
    new-instance v0, LX/1Hl;

    invoke-direct {v0}, LX/1Hl;-><init>()V

    iput-object v0, p0, LX/1Hk;->q:LX/1Hl;

    .line 227635
    if-eqz p6, :cond_0

    .line 227636
    invoke-interface {p6, p0}, LX/0pr;->a(LX/0po;)V

    .line 227637
    :cond_0
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 227638
    iput-object v0, p0, LX/1Hk;->r:LX/0SG;

    .line 227639
    iput-boolean p9, p0, LX/1Hk;->p:Z

    .line 227640
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Hk;->a:Ljava/util/Set;

    .line 227641
    iget-boolean v0, p0, LX/1Hk;->p:Z

    if-eqz v0, :cond_1

    .line 227642
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/1Hk;->g:Ljava/util/concurrent/CountDownLatch;

    .line 227643
    new-instance v0, Lcom/facebook/cache/disk/DiskStorageCache$1;

    invoke-direct {v0, p0}, Lcom/facebook/cache/disk/DiskStorageCache$1;-><init>(LX/1Hk;)V

    const v1, 0x5b71f91

    invoke-static {p8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 227644
    :goto_0
    new-instance v0, Lcom/facebook/cache/disk/DiskStorageCache$2;

    invoke-direct {v0, p0, p7}, Lcom/facebook/cache/disk/DiskStorageCache$2;-><init>(LX/1Hk;Landroid/content/Context;)V

    const v1, -0x273ad9c5

    invoke-static {p8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 227645
    return-void

    .line 227646
    :cond_1
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/1Hk;->g:Ljava/util/concurrent/CountDownLatch;

    goto :goto_0
.end method

.method private a(LX/32a;LX/1bh;Ljava/lang/String;)LX/1gI;
    .locals 8

    .prologue
    .line 227616
    iget-object v1, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 227617
    :try_start_0
    invoke-virtual {p1}, LX/32a;->a()LX/1gI;

    move-result-object v0

    .line 227618
    iget-object v2, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v2, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 227619
    iget-object v2, p0, LX/1Hk;->q:LX/1Hl;

    invoke-interface {v0}, LX/1gI;->c()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    invoke-virtual {v2, v4, v5, v6, v7}, LX/1Hl;->b(JJ)V

    .line 227620
    monitor-exit v1

    return-object v0

    .line 227621
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/cache/disk/DiskStorage$Entry;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/cache/disk/DiskStorage$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227606
    iget-object v0, p0, LX/1Hk;->r:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sget-wide v2, LX/1Hk;->c:J

    add-long/2addr v2, v0

    .line 227607
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 227608
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 227609
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35g;

    .line 227610
    invoke-virtual {v0}, LX/35g;->b()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-lez v6, :cond_0

    .line 227611
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227612
    :cond_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227613
    :cond_1
    iget-object v0, p0, LX/1Hk;->n:LX/1GU;

    invoke-interface {v0}, LX/1GU;->a()LX/43d;

    move-result-object v0

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 227614
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 227615
    return-object v1
.end method

.method private a(D)V
    .locals 7

    .prologue
    .line 227596
    iget-object v1, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 227597
    :try_start_0
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->b()V

    .line 227598
    invoke-static {p0}, LX/1Hk;->g(LX/1Hk;)Z

    .line 227599
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->c()J

    move-result-wide v2

    .line 227600
    long-to-double v4, v2

    mul-double/2addr v4, p1

    double-to-long v4, v4

    sub-long/2addr v2, v4

    .line 227601
    sget-object v0, LX/37E;->CACHE_MANAGER_TRIMMED:LX/37E;

    invoke-static {p0, v2, v3, v0}, LX/1Hk;->a(LX/1Hk;JLX/37E;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227602
    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    .line 227603
    :catch_0
    move-exception v0

    .line 227604
    iget-object v2, p0, LX/1Hk;->o:LX/1GQ;

    sget-object v3, LX/43W;->EVICTION:LX/43W;

    sget-object v4, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "trimBy: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 227605
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(LX/1Hk;JLX/37E;)V
    .locals 19
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 227574
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2}, LX/1Hh;->e()Ljava/util/Collection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/1Hk;->a(Ljava/util/Collection;)Ljava/util/Collection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 227575
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->c()J

    move-result-wide v6

    .line 227576
    sub-long v8, v6, p1

    .line 227577
    const/4 v4, 0x0

    .line 227578
    const-wide/16 v2, 0x0

    .line 227579
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide/from16 v16, v2

    move v3, v4

    move-wide/from16 v4, v16

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35g;

    .line 227580
    cmp-long v11, v4, v8

    if-gtz v11, :cond_1

    .line 227581
    move-object/from16 v0, p0

    iget-object v11, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v11, v2}, LX/1Hh;->a(LX/35g;)J

    move-result-wide v12

    .line 227582
    move-object/from16 v0, p0

    iget-object v11, v0, LX/1Hk;->a:Ljava/util/Set;

    invoke-virtual {v2}, LX/35g;->a()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v14}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 227583
    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-lez v11, :cond_0

    .line 227584
    add-int/lit8 v3, v3, 0x1

    .line 227585
    add-long/2addr v4, v12

    .line 227586
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v11

    invoke-virtual {v2}, LX/35g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/1gB;->a(Ljava/lang/String;)LX/1gB;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/1gB;->a(LX/37E;)LX/1gB;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, LX/1gB;->a(J)LX/1gB;

    move-result-object v2

    sub-long v12, v6, v4

    invoke-virtual {v2, v12, v13}, LX/1gB;->b(J)LX/1gB;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, LX/1gB;->c(J)LX/1gB;

    move-result-object v2

    .line 227587
    move-object/from16 v0, p0

    iget-object v11, v0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v11, v2}, LX/1GE;->g(LX/1gC;)V

    .line 227588
    invoke-virtual {v2}, LX/1gB;->i()V

    :cond_0
    move-wide/from16 v16, v4

    move v4, v3

    move-wide/from16 v2, v16

    move-wide/from16 v16, v2

    move v3, v4

    move-wide/from16 v4, v16

    .line 227589
    goto :goto_0

    .line 227590
    :catch_0
    move-exception v2

    .line 227591
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1Hk;->o:LX/1GQ;

    sget-object v4, LX/43W;->EVICTION:LX/43W;

    sget-object v5, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "evictAboveSize: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6, v2}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227592
    throw v2

    .line 227593
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    neg-long v4, v4

    neg-int v3, v3

    int-to-long v6, v3

    invoke-virtual {v2, v4, v5, v6, v7}, LX/1Hl;->b(JJ)V

    .line 227594
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2}, LX/1Hh;->c()V

    .line 227595
    return-void
.end method

.method public static e(LX/1Hk;)V
    .locals 12

    .prologue
    .line 227559
    iget-object v1, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 227560
    :try_start_0
    invoke-static {p0}, LX/1Hk;->g(LX/1Hk;)Z

    move-result v0

    .line 227561
    iget-object v6, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v6}, LX/1Hh;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    .line 227562
    :goto_0
    iget-object v7, p0, LX/1Hk;->l:LX/0V8;

    iget-wide v8, p0, LX/1Hk;->f:J

    iget-object v10, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v10}, LX/1Hl;->c()J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-virtual {v7, v6, v8, v9}, LX/0V8;->a(LX/0VA;J)Z

    move-result v6

    .line 227563
    if-eqz v6, :cond_3

    .line 227564
    iget-wide v6, p0, LX/1Hk;->e:J

    iput-wide v6, p0, LX/1Hk;->h:J

    .line 227565
    :goto_1
    iget-object v2, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->c()J

    move-result-wide v2

    .line 227566
    iget-wide v4, p0, LX/1Hk;->h:J

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    if-nez v0, :cond_0

    .line 227567
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->b()V

    .line 227568
    invoke-static {p0}, LX/1Hk;->g(LX/1Hk;)Z

    .line 227569
    :cond_0
    iget-wide v4, p0, LX/1Hk;->h:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 227570
    iget-wide v2, p0, LX/1Hk;->h:J

    const-wide/16 v4, 0x9

    mul-long/2addr v2, v4

    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    sget-object v0, LX/37E;->CACHE_FULL:LX/37E;

    invoke-static {p0, v2, v3, v0}, LX/1Hk;->a(LX/1Hk;JLX/37E;)V

    .line 227571
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 227572
    :cond_2
    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    goto :goto_0

    .line 227573
    :cond_3
    iget-wide v6, p0, LX/1Hk;->f:J

    iput-wide v6, p0, LX/1Hk;->h:J

    goto :goto_1
.end method

.method public static g(LX/1Hk;)Z
    .locals 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 227555
    iget-object v0, p0, LX/1Hk;->r:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 227556
    iget-object v2, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/1Hk;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/1Hk;->j:J

    sub-long/2addr v0, v2

    sget-wide v2, LX/1Hk;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 227557
    :cond_0
    invoke-direct {p0}, LX/1Hk;->h()Z

    move-result v0

    .line 227558
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 23
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 227392
    const-wide/16 v8, 0x0

    .line 227393
    const/4 v7, 0x0

    .line 227394
    const/4 v6, 0x0

    .line 227395
    const/4 v5, 0x0

    .line 227396
    const/4 v4, 0x0

    .line 227397
    const-wide/16 v2, -0x1

    .line 227398
    move-object/from16 v0, p0

    iget-object v10, v0, LX/1Hk;->r:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v14

    .line 227399
    sget-wide v10, LX/1Hk;->c:J

    add-long v16, v14, v10

    .line 227400
    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/1Hk;->p:Z

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v10}, Ljava/util/Set;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 227401
    move-object/from16 v0, p0

    iget-object v10, v0, LX/1Hk;->a:Ljava/util/Set;

    move-object v12, v10

    .line 227402
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v10}, LX/1Hh;->e()Ljava/util/Collection;

    move-result-object v10

    .line 227403
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move-wide v10, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move/from16 v22, v4

    move-wide v4, v2

    move/from16 v3, v22

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35g;

    .line 227404
    add-int/lit8 v9, v8, 0x1

    .line 227405
    invoke-virtual {v2}, LX/35g;->d()J

    move-result-wide v18

    add-long v10, v10, v18

    .line 227406
    invoke-virtual {v2}, LX/35g;->b()J

    move-result-wide v18

    cmp-long v8, v18, v16

    if-lez v8, :cond_2

    .line 227407
    const/4 v8, 0x1

    .line 227408
    add-int/lit8 v7, v6, 0x1

    .line 227409
    int-to-long v0, v3

    move-wide/from16 v18, v0

    invoke-virtual {v2}, LX/35g;->d()J

    move-result-wide v20

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v6, v0

    .line 227410
    invoke-virtual {v2}, LX/35g;->b()J

    move-result-wide v2

    sub-long/2addr v2, v14

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    move-wide v4, v2

    move v3, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto :goto_1

    .line 227411
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/1Hk;->p:Z

    if-eqz v10, :cond_1

    .line 227412
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    move-object v12, v10

    goto :goto_0

    .line 227413
    :cond_1
    const/4 v10, 0x0

    move-object v12, v10

    goto :goto_0

    .line 227414
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/1Hk;->p:Z

    if-eqz v8, :cond_3

    .line 227415
    invoke-virtual {v2}, LX/35g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    move v8, v9

    .line 227416
    goto :goto_1

    .line 227417
    :cond_4
    if-eqz v7, :cond_5

    .line 227418
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->o:LX/1GQ;

    sget-object v7, LX/43W;->READ_INVALID_ENTRY:LX/43W;

    sget-object v9, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v16, "Future timestamp found in "

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v13, " files , with a total size of "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " bytes, and a maximum time delta of "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v7, v9, v3, v4}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227419
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->d()J

    move-result-wide v2

    int-to-long v4, v8

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->c()J

    move-result-wide v2

    cmp-long v2, v2, v10

    if-eqz v2, :cond_8

    .line 227420
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/1Hk;->p:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->a:Ljava/util/Set;

    if-eq v2, v12, :cond_9

    .line 227421
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/1Hk;->t:Z

    .line 227422
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    int-to-long v4, v8

    invoke-virtual {v2, v10, v11, v4, v5}, LX/1Hl;->a(JJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 227423
    :cond_8
    move-object/from16 v0, p0

    iput-wide v14, v0, LX/1Hk;->j:J

    .line 227424
    const/4 v2, 0x1

    :goto_3
    return v2

    .line 227425
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/1Hk;->p:Z

    if-eqz v2, :cond_7

    .line 227426
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 227427
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v2, v12}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 227428
    :catch_0
    move-exception v2

    .line 227429
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1Hk;->o:LX/1GQ;

    sget-object v4, LX/43W;->GENERIC_IO:LX/43W;

    sget-object v5, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "calcFileCacheSize: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6, v2}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227430
    const/4 v2, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final U_()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 227545
    iget-object v1, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 227546
    :try_start_0
    invoke-static {p0}, LX/1Hk;->g(LX/1Hk;)Z

    .line 227547
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->c()J

    move-result-wide v2

    .line 227548
    iget-wide v4, p0, LX/1Hk;->k:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    iget-wide v4, p0, LX/1Hk;->k:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 227549
    :cond_0
    monitor-exit v1

    .line 227550
    :goto_0
    return-void

    .line 227551
    :cond_1
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-wide v6, p0, LX/1Hk;->k:J

    long-to-double v6, v6

    long-to-double v2, v2

    div-double v2, v6, v2

    sub-double v2, v4, v2

    .line 227552
    const-wide v4, 0x3f947ae147ae147bL    # 0.02

    cmpl-double v0, v2, v4

    if-lez v0, :cond_2

    .line 227553
    invoke-direct {p0, v2, v3}, LX/1Hk;->a(D)V

    .line 227554
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(J)J
    .locals 21

    .prologue
    .line 227362
    const-wide/16 v4, 0x0

    .line 227363
    move-object/from16 v0, p0

    iget-object v8, v0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v8

    .line 227364
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->r:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v10

    .line 227365
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2}, LX/1Hh;->e()Ljava/util/Collection;

    move-result-object v7

    .line 227366
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v2}, LX/1Hl;->c()J

    move-result-wide v12

    .line 227367
    const/4 v6, 0x0

    .line 227368
    const-wide/16 v2, 0x0

    .line 227369
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    move-wide/from16 v18, v2

    move v3, v6

    move-wide v6, v4

    move-wide/from16 v4, v18

    :goto_0
    :try_start_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35g;

    .line 227370
    const-wide/16 v14, 0x1

    invoke-virtual {v2}, LX/35g;->b()J

    move-result-wide v16

    sub-long v16, v10, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v14

    .line 227371
    cmp-long v16, v14, p1

    if-ltz v16, :cond_1

    .line 227372
    move-object/from16 v0, p0

    iget-object v14, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v14, v2}, LX/1Hh;->a(LX/35g;)J

    move-result-wide v14

    .line 227373
    move-object/from16 v0, p0

    iget-object v0, v0, LX/1Hk;->a:Ljava/util/Set;

    move-object/from16 v16, v0

    invoke-virtual {v2}, LX/35g;->a()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 227374
    const-wide/16 v16, 0x0

    cmp-long v16, v14, v16

    if-lez v16, :cond_0

    .line 227375
    add-int/lit8 v3, v3, 0x1

    .line 227376
    add-long/2addr v4, v14

    .line 227377
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v16

    invoke-virtual {v2}, LX/35g;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/1gB;->a(Ljava/lang/String;)LX/1gB;

    move-result-object v2

    sget-object v16, LX/37E;->CONTENT_STALE:LX/37E;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/1gB;->a(LX/37E;)LX/1gB;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, LX/1gB;->a(J)LX/1gB;

    move-result-object v2

    sub-long v14, v12, v4

    invoke-virtual {v2, v14, v15}, LX/1gB;->b(J)LX/1gB;

    move-result-object v2

    .line 227378
    move-object/from16 v0, p0

    iget-object v14, v0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v14, v2}, LX/1GE;->g(LX/1gC;)V

    .line 227379
    invoke-virtual {v2}, LX/1gB;->i()V

    :cond_0
    move-wide/from16 v18, v4

    move v4, v3

    move-wide/from16 v2, v18

    move-wide/from16 v18, v2

    move v3, v4

    move-wide/from16 v4, v18

    .line 227380
    goto :goto_0

    .line 227381
    :cond_1
    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_0

    .line 227382
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2}, LX/1Hh;->c()V

    .line 227383
    if-lez v3, :cond_3

    .line 227384
    invoke-static/range {p0 .. p0}, LX/1Hk;->g(LX/1Hk;)Z

    .line 227385
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Hk;->q:LX/1Hl;

    neg-long v4, v4

    neg-int v3, v3

    int-to-long v10, v3

    invoke-virtual {v2, v4, v5, v10, v11}, LX/1Hl;->b(JJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227386
    :cond_3
    :goto_1
    :try_start_2
    monitor-exit v8

    .line 227387
    return-wide v6

    .line 227388
    :catch_0
    move-exception v2

    .line 227389
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1Hk;->o:LX/1GQ;

    sget-object v6, LX/43W;->EVICTION:LX/43W;

    sget-object v7, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "clearOldEntries: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v6, v7, v9, v2}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v6, v4

    goto :goto_1

    .line 227390
    :catchall_0
    move-exception v2

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 227391
    :catch_1
    move-exception v2

    move-wide v4, v6

    goto :goto_2
.end method

.method public final a(LX/1bh;)LX/1gI;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 227431
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v0

    .line 227432
    iput-object p1, v0, LX/1gB;->d:LX/1bh;

    .line 227433
    move-object v4, v0

    .line 227434
    :try_start_0
    iget-object v5, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 227435
    :try_start_1
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v6

    .line 227436
    const/4 v0, 0x0

    move v3, v0

    move-object v2, v1

    move-object v0, v1

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 227437
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227438
    iput-object v0, v4, LX/1gB;->e:Ljava/lang/String;

    .line 227439
    iget-object v2, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2, v0, p1}, LX/1Hh;->b(Ljava/lang/String;Ljava/lang/Object;)LX/1gI;

    move-result-object v2

    .line 227440
    if-nez v2, :cond_0

    .line 227441
    add-int/lit8 v3, v3, 0x1

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_0

    :cond_0
    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    .line 227442
    :cond_1
    if-nez v0, :cond_2

    .line 227443
    iget-object v3, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v3, v4}, LX/1GE;->b(LX/1gC;)V

    .line 227444
    iget-object v3, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 227445
    :goto_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227446
    invoke-virtual {v4}, LX/1gB;->i()V

    :goto_2
    return-object v0

    .line 227447
    :cond_2
    :try_start_2
    iget-object v3, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v3, v4}, LX/1GE;->a(LX/1gC;)V

    .line 227448
    iget-object v3, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 227449
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 227450
    :catch_0
    move-exception v0

    .line 227451
    :try_start_4
    iget-object v2, p0, LX/1Hk;->o:LX/1GQ;

    sget-object v3, LX/43W;->GENERIC_IO:LX/43W;

    sget-object v5, LX/1Hk;->b:Ljava/lang/Class;

    const-string v6, "getResource"

    invoke-interface {v2, v3, v5, v6, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227452
    iput-object v0, v4, LX/1gB;->i:Ljava/io/IOException;

    .line 227453
    iget-object v0, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v0, v4}, LX/1GE;->e(LX/1gC;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 227454
    invoke-virtual {v4}, LX/1gB;->i()V

    move-object v0, v1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v4}, LX/1gB;->i()V

    throw v0
.end method

.method public final a(LX/1bh;LX/2uu;)LX/1gI;
    .locals 7

    .prologue
    .line 227455
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v0

    .line 227456
    iput-object p1, v0, LX/1gB;->d:LX/1bh;

    .line 227457
    move-object v1, v0

    .line 227458
    iget-object v0, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v0, v1}, LX/1GE;->c(LX/1gC;)V

    .line 227459
    iget-object v2, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v2

    .line 227460
    :try_start_0
    invoke-static {p1}, LX/1gD;->b(LX/1bh;)Ljava/lang/String;

    move-result-object v0

    .line 227461
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227462
    iput-object v0, v1, LX/1gB;->e:Ljava/lang/String;

    .line 227463
    :try_start_1
    invoke-static {p0}, LX/1Hk;->e(LX/1Hk;)V

    .line 227464
    iget-object v2, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v2, v0, p1}, LX/1Hh;->a(Ljava/lang/String;Ljava/lang/Object;)LX/32a;

    move-result-object v2

    move-object v2, v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 227465
    :try_start_2
    invoke-virtual {v2, p2}, LX/32a;->a(LX/2uu;)V

    .line 227466
    invoke-direct {p0, v2, p1, v0}, LX/1Hk;->a(LX/32a;LX/1bh;Ljava/lang/String;)LX/1gI;

    move-result-object v0

    .line 227467
    invoke-interface {v0}, LX/1gI;->c()J

    move-result-wide v4

    .line 227468
    iput-wide v4, v1, LX/1gB;->f:J

    .line 227469
    move-object v3, v1

    .line 227470
    iget-object v4, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v4}, LX/1Hl;->c()J

    move-result-wide v4

    .line 227471
    iput-wide v4, v3, LX/1gB;->h:J

    .line 227472
    iget-object v3, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v3, v1}, LX/1GE;->d(LX/1gC;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 227473
    :try_start_3
    invoke-virtual {v2}, LX/32a;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 227474
    sget-object v2, LX/1Hk;->b:Ljava/lang/Class;

    const-string v3, "Failed to delete temp file"

    invoke-static {v2, v3}, LX/03J;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 227475
    :cond_0
    invoke-virtual {v1}, LX/1gB;->i()V

    return-object v0

    .line 227476
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 227477
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-virtual {v2}, LX/32a;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 227478
    sget-object v2, LX/1Hk;->b:Ljava/lang/Class;

    const-string v3, "Failed to delete temp file"

    invoke-static {v2, v3}, LX/03J;->b(Ljava/lang/Class;Ljava/lang/String;)V

    :cond_1
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 227479
    :catch_0
    move-exception v0

    .line 227480
    :try_start_6
    iput-object v0, v1, LX/1gB;->i:Ljava/io/IOException;

    .line 227481
    iget-object v2, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v2, v1}, LX/1GE;->f(LX/1gC;)V

    .line 227482
    sget-object v2, LX/1Hk;->b:Ljava/lang/Class;

    const-string v3, "Failed inserting a file into the cache"

    invoke-static {v2, v3, v0}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227483
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 227484
    :catchall_2
    move-exception v0

    invoke-virtual {v1}, LX/1gB;->i()V

    throw v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 227485
    invoke-virtual {p0}, LX/1Hk;->d()V

    .line 227486
    return-void
.end method

.method public final b(LX/1bh;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 227487
    const/4 v1, 0x0

    .line 227488
    :try_start_0
    iget-object v4, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227489
    :try_start_1
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v5

    move v3, v2

    .line 227490
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 227491
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227492
    :try_start_2
    iget-object v1, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v1, v0, p1}, LX/1Hh;->d(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227493
    iget-object v1, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 227494
    const/4 v1, 0x1

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v1

    .line 227495
    :goto_1
    return v0

    .line 227496
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 227497
    :cond_1
    :try_start_3
    monitor-exit v4

    move v0, v2

    goto :goto_1

    .line 227498
    :catchall_0
    move-exception v0

    :goto_2
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 227499
    :catch_0
    move-exception v0

    .line 227500
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v3

    .line 227501
    iput-object p1, v3, LX/1gB;->d:LX/1bh;

    .line 227502
    move-object v3, v3

    .line 227503
    iput-object v1, v3, LX/1gB;->e:Ljava/lang/String;

    .line 227504
    move-object v1, v3

    .line 227505
    iput-object v0, v1, LX/1gB;->i:Ljava/io/IOException;

    .line 227506
    move-object v0, v1

    .line 227507
    iget-object v1, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v1, v0}, LX/1GE;->e(LX/1gC;)V

    .line 227508
    invoke-virtual {v0}, LX/1gB;->i()V

    move v0, v2

    .line 227509
    goto :goto_1

    .line 227510
    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 227511
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(LX/1bh;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 227512
    iget-object v3, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v3

    .line 227513
    :try_start_0
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v4

    move v2, v1

    .line 227514
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 227515
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227516
    iget-object v5, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227517
    const/4 v0, 0x1

    monitor-exit v3

    .line 227518
    :goto_1
    return v0

    .line 227519
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 227520
    :cond_1
    monitor-exit v3

    move v0, v1

    goto :goto_1

    .line 227521
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 227522
    iget-object v1, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 227523
    :try_start_0
    iget-object v0, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v0}, LX/1Hh;->d()V

    .line 227524
    iget-object v0, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 227525
    iget-object v0, p0, LX/1Hk;->i:LX/1GE;

    invoke-interface {v0}, LX/1GE;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227526
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/1Hk;->q:LX/1Hl;

    invoke-virtual {v0}, LX/1Hl;->b()V

    .line 227527
    monitor-exit v1

    return-void

    .line 227528
    :catch_0
    move-exception v0

    .line 227529
    iget-object v2, p0, LX/1Hk;->o:LX/1GQ;

    sget-object v3, LX/43W;->EVICTION:LX/43W;

    sget-object v4, LX/1Hk;->b:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "clearAll: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 227530
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final d(LX/1bh;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 227531
    iget-object v4, p0, LX/1Hk;->s:Ljava/lang/Object;

    monitor-enter v4

    .line 227532
    :try_start_0
    invoke-virtual {p0, p1}, LX/1Hk;->c(LX/1bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227533
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 227534
    :goto_0
    return v0

    .line 227535
    :cond_0
    :try_start_1
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v5

    move v3, v2

    .line 227536
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 227537
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227538
    iget-object v6, p0, LX/1Hk;->m:LX/1Hh;

    invoke-interface {v6, v0, p1}, LX/1Hh;->c(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 227539
    iget-object v3, p0, LX/1Hk;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227540
    :try_start_2
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 227541
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 227542
    :cond_2
    monitor-exit v4

    move v0, v2

    goto :goto_0

    .line 227543
    :catch_0
    monitor-exit v4

    move v0, v2

    goto :goto_0

    .line 227544
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
