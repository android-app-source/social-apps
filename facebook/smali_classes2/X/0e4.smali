.class public final LX/0e4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1qx;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1qx;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 91437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91438
    iput-object p1, p0, LX/0e4;->a:LX/0QB;

    .line 91439
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 91440
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0e4;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 91441
    packed-switch p2, :pswitch_data_0

    .line 91442
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91443
    :pswitch_0
    new-instance p0, LX/1qy;

    invoke-static {p1}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v0, v1}, LX/1qy;-><init>(LX/0yc;Lcom/facebook/content/SecureContextHelper;)V

    .line 91444
    move-object v0, p0

    .line 91445
    :goto_0
    return-object v0

    .line 91446
    :pswitch_1
    new-instance v1, LX/1t8;

    const/16 v0, 0x14d1

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 91447
    new-instance v0, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    new-instance p2, LX/1qh;

    invoke-direct {p2, p1}, LX/1qh;-><init>(LX/0QB;)V

    invoke-direct {v0, p0, p2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object p0, v0

    .line 91448
    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v2, p0, v0}, LX/1t8;-><init>(LX/0Or;Ljava/util/Set;LX/03V;)V

    .line 91449
    move-object v0, v1

    .line 91450
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 91451
    const/4 v0, 0x2

    return v0
.end method
