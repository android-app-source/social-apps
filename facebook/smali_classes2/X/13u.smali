.class public LX/13u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile E:LX/13u;

.field private static final a:LX/0Tn;

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field private static final d:LX/0Tn;

.field private static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;

.field private static final i:LX/0Tn;

.field private static final j:LX/0Tn;


# instance fields
.field private A:J

.field private B:J

.field private C:J

.field private D:J

.field private final k:LX/0kb;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Vw;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0q5;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final o:LX/0So;

.field private p:J

.field private q:J

.field private r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:J

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 177724
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "data_analytics"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 177725
    sput-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_mqtt_bytes_received_foreground"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->b:LX/0Tn;

    .line 177726
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_mqtt_bytes_received_background"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->c:LX/0Tn;

    .line 177727
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_mqtt_bytes_sent_foreground"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->d:LX/0Tn;

    .line 177728
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_mqtt_bytes_sent_background"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->e:LX/0Tn;

    .line 177729
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_bytes_received_foreground"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->f:LX/0Tn;

    .line 177730
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_bytes_received_background"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->g:LX/0Tn;

    .line 177731
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_bytes_sent_foreground"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->h:LX/0Tn;

    .line 177732
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "total_bytes_sent_background"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->i:LX/0Tn;

    .line 177733
    sget-object v0, LX/13u;->a:LX/0Tn;

    const-string v1, "last_data_usage_fetch_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/13u;->j:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0kb;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0Vw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0q5;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177735
    iput-object p1, p0, LX/13u;->k:LX/0kb;

    .line 177736
    iput-object p2, p0, LX/13u;->l:LX/0Ot;

    .line 177737
    iput-object p3, p0, LX/13u;->m:LX/0Ot;

    .line 177738
    iput-object p5, p0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 177739
    iput-object p6, p0, LX/13u;->o:LX/0So;

    .line 177740
    new-instance v0, Lcom/facebook/analytics/util/AnalyticsConnectionUtils$1;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/util/AnalyticsConnectionUtils$1;-><init>(LX/13u;)V

    const v1, -0x23f613d4

    invoke-static {p4, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 177741
    return-void
.end method

.method public static a(LX/0QB;)LX/13u;
    .locals 10

    .prologue
    .line 177742
    sget-object v0, LX/13u;->E:LX/13u;

    if-nez v0, :cond_1

    .line 177743
    const-class v1, LX/13u;

    monitor-enter v1

    .line 177744
    :try_start_0
    sget-object v0, LX/13u;->E:LX/13u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177745
    if-eqz v2, :cond_0

    .line 177746
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177747
    new-instance v3, LX/13u;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    const/16 v5, 0x8e

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x4bc

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-direct/range {v3 .. v9}, LX/13u;-><init>(LX/0kb;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;)V

    .line 177748
    move-object v0, v3

    .line 177749
    sput-object v0, LX/13u;->E:LX/13u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177752
    :cond_1
    sget-object v0, LX/13u;->E:LX/13u;

    return-object v0

    .line 177753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/13u;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 177760
    if-eqz p3, :cond_2

    .line 177761
    invoke-virtual {p3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    .line 177762
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "connection"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "none"

    :cond_0
    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177763
    invoke-virtual {p3}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    .line 177764
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 177765
    iget-object v1, p0, LX/13u;->k:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177766
    const-string v0, "HOTSPOT"

    .line 177767
    :cond_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 177768
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "connection_subtype"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177769
    :cond_2
    return-void
.end method

.method private static a(LX/13u;Ljava/lang/String;JJJ)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 177755
    sub-long v0, p2, p4

    div-long/2addr v0, p6

    .line 177756
    cmp-long v2, p2, v4

    if-ltz v2, :cond_0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x1f400000

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 177757
    :cond_0
    const-wide/16 v0, -0x1

    move-wide v2, v0

    .line 177758
    :goto_0
    iget-object v0, p0, LX/13u;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vw;

    invoke-virtual {v0, p1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177759
    return-void

    :cond_1
    move-wide v2, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;JJ)V
    .locals 8

    .prologue
    .line 177668
    const-wide/16 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v7}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177669
    return-void
.end method

.method public static a$redex0(LX/13u;)V
    .locals 10

    .prologue
    const/16 v4, 0x22b8

    const/4 v6, 0x0

    .line 177670
    iget-object v0, p0, LX/13u;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q5;

    .line 177671
    iget-boolean v1, v0, LX/0q5;->a:Z

    move v1, v1

    .line 177672
    if-eqz v1, :cond_0

    .line 177673
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    .line 177674
    invoke-virtual {v0, v1, v6}, LX/0q5;->a(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v2

    .line 177675
    invoke-virtual {v0, v1, v4}, LX/0q5;->b(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v3

    .line 177676
    invoke-virtual {v0, v1, v4}, LX/0q5;->c(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v4

    .line 177677
    invoke-virtual {v0, v1, v6}, LX/0q5;->b(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v5

    .line 177678
    invoke-virtual {v0, v1, v6}, LX/0q5;->c(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v0

    .line 177679
    iget-wide v8, v2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v6, v8

    .line 177680
    iput-wide v6, p0, LX/13u;->p:J

    .line 177681
    iget-wide v8, v2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v6, v8

    .line 177682
    iput-wide v6, p0, LX/13u;->q:J

    .line 177683
    iget-wide v8, v4, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v6, v8

    .line 177684
    iput-wide v6, p0, LX/13u;->w:J

    .line 177685
    iget-wide v8, v3, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v6, v8

    .line 177686
    iput-wide v6, p0, LX/13u;->v:J

    .line 177687
    iget-wide v8, v4, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v6, v8

    .line 177688
    iput-wide v6, p0, LX/13u;->y:J

    .line 177689
    iget-wide v8, v3, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v2, v8

    .line 177690
    iput-wide v2, p0, LX/13u;->x:J

    .line 177691
    iget-wide v8, v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v2, v8

    .line 177692
    iput-wide v2, p0, LX/13u;->A:J

    .line 177693
    iget-wide v8, v5, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v2, v8

    .line 177694
    iput-wide v2, p0, LX/13u;->z:J

    .line 177695
    iget-wide v8, v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v0, v8

    .line 177696
    iput-wide v0, p0, LX/13u;->C:J

    .line 177697
    iget-wide v8, v5, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v0, v8

    .line 177698
    iput-wide v0, p0, LX/13u;->B:J

    .line 177699
    :try_start_0
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 177700
    :goto_0
    move-wide v0, v8

    .line 177701
    iput-wide v0, p0, LX/13u;->r:J

    .line 177702
    :try_start_1
    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v8

    .line 177703
    :goto_1
    move-wide v0, v8

    .line 177704
    iput-wide v0, p0, LX/13u;->s:J

    .line 177705
    :try_start_2
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v8

    .line 177706
    :goto_2
    move-wide v0, v8

    .line 177707
    iput-wide v0, p0, LX/13u;->t:J

    .line 177708
    :try_start_3
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-wide v8

    .line 177709
    :goto_3
    move-wide v0, v8

    .line 177710
    iput-wide v0, p0, LX/13u;->u:J

    .line 177711
    :cond_0
    return-void

    .line 177712
    :catch_0
    move-exception v8

    .line 177713
    invoke-static {v8}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    .line 177714
    const-wide/16 v8, 0x0

    goto :goto_0

    .line 177715
    :catch_1
    move-exception v8

    .line 177716
    invoke-static {v8}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    .line 177717
    const-wide/16 v8, 0x0

    goto :goto_1

    .line 177718
    :catch_2
    move-exception v8

    .line 177719
    invoke-static {v8}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    .line 177720
    const-wide/16 v8, 0x0

    goto :goto_2

    .line 177721
    :catch_3
    move-exception v8

    .line 177722
    invoke-static {v8}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    .line 177723
    const-wide/16 v8, 0x0

    goto :goto_3
.end method

.method private static declared-synchronized b(LX/13u;)V
    .locals 18

    .prologue
    .line 177609
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/13u;->p:J

    .line 177610
    move-object/from16 v0, p0

    iget-wide v8, v0, LX/13u;->q:J

    .line 177611
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/13u;->r:J

    .line 177612
    move-object/from16 v0, p0

    iget-wide v12, v0, LX/13u;->s:J

    .line 177613
    move-object/from16 v0, p0

    iget-wide v14, v0, LX/13u;->t:J

    .line 177614
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/13u;->u:J

    move-wide/from16 v16, v0

    .line 177615
    invoke-static/range {p0 .. p0}, LX/13u;->a$redex0(LX/13u;)V

    .line 177616
    const-string v3, "total_bytes_received"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->p:J

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177617
    const-string v3, "total_bytes_sent"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->q:J

    move-object/from16 v2, p0

    move-wide v6, v8

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177618
    const-string v3, "total_device_bytes_received"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->r:J

    move-object/from16 v2, p0

    move-wide v6, v10

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177619
    const-string v3, "total_device_bytes_sent"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->s:J

    move-object/from16 v2, p0

    move-wide v6, v12

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177620
    const-string v3, "total_mobile_bytes_received"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->t:J

    move-object/from16 v2, p0

    move-wide v6, v14

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177621
    const-string v3, "total_mobile_bytes_sent"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->u:J

    move-object/from16 v2, p0

    move-wide/from16 v6, v16

    invoke-direct/range {v2 .. v7}, LX/13u;->a(Ljava/lang/String;JJ)V

    .line 177622
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Vw;

    .line 177623
    const-string v3, "device_bytes_received_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->r:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177624
    const-string v3, "device_bytes_sent_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->s:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177625
    const-string v3, "mobile_device_bytes_received_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->t:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177626
    const-string v3, "mobile_device_bytes_sent_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->u:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177627
    const-string v3, "app_bytes_received_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->p:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 177628
    const-string v3, "app_bytes_sent_since_boot"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->q:J

    invoke-virtual {v2, v3, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177629
    monitor-exit p0

    return-void

    .line 177630
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private static c(LX/13u;)V
    .locals 24

    .prologue
    .line 177631
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->f:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    .line 177632
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->g:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v12

    .line 177633
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->h:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v14

    .line 177634
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->i:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v16

    .line 177635
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->b:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 177636
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->c:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v18

    .line 177637
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->d:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v20

    .line 177638
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->e:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v22

    .line 177639
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/13u;->j:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 177640
    move-object/from16 v0, p0

    iget-object v4, v0, LX/13u;->o:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, LX/13u;->D:J

    .line 177641
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->D:J

    sub-long v2, v4, v2

    .line 177642
    const-wide/32 v4, 0x36ee80

    div-long v8, v2, v4

    .line 177643
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-nez v2, :cond_0

    .line 177644
    const-wide/16 v8, 0x1

    .line 177645
    :cond_0
    const-string v3, "total_mqtt_bytes_received_foreground"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->v:J

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177646
    const-string v3, "total_mqtt_bytes_received_background"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->w:J

    move-object/from16 v2, p0

    move-wide/from16 v6, v18

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177647
    const-string v3, "total_mqtt_bytes_sent_foreground"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->x:J

    move-object/from16 v2, p0

    move-wide/from16 v6, v20

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177648
    const-string v3, "total_mqtt_bytes_sent_background"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->y:J

    move-object/from16 v2, p0

    move-wide/from16 v6, v22

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177649
    const-string v3, "total_bytes_received_foreground"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->z:J

    move-object/from16 v2, p0

    move-wide v6, v10

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177650
    const-string v3, "total_bytes_received_background"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->A:J

    move-object/from16 v2, p0

    move-wide v6, v12

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177651
    const-string v3, "total_bytes_sent_foreground"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->B:J

    move-object/from16 v2, p0

    move-wide v6, v14

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177652
    const-string v3, "total_bytes_sent_background"

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->C:J

    move-object/from16 v2, p0

    move-wide/from16 v6, v16

    invoke-static/range {v2 .. v9}, LX/13u;->a(LX/13u;Ljava/lang/String;JJJ)V

    .line 177653
    move-object/from16 v0, p0

    iget-object v2, v0, LX/13u;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->f:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->z:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->g:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->A:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->h:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->B:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->i:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->C:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->b:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->v:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->c:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->w:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->d:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->x:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->e:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->y:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/13u;->j:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/13u;->D:J

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 177654
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 177655
    const-string v0, ""

    iget-object v1, p0, LX/13u;->k:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, LX/13u;->a(LX/13u;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/NetworkInfo;)V

    .line 177656
    return-void
.end method

.method public final declared-synchronized b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 177657
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/13u;->b(LX/13u;)V

    .line 177658
    iget-object v0, p0, LX/13u;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vw;

    invoke-virtual {v0, p1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 177659
    invoke-virtual {p0, p1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177660
    monitor-exit p0

    return-void

    .line 177661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 177662
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/13u;->b(LX/13u;)V

    .line 177663
    invoke-static {p0}, LX/13u;->c(LX/13u;)V

    .line 177664
    iget-object v0, p0, LX/13u;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vw;

    invoke-virtual {v0, p1}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 177665
    invoke-virtual {p0, p1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177666
    monitor-exit p0

    return-void

    .line 177667
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
