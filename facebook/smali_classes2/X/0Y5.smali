.class public LX/0Y5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y4;


# instance fields
.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80448
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LX/0Y5;->a:Ljava/lang/ThreadLocal;

    .line 80449
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 80450
    iput-object v0, p0, LX/0Y5;->b:LX/0Ot;

    .line 80451
    return-void
.end method

.method private a(LX/0Pn;J)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 80428
    iget-object v0, p1, LX/0Pn;->s:Ljava/util/ArrayList;

    move-object v2, v0

    .line 80429
    if-eqz v2, :cond_3

    .line 80430
    iget-object v0, p0, LX/0Y5;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    .line 80431
    if-nez v0, :cond_4

    .line 80432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80433
    iget-object v3, p0, LX/0Y5;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v3, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    move-object v4, v0

    .line 80434
    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 80435
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 80436
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80437
    if-eqz v0, :cond_2

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v3, v5

    const/16 v5, 0x400

    if-gt v3, v5, :cond_2

    .line 80438
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 80439
    if-eqz v1, :cond_0

    .line 80440
    const-string v3, ","

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80441
    :cond_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80442
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 80443
    :cond_2
    const/4 v1, 0x4

    const/16 v2, 0x41

    .line 80444
    iget v0, p1, LX/0Pn;->g:I

    move v3, v0

    .line 80445
    const-string v6, "tags"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-wide v4, p2

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;)I

    .line 80446
    :cond_3
    return-void

    :cond_4
    move-object v4, v0

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 2

    .prologue
    .line 80426
    shr-int/lit8 v0, p0, 0x10

    const v1, 0xffff

    and-int/2addr v0, v1

    .line 80427
    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0x70

    if-eq v0, v1, :cond_0

    const/16 v1, 0x86

    if-eq v0, v1, :cond_0

    const/16 v1, 0x88

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0Pn;J)V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v2, 0x42

    const/4 v1, 0x4

    const/4 v0, 0x0

    .line 80409
    iget-object v3, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    move-object v11, v3

    .line 80410
    if-nez v11, :cond_1

    .line 80411
    :cond_0
    return-void

    :cond_1
    move v3, v0

    .line 80412
    :goto_0
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    .line 80413
    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 80414
    add-int/lit8 v4, v0, 0x1

    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 80415
    if-eqz v6, :cond_0

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, v3

    const/16 v5, 0x800

    if-gt v4, v5, :cond_0

    .line 80416
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    const-string v4, "loom_id"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 80417
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    add-int v10, v3, v4

    .line 80418
    iget v3, p0, LX/0Pn;->g:I

    move v3, v3

    .line 80419
    invoke-static {v3}, LX/0Y5;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 80420
    invoke-virtual {p0}, LX/0Pn;->d()J

    move-result-wide v4

    const-wide/32 v8, 0xf4240

    mul-long/2addr v8, v4

    .line 80421
    iget v3, p0, LX/0Pn;->g:I

    move v3, v3

    .line 80422
    move-wide v4, p1

    invoke-static/range {v1 .. v9}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;J)I

    move v3, v10

    .line 80423
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 80424
    :cond_3
    iget v3, p0, LX/0Pn;->g:I

    move v3, v3

    .line 80425
    move-wide v4, p1

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;)I

    move v3, v10

    goto :goto_1
.end method

.method private static f(LX/0Pn;)J
    .locals 4

    .prologue
    .line 80406
    invoke-static {p0}, LX/0Y5;->g(LX/0Pn;)J

    move-result-wide v0

    .line 80407
    iget-short v2, p0, LX/0Pn;->t:S

    move v2, v2

    .line 80408
    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static g(LX/0Pn;)J
    .locals 4

    .prologue
    .line 80404
    iget v0, p0, LX/0Pn;->b:I

    move v0, v0

    .line 80405
    int-to-long v0, v0

    const/16 v2, 0x10

    shl-long/2addr v0, v2

    const-wide v2, 0xffffffff0000L

    and-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(LX/0Pn;)V
    .locals 14

    .prologue
    const/16 v2, 0x37

    const/4 v1, 0x4

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 80337
    sget-object v4, LX/02r;->b:LX/02r;

    move-object v6, v4

    .line 80338
    if-nez v6, :cond_0

    .line 80339
    :goto_0
    return-void

    .line 80340
    :cond_0
    iget v4, p1, LX/0Pn;->g:I

    move v7, v4

    .line 80341
    invoke-virtual {v6, v0, v3, p1, v7}, LX/02r;->a(IILjava/lang/Object;I)Z

    move-result v8

    .line 80342
    invoke-virtual {v6}, LX/02r;->d()Ljava/lang/String;

    move-result-object v4

    .line 80343
    const-string v5, "AAAAAAAAAAA"

    if-eq v4, v5, :cond_1

    .line 80344
    const-string v5, "loom_id"

    invoke-virtual {p1, v5, v4}, LX/0Pn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80345
    :cond_1
    invoke-static {p1}, LX/0Y5;->g(LX/0Pn;)J

    move-result-wide v4

    .line 80346
    iget-boolean v9, p1, LX/0Pn;->n:Z

    move v9, v9

    .line 80347
    if-nez v9, :cond_2

    .line 80348
    const-wide/high16 v10, 0x1000000000000L

    or-long/2addr v4, v10

    .line 80349
    :cond_2
    if-nez v8, :cond_5

    .line 80350
    iget-object v9, v6, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/037;

    .line 80351
    if-eqz v9, :cond_7

    iget v9, v9, LX/037;->c:I

    :goto_1
    move v9, v9

    .line 80352
    and-int/lit8 v9, v9, 0x8

    if-eqz v9, :cond_5

    invoke-virtual {v6, v7}, LX/02r;->a(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 80353
    :goto_2
    if-nez v8, :cond_3

    if-eqz v0, :cond_4

    .line 80354
    :cond_3
    const-wide/high16 v8, 0x2000000000000L

    or-long/2addr v4, v8

    .line 80355
    :cond_4
    invoke-direct {p0, p1, v4, v5}, LX/0Y5;->a(LX/0Pn;J)V

    .line 80356
    invoke-static {p1, v4, v5}, LX/0Y5;->b(LX/0Pn;J)V

    .line 80357
    invoke-static {v7}, LX/0Y5;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 80358
    iget-wide v12, p1, LX/0Pn;->d:J

    move-wide v6, v12

    .line 80359
    const-wide/32 v8, 0xf4240

    mul-long/2addr v6, v8

    .line 80360
    iget v0, p1, LX/0Pn;->g:I

    move v3, v0

    .line 80361
    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJJ)I

    goto :goto_0

    :cond_5
    move v0, v3

    .line 80362
    goto :goto_2

    .line 80363
    :cond_6
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80364
    invoke-static {v1, v2, v0, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    goto :goto_0

    :cond_7
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public final b(LX/0Pn;)V
    .locals 0

    .prologue
    .line 80402
    invoke-virtual {p0, p1}, LX/0Y5;->a(LX/0Pn;)V

    .line 80403
    return-void
.end method

.method public final c(LX/0Pn;)V
    .locals 12

    .prologue
    const/16 v2, 0x38

    const/4 v1, 0x4

    .line 80380
    iget-boolean v0, p1, LX/0Pn;->v:Z

    move v0, v0

    .line 80381
    if-eqz v0, :cond_1

    .line 80382
    iget-object v0, p0, LX/0Y5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->k()LX/03R;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_1

    .line 80383
    invoke-virtual {p0, p1}, LX/0Y5;->d(LX/0Pn;)V

    .line 80384
    :cond_0
    :goto_0
    return-void

    .line 80385
    :cond_1
    invoke-static {p1}, LX/0Y5;->f(LX/0Pn;)J

    move-result-wide v4

    .line 80386
    iget-boolean v0, p1, LX/0Pn;->n:Z

    move v0, v0

    .line 80387
    if-nez v0, :cond_2

    .line 80388
    const-wide/high16 v6, 0x1000000000000L

    or-long/2addr v4, v6

    .line 80389
    :cond_2
    invoke-direct {p0, p1, v4, v5}, LX/0Y5;->a(LX/0Pn;J)V

    .line 80390
    invoke-static {p1, v4, v5}, LX/0Y5;->b(LX/0Pn;J)V

    .line 80391
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80392
    invoke-static {v0}, LX/0Y5;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80393
    iget-wide v10, p1, LX/0Pn;->u:J

    move-wide v6, v10

    .line 80394
    const-wide/32 v8, 0xf4240

    mul-long/2addr v6, v8

    .line 80395
    iget v0, p1, LX/0Pn;->g:I

    move v3, v0

    .line 80396
    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJJ)I

    .line 80397
    :goto_1
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 80398
    if-eqz v0, :cond_0

    .line 80399
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/02r;->a(ILjava/lang/Object;I)V

    goto :goto_0

    .line 80400
    :cond_3
    iget v0, p1, LX/0Pn;->g:I

    move v0, v0

    .line 80401
    invoke-static {v1, v2, v0, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    goto :goto_1
.end method

.method public final d(LX/0Pn;)V
    .locals 5

    .prologue
    .line 80369
    invoke-static {p1}, LX/0Y5;->g(LX/0Pn;)J

    move-result-wide v0

    .line 80370
    iget-boolean v2, p1, LX/0Pn;->n:Z

    move v2, v2

    .line 80371
    if-nez v2, :cond_0

    .line 80372
    const-wide/high16 v2, 0x1000000000000L

    or-long/2addr v0, v2

    .line 80373
    :cond_0
    const/4 v2, 0x4

    const/16 v3, 0x39

    .line 80374
    iget v4, p1, LX/0Pn;->g:I

    move v4, v4

    .line 80375
    invoke-static {v2, v3, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 80376
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 80377
    if-nez v0, :cond_1

    .line 80378
    :goto_0
    return-void

    .line 80379
    :cond_1
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/02r;->b(ILjava/lang/Object;I)V

    goto :goto_0
.end method

.method public final e(LX/0Pn;)V
    .locals 6

    .prologue
    .line 80365
    const/4 v0, 0x4

    const/16 v1, 0x3a

    .line 80366
    iget v2, p1, LX/0Pn;->g:I

    move v2, v2

    .line 80367
    invoke-static {p1}, LX/0Y5;->f(LX/0Pn;)J

    move-result-wide v4

    invoke-static {v0, v1, v2, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 80368
    return-void
.end method
