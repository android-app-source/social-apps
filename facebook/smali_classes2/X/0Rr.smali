.class public abstract LX/0Rr;
.super LX/0Rc;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rc",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Rs;

.field public b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60478
    invoke-direct {p0}, LX/0Rc;-><init>()V

    .line 60479
    sget-object v0, LX/0Rs;->NOT_READY:LX/0Rs;

    iput-object v0, p0, LX/0Rr;->a:LX/0Rs;

    .line 60480
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60481
    sget-object v0, LX/0Rs;->DONE:LX/0Rs;

    iput-object v0, p0, LX/0Rr;->a:LX/0Rs;

    .line 60482
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60483
    iget-object v0, p0, LX/0Rr;->a:LX/0Rs;

    sget-object v3, LX/0Rs;->FAILED:LX/0Rs;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 60484
    sget-object v0, LX/0Rw;->a:[I

    iget-object v3, p0, LX/0Rr;->a:LX/0Rs;

    invoke-virtual {v3}, LX/0Rs;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 60485
    sget-object v0, LX/0Rs;->FAILED:LX/0Rs;

    iput-object v0, p0, LX/0Rr;->a:LX/0Rs;

    .line 60486
    invoke-virtual {p0}, LX/0Rr;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0Rr;->b:Ljava/lang/Object;

    .line 60487
    iget-object v0, p0, LX/0Rr;->a:LX/0Rs;

    sget-object v1, LX/0Rs;->DONE:LX/0Rs;

    if-eq v0, v1, :cond_1

    .line 60488
    sget-object v0, LX/0Rs;->READY:LX/0Rs;

    iput-object v0, p0, LX/0Rr;->a:LX/0Rs;

    .line 60489
    const/4 v0, 0x1

    .line 60490
    :goto_1
    move v2, v0

    .line 60491
    :goto_2
    :pswitch_0
    return v2

    :cond_0
    move v0, v2

    .line 60492
    goto :goto_0

    :pswitch_1
    move v2, v1

    .line 60493
    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60494
    invoke-virtual {p0}, LX/0Rr;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60495
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 60496
    :cond_0
    sget-object v0, LX/0Rs;->NOT_READY:LX/0Rs;

    iput-object v0, p0, LX/0Rr;->a:LX/0Rs;

    .line 60497
    iget-object v0, p0, LX/0Rr;->b:Ljava/lang/Object;

    .line 60498
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Rr;->b:Ljava/lang/Object;

    .line 60499
    return-object v0
.end method
