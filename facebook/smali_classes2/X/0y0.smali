.class public final LX/0y0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/apptab/state/TabTag;

.field public final synthetic b:LX/0gx;


# direct methods
.method public constructor <init>(LX/0gx;Lcom/facebook/apptab/state/TabTag;)V
    .locals 0

    .prologue
    .line 163833
    iput-object p1, p0, LX/0y0;->b:LX/0gx;

    iput-object p2, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x55354

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 163834
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    iget-object v0, v0, LX/0gx;->v:LX/0fd;

    .line 163835
    iget-object v2, v0, LX/0fd;->e:LX/0gg;

    .line 163836
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/0gg;->t:Z

    .line 163837
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    iget-object v0, v0, LX/0gx;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v2, "tap_top_jewel_bar"

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 163838
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 163839
    iget-object v2, p0, LX/0y0;->b:LX/0gx;

    iget-object v3, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0gx;->b(Ljava/lang/String;)I

    move-result v2

    .line 163840
    iget-object v3, p0, LX/0y0;->b:LX/0gx;

    iget-object v3, v3, LX/0gx;->v:LX/0fd;

    invoke-virtual {v3, v2}, LX/0fd;->c(I)Z

    .line 163841
    iget-object v2, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    .line 163842
    sget-object v3, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    if-ne v2, v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 163843
    if-nez v2, :cond_0

    .line 163844
    iget-object v2, p0, LX/0y0;->b:LX/0gx;

    iget-object v2, v2, LX/0gx;->v:LX/0fd;

    iget-object v3, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    .line 163845
    iget-object p1, v2, LX/0fd;->b:LX/0fG;

    invoke-interface {p1, v3}, LX/0fG;->b(Ljava/lang/String;)V

    .line 163846
    :cond_0
    iget-object v2, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163847
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    iget-object v0, v0, LX/0gx;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18q;

    iget-object v2, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    .line 163848
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string p1, "com.facebook.apptab.ui.TAB_BAR_ITEM_TAP"

    invoke-virtual {v3, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string p1, "tab_bar_tap"

    invoke-virtual {v3, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 163849
    iget-object p1, v0, LX/18q;->a:LX/0Xl;

    invoke-interface {p1, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 163850
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    iget-object v0, v0, LX/0gx;->v:LX/0fd;

    .line 163851
    iget-object v2, v0, LX/0fd;->b:LX/0fG;

    invoke-interface {v2}, LX/0fG;->C()V

    .line 163852
    :cond_1
    :goto_1
    const v0, -0x4caebf45

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 163853
    :cond_2
    iget-object v0, p0, LX/0y0;->a:Lcom/facebook/apptab/state/TabTag;

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163854
    iget-object v0, p0, LX/0y0;->b:LX/0gx;

    iget-object v0, v0, LX/0gx;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iS;

    invoke-virtual {v0}, LX/0iS;->a()V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
