.class public final LX/1iL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;


# instance fields
.field public final synthetic a:LX/16j;

.field private final b:I

.field private final c:LX/2BD;

.field private d:LX/1pB;


# direct methods
.method public constructor <init>(LX/16j;ILX/2BD;)V
    .locals 1
    .param p3    # LX/2BD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298054
    iput-object p1, p0, LX/1iL;->a:LX/16j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298055
    iput p2, p0, LX/1iL;->b:I

    .line 298056
    iput-object p3, p0, LX/1iL;->c:LX/2BD;

    .line 298057
    iget-object v0, p0, LX/1iL;->c:LX/2BD;

    if-eqz v0, :cond_0

    .line 298058
    invoke-interface {p3}, LX/2BD;->a()V

    .line 298059
    :cond_0
    return-void
.end method

.method private a(LX/1hi;LX/1pB;)V
    .locals 4

    .prologue
    .line 298048
    invoke-virtual {p1, p2}, LX/1hi;->a(LX/1pB;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 298049
    invoke-virtual {p1}, LX/1hi;->e()I

    move-result v1

    .line 298050
    :try_start_0
    iget-object v2, p0, LX/1iL;->a:LX/16j;

    iget-object v2, v2, LX/16j;->f:LX/0TU;

    new-instance v3, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;

    invoke-direct {v3, p0, p1, v1, v0}, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;-><init>(LX/1iL;LX/1hi;ILorg/apache/http/HttpResponse;)V

    const v0, 0x6d885d65

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298051
    :goto_0
    return-void

    .line 298052
    :catch_0
    move-exception v0

    .line 298053
    iget-object v2, p0, LX/1iL;->a:LX/16j;

    invoke-virtual {p1, v1, v0, v2}, LX/1hi;->a(ILjava/lang/Exception;LX/16j;)V

    goto :goto_0
.end method


# virtual methods
.method public final onBody(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 297987
    :try_start_0
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 297988
    if-eqz v0, :cond_0

    .line 297989
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "on body "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 297990
    iget-object v2, v0, LX/1hi;->a:Ljava/lang/String;

    move-object v2, v2

    .line 297991
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297992
    iget-object v1, v0, LX/1hi;->o:LX/1iN;

    .line 297993
    const/16 v0, 0xa

    invoke-static {v1, v0}, LX/1iN;->a(LX/1iN;B)V

    .line 297994
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    .line 297995
    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v2

    .line 297996
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/1hi;->i()LX/1iM;

    move-result-object v2

    :goto_0
    move-object v0, v2

    .line 297997
    if-eqz v0, :cond_0

    .line 297998
    invoke-virtual {v0, p1}, LX/1iM;->a(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297999
    const/4 p1, 0x0

    .line 298000
    :cond_0
    if-eqz p1, :cond_1

    .line 298001
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-interface {v0, p1}, LX/1AI;->releaseBodyBuffer(Ljava/nio/ByteBuffer;)V

    .line 298002
    :cond_1
    :goto_1
    return-void

    .line 298003
    :catch_0
    move-exception v0

    .line 298004
    :try_start_1
    const-string v1, "TigonHttpClientAdapter"

    const-string v2, "Can\'t write to the body buffer(%d)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, LX/1iL;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298005
    if-eqz p1, :cond_1

    .line 298006
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-interface {v0, p1}, LX/1AI;->releaseBodyBuffer(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    .line 298007
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_2

    .line 298008
    iget-object v1, p0, LX/1iL;->a:LX/16j;

    iget-object v1, v1, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-interface {v1, p1}, LX/1AI;->releaseBodyBuffer(Ljava/nio/ByteBuffer;)V

    .line 298009
    :cond_2
    throw v0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onEOM(LX/1pK;)V
    .locals 2

    .prologue
    .line 298042
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298043
    if-eqz v0, :cond_1

    .line 298044
    iget-object v1, p0, LX/1iL;->d:LX/1pB;

    if-eqz v1, :cond_0

    .line 298045
    iget-object v1, p0, LX/1iL;->d:LX/1pB;

    invoke-direct {p0, v0, v1}, LX/1iL;->a(LX/1hi;LX/1pB;)V

    .line 298046
    :cond_0
    iget-object v1, p0, LX/1iL;->a:LX/16j;

    invoke-virtual {v0, v1, p1}, LX/1hi;->a(LX/16j;LX/1pK;)V

    .line 298047
    :cond_1
    return-void
.end method

.method public final onError(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V
    .locals 3

    .prologue
    .line 298060
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298061
    if-eqz v0, :cond_0

    .line 298062
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/facebook/tigon/tigonapi/TigonError;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/tigon/tigonapi/TigonError;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298063
    iget-object v1, p0, LX/1iL;->a:LX/16j;

    invoke-virtual {v0, v1, p1, p2}, LX/1hi;->a(LX/16j;Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V

    .line 298064
    :cond_0
    return-void
.end method

.method public final onResponse(LX/1pB;)V
    .locals 7

    .prologue
    .line 298026
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298027
    if-nez v0, :cond_0

    .line 298028
    :goto_0
    return-void

    .line 298029
    :cond_0
    invoke-virtual {v0}, LX/1hi;->a()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 298030
    invoke-virtual {v0}, LX/1hi;->e()I

    .line 298031
    invoke-virtual {v0}, LX/1hi;->j()V

    .line 298032
    iget v1, p1, LX/1pB;->a:I

    const/4 v2, 0x0

    .line 298033
    const/16 v3, 0x12c

    if-ge v1, v3, :cond_3

    .line 298034
    :cond_1
    :goto_1
    move v1, v2

    .line 298035
    if-eqz v1, :cond_2

    .line 298036
    iput-object p1, p0, LX/1iL;->d:LX/1pB;

    goto :goto_0

    .line 298037
    :cond_2
    invoke-direct {p0, v0, p1}, LX/1iL;->a(LX/1hi;LX/1pB;)V

    goto :goto_0

    .line 298038
    :cond_3
    sget-object v4, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->a:[I

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_1

    aget v6, v4, v3

    .line 298039
    if-ne v1, v6, :cond_4

    .line 298040
    const/4 v2, 0x1

    goto :goto_1

    .line 298041
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public final onStarted(Lcom/facebook/tigon/iface/TigonRequest;)V
    .locals 2

    .prologue
    .line 298022
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298023
    if-eqz v0, :cond_0

    .line 298024
    invoke-virtual {v0, p1}, LX/1hi;->a(Lcom/facebook/tigon/iface/TigonRequest;)V

    .line 298025
    :cond_0
    return-void
.end method

.method public final onUploadProgress(JJ)V
    .locals 3

    .prologue
    .line 298015
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298016
    if-nez v0, :cond_1

    .line 298017
    :cond_0
    :goto_0
    return-void

    .line 298018
    :cond_1
    iget-object v1, v0, LX/1hi;->o:LX/1iN;

    .line 298019
    const/16 v0, 0xb

    invoke-static {v1, v0}, LX/1iN;->a(LX/1iN;B)V

    .line 298020
    iget-object v0, p0, LX/1iL;->c:LX/2BD;

    if-eqz v0, :cond_0

    .line 298021
    iget-object v0, p0, LX/1iL;->c:LX/2BD;

    invoke-interface {v0, p1, p2}, LX/2BD;->a(J)V

    goto :goto_0
.end method

.method public final onWillRetry(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V
    .locals 2

    .prologue
    .line 298010
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iL;->d:LX/1pB;

    .line 298011
    iget-object v0, p0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->d:LX/1hh;

    iget v1, p0, LX/1iL;->b:I

    invoke-virtual {v0, v1}, LX/1hh;->c(I)LX/1hi;

    move-result-object v0

    .line 298012
    if-eqz v0, :cond_0

    .line 298013
    invoke-virtual {v0, p1, p2}, LX/1hi;->a(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V

    .line 298014
    :cond_0
    return-void
.end method
