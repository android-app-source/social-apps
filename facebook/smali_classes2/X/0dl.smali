.class public LX/0dl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0dn;

.field public final b:LX/0dm;

.field public final c:Landroid/content/Context;

.field public final d:LX/03V;


# direct methods
.method public constructor <init>(LX/0dm;LX/0dn;Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90815
    iput-object p2, p0, LX/0dl;->a:LX/0dn;

    .line 90816
    iput-object p1, p0, LX/0dl;->b:LX/0dm;

    .line 90817
    iput-object p3, p0, LX/0dl;->c:Landroid/content/Context;

    .line 90818
    iput-object p4, p0, LX/0dl;->d:LX/03V;

    .line 90819
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/facebook/java2js/JSContext;Ljava/lang/Boolean;)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 90820
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "number_format_config"

    const-string v2, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90821
    const v0, 0x7f070041

    invoke-static {p0, v0}, LX/0dl;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/java2js/JSValue;->makeString(Lcom/facebook/java2js/JSContext;Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 90822
    :goto_0
    return-object v0

    .line 90823
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 90824
    invoke-static {v0}, Ljava/text/DecimalFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 90825
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    .line 90826
    invoke-static {p1}, Lcom/facebook/java2js/JSValue;->makeObject(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    .line 90827
    const-string v3, "decimalSeparator"

    invoke-virtual {v2}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/facebook/java2js/JSValue;->makeString(Lcom/facebook/java2js/JSContext;Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 90828
    const-string v3, "numberDelimiter"

    invoke-virtual {v2}, Ljava/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/facebook/java2js/JSValue;->makeString(Lcom/facebook/java2js/JSContext;Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 90829
    const-string v2, "minDigitsForThousandsSeparator"

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getGroupingSize()I

    move-result v0

    int-to-double v4, v0

    invoke-static {p1, v4, v5}, Lcom/facebook/java2js/JSValue;->makeNumber(Lcom/facebook/java2js/JSContext;D)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v1

    .line 90830
    goto :goto_0
.end method

.method public static b(LX/0dl;)Ljava/io/File;
    .locals 6

    .prologue
    .line 90831
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/0dl;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "componentscript"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 90832
    new-instance v1, LX/5JD;

    invoke-direct {v1}, LX/5JD;-><init>()V

    move-object v1, v1

    .line 90833
    iget-object v2, p0, LX/0dl;->c:Landroid/content/Context;

    .line 90834
    iput-object v2, v1, LX/5JD;->a:Landroid/content/Context;

    .line 90835
    move-object v1, v1

    .line 90836
    iput-object v0, v1, LX/5JD;->b:Ljava/io/File;

    .line 90837
    move-object v1, v1

    .line 90838
    const-string v2, "ComponentScriptBundle.js.meta"

    .line 90839
    iget-object v3, v1, LX/5JD;->c:Ljava/util/ArrayList;

    new-instance p0, LX/5JF;

    invoke-direct {p0, v2, v2}, LX/5JF;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90840
    move-object v3, v1

    .line 90841
    move-object v1, v3

    .line 90842
    const-string v2, "ComponentScriptBundle.js.bytecode"

    .line 90843
    iget-object v3, v1, LX/5JD;->c:Ljava/util/ArrayList;

    new-instance p0, LX/5JG;

    invoke-direct {p0, v2, v2}, LX/5JG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90844
    move-object v3, v1

    .line 90845
    move-object v1, v3

    .line 90846
    iget-object v2, v1, LX/5JD;->b:Ljava/io/File;

    invoke-static {v2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90847
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, v1, LX/5JD;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 90848
    iget-object v2, v1, LX/5JD;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5JE;

    iget-object v4, v1, LX/5JD;->b:Ljava/io/File;

    .line 90849
    new-instance v5, Ljava/io/File;

    iget-object p0, v2, LX/5JE;->c:Ljava/lang/String;

    invoke-direct {v5, v4, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v5, v2, LX/5JE;->b:Ljava/io/File;

    .line 90850
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 90851
    :cond_0
    new-instance v2, LX/5JH;

    invoke-direct {v2, v1}, LX/5JH;-><init>(LX/5JD;)V

    move-object v1, v2

    .line 90852
    invoke-virtual {v1}, LX/5JH;->a()Z

    .line 90853
    new-instance v1, Ljava/io/File;

    const-string v2, "ComponentScriptBundle.js.bytecode"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 90854
    const/4 v1, 0x0

    .line 90855
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 90856
    new-instance v0, Ljava/lang/String;

    invoke-static {v1}, LX/0aP;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90857
    if-eqz v1, :cond_0

    .line 90858
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 90859
    :cond_0
    :goto_0
    return-object v0

    .line 90860
    :catch_0
    move-exception v0

    .line 90861
    :goto_1
    :try_start_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Could not read localized JSON file from resources"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 90862
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 90863
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 90864
    :cond_1
    :goto_2
    throw v0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_2

    .line 90865
    :catch_3
    move-exception v0

    goto :goto_1
.end method
