.class public LX/1qN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static volatile h:LX/1qN;


# instance fields
.field public final d:LX/1qR;

.field public final e:LX/0Sh;

.field private final f:LX/03V;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 330566
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/1qO;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, LX/1qN;->a:[Ljava/lang/String;

    .line 330567
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/1qP;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, LX/1qN;->b:[Ljava/lang/String;

    .line 330568
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/1qQ;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sget-object v1, LX/1qQ;->b:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, LX/1qQ;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/1qQ;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/1qN;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1qR;LX/0Sh;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 330569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330570
    iput-object p1, p0, LX/1qN;->d:LX/1qR;

    .line 330571
    iput-object p2, p0, LX/1qN;->e:LX/0Sh;

    .line 330572
    iput-object p3, p0, LX/1qN;->f:LX/03V;

    .line 330573
    iput-object p4, p0, LX/1qN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 330574
    return-void
.end method

.method public static a(LX/0QB;)LX/1qN;
    .locals 7

    .prologue
    .line 330575
    sget-object v0, LX/1qN;->h:LX/1qN;

    if-nez v0, :cond_1

    .line 330576
    const-class v1, LX/1qN;

    monitor-enter v1

    .line 330577
    :try_start_0
    sget-object v0, LX/1qN;->h:LX/1qN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 330578
    if-eqz v2, :cond_0

    .line 330579
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 330580
    new-instance p0, LX/1qN;

    invoke-static {v0}, LX/1qR;->a(LX/0QB;)LX/1qR;

    move-result-object v3

    check-cast v3, LX/1qR;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1qN;-><init>(LX/1qR;LX/0Sh;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 330581
    move-object v0, p0

    .line 330582
    sput-object v0, LX/1qN;->h:LX/1qN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330583
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 330584
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330585
    :cond_1
    sget-object v0, LX/1qN;->h:LX/1qN;

    return-object v0

    .line 330586
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 330587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 330588
    iget-object v0, p0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 330589
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 330590
    const-string v1, "privacy_options"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 330591
    iget-object v1, p0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/1qN;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 330592
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330593
    sget-object v0, LX/1qO;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 330594
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 330595
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 330596
    :goto_0
    return-object v3

    .line 330597
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 330598
    iget-object v0, p0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 330599
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 330600
    const-string v1, "drafts"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 330601
    iget-object v1, p0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/1qN;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 330602
    if-nez v1, :cond_0

    .line 330603
    :goto_0
    return-object v3

    .line 330604
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330605
    sget-object v0, LX/1qP;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 330606
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 330607
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 330608
    iget-object v0, p0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 330609
    iget-object v0, p0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "drafts"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 330610
    return-void
.end method

.method public final d()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7m3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 330611
    iget-object v0, p0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 330612
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 330613
    const-string v1, "pending_story"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 330614
    iget-object v1, p0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/1qN;->c:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 330615
    sget-object v0, LX/1qQ;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 330616
    sget-object v2, LX/1qQ;->c:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 330617
    sget-object v3, LX/1qQ;->b:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 330618
    sget-object v4, LX/1qQ;->d:LX/0U1;

    invoke-virtual {v4, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 330619
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 330620
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 330621
    new-instance v6, LX/7m3;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v7, v8, v9, v10}, LX/7m3;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330622
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330623
    :catch_0
    move-exception v0

    .line 330624
    :try_start_1
    iget-object v2, p0, LX/1qN;->f:LX/03V;

    const-string v3, "composer_db_load_pending_stories_failed"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330625
    if-eqz v1, :cond_0

    .line 330626
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 330627
    :cond_0
    :goto_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 330628
    :cond_1
    if-eqz v1, :cond_0

    .line 330629
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 330630
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 330631
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
