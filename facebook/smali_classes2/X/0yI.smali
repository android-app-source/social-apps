.class public LX/0yI;
.super Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;
.source ""

# interfaces
.implements LX/0yL;
.implements LX/0dc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile A:LX/0yI;

.field private static final q:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Uh;

.field public final v:LX/0yQ;

.field private final w:LX/0yR;

.field public final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1p4;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0W3;

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164386
    const-class v0, LX/0yI;

    sput-object v0, LX/0yI;->q:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0yN;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0yP;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;LX/0yQ;LX/0Uh;LX/0yR;LX/0W3;)V
    .locals 17
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/IsZeroRatingAvailable;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsUserEligibleForDialtone;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/UseBackupRewriteRulesGatekeeper;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/UseSessionlessBackupRewriteRules;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0yN;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2gy;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p4;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p0;",
            ">;",
            "LX/0yQ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0yR;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164387
    new-instance v15, LX/0yS;

    move-object/from16 v0, p17

    invoke-direct {v15, v0}, LX/0yS;-><init>(LX/0Or;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p12

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v16, p18

    invoke-direct/range {v2 .. v16}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;-><init>(LX/0Ot;LX/0yN;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0yP;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;)V

    .line 164388
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/0yI;->p:I

    .line 164389
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, LX/0yI;->z:I

    .line 164390
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->r:LX/0Ot;

    .line 164391
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->s:LX/0Or;

    .line 164392
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->t:LX/0Or;

    .line 164393
    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->u:LX/0Uh;

    .line 164394
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->v:LX/0yQ;

    .line 164395
    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->w:LX/0yR;

    .line 164396
    invoke-direct/range {p0 .. p0}, LX/0yI;->s()V

    .line 164397
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->y:LX/0W3;

    .line 164398
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0yI;->x:LX/0Ot;

    .line 164399
    return-void
.end method

.method public static b(LX/0QB;)LX/0yI;
    .locals 3

    .prologue
    .line 164400
    sget-object v0, LX/0yI;->A:LX/0yI;

    if-nez v0, :cond_1

    .line 164401
    const-class v1, LX/0yI;

    monitor-enter v1

    .line 164402
    :try_start_0
    sget-object v0, LX/0yI;->A:LX/0yI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164403
    if-eqz v2, :cond_0

    .line 164404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0yI;->c(LX/0QB;)LX/0yI;

    move-result-object v0

    sput-object v0, LX/0yI;->A:LX/0yI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164407
    :cond_1
    sget-object v0, LX/0yI;->A:LX/0yI;

    return-object v0

    .line 164408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(LX/0QB;)LX/0yI;
    .locals 25

    .prologue
    .line 164410
    new-instance v2, LX/0yI;

    const/16 v3, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/0yM;->a(LX/0QB;)LX/0yM;

    move-result-object v4

    check-cast v4, LX/0yN;

    const/16 v5, 0x13ec

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1ce

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x13eb

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x13e2

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x13ee

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x387

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x14d1

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x17d

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1483

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v14

    check-cast v14, LX/0yP;

    const/16 v15, 0x1489

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0x15b3

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x15b4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x13de

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x13c1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const/16 v20, 0x13cc

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0yQ;->a(LX/0QB;)LX/0yQ;

    move-result-object v21

    check-cast v21, LX/0yQ;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0yR;->a(LX/0QB;)LX/0yR;

    move-result-object v23

    check-cast v23, LX/0yR;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v24

    check-cast v24, LX/0W3;

    invoke-direct/range {v2 .. v24}, LX/0yI;-><init>(LX/0Ot;LX/0yN;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0yP;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;LX/0yQ;LX/0Uh;LX/0yR;LX/0W3;)V

    .line 164411
    return-object v2
.end method

.method private c(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 164412
    const/4 v0, 0x0

    iput v0, p0, LX/0yI;->p:I

    .line 164413
    if-eqz p1, :cond_0

    .line 164414
    iget v0, p0, LX/0yI;->z:I

    iget-object v1, p0, LX/0yI;->y:LX/0W3;

    sget-wide v2, LX/0X5;->ik:J

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, LX/0yI;->z:I

    .line 164415
    :goto_0
    return-void

    .line 164416
    :cond_0
    iget-object v0, p0, LX/0yI;->y:LX/0W3;

    sget-wide v2, LX/0X5;->il:J

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0yI;->z:I

    goto :goto_0
.end method

.method private s()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 164417
    iget-object v0, p0, LX/0yI;->w:LX/0yR;

    new-instance v1, LX/0yT;

    invoke-direct {v1, p0}, LX/0yT;-><init>(LX/0yI;)V

    .line 164418
    iget-object p0, v0, LX/0yR;->a:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164419
    return-void
.end method

.method private t()Z
    .locals 3

    .prologue
    .line 164420
    iget-object v0, p0, LX/0yI;->u:LX/0Uh;

    const/16 v1, 0x688

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0yi;)V
    .locals 1

    .prologue
    .line 164421
    invoke-direct {p0}, LX/0yI;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164422
    invoke-super {p0, p1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;)V

    .line 164423
    :cond_0
    return-void
.end method

.method public final a(LX/0yi;LX/32P;)V
    .locals 2

    .prologue
    .line 164424
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    .line 164425
    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164426
    :goto_0
    return-void

    .line 164427
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_1

    .line 164428
    const-string v0, "disabled"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 164429
    :cond_1
    iget-object v0, p0, LX/0yI;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164430
    sget-object v0, LX/0yi;->DIALTONE:LX/0yi;

    invoke-super {p0, v0, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    .line 164431
    sget-object v0, LX/0yi;->NORMAL:LX/0yi;

    invoke-super {p0, v0, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    goto :goto_0

    .line 164432
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 8

    .prologue
    .line 164364
    invoke-super {p0, p1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 164365
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->f()LX/0Px;

    move-result-object v0

    .line 164366
    iget-object v1, p0, LX/0yI;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1p4;

    const/4 v3, 0x0

    .line 164367
    iget-object v2, v1, LX/1p4;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 164368
    invoke-static {v1}, LX/1p4;->a(LX/1p4;)V

    .line 164369
    :goto_0
    return-void

    .line 164370
    :cond_0
    iget-object v2, v1, LX/1p4;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1p6;

    .line 164371
    iget-object v4, v2, LX/1p6;->j:LX/04q;

    iget-object v4, v4, LX/04q;->a:Ljava/lang/String;

    move-object v5, v4

    .line 164372
    iget-object v2, v1, LX/1p4;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1p6;

    invoke-virtual {v2}, LX/1p6;->d()Ljava/lang/String;

    move-result-object v6

    .line 164373
    new-instance v7, Landroid/content/Intent;

    const-string v2, "com.facebook.rti.mqtt.ACTION_ZR_SWITCH"

    invoke-direct {v7, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164374
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    move v4, v3

    :goto_1
    if-ge v4, p1, :cond_3

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 164375
    invoke-virtual {v2, v6}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 164376
    invoke-virtual {v2, v6}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164377
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 164378
    invoke-static {v1}, LX/1p4;->b(LX/1p4;)V

    .line 164379
    const-string v3, "extra_mqtt_endpoint"

    invoke-virtual {v7, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164380
    iget-object v2, v1, LX/1p4;->c:LX/0Xl;

    invoke-interface {v2, v7}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 164381
    goto :goto_0

    .line 164382
    :cond_1
    goto :goto_0

    .line 164383
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 164384
    :cond_3
    invoke-static {v1}, LX/1p4;->a(LX/1p4;)V

    .line 164385
    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164433
    invoke-direct {p0}, LX/0yI;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164434
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 164435
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164436
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 164437
    :pswitch_0
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v6, "-1"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 164438
    const-string v0, "disabled"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    .line 164439
    :goto_2
    move v0, v3

    .line 164440
    if-eqz v0, :cond_3

    .line 164441
    :cond_2
    :goto_3
    return-void

    .line 164442
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0yI;->b(Z)V

    .line 164443
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 164444
    const-string v0, "enabled"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 164445
    :sswitch_0
    const-string v6, "X-ZERO-CARRIER-ID"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v1, v3

    goto :goto_1

    :sswitch_1
    const-string v6, "X-ZERO-FAST-HASH"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v1, v4

    goto :goto_1

    :sswitch_2
    const-string v6, "X-ZERO-TOKEN-REFRESH"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    .line 164446
    :cond_4
    iget-object v6, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getCarrierIdKey()Ljava/lang/String;

    move-result-object v1

    const-string v7, ""

    invoke-interface {v6, v1, v7}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164447
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v4

    .line 164448
    goto/16 :goto_0

    .line 164449
    :pswitch_1
    iget-object v6, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getTokenFastHashKey()Ljava/lang/String;

    move-result-object v1

    const-string v7, ""

    invoke-interface {v6, v1, v7}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164450
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v4

    .line 164451
    goto/16 :goto_0

    :pswitch_2
    move v3, v4

    .line 164452
    goto/16 :goto_2

    .line 164453
    :cond_5
    if-eqz v2, :cond_6

    .line 164454
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    sget-object v1, LX/32P;->HEADER_PARAM_MISMATCH:LX/32P;

    invoke-virtual {p0, v0, v1}, LX/0yI;->a(LX/0yi;LX/32P;)V

    :cond_6
    move v3, v2

    .line 164455
    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x4dd7d0f -> :sswitch_1
        0x17ecc500 -> :sswitch_0
        0x439b5bf7 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()LX/0Rf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164358
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    invoke-virtual {v0}, LX/0yP;->a()V

    .line 164359
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 164360
    invoke-static {}, LX/0yh;->values()[LX/0yh;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 164361
    invoke-virtual {v4}, LX/0yh;->getClearablePreferencesRoot()LX/0Tn;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164363
    :cond_0
    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 164354
    invoke-direct {p0}, LX/0yI;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164355
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0yI;->c(Z)V

    .line 164356
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->b(Z)V

    .line 164357
    return-void
.end method

.method public final e()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164310
    invoke-super {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->e()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final e_(Z)V
    .locals 3

    .prologue
    .line 164342
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    invoke-virtual {v0}, LX/0yP;->a()V

    .line 164343
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164344
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164345
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164346
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d()V

    .line 164347
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j()V

    .line 164348
    if-eqz p1, :cond_0

    .line 164349
    sget-object v0, LX/0yi;->NORMAL:LX/0yi;

    .line 164350
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getCampaignIdKey()Ljava/lang/String;

    move-result-object v2

    const-string p1, ""

    invoke-interface {v1, v2, p1}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164351
    invoke-static {v1}, Lcom/facebook/zero/sdk/token/ZeroToken;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164352
    sget-object v1, LX/32P;->PREFETCH:LX/32P;

    invoke-virtual {p0, v0, v1}, LX/0yI;->a(LX/0yi;LX/32P;)V

    .line 164353
    :cond_0
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 164341
    return-void
.end method

.method public final k()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164338
    invoke-super {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k()Ljava/util/Map;

    move-result-object v0

    .line 164339
    const-string v1, "is_dialtone_enabled"

    iget-object v2, p0, LX/0yI;->s:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164340
    return-object v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 164334
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v0

    sget-object v1, LX/7X5;->b:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1p2;->a(Ljava/lang/String;)LX/1p2;

    move-result-object v0

    invoke-interface {v0}, LX/1p2;->a()V

    .line 164335
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    invoke-virtual {v0}, LX/0yP;->a()V

    .line 164336
    invoke-super {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->l()V

    .line 164337
    return-void
.end method

.method public final n()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 164327
    invoke-super {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n()V

    .line 164328
    invoke-direct {p0}, LX/0yI;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164329
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j()V

    .line 164330
    :cond_0
    :goto_0
    return-void

    .line 164331
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164332
    iget-object v0, p0, LX/0yI;->v:LX/0yQ;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yQ;->a(Ljava/lang/Boolean;)V

    .line 164333
    goto :goto_0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 164317
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    .line 164318
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getRegistrationStatusKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "unknown"

    invoke-interface {v1, v2, v3}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 164319
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "unknown"

    invoke-interface {v1, v2, v3}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164320
    iget-object v2, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getUnregisteredReasonKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "unavailable"

    invoke-interface {v2, v0, v3}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164321
    iget-object v0, p0, LX/0yI;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_0

    const-string v0, "unknown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "unavailable"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164322
    :cond_1
    :goto_0
    return-void

    .line 164323
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 164324
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.facebook.zero.ZERO_RATING_STATE_UNREGISTERED_REASON"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164325
    const-string v0, "unregistered_reason"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164326
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final p()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 164311
    invoke-direct {p0}, LX/0yI;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164312
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164313
    :cond_0
    :goto_0
    return-void

    .line 164314
    :cond_1
    iget v0, p0, LX/0yI;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0yI;->p:I

    iget v1, p0, LX/0yI;->z:I

    if-lt v0, v1, :cond_0

    .line 164315
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/0yI;->c(Z)V

    .line 164316
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    sget-object v1, LX/32P;->HEADER_ERROR_FORCE_FETCH:LX/32P;

    invoke-virtual {p0, v0, v1}, LX/0yI;->a(LX/0yi;LX/32P;)V

    goto :goto_0
.end method
