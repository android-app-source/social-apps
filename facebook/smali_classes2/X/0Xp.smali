.class public LX/0Xp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:LX/0Xp;


# instance fields
.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/BroadcastReceiver;",
            "Ljava/util/List",
            "<",
            "Landroid/content/IntentFilter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/0a3;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Ljava/lang/Long;",
            "LX/1r9;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/0a2;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/Context;

.field private final h:LX/0Xw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79507
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0Xp;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 79565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79566
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0Xp;->c:Ljava/util/Map;

    .line 79567
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0Xp;->d:Ljava/util/Map;

    .line 79568
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/0Xp;->e:LX/0Xu;

    .line 79569
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0Xp;->f:Ljava/util/Map;

    .line 79570
    iput-object p1, p0, LX/0Xp;->g:Landroid/content/Context;

    .line 79571
    invoke-static {p1}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v0

    iput-object v0, p0, LX/0Xp;->h:LX/0Xw;

    .line 79572
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/0Xp;
    .locals 3

    .prologue
    .line 79560
    sget-object v1, LX/0Xp;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 79561
    :try_start_0
    sget-object v0, LX/0Xp;->b:LX/0Xp;

    if-nez v0, :cond_0

    .line 79562
    new-instance v0, LX/0Xp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0Xp;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/0Xp;->b:LX/0Xp;

    .line 79563
    :cond_0
    sget-object v0, LX/0Xp;->b:LX/0Xp;

    monitor-exit v1

    return-object v0

    .line 79564
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/0Xp;Landroid/content/Intent;LX/0Xu;)Ljava/util/Set;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "LX/0Xu",
            "<",
            "Ljava/lang/Long;",
            "LX/1r9;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79529
    move-object/from16 v0, p0

    iget-object v14, v0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v14

    .line 79530
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 79531
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Xp;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v4

    .line 79532
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 79533
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 79534
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v7

    .line 79535
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    move-result v2

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    move v13, v2

    .line 79536
    :goto_0
    const/4 v11, 0x0

    .line 79537
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Xp;->d:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/List;

    move-object v9, v0

    .line 79538
    if-eqz v9, :cond_2

    .line 79539
    const/4 v2, 0x0

    move v12, v2

    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-ge v12, v2, :cond_2

    .line 79540
    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/0a3;

    move-object v10, v0

    .line 79541
    iget-boolean v2, v10, LX/0a3;->e:Z

    if-eqz v2, :cond_1

    .line 79542
    if-eqz v13, :cond_7

    move-object v2, v11

    .line 79543
    :goto_2
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    move-object v11, v2

    goto :goto_1

    .line 79544
    :cond_0
    const/4 v2, 0x0

    move v13, v2

    goto :goto_0

    .line 79545
    :cond_1
    iget-object v2, v10, LX/0a3;->a:Landroid/content/IntentFilter;

    const-string v8, "LocalBroadcastManager"

    invoke-virtual/range {v2 .. v8}, Landroid/content/IntentFilter;->match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I

    move-result v2

    .line 79546
    if-ltz v2, :cond_7

    .line 79547
    if-nez v11, :cond_6

    .line 79548
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v2

    .line 79549
    :goto_3
    iget-wide v0, v10, LX/0a3;->c:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v2, v8, v10}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 79550
    const/4 v8, 0x1

    iput-boolean v8, v10, LX/0a3;->e:Z

    goto :goto_2

    .line 79551
    :catchall_0
    move-exception v2

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 79552
    :cond_2
    if-eqz v11, :cond_5

    .line 79553
    :try_start_1
    invoke-interface {v11}, LX/0Xu;->i()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0a3;

    .line 79554
    const/4 v4, 0x0

    iput-boolean v4, v2, LX/0a3;->e:Z

    goto :goto_4

    .line 79555
    :cond_3
    invoke-interface {v11}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 79556
    invoke-interface {v11, v2}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v4

    .line 79557
    new-instance v5, LX/1r9;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v4}, LX/1r9;-><init>(Landroid/content/Intent;Ljava/util/Collection;)V

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v5}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_5

    .line 79558
    :cond_4
    invoke-interface {v11}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    monitor-exit v14

    .line 79559
    :goto_6
    return-object v2

    :cond_5
    const/4 v2, 0x0

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    :cond_6
    move-object v2, v11

    goto :goto_3

    :cond_7
    move-object v2, v11

    goto :goto_2
.end method

.method public static a$redex0(LX/0Xp;J)V
    .locals 7

    .prologue
    .line 79508
    :goto_0
    iget-object v1, p0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v1

    .line 79509
    :try_start_0
    iget-object v0, p0, LX/0Xp;->e:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->f()I

    move-result v0

    if-gtz v0, :cond_0

    .line 79510
    monitor-exit v1

    .line 79511
    :goto_1
    return-void

    .line 79512
    :cond_0
    iget-object v0, p0, LX/0Xp;->e:LX/0Xu;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 79513
    if-nez v0, :cond_1

    .line 79514
    monitor-exit v1

    goto :goto_1

    .line 79515
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 79516
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    .line 79517
    if-gtz v2, :cond_2

    .line 79518
    monitor-exit v1

    goto :goto_1

    .line 79519
    :cond_2
    new-array v2, v2, [LX/1r9;

    .line 79520
    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 79521
    iget-object v0, p0, LX/0Xp;->e:LX/0Xu;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0Xu;->d(Ljava/lang/Object;)Ljava/util/Collection;

    .line 79522
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79523
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v2

    if-ge v1, v0, :cond_4

    .line 79524
    aget-object v3, v2, v1

    .line 79525
    iget-object v0, v3, LX/1r9;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0a3;

    .line 79526
    iget-object v0, v0, LX/0a3;->b:Landroid/content/BroadcastReceiver;

    iget-object v5, p0, LX/0Xp;->g:Landroid/content/Context;

    iget-object v6, v3, LX/1r9;->a:Landroid/content/Intent;

    invoke-virtual {v0, v5, v6}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_3

    .line 79527
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 79528
    :cond_4
    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 79573
    iget-object v2, p0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v2

    .line 79574
    :try_start_0
    iget-object v0, p0, LX/0Xp;->e:LX/0Xu;

    invoke-static {p0, p1, v0}, LX/0Xp;->a(LX/0Xp;Landroid/content/Intent;LX/0Xu;)Ljava/util/Set;

    move-result-object v0

    .line 79575
    if-eqz v0, :cond_2

    .line 79576
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 79577
    iget-object v4, p0, LX/0Xp;->f:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 79578
    if-eqz v0, :cond_0

    .line 79579
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 79580
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 79581
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 79582
    :cond_1
    :try_start_1
    monitor-exit v2

    move v0, v1

    .line 79583
    :goto_1
    return v0

    .line 79584
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79585
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/BroadcastReceiver;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 79449
    iget-object v8, p0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v8

    .line 79450
    :try_start_0
    iget-object v0, p0, LX/0Xp;->h:LX/0Xw;

    invoke-virtual {v0, p1}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 79451
    iget-object v0, p0, LX/0Xp;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 79452
    if-nez v0, :cond_0

    .line 79453
    monitor-exit v8

    .line 79454
    :goto_0
    return-void

    :cond_0
    move v7, v5

    .line 79455
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v7, v1, :cond_5

    .line 79456
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/IntentFilter;

    move v6, v5

    .line 79457
    :goto_2
    invoke-virtual {v1}, Landroid/content/IntentFilter;->countActions()I

    move-result v2

    if-ge v6, v2, :cond_4

    .line 79458
    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;

    move-result-object v9

    .line 79459
    iget-object v2, p0, LX/0Xp;->d:Ljava/util/Map;

    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 79460
    if-eqz v2, :cond_3

    move v4, v5

    .line 79461
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 79462
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0a3;

    iget-object v3, v3, LX/0a3;->b:Landroid/content/BroadcastReceiver;

    if-ne v3, p1, :cond_6

    .line 79463
    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0a3;

    .line 79464
    iget-object v10, v3, LX/0a3;->d:LX/0a2;

    .line 79465
    iget-object v11, v10, LX/0a2;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v11

    if-gtz v11, :cond_7

    const/4 v11, 0x1

    :goto_4
    move v10, v11

    .line 79466
    if-eqz v10, :cond_1

    .line 79467
    iget-object v10, p0, LX/0Xp;->f:Ljava/util/Map;

    iget-object v3, v3, LX/0a3;->d:LX/0a2;

    iget-wide v12, v3, LX/0a2;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v10, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79468
    :cond_1
    add-int/lit8 v3, v4, -0x1

    .line 79469
    :goto_5
    add-int/lit8 v4, v3, 0x1

    goto :goto_3

    .line 79470
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79471
    iget-object v2, p0, LX/0Xp;->d:Ljava/util/Map;

    invoke-interface {v2, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79472
    :cond_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 79473
    :cond_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 79474
    :cond_5
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_6
    move v3, v4

    goto :goto_5

    :cond_7
    const/4 v11, 0x0

    goto :goto_4
.end method

.method public final a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    .locals 1

    .prologue
    .line 79475
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Looper;)V

    .line 79476
    return-void
.end method

.method public final a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Looper;)V
    .locals 10
    .param p3    # Landroid/os/Looper;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79477
    if-nez p3, :cond_0

    .line 79478
    iget-object v0, p0, LX/0Xp;->h:LX/0Xw;

    invoke-virtual {v0, p1, p2}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 79479
    :goto_0
    return-void

    .line 79480
    :cond_0
    iget-object v7, p0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v7

    .line 79481
    :try_start_0
    invoke-virtual {p3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    move-wide v4, v8

    .line 79482
    iget-object v0, p0, LX/0Xp;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0a2;

    .line 79483
    if-nez v0, :cond_4

    .line 79484
    new-instance v6, LX/0a2;

    invoke-direct {v6, p0, p3, v4, v5}, LX/0a2;-><init>(LX/0Xp;Landroid/os/Looper;J)V

    .line 79485
    iget-object v0, p0, LX/0Xp;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79486
    :goto_1
    iget-object v0, v6, LX/0a2;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 79487
    new-instance v1, LX/0a3;

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, LX/0a3;-><init>(Landroid/content/IntentFilter;Landroid/content/BroadcastReceiver;JLX/0a2;)V

    .line 79488
    iget-object v0, p0, LX/0Xp;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 79489
    if-nez v0, :cond_1

    .line 79490
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 79491
    iget-object v2, p0, LX/0Xp;->c:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79492
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79493
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    invoke-virtual {p2}, Landroid/content/IntentFilter;->countActions()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 79494
    invoke-virtual {p2, v2}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;

    move-result-object v3

    .line 79495
    iget-object v0, p0, LX/0Xp;->d:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 79496
    if-nez v0, :cond_2

    .line 79497
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 79498
    iget-object v4, p0, LX/0Xp;->d:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79499
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79500
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 79501
    :cond_3
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    move-object v6, v0

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 79502
    iget-object v1, p0, LX/0Xp;->c:Ljava/util/Map;

    monitor-enter v1

    .line 79503
    :try_start_0
    iget-object v0, p0, LX/0Xp;->h:LX/0Xw;

    invoke-virtual {v0, p1}, LX/0Xw;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 79504
    invoke-direct {p0, p1}, LX/0Xp;->b(Landroid/content/Intent;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 79505
    monitor-exit v1

    return v0

    .line 79506
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
