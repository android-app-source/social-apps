.class public LX/11A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/11A;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170593
    iput-object p1, p0, LX/11A;->a:LX/0Zb;

    .line 170594
    iput-object p2, p0, LX/11A;->b:LX/0if;

    .line 170595
    return-void
.end method

.method public static a(LX/0QB;)LX/11A;
    .locals 5

    .prologue
    .line 170600
    sget-object v0, LX/11A;->c:LX/11A;

    if-nez v0, :cond_1

    .line 170601
    const-class v1, LX/11A;

    monitor-enter v1

    .line 170602
    :try_start_0
    sget-object v0, LX/11A;->c:LX/11A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 170603
    if-eqz v2, :cond_0

    .line 170604
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 170605
    new-instance p0, LX/11A;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/11A;-><init>(LX/0Zb;LX/0if;)V

    .line 170606
    move-object v0, p0

    .line 170607
    sput-object v0, LX/11A;->c:LX/11A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170608
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 170609
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170610
    :cond_1
    sget-object v0, LX/11A;->c:LX/11A;

    return-object v0

    .line 170611
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 170612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/util/List;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 170596
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 170597
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    .line 170598
    iget-object v0, v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 170599
    :cond_0
    return-object v1
.end method
