.class public LX/0Tj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile n:LX/0Tj;


# instance fields
.field private final a:LX/0Tl;

.field private final b:LX/0So;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:Ljava/lang/Object;

.field public i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile j:Z

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$OnChangesListener;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:J

.field public volatile m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63517
    const-class v0, LX/0Tj;

    sput-object v0, LX/0Tj;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Tl;Ljava/util/Set;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tl;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$OnChangesListener;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63505
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Tj;->d:Ljava/util/Map;

    .line 63506
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0Tj;->e:Ljava/util/Map;

    .line 63507
    new-instance v0, LX/0UE;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0UE;-><init>(I)V

    iput-object v0, p0, LX/0Tj;->f:Ljava/util/Collection;

    .line 63508
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0Tj;->h:Ljava/lang/Object;

    .line 63509
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0Tj;->k:Ljava/util/List;

    .line 63510
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Tj;->l:J

    .line 63511
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Tj;->m:Z

    .line 63512
    iput-object p1, p0, LX/0Tj;->a:LX/0Tl;

    .line 63513
    iget-object v0, p0, LX/0Tj;->k:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 63514
    iput-object p3, p0, LX/0Tj;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 63515
    iput-object p4, p0, LX/0Tj;->b:LX/0So;

    .line 63516
    return-void
.end method

.method public static a(LX/0QB;)LX/0Tj;
    .locals 9

    .prologue
    .line 63484
    sget-object v0, LX/0Tj;->n:LX/0Tj;

    if-nez v0, :cond_1

    .line 63485
    const-class v1, LX/0Tj;

    monitor-enter v1

    .line 63486
    :try_start_0
    sget-object v0, LX/0Tj;->n:LX/0Tj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 63487
    if-eqz v2, :cond_0

    .line 63488
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 63489
    new-instance v6, LX/0Tj;

    .line 63490
    new-instance v5, LX/0Tk;

    invoke-static {v0}, LX/0Tq;->a(LX/0QB;)LX/0Tq;

    move-result-object v3

    check-cast v3, LX/0Tq;

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v4

    check-cast v4, Ljava/util/Random;

    .line 63491
    new-instance v8, LX/0U7;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v8, p0}, LX/0U7;-><init>(LX/0QB;)V

    move-object v8, v8

    .line 63492
    invoke-direct {v5, v3, v7, v4, v8}, LX/0Tk;-><init>(LX/0Tq;LX/0Ot;Ljava/util/Random;LX/0Or;)V

    .line 63493
    move-object v3, v5

    .line 63494
    check-cast v3, LX/0Tl;

    .line 63495
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v7, LX/0U9;

    invoke-direct {v7, v0}, LX/0U9;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v7}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v4

    .line 63496
    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {v6, v3, v7, v4, v5}, LX/0Tj;-><init>(LX/0Tl;Ljava/util/Set;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V

    .line 63497
    move-object v0, v6

    .line 63498
    sput-object v0, LX/0Tj;->n:LX/0Tj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63499
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 63500
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63501
    :cond_1
    sget-object v0, LX/0Tj;->n:LX/0Tj;

    return-object v0

    .line 63502
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 63503
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized c(LX/0Tj;)V
    .locals 5

    .prologue
    .line 63479
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Tj;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0Tj;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 63480
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 63481
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0Tj;->g:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$1;

    invoke-direct {v1, p0}, Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$1;-><init>(LX/0Tj;)V

    iget-wide v2, p0, LX/0Tj;->l:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 63482
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Tj;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static d(LX/0Tj;)V
    .locals 4

    .prologue
    .line 63462
    monitor-enter p0

    .line 63463
    :try_start_0
    iget-object v0, p0, LX/0Tj;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63464
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 63465
    :goto_0
    move-object v0, v0

    .line 63466
    iget-object v1, p0, LX/0Tj;->f:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63467
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    .line 63468
    :goto_1
    move-object v1, v1

    .line 63469
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63470
    iget-object v2, p0, LX/0Tj;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 63471
    :try_start_1
    iget-object v3, p0, LX/0Tj;->a:LX/0Tl;

    invoke-interface {v3, v0, v1}, LX/0Tl;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 63472
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 63473
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 63474
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 63475
    :cond_0
    :try_start_4
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, LX/0Tj;->e:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 63476
    iget-object v1, p0, LX/0Tj;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    goto :goto_0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 63477
    :cond_1
    new-instance v1, LX/0UE;

    iget-object v2, p0, LX/0Tj;->f:Ljava/util/Collection;

    invoke-direct {v1, v2}, LX/0UE;-><init>(Ljava/util/Collection;)V

    .line 63478
    iget-object v2, p0, LX/0Tj;->f:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->clear()V

    goto :goto_1
.end method

.method private static declared-synchronized h(LX/0Tj;)V
    .locals 2

    .prologue
    .line 63459
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Tj;->j:Z

    const-string v1, "FbSharedPreferencesCache used before initialized"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63460
    monitor-exit p0

    return-void

    .line 63461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized i(LX/0Tj;)V
    .locals 8

    .prologue
    const-wide/32 v6, 0x493e0

    .line 63392
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Tj;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 63393
    :cond_0
    iget-boolean v2, p0, LX/0Tj;->j:Z

    move v2, v2

    .line 63394
    if-nez v2, :cond_1

    .line 63395
    const-wide/32 v2, 0x493e0

    const v4, 0x13a7a664

    invoke-static {p0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 63396
    iget-object v2, p0, LX/0Tj;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 63397
    sub-long/2addr v2, v0

    cmp-long v2, v2, v6

    if-lez v2, :cond_0

    .line 63398
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Timed out waiting for shared prefs to initialize"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 63400
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 63448
    monitor-enter p0

    :try_start_0
    const-string v0, "FbSharedPreferencesCache.init"

    const v1, 0x480bf462

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 63449
    :try_start_1
    const-string v0, "FbSharedPreferencesCache.loadInitialValues"

    const v1, 0x150300bb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 63450
    :try_start_2
    iget-object v0, p0, LX/0Tj;->a:LX/0Tl;

    iget-object v1, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, LX/0Tl;->a(Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63451
    const v0, -0x3d9b13cb

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V

    .line 63452
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Tj;->j:Z

    .line 63453
    const v0, 0x559885c4

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 63454
    const v0, -0x2ce5848e

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 63455
    monitor-exit p0

    return-void

    .line 63456
    :catchall_0
    move-exception v0

    const v1, 0x2ed618d6

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 63457
    :catchall_1
    move-exception v0

    const v1, 0x2ff54f5d

    :try_start_6
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 63458
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Map;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/0Tn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63421
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63422
    :cond_0
    return-void

    .line 63423
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 63424
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 63425
    monitor-enter p0

    .line 63426
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 63427
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Tn;

    .line 63428
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 63429
    iget-object v4, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 63430
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63431
    iget-object v4, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63432
    iget-object v4, p0, LX/0Tj;->e:Ljava/util/Map;

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63433
    iget-object v0, p0, LX/0Tj;->f:Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63434
    :cond_3
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 63435
    iget-object v3, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 63436
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63437
    iget-object v3, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63438
    iget-object v3, p0, LX/0Tj;->f:Ljava/util/Collection;

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63439
    iget-object v3, p0, LX/0Tj;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 63440
    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63441
    invoke-static {p0}, LX/0Tj;->c(LX/0Tj;)V

    .line 63442
    iget-object v0, p0, LX/0Tj;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TG;

    .line 63443
    iget-object v3, v0, LX/0TG;->g:LX/0UG;

    iget-object v4, v0, LX/0TG;->d:Ljava/util/concurrent/ExecutorService;

    .line 63444
    iget-object p0, v3, LX/0UG;->a:LX/0UH;

    invoke-virtual {p0, v1, v0, v4}, LX/0UI;->a(Ljava/util/Collection;Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    .line 63445
    iget-object p0, v3, LX/0UG;->b:LX/0UJ;

    invoke-virtual {p0, v1, v0, v4}, LX/0UI;->a(Ljava/util/Collection;Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    .line 63446
    goto :goto_2

    .line 63447
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;)Z
    .locals 1

    .prologue
    .line 63418
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Tj;->h(LX/0Tj;)V

    .line 63419
    iget-object v0, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 63420
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0Tn;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 63410
    monitor-enter p0

    .line 63411
    :try_start_0
    invoke-static {p0}, LX/0Tj;->i(LX/0Tj;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63412
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/0Tj;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63413
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 63414
    :catch_0
    move-exception v0

    .line 63415
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 63416
    sget-object v1, LX/0Tj;->c:Ljava/lang/Class;

    const-string v2, "Error while trying to initialize shared prefs"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63417
    iget-boolean v0, p0, LX/0Tj;->j:Z

    const-string v1, "Interrupted before FbSharedPreferencesCache initialized"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final declared-synchronized c(LX/0Tn;)Ljava/util/SortedMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63401
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Tj;->h(LX/0Tj;)V

    .line 63402
    iget-object v0, p0, LX/0Tj;->d:Ljava/util/Map;

    .line 63403
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v3

    .line 63404
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 63405
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-virtual {v2, p1}, LX/0To;->a(LX/0To;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63406
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 63407
    :cond_1
    move-object v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63408
    monitor-exit p0

    return-object v0

    .line 63409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
