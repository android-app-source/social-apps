.class public final LX/0RY;
.super LX/0RV;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ExplicitComplexProvider"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0RG;


# direct methods
.method public constructor <init>(LX/0RG;)V
    .locals 0

    .prologue
    .line 60086
    iput-object p1, p0, LX/0RY;->a:LX/0RG;

    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 60087
    iget-object v0, p0, LX/0RY;->a:LX/0RG;

    iget-object v0, v0, LX/0RG;->d:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    invoke-virtual {v0}, LX/0S7;->d()Landroid/content/Context;

    move-result-object v0

    .line 60088
    if-nez v0, :cond_0

    .line 60089
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 60090
    :cond_0
    move-object v1, v0

    .line 60091
    :goto_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    .line 60092
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_0

    .line 60093
    :cond_1
    move-object v1, v1

    .line 60094
    if-nez v1, :cond_2

    .line 60095
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 60096
    :cond_2
    instance-of v2, v1, Landroid/app/Service;

    if-nez v2, :cond_3

    instance-of v1, v1, Landroid/app/Application;

    if-eqz v1, :cond_4

    .line 60097
    :cond_3
    new-instance v0, LX/4fr;

    const-string v1, "Can\'t inject @ForWindowedContext Context. The Context initiating the current injection chain doesn\'t have access to a window."

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60098
    :cond_4
    return-object v0
.end method
