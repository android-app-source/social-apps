.class public final LX/1jT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UsingDefaultJsonDeserializer"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/http/HttpHost;",
            ">;"
        }
    .end annotation
.end field

.field public final bytesHeaders:LX/1jU;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bytesHeaders"
    .end annotation
.end field

.field public final bytesPayload:LX/1jU;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bytesPayload"
    .end annotation
.end field

.field public numConnections:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "numConnections"
    .end annotation
.end field

.field public numGets:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "numGets"
    .end annotation
.end field

.field public numPosts:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "numPosts"
    .end annotation
.end field

.field public final requestName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "requestName"
    .end annotation
.end field

.field public totalHttpFlows:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "totalHttpFlows"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 300120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300121
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1jT;->a:Ljava/util/Set;

    .line 300122
    new-instance v0, LX/1jU;

    invoke-direct {v0}, LX/1jU;-><init>()V

    iput-object v0, p0, LX/1jT;->bytesHeaders:LX/1jU;

    .line 300123
    new-instance v0, LX/1jU;

    invoke-direct {v0}, LX/1jU;-><init>()V

    iput-object v0, p0, LX/1jT;->bytesPayload:LX/1jU;

    .line 300124
    iput-object p1, p0, LX/1jT;->requestName:Ljava/lang/String;

    .line 300125
    return-void
.end method
