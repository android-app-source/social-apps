.class public final LX/0Qi;
.super LX/0Qj;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Qj",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final INSTANCE:LX/0Qi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58463
    new-instance v0, LX/0Qi;

    invoke-direct {v0}, LX/0Qi;-><init>()V

    sput-object v0, LX/0Qi;->INSTANCE:LX/0Qi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58464
    invoke-direct {p0}, LX/0Qj;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58465
    sget-object v0, LX/0Qi;->INSTANCE:LX/0Qi;

    return-object v0
.end method


# virtual methods
.method public final doEquivalent(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 58466
    const/4 v0, 0x0

    return v0
.end method

.method public final doHash(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 58467
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
