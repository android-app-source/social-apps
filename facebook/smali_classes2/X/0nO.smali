.class public LX/0nO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0nN;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0xcd01b6e7cfbcee7L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 135140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/1Xt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/1Xt;"
        }
    .end annotation

    .prologue
    .line 135091
    new-instance v0, LX/4qa;

    .line 135092
    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 135093
    invoke-direct {v0, v1, p1}, LX/4qa;-><init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method public static a(LX/0mu;LX/0lJ;)LX/1Xt;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135127
    invoke-virtual {p0, p1}, LX/0mu;->b(LX/0lJ;)LX/0lS;

    move-result-object v0

    .line 135128
    new-array v1, v4, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, LX/0lS;->a([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 135129
    if-eqz v1, :cond_1

    .line 135130
    invoke-virtual {p0}, LX/0m4;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135131
    invoke-static {v1}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 135132
    :cond_0
    new-instance v0, LX/4qh;

    invoke-direct {v0, v1}, LX/4qh;-><init>(Ljava/lang/reflect/Constructor;)V

    .line 135133
    :goto_0
    return-object v0

    .line 135134
    :cond_1
    new-array v1, v4, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, LX/0lS;->b([Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 135135
    if-eqz v1, :cond_3

    .line 135136
    invoke-virtual {p0}, LX/0m4;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135137
    invoke-static {v1}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 135138
    :cond_2
    new-instance v0, LX/4qi;

    invoke-direct {v0, v1}, LX/4qi;-><init>(Ljava/lang/reflect/Method;)V

    goto :goto_0

    .line 135139
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/4rm;)LX/1Xt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4rm",
            "<*>;)",
            "LX/1Xt;"
        }
    .end annotation

    .prologue
    .line 135126
    new-instance v0, LX/4qc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/4qc;-><init>(LX/4rm;LX/2At;)V

    return-object v0
.end method

.method public static a(LX/4rm;LX/2At;)LX/1Xt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4rm",
            "<*>;",
            "LX/2At;",
            ")",
            "LX/1Xt;"
        }
    .end annotation

    .prologue
    .line 135125
    new-instance v0, LX/4qc;

    invoke-direct {v0, p0, p1}, LX/4qc;-><init>(LX/4rm;LX/2At;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0lJ;)LX/1Xt;
    .locals 2

    .prologue
    .line 135094
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135095
    const-class v1, Ljava/lang/String;

    if-eq v0, v1, :cond_0

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 135096
    :cond_0
    invoke-static {v0}, LX/4qj;->a(Ljava/lang/Class;)LX/4qj;

    move-result-object v0

    .line 135097
    :goto_0
    return-object v0

    .line 135098
    :cond_1
    const-class v1, Ljava/util/UUID;

    if-ne v0, v1, :cond_2

    .line 135099
    new-instance v0, LX/4qk;

    invoke-direct {v0}, LX/4qk;-><init>()V

    goto :goto_0

    .line 135100
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 135101
    invoke-static {v0}, LX/1Xw;->g(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 135102
    :cond_3
    const-class v1, Ljava/lang/Integer;

    if-ne v0, v1, :cond_4

    .line 135103
    new-instance v0, LX/2v9;

    invoke-direct {v0}, LX/2v9;-><init>()V

    goto :goto_0

    .line 135104
    :cond_4
    const-class v1, Ljava/lang/Long;

    if-ne v0, v1, :cond_5

    .line 135105
    new-instance v0, LX/4qf;

    invoke-direct {v0}, LX/4qf;-><init>()V

    goto :goto_0

    .line 135106
    :cond_5
    const-class v1, Ljava/util/Date;

    if-ne v0, v1, :cond_6

    .line 135107
    new-instance v0, LX/4qZ;

    invoke-direct {v0}, LX/4qZ;-><init>()V

    goto :goto_0

    .line 135108
    :cond_6
    const-class v1, Ljava/util/Calendar;

    if-ne v0, v1, :cond_7

    .line 135109
    new-instance v0, LX/4qX;

    invoke-direct {v0}, LX/4qX;-><init>()V

    goto :goto_0

    .line 135110
    :cond_7
    const-class v1, Ljava/lang/Boolean;

    if-ne v0, v1, :cond_8

    .line 135111
    new-instance v0, LX/4qV;

    invoke-direct {v0}, LX/4qV;-><init>()V

    goto :goto_0

    .line 135112
    :cond_8
    const-class v1, Ljava/lang/Byte;

    if-ne v0, v1, :cond_9

    .line 135113
    new-instance v0, LX/4qW;

    invoke-direct {v0}, LX/4qW;-><init>()V

    goto :goto_0

    .line 135114
    :cond_9
    const-class v1, Ljava/lang/Character;

    if-ne v0, v1, :cond_a

    .line 135115
    new-instance v0, LX/4qY;

    invoke-direct {v0}, LX/4qY;-><init>()V

    goto :goto_0

    .line 135116
    :cond_a
    const-class v1, Ljava/lang/Short;

    if-ne v0, v1, :cond_b

    .line 135117
    new-instance v0, LX/4qg;

    invoke-direct {v0}, LX/4qg;-><init>()V

    goto :goto_0

    .line 135118
    :cond_b
    const-class v1, Ljava/lang/Float;

    if-ne v0, v1, :cond_c

    .line 135119
    new-instance v0, LX/4qd;

    invoke-direct {v0}, LX/4qd;-><init>()V

    goto :goto_0

    .line 135120
    :cond_c
    const-class v1, Ljava/lang/Double;

    if-ne v0, v1, :cond_d

    .line 135121
    new-instance v0, LX/4qb;

    invoke-direct {v0}, LX/4qb;-><init>()V

    goto :goto_0

    .line 135122
    :cond_d
    const-class v1, Ljava/util/Locale;

    if-ne v0, v1, :cond_e

    .line 135123
    new-instance v0, LX/4qe;

    invoke-direct {v0}, LX/4qe;-><init>()V

    goto/16 :goto_0

    .line 135124
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
