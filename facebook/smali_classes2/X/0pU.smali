.class public LX/0pU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pU;


# instance fields
.field public final a:LX/0SG;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3m0;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144512
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0pU;->b:Ljava/util/ArrayList;

    .line 144513
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0pU;->c:Ljava/util/ArrayList;

    .line 144514
    iput-object p1, p0, LX/0pU;->a:LX/0SG;

    .line 144515
    return-void
.end method

.method public static a(LX/0QB;)LX/0pU;
    .locals 4

    .prologue
    .line 144516
    sget-object v0, LX/0pU;->d:LX/0pU;

    if-nez v0, :cond_1

    .line 144517
    const-class v1, LX/0pU;

    monitor-enter v1

    .line 144518
    :try_start_0
    sget-object v0, LX/0pU;->d:LX/0pU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144519
    if-eqz v2, :cond_0

    .line 144520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144521
    new-instance p0, LX/0pU;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/0pU;-><init>(LX/0SG;)V

    .line 144522
    move-object v0, p0

    .line 144523
    sput-object v0, LX/0pU;->d:LX/0pU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144524
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144525
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144526
    :cond_1
    sget-object v0, LX/0pU;->d:LX/0pU;

    return-object v0

    .line 144527
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/0pU;)LX/3m0;
    .locals 2

    .prologue
    .line 144529
    iget-object v0, p0, LX/0pU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144530
    const/4 v0, 0x0

    .line 144531
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0pU;->b:Ljava/util/ArrayList;

    iget-object v1, p0, LX/0pU;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3m0;

    goto :goto_0
.end method
