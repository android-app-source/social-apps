.class public final LX/1mj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/1dK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 314089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 314090
    check-cast p1, LX/1dK;

    check-cast p2, LX/1dK;

    const/4 v0, 0x1

    .line 314091
    iget-object v1, p1, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v1, v1

    .line 314092
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 314093
    iget-object v2, p2, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v2, v2

    .line 314094
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 314095
    if-lt v1, v2, :cond_3

    if-le v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    .line 314096
    :cond_1
    iget-object v1, p1, LX/1dK;->b:LX/1X1;

    move-object v1, v1

    .line 314097
    invoke-static {v1}, LX/1X1;->b(LX/1X1;)Z

    move-result v1

    .line 314098
    iget-object v2, p2, LX/1dK;->b:LX/1X1;

    move-object v2, v2

    .line 314099
    invoke-static {v2}, LX/1X1;->b(LX/1X1;)Z

    move-result v2

    if-ne v1, v2, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 314100
    :cond_2
    iget-object v1, p1, LX/1dK;->b:LX/1X1;

    move-object v1, v1

    .line 314101
    invoke-static {v1}, LX/1X1;->b(LX/1X1;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method
