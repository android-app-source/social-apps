.class public final LX/0aX;
.super LX/0aS;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aS",
        "<",
        "LX/Juj;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0aX;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Juj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84843
    sget-object v0, LX/0Zz;->LOCAL:LX/0Zz;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/0aY;->C:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, p1, v1}, LX/0aS;-><init>(LX/0Zz;LX/0Ot;[Ljava/lang/String;)V

    .line 84844
    return-void
.end method

.method public static a(LX/0QB;)LX/0aX;
    .locals 4

    .prologue
    .line 84845
    sget-object v0, LX/0aX;->a:LX/0aX;

    if-nez v0, :cond_1

    .line 84846
    const-class v1, LX/0aX;

    monitor-enter v1

    .line 84847
    :try_start_0
    sget-object v0, LX/0aX;->a:LX/0aX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84848
    if-eqz v2, :cond_0

    .line 84849
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84850
    new-instance v3, LX/0aX;

    const/16 p0, 0x2b2f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0aX;-><init>(LX/0Ot;)V

    .line 84851
    move-object v0, v3

    .line 84852
    sput-object v0, LX/0aX;->a:LX/0aX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84853
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84854
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84855
    :cond_1
    sget-object v0, LX/0aX;->a:LX/0aX;

    return-object v0

    .line 84856
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 84858
    check-cast p3, LX/Juj;

    .line 84859
    const-string v0, "default_sms"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 84860
    if-eqz v0, :cond_0

    iget-object v1, p3, LX/Juj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMl;

    invoke-virtual {v1}, LX/FMl;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p3, LX/Juj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNi;

    iget-object v2, p3, LX/Juj;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/FNi;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84861
    new-instance v1, LX/2HB;

    iget-object v2, p3, LX/Juj;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 84862
    new-instance v2, LX/3pe;

    invoke-direct {v2}, LX/3pe;-><init>()V

    iget-object p0, p3, LX/Juj;->a:Landroid/content/Context;

    const p1, 0x7f0803e5

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v2

    .line 84863
    new-instance p0, Landroid/content/Intent;

    iget-object p1, p3, LX/Juj;->a:Landroid/content/Context;

    const-class p2, Lcom/facebook/messaging/sms/NeueSmsPreferenceActivity;

    invoke-direct {p0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84864
    iget-object p1, p3, LX/Juj;->a:Landroid/content/Context;

    const/4 p2, 0x0

    const/high16 v0, 0x8000000

    invoke-static {p1, p2, p0, v0}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    .line 84865
    iget-object p1, p3, LX/Juj;->a:Landroid/content/Context;

    const p2, 0x7f0803e4

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object p1, p3, LX/Juj;->a:Landroid/content/Context;

    const p2, 0x7f0803e5

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    iget-object p1, p3, LX/Juj;->b:LX/2zj;

    invoke-interface {p1}, LX/2zj;->g()I

    move-result p1

    invoke-virtual {v1, p1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    .line 84866
    iput-object p0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 84867
    move-object v1, v1

    .line 84868
    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v1

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    .line 84869
    iget-object v2, p3, LX/Juj;->c:LX/3RK;

    const/16 p0, 0x272c

    invoke-virtual {v2, p0, v1}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 84870
    iget-object v1, p3, LX/Juj;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/30m;

    .line 84871
    const-string v2, "sms_takeover_fallback_notification"

    invoke-static {v1, v2}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 84872
    invoke-static {v1, v2}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 84873
    :goto_0
    return-void

    .line 84874
    :cond_0
    iget-object v1, p3, LX/Juj;->c:LX/3RK;

    const/16 v2, 0x272c

    invoke-virtual {v1, v2}, LX/3RK;->a(I)V

    .line 84875
    goto :goto_0
.end method
