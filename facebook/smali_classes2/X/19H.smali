.class public final LX/19H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 207614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 207615
    new-instance v1, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    const-class v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    .line 207616
    iput-object v0, v1, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 207617
    move-object v0, v1

    .line 207618
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 207619
    iput-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->b:Ljava/lang/String;

    .line 207620
    move-object v1, v0

    .line 207621
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 207622
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    .line 207623
    move-object v0, v1

    .line 207624
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 207625
    new-array v0, p1, [Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    return-object v0
.end method
