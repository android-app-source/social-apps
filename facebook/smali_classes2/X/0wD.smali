.class public final enum LX/0wD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wD;

.field public static final enum APP_INVITES_FEED:LX/0wD;

.field public static final enum ELECTION_HUB:LX/0wD;

.field public static final enum EVENT:LX/0wD;

.field public static final enum FULLSCREEN_VIDEO_PLAYER:LX/0wD;

.field public static final enum FUNDRAISER_PERSON_TO_CHARITY:LX/0wD;

.field public static final enum GROUP:LX/0wD;

.field public static final enum MESSENGER:LX/0wD;

.field public static final enum MESSENGER_CONTACT_DETAILS:LX/0wD;

.field public static final enum MESSENGER_MONTAGE_VIEWER:LX/0wD;

.field public static final enum MESSENGER_THREAD_ACTION_PANEL:LX/0wD;

.field public static final enum NEWSFEED:LX/0wD;

.field public static final enum PAGE_REVIEW:LX/0wD;

.field public static final enum PAGE_TIMELINE:LX/0wD;

.field public static final enum PERMALINK:LX/0wD;

.field public static final enum PHOTO_VIEWER:LX/0wD;

.field public static final enum SEARCH_RESULTS:LX/0wD;

.field public static final enum THROWBACK:LX/0wD;

.field public static final enum TIMELINE:LX/0wD;

.field public static final enum TIMELINE_SELF:LX/0wD;

.field public static final enum TIMELINE_SOMEONE_ELSE:LX/0wD;

.field public static final enum UNKNOWN:LX/0wD;

.field public static final enum VIDEO_CHANNEL:LX/0wD;


# instance fields
.field private final location:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159507
    new-instance v0, LX/0wD;

    const-string v1, "MESSENGER"

    const-string v2, "messenger"

    invoke-direct {v0, v1, v4, v2}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->MESSENGER:LX/0wD;

    .line 159508
    new-instance v0, LX/0wD;

    const-string v1, "MESSENGER_CONTACT_DETAILS"

    const-string v2, "messenger_contact_details"

    invoke-direct {v0, v1, v5, v2}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->MESSENGER_CONTACT_DETAILS:LX/0wD;

    .line 159509
    new-instance v0, LX/0wD;

    const-string v1, "MESSENGER_MONTAGE_VIEWER"

    const-string v2, "messenger_montage_viewer"

    invoke-direct {v0, v1, v6, v2}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->MESSENGER_MONTAGE_VIEWER:LX/0wD;

    .line 159510
    new-instance v0, LX/0wD;

    const-string v1, "MESSENGER_THREAD_ACTION_PANEL"

    const-string v2, "messenger_thread_action_panel"

    invoke-direct {v0, v1, v7, v2}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->MESSENGER_THREAD_ACTION_PANEL:LX/0wD;

    .line 159511
    new-instance v0, LX/0wD;

    const-string v1, "NEWSFEED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v8, v2}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->NEWSFEED:LX/0wD;

    .line 159512
    new-instance v0, LX/0wD;

    const-string v1, "TIMELINE"

    const/4 v2, 0x5

    const-string v3, "profile"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->TIMELINE:LX/0wD;

    .line 159513
    new-instance v0, LX/0wD;

    const-string v1, "TIMELINE_SELF"

    const/4 v2, 0x6

    const-string v3, "profile_self"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->TIMELINE_SELF:LX/0wD;

    .line 159514
    new-instance v0, LX/0wD;

    const-string v1, "TIMELINE_SOMEONE_ELSE"

    const/4 v2, 0x7

    const-string v3, "profile_someone_else"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    .line 159515
    new-instance v0, LX/0wD;

    const-string v1, "PAGE_REVIEW"

    const/16 v2, 0x8

    const-string v3, "page_review"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->PAGE_REVIEW:LX/0wD;

    .line 159516
    new-instance v0, LX/0wD;

    const-string v1, "PAGE_TIMELINE"

    const/16 v2, 0x9

    const-string v3, "page"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->PAGE_TIMELINE:LX/0wD;

    .line 159517
    new-instance v0, LX/0wD;

    const-string v1, "PERMALINK"

    const/16 v2, 0xa

    const-string v3, "permalink"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->PERMALINK:LX/0wD;

    .line 159518
    new-instance v0, LX/0wD;

    const-string v1, "PHOTO_VIEWER"

    const/16 v2, 0xb

    const-string v3, "photo_viewer"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->PHOTO_VIEWER:LX/0wD;

    .line 159519
    new-instance v0, LX/0wD;

    const-string v1, "VIDEO_CHANNEL"

    const/16 v2, 0xc

    const-string v3, "video_channel"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    .line 159520
    new-instance v0, LX/0wD;

    const-string v1, "FULLSCREEN_VIDEO_PLAYER"

    const/16 v2, 0xd

    const-string v3, "fullscreen_video_player"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->FULLSCREEN_VIDEO_PLAYER:LX/0wD;

    .line 159521
    new-instance v0, LX/0wD;

    const-string v1, "GROUP"

    const/16 v2, 0xe

    const-string v3, "group"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->GROUP:LX/0wD;

    .line 159522
    new-instance v0, LX/0wD;

    const-string v1, "EVENT"

    const/16 v2, 0xf

    const-string v3, "event"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->EVENT:LX/0wD;

    .line 159523
    new-instance v0, LX/0wD;

    const-string v1, "APP_INVITES_FEED"

    const/16 v2, 0x10

    const-string v3, "app_invites_feed"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->APP_INVITES_FEED:LX/0wD;

    .line 159524
    new-instance v0, LX/0wD;

    const-string v1, "SEARCH_RESULTS"

    const/16 v2, 0x11

    const-string v3, "search_results"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->SEARCH_RESULTS:LX/0wD;

    .line 159525
    new-instance v0, LX/0wD;

    const-string v1, "THROWBACK"

    const/16 v2, 0x12

    const-string v3, "throwback"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->THROWBACK:LX/0wD;

    .line 159526
    new-instance v0, LX/0wD;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x13

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->UNKNOWN:LX/0wD;

    .line 159527
    new-instance v0, LX/0wD;

    const-string v1, "FUNDRAISER_PERSON_TO_CHARITY"

    const/16 v2, 0x14

    const-string v3, "fundraiser_person_to_charity"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->FUNDRAISER_PERSON_TO_CHARITY:LX/0wD;

    .line 159528
    new-instance v0, LX/0wD;

    const-string v1, "ELECTION_HUB"

    const/16 v2, 0x15

    const-string v3, "election_hub"

    invoke-direct {v0, v1, v2, v3}, LX/0wD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wD;->ELECTION_HUB:LX/0wD;

    .line 159529
    const/16 v0, 0x16

    new-array v0, v0, [LX/0wD;

    sget-object v1, LX/0wD;->MESSENGER:LX/0wD;

    aput-object v1, v0, v4

    sget-object v1, LX/0wD;->MESSENGER_CONTACT_DETAILS:LX/0wD;

    aput-object v1, v0, v5

    sget-object v1, LX/0wD;->MESSENGER_MONTAGE_VIEWER:LX/0wD;

    aput-object v1, v0, v6

    sget-object v1, LX/0wD;->MESSENGER_THREAD_ACTION_PANEL:LX/0wD;

    aput-object v1, v0, v7

    sget-object v1, LX/0wD;->NEWSFEED:LX/0wD;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0wD;->TIMELINE:LX/0wD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0wD;->TIMELINE_SELF:LX/0wD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0wD;->PAGE_REVIEW:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0wD;->PAGE_TIMELINE:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0wD;->PERMALINK:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0wD;->PHOTO_VIEWER:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0wD;->FULLSCREEN_VIDEO_PLAYER:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0wD;->GROUP:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0wD;->EVENT:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0wD;->APP_INVITES_FEED:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0wD;->SEARCH_RESULTS:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0wD;->THROWBACK:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0wD;->UNKNOWN:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0wD;->FUNDRAISER_PERSON_TO_CHARITY:LX/0wD;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0wD;->ELECTION_HUB:LX/0wD;

    aput-object v2, v0, v1

    sput-object v0, LX/0wD;->$VALUES:[LX/0wD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 159531
    iput-object p3, p0, LX/0wD;->location:Ljava/lang/String;

    .line 159532
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wD;
    .locals 1

    .prologue
    .line 159533
    const-class v0, LX/0wD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wD;

    return-object v0
.end method

.method public static values()[LX/0wD;
    .locals 1

    .prologue
    .line 159534
    sget-object v0, LX/0wD;->$VALUES:[LX/0wD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wD;

    return-object v0
.end method


# virtual methods
.method public final stringValueOf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159535
    iget-object v0, p0, LX/0wD;->location:Ljava/lang/String;

    return-object v0
.end method
