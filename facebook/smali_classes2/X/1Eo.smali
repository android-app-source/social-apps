.class public LX/1Eo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1Eo;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/1Ep;

.field private final c:LX/0if;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0un;LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 221172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221173
    iput-object p1, p0, LX/1Eo;->a:LX/0Zb;

    .line 221174
    const-string v0, "backstage_cache"

    const-string v1, "backstage_main_list_query"

    invoke-virtual {p2, v0, v1}, LX/0un;->a(Ljava/lang/String;Ljava/lang/String;)LX/1Ep;

    move-result-object v0

    iput-object v0, p0, LX/1Eo;->b:LX/1Ep;

    .line 221175
    iput-object p3, p0, LX/1Eo;->c:LX/0if;

    .line 221176
    return-void
.end method

.method public static a(LX/0QB;)LX/1Eo;
    .locals 6

    .prologue
    .line 221177
    sget-object v0, LX/1Eo;->e:LX/1Eo;

    if-nez v0, :cond_1

    .line 221178
    const-class v1, LX/1Eo;

    monitor-enter v1

    .line 221179
    :try_start_0
    sget-object v0, LX/1Eo;->e:LX/1Eo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221180
    if-eqz v2, :cond_0

    .line 221181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221182
    new-instance p0, LX/1Eo;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const-class v4, LX/0un;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0un;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-direct {p0, v3, v4, v5}, LX/1Eo;-><init>(LX/0Zb;LX/0un;LX/0if;)V

    .line 221183
    move-object v0, p0

    .line 221184
    sput-object v0, LX/1Eo;->e:LX/1Eo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221185
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221186
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221187
    :cond_1
    sget-object v0, LX/1Eo;->e:LX/1Eo;

    return-object v0

    .line 221188
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221190
    iget-object v0, p0, LX/1Eo;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 221191
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1Eo;->d:Ljava/lang/String;

    .line 221192
    :cond_0
    iget-object v0, p0, LX/1Eo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static a(LX/1Eo;LX/7gQ;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # LX/7gQ;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221193
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/7gQ;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 221194
    invoke-virtual {p1}, LX/7gQ;->getModuleName()Ljava/lang/String;

    move-result-object v0

    .line 221195
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 221196
    invoke-direct {p0}, LX/1Eo;->a()Ljava/lang/String;

    move-result-object v0

    .line 221197
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 221198
    const-string v0, "backstage_id"

    invoke-direct {p0}, LX/1Eo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 221199
    if-eqz p2, :cond_0

    .line 221200
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221201
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 221202
    :cond_0
    iget-object v0, p0, LX/1Eo;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 221203
    return-void
.end method


# virtual methods
.method public final a(LX/7gQ;)V
    .locals 1

    .prologue
    .line 221204
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/1Eo;->a(LX/1Eo;LX/7gQ;Landroid/os/Bundle;)V

    .line 221205
    return-void
.end method
