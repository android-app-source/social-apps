.class public LX/0gJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0xB;

.field private final b:LX/0gI;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gL;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gN;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0gK;


# direct methods
.method public constructor <init>(LX/0xB;LX/0gI;LX/0gK;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xB;",
            "LX/0gI;",
            "LX/0gK;",
            "LX/0Ot",
            "<",
            "LX/0gL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 111891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111892
    iput-object p1, p0, LX/0gJ;->a:LX/0xB;

    .line 111893
    iput-object p2, p0, LX/0gJ;->b:LX/0gI;

    .line 111894
    iput-object p3, p0, LX/0gJ;->e:LX/0gK;

    .line 111895
    iput-object p4, p0, LX/0gJ;->c:LX/0Ot;

    .line 111896
    iput-object p5, p0, LX/0gJ;->d:LX/0Ot;

    .line 111897
    iget-object v0, p0, LX/0gJ;->e:LX/0gK;

    invoke-virtual {v0}, LX/0gK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111898
    iget-object v0, p0, LX/0gJ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gL;

    new-instance v1, LX/AEw;

    invoke-direct {v1, p0}, LX/AEw;-><init>(LX/0gJ;)V

    .line 111899
    iput-object v1, v0, LX/0gL;->f:LX/AEw;

    .line 111900
    :goto_0
    return-void

    .line 111901
    :cond_0
    iget-object v0, p0, LX/0gJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gN;

    new-instance v1, LX/AEx;

    invoke-direct {v1, p0}, LX/AEx;-><init>(LX/0gJ;)V

    .line 111902
    iput-object v1, v0, LX/0gN;->g:LX/AEx;

    .line 111903
    goto :goto_0
.end method

.method public static a$redex0(LX/0gJ;I)V
    .locals 2

    .prologue
    .line 111904
    iget-object v0, p0, LX/0gJ;->a:LX/0xB;

    sget-object v1, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 111905
    if-eq v0, p1, :cond_0

    .line 111906
    iget-object v0, p0, LX/0gJ;->a:LX/0xB;

    sget-object v1, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v0, v1, p1}, LX/0xB;->a(LX/12j;I)V

    .line 111907
    :cond_0
    iget-object v0, p0, LX/0gJ;->b:LX/0gI;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    .line 111908
    return-void
.end method
