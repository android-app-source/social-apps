.class public LX/0du;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# static fields
.field private static a:Z

.field private static b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 91136
    sput-boolean v0, LX/0du;->a:Z

    .line 91137
    sput-boolean v0, LX/0du;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91138
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 91139
    return-void
.end method

.method public static a(LX/0Ot;LX/0Ot;)LX/1h1;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/onion/OnionRewriterFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0dv;",
            ">;)",
            "LX/1h1;"
        }
    .end annotation

    .prologue
    .line 91140
    sget-boolean v0, LX/0du;->a:Z

    if-nez v0, :cond_0

    .line 91141
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dv;

    invoke-virtual {v0}, LX/0dv;->a()Z

    move-result v0

    sput-boolean v0, LX/0du;->b:Z

    .line 91142
    const/4 v0, 0x1

    sput-boolean v0, LX/0du;->a:Z

    .line 91143
    :cond_0
    sget-boolean v0, LX/0du;->b:Z

    if-eqz v0, :cond_1

    .line 91144
    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9t;

    .line 91145
    iget-object p0, v0, LX/F9t;->a:LX/F9s;

    move-object v0, p0

    .line 91146
    :goto_0
    return-object v0

    .line 91147
    :cond_1
    new-instance v0, LX/1h0;

    invoke-direct {v0}, LX/1h0;-><init>()V

    goto :goto_0
.end method

.method public static b(LX/0Ot;LX/0Ot;)LX/0dx;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/onion/TorProxyFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0dv;",
            ">;)",
            "LX/0dx;"
        }
    .end annotation

    .prologue
    .line 91148
    sget-boolean v0, LX/0du;->a:Z

    if-nez v0, :cond_0

    .line 91149
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dv;

    invoke-virtual {v0}, LX/0dv;->a()Z

    move-result v0

    sput-boolean v0, LX/0du;->b:Z

    .line 91150
    const/4 v0, 0x1

    sput-boolean v0, LX/0du;->a:Z

    .line 91151
    :cond_0
    sget-boolean v0, LX/0du;->b:Z

    if-eqz v0, :cond_1

    .line 91152
    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9y;

    .line 91153
    iget-object p0, v0, LX/F9y;->a:LX/F9x;

    move-object v0, p0

    .line 91154
    :goto_0
    return-object v0

    .line 91155
    :cond_1
    new-instance v0, LX/0dw;

    invoke-direct {v0}, LX/0dw;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 91156
    return-void
.end method
