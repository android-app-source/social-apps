.class public LX/0YF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0YF;


# instance fields
.field private final a:LX/0Zr;


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80788
    iput-object p1, p0, LX/0YF;->a:LX/0Zr;

    .line 80789
    return-void
.end method

.method public static a(LX/0QB;)LX/0YF;
    .locals 4

    .prologue
    .line 80790
    sget-object v0, LX/0YF;->b:LX/0YF;

    if-nez v0, :cond_1

    .line 80791
    const-class v1, LX/0YF;

    monitor-enter v1

    .line 80792
    :try_start_0
    sget-object v0, LX/0YF;->b:LX/0YF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80793
    if-eqz v2, :cond_0

    .line 80794
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80795
    new-instance p0, LX/0YF;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v3

    check-cast v3, LX/0Zr;

    invoke-direct {p0, v3}, LX/0YF;-><init>(LX/0Zr;)V

    .line 80796
    move-object v0, p0

    .line 80797
    sput-object v0, LX/0YF;->b:LX/0YF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80798
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80799
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80800
    :cond_1
    sget-object v0, LX/0YF;->b:LX/0YF;

    return-object v0

    .line 80801
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0TP;Z)LX/0Te;
    .locals 3

    .prologue
    .line 80803
    iget-object v0, p0, LX/0YF;->a:LX/0Zr;

    invoke-virtual {v0, p1, p2, p3}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;Z)Landroid/os/HandlerThread;

    move-result-object v0

    .line 80804
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 80805
    new-instance v1, LX/0Td;

    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v1, v2}, LX/0Td;-><init>(Landroid/os/Handler;)V

    return-object v1
.end method
