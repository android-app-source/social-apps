.class public final LX/0bO;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "LX/0bP;",
        "LX/8Mf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/8Mf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86866
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86867
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86868
    const-class v0, LX/0bP;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 86869
    check-cast p1, LX/0bP;

    check-cast p2, LX/8Mf;

    .line 86870
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86871
    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86872
    :cond_0
    :goto_0
    return-void

    .line 86873
    :cond_1
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86874
    sget-object v1, LX/8KZ;->UPLOADING:LX/8KZ;

    if-ne v0, v1, :cond_0

    .line 86875
    iget v0, p1, LX/0b5;->c:F

    move v0, v0

    .line 86876
    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 86877
    if-ltz v0, :cond_0

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_0

    .line 86878
    iget-object v1, p2, LX/8Mf;->c:LX/7m8;

    .line 86879
    iget-object v2, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v2, v2

    .line 86880
    iget-object v3, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v3

    .line 86881
    iget-object v3, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v3, v3

    .line 86882
    iget-wide v6, v3, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v4, v6

    .line 86883
    const/16 v3, 0x3e7

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v2, v4, v5, v0}, LX/7m8;->a(Ljava/lang/String;JI)V

    goto :goto_0
.end method
