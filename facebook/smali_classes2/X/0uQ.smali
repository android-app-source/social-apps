.class public LX/0uQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field private static final l:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 156360
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "analytics"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 156361
    sput-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "process_stat_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->b:LX/0Tn;

    .line 156362
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "batch_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->c:LX/0Tn;

    .line 156363
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "contacts_upload_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->d:LX/0Tn;

    .line 156364
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "device_info_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->e:LX/0Tn;

    .line 156365
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "device_stat_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->f:LX/0Tn;

    .line 156366
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "user_logged_in"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->g:LX/0Tn;

    .line 156367
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "sampling_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->h:LX/0Tn;

    .line 156368
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "sampling_config_checksum"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->i:LX/0Tn;

    .line 156369
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "periodic_events_last_sent"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->j:LX/0Tn;

    .line 156370
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "device_info_need_upload_phone/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->l:LX/0Tn;

    .line 156371
    sget-object v0, LX/0uQ;->a:LX/0Tn;

    const-string v1, "device_info_oldest_known_installer"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0uQ;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 156359
    sget-object v0, LX/0uQ;->l:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
