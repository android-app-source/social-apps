.class public LX/1FK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public a:LX/1FJ;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(LX/1FJ;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 222159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222160
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222161
    if-ltz p2, :cond_0

    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 222162
    iget v1, v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->b:I

    move v0, v1

    .line 222163
    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222164
    invoke-virtual {p1}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/1FK;->a:LX/1FJ;

    .line 222165
    iput p2, p0, LX/1FK;->b:I

    .line 222166
    return-void

    .line 222167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 222155
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1FK;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222156
    new-instance v0, LX/4eQ;

    invoke-direct {v0}, LX/4eQ;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 222158
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)B
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 222148
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1FK;->d()V

    .line 222149
    if-ltz p1, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, LX/03g;->a(Z)V

    .line 222150
    iget v2, p0, LX/1FK;->b:I

    if-ge p1, v2, :cond_1

    :goto_1
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222151
    iget-object v0, p0, LX/1FK;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    invoke-virtual {v0, p1}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->a(I)B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    .line 222152
    goto :goto_0

    :cond_1
    move v0, v1

    .line 222153
    goto :goto_1

    .line 222154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 222145
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1FK;->d()V

    .line 222146
    iget v0, p0, LX/1FK;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 222147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I[BII)V
    .locals 2

    .prologue
    .line 222129
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1FK;->d()V

    .line 222130
    add-int v0, p1, p4

    iget v1, p0, LX/1FK;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222131
    iget-object v0, p0, LX/1FK;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->b(I[BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222132
    monitor-exit p0

    return-void

    .line 222133
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 222134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()J
    .locals 4

    .prologue
    .line 222140
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1FK;->d()V

    .line 222141
    iget-object v0, p0, LX/1FK;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 222142
    iget-wide v2, v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->a:J

    move-wide v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222143
    monitor-exit p0

    return-wide v0

    .line 222144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 222139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FK;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized close()V
    .locals 1

    .prologue
    .line 222135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FK;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 222136
    const/4 v0, 0x0

    iput-object v0, p0, LX/1FK;->a:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222137
    monitor-exit p0

    return-void

    .line 222138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
