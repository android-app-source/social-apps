.class public LX/0WZ;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/gk/store/GatekeeperStoreLogger;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0UY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76545
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0UY;
    .locals 3

    .prologue
    .line 76546
    sget-object v0, LX/0WZ;->a:LX/0UY;

    if-nez v0, :cond_1

    .line 76547
    const-class v1, LX/0WZ;

    monitor-enter v1

    .line 76548
    :try_start_0
    sget-object v0, LX/0WZ;->a:LX/0UY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76549
    if-eqz v2, :cond_0

    .line 76550
    :try_start_1
    invoke-static {}, Lcom/facebook/gk/sessionless/GkSessionlessModule;->b()LX/0UY;

    move-result-object v0

    sput-object v0, LX/0WZ;->a:LX/0UY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76551
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76552
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76553
    :cond_1
    sget-object v0, LX/0WZ;->a:LX/0UY;

    return-object v0

    .line 76554
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76556
    invoke-static {}, Lcom/facebook/gk/sessionless/GkSessionlessModule;->b()LX/0UY;

    move-result-object v0

    return-object v0
.end method
