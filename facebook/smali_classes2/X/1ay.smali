.class public LX/1ay;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0id;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0id;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279011
    iput-object p1, p0, LX/1ay;->a:Landroid/content/Context;

    .line 279012
    iput-object p2, p0, LX/1ay;->b:Lcom/facebook/content/SecureContextHelper;

    .line 279013
    iput-object p3, p0, LX/1ay;->c:LX/0id;

    .line 279014
    return-void
.end method

.method public static a(LX/0QB;)LX/1ay;
    .locals 1

    .prologue
    .line 279009
    invoke-static {p0}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1ay;
    .locals 4

    .prologue
    .line 279015
    new-instance v3, LX/1ay;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v2

    check-cast v2, LX/0id;

    invoke-direct {v3, v0, v1, v2}, LX/1ay;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0id;)V

    .line 279016
    return-object v3
.end method

.method public static b(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 279006
    const-string v0, "extra_composer_internal_session_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279007
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "extra_composer_internal_session_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 279008
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)V
    .locals 3

    .prologue
    .line 279003
    iget-object v0, p0, LX/1ay;->c:LX/0id;

    const-string v1, "ComposerIntentLauncher"

    invoke-virtual {v0, p3, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 279004
    iget-object v1, p0, LX/1ay;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/1ay;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, p2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 279005
    return-void
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 279000
    iget-object v0, p0, LX/1ay;->c:LX/0id;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ComposerIntentLauncher"

    invoke-virtual {v0, v1, v2}, LX/0id;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279001
    iget-object v0, p0, LX/1ay;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/1ay;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 279002
    return-void
.end method
