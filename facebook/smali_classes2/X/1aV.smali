.class public final enum LX/1aV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1aV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1aV;

.field public static final enum DRAWABLE:LX/1aV;

.field public static final enum NONE:LX/1aV;

.field public static final enum VIEW:LX/1aV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 277585
    new-instance v0, LX/1aV;

    const-string v1, "VIEW"

    invoke-direct {v0, v1, v2}, LX/1aV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1aV;->VIEW:LX/1aV;

    .line 277586
    new-instance v0, LX/1aV;

    const-string v1, "DRAWABLE"

    invoke-direct {v0, v1, v3}, LX/1aV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1aV;->DRAWABLE:LX/1aV;

    .line 277587
    new-instance v0, LX/1aV;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/1aV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1aV;->NONE:LX/1aV;

    .line 277588
    const/4 v0, 0x3

    new-array v0, v0, [LX/1aV;

    sget-object v1, LX/1aV;->VIEW:LX/1aV;

    aput-object v1, v0, v2

    sget-object v1, LX/1aV;->DRAWABLE:LX/1aV;

    aput-object v1, v0, v3

    sget-object v1, LX/1aV;->NONE:LX/1aV;

    aput-object v1, v0, v4

    sput-object v0, LX/1aV;->$VALUES:[LX/1aV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 277589
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1aV;
    .locals 1

    .prologue
    .line 277590
    const-class v0, LX/1aV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1aV;

    return-object v0
.end method

.method public static values()[LX/1aV;
    .locals 1

    .prologue
    .line 277591
    sget-object v0, LX/1aV;->$VALUES:[LX/1aV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1aV;

    return-object v0
.end method
