.class public abstract LX/0gS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# static fields
.field private static final c:LX/0Zh;


# instance fields
.field public a:LX/0Zh;

.field public b:LX/0n9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112090
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    sput-object v0, LX/0gS;->c:LX/0Zh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112087
    sget-object v0, LX/0gS;->c:LX/0Zh;

    iput-object v0, p0, LX/0gS;->a:LX/0Zh;

    .line 112088
    const/4 v0, 0x0

    iput-object v0, p0, LX/0gS;->b:LX/0n9;

    .line 112089
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 112065
    if-eqz p1, :cond_5

    instance-of v1, p1, LX/0zL;

    if-eqz v1, :cond_5

    .line 112066
    check-cast p1, LX/0zL;

    .line 112067
    invoke-virtual {p1}, LX/0zL;->j()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p1, v0}, LX/0zL;->b(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/0n9;

    if-eqz v1, :cond_2

    .line 112068
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/0zL;->j()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v0

    .line 112069
    :goto_0
    invoke-virtual {p1}, LX/0zL;->j()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 112070
    invoke-virtual {p1, v1}, LX/0zL;->b(I)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0gS;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112071
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object p1, v2

    .line 112072
    :cond_1
    :goto_1
    return-object p1

    .line 112073
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/0zL;->j()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 112074
    :goto_2
    invoke-virtual {p1}, LX/0zL;->j()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 112075
    invoke-virtual {p1, v0}, LX/0zL;->b(I)Ljava/lang/Object;

    move-result-object v2

    .line 112076
    if-eqz v2, :cond_3

    .line 112077
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112078
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 112079
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    move-object p1, v1

    .line 112080
    goto :goto_1

    .line 112081
    :cond_5
    if-eqz p1, :cond_1

    instance-of v0, p1, LX/0n9;

    if-eqz v0, :cond_1

    .line 112082
    check-cast p1, LX/0n9;

    .line 112083
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 112084
    invoke-direct {p0, p1, v0}, LX/0gS;->b(LX/0n9;Ljava/util/Map;)V

    move-object p1, v0

    .line 112085
    goto :goto_1
.end method

.method private static a(LX/0gS;LX/0zL;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 112046
    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 112047
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 112048
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 112049
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 112050
    invoke-static {p1, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 112051
    goto :goto_0

    .line 112052
    :cond_0
    instance-of v1, v0, Ljava/lang/Enum;

    if-eqz v1, :cond_1

    .line 112053
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 112054
    invoke-virtual {v0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112055
    invoke-static {p1, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 112056
    goto :goto_1

    .line 112057
    :cond_1
    instance-of v1, v0, LX/0gS;

    if-eqz v1, :cond_2

    .line 112058
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gS;

    .line 112059
    invoke-virtual {v0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0zL;->c(LX/0nA;)V

    goto :goto_2

    .line 112060
    :cond_2
    instance-of v1, v0, Ljava/util/Map;

    if-eqz v1, :cond_3

    .line 112061
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 112062
    invoke-virtual {p1}, LX/0zL;->k()LX/0n9;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, LX/0gS;->a(LX/0n9;Ljava/util/Map;)V

    goto :goto_3

    .line 112063
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "List value type is not supported: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112064
    :cond_4
    return-void
.end method

.method private a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 112020
    if-nez p3, :cond_0

    .line 112021
    :goto_0
    return-void

    .line 112022
    :cond_0
    instance-of v0, p3, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 112023
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "true"

    .line 112024
    :goto_1
    invoke-static {p1, p2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112025
    goto :goto_0

    :cond_1
    const-string v0, "false"

    goto :goto_1

    .line 112026
    :cond_2
    instance-of v0, p3, Ljava/lang/Number;

    if-eqz v0, :cond_3

    .line 112027
    check-cast p3, Ljava/lang/Number;

    .line 112028
    invoke-static {p1, p2, p3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112029
    goto :goto_0

    .line 112030
    :cond_3
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 112031
    check-cast p3, Ljava/lang/String;

    .line 112032
    invoke-static {p1, p2, p3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112033
    goto :goto_0

    .line 112034
    :cond_4
    instance-of v0, p3, Ljava/lang/Enum;

    if-eqz v0, :cond_5

    .line 112035
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112036
    invoke-static {p1, p2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112037
    goto :goto_0

    .line 112038
    :cond_5
    instance-of v0, p3, LX/0gS;

    if-eqz v0, :cond_6

    .line 112039
    check-cast p3, LX/0gS;

    invoke-virtual {p3}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    goto :goto_0

    .line 112040
    :cond_6
    instance-of v0, p3, Ljava/util/List;

    if-eqz v0, :cond_7

    .line 112041
    invoke-virtual {p1, p2}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v0

    .line 112042
    check-cast p3, Ljava/util/List;

    invoke-static {p0, v0, p3}, LX/0gS;->a(LX/0gS;LX/0zL;Ljava/util/List;)V

    goto :goto_0

    .line 112043
    :cond_7
    instance-of v0, p3, Ljava/util/Map;

    if-eqz v0, :cond_8

    .line 112044
    invoke-virtual {p1, p2}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v0

    check-cast p3, Ljava/util/Map;

    invoke-virtual {p0, v0, p3}, LX/0gS;->a(LX/0n9;Ljava/util/Map;)V

    goto :goto_0

    .line 112045
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected object value type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(LX/0n9;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n9;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112013
    if-eqz p1, :cond_0

    .line 112014
    const/4 v0, 0x0

    .line 112015
    :goto_0
    iget v1, p1, LX/0n9;->c:I

    move v1, v1

    .line 112016
    if-ge v0, v1, :cond_0

    .line 112017
    invoke-virtual {p1, v0}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, LX/0gS;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112018
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112019
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/0n9;
    .locals 1

    .prologue
    .line 112010
    iget-object v0, p0, LX/0gS;->b:LX/0n9;

    if-nez v0, :cond_0

    .line 112011
    iget-object v0, p0, LX/0gS;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    iput-object v0, p0, LX/0gS;->b:LX/0n9;

    .line 112012
    :cond_0
    iget-object v0, p0, LX/0gS;->b:LX/0n9;

    return-object v0
.end method

.method public final a(LX/0n9;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n9;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112005
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112006
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 112007
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 112008
    invoke-direct {p0, p1, v1, v0}, LX/0gS;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 112009
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0gS;)V
    .locals 2

    .prologue
    .line 112091
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    invoke-virtual {p2}, LX/0gS;->a()LX/0n9;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 112092
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 111985
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    .line 111986
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111987
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Double;)V
    .locals 1

    .prologue
    .line 111988
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    .line 111989
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111990
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 111991
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    .line 111992
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111993
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 111994
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    .line 111995
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111996
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 111997
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/0gS;->a(LX/0gS;LX/0zL;Ljava/util/List;)V

    .line 111998
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111999
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 112000
    iget-object v1, p0, LX/0gS;->b:LX/0n9;

    invoke-direct {p0, v1, v0}, LX/0gS;->b(LX/0n9;Ljava/util/Map;)V

    .line 112001
    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 112003
    invoke-virtual {p0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 112004
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 112002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
