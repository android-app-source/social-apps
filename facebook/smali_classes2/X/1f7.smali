.class public LX/1f7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:J

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field private static final d:LX/0Tn;

.field private static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;

.field private static final i:LX/0Tn;

.field private static final j:LX/0Tn;

.field private static final k:LX/0Tn;

.field private static final l:LX/0Tn;

.field private static final m:LX/0Tn;

.field private static final n:LX/0Tn;

.field private static final o:LX/0Tn;

.field private static volatile u:LX/1f7;


# instance fields
.field private final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final q:LX/0Zb;

.field private final r:LX/1Ao;

.field private final s:LX/0SG;

.field private final t:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 290976
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/1f7;->a:J

    .line 290977
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "photos_cache_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 290978
    sput-object v0, LX/1f7;->b:LX/0Tn;

    const-string v1, "tracking_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 290979
    sput-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->d:LX/0Tn;

    .line 290980
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "o_width"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->e:LX/0Tn;

    .line 290981
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "o_height"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->f:LX/0Tn;

    .line 290982
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "o_image_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->g:LX/0Tn;

    .line 290983
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "o_cache_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->h:LX/0Tn;

    .line 290984
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "o_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->i:LX/0Tn;

    .line 290985
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "n_width"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->j:LX/0Tn;

    .line 290986
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "n_height"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->k:LX/0Tn;

    .line 290987
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "n_image_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->l:LX/0Tn;

    .line 290988
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "n_cache_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->m:LX/0Tn;

    .line 290989
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "n_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->n:LX/0Tn;

    .line 290990
    sget-object v0, LX/1f7;->c:LX/0Tn;

    const-string v1, "changed_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1f7;->o:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/1Ao;LX/0SG;Ljava/util/Random;)V
    .locals 0
    .param p5    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 290969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290970
    iput-object p1, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 290971
    iput-object p2, p0, LX/1f7;->q:LX/0Zb;

    .line 290972
    iput-object p3, p0, LX/1f7;->r:LX/1Ao;

    .line 290973
    iput-object p4, p0, LX/1f7;->s:LX/0SG;

    .line 290974
    iput-object p5, p0, LX/1f7;->t:Ljava/util/Random;

    .line 290975
    return-void
.end method

.method public static a(LX/0QB;)LX/1f7;
    .locals 9

    .prologue
    .line 290956
    sget-object v0, LX/1f7;->u:LX/1f7;

    if-nez v0, :cond_1

    .line 290957
    const-class v1, LX/1f7;

    monitor-enter v1

    .line 290958
    :try_start_0
    sget-object v0, LX/1f7;->u:LX/1f7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 290959
    if-eqz v2, :cond_0

    .line 290960
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 290961
    new-instance v3, LX/1f7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/1An;->a(LX/0QB;)LX/1An;

    move-result-object v6

    check-cast v6, LX/1Ao;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v8

    check-cast v8, Ljava/util/Random;

    invoke-direct/range {v3 .. v8}, LX/1f7;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/1Ao;LX/0SG;Ljava/util/Random;)V

    .line 290962
    move-object v0, v3

    .line 290963
    sput-object v0, LX/1f7;->u:LX/1f7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290964
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 290965
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 290966
    :cond_1
    sget-object v0, LX/1f7;->u:LX/1f7;

    return-object v0

    .line 290967
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 290968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 290937
    iget-object v1, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1f7;->d:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 290938
    iget-object v1, p0, LX/1f7;->q:LX/0Zb;

    const-string v2, "photos_cache_key_tracking"

    invoke-interface {v1, v2, v0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 290939
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 290940
    const-string v2, "original_cache_key"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->h:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 290941
    const-string v2, "original_image_url"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->g:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 290942
    const-string v2, "original_image_width"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->e:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 290943
    const-string v2, "original_image_height"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->f:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 290944
    iget-object v2, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1f7;->n:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290945
    const-string v2, "new_cache_key"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->m:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 290946
    const-string v2, "new_image_url"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->l:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 290947
    const-string v2, "new_image_width"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->j:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 290948
    const-string v2, "new_image_height"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->k:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 290949
    const-string v2, "cache_key_change_count"

    iget-object v3, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1f7;->o:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 290950
    iget-object v2, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1f7;->n:LX/0Tn;

    invoke-interface {v2, v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    iget-object v4, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1f7;->i:LX/0Tn;

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 290951
    const-string v4, "cache_key_change_duration"

    invoke-virtual {v1, v4, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 290952
    :cond_0
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 290953
    :cond_1
    iget-object v1, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1f7;->c:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 290954
    iget-object v1, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1f7;->d:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 290955
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5

    .prologue
    .line 290905
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 290906
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->o:LX/0Tn;

    iget-object v2, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1f7;->o:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 290907
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->n:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290908
    :goto_0
    return-void

    .line 290909
    :cond_0
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->m:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->l:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->j:LX/0Tn;

    invoke-interface {v0, v1, p3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->k:LX/0Tn;

    invoke-interface {v0, v1, p4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->n:LX/0Tn;

    iget-object v2, p0, LX/1f7;->s:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 290910
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 290931
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 290932
    iget-object v0, p0, LX/1f7;->t:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit8 v0, v0, 0x1e

    if-eqz v0, :cond_1

    .line 290933
    :goto_1
    return-void

    .line 290934
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 290935
    :cond_1
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->d:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->h:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->g:LX/0Tn;

    invoke-interface {v0, v1, p3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->e:LX/0Tn;

    invoke-interface {v0, v1, p4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->f:LX/0Tn;

    invoke-interface {v0, v1, p5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->i:LX/0Tn;

    iget-object v2, p0, LX/1f7;->s:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 290936
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;LX/1bf;II)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 290914
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/1f7;->r:LX/1Ao;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v2

    invoke-interface {v2}, LX/1bh;->toString()Ljava/lang/String;

    move-result-object v2

    .line 290915
    iget-object v3, p2, LX/1bf;->b:Landroid/net/Uri;

    move-object v3, v3

    .line 290916
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 290917
    iget-object v4, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1f7;->d:LX/0Tn;

    invoke-interface {v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v0

    .line 290918
    :goto_0
    if-eqz v4, :cond_2

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v5, p4

    .line 290919
    invoke-direct/range {v0 .. v5}, LX/1f7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290920
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v4, v1

    .line 290921
    goto :goto_0

    .line 290922
    :cond_2
    :try_start_1
    iget-object v4, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1f7;->i:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 290923
    iget-object v6, p0, LX/1f7;->s:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    sget-wide v6, LX/1f7;->a:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 290924
    :goto_2
    if-eqz v0, :cond_4

    .line 290925
    invoke-direct {p0}, LX/1f7;->a()V

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v5, p4

    .line 290926
    invoke-direct/range {v0 .. v5}, LX/1f7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 290927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    .line 290928
    goto :goto_2

    .line 290929
    :cond_4
    :try_start_2
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->d:LX/0Tn;

    const-string v4, ""

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->e:LX/0Tn;

    const/4 v4, -0x1

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    if-ne p3, v0, :cond_0

    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->f:LX/0Tn;

    const/4 v4, -0x1

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    if-ne p4, v0, :cond_0

    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1f7;->h:LX/0Tn;

    const-string v4, ""

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290930
    invoke-direct {p0, v2, v3, p3, p4}, LX/1f7;->a(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized clearUserData()V
    .locals 2

    .prologue
    .line 290911
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1f7;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1f7;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290912
    monitor-exit p0

    return-void

    .line 290913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
