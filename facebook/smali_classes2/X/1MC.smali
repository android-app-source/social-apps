.class public LX/1MC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

.field public final b:LX/1MD;

.field public final c:LX/0gd;

.field private final d:LX/03V;

.field private final e:LX/0SG;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;LX/1MD;LX/0gd;LX/03V;LX/0SG;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 234872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234873
    iput-object p1, p0, LX/1MC;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    .line 234874
    iput-object p2, p0, LX/1MC;->b:LX/1MD;

    .line 234875
    iput-object p3, p0, LX/1MC;->c:LX/0gd;

    .line 234876
    iput-object p4, p0, LX/1MC;->d:LX/03V;

    .line 234877
    iput-object p5, p0, LX/1MC;->e:LX/0SG;

    .line 234878
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/32 v12, 0x93a80

    const/16 v5, 0x46

    const/4 v1, 0x0

    .line 234879
    iget-object v0, p0, LX/1MC;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    .line 234880
    iget-object v2, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->i:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    move-object v2, v2

    .line 234881
    iget-object v0, p0, LX/1MC;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-virtual {v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b()V

    .line 234882
    if-nez v2, :cond_0

    move-object v0, v1

    .line 234883
    :goto_0
    return-object v0

    .line 234884
    :cond_0
    iget v0, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    if-eq v0, v5, :cond_1

    .line 234885
    iget-object v0, p0, LX/1MC;->d:LX/03V;

    const-string v3, "composer_session_version_not_current"

    const-string v4, "currentVersion=%s, sessionVersion=%s"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget v2, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 234886
    goto :goto_0

    .line 234887
    :cond_1
    iget-object v0, p0, LX/1MC;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 234888
    iget-wide v6, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 234889
    cmp-long v0, v6, v12

    if-lez v0, :cond_2

    .line 234890
    iget-object v0, p0, LX/1MC;->d:LX/03V;

    const-string v3, "composer_session_expired"

    const-string v8, "now=%dms, sessionTime=%dms, elapsed=%ds, expiration=%ds"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v10, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v8, v4, v2, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 234891
    goto :goto_0

    .line 234892
    :cond_2
    iget-object v0, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v0

    .line 234893
    const/4 v3, 0x3

    if-lt v0, v3, :cond_3

    .line 234894
    iget-object v2, p0, LX/1MC;->d:LX/03V;

    const-string v3, "composer_session_max_load_attempts_reached"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "loaded "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " times"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 234895
    goto :goto_0

    .line 234896
    :cond_3
    new-instance v1, LX/HvJ;

    invoke-direct {v1, v2}, LX/HvJ;-><init>(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    iget-object v2, v2, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setSavedSessionLoadAttempts(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    .line 234897
    iput-object v0, v1, LX/HvJ;->c:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 234898
    move-object v0, v1

    .line 234899
    invoke-virtual {v0}, LX/HvJ;->a()Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    move-result-object v0

    goto/16 :goto_0
.end method
