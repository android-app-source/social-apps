.class public LX/0Tv;
.super LX/0Tw;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Tz;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "LX/0Tz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63902
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;Z)V

    .line 63903
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/0Px;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "LX/0Tz;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 63898
    invoke-direct {p0, p1, p2}, LX/0Tw;-><init>(Ljava/lang/String;I)V

    .line 63899
    iput-object p3, p0, LX/0Tv;->a:LX/0Px;

    .line 63900
    iput-boolean p4, p0, LX/0Tv;->b:Z

    .line 63901
    return-void
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 63894
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 63895
    invoke-virtual {v0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63896
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63897
    :cond_0
    return-void
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 63904
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 63905
    invoke-virtual {v0, p1, p2, p3}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 63906
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63907
    :cond_0
    return-void
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 63880
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 63881
    invoke-virtual {v0, p1}, LX/0Tz;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63883
    :cond_0
    return-void
.end method

.method public b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 63890
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 63891
    invoke-virtual {v0, p1}, LX/0Tz;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63892
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63893
    :cond_0
    return-void
.end method

.method public c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 63884
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 63885
    invoke-virtual {v0, p1}, LX/0Tz;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63886
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63887
    :cond_0
    iget-boolean v0, p0, LX/0Tv;->b:Z

    if-eqz v0, :cond_1

    .line 63888
    const-string v0, "PRAGMA synchronous=OFF"

    const v1, 0xe924efb

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x1b8123e6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 63889
    :cond_1
    return-void
.end method
