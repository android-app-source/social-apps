.class public final LX/0UQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Up;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Up;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 64581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64582
    iput-object p1, p0, LX/0UQ;->a:LX/0QB;

    .line 64583
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64579
    new-instance v0, LX/0UQ;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0UQ;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 64580
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64567
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0UQ;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64569
    packed-switch p2, :pswitch_data_0

    .line 64570
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64571
    :pswitch_0
    invoke-static {p1}, LX/0V1;->a(LX/0QB;)LX/0V1;

    move-result-object v0

    .line 64572
    :goto_0
    return-object v0

    .line 64573
    :pswitch_1
    new-instance v1, LX/0VR;

    invoke-static {p1}, LX/0VS;->a(LX/0QB;)LX/0VS;

    move-result-object v0

    check-cast v0, LX/0VS;

    invoke-direct {v1, v0}, LX/0VR;-><init>(LX/0VS;)V

    .line 64574
    move-object v0, v1

    .line 64575
    goto :goto_0

    .line 64576
    :pswitch_2
    new-instance v1, LX/0VW;

    invoke-static {p1}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v0

    check-cast v0, LX/00G;

    invoke-direct {v1, v0}, LX/0VW;-><init>(LX/00G;)V

    .line 64577
    move-object v0, v1

    .line 64578
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64568
    const/4 v0, 0x3

    return v0
.end method
