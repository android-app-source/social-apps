.class public LX/1JH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public a:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0pG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1JR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/1JS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/1JT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/Bny;

.field public k:LX/Bnz;

.field private final l:[LX/1JI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230039
    const-class v0, LX/1JH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1JH;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 230034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230035
    const/4 v0, 0x7

    new-array v0, v0, [LX/1JI;

    const/4 v1, 0x0

    new-instance v2, LX/1JJ;

    invoke-direct {v2, p0}, LX/1JJ;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, LX/1JK;

    invoke-direct {v2, p0}, LX/1JK;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/1JL;

    invoke-direct {v2, p0}, LX/1JL;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LX/1JM;

    invoke-direct {v2, p0}, LX/1JM;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, LX/1JO;

    invoke-direct {v2, p0}, LX/1JO;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, LX/1JP;

    invoke-direct {v2, p0}, LX/1JP;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, LX/1JQ;

    invoke-direct {v2, p0}, LX/1JQ;-><init>(LX/1JH;)V

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1JH;->l:[LX/1JI;

    .line 230036
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/1JH;->h:Ljava/util/LinkedHashMap;

    .line 230037
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/1JH;->i:Ljava/util/LinkedHashMap;

    .line 230038
    return-void
.end method

.method public static a(LX/0QB;)LX/1JH;
    .locals 7

    .prologue
    .line 230029
    new-instance v0, LX/1JH;

    invoke-direct {v0}, LX/1JH;-><init>()V

    .line 230030
    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0pG;->a(LX/0QB;)LX/0pG;

    move-result-object v3

    check-cast v3, LX/0pG;

    invoke-static {p0}, LX/1JR;->b(LX/0QB;)LX/1JR;

    move-result-object v4

    check-cast v4, LX/1JR;

    const-class v5, LX/1JS;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1JS;

    const-class v6, LX/1JT;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1JT;

    .line 230031
    iput-object v1, v0, LX/1JH;->a:LX/0Sh;

    iput-object v2, v0, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v0, LX/1JH;->c:LX/0pG;

    iput-object v4, v0, LX/1JH;->d:LX/1JR;

    iput-object v5, v0, LX/1JH;->f:LX/1JS;

    iput-object v6, v0, LX/1JH;->g:LX/1JT;

    .line 230032
    move-object v0, v0

    .line 230033
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1

    .prologue
    .line 230028
    invoke-static {p0}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(LX/1JH;)V
    .locals 3

    .prologue
    .line 230026
    iget-object v0, p0, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$13;

    invoke-direct {v1, p0}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$13;-><init>(LX/1JH;)V

    const v2, 0xa5eeced

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 230027
    return-void
.end method

.method public static k(LX/1JH;)V
    .locals 7

    .prologue
    .line 229987
    iget-object v0, p0, LX/1JH;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 229988
    invoke-virtual {p0}, LX/1JH;->g()LX/Bny;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/1JH;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 229989
    iget-object v2, v0, LX/Bny;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 229990
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 229991
    invoke-interface {v3, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 229992
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 229993
    invoke-interface {v4, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 229994
    iget-object v2, v0, LX/Bny;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    .line 229995
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229996
    :goto_0
    iget-object v2, v0, LX/Bny;->d:LX/1JR;

    invoke-virtual {v2}, LX/1JR;->h()I

    move-result v2

    .line 229997
    iget-object v4, v0, LX/Bny;->b:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    .line 229998
    sub-int/2addr v2, v4

    .line 229999
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    if-lez v2, :cond_0

    iget-object v4, v0, LX/Bny;->h:LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->j()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 230000
    :cond_0
    return-void

    .line 230001
    :cond_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 230002
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 230003
    iget-object v1, v0, LX/Bny;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 230004
    :cond_2
    iget-object v2, v0, LX/Bny;->g:LX/0gX;

    invoke-virtual {v2, v5}, LX/0gX;->a(Ljava/util/Set;)V

    goto :goto_0

    .line 230005
    :cond_3
    const/4 v4, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v3, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 230006
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 230007
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 230008
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 230009
    new-instance p0, LX/4DO;

    invoke-direct {p0}, LX/4DO;-><init>()V

    invoke-virtual {p0, v2}, LX/4DO;->a(Ljava/lang/String;)LX/4DO;

    move-result-object p0

    .line 230010
    invoke-static {}, LX/58t;->a()LX/58n;

    move-result-object v1

    .line 230011
    const-string v3, "input"

    invoke-virtual {v1, v3, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 230012
    iget-object p0, v0, LX/Bny;->e:LX/0tE;

    invoke-virtual {p0, v1}, LX/0tE;->b(LX/0gW;)V

    .line 230013
    iget-object p0, v0, LX/Bny;->f:LX/0tG;

    invoke-virtual {p0, v1}, LX/0tG;->a(LX/0gW;)V

    .line 230014
    move-object p0, v1

    .line 230015
    invoke-interface {v5, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230016
    iget-object v2, v0, LX/Bny;->i:LX/0TF;

    invoke-interface {v4, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 230017
    :cond_4
    iget-object v2, v0, LX/Bny;->g:LX/0gX;

    invoke-virtual {v2, v4}, LX/0gX;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    .line 230018
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 230019
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2o1;

    .line 230020
    iget-object p0, v4, LX/2o1;->a:LX/0gM;

    move-object p0, p0

    .line 230021
    if-eqz p0, :cond_5

    .line 230022
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 230023
    iget-object p0, v0, LX/Bny;->b:Ljava/util/Map;

    .line 230024
    iget-object v1, v4, LX/2o1;->a:LX/0gM;

    move-object v4, v1

    .line 230025
    invoke-interface {p0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 229986
    iget-object v0, p0, LX/1JH;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/util/List;)Z
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 229972
    iget-object v0, p0, LX/1JH;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 229973
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 229974
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1JH;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 229975
    iget-object v5, p0, LX/1JH;->l:[LX/1JI;

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v7, v5, v3

    .line 229976
    invoke-interface {v7, v0}, LX/1JI;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 229977
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 229978
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    .line 229979
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/1JH;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    move-object v0, v5

    .line 229980
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 229981
    iget-object v5, p0, LX/1JH;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v0, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229982
    iget-object v5, p0, LX/1JH;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229983
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 229984
    goto :goto_0

    .line 229985
    :cond_2
    if-lez v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    return v2
.end method

.method public final g()LX/Bny;
    .locals 10
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 229956
    iget-object v0, p0, LX/1JH;->j:LX/Bny;

    if-nez v0, :cond_1

    .line 229957
    iget-object v0, p0, LX/1JH;->f:LX/1JS;

    .line 229958
    iget-object v1, p0, LX/1JH;->k:LX/Bnz;

    if-nez v1, :cond_0

    .line 229959
    iget-object v1, p0, LX/1JH;->g:LX/1JT;

    .line 229960
    new-instance v2, LX/Bnz;

    invoke-direct {v2, p0}, LX/Bnz;-><init>(LX/1JH;)V

    .line 229961
    const/16 v3, 0xbc

    invoke-static {v1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v1}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    invoke-static {v1}, LX/0pG;->a(LX/0QB;)LX/0pG;

    move-result-object v5

    check-cast v5, LX/0pG;

    invoke-static {v1}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v6

    check-cast v6, LX/0Ym;

    invoke-static {v1}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v7

    check-cast v7, LX/0pn;

    invoke-static {v1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    const/16 v9, 0x1c88

    invoke-static {v1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 229962
    iput-object v3, v2, LX/Bnz;->c:LX/0Ot;

    iput-object v4, v2, LX/Bnz;->d:Lcom/facebook/user/model/User;

    iput-object v5, v2, LX/Bnz;->e:LX/0pG;

    iput-object v6, v2, LX/Bnz;->f:LX/0Ym;

    iput-object v7, v2, LX/Bnz;->g:LX/0pn;

    iput-object v8, v2, LX/Bnz;->h:Ljava/util/concurrent/ExecutorService;

    iput-object v9, v2, LX/Bnz;->i:LX/0Ot;

    .line 229963
    move-object v1, v2

    .line 229964
    iput-object v1, p0, LX/1JH;->k:LX/Bnz;

    .line 229965
    :cond_0
    iget-object v1, p0, LX/1JH;->k:LX/Bnz;

    move-object v1, v1

    .line 229966
    new-instance v2, LX/Bny;

    invoke-direct {v2, v1}, LX/Bny;-><init>(LX/Bnz;)V

    .line 229967
    invoke-static {v0}, LX/1JR;->b(LX/0QB;)LX/1JR;

    move-result-object v3

    check-cast v3, LX/1JR;

    invoke-static {v0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v4

    check-cast v4, LX/0tE;

    invoke-static {v0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v5

    check-cast v5, LX/0tG;

    invoke-static {v0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v6

    check-cast v6, LX/0gX;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    .line 229968
    iput-object v3, v2, LX/Bny;->d:LX/1JR;

    iput-object v4, v2, LX/Bny;->e:LX/0tE;

    iput-object v5, v2, LX/Bny;->f:LX/0tG;

    iput-object v6, v2, LX/Bny;->g:LX/0gX;

    iput-object v7, v2, LX/Bny;->h:LX/0Uo;

    .line 229969
    move-object v0, v2

    .line 229970
    iput-object v0, p0, LX/1JH;->j:LX/Bny;

    .line 229971
    :cond_1
    iget-object v0, p0, LX/1JH;->j:LX/Bny;

    return-object v0
.end method
