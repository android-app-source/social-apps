.class public LX/17S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Landroid/content/pm/PackageManager;

.field private final b:LX/17T;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/ufiservices/UriIntentGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/17U;

.field private final e:LX/0Zb;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:LX/0Uh;

.field private final h:LX/17W;

.field private final i:LX/17d;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17T;LX/0Ot;LX/17U;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/17W;LX/17d;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/17T;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/ufiservices/UriIntentGenerator;",
            ">;",
            "LX/17U;",
            "LX/0Zb;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/17W;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 197941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197942
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/17S;->a:Landroid/content/pm/PackageManager;

    .line 197943
    iput-object p2, p0, LX/17S;->b:LX/17T;

    .line 197944
    iput-object p3, p0, LX/17S;->c:LX/0Ot;

    .line 197945
    iput-object p4, p0, LX/17S;->d:LX/17U;

    .line 197946
    iput-object p5, p0, LX/17S;->e:LX/0Zb;

    .line 197947
    iput-object p6, p0, LX/17S;->f:Ljava/util/concurrent/ExecutorService;

    .line 197948
    iput-object p7, p0, LX/17S;->g:LX/0Uh;

    .line 197949
    iput-object p8, p0, LX/17S;->h:LX/17W;

    .line 197950
    iput-object p9, p0, LX/17S;->i:LX/17d;

    .line 197951
    return-void
.end method

.method public static a(LX/0QB;)LX/17S;
    .locals 13

    .prologue
    .line 197930
    const-class v1, LX/17S;

    monitor-enter v1

    .line 197931
    :try_start_0
    sget-object v0, LX/17S;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 197932
    sput-object v2, LX/17S;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 197933
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197934
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 197935
    new-instance v3, LX/17S;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v5

    check-cast v5, LX/17T;

    const/16 v6, 0x255e

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/17U;->a(LX/0QB;)LX/17U;

    move-result-object v7

    check-cast v7, LX/17U;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v12

    check-cast v12, LX/17d;

    invoke-direct/range {v3 .. v12}, LX/17S;-><init>(Landroid/content/Context;LX/17T;LX/0Ot;LX/17U;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/17W;LX/17d;)V

    .line 197936
    move-object v0, v3

    .line 197937
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 197938
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/17S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197939
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 197940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(ZLjava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;
    .locals 1

    .prologue
    .line 197929
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/17S;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v0

    return-object v0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;
    .locals 1

    .prologue
    .line 197928
    new-instance v0, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/17S;I)Z
    .locals 2

    .prologue
    .line 197927
    iget-object v0, p0, LX/17S;->g:LX/0Uh;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 197926
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "124024574287414"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;
    .locals 5
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 197902
    if-nez p3, :cond_1

    move-object v0, v1

    .line 197903
    :cond_0
    :goto_0
    return-object v0

    .line 197904
    :cond_1
    sget-object v0, LX/99u;->a:[I

    invoke-virtual {p2}, LX/99r;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 197905
    const/4 v0, 0x0

    :goto_1
    move v0, v0

    .line 197906
    if-eqz v0, :cond_2

    .line 197907
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 197908
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 197909
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p2

    if-eqz p2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jz()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object p2

    if-eqz p2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jz()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;->a()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_5

    .line 197910
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jz()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;->a()Ljava/lang/String;

    move-result-object v0

    .line 197911
    :goto_3
    move-object v0, v0

    .line 197912
    if-eqz v0, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://instagram.com/_uid/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v2, v0

    .line 197913
    if-eqz v2, :cond_2

    .line 197914
    iget-object v0, p0, LX/17S;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qo;

    invoke-virtual {v0, v2}, LX/5Qo;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 197915
    const-string v2, "com.instagram.android"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 197916
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 197917
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197918
    :cond_2
    invoke-virtual {p0}, LX/17S;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197919
    invoke-static {p3}, LX/14w;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 197920
    if-eqz v2, :cond_3

    .line 197921
    iget-object v0, p0, LX/17S;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qo;

    invoke-virtual {v0, v2}, LX/5Qo;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    .line 197922
    goto/16 :goto_0

    .line 197923
    :pswitch_0
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    .line 197924
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 197925
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/99r;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 197897
    iget-object v0, p0, LX/17S;->a:Landroid/content/pm/PackageManager;

    const-string v1, "com.instagram.android"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 197898
    iget-object v0, p0, LX/17S;->d:LX/17U;

    const/4 v4, 0x0

    move-object v2, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, LX/17U;->a(Landroid/content/Intent;Landroid/content/Context;LX/0lF;ZLX/1vY;)V

    .line 197899
    new-instance v0, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;

    invoke-direct {v0, p2, v3, v3}, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;-><init>(LX/99r;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 197900
    invoke-virtual {p0, p1, v0}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 197901
    return-void
.end method

.method public final a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197893
    iget-object v0, p0, LX/17S;->b:LX/17T;

    const-string v1, "com.instagram.android"

    invoke-virtual {v0, p1, v1}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 197894
    new-instance v0, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;-><init>(LX/99r;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 197895
    invoke-virtual {p0, p1, v0}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 197896
    return-void
.end method

.method public final a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;LX/0lF;)V
    .locals 7
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 197952
    invoke-direct {p0, p1, p2, p3}, LX/17S;->b(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 197953
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    move v6, v0

    .line 197954
    :goto_0
    if-eqz v6, :cond_2

    .line 197955
    iget-object v0, p0, LX/17S;->d:LX/17U;

    move-object v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/17U;->a(Landroid/content/Intent;Landroid/content/Context;LX/0lF;ZLX/1vY;)V

    .line 197956
    :goto_1
    if-eqz v6, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 197957
    :cond_0
    new-instance v0, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;

    invoke-direct {v0, p2, p3, v5}, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;-><init>(LX/99r;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 197958
    invoke-virtual {p0, p1, v0}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 197959
    return-void

    :cond_1
    move v6, v4

    .line 197960
    goto :goto_0

    .line 197961
    :cond_2
    iget-object v0, p0, LX/17S;->b:LX/17T;

    const-string v2, "com.instagram.android"

    invoke-virtual {v0, p1, v2}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 197848
    iget-object v0, p0, LX/17S;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;

    iget-object v2, p0, LX/17S;->e:LX/0Zb;

    invoke-direct {v1, p1, v2, p2}, Lcom/facebook/feed/thirdparty/instagram/InstagramAttributionLogRunnable;-><init>(Landroid/content/Context;LX/0Zb;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    const v2, 0x61325ba

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 197849
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;LX/99r;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 197850
    invoke-static {p2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197851
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v0

    .line 197852
    iput-object p2, v0, LX/47H;->a:Ljava/lang/String;

    .line 197853
    move-object v0, v0

    .line 197854
    iput-object v2, v0, LX/47H;->d:LX/47G;

    .line 197855
    move-object v0, v0

    .line 197856
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 197857
    iget-object v1, p0, LX/17S;->h:LX/17W;

    invoke-virtual {v1, p1, v0}, LX/17W;->a(Landroid/content/Context;LX/47I;)Z

    .line 197858
    :cond_0
    new-instance v0, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;

    invoke-direct {v0, p3, v2, p2}, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramClickEvent;-><init>(LX/99r;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 197859
    invoke-virtual {p0, p1, v0}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 197860
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 197861
    iget-object v0, p0, LX/17S;->a:Landroid/content/pm/PackageManager;

    const-string v1, "com.instagram.android"

    invoke-static {v0, v1}, LX/32v;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/99r;)Z
    .locals 2

    .prologue
    .line 197862
    invoke-virtual {p0}, LX/17S;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197863
    sget-object v0, LX/99u;->a:[I

    invoke-virtual {p1}, LX/99r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 197864
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 197865
    :goto_1
    if-eqz v0, :cond_0

    .line 197866
    invoke-virtual {p0, p1}, LX/17S;->b(LX/99r;)V

    .line 197867
    :cond_0
    return v0

    .line 197868
    :cond_1
    sget-object v0, LX/99u;->a:[I

    invoke-virtual {p1}, LX/99r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 197869
    const/4 v0, 0x0

    :goto_2
    move v0, v0

    .line 197870
    goto :goto_1

    .line 197871
    :pswitch_0
    sget v0, LX/1TU;->q:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    goto :goto_0

    .line 197872
    :pswitch_1
    sget v0, LX/1TU;->t:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    goto :goto_0

    .line 197873
    :pswitch_2
    sget v0, LX/1TU;->u:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b(LX/99r;)V
    .locals 2

    .prologue
    .line 197874
    invoke-virtual {p1}, LX/99r;->shouldLogImpression()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197875
    iget-object v0, p0, LX/17S;->e:LX/0Zb;

    new-instance v1, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramImpressionEvent;

    invoke-direct {v1, p1}, Lcom/facebook/feed/thirdparty/instagram/InstagramUtils$InstagramImpressionEvent;-><init>(LX/99r;)V

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 197876
    :cond_0
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 197877
    sget v0, LX/1TU;->e:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197878
    const/16 v0, 0x63

    .line 197879
    :goto_0
    return v0

    .line 197880
    :cond_0
    sget v0, LX/1TU;->d:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197881
    const/16 v0, 0x4b

    goto :goto_0

    .line 197882
    :cond_1
    sget v0, LX/1TU;->c:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197883
    const/16 v0, 0x32

    goto :goto_0

    .line 197884
    :cond_2
    sget v0, LX/1TU;->b:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197885
    const/16 v0, 0x19

    goto :goto_0

    .line 197886
    :cond_3
    sget v0, LX/1TU;->a:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 197887
    const/16 v0, 0xf

    goto :goto_0

    .line 197888
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 197889
    sget v0, LX/1TU;->v:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 197890
    sget v0, LX/1TU;->w:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 197891
    sget v0, LX/1TU;->g:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 197892
    sget v0, LX/1TU;->h:I

    invoke-static {p0, v0}, LX/17S;->a(LX/17S;I)Z

    move-result v0

    return v0
.end method
