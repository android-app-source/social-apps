.class public LX/0wp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile Q:LX/0wp;


# instance fields
.field public final A:I

.field public final B:I

.field public final C:I

.field public final D:I

.field public final E:Z

.field public final F:Z

.field public final G:Z

.field public final H:I

.field public final I:I

.field public final J:F

.field public final K:F

.field public final L:Z

.field public final M:J

.field public final N:LX/0xt;

.field public final O:LX/0xt;

.field public final P:Z

.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:LX/0xn;

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:F

.field public final x:F

.field public final y:Z

.field public final z:I


# direct methods
.method public constructor <init>(LX/0ad;LX/0wq;LX/0W3;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v7, 0x10000

    const/16 v6, 0x61a8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 160829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160830
    invoke-virtual {p2}, LX/0wq;->e()Z

    move-result v0

    .line 160831
    sget-char v3, LX/0ws;->aY:C

    if-eqz v0, :cond_0

    const-string v0, "MPEG_DASH"

    :goto_0
    invoke-interface {p1, v3, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0wp;->a:Ljava/lang/String;

    .line 160832
    sget v0, LX/0ws;->aK:I

    const/high16 v3, 0x20000

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->e:I

    .line 160833
    sget v0, LX/0ws;->aL:I

    invoke-interface {p1, v0, v7}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->f:I

    .line 160834
    sget v0, LX/0ws;->aW:I

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->g:I

    .line 160835
    goto :goto_1

    :goto_1
    move v0, v2

    iput-boolean v0, p0, LX/0wp;->b:Z

    .line 160836
    sget-short v0, LX/0ws;->au:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->c:Z

    .line 160837
    sget-short v0, LX/0ws;->aA:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->d:Z

    .line 160838
    sget-short v0, LX/0ws;->av:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->h:Z

    .line 160839
    sget-short v0, LX/0ws;->az:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->i:Z

    .line 160840
    sget-short v0, LX/0ws;->ay:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->j:Z

    .line 160841
    sget-short v0, LX/0ws;->aE:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->k:Z

    .line 160842
    sget-object v0, LX/0xn;->CUSTOM_ABR:LX/0xn;

    invoke-virtual {v0}, LX/0xn;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160843
    sget-char v3, LX/0ws;->at:C

    invoke-interface {p1, v3, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160844
    invoke-static {v0}, LX/0xn;->of(Ljava/lang/String;)LX/0xn;

    move-result-object v0

    iput-object v0, p0, LX/0wp;->l:LX/0xn;

    .line 160845
    sget v0, LX/0ws;->am:I

    sget-wide v4, LX/0X5;->hA:J

    invoke-interface {p3, v4, v5, v1}, LX/0W4;->a(JI)I

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->m:I

    .line 160846
    sget v0, LX/0ws;->an:I

    iget v3, p0, LX/0wp;->m:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->o:I

    .line 160847
    sget v0, LX/0ws;->ao:I

    sget-wide v4, LX/0X5;->hB:J

    invoke-interface {p3, v4, v5, v1}, LX/0W4;->a(JI)I

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->n:I

    .line 160848
    sget v0, LX/0ws;->ap:I

    iget v3, p0, LX/0wp;->n:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->p:I

    .line 160849
    sget v0, LX/0ws;->al:I

    sget-wide v4, LX/0X5;->hD:J

    const/16 v3, 0x280

    invoke-interface {p3, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->q:I

    .line 160850
    sget v0, LX/0ws;->ak:I

    sget-wide v4, LX/0X5;->hC:J

    const/16 v3, 0x1aa

    invoke-interface {p3, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->r:I

    .line 160851
    sget v0, LX/0ws;->aN:I

    const v3, 0xc350

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->s:I

    .line 160852
    sget v0, LX/0ws;->aR:I

    const/16 v3, 0x2710

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->t:I

    .line 160853
    sget v0, LX/0ws;->aM:I

    invoke-interface {p1, v0, v6}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->u:I

    .line 160854
    sget v0, LX/0ws;->aS:I

    invoke-interface {p1, v0, v6}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->v:I

    .line 160855
    sget v0, LX/0ws;->ar:F

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-interface {p1, v0, v3}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/0wp;->w:F

    .line 160856
    sget v0, LX/0ws;->aU:F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-interface {p1, v0, v3}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/0wp;->x:F

    .line 160857
    sget-short v0, LX/0ws;->aZ:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->y:Z

    .line 160858
    sget v0, LX/0ws;->aF:I

    const/16 v3, 0xc8

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->z:I

    .line 160859
    sget v0, LX/0ws;->aq:I

    const/16 v3, 0x3c

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->A:I

    .line 160860
    sget v0, LX/0ws;->as:I

    invoke-interface {p1, v0, v7}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->B:I

    .line 160861
    sget v0, LX/0ws;->aO:I

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->C:I

    .line 160862
    sget v0, LX/0ws;->aP:I

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->D:I

    .line 160863
    sget-short v0, LX/0ws;->aD:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->E:Z

    .line 160864
    sget-short v0, LX/0ws;->aV:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->F:Z

    .line 160865
    sget-short v0, LX/0ws;->aw:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->G:Z

    .line 160866
    sget v0, LX/0ws;->aJ:I

    const/16 v3, 0x1770

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->H:I

    .line 160867
    sget v0, LX/0ws;->aH:I

    const/16 v3, 0x2ee0

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wp;->I:I

    .line 160868
    sget v0, LX/0ws;->aI:F

    const v3, 0x3e4ccccd    # 0.2f

    invoke-interface {p1, v0, v3}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/0wp;->J:F

    .line 160869
    sget v0, LX/0ws;->aG:F

    const v3, 0x3f4ccccd    # 0.8f

    invoke-interface {p1, v0, v3}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/0wp;->K:F

    .line 160870
    sget-short v0, LX/0ws;->aB:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->L:Z

    .line 160871
    sget-wide v2, LX/0ws;->aQ:J

    const-wide/16 v4, 0x0

    invoke-interface {p1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LX/0wp;->M:J

    .line 160872
    sget-object v0, LX/0xt;->BANDWIDTH_METER:LX/0xt;

    invoke-virtual {v0}, LX/0xt;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160873
    sget-char v2, LX/0ws;->aT:C

    invoke-interface {p1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 160874
    invoke-static {v2}, LX/0xt;->of(Ljava/lang/String;)LX/0xt;

    move-result-object v2

    iput-object v2, p0, LX/0wp;->N:LX/0xt;

    .line 160875
    sget-char v2, LX/0ws;->aX:C

    invoke-interface {p1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160876
    invoke-static {v0}, LX/0xt;->of(Ljava/lang/String;)LX/0xt;

    move-result-object v0

    iput-object v0, p0, LX/0wp;->O:LX/0xt;

    .line 160877
    sget-short v0, LX/0ws;->aC:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wp;->P:Z

    .line 160878
    return-void

    .line 160879
    :cond_0
    const-string v0, "NONE"

    goto/16 :goto_0
    .line 160880
.end method

.method public static a(LX/0QB;)LX/0wp;
    .locals 6

    .prologue
    .line 160813
    sget-object v0, LX/0wp;->Q:LX/0wp;

    if-nez v0, :cond_1

    .line 160814
    const-class v1, LX/0wp;

    monitor-enter v1

    .line 160815
    :try_start_0
    sget-object v0, LX/0wp;->Q:LX/0wp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 160816
    if-eqz v2, :cond_0

    .line 160817
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 160818
    new-instance p0, LX/0wp;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v4

    check-cast v4, LX/0wq;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/0wp;-><init>(LX/0ad;LX/0wq;LX/0W3;)V

    .line 160819
    move-object v0, p0

    .line 160820
    sput-object v0, LX/0wp;->Q:LX/0wp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160821
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 160822
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 160823
    :cond_1
    sget-object v0, LX/0wp;->Q:LX/0wp;

    return-object v0

    .line 160824
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 160825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ka;Z)I
    .locals 1

    .prologue
    .line 160828
    if-eqz p2, :cond_1

    invoke-virtual {p1}, LX/0ka;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/0wp;->o:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0wp;->p:I

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LX/0ka;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/0wp;->m:I

    goto :goto_0

    :cond_2
    iget v0, p0, LX/0wp;->n:I

    goto :goto_0
.end method

.method public final a(LX/0ka;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 160881
    iget-boolean v1, p0, LX/0wp;->j:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/0wp;->i:Z

    if-eqz v1, :cond_3

    .line 160882
    :cond_0
    invoke-virtual {p1}, LX/0ka;->b()Z

    move-result v1

    .line 160883
    if-eqz v1, :cond_1

    iget-boolean v2, p0, LX/0wp;->i:Z

    if-nez v2, :cond_2

    :cond_1
    if-nez v1, :cond_3

    iget-boolean v1, p0, LX/0wp;->j:Z

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 160884
    :cond_3
    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 160827
    iget-object v0, p0, LX/0wp;->a:Ljava/lang/String;

    const-string v1, "WEBM_DASH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 160826
    iget-object v0, p0, LX/0wp;->l:LX/0xn;

    sget-object v1, LX/0xn;->MANUAL:LX/0xn;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
