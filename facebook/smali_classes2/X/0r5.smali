.class public LX/0r5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0pV;

.field private final c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final d:LX/0r4;

.field private e:J

.field public final f:LX/0Uo;

.field public g:LX/0r6;

.field public h:LX/0r7;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149023
    const-class v0, LX/0r5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0r5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0pV;Landroid/os/Handler;LX/0Uo;LX/0r4;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/0r4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149025
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0r5;->e:J

    .line 149026
    new-instance v0, Lcom/facebook/feed/loader/AutoRefreshScheduler$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/loader/AutoRefreshScheduler$1;-><init>(LX/0r5;)V

    iput-object v0, p0, LX/0r5;->i:Ljava/lang/Runnable;

    .line 149027
    iput-object p1, p0, LX/0r5;->b:LX/0pV;

    .line 149028
    iput-object p3, p0, LX/0r5;->f:LX/0Uo;

    .line 149029
    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149030
    iput-object p2, p0, LX/0r5;->c:Landroid/os/Handler;

    .line 149031
    iput-object p4, p0, LX/0r5;->d:LX/0r4;

    .line 149032
    sget-object v0, LX/0r6;->OFF:LX/0r6;

    iput-object v0, p0, LX/0r5;->g:LX/0r6;

    .line 149033
    sget-object v0, LX/0r7;->a:LX/0r7;

    move-object v0, v0

    .line 149034
    iput-object v0, p0, LX/0r5;->h:LX/0r7;

    .line 149035
    return-void
.end method

.method private a(JZJJ)V
    .locals 10

    .prologue
    .line 149021
    iget-object v0, p0, LX/0r5;->h:LX/0r7;

    iget-wide v8, p0, LX/0r5;->e:J

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    invoke-virtual/range {v0 .. v9}, LX/0r7;->a(JZJJJ)V

    .line 149022
    return-void
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 149015
    sget-object v0, LX/0r6;->ON:LX/0r6;

    iput-object v0, p0, LX/0r5;->g:LX/0r6;

    .line 149016
    iget-object v0, p0, LX/0r5;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0r5;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 149017
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 149018
    iget-object v0, p0, LX/0r5;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0r5;->i:Ljava/lang/Runnable;

    const v2, -0x46f1647f

    invoke-static {v0, v1, p1, p2, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 149019
    :goto_0
    return-void

    .line 149020
    :cond_0
    iget-object v0, p0, LX/0r5;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private c(JZJ)J
    .locals 7
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 149036
    iget-wide v0, p0, LX/0r5;->e:J

    sub-long v0, p1, v0

    .line 149037
    if-eqz p3, :cond_0

    .line 149038
    sub-long v2, p1, p4

    .line 149039
    iget-object v4, p0, LX/0r5;->d:LX/0r4;

    invoke-interface {v4}, LX/0r4;->c()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 149040
    iget-object v4, p0, LX/0r5;->d:LX/0r4;

    invoke-interface {v4}, LX/0r4;->d()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 149041
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 149042
    :goto_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0

    .line 149043
    :cond_0
    iget-object v2, p0, LX/0r5;->d:LX/0r4;

    invoke-interface {v2}, LX/0r4;->b()J

    move-result-wide v2

    sub-long v0, v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 149013
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, LX/0r5;->b(J)V

    .line 149014
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 149009
    iput-wide p1, p0, LX/0r5;->e:J

    .line 149010
    iget-object v1, p0, LX/0r5;->h:LX/0r7;

    .line 149011
    iput-wide p1, v1, LX/0r7;->f:J

    .line 149012
    return-void
.end method

.method public final a(JZJ)Z
    .locals 4

    .prologue
    .line 149008
    invoke-direct/range {p0 .. p5}, LX/0r5;->c(JZJ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 148998
    sget-object v0, LX/0r6;->OFF:LX/0r6;

    iput-object v0, p0, LX/0r5;->g:LX/0r6;

    .line 148999
    iget-object v0, p0, LX/0r5;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0r5;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 149000
    return-void
.end method

.method public final b(JZJ)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    .line 149001
    invoke-direct/range {p0 .. p5}, LX/0r5;->c(JZJ)J

    move-result-wide v1

    .line 149002
    invoke-direct {p0, v1, v2}, LX/0r5;->b(J)V

    .line 149003
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149004
    const-string v3, "schedule:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-long v4, v1, v6

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hitPrevious:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sinceFetch:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, LX/0r5;->e:J

    sub-long v4, p1, v4

    div-long/2addr v4, v6

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sinceHit:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, p1, p4

    div-long/2addr v4, v6

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149005
    iget-object v3, p0, LX/0r5;->b:LX/0pV;

    sget-object v4, LX/0r5;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move v3, p3

    move-wide v4, p1

    move-wide v6, p4

    .line 149006
    invoke-direct/range {v0 .. v7}, LX/0r5;->a(JZJJ)V

    .line 149007
    const-wide/16 v4, 0x0

    cmp-long v0, v1, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
