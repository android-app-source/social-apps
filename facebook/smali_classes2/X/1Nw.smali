.class public final LX/1Nw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 238411
    iput-object p1, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 238412
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    if-eqz v0, :cond_1

    .line 238413
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, v2}, LX/1O1;->setAlpha(I)V

    .line 238414
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->start()V

    .line 238415
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->D:Z

    if-eqz v0, :cond_0

    .line 238416
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:LX/1PH;

    if-eqz v0, :cond_0

    .line 238417
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:LX/1PH;

    invoke-interface {v0}, LX/1PH;->a()V

    .line 238418
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v1}, LX/1Nz;->getTop()I

    move-result v1

    .line 238419
    iput v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    .line 238420
    return-void

    .line 238421
    :cond_1
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->stop()V

    .line 238422
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/1Nz;->setVisibility(I)V

    .line 238423
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorViewAlpha(Landroid/support/v4/widget/SwipeRefreshLayout;I)V

    .line 238424
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-eqz v0, :cond_2

    .line 238425
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setAnimationProgress(Landroid/support/v4/widget/SwipeRefreshLayout;F)V

    goto :goto_0

    .line 238426
    :cond_2
    iget-object v0, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iget-object v2, p0, LX/1Nw;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v2, v2, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    .line 238427
    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    .line 238428
    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 238429
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 238430
    return-void
.end method
