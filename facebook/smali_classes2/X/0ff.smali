.class public LX/0ff;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/regex/Pattern;


# instance fields
.field public final b:Ljava/util/regex/Matcher;

.field public final c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109332
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "([a-z]+(?:_[A-Z]+)?)-([0-9]+)(?:-(\\w+))?\\.("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0eh;->getMatchAnyPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")(?:\\.("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LX/0gQ;->getMatchAnyPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))?(?:\\.("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LX/0gR;->getMatchAnyPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0ff;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 109384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109385
    iput-object p1, p0, LX/0ff;->c:Ljava/lang/String;

    .line 109386
    sget-object v0, LX/0ff;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    .line 109387
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    .line 109388
    const/4 v1, 0x0

    .line 109389
    :try_start_0
    invoke-virtual {p0}, LX/0ff;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0ff;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0ff;->i()LX/0gR;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0ff;->i()LX/0gR;

    move-result-object v0

    sget-object v2, LX/0gR;->NONE:LX/0gR;

    if-eq v0, v2, :cond_1

    const/4 v0, 0x0

    .line 109390
    invoke-static {p0}, LX/0ff;->s(LX/0ff;)[Ljava/lang/String;

    move-result-object v2

    .line 109391
    if-eqz v2, :cond_0

    array-length v3, v2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 109392
    :cond_0
    :goto_0
    move-object v0, v0

    .line 109393
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/0ff;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109394
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 109395
    goto :goto_1

    .line 109396
    :catch_0
    iput-boolean v1, p0, LX/0ff;->d:Z

    goto :goto_2

    .line 109397
    :cond_2
    array-length v4, v2

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v4, :cond_3

    aget-object p1, v2, v3

    .line 109398
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-eqz p1, :cond_0

    .line 109399
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v2

    .line 109400
    goto :goto_0
.end method

.method public static a(LX/0ff;LX/0gR;)LX/0ff;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109381
    invoke-virtual {p0}, LX/0ff;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109382
    const/4 v0, 0x0

    .line 109383
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0ff;

    iget-object v1, p0, LX/0ff;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\\."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0gQ;->DELTA:LX/0gQ;

    invoke-virtual {v3}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".*$"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/0gQ;->DELTA:LX/0gQ;

    invoke-virtual {v4}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, LX/0gR;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ff;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;JLjava/lang/String;LX/0gQ;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 109377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0eh;->LANGPACK:LX/0eh;

    invoke-virtual {v1}, LX/0eh;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109378
    sget-object v1, LX/0gQ;->NONE:LX/0gQ;

    if-eq p4, v1, :cond_0

    .line 109379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109380
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 109376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static s(LX/0ff;)[Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 109373
    invoke-virtual {p0}, LX/0ff;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 109374
    :cond_0
    const/4 v0, 0x0

    .line 109375
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 109372
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 109369
    invoke-virtual {p0}, LX/0ff;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109370
    const/4 v0, 0x0

    .line 109371
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109368
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 109361
    if-ne p0, p1, :cond_0

    .line 109362
    const/4 v0, 0x1

    .line 109363
    :goto_0
    return v0

    .line 109364
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 109365
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 109366
    :cond_2
    check-cast p1, LX/0ff;

    .line 109367
    iget-object v0, p0, LX/0ff;->c:Ljava/lang/String;

    iget-object v1, p1, LX/0ff;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109401
    invoke-virtual {p0}, LX/0ff;->h()LX/0gQ;

    move-result-object v0

    sget-object v1, LX/0gQ;->DELTA:LX/0gQ;

    if-eq v0, v1, :cond_0

    .line 109402
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 109403
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0eh;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109358
    invoke-virtual {p0}, LX/0ff;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109359
    const/4 v0, 0x0

    .line 109360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0eh;->fromFormat(Ljava/lang/String;)LX/0eh;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()LX/0gQ;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109355
    invoke-virtual {p0}, LX/0ff;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109356
    const/4 v0, 0x0

    .line 109357
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0gQ;->fromAnnotation(Ljava/lang/String;)LX/0gQ;

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 109354
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0ff;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()LX/0gR;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109351
    invoke-virtual {p0}, LX/0ff;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109352
    const/4 v0, 0x0

    .line 109353
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0gR;->fromAnnotation(Ljava/lang/String;)LX/0gR;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 109350
    invoke-virtual {p0}, LX/0ff;->g()LX/0eh;

    move-result-object v0

    sget-object v1, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 109349
    invoke-virtual {p0}, LX/0ff;->h()LX/0gQ;

    move-result-object v0

    sget-object v1, LX/0gQ;->DELTA:LX/0gQ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()LX/0ff;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109342
    invoke-static {p0}, LX/0ff;->s(LX/0ff;)[Ljava/lang/String;

    move-result-object v0

    .line 109343
    iget-boolean v1, p0, LX/0ff;->d:Z

    move v1, v1

    .line 109344
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    move-object v0, v0

    .line 109345
    if-nez v0, :cond_1

    .line 109346
    const/4 v0, 0x0

    .line 109347
    :goto_1
    move-object v1, v0

    .line 109348
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_0
    new-instance v0, LX/0ff;

    invoke-direct {v0, v1}, LX/0ff;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    iget-object v1, p0, LX/0ff;->c:Ljava/lang/String;

    iget-object v2, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\\."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/0gQ;->DELTA:LX/0gQ;

    invoke-virtual {v2}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109339
    invoke-static {p0}, LX/0ff;->s(LX/0ff;)[Ljava/lang/String;

    move-result-object v0

    .line 109340
    iget-boolean v1, p0, LX/0ff;->d:Z

    move v1, v1

    .line 109341
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()LX/0ff;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109334
    invoke-virtual {p0}, LX/0ff;->n()Ljava/lang/String;

    move-result-object v0

    .line 109335
    if-nez v0, :cond_1

    .line 109336
    const/4 v0, 0x0

    .line 109337
    :goto_0
    move-object v1, v0

    .line 109338
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, LX/0ff;

    invoke-direct {v0, v1}, LX/0ff;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, LX/0ff;->c:Ljava/lang/String;

    iget-object v2, p0, LX/0ff;->b:Ljava/util/regex/Matcher;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\\."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/0gQ;->DELTA:LX/0gQ;

    invoke-virtual {v2}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0gQ;->NEW:LX/0gQ;

    invoke-virtual {v3}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()LX/0ff;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109333
    sget-object v0, LX/0gR;->PATCHED:LX/0gR;

    invoke-static {p0, v0}, LX/0ff;->a(LX/0ff;LX/0gR;)LX/0ff;

    move-result-object v0

    return-object v0
.end method
