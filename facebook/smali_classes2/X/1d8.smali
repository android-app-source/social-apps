.class public LX/1d8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1dB;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 284091
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 284092
    new-instance v0, LX/1d9;

    invoke-direct {v0}, LX/1d9;-><init>()V

    sput-object v0, LX/1d8;->a:LX/1dB;

    .line 284093
    :goto_0
    return-void

    .line 284094
    :cond_0
    new-instance v0, LX/1dA;

    invoke-direct {v0}, LX/1dA;-><init>()V

    sput-object v0, LX/1d8;->a:LX/1dB;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284096
    return-void
.end method

.method public static b(Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 1

    .prologue
    .line 284097
    sget-object v0, LX/1d8;->a:LX/1dB;

    invoke-interface {v0, p0}, LX/1dB;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    return v0
.end method
