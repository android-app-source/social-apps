.class public LX/1QZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Qa;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final C:Ljava/lang/Object;

.field public static final b:Ljava/lang/String;


# instance fields
.field public A:LX/1EG;

.field public B:J

.field public a:LX/0Px;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Rx;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BMP;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1lT;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1lV;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1q1;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1k4;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1E1;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2vA;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/32f;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kD;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0qb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1q6;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Wd;
    .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private v:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final w:LX/1Qb;

.field public final x:LX/1Qd;

.field public final y:LX/1Qe;

.field public final z:LX/1Qf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 244948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/1QZ;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_user_in_holdout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1QZ;->b:Ljava/lang/String;

    .line 244949
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1QZ;->C:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 244918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244919
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244920
    iput-object v0, p0, LX/1QZ;->c:LX/0Ot;

    .line 244921
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244922
    iput-object v0, p0, LX/1QZ;->e:LX/0Ot;

    .line 244923
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244924
    iput-object v0, p0, LX/1QZ;->f:LX/0Ot;

    .line 244925
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244926
    iput-object v0, p0, LX/1QZ;->g:LX/0Ot;

    .line 244927
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244928
    iput-object v0, p0, LX/1QZ;->j:LX/0Ot;

    .line 244929
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244930
    iput-object v0, p0, LX/1QZ;->k:LX/0Ot;

    .line 244931
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244932
    iput-object v0, p0, LX/1QZ;->l:LX/0Ot;

    .line 244933
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244934
    iput-object v0, p0, LX/1QZ;->n:LX/0Ot;

    .line 244935
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244936
    iput-object v0, p0, LX/1QZ;->o:LX/0Ot;

    .line 244937
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244938
    iput-object v0, p0, LX/1QZ;->q:LX/0Ot;

    .line 244939
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 244940
    iput-object v0, p0, LX/1QZ;->t:LX/0Ot;

    .line 244941
    new-instance v0, LX/1Qb;

    invoke-direct {v0, p0}, LX/1Qb;-><init>(LX/1QZ;)V

    iput-object v0, p0, LX/1QZ;->w:LX/1Qb;

    .line 244942
    new-instance v0, LX/1Qd;

    invoke-direct {v0, p0}, LX/1Qd;-><init>(LX/1QZ;)V

    iput-object v0, p0, LX/1QZ;->x:LX/1Qd;

    .line 244943
    new-instance v0, LX/1Qe;

    invoke-direct {v0, p0}, LX/1Qe;-><init>(LX/1QZ;)V

    iput-object v0, p0, LX/1QZ;->y:LX/1Qe;

    .line 244944
    new-instance v0, LX/1Qf;

    invoke-direct {v0, p0}, LX/1Qf;-><init>(LX/1QZ;)V

    iput-object v0, p0, LX/1QZ;->z:LX/1Qf;

    .line 244945
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 244946
    iput-object v0, p0, LX/1QZ;->a:LX/0Px;

    .line 244947
    return-void
.end method

.method public static a(LX/0QB;)LX/1QZ;
    .locals 7

    .prologue
    .line 244891
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 244892
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 244893
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 244894
    if-nez v1, :cond_0

    .line 244895
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244896
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 244897
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 244898
    sget-object v1, LX/1QZ;->C:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 244899
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 244900
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 244901
    :cond_1
    if-nez v1, :cond_4

    .line 244902
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 244903
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 244904
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1QZ;->b(LX/0QB;)LX/1QZ;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 244905
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 244906
    if-nez v1, :cond_2

    .line 244907
    sget-object v0, LX/1QZ;->C:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 244908
    :goto_1
    if-eqz v0, :cond_3

    .line 244909
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 244910
    :goto_3
    check-cast v0, LX/1QZ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 244911
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 244912
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 244913
    :catchall_1
    move-exception v0

    .line 244914
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 244915
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 244916
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 244917
    :cond_2
    :try_start_8
    sget-object v0, LX/1QZ;->C:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244887
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244888
    :cond_0
    iget-object v0, p0, LX/1QZ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q1;

    iget-object v1, p0, LX/1QZ;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1q1;->b(J)V

    .line 244889
    :cond_1
    iput-object p1, p0, LX/1QZ;->a:LX/0Px;

    .line 244890
    return-void
.end method

.method private static a(LX/1QZ;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ck;LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/03V;LX/0qb;LX/0Ot;LX/0Wd;LX/0Uh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1QZ;",
            "LX/0Ot",
            "<",
            "LX/1Rx;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BMP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1lT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1lV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1q1;",
            ">;",
            "LX/1Ck;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/1k4;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1E1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2vA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/32f;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/1kD;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0qb;",
            "LX/0Ot",
            "<",
            "LX/1q6;",
            ">;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 244886
    iput-object p1, p0, LX/1QZ;->c:LX/0Ot;

    iput-object p2, p0, LX/1QZ;->d:LX/0Or;

    iput-object p3, p0, LX/1QZ;->e:LX/0Ot;

    iput-object p4, p0, LX/1QZ;->f:LX/0Ot;

    iput-object p5, p0, LX/1QZ;->g:LX/0Ot;

    iput-object p6, p0, LX/1QZ;->h:LX/1Ck;

    iput-object p7, p0, LX/1QZ;->i:LX/0SG;

    iput-object p8, p0, LX/1QZ;->j:LX/0Ot;

    iput-object p9, p0, LX/1QZ;->k:LX/0Ot;

    iput-object p10, p0, LX/1QZ;->l:LX/0Ot;

    iput-object p11, p0, LX/1QZ;->m:LX/0Or;

    iput-object p12, p0, LX/1QZ;->n:LX/0Ot;

    iput-object p13, p0, LX/1QZ;->o:LX/0Ot;

    iput-object p14, p0, LX/1QZ;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/1QZ;->q:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/1QZ;->r:LX/03V;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/1QZ;->s:LX/0qb;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/1QZ;->t:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/1QZ;->u:LX/0Wd;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/1QZ;->v:LX/0Uh;

    return-void
.end method

.method private a(LX/24P;)V
    .locals 4

    .prologue
    .line 244878
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244879
    new-instance v0, LX/32d;

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    invoke-direct {v0, v1}, LX/32d;-><init>(LX/1RN;)V

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    .line 244880
    new-instance v2, LX/32e;

    iget-boolean v3, v1, LX/32e;->b:Z

    invoke-direct {v2, p1, v3}, LX/32e;-><init>(LX/24P;Z)V

    move-object v1, v2

    .line 244881
    invoke-virtual {v0, v1}, LX/32d;->a(LX/32e;)LX/32d;

    move-result-object v0

    invoke-virtual {v0}, LX/32d;->a()LX/1RN;

    move-result-object v0

    .line 244882
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/1QZ;->a:LX/0Px;

    const/4 v2, 0x1

    iget-object v3, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 244883
    invoke-direct {p0, v0}, LX/1QZ;->a(LX/0Px;)V

    .line 244884
    iget-object v1, p0, LX/1QZ;->A:LX/1EG;

    invoke-virtual {v1, v0}, LX/1EG;->a(LX/0Px;)V

    .line 244885
    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 244873
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244874
    invoke-direct {p0, p4}, LX/1QZ;->a(Z)V

    .line 244875
    iget-object v0, p0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rx;

    invoke-virtual {v0, p1, p2, p3}, LX/1Rx;->a(Ljava/lang/Class;Ljava/lang/String;Z)V

    .line 244876
    :cond_0
    iget-object v0, p0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rx;

    invoke-virtual {v0, p1}, LX/1Rx;->a(Ljava/lang/Class;)V

    .line 244877
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 244867
    iget-object v0, p0, LX/1QZ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q1;

    iget-object v1, p0, LX/1QZ;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, LX/1q1;->a(JZ)V

    .line 244868
    iget-object v0, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 244869
    sget-object v0, LX/24P;->DISMISSED:LX/24P;

    invoke-direct {p0, v0}, LX/1QZ;->a(LX/24P;)V

    .line 244870
    :goto_0
    return-void

    .line 244871
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 244872
    iput-object v0, p0, LX/1QZ;->a:LX/0Px;

    goto :goto_0
.end method

.method public static a(LX/1kK;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 244861
    sget-object v3, LX/1kr;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244862
    invoke-interface {p0}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244863
    const/4 v0, 0x1

    .line 244864
    :goto_1
    return v0

    .line 244865
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 244866
    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/1QZ;
    .locals 23

    .prologue
    .line 244858
    new-instance v2, LX/1QZ;

    invoke-direct {v2}, LX/1QZ;-><init>()V

    .line 244859
    const/16 v3, 0xfcf

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2ff5

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xfd8

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xfda

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xfde

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    const/16 v10, 0xfe0

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xfcd

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2ff9

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2ffa

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0xfd9

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xfd2

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v16

    check-cast v16, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v17, 0xfdb

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0qb;->a(LX/0QB;)LX/0qb;

    move-result-object v19

    check-cast v19, LX/0qb;

    const/16 v20, 0xfd6

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v21

    check-cast v21, LX/0Wd;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-static/range {v2 .. v22}, LX/1QZ;->a(LX/1QZ;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ck;LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/03V;LX/0qb;LX/0Ot;LX/0Wd;LX/0Uh;)V

    .line 244860
    return-object v2
.end method

.method public static b(LX/1kK;)Z
    .locals 2

    .prologue
    .line 244950
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1RN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 244857
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    goto :goto_0
.end method

.method public final a(LX/0Px;LX/1EG;ZZ)V
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;",
            "Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$NewPromptCallback;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 244798
    if-nez p4, :cond_1

    iget-object v0, p0, LX/1QZ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q1;

    iget-object v1, p0, LX/1QZ;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, LX/1q1;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244799
    :cond_0
    :goto_0
    return-void

    .line 244800
    :cond_1
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244801
    iget-object v0, p0, LX/1QZ;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q6;

    .line 244802
    iput-object v8, v0, LX/1q6;->b:Ljava/lang/String;

    .line 244803
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    .line 244804
    invoke-direct {p0, v4}, LX/1QZ;->a(Z)V

    .line 244805
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 244806
    invoke-virtual {p2, v0}, LX/1EG;->a(LX/0Px;)V

    goto :goto_0

    .line 244807
    :cond_2
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    .line 244808
    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v5

    .line 244809
    iget-object v1, v0, LX/1RN;->a:LX/1kK;

    if-eqz v1, :cond_a

    iget-object v1, v0, LX/1RN;->a:LX/1kK;

    invoke-static {v1}, LX/1QZ;->b(LX/1kK;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, LX/1QZ;->s:LX/0qb;

    invoke-virtual {v1}, LX/0qb;->e()Z

    move-result v1

    if-nez v1, :cond_a

    move v2, v3

    .line 244810
    :goto_1
    iget-object v1, v0, LX/1RN;->a:LX/1kK;

    if-eqz v1, :cond_b

    if-nez v2, :cond_b

    .line 244811
    iget-object v1, p0, LX/1QZ;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1q6;

    iget-object v6, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v6}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v6

    .line 244812
    iput-object v6, v1, LX/1q6;->b:Ljava/lang/String;

    .line 244813
    :goto_2
    if-nez v2, :cond_3

    iget-object v2, p0, LX/1QZ;->s:LX/0qb;

    iget-object v1, p0, LX/1QZ;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qg;

    sget-object v6, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v2, v1, v6}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    move v4, v3

    .line 244814
    :cond_4
    const/4 v1, 0x1

    .line 244815
    if-eqz p4, :cond_c

    move p3, v1

    .line 244816
    :cond_5
    :goto_3
    move v0, p3

    .line 244817
    if-eqz v0, :cond_6

    if-eqz v4, :cond_6

    .line 244818
    invoke-direct {p0, p1}, LX/1QZ;->a(LX/0Px;)V

    .line 244819
    invoke-virtual {p2, p1}, LX/1EG;->a(LX/0Px;)V

    .line 244820
    :cond_6
    iget-object v0, p0, LX/1QZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kD;

    .line 244821
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 244822
    :goto_4
    const/4 v1, 0x1

    move v2, v1

    :goto_5
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_8

    .line 244823
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RN;

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v3

    .line 244824
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RN;

    .line 244825
    invoke-static {v0, v3}, LX/1kD;->c(LX/1kD;Ljava/lang/String;)LX/1lA;

    move-result-object v4

    .line 244826
    iput-object v1, v4, LX/1lA;->a:LX/1RN;

    .line 244827
    iget-object v1, v0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lA;

    .line 244828
    if-eqz v1, :cond_f

    iget-object v1, v1, LX/1lA;->b:Ljava/lang/String;

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_6
    move v1, v1

    .line 244829
    if-nez v1, :cond_7

    .line 244830
    const-string v1, "Other prompt has higher priority"

    invoke-virtual {v0, v3, v1}, LX/1kD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244831
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 244832
    :cond_8
    iget-object v1, v0, LX/1kD;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_10

    .line 244833
    :cond_9
    goto/16 :goto_0

    :cond_a
    move v2, v4

    .line 244834
    goto/16 :goto_1

    .line 244835
    :cond_b
    iget-object v1, p0, LX/1QZ;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1q6;

    .line 244836
    iput-object v8, v1, LX/1q6;->b:Ljava/lang/String;

    .line 244837
    goto/16 :goto_2

    .line 244838
    :cond_c
    invoke-static {v5, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 244839
    const/4 p3, 0x0

    goto :goto_3

    .line 244840
    :cond_d
    if-nez v5, :cond_5

    if-eqz v0, :cond_5

    move p3, v1

    .line 244841
    goto :goto_3

    .line 244842
    :cond_e
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RN;

    .line 244843
    iput-object v1, v0, LX/1kD;->d:LX/1RN;

    .line 244844
    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This prompt is being shown"

    invoke-virtual {v0, v1, v2}, LX/1kD;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_f
    const/4 v1, 0x0

    goto :goto_6

    .line 244845
    :cond_10
    invoke-virtual {v0}, LX/1kD;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 244846
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 244847
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 244848
    invoke-virtual {v0, v1}, LX/1kD;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 244849
    if-eqz v4, :cond_12

    const-string v3, "This prompt is being shown"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v3, 0x1

    .line 244850
    :goto_8
    invoke-virtual {v0, v1}, LX/1kD;->b(Ljava/lang/String;)LX/1RN;

    move-result-object v5

    .line 244851
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "composer_prompt_not_displayed_reason"

    invoke-direct {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "prompt_type"

    invoke-virtual {v6, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "reason"

    invoke-virtual {v6, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "shown"

    invoke-virtual {v4, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 244852
    if-eqz v5, :cond_11

    .line 244853
    const-string v4, "prompt_id"

    iget-object v5, v5, LX/1RN;->a:LX/1kK;

    invoke-interface {v5}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 244854
    :cond_11
    move-object v1, v3

    .line 244855
    iget-object v3, v0, LX/1kD;->a:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_7

    .line 244856
    :cond_12
    const/4 v3, 0x0

    goto :goto_8
.end method

.method public final a(LX/1RN;LX/5S9;)V
    .locals 4

    .prologue
    .line 244690
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244691
    :cond_0
    :goto_0
    return-void

    .line 244692
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    invoke-virtual {v0, v1}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244693
    iget-object v0, p0, LX/1QZ;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oY;

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244694
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    invoke-static {p1, v1}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(LX/1RN;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 244695
    sget-object v2, LX/5oV;->SHOW_FLYOUT:LX/5oV;

    invoke-static {v0, v2, v1}, LX/5oW;->b(LX/5oW;LX/5oU;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 244696
    sget-object v3, LX/5S9;->IMPLICIT:LX/5S9;

    if-eq p2, v3, :cond_2

    .line 244697
    const-string v3, "tap_origin"

    invoke-virtual {p2}, LX/5S9;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 244698
    :cond_2
    iget-object v3, v0, LX/5oW;->b:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 244699
    iget-object v0, p0, LX/1QZ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32f;

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    .line 244700
    iget-object v2, v0, LX/32f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1kp;->s:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244701
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 244702
    iget-object v2, v0, LX/32f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v3, 0x1

    new-array v3, v3, [LX/0Tn;

    const/4 p1, 0x0

    sget-object p2, LX/1kp;->s:LX/0Tn;

    aput-object p2, v3, p1

    invoke-static {v3}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 244703
    :cond_3
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    invoke-direct {p0, v0}, LX/1QZ;->a(LX/24P;)V

    goto :goto_0
.end method

.method public final a(LX/1k5;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 244777
    iget-boolean v0, p1, LX/1k5;->c:Z

    if-eqz v0, :cond_0

    .line 244778
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v1, p1, LX/1k5;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 244779
    sget-object v2, LX/5oV;->POST_WITH_PROMPT:LX/5oV;

    invoke-static {v0, v2, v1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 244780
    iget-object v0, p0, LX/1QZ;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vA;

    iget-object v1, p1, LX/1k5;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v1, v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    .line 244781
    if-nez v1, :cond_1

    .line 244782
    :goto_0
    iget-object v0, p1, LX/1k5;->a:Ljava/lang/Class;

    iget-object v1, p1, LX/1k5;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v1, v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    iget-boolean v2, p1, LX/1k5;->c:Z

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, LX/1QZ;->a(Ljava/lang/Class;Ljava/lang/String;ZZ)V

    .line 244783
    :goto_1
    iget-object v0, p0, LX/1QZ;->A:LX/1EG;

    iget-object v1, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/1EG;->a(LX/0Px;)V

    .line 244784
    return-void

    .line 244785
    :cond_0
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v1, p1, LX/1k5;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 244786
    sget-object v2, LX/5oV;->POST_WITHOUT_PROMPT:LX/5oV;

    invoke-static {v0, v2, v1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 244787
    goto :goto_1

    .line 244788
    :cond_1
    iget-object v2, v0, LX/2vA;->a:LX/32g;

    .line 244789
    invoke-static {v2, v1}, LX/32g;->l(LX/32g;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 244790
    :goto_2
    iget-object v2, v0, LX/2vA;->b:LX/32i;

    .line 244791
    invoke-static {v2, v1}, LX/32i;->h(LX/32i;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 244792
    :goto_3
    goto :goto_0

    .line 244793
    :cond_2
    invoke-static {v1}, LX/32g;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    .line 244794
    if-nez v3, :cond_3

    .line 244795
    :goto_4
    goto :goto_2

    .line 244796
    :cond_3
    iget-object v4, v2, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto :goto_4

    .line 244797
    :cond_4
    invoke-static {v2, v1}, LX/32i;->f(LX/32i;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final a(LX/1k7;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 244772
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v1, p1, LX/1k7;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 244773
    sget-object p1, LX/5oV;->CANCEL_COMPOSER:LX/5oV;

    invoke-static {v0, p1, v1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 244774
    iget-object v0, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244775
    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    sget-object v1, LX/5S9;->IMPLICIT:LX/5S9;

    invoke-virtual {p0, v0, v1}, LX/1QZ;->b(LX/1RN;LX/5S9;)V

    .line 244776
    :cond_0
    return-void
.end method

.method public final a(LX/1k8;)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 244746
    iget-object v0, p0, LX/1QZ;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BMP;

    const/4 v1, 0x0

    iget-object v2, p1, LX/1k8;->a:LX/1RN;

    invoke-virtual {v0, v1, v2}, LX/B5s;->a(LX/88f;LX/1RN;)V

    .line 244747
    iget-object v0, p0, LX/1QZ;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vA;

    iget-object v1, p1, LX/1k8;->a:LX/1RN;

    iget-object v1, v1, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    .line 244748
    if-eqz v1, :cond_0

    .line 244749
    iget-object v2, v0, LX/2vA;->a:LX/32g;

    .line 244750
    invoke-static {v2, v1}, LX/32g;->l(LX/32g;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 244751
    :goto_0
    iget-object v2, v0, LX/2vA;->b:LX/32i;

    .line 244752
    invoke-static {v2, v1}, LX/32i;->h(LX/32i;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 244753
    :cond_0
    :goto_1
    iget-object v0, p1, LX/1k8;->a:LX/1RN;

    invoke-static {v0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v1

    .line 244754
    iget-object v0, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->r()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v1}, LX/1QZ;->a(LX/1kK;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244755
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p1, LX/1k8;->a:LX/1RN;

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, LX/1k8;->c:Z

    iget-boolean v3, p1, LX/1k8;->d:Z

    invoke-direct {p0, v0, v1, v2, v3}, LX/1QZ;->a(Ljava/lang/Class;Ljava/lang/String;ZZ)V

    .line 244756
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v1, p1, LX/1k8;->b:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 244757
    sget-object v2, LX/5oV;->CLOSE_SUGGESTION:LX/5oV;

    invoke-static {v0, v2, v1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 244758
    iget-object v0, p0, LX/1QZ;->A:LX/1EG;

    iget-object v1, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/1EG;->a(LX/0Px;)V

    .line 244759
    :cond_2
    return-void

    .line 244760
    :cond_3
    invoke-static {v1}, LX/32g;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    .line 244761
    if-nez v4, :cond_4

    .line 244762
    :goto_2
    goto :goto_0

    .line 244763
    :cond_4
    invoke-static {v2, v1}, LX/32g;->g(LX/32g;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 244764
    if-nez v3, :cond_5

    iget v3, v2, LX/32g;->a:I

    .line 244765
    :goto_3
    iget-object v5, v2, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    iget v6, v2, LX/32g;->b:I

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v5, v4, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto :goto_2

    .line 244766
    :cond_5
    iget v5, v2, LX/32g;->c:I

    mul-int/2addr v3, v5

    goto :goto_3

    .line 244767
    :cond_6
    invoke-static {v1}, LX/32i;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    .line 244768
    if-nez v3, :cond_7

    .line 244769
    :goto_4
    goto/16 :goto_1

    .line 244770
    :cond_7
    invoke-static {v2, v1}, LX/32i;->e(LX/32i;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 244771
    iget-object v5, v2, LX/32i;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v5, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto :goto_4
.end method

.method public final a(LX/1k9;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 244744
    iget-object v0, p0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rx;

    iget-object v1, p1, LX/1k9;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, LX/1Rx;->a(Ljava/lang/Class;)V

    .line 244745
    return-void
.end method

.method public final a(ZLjava/lang/String;Z)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 244726
    iget-object v0, p0, LX/1QZ;->A:LX/1EG;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 244727
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 244728
    iget-object v4, p0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Rx;

    invoke-virtual {v4}, LX/1Rx;->a()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v5

    .line 244729
    :goto_1
    move v0, v4

    .line 244730
    if-nez v0, :cond_1

    .line 244731
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 244732
    goto :goto_0

    .line 244733
    :cond_1
    iget-object v0, p0, LX/1QZ;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/1QZ;->B:J

    .line 244734
    new-instance v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;-><init>(LX/1QZ;ZLjava/lang/String;Z)V

    .line 244735
    iget-object v2, p0, LX/1QZ;->v:LX/0Uh;

    const/16 v3, 0x5cd

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244736
    iget-object v1, p0, LX/1QZ;->u:LX/0Wd;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2

    .line 244737
    :cond_2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    .line 244738
    :cond_3
    iget-object v4, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1E1;

    invoke-virtual {v4}, LX/1E1;->e()Z

    move-result v4

    if-nez v4, :cond_4

    .line 244739
    iget-object v4, p0, LX/1QZ;->r:LX/03V;

    sget-object v6, LX/1QZ;->b:Ljava/lang/String;

    const-string v7, "InlineComposerPromptFetcher#isPREnabled=true but user is in PRODUCTION_PROMPTS_HOLDOUT and not in FRIEND_SHARING_INSPIRATIONS"

    invoke-virtual {v4, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 244740
    goto :goto_1

    .line 244741
    :cond_4
    if-eqz p3, :cond_5

    move v4, v6

    .line 244742
    goto :goto_1

    .line 244743
    :cond_5
    const-string v4, "on_resume"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "on_ptr"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, LX/1QZ;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v8

    iget-wide v10, p0, LX/1QZ;->B:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3a98

    cmp-long v4, v8, v10

    if-lez v4, :cond_7

    :cond_6
    move v4, v6

    goto :goto_1

    :cond_7
    move v4, v5

    goto :goto_1
.end method

.method public final b(LX/1RN;LX/5S9;)V
    .locals 4

    .prologue
    .line 244709
    iget-object v0, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244710
    :cond_0
    :goto_0
    return-void

    .line 244711
    :cond_1
    sget-object v0, LX/24P;->MINIMIZED:LX/24P;

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    invoke-virtual {v0, v1}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244712
    sget-object v0, LX/5S9;->ICON:LX/5S9;

    if-eq p2, v0, :cond_2

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    .line 244713
    sget-object v1, LX/5S9;->XOUT:LX/5S9;

    if-ne p2, v1, :cond_5

    invoke-static {v0}, LX/1QZ;->a(LX/1kK;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 244714
    if-eqz v0, :cond_2

    .line 244715
    sget-object v0, LX/5S9;->XOUT:LX/5S9;

    if-ne p2, v0, :cond_6

    iget-object v0, p0, LX/1QZ;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->r()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 244716
    if-eqz v0, :cond_4

    .line 244717
    :cond_2
    iget-object v0, p0, LX/1QZ;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oY;

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244718
    iget-object v0, p0, LX/1QZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    invoke-static {p1, v1}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(LX/1RN;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 244719
    sget-object v2, LX/5oV;->HIDE_FLYOUT:LX/5oV;

    invoke-static {v0, v2, v1}, LX/5oW;->b(LX/5oW;LX/5oU;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 244720
    sget-object v3, LX/5S9;->IMPLICIT:LX/5S9;

    if-eq p2, v3, :cond_3

    .line 244721
    const-string v3, "tap_origin"

    invoke-virtual {p2}, LX/5S9;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 244722
    :cond_3
    iget-object v3, v0, LX/5oW;->b:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 244723
    :cond_4
    iget-object v0, p0, LX/1QZ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32f;

    invoke-virtual {p0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    .line 244724
    iget-object v2, v0, LX/32f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1kp;->s:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 244725
    sget-object v0, LX/24P;->MINIMIZED:LX/24P;

    invoke-direct {p0, v0}, LX/1QZ;->a(LX/24P;)V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 244704
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 244705
    iput-object v0, p0, LX/1QZ;->a:LX/0Px;

    .line 244706
    iget-object v0, p0, LX/1QZ;->A:LX/1EG;

    if-eqz v0, :cond_0

    .line 244707
    iget-object v0, p0, LX/1QZ;->A:LX/1EG;

    iget-object v1, p0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/1EG;->a(LX/0Px;)V

    .line 244708
    :cond_0
    return-void
.end method
