.class public LX/1fE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1bp;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final b:F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public c:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public d:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public e:J
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public f:F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public g:F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 291109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291110
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 291111
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/1fE;->b:F

    .line 291112
    invoke-virtual {p0}, LX/1fE;->a()V

    .line 291113
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 291117
    const/4 v0, 0x0

    iput-object v0, p0, LX/1fE;->a:LX/1bp;

    .line 291118
    invoke-virtual {p0}, LX/1fE;->b()V

    .line 291119
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 291114
    iput-boolean v0, p0, LX/1fE;->c:Z

    .line 291115
    iput-boolean v0, p0, LX/1fE;->d:Z

    .line 291116
    return-void
.end method
