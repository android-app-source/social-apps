.class public abstract LX/1Ae;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BUI",
        "LDER:Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder",
        "<TBUI",
        "LDER;",
        "TREQUEST;TIMAGE;TINFO;>;REQUEST:",
        "Ljava/lang/Object;",
        "IMAGE:",
        "Ljava/lang/Object;",
        "INFO:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Af;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/NullPointerException;

.field private static final r:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TREQUEST;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TREQUEST;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TREQUEST;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TIMAGE;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<-TINFO;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/4AR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:LX/1aZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 211070
    new-instance v0, LX/1Ag;

    invoke-direct {v0}, LX/1Ag;-><init>()V

    sput-object v0, LX/1Ae;->a:LX/1Ai;

    .line 211071
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "No image request was specified!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1Ae;->b:Ljava/lang/NullPointerException;

    .line 211072
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, LX/1Ae;->r:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211043
    iput-object p1, p0, LX/1Ae;->c:Landroid/content/Context;

    .line 211044
    iput-object p2, p0, LX/1Ae;->d:Ljava/util/Set;

    .line 211045
    invoke-direct {p0}, LX/1Ae;->o()V

    .line 211046
    return-void
.end method

.method public static k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 211047
    sget-object v0, LX/1Ae;->r:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 211048
    iput-object v1, p0, LX/1Ae;->e:Ljava/lang/Object;

    .line 211049
    iput-object v1, p0, LX/1Ae;->f:Ljava/lang/Object;

    .line 211050
    iput-object v1, p0, LX/1Ae;->g:Ljava/lang/Object;

    .line 211051
    iput-object v1, p0, LX/1Ae;->h:[Ljava/lang/Object;

    .line 211052
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Ae;->i:Z

    .line 211053
    iput-object v1, p0, LX/1Ae;->k:LX/1Ai;

    .line 211054
    iput-object v1, p0, LX/1Ae;->l:LX/4AR;

    .line 211055
    iput-boolean v2, p0, LX/1Ae;->m:Z

    .line 211056
    iput-boolean v2, p0, LX/1Ae;->n:Z

    .line 211057
    iput-object v1, p0, LX/1Ae;->q:LX/1aZ;

    .line 211058
    iput-object v1, p0, LX/1Ae;->p:Ljava/lang/String;

    .line 211059
    return-void
.end method


# virtual methods
.method public final a(LX/1Ai;)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ai",
            "<-TINFO;>;)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211060
    iput-object p1, p0, LX/1Ae;->k:LX/1Ai;

    .line 211061
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211062
    iput-boolean p1, p0, LX/1Ae;->m:Z

    .line 211063
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final a([Ljava/lang/Object;)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TREQUEST;)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211064
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/1Ae;->a([Ljava/lang/Object;Z)LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final a([Ljava/lang/Object;Z)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TREQUEST;Z)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211065
    iput-object p1, p0, LX/1Ae;->h:[Ljava/lang/Object;

    .line 211066
    iput-boolean p2, p0, LX/1Ae;->i:Z

    .line 211067
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1aZ;)LX/1Af;
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 211068
    invoke-virtual {p0, p1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)LX/1Af;
    .locals 1

    .prologue
    .line 211069
    invoke-virtual {p0, p1}, LX/1Ae;->b(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;LX/1bj;)LX/1Gd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;",
            "LX/1bj;",
            ")",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TIMAGE;>;>;"
        }
    .end annotation

    .prologue
    .line 211073
    invoke-virtual {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v0

    .line 211074
    new-instance v1, LX/4AQ;

    invoke-direct {v1, p0, p1, v0, p2}, LX/4AQ;-><init>(LX/1Ae;Ljava/lang/Object;Ljava/lang/Object;LX/1bj;)V

    return-object v1
.end method

.method public synthetic a()LX/1aZ;
    .locals 1

    .prologue
    .line 211075
    invoke-virtual {p0}, LX/1Ae;->h()LX/1bp;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;LX/1bj;)LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;",
            "Ljava/lang/Object;",
            "LX/1bj;",
            ")",
            "LX/1ca",
            "<TIMAGE;>;"
        }
    .end annotation
.end method

.method public final a(LX/1bp;)V
    .locals 2

    .prologue
    .line 211076
    iget-object v0, p0, LX/1Ae;->d:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 211077
    iget-object v0, p0, LX/1Ae;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ai;

    .line 211078
    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    goto :goto_0

    .line 211079
    :cond_0
    iget-object v0, p0, LX/1Ae;->k:LX/1Ai;

    if-eqz v0, :cond_1

    .line 211080
    iget-object v0, p0, LX/1Ae;->k:LX/1Ai;

    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    .line 211081
    :cond_1
    iget-boolean v0, p0, LX/1Ae;->n:Z

    if-eqz v0, :cond_2

    .line 211082
    sget-object v0, LX/1Ae;->a:LX/1Ai;

    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    .line 211083
    :cond_2
    return-void
.end method

.method public b()LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211038
    invoke-direct {p0}, LX/1Ae;->o()V

    .line 211039
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1aZ;)LX/1Ae;
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aZ;",
            ")TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 211040
    iput-object p1, p0, LX/1Ae;->q:LX/1aZ;

    .line 211041
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 210961
    iput-object p1, p0, LX/1Ae;->e:Ljava/lang/Object;

    .line 210962
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 210964
    iput-boolean p1, p0, LX/1Ae;->o:Z

    .line 210965
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1bp;)V
    .locals 2

    .prologue
    .line 210966
    iget-boolean v0, p0, LX/1Ae;->m:Z

    if-nez v0, :cond_0

    .line 210967
    :goto_0
    return-void

    .line 210968
    :cond_0
    iget-object v0, p1, LX/1bp;->e:LX/1fD;

    move-object v0, v0

    .line 210969
    if-nez v0, :cond_1

    .line 210970
    new-instance v0, LX/1fD;

    invoke-direct {v0}, LX/1fD;-><init>()V

    .line 210971
    iput-object v0, p1, LX/1bp;->e:LX/1fD;

    .line 210972
    :cond_1
    iget-boolean v1, p0, LX/1Ae;->m:Z

    .line 210973
    iput-boolean v1, v0, LX/1fD;->a:Z

    .line 210974
    invoke-virtual {p0, p1}, LX/1Ae;->c(LX/1bp;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 210975
    iput-object p1, p0, LX/1Ae;->f:Ljava/lang/Object;

    .line 210976
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 210977
    iput-boolean p1, p0, LX/1Ae;->n:Z

    .line 210978
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 210963
    iget-object v0, p0, LX/1Ae;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public final c(LX/1bp;)V
    .locals 1

    .prologue
    .line 210979
    iget-object v0, p1, LX/1bp;->f:LX/1fE;

    move-object v0, v0

    .line 210980
    if-nez v0, :cond_0

    .line 210981
    iget-object v0, p0, LX/1Ae;->c:Landroid/content/Context;

    .line 210982
    new-instance p0, LX/1fE;

    invoke-direct {p0, v0}, LX/1fE;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    .line 210983
    iput-object v0, p1, LX/1bp;->f:LX/1fE;

    .line 210984
    iget-object p0, p1, LX/1bp;->f:LX/1fE;

    if-eqz p0, :cond_0

    .line 210985
    iget-object p0, p1, LX/1bp;->f:LX/1fE;

    .line 210986
    iput-object p1, p0, LX/1fE;->a:LX/1bp;

    .line 210987
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/Object;)LX/1Ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;)TBUI",
            "LDER;"
        }
    .end annotation

    .prologue
    .line 210988
    iput-object p1, p0, LX/1Ae;->g:Ljava/lang/Object;

    .line 210989
    invoke-virtual {p0}, LX/1Ae;->n()LX/1Ae;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)LX/1Gd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQUEST;)",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TIMAGE;>;>;"
        }
    .end annotation

    .prologue
    .line 210990
    sget-object v0, LX/1bj;->FULL_FETCH:LX/1bj;

    invoke-virtual {p0, p1, v0}, LX/1Ae;->a(Ljava/lang/Object;LX/1bj;)LX/1Gd;

    move-result-object v0

    return-object v0
.end method

.method public h()LX/1bp;
    .locals 1

    .prologue
    .line 210991
    invoke-virtual {p0}, LX/1Ae;->i()V

    .line 210992
    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Ae;->h:[Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Ae;->g:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 210993
    iget-object v0, p0, LX/1Ae;->g:Ljava/lang/Object;

    iput-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    .line 210994
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ae;->g:Ljava/lang/Object;

    .line 210995
    :cond_0
    invoke-virtual {p0}, LX/1Ae;->j()LX/1bp;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 210996
    iget-object v0, p0, LX/1Ae;->h:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Cannot specify both ImageRequest and FirstAvailableImageRequests!"

    invoke-static {v0, v3}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 210997
    iget-object v0, p0, LX/1Ae;->j:LX/1Gd;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Ae;->h:[Ljava/lang/Object;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1Ae;->g:Ljava/lang/Object;

    if-nez v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Cannot specify DataSourceSupplier with other ImageRequests! Use one or the other."

    invoke-static {v1, v0}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 210998
    return-void

    :cond_3
    move v0, v1

    .line 210999
    goto :goto_0
.end method

.method public j()LX/1bp;
    .locals 2

    .prologue
    .line 211000
    invoke-virtual {p0}, LX/1Ae;->m()LX/1bp;

    move-result-object v0

    .line 211001
    iget-boolean v1, p0, LX/1Ae;->o:Z

    move v1, v1

    .line 211002
    iput-boolean v1, v0, LX/1bp;->q:Z

    .line 211003
    iget-object v1, p0, LX/1Ae;->p:Ljava/lang/String;

    move-object v1, v1

    .line 211004
    iput-object v1, v0, LX/1bp;->r:Ljava/lang/String;

    .line 211005
    iget-object v1, p0, LX/1Ae;->l:LX/4AR;

    move-object v1, v1

    .line 211006
    iput-object v1, v0, LX/1bp;->h:LX/4AR;

    .line 211007
    invoke-virtual {p0, v0}, LX/1Ae;->b(LX/1bp;)V

    .line 211008
    invoke-virtual {p0, v0}, LX/1Ae;->a(LX/1bp;)V

    .line 211009
    return-object v0
.end method

.method public l()LX/1Gd;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TIMAGE;>;>;"
        }
    .end annotation

    .prologue
    .line 211010
    iget-object v0, p0, LX/1Ae;->j:LX/1Gd;

    if-eqz v0, :cond_1

    .line 211011
    iget-object v0, p0, LX/1Ae;->j:LX/1Gd;

    .line 211012
    :cond_0
    :goto_0
    return-object v0

    .line 211013
    :cond_1
    const/4 v0, 0x0

    .line 211014
    iget-object v1, p0, LX/1Ae;->f:Ljava/lang/Object;

    if-eqz v1, :cond_4

    .line 211015
    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/1Ae;->e(Ljava/lang/Object;)LX/1Gd;

    move-result-object v0

    .line 211016
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/1Ae;->g:Ljava/lang/Object;

    if-eqz v1, :cond_3

    .line 211017
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 211018
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211019
    iget-object v0, p0, LX/1Ae;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/1Ae;->e(Ljava/lang/Object;)LX/1Gd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211020
    invoke-static {v1}, LX/1fC;->a(Ljava/util/List;)LX/1fC;

    move-result-object v0

    .line 211021
    :cond_3
    if-nez v0, :cond_0

    .line 211022
    sget-object v0, LX/1Ae;->b:Ljava/lang/NullPointerException;

    .line 211023
    new-instance v1, LX/49C;

    invoke-direct {v1, v0}, LX/49C;-><init>(Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 211024
    goto :goto_0

    .line 211025
    :cond_4
    iget-object v1, p0, LX/1Ae;->h:[Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 211026
    iget-object v0, p0, LX/1Ae;->h:[Ljava/lang/Object;

    iget-boolean v1, p0, LX/1Ae;->i:Z

    const/4 v3, 0x0

    .line 211027
    new-instance v4, Ljava/util/ArrayList;

    array-length v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 211028
    if-eqz v1, :cond_5

    move v2, v3

    .line 211029
    :goto_2
    array-length v5, v0

    if-ge v2, v5, :cond_5

    .line 211030
    aget-object v5, v0, v2

    sget-object v6, LX/1bj;->BITMAP_MEMORY_CACHE:LX/1bj;

    invoke-virtual {p0, v5, v6}, LX/1Ae;->a(Ljava/lang/Object;LX/1bj;)LX/1Gd;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211031
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 211032
    :cond_5
    :goto_3
    array-length v2, v0

    if-ge v3, v2, :cond_6

    .line 211033
    aget-object v2, v0, v3

    invoke-virtual {p0, v2}, LX/1Ae;->e(Ljava/lang/Object;)LX/1Gd;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211034
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 211035
    :cond_6
    new-instance v2, LX/1fB;

    invoke-direct {v2, v4}, LX/1fB;-><init>(Ljava/util/List;)V

    move-object v2, v2

    .line 211036
    move-object v0, v2

    .line 211037
    goto :goto_1
.end method

.method public abstract m()LX/1bp;
.end method

.method public abstract n()LX/1Ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBUI",
            "LDER;"
        }
    .end annotation
.end method
