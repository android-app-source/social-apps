.class public final LX/1Nm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1J7;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0

    .prologue
    .line 237718
    iput-object p1, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 237659
    const/4 v0, -0x1

    return v0
.end method

.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 237717
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 237660
    const-string v0, "NewsFeedFragment.FeedScrollListener.onScroll"

    const v1, 0x572459f3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 237661
    :try_start_0
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->n:Z

    if-nez v0, :cond_0

    if-le p2, v2, :cond_0

    .line 237662
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    new-instance v1, LX/29v;

    invoke-direct {v1}, LX/29v;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 237663
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    const/4 v1, 0x1

    .line 237664
    iput-boolean v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->n:Z

    .line 237665
    :cond_0
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->w:LX/0ad;

    invoke-static {v0}, LX/1Sr;->b(LX/0ad;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 237666
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->f:I

    if-ne v0, p2, :cond_1

    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->g:I

    add-int v1, p2, p3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->h:I

    if-eq v0, p4, :cond_2

    .line 237667
    :cond_1
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 237668
    iput p2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->f:I

    .line 237669
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    add-int v1, p2, p3

    .line 237670
    iput v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->g:I

    .line 237671
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 237672
    iput p4, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->h:I

    .line 237673
    const-string v0, "NewsFeedFragment.removeFeedUnitIfHidden"

    const v1, -0x74b5f6c4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 237674
    :try_start_1
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, p1, v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->b(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0g8;I)V

    .line 237675
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;->g:I

    invoke-static {v0, p1, v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->b(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0g8;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237676
    const v0, 0x61a6f174

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 237677
    :cond_2
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 237678
    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/0g2;->q:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v1}, LX/0gC;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 237679
    :cond_3
    :goto_0
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->Q:LX/1EU;

    invoke-virtual {v0}, LX/1EU;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 237680
    iget-object v0, p0, LX/1Nm;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    add-int v1, p2, p3

    add-int/lit8 v1, v1, -0x1

    .line 237681
    invoke-static {v0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v2

    if-nez v2, :cond_9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 237682
    :cond_4
    :goto_1
    const v0, 0x78df34d4

    invoke-static {v0}, LX/02m;->a(I)V

    return-void

    .line 237683
    :catchall_0
    move-exception v0

    const v1, 0x4c743dde    # 6.4026488E7f

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 237684
    :catchall_1
    move-exception v0

    const v1, 0x571e46ac

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 237685
    :cond_5
    :try_start_4
    iget-object v1, v0, LX/0g2;->ae:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v1

    invoke-interface {p1}, LX/0g8;->r()I

    move-result v2

    iget-object v3, v0, LX/0g2;->ae:LX/1UQ;

    invoke-virtual {v3}, LX/1UQ;->c()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v1, v1

    .line 237686
    invoke-static {v0}, LX/0g2;->ad(LX/0g2;)I

    move-result v2

    .line 237687
    iget-object v3, v0, LX/0g2;->ae:LX/1UQ;

    invoke-interface {v3, v1}, LX/1Qr;->h_(I)I

    move-result v1

    .line 237688
    iget-object v3, v0, LX/0g2;->c:LX/0fz;

    .line 237689
    iget-object v4, v3, LX/0fz;->g:LX/0qu;

    .line 237690
    invoke-static {v4}, LX/0qu;->j(LX/0qu;)V

    .line 237691
    invoke-virtual {v4, v1}, LX/0qu;->b(I)I

    move-result p1

    const/4 v3, -0x1

    if-eq p1, v3, :cond_8

    const/4 p1, 0x1

    :goto_2
    move v4, p1

    .line 237692
    move v3, v4

    .line 237693
    if-eqz v3, :cond_3

    .line 237694
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0g2;->a(Z)V

    .line 237695
    iget-object v3, v0, LX/0g2;->c:LX/0fz;

    .line 237696
    iget-object v4, v3, LX/0fz;->g:LX/0qu;

    invoke-virtual {v4, v1}, LX/0qu;->b(I)I

    move-result v4

    move v3, v4

    .line 237697
    iget-object v4, v0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v4, v3}, LX/0fz;->e(I)I

    move-result v4

    .line 237698
    if-lez v4, :cond_7

    .line 237699
    iget-object v5, v0, LX/0g2;->r:LX/0Zb;

    const-string p4, "feed_gap_drop_on_scroll"

    const/4 p1, 0x0

    invoke-interface {v5, p4, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 237700
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result p4

    if-eqz p4, :cond_6

    .line 237701
    const-string p4, "first"

    invoke-virtual {v5, p4, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 237702
    const-string p4, "last"

    invoke-virtual {v5, p4, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 237703
    const-string p4, "gap"

    invoke-virtual {v5, p4, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 237704
    const-string p4, "dropped"

    invoke-virtual {v5, p4, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 237705
    const-string p4, "tracking"

    invoke-static {v0}, LX/0g2;->ab(LX/0g2;)LX/162;

    move-result-object p1

    invoke-virtual {v5, p4, p1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 237706
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 237707
    :cond_6
    iget-object v1, v0, LX/0g2;->W:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->b()V

    .line 237708
    invoke-static {v0}, LX/0g2;->R(LX/0g2;)V

    .line 237709
    :cond_7
    invoke-virtual {v0}, LX/0g2;->d()V

    goto/16 :goto_0

    :cond_8
    const/4 p1, 0x0

    goto :goto_2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 237710
    :cond_9
    iget-object v2, v0, LX/0g2;->S:LX/1NJ;

    .line 237711
    iget v3, v2, LX/1NJ;->k:I

    if-ltz v3, :cond_a

    iget-object v3, v2, LX/1NJ;->h:LX/1UQ;

    if-nez v3, :cond_b

    .line 237712
    :cond_a
    :goto_3
    goto/16 :goto_1

    .line 237713
    :cond_b
    const/4 v3, 0x0

    iget-object v0, v2, LX/1NJ;->h:LX/1UQ;

    invoke-interface {v0, v1}, LX/1Qr;->h_(I)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 237714
    iget v0, v2, LX/1NJ;->k:I

    if-le v3, v0, :cond_a

    .line 237715
    sget-object v3, LX/1lq;->HideNSBP:LX/1lq;

    invoke-virtual {v2, v3}, LX/1NJ;->a(LX/1lq;)V

    .line 237716
    const/4 v3, -0x1

    iput v3, v2, LX/1NJ;->k:I

    goto :goto_3
.end method
