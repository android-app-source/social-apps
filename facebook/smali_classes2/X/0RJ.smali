.class public final enum LX/0RJ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0RK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0RJ;",
        ">;",
        "LX/0RK;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0RJ;

.field public static final enum INSTANCE:LX/0RJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59880
    new-instance v0, LX/0RJ;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/0RJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0RJ;->INSTANCE:LX/0RJ;

    .line 59881
    const/4 v0, 0x1

    new-array v0, v0, [LX/0RJ;

    sget-object v1, LX/0RJ;->INSTANCE:LX/0RJ;

    aput-object v1, v0, v2

    sput-object v0, LX/0RJ;->$VALUES:[LX/0RJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59879
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0RJ;
    .locals 1

    .prologue
    .line 59878
    const-class v0, LX/0RJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0RJ;

    return-object v0
.end method

.method public static values()[LX/0RJ;
    .locals 1

    .prologue
    .line 59877
    sget-object v0, LX/0RJ;->$VALUES:[LX/0RJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0RJ;

    return-object v0
.end method


# virtual methods
.method public final getAnnotation()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 59872
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAnnotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59876
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasAttributes()Z
    .locals 1

    .prologue
    .line 59875
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59874
    const-string v0, "[none]"

    return-object v0
.end method

.method public final withoutAttributes()LX/0RK;
    .locals 2

    .prologue
    .line 59873
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Key already has no attributes."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
