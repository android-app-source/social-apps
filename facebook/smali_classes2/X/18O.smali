.class public final enum LX/18O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/18O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/18O;

.field public static final enum Avoid:LX/18O;

.field public static final enum Perform:LX/18O;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206456
    new-instance v0, LX/18O;

    const-string v1, "Perform"

    invoke-direct {v0, v1, v2}, LX/18O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18O;->Perform:LX/18O;

    .line 206457
    new-instance v0, LX/18O;

    const-string v1, "Avoid"

    invoke-direct {v0, v1, v3}, LX/18O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18O;->Avoid:LX/18O;

    .line 206458
    const/4 v0, 0x2

    new-array v0, v0, [LX/18O;

    sget-object v1, LX/18O;->Perform:LX/18O;

    aput-object v1, v0, v2

    sget-object v1, LX/18O;->Avoid:LX/18O;

    aput-object v1, v0, v3

    sput-object v0, LX/18O;->$VALUES:[LX/18O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/18O;
    .locals 1

    .prologue
    .line 206462
    const-class v0, LX/18O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/18O;

    return-object v0
.end method

.method public static values()[LX/18O;
    .locals 1

    .prologue
    .line 206461
    sget-object v0, LX/18O;->$VALUES:[LX/18O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/18O;

    return-object v0
.end method


# virtual methods
.method public final avoidShift()Z
    .locals 1

    .prologue
    .line 206460
    sget-object v0, LX/18O;->Avoid:LX/18O;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performShift()Z
    .locals 1

    .prologue
    .line 206459
    sget-object v0, LX/18O;->Perform:LX/18O;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
