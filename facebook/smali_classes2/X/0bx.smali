.class public LX/0bx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0bx;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Random;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15s;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/00H;

.field public f:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/00H;)V
    .locals 1
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/http/executors/qebased/QeBasedHttpExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15s;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/00H;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87496
    iput-object p1, p0, LX/0bx;->a:LX/0Ot;

    .line 87497
    iput-object p2, p0, LX/0bx;->c:LX/0Ot;

    .line 87498
    iput-object p3, p0, LX/0bx;->d:LX/0Ot;

    .line 87499
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/0bx;->b:Ljava/util/Random;

    .line 87500
    iput-object p4, p0, LX/0bx;->e:LX/00H;

    .line 87501
    const/16 v0, 0xa

    iput v0, p0, LX/0bx;->f:I

    .line 87502
    return-void
.end method

.method public static a(LX/0QB;)LX/0bx;
    .locals 7

    .prologue
    .line 87476
    sget-object v0, LX/0bx;->g:LX/0bx;

    if-nez v0, :cond_1

    .line 87477
    const-class v1, LX/0bx;

    monitor-enter v1

    .line 87478
    :try_start_0
    sget-object v0, LX/0bx;->g:LX/0bx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87479
    if-eqz v2, :cond_0

    .line 87480
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87481
    new-instance v4, LX/0bx;

    const/16 v3, 0x2500

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0xb63

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x1032

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, LX/00H;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    invoke-direct {v4, v5, v6, p0, v3}, LX/0bx;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/00H;)V

    .line 87482
    move-object v0, v4

    .line 87483
    sput-object v0, LX/0bx;->g:LX/0bx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87484
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87485
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87486
    :cond_1
    sget-object v0, LX/0bx;->g:LX/0bx;

    return-object v0

    .line 87487
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 87489
    iget-object v0, p0, LX/0bx;->e:LX/00H;

    .line 87490
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 87491
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_1

    .line 87492
    :cond_0
    :goto_0
    return-void

    .line 87493
    :cond_1
    iget-object v0, p0, LX/0bx;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0by;->K:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87494
    iget-object v0, p0, LX/0bx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    goto :goto_0
.end method
