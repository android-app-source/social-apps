.class public LX/1qI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1qI;


# instance fields
.field private final a:LX/1qJ;

.field private b:LX/2GJ;


# direct methods
.method public constructor <init>(LX/1qJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 330449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330450
    iput-object p1, p0, LX/1qI;->a:LX/1qJ;

    .line 330451
    return-void
.end method

.method public static a(LX/0QB;)LX/1qI;
    .locals 4

    .prologue
    .line 330452
    sget-object v0, LX/1qI;->c:LX/1qI;

    if-nez v0, :cond_1

    .line 330453
    const-class v1, LX/1qI;

    monitor-enter v1

    .line 330454
    :try_start_0
    sget-object v0, LX/1qI;->c:LX/1qI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 330455
    if-eqz v2, :cond_0

    .line 330456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 330457
    new-instance p0, LX/1qI;

    const-class v3, LX/1qJ;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1qJ;

    invoke-direct {p0, v3}, LX/1qI;-><init>(LX/1qJ;)V

    .line 330458
    move-object v0, p0

    .line 330459
    sput-object v0, LX/1qI;->c:LX/1qI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330460
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 330461
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330462
    :cond_1
    sget-object v0, LX/1qI;->c:LX/1qI;

    return-object v0

    .line 330463
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 330464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 330465
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    if-eqz v0, :cond_0

    .line 330466
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    .line 330467
    invoke-static {v0}, LX/2GJ;->e(LX/2GJ;)V

    .line 330468
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 6

    .prologue
    .line 330469
    iget-object v0, p0, LX/1qI;->a:LX/1qJ;

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-wide/32 v4, 0x5265c00

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1qJ;->a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)LX/2GJ;

    move-result-object v0

    iput-object v0, p0, LX/1qI;->b:LX/2GJ;

    .line 330470
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    .line 330471
    invoke-static {v0}, LX/2GJ;->e(LX/2GJ;)V

    .line 330472
    return-void
.end method

.method public final onOperationQueued(Ljava/lang/Class;LX/1qE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/1qE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330473
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    if-eqz v0, :cond_0

    .line 330474
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    .line 330475
    iget-object p0, v0, LX/2GJ;->w:Ljava/util/Map;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330476
    :cond_0
    return-void
.end method

.method public final onQueueEmpty(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330477
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    if-eqz v0, :cond_0

    .line 330478
    iget-object v0, p0, LX/1qI;->b:LX/2GJ;

    .line 330479
    iget-object p0, v0, LX/2GJ;->w:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330480
    invoke-static {v0}, LX/2GJ;->e(LX/2GJ;)V

    .line 330481
    :cond_0
    return-void
.end method
