.class public final LX/18s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/fragment/FbChromeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/fragment/FbChromeFragment;)V
    .locals 0

    .prologue
    .line 207083
    iput-object p1, p0, LX/18s;->a:Lcom/facebook/katana/fragment/FbChromeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x2a905e08

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 207061
    iget-object v1, p0, LX/18s;->a:Lcom/facebook/katana/fragment/FbChromeFragment;

    .line 207062
    invoke-static {p2}, LX/18q;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 207063
    iget-object p0, v1, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {p0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207064
    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 207065
    if-nez v2, :cond_1

    .line 207066
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Z)V

    .line 207067
    :cond_0
    :goto_1
    const/16 v1, 0x27

    const v2, 0x1f337a7d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 207068
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 207069
    instance-of p0, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    if-eqz p0, :cond_2

    check-cast v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 207070
    invoke-virtual {v2}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j()Z

    move-result p0

    if-eqz p0, :cond_6

    sget-object p0, LX/Gda;->NEWS_FEED_TAB:LX/Gda;

    invoke-static {v2, p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/Gda;)Z

    move-result p0

    if-eqz p0, :cond_6

    const/4 p0, 0x1

    :goto_2
    move v2, p0

    .line 207071
    if-nez v2, :cond_0

    .line 207072
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 207073
    if-eqz v2, :cond_4

    .line 207074
    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object p0

    .line 207075
    instance-of p2, p0, LX/0fv;

    if-eqz p2, :cond_3

    .line 207076
    iget-boolean p2, p0, Landroid/support/v4/app/Fragment;->mHidden:Z

    move p2, p2

    .line 207077
    if-nez p2, :cond_7

    .line 207078
    iget-boolean p2, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move p2, p2

    .line 207079
    if-eqz p2, :cond_7

    check-cast p0, LX/0fv;

    invoke-interface {p0}, LX/0fv;->j()Z

    move-result p0

    if-eqz p0, :cond_7

    :cond_3
    const/4 p0, 0x1

    :goto_3
    move p0, p0

    .line 207080
    if-nez p0, :cond_4

    .line 207081
    check-cast v2, LX/0fv;

    invoke-interface {v2}, LX/0fv;->f()V

    .line 207082
    :cond_4
    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    const/4 p0, 0x0

    goto :goto_2

    :cond_7
    const/4 p0, 0x0

    goto :goto_3
.end method
