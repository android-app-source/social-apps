.class public LX/0Td;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""

# interfaces
.implements LX/0Te;


# instance fields
.field public final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 63345
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 63346
    iput-object p1, p0, LX/0Td;->a:Landroid/os/Handler;

    .line 63347
    return-void
.end method

.method private b(Ljava/lang/Runnable;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63348
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0Td;->c(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/facebook/common/executors/ListenableScheduledFutureImpl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63349
    new-instance v0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    .line 63350
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    move-object v1, v1

    .line 63351
    invoke-direct {v0, v1, p1, p2}, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method private b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/facebook/common/executors/ListenableScheduledFutureImpl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63352
    new-instance v0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    .line 63353
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    move-object v1, v1

    .line 63354
    invoke-direct {v0, v1, p1}, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;-><init>(Landroid/os/Handler;Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method private c(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0YG;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "LX/0YG",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63355
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63356
    :cond_0
    invoke-direct {p0, p1, p2}, LX/0Td;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    move-result-object v0

    .line 63357
    const v1, -0x3852e4dc

    invoke-static {p0, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 63358
    return-object v0
.end method

.method private c(Ljava/util/concurrent/Callable;)LX/0YG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "LX/0YG",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63359
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63360
    :cond_0
    invoke-direct {p0, p1}, LX/0Td;->b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    move-result-object v0

    .line 63361
    const v1, -0x9a877c7

    invoke-static {p0, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 63362
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63363
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63364
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0Td;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    move-result-object v0

    .line 63365
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const v4, -0xb81a39e

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 63366
    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 63367
    invoke-direct {p0, p1}, LX/0Td;->b(Ljava/util/concurrent/Callable;)Lcom/facebook/common/executors/ListenableScheduledFutureImpl;

    move-result-object v0

    .line 63368
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const v4, 0xd6ed558

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 63369
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 63370
    invoke-direct {p0, p1}, LX/0Td;->b(Ljava/lang/Runnable;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63371
    invoke-direct {p0, p1, p2}, LX/0Td;->c(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 63326
    invoke-direct {p0, p1}, LX/0Td;->c(Ljava/util/concurrent/Callable;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 63372
    iget-object v0, p0, LX/0Td;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 63373
    return-void
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 63374
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 63375
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 63321
    instance-of v0, p1, Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 63322
    instance-of v0, p1, LX/17C;

    if-nez v0, :cond_0

    .line 63323
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "%s submitted as runnable to %s. Potential deadlocks on get()."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63324
    :cond_0
    iget-object v0, p0, LX/0Td;->a:Landroid/os/Handler;

    const v1, 0x209d8eab

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 63325
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 63327
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 63328
    const/4 v0, 0x0

    return v0
.end method

.method public final newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63329
    new-instance v0, Lcom/facebook/common/executors/HandlerListeningExecutorServiceImpl$ListenableScheduledRunnableFuture;

    .line 63330
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    move-object v1, v1

    .line 63331
    invoke-direct {v0, v1, p1, p2}, Lcom/facebook/common/executors/HandlerListeningExecutorServiceImpl$ListenableScheduledRunnableFuture;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63332
    new-instance v0, Lcom/facebook/common/executors/HandlerListeningExecutorServiceImpl$ListenableScheduledRunnableFuture;

    .line 63333
    iget-object v1, p0, LX/0Td;->a:Landroid/os/Handler;

    move-object v1, v1

    .line 63334
    invoke-direct {v0, v1, p1}, Lcom/facebook/common/executors/HandlerListeningExecutorServiceImpl$ListenableScheduledRunnableFuture;-><init>(Landroid/os/Handler;Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63335
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0Td;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63336
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0Td;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63337
    invoke-virtual/range {p0 .. p6}, LX/0Td;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63338
    invoke-virtual/range {p0 .. p6}, LX/0Td;->b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final shutdown()V
    .locals 1

    .prologue
    .line 63339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 63341
    invoke-direct {p0, p1}, LX/0Td;->b(Ljava/lang/Runnable;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63342
    invoke-direct {p0, p1, p2}, LX/0Td;->c(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 63343
    invoke-direct {p0, p1}, LX/0Td;->c(Ljava/util/concurrent/Callable;)LX/0YG;

    move-result-object v0

    return-object v0
.end method
