.class public LX/1Kv;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public final a:LX/0pn;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:Ljava/util/Timer;

.field public d:LX/7mu;

.field public e:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0pn;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233356
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233357
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Kv;->f:Z

    .line 233358
    iput-object p2, p0, LX/1Kv;->a:LX/0pn;

    .line 233359
    iput-object p1, p0, LX/1Kv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 233360
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 9

    .prologue
    .line 233366
    iget-object v0, p0, LX/1Kv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->h:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 233367
    :cond_0
    :goto_0
    return-void

    .line 233368
    :cond_1
    iget-object v0, p0, LX/1Kv;->e:LX/1Iu;

    .line 233369
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 233370
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 233371
    iget-object v1, p0, LX/1Kv;->d:LX/7mu;

    if-eqz v1, :cond_2

    .line 233372
    :goto_1
    iget-boolean v0, p0, LX/1Kv;->f:Z

    if-nez v0, :cond_0

    .line 233373
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Kv;->f:Z

    .line 233374
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    iput-object v3, p0, LX/1Kv;->c:Ljava/util/Timer;

    .line 233375
    iget-object v3, p0, LX/1Kv;->c:Ljava/util/Timer;

    new-instance v4, Lcom/facebook/debug/feed/HomeStoriesViewController$1;

    invoke-direct {v4, p0}, Lcom/facebook/debug/feed/HomeStoriesViewController$1;-><init>(LX/1Kv;)V

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x3e8

    invoke-virtual/range {v3 .. v8}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 233376
    goto :goto_0

    .line 233377
    :cond_2
    new-instance v1, LX/7mu;

    invoke-direct {v1, v0}, LX/7mu;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/1Kv;->d:LX/7mu;

    .line 233378
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x2bc

    const/16 v3, 0x1e

    const/4 v4, 0x3

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 233379
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget-object v3, p0, LX/1Kv;->d:LX/7mu;

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 233361
    iget-boolean v0, p0, LX/1Kv;->f:Z

    if-nez v0, :cond_0

    .line 233362
    :goto_0
    return-void

    .line 233363
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Kv;->f:Z

    .line 233364
    iget-object v0, p0, LX/1Kv;->c:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 233365
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Kv;->c:Ljava/util/Timer;

    goto :goto_0
.end method
