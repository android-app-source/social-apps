.class public LX/0hw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Wd;

.field public final d:LX/0Zb;

.field public final e:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Wd;LX/0Zb;LX/0Uh;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .param p5    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Zb;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 119284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119285
    iput-object p1, p0, LX/0hw;->a:Landroid/content/Context;

    .line 119286
    iput-object p2, p0, LX/0hw;->b:LX/0Ot;

    .line 119287
    iput-object p3, p0, LX/0hw;->c:LX/0Wd;

    .line 119288
    iput-object p4, p0, LX/0hw;->d:LX/0Zb;

    .line 119289
    iput-object p5, p0, LX/0hw;->e:LX/0Uh;

    .line 119290
    return-void
.end method

.method public static b(LX/0QB;)LX/0hw;
    .locals 6

    .prologue
    .line 119299
    new-instance v0, LX/0hw;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xafd

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v3

    check-cast v3, LX/0Wd;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct/range {v0 .. v5}, LX/0hw;-><init>(Landroid/content/Context;LX/0Ot;LX/0Wd;LX/0Zb;LX/0Uh;)V

    .line 119300
    return-object v0
.end method


# virtual methods
.method public final a(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 119293
    const/4 v0, 0x0

    .line 119294
    :try_start_0
    iget-object v1, p0, LX/0hw;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v1

    .line 119295
    iget-object v2, v1, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    move-object v0, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119296
    :goto_0
    if-eqz p1, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119297
    iget-object v0, p0, LX/0hw;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119298
    :cond_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 119291
    iget-object v0, p0, LX/0hw;->c:LX/0Wd;

    new-instance v1, Lcom/facebook/growth/sem/SemTrackingLogger$1;

    invoke-direct {v1, p0}, Lcom/facebook/growth/sem/SemTrackingLogger$1;-><init>(LX/0hw;)V

    const v2, -0x785879d6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 119292
    return-void
.end method
