.class public LX/0tO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0tO;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0hB;

.field public final c:Landroid/content/Context;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(LX/0ad;LX/0hB;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154533
    iput-object p1, p0, LX/0tO;->a:LX/0ad;

    .line 154534
    iput-object p2, p0, LX/0tO;->b:LX/0hB;

    .line 154535
    iput-object p3, p0, LX/0tO;->c:Landroid/content/Context;

    .line 154536
    iget-object v0, p0, LX/0tO;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 154537
    iget-object v1, p0, LX/0tO;->c:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/0tO;->d:I

    .line 154538
    iget-object v0, p0, LX/0tO;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 154539
    iget-object v1, p0, LX/0tO;->c:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/0tO;->e:I

    .line 154540
    return-void
.end method

.method public static a(LX/0QB;)LX/0tO;
    .locals 6

    .prologue
    .line 154519
    sget-object v0, LX/0tO;->f:LX/0tO;

    if-nez v0, :cond_1

    .line 154520
    const-class v1, LX/0tO;

    monitor-enter v1

    .line 154521
    :try_start_0
    sget-object v0, LX/0tO;->f:LX/0tO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154522
    if-eqz v2, :cond_0

    .line 154523
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154524
    new-instance p0, LX/0tO;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/0tO;-><init>(LX/0ad;LX/0hB;Landroid/content/Context;)V

    .line 154525
    move-object v0, p0

    .line 154526
    sput-object v0, LX/0tO;->f:LX/0tO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154527
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154528
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154529
    :cond_1
    sget-object v0, LX/0tO;->f:LX/0tO;

    return-object v0

    .line 154530
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154531
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154518
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    .line 154516
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/0wk;->q:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    move v0, v0

    .line 154517
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0tO;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 5

    .prologue
    .line 154515
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/0wk;->y:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154509
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-short v1, LX/0wk;->y:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154510
    const/4 v0, 0x2

    .line 154511
    :goto_0
    return v0

    .line 154512
    :cond_0
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-short v1, LX/0wk;->q:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154513
    const/4 v0, 0x1

    goto :goto_0

    .line 154514
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final h()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v0, 0x96

    .line 154541
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154542
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->H:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    .line 154543
    :cond_0
    :goto_0
    return v0

    .line 154544
    :cond_1
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->q:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154545
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->o:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final i()I
    .locals 4

    .prologue
    const/16 v3, 0xfa

    const/4 v0, 0x0

    .line 154504
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154505
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget v1, LX/0wk;->E:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    .line 154506
    :cond_0
    :goto_0
    return v0

    .line 154507
    :cond_1
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154508
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget v1, LX/0wk;->r:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final l()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 154501
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154502
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->v:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    .line 154503
    :cond_0
    return v0
.end method

.method public final n()I
    .locals 4

    .prologue
    const/16 v0, 0xe

    .line 154498
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154499
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->z:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    .line 154500
    :cond_0
    return v0
.end method

.method public final o()I
    .locals 3

    .prologue
    .line 154495
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-short v1, LX/0wk;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154496
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget v1, LX/0wk;->A:I

    iget v2, p0, LX/0tO;->d:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 154497
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0tO;->d:I

    goto :goto_0
.end method

.method public final p()I
    .locals 3

    .prologue
    .line 154492
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget-short v1, LX/0wk;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154493
    iget-object v0, p0, LX/0tO;->a:LX/0ad;

    sget v1, LX/0wk;->I:I

    iget v2, p0, LX/0tO;->e:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 154494
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0tO;->e:I

    goto :goto_0
.end method

.method public final q()F
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 154489
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154490
    iget-object v1, p0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->B:F

    invoke-interface {v1, v2, v0}, LX/0ad;->a(FF)F

    move-result v0

    .line 154491
    :cond_0
    return v0
.end method
