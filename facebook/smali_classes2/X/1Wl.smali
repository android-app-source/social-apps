.class public final enum LX/1Wl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Wl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Wl;

.field public static final enum HIDDEN:LX/1Wl;

.field public static final enum VISIBLE:LX/1Wl;


# instance fields
.field public final visibilityValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 269413
    new-instance v0, LX/1Wl;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v3, v3}, LX/1Wl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Wl;->VISIBLE:LX/1Wl;

    .line 269414
    new-instance v0, LX/1Wl;

    const-string v1, "HIDDEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v4, v2}, LX/1Wl;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Wl;->HIDDEN:LX/1Wl;

    .line 269415
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Wl;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    aput-object v1, v0, v3

    sget-object v1, LX/1Wl;->HIDDEN:LX/1Wl;

    aput-object v1, v0, v4

    sput-object v0, LX/1Wl;->$VALUES:[LX/1Wl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 269416
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 269417
    iput p3, p0, LX/1Wl;->visibilityValue:I

    .line 269418
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Wl;
    .locals 1

    .prologue
    .line 269419
    const-class v0, LX/1Wl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Wl;

    return-object v0
.end method

.method public static values()[LX/1Wl;
    .locals 1

    .prologue
    .line 269420
    sget-object v0, LX/1Wl;->$VALUES:[LX/1Wl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Wl;

    return-object v0
.end method
