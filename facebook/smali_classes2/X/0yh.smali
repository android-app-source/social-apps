.class public final enum LX/0yh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yh;

.field public static final enum DIALTONE:LX/0yh;

.field public static final enum NORMAL:LX/0yh;


# instance fields
.field public mZeroTokenType:LX/0yi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 165835
    new-instance v0, LX/0yh;

    const-string v1, "NORMAL"

    sget-object v2, LX/0yi;->NORMAL:LX/0yi;

    invoke-direct {v0, v1, v3, v2}, LX/0yh;-><init>(Ljava/lang/String;ILX/0yi;)V

    sput-object v0, LX/0yh;->NORMAL:LX/0yh;

    .line 165836
    new-instance v0, LX/0yh;

    const-string v1, "DIALTONE"

    sget-object v2, LX/0yi;->DIALTONE:LX/0yi;

    invoke-direct {v0, v1, v4, v2}, LX/0yh;-><init>(Ljava/lang/String;ILX/0yi;)V

    sput-object v0, LX/0yh;->DIALTONE:LX/0yh;

    .line 165837
    const/4 v0, 0x2

    new-array v0, v0, [LX/0yh;

    sget-object v1, LX/0yh;->NORMAL:LX/0yh;

    aput-object v1, v0, v3

    sget-object v1, LX/0yh;->DIALTONE:LX/0yh;

    aput-object v1, v0, v4

    sput-object v0, LX/0yh;->$VALUES:[LX/0yh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/0yi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 165833
    iput-object p3, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    .line 165834
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yh;
    .locals 1

    .prologue
    .line 165818
    const-class v0, LX/0yh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yh;

    return-object v0
.end method

.method public static values()[LX/0yh;
    .locals 1

    .prologue
    .line 165838
    sget-object v0, LX/0yh;->$VALUES:[LX/0yh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yh;

    return-object v0
.end method


# virtual methods
.method public final getBackupRewriteRulesKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165839
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getBackupRewriteRulesKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getBaseToken()LX/0yi;
    .locals 1

    .prologue
    .line 165844
    iget-object v0, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    return-object v0
.end method

.method public final getCampaignIdKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165840
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getCampaignIdKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getCarrierIdKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165841
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getCarrierIdKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getCarrierLogoUrlKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165842
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getCarrierLogoUrlKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getCarrierNameKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165843
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getCarrierNameKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getClearablePreferencesRoot()LX/0Tn;
    .locals 2

    .prologue
    .line 165830
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getClearablePreferencesRoot()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getLastTimeCheckedKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165831
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getLastTimeCheckedKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getModeNumber()B
    .locals 1

    .prologue
    .line 165829
    iget-object v0, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getModeNumber()B

    move-result v0

    return v0
.end method

.method public final getPoolPricingMapKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165828
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getPoolPricingMapKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getRegistrationStatusKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165827
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getRegistrationStatusKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getRewriteRulesKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165826
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getRewriteRulesKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getStatusKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165825
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getTokenFastHashKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165824
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getTokenFastHashKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getTokenHashKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165823
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getTokenHashKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getTokenRequestTimeKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165822
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getTokenRequestTimeKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getTokenTTLKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165821
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getTokenTTLKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getUIFeaturesKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165820
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public final getUnregisteredReasonKey()LX/0Tn;
    .locals 2

    .prologue
    .line 165819
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    iget-object v1, p0, LX/0yh;->mZeroTokenType:LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getUnregisteredReasonKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
