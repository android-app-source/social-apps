.class public final enum LX/1Xu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Xu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Xu;

.field public static final enum ALWAYS:LX/1Xu;

.field public static final enum NON_DEFAULT:LX/1Xu;

.field public static final enum NON_EMPTY:LX/1Xu;

.field public static final enum NON_NULL:LX/1Xu;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272588
    new-instance v0, LX/1Xu;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, LX/1Xu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xu;->ALWAYS:LX/1Xu;

    .line 272589
    new-instance v0, LX/1Xu;

    const-string v1, "NON_NULL"

    invoke-direct {v0, v1, v3}, LX/1Xu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xu;->NON_NULL:LX/1Xu;

    .line 272590
    new-instance v0, LX/1Xu;

    const-string v1, "NON_DEFAULT"

    invoke-direct {v0, v1, v4}, LX/1Xu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xu;->NON_DEFAULT:LX/1Xu;

    .line 272591
    new-instance v0, LX/1Xu;

    const-string v1, "NON_EMPTY"

    invoke-direct {v0, v1, v5}, LX/1Xu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xu;->NON_EMPTY:LX/1Xu;

    .line 272592
    const/4 v0, 0x4

    new-array v0, v0, [LX/1Xu;

    sget-object v1, LX/1Xu;->ALWAYS:LX/1Xu;

    aput-object v1, v0, v2

    sget-object v1, LX/1Xu;->NON_NULL:LX/1Xu;

    aput-object v1, v0, v3

    sget-object v1, LX/1Xu;->NON_DEFAULT:LX/1Xu;

    aput-object v1, v0, v4

    sget-object v1, LX/1Xu;->NON_EMPTY:LX/1Xu;

    aput-object v1, v0, v5

    sput-object v0, LX/1Xu;->$VALUES:[LX/1Xu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 272593
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Xu;
    .locals 1

    .prologue
    .line 272594
    const-class v0, LX/1Xu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Xu;

    return-object v0
.end method

.method public static values()[LX/1Xu;
    .locals 1

    .prologue
    .line 272595
    sget-object v0, LX/1Xu;->$VALUES:[LX/1Xu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Xu;

    return-object v0
.end method
