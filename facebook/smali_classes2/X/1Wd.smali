.class public LX/1Wd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7X;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7Z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 269205
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1Wd;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7Z;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269169
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 269170
    iput-object p1, p0, LX/1Wd;->b:LX/0Ot;

    .line 269171
    return-void
.end method

.method public static a(LX/0QB;)LX/1Wd;
    .locals 4

    .prologue
    .line 269194
    const-class v1, LX/1Wd;

    monitor-enter v1

    .line 269195
    :try_start_0
    sget-object v0, LX/1Wd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269196
    sput-object v2, LX/1Wd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269197
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269198
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269199
    new-instance v3, LX/1Wd;

    const/16 p0, 0x1f97

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Wd;-><init>(LX/0Ot;)V

    .line 269200
    move-object v0, v3

    .line 269201
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269202
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Wd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269203
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269204
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269206
    const v0, 0xce04c58

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 269189
    iget-object v0, p0, LX/1Wd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 269190
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/3mm;->c(LX/1De;)LX/3mp;

    move-result-object v1

    const p0, 0x7f081b4f

    invoke-virtual {v1, p0}, LX/3mp;->h(I)LX/3mp;

    move-result-object v1

    .line 269191
    const p0, 0xce04c58

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 269192
    invoke-virtual {v1, p0}, LX/3mp;->a(LX/1dQ;)LX/3mp;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 269193
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 269172
    invoke-static {}, LX/1dS;->b()V

    .line 269173
    iget v0, p1, LX/1dQ;->b:I

    .line 269174
    packed-switch v0, :pswitch_data_0

    .line 269175
    :goto_0
    return-object v2

    .line 269176
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 269177
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 269178
    check-cast v1, LX/C7Y;

    .line 269179
    iget-object v3, p0, LX/1Wd;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C7Z;

    iget-object v4, v1, LX/C7Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269180
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 269181
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const p2, 0x490bb763

    invoke-static {p1, p2}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p1

    .line 269182
    if-nez p1, :cond_1

    .line 269183
    :cond_0
    :goto_1
    goto :goto_0

    .line 269184
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p1

    .line 269185
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 269186
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 269187
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 269188
    iget-object p2, v3, LX/C7Z;->a:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xce04c58
        :pswitch_0
    .end packed-switch
.end method
