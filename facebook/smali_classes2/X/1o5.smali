.class public final LX/1o5;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1o2;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/1o3;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 318383
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "src"

    aput-object v2, v0, v1

    sput-object v0, LX/1o5;->b:[Ljava/lang/String;

    .line 318384
    sput v3, LX/1o5;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 318374
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 318375
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/1o5;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1o5;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1o5;LX/1De;IILX/1o3;)V
    .locals 1

    .prologue
    .line 318379
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 318380
    iput-object p4, p0, LX/1o5;->a:LX/1o3;

    .line 318381
    iget-object v0, p0, LX/1o5;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 318382
    return-void
.end method


# virtual methods
.method public final a(LX/1dc;)LX/1o5;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o5;"
        }
    .end annotation

    .prologue
    .line 318376
    iget-object v0, p0, LX/1o5;->a:LX/1o3;

    iput-object p1, v0, LX/1o3;->a:LX/1dc;

    .line 318377
    iget-object v0, p0, LX/1o5;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 318378
    return-object p0
.end method

.method public final a(LX/1n6;)LX/1o5;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o5;"
        }
    .end annotation

    .prologue
    .line 318385
    iget-object v0, p0, LX/1o5;->a:LX/1o3;

    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1o3;->a:LX/1dc;

    .line 318386
    iget-object v0, p0, LX/1o5;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 318387
    return-object p0
.end method

.method public final a(Landroid/widget/ImageView$ScaleType;)LX/1o5;
    .locals 1

    .prologue
    .line 318359
    iget-object v0, p0, LX/1o5;->a:LX/1o3;

    iput-object p1, v0, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    .line 318360
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 318355
    invoke-super {p0}, LX/1X5;->a()V

    .line 318356
    const/4 v0, 0x0

    iput-object v0, p0, LX/1o5;->a:LX/1o3;

    .line 318357
    sget-object v0, LX/1o2;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 318358
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1o2;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 318361
    iget-object v1, p0, LX/1o5;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1o5;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/1o5;->c:I

    if-ge v1, v2, :cond_2

    .line 318362
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 318363
    :goto_0
    sget v2, LX/1o5;->c:I

    if-ge v0, v2, :cond_1

    .line 318364
    iget-object v2, p0, LX/1o5;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 318365
    sget-object v2, LX/1o5;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318366
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318367
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318368
    :cond_2
    iget-object v0, p0, LX/1o5;->a:LX/1o3;

    .line 318369
    invoke-virtual {p0}, LX/1o5;->a()V

    .line 318370
    return-object v0
.end method

.method public final h(I)LX/1o5;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 318371
    iget-object v0, p0, LX/1o5;->a:LX/1o3;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1o3;->a:LX/1dc;

    .line 318372
    iget-object v0, p0, LX/1o5;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 318373
    return-object p0
.end method
