.class public abstract LX/0gG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111814
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, LX/0gG;->a:Landroid/database/DataSetObservable;

    return-void
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 111812
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method destroyItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 111811
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 111802
    const/4 v0, -0x1

    return v0
.end method

.method public a(Landroid/view/View;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 111810
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method instantiateItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111809
    invoke-virtual {p0, p1, p2}, LX/0gG;->a(Landroid/view/View;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 111807
    iget-object v0, p0, LX/0gG;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 111808
    return-void
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 111806
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 111805
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 111803
    invoke-static {}, LX/0gG;->d()V

    .line 111804
    return-void
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)Z
.end method

.method public abstract b()I
.end method

.method public b(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 111794
    iget-object v0, p0, LX/0gG;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 111795
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 111796
    return-void
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 111797
    return-void
.end method

.method public d(I)F
    .locals 1

    .prologue
    .line 111798
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public kV_()V
    .locals 1

    .prologue
    .line 111799
    iget-object v0, p0, LX/0gG;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 111800
    return-void
.end method

.method public lf_()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 111801
    const/4 v0, 0x0

    return-object v0
.end method
