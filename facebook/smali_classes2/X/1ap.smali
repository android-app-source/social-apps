.class public LX/1ap;
.super LX/1aq;
.source ""


# instance fields
.field public a:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public c:J
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public d:[I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public e:[I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public g:[Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final i:[Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 278563
    invoke-direct {p0, p1}, LX/1aq;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 278564
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "At least one layer required!"

    invoke-static {v0, v2}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 278565
    iput-object p1, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    .line 278566
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, LX/1ap;->d:[I

    .line 278567
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, LX/1ap;->e:[I

    .line 278568
    const/16 v0, 0xff

    iput v0, p0, LX/1ap;->f:I

    .line 278569
    array-length v0, p1

    new-array v0, v0, [Z

    iput-object v0, p0, LX/1ap;->g:[Z

    .line 278570
    iput v1, p0, LX/1ap;->h:I

    .line 278571
    const/16 v1, 0xff

    const/4 v2, 0x0

    .line 278572
    const/4 v0, 0x2

    iput v0, p0, LX/1ap;->a:I

    .line 278573
    iget-object v0, p0, LX/1ap;->d:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 278574
    iget-object v0, p0, LX/1ap;->d:[I

    aput v1, v0, v2

    .line 278575
    iget-object v0, p0, LX/1ap;->e:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 278576
    iget-object v0, p0, LX/1ap;->e:[I

    aput v1, v0, v2

    .line 278577
    iget-object v0, p0, LX/1ap;->g:[Z

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([ZZ)V

    .line 278578
    iget-object v0, p0, LX/1ap;->g:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, v2

    .line 278579
    return-void

    :cond_0
    move v0, v1

    .line 278580
    goto :goto_0
.end method

.method private a(F)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/16 v7, 0xff

    const/4 v1, 0x0

    .line 278523
    move v0, v1

    move v2, v3

    .line 278524
    :goto_0
    iget-object v4, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    array-length v4, v4

    if-ge v0, v4, :cond_5

    .line 278525
    iget-object v4, p0, LX/1ap;->g:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_4

    move v4, v3

    .line 278526
    :goto_1
    iget-object v5, p0, LX/1ap;->e:[I

    iget-object v6, p0, LX/1ap;->d:[I

    aget v6, v6, v0

    int-to-float v6, v6

    mul-int/lit16 v4, v4, 0xff

    int-to-float v4, v4

    mul-float/2addr v4, p1

    add-float/2addr v4, v6

    float-to-int v4, v4

    aput v4, v5, v0

    .line 278527
    iget-object v4, p0, LX/1ap;->e:[I

    aget v4, v4, v0

    if-gez v4, :cond_0

    .line 278528
    iget-object v4, p0, LX/1ap;->e:[I

    aput v1, v4, v0

    .line 278529
    :cond_0
    iget-object v4, p0, LX/1ap;->e:[I

    aget v4, v4, v0

    if-le v4, v7, :cond_1

    .line 278530
    iget-object v4, p0, LX/1ap;->e:[I

    aput v7, v4, v0

    .line 278531
    :cond_1
    iget-object v4, p0, LX/1ap;->g:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/1ap;->e:[I

    aget v4, v4, v0

    if-ge v4, v7, :cond_2

    move v2, v1

    .line 278532
    :cond_2
    iget-object v4, p0, LX/1ap;->g:[Z

    aget-boolean v4, v4, v0

    if-nez v4, :cond_3

    iget-object v4, p0, LX/1ap;->e:[I

    aget v4, v4, v0

    if-lez v4, :cond_3

    move v2, v1

    .line 278533
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278534
    :cond_4
    const/4 v4, -0x1

    goto :goto_1

    .line 278535
    :cond_5
    return v2
.end method

.method private static h()J
    .locals 2

    .prologue
    .line 278522
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 278520
    iget v0, p0, LX/1ap;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1ap;->h:I

    .line 278521
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 278517
    iget v0, p0, LX/1ap;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1ap;->h:I

    .line 278518
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278519
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 278513
    iput p1, p0, LX/1ap;->b:I

    .line 278514
    iget v0, p0, LX/1ap;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 278515
    const/4 v0, 0x0

    iput v0, p0, LX/1ap;->a:I

    .line 278516
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 278536
    iget v0, p0, LX/1ap;->a:I

    packed-switch v0, :pswitch_data_0

    .line 278537
    :goto_0
    :pswitch_0
    iget-object v0, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    if-ge v3, v0, :cond_5

    .line 278538
    iget-object v0, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, v3

    iget-object v1, p0, LX/1ap;->e:[I

    aget v1, v1, v3

    iget v4, p0, LX/1ap;->f:I

    mul-int/2addr v1, v4

    div-int/lit16 v1, v1, 0xff

    .line 278539
    if-eqz v0, :cond_0

    if-lez v1, :cond_0

    .line 278540
    iget v4, p0, LX/1ap;->h:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/1ap;->h:I

    .line 278541
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 278542
    iget v4, p0, LX/1ap;->h:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, LX/1ap;->h:I

    .line 278543
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 278544
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 278545
    :pswitch_1
    iget-object v0, p0, LX/1ap;->e:[I

    iget-object v4, p0, LX/1ap;->d:[I

    iget-object v5, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    array-length v5, v5

    invoke-static {v0, v3, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 278546
    invoke-static {}, LX/1ap;->h()J

    move-result-wide v4

    iput-wide v4, p0, LX/1ap;->c:J

    .line 278547
    iget v0, p0, LX/1ap;->b:I

    if-nez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 278548
    :goto_1
    invoke-direct {p0, v0}, LX/1ap;->a(F)Z

    move-result v4

    .line 278549
    if-eqz v4, :cond_2

    move v0, v1

    :goto_2
    iput v0, p0, LX/1ap;->a:I

    move v2, v4

    .line 278550
    goto :goto_0

    .line 278551
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 278552
    goto :goto_2

    .line 278553
    :pswitch_2
    iget v0, p0, LX/1ap;->b:I

    if-lez v0, :cond_3

    move v0, v2

    :goto_3
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 278554
    invoke-static {}, LX/1ap;->h()J

    move-result-wide v4

    iget-wide v6, p0, LX/1ap;->c:J

    sub-long/2addr v4, v6

    long-to-float v0, v4

    iget v4, p0, LX/1ap;->b:I

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 278555
    invoke-direct {p0, v0}, LX/1ap;->a(F)Z

    move-result v0

    .line 278556
    if-eqz v0, :cond_4

    :goto_4
    iput v1, p0, LX/1ap;->a:I

    move v2, v0

    .line 278557
    goto :goto_0

    :cond_3
    move v0, v3

    .line 278558
    goto :goto_3

    :cond_4
    move v1, v2

    .line 278559
    goto :goto_4

    .line 278560
    :cond_5
    if-nez v2, :cond_6

    .line 278561
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278562
    :cond_6
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final f()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 278506
    const/4 v0, 0x2

    iput v0, p0, LX/1ap;->a:I

    move v0, v1

    .line 278507
    :goto_0
    iget-object v2, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278508
    iget-object v3, p0, LX/1ap;->e:[I

    iget-object v2, p0, LX/1ap;->g:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    const/16 v2, 0xff

    :goto_1
    aput v2, v3, v0

    .line 278509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 278510
    goto :goto_1

    .line 278511
    :cond_1
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278512
    return-void
.end method

.method public final f(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278501
    iput v1, p0, LX/1ap;->a:I

    .line 278502
    iget-object v0, p0, LX/1ap;->g:[Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 278503
    iget-object v0, p0, LX/1ap;->g:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 278504
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278505
    return-void
.end method

.method public final g(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278496
    iput v3, p0, LX/1ap;->a:I

    .line 278497
    iget-object v0, p0, LX/1ap;->g:[Z

    add-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 278498
    iget-object v0, p0, LX/1ap;->g:[Z

    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, LX/1ap;->i:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 278499
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278500
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 278495
    iget v0, p0, LX/1ap;->f:I

    return v0
.end method

.method public final invalidateSelf()V
    .locals 1

    .prologue
    .line 278492
    iget v0, p0, LX/1ap;->h:I

    if-nez v0, :cond_0

    .line 278493
    invoke-super {p0}, LX/1aq;->invalidateSelf()V

    .line 278494
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 278488
    iget v0, p0, LX/1ap;->f:I

    if-eq v0, p1, :cond_0

    .line 278489
    iput p1, p0, LX/1ap;->f:I

    .line 278490
    invoke-virtual {p0}, LX/1ap;->invalidateSelf()V

    .line 278491
    :cond_0
    return-void
.end method
