.class public LX/1VE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I

.field private static volatile g:LX/1VE;


# instance fields
.field private final b:LX/14w;

.field private final c:LX/1VF;

.field private final d:LX/0qn;

.field private final e:LX/0ad;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 257577
    const v0, 0x7f0a010c

    sput v0, LX/1VE;->a:I

    return-void
.end method

.method public constructor <init>(LX/14w;LX/1VF;LX/0qn;Landroid/content/res/Resources;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257579
    iput-object p1, p0, LX/1VE;->b:LX/14w;

    .line 257580
    iput-object p2, p0, LX/1VE;->c:LX/1VF;

    .line 257581
    iput-object p3, p0, LX/1VE;->d:LX/0qn;

    .line 257582
    iput-object p4, p0, LX/1VE;->f:Landroid/content/res/Resources;

    .line 257583
    iput-object p5, p0, LX/1VE;->e:LX/0ad;

    .line 257584
    return-void
.end method

.method public static a(LX/0QB;)LX/1VE;
    .locals 9

    .prologue
    .line 257556
    sget-object v0, LX/1VE;->g:LX/1VE;

    if-nez v0, :cond_1

    .line 257557
    const-class v1, LX/1VE;

    monitor-enter v1

    .line 257558
    :try_start_0
    sget-object v0, LX/1VE;->g:LX/1VE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 257559
    if-eqz v2, :cond_0

    .line 257560
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 257561
    new-instance v3, LX/1VE;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v5

    check-cast v5, LX/1VF;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v6

    check-cast v6, LX/0qn;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1VE;-><init>(LX/14w;LX/1VF;LX/0qn;Landroid/content/res/Resources;LX/0ad;)V

    .line 257562
    move-object v0, v3

    .line 257563
    sput-object v0, LX/1VE;->g:LX/1VE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 257565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 257566
    :cond_1
    sget-object v0, LX/1VE;->g:LX/1VE;

    return-object v0

    .line 257567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 257568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 257569
    iget-object v0, p0, LX/1VE;->b:LX/14w;

    invoke-virtual {v0, p1}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 257570
    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, LX/1Ua;->j:LX/1Ua;

    .line 257571
    :goto_1
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 257572
    invoke-static {p1}, LX/14w;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/1VE;->b:LX/14w;

    invoke-virtual {v2, v1}, LX/14w;->j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/1X9;->TOP:LX/1X9;

    .line 257573
    :goto_2
    new-instance v2, LX/1X6;

    invoke-direct {v2, p1, v0, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    return-object v2

    .line 257574
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 257575
    :cond_1
    sget-object v0, LX/1Ua;->h:LX/1Ua;

    goto :goto_1

    .line 257576
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;
    .locals 2
    .param p2    # LX/1SX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper;",
            ")",
            "LX/1dl;"
        }
    .end annotation

    .prologue
    .line 257585
    iget-object v0, p0, LX/1VE;->c:LX/1VF;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p1, p2, v1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;Z)Z

    move-result v0

    .line 257586
    if-nez v0, :cond_0

    .line 257587
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    .line 257588
    :goto_0
    return-object v0

    .line 257589
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257590
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257591
    iget-object v1, p0, LX/1VE;->d:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 257592
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 257593
    if-eqz v0, :cond_1

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    goto :goto_0

    :cond_1
    sget-object v0, LX/1dl;->VISIBLE:LX/1dl;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 257543
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257544
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 257545
    :goto_0
    return v0

    .line 257546
    :cond_0
    invoke-interface {p2}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    if-ne v0, v2, :cond_1

    .line 257547
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 257548
    if-nez v0, :cond_1

    .line 257549
    iget-object v0, p0, LX/1VE;->e:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/0wi;->h:S

    invoke-interface {v0, v2, v3, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 257550
    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 257551
    iget-object v1, p0, LX/1VE;->b:LX/14w;

    invoke-virtual {v1, p1}, LX/14w;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, LX/1VE;->e:LX/0ad;

    sget-short v2, LX/0wi;->o:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 257552
    invoke-virtual {p0, p1}, LX/1VE;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 257553
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1VE;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b1078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1VE;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 257554
    invoke-virtual {p0, p1}, LX/1VE;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 257555
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1VE;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b1077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1VE;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method
