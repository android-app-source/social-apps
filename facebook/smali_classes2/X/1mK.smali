.class public abstract LX/1mK;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/1mL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313618
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 313619
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p0, p0, v0}, LX/1mK;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 313620
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)LX/1mL;
    .locals 2

    .prologue
    .line 313621
    if-nez p0, :cond_0

    .line 313622
    const/4 v0, 0x0

    .line 313623
    :goto_0
    return-object v0

    .line 313624
    :cond_0
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 313625
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/1mL;

    if-eqz v1, :cond_1

    .line 313626
    check-cast v0, LX/1mL;

    goto :goto_0

    .line 313627
    :cond_1
    new-instance v0, Lcom/facebook/fbservice/service/IBlueService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/facebook/fbservice/service/IBlueService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 313628
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 313629
    sparse-switch p1, :sswitch_data_0

    .line 313630
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 313631
    :sswitch_0
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 313632
    :sswitch_1
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313633
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 313634
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 313635
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    .line 313636
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v2, v7

    .line 313637
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 313638
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 313639
    :goto_3
    invoke-virtual {p0, v3, v1, v2, v0}, LX/1mK;->startOperation(Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    .line 313640
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313641
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object v1, v6

    .line 313642
    goto :goto_1

    :cond_1
    move v2, v4

    .line 313643
    goto :goto_2

    :cond_2
    move-object v0, v6

    .line 313644
    goto :goto_3

    .line 313645
    :sswitch_2
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313646
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 313647
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 313648
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v0

    .line 313649
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v3, v7

    .line 313650
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/1qA;->asInterface(Landroid/os/IBinder;)LX/1qB;

    move-result-object v4

    .line 313651
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 313652
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v0

    :goto_6
    move-object v0, p0

    .line 313653
    invoke-virtual/range {v0 .. v5}, LX/1mK;->startOperationWithCompletionHandler(Ljava/lang/String;Landroid/os/Bundle;ZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    .line 313654
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313655
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move-object v2, v6

    .line 313656
    goto :goto_4

    :cond_4
    move v3, v4

    .line 313657
    goto :goto_5

    :cond_5
    move-object v5, v6

    .line 313658
    goto :goto_6

    .line 313659
    :sswitch_3
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313660
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 313661
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 313662
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v0

    .line 313663
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    move v3, v7

    .line 313664
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v4, v7

    .line 313665
    :cond_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/1qA;->asInterface(Landroid/os/IBinder;)LX/1qB;

    move-result-object v5

    .line 313666
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 313667
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    move-object v6, v0

    :cond_7
    move-object v0, p0

    .line 313668
    invoke-virtual/range {v0 .. v6}, LX/1mK;->startOperationWithCompletionHandlerAppInit(Ljava/lang/String;Landroid/os/Bundle;ZZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    .line 313669
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313670
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move-object v2, v6

    .line 313671
    goto :goto_7

    :cond_9
    move v3, v4

    .line 313672
    goto :goto_8

    .line 313673
    :sswitch_4
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313674
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 313675
    invoke-virtual {p0, v0}, LX/1mK;->cancel(Ljava/lang/String;)Z

    move-result v0

    .line 313676
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313677
    if-eqz v0, :cond_a

    move v4, v7

    :cond_a
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 313678
    :sswitch_5
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313679
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 313680
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 313681
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    .line 313682
    :goto_9
    invoke-virtual {p0, v1, v0}, LX/1mK;->changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z

    move-result v0

    .line 313683
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313684
    if-eqz v0, :cond_b

    move v4, v7

    :cond_b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_c
    move-object v0, v6

    .line 313685
    goto :goto_9

    .line 313686
    :sswitch_6
    const-string v0, "com.facebook.fbservice.service.IBlueService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 313687
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 313688
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, LX/1qA;->asInterface(Landroid/os/IBinder;)LX/1qB;

    move-result-object v1

    .line 313689
    invoke-virtual {p0, v0, v1}, LX/1mK;->registerCompletionHandler(Ljava/lang/String;LX/1qB;)Z

    move-result v0

    .line 313690
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313691
    if-eqz v0, :cond_d

    move v4, v7

    :cond_d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
