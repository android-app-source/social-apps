.class public LX/11w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/11w;


# instance fields
.field private final b:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33h;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field public volatile k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173581
    const-class v0, LX/11w;

    sput-object v0, LX/11w;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33h;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173571
    iput-object p1, p0, LX/11w;->b:LX/0Xl;

    .line 173572
    iput-object p2, p0, LX/11w;->c:LX/0Ot;

    .line 173573
    iput-object p3, p0, LX/11w;->d:LX/0Ot;

    .line 173574
    iput-object p4, p0, LX/11w;->e:LX/0Ot;

    .line 173575
    iput-object p5, p0, LX/11w;->f:LX/0Ot;

    .line 173576
    iput-object p6, p0, LX/11w;->g:LX/0Or;

    .line 173577
    iput-object p7, p0, LX/11w;->h:LX/0Or;

    .line 173578
    iput-object p8, p0, LX/11w;->i:LX/0Ot;

    .line 173579
    iput-object p9, p0, LX/11w;->j:LX/0Ot;

    .line 173580
    return-void
.end method

.method public static a(LX/0QB;)LX/11w;
    .locals 13

    .prologue
    .line 173557
    sget-object v0, LX/11w;->l:LX/11w;

    if-nez v0, :cond_1

    .line 173558
    const-class v1, LX/11w;

    monitor-enter v1

    .line 173559
    :try_start_0
    sget-object v0, LX/11w;->l:LX/11w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 173560
    if-eqz v2, :cond_0

    .line 173561
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 173562
    new-instance v3, LX/11w;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0xf9a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x29d

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1405

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x13eb

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x13c1

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x14d1

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x13f1

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x13ec

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/11w;-><init>(LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 173563
    move-object v0, v3

    .line 173564
    sput-object v0, LX/11w;->l:LX/11w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173565
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 173566
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 173567
    :cond_1
    sget-object v0, LX/11w;->l:LX/11w;

    return-object v0

    .line 173568
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 173569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/11w;Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V
    .locals 3
    .param p0    # LX/11w;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173582
    iput-object p1, p0, LX/11w;->k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    .line 173583
    iget-object v0, p0, LX/11w;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    invoke-static {v0}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 173584
    iget-object v0, p0, LX/11w;->b:LX/0Xl;

    const-string v1, "com.facebook.zero.ZERO_RATING_INDICATOR_DATA_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 173585
    return-void

    .line 173586
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 173550
    iget-object v0, p0, LX/11w;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173551
    invoke-static {p0, v2}, LX/11w;->b(LX/11w;Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V

    .line 173552
    :goto_0
    return-void

    .line 173553
    :cond_0
    iget-object v0, p0, LX/11w;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yU;

    iget-object v1, p0, LX/11w;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yh;

    invoke-virtual {v1}, LX/0yh;->getBaseToken()LX/0yi;

    move-result-object v1

    invoke-virtual {v1}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 173554
    if-eqz v0, :cond_1

    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 173555
    :cond_1
    invoke-static {p0, v2}, LX/11w;->b(LX/11w;Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V

    goto :goto_0

    .line 173556
    :cond_2
    iget-object v0, p0, LX/11w;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wd;

    new-instance v1, Lcom/facebook/zero/service/FbZeroIndicatorManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/zero/service/FbZeroIndicatorManager$1;-><init>(LX/11w;)V

    const v2, -0x276739c5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173538
    sget-object v0, LX/0df;->J:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173539
    iget-object v0, p0, LX/11w;->k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    if-nez v0, :cond_1

    .line 173540
    monitor-enter p0

    .line 173541
    :try_start_0
    iget-object v0, p0, LX/11w;->k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 173542
    :try_start_1
    iget-object v0, p0, LX/11w;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->J:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173543
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173544
    iget-object v0, p0, LX/11w;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33h;

    invoke-virtual {v0, v1}, LX/33h;->a(Ljava/lang/String;)Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    move-result-object v0

    iput-object v0, p0, LX/11w;->k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173545
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 173546
    :cond_1
    iget-object v0, p0, LX/11w;->k:Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    return-object v0

    .line 173547
    :catch_0
    move-exception v0

    .line 173548
    :try_start_3
    sget-object v1, LX/11w;->a:Ljava/lang/Class;

    const-string v2, "Error deserializing indicator data %s: "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 173549
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
