.class public LX/0tt;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0uf;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0uf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155777
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0uf;
    .locals 6

    .prologue
    .line 155778
    sget-object v0, LX/0tt;->a:LX/0uf;

    if-nez v0, :cond_1

    .line 155779
    const-class v1, LX/0tt;

    monitor-enter v1

    .line 155780
    :try_start_0
    sget-object v0, LX/0tt;->a:LX/0uf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155781
    if-eqz v2, :cond_0

    .line 155782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155783
    invoke-static {v0}, LX/0tu;->a(LX/0QB;)LX/0tw;

    move-result-object v3

    check-cast v3, LX/0tw;

    invoke-static {v0}, LX/0tx;->a(LX/0QB;)LX/0ud;

    move-result-object v4

    check-cast v4, LX/0ud;

    invoke-static {v0}, LX/0ua;->a(LX/0QB;)LX/0uc;

    move-result-object v5

    check-cast v5, LX/0uc;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    invoke-static {v3, v4, v5, p0}, LX/0tv;->a(LX/0tw;LX/0ud;LX/0uc;LX/0So;)LX/0uf;

    move-result-object v3

    move-object v0, v3

    .line 155784
    sput-object v0, LX/0tt;->a:LX/0uf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155785
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155786
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155787
    :cond_1
    sget-object v0, LX/0tt;->a:LX/0uf;

    return-object v0

    .line 155788
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 155790
    invoke-static {p0}, LX/0tu;->a(LX/0QB;)LX/0tw;

    move-result-object v0

    check-cast v0, LX/0tw;

    invoke-static {p0}, LX/0tx;->a(LX/0QB;)LX/0ud;

    move-result-object v1

    check-cast v1, LX/0ud;

    invoke-static {p0}, LX/0ua;->a(LX/0QB;)LX/0uc;

    move-result-object v2

    check-cast v2, LX/0uc;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0, v1, v2, v3}, LX/0tv;->a(LX/0tw;LX/0ud;LX/0uc;LX/0So;)LX/0uf;

    move-result-object v0

    return-object v0
.end method
