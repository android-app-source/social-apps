.class public LX/0Zh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/0n9;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/0zL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field public final d:I


# direct methods
.method private constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 83784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83785
    new-instance v0, LX/0Zi;

    invoke-direct {v0, p1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/0Zh;->a:LX/0Zi;

    .line 83786
    new-instance v0, LX/0Zi;

    invoke-direct {v0, p2}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/0Zh;->b:LX/0Zi;

    .line 83787
    iput p3, p0, LX/0Zh;->c:I

    .line 83788
    iput p4, p0, LX/0Zh;->d:I

    .line 83789
    return-void
.end method

.method public static a()LX/0Zh;
    .locals 5

    .prologue
    .line 83773
    new-instance v0, LX/0Zh;

    const/16 v1, 0x8

    const/4 v2, 0x2

    const/16 v3, 0x10

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, LX/0Zh;-><init>(IIII)V

    return-object v0
.end method


# virtual methods
.method public final b()LX/0n9;
    .locals 2

    .prologue
    .line 83779
    iget-object v0, p0, LX/0Zh;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0n9;

    .line 83780
    if-nez v0, :cond_0

    .line 83781
    new-instance v0, LX/0n9;

    iget v1, p0, LX/0Zh;->c:I

    invoke-direct {v0, v1}, LX/0n9;-><init>(I)V

    .line 83782
    :cond_0
    invoke-virtual {v0, p0}, LX/0nA;->a(LX/0Zh;)V

    .line 83783
    return-object v0
.end method

.method public final c()LX/0zL;
    .locals 2

    .prologue
    .line 83774
    iget-object v0, p0, LX/0Zh;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zL;

    .line 83775
    if-nez v0, :cond_0

    .line 83776
    new-instance v0, LX/0zL;

    iget v1, p0, LX/0Zh;->c:I

    invoke-direct {v0, v1}, LX/0zL;-><init>(I)V

    .line 83777
    :cond_0
    invoke-virtual {v0, p0}, LX/0nA;->a(LX/0Zh;)V

    .line 83778
    return-object v0
.end method
