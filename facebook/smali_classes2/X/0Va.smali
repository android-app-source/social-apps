.class public LX/0Va;
.super Ljava/util/concurrent/FutureTask;
.source ""

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Sx;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 67927
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 67928
    new-instance v0, LX/0Sx;

    invoke-direct {v0}, LX/0Sx;-><init>()V

    iput-object v0, p0, LX/0Va;->a:LX/0Sx;

    .line 67929
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 67930
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 67931
    new-instance v0, LX/0Sx;

    invoke-direct {v0}, LX/0Sx;-><init>()V

    iput-object v0, p0, LX/0Va;->a:LX/0Sx;

    .line 67932
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TV;)",
            "LX/0Va",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 67933
    new-instance v0, LX/0Va;

    invoke-direct {v0, p0, p1}, LX/0Va;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)LX/0Va;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "LX/0Va",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 67934
    new-instance v0, LX/0Va;

    invoke-direct {v0, p0}, LX/0Va;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 67935
    iget-object v0, p0, LX/0Va;->a:LX/0Sx;

    invoke-virtual {v0, p1, p2}, LX/0Sx;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 67936
    return-void
.end method

.method public final done()V
    .locals 1

    .prologue
    .line 67937
    iget-object v0, p0, LX/0Va;->a:LX/0Sx;

    invoke-virtual {v0}, LX/0Sx;->a()V

    .line 67938
    return-void
.end method
