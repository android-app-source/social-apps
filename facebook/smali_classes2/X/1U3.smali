.class public LX/1U3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/pymgf/PymgfMultiRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255185
    iput-object p1, p0, LX/1U3;->a:LX/0Ot;

    .line 255186
    iput-object p2, p0, LX/1U3;->b:LX/0Ot;

    .line 255187
    iput-object p3, p0, LX/1U3;->c:LX/0Ot;

    .line 255188
    return-void
.end method

.method public static a(LX/0QB;)LX/1U3;
    .locals 6

    .prologue
    .line 255189
    const-class v1, LX/1U3;

    monitor-enter v1

    .line 255190
    :try_start_0
    sget-object v0, LX/1U3;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 255191
    sput-object v2, LX/1U3;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 255192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 255194
    new-instance v3, LX/1U3;

    const/16 v4, 0xad6

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x232b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x2326

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/1U3;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 255195
    move-object v0, v3

    .line 255196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 255197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1U3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 255199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255180
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255181
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255182
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255183
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255176
    const-class v0, Lcom/facebook/graphql/model/GraphQLNoContentGoodFriendsFeedUnit;

    iget-object v1, p0, LX/1U3;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255177
    const-class v0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;

    iget-object v1, p0, LX/1U3;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255178
    const-class v0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;

    iget-object v1, p0, LX/1U3;->c:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255179
    return-void
.end method
