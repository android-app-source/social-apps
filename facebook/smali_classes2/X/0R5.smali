.class public abstract LX/0R5;
.super LX/0Q9;
.source ""


# instance fields
.field private final a:LX/0QA;


# direct methods
.method public constructor <init>(LX/0QA;)V
    .locals 0

    .prologue
    .line 59357
    invoke-direct {p0}, LX/0Q9;-><init>()V

    .line 59358
    iput-object p1, p0, LX/0R5;->a:LX/0QA;

    .line 59359
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Class;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation
.end method

.method public final a(LX/0RI;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 59356
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->a(LX/0RI;)Z

    move-result v0

    return v0
.end method

.method public getApplicationInjector()LX/0QA;
    .locals 1

    .prologue
    .line 59355
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getApplicationInjector()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public final getBinders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59349
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getBinders()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getInjectorThreadStack()LX/0S7;
    .locals 1

    .prologue
    .line 59354
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59353
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    return-object v0
.end method

.method public final getProcessIdentifier()I
    .locals 1

    .prologue
    .line 59352
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getProcessIdentifier()I

    move-result v0

    return v0
.end method

.method public getScopeAwareInjector()LX/0R6;
    .locals 1

    .prologue
    .line 59351
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeUnawareInjector()LX/0QD;
    .locals 1

    .prologue
    .line 59350
    iget-object v0, p0, LX/0R5;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    return-object v0
.end method
