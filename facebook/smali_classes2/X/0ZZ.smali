.class public LX/0ZZ;
.super LX/0Za;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final i:LX/0Zh;

.field private static volatile k:LX/0ZZ;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mM;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0aD;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83662
    const-class v0, LX/0ZZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ZZ;->a:Ljava/lang/String;

    .line 83663
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    sput-object v0, LX/0ZZ;->i:LX/0Zh;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;Ljava/util/Random;LX/0Or;LX/0Zm;LX/0Ot;LX/0Or;LX/0aD;LX/0Ot;Lcom/facebook/analytics/AnalyticsStats;)V
    .locals 0
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/annotations/IsDuplicateEventLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/0Ot",
            "<",
            "LX/0mM;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0aD;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/analytics/AnalyticsStats;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83653
    invoke-direct {p0, p3, p5, p10}, LX/0Za;-><init>(Ljava/util/Random;LX/0Zm;Lcom/facebook/analytics/AnalyticsStats;)V

    .line 83654
    iput-object p1, p0, LX/0ZZ;->b:LX/0Ot;

    .line 83655
    iput-object p2, p0, LX/0ZZ;->c:LX/0Ot;

    .line 83656
    iput-object p4, p0, LX/0ZZ;->d:LX/0Or;

    .line 83657
    iput-object p6, p0, LX/0ZZ;->e:LX/0Ot;

    .line 83658
    iput-object p7, p0, LX/0ZZ;->f:LX/0Or;

    .line 83659
    iput-object p8, p0, LX/0ZZ;->g:LX/0aD;

    .line 83660
    iput-object p9, p0, LX/0ZZ;->h:LX/0Ot;

    .line 83661
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZZ;
    .locals 14

    .prologue
    .line 83640
    sget-object v0, LX/0ZZ;->k:LX/0ZZ;

    if-nez v0, :cond_1

    .line 83641
    const-class v1, LX/0ZZ;

    monitor-enter v1

    .line 83642
    :try_start_0
    sget-object v0, LX/0ZZ;->k:LX/0ZZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83643
    if-eqz v2, :cond_0

    .line 83644
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83645
    new-instance v3, LX/0ZZ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xd4

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v8

    check-cast v8, LX/0Zm;

    const/16 v9, 0x81

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x143c

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/0aD;->a(LX/0QB;)LX/0aD;

    move-result-object v11

    check-cast v11, LX/0aD;

    const/16 v12, 0x2e3

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, Lcom/facebook/analytics/AnalyticsStats;->a(LX/0QB;)Lcom/facebook/analytics/AnalyticsStats;

    move-result-object v13

    check-cast v13, Lcom/facebook/analytics/AnalyticsStats;

    invoke-direct/range {v3 .. v13}, LX/0ZZ;-><init>(LX/0Ot;LX/0Ot;Ljava/util/Random;LX/0Or;LX/0Zm;LX/0Ot;LX/0Or;LX/0aD;LX/0Ot;Lcom/facebook/analytics/AnalyticsStats;)V

    .line 83646
    move-object v0, v3

    .line 83647
    sput-object v0, LX/0ZZ;->k:LX/0ZZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83648
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83649
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83650
    :cond_1
    sget-object v0, LX/0ZZ;->k:LX/0ZZ;

    return-object v0

    .line 83651
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83652
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)LX/0mj;
    .locals 4

    .prologue
    .line 83637
    iget-object v0, p0, LX/0ZZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mh;

    invoke-static {p1}, LX/0ZZ;->g(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Ljava/lang/String;

    move-result-object v1

    .line 83638
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 83639
    invoke-static {p1}, LX/0ZZ;->h(Lcom/facebook/analytics/HoneyAnalyticsEvent;)LX/0mq;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/analytics/HoneyAnalyticsEvent;ZZ)LX/0mj;
    .locals 6

    .prologue
    .line 83630
    instance-of v0, p1, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 83631
    check-cast v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83632
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to log a non sampled event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83633
    :cond_0
    invoke-direct {p0, p1, p3}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)LX/0mj;

    move-result-object v0

    .line 83634
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/0ZZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mh;

    invoke-static {p1}, LX/0ZZ;->g(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Ljava/lang/String;

    move-result-object v1

    .line 83635
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 83636
    invoke-static {p1}, LX/0ZZ;->h(Lcom/facebook/analytics/HoneyAnalyticsEvent;)LX/0mq;

    move-result-object v4

    move v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/0mj;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V
    .locals 2

    .prologue
    .line 83611
    invoke-virtual {p2}, LX/0mj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83612
    invoke-direct {p0, p1, p2}, LX/0ZZ;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    .line 83613
    iget-object v0, p0, LX/0ZZ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83614
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 83615
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 83616
    invoke-static {v0}, LX/0ZZ;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 83617
    instance-of v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 83618
    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    move-result-object v0

    .line 83619
    const-string p2, "reversed"

    invoke-static {v0}, LX/0ZZ;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p1

    .line 83620
    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 83621
    iget-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    move-object v0, p2

    .line 83622
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    .line 83623
    check-cast p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 83624
    iget-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    move-object v0, v0

    .line 83625
    invoke-static {v0}, LX/0ZZ;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83626
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 83627
    :cond_0
    move-object v0, v1

    .line 83628
    invoke-virtual {p0, v0}, LX/0ZZ;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 83629
    :cond_1
    return-void
.end method

.method private b(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V
    .locals 8

    .prologue
    .line 83545
    const-string v2, "buildAndDispatch"

    const v3, 0x34943a3c

    invoke-static {v2, v3}, LX/03q;->a(Ljava/lang/String;I)V

    .line 83546
    :try_start_0
    invoke-direct {p0, p1}, LX/0ZZ;->f(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 83547
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v0

    .line 83548
    iget-object v0, p0, LX/0Za;->c:Lcom/facebook/analytics/AnalyticsStats;

    invoke-virtual {v0, v2}, Lcom/facebook/analytics/AnalyticsStats;->a(Ljava/lang/String;)V

    .line 83549
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    move-object v2, v0

    .line 83550
    const-string v3, "AUTO_SET"

    if-eq v2, v3, :cond_0

    .line 83551
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    move-object v2, v0

    .line 83552
    invoke-virtual {p2, v2}, LX/0mj;->a(Ljava/lang/String;)LX/0mj;

    .line 83553
    :cond_0
    iget-wide v6, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v2, v6

    .line 83554
    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 83555
    iget-wide v6, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v2, v6

    .line 83556
    invoke-virtual {p2, v2, v3}, LX/0mj;->a(J)LX/0mj;

    .line 83557
    :cond_1
    instance-of v2, p1, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v2, :cond_6

    .line 83558
    move-object v0, p1

    check-cast v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    move-object v2, v0

    .line 83559
    invoke-static {v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 83560
    iget-object v0, v2, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->e:Ljava/lang/String;

    move-object v0, v0

    .line 83561
    invoke-static {v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 83562
    iget-object v1, v2, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->f:Ljava/lang/String;

    move-object v1, v1

    .line 83563
    invoke-virtual {p2, v0, v1}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 83564
    invoke-static {v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 83565
    iget-object v0, v2, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->g:Ljava/lang/String;

    move-object v0, v0

    .line 83566
    invoke-virtual {p2, v0}, LX/0mj;->b(Ljava/lang/String;)LX/0mj;

    .line 83567
    invoke-static {v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 83568
    iget-object v0, v2, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->h:Ljava/lang/String;

    move-object v0, v0

    .line 83569
    invoke-static {v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 83570
    iget-object v1, v2, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->i:Ljava/lang/String;

    move-object v1, v1

    .line 83571
    invoke-virtual {p2, v0, v1}, LX/0mj;->b(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 83572
    :cond_2
    :goto_0
    invoke-static {p1, p2}, LX/0ZZ;->f(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    .line 83573
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    move-object v1, v0

    .line 83574
    if-eqz v1, :cond_3

    .line 83575
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v2

    .line 83576
    invoke-virtual {p2}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    const-string v3, "enabled_features"

    invoke-virtual {v0, v3}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v3

    .line 83577
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    .line 83578
    invoke-virtual {v1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    .line 83579
    invoke-static {v3, v4}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 83580
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 83581
    :cond_3
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v0

    .line 83582
    invoke-static {v2}, LX/0mr;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 83583
    const-string v2, "CTScanV2Event"

    invoke-direct {p0, p1, p2}, LX/0ZZ;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83584
    :cond_4
    iget-object v2, p0, LX/0ZZ;->g:LX/0aD;

    .line 83585
    iget-boolean v0, v2, LX/0aD;->a:Z

    move v2, v0

    .line 83586
    if-eqz v2, :cond_5

    .line 83587
    invoke-direct {p0, p1, p2}, LX/0ZZ;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)Ljava/lang/String;

    .line 83588
    :cond_5
    invoke-virtual {p2}, LX/0mj;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83589
    const v2, -0x711a4bc4

    invoke-static {v2}, LX/03q;->a(I)V

    .line 83590
    return-void

    .line 83591
    :cond_6
    :try_start_1
    instance-of v2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v2, :cond_7

    .line 83592
    move-object v0, p1

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, v0

    .line 83593
    iget-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 83594
    iget-object v1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    move-object v1, v1

    .line 83595
    invoke-virtual {p2, v0, v1}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 83596
    iget-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    move-object v0, v0

    .line 83597
    invoke-virtual {p2, v0}, LX/0mj;->b(Ljava/lang/String;)LX/0mj;

    .line 83598
    iget-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    move-object v0, v0

    .line 83599
    iget-object v1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->h:Ljava/lang/String;

    move-object v1, v1

    .line 83600
    invoke-virtual {p2, v0, v1}, LX/0mj;->b(Ljava/lang/String;Ljava/lang/String;)LX/0mj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83601
    goto :goto_0

    .line 83602
    :catchall_0
    move-exception v2

    const v3, 0x7595f22e

    invoke-static {v3}, LX/03q;->a(I)V

    throw v2

    .line 83603
    :cond_7
    :try_start_2
    instance-of v2, p1, Lcom/facebook/analytics/HoneyExperimentEvent;

    if-eqz v2, :cond_2

    .line 83604
    move-object v0, p1

    check-cast v0, Lcom/facebook/analytics/HoneyExperimentEvent;

    move-object v2, v0

    .line 83605
    iget-object v0, v2, Lcom/facebook/analytics/HoneyExperimentEvent;->c:Ljava/lang/String;

    move-object v0, v0

    .line 83606
    if-eqz v0, :cond_8

    .line 83607
    const-string v1, "exprID"

    .line 83608
    iget-object v2, p2, LX/0mj;->r:LX/0n9;

    .line 83609
    invoke-static {v2, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 83610
    :cond_8
    goto/16 :goto_0
.end method

.method private c(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 83523
    sget-object v0, LX/0ZZ;->i:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 83524
    :try_start_0
    const-string v0, "name"

    .line 83525
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 83526
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83527
    const-string v2, "time"

    iget-object v0, p0, LX/0ZZ;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 83528
    invoke-static {v1, v2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83529
    iget-object v0, p2, LX/0mj;->c:Ljava/lang/String;

    move-object v0, v0

    .line 83530
    if-eqz v0, :cond_0

    .line 83531
    const-string v0, "module"

    .line 83532
    iget-object v2, p2, LX/0mj;->c:Ljava/lang/String;

    move-object v2, v2

    .line 83533
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83534
    :cond_0
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 83535
    const-string v2, "{"

    invoke-virtual {v0, v2}, Ljava/io/StringWriter;->append(Ljava/lang/CharSequence;)Ljava/io/StringWriter;

    .line 83536
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V

    .line 83537
    const-string v2, ",\"extra\":{"

    invoke-virtual {v0, v2}, Ljava/io/StringWriter;->append(Ljava/lang/CharSequence;)Ljava/io/StringWriter;

    .line 83538
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v2

    invoke-virtual {p2}, LX/0mj;->c()LX/0n9;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V

    .line 83539
    const-string v2, "} }"

    invoke-virtual {v0, v2}, Ljava/io/StringWriter;->append(Ljava/lang/CharSequence;)Ljava/io/StringWriter;

    .line 83540
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 83541
    invoke-virtual {v1}, LX/0nA;->a()V

    return-object v0

    .line 83542
    :catch_0
    move-exception v0

    .line 83543
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83544
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 83520
    if-nez p0, :cond_0

    .line 83521
    const/4 v0, 0x0

    .line 83522
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Z
    .locals 1

    .prologue
    .line 83519
    const-string v0, "upload_this_event_now"

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized f(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 3
    .param p1    # Lcom/facebook/analytics/HoneyAnalyticsEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83506
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/0ZZ;->j:Ljava/lang/String;

    .line 83507
    if-eqz p1, :cond_1

    .line 83508
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    move-object v0, v0

    .line 83509
    const-string v2, "AUTO_SET"

    if-eq v0, v2, :cond_1

    .line 83510
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    move-object v0, v0

    .line 83511
    :goto_0
    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83512
    iput-object v0, p0, LX/0ZZ;->j:Ljava/lang/String;

    .line 83513
    iget-object v0, p0, LX/0ZZ;->j:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 83514
    iget-object v0, p0, LX/0ZZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mM;

    invoke-virtual {v0}, LX/0mM;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83515
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 83516
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0ZZ;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 83517
    :cond_2
    iget-object v0, p0, LX/0ZZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mM;

    iget-object v1, p0, LX/0ZZ;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0mM;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 83518
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static f(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V
    .locals 1

    .prologue
    .line 83664
    instance-of v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v0, :cond_1

    .line 83665
    check-cast p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(LX/0n9;)V

    .line 83666
    :cond_0
    :goto_0
    return-void

    .line 83667
    :cond_1
    instance-of v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;

    if-eqz v0, :cond_2

    .line 83668
    check-cast p0, Lcom/facebook/analytics/HoneyExperimentEvent;

    invoke-virtual {p1}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/HoneyExperimentEvent;->a(LX/0n9;)V

    goto :goto_0

    .line 83669
    :cond_2
    instance-of v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v0, :cond_0

    .line 83670
    check-cast p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->p()LX/0n9;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0mj;->a(LX/0n9;)V

    goto :goto_0
.end method

.method private static g(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 83458
    instance-of v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v0, :cond_0

    .line 83459
    check-cast p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 83460
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    move-object v0, v0

    .line 83461
    :goto_0
    return-object v0

    .line 83462
    :cond_0
    instance-of v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v0, :cond_1

    .line 83463
    check-cast p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->q()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83464
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h(Lcom/facebook/analytics/HoneyAnalyticsEvent;)LX/0mq;
    .locals 3

    .prologue
    .line 83467
    instance-of v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v0, :cond_1

    .line 83468
    :cond_0
    sget-object v0, LX/0mq;->CLIENT_EVENT:LX/0mq;

    .line 83469
    :goto_0
    return-object v0

    .line 83470
    :cond_1
    instance-of v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;

    if-eqz v0, :cond_2

    .line 83471
    sget-object v0, LX/0mq;->EXPERIMENT:LX/0mq;

    goto :goto_0

    .line 83472
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 2

    .prologue
    .line 83473
    if-nez p1, :cond_0

    .line 83474
    :goto_0
    return-void

    .line 83475
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1}, LX/0ZZ;->e(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Z

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;ZZ)LX/0mj;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V
    .locals 1

    .prologue
    .line 83476
    if-nez p1, :cond_1

    .line 83477
    :cond_0
    :goto_0
    return-void

    .line 83478
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/0Za;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83479
    invoke-virtual {p0, p1}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 83480
    if-nez p1, :cond_0

    .line 83481
    :goto_0
    return-void

    .line 83482
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v0

    .line 83483
    if-nez v0, :cond_1

    .line 83484
    iget-boolean v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->k:Z

    move v0, v0

    .line 83485
    if-eqz v0, :cond_2

    .line 83486
    :cond_1
    invoke-virtual {p0, p1}, LX/0ZZ;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 83487
    :cond_2
    invoke-virtual {p0, p1}, LX/0ZZ;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83465
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0ZZ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 83466
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83488
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 83489
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83490
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 83491
    :cond_0
    invoke-virtual {p0, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 83492
    return-void
.end method

.method public final b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 1

    .prologue
    .line 83493
    if-nez p1, :cond_0

    .line 83494
    :goto_0
    return-void

    .line 83495
    :cond_0
    invoke-static {p1}, LX/0ZZ;->e(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)LX/0mj;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0ZZ;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V
    .locals 1

    .prologue
    .line 83496
    if-nez p1, :cond_1

    .line 83497
    :cond_0
    :goto_0
    return-void

    .line 83498
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/0Za;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83499
    invoke-virtual {p0, p1}, LX/0ZZ;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 2

    .prologue
    .line 83500
    if-nez p1, :cond_0

    .line 83501
    :goto_0
    return-void

    .line 83502
    :cond_0
    const/4 v0, 0x1

    invoke-static {p1}, LX/0ZZ;->e(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Z

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;ZZ)LX/0mj;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    goto :goto_0
.end method

.method public final d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 83503
    if-nez p1, :cond_0

    .line 83504
    :goto_0
    return-void

    .line 83505
    :cond_0
    invoke-direct {p0, p1, v0, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;ZZ)LX/0mj;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0ZZ;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;LX/0mj;)V

    goto :goto_0
.end method
