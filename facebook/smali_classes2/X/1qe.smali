.class public LX/1qe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1qe;


# instance fields
.field public final a:LX/1fM;

.field public final b:LX/1fJ;

.field private final c:LX/1uY;


# direct methods
.method public constructor <init>(LX/1fM;LX/1uY;LX/1fJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331257
    iput-object p1, p0, LX/1qe;->a:LX/1fM;

    .line 331258
    iput-object p2, p0, LX/1qe;->c:LX/1uY;

    .line 331259
    iput-object p3, p0, LX/1qe;->b:LX/1fJ;

    .line 331260
    return-void
.end method

.method public static a(LX/0QB;)LX/1qe;
    .locals 6

    .prologue
    .line 331261
    sget-object v0, LX/1qe;->d:LX/1qe;

    if-nez v0, :cond_1

    .line 331262
    const-class v1, LX/1qe;

    monitor-enter v1

    .line 331263
    :try_start_0
    sget-object v0, LX/1qe;->d:LX/1qe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331264
    if-eqz v2, :cond_0

    .line 331265
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331266
    new-instance p0, LX/1qe;

    invoke-static {v0}, LX/1fM;->a(LX/0QB;)LX/1fM;

    move-result-object v3

    check-cast v3, LX/1fM;

    invoke-static {v0}, LX/1uY;->a(LX/0QB;)LX/1uY;

    move-result-object v4

    check-cast v4, LX/1uY;

    invoke-static {v0}, LX/1fJ;->a(LX/0QB;)LX/1fJ;

    move-result-object v5

    check-cast v5, LX/1fJ;

    invoke-direct {p0, v3, v4, v5}, LX/1qe;-><init>(LX/1fM;LX/1uY;LX/1fJ;)V

    .line 331267
    move-object v0, p0

    .line 331268
    sput-object v0, LX/1qe;->d:LX/1qe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331269
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331270
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331271
    :cond_1
    sget-object v0, LX/1qe;->d:LX/1qe;

    return-object v0

    .line 331272
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/api/feed/FetchFeedResult;)Z
    .locals 2

    .prologue
    .line 331274
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 331275
    if-eqz v0, :cond_0

    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    .line 331276
    iget-object p0, v0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v0, p0

    .line 331277
    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331278
    new-instance v0, LX/1t9;

    invoke-direct {v0, p1}, LX/1t9;-><init>(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 331279
    invoke-static {p1}, LX/1qe;->b(Lcom/facebook/api/feed/FetchFeedResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331280
    iget-object v1, p0, LX/1qe;->a:LX/1fM;

    invoke-virtual {v1, v0}, LX/1fM;->a(LX/1t9;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 331281
    :goto_0
    iget-object v1, p0, LX/1qe;->c:LX/1uY;

    invoke-virtual {v1, p1}, LX/1uY;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 331282
    return-object v0

    .line 331283
    :cond_0
    new-instance v1, LX/1tO;

    iget-object v2, p0, LX/1qe;->a:LX/1fM;

    invoke-direct {v1, v2, v0}, LX/1tO;-><init>(LX/1fM;LX/1fd;)V

    .line 331284
    iget-object v2, p0, LX/1qe;->b:LX/1fJ;

    invoke-virtual {v2, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 331285
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 331286
    goto :goto_0
.end method
