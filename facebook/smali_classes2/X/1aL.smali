.class public LX/1aL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 276732
    const-class v0, LX/1aL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1aL;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 276719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276720
    return-void
.end method

.method public static a(Landroid/view/View;)LX/1Ra;
    .locals 3
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(TV;)",
            "LX/1Ra",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 276721
    if-nez p0, :cond_1

    move-object v0, v1

    .line 276722
    :cond_0
    :goto_0
    return-object v0

    .line 276723
    :cond_1
    const v0, 0x7f0d00f2

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ra;

    .line 276724
    if-eqz v0, :cond_0

    .line 276725
    iput-object v1, v0, LX/1Ra;->a:Landroid/view/View;

    .line 276726
    const v2, 0x7f0d00f2

    invoke-virtual {p0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/1Ra;)Landroid/view/View;
    .locals 1
    .param p0    # LX/1Ra;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "LX/1Ra",
            "<TV;>;)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 276727
    if-nez p0, :cond_0

    .line 276728
    const/4 v0, 0x0

    .line 276729
    :goto_0
    return-object v0

    .line 276730
    :cond_0
    iget-object v0, p0, LX/1Ra;->a:Landroid/view/View;

    move-object v0, v0

    .line 276731
    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/1Ra;LX/03V;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(TV;",
            "LX/1Ra",
            "<TV;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 276710
    const v0, 0x7f0d00f2

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 276711
    if-eqz v0, :cond_0

    .line 276712
    sget-object v0, LX/1aL;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Recycle callback has not happened for View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276713
    :cond_0
    const v0, 0x7f0d00f2

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 276714
    iput-object p0, p1, LX/1Ra;->a:Landroid/view/View;

    .line 276715
    return-void
.end method

.method public static b(Landroid/view/View;)LX/1Ra;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(TV;)",
            "LX/1Ra",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 276716
    if-nez p0, :cond_0

    .line 276717
    const/4 v0, 0x0

    .line 276718
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d00f2

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ra;

    goto :goto_0
.end method
