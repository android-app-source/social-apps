.class public LX/1rp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/1rp;


# instance fields
.field private final a:LX/1rq;

.field private final b:LX/1rr;

.field private final c:Ljava/util/concurrent/Executor;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/0xB;

.field private final f:LX/1s3;

.field private final g:LX/0ad;

.field private final h:LX/1rU;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1vq",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/1vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vq",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1rq;LX/1rr;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0xB;LX/1s3;LX/0ad;LX/1rU;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1rp;->i:Ljava/util/List;

    .line 333270
    iput-object p1, p0, LX/1rp;->a:LX/1rq;

    .line 333271
    iput-object p2, p0, LX/1rp;->b:LX/1rr;

    .line 333272
    iput-object p3, p0, LX/1rp;->c:Ljava/util/concurrent/Executor;

    .line 333273
    iput-object p4, p0, LX/1rp;->d:Ljava/util/concurrent/Executor;

    .line 333274
    iput-object p5, p0, LX/1rp;->e:LX/0xB;

    .line 333275
    iput-object p6, p0, LX/1rp;->f:LX/1s3;

    .line 333276
    iput-object p7, p0, LX/1rp;->g:LX/0ad;

    .line 333277
    iput-object p8, p0, LX/1rp;->h:LX/1rU;

    .line 333278
    return-void
.end method

.method public static a(LX/0QB;)LX/1rp;
    .locals 12

    .prologue
    .line 333279
    sget-object v0, LX/1rp;->l:LX/1rp;

    if-nez v0, :cond_1

    .line 333280
    const-class v1, LX/1rp;

    monitor-enter v1

    .line 333281
    :try_start_0
    sget-object v0, LX/1rp;->l:LX/1rp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 333282
    if-eqz v2, :cond_0

    .line 333283
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 333284
    new-instance v3, LX/1rp;

    const-class v4, LX/1rq;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1rq;

    invoke-static {v0}, LX/1rr;->b(LX/0QB;)LX/1rr;

    move-result-object v5

    check-cast v5, LX/1rr;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v8

    check-cast v8, LX/0xB;

    .line 333285
    new-instance v10, LX/1s3;

    invoke-static {v0}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v9

    check-cast v9, LX/1dy;

    invoke-direct {v10, v9}, LX/1s3;-><init>(LX/1dy;)V

    .line 333286
    move-object v9, v10

    .line 333287
    check-cast v9, LX/1s3;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v11

    check-cast v11, LX/1rU;

    invoke-direct/range {v3 .. v11}, LX/1rp;-><init>(LX/1rq;LX/1rr;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0xB;LX/1s3;LX/0ad;LX/1rU;)V

    .line 333288
    move-object v0, v3

    .line 333289
    sput-object v0, LX/1rp;->l:LX/1rp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333290
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 333291
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 333292
    :cond_1
    sget-object v0, LX/1rp;->l:LX/1rp;

    return-object v0

    .line 333293
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 333294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/2kW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rp;->j:LX/2kW;

    if-eqz v0, :cond_0

    .line 333296
    iget-object v0, p0, LX/1rp;->j:LX/2kW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333297
    :goto_0
    monitor-exit p0

    return-object v0

    .line 333298
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "notifications_session:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/1rp;->h:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "newapi"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333299
    iget-object v1, p0, LX/1rp;->a:LX/1rq;

    iget-object v2, p0, LX/1rp;->b:LX/1rr;

    invoke-virtual {v1, v0, v2}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    sget-object v1, LX/2jq;->CHECK_SERVER_FOR_NEWDATA:LX/2jq;

    .line 333300
    iput-object v1, v0, LX/2jj;->i:LX/2jq;

    .line 333301
    move-object v0, v0

    .line 333302
    new-instance v1, LX/2jt;

    invoke-direct {v1}, LX/2jt;-><init>()V

    .line 333303
    iput-object v1, v0, LX/2jj;->l:LX/2js;

    .line 333304
    move-object v0, v0

    .line 333305
    new-instance v1, LX/2ju;

    invoke-direct {v1}, LX/2ju;-><init>()V

    .line 333306
    iput-object v1, v0, LX/2jj;->m:LX/0QK;

    .line 333307
    move-object v0, v0

    .line 333308
    iget-object v1, p0, LX/1rp;->f:LX/1s3;

    .line 333309
    iput-object v1, v0, LX/2jj;->n:LX/1s3;

    .line 333310
    move-object v0, v0

    .line 333311
    const/16 v1, 0xa

    .line 333312
    iput v1, v0, LX/2jj;->o:I

    .line 333313
    move-object v0, v0

    .line 333314
    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, LX/1rp;->j:LX/2kW;

    .line 333315
    new-instance v0, LX/2kc;

    invoke-direct {v0, p0}, LX/2kc;-><init>(LX/1rp;)V

    iput-object v0, p0, LX/1rp;->k:LX/1vq;

    .line 333316
    iget-object v0, p0, LX/1rp;->j:LX/2kW;

    iget-object v1, p0, LX/1rp;->k:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 333317
    iget-object v0, p0, LX/1rp;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 333318
    iget-object v2, p0, LX/1rp;->j:LX/2kW;

    invoke-virtual {v2, v0}, LX/2kW;->a(LX/1vq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 333319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 333320
    :cond_1
    :try_start_2
    const-string v0, "oldapi"

    goto :goto_1

    .line 333321
    :cond_2
    iget-object v0, p0, LX/1rp;->j:LX/2kW;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(LX/1vq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vq",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333322
    iget-object v0, p0, LX/1rp;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333323
    iget-object v0, p0, LX/1rp;->j:LX/2kW;

    if-eqz v0, :cond_0

    .line 333324
    iget-object v0, p0, LX/1rp;->j:LX/2kW;

    invoke-virtual {v0, p1}, LX/2kW;->a(LX/1vq;)V

    .line 333325
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 333326
    iget-object v0, p0, LX/1rp;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/notifications/util/NotificationsConnectionControllerManager$3;

    invoke-direct {v1, p0}, Lcom/facebook/notifications/util/NotificationsConnectionControllerManager$3;-><init>(LX/1rp;)V

    const v2, 0x4d2244f6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 333327
    return-void
.end method
