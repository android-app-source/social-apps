.class public LX/0ou;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:LX/0ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ot",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final g:LX/0So;

.field private final h:J

.field private i:J

.field private j:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143288
    const-class v0, LX/0ou;

    sput-object v0, LX/0ou;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;IIIJLX/0ot;LX/0So;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;IIIJ",
            "LX/0ot",
            "<TT;>;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143279
    iput-object p1, p0, LX/0ou;->b:Ljava/lang/Class;

    .line 143280
    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0ou;->c:I

    .line 143281
    iget v0, p0, LX/0ou;->c:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0ou;->d:I

    .line 143282
    const/4 v0, 0x1

    invoke-static {p4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0ou;->e:I

    .line 143283
    iput-wide p5, p0, LX/0ou;->h:J

    .line 143284
    iput-object p7, p0, LX/0ou;->f:LX/0ot;

    .line 143285
    iput-object p8, p0, LX/0ou;->g:LX/0So;

    .line 143286
    iget-object v0, p0, LX/0ou;->b:Ljava/lang/Class;

    iget v1, p0, LX/0ou;->c:I

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/0ou;->j:[Ljava/lang/Object;

    .line 143287
    return-void
.end method

.method private static a(LX/0ou;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143273
    iget-object v0, p0, LX/0ou;->b:Ljava/lang/Class;

    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 143274
    iget-object v1, p0, LX/0ou;->j:[Ljava/lang/Object;

    iget-object v2, p0, LX/0ou;->j:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143275
    iput-object v0, p0, LX/0ou;->j:[Ljava/lang/Object;

    .line 143276
    iget v0, p0, LX/0ou;->k:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/0ou;->k:I

    .line 143277
    return-void
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 143246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ou;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 143247
    iget v2, p0, LX/0ou;->k:I

    iget v3, p0, LX/0ou;->e:I

    mul-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_0

    .line 143248
    iput-wide v0, p0, LX/0ou;->i:J

    .line 143249
    :cond_0
    iget-wide v2, p0, LX/0ou;->i:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, LX/0ou;->h:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 143250
    invoke-static {p0}, LX/0ou;->d(LX/0ou;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143251
    :cond_1
    monitor-exit p0

    return-void

    .line 143252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/0ou;)V
    .locals 2

    .prologue
    .line 143268
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ou;->j:[Ljava/lang/Object;

    array-length v0, v0

    iget v1, p0, LX/0ou;->e:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/0ou;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 143269
    iget-object v1, p0, LX/0ou;->j:[Ljava/lang/Object;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 143270
    invoke-static {p0, v0}, LX/0ou;->a(LX/0ou;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143271
    :cond_0
    monitor-exit p0

    return-void

    .line 143272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 143261
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ou;->k:I

    if-lez v0, :cond_0

    .line 143262
    iget v0, p0, LX/0ou;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0ou;->k:I

    .line 143263
    iget-object v0, p0, LX/0ou;->j:[Ljava/lang/Object;

    iget v1, p0, LX/0ou;->k:I

    aget-object v0, v0, v1

    .line 143264
    iget-object v1, p0, LX/0ou;->j:[Ljava/lang/Object;

    iget v2, p0, LX/0ou;->k:I

    const/4 v3, 0x0

    aput-object v3, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143265
    :goto_0
    monitor-exit p0

    return-object v0

    .line 143266
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0ou;->f:LX/0ot;

    invoke-interface {v0}, LX/0ot;->a()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 143267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 143253
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0ou;->c()V

    .line 143254
    iget-object v0, p0, LX/0ou;->f:LX/0ot;

    invoke-interface {v0, p1}, LX/0ot;->a(Ljava/lang/Object;)V

    .line 143255
    iget v0, p0, LX/0ou;->k:I

    iget v1, p0, LX/0ou;->d:I

    if-ge v0, v1, :cond_1

    .line 143256
    iget v0, p0, LX/0ou;->k:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/0ou;->j:[Ljava/lang/Object;

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 143257
    iget v0, p0, LX/0ou;->d:I

    iget-object v1, p0, LX/0ou;->j:[Ljava/lang/Object;

    array-length v1, v1

    iget v2, p0, LX/0ou;->e:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0ou;->a(LX/0ou;I)V

    .line 143258
    :cond_0
    iget-object v0, p0, LX/0ou;->j:[Ljava/lang/Object;

    iget v1, p0, LX/0ou;->k:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0ou;->k:I

    aput-object p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143259
    :cond_1
    monitor-exit p0

    return-void

    .line 143260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
