.class public final LX/1fl;
.super LX/1cZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1cZ",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final synthetic a:LX/1fB;

.field private b:I

.field private c:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1fB;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 292292
    iput-object p1, p0, LX/1fl;->a:LX/1fB;

    invoke-direct {p0}, LX/1cZ;-><init>()V

    .line 292293
    const/4 v0, 0x0

    iput v0, p0, LX/1fl;->b:I

    .line 292294
    iput-object v1, p0, LX/1fl;->c:LX/1ca;

    .line 292295
    iput-object v1, p0, LX/1fl;->d:LX/1ca;

    .line 292296
    invoke-direct {p0}, LX/1fl;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292297
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No data source supplier or supplier returned null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    .line 292298
    :cond_0
    return-void
.end method

.method public static a(LX/1fl;LX/1ca;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 292236
    const/4 v0, 0x0

    .line 292237
    monitor-enter p0

    .line 292238
    :try_start_0
    iget-object v1, p0, LX/1fl;->c:LX/1ca;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/1fl;->d:LX/1ca;

    if-ne p1, v1, :cond_1

    .line 292239
    :cond_0
    monitor-exit p0

    .line 292240
    :goto_0
    return-void

    .line 292241
    :cond_1
    iget-object v1, p0, LX/1fl;->d:LX/1ca;

    if-eqz v1, :cond_2

    if-eqz p2, :cond_3

    .line 292242
    :cond_2
    iget-object v0, p0, LX/1fl;->d:LX/1ca;

    .line 292243
    iput-object p1, p0, LX/1fl;->d:LX/1ca;

    .line 292244
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292245
    invoke-static {v0}, LX/1fl;->e(LX/1ca;)V

    goto :goto_0

    .line 292246
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized a(LX/1ca;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 292286
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 292287
    const/4 v0, 0x0

    .line 292288
    :goto_0
    monitor-exit p0

    return v0

    .line 292289
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/1fl;->c:LX/1ca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292290
    const/4 v0, 0x1

    goto :goto_0

    .line 292291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(LX/1ca;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 292280
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1fl;->c:LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_1

    .line 292281
    :cond_0
    const/4 v0, 0x0

    .line 292282
    :goto_0
    monitor-exit p0

    return v0

    .line 292283
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, LX/1fl;->c:LX/1ca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292284
    const/4 v0, 0x1

    goto :goto_0

    .line 292285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/1fl;LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292274
    invoke-direct {p0, p1}, LX/1fl;->b(LX/1ca;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 292275
    :cond_0
    :goto_0
    return-void

    .line 292276
    :cond_1
    invoke-static {p0}, LX/1fl;->j(LX/1fl;)LX/1ca;

    move-result-object v0

    if-eq p1, v0, :cond_2

    .line 292277
    invoke-static {p1}, LX/1fl;->e(LX/1ca;)V

    .line 292278
    :cond_2
    invoke-direct {p0}, LX/1fl;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292279
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method private static e(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292271
    if-eqz p0, :cond_0

    .line 292272
    invoke-interface {p0}, LX/1ca;->g()Z

    .line 292273
    :cond_0
    return-void
.end method

.method private h()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 292299
    invoke-direct {p0}, LX/1fl;->i()LX/1Gd;

    move-result-object v0

    .line 292300
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;

    .line 292301
    :goto_0
    invoke-direct {p0, v0}, LX/1fl;->a(LX/1ca;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 292302
    new-instance v2, LX/1fn;

    invoke-direct {v2, p0}, LX/1fn;-><init>(LX/1fl;)V

    .line 292303
    sget-object v1, LX/1fo;->a:LX/1fo;

    move-object v1, v1

    .line 292304
    invoke-interface {v0, v2, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 292305
    const/4 v0, 0x1

    .line 292306
    :goto_1
    return v0

    .line 292307
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292308
    :cond_1
    invoke-static {v0}, LX/1fl;->e(LX/1ca;)V

    move v0, v1

    .line 292309
    goto :goto_1
.end method

.method private declared-synchronized i()LX/1Gd;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292267
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LX/1fl;->b:I

    iget-object v1, p0, LX/1fl;->a:LX/1fB;

    iget-object v1, v1, LX/1fB;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 292268
    iget-object v0, p0, LX/1fl;->a:LX/1fB;

    iget-object v0, v0, LX/1fB;->a:Ljava/util/List;

    iget v1, p0, LX/1fl;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1fl;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292269
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized j(LX/1fl;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292266
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fl;->d:LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 292263
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1fl;->j(LX/1fl;)LX/1ca;

    move-result-object v0

    .line 292264
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1ca;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292260
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1fl;->j(LX/1fl;)LX/1ca;

    move-result-object v0

    .line 292261
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1ca;->d()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 292247
    monitor-enter p0

    .line 292248
    :try_start_0
    invoke-super {p0}, LX/1cZ;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292249
    const/4 v0, 0x0

    monitor-exit p0

    .line 292250
    :goto_0
    return v0

    .line 292251
    :cond_0
    iget-object v0, p0, LX/1fl;->c:LX/1ca;

    .line 292252
    const/4 v1, 0x0

    iput-object v1, p0, LX/1fl;->c:LX/1ca;

    .line 292253
    iget-object v1, p0, LX/1fl;->d:LX/1ca;

    .line 292254
    const/4 v2, 0x0

    iput-object v2, p0, LX/1fl;->d:LX/1ca;

    .line 292255
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292256
    invoke-static {v1}, LX/1fl;->e(LX/1ca;)V

    .line 292257
    invoke-static {v0}, LX/1fl;->e(LX/1ca;)V

    .line 292258
    const/4 v0, 0x1

    goto :goto_0

    .line 292259
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
