.class public LX/1gi;
.super LX/0jZ;
.source ""


# static fields
.field private static final r:[I


# instance fields
.field public final a:LX/0fz;

.field public final b:LX/1ge;

.field private final c:LX/1gf;

.field private final d:LX/1gg;

.field private final e:LX/0ad;

.field public f:LX/1gh;

.field private final g:LX/0pV;

.field private final h:Lcom/facebook/user/model/User;

.field private final i:Z

.field public final j:LX/1gj;

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1lu;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1jZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:LX/1gl;

.field private final p:LX/0pW;

.field public q:LX/0g3;

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/api/feedtype/FeedType;

.field private v:Lcom/facebook/api/feed/FetchFeedParams;

.field private w:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public x:LX/1gm;

.field private y:I

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 294770
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/1gi;->r:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
        0x9
    .end array-data
.end method

.method public constructor <init>(Landroid/os/Looper;LX/0fz;LX/1ge;LX/1gf;LX/0g3;LX/1gg;LX/1gh;Lcom/facebook/api/feedtype/FeedType;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/0ad;LX/0pV;Lcom/facebook/user/model/User;LX/0pW;LX/1gj;)V
    .locals 3
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0fz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1ge;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1gf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0g3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1gg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/1gh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "LX/0fz;",
            "LX/1ge;",
            "LX/1gf;",
            "LX/0g3;",
            "LX/1gg;",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoaderUIHandler$CollectionChangeListener;",
            "Lcom/facebook/api/feedtype/FeedType;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0ad;",
            "LX/0pV;",
            "Lcom/facebook/user/model/User;",
            "LX/0pW;",
            "LX/1gj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 294495
    sget-object v1, LX/1gi;->r:[I

    invoke-direct {p0, p1, v1}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 294496
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1gi;->k:LX/0Ot;

    .line 294497
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1gi;->l:LX/0Ot;

    .line 294498
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1gi;->m:LX/0Ot;

    .line 294499
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1gi;->n:LX/0Ot;

    .line 294500
    new-instance v1, LX/1gk;

    invoke-direct {v1, p0}, LX/1gk;-><init>(LX/1gi;)V

    iput-object v1, p0, LX/1gi;->o:LX/1gl;

    .line 294501
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1gi;->s:Z

    .line 294502
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1gi;->t:Z

    .line 294503
    const/16 v1, 0x231

    iput v1, p0, LX/1gi;->y:I

    .line 294504
    const/16 v1, 0x231

    iput v1, p0, LX/1gi;->z:I

    .line 294505
    iput-object p2, p0, LX/1gi;->a:LX/0fz;

    .line 294506
    iput-object p3, p0, LX/1gi;->b:LX/1ge;

    .line 294507
    iput-object p4, p0, LX/1gi;->c:LX/1gf;

    .line 294508
    iput-object p5, p0, LX/1gi;->q:LX/0g3;

    .line 294509
    iput-object p6, p0, LX/1gi;->d:LX/1gg;

    .line 294510
    new-instance v1, Lcom/facebook/api/feed/FetchFeedParams;

    new-instance v2, LX/0rT;

    invoke-direct {v2}, LX/0rT;-><init>()V

    invoke-direct {v1, v2}, Lcom/facebook/api/feed/FetchFeedParams;-><init>(LX/0rT;)V

    iput-object v1, p0, LX/1gi;->v:Lcom/facebook/api/feed/FetchFeedParams;

    .line 294511
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    iput-object v1, p0, LX/1gi;->w:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 294512
    iput-object p11, p0, LX/1gi;->e:LX/0ad;

    .line 294513
    iput-object p7, p0, LX/1gi;->f:LX/1gh;

    .line 294514
    iput-object p12, p0, LX/1gi;->g:LX/0pV;

    .line 294515
    iput-object p8, p0, LX/1gi;->u:Lcom/facebook/api/feedtype/FeedType;

    .line 294516
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1gi;->h:Lcom/facebook/user/model/User;

    .line 294517
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1gi;->p:LX/0pW;

    .line 294518
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1gi;->j:LX/1gj;

    .line 294519
    invoke-interface {p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, LX/1gi;->i:Z

    .line 294520
    return-void

    .line 294521
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(ILX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 294737
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294738
    :cond_0
    :goto_0
    return-void

    .line 294739
    :cond_1
    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    .line 294740
    iget-object v0, p0, LX/1gi;->p:LX/0pW;

    invoke-virtual {v0, v1}, LX/0pW;->a(Z)V

    .line 294741
    :cond_2
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 294742
    :goto_1
    iget-object v1, p0, LX/1gi;->p:LX/0pW;

    invoke-static {p1}, LX/1jh;->b(I)Z

    move-result v2

    .line 294743
    packed-switch p1, :pswitch_data_0

    .line 294744
    sparse-switch p1, :sswitch_data_0

    .line 294745
    const/4 v3, 0x0

    :goto_2
    move v3, v3

    .line 294746
    :goto_3
    move v3, v3

    .line 294747
    iget-object v4, v1, LX/0pW;->a:LX/0Yi;

    const v1, 0xa0038

    const p2, 0xa0013

    const/4 p1, 0x1

    .line 294748
    iput-boolean p1, v4, LX/0Yi;->s:Z

    .line 294749
    if-eqz v2, :cond_4

    .line 294750
    iget-boolean v5, v4, LX/0Yi;->v:Z

    if-nez v5, :cond_3

    iget-object v5, v4, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string p0, "NNFHotStartTTI"

    invoke-interface {v5, v1, p0}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 294751
    iput-boolean p1, v4, LX/0Yi;->v:Z

    .line 294752
    iget-object v5, v4, LX/0Yi;->j:LX/0Yl;

    const-string p0, "NNFHotStartTTI"

    invoke-virtual {v5, v1, p0}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 294753
    :cond_3
    invoke-static {v4}, LX/0Yi;->P(LX/0Yi;)V

    .line 294754
    :cond_4
    if-eqz v3, :cond_7

    .line 294755
    invoke-static {v4}, LX/0Yi;->P(LX/0Yi;)V

    .line 294756
    const v5, 0xa004d

    const-string p0, "NNFColdStartNetwork"

    invoke-static {v4, v5, p0}, LX/0Yi;->c(LX/0Yi;ILjava/lang/String;)V

    .line 294757
    sget-object v5, LX/0ao;->FRESH_FEED_NETWORK:LX/0ao;

    invoke-static {v4, v5}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 294758
    :goto_4
    iget-object v5, v4, LX/0Yi;->j:LX/0Yl;

    const-string p0, "NNFWarmStartTTI"

    invoke-virtual {v5, p2, p0}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 294759
    iget-object v5, v4, LX/0Yi;->j:LX/0Yl;

    const-string p0, "NNFWarmStartTTI"

    invoke-virtual {v5, p2, p0}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    .line 294760
    :cond_5
    goto :goto_0

    :cond_6
    move v0, v2

    .line 294761
    goto :goto_1

    .line 294762
    :pswitch_0
    const/4 v3, 0x1

    goto :goto_3

    .line 294763
    :sswitch_0
    const/4 v3, 0x1

    goto :goto_2

    .line 294764
    :cond_7
    iget-object v5, v4, LX/0Yi;->j:LX/0Yl;

    const p0, 0xa004d

    const-string p1, "NNFColdStartNetwork"

    invoke-virtual {v5, p0, p1}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 294765
    if-eqz v0, :cond_8

    .line 294766
    sget-object v5, LX/0ao;->FRESH_FEED_CACHE_UNREAD:LX/0ao;

    invoke-static {v4, v5}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 294767
    goto :goto_4

    .line 294768
    :cond_8
    sget-object v5, LX/0ao;->FRESH_FEED_CACHE:LX/0ao;

    invoke-static {v4, v5}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 294769
    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(ILX/0Px;I)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 294693
    const-string v0, "FreshFeedDataLoaderUIHandler.doSendStoriesToUI"

    const v1, 0x30446e48

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294694
    :try_start_0
    iget-boolean v3, p0, LX/1gi;->s:Z

    .line 294695
    iget-boolean v4, p0, LX/1gi;->t:Z

    .line 294696
    iget-boolean v0, p0, LX/1gi;->s:Z

    if-eqz v0, :cond_0

    .line 294697
    iget-boolean v0, p0, LX/1gi;->t:Z

    if-eqz v0, :cond_1

    .line 294698
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1gi;->t:Z

    .line 294699
    iget-object v0, p0, LX/1gi;->g:LX/0pV;

    const-string v1, "FeedDataLoaderUIHandler"

    const-string v2, "Avoiding clear UI"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 294700
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/1gi;->i:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 294701
    iget-object v0, p0, LX/1gi;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    invoke-virtual {v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a()LX/0Px;

    move-result-object v0

    .line 294702
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    .line 294703
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p2

    move-object v2, p2

    .line 294704
    :goto_1
    invoke-static {p0}, LX/1gi;->m(LX/1gi;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294705
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 294706
    invoke-static {p0, v0, p1}, LX/1gi;->a(LX/1gi;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 294707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 294708
    :cond_1
    const/16 v0, 0x231

    iput v0, p0, LX/1gi;->y:I

    .line 294709
    const/16 v0, 0x231

    iput v0, p0, LX/1gi;->z:I

    .line 294710
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1gi;->s:Z

    .line 294711
    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->n()V

    .line 294712
    iget-object v0, p0, LX/1gi;->j:LX/1gj;

    invoke-virtual {v0}, LX/1gj;->a()V

    .line 294713
    iget-object v0, p0, LX/1gi;->q:LX/0g3;

    invoke-interface {v0}, LX/0g3;->a()V

    .line 294714
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    if-eqz v0, :cond_0

    .line 294715
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    invoke-virtual {v0}, LX/1gm;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 294716
    :catchall_0
    move-exception v0

    const v1, -0x72e2e54

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 294717
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->l()LX/0qm;

    move-result-object v0

    invoke-virtual {v0}, LX/0qm;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 294718
    invoke-direct {p0}, LX/1gi;->l()V

    .line 294719
    :cond_3
    const/16 v0, 0xc

    if-eq v0, p1, :cond_4

    const/16 v0, 0xe

    if-ne v0, p1, :cond_a

    .line 294720
    :cond_4
    invoke-direct {p0, v2, p1}, LX/1gi;->a(LX/0Px;I)V

    .line 294721
    :goto_3
    iget-object v0, p0, LX/1gi;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jZ;

    invoke-virtual {v0, v2, p1, v3, v4}, LX/1jZ;->a(LX/0Px;IZZ)V

    .line 294722
    invoke-direct {p0, p1, v2}, LX/1gi;->a(ILX/0Px;)V

    .line 294723
    iget-object v11, p0, LX/1gi;->q:LX/0g3;

    const/4 v12, 0x0

    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, LX/1gi;->v:Lcom/facebook/api/feed/FetchFeedParams;

    iget-object v3, p0, LX/1gi;->w:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v4, 0x0

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;Ljava/lang/String;LX/0ta;JZ)V

    sget-object v6, LX/18G;->SUCCESS:LX/18G;

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, LX/0qw;->FULL:LX/0qw;

    sget-object v10, LX/18O;->Perform:LX/18O;

    move-object v3, v11

    move v4, v12

    move-object v5, v0

    invoke-interface/range {v3 .. v10}, LX/0g3;->a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V

    .line 294724
    if-eqz p1, :cond_5

    const/16 v0, 0xa

    if-eq p1, v0, :cond_5

    const/4 v0, 0x5

    if-eq p1, v0, :cond_5

    const/4 v0, 0x3

    if-ne p1, v0, :cond_c

    .line 294725
    :cond_5
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    sget-object v4, LX/1lq;->HideLoadingIndicator:LX/1lq;

    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->i()I

    move-result v5

    const/4 v0, 0x5

    if-eq p1, v0, :cond_6

    const/4 v0, 0x3

    if-ne p1, v0, :cond_b

    :cond_6
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    :goto_4
    invoke-interface {v1, v3, v4, v5, v0}, LX/0g3;->a(ILX/1lq;ILX/0ta;)V

    .line 294726
    :cond_7
    :goto_5
    iget-object v0, p0, LX/1gi;->d:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 294727
    iget-object v0, p0, LX/1gi;->d:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->c()V

    .line 294728
    :cond_8
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    if-eqz v0, :cond_9

    .line 294729
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    invoke-virtual {v0, v2}, LX/1gm;->a(LX/0Px;)V

    .line 294730
    :cond_9
    invoke-direct {p0, v2}, LX/1gi;->a(LX/0Px;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294731
    const v0, -0x174146b1

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294732
    return-void

    .line 294733
    :cond_a
    :try_start_2
    invoke-direct {p0, v2, p1}, LX/1gi;->b(LX/0Px;I)V

    goto :goto_3

    .line 294734
    :cond_b
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    goto :goto_4

    .line 294735
    :cond_c
    const/16 v0, 0x9

    if-ne p1, v0, :cond_7

    .line 294736
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, LX/1lq;->ShowNSBPFullyLoadedText:LX/1lq;

    :goto_6
    iget-object v4, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v4}, LX/0fz;->i()I

    move-result v4

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-interface {v1, v3, v0, v4, v5}, LX/0g3;->a(ILX/1lq;ILX/0ta;)V

    goto :goto_5

    :cond_d
    sget-object v0, LX/1lq;->HideNSBPIfNotFullyLoaded:LX/1lq;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_e
    move-object v2, p2

    goto/16 :goto_1
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 294688
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-static {v0}, LX/1gj;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, LX/1gi;->j:LX/1gj;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jQ;

    invoke-virtual {v1, v0}, LX/1gj;->a(LX/0jQ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294689
    :cond_0
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    .line 294690
    iget-object v1, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string p0, "onFirstSponsoredFeedUnitInvalidated"

    invoke-virtual {v1, v2, p0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 294691
    iget-object v1, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c$redex0(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 294692
    :cond_1
    return-void
.end method

.method private a(LX/0Px;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 294681
    iget-object v0, p0, LX/1gi;->q:LX/0g3;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0g3;->a(Z)V

    .line 294682
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 294683
    invoke-static {p0, v0, p2}, LX/1gi;->a(LX/1gi;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 294684
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_OLD"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b(Ljava/lang/String;)V

    .line 294685
    iget-object v3, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v3, v0}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 294686
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 294687
    :cond_0
    return-void
.end method

.method public static a(LX/1gi;I)V
    .locals 3

    .prologue
    .line 294670
    sparse-switch p1, :sswitch_data_0

    .line 294671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid finish status"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294672
    :sswitch_0
    iget-object v0, p0, LX/1gi;->c:LX/1gf;

    .line 294673
    iget-object v1, v0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->b()V

    .line 294674
    :goto_0
    return-void

    .line 294675
    :sswitch_1
    iget-object v0, p0, LX/1gi;->c:LX/1gf;

    .line 294676
    iget-object v1, v0, LX/1gf;->b:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->b()V

    .line 294677
    goto :goto_0

    .line 294678
    :sswitch_2
    iget-object v0, p0, LX/1gi;->c:LX/1gf;

    .line 294679
    iget-object v1, v0, LX/1gf;->c:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->b()V

    .line 294680
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xd -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/1gi;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1gi;",
            "LX/0Ot",
            "<",
            "LX/1lu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pd;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1jZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294669
    iput-object p1, p0, LX/1gi;->k:LX/0Ot;

    iput-object p2, p0, LX/1gi;->l:LX/0Ot;

    iput-object p3, p0, LX/1gi;->m:LX/0Ot;

    iput-object p4, p0, LX/1gi;->n:LX/0Ot;

    return-void
.end method

.method private static a(LX/1gi;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V
    .locals 1

    .prologue
    .line 294663
    const/16 v0, 0xc

    if-eq v0, p2, :cond_0

    const/16 v0, 0xe

    if-ne v0, p2, :cond_1

    .line 294664
    :cond_0
    iget v0, p0, LX/1gi;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1gi;->z:I

    .line 294665
    iget v0, p0, LX/1gi;->z:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 294666
    :goto_0
    return-void

    .line 294667
    :cond_1
    iget v0, p0, LX/1gi;->y:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 294668
    iget v0, p0, LX/1gi;->y:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1gi;->y:I

    goto :goto_0
.end method

.method private b(LX/0Px;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 294633
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    invoke-interface {v1, v0}, LX/0g3;->a(Z)V

    .line 294634
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 294635
    iget-object v3, p0, LX/1gi;->j:LX/1gj;

    iget-object v4, p0, LX/1gi;->a:LX/0fz;

    const/4 v6, 0x1

    .line 294636
    invoke-static {v0}, LX/1gj;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v3, v0}, LX/1gj;->a(LX/0jQ;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    .line 294637
    :goto_1
    move v3, v5

    .line 294638
    if-eqz v3, :cond_2

    .line 294639
    invoke-static {p0}, LX/1gi;->m(LX/1gi;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 294640
    invoke-static {p0, v0, p2}, LX/1gi;->a(LX/1gi;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 294641
    :cond_0
    iget-object v3, p0, LX/1gi;->a:LX/0fz;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iget-object v4, p0, LX/1gi;->w:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v3, v0, v4}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 294642
    iget-object v0, p0, LX/1gi;->j:LX/1gj;

    iget-object v3, p0, LX/1gi;->a:LX/0fz;

    const/4 v4, 0x0

    .line 294643
    iget-object v5, v0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 294644
    const/4 v5, 0x0

    .line 294645
    :goto_2
    move-object v6, v5

    .line 294646
    if-nez v6, :cond_7

    .line 294647
    :cond_1
    :goto_3
    move-object v0, v4

    .line 294648
    if-eqz v0, :cond_2

    .line 294649
    invoke-static {p0, v0, p2}, LX/1gi;->a(LX/1gi;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 294650
    iget-object v3, p0, LX/1gi;->a:LX/0fz;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iget-object v4, p0, LX/1gi;->w:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v3, v0, v4}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 294651
    iget-object v0, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v0

    .line 294652
    iget-object v3, p0, LX/1gi;->g:LX/0pV;

    const-string v4, "FeedDataLoaderUIHandler"

    const-string v5, "Add sponsored unit at position"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294653
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 294654
    :cond_3
    return-void

    .line 294655
    :cond_4
    iget-object v5, v3, LX/1gj;->e:LX/0ad;

    sget-short v7, LX/0fe;->R:S

    const/4 v8, 0x0

    invoke-interface {v5, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 294656
    if-eqz v5, :cond_5

    sget-object v5, LX/2df;->TAIL:LX/2df;

    .line 294657
    :goto_4
    const/4 v7, 0x0

    invoke-static {v3, v4, v0, v7, v5}, LX/1gj;->a(LX/1gj;LX/0fz;Lcom/facebook/feed/model/ClientFeedUnitEdge;LX/2dg;LX/2df;)Z

    move-result v5

    if-nez v5, :cond_6

    move v5, v6

    goto :goto_1

    .line 294658
    :cond_5
    sget-object v5, LX/2df;->HEAD:LX/2df;

    goto :goto_4

    .line 294659
    :cond_6
    const/4 v5, 0x0

    goto :goto_1

    .line 294660
    :cond_7
    iget-object v5, v6, LX/2dg;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-object v5, v5

    .line 294661
    sget-object v7, LX/2df;->HEAD:LX/2df;

    invoke-static {v0, v3, v5, v6, v7}, LX/1gj;->a(LX/1gj;LX/0fz;Lcom/facebook/feed/model/ClientFeedUnitEdge;LX/2dg;LX/2df;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v4, v5

    .line 294662
    goto :goto_3

    :cond_8
    iget-object v5, v0, LX/1gj;->a:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2dg;

    goto :goto_2
.end method

.method private c(II)V
    .locals 11

    .prologue
    .line 294606
    const-string v0, "FreshFeedDataLoaderUIHandler.doNetworkCompleteRequest"

    const v1, 0x5e1194e2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294607
    :try_start_0
    invoke-static {p0, p2}, LX/1gi;->a(LX/1gi;I)V

    .line 294608
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    if-eqz v0, :cond_3

    .line 294609
    iget-object v0, p0, LX/1gi;->x:LX/1gm;

    const/4 v6, 0x7

    .line 294610
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v5, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v4, v5}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 294611
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ae(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 294612
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4, p2}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->l(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 294613
    if-ne p2, v6, :cond_1

    if-nez p1, :cond_1

    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->V:LX/1EU;

    invoke-virtual {v4}, LX/1EU;->b()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->V:LX/1EU;

    invoke-virtual {v4}, LX/1EU;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 294614
    :cond_0
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 294615
    :cond_1
    if-ne p2, v6, :cond_2

    .line 294616
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 294617
    iget-object v7, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->S:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/0pP;->e:LX/0Tn;

    iget-object v9, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    invoke-interface {v7, v8, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 294618
    :cond_2
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 294619
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v4}, LX/1ge;->c()V

    .line 294620
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z:I

    if-nez v4, :cond_3

    .line 294621
    sget-object v4, LX/00a;->a:LX/00a;

    move-object v4, v4

    .line 294622
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/00a;->a(Ljava/lang/String;)V

    .line 294623
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v5, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v6, "Empty head fetch, and empty DB. Do full fetch."

    invoke-virtual {v4, v5, v6}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 294624
    iget-object v4, v0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    sget-object v5, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-virtual {v4, v5}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(LX/0gf;)Z

    .line 294625
    :cond_3
    const/4 v0, 0x7

    if-ne p2, v0, :cond_5

    .line 294626
    iget-object v0, p0, LX/1gi;->q:LX/0g3;

    sget-object v1, LX/1lq;->HideLoadingIndicator:LX/1lq;

    iget-object v2, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->i()I

    move-result v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-interface {v0, p1, v1, v2, v3}, LX/0g3;->a(ILX/1lq;ILX/0ta;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294627
    :cond_4
    :goto_0
    const v0, 0x1dd1918c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294628
    return-void

    .line 294629
    :cond_5
    const/16 v0, 0x8

    if-ne p2, v0, :cond_4

    .line 294630
    :try_start_1
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    iget-object v0, p0, LX/1gi;->b:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-interface {v1, v0, p1, v2}, LX/0g3;->a(ZILX/0ta;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 294631
    :catchall_0
    move-exception v0

    const v1, -0x694b6ad6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 294632
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(II)V
    .locals 5

    .prologue
    .line 294592
    const-string v0, "FreshFeedDataLoaderUIHandler.doFirstNetworkResponseReceived"

    const v1, 0x68181626

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294593
    :try_start_0
    iget-object v0, p0, LX/1gi;->p:LX/0pW;

    invoke-static {p1}, LX/1jh;->b(I)Z

    move-result v1

    .line 294594
    iget-object v2, v0, LX/0pW;->a:LX/0Yi;

    const v0, 0xa0038

    const/4 p1, 0x1

    .line 294595
    if-eqz v1, :cond_0

    invoke-static {v2}, LX/0Yi;->E(LX/0Yi;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 294596
    iget-object v3, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0xa0040

    const-string p0, "NNFPullToRefreshNetworkTime"

    invoke-interface {v3, v4, p0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 294597
    :cond_0
    if-nez p2, :cond_2

    if-eqz v1, :cond_2

    .line 294598
    iget-boolean v3, v2, LX/0Yi;->s:Z

    if-nez v3, :cond_1

    .line 294599
    iput-boolean p1, v2, LX/0Yi;->s:Z

    .line 294600
    :cond_1
    iget-boolean v3, v2, LX/0Yi;->v:Z

    if-nez v3, :cond_2

    iget-object v3, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNFHotStartTTI"

    invoke-interface {v3, v0, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 294601
    iput-boolean p1, v2, LX/0Yi;->v:Z

    .line 294602
    iget-object v3, v2, LX/0Yi;->j:LX/0Yl;

    const-string v4, "NNFHotStartTTI"

    invoke-virtual {v3, v0, v4}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294603
    :cond_2
    const v0, 0x41c3a56c    # 24.455772f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294604
    return-void

    .line 294605
    :catchall_0
    move-exception v0

    const v1, 0x7bfe55a3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 294587
    iget-object v0, p0, LX/1gi;->h:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    .line 294588
    iget-object v0, p0, LX/1gi;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lu;

    iget-object v1, p0, LX/1gi;->o:LX/1gl;

    iget-object v2, p0, LX/1gi;->a:LX/0fz;

    .line 294589
    iget-object p0, v2, LX/0fz;->d:LX/0qm;

    move-object v2, p0

    .line 294590
    invoke-virtual {v0, v1, v2}, LX/1lu;->a(LX/1gl;LX/0qm;)V

    .line 294591
    :cond_0
    return-void
.end method

.method private static m(LX/1gi;)Z
    .locals 3

    .prologue
    .line 294586
    iget-object v0, p0, LX/1gi;->e:LX/0ad;

    sget-short v1, LX/0fe;->ad:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 294582
    invoke-super {p0}, LX/0jZ;->a()V

    .line 294583
    iput-boolean v0, p0, LX/1gi;->s:Z

    .line 294584
    iput-boolean v0, p0, LX/1gi;->t:Z

    .line 294585
    return-void
.end method

.method public final a(ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294580
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1, p2}, LX/1gi;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 294581
    return-void
.end method

.method public final a(LX/0Px;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 294578
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p2, p3, p1}, LX/1gi;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 294579
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 294576
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/1gi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 294577
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 294522
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 294523
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294524
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, LX/1gi;->c(II)V

    .line 294525
    :goto_0
    return-void

    .line 294526
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AjU;

    const/4 v2, 0x0

    .line 294527
    iget v1, v0, LX/AjU;->a:I

    invoke-static {p0, v1}, LX/1gi;->a(LX/1gi;I)V

    .line 294528
    iget-object v1, p0, LX/1gi;->x:LX/1gm;

    if-eqz v1, :cond_1

    .line 294529
    iget-object v1, p0, LX/1gi;->x:LX/1gm;

    iget v3, v0, LX/AjU;->a:I

    .line 294530
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string p1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v4, p1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 294531
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ae(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 294532
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 294533
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4, v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->l(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 294534
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v4}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 294535
    :cond_0
    iget-object v4, v1, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v4, v4, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v4}, LX/1ge;->c()V

    .line 294536
    :cond_1
    iget v1, v0, LX/AjU;->a:I

    const/4 v3, 0x7

    if-ne v1, v3, :cond_6

    .line 294537
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    sget-object v3, LX/1lq;->HideLoadingIndicator:LX/1lq;

    iget-object v4, p0, LX/1gi;->a:LX/0fz;

    invoke-virtual {v4}, LX/0fz;->i()I

    move-result v4

    sget-object p1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-interface {v1, v2, v3, v4, p1}, LX/0g3;->a(ILX/1lq;ILX/0ta;)V

    .line 294538
    :cond_2
    :goto_1
    iget-object v1, p0, LX/1gi;->q:LX/0g3;

    iget-object v2, v0, LX/AjU;->b:LX/0gf;

    iget-object v3, v0, LX/AjU;->c:Ljava/lang/Throwable;

    invoke-interface {v1, v2, v3}, LX/0g3;->a(LX/0gf;Ljava/lang/Throwable;)V

    .line 294539
    goto :goto_0

    .line 294540
    :pswitch_3
    iget-object v0, p0, LX/1gi;->b:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->b()V

    .line 294541
    goto :goto_0

    .line 294542
    :pswitch_4
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v1, v0, v2}, LX/1gi;->a(ILX/0Px;I)V

    goto :goto_0

    .line 294543
    :pswitch_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1gi;->s:Z

    .line 294544
    goto :goto_0

    .line 294545
    :pswitch_6
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 294546
    iget-object v2, p0, LX/1gi;->f:LX/1gh;

    if-eqz v2, :cond_3

    .line 294547
    iget-object v2, p0, LX/1gi;->f:LX/1gh;

    .line 294548
    iget-object v3, v2, LX/1gh;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v3, v3, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v4, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v3, v4}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 294549
    iget-object v3, v2, LX/1gh;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-static {v3, v1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->h(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 294550
    iget-object v3, v2, LX/1gh;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 294551
    iget-object v4, v3, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-static {v4}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 294552
    iget-object v4, v3, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1JH;

    .line 294553
    iget-object v2, v4, LX/1JH;->d:LX/1JR;

    invoke-virtual {v2}, LX/1JR;->a()Z

    move-result v2

    if-nez v2, :cond_8

    .line 294554
    :goto_3
    goto :goto_2

    .line 294555
    :cond_3
    goto/16 :goto_0

    .line 294556
    :pswitch_7
    iget-object v0, p0, LX/1gi;->f:LX/1gh;

    if-eqz v0, :cond_5

    .line 294557
    iget-object v0, p0, LX/1gi;->f:LX/1gh;

    .line 294558
    iget-object v1, v0, LX/1gh;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 294559
    iget-object v2, v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 294560
    iget-object v2, v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1JH;

    .line 294561
    iget-object p0, v2, LX/1JH;->d:LX/1JR;

    invoke-virtual {p0}, LX/1JR;->a()Z

    move-result p0

    if-nez p0, :cond_a

    .line 294562
    :cond_4
    :goto_5
    goto :goto_4

    .line 294563
    :cond_5
    goto/16 :goto_0

    .line 294564
    :pswitch_8
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1gi;->t:Z

    .line 294565
    goto/16 :goto_0

    .line 294566
    :pswitch_9
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, LX/1gi;->d(II)V

    goto/16 :goto_0

    .line 294567
    :pswitch_a
    iget-object v0, p0, LX/1gi;->j:LX/1gj;

    invoke-virtual {v0}, LX/1gj;->a()V

    .line 294568
    goto/16 :goto_0

    .line 294569
    :cond_6
    iget v1, v0, LX/AjU;->a:I

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 294570
    iget-object v3, p0, LX/1gi;->q:LX/0g3;

    iget-object v1, p0, LX/1gi;->b:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->d()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_6
    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-interface {v3, v1, v2, v4}, LX/0g3;->a(ZILX/0ta;)V

    goto/16 :goto_1

    :cond_7
    move v1, v2

    goto :goto_6

    .line 294571
    :cond_8
    iget-object v2, v4, LX/1JH;->d:LX/1JR;

    invoke-virtual {v2}, LX/1JR;->b()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 294572
    invoke-static {v4}, LX/1JH;->i(LX/1JH;)V

    goto :goto_3

    .line 294573
    :cond_9
    iget-object v2, v4, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$1;

    invoke-direct {v1, v4, v0}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$1;-><init>(LX/1JH;Ljava/util/List;)V

    const v3, -0x79db4618

    invoke-static {v2, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3

    .line 294574
    :cond_a
    iget-object p0, v2, LX/1JH;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {p0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_4

    .line 294575
    iget-object p0, v2, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$3;

    invoke-direct {v0, v2}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$3;-><init>(LX/1JH;)V

    const v1, -0x16db332c

    invoke-static {p0, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method
