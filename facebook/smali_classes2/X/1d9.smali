.class public final LX/1d9;
.super LX/1dA;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284113
    invoke-direct {p0}, LX/1dA;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1d7;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 284110
    new-instance v0, LX/1dC;

    invoke-direct {v0, p0, p1}, LX/1dC;-><init>(LX/1d9;LX/1d7;)V

    .line 284111
    new-instance p0, LX/1dE;

    invoke-direct {p0, v0}, LX/1dE;-><init>(LX/1dD;)V

    move-object v0, p0

    .line 284112
    return-object v0
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityManager;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284108
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 284109
    return-object v0
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityManager;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284106
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 284107
    return-object v0
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;LX/1d7;)Z
    .locals 1

    .prologue
    .line 284098
    iget-object v0, p2, LX/1d7;->a:Ljava/lang/Object;

    .line 284099
    check-cast v0, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    move-result p0

    move v0, p0

    .line 284100
    return v0
.end method

.method public final b(Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 1

    .prologue
    .line 284104
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    move v0, v0

    .line 284105
    return v0
.end method

.method public final b(Landroid/view/accessibility/AccessibilityManager;LX/1d7;)Z
    .locals 1

    .prologue
    .line 284101
    iget-object v0, p2, LX/1d7;->a:Ljava/lang/Object;

    .line 284102
    check-cast v0, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    move-result p0

    move v0, p0

    .line 284103
    return v0
.end method
