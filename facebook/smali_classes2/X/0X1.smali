.class public LX/0X1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0WP;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2P7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0WP;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WP;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2P7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76909
    iput-object p1, p0, LX/0X1;->a:LX/0WP;

    .line 76910
    iput-object p2, p0, LX/0X1;->b:LX/0Ot;

    .line 76911
    iput-object p3, p0, LX/0X1;->c:LX/0Ot;

    .line 76912
    return-void
.end method

.method public static d(LX/0X1;Ljava/lang/String;)LX/0WS;
    .locals 3
    .param p0    # LX/0X1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 76913
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76914
    const/4 v0, 0x0

    .line 76915
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0X1;->a:LX/0WP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "logged_in_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    goto :goto_0
.end method
