.class public LX/0YP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y8;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0YP;


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0YR;

.field private final c:LX/0Xe;


# direct methods
.method public constructor <init>(LX/0YR;LX/0Xe;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 81167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81168
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0YP;->a:Landroid/util/SparseArray;

    .line 81169
    iput-object p1, p0, LX/0YP;->b:LX/0YR;

    .line 81170
    iput-object p2, p0, LX/0YP;->c:LX/0Xe;

    .line 81171
    return-void
.end method

.method public static a(LX/0QB;)LX/0YP;
    .locals 5

    .prologue
    .line 81172
    sget-object v0, LX/0YP;->d:LX/0YP;

    if-nez v0, :cond_1

    .line 81173
    const-class v1, LX/0YP;

    monitor-enter v1

    .line 81174
    :try_start_0
    sget-object v0, LX/0YP;->d:LX/0YP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 81175
    if-eqz v2, :cond_0

    .line 81176
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 81177
    new-instance p0, LX/0YP;

    invoke-static {v0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v3

    check-cast v3, LX/0YR;

    invoke-static {v0}, LX/0XY;->a(LX/0QB;)LX/0Xe;

    move-result-object v4

    check-cast v4, LX/0Xe;

    invoke-direct {p0, v3, v4}, LX/0YP;-><init>(LX/0YR;LX/0Xe;)V

    .line 81178
    move-object v0, p0

    .line 81179
    sput-object v0, LX/0YP;->d:LX/0YP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81180
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 81181
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 81182
    :cond_1
    sget-object v0, LX/0YP;->d:LX/0YP;

    return-object v0

    .line 81183
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 81184
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const v3, 0x7fffffff

    const/4 v1, 0x0

    .line 81185
    iget-object v0, p0, LX/0YP;->b:LX/0YR;

    if-nez v0, :cond_0

    move v0, v1

    .line 81186
    :goto_0
    return v0

    .line 81187
    :cond_0
    iget-object v4, p0, LX/0YP;->a:Landroid/util/SparseArray;

    monitor-enter v4

    .line 81188
    :try_start_0
    iget-object v0, p0, LX/0YP;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 81189
    if-eqz v0, :cond_5

    .line 81190
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 81191
    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81192
    if-eq v0, v3, :cond_4

    .line 81193
    iget-object v4, p0, LX/0YP;->c:LX/0Xe;

    invoke-virtual {v4}, LX/0Xe;->a()LX/0ar;

    move-result-object v4

    .line 81194
    if-eqz v4, :cond_2

    .line 81195
    iget-object v0, p0, LX/0YP;->c:LX/0Xe;

    const v5, 0x5d0001

    invoke-virtual {v4, v5}, LX/0ar;->a(I)I

    move-result v4

    invoke-virtual {v0, v4}, LX/0Xe;->a(I)I

    move-result v0

    if-eq v0, v3, :cond_1

    move v0, v2

    goto :goto_0

    .line 81196
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 81197
    goto :goto_0

    .line 81198
    :cond_2
    iget-object v4, p0, LX/0YP;->c:LX/0Xe;

    invoke-virtual {v4, v0}, LX/0Xe;->a(I)I

    move-result v0

    if-eq v0, v3, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 81199
    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 81200
    iget-object v1, p0, LX/0YP;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81201
    :try_start_0
    iget-object v0, p0, LX/0YP;->a:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 81202
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 1

    .prologue
    .line 81203
    iget v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    move v0, v0

    .line 81204
    invoke-direct {p0, v0}, LX/0YP;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81205
    iget-object v0, p0, LX/0YP;->b:LX/0YR;

    invoke-interface {v0}, LX/0YR;->a()LX/1hM;

    move-result-object v0

    .line 81206
    if-eqz v0, :cond_0

    .line 81207
    invoke-virtual {v0}, LX/1hM;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/util/List;)V

    .line 81208
    :cond_0
    return-void
.end method
