.class public LX/0Sr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:B

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile c:LX/0R7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Or;LX/0R7;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<TT;>;",
            "LX/0R7;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62227
    iput-object p1, p0, LX/0Sr;->b:LX/0Or;

    .line 62228
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v0

    .line 62229
    iget-byte p1, v0, LX/0SD;->a:B

    move v0, p1

    .line 62230
    iput-byte v0, p0, LX/0Sr;->a:B

    .line 62231
    iput-object p2, p0, LX/0Sr;->c:LX/0R7;

    .line 62232
    return-void
.end method

.method public static a(LX/0Or;LX/0R7;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Or",
            "<TT;>;",
            "LX/0R7;",
            ")",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62233
    instance-of v0, p0, LX/0Ot;

    if-eqz v0, :cond_0

    .line 62234
    check-cast p0, LX/0Ot;

    move-object v0, p0

    .line 62235
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0Sr;

    invoke-direct {v0, p0, p1}, LX/0Sr;-><init>(LX/0Or;LX/0R7;)V

    goto :goto_0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 62236
    iget-object v0, p0, LX/0Sr;->c:LX/0R7;

    if-eqz v0, :cond_1

    .line 62237
    monitor-enter p0

    .line 62238
    :try_start_0
    iget-object v0, p0, LX/0Sr;->c:LX/0R7;

    if-eqz v0, :cond_0

    .line 62239
    iget-object v1, p0, LX/0Sr;->c:LX/0R7;

    .line 62240
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 62241
    iget-byte v0, p0, LX/0Sr;->a:B

    invoke-virtual {v2, v0}, LX/0SD;->b(B)B

    move-result v3

    .line 62242
    invoke-interface {v1}, LX/0R7;->a()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 62243
    :try_start_1
    iget-object v0, p0, LX/0Sr;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0Sr;->d:Ljava/lang/Object;

    .line 62244
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Sr;->b:LX/0Or;

    .line 62245
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Sr;->c:LX/0R7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62246
    :try_start_2
    invoke-interface {v1, v4}, LX/0R7;->a(Ljava/lang/Object;)V

    .line 62247
    iput-byte v3, v2, LX/0SD;->a:B

    .line 62248
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62249
    :cond_1
    iget-object v0, p0, LX/0Sr;->d:Ljava/lang/Object;

    return-object v0

    .line 62250
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1, v4}, LX/0R7;->a(Ljava/lang/Object;)V

    .line 62251
    iput-byte v3, v2, LX/0SD;->a:B

    .line 62252
    throw v0

    .line 62253
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
