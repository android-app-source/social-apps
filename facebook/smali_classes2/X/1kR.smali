.class public LX/1kR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0SG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0tX;

.field private final e:LX/0sa;

.field private final f:LX/0Wd;

.field private final g:LX/0sU;

.field private final h:LX/1E5;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0sa;LX/0Wd;LX/0sU;LX/1E5;)V
    .locals 0
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309629
    iput-object p1, p0, LX/1kR;->a:Landroid/content/Context;

    .line 309630
    iput-object p2, p0, LX/1kR;->b:LX/0SG;

    .line 309631
    iput-object p3, p0, LX/1kR;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 309632
    iput-object p4, p0, LX/1kR;->d:LX/0tX;

    .line 309633
    iput-object p5, p0, LX/1kR;->e:LX/0sa;

    .line 309634
    iput-object p6, p0, LX/1kR;->f:LX/0Wd;

    .line 309635
    iput-object p7, p0, LX/1kR;->g:LX/0sU;

    .line 309636
    iput-object p8, p0, LX/1kR;->h:LX/1E5;

    .line 309637
    return-void
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309618
    if-eqz p0, :cond_0

    .line 309619
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 309620
    if-eqz v0, :cond_0

    .line 309621
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 309622
    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 309623
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 309624
    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309625
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 309626
    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;->a()LX/0Px;

    move-result-object v0

    .line 309627
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1kR;J)LX/0zO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0zO",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309614
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1kR;->a(JLX/3Aj;)LX/0gW;

    move-result-object v0

    .line 309615
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v1

    .line 309616
    const-string v2, "count"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "is_inspiration"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "image_scale"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "image_size"

    const/16 v3, 0x20

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "image_high_width"

    iget-object v3, p0, LX/1kR;->e:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "supported_prompts"

    sget-object v3, LX/1kr;->a:LX/0Px;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 309617
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x384

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1kR;
    .locals 9

    .prologue
    .line 309612
    new-instance v0, LX/1kR;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v5

    check-cast v5, LX/0sa;

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v6

    check-cast v6, LX/0Wd;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-static {p0}, LX/1E5;->b(LX/0QB;)LX/1E5;

    move-result-object v8

    check-cast v8, LX/1E5;

    invoke-direct/range {v0 .. v8}, LX/1kR;-><init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0sa;LX/0Wd;LX/0sU;LX/1E5;)V

    .line 309613
    return-object v0
.end method

.method public static b(LX/1kR;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309638
    iget-object v0, p0, LX/1kR;->f:LX/0Wd;

    new-instance v1, LX/1va;

    invoke-direct {v1, p0}, LX/1va;-><init>(LX/1kR;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(JLX/3Aj;)LX/0gW;
    .locals 7
    .param p3    # LX/3Aj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 309605
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 309606
    new-instance v1, LX/1kq;

    invoke-direct {v1}, LX/1kq;-><init>()V

    move-object v1, v1

    .line 309607
    const-string v2, "start_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "end_time"

    const-wide/16 v4, 0x1c20

    add-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "all_prompts"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "frame_scale"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v2, "activity_count"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 309608
    if-eqz p3, :cond_0

    .line 309609
    const-string v0, "location_info"

    invoke-virtual {v1, v0, p3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 309610
    :cond_0
    iget-object v0, p0, LX/1kR;->g:LX/0sU;

    invoke-virtual {v0, v1}, LX/0sU;->a(LX/0gW;)V

    .line 309611
    return-object v1
.end method

.method public final a()LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309603
    iget-object v0, p0, LX/1kR;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1kp;->b:LX/0Tn;

    iget-object v2, p0, LX/1kR;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 309604
    invoke-static {p0, v0, v1}, LX/1kR;->b(LX/1kR;J)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->b:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # LX/0QK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309598
    if-nez p1, :cond_0

    .line 309599
    invoke-static {p0}, LX/1kR;->b(LX/1kR;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 309600
    :goto_0
    return-object v0

    .line 309601
    :cond_0
    invoke-virtual {p0}, LX/1kR;->a()LX/0zO;

    move-result-object v0

    .line 309602
    iget-object v1, p0, LX/1kR;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/1l0;

    invoke-direct {v1, p0, p2}, LX/1l0;-><init>(LX/1kR;LX/0QK;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 309589
    iget-object v0, p0, LX/1kR;->a:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 309590
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 309591
    iget-object v2, p0, LX/1kR;->h:LX/1E5;

    .line 309592
    invoke-virtual {v2, v1}, LX/1E5;->b(Z)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 309593
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    move v0, v1

    .line 309594
    :goto_1
    return v0

    .line 309595
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    move v0, v1

    .line 309596
    goto :goto_1

    .line 309597
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
