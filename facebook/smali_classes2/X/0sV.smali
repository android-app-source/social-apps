.class public LX/0sV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile z:LX/0sV;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z

.field public final s:Z

.field public final t:Z

.field public final u:Z

.field public final v:Z

.field public final w:LX/0Uh;

.field public final x:LX/0ad;

.field public final y:LX/0W3;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;LX/0W3;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x14

    const/4 v1, 0x5

    const/4 v3, 0x3

    const/4 v5, 0x0

    .line 152058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152059
    iput-object p2, p0, LX/0sV;->w:LX/0Uh;

    .line 152060
    iput-object p1, p0, LX/0sV;->x:LX/0ad;

    .line 152061
    iput-object p3, p0, LX/0sV;->y:LX/0W3;

    .line 152062
    sget-short v0, LX/0sW;->g:S

    const/16 v4, 0x2a8

    invoke-virtual {p2, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->a:Z

    .line 152063
    sget v4, LX/0sW;->m:I

    const/16 v0, 0x2a

    invoke-virtual {p2, v0, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {p1, v4, v0}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0sV;->b:I

    .line 152064
    sget v0, LX/0sW;->h:I

    const/16 v4, 0x2a

    invoke-virtual {p2, v4, v5}, LX/0Uh;->a(IZ)Z

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0sV;->c:I

    .line 152065
    sget v0, LX/0sW;->o:I

    const/16 v1, 0x2a

    invoke-virtual {p2, v1, v5}, LX/0Uh;->a(IZ)Z

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0sV;->d:I

    .line 152066
    sget v1, LX/0sW;->i:I

    const/16 v0, 0x2a

    invoke-virtual {p2, v0, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    invoke-interface {p1, v1, v0}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0sV;->e:I

    .line 152067
    sget-short v0, LX/0sW;->p:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->r:Z

    .line 152068
    sget v0, LX/0sW;->n:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0sV;->f:I

    .line 152069
    sget-short v0, LX/0sW;->q:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->g:Z

    .line 152070
    sget-short v0, LX/0sW;->k:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->h:Z

    .line 152071
    sget-short v0, LX/0sW;->j:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->i:Z

    .line 152072
    sget-short v0, LX/0sW;->e:S

    const/16 v1, 0x27

    invoke-virtual {p2, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->j:Z

    .line 152073
    sget-short v0, LX/0sW;->t:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->k:Z

    .line 152074
    sget-short v0, LX/0sW;->s:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->l:Z

    .line 152075
    sget-short v0, LX/0sW;->u:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->m:Z

    .line 152076
    sget-short v0, LX/0sW;->v:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->n:Z

    .line 152077
    sget-short v0, LX/0sW;->w:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->o:Z

    .line 152078
    sget-short v0, LX/0sW;->b:S

    const/16 v1, 0x2c

    invoke-virtual {p2, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->p:Z

    .line 152079
    sget-short v0, LX/0sW;->f:S

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->q:Z

    .line 152080
    sget-short v0, LX/0sW;->x:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->s:Z

    .line 152081
    sget-short v0, LX/0sW;->l:S

    const/16 v1, 0x29

    invoke-virtual {p2, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->t:Z

    .line 152082
    sget-short v0, LX/0sW;->d:S

    invoke-interface {p1, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->u:Z

    .line 152083
    sget-short v0, LX/0sW;->a:S

    const/16 v1, 0x29

    invoke-virtual {p2, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0sV;->v:Z

    .line 152084
    return-void

    :cond_0
    move v0, v2

    .line 152085
    goto/16 :goto_0

    .line 152086
    :cond_1
    const/4 v0, 0x2

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)LX/0sV;
    .locals 6

    .prologue
    .line 152087
    sget-object v0, LX/0sV;->z:LX/0sV;

    if-nez v0, :cond_1

    .line 152088
    const-class v1, LX/0sV;

    monitor-enter v1

    .line 152089
    :try_start_0
    sget-object v0, LX/0sV;->z:LX/0sV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152090
    if-eqz v2, :cond_0

    .line 152091
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152092
    new-instance p0, LX/0sV;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/0sV;-><init>(LX/0ad;LX/0Uh;LX/0W3;)V

    .line 152093
    move-object v0, p0

    .line 152094
    sput-object v0, LX/0sV;->z:LX/0sV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152095
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152096
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152097
    :cond_1
    sget-object v0, LX/0sV;->z:LX/0sV;

    return-object v0

    .line 152098
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152099
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 4

    .prologue
    .line 152100
    iget-object v0, p0, LX/0sV;->y:LX/0W3;

    sget-wide v2, LX/0X5;->bo:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 5

    .prologue
    .line 152101
    iget-object v0, p0, LX/0sV;->x:LX/0ad;

    sget-short v1, LX/0sW;->r:S

    iget-object v2, p0, LX/0sV;->w:LX/0Uh;

    const/16 v3, 0x2b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
