.class public abstract LX/1X5;
.super LX/1Dp;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/facebook/components/ComponentLifecycle;",
        ">",
        "LX/1Dp;"
    }
.end annotation


# instance fields
.field private a:LX/1De;

.field private b:I
    .annotation build Landroid/support/annotation/AttrRes;
    .end annotation
.end field

.field private c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public d:LX/1X1;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270451
    invoke-direct {p0}, LX/1Dp;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 270468
    invoke-super {p0}, LX/1Dp;->a()V

    .line 270469
    iput-object v1, p0, LX/1X5;->a:LX/1De;

    .line 270470
    iput v0, p0, LX/1X5;->b:I

    .line 270471
    iput v0, p0, LX/1X5;->c:I

    .line 270472
    iput-object v1, p0, LX/1X5;->d:LX/1X1;

    .line 270473
    return-void
.end method

.method public final a(LX/1De;IILX/1X1;)V
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1X1",
            "<T",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 270456
    iget-object v0, p1, LX/1De;->h:LX/1Ds;

    move-object v0, v0

    .line 270457
    invoke-super {p0, p1, v0}, LX/1Dp;->a(LX/1De;LX/1Ds;)V

    .line 270458
    iput-object p4, p0, LX/1X5;->d:LX/1X1;

    .line 270459
    iput-object p1, p0, LX/1X5;->a:LX/1De;

    .line 270460
    iput p2, p0, LX/1X5;->b:I

    .line 270461
    iput p3, p0, LX/1X5;->c:I

    .line 270462
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 270463
    :cond_0
    iget-object v0, p4, LX/1X1;->e:LX/1S3;

    const/4 p0, 0x0

    .line 270464
    invoke-virtual {p1, p2, p3}, LX/1De;->a(II)V

    .line 270465
    invoke-virtual {v0, p1, p4}, LX/1S3;->c(LX/1De;LX/1X1;)V

    .line 270466
    invoke-virtual {p1, p0, p0}, LX/1De;->a(II)V

    .line 270467
    :cond_1
    return-void
.end method

.method public final b()LX/1Dg;
    .locals 1

    .prologue
    .line 270474
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/1Di;
    .locals 4

    .prologue
    .line 270452
    iget-object v0, p0, LX/1X5;->a:LX/1De;

    .line 270453
    iget v1, p0, LX/1X5;->b:I

    .line 270454
    iget v2, p0, LX/1X5;->c:I

    .line 270455
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-static {v0, v3, v1, v2}, LX/1n9;->a(LX/1De;LX/1X1;II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public abstract d()LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<T",
            "L;",
            ">;"
        }
    .end annotation
.end method
