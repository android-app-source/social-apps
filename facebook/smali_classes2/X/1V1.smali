.class public LX/1V1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1XB;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1n2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 256982
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1V1;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1n2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256983
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 256984
    iput-object p1, p0, LX/1V1;->b:LX/0Ot;

    .line 256985
    return-void
.end method

.method public static a(LX/0QB;)LX/1V1;
    .locals 4

    .prologue
    .line 256986
    const-class v1, LX/1V1;

    monitor-enter v1

    .line 256987
    :try_start_0
    sget-object v0, LX/1V1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 256988
    sput-object v2, LX/1V1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256989
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256990
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 256991
    new-instance v3, LX/1V1;

    const/16 p0, 0x6fc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1V1;-><init>(LX/0Ot;)V

    .line 256992
    move-object v0, v3

    .line 256993
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 256994
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256995
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 256996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 256997
    check-cast p2, LX/1XA;

    .line 256998
    iget-object v0, p0, LX/1V1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1n2;

    iget-object v2, p2, LX/1XA;->a:LX/1X1;

    iget-object v3, p2, LX/1XA;->b:LX/1X9;

    iget v4, p2, LX/1XA;->c:I

    iget-object v5, p2, LX/1XA;->d:LX/1X6;

    iget-boolean v6, p2, LX/1XA;->e:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/1n2;->a(LX/1De;LX/1X1;LX/1X9;ILX/1X6;Z)LX/1Dg;

    move-result-object v0

    .line 256999
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257000
    invoke-static {}, LX/1dS;->b()V

    .line 257001
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1XB;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 257002
    new-instance v1, LX/1XA;

    invoke-direct {v1, p0}, LX/1XA;-><init>(LX/1V1;)V

    .line 257003
    sget-object v2, LX/1V1;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1XB;

    .line 257004
    if-nez v2, :cond_0

    .line 257005
    new-instance v2, LX/1XB;

    invoke-direct {v2}, LX/1XB;-><init>()V

    .line 257006
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1XB;->a$redex0(LX/1XB;LX/1De;IILX/1XA;)V

    .line 257007
    move-object v1, v2

    .line 257008
    move-object v0, v1

    .line 257009
    return-object v0
.end method
