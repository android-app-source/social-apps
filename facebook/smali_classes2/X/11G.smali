.class public final LX/11G;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Z

.field public static d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171420
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 171421
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/11G;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 171422
    sput-boolean v1, LX/11G;->c:Z

    .line 171423
    sput-boolean v1, LX/11G;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171419
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 171388
    sget-boolean v0, LX/11G;->c:Z

    if-nez v0, :cond_0

    .line 171389
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, LX/0lF;

    const-class v2, LX/0lF;

    invoke-static {v2}, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171390
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/String;

    sget-object v2, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171391
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Integer;

    const-class v2, Ljava/lang/Integer;

    const-class v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/163;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171392
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Long;

    const-class v2, Ljava/lang/Long;

    const-class v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/163;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171393
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Boolean;

    const-class v2, Ljava/lang/Boolean;

    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/163;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171394
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Float;

    const-class v2, Ljava/lang/Float;

    const-class v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/163;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171395
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Double;

    const-class v2, Ljava/lang/Double;

    const-class v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/163;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171396
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Landroid/net/Uri;

    new-instance v2, Lcom/facebook/common/json/UriDeserializer;

    invoke-direct {v2}, Lcom/facebook/common/json/UriDeserializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171397
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, LX/0nW;

    sget-object v2, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$TokenBufferDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$TokenBufferDeserializer;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171398
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Object;

    sget-object v2, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171399
    const/4 v0, 0x1

    sput-boolean v0, LX/11G;->c:Z

    .line 171400
    :cond_0
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 171401
    if-eqz v0, :cond_1

    .line 171402
    :goto_0
    return-object v0

    .line 171403
    :cond_1
    const/4 v0, 0x0

    .line 171404
    sget-object v1, LX/11G;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171405
    :goto_1
    move v0, v0

    .line 171406
    if-eqz v0, :cond_2

    .line 171407
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    goto :goto_0

    .line 171408
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 171409
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x24

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Deserializer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171410
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "$Deserializer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171411
    sget-boolean v3, LX/11G;->d:Z

    if-eqz v3, :cond_4

    .line 171412
    invoke-static {v2}, LX/11G;->a(Ljava/lang/String;)Z

    move-result v0

    .line 171413
    :cond_4
    if-nez v0, :cond_5

    .line 171414
    invoke-static {v1}, LX/11G;->a(Ljava/lang/String;)Z

    move-result v0

    .line 171415
    :cond_5
    if-nez v0, :cond_6

    sget-boolean v1, LX/11G;->d:Z

    if-nez v1, :cond_6

    .line 171416
    invoke-static {v2}, LX/11G;->a(Ljava/lang/String;)Z

    move-result v0

    .line 171417
    :cond_6
    sget-object v1, LX/11G;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, p0, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 171386
    sget-object v0, LX/11G;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, p1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171387
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 171383
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2

    .line 171384
    const/4 v0, 0x1

    .line 171385
    :goto_0
    return v0

    :catch_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    goto :goto_1

    :catch_2
    goto :goto_1
.end method
