.class public LX/1Ea;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1Ea;


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Eb;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Or;LX/1Eb;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/AdvancedUploadAutoRetryPolicy;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1Eb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220652
    iput-object p1, p0, LX/1Ea;->a:LX/0SG;

    .line 220653
    iput-object p2, p0, LX/1Ea;->b:LX/0Or;

    .line 220654
    iput-object p3, p0, LX/1Ea;->c:LX/1Eb;

    .line 220655
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ea;
    .locals 7

    .prologue
    .line 220635
    sget-object v0, LX/1Ea;->d:LX/1Ea;

    if-nez v0, :cond_1

    .line 220636
    const-class v1, LX/1Ea;

    monitor-enter v1

    .line 220637
    :try_start_0
    sget-object v0, LX/1Ea;->d:LX/1Ea;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 220638
    if-eqz v2, :cond_0

    .line 220639
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 220640
    new-instance v5, LX/1Ea;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 v4, 0x343

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 220641
    new-instance p0, LX/1Eb;

    invoke-static {v0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v4

    check-cast v4, LX/0u7;

    invoke-direct {p0, v4}, LX/1Eb;-><init>(LX/0u7;)V

    .line 220642
    move-object v4, p0

    .line 220643
    check-cast v4, LX/1Eb;

    invoke-direct {v5, v3, v6, v4}, LX/1Ea;-><init>(LX/0SG;LX/0Or;LX/1Eb;)V

    .line 220644
    move-object v0, v5

    .line 220645
    sput-object v0, LX/1Ea;->d:LX/1Ea;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220646
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 220647
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 220648
    :cond_1
    sget-object v0, LX/1Ea;->d:LX/1Ea;

    return-object v0

    .line 220649
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 220650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/LinkedList;J)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;J)",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 220611
    const/4 v1, 0x0

    .line 220612
    const-wide v4, 0x7fffffffffffffffL

    .line 220613
    invoke-virtual {p0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220614
    invoke-static {v0, p1, p2}, LX/1Ea;->b(Lcom/facebook/photos/upload/operation/UploadOperation;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220615
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    move-object v2, v2

    .line 220616
    iget-wide v10, v2, LX/8LU;->c:J

    move-wide v2, v10

    .line 220617
    cmp-long v7, v2, v4

    if-gez v7, :cond_1

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    :goto_1
    move-wide v4, v0

    move-object v1, v2

    .line 220618
    goto :goto_0

    .line 220619
    :cond_0
    return-object v1

    :cond_1
    move-object v2, v1

    move-wide v0, v4

    goto :goto_1
.end method

.method public static b(Lcom/facebook/photos/upload/operation/UploadOperation;J)Z
    .locals 7

    .prologue
    .line 220624
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    move-object v0, v0

    .line 220625
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220626
    iget-boolean v1, v0, LX/8LU;->j:Z

    move v1, v1

    .line 220627
    if-eqz v1, :cond_0

    .line 220628
    iget v1, v0, LX/8LU;->g:I

    move v1, v1

    .line 220629
    const/16 v2, 0x12c

    if-ge v1, v2, :cond_0

    .line 220630
    iget v1, v0, LX/8LU;->i:I

    move v0, v1

    .line 220631
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 220632
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    move-object v0, v0

    .line 220633
    iget-wide v5, v0, LX/8LU;->b:J

    move-wide v0, v5

    .line 220634
    const-wide/32 v2, 0x2932e00

    add-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1EZ;)V
    .locals 4

    .prologue
    .line 220621
    iget-object v0, p0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v0}, LX/1Eb;->a()V

    .line 220622
    iget-object v0, p0, LX/1Ea;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, LX/1EZ;->a(J)V

    .line 220623
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 220620
    iget-object v0, p0, LX/1Ea;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
