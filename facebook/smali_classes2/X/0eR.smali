.class public LX/0eR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Wx;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/33J;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0eS;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91954
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/mobileconfig/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0eR;->a:Ljava/lang/String;

    .line 91955
    iput-object p2, p0, LX/0eR;->b:Ljava/lang/String;

    .line 91956
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0eR;->c:Ljava/util/List;

    .line 91957
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0eR;->d:Ljava/util/List;

    .line 91958
    const/4 v0, 0x0

    iput-object v0, p0, LX/0eR;->e:LX/0eS;

    .line 91959
    return-void
.end method

.method public static h(LX/0eR;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 91938
    invoke-virtual {p0}, LX/0eR;->a()LX/0eT;

    move-result-object v1

    .line 91939
    if-nez v1, :cond_2

    .line 91940
    const-string v1, ""

    .line 91941
    :goto_0
    move-object v1, v1

    .line 91942
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91943
    :cond_0
    :goto_1
    return v0

    .line 91944
    :cond_1
    sget-object v2, LX/0eY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 91945
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    .line 91946
    :cond_2
    invoke-interface {v1}, LX/0eT;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 91947
    if-nez v1, :cond_3

    .line 91948
    const-string v1, ""

    goto :goto_0

    .line 91949
    :cond_3
    :try_start_0
    invoke-static {v1}, LX/0eV;->a(Ljava/nio/ByteBuffer;)LX/0eV;

    move-result-object v1

    .line 91950
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_4

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v2, p0

    invoke-virtual {v1, v2}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object v1, v2
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91951
    goto :goto_0

    .line 91952
    :catch_0
    const-string v1, ""

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/0eT;
    .locals 11

    .prologue
    .line 91918
    iget-object v0, p0, LX/0eR;->e:LX/0eS;

    if-nez v0, :cond_2

    .line 91919
    new-instance v0, LX/0eS;

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 91920
    iget-object v2, p0, LX/0eR;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0eR;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/0eR;->b:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 91921
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/0eR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sessionless.data/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91922
    :goto_0
    move-object v2, v2

    .line 91923
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91924
    new-instance v2, LX/0eU;

    invoke-direct {v2, p0}, LX/0eU;-><init>(LX/0eR;)V

    invoke-virtual {v3, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v6

    .line 91925
    if-nez v6, :cond_3

    .line 91926
    const-string v1, ""

    .line 91927
    :cond_1
    move-object v1, v1

    .line 91928
    invoke-direct {v0, v1}, LX/0eS;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0eR;->e:LX/0eS;

    .line 91929
    :cond_2
    iget-object v0, p0, LX/0eR;->e:LX/0eS;

    return-object v0

    .line 91930
    :cond_3
    const-string v2, ""

    .line 91931
    array-length v7, v6

    move v5, v1

    move v3, v4

    move-object v1, v2

    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v8, v6, v5

    .line 91932
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 91933
    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0xb

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 91934
    :goto_2
    if-le v2, v3, :cond_4

    .line 91935
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 91936
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 91937
    :catch_0
    move v2, v4

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/0eR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/0eR;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".data/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 91917
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 91916
    const/4 v0, 0x0

    return-object v0
.end method

.method public final clearCurrentUserData()V
    .locals 0

    .prologue
    .line 91915
    return-void
.end method

.method public final clearOverrides()V
    .locals 0

    .prologue
    .line 91914
    return-void
.end method

.method public final deleteOldUserData(I)V
    .locals 0

    .prologue
    .line 91913
    return-void
.end method

.method public final getFrameworkStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91912
    const-string v0, "UNINITIALIZED"

    return-object v0
.end method

.method public final getQEInfoFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91911
    const-string v0, ""

    return-object v0
.end method

.method public final isQEInfoAvailable()Z
    .locals 1

    .prologue
    .line 91960
    const/4 v0, 0x0

    return v0
.end method

.method public final isTigonServiceSet()Z
    .locals 1

    .prologue
    .line 91910
    const/4 v0, 0x0

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .prologue
    .line 91909
    const/4 v0, 0x1

    return v0
.end method

.method public final logExposure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91907
    iget-object v0, p0, LX/0eR;->c:Ljava/util/List;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91908
    return-void
.end method

.method public final logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 91905
    iget-object v7, p0, LX/0eR;->d:Ljava/util/List;

    new-instance v0, LX/33J;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/33J;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91906
    return-void
.end method

.method public final refreshConfigInfos(I)Z
    .locals 1

    .prologue
    .line 91904
    const/4 v0, 0x0

    return v0
.end method

.method public final registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
    .locals 1

    .prologue
    .line 91903
    const/4 v0, 0x0

    return v0
.end method

.method public final setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
    .locals 0

    .prologue
    .line 91902
    return-void
.end method

.method public final tryUpdateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 91901
    const/4 v0, 0x0

    return v0
.end method

.method public final updateConfigs()Z
    .locals 1

    .prologue
    .line 91900
    const/4 v0, 0x0

    return v0
.end method

.method public final updateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 91899
    const/4 v0, 0x0

    return v0
.end method
