.class public LX/0s6;
.super LX/01H;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0s6;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151226
    invoke-direct {p0, p1, p2}, LX/01H;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 151227
    return-void
.end method

.method public static a(LX/0QB;)LX/0s6;
    .locals 5

    .prologue
    .line 151228
    sget-object v0, LX/0s6;->a:LX/0s6;

    if-nez v0, :cond_1

    .line 151229
    const-class v1, LX/0s6;

    monitor-enter v1

    .line 151230
    :try_start_0
    sget-object v0, LX/0s6;->a:LX/0s6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151231
    if-eqz v2, :cond_0

    .line 151232
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151233
    new-instance p0, LX/0s6;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0s7;->b(LX/0QB;)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0, v3, v4}, LX/0s6;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 151234
    move-object v0, p0

    .line 151235
    sput-object v0, LX/0s6;->a:LX/0s6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151236
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151237
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151238
    :cond_1
    sget-object v0, LX/0s6;->a:LX/0s6;

    return-object v0

    .line 151239
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151240
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
