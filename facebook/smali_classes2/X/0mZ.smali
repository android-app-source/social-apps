.class public final LX/0mZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "LX/0mj;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0ma;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0mR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0mb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0mc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0mI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0mI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0mM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0mP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/0mO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0mg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0mg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/0me;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/0mU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/40D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 132685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132686
    if-nez p1, :cond_0

    .line 132687
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132688
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0mZ;->a:Landroid/content/Context;

    .line 132689
    return-void
.end method


# virtual methods
.method public final a(LX/0mI;)LX/0mZ;
    .locals 0

    .prologue
    .line 132677
    iput-object p1, p0, LX/0mZ;->g:LX/0mI;

    .line 132678
    return-object p0
.end method

.method public final a(LX/0mM;)LX/0mZ;
    .locals 0

    .prologue
    .line 132679
    iput-object p1, p0, LX/0mZ;->j:LX/0mM;

    .line 132680
    return-object p0
.end method

.method public final a(LX/0mO;)LX/0mZ;
    .locals 0

    .prologue
    .line 132681
    iput-object p1, p0, LX/0mZ;->l:LX/0mO;

    .line 132682
    return-object p0
.end method

.method public final a(LX/0mP;)LX/0mZ;
    .locals 0

    .prologue
    .line 132692
    iput-object p1, p0, LX/0mZ;->k:LX/0mP;

    .line 132693
    return-object p0
.end method

.method public final a(LX/0mR;)LX/0mZ;
    .locals 0

    .prologue
    .line 132683
    iput-object p1, p0, LX/0mZ;->d:LX/0mR;

    .line 132684
    return-object p0
.end method

.method public final a(LX/0mU;)LX/0mZ;
    .locals 0

    .prologue
    .line 132690
    iput-object p1, p0, LX/0mZ;->r:LX/0mU;

    .line 132691
    return-object p0
.end method

.method public final a(LX/0ma;)LX/0mZ;
    .locals 0

    .prologue
    .line 132673
    iput-object p1, p0, LX/0mZ;->c:LX/0ma;

    .line 132674
    return-object p0
.end method

.method public final a(LX/0mb;)LX/0mZ;
    .locals 0

    .prologue
    .line 132675
    iput-object p1, p0, LX/0mZ;->e:LX/0mb;

    .line 132676
    return-object p0
.end method

.method public final a(LX/0mc;)LX/0mZ;
    .locals 0

    .prologue
    .line 132656
    iput-object p1, p0, LX/0mZ;->f:LX/0mc;

    .line 132657
    return-object p0
.end method

.method public final a(LX/0me;)LX/0mZ;
    .locals 0

    .prologue
    .line 132658
    iput-object p1, p0, LX/0mZ;->q:LX/0me;

    .line 132659
    return-object p0
.end method

.method public final a(LX/0mg;)LX/0mZ;
    .locals 0

    .prologue
    .line 132660
    iput-object p1, p0, LX/0mZ;->o:LX/0mg;

    .line 132661
    return-object p0
.end method

.method public final a(LX/40D;)LX/0mZ;
    .locals 0

    .prologue
    .line 132671
    iput-object p1, p0, LX/0mZ;->s:LX/40D;

    .line 132672
    return-object p0
.end method

.method public final a(Ljava/lang/Class;)LX/0mZ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;)",
            "LX/0mZ;"
        }
    .end annotation

    .prologue
    .line 132662
    iput-object p1, p0, LX/0mZ;->i:Ljava/lang/Class;

    .line 132663
    return-object p0
.end method

.method public final a()LX/0mh;
    .locals 1

    .prologue
    .line 132664
    new-instance v0, LX/0mh;

    invoke-direct {v0, p0}, LX/0mh;-><init>(LX/0mZ;)V

    return-object v0
.end method

.method public final b(LX/0mI;)LX/0mZ;
    .locals 0

    .prologue
    .line 132665
    iput-object p1, p0, LX/0mZ;->h:LX/0mI;

    .line 132666
    return-object p0
.end method

.method public final b(Ljava/lang/Class;)LX/0mZ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;)",
            "LX/0mZ;"
        }
    .end annotation

    .prologue
    .line 132667
    iput-object p1, p0, LX/0mZ;->m:Ljava/lang/Class;

    .line 132668
    return-object p0
.end method

.method public final c(Ljava/lang/Class;)LX/0mZ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;)",
            "LX/0mZ;"
        }
    .end annotation

    .prologue
    .line 132669
    iput-object p1, p0, LX/0mZ;->n:Ljava/lang/Class;

    .line 132670
    return-object p0
.end method
