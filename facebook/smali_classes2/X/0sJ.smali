.class public LX/0sJ;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0sK;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0sK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151603
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0sK;
    .locals 4

    .prologue
    .line 151605
    sget-object v0, LX/0sJ;->a:LX/0sK;

    if-nez v0, :cond_1

    .line 151606
    const-class v1, LX/0sJ;

    monitor-enter v1

    .line 151607
    :try_start_0
    sget-object v0, LX/0sJ;->a:LX/0sK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151608
    if-eqz v2, :cond_0

    .line 151609
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151610
    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v3}, LX/0sK;->a(Landroid/content/Context;)LX/0sK;

    move-result-object v3

    move-object v0, v3

    .line 151611
    sput-object v0, LX/0sJ;->a:LX/0sK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151612
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151613
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151614
    :cond_1
    sget-object v0, LX/0sJ;->a:LX/0sK;

    return-object v0

    .line 151615
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 151604
    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/0sK;->a(Landroid/content/Context;)LX/0sK;

    move-result-object v0

    return-object v0
.end method
