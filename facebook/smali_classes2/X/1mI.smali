.class public LX/1mI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 313479
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "video/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 313480
    sput-object v0, LX/1mI;->a:LX/0Tn;

    const-string v1, "fullscreen_seek"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->b:LX/0Tn;

    .line 313481
    sget-object v0, LX/1mI;->a:LX/0Tn;

    const-string v1, "fullscreen_userpaused"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->c:LX/0Tn;

    .line 313482
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_all_connections_option"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->d:LX/0Tn;

    .line 313483
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_wifi_only_option"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->e:LX/0Tn;

    .line 313484
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_no_autoplay_option"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->f:LX/0Tn;

    .line 313485
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_eligibility"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->g:LX/0Tn;

    .line 313486
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "previous_autoplay_eligibility"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->h:LX/0Tn;

    .line 313487
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_has_user_touched_setting"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->i:LX/0Tn;

    .line 313488
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "autoplay_policy_version_updated"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->j:LX/0Tn;

    .line 313489
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "subtitle_preferred_locale"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1mI;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313491
    return-void
.end method
