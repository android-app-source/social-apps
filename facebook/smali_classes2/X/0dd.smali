.class public LX/0dd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90627
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "messages/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->a:LX/0Tn;

    .line 90628
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "messages/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90629
    sput-object v0, LX/0dd;->b:LX/0Tn;

    const-string v1, "notifications/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90630
    sput-object v0, LX/0dd;->c:LX/0Tn;

    const-string v1, "chat_heads_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->d:LX/0Tn;

    .line 90631
    sget-object v0, LX/0dd;->c:LX/0Tn;

    const-string v1, "primary_chat_heads_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->e:LX/0Tn;

    .line 90632
    sget-object v0, LX/0dd;->a:LX/0Tn;

    const-string v1, "notifications/chat_heads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90633
    sput-object v0, LX/0dd;->f:LX/0Tn;

    const-string v1, "/dock_x_percentage"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->g:LX/0Tn;

    .line 90634
    sget-object v0, LX/0dd;->f:LX/0Tn;

    const-string v1, "/dock_y_percentage"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->h:LX/0Tn;

    .line 90635
    sget-object v0, LX/0dd;->f:LX/0Tn;

    const-string v1, "/has_chat_head_settings_been_reported"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->i:LX/0Tn;

    .line 90636
    sget-object v0, LX/0dd;->f:LX/0Tn;

    const-string v1, "/should_present_accessibility_hint"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->j:LX/0Tn;

    .line 90637
    sget-object v0, LX/0dd;->f:LX/0Tn;

    const-string v1, "/debug_shading_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dd;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
