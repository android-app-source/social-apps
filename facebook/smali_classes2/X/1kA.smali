.class public LX/1kA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R0;
.implements LX/1R6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R0",
        "<TP;TE;>;",
        "LX/1R6;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field public d:I

.field private final e:LX/1R0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R0",
            "<TP;TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/1RA",
            "<TP;TE;>;>;"
        }
    .end annotation
.end field

.field public final g:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final h:LX/0Zb;

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(LX/1R0;LX/1PW;LX/0Zb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1R0",
            "<TP;TE;>;TE;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 309005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309006
    iput v0, p0, LX/1kA;->a:I

    .line 309007
    iput v0, p0, LX/1kA;->b:I

    .line 309008
    iput v0, p0, LX/1kA;->c:I

    .line 309009
    iput v0, p0, LX/1kA;->d:I

    .line 309010
    iput-boolean v0, p0, LX/1kA;->i:Z

    .line 309011
    iput-boolean v0, p0, LX/1kA;->j:Z

    .line 309012
    iput-object p1, p0, LX/1kA;->e:LX/1R0;

    .line 309013
    iput-object p3, p0, LX/1kA;->h:LX/0Zb;

    .line 309014
    new-instance v0, LX/0aq;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1kA;->f:LX/0aq;

    .line 309015
    iput-object p2, p0, LX/1kA;->g:LX/1PW;

    .line 309016
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 309017
    :try_start_0
    invoke-static {p0}, LX/1YB;->a(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 309018
    :cond_0
    :goto_0
    return-object p1

    .line 309019
    :catch_0
    if-eqz p0, :cond_0

    .line 309020
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static e(LX/1kA;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 309021
    iget-object v0, p0, LX/1kA;->h:LX/0Zb;

    const-string v1, "android_fresh_feed_cached_adapter_stats"

    invoke-interface {v0, v1, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 309022
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309023
    const-string v1, "get_count"

    iget v2, p0, LX/1kA;->d:I

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 309024
    const-string v1, "hit_count"

    iget v2, p0, LX/1kA;->a:I

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 309025
    const-string v1, "miss_count"

    iget v2, p0, LX/1kA;->b:I

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 309026
    const-string v1, "cached_count"

    iget v2, p0, LX/1kA;->c:I

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 309027
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 309028
    :cond_0
    iput v3, p0, LX/1kA;->a:I

    .line 309029
    iput v3, p0, LX/1kA;->b:I

    .line 309030
    iput v3, p0, LX/1kA;->c:I

    .line 309031
    iput v3, p0, LX/1kA;->d:I

    .line 309032
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1PW;)LX/1RA;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TE;)",
            "LX/1RA",
            "<TP;TE;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 309033
    iget v1, p0, LX/1kA;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1kA;->d:I

    .line 309034
    invoke-static {p1, v0}, LX/1kA;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 309035
    if-eqz v1, :cond_1

    iget-object v2, p0, LX/1kA;->g:LX/1PW;

    if-ne v2, p2, :cond_1

    .line 309036
    iget-object v0, p0, LX/1kA;->f:LX/0aq;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 309037
    if-eqz p1, :cond_1

    .line 309038
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 309039
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 309040
    iget-object v2, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 309041
    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    iget-boolean v3, p0, LX/1kA;->j:Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 309042
    if-ne v1, v2, :cond_4

    .line 309043
    :cond_0
    :goto_0
    move v1, v4

    .line 309044
    if-eqz v1, :cond_1

    .line 309045
    :try_start_0
    const-string v0, "CachingFeedUnitAdapterFactory.create[hasChanged]"

    const v1, -0x6fe50436

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 309046
    invoke-virtual {p0, p1}, LX/1kA;->b(Ljava/lang/Object;)LX/1RA;

    move-result-object v0

    .line 309047
    iget v1, p0, LX/1kA;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1kA;->b:I

    .line 309048
    iget v1, p0, LX/1kA;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/1kA;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309049
    const v1, -0x4f3a0249

    invoke-static {v1}, LX/02m;->a(I)V

    .line 309050
    :cond_1
    if-nez v0, :cond_3

    .line 309051
    :try_start_1
    const-string v0, "CachingFeedUnitAdapterFactory.create[cacheMiss]"

    const v1, 0x5fda58b4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 309052
    iget-object v0, p0, LX/1kA;->e:LX/1R0;

    invoke-interface {v0, p1, p2}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v0

    .line 309053
    iget v1, p0, LX/1kA;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1kA;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 309054
    const v1, -0x5a7cdf39

    invoke-static {v1}, LX/02m;->a(I)V

    .line 309055
    :goto_1
    iget v1, p0, LX/1kA;->d:I

    const/16 v2, 0xa

    if-lt v1, v2, :cond_2

    .line 309056
    invoke-static {p0}, LX/1kA;->e(LX/1kA;)V

    .line 309057
    :cond_2
    return-object v0

    .line 309058
    :catchall_0
    move-exception v0

    const v1, 0x74ca4d44

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 309059
    :catchall_1
    move-exception v0

    const v1, 0x7af2af56

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 309060
    :cond_3
    :try_start_2
    const-string v1, "CachingFeedUnitAdapterFactory.create[cacheHit]"

    const v2, -0x2f86ea44

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 309061
    iget v1, p0, LX/1kA;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1kA;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 309062
    const v1, -0x56232080

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    :catchall_2
    move-exception v0

    const v1, -0x301e3d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 309063
    :cond_4
    if-eqz v3, :cond_8

    .line 309064
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v6

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v7

    if-eq v6, v7, :cond_5

    move v4, v5

    .line 309065
    goto/16 :goto_0

    .line 309066
    :cond_5
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 309067
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v6

    invoke-virtual {v6}, LX/15i;->d()Z

    move-result v6

    if-eqz v6, :cond_6

    move v4, v5

    .line 309068
    goto/16 :goto_0

    .line 309069
    :cond_6
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v6

    invoke-virtual {v6}, LX/15i;->b()Z

    move-result v6

    if-eqz v6, :cond_7

    move v4, v5

    .line 309070
    goto/16 :goto_0

    .line 309071
    :cond_7
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v6

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 309072
    invoke-interface {v1}, LX/0jX;->d()LX/0Px;

    move-result-object v6

    invoke-interface {v2}, LX/0jX;->d()LX/0Px;

    move-result-object v7

    if-ne v6, v7, :cond_0

    :cond_8
    move v4, v5

    .line 309073
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 309074
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 309075
    if-eqz v0, :cond_0

    .line 309076
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1kA;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 309077
    if-eqz v0, :cond_0

    .line 309078
    iget-object v1, p0, LX/1kA;->f:LX/0aq;

    invoke-virtual {v1, v0}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309079
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/1RA;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)",
            "LX/1RA;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 309080
    iget-boolean v0, p0, LX/1kA;->i:Z

    if-nez v0, :cond_0

    .line 309081
    iget-object v0, p0, LX/1kA;->g:LX/1PW;

    instance-of v0, v0, LX/1Px;

    if-eqz v0, :cond_0

    .line 309082
    iget-object v0, p0, LX/1kA;->g:LX/1PW;

    check-cast v0, LX/1Px;

    .line 309083
    invoke-interface {v0, p0}, LX/1Px;->a(LX/1R6;)V

    .line 309084
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1kA;->i:Z

    .line 309085
    :cond_0
    invoke-static {p1, v1}, LX/1kA;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309086
    if-nez v2, :cond_1

    move-object v0, v1

    .line 309087
    :goto_0
    return-object v0

    .line 309088
    :cond_1
    :try_start_0
    const-string v0, "CachingFeedUnitAdapterFactory.createAndCacheFeedUnitAdapter"

    const v1, 0x4fae210c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 309089
    iget-object v0, p0, LX/1kA;->e:LX/1R0;

    iget-object v1, p0, LX/1kA;->g:LX/1PW;

    invoke-interface {v0, p1, v1}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v0

    .line 309090
    iget-object v1, p0, LX/1kA;->f:LX/0aq;

    invoke-virtual {v1, v2, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309091
    const v1, -0x5a63a3dc

    invoke-static {v1}, LX/02m;->a(I)V

    .line 309092
    iget v1, p0, LX/1kA;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1kA;->c:I

    goto :goto_0

    .line 309093
    :catchall_0
    move-exception v0

    const v1, 0x168ec1af

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c(Ljava/lang/Object;)LX/1RA;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)",
            "LX/1RA;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 309094
    invoke-static {p1, v0}, LX/1kA;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 309095
    if-nez v1, :cond_0

    .line 309096
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1kA;->f:LX/0aq;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    goto :goto_0
.end method
