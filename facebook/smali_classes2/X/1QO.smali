.class public LX/1QO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0gw;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x11

    const/16 v1, 0xe

    .line 244533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244534
    iput-object p1, p0, LX/1QO;->a:Landroid/content/Context;

    .line 244535
    invoke-virtual {p2}, LX/0gw;->a()Z

    move-result v3

    .line 244536
    if-eqz v3, :cond_0

    const/16 v0, 0x24

    :goto_0
    iput v0, p0, LX/1QO;->b:I

    .line 244537
    if-eqz v3, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, LX/1QO;->c:I

    .line 244538
    if-eqz v3, :cond_2

    move v0, v2

    :goto_2
    iput v0, p0, LX/1QO;->d:I

    .line 244539
    if-eqz v3, :cond_3

    :goto_3
    iput v2, p0, LX/1QO;->e:I

    .line 244540
    return-void

    .line 244541
    :cond_0
    const/16 v0, 0x1e

    goto :goto_0

    .line 244542
    :cond_1
    const/16 v0, 0xc

    goto :goto_1

    :cond_2
    move v0, v1

    .line 244543
    goto :goto_2

    :cond_3
    move v2, v1

    .line 244544
    goto :goto_3
.end method

.method private static b(Ljava/lang/String;)LX/0xr;
    .locals 1

    .prologue
    .line 244545
    const-string v0, "thin"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244546
    sget-object v0, LX/0xr;->THIN:LX/0xr;

    .line 244547
    :goto_0
    return-object v0

    .line 244548
    :cond_0
    const-string v0, "light"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244549
    sget-object v0, LX/0xr;->LIGHT:LX/0xr;

    goto :goto_0

    .line 244550
    :cond_1
    const-string v0, "normal"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "system-normal"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244551
    :cond_2
    sget-object v0, LX/0xr;->REGULAR:LX/0xr;

    goto :goto_0

    .line 244552
    :cond_3
    const-string v0, "medium"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "semi-bold"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 244553
    :cond_4
    sget-object v0, LX/0xr;->MEDIUM:LX/0xr;

    goto :goto_0

    .line 244554
    :cond_5
    const-string v0, "bold"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "extra-bold"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 244555
    :cond_6
    sget-object v0, LX/0xr;->BOLD:LX/0xr;

    goto :goto_0

    .line 244556
    :cond_7
    const-string v0, "black"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 244557
    sget-object v0, LX/0xr;->BLACK:LX/0xr;

    goto :goto_0

    .line 244558
    :cond_8
    sget-object v0, LX/0xr;->BUILTIN:LX/0xr;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1QO;
    .locals 3

    .prologue
    .line 244559
    new-instance v2, LX/1QO;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v1

    check-cast v1, LX/0gw;

    invoke-direct {v2, v0, v1}, LX/1QO;-><init>(Landroid/content/Context;LX/0gw;)V

    .line 244560
    return-object v2
.end method


# virtual methods
.method public createFeedFont(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)LX/5KJ;
    .locals 5
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 244561
    new-instance v1, LX/5KJ;

    iget-object v2, p0, LX/1QO;->a:Landroid/content/Context;

    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    if-nez p2, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v0}, LX/1QO;->b(Ljava/lang/String;)LX/0xr;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v2

    if-nez p1, :cond_1

    const-string v0, ""

    .line 244562
    :goto_1
    const-string v3, "large-font-message"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 244563
    iget v3, p0, LX/1QO;->b:I

    .line 244564
    :goto_2
    move v0, v3

    .line 244565
    invoke-direct {v1, v2, v0}, LX/5KJ;-><init>(Landroid/graphics/Typeface;I)V

    return-object v1

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 244566
    :cond_2
    const-string v3, "timestamp"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 244567
    iget v3, p0, LX/1QO;->c:I

    goto :goto_2

    .line 244568
    :cond_3
    const-string v3, "message"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 244569
    iget v3, p0, LX/1QO;->d:I

    goto :goto_2

    .line 244570
    :cond_4
    iget v3, p0, LX/1QO;->e:I

    goto :goto_2
.end method

.method public createFont(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)LX/5KJ;
    .locals 5
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 244571
    new-instance v1, LX/5KJ;

    iget-object v2, p0, LX/1QO;->a:Landroid/content/Context;

    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    if-nez p2, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v0}, LX/1QO;->b(Ljava/lang/String;)LX/0xr;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-direct {v1, v0, v2}, LX/5KJ;-><init>(Landroid/graphics/Typeface;I)V

    return-object v1

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
