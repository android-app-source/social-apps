.class public LX/11u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/12P;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/120;

.field public e:Landroid/view/ViewStub;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:LX/11v;

.field private h:LX/12S;

.field private i:LX/12I;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/11v;LX/120;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/11v;",
            "LX/120;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173397
    new-instance v0, LX/12P;

    invoke-direct {v0, p1}, LX/12P;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/11u;->a:LX/12P;

    .line 173398
    iput-object p2, p0, LX/11u;->c:LX/0Or;

    .line 173399
    iput-object p3, p0, LX/11u;->b:Lcom/facebook/content/SecureContextHelper;

    .line 173400
    iput-object p4, p0, LX/11u;->g:LX/11v;

    .line 173401
    iget-object v0, p0, LX/11u;->g:LX/11v;

    iget-object v1, p0, LX/11u;->a:LX/12P;

    .line 173402
    iput-object v1, v0, LX/11v;->o:LX/12Q;

    .line 173403
    new-instance v0, LX/12R;

    invoke-direct {v0, p0}, LX/12R;-><init>(LX/11u;)V

    iput-object v0, p0, LX/11u;->h:LX/12S;

    .line 173404
    iput-object p5, p0, LX/11u;->d:LX/120;

    .line 173405
    new-instance v0, LX/12T;

    invoke-direct {v0, p0}, LX/12T;-><init>(LX/11u;)V

    iput-object v0, p0, LX/11u;->i:LX/12I;

    .line 173406
    return-void
.end method

.method public static a(LX/0QB;)LX/11u;
    .locals 1

    .prologue
    .line 173479
    invoke-static {p0}, LX/11u;->b(LX/0QB;)LX/11u;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/11u;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 173475
    iget-object v0, p0, LX/11u;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 173476
    new-instance v0, LX/Bc7;

    invoke-direct {v0, p0, p1}, LX/Bc7;-><init>(LX/11u;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 173477
    :goto_0
    return-void

    .line 173478
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/11u;
    .locals 6

    .prologue
    .line 173473
    new-instance v0, LX/11u;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x2fd

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/11v;->b(LX/0QB;)LX/11v;

    move-result-object v4

    check-cast v4, LX/11v;

    invoke-static {p0}, LX/120;->a(LX/0QB;)LX/120;

    move-result-object v5

    check-cast v5, LX/120;

    invoke-direct/range {v0 .. v5}, LX/11u;-><init>(Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/11v;LX/120;)V

    .line 173474
    return-object v0
.end method

.method public static b$redex0(LX/11u;LX/120;)V
    .locals 1

    .prologue
    .line 173466
    invoke-virtual {p1}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173467
    if-nez v0, :cond_0

    .line 173468
    invoke-virtual {p1}, LX/120;->c()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173469
    invoke-static {p0, v0}, LX/11u;->a(LX/11u;Landroid/view/View;)V

    .line 173470
    invoke-virtual {p1}, LX/120;->g()V

    .line 173471
    :goto_0
    return-void

    .line 173472
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->c()V

    goto :goto_0
.end method

.method public static c(LX/120;)V
    .locals 1

    .prologue
    .line 173462
    invoke-virtual {p0}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173463
    if-nez v0, :cond_0

    .line 173464
    :goto_0
    return-void

    .line 173465
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b()V

    goto :goto_0
.end method

.method public static i(LX/11u;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173459
    iget-object v0, p0, LX/11u;->a:LX/12P;

    invoke-virtual {v0}, LX/12P;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/11u;->a:LX/12P;

    .line 173460
    invoke-virtual {v0}, LX/12P;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    move-object v0, p0

    .line 173461
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/11u;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173480
    iget-object v0, p0, LX/11u;->a:LX/12P;

    invoke-virtual {v0}, LX/12P;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/11u;->a:LX/12P;

    .line 173481
    iget-object p0, v0, LX/12P;->a:Ljava/lang/String;

    move-object v0, p0

    .line 173482
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/120;)V
    .locals 1

    .prologue
    .line 173454
    invoke-virtual {p1}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173455
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173456
    invoke-static {p0, p1}, LX/11u;->b$redex0(LX/11u;LX/120;)V

    .line 173457
    :goto_0
    return-void

    .line 173458
    :cond_0
    invoke-static {p1}, LX/11u;->c(LX/120;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 173428
    iget-object v0, p0, LX/11u;->a:LX/12P;

    invoke-virtual {v0}, LX/12P;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173429
    const/4 v4, 0x0

    .line 173430
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_3

    .line 173431
    iget-object v0, p0, LX/11u;->e:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 173432
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0, v0}, LX/11u;->a(LX/11u;Landroid/view/View;)V

    .line 173433
    iget-object v0, p0, LX/11u;->g:LX/11v;

    .line 173434
    iget-object v1, v0, LX/11v;->i:LX/0Uh;

    const/16 v2, 0x684

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 173435
    if-eqz v0, :cond_0

    .line 173436
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0a02cf

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 173437
    :cond_0
    :goto_0
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0}, LX/11u;->i(LX/11u;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 173438
    invoke-static {p0}, LX/11u;->j(LX/11u;)Ljava/lang/String;

    move-result-object v0

    .line 173439
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 173440
    iget-object v1, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020d7e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 173441
    iget-object v2, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getLineHeight()I

    move-result v2

    .line 173442
    invoke-virtual {v1, v4, v4, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 173443
    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 173444
    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 173445
    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 173446
    new-instance v3, Landroid/text/style/ImageSpan;

    invoke-direct {v3, v1, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 173447
    iget-object v1, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 173448
    iget-object v1, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/Bc6;

    invoke-direct {v2, p0, v0}, LX/Bc6;-><init>(LX/11u;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173449
    :cond_1
    :goto_1
    return-void

    .line 173450
    :cond_2
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_4

    .line 173451
    :goto_2
    goto :goto_1

    .line 173452
    :cond_3
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 173453
    :cond_4
    iget-object v0, p0, LX/11u;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final b(Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 173426
    iget-object v0, p0, LX/11u;->d:LX/120;

    invoke-virtual {v0, p1}, LX/120;->a(Landroid/view/ViewStub;)V

    .line 173427
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 173422
    iget-object v0, p0, LX/11u;->d:LX/120;

    iget-object v1, p0, LX/11u;->i:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->a(LX/12I;)V

    .line 173423
    iget-object v0, p0, LX/11u;->d:LX/120;

    invoke-virtual {v0}, LX/120;->a()V

    .line 173424
    iget-object v0, p0, LX/11u;->d:LX/120;

    invoke-virtual {p0, v0}, LX/11u;->a(LX/120;)V

    .line 173425
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 173413
    iget-object v0, p0, LX/11u;->g:LX/11v;

    iget-object v1, p0, LX/11u;->h:LX/12S;

    .line 173414
    iput-object v1, v0, LX/11v;->p:LX/12S;

    .line 173415
    move-object v0, v0

    .line 173416
    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    .line 173417
    iput-object v1, v0, LX/11v;->q:LX/0yY;

    .line 173418
    iget-object v0, p0, LX/11u;->g:LX/11v;

    invoke-virtual {v0}, LX/11v;->a()V

    .line 173419
    invoke-virtual {p0}, LX/11u;->b()V

    .line 173420
    invoke-virtual {p0}, LX/11u;->c()V

    .line 173421
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 173410
    iget-object v0, p0, LX/11u;->d:LX/120;

    iget-object v1, p0, LX/11u;->i:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->b(LX/12I;)V

    .line 173411
    iget-object v0, p0, LX/11u;->d:LX/120;

    invoke-virtual {v0}, LX/120;->b()V

    .line 173412
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 173407
    iget-object v0, p0, LX/11u;->g:LX/11v;

    invoke-virtual {v0}, LX/11v;->b()V

    .line 173408
    invoke-virtual {p0}, LX/11u;->e()V

    .line 173409
    return-void
.end method
