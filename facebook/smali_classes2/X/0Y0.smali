.class public LX/0Y0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0Y0;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79903
    iput-object p1, p0, LX/0Y0;->a:LX/0Ot;

    .line 79904
    iput-object p2, p0, LX/0Y0;->b:LX/0Ot;

    .line 79905
    return-void
.end method

.method public static a(LX/0QB;)LX/0Y0;
    .locals 5

    .prologue
    .line 79907
    sget-object v0, LX/0Y0;->c:LX/0Y0;

    if-nez v0, :cond_1

    .line 79908
    const-class v1, LX/0Y0;

    monitor-enter v1

    .line 79909
    :try_start_0
    sget-object v0, LX/0Y0;->c:LX/0Y0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79910
    if-eqz v2, :cond_0

    .line 79911
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79912
    new-instance v3, LX/0Y0;

    const/16 v4, 0x24e

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2ca

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0Y0;-><init>(LX/0Ot;LX/0Ot;)V

    .line 79913
    move-object v0, v3

    .line 79914
    sput-object v0, LX/0Y0;->c:LX/0Y0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79915
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79916
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79917
    :cond_1
    sget-object v0, LX/0Y0;->c:LX/0Y0;

    return-object v0

    .line 79918
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79919
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79906
    iget-object v0, p0, LX/0Y0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    invoke-virtual {v0}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
