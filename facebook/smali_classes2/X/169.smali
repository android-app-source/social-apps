.class public LX/169;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Random;


# instance fields
.field public b:J

.field public c:J

.field public d:Ljava/lang/String;

.field public e:[I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184612
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/169;->a:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 184603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184604
    const/4 v0, 0x1

    iput v0, p0, LX/169;->h:I

    .line 184605
    const/4 v0, 0x2

    iput v0, p0, LX/169;->h:I

    .line 184606
    sget-object v0, LX/169;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 184607
    iput-object v0, p0, LX/169;->d:Ljava/lang/String;

    .line 184608
    const/4 v0, -0x1

    iput v0, p0, LX/169;->f:I

    .line 184609
    const/4 v0, 0x0

    iput v0, p0, LX/169;->g:I

    .line 184610
    const/4 v0, 0x0

    iput-object v0, p0, LX/169;->e:[I

    .line 184611
    return-void
.end method

.method public static b(LX/169;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 10

    .prologue
    .line 184585
    iget-wide v0, p0, LX/169;->b:J

    sub-long v2, p1, v0

    .line 184586
    const/4 v0, 0x0

    .line 184587
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const-wide/16 v4, 0x40

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 184588
    :cond_0
    sget-object v0, LX/1rF;->USER_ACTION:LX/1rF;

    invoke-static {p0, p1, p2, v0}, LX/169;->c(LX/169;JLX/1rF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 184589
    :cond_1
    iget-object v1, p0, LX/169;->e:[I

    if-nez v1, :cond_3

    .line 184590
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 184591
    iput-wide p1, p0, LX/169;->c:J

    iput-wide p1, p0, LX/169;->b:J

    .line 184592
    iget v8, p0, LX/169;->h:I

    new-array v8, v8, [I

    iput-object v8, p0, LX/169;->e:[I

    .line 184593
    iget-object v8, p0, LX/169;->e:[I

    aput v7, v8, v9

    .line 184594
    :goto_0
    iget v8, p0, LX/169;->h:I

    if-ge v7, v8, :cond_2

    .line 184595
    iget-object v8, p0, LX/169;->e:[I

    aput v9, v8, v7

    .line 184596
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 184597
    :cond_2
    iget v7, p0, LX/169;->f:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, LX/169;->f:I

    .line 184598
    iget v7, p0, LX/169;->g:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, LX/169;->g:I

    .line 184599
    :goto_1
    return-object v0

    .line 184600
    :cond_3
    iget-object v1, p0, LX/169;->e:[I

    long-to-int v4, v2

    shr-int/lit8 v4, v4, 0x5

    aget v5, v1, v4

    const/4 v6, 0x1

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    shl-int v2, v6, v2

    or-int/2addr v2, v5

    aput v2, v1, v4

    .line 184601
    iput-wide p1, p0, LX/169;->c:J

    .line 184602
    iget v1, p0, LX/169;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/169;->g:I

    goto :goto_1
.end method

.method public static c(LX/169;JLX/1rF;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8

    .prologue
    .line 184572
    const-wide/16 v5, 0x1

    .line 184573
    iget-object v1, p0, LX/169;->e:[I

    if-nez v1, :cond_1

    .line 184574
    const/4 v1, 0x0

    .line 184575
    :cond_0
    :goto_0
    move-object v0, v1

    .line 184576
    const/4 v1, 0x0

    iput-object v1, p0, LX/169;->e:[I

    .line 184577
    const-wide/16 v1, 0x0

    iput-wide v1, p0, LX/169;->c:J

    .line 184578
    return-object v0

    .line 184579
    :cond_1
    iget-wide v1, p0, LX/169;->c:J

    cmp-long v1, p1, v1

    if-lez v1, :cond_2

    .line 184580
    const-wide/16 v1, 0x40

    iget-wide v3, p0, LX/169;->b:J

    sub-long v3, p1, v3

    add-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    .line 184581
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "time_spent_bit_array"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tos_id"

    iget-object v4, p0, LX/169;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "start_time"

    iget-wide v5, p0, LX/169;->b:J

    invoke-virtual {v2, v3, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "tos_array"

    iget-object v4, p0, LX/169;->e:[I

    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "tos_len"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "tos_seq"

    iget v3, p0, LX/169;->f:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "tos_cum"

    iget v3, p0, LX/169;->g:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 184582
    sget-object v2, LX/1rF;->CLOCK_CHANGE:LX/1rF;

    if-ne p3, v2, :cond_0

    .line 184583
    const-string v2, "trigger"

    const-string v3, "clock_change"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 184584
    :cond_2
    iget-wide v1, p0, LX/169;->c:J

    iget-wide v3, p0, LX/169;->b:J

    sub-long/2addr v1, v3

    add-long/2addr v1, v5

    long-to-int v1, v1

    goto :goto_1
.end method
