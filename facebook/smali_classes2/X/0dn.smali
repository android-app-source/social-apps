.class public LX/0dn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0dn;


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90889
    iput-object p1, p0, LX/0dn;->a:LX/0W3;

    .line 90890
    return-void
.end method

.method public static a(LX/0QB;)LX/0dn;
    .locals 4

    .prologue
    .line 90891
    sget-object v0, LX/0dn;->b:LX/0dn;

    if-nez v0, :cond_1

    .line 90892
    const-class v1, LX/0dn;

    monitor-enter v1

    .line 90893
    :try_start_0
    sget-object v0, LX/0dn;->b:LX/0dn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90894
    if-eqz v2, :cond_0

    .line 90895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90896
    new-instance p0, LX/0dn;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/0dn;-><init>(LX/0W3;)V

    .line 90897
    move-object v0, p0

    .line 90898
    sput-object v0, LX/0dn;->b:LX/0dn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90899
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90900
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90901
    :cond_1
    sget-object v0, LX/0dn;->b:LX/0dn;

    return-object v0

    .line 90902
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0dn;J)Z
    .locals 7

    .prologue
    .line 90904
    iget-object v0, p0, LX/0dn;->a:LX/0W3;

    invoke-interface {v0, p1, p2}, LX/0W4;->b(J)Z

    move-result v0

    .line 90905
    const-string v1, "true"

    iget-object v2, p0, LX/0dn;->a:LX/0W3;

    sget-wide v4, LX/0X5;->jb:J

    invoke-interface {v2, v4, v5}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90906
    iget-object v1, p0, LX/0dn;->a:LX/0W3;

    invoke-interface {v1, p1, p2}, LX/0W4;->i(J)V

    .line 90907
    :cond_0
    return v0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 90908
    sget-wide v0, LX/0X5;->jf:J

    invoke-static {p0, v0, v1}, LX/0dn;->a(LX/0dn;J)Z

    move-result v0

    return v0
.end method
