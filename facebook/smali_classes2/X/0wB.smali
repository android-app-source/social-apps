.class public final LX/0wB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/0wC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159491
    sget-object v0, LX/0wC;->NUMBER_1:LX/0wC;

    sput-object v0, LX/0wB;->a:LX/0wC;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 159490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()LX/0wC;
    .locals 1

    .prologue
    .line 159472
    invoke-static {}, LX/0wB;->e()I

    move-result v0

    invoke-static {v0}, LX/0wB;->a(I)LX/0wC;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)LX/0wC;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 159480
    const/16 v0, 0x1e0

    if-le p0, v0, :cond_0

    .line 159481
    sget-object v0, LX/0wC;->NUMBER_4:LX/0wC;

    .line 159482
    :goto_0
    return-object v0

    .line 159483
    :cond_0
    const/16 v0, 0x140

    if-le p0, v0, :cond_1

    .line 159484
    sget-object v0, LX/0wC;->NUMBER_3:LX/0wC;

    goto :goto_0

    .line 159485
    :cond_1
    const/16 v0, 0xf0

    if-le p0, v0, :cond_2

    .line 159486
    sget-object v0, LX/0wC;->NUMBER_2:LX/0wC;

    goto :goto_0

    .line 159487
    :cond_2
    const/16 v0, 0xa0

    if-le p0, v0, :cond_3

    .line 159488
    sget-object v0, LX/0wC;->NUMBER_1_5:LX/0wC;

    goto :goto_0

    .line 159489
    :cond_3
    sget-object v0, LX/0wC;->NUMBER_1:LX/0wC;

    goto :goto_0
.end method

.method public static final b()I
    .locals 1

    .prologue
    .line 159479
    const/16 v0, 0x28

    invoke-static {v0}, LX/0wB;->b(I)I

    move-result v0

    return v0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 159478
    invoke-static {}, LX/0wB;->e()I

    move-result v0

    mul-int/2addr v0, p0

    div-int/lit16 v0, v0, 0xa0

    return v0
.end method

.method public static final c()I
    .locals 1

    .prologue
    .line 159477
    const/16 v0, 0x5e

    invoke-static {v0}, LX/0wB;->b(I)I

    move-result v0

    return v0
.end method

.method public static final d()I
    .locals 1

    .prologue
    .line 159476
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method private static final e()I
    .locals 1

    .prologue
    .line 159473
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    if-nez v0, :cond_1

    .line 159474
    :cond_0
    const/4 v0, 0x0

    .line 159475
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    goto :goto_0
.end method
