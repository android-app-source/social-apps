.class public abstract LX/12o;
.super LX/0Rf;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rf",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175761
    invoke-direct {p0}, LX/0Rf;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 175751
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 175752
    check-cast p1, Ljava/util/Map$Entry;

    .line 175753
    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 175754
    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 175755
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 175760
    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->hashCode()I

    move-result v0

    return v0
.end method

.method public isHashCodeFast()Z
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "not used in GWT"
    .end annotation

    .prologue
    .line 175759
    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->isHashCodeFast()Z

    move-result v0

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 175758
    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 175757
    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    return v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "serialization"
    .end annotation

    .prologue
    .line 175756
    new-instance v0, LX/4yG;

    invoke-virtual {p0}, LX/12o;->a()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4yG;-><init>(LX/0P1;)V

    return-object v0
.end method
