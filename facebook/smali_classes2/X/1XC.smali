.class public final LX/1XC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1VH;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1EO;

.field public d:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic e:LX/1VH;


# direct methods
.method public constructor <init>(LX/1VH;)V
    .locals 1

    .prologue
    .line 270656
    iput-object p1, p0, LX/1XC;->e:LX/1VH;

    .line 270657
    move-object v0, p1

    .line 270658
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270659
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270660
    const-string v0, "FeedStoryMessageFlyoutComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270661
    if-ne p0, p1, :cond_1

    .line 270662
    :cond_0
    :goto_0
    return v0

    .line 270663
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270664
    goto :goto_0

    .line 270665
    :cond_3
    check-cast p1, LX/1XC;

    .line 270666
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270667
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270668
    if-eq v2, v3, :cond_0

    .line 270669
    iget-object v2, p0, LX/1XC;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1XC;->a:LX/1X1;

    iget-object v3, p1, LX/1XC;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270670
    goto :goto_0

    .line 270671
    :cond_5
    iget-object v2, p1, LX/1XC;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 270672
    :cond_6
    iget-object v2, p0, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 270673
    goto :goto_0

    .line 270674
    :cond_8
    iget-object v2, p1, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 270675
    :cond_9
    iget-object v2, p0, LX/1XC;->c:LX/1EO;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/1XC;->c:LX/1EO;

    iget-object v3, p1, LX/1XC;->c:LX/1EO;

    invoke-virtual {v2, v3}, LX/1EO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 270676
    goto :goto_0

    .line 270677
    :cond_b
    iget-object v2, p1, LX/1XC;->c:LX/1EO;

    if-nez v2, :cond_a

    .line 270678
    :cond_c
    iget-object v2, p0, LX/1XC;->d:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/1XC;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p1, LX/1XC;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 270679
    goto :goto_0

    .line 270680
    :cond_d
    iget-object v2, p1, LX/1XC;->d:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 270681
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1XC;

    .line 270682
    iget-object v1, v0, LX/1XC;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1XC;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/1XC;->a:LX/1X1;

    .line 270683
    return-object v0

    .line 270684
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
