.class public final LX/0vN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final c:[LX/0vO;

.field public static final d:LX/0vN;

.field public static final e:LX/0vN;


# instance fields
.field public final a:Z

.field public final b:[LX/0vO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157844
    new-array v0, v3, [LX/0vO;

    sput-object v0, LX/0vN;->c:[LX/0vO;

    .line 157845
    new-instance v0, LX/0vN;

    const/4 v1, 0x1

    sget-object v2, LX/0vN;->c:[LX/0vO;

    invoke-direct {v0, v1, v2}, LX/0vN;-><init>(Z[LX/0vO;)V

    sput-object v0, LX/0vN;->d:LX/0vN;

    .line 157846
    new-instance v0, LX/0vN;

    sget-object v1, LX/0vN;->c:[LX/0vO;

    invoke-direct {v0, v3, v1}, LX/0vN;-><init>(Z[LX/0vO;)V

    sput-object v0, LX/0vN;->e:LX/0vN;

    return-void
.end method

.method public constructor <init>(Z[LX/0vO;)V
    .locals 0

    .prologue
    .line 157847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157848
    iput-boolean p1, p0, LX/0vN;->a:Z

    .line 157849
    iput-object p2, p0, LX/0vN;->b:[LX/0vO;

    .line 157850
    return-void
.end method


# virtual methods
.method public final b(LX/0vO;)LX/0vN;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 157851
    iget-object v5, p0, LX/0vN;->b:[LX/0vO;

    .line 157852
    array-length v6, v5

    .line 157853
    const/4 v0, 0x1

    if-ne v6, v0, :cond_1

    aget-object v0, v5, v4

    if-ne v0, p1, :cond_1

    .line 157854
    sget-object p0, LX/0vN;->e:LX/0vN;

    .line 157855
    :cond_0
    :goto_0
    return-object p0

    .line 157856
    :cond_1
    if-eqz v6, :cond_0

    .line 157857
    add-int/lit8 v0, v6, -0x1

    new-array v2, v0, [LX/0vO;

    move v3, v4

    move v1, v4

    .line 157858
    :goto_1
    if-ge v3, v6, :cond_2

    .line 157859
    aget-object v7, v5, v3

    .line 157860
    if-eq v7, p1, :cond_5

    .line 157861
    add-int/lit8 v0, v6, -0x1

    if-eq v1, v0, :cond_0

    .line 157862
    add-int/lit8 v0, v1, 0x1

    aput-object v7, v2, v1

    .line 157863
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 157864
    :cond_2
    if-nez v1, :cond_3

    .line 157865
    sget-object p0, LX/0vN;->e:LX/0vN;

    goto :goto_0

    .line 157866
    :cond_3
    add-int/lit8 v0, v6, -0x1

    if-ge v1, v0, :cond_4

    .line 157867
    new-array v0, v1, [LX/0vO;

    .line 157868
    invoke-static {v2, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157869
    :goto_3
    new-instance v1, LX/0vN;

    iget-boolean v2, p0, LX/0vN;->a:Z

    invoke-direct {v1, v2, v0}, LX/0vN;-><init>(Z[LX/0vO;)V

    move-object p0, v1

    goto :goto_0

    :cond_4
    move-object v0, v2

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method
