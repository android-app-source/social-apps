.class public abstract LX/1M3;
.super LX/1M4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/1M4",
        "<",
        "Lcom/google/common/collect/Multiset$Entry",
        "<TE;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 234774
    invoke-direct {p0}, LX/1M4;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 234772
    invoke-virtual {p0}, LX/1M3;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->clear()V

    .line 234773
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 234758
    instance-of v1, p1, LX/4wx;

    if-eqz v1, :cond_0

    .line 234759
    check-cast p1, LX/4wx;

    .line 234760
    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v1

    if-gtz v1, :cond_1

    .line 234761
    :cond_0
    :goto_0
    return v0

    .line 234762
    :cond_1
    invoke-virtual {p0}, LX/1M3;->a()LX/1M1;

    move-result-object v1

    invoke-virtual {p1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v1

    .line 234763
    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 234764
    instance-of v1, p1, LX/4wx;

    if-eqz v1, :cond_0

    .line 234765
    check-cast p1, LX/4wx;

    .line 234766
    invoke-virtual {p1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v1

    .line 234767
    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v2

    .line 234768
    if-eqz v2, :cond_0

    .line 234769
    invoke-virtual {p0}, LX/1M3;->a()LX/1M1;

    move-result-object v3

    .line 234770
    invoke-interface {v3, v1, v2, v0}, LX/1M1;->a(Ljava/lang/Object;II)Z

    move-result v0

    .line 234771
    :cond_0
    return v0
.end method
