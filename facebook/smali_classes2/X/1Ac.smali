.class public LX/1Ac;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field public b:LX/0hB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:Landroid/view/View;

.field private d:Landroid/view/WindowManager;

.field public final e:Landroid/os/Handler;

.field public final f:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210779
    const-class v0, LX/1Ac;

    sput-object v0, LX/1Ac;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 210813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210814
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/1Ac;->e:Landroid/os/Handler;

    .line 210815
    new-instance v0, Lcom/facebook/delights/floating/DelightsPopup$1;

    invoke-direct {v0, p0}, Lcom/facebook/delights/floating/DelightsPopup$1;-><init>(LX/1Ac;)V

    iput-object v0, p0, LX/1Ac;->f:Ljava/lang/Runnable;

    .line 210816
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ac;
    .locals 4

    .prologue
    .line 210817
    const-class v1, LX/1Ac;

    monitor-enter v1

    .line 210818
    :try_start_0
    sget-object v0, LX/1Ac;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 210819
    sput-object v2, LX/1Ac;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210820
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210821
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 210822
    new-instance p0, LX/1Ac;

    invoke-direct {p0}, LX/1Ac;-><init>()V

    .line 210823
    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    .line 210824
    iput-object v3, p0, LX/1Ac;->b:LX/0hB;

    .line 210825
    move-object v0, p0

    .line 210826
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 210827
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Ac;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210828
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 210829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 210800
    iget-object v0, p0, LX/1Ac;->c:Landroid/view/View;

    const/4 v2, 0x0

    .line 210801
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    .line 210802
    :goto_0
    move v0, v1

    .line 210803
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    if-eqz v0, :cond_1

    .line 210804
    :try_start_0
    iget-object v0, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    iget-object v1, p0, LX/1Ac;->c:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210805
    :cond_1
    :goto_1
    iput-object v3, p0, LX/1Ac;->c:Landroid/view/View;

    .line 210806
    iput-object v3, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    .line 210807
    return-void

    .line 210808
    :catch_0
    move-exception v0

    .line 210809
    sget-object v1, LX/1Ac;->a:Ljava/lang/Class;

    const-string v2, "Exception while trying to remove the view from the window."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 210810
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 210811
    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 210812
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;J)V
    .locals 10

    .prologue
    .line 210780
    if-nez p1, :cond_0

    .line 210781
    :goto_0
    return-void

    .line 210782
    :cond_0
    invoke-virtual {p0}, LX/1Ac;->a()V

    .line 210783
    iget-object v0, p0, LX/1Ac;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/1Ac;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 210784
    iget-object v0, p0, LX/1Ac;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/1Ac;->f:Ljava/lang/Runnable;

    const v2, 0x266c5e4a

    invoke-static {v0, v1, p2, p3, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 210785
    iput-object p1, p0, LX/1Ac;->c:Landroid/view/View;

    .line 210786
    iget-object v0, p0, LX/1Ac;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    .line 210787
    iget-object v0, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    .line 210788
    iget-object v4, p0, LX/1Ac;->b:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->d()I

    move-result v4

    .line 210789
    iget-object v5, p0, LX/1Ac;->b:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v6

    .line 210790
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v7

    .line 210791
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 210792
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 210793
    const/4 v4, 0x1

    if-eq v7, v4, :cond_1

    const/4 v4, 0x3

    if-ne v7, v4, :cond_2

    :cond_1
    move v6, v5

    .line 210794
    :cond_2
    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    const/4 v7, 0x2

    const v8, 0x1000018

    const/4 v9, -0x3

    invoke-direct/range {v4 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 210795
    const/16 v5, 0x11

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 210796
    move-object v0, v4

    .line 210797
    :try_start_0
    iget-object v1, p0, LX/1Ac;->d:Landroid/view/WindowManager;

    iget-object v2, p0, LX/1Ac;->c:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 210798
    :catch_0
    move-exception v0

    .line 210799
    sget-object v1, LX/1Ac;->a:Ljava/lang/Class;

    const-string v2, "Exception while trying to add the view to the window manager."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
