.class public LX/0pY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pZ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0pY;


# instance fields
.field private final a:LX/0cb;

.field private final b:LX/0ZX;

.field private final c:LX/0pa;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0pb;


# direct methods
.method public constructor <init>(LX/0cb;LX/0ZX;LX/0pa;LX/0Or;LX/0pb;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cb;",
            "LX/0ZX;",
            "Lcom/facebook/abtest/qe/bootstrap/framework/QuickExperimentConfigRequestObserver;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0pb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144940
    iput-object p1, p0, LX/0pY;->a:LX/0cb;

    .line 144941
    iput-object p2, p0, LX/0pY;->b:LX/0ZX;

    .line 144942
    iput-object p3, p0, LX/0pY;->c:LX/0pa;

    .line 144943
    iput-object p4, p0, LX/0pY;->d:LX/0Or;

    .line 144944
    iput-object p5, p0, LX/0pY;->e:LX/0pb;

    .line 144945
    return-void
.end method

.method public static a(LX/0QB;)LX/0pY;
    .locals 9

    .prologue
    .line 144918
    sget-object v0, LX/0pY;->f:LX/0pY;

    if-nez v0, :cond_1

    .line 144919
    const-class v1, LX/0pY;

    monitor-enter v1

    .line 144920
    :try_start_0
    sget-object v0, LX/0pY;->f:LX/0pY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144921
    if-eqz v2, :cond_0

    .line 144922
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144923
    new-instance v3, LX/0pY;

    invoke-static {v0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v4

    check-cast v4, LX/0cb;

    invoke-static {v0}, LX/0ZX;->a(LX/0QB;)LX/0ZX;

    move-result-object v5

    check-cast v5, LX/0ZX;

    invoke-static {v0}, LX/0pa;->a(LX/0QB;)LX/0pa;

    move-result-object v6

    check-cast v6, LX/0pa;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0pb;->a(LX/0QB;)LX/0pb;

    move-result-object v8

    check-cast v8, LX/0pb;

    invoke-direct/range {v3 .. v8}, LX/0pY;-><init>(LX/0cb;LX/0ZX;LX/0pa;LX/0Or;LX/0pb;)V

    .line 144924
    move-object v0, v3

    .line 144925
    sput-object v0, LX/0pY;->f:LX/0pY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144926
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144927
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144928
    :cond_1
    sget-object v0, LX/0pY;->f:LX/0pY;

    return-object v0

    .line 144929
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2Ff;Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;",
            "Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;",
            ")TCONFIG;"
        }
    .end annotation

    .prologue
    .line 144931
    instance-of v0, p0, LX/2Fe;

    if-eqz v0, :cond_0

    .line 144932
    check-cast p0, LX/2Fe;

    .line 144933
    iget-object v0, p1, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->h:LX/2WT;

    move-object v0, v0

    .line 144934
    invoke-interface {p0, v0}, LX/2Fe;->a(LX/2WT;)Ljava/lang/Object;

    move-result-object v0

    .line 144935
    :goto_0
    return-object v0

    .line 144936
    :cond_0
    instance-of v0, p0, LX/3yC;

    if-eqz v0, :cond_1

    .line 144937
    check-cast p0, LX/3yC;

    invoke-interface {p0}, LX/3yC;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 144938
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be an instance of either DeprecatedQuickExperiment or NewQuickExperiment, but had signature: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d(LX/2Ff;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)",
            "Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 144910
    invoke-interface {p1}, LX/2Ff;->a()Ljava/lang/String;

    move-result-object v0

    .line 144911
    if-nez v0, :cond_0

    .line 144912
    new-instance v0, LX/2WQ;

    invoke-direct {v0}, LX/2WQ;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    const-string v1, "local_default_group"

    invoke-virtual {v0, v1}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v0

    invoke-static {}, LX/0pb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    .line 144913
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 144914
    iput-object v1, v0, LX/2WQ;->g:Ljava/util/Map;

    .line 144915
    move-object v0, v0

    .line 144916
    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    .line 144917
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/0pY;->a:LX/0cb;

    invoke-interface {v1, v0}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2Ff;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)TCONFIG;"
        }
    .end annotation

    .prologue
    .line 144906
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144907
    invoke-direct {p0, p1}, LX/0pY;->d(LX/2Ff;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    .line 144908
    invoke-static {p1, v0}, LX/0pY;->a(LX/2Ff;Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)Ljava/lang/Object;

    move-result-object v0

    .line 144909
    return-object v0
.end method

.method public final b(LX/2Ff;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)V"
        }
    .end annotation

    .prologue
    .line 144876
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144877
    iget-object v0, p0, LX/0pY;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/2Ff;->a()Ljava/lang/String;

    move-result-object v0

    .line 144878
    const-string v1, "sessionless__"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    move v0, v1

    .line 144879
    if-nez v0, :cond_0

    .line 144880
    const-string v0, "QuickExperimentControllerImpl"

    const-string v1, "Exposure of experiment %s occurred when no user was logged in"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144881
    :goto_0
    return-void

    .line 144882
    :cond_0
    invoke-direct {p0, p1}, LX/0pY;->d(LX/2Ff;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    .line 144883
    iget-object v1, p0, LX/0pY;->b:LX/0ZX;

    const-string v2, "QuickExperimentControllerImplLoggingContext"

    const/4 v3, 0x0

    .line 144884
    iget-boolean v4, v0, LX/2Wx;->c:Z

    move v4, v4

    .line 144885
    if-nez v4, :cond_2

    .line 144886
    :cond_1
    :goto_1
    goto :goto_0

    .line 144887
    :cond_2
    iget-object v4, v0, LX/2Wx;->a:Ljava/lang/String;

    move-object v4, v4

    .line 144888
    invoke-static {v1, v4}, LX/0ZX;->a(LX/0ZX;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 144889
    sget-object v4, LX/1gX;->EXPOSURE:LX/1gX;

    .line 144890
    sget-object v8, LX/0of;->MARAUDER:LX/0of;

    .line 144891
    const-string v5, "logging_channel"

    .line 144892
    iget-object v6, v0, LX/2Wx;->e:Ljava/lang/String;

    const-string v7, "local_default_group"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 144893
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    .line 144894
    :goto_2
    move-object v6, v6

    .line 144895
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 144896
    :try_start_0
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, LX/0of;->valueOfIgnoreCase(Ljava/lang/String;)LX/0of;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 144897
    :cond_3
    :goto_3
    iget-object v5, v0, LX/2Wx;->a:Ljava/lang/String;

    move-object v6, v5

    .line 144898
    iget-object v5, v0, LX/2Wx;->e:Ljava/lang/String;

    move-object v7, v5

    .line 144899
    move-object v5, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    invoke-static/range {v5 .. v11}, LX/0ZX;->a(LX/0ZX;Ljava/lang/String;Ljava/lang/String;LX/0of;Ljava/lang/String;LX/0lF;LX/1gX;)V

    .line 144900
    goto :goto_1

    .line 144901
    :catch_0
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    goto :goto_3

    .line 144902
    :cond_4
    iget-object v6, v0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    invoke-virtual {v6, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 144903
    if-nez v6, :cond_5

    .line 144904
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    goto :goto_2

    .line 144905
    :cond_5
    invoke-static {v6}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    goto :goto_2
.end method

.method public final c(LX/2Ff;)LX/3yG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)",
            "LX/3yG;"
        }
    .end annotation

    .prologue
    .line 144871
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144872
    invoke-direct {p0, p1}, LX/0pY;->d(LX/2Ff;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    .line 144873
    new-instance v1, LX/3yG;

    .line 144874
    iget-object p0, v0, LX/2Wx;->e:Ljava/lang/String;

    move-object v0, p0

    .line 144875
    invoke-direct {v1, v0}, LX/3yG;-><init>(Ljava/lang/String;)V

    return-object v1
.end method
