.class public final LX/0s8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0s9;


# instance fields
.field public final synthetic a:LX/0s0;


# direct methods
.method public constructor <init>(LX/0s0;)V
    .locals 0

    .prologue
    .line 151258
    iput-object p1, p0, LX/0s8;->a:LX/0s0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151260
    iget-object v1, p0, LX/0s8;->a:LX/0s0;

    iget-object v1, v1, LX/0s0;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151262
    const-string v0, "api"

    invoke-direct {p0, v0}, LX/0s8;->a(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151261
    const-string v0, "graph"

    invoke-direct {p0, v0}, LX/0s8;->a(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151256
    const-string v0, "graph-video"

    invoke-direct {p0, v0}, LX/0s8;->a(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151257
    const-string v0, "https://rupload.facebook.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151255
    const-string v0, "graph.secure"

    invoke-direct {p0, v0}, LX/0s8;->a(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151254
    const-string v0, "secure"

    invoke-direct {p0, v0}, LX/0s8;->a(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151252
    const-string v0, "http://h.%s"

    .line 151253
    iget-object v1, p0, LX/0s8;->a:LX/0s0;

    iget-object v1, v1, LX/0s0;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151251
    iget-object v0, p0, LX/0s8;->a:LX/0s0;

    iget-object v0, v0, LX/0s0;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/katana/net/HttpOperation;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151245
    iget-object v0, p0, LX/0s8;->a:LX/0s0;

    iget-object v0, v0, LX/0s0;->a:Landroid/content/Context;

    .line 151246
    invoke-static {v0}, LX/14o;->b(Landroid/content/Context;)LX/13t;

    move-result-object p0

    .line 151247
    if-eqz p0, :cond_0

    .line 151248
    invoke-virtual {p0}, LX/13t;->a()Ljava/lang/String;

    move-result-object p0

    .line 151249
    :goto_0
    move-object v0, p0

    .line 151250
    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
