.class public final LX/17G;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189571
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 189572
    sput-object v0, LX/17G;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 189573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Collection;)LX/0Px;
    .locals 1
    .param p0    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189574
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Iterator;)LX/0Px;
    .locals 1
    .param p0    # Ljava/util/Iterator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<TT;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189575
    if-nez p0, :cond_0

    sget-object v0, LX/17G;->a:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
