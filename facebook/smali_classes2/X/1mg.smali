.class public LX/1mg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/util/List",
            "<",
            "LX/48B;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/48B;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1X1;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 314007
    new-instance v0, LX/0Zi;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1mg;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 314008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314009
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1mg;->b:Ljava/util/Map;

    .line 314010
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1mg;->c:Ljava/util/Map;

    .line 314011
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/1mg;->d:Ljava/util/Set;

    .line 314012
    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/48B;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/48B;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314013
    sget-object v0, LX/1mg;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 314014
    if-nez v0, :cond_0

    .line 314015
    new-instance v1, Ljava/util/ArrayList;

    if-nez p0, :cond_2

    const/4 v0, 0x4

    :goto_0
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, v1

    .line 314016
    :cond_0
    if-eqz p0, :cond_1

    .line 314017
    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 314018
    :cond_1
    return-object v0

    .line 314019
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1X1;)V
    .locals 4

    .prologue
    .line 314020
    iget-object v0, p1, LX/1X1;->e:LX/1S3;

    move-object v1, v0

    .line 314021
    invoke-virtual {v1}, LX/1S3;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314022
    :goto_0
    return-void

    .line 314023
    :cond_0
    iget-object v0, p1, LX/1X1;->c:Ljava/lang/String;

    move-object v2, v0

    .line 314024
    iget-object v0, p0, LX/1mg;->c:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    .line 314025
    iget-object v3, p0, LX/1mg;->d:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 314026
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot set State for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", found another Component with the same key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314027
    :cond_1
    iget-object v3, p0, LX/1mg;->d:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 314028
    if-eqz v0, :cond_2

    .line 314029
    invoke-virtual {v1, v0, p1}, LX/1S3;->a(LX/1X1;LX/1X1;)V

    move-object v1, v0

    .line 314030
    :goto_1
    iget-object v0, p0, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 314031
    if-eqz v0, :cond_3

    .line 314032
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48B;

    .line 314033
    invoke-interface {v0, v1, p1}, LX/48B;->a(LX/1X1;LX/1X1;)V

    goto :goto_2

    .line 314034
    :cond_2
    iget-object v0, p1, LX/1X1;->f:LX/1De;

    move-object v0, v0

    .line 314035
    invoke-virtual {v1, v0, p1}, LX/1S3;->d(LX/1De;LX/1X1;)V

    move-object v1, p1

    .line 314036
    goto :goto_1

    .line 314037
    :cond_3
    iget-object v0, p0, LX/1mg;->c:Ljava/util/Map;

    .line 314038
    sget-boolean v1, LX/1V5;->j:Z

    if-eqz v1, :cond_4

    .line 314039
    invoke-virtual {p1}, LX/1X1;->f()LX/1X1;

    move-result-object p1

    .line 314040
    :cond_4
    move-object v1, p1

    .line 314041
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final c(LX/1mg;)V
    .locals 7

    .prologue
    .line 314042
    iget-object v0, p1, LX/1mg;->b:Ljava/util/Map;

    move-object v0, v0

    .line 314043
    if-nez v0, :cond_1

    .line 314044
    :cond_0
    iget-object v0, p1, LX/1mg;->c:Ljava/util/Map;

    move-object v0, v0

    .line 314045
    iget-object v1, p0, LX/1mg;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 314046
    return-void

    .line 314047
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 314048
    iget-object v2, p0, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 314049
    if-eqz v2, :cond_2

    .line 314050
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 314051
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ne v5, v6, :cond_3

    .line 314052
    iget-object v3, p0, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314053
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 314054
    sget-object v1, LX/1mg;->a:LX/0Zi;

    invoke-virtual {v1, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 314055
    goto :goto_0

    .line 314056
    :cond_3
    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
