.class public LX/1T1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R9;"
    }
.end annotation


# instance fields
.field public a:I

.field public final b:LX/1R2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R2",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Wd;

.field public final d:LX/0g8;

.field private e:LX/1UA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T1",
            "<TT;>.PrepareTask;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field public final j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>(LX/1R2;LX/0g8;LX/0Wd;)V
    .locals 2
    .param p1    # LX/1R2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1R2",
            "<TT;>;",
            "LX/0g8;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 249524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249525
    iput v0, p0, LX/1T1;->f:I

    .line 249526
    iput v1, p0, LX/1T1;->g:I

    .line 249527
    iput v0, p0, LX/1T1;->h:I

    .line 249528
    iput-boolean v1, p0, LX/1T1;->i:Z

    .line 249529
    new-instance v0, LX/1T2;

    invoke-direct {v0, p0}, LX/1T2;-><init>(LX/1T1;)V

    iput-object v0, p0, LX/1T1;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 249530
    iput-object p1, p0, LX/1T1;->b:LX/1R2;

    .line 249531
    iput-object p3, p0, LX/1T1;->c:LX/0Wd;

    .line 249532
    iput-object p2, p0, LX/1T1;->d:LX/0g8;

    .line 249533
    return-void
.end method

.method public static a(LX/1T1;I)V
    .locals 3

    .prologue
    .line 249511
    const/4 v0, 0x0

    .line 249512
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 249513
    invoke-static {p0}, LX/1T1;->h(LX/1T1;)I

    move-result v1

    .line 249514
    :goto_0
    if-nez v0, :cond_3

    iget v0, p0, LX/1T1;->g:I

    if-le v0, v1, :cond_0

    .line 249515
    iget v0, p0, LX/1T1;->g:I

    const/16 v2, 0x28

    if-ge v0, v2, :cond_4

    iget v0, p0, LX/1T1;->g:I

    iget-object v2, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v2}, LX/1R2;->b()I

    move-result v2

    if-ge v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 249516
    if-eqz v0, :cond_3

    .line 249517
    :cond_0
    iget v0, p0, LX/1T1;->g:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, LX/1T1;->g:I

    invoke-direct {p0, v0}, LX/1T1;->c(I)Z

    move-result v0

    goto :goto_0

    .line 249518
    :cond_1
    invoke-static {p0}, LX/1T1;->i$redex0(LX/1T1;)I

    move-result v1

    .line 249519
    :goto_2
    if-nez v0, :cond_3

    iget v0, p0, LX/1T1;->f:I

    if-ge v0, v1, :cond_2

    .line 249520
    iget v0, p0, LX/1T1;->f:I

    const/16 v2, 0x28

    if-ge v0, v2, :cond_5

    iget v0, p0, LX/1T1;->f:I

    if-ltz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 249521
    if-eqz v0, :cond_3

    .line 249522
    :cond_2
    iget v0, p0, LX/1T1;->f:I

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, LX/1T1;->f:I

    invoke-direct {p0, v0}, LX/1T1;->c(I)Z

    move-result v0

    goto :goto_2

    .line 249523
    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b$redex0(LX/1T1;I)V
    .locals 3

    .prologue
    .line 249503
    const/4 v0, 0x0

    .line 249504
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 249505
    iget-object v1, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v1}, LX/1R2;->b()I

    move-result v1

    .line 249506
    :goto_0
    if-nez v0, :cond_1

    iget v0, p0, LX/1T1;->g:I

    if-ge v0, v1, :cond_1

    .line 249507
    iget v0, p0, LX/1T1;->g:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, LX/1T1;->g:I

    invoke-direct {p0, v0}, LX/1T1;->d(I)Z

    move-result v0

    goto :goto_0

    .line 249508
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    iget v0, p0, LX/1T1;->f:I

    const/16 v1, 0x28

    if-lt v0, v1, :cond_1

    .line 249509
    iget v0, p0, LX/1T1;->f:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LX/1T1;->f:I

    invoke-direct {p0, v0}, LX/1T1;->d(I)Z

    move-result v0

    goto :goto_1

    .line 249510
    :cond_1
    return-void
.end method

.method private c(I)Z
    .locals 2

    .prologue
    .line 249497
    iget-object v0, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 249498
    const/4 v0, 0x0

    .line 249499
    :goto_0
    return v0

    .line 249500
    :cond_0
    iget-object v0, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->a(I)LX/1Rk;

    move-result-object v0

    .line 249501
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    move-object v1, v1

    .line 249502
    iget v0, v0, LX/1Rk;->b:I

    invoke-virtual {v1, v0}, LX/1RA;->f(I)Z

    move-result v0

    goto :goto_0
.end method

.method private d(I)Z
    .locals 2

    .prologue
    .line 249491
    iget-object v0, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 249492
    const/4 v0, 0x0

    .line 249493
    :goto_0
    return v0

    .line 249494
    :cond_0
    iget-object v0, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->a(I)LX/1Rk;

    move-result-object v0

    .line 249495
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    move-object v1, v1

    .line 249496
    iget v0, v0, LX/1Rk;->b:I

    invoke-virtual {v1, v0}, LX/1RA;->i(I)Z

    move-result v0

    goto :goto_0
.end method

.method public static d$redex0(LX/1T1;)V
    .locals 2

    .prologue
    .line 249534
    iget-object v0, p0, LX/1T1;->d:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/1T1;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 249535
    return-void
.end method

.method public static h(LX/1T1;)I
    .locals 2

    .prologue
    .line 249490
    iget-object v0, p0, LX/1T1;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/1T1;->h:I

    add-int/lit8 v1, v1, 0x78

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static i$redex0(LX/1T1;)I
    .locals 2

    .prologue
    .line 249489
    const/4 v0, 0x0

    iget v1, p0, LX/1T1;->h:I

    add-int/lit8 v1, v1, -0x78

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 249483
    invoke-virtual {p0}, LX/1T1;->b()V

    .line 249484
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1T1;->i:Z

    .line 249485
    invoke-static {p0}, LX/1T1;->d$redex0(LX/1T1;)V

    .line 249486
    new-instance v0, LX/1UA;

    iget-object v1, p0, LX/1T1;->c:LX/0Wd;

    invoke-direct {v0, p0, p0, v1}, LX/1UA;-><init>(LX/1T1;LX/1T1;Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, LX/1T1;->e:LX/1UA;

    .line 249487
    iget-object v0, p0, LX/1T1;->e:LX/1UA;

    invoke-virtual {v0}, LX/1Rm;->a()V

    .line 249488
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 249480
    iget-object v0, p0, LX/1T1;->e:LX/1UA;

    if-eqz v0, :cond_0

    .line 249481
    iget-object v0, p0, LX/1T1;->e:LX/1UA;

    invoke-virtual {v0}, LX/1Rm;->b()V

    .line 249482
    :cond_0
    return-void
.end method
