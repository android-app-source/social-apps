.class public final LX/1P6;
.super LX/1P5;
.source ""


# direct methods
.method public constructor <init>(LX/1OR;)V
    .locals 1

    .prologue
    .line 243611
    invoke-direct {p0, p1}, LX/1P5;-><init>(LX/1OR;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 243625
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 243626
    invoke-static {p1}, LX/1OR;->j(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 243623
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->h(I)V

    .line 243624
    return-void
.end method

.method public final b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 243621
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 243622
    invoke-static {p1}, LX/1OR;->l(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 243620
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->z()I

    move-result v0

    return v0
.end method

.method public final c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 243618
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 243619
    invoke-static {p1}, LX/1OR;->h(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 243617
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->x()I

    move-result v0

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->B()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 243615
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 243616
    invoke-static {p1}, LX/1OR;->g(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 243614
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->x()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 243613
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->x()I

    move-result v0

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->z()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->B()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 243612
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->B()I

    move-result v0

    return v0
.end method
