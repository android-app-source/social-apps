.class public LX/1id;
.super LX/1iY;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Landroid/net/ConnectivityManager;

.field private final c:LX/1Gk;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/net/ConnectivityManager;LX/1Gk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298720
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 298721
    iput-object p1, p0, LX/1id;->a:LX/0Zb;

    .line 298722
    iput-object p2, p0, LX/1id;->b:Landroid/net/ConnectivityManager;

    .line 298723
    iput-object p3, p0, LX/1id;->c:LX/1Gk;

    .line 298724
    return-void
.end method

.method private a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 6

    .prologue
    .line 298725
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "http_error"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 298726
    const-string v0, "stage"

    invoke-virtual {p1}, LX/1iv;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298727
    const-string v1, "error"

    if-eqz p4, :cond_4

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298728
    iget-object v0, p0, LX/1id;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 298729
    if-eqz v0, :cond_5

    .line 298730
    const-string v1, "network"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298731
    const-string v1, "network_state"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298732
    :goto_1
    invoke-static {p3}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v1

    .line 298733
    iget-object v0, v1, LX/1iV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 298734
    if-eqz v0, :cond_0

    .line 298735
    const-string v2, "request_name"

    invoke-virtual {v3, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298736
    :cond_0
    instance-of v0, p2, Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    move-object v2, v0

    .line 298737
    :goto_2
    const-string v0, "uri"

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298738
    const-string v0, "method"

    invoke-interface {p2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298739
    iget-object v0, v1, LX/1iV;->g:Ljava/lang/String;

    move-object v0, v0

    .line 298740
    if-eqz v0, :cond_1

    .line 298741
    const-string v1, "category"

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298742
    :cond_1
    const-string v0, "http.proxy_host"

    invoke-interface {p3, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/HttpHost;

    .line 298743
    if-eqz v0, :cond_7

    .line 298744
    const-string v1, "proxy"

    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298745
    :cond_2
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 298746
    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x5f

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298747
    const-string v1, "stage"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298748
    const-string v1, "error"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298749
    const-string v1, "network"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298750
    const-string v1, "network_state"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298751
    const-string v1, "category"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298752
    const-string v1, "method"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298753
    const-string v1, "proxy"

    invoke-static {v3, v1, v0}, LX/1id;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 298754
    iget-object v1, p0, LX/1id;->c:LX/1Gk;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/32 v4, 0x36ee80

    invoke-virtual {v1, v0, v4, v5}, LX/1Gk;->a(Ljava/lang/String;J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 298755
    iget-object v0, p0, LX/1id;->a:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 298756
    :cond_3
    return-void

    .line 298757
    :cond_4
    const-string v0, "none"

    goto/16 :goto_0

    .line 298758
    :cond_5
    const-string v0, "network"

    const-string v1, "none"

    invoke-virtual {v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_1

    .line 298759
    :cond_6
    invoke-interface {p2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_2

    .line 298760
    :cond_7
    const-string v0, "fb_http_retried_exceptions"

    invoke-interface {p3, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 298761
    if-eqz v0, :cond_2

    .line 298762
    new-instance v4, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v1}, LX/162;-><init>(LX/0mC;)V

    .line 298763
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 298764
    invoke-virtual {v4, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_4

    .line 298765
    :cond_8
    const-string v1, "retried_exceptions"

    invoke-virtual {v3, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 298766
    const-string v1, "retry_count"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_3
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 298767
    invoke-virtual {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298768
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298769
    const/16 v1, 0x5f

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298770
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 0
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298771
    invoke-direct {p0, p1, p2, p4, p5}, LX/1id;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 298772
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 3

    .prologue
    .line 298773
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 298774
    const-string v0, "fb_http_retried_exceptions"

    invoke-interface {p2, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298775
    sget-object v0, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p2, v2}, LX/1id;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 298776
    const-string v0, "fb_http_retried_exceptions"

    invoke-interface {p2, v0}, Lorg/apache/http/protocol/HttpContext;->removeAttribute(Ljava/lang/String;)Ljava/lang/Object;

    .line 298777
    :cond_0
    return-void
.end method
