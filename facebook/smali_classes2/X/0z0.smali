.class public LX/0z0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0yz",
        "<",
        "LX/6VO;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/concurrent/atomic/AtomicInteger;

.field public c:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166521
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0z0;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166522
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0z0;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166523
    return-void
.end method


# virtual methods
.method public final a(LX/6VO;)V
    .locals 2

    .prologue
    .line 166524
    check-cast p1, LX/6VO;

    .line 166525
    iget-object v0, p1, LX/6VO;->a:LX/14q;

    move-object v0, v0

    .line 166526
    sget-object v1, LX/14q;->VIDEO_PLAY:LX/14q;

    if-eq v0, v1, :cond_1

    .line 166527
    :cond_0
    :goto_0
    return-void

    .line 166528
    :cond_1
    iget-object v0, p0, LX/0z0;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166529
    iget v1, p1, LX/6VO;->c:I

    move v1, v1

    .line 166530
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 166531
    iget v0, p1, LX/6VO;->c:I

    move v0, v0

    .line 166532
    iget v1, p0, LX/0z0;->a:I

    if-lt v0, v1, :cond_0

    .line 166533
    iget-object v0, p0, LX/0z0;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0
.end method

.method public final b(LX/6VO;)V
    .locals 2

    .prologue
    .line 166534
    check-cast p1, LX/6VO;

    .line 166535
    iget-object v0, p1, LX/6VO;->a:LX/14q;

    move-object v0, v0

    .line 166536
    sget-object v1, LX/14q;->VIDEO_PLAY:LX/14q;

    if-eq v0, v1, :cond_1

    .line 166537
    :cond_0
    :goto_0
    return-void

    .line 166538
    :cond_1
    iget-object v0, p0, LX/0z0;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166539
    iget v1, p1, LX/6VO;->c:I

    move v1, v1

    .line 166540
    neg-int v1, v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 166541
    iget v0, p1, LX/6VO;->c:I

    move v0, v0

    .line 166542
    iget v1, p0, LX/0z0;->a:I

    if-lt v0, v1, :cond_0

    .line 166543
    iget-object v0, p0, LX/0z0;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0
.end method
