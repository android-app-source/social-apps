.class public LX/1LY;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fs;
.implements LX/1KU;


# instance fields
.field private final a:LX/1K8;

.field public b:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1K8;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233971
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233972
    iput-object p1, p0, LX/1LY;->a:LX/1K8;

    .line 233973
    return-void
.end method

.method private b()LX/0fz;
    .locals 1

    .prologue
    .line 233968
    iget-object v0, p0, LX/1LY;->b:LX/1Iu;

    .line 233969
    iget-object p0, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 233970
    check-cast v0, LX/0fz;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1OP;)V
    .locals 5

    .prologue
    .line 233931
    const-string v0, "FeedUnitCollectionController.onAdapterDataChanged"

    const v1, -0x4ebbf12

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 233932
    :try_start_0
    invoke-direct {p0}, LX/1LY;->b()LX/0fz;

    move-result-object v0

    .line 233933
    if-nez v0, :cond_1

    .line 233934
    :cond_0
    iget-object v0, p0, LX/1LY;->a:LX/1K8;

    invoke-direct {p0}, LX/1LY;->b()LX/0fz;

    move-result-object v1

    .line 233935
    iget-object v2, v1, LX/0fz;->g:LX/0qu;

    .line 233936
    iget-object v3, v2, LX/0qu;->d:Ljava/util/List;

    .line 233937
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v2, LX/0qu;->d:Ljava/util/List;

    .line 233938
    move-object v2, v3

    .line 233939
    move-object v1, v2

    .line 233940
    invoke-virtual {v0, v1}, LX/1K8;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233941
    const v0, 0x274e6307

    invoke-static {v0}, LX/02m;->a(I)V

    .line 233942
    return-void

    .line 233943
    :catchall_0
    move-exception v0

    const v1, 0x430303b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 233944
    :cond_1
    const/4 v1, 0x0

    .line 233945
    invoke-virtual {v0}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 233946
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 233947
    if-eqz v1, :cond_2

    .line 233948
    instance-of v4, v1, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v4, :cond_4

    .line 233949
    check-cast v1, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v1

    .line 233950
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 233951
    instance-of p1, v1, LX/16p;

    if-eqz p1, :cond_3

    .line 233952
    check-cast v1, LX/16p;

    invoke-interface {v1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 233953
    if-eqz v1, :cond_3

    .line 233954
    iput v2, v1, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    .line 233955
    goto :goto_1

    .line 233956
    :cond_4
    instance-of v4, v1, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v4, :cond_5

    .line 233957
    check-cast v1, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 233958
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 233959
    iput v2, v1, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    .line 233960
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 233961
    goto :goto_0
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 233963
    iget-object v0, p0, LX/1LY;->a:LX/1K8;

    invoke-virtual {v0}, LX/1K8;->a()V

    .line 233964
    invoke-direct {p0}, LX/1LY;->b()LX/0fz;

    move-result-object v0

    const/4 v1, 0x1

    .line 233965
    iget-object p0, v0, LX/0fz;->g:LX/0qu;

    .line 233966
    iput-boolean v1, p0, LX/0qu;->c:Z

    .line 233967
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 233962
    return-void
.end method
