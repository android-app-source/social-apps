.class public LX/0fK;
.super LX/0fL;
.source ""


# instance fields
.field private final g:LX/10h;

.field private final h:LX/0fP;

.field private final i:LX/10b;

.field private final j:LX/10p;

.field private final k:LX/0fO;

.field public l:LX/2t7;

.field public m:LX/10s;

.field public n:LX/GgR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/divebar/DrawerBasedDivebarControllerImpl$DisableSwipeToOpenDrawerInterceptor;

.field public p:LX/10i;

.field public q:Lcom/facebook/ui/drawers/DrawerContentFragment;

.field public r:Landroid/content/Intent;


# direct methods
.method public constructor <init>(LX/10b;LX/0Sh;LX/10c;LX/0fP;LX/0fO;)V
    .locals 9
    .param p1    # LX/10b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 106262
    invoke-direct {p0, p2}, LX/0fL;-><init>(LX/0Sh;)V

    .line 106263
    new-instance v0, LX/10h;

    invoke-direct {v0, p0}, LX/10h;-><init>(LX/0fK;)V

    iput-object v0, p0, LX/0fK;->g:LX/10h;

    .line 106264
    iput-object v1, p0, LX/0fK;->l:LX/2t7;

    .line 106265
    iput-object v1, p0, LX/0fK;->m:LX/10s;

    .line 106266
    iput-object v1, p0, LX/0fK;->n:LX/GgR;

    .line 106267
    iput-object v1, p0, LX/0fK;->o:Lcom/facebook/divebar/DrawerBasedDivebarControllerImpl$DisableSwipeToOpenDrawerInterceptor;

    .line 106268
    iput-object p1, p0, LX/0fK;->i:LX/10b;

    .line 106269
    new-instance v2, LX/10i;

    invoke-static {p3}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v4

    check-cast v4, LX/0Xp;

    invoke-static {p3}, LX/10j;->a(LX/0QB;)LX/10j;

    move-result-object v5

    check-cast v5, LX/10k;

    invoke-static {p3}, LX/10l;->a(LX/0QB;)LX/10l;

    move-result-object v6

    check-cast v6, LX/10k;

    invoke-static {p3}, LX/10m;->a(LX/0QB;)LX/10m;

    move-result-object v7

    check-cast v7, LX/10k;

    invoke-static {p3}, LX/10n;->a(LX/0QB;)LX/10n;

    move-result-object v8

    check-cast v8, LX/10k;

    move-object v3, p1

    invoke-direct/range {v2 .. v8}, LX/10i;-><init>(LX/10b;LX/0Xp;LX/10k;LX/10k;LX/10k;LX/10k;)V

    .line 106270
    move-object v0, v2

    .line 106271
    iput-object v0, p0, LX/0fK;->p:LX/10i;

    .line 106272
    iput-object p4, p0, LX/0fK;->h:LX/0fP;

    .line 106273
    iput-object p5, p0, LX/0fK;->k:LX/0fO;

    .line 106274
    invoke-direct {p0, p1}, LX/0fK;->a(LX/10b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LX/0fK;->b(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, LX/10f;->LOAD_IMMEDIATELY:LX/10f;

    .line 106275
    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106276
    iput-object v0, p0, LX/0fL;->h:LX/10f;

    .line 106277
    new-instance v0, LX/10o;

    iget-object v1, p0, LX/0fK;->i:LX/10b;

    invoke-direct {v0, p0, v1}, LX/10o;-><init>(LX/0fK;LX/10b;)V

    iput-object v0, p0, LX/0fK;->j:LX/10p;

    .line 106278
    return-void

    .line 106279
    :cond_1
    sget-object v0, LX/10f;->LOAD_WHEN_FOCUSED:LX/10f;

    goto :goto_0
.end method

.method public static a(LX/0fK;Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    .line 106280
    const-string v0, "DrawerBasedDiverbarControllerImpl.attachToActivity"

    const v1, 0x42984e17

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 106281
    :try_start_0
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106282
    iget-object v0, p0, LX/0fM;->c:Landroid/content/Context;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0fM;->e:Landroid/view/View;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106283
    if-eqz v0, :cond_1

    .line 106284
    :cond_0
    const v0, -0x49100793

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106285
    :goto_1
    return-void

    .line 106286
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    invoke-virtual {v0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 106287
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    .line 106288
    sget-object v1, LX/A7N;->ENSURE_BACKGROUND:LX/A7N;

    invoke-static {v0, v1}, LX/0fP;->a(LX/0fP;LX/A7N;)V

    .line 106289
    :cond_2
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    iget-object v1, p0, LX/0fK;->i:LX/10b;

    invoke-virtual {v0, v1, p0, p2}, LX/0fP;->a(LX/10b;LX/0fM;Z)V

    .line 106290
    instance-of v0, p1, LX/0ex;

    if-eqz v0, :cond_3

    .line 106291
    iget-object v0, p0, LX/0fK;->p:LX/10i;

    check-cast p1, LX/0ex;

    .line 106292
    invoke-static {v0}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object v1

    invoke-interface {v1, p1, p0}, LX/10k;->a(LX/0ex;LX/0fK;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106293
    :cond_3
    const v0, 0x7785c266

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, -0x527c72ad

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/10b;)Z
    .locals 1

    .prologue
    .line 106294
    iget-object v0, p0, LX/0fK;->k:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10b;->LEFT:LX/10b;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/A7U;)LX/0gs;
    .locals 2

    .prologue
    .line 106295
    sget-object v0, LX/98d;->a:[I

    invoke-virtual {p0}, LX/A7U;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106296
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 106297
    :pswitch_0
    sget-object v0, LX/0gs;->OPENED:LX/0gs;

    goto :goto_0

    .line 106298
    :pswitch_1
    sget-object v0, LX/0gs;->CLOSED:LX/0gs;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(LX/10b;)Z
    .locals 1

    .prologue
    .line 106232
    iget-object v0, p0, LX/0fK;->k:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10b;->RIGHT:LX/10b;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 106299
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 106300
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 106301
    iget-object v1, p0, LX/0fK;->p:LX/10i;

    .line 106302
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106303
    invoke-static {v1}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object p0

    invoke-interface {p0}, LX/10k;->b()Z

    move-result p0

    if-nez p0, :cond_0

    .line 106304
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 106305
    :cond_0
    move v0, v0

    .line 106306
    return v0
.end method

.method public final a(LX/0fP;)V
    .locals 1

    .prologue
    .line 106307
    invoke-super {p0, p1}, LX/0fL;->a(LX/0fP;)V

    .line 106308
    iget-object v0, p0, LX/0fK;->j:LX/10p;

    .line 106309
    iget-object p0, p1, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106310
    return-void
.end method

.method public final a(LX/0fT;)V
    .locals 2

    .prologue
    .line 106311
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106312
    :cond_0
    :goto_0
    return-void

    .line 106313
    :cond_1
    iget-object v0, p0, LX/0fK;->l:LX/2t7;

    if-eqz v0, :cond_2

    .line 106314
    iget-object v0, p0, LX/0fM;->d:LX/0fP;

    move-object v0, v0

    .line 106315
    iget-object v1, p0, LX/0fK;->l:LX/2t7;

    invoke-virtual {v0, v1}, LX/0fP;->b(LX/0fR;)V

    .line 106316
    const/4 v0, 0x0

    iput-object v0, p0, LX/0fK;->l:LX/2t7;

    .line 106317
    :cond_2
    if-eqz p1, :cond_0

    .line 106318
    new-instance v0, LX/2t7;

    invoke-direct {v0, p1}, LX/2t7;-><init>(LX/0fT;)V

    iput-object v0, p0, LX/0fK;->l:LX/2t7;

    .line 106319
    iget-object v0, p0, LX/0fM;->d:LX/0fP;

    move-object v0, v0

    .line 106320
    iget-object v1, p0, LX/0fK;->l:LX/2t7;

    invoke-virtual {v0, v1}, LX/0fP;->a(LX/0fR;)V

    goto :goto_0
.end method

.method public final a(LX/GgR;)V
    .locals 2
    .param p1    # LX/GgR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106321
    iput-object p1, p0, LX/0fK;->n:LX/GgR;

    .line 106322
    iget-object v0, p0, LX/0fK;->n:LX/GgR;

    if-eqz v0, :cond_0

    .line 106323
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    iget-object v1, p0, LX/0fK;->g:LX/10h;

    .line 106324
    iget-object p1, v0, LX/0fP;->h:LX/A7T;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/10h;

    .line 106325
    iget-object v0, p1, LX/A7T;->l:Ljava/util/Set;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106326
    :goto_0
    return-void

    .line 106327
    :cond_0
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    iget-object v1, p0, LX/0fK;->g:LX/10h;

    .line 106328
    iget-object p1, v0, LX/0fP;->h:LX/A7T;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/10h;

    .line 106329
    iget-object v0, p1, LX/A7T;->l:Ljava/util/Set;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 106330
    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 106331
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/0fK;->a(LX/0fK;Landroid/app/Activity;Z)V

    .line 106332
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 106333
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    .line 106334
    iget-object v1, v0, LX/0fP;->h:LX/A7T;

    .line 106335
    iget-object p0, v1, LX/A7T;->b:LX/1wz;

    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result p0

    move v1, p0

    .line 106336
    if-nez v1, :cond_0

    .line 106337
    iget-object v1, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v1, p1}, LX/A7T;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 106338
    iget-object v1, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v1, p2}, LX/A7T;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 106339
    :goto_0
    return-void

    .line 106340
    :cond_0
    iget-object v1, v0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v1, p2}, LX/A7T;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/drawers/DrawerContentFragment;)V
    .locals 0

    .prologue
    .line 106341
    iput-object p1, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    .line 106342
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 106255
    invoke-super {p0, p1}, LX/0fL;->a(Z)V

    .line 106256
    iget-object v0, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    if-eqz v0, :cond_0

    .line 106257
    if-eqz p1, :cond_1

    .line 106258
    iget-object v0, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->c()V

    .line 106259
    :cond_0
    :goto_0
    return-void

    .line 106260
    :cond_1
    iget-object v0, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->d()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 106261
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    return v0
.end method

.method public final a_(Z)V
    .locals 2

    .prologue
    .line 106152
    iget-object v0, p0, LX/0fK;->p:LX/10i;

    .line 106153
    invoke-static {v0}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object v1

    invoke-interface {v1}, LX/10k;->c()Z

    move-result v1

    move v0, v1

    .line 106154
    if-nez v0, :cond_0

    .line 106155
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106156
    iget-object v0, p0, LX/0fM;->d:LX/0fP;

    invoke-virtual {v0, p0, p1}, LX/0fP;->a(LX/0fM;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 106157
    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 106158
    iget-object v0, p0, LX/0fK;->i:LX/10b;

    invoke-direct {p0, v0}, LX/0fK;->a(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106159
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106160
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 106161
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const p0, 0x7f0a004c

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106162
    move-object v0, v1

    .line 106163
    :goto_0
    return-object v0

    .line 106164
    :cond_0
    iget-object v0, p0, LX/0fK;->i:LX/10b;

    invoke-direct {p0, v0}, LX/0fK;->b(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106165
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106166
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 106167
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const p0, 0x7f0a004b

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106168
    move-object v0, v1

    .line 106169
    goto :goto_0

    .line 106170
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106171
    new-instance v1, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-direct {v1, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    .line 106172
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const p0, 0x7f0a0232

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-direct {v2, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106173
    const v2, 0x7f0a0233

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setTextColor(I)V

    .line 106174
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 106175
    const v0, 0x7f080024

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 106176
    move-object v0, v1

    .line 106177
    goto :goto_0
.end method

.method public final b(LX/0fP;)V
    .locals 2

    .prologue
    .line 106178
    iget-object v0, p0, LX/0fK;->j:LX/10p;

    .line 106179
    iget-object v1, p1, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 106180
    invoke-super {p0, p1}, LX/0fL;->b(LX/0fP;)V

    .line 106181
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 106182
    iget-boolean v0, p0, LX/0fM;->a:Z

    move v0, v0

    .line 106183
    return v0
.end method

.method public final b_(Z)V
    .locals 1

    .prologue
    .line 106184
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    .line 106185
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    if-eqz p0, :cond_0

    .line 106186
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    .line 106187
    iput-boolean p1, p0, LX/A7O;->k:Z

    .line 106188
    :cond_0
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    if-eqz p0, :cond_1

    .line 106189
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    .line 106190
    iput-boolean p1, p0, LX/A7O;->k:Z

    .line 106191
    :cond_1
    return-void
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 106192
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    .line 106193
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    if-eqz p0, :cond_0

    .line 106194
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    .line 106195
    iput-boolean p1, p0, LX/A7O;->l:Z

    .line 106196
    :cond_0
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    if-eqz p0, :cond_1

    .line 106197
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    .line 106198
    iput-boolean p1, p0, LX/A7O;->l:Z

    .line 106199
    :cond_1
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 106200
    iget-object v0, p0, LX/0fK;->p:LX/10i;

    .line 106201
    invoke-static {v0}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object v1

    invoke-interface {v1}, LX/10k;->d()Z

    move-result v1

    move v0, v1

    .line 106202
    if-nez v0, :cond_0

    .line 106203
    const/4 v0, 0x1

    .line 106204
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106205
    iget-object v1, p0, LX/0fM;->d:LX/0fP;

    invoke-virtual {v1, v0}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 106206
    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final fU_()Z
    .locals 3

    .prologue
    .line 106207
    iget-object v0, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106208
    const/4 v0, 0x1

    .line 106209
    :goto_0
    return v0

    .line 106210
    :cond_0
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106211
    iget-object v0, p0, LX/0fM;->d:LX/0fP;

    move-object v0, v0

    .line 106212
    const/4 v1, 0x1

    .line 106213
    sget-object v2, LX/A7M;->b:[I

    .line 106214
    iget-object p0, v0, LX/0fP;->q:LX/10e;

    move-object p0, p0

    .line 106215
    invoke-virtual {p0}, LX/10e;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 106216
    const/4 v1, 0x0

    :cond_1
    :goto_1
    move v0, v1

    .line 106217
    goto :goto_0

    .line 106218
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 106219
    :pswitch_0
    iget-object v2, v0, LX/0fP;->j:LX/A7O;

    .line 106220
    iget-object p0, v2, LX/A7O;->d:LX/0fM;

    move-object v2, p0

    .line 106221
    invoke-virtual {v2}, LX/0fM;->fY_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 106222
    invoke-virtual {v0, v1}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 106223
    :pswitch_1
    iget-object v2, v0, LX/0fP;->k:LX/A7O;

    .line 106224
    iget-object p0, v2, LX/A7O;->d:LX/0fM;

    move-object v2, p0

    .line 106225
    invoke-virtual {v2}, LX/0fM;->fY_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 106226
    invoke-virtual {v0, v1}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 106227
    invoke-virtual {p0}, LX/0fM;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fK;->o:Lcom/facebook/divebar/DrawerBasedDivebarControllerImpl$DisableSwipeToOpenDrawerInterceptor;

    if-eqz v0, :cond_0

    .line 106228
    iget-object v0, p0, LX/0fM;->d:LX/0fP;

    move-object v0, v0

    .line 106229
    iget-object v1, p0, LX/0fK;->o:Lcom/facebook/divebar/DrawerBasedDivebarControllerImpl$DisableSwipeToOpenDrawerInterceptor;

    invoke-virtual {v0, v1}, LX/0fP;->b(LX/0fR;)V

    .line 106230
    const/4 v0, 0x0

    iput-object v0, p0, LX/0fK;->o:Lcom/facebook/divebar/DrawerBasedDivebarControllerImpl$DisableSwipeToOpenDrawerInterceptor;

    .line 106231
    :cond_0
    return-void
.end method

.method public final h()LX/0gi;
    .locals 2

    .prologue
    .line 106233
    iget-object v0, p0, LX/0fK;->p:LX/10i;

    .line 106234
    iget-object v1, v0, LX/10i;->d:LX/10k;

    iget-object p0, v0, LX/10i;->f:LX/10b;

    invoke-interface {v1, p0}, LX/10k;->a(LX/10b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106235
    sget-object v1, LX/0gi;->SNACKS_DIVEBAR:LX/0gi;

    .line 106236
    :goto_0
    move-object v0, v1

    .line 106237
    return-object v0

    .line 106238
    :cond_0
    iget-object v1, v0, LX/10i;->c:LX/10k;

    iget-object p0, v0, LX/10i;->f:LX/10b;

    invoke-interface {v1, p0}, LX/10k;->a(LX/10b;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106239
    sget-object v1, LX/0gi;->NOW_SIDEBAR:LX/0gi;

    goto :goto_0

    .line 106240
    :cond_1
    iget-object v1, v0, LX/10i;->e:LX/10k;

    iget-object p0, v0, LX/10i;->f:LX/10b;

    invoke-interface {v1, p0}, LX/10k;->a(LX/10b;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106241
    sget-object v1, LX/0gi;->INSPIRATION_CAMERA_DIVEBAR:LX/0gi;

    goto :goto_0

    .line 106242
    :cond_2
    sget-object v1, LX/0gi;->DEFAULT_DIVEBAR:LX/0gi;

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 106243
    iget-object v0, p0, LX/0fK;->h:LX/0fP;

    .line 106244
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    if-eqz p0, :cond_0

    .line 106245
    iget-object p0, v0, LX/0fP;->j:LX/A7O;

    invoke-virtual {p0}, LX/A7O;->b()V

    .line 106246
    :cond_0
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    if-eqz p0, :cond_1

    .line 106247
    iget-object p0, v0, LX/0fP;->k:LX/A7O;

    invoke-virtual {p0}, LX/A7O;->b()V

    .line 106248
    :cond_1
    return-void
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 106249
    iget-object v0, p0, LX/0fK;->i:LX/10b;

    sget-object v1, LX/10b;->LEFT:LX/10b;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0d207e

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d207f

    goto :goto_0
.end method

.method public final k()Lcom/facebook/ui/drawers/DrawerContentFragment;
    .locals 2

    .prologue
    .line 106250
    iget-object v0, p0, LX/0fK;->p:LX/10i;

    .line 106251
    iget-object v1, p0, LX/0fM;->c:Landroid/content/Context;

    move-object v1, v1

    .line 106252
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106253
    invoke-static {v0}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object p0

    invoke-interface {p0}, LX/10k;->a()Lcom/facebook/ui/drawers/DrawerContentFragment;

    move-result-object p0

    move-object v0, p0

    .line 106254
    return-object v0
.end method
