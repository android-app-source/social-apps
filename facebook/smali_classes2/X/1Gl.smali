.class public LX/1Gl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/1Gl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225975
    const-class v0, LX/1Gl;

    sput-object v0, LX/1Gl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225977
    return-void
.end method

.method public static a(Lorg/apache/http/HttpMessage;)J
    .locals 6

    .prologue
    .line 225978
    const-wide/16 v0, 0x0

    .line 225979
    invoke-interface {p0}, Lorg/apache/http/HttpMessage;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v2

    .line 225980
    :goto_0
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 225981
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v3

    .line 225982
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 225983
    goto :goto_0

    .line 225984
    :cond_0
    return-wide v0
.end method

.method public static a(LX/0QB;)LX/1Gl;
    .locals 3

    .prologue
    .line 225985
    sget-object v0, LX/1Gl;->b:LX/1Gl;

    if-nez v0, :cond_1

    .line 225986
    const-class v1, LX/1Gl;

    monitor-enter v1

    .line 225987
    :try_start_0
    sget-object v0, LX/1Gl;->b:LX/1Gl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225988
    if-eqz v2, :cond_0

    .line 225989
    :try_start_1
    new-instance v0, LX/1Gl;

    invoke-direct {v0}, LX/1Gl;-><init>()V

    .line 225990
    move-object v0, v0

    .line 225991
    sput-object v0, LX/1Gl;->b:LX/1Gl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225994
    :cond_1
    sget-object v0, LX/1Gl;->b:LX/1Gl;

    return-object v0

    .line 225995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 225997
    const/4 v0, 0x0

    .line 225998
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 225999
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 226000
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 226001
    :cond_0
    if-nez v0, :cond_1

    .line 226002
    const-string v1, "Content-Type"

    invoke-interface {p0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 226003
    if-eqz v1, :cond_1

    .line 226004
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 226005
    :cond_1
    return-object v0
.end method

.method public static c(Lorg/apache/http/HttpResponse;)J
    .locals 3

    .prologue
    .line 226006
    const-string v0, "Content-Length"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 226007
    if-eqz v0, :cond_0

    .line 226008
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 226009
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 226010
    :goto_0
    return-wide v0

    .line 226011
    :catch_0
    move-exception v0

    .line 226012
    sget-object v1, LX/1Gl;->a:Ljava/lang/Class;

    const-string v2, "Failure parsing Content-Length header"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226013
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
