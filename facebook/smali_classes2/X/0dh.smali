.class public LX/0dh;
.super LX/0dP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:LX/0Tn;

.field private static volatile e:LX/0dh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90716
    sget-object v0, LX/0df;->G:LX/0Tn;

    sput-object v0, LX/0dh;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0yU;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90717
    sget-object v0, LX/0dh;->d:LX/0Tn;

    invoke-direct {p0, v0, p1}, LX/0dP;-><init>(LX/0Tn;LX/0yU;)V

    .line 90718
    return-void
.end method

.method public static a(LX/0QB;)LX/0dh;
    .locals 4

    .prologue
    .line 90719
    sget-object v0, LX/0dh;->e:LX/0dh;

    if-nez v0, :cond_1

    .line 90720
    const-class v1, LX/0dh;

    monitor-enter v1

    .line 90721
    :try_start_0
    sget-object v0, LX/0dh;->e:LX/0dh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90722
    if-eqz v2, :cond_0

    .line 90723
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90724
    new-instance p0, LX/0dh;

    invoke-static {v0}, LX/0yU;->b(LX/0QB;)LX/0yU;

    move-result-object v3

    check-cast v3, LX/0yU;

    invoke-direct {p0, v3}, LX/0dh;-><init>(LX/0yU;)V

    .line 90725
    move-object v0, p0

    .line 90726
    sput-object v0, LX/0dh;->e:LX/0dh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90727
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90728
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90729
    :cond_1
    sget-object v0, LX/0dh;->e:LX/0dh;

    return-object v0

    .line 90730
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
