.class public LX/10m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/10k;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0fO;

.field private final b:LX/0gh;


# direct methods
.method public constructor <init>(LX/0fO;LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169354
    iput-object p1, p0, LX/10m;->a:LX/0fO;

    .line 169355
    iput-object p2, p0, LX/10m;->b:LX/0gh;

    .line 169356
    return-void
.end method

.method public static a(LX/0QB;)LX/10m;
    .locals 5

    .prologue
    .line 169342
    const-class v1, LX/10m;

    monitor-enter v1

    .line 169343
    :try_start_0
    sget-object v0, LX/10m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 169344
    sput-object v2, LX/10m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 169345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 169347
    new-instance p0, LX/10m;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v3

    check-cast v3, LX/0fO;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v4

    check-cast v4, LX/0gh;

    invoke-direct {p0, v3, v4}, LX/10m;-><init>(LX/0fO;LX/0gh;)V

    .line 169348
    move-object v0, p0

    .line 169349
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 169350
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/10m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169351
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 169352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/ui/drawers/DrawerContentFragment;
    .locals 1

    .prologue
    .line 169341
    new-instance v0, Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    invoke-direct {v0}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;-><init>()V

    return-object v0
.end method

.method public final a(LX/0ex;LX/0fK;)V
    .locals 0

    .prologue
    .line 169340
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 169332
    if-eqz p1, :cond_0

    .line 169333
    iget-object v0, p0, LX/10m;->b:LX/0gh;

    const-string v1, "tap_dive_bar"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    const-string v1, "snacks_divebar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 169334
    :goto_0
    return-void

    .line 169335
    :cond_0
    iget-object v0, p0, LX/10m;->b:LX/0gh;

    .line 169336
    iget-object v1, v0, LX/0gh;->w:Ljava/lang/String;

    move-object v0, v1

    .line 169337
    if-nez v0, :cond_1

    .line 169338
    iget-object v0, p0, LX/10m;->b:LX/0gh;

    const-string v1, "tap_outside"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 169339
    :cond_1
    iget-object v0, p0, LX/10m;->b:LX/0gh;

    const-string v1, "snacks_divebar"

    invoke-virtual {v0, v1}, LX/0gh;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/10b;)Z
    .locals 1

    .prologue
    .line 169328
    sget-object v0, LX/10b;->RIGHT:LX/10b;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/10m;->a:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 169331
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 169330
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 169329
    const/4 v0, 0x0

    return v0
.end method
