.class public final LX/1kz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1ky;

.field public final synthetic b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;LX/1ky;)V
    .locals 0

    .prologue
    .line 310850
    iput-object p1, p0, LX/1kz;->b:LX/0tX;

    iput-object p2, p0, LX/1kz;->a:LX/1ky;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 310851
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 310852
    iget-object v0, p0, LX/1kz;->a:LX/1ky;

    .line 310853
    iget-object v1, v0, LX/1ky;->a:Ljava/util/concurrent/Future;

    move-object v0, v1

    .line 310854
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 310855
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 310856
    return-void
.end method
