.class public abstract LX/0dW;
.super LX/0dX;
.source ""

# interfaces
.implements LX/0dY;
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0dX",
        "<TE;>;",
        "LX/0dY",
        "<TE;>;",
        "Ljava/util/NavigableSet",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/50U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50U",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final transient a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field

.field public transient b:LX/0dW;
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 90444
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 90445
    sput-object v0, LX/0dW;->c:Ljava/util/Comparator;

    .line 90446
    new-instance v0, LX/50U;

    .line 90447
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 90448
    sget-object v2, LX/0dW;->c:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    sput-object v0, LX/0dW;->d:LX/50U;

    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 90449
    invoke-direct {p0}, LX/0dX;-><init>()V

    .line 90450
    iput-object p1, p0, LX/0dW;->a:Ljava/util/Comparator;

    .line 90451
    return-void
.end method

.method public static varargs a(Ljava/util/Comparator;I[Ljava/lang/Object;)LX/0dW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;I[TE;)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 90452
    if-nez p1, :cond_0

    .line 90453
    invoke-static {p0}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object v0

    .line 90454
    :goto_0
    return-object v0

    .line 90455
    :cond_0
    invoke-static {p2, p1}, LX/0P8;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 90456
    const/4 v1, 0x0

    invoke-static {p2, v1, p1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    move v2, v0

    move v1, v0

    .line 90457
    :goto_1
    if-ge v2, p1, :cond_1

    .line 90458
    aget-object v3, p2, v2

    .line 90459
    add-int/lit8 v0, v1, -0x1

    aget-object v0, p2, v0

    .line 90460
    invoke-interface {p0, v3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 90461
    add-int/lit8 v0, v1, 0x1

    aput-object v3, p2, v1

    .line 90462
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 90463
    :cond_1
    const/4 v0, 0x0

    invoke-static {p2, v1, p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 90464
    new-instance v0, LX/50U;

    invoke-static {p2, v1}, LX/0Px;->asImmutableList([Ljava/lang/Object;I)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(Ljava/util/Comparator;)LX/50U;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;)",
            "LX/50U",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90465
    sget-object v0, LX/0dW;->c:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90466
    sget-object v0, LX/0dW;->d:LX/50U;

    .line 90467
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/50U;

    .line 90468
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 90469
    invoke-direct {v0, v1, p0}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static b(Ljava/util/Comparator;)LX/4yi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<TE;>;)",
            "LX/4yi",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90470
    new-instance v0, LX/4yi;

    invoke-direct {v0, p0}, LX/4yi;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 90471
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 90472
    iget-object v0, p0, LX/0dW;->a:Ljava/util/Comparator;

    .line 90473
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p0

    move v0, p0

    .line 90474
    return v0
.end method

.method public a()LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90483
    iget-object v0, p0, LX/0dW;->b:LX/0dW;

    .line 90484
    if-nez v0, :cond_0

    .line 90485
    invoke-virtual {p0}, LX/0dW;->c()LX/0dW;

    move-result-object v0

    iput-object v0, p0, LX/0dW;->b:LX/0dW;

    .line 90486
    iput-object p0, v0, LX/0dW;->b:LX/0dW;

    .line 90487
    :cond_0
    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;Z)LX/0dW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract b()LX/0Rc;
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;Z)LX/0dW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final b(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90475
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90476
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90477
    iget-object v0, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 90478
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0dW;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0

    .line 90479
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90480
    new-instance v0, LX/4xR;

    invoke-direct {v0, p0}, LX/4xR;-><init>(LX/0dW;)V

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90481
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/0dW;->a(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 90482
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0dW;->d(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 90442
    iget-object v0, p0, LX/0dW;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public final d(Ljava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 90443
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/0dW;->b(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public synthetic descendingIterator()Ljava/util/Iterator;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 90425
    invoke-virtual {p0}, LX/0dW;->b()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic descendingSet()Ljava/util/NavigableSet;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 90426
    invoke-virtual {p0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 90427
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 90428
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0dW;->c(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->b()LX/0Rc;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0RZ;->c(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 90429
    invoke-virtual {p0, p1, p2}, LX/0dW;->c(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 90441
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0dW;->c(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 90430
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0dW;->d(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract iterator()LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 90431
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 90432
    invoke-virtual {p0}, LX/0dW;->b()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 90433
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0dW;->c(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->b()LX/0Rc;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0RZ;->c(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 90434
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 90435
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 90436
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0dW;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 2

    .prologue
    .line 90437
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, LX/0dW;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 90438
    invoke-virtual {p0, p1, p2}, LX/0dW;->d(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 90439
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0dW;->d(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 90440
    new-instance v0, LX/4yj;

    iget-object v1, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-virtual {p0}, LX/0dW;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4yj;-><init>(Ljava/util/Comparator;[Ljava/lang/Object;)V

    return-object v0
.end method
