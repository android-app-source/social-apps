.class public LX/0X2;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76963
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 76964
    return-void
.end method

.method public static a(LX/0Or;)LX/03R;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/03R;"
        }
    .end annotation

    .prologue
    .line 76965
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 76966
    if-nez v0, :cond_0

    .line 76967
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 76968
    :goto_0
    return-object v0

    .line 76969
    :cond_0
    iget-boolean p0, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, p0

    .line 76970
    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public static a(LX/0WJ;)LX/03R;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAWorkUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76971
    invoke-virtual {p0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76972
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 76973
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 76974
    iget-boolean p0, v0, Lcom/facebook/user/model/User;->p:Z

    move v0, p0

    .line 76975
    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public static a(LX/0WJ;Landroid/content/Context;)LX/0SI;
    .locals 1
    .annotation runtime Lcom/facebook/auth/viewercontext/ViewerContextManagerForApp;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76976
    new-instance v0, LX/0X3;

    invoke-direct {v0, p0, p1}, LX/0X3;-><init>(LX/0WJ;Landroid/content/Context;)V

    return-object v0
.end method

.method public static a(LX/0WJ;)Lcom/facebook/auth/credentials/UserTokenCredentials;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76977
    invoke-virtual {p0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 76978
    if-eqz v1, :cond_0

    .line 76979
    new-instance v0, Lcom/facebook/auth/credentials/UserTokenCredentials;

    .line 76980
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 76981
    iget-object p0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v1, p0

    .line 76982
    invoke-direct {v0, v2, v1}, Lcom/facebook/auth/credentials/UserTokenCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 76983
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Or;LX/01T;)Lcom/facebook/user/model/User;
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/01T;",
            ")",
            "Lcom/facebook/user/model/User;"
        }
    .end annotation

    .prologue
    .line 76984
    sget-object v0, LX/01T;->PAA:LX/01T;

    if-ne p2, v0, :cond_0

    .line 76985
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot use default viewer context user provider for Page Manager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76986
    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 76987
    if-nez v0, :cond_2

    .line 76988
    const/4 v0, 0x0

    .line 76989
    :cond_1
    return-object v0

    .line 76990
    :cond_2
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 76991
    iget-object v2, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 76992
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76993
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "viewer context id and logged in user id should always be the same in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/UserKey;
    .locals 1
    .param p0    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76994
    if-eqz p0, :cond_0

    .line 76995
    iget-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 76996
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/03R;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76997
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0SI;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76954
    invoke-interface {p0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 76955
    if-nez v0, :cond_0

    .line 76956
    const/4 v0, 0x0

    .line 76957
    :goto_0
    return-object v0

    .line 76958
    :cond_0
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 76959
    goto :goto_0
.end method

.method public static a(Lcom/facebook/auth/viewercontext/ViewerContext;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76950
    if-eqz p0, :cond_0

    .line 76951
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    .line 76952
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0WJ;)LX/03R;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76960
    invoke-virtual {p0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 76961
    iget-boolean p0, v0, Lcom/facebook/user/model/User;->F:Z

    move v0, p0

    .line 76962
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public static b(LX/0WJ;Landroid/content/Context;)LX/0SI;
    .locals 1
    .annotation runtime Lcom/facebook/auth/viewercontext/ViewerContextManagerForContext;
    .end annotation

    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76953
    new-instance v0, LX/0X3;

    invoke-direct {v0, p0, p1}, LX/0X3;-><init>(LX/0WJ;Landroid/content/Context;)V

    return-object v0
.end method

.method public static b(LX/0Or;)LX/0XJ;
    .locals 2
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/user/gender/UserGender;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0XJ;"
        }
    .end annotation

    .prologue
    .line 76942
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 76943
    if-eqz v0, :cond_0

    .line 76944
    iget-object v1, v0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v1, v1

    .line 76945
    if-nez v1, :cond_1

    .line 76946
    :cond_0
    sget-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 76947
    :goto_0
    return-object v0

    .line 76948
    :cond_1
    iget-object v1, v0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v0, v1

    .line 76949
    goto :goto_0
.end method

.method public static b(LX/0SI;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76941
    invoke-interface {p0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/user/model/UserKey;
    .locals 3
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76938
    if-eqz p0, :cond_0

    new-instance v0, Lcom/facebook/user/model/UserKey;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    .line 76939
    iget-object v2, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 76940
    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/user/model/User;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAMessengerOnlyUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76933
    if-nez p0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 76934
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    move v0, v0

    .line 76935
    if-eqz v0, :cond_1

    .line 76936
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    move v0, v0

    .line 76937
    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0Or;)Lcom/facebook/user/model/PicSquare;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/user/model/PicSquare;"
        }
    .end annotation

    .prologue
    .line 76929
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 76930
    if-nez v0, :cond_0

    .line 76931
    const/4 v0, 0x0

    .line 76932
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/user/model/User;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserDeactivatedOnFbNotOnMessenger;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76924
    if-nez p0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 76925
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    move v0, v0

    .line 76926
    if-eqz v0, :cond_1

    .line 76927
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    move v0, v0

    .line 76928
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/0Or;)Lcom/facebook/user/model/PicSquare;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/user/model/PicSquare;"
        }
    .end annotation

    .prologue
    .line 76920
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 76921
    if-nez v0, :cond_0

    .line 76922
    const/4 v0, 0x0

    .line 76923
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/user/model/User;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserMessengerOnlyDeactivatedUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76917
    if-nez p0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 76918
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->aa:Z

    move v0, v0

    .line 76919
    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NontrivialConfigureMethod"
        }
    .end annotation

    .prologue
    .line 76916
    return-void
.end method
