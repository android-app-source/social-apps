.class public LX/1b1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17Y;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279051
    iput-object p1, p0, LX/1b1;->a:LX/17Y;

    .line 279052
    iput-object p2, p0, LX/1b1;->b:Lcom/facebook/content/SecureContextHelper;

    .line 279053
    iput-object p3, p0, LX/1b1;->c:Landroid/content/Context;

    .line 279054
    return-void
.end method

.method public static a(LX/0QB;)LX/1b1;
    .locals 1

    .prologue
    .line 279055
    invoke-static {p0}, LX/1b1;->b(LX/0QB;)LX/1b1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1b1;
    .locals 4

    .prologue
    .line 279056
    new-instance v3, LX/1b1;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v0, v1, v2}, LX/1b1;-><init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    .line 279057
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 279058
    iget-object v0, p0, LX/1b1;->a:LX/17Y;

    iget-object v1, p0, LX/1b1;->c:Landroid/content/Context;

    sget-object v2, LX/0ax;->iF:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 279059
    iget-object v2, p0, LX/1b1;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x65

    iget-object v0, p0, LX/1b1;->c:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 279060
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 279061
    iget-object v0, p0, LX/1b1;->a:LX/17Y;

    iget-object v1, p0, LX/1b1;->c:Landroid/content/Context;

    sget-object v2, LX/0ax;->iG:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 279062
    iget-object v2, p0, LX/1b1;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x65

    iget-object v0, p0, LX/1b1;->c:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 279063
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 279064
    sget-object v0, LX/0ax;->dn:Ljava/lang/String;

    const-string v1, "/help/526170970918554/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279065
    iget-object v1, p0, LX/1b1;->a:LX/17Y;

    iget-object v2, p0, LX/1b1;->c:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 279066
    iget-object v1, p0, LX/1b1;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/1b1;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 279067
    return-void
.end method
