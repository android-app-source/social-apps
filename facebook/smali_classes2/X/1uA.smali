.class public LX/1uA;
.super LX/0vw;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 338514
    invoke-direct {p0}, LX/0vw;-><init>()V

    return-void
.end method


# virtual methods
.method public final D(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 338517
    invoke-virtual {p1}, Landroid/view/View;->isPaddingRelative()Z

    move-result v0

    move v0, v0

    .line 338518
    return v0
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 338515
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayerPaint(Landroid/graphics/Paint;)V

    .line 338516
    return-void
.end method

.method public final b(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 338510
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 338511
    return-void
.end method

.method public final e(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 338512
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutDirection(I)V

    .line 338513
    return-void
.end method

.method public final h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 338508
    invoke-virtual {p1}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    move v0, v0

    .line 338509
    return v0
.end method

.method public final n(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 338506
    invoke-virtual {p1}, Landroid/view/View;->getPaddingStart()I

    move-result v0

    move v0, v0

    .line 338507
    return v0
.end method

.method public final o(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 338504
    invoke-virtual {p1}, Landroid/view/View;->getPaddingEnd()I

    move-result v0

    move v0, v0

    .line 338505
    return v0
.end method

.method public final x(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 338502
    invoke-virtual {p1}, Landroid/view/View;->getWindowSystemUiVisibility()I

    move-result v0

    move v0, v0

    .line 338503
    return v0
.end method
