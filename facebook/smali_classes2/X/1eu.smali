.class public LX/1eu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1eu;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/1c3;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1c3;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289270
    iput-object p1, p0, LX/1eu;->a:Landroid/content/res/Resources;

    .line 289271
    iput-object p2, p0, LX/1eu;->b:LX/1c3;

    .line 289272
    return-void
.end method

.method public static a(LX/0QB;)LX/1eu;
    .locals 5

    .prologue
    .line 289273
    sget-object v0, LX/1eu;->c:LX/1eu;

    if-nez v0, :cond_1

    .line 289274
    const-class v1, LX/1eu;

    monitor-enter v1

    .line 289275
    :try_start_0
    sget-object v0, LX/1eu;->c:LX/1eu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 289276
    if-eqz v2, :cond_0

    .line 289277
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 289278
    new-instance p0, LX/1eu;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v3

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1bs;->a(LX/0QB;)LX/1c3;

    move-result-object v4

    check-cast v4, LX/1c3;

    invoke-direct {p0, v3, v4}, LX/1eu;-><init>(Landroid/content/res/Resources;LX/1c3;)V

    .line 289279
    move-object v0, p0

    .line 289280
    sput-object v0, LX/1eu;->c:LX/1eu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289281
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 289282
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 289283
    :cond_1
    sget-object v0, LX/1eu;->c:LX/1eu;

    return-object v0

    .line 289284
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 289285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1FJ;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 289286
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 289287
    instance-of v1, v0, LX/1q8;

    if-eqz v1, :cond_0

    .line 289288
    new-instance v0, LX/7U2;

    invoke-direct {v0, p1}, LX/7U2;-><init>(LX/1FJ;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289289
    :goto_0
    invoke-virtual {p1}, LX/1FJ;->close()V

    :goto_1
    return-object v0

    .line 289290
    :cond_0
    :try_start_1
    instance-of v1, v0, LX/4e7;

    if-eqz v1, :cond_1

    .line 289291
    new-instance v1, LX/7Tz;

    iget-object v2, p0, LX/1eu;->b:LX/1c3;

    invoke-virtual {v2, v0}, LX/1c3;->a(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/4dF;

    invoke-direct {v1, v0, p1}, LX/7Tz;-><init>(LX/4dF;LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    .line 289292
    :cond_1
    new-instance v0, LX/7U1;

    iget-object v1, p0, LX/1eu;->a:Landroid/content/res/Resources;

    invoke-direct {v0, v1, p1}, LX/7U1;-><init>(Landroid/content/res/Resources;LX/1FJ;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 289293
    :catch_0
    invoke-virtual {p1}, LX/1FJ;->close()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
