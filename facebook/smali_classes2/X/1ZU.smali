.class public final LX/1ZU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Kw;


# direct methods
.method public constructor <init>(LX/1Kw;)V
    .locals 0

    .prologue
    .line 274746
    iput-object p1, p0, LX/1ZU;->a:LX/1Kw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 274747
    sget-object v0, LX/1Kw;->a:Ljava/lang/Class;

    const-string v1, "Failed to load substories"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274748
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 274749
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 274750
    iget-object v0, p0, LX/1ZU;->a:LX/1Kw;

    iget-object v0, v0, LX/1Kw;->d:LX/1Pq;

    if-nez v0, :cond_0

    .line 274751
    :goto_0
    return-void

    .line 274752
    :cond_0
    iget-object v0, p0, LX/1ZU;->a:LX/1Kw;

    iget-object v0, v0, LX/1Kw;->e:LX/1Iu;

    .line 274753
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 274754
    check-cast v0, LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 274755
    iget-object v0, p0, LX/1ZU;->a:LX/1Kw;

    iget-object v0, v0, LX/1Kw;->d:LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
