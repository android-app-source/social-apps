.class public LX/0tu;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/contextual/RawContextualConfigSource;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0tw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155791
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0tw;
    .locals 5

    .prologue
    .line 155793
    sget-object v0, LX/0tu;->a:LX/0tw;

    if-nez v0, :cond_1

    .line 155794
    const-class v1, LX/0tu;

    monitor-enter v1

    .line 155795
    :try_start_0
    sget-object v0, LX/0tu;->a:LX/0tw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155796
    if-eqz v2, :cond_0

    .line 155797
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155798
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    invoke-static {v3, v4, p0}, LX/0tv;->a(LX/0ad;LX/0W3;LX/0So;)LX/0tw;

    move-result-object v3

    move-object v0, v3

    .line 155799
    sput-object v0, LX/0tu;->a:LX/0tw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155802
    :cond_1
    sget-object v0, LX/0tu;->a:LX/0tw;

    return-object v0

    .line 155803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 155792
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {v0, v1, v2}, LX/0tv;->a(LX/0ad;LX/0W3;LX/0So;)LX/0tw;

    move-result-object v0

    return-object v0
.end method
