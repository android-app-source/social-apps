.class public LX/1Bd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Bd;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Be;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Be;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213346
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Bd;->b:Ljava/util/HashMap;

    .line 213347
    iput-object p1, p0, LX/1Bd;->a:LX/0Ot;

    .line 213348
    return-void
.end method

.method public static a(LX/0QB;)LX/1Bd;
    .locals 4

    .prologue
    .line 213379
    sget-object v0, LX/1Bd;->c:LX/1Bd;

    if-nez v0, :cond_1

    .line 213380
    const-class v1, LX/1Bd;

    monitor-enter v1

    .line 213381
    :try_start_0
    sget-object v0, LX/1Bd;->c:LX/1Bd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213382
    if-eqz v2, :cond_0

    .line 213383
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213384
    new-instance v3, LX/1Bd;

    const/16 p0, 0x1f4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Bd;-><init>(LX/0Ot;)V

    .line 213385
    move-object v0, v3

    .line 213386
    sput-object v0, LX/1Bd;->c:LX/1Bd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213387
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213388
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213389
    :cond_1
    sget-object v0, LX/1Bd;->c:LX/1Bd;

    return-object v0

    .line 213390
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 213373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 213374
    invoke-static {v1}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 213375
    :cond_0
    :goto_0
    return-object v0

    .line 213376
    :cond_1
    invoke-static {v1}, LX/2yo;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 213377
    if-eqz v1, :cond_0

    .line 213378
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213349
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 213350
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 213351
    invoke-static {v6}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 213352
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/1VO;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 213353
    :cond_0
    :goto_0
    return-void

    .line 213354
    :cond_1
    invoke-static {v0}, LX/1Bd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    .line 213355
    if-eqz v3, :cond_0

    .line 213356
    iget-object v0, p0, LX/1Bd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Be;

    .line 213357
    invoke-virtual {v2}, LX/1Be;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, LX/1Be;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 213358
    const/4 v0, 0x0

    .line 213359
    :goto_1
    move v0, v0

    .line 213360
    if-eqz v0, :cond_0

    .line 213361
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    .line 213362
    if-eqz v0, :cond_0

    .line 213363
    invoke-virtual {v2}, LX/1Be;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m()I

    move-result v0

    move v7, v0

    .line 213364
    :goto_2
    if-lez v7, :cond_0

    .line 213365
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    .line 213366
    if-nez v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    :goto_3
    move-object v4, v0

    .line 213367
    invoke-static {p1}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    .line 213368
    new-instance v0, Lcom/facebook/feed/logging/BrowserPrefetchVpvLoggingHandler$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feed/logging/BrowserPrefetchVpvLoggingHandler$1;-><init>(LX/1Bd;LX/1Be;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Z)V

    .line 213369
    iget-object v1, p0, LX/1Bd;->b:Ljava/util/HashMap;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213370
    iget-object v1, v2, LX/1Be;->j:Landroid/os/Handler;

    move-object v1, v1

    .line 213371
    int-to-long v2, v7

    const v4, -0x6b8177b7

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 213372
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l()I

    move-result v0

    move v7, v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v0

    goto :goto_3
.end method
