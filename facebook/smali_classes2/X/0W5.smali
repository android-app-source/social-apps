.class public final LX/0W5;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 75440
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "overscroll_glow"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "overscroll_edge"

    aput-object v3, v1, v2

    .line 75441
    sput-object v1, LX/0W5;->a:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [I

    sput-object v1, LX/0W5;->b:[I

    .line 75442
    :try_start_0
    const-string v1, "com.android.internal.R$drawable"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 75443
    sget-object v3, LX/0W5;->a:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 75444
    sget-object v6, LX/0W5;->b:[I

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    aput v5, v6, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75445
    add-int/lit8 v1, v1, 0x1

    .line 75446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75447
    :catch_0
    :cond_0
    return-void
.end method

.method public static a(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 75448
    move v0, v1

    :goto_0
    sget-object v2, LX/0W5;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 75449
    sget-object v2, LX/0W5;->b:[I

    aget v2, v2, v0

    if-ne v2, p0, :cond_0

    .line 75450
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 75451
    :goto_1
    return-object v0

    .line 75452
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75453
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
