.class public final LX/18g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/3TD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

.field public final synthetic b:LX/18Q;


# direct methods
.method public constructor <init>(LX/18Q;Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V
    .locals 0

    .prologue
    .line 206971
    iput-object p1, p0, LX/18g;->b:LX/18Q;

    iput-object p2, p0, LX/18g;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 206972
    iget-object v0, p0, LX/18g;->b:LX/18Q;

    iget-object v0, v0, LX/18Q;->h:Ljava/util/Set;

    iget-object v1, p0, LX/18g;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 206973
    iget-object v0, p0, LX/18g;->b:LX/18Q;

    iget-object v0, v0, LX/18Q;->e:LX/03V;

    const-string v1, "megaphone"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failure fetching megaphone for location"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/18g;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206974
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 206975
    check-cast p1, LX/3TD;

    .line 206976
    iget-object v0, p0, LX/18g;->b:LX/18Q;

    iget-object v0, v0, LX/18Q;->h:Ljava/util/Set;

    iget-object v1, p0, LX/18g;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 206977
    iget-object v0, p0, LX/18g;->b:LX/18Q;

    iget-object v1, p0, LX/18g;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iget-object v2, p0, LX/18g;->b:LX/18Q;

    iget-object v2, v2, LX/18Q;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, p1, v2, v3}, LX/18Q;->a(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;LX/3TD;J)V

    .line 206978
    return-void
.end method
