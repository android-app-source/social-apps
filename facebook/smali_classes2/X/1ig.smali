.class public LX/1ig;
.super LX/1iY;
.source ""


# instance fields
.field private a:LX/1ih;


# direct methods
.method public constructor <init>(LX/1ih;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298811
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 298812
    iput-object p1, p0, LX/1ig;->a:LX/1ih;

    .line 298813
    return-void
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4

    .prologue
    .line 298814
    invoke-super {p0, p1, p2}, LX/1iY;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 298815
    if-nez p1, :cond_1

    .line 298816
    :cond_0
    :goto_0
    return-void

    .line 298817
    :cond_1
    const-string v0, "X-FB-Checkpoint"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 298818
    if-eqz v0, :cond_0

    .line 298819
    const-string v0, "X-FB-Checkpoint"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 298820
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298821
    iget-object v1, p0, LX/1ig;->a:LX/1ih;

    const/4 p2, 0x0

    .line 298822
    iget-object v2, v1, LX/1ih;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 298823
    if-eqz v2, :cond_2

    invoke-static {v1, v0}, LX/1ih;->b(LX/1ih;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 298824
    iget-object v2, v1, LX/1ih;->g:LX/0ad;

    sget-short v3, LX/Ehe;->a:S

    invoke-interface {v2, v3, p2}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "351635951704203"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 298825
    const-string v2, "com.facebook.checkpoint.USER_IN_CHECKPOINT_RN"

    .line 298826
    :goto_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 298827
    const-string p0, "arg_is_checkpoint"

    const/4 p1, 0x1

    invoke-virtual {v3, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298828
    const-string p0, "arg_is_blocking_checkpoint"

    invoke-virtual {v3, p0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298829
    iget-object p0, v1, LX/1ih;->h:LX/0Xl;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {p0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 298830
    iput-object v0, v1, LX/1ih;->b:Ljava/lang/String;

    .line 298831
    :cond_2
    goto :goto_0

    .line 298832
    :cond_3
    const-string v2, "com.facebook.checkpoint.USER_IN_CHECKPOINT"

    goto :goto_1
.end method
