.class public LX/1GO;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Gf;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Gf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 225439
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Gf;
    .locals 12

    .prologue
    .line 225427
    sget-object v0, LX/1GO;->a:LX/1Gf;

    if-nez v0, :cond_1

    .line 225428
    const-class v1, LX/1GO;

    monitor-enter v1

    .line 225429
    :try_start_0
    sget-object v0, LX/1GO;->a:LX/1Gf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225430
    if-eqz v2, :cond_0

    .line 225431
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225432
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v4

    check-cast v4, LX/1GQ;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v5

    check-cast v5, LX/0pq;

    invoke-static {v0}, LX/1GT;->a(LX/0QB;)LX/1GT;

    move-result-object v6

    check-cast v6, LX/1GT;

    invoke-static {v0}, LX/1GV;->a(LX/0QB;)LX/1GE;

    move-result-object v7

    check-cast v7, LX/1GE;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/1GZ;->a(LX/0QB;)LX/1GZ;

    move-result-object v10

    check-cast v10, LX/1GZ;

    invoke-static {v0}, LX/1Gb;->a(LX/0QB;)LX/1Gb;

    move-result-object v11

    check-cast v11, LX/1Gb;

    invoke-static/range {v3 .. v11}, LX/1Aq;->a(Landroid/content/Context;LX/1GQ;LX/0pq;LX/1GT;LX/1GE;LX/0ad;LX/0Uh;LX/1GZ;LX/1Gb;)LX/1Gf;

    move-result-object v3

    move-object v0, v3

    .line 225433
    sput-object v0, LX/1GO;->a:LX/1Gf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225436
    :cond_1
    sget-object v0, LX/1GO;->a:LX/1Gf;

    return-object v0

    .line 225437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 225426
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v1

    check-cast v1, LX/1GQ;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v2

    check-cast v2, LX/0pq;

    invoke-static {p0}, LX/1GT;->a(LX/0QB;)LX/1GT;

    move-result-object v3

    check-cast v3, LX/1GT;

    invoke-static {p0}, LX/1GV;->a(LX/0QB;)LX/1GE;

    move-result-object v4

    check-cast v4, LX/1GE;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/1GZ;->a(LX/0QB;)LX/1GZ;

    move-result-object v7

    check-cast v7, LX/1GZ;

    invoke-static {p0}, LX/1Gb;->a(LX/0QB;)LX/1Gb;

    move-result-object v8

    check-cast v8, LX/1Gb;

    invoke-static/range {v0 .. v8}, LX/1Aq;->a(Landroid/content/Context;LX/1GQ;LX/0pq;LX/1GT;LX/1GE;LX/0ad;LX/0Uh;LX/1GZ;LX/1Gb;)LX/1Gf;

    move-result-object v0

    return-object v0
.end method
