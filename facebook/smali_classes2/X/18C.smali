.class public LX/18C;
.super Ljava/util/AbstractSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/FeedUnit;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 206023
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 206024
    iput-object p1, p0, LX/18C;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 206025
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 206009
    iget-object v0, p0, LX/18C;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 206010
    :goto_0
    return-void

    .line 206011
    :cond_0
    monitor-enter p0

    .line 206012
    :try_start_0
    iget-object v0, p0, LX/18C;->b:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 206013
    monitor-exit p0

    goto :goto_0

    .line 206014
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 206015
    :cond_1
    :try_start_1
    const-string v0, "TagSet.lazyInit"

    const v1, 0x185231e1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206016
    :try_start_2
    iget-object v0, p0, LX/18C;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/2vM;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/18C;->b:Ljava/util/Set;

    .line 206017
    const/4 v0, 0x0

    iput-object v0, p0, LX/18C;->a:Lcom/facebook/graphql/model/FeedUnit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206018
    const v0, -0x38179a0e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 206019
    monitor-exit p0

    goto :goto_0

    .line 206020
    :catchall_1
    move-exception v0

    const v1, -0x5e35972

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 206021
    invoke-direct {p0}, LX/18C;->b()V

    .line 206022
    iget-object v0, p0, LX/18C;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206005
    invoke-direct {p0}, LX/18C;->b()V

    .line 206006
    iget-object v0, p0, LX/18C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 206007
    invoke-direct {p0}, LX/18C;->b()V

    .line 206008
    iget-object v0, p0, LX/18C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
