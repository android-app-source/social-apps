.class public final enum LX/1Bu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Bu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Bu;

.field public static final enum DISABLED:LX/1Bu;

.field public static final enum DRY_RUN:LX/1Bu;

.field public static final enum ERROR_RESPONSE:LX/1Bu;

.field public static final enum EXCEPTION:LX/1Bu;

.field public static final enum EXPIRED:LX/1Bu;

.field public static final enum NOT_FINISH:LX/1Bu;

.field public static final enum NOT_HTML:LX/1Bu;

.field public static final enum NO_CONTEXT:LX/1Bu;

.field public static final enum NO_HOOKUP:LX/1Bu;

.field public static final enum NO_NETWORK:LX/1Bu;

.field public static final enum NO_VIDEO_AUTOPLAY:LX/1Bu;

.field public static final enum NO_VPV:LX/1Bu;

.field public static final enum RESOURCE_INIT_FAIL:LX/1Bu;

.field public static final enum STORE_ERROR:LX/1Bu;

.field public static final enum TIMEOUT:LX/1Bu;

.field public static final enum VPV_NOT_TRIGGERED:LX/1Bu;

.field public static final enum ZERO_RATING:LX/1Bu;


# instance fields
.field public extra:Ljava/lang/String;

.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 214356
    new-instance v0, LX/1Bu;

    const-string v1, "NO_HOOKUP"

    const-string v2, "no_hookup"

    invoke-direct {v0, v1, v4, v2}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NO_HOOKUP:LX/1Bu;

    .line 214357
    new-instance v0, LX/1Bu;

    const-string v1, "DISABLED"

    const-string v2, "disabled"

    invoke-direct {v0, v1, v5, v2}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->DISABLED:LX/1Bu;

    .line 214358
    new-instance v0, LX/1Bu;

    const-string v1, "NO_CONTEXT"

    const-string v2, "no_context"

    invoke-direct {v0, v1, v6, v2}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NO_CONTEXT:LX/1Bu;

    .line 214359
    new-instance v0, LX/1Bu;

    const-string v1, "NO_VPV"

    const-string v2, "no_vpv"

    invoke-direct {v0, v1, v7, v2}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NO_VPV:LX/1Bu;

    .line 214360
    new-instance v0, LX/1Bu;

    const-string v1, "VPV_NOT_TRIGGERED"

    const-string v2, "vpv_not_triggered"

    invoke-direct {v0, v1, v8, v2}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->VPV_NOT_TRIGGERED:LX/1Bu;

    .line 214361
    new-instance v0, LX/1Bu;

    const-string v1, "NO_NETWORK"

    const/4 v2, 0x5

    const-string v3, "no_network"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NO_NETWORK:LX/1Bu;

    .line 214362
    new-instance v0, LX/1Bu;

    const-string v1, "NO_VIDEO_AUTOPLAY"

    const/4 v2, 0x6

    const-string v3, "no_video_autoplay"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NO_VIDEO_AUTOPLAY:LX/1Bu;

    .line 214363
    new-instance v0, LX/1Bu;

    const-string v1, "RESOURCE_INIT_FAIL"

    const/4 v2, 0x7

    const-string v3, "res_init_fail"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->RESOURCE_INIT_FAIL:LX/1Bu;

    .line 214364
    new-instance v0, LX/1Bu;

    const-string v1, "TIMEOUT"

    const/16 v2, 0x8

    const-string v3, "timeout"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->TIMEOUT:LX/1Bu;

    .line 214365
    new-instance v0, LX/1Bu;

    const-string v1, "ERROR_RESPONSE"

    const/16 v2, 0x9

    const-string v3, "error_response"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->ERROR_RESPONSE:LX/1Bu;

    .line 214366
    new-instance v0, LX/1Bu;

    const-string v1, "EXPIRED"

    const/16 v2, 0xa

    const-string v3, "expired"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->EXPIRED:LX/1Bu;

    .line 214367
    new-instance v0, LX/1Bu;

    const-string v1, "STORE_ERROR"

    const/16 v2, 0xb

    const-string v3, "store_error"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->STORE_ERROR:LX/1Bu;

    .line 214368
    new-instance v0, LX/1Bu;

    const-string v1, "EXCEPTION"

    const/16 v2, 0xc

    const-string v3, "exception"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->EXCEPTION:LX/1Bu;

    .line 214369
    new-instance v0, LX/1Bu;

    const-string v1, "NOT_FINISH"

    const/16 v2, 0xd

    const-string v3, "not_finish"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NOT_FINISH:LX/1Bu;

    .line 214370
    new-instance v0, LX/1Bu;

    const-string v1, "NOT_HTML"

    const/16 v2, 0xe

    const-string v3, "not_html"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->NOT_HTML:LX/1Bu;

    .line 214371
    new-instance v0, LX/1Bu;

    const-string v1, "DRY_RUN"

    const/16 v2, 0xf

    const-string v3, "dry_run"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->DRY_RUN:LX/1Bu;

    .line 214372
    new-instance v0, LX/1Bu;

    const-string v1, "ZERO_RATING"

    const/16 v2, 0x10

    const-string v3, "zero_rating"

    invoke-direct {v0, v1, v2, v3}, LX/1Bu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Bu;->ZERO_RATING:LX/1Bu;

    .line 214373
    const/16 v0, 0x11

    new-array v0, v0, [LX/1Bu;

    sget-object v1, LX/1Bu;->NO_HOOKUP:LX/1Bu;

    aput-object v1, v0, v4

    sget-object v1, LX/1Bu;->DISABLED:LX/1Bu;

    aput-object v1, v0, v5

    sget-object v1, LX/1Bu;->NO_CONTEXT:LX/1Bu;

    aput-object v1, v0, v6

    sget-object v1, LX/1Bu;->NO_VPV:LX/1Bu;

    aput-object v1, v0, v7

    sget-object v1, LX/1Bu;->VPV_NOT_TRIGGERED:LX/1Bu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/1Bu;->NO_NETWORK:LX/1Bu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1Bu;->NO_VIDEO_AUTOPLAY:LX/1Bu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1Bu;->RESOURCE_INIT_FAIL:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1Bu;->TIMEOUT:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1Bu;->ERROR_RESPONSE:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1Bu;->EXPIRED:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1Bu;->STORE_ERROR:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1Bu;->EXCEPTION:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1Bu;->NOT_FINISH:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1Bu;->NOT_HTML:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1Bu;->DRY_RUN:LX/1Bu;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/1Bu;->ZERO_RATING:LX/1Bu;

    aput-object v2, v0, v1

    sput-object v0, LX/1Bu;->$VALUES:[LX/1Bu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 214351
    iput-object p3, p0, LX/1Bu;->value:Ljava/lang/String;

    .line 214352
    return-void
.end method

.method public static getCatchThemAllReasonInPrepare()LX/1Bu;
    .locals 1

    .prologue
    .line 214355
    sget-object v0, LX/1Bu;->VPV_NOT_TRIGGERED:LX/1Bu;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Bu;
    .locals 1

    .prologue
    .line 214374
    const-class v0, LX/1Bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Bu;

    return-object v0
.end method

.method public static values()[LX/1Bu;
    .locals 1

    .prologue
    .line 214354
    sget-object v0, LX/1Bu;->$VALUES:[LX/1Bu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Bu;

    return-object v0
.end method


# virtual methods
.method public final toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 214353
    iget-object v0, p0, LX/1Bu;->extra:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Bu;->value:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1Bu;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Bu;->extra:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
