.class public LX/1gT;
.super LX/0cD;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final b:LX/1gU;

.field private c:Z


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 294278
    sget-object v0, LX/0cB;->a:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, LX/0cD;-><init>(Landroid/net/Uri;I)V

    .line 294279
    new-instance v0, LX/1gU;

    sget-object v1, LX/0cB;->c:Landroid/net/Uri;

    const-string v2, "peer://msg_notification_chathead/active_threads"

    invoke-direct {v0, v1, v2}, LX/1gU;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    iput-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294280
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 294262
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294263
    iget-object v1, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 294264
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1gT;->c:Z

    .line 294265
    return-void
.end method

.method public final a(Landroid/net/Uri;LX/4gt;)V
    .locals 2

    .prologue
    .line 294266
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    invoke-virtual {v0, p1}, LX/1gU;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294267
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294268
    invoke-static {v0, p1}, LX/1gU;->b(LX/1gU;Landroid/net/Uri;)LX/0am;

    move-result-object v1

    .line 294269
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 294270
    iget-object p0, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, LX/4gt;->a:Ljava/lang/Object;

    .line 294271
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object p0, p2, LX/4gt;->a:Ljava/lang/Object;

    invoke-virtual {v1, p0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294272
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/4gt;->b:Z

    .line 294273
    :cond_0
    :goto_0
    return-void

    .line 294274
    :cond_1
    sget-object v0, LX/0cB;->d:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294275
    iget-boolean v0, p0, LX/1gT;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/4gt;->a:Ljava/lang/Object;

    .line 294276
    iget-boolean v0, p0, LX/1gT;->c:Z

    if-eqz v0, :cond_0

    .line 294277
    const/4 v0, 0x1

    iput-boolean v0, p2, LX/4gt;->b:Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 294258
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294259
    iget-object v1, v0, LX/1gU;->d:Ljava/lang/String;

    iget-object v2, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 294260
    sget-object v0, LX/0cB;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, LX/1gT;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 294261
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/Object;)Z
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 294236
    sget-object v2, LX/0cB;->b:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 294237
    invoke-virtual {p0}, LX/0cD;->a()V

    .line 294238
    :cond_0
    :goto_0
    return v0

    .line 294239
    :cond_1
    iget-object v2, p0, LX/1gT;->b:LX/1gU;

    invoke-virtual {v2, p1}, LX/1gU;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 294240
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294241
    iget-object v1, v0, LX/1gU;->a:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 294242
    iget-object v1, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 294243
    const/4 v1, 0x1

    .line 294244
    :goto_1
    move v0, v1

    .line 294245
    goto :goto_0

    .line 294246
    :cond_2
    sget-object v2, LX/0cB;->d:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 294247
    iget-boolean v2, p0, LX/1gT;->c:Z

    .line 294248
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, LX/1gT;->c:Z

    .line 294249
    iget-boolean v3, p0, LX/1gT;->c:Z

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 294250
    goto :goto_0

    .line 294251
    :cond_4
    invoke-static {v0, p1}, LX/1gU;->b(LX/1gU;Landroid/net/Uri;)LX/0am;

    move-result-object v1

    .line 294252
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 294253
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 294254
    if-eqz v2, :cond_5

    .line 294255
    iget-object v2, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 294256
    :cond_5
    iget-object v2, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 294257
    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 294227
    iget-object v0, p0, LX/1gT;->b:LX/1gU;

    .line 294228
    iget-object v1, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 294229
    iget-object v1, v0, LX/1gU;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 294230
    if-nez v1, :cond_1

    .line 294231
    const-class v1, LX/1gU;

    const-string v2, "%s should not be null in the bundle, it happened because some bad upgrade had happened."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, LX/1gU;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294232
    :cond_0
    sget-object v0, LX/0cB;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/1gT;->c:Z

    .line 294233
    return-void

    .line 294234
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 294235
    iget-object v3, v0, LX/1gU;->c:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
