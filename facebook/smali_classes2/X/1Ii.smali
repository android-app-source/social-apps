.class public LX/1Ii;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 229214
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;
    .locals 4

    .prologue
    .line 229215
    sget-object v0, LX/1Ii;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    if-nez v0, :cond_1

    .line 229216
    const-class v1, LX/1Ii;

    monitor-enter v1

    .line 229217
    :try_start_0
    sget-object v0, LX/1Ii;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 229218
    if-eqz v2, :cond_0

    .line 229219
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 229220
    const-class v3, LX/1Ij;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Ij;

    const-class p0, LX/1Ik;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/1Ik;

    invoke-static {v3, p0}, LX/1Il;->a(LX/1Ij;LX/1Ik;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v3

    move-object v0, v3

    .line 229221
    sput-object v0, LX/1Ii;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229222
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 229223
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229224
    :cond_1
    sget-object v0, LX/1Ii;->a:Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    return-object v0

    .line 229225
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 229226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 229227
    const-class v0, LX/1Ij;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/1Ij;

    const-class v1, LX/1Ik;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1Ik;

    invoke-static {v0, v1}, LX/1Il;->a(LX/1Ij;LX/1Ik;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v0

    return-object v0
.end method
