.class public LX/0yH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0yH;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0yI;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0dh;

.field private final f:LX/0yW;

.field private final g:LX/00H;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private volatile i:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yI;LX/0Or;LX/0Or;LX/0dh;LX/0yW;LX/00H;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneManualSwitcherFeatureAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0yI;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0dh;",
            "LX/0yW;",
            "LX/00H;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164301
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0yH;->h:Ljava/util/Map;

    .line 164302
    iput-object p1, p0, LX/0yH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 164303
    iput-object p2, p0, LX/0yH;->b:LX/0yI;

    .line 164304
    iput-object p3, p0, LX/0yH;->c:LX/0Or;

    .line 164305
    iput-object p4, p0, LX/0yH;->d:LX/0Or;

    .line 164306
    iput-object p5, p0, LX/0yH;->e:LX/0dh;

    .line 164307
    iput-object p6, p0, LX/0yH;->f:LX/0yW;

    .line 164308
    iput-object p7, p0, LX/0yH;->g:LX/00H;

    .line 164309
    return-void
.end method

.method public static a(LX/0QB;)LX/0yH;
    .locals 11

    .prologue
    .line 164287
    sget-object v0, LX/0yH;->j:LX/0yH;

    if-nez v0, :cond_1

    .line 164288
    const-class v1, LX/0yH;

    monitor-enter v1

    .line 164289
    :try_start_0
    sget-object v0, LX/0yH;->j:LX/0yH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164290
    if-eqz v2, :cond_0

    .line 164291
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164292
    new-instance v3, LX/0yH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v5

    check-cast v5, LX/0yI;

    const/16 v6, 0x14d1

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1486

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0dh;->a(LX/0QB;)LX/0dh;

    move-result-object v8

    check-cast v8, LX/0dh;

    invoke-static {v0}, LX/0yW;->a(LX/0QB;)LX/0yW;

    move-result-object v9

    check-cast v9, LX/0yW;

    const-class v10, LX/00H;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/00H;

    invoke-direct/range {v3 .. v10}, LX/0yH;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yI;LX/0Or;LX/0Or;LX/0dh;LX/0yW;LX/00H;)V

    .line 164293
    move-object v0, v3

    .line 164294
    sput-object v0, LX/0yH;->j:LX/0yH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164295
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164296
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164297
    :cond_1
    sget-object v0, LX/0yH;->j:LX/0yH;

    return-object v0

    .line 164298
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164299
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/0yH;)V
    .locals 7

    .prologue
    .line 164276
    iget-boolean v0, p0, LX/0yH;->i:Z

    if-nez v0, :cond_2

    .line 164277
    monitor-enter p0

    .line 164278
    :try_start_0
    iget-boolean v0, p0, LX/0yH;->i:Z

    if-nez v0, :cond_1

    .line 164279
    invoke-static {}, LX/0yY;->values()[LX/0yY;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 164280
    invoke-static {v3}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v3

    .line 164281
    iget-object v4, p0, LX/0yH;->h:Ljava/util/Map;

    iget-object v5, p0, LX/0yH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v6, 0x1

    invoke-interface {v5, v3, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164282
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164283
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yH;->i:Z

    .line 164284
    :cond_1
    monitor-exit p0

    .line 164285
    :cond_2
    return-void

    .line 164286
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 164275
    iget-object v0, p0, LX/0yH;->e:LX/0dh;

    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {v0, v1}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 164245
    invoke-direct {p0}, LX/0yH;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0yH;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0yY;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 164252
    sget-object v0, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/0yY;->DIALTONE_PHOTOCAP_ERROR:LX/0yY;

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v2

    .line 164253
    :goto_0
    return v0

    .line 164254
    :cond_1
    iget-object v0, p0, LX/0yH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 164255
    goto :goto_0

    .line 164256
    :cond_2
    invoke-static {p0}, LX/0yH;->c(LX/0yH;)V

    .line 164257
    iget-object v0, p0, LX/0yH;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/0yH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dQ;->r:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_3
    iget-object v0, p0, LX/0yH;->b:LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 164258
    iget-object v0, p0, LX/0yH;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    if-ne p1, v0, :cond_e

    iget-object v0, p0, LX/0yH;->b:LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    sget-object v3, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/0yH;->b:LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    sget-object v3, LX/0yY;->MESSAGE_CAPPING:LX/0yY;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_4
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 164259
    if-eqz v0, :cond_c

    .line 164260
    :cond_5
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, LX/0yH;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 164261
    goto :goto_0

    .line 164262
    :cond_6
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, LX/0yH;->e()Z

    move-result v0

    if-nez v0, :cond_8

    .line 164263
    iget-object v0, p0, LX/0yH;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 164264
    :cond_8
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 164265
    invoke-direct {p0}, LX/0yH;->f()Z

    move-result v0

    goto/16 :goto_0

    .line 164266
    :cond_9
    iget-object v0, p0, LX/0yH;->g:LX/00H;

    .line 164267
    iget-object v3, v0, LX/00H;->j:LX/01T;

    move-object v0, v3

    .line 164268
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v3, :cond_a

    iget-object v0, p0, LX/0yH;->b:LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    sget-object v3, LX/0yY;->MESSENGER_ZERO_BALANCE_DETECTION:LX/0yY;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/0yH;->f:LX/0yW;

    invoke-virtual {v0}, LX/0yW;->a()LX/2XZ;

    move-result-object v0

    sget-object v3, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    if-eq v0, v3, :cond_a

    move v0, v1

    .line 164269
    goto/16 :goto_0

    .line 164270
    :cond_a
    invoke-static {p1}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v0

    .line 164271
    iget-object v1, p0, LX/0yH;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, LX/0yH;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto/16 :goto_0

    .line 164272
    :cond_c
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 164273
    invoke-direct {p0}, LX/0yH;->f()Z

    move-result v0

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 164274
    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final b()LX/0yY;
    .locals 1

    .prologue
    .line 164246
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {p0, v0}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164247
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    .line 164248
    :goto_0
    return-object v0

    .line 164249
    :cond_0
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p0, v0}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164250
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    goto :goto_0

    .line 164251
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
