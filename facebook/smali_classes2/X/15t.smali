.class public final LX/15t;
.super LX/15u;
.source ""


# instance fields
.field public M:Ljava/io/Reader;

.field public N:[C

.field public O:LX/0lD;

.field public final P:LX/0lt;

.field public final Q:I

.field public R:Z


# direct methods
.method public constructor <init>(LX/12A;ILjava/io/Reader;LX/0lD;LX/0lt;)V
    .locals 1

    .prologue
    .line 182063
    invoke-direct {p0, p1, p2}, LX/15u;-><init>(LX/12A;I)V

    .line 182064
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 182065
    iput-object p3, p0, LX/15t;->M:Ljava/io/Reader;

    .line 182066
    invoke-virtual {p1}, LX/12A;->g()[C

    move-result-object v0

    iput-object v0, p0, LX/15t;->N:[C

    .line 182067
    iput-object p4, p0, LX/15t;->O:LX/0lD;

    .line 182068
    iput-object p5, p0, LX/15t;->P:LX/0lt;

    .line 182069
    iget v0, p5, LX/0lt;->l:I

    move v0, v0

    .line 182070
    iput v0, p0, LX/15t;->Q:I

    .line 182071
    return-void
.end method

.method private V()LX/15z;
    .locals 4

    .prologue
    .line 182072
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->p:Z

    .line 182073
    iget-object v0, p0, LX/15u;->m:LX/15z;

    .line 182074
    const/4 v1, 0x0

    iput-object v1, p0, LX/15t;->m:LX/15z;

    .line 182075
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_1

    .line 182076
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/15t;->l:LX/15y;

    .line 182077
    :cond_0
    :goto_0
    iput-object v0, p0, LX/15t;->K:LX/15z;

    return-object v0

    .line 182078
    :cond_1
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 182079
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/15t;->l:LX/15y;

    goto :goto_0
.end method

.method private W()C
    .locals 5

    .prologue
    const/16 v4, 0x39

    const/16 v1, 0x30

    .line 182080
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 182081
    :cond_0
    :goto_0
    return v0

    .line 182082
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    aget-char v0, v0, v2

    .line 182083
    if-lt v0, v1, :cond_2

    if-le v0, v4, :cond_3

    :cond_2
    move v0, v1

    .line 182084
    goto :goto_0

    .line 182085
    :cond_3
    sget-object v2, LX/0lr;->ALLOW_NUMERIC_LEADING_ZEROS:LX/0lr;

    invoke-virtual {p0, v2}, LX/15w;->a(LX/0lr;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 182086
    const-string v2, "Leading zeroes not allowed"

    invoke-virtual {p0, v2}, LX/15u;->c(Ljava/lang/String;)V

    .line 182087
    :cond_4
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/15t;->d:I

    .line 182088
    if-ne v0, v1, :cond_0

    .line 182089
    :cond_5
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_6

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182090
    :cond_6
    iget-object v0, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    aget-char v0, v0, v2

    .line 182091
    if-lt v0, v1, :cond_7

    if-le v0, v4, :cond_8

    :cond_7
    move v0, v1

    .line 182092
    goto :goto_0

    .line 182093
    :cond_8
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/15t;->d:I

    .line 182094
    if-eq v0, v1, :cond_5

    goto :goto_0
.end method

.method private X()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x27

    .line 182095
    iget v1, p0, LX/15u;->d:I

    .line 182096
    iget v0, p0, LX/15t;->Q:I

    .line 182097
    iget v2, p0, LX/15u;->e:I

    .line 182098
    if-ge v1, v2, :cond_3

    .line 182099
    sget-object v3, LX/12H;->a:[I

    move-object v3, v3

    .line 182100
    array-length v4, v3

    .line 182101
    :cond_0
    iget-object v5, p0, LX/15t;->N:[C

    aget-char v5, v5, v1

    .line 182102
    if-ne v5, v7, :cond_1

    .line 182103
    iget v2, p0, LX/15u;->d:I

    .line 182104
    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/15t;->d:I

    .line 182105
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    iget-object v4, p0, LX/15t;->N:[C

    sub-int/2addr v1, v2

    invoke-virtual {v3, v4, v2, v1, v0}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    .line 182106
    :goto_0
    return-object v0

    .line 182107
    :cond_1
    if-ge v5, v4, :cond_2

    aget v6, v3, v5

    if-nez v6, :cond_3

    .line 182108
    :cond_2
    mul-int/lit8 v0, v0, 0x21

    add-int/2addr v0, v5

    .line 182109
    add-int/lit8 v1, v1, 0x1

    .line 182110
    if-lt v1, v2, :cond_0

    .line 182111
    :cond_3
    iget v2, p0, LX/15u;->d:I

    .line 182112
    iput v1, p0, LX/15t;->d:I

    .line 182113
    invoke-direct {p0, v2, v0, v7}, LX/15t;->a(III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Y()LX/15z;
    .locals 7

    .prologue
    const/16 v6, 0x5c

    const/16 v5, 0x27

    .line 182114
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v1

    .line 182115
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182116
    iget v2, v0, LX/15x;->j:I

    move v0, v2

    .line 182117
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_0

    .line 182118
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-nez v2, :cond_0

    .line 182119
    const-string v2, ": was expecting closing quote for a string value"

    invoke-virtual {p0, v2}, LX/15v;->d(Ljava/lang/String;)V

    .line 182120
    :cond_0
    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v2, v2, v3

    .line 182121
    if-gt v2, v6, :cond_1

    .line 182122
    if-ne v2, v6, :cond_2

    .line 182123
    invoke-virtual {p0}, LX/15t;->R()C

    move-result v2

    .line 182124
    :cond_1
    :goto_1
    array-length v3, v1

    if-lt v0, v3, :cond_4

    .line 182125
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 182126
    const/4 v0, 0x0

    move v3, v0

    .line 182127
    :goto_2
    add-int/lit8 v0, v3, 0x1

    aput-char v2, v1, v3

    goto :goto_0

    .line 182128
    :cond_2
    if-gt v2, v5, :cond_1

    .line 182129
    if-eq v2, v5, :cond_3

    .line 182130
    const/16 v3, 0x20

    if-ge v2, v3, :cond_1

    .line 182131
    const-string v3, "string value"

    invoke-virtual {p0, v2, v3}, LX/15v;->c(ILjava/lang/String;)V

    goto :goto_1

    .line 182132
    :cond_3
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 182133
    iput v0, v1, LX/15x;->j:I

    .line 182134
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    return-object v0

    :cond_4
    move v3, v0

    goto :goto_2
.end method

.method private Z()V
    .locals 7

    .prologue
    const/16 v6, 0x5c

    const/16 v5, 0x22

    .line 182135
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->k()[C

    move-result-object v1

    .line 182136
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182137
    iget v2, v0, LX/15x;->j:I

    move v0, v2

    .line 182138
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_0

    .line 182139
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-nez v2, :cond_0

    .line 182140
    const-string v2, ": was expecting closing quote for a string value"

    invoke-virtual {p0, v2}, LX/15v;->d(Ljava/lang/String;)V

    .line 182141
    :cond_0
    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v2, v2, v3

    .line 182142
    if-gt v2, v6, :cond_1

    .line 182143
    if-ne v2, v6, :cond_2

    .line 182144
    invoke-virtual {p0}, LX/15t;->R()C

    move-result v2

    .line 182145
    :cond_1
    :goto_1
    array-length v3, v1

    if-lt v0, v3, :cond_4

    .line 182146
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 182147
    const/4 v0, 0x0

    move v3, v0

    .line 182148
    :goto_2
    add-int/lit8 v0, v3, 0x1

    aput-char v2, v1, v3

    goto :goto_0

    .line 182149
    :cond_2
    if-gt v2, v5, :cond_1

    .line 182150
    if-eq v2, v5, :cond_3

    .line 182151
    const/16 v3, 0x20

    if-ge v2, v3, :cond_1

    .line 182152
    const-string v3, "string value"

    invoke-virtual {p0, v2, v3}, LX/15v;->c(ILjava/lang/String;)V

    goto :goto_1

    .line 182153
    :cond_3
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 182154
    iput v0, v1, LX/15x;->j:I

    .line 182155
    return-void

    :cond_4
    move v3, v0

    goto :goto_2
.end method

.method private a(IZ)LX/15z;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 182156
    const/16 v0, 0x49

    if-ne p1, v0, :cond_4

    .line 182157
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 182158
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182159
    invoke-virtual {p0}, LX/15v;->T()V

    .line 182160
    :cond_0
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v6, v1, 0x1

    iput v6, p0, LX/15t;->d:I

    aget-char p1, v0, v1

    .line 182161
    const/16 v0, 0x4e

    if-ne p1, v0, :cond_5

    .line 182162
    if-eqz p2, :cond_1

    const-string v0, "-INF"

    .line 182163
    :goto_0
    invoke-direct {p0, v0, v7}, LX/15t;->a(Ljava/lang/String;I)V

    .line 182164
    sget-object v1, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v1}, LX/15w;->a(LX/0lr;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 182165
    if-eqz p2, :cond_2

    :goto_1
    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    .line 182166
    :goto_2
    return-object v0

    .line 182167
    :cond_1
    const-string v0, "+INF"

    goto :goto_0

    :cond_2
    move-wide v2, v4

    .line 182168
    goto :goto_1

    .line 182169
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non-standard token \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182170
    :cond_4
    :goto_3
    const-string v0, "expected digit (0-9) to follow minus sign, for valid numeric value"

    invoke-virtual {p0, p1, v0}, LX/15u;->a(ILjava/lang/String;)V

    .line 182171
    const/4 v0, 0x0

    goto :goto_2

    .line 182172
    :cond_5
    const/16 v0, 0x6e

    if-ne p1, v0, :cond_4

    .line 182173
    if-eqz p2, :cond_6

    const-string v0, "-Infinity"

    .line 182174
    :goto_4
    invoke-direct {p0, v0, v7}, LX/15t;->a(Ljava/lang/String;I)V

    .line 182175
    sget-object v1, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v1}, LX/15w;->a(LX/0lr;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 182176
    if-eqz p2, :cond_7

    :goto_5
    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    goto :goto_2

    .line 182177
    :cond_6
    const-string v0, "+Infinity"

    goto :goto_4

    :cond_7
    move-wide v2, v4

    .line 182178
    goto :goto_5

    .line 182179
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non-standard token \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private a(III)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x5c

    .line 182180
    iget-object v0, p0, LX/15u;->n:LX/15x;

    iget-object v1, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    sub-int/2addr v2, p1

    invoke-virtual {v0, v1, p1, v2}, LX/15x;->a([CII)V

    .line 182181
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->k()[C

    move-result-object v1

    .line 182182
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182183
    iget v2, v0, LX/15x;->j:I

    move v0, v2

    .line 182184
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_0

    .line 182185
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-nez v2, :cond_0

    .line 182186
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ": was expecting closing \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-char v3, p3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' for name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/15v;->d(Ljava/lang/String;)V

    .line 182187
    :cond_0
    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v3, v2, v3

    .line 182188
    if-gt v3, v5, :cond_2

    .line 182189
    if-ne v3, v5, :cond_1

    .line 182190
    invoke-virtual {p0}, LX/15t;->R()C

    move-result v2

    .line 182191
    :goto_1
    mul-int/lit8 v4, p2, 0x21

    add-int p2, v4, v3

    .line 182192
    add-int/lit8 v3, v0, 0x1

    aput-char v2, v1, v0

    .line 182193
    array-length v0, v1

    if-lt v3, v0, :cond_4

    .line 182194
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 182195
    const/4 v0, 0x0

    goto :goto_0

    .line 182196
    :cond_1
    if-gt v3, p3, :cond_2

    .line 182197
    if-eq v3, p3, :cond_3

    .line 182198
    const/16 v2, 0x20

    if-ge v3, v2, :cond_2

    .line 182199
    const-string v2, "name"

    invoke-virtual {p0, v3, v2}, LX/15v;->c(ILjava/lang/String;)V

    :cond_2
    move v2, v3

    goto :goto_1

    .line 182200
    :cond_3
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 182201
    iput v0, v1, LX/15x;->j:I

    .line 182202
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182203
    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v1

    .line 182204
    invoke-virtual {v0}, LX/15x;->d()I

    move-result v2

    .line 182205
    invoke-virtual {v0}, LX/15x;->c()I

    move-result v0

    .line 182206
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    invoke-virtual {v3, v1, v2, v0, p2}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method private a(II[I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 182207
    iget-object v0, p0, LX/15u;->n:LX/15x;

    iget-object v1, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    sub-int/2addr v2, p1

    invoke-virtual {v0, v1, p1, v2}, LX/15x;->a([CII)V

    .line 182208
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->k()[C

    move-result-object v1

    .line 182209
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182210
    iget v2, v0, LX/15x;->j:I

    move v0, v2

    .line 182211
    array-length v3, p3

    .line 182212
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->e:I

    if-lt v2, v4, :cond_0

    .line 182213
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182214
    :cond_0
    iget-object v2, p0, LX/15t;->N:[C

    iget v4, p0, LX/15u;->d:I

    aget-char v4, v2, v4

    .line 182215
    if-gt v4, v3, :cond_2

    .line 182216
    aget v2, p3, v4

    if-eqz v2, :cond_3

    .line 182217
    :cond_1
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 182218
    iput v0, v1, LX/15x;->j:I

    .line 182219
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182220
    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v1

    .line 182221
    invoke-virtual {v0}, LX/15x;->d()I

    move-result v2

    .line 182222
    invoke-virtual {v0}, LX/15x;->c()I

    move-result v0

    .line 182223
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    invoke-virtual {v3, v1, v2, v0, p2}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 182224
    :cond_2
    invoke-static {v4}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182225
    :cond_3
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/15t;->d:I

    .line 182226
    mul-int/lit8 v2, p2, 0x21

    add-int p2, v2, v4

    .line 182227
    add-int/lit8 v2, v0, 0x1

    aput-char v4, v1, v0

    .line 182228
    array-length v0, v1

    if-lt v2, v0, :cond_4

    .line 182229
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 182230
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private a(LX/15z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 182231
    if-nez p1, :cond_0

    .line 182232
    const/4 v0, 0x0

    .line 182233
    :goto_0
    return-object v0

    .line 182234
    :cond_0
    sget-object v0, LX/32w;->a:[I

    invoke-virtual {p1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182235
    invoke-virtual {p1}, LX/15z;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182236
    :pswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182237
    :pswitch_1
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 182238
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 182239
    :cond_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_1

    .line 182240
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v1

    if-nez v1, :cond_1

    .line 182241
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/15t;->h(Ljava/lang/String;)V

    .line 182242
    :cond_1
    iget-object v1, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    aget-char v1, v1, v2

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_2

    .line 182243
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/15t;->h(Ljava/lang/String;)V

    .line 182244
    :cond_2
    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/15t;->d:I

    .line 182245
    add-int/lit8 p2, p2, 0x1

    if-lt p2, v0, :cond_0

    .line 182246
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_4

    .line 182247
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_4

    .line 182248
    :cond_3
    :goto_0
    return-void

    .line 182249
    :cond_4
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    aget-char v0, v0, v1

    .line 182250
    const/16 v1, 0x30

    if-lt v0, v1, :cond_3

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_3

    .line 182251
    invoke-static {v0}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182252
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/15t;->h(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aa()V
    .locals 7

    .prologue
    const/16 v6, 0x5c

    const/16 v5, 0x22

    .line 182284
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 182285
    iget v1, p0, LX/15u;->d:I

    .line 182286
    iget v0, p0, LX/15u;->e:I

    .line 182287
    iget-object v3, p0, LX/15t;->N:[C

    .line 182288
    :goto_0
    if-lt v1, v0, :cond_1

    .line 182289
    iput v1, p0, LX/15t;->d:I

    .line 182290
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182291
    const-string v0, ": was expecting closing quote for a string value"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 182292
    :cond_0
    iget v1, p0, LX/15u;->d:I

    .line 182293
    iget v0, p0, LX/15u;->e:I

    .line 182294
    :cond_1
    add-int/lit8 v2, v1, 0x1

    aget-char v1, v3, v1

    .line 182295
    if-gt v1, v6, :cond_4

    .line 182296
    if-ne v1, v6, :cond_2

    .line 182297
    iput v2, p0, LX/15t;->d:I

    .line 182298
    invoke-virtual {p0}, LX/15t;->R()C

    .line 182299
    iget v1, p0, LX/15u;->d:I

    .line 182300
    iget v0, p0, LX/15u;->e:I

    goto :goto_0

    .line 182301
    :cond_2
    if-gt v1, v5, :cond_4

    .line 182302
    if-ne v1, v5, :cond_3

    .line 182303
    iput v2, p0, LX/15t;->d:I

    .line 182304
    return-void

    .line 182305
    :cond_3
    const/16 v4, 0x20

    if-ge v1, v4, :cond_4

    .line 182306
    iput v2, p0, LX/15t;->d:I

    .line 182307
    const-string v4, "string value"

    invoke-virtual {p0, v1, v4}, LX/15v;->c(ILjava/lang/String;)V

    :cond_4
    move v1, v2

    .line 182308
    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 182253
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182254
    :cond_0
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 182255
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15t;->d:I

    .line 182256
    :cond_1
    iget v0, p0, LX/15u;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15t;->g:I

    .line 182257
    iget v0, p0, LX/15u;->d:I

    iput v0, p0, LX/15t;->h:I

    .line 182258
    return-void
.end method

.method private ac()V
    .locals 1

    .prologue
    .line 182411
    iget v0, p0, LX/15u;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15t;->g:I

    .line 182412
    iget v0, p0, LX/15u;->d:I

    iput v0, p0, LX/15t;->h:I

    .line 182413
    return-void
.end method

.method private ad()I
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 182397
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182398
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    .line 182399
    if-le v0, v3, :cond_3

    .line 182400
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    .line 182401
    return v0

    .line 182402
    :cond_2
    invoke-direct {p0}, LX/15t;->af()V

    goto :goto_0

    .line 182403
    :cond_3
    if-eq v0, v3, :cond_0

    .line 182404
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 182405
    invoke-direct {p0}, LX/15t;->ac()V

    goto :goto_0

    .line 182406
    :cond_4
    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    .line 182407
    invoke-direct {p0}, LX/15t;->ab()V

    goto :goto_0

    .line 182408
    :cond_5
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 182409
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0

    .line 182410
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected end-of-input within/between "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0
.end method

.method private ae()I
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 182383
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 182384
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    .line 182385
    if-le v0, v3, :cond_2

    .line 182386
    const/16 v1, 0x2f

    if-ne v0, v1, :cond_6

    .line 182387
    invoke-direct {p0}, LX/15t;->af()V

    goto :goto_0

    .line 182388
    :cond_2
    if-eq v0, v3, :cond_0

    .line 182389
    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 182390
    invoke-direct {p0}, LX/15t;->ac()V

    goto :goto_0

    .line 182391
    :cond_3
    const/16 v1, 0xd

    if-ne v0, v1, :cond_4

    .line 182392
    invoke-direct {p0}, LX/15t;->ab()V

    goto :goto_0

    .line 182393
    :cond_4
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 182394
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0

    .line 182395
    :cond_5
    invoke-virtual {p0}, LX/15u;->P()V

    .line 182396
    const/4 v0, -0x1

    :cond_6
    return v0
.end method

.method private af()V
    .locals 4

    .prologue
    const/16 v3, 0x2f

    .line 182372
    sget-object v0, LX/0lr;->ALLOW_COMMENTS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182373
    const-string v0, "maybe a (non-standard) comment? (not recognized as one since Feature \'ALLOW_COMMENTS\' not enabled for parser)"

    invoke-virtual {p0, v3, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 182374
    :cond_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182375
    const-string v0, " in a comment"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 182376
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    .line 182377
    if-ne v0, v3, :cond_2

    .line 182378
    invoke-direct {p0}, LX/15t;->ah()V

    .line 182379
    :goto_0
    return-void

    .line 182380
    :cond_2
    const/16 v1, 0x2a

    if-ne v0, v1, :cond_3

    .line 182381
    invoke-direct {p0}, LX/15t;->ag()V

    goto :goto_0

    .line 182382
    :cond_3
    const-string v1, "was expecting either \'*\' or \'/\' for a comment"

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private ag()V
    .locals 4

    .prologue
    const/16 v3, 0x2a

    .line 182414
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182415
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    .line 182416
    if-gt v0, v3, :cond_0

    .line 182417
    if-ne v0, v3, :cond_3

    .line 182418
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182419
    :cond_2
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_0

    .line 182420
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15t;->d:I

    .line 182421
    :goto_1
    return-void

    .line 182422
    :cond_3
    const/16 v1, 0x20

    if-ge v0, v1, :cond_0

    .line 182423
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 182424
    invoke-direct {p0}, LX/15t;->ac()V

    goto :goto_0

    .line 182425
    :cond_4
    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    .line 182426
    invoke-direct {p0}, LX/15t;->ab()V

    goto :goto_0

    .line 182427
    :cond_5
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 182428
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0

    .line 182429
    :cond_6
    const-string v0, " in a comment"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 182362
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182363
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    .line 182364
    const/16 v1, 0x20

    if-ge v0, v1, :cond_0

    .line 182365
    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 182366
    invoke-direct {p0}, LX/15t;->ac()V

    .line 182367
    :cond_2
    :goto_1
    return-void

    .line 182368
    :cond_3
    const/16 v1, 0xd

    if-ne v0, v1, :cond_4

    .line 182369
    invoke-direct {p0}, LX/15t;->ab()V

    goto :goto_1

    .line 182370
    :cond_4
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 182371
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0
.end method

.method private b(Z)LX/15z;
    .locals 14

    .prologue
    const/16 v10, 0x2d

    const/16 v12, 0x39

    const/16 v11, 0x30

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182309
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v4

    .line 182310
    if-eqz p1, :cond_18

    .line 182311
    aput-char v10, v4, v2

    move v0, v1

    .line 182312
    :goto_0
    iget v3, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-ge v3, v5, :cond_a

    iget-object v3, p0, LX/15t;->N:[C

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/15t;->d:I

    aget-char v3, v3, v5

    .line 182313
    :goto_1
    if-ne v3, v11, :cond_0

    .line 182314
    invoke-direct {p0}, LX/15t;->W()C

    move-result v3

    :cond_0
    move v5, v2

    move v13, v3

    move-object v3, v4

    move v4, v13

    .line 182315
    :goto_2
    if-lt v4, v11, :cond_17

    if-gt v4, v12, :cond_17

    .line 182316
    add-int/lit8 v5, v5, 0x1

    .line 182317
    array-length v6, v3

    if-lt v0, v6, :cond_1

    .line 182318
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 182319
    :cond_1
    add-int/lit8 v6, v0, 0x1

    aput-char v4, v3, v0

    .line 182320
    iget v0, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->e:I

    if-lt v0, v4, :cond_b

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_b

    move v7, v1

    move v0, v2

    move v9, v5

    move-object v4, v3

    move v5, v6

    .line 182321
    :goto_3
    if-nez v9, :cond_2

    .line 182322
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Missing integer part (next char "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/15u;->c(Ljava/lang/String;)V

    .line 182323
    :cond_2
    const/16 v3, 0x2e

    if-ne v0, v3, :cond_16

    .line 182324
    add-int/lit8 v3, v5, 0x1

    aput-char v0, v4, v5

    move-object v5, v4

    move v4, v3

    move v3, v0

    move v0, v2

    .line 182325
    :goto_4
    iget v6, p0, LX/15u;->d:I

    iget v8, p0, LX/15u;->e:I

    if-lt v6, v8, :cond_c

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v6

    if-nez v6, :cond_c

    move v6, v1

    .line 182326
    :goto_5
    if-nez v0, :cond_3

    .line 182327
    const-string v7, "Decimal point not followed by a digit"

    invoke-virtual {p0, v3, v7}, LX/15u;->a(ILjava/lang/String;)V

    :cond_3
    move v8, v0

    move v0, v4

    move v13, v3

    move-object v3, v5

    move v5, v13

    .line 182328
    :goto_6
    const/16 v4, 0x65

    if-eq v5, v4, :cond_4

    const/16 v4, 0x45

    if-ne v5, v4, :cond_13

    .line 182329
    :cond_4
    array-length v4, v3

    if-lt v0, v4, :cond_5

    .line 182330
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 182331
    :cond_5
    add-int/lit8 v4, v0, 0x1

    aput-char v5, v3, v0

    .line 182332
    iget v0, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-ge v0, v5, :cond_d

    iget-object v0, p0, LX/15t;->N:[C

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, LX/15t;->d:I

    aget-char v5, v0, v5

    .line 182333
    :goto_7
    if-eq v5, v10, :cond_6

    const/16 v0, 0x2b

    if-ne v5, v0, :cond_12

    .line 182334
    :cond_6
    array-length v0, v3

    if-lt v4, v0, :cond_11

    .line 182335
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v3

    move v0, v2

    .line 182336
    :goto_8
    add-int/lit8 v4, v0, 0x1

    aput-char v5, v3, v0

    .line 182337
    iget v0, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-ge v0, v5, :cond_e

    iget-object v0, p0, LX/15t;->N:[C

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, LX/15t;->d:I

    aget-char v0, v0, v5

    move-object v5, v3

    move v3, v2

    :goto_9
    move v7, v0

    move v0, v4

    .line 182338
    :goto_a
    if-gt v7, v12, :cond_10

    if-lt v7, v11, :cond_10

    .line 182339
    add-int/lit8 v3, v3, 0x1

    .line 182340
    array-length v4, v5

    if-lt v0, v4, :cond_7

    .line 182341
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v5

    move v0, v2

    .line 182342
    :cond_7
    add-int/lit8 v4, v0, 0x1

    aput-char v7, v5, v0

    .line 182343
    iget v0, p0, LX/15u;->d:I

    iget v10, p0, LX/15u;->e:I

    if-lt v0, v10, :cond_f

    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_f

    move v2, v3

    move v0, v1

    move v1, v4

    .line 182344
    :goto_b
    if-nez v2, :cond_8

    .line 182345
    const-string v3, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v7, v3}, LX/15u;->a(ILjava/lang/String;)V

    .line 182346
    :cond_8
    :goto_c
    if-nez v0, :cond_9

    .line 182347
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/15t;->d:I

    .line 182348
    :cond_9
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 182349
    iput v1, v0, LX/15x;->j:I

    .line 182350
    invoke-virtual {p0, p1, v9, v8, v2}, LX/15u;->a(ZIII)LX/15z;

    move-result-object v0

    return-object v0

    .line 182351
    :cond_a
    const-string v3, "No digit following minus sign"

    invoke-direct {p0, v3}, LX/15t;->f(Ljava/lang/String;)C

    move-result v3

    goto/16 :goto_1

    .line 182352
    :cond_b
    iget-object v0, p0, LX/15t;->N:[C

    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v7, v4, 0x1

    iput v7, p0, LX/15t;->d:I

    aget-char v4, v0, v4

    move v0, v6

    goto/16 :goto_2

    .line 182353
    :cond_c
    iget-object v3, p0, LX/15t;->N:[C

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, LX/15t;->d:I

    aget-char v3, v3, v6

    .line 182354
    if-lt v3, v11, :cond_15

    if-gt v3, v12, :cond_15

    .line 182355
    add-int/lit8 v0, v0, 0x1

    .line 182356
    array-length v6, v5

    if-lt v4, v6, :cond_14

    .line 182357
    iget-object v4, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v4}, LX/15x;->n()[C

    move-result-object v5

    move v6, v2

    .line 182358
    :goto_d
    add-int/lit8 v4, v6, 0x1

    aput-char v3, v5, v6

    goto/16 :goto_4

    .line 182359
    :cond_d
    const-string v0, "expected a digit for number exponent"

    invoke-direct {p0, v0}, LX/15t;->f(Ljava/lang/String;)C

    move-result v5

    goto/16 :goto_7

    .line 182360
    :cond_e
    const-string v0, "expected a digit for number exponent"

    invoke-direct {p0, v0}, LX/15t;->f(Ljava/lang/String;)C

    move-result v0

    move-object v5, v3

    move v3, v2

    goto/16 :goto_9

    .line 182361
    :cond_f
    iget-object v0, p0, LX/15t;->N:[C

    iget v7, p0, LX/15u;->d:I

    add-int/lit8 v10, v7, 0x1

    iput v10, p0, LX/15t;->d:I

    aget-char v0, v0, v7

    goto/16 :goto_9

    :cond_10
    move v2, v3

    move v1, v0

    move v0, v6

    goto :goto_b

    :cond_11
    move v0, v4

    goto/16 :goto_8

    :cond_12
    move v7, v5

    move v0, v4

    move-object v5, v3

    move v3, v2

    goto/16 :goto_a

    :cond_13
    move v1, v0

    move v0, v6

    goto :goto_c

    :cond_14
    move v6, v4

    goto :goto_d

    :cond_15
    move v6, v7

    goto/16 :goto_5

    :cond_16
    move v8, v2

    move v6, v7

    move-object v3, v4

    move v13, v5

    move v5, v0

    move v0, v13

    goto/16 :goto_6

    :cond_17
    move v7, v2

    move v9, v5

    move v5, v0

    move v0, v4

    move-object v4, v3

    goto/16 :goto_3

    :cond_18
    move v0, v2

    goto/16 :goto_0
.end method

.method private b(LX/0ln;)[B
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x22

    const/4 v5, -0x2

    .line 181958
    invoke-virtual {p0}, LX/15u;->Q()LX/2SG;

    move-result-object v2

    .line 181959
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    .line 181960
    invoke-virtual {p0}, LX/15u;->K()V

    .line 181961
    :cond_1
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/15t;->d:I

    aget-char v1, v0, v1

    .line 181962
    const/16 v0, 0x20

    if-le v1, v0, :cond_0

    .line 181963
    invoke-virtual {p1, v1}, LX/0ln;->b(C)I

    move-result v0

    .line 181964
    if-gez v0, :cond_3

    .line 181965
    if-ne v1, v6, :cond_2

    .line 181966
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    .line 181967
    :goto_1
    return-object v0

    .line 181968
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, LX/15u;->a(LX/0ln;CI)I

    move-result v0

    .line 181969
    if-ltz v0, :cond_0

    .line 181970
    :cond_3
    iget v1, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v1, v3, :cond_4

    .line 181971
    invoke-virtual {p0}, LX/15u;->K()V

    .line 181972
    :cond_4
    iget-object v1, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v3, v1, v3

    .line 181973
    invoke-virtual {p1, v3}, LX/0ln;->b(C)I

    move-result v1

    .line 181974
    if-gez v1, :cond_5

    .line 181975
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v3, v1}, LX/15u;->a(LX/0ln;CI)I

    move-result v1

    .line 181976
    :cond_5
    shl-int/lit8 v0, v0, 0x6

    or-int/2addr v1, v0

    .line 181977
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_6

    .line 181978
    invoke-virtual {p0}, LX/15u;->K()V

    .line 181979
    :cond_6
    iget-object v0, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v3, v0, v3

    .line 181980
    invoke-virtual {p1, v3}, LX/0ln;->b(C)I

    move-result v0

    .line 181981
    if-gez v0, :cond_b

    .line 181982
    if-eq v0, v5, :cond_8

    .line 181983
    if-ne v3, v6, :cond_7

    .line 181984
    iget-boolean v0, p1, LX/0ln;->a:Z

    move v0, v0

    .line 181985
    if-nez v0, :cond_7

    .line 181986
    shr-int/lit8 v0, v1, 0x4

    .line 181987
    invoke-virtual {v2, v0}, LX/2SG;->a(I)V

    .line 181988
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    goto :goto_1

    .line 181989
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v3, v0}, LX/15u;->a(LX/0ln;CI)I

    move-result v0

    .line 181990
    :cond_8
    if-ne v0, v5, :cond_b

    .line 181991
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_9

    .line 181992
    invoke-virtual {p0}, LX/15u;->K()V

    .line 181993
    :cond_9
    iget-object v0, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v0, v0, v3

    .line 181994
    invoke-virtual {p1, v0}, LX/0ln;->a(C)Z

    move-result v3

    if-nez v3, :cond_a

    .line 181995
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected padding character \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 181996
    iget-char v2, p1, LX/0ln;->b:C

    move v2, v2

    .line 181997
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v7, v1}, LX/15u;->a(LX/0ln;IILjava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 181998
    :cond_a
    shr-int/lit8 v0, v1, 0x4

    .line 181999
    invoke-virtual {v2, v0}, LX/2SG;->a(I)V

    goto/16 :goto_0

    .line 182000
    :cond_b
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v1, v0

    .line 182001
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_c

    .line 182002
    invoke-virtual {p0}, LX/15u;->K()V

    .line 182003
    :cond_c
    iget-object v0, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v3, v0, v3

    .line 182004
    invoke-virtual {p1, v3}, LX/0ln;->b(C)I

    move-result v0

    .line 182005
    if-gez v0, :cond_f

    .line 182006
    if-eq v0, v5, :cond_e

    .line 182007
    if-ne v3, v6, :cond_d

    .line 182008
    iget-boolean v0, p1, LX/0ln;->a:Z

    move v0, v0

    .line 182009
    if-nez v0, :cond_d

    .line 182010
    shr-int/lit8 v0, v1, 0x2

    .line 182011
    invoke-virtual {v2, v0}, LX/2SG;->b(I)V

    .line 182012
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    goto/16 :goto_1

    .line 182013
    :cond_d
    invoke-virtual {p0, p1, v3, v7}, LX/15u;->a(LX/0ln;CI)I

    move-result v0

    .line 182014
    :cond_e
    if-ne v0, v5, :cond_f

    .line 182015
    shr-int/lit8 v0, v1, 0x2

    .line 182016
    invoke-virtual {v2, v0}, LX/2SG;->b(I)V

    goto/16 :goto_0

    .line 182017
    :cond_f
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    .line 182018
    invoke-virtual {v2, v0}, LX/2SG;->c(I)V

    goto/16 :goto_0
.end method

.method private f(Ljava/lang/String;)C
    .locals 3

    .prologue
    .line 182280
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 182281
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182282
    invoke-virtual {p0, p1}, LX/15v;->d(Ljava/lang/String;)V

    .line 182283
    :cond_0
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    return v0
.end method

.method private f(I)LX/15z;
    .locals 13

    .prologue
    const/16 v11, 0x2d

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v10, 0x39

    const/16 v9, 0x30

    .line 182019
    if-ne p1, v11, :cond_1

    move v0, v1

    .line 182020
    :goto_0
    iget v4, p0, LX/15u;->d:I

    .line 182021
    add-int/lit8 v5, v4, -0x1

    .line 182022
    iget v7, p0, LX/15u;->e:I

    .line 182023
    if-eqz v0, :cond_2

    .line 182024
    iget v3, p0, LX/15u;->e:I

    if-ge v4, v3, :cond_b

    .line 182025
    iget-object v6, p0, LX/15t;->N:[C

    add-int/lit8 v3, v4, 0x1

    aget-char p1, v6, v4

    .line 182026
    if-gt p1, v10, :cond_0

    if-ge p1, v9, :cond_3

    .line 182027
    :cond_0
    iput v3, p0, LX/15t;->d:I

    .line 182028
    invoke-direct {p0, p1, v1}, LX/15t;->a(IZ)LX/15z;

    move-result-object v0

    .line 182029
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 182030
    goto :goto_0

    :cond_2
    move v3, v4

    .line 182031
    :cond_3
    if-eq p1, v9, :cond_b

    .line 182032
    :goto_2
    iget v4, p0, LX/15u;->e:I

    if-ge v3, v4, :cond_b

    .line 182033
    iget-object v6, p0, LX/15t;->N:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v6, v3

    .line 182034
    if-lt v3, v9, :cond_4

    if-gt v3, v10, :cond_4

    .line 182035
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    goto :goto_2

    .line 182036
    :cond_4
    const/16 v6, 0x2e

    if-ne v3, v6, :cond_e

    move v3, v2

    move v6, v4

    .line 182037
    :goto_3
    if-ge v6, v7, :cond_b

    .line 182038
    iget-object v8, p0, LX/15t;->N:[C

    add-int/lit8 v4, v6, 0x1

    aget-char v6, v8, v6

    .line 182039
    if-lt v6, v9, :cond_5

    if-gt v6, v10, :cond_5

    .line 182040
    add-int/lit8 v3, v3, 0x1

    move v6, v4

    goto :goto_3

    .line 182041
    :cond_5
    if-nez v3, :cond_6

    .line 182042
    const-string v8, "Decimal point not followed by a digit"

    invoke-virtual {p0, v6, v8}, LX/15u;->a(ILjava/lang/String;)V

    :cond_6
    move v12, v3

    move v3, v4

    move v4, v6

    move v6, v12

    .line 182043
    :goto_4
    const/16 v8, 0x65

    if-eq v4, v8, :cond_7

    const/16 v8, 0x45

    if-ne v4, v8, :cond_a

    .line 182044
    :cond_7
    if-ge v3, v7, :cond_b

    .line 182045
    iget-object v8, p0, LX/15t;->N:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v8, v3

    .line 182046
    if-eq v3, v11, :cond_8

    const/16 v8, 0x2b

    if-ne v3, v8, :cond_d

    .line 182047
    :cond_8
    if-ge v4, v7, :cond_b

    .line 182048
    iget-object v8, p0, LX/15t;->N:[C

    add-int/lit8 v3, v4, 0x1

    aget-char v4, v8, v4

    .line 182049
    :goto_5
    if-gt v4, v10, :cond_9

    if-lt v4, v9, :cond_9

    .line 182050
    add-int/lit8 v2, v2, 0x1

    .line 182051
    if-ge v3, v7, :cond_b

    .line 182052
    iget-object v8, p0, LX/15t;->N:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v8, v3

    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_5

    .line 182053
    :cond_9
    if-nez v2, :cond_a

    .line 182054
    const-string v7, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v4, v7}, LX/15u;->a(ILjava/lang/String;)V

    .line 182055
    :cond_a
    add-int/lit8 v3, v3, -0x1

    .line 182056
    iput v3, p0, LX/15t;->d:I

    .line 182057
    sub-int/2addr v3, v5

    .line 182058
    iget-object v4, p0, LX/15u;->n:LX/15x;

    iget-object v7, p0, LX/15t;->N:[C

    invoke-virtual {v4, v7, v5, v3}, LX/15x;->a([CII)V

    .line 182059
    invoke-virtual {p0, v0, v1, v6, v2}, LX/15u;->a(ZIII)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 182060
    :cond_b
    if-eqz v0, :cond_c

    add-int/lit8 v1, v5, 0x1

    :goto_6
    iput v1, p0, LX/15t;->d:I

    .line 182061
    invoke-direct {p0, v0}, LX/15t;->b(Z)LX/15z;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    move v1, v5

    .line 182062
    goto :goto_6

    :cond_d
    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_5

    :cond_e
    move v6, v2

    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_4
.end method

.method private g(I)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x22

    .line 182259
    if-eq p1, v7, :cond_0

    .line 182260
    invoke-direct {p0, p1}, LX/15t;->h(I)Ljava/lang/String;

    move-result-object v0

    .line 182261
    :goto_0
    return-object v0

    .line 182262
    :cond_0
    iget v1, p0, LX/15u;->d:I

    .line 182263
    iget v0, p0, LX/15t;->Q:I

    .line 182264
    iget v2, p0, LX/15u;->e:I

    .line 182265
    if-ge v1, v2, :cond_3

    .line 182266
    sget-object v3, LX/12H;->a:[I

    move-object v3, v3

    .line 182267
    array-length v4, v3

    .line 182268
    :cond_1
    iget-object v5, p0, LX/15t;->N:[C

    aget-char v5, v5, v1

    .line 182269
    if-ge v5, v4, :cond_2

    aget v6, v3, v5

    if-eqz v6, :cond_2

    .line 182270
    if-ne v5, v7, :cond_3

    .line 182271
    iget v2, p0, LX/15u;->d:I

    .line 182272
    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/15t;->d:I

    .line 182273
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    iget-object v4, p0, LX/15t;->N:[C

    sub-int/2addr v1, v2

    invoke-virtual {v3, v4, v2, v1, v0}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182274
    :cond_2
    mul-int/lit8 v0, v0, 0x21

    add-int/2addr v0, v5

    .line 182275
    add-int/lit8 v1, v1, 0x1

    .line 182276
    if-lt v1, v2, :cond_1

    .line 182277
    :cond_3
    iget v2, p0, LX/15u;->d:I

    .line 182278
    iput v1, p0, LX/15t;->d:I

    .line 182279
    invoke-direct {p0, v2, v0, v7}, LX/15t;->a(III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h(I)Ljava/lang/String;
    .locals 7

    .prologue
    .line 181663
    const/16 v0, 0x27

    if-ne p1, v0, :cond_0

    sget-object v0, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181664
    invoke-direct {p0}, LX/15t;->X()Ljava/lang/String;

    move-result-object v0

    .line 181665
    :goto_0
    return-object v0

    .line 181666
    :cond_0
    sget-object v0, LX/0lr;->ALLOW_UNQUOTED_FIELD_NAMES:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 181667
    const-string v0, "was expecting double-quote to start field name"

    invoke-virtual {p0, p1, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 181668
    :cond_1
    sget-object v0, LX/12H;->c:[I

    move-object v2, v0

    .line 181669
    array-length v3, v2

    .line 181670
    if-ge p1, v3, :cond_6

    .line 181671
    aget v0, v2, p1

    if-nez v0, :cond_5

    const/16 v0, 0x30

    if-lt p1, v0, :cond_2

    const/16 v0, 0x39

    if-le p1, v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    .line 181672
    :goto_1
    if-nez v0, :cond_3

    .line 181673
    const-string v0, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name"

    invoke-virtual {p0, p1, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 181674
    :cond_3
    iget v1, p0, LX/15u;->d:I

    .line 181675
    iget v0, p0, LX/15t;->Q:I

    .line 181676
    iget v4, p0, LX/15u;->e:I

    .line 181677
    if-ge v1, v4, :cond_9

    .line 181678
    :cond_4
    iget-object v5, p0, LX/15t;->N:[C

    aget-char v5, v5, v1

    .line 181679
    if-ge v5, v3, :cond_7

    .line 181680
    aget v6, v2, v5

    if-eqz v6, :cond_8

    .line 181681
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, -0x1

    .line 181682
    iput v1, p0, LX/15t;->d:I

    .line 181683
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    iget-object v4, p0, LX/15t;->N:[C

    sub-int/2addr v1, v2

    invoke-virtual {v3, v4, v2, v1, v0}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 181684
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 181685
    :cond_6
    int-to-char v0, p1

    invoke-static {v0}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v0

    goto :goto_1

    .line 181686
    :cond_7
    int-to-char v6, v5

    invoke-static {v6}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v6

    if-nez v6, :cond_8

    .line 181687
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, -0x1

    .line 181688
    iput v1, p0, LX/15t;->d:I

    .line 181689
    iget-object v3, p0, LX/15t;->P:LX/0lt;

    iget-object v4, p0, LX/15t;->N:[C

    sub-int/2addr v1, v2

    invoke-virtual {v3, v4, v2, v1, v0}, LX/0lt;->a([CIII)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 181690
    :cond_8
    mul-int/lit8 v0, v0, 0x21

    add-int/2addr v0, v5

    .line 181691
    add-int/lit8 v1, v1, 0x1

    .line 181692
    if-lt v1, v4, :cond_4

    .line 181693
    :cond_9
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v3, v3, -0x1

    .line 181694
    iput v1, p0, LX/15t;->d:I

    .line 181695
    invoke-direct {p0, v3, v0, v2}, LX/15t;->a(II[I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 181696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 181697
    :goto_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_0

    .line 181698
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181699
    :cond_0
    iget-object v1, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    aget-char v1, v1, v2

    .line 181700
    invoke-static {v1}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181701
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/15t;->d:I

    .line 181702
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 181703
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized token \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': was expecting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 181704
    return-void
.end method

.method private i(I)LX/15z;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 181705
    sparse-switch p1, :sswitch_data_0

    .line 181706
    :cond_0
    :goto_0
    const-string v0, "expected a valid value (number, String, array, object, \'true\', \'false\' or \'null\')"

    invoke-virtual {p0, p1, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 181707
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 181708
    :sswitch_0
    sget-object v0, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181709
    invoke-direct {p0}, LX/15t;->Y()LX/15z;

    move-result-object v0

    goto :goto_1

    .line 181710
    :sswitch_1
    const-string v0, "NaN"

    invoke-direct {p0, v0, v1}, LX/15t;->a(Ljava/lang/String;I)V

    .line 181711
    sget-object v0, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181712
    const-string v0, "NaN"

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 181713
    :cond_1
    const-string v0, "Non-standard token \'NaN\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 181714
    :sswitch_2
    const-string v0, "Infinity"

    invoke-direct {p0, v0, v1}, LX/15t;->a(Ljava/lang/String;I)V

    .line 181715
    sget-object v0, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181716
    const-string v0, "Infinity"

    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 181717
    :cond_2
    const-string v0, "Non-standard token \'Infinity\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 181718
    :sswitch_3
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_3

    .line 181719
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_3

    .line 181720
    invoke-virtual {p0}, LX/15v;->T()V

    .line 181721
    :cond_3
    iget-object v0, p0, LX/15t;->N:[C

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15t;->d:I

    aget-char v0, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/15t;->a(IZ)LX/15z;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_0
        0x2b -> :sswitch_3
        0x49 -> :sswitch_2
        0x4e -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final I()Ljava/lang/String;
    .locals 2

    .prologue
    .line 181722
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 181723
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_0

    .line 181724
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181725
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181726
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 181727
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/15u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final L()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 181728
    iget-wide v2, p0, LX/15u;->f:J

    iget v1, p0, LX/15u;->e:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/15t;->f:J

    .line 181729
    iget v1, p0, LX/15u;->h:I

    iget v2, p0, LX/15u;->e:I

    sub-int/2addr v1, v2

    iput v1, p0, LX/15t;->h:I

    .line 181730
    iget-object v1, p0, LX/15t;->M:Ljava/io/Reader;

    if-eqz v1, :cond_0

    .line 181731
    iget-object v1, p0, LX/15t;->M:Ljava/io/Reader;

    iget-object v2, p0, LX/15t;->N:[C

    iget-object v3, p0, LX/15t;->N:[C

    array-length v3, v3

    invoke-virtual {v1, v2, v0, v3}, Ljava/io/Reader;->read([CII)I

    move-result v1

    .line 181732
    if-lez v1, :cond_1

    .line 181733
    iput v0, p0, LX/15t;->d:I

    .line 181734
    iput v1, p0, LX/15t;->e:I

    .line 181735
    const/4 v0, 0x1

    .line 181736
    :cond_0
    return v0

    .line 181737
    :cond_1
    invoke-virtual {p0}, LX/15t;->N()V

    .line 181738
    if-nez v1, :cond_0

    .line 181739
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reader returned 0 characters when trying to read "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/15u;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final M()V
    .locals 6

    .prologue
    .line 181740
    iget v0, p0, LX/15u;->d:I

    .line 181741
    iget v1, p0, LX/15u;->e:I

    .line 181742
    if-ge v0, v1, :cond_2

    .line 181743
    sget-object v2, LX/12H;->a:[I

    move-object v2, v2

    .line 181744
    array-length v3, v2

    .line 181745
    :cond_0
    iget-object v4, p0, LX/15t;->N:[C

    aget-char v4, v4, v0

    .line 181746
    if-ge v4, v3, :cond_1

    aget v5, v2, v4

    if-eqz v5, :cond_1

    .line 181747
    const/16 v1, 0x22

    if-ne v4, v1, :cond_2

    .line 181748
    iget-object v1, p0, LX/15u;->n:LX/15x;

    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->d:I

    sub-int v4, v0, v4

    invoke-virtual {v1, v2, v3, v4}, LX/15x;->a([CII)V

    .line 181749
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15t;->d:I

    .line 181750
    :goto_0
    return-void

    .line 181751
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 181752
    if-lt v0, v1, :cond_0

    .line 181753
    :cond_2
    iget-object v1, p0, LX/15u;->n:LX/15x;

    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->d:I

    sub-int v4, v0, v4

    invoke-virtual {v1, v2, v3, v4}, LX/15x;->b([CII)V

    .line 181754
    iput v0, p0, LX/15t;->d:I

    .line 181755
    invoke-direct {p0}, LX/15t;->Z()V

    goto :goto_0
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 181756
    iget-object v0, p0, LX/15t;->M:Ljava/io/Reader;

    if-eqz v0, :cond_2

    .line 181757
    iget-object v0, p0, LX/15u;->b:LX/12A;

    .line 181758
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 181759
    if-nez v0, :cond_0

    sget-object v0, LX/0lr;->AUTO_CLOSE_SOURCE:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181760
    :cond_0
    iget-object v0, p0, LX/15t;->M:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 181761
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/15t;->M:Ljava/io/Reader;

    .line 181762
    :cond_2
    return-void
.end method

.method public final O()V
    .locals 2

    .prologue
    .line 181763
    invoke-super {p0}, LX/15u;->O()V

    .line 181764
    iget-object v0, p0, LX/15t;->N:[C

    .line 181765
    if-eqz v0, :cond_0

    .line 181766
    const/4 v1, 0x0

    iput-object v1, p0, LX/15t;->N:[C

    .line 181767
    iget-object v1, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->a([C)V

    .line 181768
    :cond_0
    return-void
.end method

.method public final R()C
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 181769
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_0

    .line 181770
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181771
    const-string v0, " in character escape sequence"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 181772
    :cond_0
    iget-object v0, p0, LX/15t;->N:[C

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/15t;->d:I

    aget-char v0, v0, v2

    .line 181773
    sparse-switch v0, :sswitch_data_0

    .line 181774
    invoke-virtual {p0, v0}, LX/15v;->a(C)C

    move-result v0

    .line 181775
    :goto_0
    :sswitch_0
    return v0

    .line 181776
    :sswitch_1
    const/16 v0, 0x8

    goto :goto_0

    .line 181777
    :sswitch_2
    const/16 v0, 0x9

    goto :goto_0

    .line 181778
    :sswitch_3
    const/16 v0, 0xa

    goto :goto_0

    .line 181779
    :sswitch_4
    const/16 v0, 0xc

    goto :goto_0

    .line 181780
    :sswitch_5
    const/16 v0, 0xd

    goto :goto_0

    :sswitch_6
    move v0, v1

    .line 181781
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 181782
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_1

    .line 181783
    invoke-virtual {p0}, LX/15t;->L()Z

    move-result v2

    if-nez v2, :cond_1

    .line 181784
    const-string v2, " in character escape sequence"

    invoke-virtual {p0, v2}, LX/15v;->d(Ljava/lang/String;)V

    .line 181785
    :cond_1
    iget-object v2, p0, LX/15t;->N:[C

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/15t;->d:I

    aget-char v2, v2, v3

    .line 181786
    invoke-static {v2}, LX/12H;->a(I)I

    move-result v3

    .line 181787
    if-gez v3, :cond_2

    .line 181788
    const-string v4, "expected a hex-digit for character escape sequence"

    invoke-virtual {p0, v2, v4}, LX/15v;->b(ILjava/lang/String;)V

    .line 181789
    :cond_2
    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v1, v3

    .line 181790
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 181791
    :cond_3
    int-to-char v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x2f -> :sswitch_0
        0x5c -> :sswitch_0
        0x62 -> :sswitch_1
        0x66 -> :sswitch_4
        0x6e -> :sswitch_3
        0x72 -> :sswitch_5
        0x74 -> :sswitch_2
        0x75 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 181792
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_3

    .line 181793
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->p:Z

    .line 181794
    iget-object v0, p0, LX/15u;->m:LX/15z;

    .line 181795
    const/4 v1, 0x0

    iput-object v1, p0, LX/15t;->m:LX/15z;

    .line 181796
    iput-object v0, p0, LX/15t;->K:LX/15z;

    .line 181797
    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 181798
    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    .line 181799
    :cond_0
    :goto_0
    return p1

    .line 181800
    :cond_1
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_2

    .line 181801
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/15t;->l:LX/15y;

    goto :goto_0

    .line 181802
    :cond_2
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 181803
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/15t;->l:LX/15y;

    goto :goto_0

    .line 181804
    :cond_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    goto :goto_0
.end method

.method public final a(Ljava/io/Writer;)I
    .locals 3

    .prologue
    .line 181805
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    .line 181806
    if-gtz v0, :cond_0

    .line 181807
    const/4 v0, 0x0

    .line 181808
    :goto_0
    return v0

    .line 181809
    :cond_0
    iget v1, p0, LX/15u;->d:I

    .line 181810
    iget-object v2, p0, LX/15t;->N:[C

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/Writer;->write([CII)V

    goto :goto_0
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 181811
    iget-object v0, p0, LX/15t;->O:LX/0lD;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181812
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 181813
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_0

    .line 181814
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181815
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181816
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 181817
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, LX/15u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0lD;)V
    .locals 0

    .prologue
    .line 181818
    iput-object p1, p0, LX/15t;->O:LX/0lD;

    .line 181819
    return-void
.end method

.method public final a(LX/0ln;)[B
    .locals 3

    .prologue
    .line 181820
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/15u;->r:[B

    if-nez v0, :cond_1

    .line 181821
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 181822
    :cond_1
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_3

    .line 181823
    :try_start_0
    invoke-direct {p0, p1}, LX/15t;->b(LX/0ln;)[B

    move-result-object v0

    iput-object v0, p0, LX/15t;->r:[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181824
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181825
    :cond_2
    :goto_0
    iget-object v0, p0, LX/15u;->r:[B

    return-object v0

    .line 181826
    :catch_0
    move-exception v0

    .line 181827
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to decode VALUE_STRING as base64 ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 181828
    :cond_3
    iget-object v0, p0, LX/15u;->r:[B

    if-nez v0, :cond_2

    .line 181829
    invoke-virtual {p0}, LX/15u;->Q()LX/2SG;

    move-result-object v0

    .line 181830
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0, p1}, LX/15v;->a(Ljava/lang/String;LX/2SG;LX/0ln;)V

    .line 181831
    invoke-virtual {v0}, LX/2SG;->c()[B

    move-result-object v0

    iput-object v0, p0, LX/15t;->r:[B

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181832
    iget-object v0, p0, LX/15t;->M:Ljava/io/Reader;

    return-object v0
.end method

.method public final c()LX/15z;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x7d

    const/16 v7, 0x5d

    const/4 v6, 0x1

    .line 181833
    const/4 v0, 0x0

    iput v0, p0, LX/15t;->A:I

    .line 181834
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_0

    .line 181835
    invoke-direct {p0}, LX/15t;->V()LX/15z;

    move-result-object v0

    .line 181836
    :goto_0
    return-object v0

    .line 181837
    :cond_0
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_1

    .line 181838
    invoke-direct {p0}, LX/15t;->aa()V

    .line 181839
    :cond_1
    invoke-direct {p0}, LX/15t;->ae()I

    move-result v0

    .line 181840
    if-gez v0, :cond_2

    .line 181841
    invoke-virtual {p0}, LX/15w;->close()V

    .line 181842
    iput-object v1, p0, LX/15t;->K:LX/15z;

    move-object v0, v1

    goto :goto_0

    .line 181843
    :cond_2
    iget-wide v2, p0, LX/15u;->f:J

    iget v4, p0, LX/15u;->d:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/15t;->i:J

    .line 181844
    iget v2, p0, LX/15u;->g:I

    iput v2, p0, LX/15t;->j:I

    .line 181845
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->h:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/15t;->k:I

    .line 181846
    iput-object v1, p0, LX/15t;->r:[B

    .line 181847
    if-ne v0, v7, :cond_4

    .line 181848
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 181849
    invoke-virtual {p0, v0, v8}, LX/15u;->a(IC)V

    .line 181850
    :cond_3
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 181851
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 181852
    iput-object v0, p0, LX/15t;->l:LX/15y;

    .line 181853
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    iput-object v0, p0, LX/15t;->K:LX/15z;

    goto :goto_0

    .line 181854
    :cond_4
    if-ne v0, v8, :cond_6

    .line 181855
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->d()Z

    move-result v1

    if-nez v1, :cond_5

    .line 181856
    invoke-virtual {p0, v0, v7}, LX/15u;->a(IC)V

    .line 181857
    :cond_5
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 181858
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 181859
    iput-object v0, p0, LX/15t;->l:LX/15y;

    .line 181860
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    iput-object v0, p0, LX/15t;->K:LX/15z;

    goto :goto_0

    .line 181861
    :cond_6
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/15y;->k()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 181862
    const/16 v1, 0x2c

    if-eq v0, v1, :cond_7

    .line 181863
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "was expecting comma to separate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v2}, LX/12V;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " entries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 181864
    :cond_7
    invoke-direct {p0}, LX/15t;->ad()I

    move-result v0

    .line 181865
    :cond_8
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->d()Z

    move-result v1

    .line 181866
    if-eqz v1, :cond_a

    .line 181867
    invoke-direct {p0, v0}, LX/15t;->g(I)Ljava/lang/String;

    move-result-object v0

    .line 181868
    iget-object v2, p0, LX/15u;->l:LX/15y;

    .line 181869
    iput-object v0, v2, LX/15y;->f:Ljava/lang/String;

    .line 181870
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    iput-object v0, p0, LX/15t;->K:LX/15z;

    .line 181871
    invoke-direct {p0}, LX/15t;->ad()I

    move-result v0

    .line 181872
    const/16 v2, 0x3a

    if-eq v0, v2, :cond_9

    .line 181873
    const-string v2, "was expecting a colon to separate field name and value"

    invoke-virtual {p0, v0, v2}, LX/15v;->b(ILjava/lang/String;)V

    .line 181874
    :cond_9
    invoke-direct {p0}, LX/15t;->ad()I

    move-result v0

    .line 181875
    :cond_a
    sparse-switch v0, :sswitch_data_0

    .line 181876
    invoke-direct {p0, v0}, LX/15t;->i(I)LX/15z;

    move-result-object v0

    .line 181877
    :goto_1
    if-eqz v1, :cond_d

    .line 181878
    iput-object v0, p0, LX/15t;->m:LX/15z;

    .line 181879
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto/16 :goto_0

    .line 181880
    :sswitch_0
    iput-boolean v6, p0, LX/15t;->R:Z

    .line 181881
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    goto :goto_1

    .line 181882
    :sswitch_1
    if-nez v1, :cond_b

    .line 181883
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v0, v2, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/15t;->l:LX/15y;

    .line 181884
    :cond_b
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    goto :goto_1

    .line 181885
    :sswitch_2
    if-nez v1, :cond_c

    .line 181886
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v0, v2, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/15t;->l:LX/15y;

    .line 181887
    :cond_c
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    goto :goto_1

    .line 181888
    :sswitch_3
    const-string v2, "expected a value"

    invoke-virtual {p0, v0, v2}, LX/15v;->b(ILjava/lang/String;)V

    .line 181889
    :sswitch_4
    const-string v0, "true"

    invoke-direct {p0, v0, v6}, LX/15t;->a(Ljava/lang/String;I)V

    .line 181890
    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    goto :goto_1

    .line 181891
    :sswitch_5
    const-string v0, "false"

    invoke-direct {p0, v0, v6}, LX/15t;->a(Ljava/lang/String;I)V

    .line 181892
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    goto :goto_1

    .line 181893
    :sswitch_6
    const-string v0, "null"

    invoke-direct {p0, v0, v6}, LX/15t;->a(Ljava/lang/String;I)V

    .line 181894
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    goto :goto_1

    .line 181895
    :sswitch_7
    invoke-direct {p0, v0}, LX/15t;->f(I)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 181896
    :cond_d
    iput-object v0, p0, LX/15t;->K:LX/15z;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x2d -> :sswitch_7
        0x30 -> :sswitch_7
        0x31 -> :sswitch_7
        0x32 -> :sswitch_7
        0x33 -> :sswitch_7
        0x34 -> :sswitch_7
        0x35 -> :sswitch_7
        0x36 -> :sswitch_7
        0x37 -> :sswitch_7
        0x38 -> :sswitch_7
        0x39 -> :sswitch_7
        0x5b -> :sswitch_1
        0x5d -> :sswitch_3
        0x66 -> :sswitch_5
        0x6e -> :sswitch_6
        0x74 -> :sswitch_4
        0x7b -> :sswitch_2
        0x7d -> :sswitch_3
    .end sparse-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 181897
    invoke-super {p0}, LX/15u;->close()V

    .line 181898
    iget-object v0, p0, LX/15t;->P:LX/0lt;

    invoke-virtual {v0}, LX/0lt;->b()V

    .line 181899
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 181900
    iget-object v1, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v2, :cond_4

    .line 181901
    iput-boolean v3, p0, LX/15t;->p:Z

    .line 181902
    iget-object v1, p0, LX/15u;->m:LX/15z;

    .line 181903
    iput-object v0, p0, LX/15t;->m:LX/15z;

    .line 181904
    iput-object v1, p0, LX/15t;->K:LX/15z;

    .line 181905
    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_2

    .line 181906
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_0

    .line 181907
    iput-boolean v3, p0, LX/15t;->R:Z

    .line 181908
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181909
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 181910
    :cond_1
    :goto_0
    return-object v0

    .line 181911
    :cond_2
    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_3

    .line 181912
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/15t;->l:LX/15y;

    goto :goto_0

    .line 181913
    :cond_3
    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-ne v1, v2, :cond_1

    .line 181914
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/15t;->l:LX/15y;

    goto :goto_0

    .line 181915
    :cond_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 181916
    iget-object v0, p0, LX/15v;->K:LX/15z;

    .line 181917
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 181918
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_0

    .line 181919
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181920
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181921
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 181922
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, v0}, LX/15t;->a(LX/15z;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 181923
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_4

    .line 181924
    sget-object v0, LX/32w;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 181925
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    .line 181926
    :goto_0
    return-object v0

    .line 181927
    :pswitch_0
    iget-boolean v0, p0, LX/15u;->p:Z

    if-nez v0, :cond_1

    .line 181928
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    .line 181929
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 181930
    iget-object v2, p0, LX/15u;->o:[C

    if-nez v2, :cond_2

    .line 181931
    iget-object v2, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v2, v1}, LX/12A;->a(I)[C

    move-result-object v2

    iput-object v2, p0, LX/15t;->o:[C

    .line 181932
    :cond_0
    :goto_1
    iget-object v2, p0, LX/15u;->o:[C

    invoke-virtual {v0, v3, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 181933
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/15t;->p:Z

    .line 181934
    :cond_1
    iget-object v0, p0, LX/15u;->o:[C

    goto :goto_0

    .line 181935
    :cond_2
    iget-object v2, p0, LX/15u;->o:[C

    array-length v2, v2

    if-ge v2, v1, :cond_0

    .line 181936
    new-array v2, v1, [C

    iput-object v2, p0, LX/15t;->o:[C

    goto :goto_1

    .line 181937
    :pswitch_1
    iget-boolean v0, p0, LX/15t;->R:Z

    if-eqz v0, :cond_3

    .line 181938
    iput-boolean v3, p0, LX/15t;->R:Z

    .line 181939
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181940
    :cond_3
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v0

    goto :goto_0

    .line 181941
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final q()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181942
    iget-object v1, p0, LX/15v;->K:LX/15z;

    if-eqz v1, :cond_0

    .line 181943
    sget-object v1, LX/32w;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 181944
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    array-length v0, v0

    .line 181945
    :cond_0
    :goto_0
    return v0

    .line 181946
    :pswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 181947
    :pswitch_1
    iget-boolean v1, p0, LX/15t;->R:Z

    if-eqz v1, :cond_1

    .line 181948
    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181949
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181950
    :cond_1
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->c()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final r()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181951
    iget-object v1, p0, LX/15v;->K:LX/15z;

    if-eqz v1, :cond_0

    .line 181952
    sget-object v1, LX/32w;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 181953
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 181954
    :pswitch_1
    iget-boolean v1, p0, LX/15t;->R:Z

    if-eqz v1, :cond_1

    .line 181955
    iput-boolean v0, p0, LX/15t;->R:Z

    .line 181956
    invoke-virtual {p0}, LX/15t;->M()V

    .line 181957
    :cond_1
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
