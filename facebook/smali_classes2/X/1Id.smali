.class public LX/1Id;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BU;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1BU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228977
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1Id;->a:Ljava/util/List;

    .line 228978
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228979
    iget-object v2, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228980
    :cond_0
    return-void
.end method

.method public varargs constructor <init>([LX/1BU;)V
    .locals 1

    .prologue
    .line 228981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228982
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1Id;->a:Ljava/util/List;

    .line 228983
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 229000
    const-string v0, "ForwardingRequestListener"

    invoke-static {v0, p0, p1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 229001
    return-void
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 228984
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228985
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228986
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228987
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, LX/1BU;->a(LX/1bf;Ljava/lang/Object;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228988
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228989
    :catch_0
    move-exception v0

    .line 228990
    const-string v3, "InternalListener exception in onRequestStart"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228991
    :cond_0
    return-void
.end method

.method public final a(LX/1bf;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 4

    .prologue
    .line 228992
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228993
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228994
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228995
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, LX/1BU;->a(LX/1bf;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228996
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228997
    :catch_0
    move-exception v0

    .line 228998
    const-string v3, "InternalListener exception in onRequestFailure"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228999
    :cond_0
    return-void
.end method

.method public final a(LX/1bf;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 228960
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228961
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228962
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228963
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, LX/1BU;->a(LX/1bf;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228964
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228965
    :catch_0
    move-exception v0

    .line 228966
    const-string v3, "InternalListener exception in onRequestSuccess"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228967
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 228968
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228969
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228970
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228971
    :try_start_0
    invoke-interface {v0, p1}, LX/1BU;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228972
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228973
    :catch_0
    move-exception v0

    .line 228974
    const-string v3, "InternalListener exception in onRequestCancellation"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228975
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 228913
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228914
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228915
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228916
    :try_start_0
    invoke-interface {v0, p1, p2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228917
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228918
    :catch_0
    move-exception v0

    .line 228919
    const-string v3, "InternalListener exception in onProducerStart"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228920
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 228921
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228922
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228923
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228924
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228925
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228926
    :catch_0
    move-exception v0

    .line 228927
    const-string v3, "InternalListener exception in onIntermediateChunkStart"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228928
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 4
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228929
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228930
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228931
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228932
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228933
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228934
    :catch_0
    move-exception v0

    .line 228935
    const-string v3, "InternalListener exception in onProducerFinishWithFailure"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228936
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228937
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228938
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228939
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228940
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228941
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228942
    :catch_0
    move-exception v0

    .line 228943
    const-string v3, "InternalListener exception in onProducerFinishWithSuccess"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228944
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228952
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 228953
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228954
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    .line 228955
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, LX/1BV;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228956
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228957
    :catch_0
    move-exception v0

    .line 228958
    const-string v3, "InternalListener exception in onProducerFinishWithCancellation"

    invoke-static {v3, v0}, LX/1Id;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 228959
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 228945
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 228946
    :goto_0
    if-ge v2, v3, :cond_1

    .line 228947
    iget-object v0, p0, LX/1Id;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BU;

    invoke-interface {v0, p1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228948
    const/4 v0, 0x1

    .line 228949
    :goto_1
    return v0

    .line 228950
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 228951
    goto :goto_1
.end method
