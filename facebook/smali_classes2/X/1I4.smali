.class public LX/1I4;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1IW;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1IW;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 228117
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1IW;
    .locals 7

    .prologue
    .line 228118
    sget-object v0, LX/1I4;->a:LX/1IW;

    if-nez v0, :cond_1

    .line 228119
    const-class v1, LX/1I4;

    monitor-enter v1

    .line 228120
    :try_start_0
    sget-object v0, LX/1I4;->a:LX/1IW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 228121
    if-eqz v2, :cond_0

    .line 228122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 228123
    invoke-static {v0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v3

    check-cast v3, LX/1Hn;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v4

    check-cast v4, LX/0WP;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v6

    check-cast v6, LX/1BA;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3, v4, v5, v6, p0}, LX/1Hw;->a(LX/1Hn;LX/0WP;LX/03V;LX/1BA;LX/0Or;)LX/1IW;

    move-result-object v3

    move-object v0, v3

    .line 228124
    sput-object v0, LX/1I4;->a:LX/1IW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228125
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 228126
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 228127
    :cond_1
    sget-object v0, LX/1I4;->a:LX/1IW;

    return-object v0

    .line 228128
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 228129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 228130
    invoke-static {p0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v0

    check-cast v0, LX/1Hn;

    invoke-static {p0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v1

    check-cast v1, LX/0WP;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v3

    check-cast v3, LX/1BA;

    const/16 v4, 0x15e7

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/1Hw;->a(LX/1Hn;LX/0WP;LX/03V;LX/1BA;LX/0Or;)LX/1IW;

    move-result-object v0

    return-object v0
.end method
