.class public LX/0oz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0p0;
.implements LX/0Up;
.implements LX/0p1;
.implements LX/0p2;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field private static volatile y:LX/0oz;


# instance fields
.field public e:LX/0kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0p7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0p8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0p9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final o:LX/0YZ;

.field public final p:LX/0YZ;

.field public final q:LX/0YZ;

.field public final r:Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;

.field public s:Z

.field public final t:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field private volatile v:Z

.field public volatile w:Landroid/net/NetworkInfo;

.field private volatile x:LX/0p0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 143568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/0oz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".DATA_CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0oz;->a:Ljava/lang/String;

    .line 143569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/0oz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".BANDWIDTH_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0oz;->b:Ljava/lang/String;

    .line 143570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/0oz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".LATENCY_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0oz;->c:Ljava/lang/String;

    .line 143571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/0oz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CONNECTION_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0oz;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 143555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143556
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 143557
    iput-object v0, p0, LX/0oz;->m:LX/0Ot;

    .line 143558
    new-instance v0, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;-><init>(LX/0oz;)V

    iput-object v0, p0, LX/0oz;->r:Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;

    .line 143559
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0oz;->s:Z

    .line 143560
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143561
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143562
    iput-boolean v2, p0, LX/0oz;->v:Z

    .line 143563
    const/4 v0, 0x0

    iput-object v0, p0, LX/0oz;->w:Landroid/net/NetworkInfo;

    .line 143564
    new-instance v0, LX/0p4;

    invoke-direct {v0, p0}, LX/0p4;-><init>(LX/0oz;)V

    iput-object v0, p0, LX/0oz;->p:LX/0YZ;

    .line 143565
    new-instance v0, LX/0p5;

    invoke-direct {v0, p0}, LX/0p5;-><init>(LX/0oz;)V

    iput-object v0, p0, LX/0oz;->o:LX/0YZ;

    .line 143566
    new-instance v0, LX/0p6;

    invoke-direct {v0, p0}, LX/0p6;-><init>(LX/0oz;)V

    iput-object v0, p0, LX/0oz;->q:LX/0YZ;

    .line 143567
    return-void
.end method

.method public static a(LX/0QB;)LX/0oz;
    .locals 13

    .prologue
    .line 143540
    sget-object v0, LX/0oz;->y:LX/0oz;

    if-nez v0, :cond_1

    .line 143541
    const-class v1, LX/0oz;

    monitor-enter v1

    .line 143542
    :try_start_0
    sget-object v0, LX/0oz;->y:LX/0oz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143543
    if-eqz v2, :cond_0

    .line 143544
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143545
    new-instance v3, LX/0oz;

    invoke-direct {v3}, LX/0oz;-><init>()V

    .line 143546
    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v7

    check-cast v7, LX/0p7;

    invoke-static {v0}, LX/0p8;->a(LX/0QB;)LX/0p8;

    move-result-object v8

    check-cast v8, LX/0p8;

    invoke-static {v0}, LX/0p9;->a(LX/0QB;)LX/0p9;

    move-result-object v9

    check-cast v9, LX/0p9;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    const/16 v12, 0xb66

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object p0

    check-cast p0, Landroid/os/Handler;

    .line 143547
    iput-object v4, v3, LX/0oz;->e:LX/0kb;

    iput-object v5, v3, LX/0oz;->f:LX/0Xl;

    iput-object v6, v3, LX/0oz;->g:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v7, v3, LX/0oz;->h:LX/0p7;

    iput-object v8, v3, LX/0oz;->i:LX/0p8;

    iput-object v9, v3, LX/0oz;->j:LX/0p9;

    iput-object v10, v3, LX/0oz;->k:LX/0W3;

    iput-object v11, v3, LX/0oz;->l:LX/0Sh;

    iput-object v12, v3, LX/0oz;->m:LX/0Ot;

    iput-object p0, v3, LX/0oz;->n:Landroid/os/Handler;

    .line 143548
    move-object v0, v3

    .line 143549
    sput-object v0, LX/0oz;->y:LX/0oz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143552
    :cond_1
    sget-object v0, LX/0oz;->y:LX/0oz;

    return-object v0

    .line 143553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static o(LX/0oz;)LX/0p0;
    .locals 2

    .prologue
    .line 143531
    iget-object v0, p0, LX/0oz;->x:LX/0p0;

    .line 143532
    if-nez v0, :cond_1

    .line 143533
    monitor-enter p0

    .line 143534
    :try_start_0
    iget-object v0, p0, LX/0oz;->x:LX/0p0;

    .line 143535
    if-nez v0, :cond_0

    .line 143536
    new-instance v1, LX/448;

    iget-object v0, p0, LX/0oz;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    invoke-direct {v1, v0}, LX/448;-><init>(LX/1MP;)V

    iput-object v1, p0, LX/0oz;->x:LX/0p0;

    move-object v0, v1

    .line 143537
    :cond_0
    monitor-exit p0

    .line 143538
    :cond_1
    return-object v0

    .line 143539
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static r(LX/0oz;)Z
    .locals 1

    .prologue
    .line 143530
    iget-object v0, p0, LX/0oz;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    return v0
.end method

.method public static t(LX/0oz;)V
    .locals 10

    .prologue
    .line 143516
    iget-object v0, p0, LX/0oz;->f:LX/0Xl;

    if-eqz v0, :cond_0

    .line 143517
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 143518
    sget-object v0, LX/0oz;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, LX/0oz;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, LX/0oz;->c:Ljava/lang/String;

    iget-object v0, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    sget-object v2, LX/0oz;->d:Ljava/lang/String;

    invoke-static {p0}, LX/0oz;->r(LX/0oz;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143519
    iget-object v0, p0, LX/0oz;->f:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 143520
    :cond_0
    iget-object v0, p0, LX/0oz;->j:LX/0p9;

    invoke-direct {p0}, LX/0oz;->u()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0p3;

    iget-object v3, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0p3;

    invoke-static {p0}, LX/0oz;->r(LX/0oz;)Z

    move-result v4

    iget-boolean v5, p0, LX/0oz;->s:Z

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 143521
    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v1, v8, v7

    invoke-virtual {v2}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    const/4 v9, 0x2

    invoke-virtual {v3}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v8, v9

    const/4 v9, 0x3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v8, v9

    const/4 v9, 0x4

    if-nez v5, :cond_2

    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v8, v9

    .line 143522
    invoke-static {v0, v2, v3, v4, v5}, LX/0p9;->a(LX/0p9;LX/0p3;LX/0p3;ZZ)V

    .line 143523
    iget-object v6, v0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v6

    if-nez v6, :cond_3

    .line 143524
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v6, v7

    .line 143525
    goto :goto_0

    .line 143526
    :cond_3
    sget-object v6, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v2, v6}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v0, LX/0p9;->j:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 143527
    iget-object v6, v0, LX/0p9;->j:Ljava/util/Map;

    invoke-interface {v6, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143528
    sget-object v6, LX/0p9;->a:LX/0Tn;

    invoke-virtual {v6, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    .line 143529
    iget-object v7, v0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    invoke-virtual {v2}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v6, v8}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    goto :goto_1
.end method

.method private u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 143506
    invoke-direct {p0}, LX/0oz;->v()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 143507
    if-nez v0, :cond_0

    .line 143508
    const-string v0, "UNKNOWN"

    .line 143509
    :goto_0
    return-object v0

    .line 143510
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 143511
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 143512
    :pswitch_0
    iget-object v0, p0, LX/0oz;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143513
    const-string v0, "HOTSPOT"

    goto :goto_0

    .line 143514
    :cond_1
    const-string v0, "WIFI"

    goto :goto_0

    .line 143515
    :pswitch_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v0}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private v()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 143503
    iget-object v0, p0, LX/0oz;->w:Landroid/net/NetworkInfo;

    if-nez v0, :cond_0

    .line 143504
    iget-object v0, p0, LX/0oz;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    iput-object v0, p0, LX/0oz;->w:Landroid/net/NetworkInfo;

    .line 143505
    :cond_0
    iget-object v0, p0, LX/0oz;->w:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method public static x(LX/0oz;)J
    .locals 6

    .prologue
    .line 143502
    iget-object v0, p0, LX/0oz;->k:LX/0W3;

    sget-wide v2, LX/0X5;->bw:J

    const-wide/16 v4, 0x3a98

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(D)I
    .locals 3

    .prologue
    .line 143495
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-lez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_1

    .line 143496
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Confidence must be between 0 and 1, exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143497
    :cond_1
    invoke-virtual {p0}, LX/0oz;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143498
    invoke-static {p0}, LX/0oz;->o(LX/0oz;)LX/0p0;

    move-result-object p0

    .line 143499
    :cond_2
    move-object v0, p0

    .line 143500
    invoke-interface {v0}, LX/0p0;->h()I

    move-result v0

    .line 143501
    if-gtz v0, :cond_3

    const/4 v0, -0x1

    :cond_3
    return v0
.end method

.method public final a()V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 143490
    const/4 v0, 0x0

    iput-object v0, p0, LX/0oz;->w:Landroid/net/NetworkInfo;

    .line 143491
    iget-object v0, p0, LX/0oz;->g:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$NetworkConnectivityChangeRunnable;

    invoke-static {p0}, LX/0oz;->r(LX/0oz;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2}, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$NetworkConnectivityChangeRunnable;-><init>(LX/0oz;Z)V

    .line 143492
    iget-object v5, p0, LX/0oz;->k:LX/0W3;

    sget-wide v7, LX/0X5;->bv:J

    const-wide/16 v9, 0x2710

    invoke-interface {v5, v7, v8, v9, v10}, LX/0W4;->a(JJ)J

    move-result-wide v5

    move-wide v2, v5

    .line 143493
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 143494
    return-void
.end method

.method public final a(LX/0p3;)V
    .locals 1

    .prologue
    .line 143487
    iget-object v0, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143488
    invoke-static {p0}, LX/0oz;->t(LX/0oz;)V

    .line 143489
    return-void
.end method

.method public final b()LX/0p3;
    .locals 1

    .prologue
    .line 143572
    invoke-virtual {p0}, LX/0oz;->init()V

    .line 143573
    iget-object v0, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    return-object v0
.end method

.method public final b(LX/0p3;)V
    .locals 1

    .prologue
    .line 143446
    iget-object v0, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143447
    invoke-static {p0}, LX/0oz;->t(LX/0oz;)V

    .line 143448
    return-void
.end method

.method public final c()LX/0p3;
    .locals 3

    .prologue
    .line 143451
    invoke-virtual {p0}, LX/0oz;->init()V

    .line 143452
    invoke-virtual {p0}, LX/0oz;->b()LX/0p3;

    move-result-object v0

    .line 143453
    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 143454
    :cond_0
    :goto_0
    return-object v0

    .line 143455
    :cond_1
    invoke-direct {p0}, LX/0oz;->v()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 143456
    if-nez v1, :cond_2

    .line 143457
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    goto :goto_0

    .line 143458
    :cond_2
    iget-object v0, p0, LX/0oz;->j:LX/0p9;

    invoke-direct {p0}, LX/0oz;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0p9;->a(Ljava/lang/String;)LX/0p3;

    move-result-object v0

    .line 143459
    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v0, v2}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143460
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    invoke-static {v0, v1}, LX/0km;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/0p3;->POOR:LX/0p3;

    goto :goto_0

    :cond_3
    sget-object v0, LX/0p3;->GOOD:LX/0p3;

    goto :goto_0
.end method

.method public final d()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143461
    iget-object v0, p0, LX/0oz;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->f()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0p3;
    .locals 1

    .prologue
    .line 143449
    invoke-virtual {p0}, LX/0oz;->init()V

    .line 143450
    iget-object v0, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    return-object v0
.end method

.method public final f()D
    .locals 2

    .prologue
    .line 143462
    iget-object v0, p0, LX/0oz;->h:LX/0p7;

    invoke-virtual {v0}, LX/0p7;->b()D

    move-result-wide v0

    return-wide v0
.end method

.method public final g()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143463
    iget-object v0, p0, LX/0oz;->h:LX/0p7;

    invoke-virtual {v0}, LX/0p7;->c()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 143464
    iget-object v0, p0, LX/0oz;->h:LX/0p7;

    invoke-virtual {v0}, LX/0p7;->b()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143465
    const-string v0, ""

    return-object v0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 143466
    iget-boolean v0, p0, LX/0oz;->v:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/0oz;->l:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 143467
    monitor-enter p0

    .line 143468
    :try_start_0
    iget-boolean v0, p0, LX/0oz;->v:Z

    if-nez v0, :cond_1

    .line 143469
    iget-object v0, p0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, LX/0oz;->h:LX/0p7;

    invoke-virtual {v1, p0}, LX/0p7;->a(LX/0p2;)LX/0p3;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143470
    iget-object v0, p0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, LX/0oz;->i:LX/0p8;

    invoke-virtual {v1, p0}, LX/0p8;->a(LX/0p1;)LX/0p3;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143471
    iget-object v0, p0, LX/0oz;->f:LX/0Xl;

    if-eqz v0, :cond_0

    .line 143472
    iget-object v0, p0, LX/0oz;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    iget-object v2, p0, LX/0oz;->q:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 143473
    iget-object v0, p0, LX/0oz;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/0oz;->p:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object v2, p0, LX/0oz;->o:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/0oz;->n:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 143474
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0oz;->v:Z

    .line 143475
    :cond_1
    monitor-exit p0

    .line 143476
    :cond_2
    return-void

    .line 143477
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143478
    invoke-virtual {p0}, LX/0oz;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143479
    invoke-static {p0}, LX/0oz;->o(LX/0oz;)LX/0p0;

    move-result-object p0

    .line 143480
    :cond_0
    move-object v0, p0

    .line 143481
    invoke-interface {v0}, LX/0p0;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()D
    .locals 4

    .prologue
    .line 143482
    iget-object v0, p0, LX/0oz;->i:LX/0p8;

    .line 143483
    iget-object v2, v0, LX/0p8;->h:LX/1pb;

    if-nez v2, :cond_0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    :goto_0
    move-wide v0, v2

    .line 143484
    return-wide v0

    :cond_0
    iget-object v2, v0, LX/0p8;->h:LX/1pb;

    invoke-interface {v2}, LX/1pb;->a()D

    move-result-wide v2

    goto :goto_0
.end method

.method public final l()Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 143485
    iget-object v0, p0, LX/0oz;->k:LX/0W3;

    sget-wide v2, LX/0X5;->bt:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 4

    .prologue
    .line 143486
    iget-object v0, p0, LX/0oz;->k:LX/0W3;

    sget-wide v2, LX/0X5;->bu:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
