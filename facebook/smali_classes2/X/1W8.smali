.class public LX/1W8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0ad;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WE",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/1W9;

.field private final e:LX/1WB;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/1z6;",
            "Landroid/util/Pair",
            "<",
            "LX/1nq;",
            "LX/1Ua;",
            ">;>;"
        }
    .end annotation
.end field

.field public final g:LX/1VI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0Ot;LX/1W9;LX/1WB;LX/1VI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1WE;",
            ">;",
            "LX/1W9;",
            "LX/1WB;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267602
    iput-object p1, p0, LX/1W8;->a:Landroid/content/Context;

    .line 267603
    iput-object p2, p0, LX/1W8;->b:LX/0ad;

    .line 267604
    iput-object p3, p0, LX/1W8;->c:LX/0Ot;

    .line 267605
    iput-object p4, p0, LX/1W8;->d:LX/1W9;

    .line 267606
    iput-object p5, p0, LX/1W8;->e:LX/1WB;

    .line 267607
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1W8;->f:Ljava/util/HashMap;

    .line 267608
    iput-object p6, p0, LX/1W8;->g:LX/1VI;

    .line 267609
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 267553
    const/4 v0, -0x1

    .line 267554
    packed-switch p0, :pswitch_data_0

    .line 267555
    :goto_0
    :pswitch_0
    return v0

    .line 267556
    :pswitch_1
    const v0, 0x7f0b0050

    goto :goto_0

    .line 267557
    :pswitch_2
    const v0, 0x7f0b0052

    goto :goto_0

    .line 267558
    :pswitch_3
    const v0, 0x7f0b0054

    goto :goto_0

    .line 267559
    :pswitch_4
    const v0, 0x7f0b0056

    goto :goto_0

    .line 267560
    :pswitch_5
    const v0, 0x7f0b0058

    goto :goto_0

    .line 267561
    :pswitch_6
    const v0, 0x7f0b0059

    goto :goto_0

    .line 267562
    :pswitch_7
    const v0, 0x7f0b005a

    goto :goto_0

    .line 267563
    :pswitch_8
    const v0, 0x7f0b005b

    goto :goto_0

    .line 267564
    :pswitch_9
    const v0, 0x7f0b005c

    goto :goto_0

    .line 267565
    :pswitch_a
    const v0, 0x7f0b005d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/1W8;
    .locals 10

    .prologue
    .line 267590
    const-class v1, LX/1W8;

    monitor-enter v1

    .line 267591
    :try_start_0
    sget-object v0, LX/1W8;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267592
    sput-object v2, LX/1W8;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267593
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267594
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267595
    new-instance v3, LX/1W8;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 v6, 0x745

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1W9;->a(LX/0QB;)LX/1W9;

    move-result-object v7

    check-cast v7, LX/1W9;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v8

    check-cast v8, LX/1WB;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v9

    check-cast v9, LX/1VI;

    invoke-direct/range {v3 .. v9}, LX/1W8;-><init>(Landroid/content/Context;LX/0ad;LX/0Ot;LX/1W9;LX/1WB;LX/1VI;)V

    .line 267596
    move-object v0, v3

    .line 267597
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267598
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1W8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267599
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267566
    iget-object v1, p0, LX/1W8;->e:LX/1WB;

    .line 267567
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267568
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v1

    .line 267569
    iget-object v0, p0, LX/1W8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WE;

    invoke-virtual {v0, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v2

    if-eqz v1, :cond_0

    iget v0, v1, LX/1z6;->b:I

    :goto_0
    invoke-static {v0}, LX/1W8;->a(I)I

    move-result v0

    invoke-virtual {v2, v0}, LX/1XG;->h(I)LX/1XG;

    move-result-object v0

    .line 267570
    iget-object v2, p0, LX/1W8;->g:LX/1VI;

    invoke-virtual {v2}, LX/1VI;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 267571
    const v2, 0x7f0a00aa

    .line 267572
    :goto_1
    move v2, v2

    .line 267573
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1XG;->a(Ljava/lang/Integer;)LX/1XG;

    move-result-object v2

    if-eqz v1, :cond_1

    iget v0, v1, LX/1z6;->i:I

    .line 267574
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 267575
    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 267576
    :goto_3
    move-object v0, p0

    .line 267577
    invoke-virtual {v2, v0}, LX/1XG;->a(Landroid/text/Layout$Alignment;)LX/1XG;

    move-result-object v2

    if-eqz v1, :cond_2

    iget-object v0, v1, LX/1z6;->e:Ljava/lang/String;

    :goto_4
    const/4 v1, 0x0

    const/4 p1, 0x0

    .line 267578
    if-nez v0, :cond_5

    .line 267579
    :goto_5
    move-object v0, v1

    .line 267580
    invoke-virtual {v2, v0}, LX/1XG;->a(Landroid/graphics/Typeface;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x18

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const-string v0, "light"

    goto :goto_4

    .line 267581
    :cond_3
    iget-object v2, p0, LX/1W8;->b:LX/0ad;

    sget-char p1, LX/0wk;->d:C

    const/4 p2, 0x0

    invoke-interface {v2, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267582
    const-string p1, "grey"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 267583
    const v2, 0x7f0a00e4

    goto :goto_1

    .line 267584
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 267585
    :pswitch_0
    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_3

    .line 267586
    :pswitch_1
    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_3

    .line 267587
    :cond_5
    const/4 p0, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p2

    packed-switch p2, :pswitch_data_1

    :cond_6
    :goto_6
    packed-switch p0, :pswitch_data_2

    goto :goto_5

    .line 267588
    :pswitch_2
    const-string v1, "sans-serif-light"

    invoke-static {v1, p1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    goto :goto_5

    .line 267589
    :pswitch_3
    const-string p2, "light"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    move p0, p1

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x6233516
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method
