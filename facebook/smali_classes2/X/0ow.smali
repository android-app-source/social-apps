.class public LX/0ow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/0ow;


# instance fields
.field public final b:LX/0ox;

.field public final c:Landroid/content/Context;

.field public final d:LX/0Zb;

.field public final e:LX/0oy;

.field public final f:LX/0Uh;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143346
    const-class v0, LX/0ow;

    sput-object v0, LX/0ow;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ox;LX/0Zb;LX/0oy;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 143338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143339
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0ow;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 143340
    iput-object p1, p0, LX/0ow;->c:Landroid/content/Context;

    .line 143341
    iput-object p2, p0, LX/0ow;->b:LX/0ox;

    .line 143342
    iput-object p3, p0, LX/0ow;->d:LX/0Zb;

    .line 143343
    iput-object p4, p0, LX/0ow;->e:LX/0oy;

    .line 143344
    iput-object p5, p0, LX/0ow;->f:LX/0Uh;

    .line 143345
    return-void
.end method

.method public static a(LX/0QB;)LX/0ow;
    .locals 9

    .prologue
    .line 143325
    sget-object v0, LX/0ow;->h:LX/0ow;

    if-nez v0, :cond_1

    .line 143326
    const-class v1, LX/0ow;

    monitor-enter v1

    .line 143327
    :try_start_0
    sget-object v0, LX/0ow;->h:LX/0ow;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143328
    if-eqz v2, :cond_0

    .line 143329
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143330
    new-instance v3, LX/0ow;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const-class v5, LX/0ox;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0ox;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v7

    check-cast v7, LX/0oy;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/0ow;-><init>(Landroid/content/Context;LX/0ox;LX/0Zb;LX/0oy;LX/0Uh;)V

    .line 143331
    move-object v0, v3

    .line 143332
    sput-object v0, LX/0ow;->h:LX/0ow;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143333
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143334
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143335
    :cond_1
    sget-object v0, LX/0ow;->h:LX/0ow;

    return-object v0

    .line 143336
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143337
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/0ow;I)V
    .locals 3

    .prologue
    .line 143317
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ow;->c:Landroid/content/Context;

    const-string v1, "crash_count"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 143318
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 143319
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 143320
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143321
    :goto_0
    monitor-exit p0

    return-void

    .line 143322
    :catch_0
    move-exception v0

    .line 143323
    :try_start_1
    sget-object v1, LX/0ow;->a:Ljava/lang/Class;

    const-string v2, "Exception when attempting to write crash count file"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 143315
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0ow;->a(LX/0ow;I)V

    .line 143316
    return-void
.end method
