.class public LX/15X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/15X;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180864
    iput-object p1, p0, LX/15X;->a:LX/0ad;

    .line 180865
    return-void
.end method

.method public static a(LX/0QB;)LX/15X;
    .locals 4

    .prologue
    .line 180850
    sget-object v0, LX/15X;->b:LX/15X;

    if-nez v0, :cond_1

    .line 180851
    const-class v1, LX/15X;

    monitor-enter v1

    .line 180852
    :try_start_0
    sget-object v0, LX/15X;->b:LX/15X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180853
    if-eqz v2, :cond_0

    .line 180854
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180855
    new-instance p0, LX/15X;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/15X;-><init>(LX/0ad;)V

    .line 180856
    move-object v0, p0

    .line 180857
    sput-object v0, LX/15X;->b:LX/15X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180858
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180859
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180860
    :cond_1
    sget-object v0, LX/15X;->b:LX/15X;

    return-object v0

    .line 180861
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180862
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180849
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->aa:C

    const v2, 0x7f08197f

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180848
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->Z:C

    const v2, 0x7f081981

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180847
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->Y:C

    const v2, 0x7f081980

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180846
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->t:C

    const v2, 0x7f08197d

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180845
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->s:C

    const v2, 0x7f08197e

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180844
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->q:C

    const v2, 0x7f081982

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180841
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->i:C

    const v2, 0x7f081985

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180843
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->m:C

    const v2, 0x7f081983

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180842
    iget-object v0, p0, LX/15X;->a:LX/0ad;

    sget-char v1, LX/0wh;->l:C

    const v2, 0x7f081984

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
