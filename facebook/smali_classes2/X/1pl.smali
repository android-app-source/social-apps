.class public LX/1pl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:LX/1pl;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0kL;

.field private final e:LX/03V;

.field private f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public g:I

.field public h:J

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 329725
    const-class v0, LX/1pl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1pl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0kL;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoCapFeatureEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0kL;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329660
    const/4 v0, 0x0

    iput v0, p0, LX/1pl;->g:I

    .line 329661
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1pl;->h:J

    .line 329662
    const/4 v0, 0x0

    iput-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    .line 329663
    iput-object p1, p0, LX/1pl;->b:Landroid/content/Context;

    .line 329664
    iput-object p2, p0, LX/1pl;->c:LX/0Or;

    .line 329665
    iput-object p3, p0, LX/1pl;->d:LX/0kL;

    .line 329666
    iput-object p4, p0, LX/1pl;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 329667
    iput-object p5, p0, LX/1pl;->e:LX/03V;

    .line 329668
    return-void
.end method

.method public static a(LX/0QB;)LX/1pl;
    .locals 9

    .prologue
    .line 329697
    sget-object v0, LX/1pl;->j:LX/1pl;

    if-nez v0, :cond_1

    .line 329698
    const-class v1, LX/1pl;

    monitor-enter v1

    .line 329699
    :try_start_0
    sget-object v0, LX/1pl;->j:LX/1pl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 329700
    if-eqz v2, :cond_0

    .line 329701
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 329702
    new-instance v3, LX/1pl;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x1487

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/1pl;-><init>(Landroid/content/Context;LX/0Or;LX/0kL;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V

    .line 329703
    move-object v0, v3

    .line 329704
    sput-object v0, LX/1pl;->j:LX/1pl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 329706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 329707
    :cond_1
    sget-object v0, LX/1pl;->j:LX/1pl;

    return-object v0

    .line 329708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 329709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(IJLjava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329710
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/1pl;->g:I

    .line 329711
    iput-wide p2, p0, LX/1pl;->h:J

    .line 329712
    iget-object v0, p0, LX/1pl;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->y:LX/0Tn;

    iget-wide v2, p0, LX/1pl;->h:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 329713
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 329714
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    .line 329715
    :goto_0
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 329716
    new-instance v0, LX/0lC;

    invoke-direct {v0}, LX/0lC;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329717
    :try_start_1
    invoke-virtual {v0, p4}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 329718
    if-eqz v0, :cond_0

    .line 329719
    iget-object v1, p0, LX/1pl;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dQ;->x:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329720
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 329721
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 329722
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 329723
    :catch_0
    move-exception v0

    .line 329724
    :try_start_3
    iget-object v1, p0, LX/1pl;->e:LX/03V;

    sget-object v2, LX/1pl;->a:Ljava/lang/String;

    const-string v3, "Error while serializing freePhotos"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/15i;I)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateQuotaFromMutation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 329685
    if-eqz p2, :cond_2

    .line 329686
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 329687
    const v0, 0x46733b6

    invoke-static {p1, p2, v6, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 329688
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/2uF;->i()LX/3Sg;

    move-result-object v0

    .line 329689
    :goto_1
    invoke-virtual {v0}, LX/3Sg;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329690
    invoke-virtual {v0}, LX/3Sg;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 329691
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329692
    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v5}, LX/15i;->k(II)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 329693
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 329694
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 329695
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v0

    invoke-virtual {p1, p2, v5}, LX/15i;->k(II)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3, v1}, LX/1pl;->a(IJLjava/util/Map;)V

    .line 329696
    :cond_2
    return-void
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 329669
    iget-object v0, p0, LX/1pl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 329670
    :goto_0
    return v0

    .line 329671
    :cond_0
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 329672
    iget-object v0, p0, LX/1pl;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->x:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329673
    if-eqz v0, :cond_1

    .line 329674
    :try_start_0
    new-instance v2, LX/0lC;

    invoke-direct {v2}, LX/0lC;-><init>()V

    .line 329675
    new-instance v3, LX/5N5;

    invoke-direct {v3, p0}, LX/5N5;-><init>(LX/1pl;)V

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 329676
    if-eqz v0, :cond_1

    .line 329677
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, LX/1pl;->i:Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329678
    :cond_1
    :goto_1
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 329679
    iget-object v0, p0, LX/1pl;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 329680
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 329681
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 329682
    :catch_0
    move-exception v0

    .line 329683
    iget-object v2, p0, LX/1pl;->e:LX/03V;

    sget-object v3, LX/1pl;->a:Ljava/lang/String;

    const-string v4, "Error while de-serializing freePhotos"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 329684
    goto :goto_0
.end method

.method public final b(LX/15i;I)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateQuota"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 329647
    if-eqz p2, :cond_2

    .line 329648
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 329649
    const v0, 0x70d8a764    # 5.36409E29f

    invoke-static {p1, p2, v6, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 329650
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/2uF;->i()LX/3Sg;

    move-result-object v0

    .line 329651
    :goto_1
    invoke-virtual {v0}, LX/3Sg;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329652
    invoke-virtual {v0}, LX/3Sg;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 329653
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329654
    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v5}, LX/15i;->k(II)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 329655
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 329656
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 329657
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v0

    invoke-virtual {p1, p2, v5}, LX/15i;->k(II)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3, v1}, LX/1pl;->a(IJLjava/util/Map;)V

    .line 329658
    :cond_2
    return-void
.end method
