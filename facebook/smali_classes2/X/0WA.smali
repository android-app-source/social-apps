.class public LX/0WA;
.super LX/0WB;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile i:LX/0WA;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0WV;

.field private final d:LX/03V;

.field private final e:LX/0Uh;

.field private final f:LX/0Uh;

.field private final g:LX/0Wd;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75549
    const-class v0, LX/0WA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0WA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0WV;LX/03V;LX/0Uh;LX/0Uh;LX/0Wd;)V
    .locals 1
    .param p4    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0WV;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 75550
    invoke-direct {p0}, LX/0WB;-><init>()V

    .line 75551
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0WA;->h:Z

    .line 75552
    iput-object p1, p0, LX/0WA;->b:LX/0Ot;

    .line 75553
    iput-object p2, p0, LX/0WA;->c:LX/0WV;

    .line 75554
    iput-object p3, p0, LX/0WA;->d:LX/03V;

    .line 75555
    iput-object p4, p0, LX/0WA;->e:LX/0Uh;

    .line 75556
    iput-object p5, p0, LX/0WA;->f:LX/0Uh;

    .line 75557
    iput-object p6, p0, LX/0WA;->g:LX/0Wd;

    .line 75558
    return-void
.end method

.method public static a(LX/0QB;)LX/0WA;
    .locals 10

    .prologue
    .line 75559
    sget-object v0, LX/0WA;->i:LX/0WA;

    if-nez v0, :cond_1

    .line 75560
    const-class v1, LX/0WA;

    monitor-enter v1

    .line 75561
    :try_start_0
    sget-object v0, LX/0WA;->i:LX/0WA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75562
    if-eqz v2, :cond_0

    .line 75563
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75564
    new-instance v3, LX/0WA;

    const/16 v4, 0xbc

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v9

    check-cast v9, LX/0Wd;

    invoke-direct/range {v3 .. v9}, LX/0WA;-><init>(LX/0Ot;LX/0WV;LX/03V;LX/0Uh;LX/0Uh;LX/0Wd;)V

    .line 75565
    move-object v0, v3

    .line 75566
    sput-object v0, LX/0WA;->i:LX/0WA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75567
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75568
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75569
    :cond_1
    sget-object v0, LX/0WA;->i:LX/0WA;

    return-object v0

    .line 75570
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75572
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 75573
    const/16 v1, 0x1

    move v1, v1

    .line 75574
    if-eqz v1, :cond_0

    .line 75575
    invoke-virtual {p0}, LX/0WA;->d()LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 75576
    const-string v1, "en"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75577
    :goto_0
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    .line 75578
    :cond_0
    iget-object v1, p0, LX/0WA;->g:LX/0Wd;

    new-instance v2, Lcom/facebook/katana/languages/Fb4aSupportedLanguages$1;

    invoke-direct {v2, p0}, Lcom/facebook/katana/languages/Fb4aSupportedLanguages$1;-><init>(LX/0WA;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 75579
    sget-object v1, LX/01Q;->f:[Ljava/lang/String;

    move-object v1, v1

    .line 75580
    invoke-static {v1}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_0
.end method

.method public final c()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75581
    const/16 v0, 0x0

    move v0, v0

    .line 75582
    if-nez v0, :cond_0

    .line 75583
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 75584
    :goto_0
    return-object v0

    .line 75585
    :cond_0
    sget-object v0, LX/01Q;->f:[Ljava/lang/String;

    move-object v0, v0

    .line 75586
    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 75587
    const-string v1, "en"

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 75588
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 75589
    const/16 v0, 0x1

    move v0, v0

    .line 75590
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0WA;->e:LX/0Uh;

    const/16 v1, 0x2a

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 75591
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 75592
    :goto_0
    return-object v0

    .line 75593
    :cond_1
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 75594
    sget-object v1, LX/0eH;->a:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 75595
    iget-object v1, p0, LX/0WA;->e:LX/0Uh;

    const/16 v2, 0x27

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75596
    const-string v1, "my"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75597
    :cond_2
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x46d

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 75598
    const-string v1, "qz"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75599
    :cond_3
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x3cc

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 75600
    const-string v1, "fb"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75601
    :cond_4
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x446

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 75602
    const-string v1, "sr"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75603
    :cond_5
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x37d

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 75604
    const-string v1, "bg"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75605
    :cond_6
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x368

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 75606
    const-string v1, "sq"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75607
    :cond_7
    iget-object v1, p0, LX/0WA;->f:LX/0Uh;

    const/16 v2, 0x3f4

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 75608
    const-string v1, "lt"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 75609
    :cond_8
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method
