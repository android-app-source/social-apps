.class public LX/17v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17Q;

.field public final b:LX/0Zb;

.field public final c:Landroid/content/Context;

.field public final d:LX/17d;


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;Landroid/content/Context;LX/17d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 199730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199731
    iput-object p1, p0, LX/17v;->a:LX/17Q;

    .line 199732
    iput-object p2, p0, LX/17v;->b:LX/0Zb;

    .line 199733
    iput-object p3, p0, LX/17v;->c:Landroid/content/Context;

    .line 199734
    iput-object p4, p0, LX/17v;->d:LX/17d;

    .line 199735
    return-void
.end method

.method public static a(LX/17v;Landroid/net/Uri;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 199736
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/17d;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 199737
    :cond_0
    :goto_0
    return v0

    .line 199738
    :cond_1
    invoke-static {p1}, LX/17d;->c(Landroid/net/Uri;)LX/32n;

    move-result-object v2

    .line 199739
    sget-object v1, LX/32n;->NONE:LX/32n;

    if-eq v2, v1, :cond_0

    .line 199740
    iget-object v1, p0, LX/17v;->d:LX/17d;

    invoke-virtual {v1, p1}, LX/17d;->b(Landroid/net/Uri;)Ljava/util/Collection;

    move-result-object v1

    .line 199741
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 199742
    const/4 v4, 0x0

    .line 199743
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    move v3, v4

    .line 199744
    :goto_1
    move v1, v3

    .line 199745
    sget-object v3, LX/32n;->INSTALLED:LX/32n;

    if-ne v2, v3, :cond_3

    .line 199746
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 199747
    :cond_3
    sget-object v3, LX/32n;->NOT_INSTALLED:LX/32n;

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 199748
    goto :goto_0

    .line 199749
    :cond_4
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 199750
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 199751
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 199752
    iget-object p1, p0, LX/17v;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    .line 199753
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p1, v3, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199754
    :goto_2
    move v3, v6

    .line 199755
    if-eqz v3, :cond_5

    .line 199756
    const/4 v3, 0x1

    goto :goto_1

    :cond_6
    move v3, v4

    .line 199757
    goto :goto_1

    .line 199758
    :catch_0
    move v6, v7

    goto :goto_2

    :cond_7
    move v6, v7

    goto :goto_2
.end method
