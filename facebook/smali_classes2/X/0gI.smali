.class public LX/0gI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/0gI;


# instance fields
.field public final a:LX/0xB;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AGo;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AFg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/7gh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7gh",
            "<",
            "Lcom/facebook/audience/direct/data/DirectUserThreadProvider$DirectUserThreadObserver;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1El;

.field private final g:LX/AEv;

.field public final h:LX/AF0;

.field public final i:LX/1EY;

.field public final j:LX/AFJ;

.field private final k:LX/0gK;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/1El;LX/AEv;LX/AF0;LX/1EY;LX/AFJ;LX/0xB;LX/0gK;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/AGo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AFg;",
            ">;",
            "LX/1El;",
            "LX/AEv;",
            "LX/AF0;",
            "LX/1EY;",
            "LX/AFJ;",
            "LX/0xB;",
            "LX/0gK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 111854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111855
    new-instance v0, LX/7gh;

    invoke-direct {v0}, LX/7gh;-><init>()V

    iput-object v0, p0, LX/0gI;->e:LX/7gh;

    .line 111856
    iput-object p1, p0, LX/0gI;->d:Ljava/util/concurrent/ExecutorService;

    .line 111857
    iput-object p2, p0, LX/0gI;->b:LX/0Ot;

    .line 111858
    iput-object p3, p0, LX/0gI;->c:LX/0Ot;

    .line 111859
    iput-object p4, p0, LX/0gI;->f:LX/1El;

    .line 111860
    iget-object v0, p0, LX/0gI;->f:LX/1El;

    new-instance v1, LX/AF2;

    invoke-direct {v1, p0}, LX/AF2;-><init>(LX/0gI;)V

    invoke-virtual {v0, v1}, LX/1El;->a(LX/AF1;)V

    .line 111861
    iput-object p5, p0, LX/0gI;->g:LX/AEv;

    .line 111862
    iput-object p6, p0, LX/0gI;->h:LX/AF0;

    .line 111863
    iput-object p7, p0, LX/0gI;->i:LX/1EY;

    .line 111864
    iput-object p8, p0, LX/0gI;->j:LX/AFJ;

    .line 111865
    iget-object v0, p0, LX/0gI;->i:LX/1EY;

    new-instance v1, LX/AF3;

    invoke-direct {v1, p0}, LX/AF3;-><init>(LX/0gI;)V

    .line 111866
    iget-object v2, v0, LX/1EY;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 111867
    iget-object v2, v0, LX/1EY;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111868
    :cond_0
    iget-object v0, p0, LX/0gI;->j:LX/AFJ;

    new-instance v1, LX/AF4;

    invoke-direct {v1, p0}, LX/AF4;-><init>(LX/0gI;)V

    .line 111869
    iput-object v1, v0, LX/AFJ;->c:LX/AF4;

    .line 111870
    iget-object v0, p0, LX/0gI;->g:LX/AEv;

    sget-object v1, LX/7gw;->DIRECT:LX/7gw;

    new-instance v2, LX/AF5;

    invoke-direct {v2, p0}, LX/AF5;-><init>(LX/0gI;)V

    .line 111871
    iget-object p1, v0, LX/AEv;->b:Ljava/util/Map;

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111872
    iput-object p9, p0, LX/0gI;->a:LX/0xB;

    .line 111873
    iput-object p10, p0, LX/0gI;->k:LX/0gK;

    .line 111874
    return-void
.end method

.method public static a(LX/0QB;)LX/0gI;
    .locals 14

    .prologue
    .line 111875
    sget-object v0, LX/0gI;->l:LX/0gI;

    if-nez v0, :cond_1

    .line 111876
    const-class v1, LX/0gI;

    monitor-enter v1

    .line 111877
    :try_start_0
    sget-object v0, LX/0gI;->l:LX/0gI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 111878
    if-eqz v2, :cond_0

    .line 111879
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 111880
    new-instance v3, LX/0gI;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x17b3

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x17b2

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1El;->a(LX/0QB;)LX/1El;

    move-result-object v7

    check-cast v7, LX/1El;

    invoke-static {v0}, LX/AEv;->a(LX/0QB;)LX/AEv;

    move-result-object v8

    check-cast v8, LX/AEv;

    .line 111881
    new-instance v10, LX/AF0;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v9

    check-cast v9, LX/0fO;

    invoke-direct {v10, v9}, LX/AF0;-><init>(LX/0fO;)V

    .line 111882
    move-object v9, v10

    .line 111883
    check-cast v9, LX/AF0;

    invoke-static {v0}, LX/1EY;->a(LX/0QB;)LX/1EY;

    move-result-object v10

    check-cast v10, LX/1EY;

    invoke-static {v0}, LX/AFJ;->a(LX/0QB;)LX/AFJ;

    move-result-object v11

    check-cast v11, LX/AFJ;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v12

    check-cast v12, LX/0xB;

    invoke-static {v0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v13

    check-cast v13, LX/0gK;

    invoke-direct/range {v3 .. v13}, LX/0gI;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/1El;LX/AEv;LX/AF0;LX/1EY;LX/AFJ;LX/0xB;LX/0gK;)V

    .line 111884
    move-object v0, v3

    .line 111885
    sput-object v0, LX/0gI;->l:LX/0gI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111886
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 111887
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 111888
    :cond_1
    sget-object v0, LX/0gI;->l:LX/0gI;

    return-object v0

    .line 111889
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 111890
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/0gI;LX/0Px;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7h0;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 111842
    if-nez p1, :cond_0

    .line 111843
    :goto_0
    return-void

    .line 111844
    :cond_0
    iget-object v0, p0, LX/0gI;->g:LX/AEv;

    new-instance v1, LX/AFA;

    invoke-direct {v1, p0, p2}, LX/AFA;-><init>(LX/0gI;Z)V

    .line 111845
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 111846
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, p0, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7h0;

    .line 111847
    iget-boolean p2, v2, LX/7h0;->h:Z

    move p2, p2

    .line 111848
    if-nez p2, :cond_1

    .line 111849
    iget-object p2, v2, LX/7h0;->i:Ljava/lang/String;

    move-object v2, p2

    .line 111850
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 111851
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 111852
    :cond_2
    iget-object v2, v0, LX/AEv;->c:LX/AFD;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    new-instance v4, LX/AEt;

    invoke-direct {v4, v0, p1, v1}, LX/AEt;-><init>(LX/AEv;LX/0Px;LX/AFA;)V

    invoke-virtual {v2, v3, v4}, LX/AFD;->a(Ljava/util/List;LX/AEs;)V

    .line 111853
    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 111822
    iget-object v0, p0, LX/0gI;->k:LX/0gK;

    invoke-virtual {v0}, LX/0gK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111823
    iget-object v0, p0, LX/0gI;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFg;

    new-instance v1, LX/AF6;

    invoke-direct {v1, p0, p1}, LX/AF6;-><init>(LX/0gI;Z)V

    .line 111824
    if-eqz p1, :cond_1

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 111825
    :goto_0
    new-instance v3, LX/AGc;

    invoke-direct {v3}, LX/AGc;-><init>()V

    move-object v3, v3

    .line 111826
    const-string p0, "3"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 111827
    const-string p0, "2"

    const/16 p1, 0x1770

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 111828
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 111829
    iget-object p0, v0, LX/AFg;->b:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v3, v3

    .line 111830
    new-instance p0, LX/2rB;

    invoke-direct {p0, v0, v1}, LX/2rB;-><init>(LX/AFg;LX/AF6;)V

    iget-object p1, v0, LX/AFg;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 111831
    :goto_1
    return-void

    .line 111832
    :cond_0
    iget-object v0, p0, LX/0gI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AGo;

    new-instance v1, LX/AF7;

    invoke-direct {v1, p0, p1}, LX/AF7;-><init>(LX/0gI;Z)V

    .line 111833
    if-eqz p1, :cond_2

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 111834
    :goto_2
    new-instance v3, LX/2rF;

    invoke-direct {v3}, LX/2rF;-><init>()V

    move-object v3, v3

    .line 111835
    const-string p0, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 111836
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 111837
    iget-object p0, v0, LX/AGo;->b:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v3, v3

    .line 111838
    new-instance p0, LX/AGn;

    invoke-direct {p0, v0, v1}, LX/AGn;-><init>(LX/AGo;LX/AF7;)V

    iget-object p1, v0, LX/AGo;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 111839
    goto :goto_1

    .line 111840
    :cond_1
    sget-object v2, LX/0zS;->b:LX/0zS;

    goto :goto_0

    .line 111841
    :cond_2
    sget-object v2, LX/0zS;->b:LX/0zS;

    goto :goto_2
.end method
