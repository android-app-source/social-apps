.class public LX/1UQ;
.super LX/1UR;
.source ""

# interfaces
.implements LX/1Qr;
.implements LX/1Rq;
.implements LX/1OP;
.implements LX/1Rs;


# instance fields
.field public final a:LX/1Rq;

.field private final b:LX/1R4;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

.field public e:LX/1XQ;

.field public f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;LX/1Rq;Ljava/util/List;LX/0g7;LX/1Jy;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;",
            "LX/1Rq;",
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;",
            "LX/0g7;",
            "LX/1Jy;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 255864
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0, v1}, LX/1UR;-><init>(LX/0Px;Z)V

    .line 255865
    new-instance v0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-direct {v0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;-><init>()V

    iput-object v0, p0, LX/1UQ;->d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    .line 255866
    const/4 v0, -0x1

    iput v0, p0, LX/1UQ;->f:I

    .line 255867
    iput-object p1, p0, LX/1UQ;->g:Ljava/util/List;

    .line 255868
    iput-object p3, p0, LX/1UQ;->h:Ljava/util/List;

    .line 255869
    invoke-direct {p0}, LX/1UQ;->f()LX/1R4;

    move-result-object v0

    iput-object v0, p0, LX/1UQ;->b:LX/1R4;

    .line 255870
    new-instance v2, LX/1XQ;

    invoke-static {p5}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    invoke-static {p5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    const-class v3, LX/1XR;

    invoke-interface {p5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1XR;

    invoke-static {p5}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v8

    check-cast v8, LX/0Wd;

    const/16 v3, 0x67b

    invoke-static {p5, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    move-object v3, p4

    move-object v4, p0

    invoke-direct/range {v2 .. v9}, LX/1XQ;-><init>(LX/0g7;LX/1UQ;LX/1LV;Landroid/content/res/Resources;LX/1XR;LX/0Wd;LX/0Or;)V

    .line 255871
    move-object v0, v2

    .line 255872
    iput-object v0, p0, LX/1UQ;->e:LX/1XQ;

    .line 255873
    new-instance v0, LX/1XV;

    invoke-direct {v0, p0}, LX/1XV;-><init>(LX/1UQ;)V

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 255874
    new-instance v0, LX/1XW;

    invoke-direct {v0, p0}, LX/1XW;-><init>(LX/1UQ;)V

    invoke-virtual {p0, v0}, LX/1UQ;->a(LX/1KR;)V

    .line 255875
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1UQ;->c:Ljava/util/List;

    .line 255876
    iput-object p2, p0, LX/1UQ;->a:LX/1Rq;

    .line 255877
    new-instance v0, LX/1XX;

    invoke-direct {v0, p0}, LX/1XX;-><init>(LX/1UQ;)V

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 255878
    iget-object v0, p0, LX/1UQ;->d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 255879
    return-void
.end method

.method private static a(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 255910
    move v1, v0

    move v2, v0

    .line 255911
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 255912
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    add-int/2addr v2, v0

    .line 255913
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255914
    :cond_0
    return v2
.end method

.method public static a(LX/0Px;LX/1Rq;LX/0Px;LX/0g7;LX/1Jy;)LX/1UQ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;",
            "LX/1Rq;",
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;",
            "LX/0g7;",
            "LX/1Jy;",
            ")",
            "LX/1UQ;"
        }
    .end annotation

    .prologue
    .line 255915
    invoke-static {p0, p3}, LX/1UQ;->a(LX/0Px;LX/0g7;)Ljava/util/List;

    move-result-object v1

    .line 255916
    invoke-static {p2, p3}, LX/1UQ;->a(LX/0Px;LX/0g7;)Ljava/util/List;

    move-result-object v3

    .line 255917
    new-instance v0, LX/1UQ;

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/1UQ;-><init>(Ljava/util/List;LX/1Rq;Ljava/util/List;LX/0g7;LX/1Jy;)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/0g7;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;",
            "LX/0g7;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255918
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 255919
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    .line 255920
    new-instance v4, LX/1US;

    .line 255921
    iget-object v5, p1, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v5, v5

    .line 255922
    invoke-direct {v4, v0, v5}, LX/1US;-><init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V

    .line 255923
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255924
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255925
    :cond_0
    return-object v2
.end method

.method private f()LX/1R4;
    .locals 1

    .prologue
    .line 255926
    new-instance v0, LX/1XP;

    invoke-direct {v0, p0}, LX/1XP;-><init>(LX/1UQ;)V

    return-object v0
.end method

.method private h(I)V
    .locals 6

    .prologue
    .line 255927
    invoke-virtual {p0, p1}, LX/1UR;->e(I)LX/1OO;

    move-result-object v0

    iget-object v1, p0, LX/1UQ;->a:LX/1Rq;

    if-eq v0, v1, :cond_0

    .line 255928
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position %d does not belong to the NewsFeedAdapter (positions %d to %d)"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result v4

    iget-object v5, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v5}, LX/1OP;->ij_()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255929
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1KR;)V
    .locals 1

    .prologue
    .line 255930
    iget-object v0, p0, LX/1UQ;->d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->a(LX/1KR;)V

    .line 255931
    return-void
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 255932
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v0, v0

    .line 255933
    iget-object v1, p0, LX/1UQ;->a:LX/1Rq;

    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 255934
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 255935
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 255936
    iget-object v0, p0, LX/1UQ;->e:LX/1XQ;

    .line 255937
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 255938
    iget p0, v0, LX/1XQ;->g:I

    if-ne p0, v1, :cond_0

    .line 255939
    :goto_0
    return-void

    .line 255940
    :cond_0
    iput v1, v0, LX/1XQ;->g:I

    .line 255941
    invoke-static {v0}, LX/1XQ;->c(LX/1XQ;)V

    .line 255942
    invoke-virtual {v0}, LX/1XQ;->a()V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 255907
    iget v0, p0, LX/1UQ;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 255908
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-virtual {p0, v0}, LX/1UR;->a(LX/1OO;)I

    move-result v0

    iput v0, p0, LX/1UQ;->f:I

    .line 255909
    :cond_0
    iget v0, p0, LX/1UQ;->f:I

    return v0
.end method

.method public final b(LX/1KR;)V
    .locals 1

    .prologue
    .line 255943
    iget-object v0, p0, LX/1UQ;->d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b(LX/1KR;)V

    .line 255944
    return-void
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 255897
    if-gez p1, :cond_1

    .line 255898
    :cond_0
    :goto_0
    return v0

    .line 255899
    :cond_1
    iget-object v2, p0, LX/1UQ;->g:Ljava/util/List;

    invoke-static {v2}, LX/1UQ;->a(Ljava/util/List;)I

    move-result v2

    .line 255900
    if-ge p1, v2, :cond_2

    move v0, v1

    .line 255901
    goto :goto_0

    .line 255902
    :cond_2
    sub-int v2, p1, v2

    .line 255903
    iget-object v3, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v3, v2}, LX/1Rs;->b(I)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 255904
    goto :goto_0

    .line 255905
    :cond_3
    iget-object v3, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v3}, LX/1OP;->ij_()I

    move-result v3

    sub-int/2addr v2, v3

    .line 255906
    iget-object v3, p0, LX/1UQ;->h:Ljava/util/List;

    invoke-static {v3}, LX/1UQ;->a(Ljava/util/List;)I

    move-result v3

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 255896
    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result v0

    iget-object v1, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 255894
    invoke-direct {p0, p1}, LX/1UQ;->h(I)V

    .line 255895
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-virtual {p0, p1}, LX/1UR;->f(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Qr;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 255893
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    return v0
.end method

.method public final dispose()V
    .locals 2

    .prologue
    .line 255888
    invoke-super {p0}, LX/1UR;->dispose()V

    .line 255889
    iget-object v0, p0, LX/1UQ;->e:LX/1XQ;

    .line 255890
    iget-object v1, v0, LX/1XQ;->j:LX/1XU;

    invoke-virtual {v1}, LX/1Rm;->b()V

    .line 255891
    iget-object v0, p0, LX/1UQ;->d:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->dispose()V

    .line 255892
    return-void
.end method

.method public final e()LX/1R4;
    .locals 1

    .prologue
    .line 255887
    iget-object v0, p0, LX/1UQ;->b:LX/1R4;

    return-object v0
.end method

.method public final g(I)I
    .locals 2

    .prologue
    .line 255885
    invoke-direct {p0, p1}, LX/1UQ;->h(I)V

    .line 255886
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-virtual {p0, p1}, LX/1UR;->f(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Qr;->g(I)I

    move-result v0

    return v0
.end method

.method public final h_(I)I
    .locals 2

    .prologue
    .line 255882
    invoke-virtual {p0, p1}, LX/1UR;->e(I)LX/1OO;

    move-result-object v0

    iget-object v1, p0, LX/1UQ;->a:LX/1Rq;

    if-eq v0, v1, :cond_0

    .line 255883
    const/high16 v0, -0x80000000

    .line 255884
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-virtual {p0, p1}, LX/1UR;->f(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Qr;->h_(I)I

    move-result v0

    goto :goto_0
.end method

.method public final m_(I)I
    .locals 2

    .prologue
    .line 255881
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->m_(I)I

    move-result v0

    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final n_(I)I
    .locals 2

    .prologue
    .line 255880
    iget-object v0, p0, LX/1UQ;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->n_(I)I

    move-result v0

    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
