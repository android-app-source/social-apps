.class public final LX/0S5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0RB;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/0RN;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fR;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;LX/0RB;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;",
            "LX/0RB;",
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/0RN;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fR;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 60716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60717
    iput-object p1, p0, LX/0S5;->a:Ljava/util/Map;

    .line 60718
    iput-object p2, p0, LX/0S5;->b:LX/0RB;

    .line 60719
    iput-object p3, p0, LX/0S5;->c:Ljava/util/Map;

    .line 60720
    iput-object p4, p0, LX/0S5;->d:Ljava/util/Map;

    .line 60721
    iput-object p5, p0, LX/0S5;->e:Ljava/util/List;

    .line 60722
    return-void
.end method
