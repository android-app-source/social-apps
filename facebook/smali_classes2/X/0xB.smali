.class public LX/0xB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0xB;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 162191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162192
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, LX/0xB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 162193
    return-void
.end method

.method public static a(LX/0QB;)LX/0xB;
    .locals 4

    .prologue
    .line 162194
    sget-object v0, LX/0xB;->b:LX/0xB;

    if-nez v0, :cond_1

    .line 162195
    const-class v1, LX/0xB;

    monitor-enter v1

    .line 162196
    :try_start_0
    sget-object v0, LX/0xB;->b:LX/0xB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 162197
    if-eqz v2, :cond_0

    .line 162198
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 162199
    new-instance p0, LX/0xB;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/0xB;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 162200
    move-object v0, p0

    .line 162201
    sput-object v0, LX/0xB;->b:LX/0xB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162202
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 162203
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162204
    :cond_1
    sget-object v0, LX/0xB;->b:LX/0xB;

    return-object v0

    .line 162205
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 162206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/12j;)I
    .locals 3

    .prologue
    .line 162207
    iget-object v0, p0, LX/0xB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/12j;->getCountPrefKey(LX/12j;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final a(LX/0xA;)V
    .locals 2

    .prologue
    .line 162208
    iget-object v0, p0, LX/0xB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {}, LX/12j;->getCountPrefKeys()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 162209
    return-void
.end method

.method public final a(LX/12j;I)V
    .locals 2

    .prologue
    .line 162210
    iget-object v0, p0, LX/0xB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 162211
    invoke-static {p1}, LX/12j;->getCountPrefKey(LX/12j;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 162212
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 162213
    return-void
.end method

.method public final b(LX/0xA;)V
    .locals 2

    .prologue
    .line 162214
    iget-object v0, p0, LX/0xB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {}, LX/12j;->getCountPrefKeys()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(Ljava/util/Set;LX/0dN;)V

    .line 162215
    return-void
.end method
