.class public LX/14g;
.super LX/14h;
.source ""


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpEntity;)V
    .locals 0

    .prologue
    .line 179113
    invoke-direct {p0, p1}, LX/14h;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 179114
    return-void
.end method


# virtual methods
.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 3

    .prologue
    .line 179112
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "Content-Encoding"

    const-string v2, "gzip"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 179111
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 179110
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 179103
    if-nez p1, :cond_0

    .line 179104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179105
    :cond_0
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, p1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 179106
    :try_start_0
    iget-object v0, p0, Lorg/apache/http/entity/HttpEntityWrapper;->wrappedEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v0, v1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179107
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 179108
    return-void

    .line 179109
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    throw v0
.end method
