.class public LX/17d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/17d;


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 199238
    const-class v0, LX/17d;

    sput-object v0, LX/17d;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 199239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199240
    iput-object p1, p0, LX/17d;->b:LX/0Zb;

    .line 199241
    iput-object p2, p0, LX/17d;->c:LX/0Uh;

    .line 199242
    return-void
.end method

.method public static a(LX/0QB;)LX/17d;
    .locals 5

    .prologue
    .line 199224
    sget-object v0, LX/17d;->d:LX/17d;

    if-nez v0, :cond_1

    .line 199225
    const-class v1, LX/17d;

    monitor-enter v1

    .line 199226
    :try_start_0
    sget-object v0, LX/17d;->d:LX/17d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 199227
    if-eqz v2, :cond_0

    .line 199228
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 199229
    new-instance p0, LX/17d;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/17d;-><init>(LX/0Zb;LX/0Uh;)V

    .line 199230
    move-object v0, p0

    .line 199231
    sput-object v0, LX/17d;->d:LX/17d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199232
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 199233
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 199234
    :cond_1
    sget-object v0, LX/17d;->d:LX/17d;

    return-object v0

    .line 199235
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 199236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/31z;)LX/31z;
    .locals 9
    .param p1    # LX/31z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 199243
    if-nez p1, :cond_1

    move-object p1, v1

    .line 199244
    :cond_0
    return-object p1

    .line 199245
    :cond_1
    iget-object v0, p0, LX/17d;->c:LX/0Uh;

    const/16 v2, 0x452

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 199246
    if-eqz v0, :cond_0

    .line 199247
    iget-object v0, p1, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32r;

    .line 199248
    iget-object v3, v0, LX/32r;->appSiteUrl:Ljava/lang/String;

    invoke-static {v3}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v3

    .line 199249
    iget-object v4, v0, LX/32r;->marketUri:Ljava/lang/String;

    invoke-static {v4}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v4

    .line 199250
    iget-object v5, v0, LX/32r;->fallbackUrl:Ljava/lang/String;

    invoke-static {v5}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v5

    .line 199251
    if-nez v3, :cond_3

    if-nez v4, :cond_3

    if-eqz v5, :cond_4

    .line 199252
    :cond_3
    iget-object v6, p0, LX/17d;->b:LX/0Zb;

    const-string v7, "fb4a_sanitized_thirdparty_appsite"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 199253
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 199254
    const-string v7, "appsite_data"

    invoke-virtual {v0}, LX/32r;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 199255
    :cond_4
    if-eqz v3, :cond_5

    .line 199256
    iput-object v1, v0, LX/32r;->appSiteUrl:Ljava/lang/String;

    .line 199257
    :cond_5
    if-eqz v4, :cond_6

    .line 199258
    iput-object v1, v0, LX/32r;->marketUri:Ljava/lang/String;

    .line 199259
    :cond_6
    if-eqz v5, :cond_2

    .line 199260
    iput-object v1, v0, LX/32r;->fallbackUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 199261
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 199262
    const-string v0, "market_uri"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199263
    const-string v1, "ref"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 199264
    const-string v1, "app_name"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 199265
    if-nez v0, :cond_2

    .line 199266
    if-eqz p2, :cond_7

    iget-object v1, p2, LX/31z;->appSites:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 199267
    iget-object v1, p2, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32r;

    .line 199268
    iget-object v1, v0, LX/32r;->marketUri:Ljava/lang/String;

    invoke-static {v1}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 199269
    iget-object v3, v0, LX/32r;->appName:Ljava/lang/String;

    .line 199270
    iget-object v0, v0, LX/32r;->isAppLink:Ljava/lang/String;

    move-object v9, v3

    move-object v3, v1

    move-object v1, v9

    .line 199271
    :goto_1
    if-nez v3, :cond_3

    .line 199272
    :cond_0
    :goto_2
    return-object v2

    :cond_1
    move-object v0, v1

    .line 199273
    goto :goto_0

    :cond_2
    move-object v1, v3

    move-object v3, v0

    move-object v0, v2

    .line 199274
    :cond_3
    const-string v6, "id"

    invoke-virtual {v3, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 199275
    new-instance v7, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v7, v8, v3}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 199276
    iput-object p1, v7, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199277
    if-eqz v0, :cond_4

    .line 199278
    const-string v3, "is_app_link"

    invoke-virtual {v7, v3, v0}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199279
    :cond_4
    const/high16 v0, 0x10000000

    invoke-virtual {v7, v0}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->addFlags(I)Landroid/content/Intent;

    .line 199280
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 199281
    const-string v0, "extra_app_name"

    invoke-virtual {v7, v0, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199282
    :cond_5
    const/high16 v0, 0x10000

    invoke-virtual {v4, v7, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 199283
    if-eqz v0, :cond_0

    .line 199284
    invoke-static {p1}, LX/17d;->f(Landroid/net/Uri;)LX/6Xd;

    move-result-object v0

    .line 199285
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    .line 199286
    invoke-virtual {v1}, LX/0lC;->e()LX/0m9;

    move-result-object v2

    .line 199287
    invoke-virtual {v1}, LX/0lC;->e()LX/0m9;

    move-result-object v3

    .line 199288
    const-string v4, "version"

    const-string v8, "2"

    invoke-virtual {v2, v4, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 199289
    const-string v4, "method"

    const-string v8, "applink"

    invoke-virtual {v3, v4, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 199290
    const-string v4, "bridge_args"

    invoke-virtual {v2, v4, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 199291
    iget-object v3, v0, LX/6Xd;->f:LX/0lF;

    if-eqz v3, :cond_6

    .line 199292
    const-string v3, "method_args"

    iget-object v0, v0, LX/6Xd;->f:LX/0lF;

    invoke-virtual {v2, v3, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 199293
    :goto_3
    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, p0}, LX/6Xe;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199294
    :goto_4
    const-string v0, "application_link_type"

    const-string v1, "app_store"

    invoke-virtual {v7, v0, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199295
    const-string v0, "ref"

    invoke-virtual {v7, v0, v5}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199296
    const-string v0, "unit_type"

    const-string v1, "single_app_install"

    invoke-virtual {v7, v0, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199297
    const-string v0, "package_name"

    invoke-virtual {v7, v0, v6}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199298
    const-string v0, "app_id"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199299
    invoke-static {v0, v7}, LX/17d;->a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    goto/16 :goto_2

    .line 199300
    :cond_6
    :try_start_1
    const-string v0, "method_args"

    invoke-virtual {v1}, LX/0lC;->e()LX/0m9;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 199301
    :catch_0
    move-exception v0

    .line 199302
    sget-object v1, LX/17d;->a:Ljava/lang/Class;

    const-string v2, "Error persisting install data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    move-object v1, v3

    move-object v3, v0

    move-object v0, v2

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;LX/31z;LX/47G;)Landroid/content/Intent;
    .locals 6
    .param p3    # LX/47G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 199303
    invoke-static {p1}, LX/17d;->f(Landroid/net/Uri;)LX/6Xd;

    move-result-object v1

    .line 199304
    const-string v0, "extra_applink_key"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199305
    if-nez v0, :cond_0

    .line 199306
    const-string v0, "applink_data"

    .line 199307
    :cond_0
    iget-object v2, v1, LX/6Xd;->c:Ljava/lang/String;

    .line 199308
    invoke-static {p0, p1, p2, v1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;LX/31z;LX/6Xd;)Landroid/content/Intent;

    move-result-object v3

    .line 199309
    if-nez v3, :cond_1

    .line 199310
    const/4 v0, 0x0

    .line 199311
    :goto_0
    return-object v0

    .line 199312
    :cond_1
    const-string v4, "access_token"

    iget-object v5, v1, LX/6Xd;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199313
    const-string v4, "expires_in"

    iget-object v5, v1, LX/6Xd;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199314
    const-string v4, "application_link_type"

    const-string v5, "native"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199315
    const-string v4, "unit_type"

    const-string v5, "app_launch"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199316
    const-string v4, "ref"

    iget-object v5, v1, LX/6Xd;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199317
    iget-object v4, v1, LX/6Xd;->f:LX/0lF;

    if-eqz v4, :cond_2

    .line 199318
    :try_start_0
    iget-object v1, v1, LX/6Xd;->f:LX/0lF;

    invoke-static {v1}, LX/17d;->a(LX/0lF;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199319
    :cond_2
    :goto_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, LX/47G;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199320
    const-string v0, "extra_direct_installs_enabled"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199321
    :cond_3
    invoke-static {v2, v3}, LX/17d;->a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 199322
    :catch_0
    move-exception v0

    .line 199323
    sget-object v1, LX/17d;->a:Ljava/lang/Class;

    const-string v4, "Error writing applink data as json"

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;LX/31z;LX/6Xd;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 199324
    iget-object v2, p3, LX/6Xd;->c:Ljava/lang/String;

    .line 199325
    iget-object v5, p3, LX/6Xd;->e:Ljava/lang/String;

    .line 199326
    if-eqz p2, :cond_c

    iget-object v0, p2, LX/31z;->appSites:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p2, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 199327
    iget-object v0, p2, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32r;

    .line 199328
    iget-object v1, v0, LX/32r;->packageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 199329
    iget-object v1, v0, LX/32r;->appSite:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 199330
    new-instance v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.application."

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_3

    move-object v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 199331
    new-instance v1, Landroid/content/ComponentName;

    iget-object v4, v0, LX/32r;->packageName:Ljava/lang/String;

    iget-object v7, v0, LX/32r;->appSite:Ljava/lang/String;

    invoke-direct {v1, v4, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 199332
    :goto_2
    const/high16 v1, 0x10000000

    invoke-virtual {v3, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->addFlags(I)Landroid/content/Intent;

    .line 199333
    iget-object v1, v0, LX/32r;->appName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 199334
    const-string v1, "extra_app_name"

    iget-object v4, v0, LX/32r;->appName:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199335
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v4, 0x10000

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 199336
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 199337
    const-string v4, "is_app_link"

    iget-object v7, v0, LX/32r;->isAppLink:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199338
    const-string v4, "android.intent.action.DIAL"

    invoke-virtual {v3}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "android.intent.action.CALL"

    invoke-virtual {v3}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 199339
    :cond_2
    :goto_3
    return-object v3

    .line 199340
    :cond_3
    const-string v1, ""

    goto :goto_1

    .line 199341
    :cond_4
    iget-object v1, v0, LX/32r;->appSiteUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 199342
    iget-object v1, v0, LX/32r;->appSiteUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 199343
    const-string v1, "telprompt"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 199344
    const-string v4, "android.intent.action.DIAL"

    .line 199345
    invoke-static {v3}, LX/17d;->i(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v4

    .line 199346
    :goto_4
    new-instance v4, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    invoke-direct {v4, v3, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v3, v4

    .line 199347
    goto :goto_2

    .line 199348
    :cond_5
    const-string v1, "tel"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 199349
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v4, "android.permission.CALL_PHONE"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v4, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 199350
    if-nez v1, :cond_6

    const-string v1, "android.intent.action.CALL"

    .line 199351
    :goto_5
    invoke-static {v3}, LX/17d;->i(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    move-object v9, v3

    move-object v3, v1

    move-object v1, v9

    .line 199352
    goto :goto_4

    .line 199353
    :cond_6
    const-string v1, "android.intent.action.DIAL"

    goto :goto_5

    .line 199354
    :cond_7
    const-string v4, "android.intent.action.VIEW"

    .line 199355
    if-eqz v5, :cond_17

    .line 199356
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "target_url"

    invoke-virtual {v1, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v4

    goto :goto_4

    .line 199357
    :cond_8
    new-instance v1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v5}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v3, v1

    goto/16 :goto_2

    .line 199358
    :cond_9
    invoke-virtual {v3}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-nez v4, :cond_b

    .line 199359
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 199360
    iget-object v7, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, LX/32r;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 199361
    new-instance v4, Landroid/content/ComponentName;

    iget-object v7, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v7, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 199362
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 199363
    iput-object p1, v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199364
    iget-object v1, v0, LX/32r;->keyHashes:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/32r;->keyHashes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 199365
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v4, v0, LX/32r;->packageName:Ljava/lang/String;

    const/16 v7, 0x40

    invoke-virtual {v1, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v4, v0, LX/32r;->keyHashes:Ljava/util/List;

    invoke-static {v1, v4}, LX/17d;->a(Landroid/content/pm/PackageInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 199366
    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Could not find package for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/32r;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 199367
    :cond_c
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 199368
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199369
    const-string v1, "class_name"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199370
    const/4 v6, 0x0

    .line 199371
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v3, v6

    .line 199372
    :goto_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "key_hash"

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 199373
    if-nez v7, :cond_18

    .line 199374
    move-object v6, v8

    .line 199375
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 199376
    :try_start_1
    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 199377
    if-eqz v7, :cond_d

    .line 199378
    new-instance v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    invoke-direct {v3, v7}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Landroid/content/Intent;)V

    .line 199379
    iput-object p1, v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199380
    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    const/16 v7, 0x40

    invoke-virtual {v4, v0, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    invoke-static {v7, v6}, LX/17d;->a(Landroid/content/pm/PackageInfo;Ljava/util/List;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-nez v7, :cond_2

    .line 199381
    :cond_d
    :goto_7
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_e

    .line 199382
    sget-object v0, LX/17d;->a:Ljava/lang/Class;

    const-string v1, "Native application url did not specify Android key hash."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199383
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199384
    :catch_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "No launch intent found, or could not verify signatures for "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 199385
    :cond_e
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eq v3, v7, :cond_f

    .line 199386
    sget-object v0, LX/17d;->a:Ljava/lang/Class;

    const-string v1, "Native application url specified a class_name, but no package_name. Neither or bothmust be specified in the legacy case."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199387
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199388
    :cond_f
    new-instance v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "com.facebook.application."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_12

    :goto_8
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v2, v5}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 199389
    iput-object p1, v3, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199390
    const/high16 v2, 0x10000000

    invoke-virtual {v3, v2}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->addFlags(I)Landroid/content/Intent;

    .line 199391
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 199392
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 199393
    :cond_10
    const-string v1, "app_name"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199394
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 199395
    const-string v2, "extra_app_name"

    invoke-virtual {v3, v2, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199396
    :cond_11
    const/high16 v1, 0x10000

    invoke-virtual {v4, v3, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 199397
    if-nez v1, :cond_13

    .line 199398
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199399
    :cond_12
    const-string v2, ""

    goto :goto_8

    .line 199400
    :cond_13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 199401
    iget-object v0, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 199402
    if-nez v0, :cond_14

    .line 199403
    sget-object v0, LX/17d;->a:Ljava/lang/Class;

    const-string v1, "Native application url referenced ResolveInfo that has null activityInfo."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199404
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199405
    :cond_14
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    .line 199406
    if-nez v0, :cond_15

    .line 199407
    sget-object v0, LX/17d;->a:Ljava/lang/Class;

    const-string v1, "Native application url referenced ActivityInfo that has null packageName."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199408
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199409
    :cond_15
    const/4 v1, 0x0

    .line 199410
    const/16 v2, 0x40

    :try_start_2
    invoke-virtual {v4, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 199411
    :goto_9
    if-nez v1, :cond_16

    .line 199412
    sget-object v1, LX/17d;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not getPackageInfo for package: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199413
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 199414
    :cond_16
    invoke-static {v1, v6}, LX/17d;->a(Landroid/content/pm/PackageInfo;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 199415
    sget-object v1, LX/17d;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not verify signature for package: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 199416
    const/4 v3, 0x0

    goto/16 :goto_3

    :catch_2
    goto :goto_9

    :cond_17
    move-object v1, v3

    move-object v3, v4

    goto/16 :goto_4

    .line 199417
    :cond_18
    const/16 v9, 0x3d

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 199418
    if-ltz v9, :cond_19

    .line 199419
    invoke-virtual {v7, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 199420
    :cond_19
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199421
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 199211
    if-nez p1, :cond_1

    .line 199212
    :cond_0
    :goto_0
    return-object v0

    .line 199213
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 199214
    new-instance v1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, p1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 199215
    iput-object p2, v1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199216
    const/high16 v3, 0x10000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 199217
    if-eqz v2, :cond_0

    .line 199218
    const-string v0, "application_link_type"

    const-string v2, "web"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199219
    const-string v0, "ref"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199220
    const-string v2, "ref"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199221
    const-string v0, "app_id"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199222
    invoke-static {v0, v1}, LX/17d;->a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-object v0, v1

    .line 199223
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 199568
    if-nez p0, :cond_0

    .line 199569
    :goto_0
    return-object p1

    .line 199570
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 199571
    const-string v2, "app_id"

    invoke-virtual {p1, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 199572
    :catch_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static a(LX/0lF;)Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 199422
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 199423
    invoke-virtual {p0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v5

    .line 199424
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 199425
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 199426
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 199427
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 199428
    invoke-virtual {v0}, LX/0lF;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199429
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 199430
    :cond_0
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 199431
    invoke-static {v0}, LX/17d;->a(LX/0lF;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 199432
    :cond_1
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 199433
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v2

    if-nez v2, :cond_2

    .line 199434
    new-array v0, v3, [Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 199435
    :cond_2
    invoke-virtual {v0, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 199436
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v2

    new-array v6, v2, [Ljava/lang/String;

    move v2, v3

    .line 199437
    :goto_1
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 199438
    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v7

    invoke-virtual {v7}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 199439
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 199440
    :cond_3
    invoke-virtual {v4, v1, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 199441
    :cond_4
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v2

    new-array v6, v2, [Landroid/os/Bundle;

    move v2, v3

    .line 199442
    :goto_2
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v7

    if-ge v2, v7, :cond_5

    .line 199443
    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/17d;->a(LX/0lF;)Landroid/os/Bundle;

    move-result-object v7

    aput-object v7, v6, v2

    .line 199444
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 199445
    :cond_5
    invoke-virtual {v4, v1, v6}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto/16 :goto_0

    .line 199446
    :cond_6
    sget-object v2, LX/17d;->a:Ljava/lang/Class;

    const-string v6, "Unsupported value type in bundle for key %s with value %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v3

    const/4 v1, 0x1

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-static {v2, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 199447
    :cond_7
    return-object v4
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199566
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;Ljava/util/Map;)V

    .line 199567
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;Ljava/util/Map;)V
    .locals 12
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199465
    instance-of v0, p1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    if-nez v0, :cond_1

    .line 199466
    instance-of v0, p1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$ChooserActivityIntent;

    if-eqz v0, :cond_0

    .line 199467
    const-string v0, "extra_logging_params"

    .line 199468
    if-nez p3, :cond_18

    .line 199469
    const/4 v1, 0x0

    .line 199470
    :goto_0
    move-object v1, v1

    .line 199471
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 199472
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, p1

    .line 199473
    check-cast v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;

    .line 199474
    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 199475
    const-string v4, "app_id"

    invoke-virtual {v0, v4, v10, v11}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 199476
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "open_application"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 199477
    cmp-long v7, v4, v10

    if-eqz v7, :cond_2

    .line 199478
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199479
    const-string v7, "app_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199480
    :cond_2
    const-string v4, "unit_type"

    invoke-virtual {v0, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199481
    if-eqz v4, :cond_3

    .line 199482
    const-string v5, "unit_type"

    invoke-virtual {v6, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199483
    :cond_3
    const-string v4, "application_link_type"

    invoke-virtual {v0, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199484
    if-eqz v4, :cond_4

    .line 199485
    const-string v5, "application_link_type"

    invoke-virtual {v6, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199486
    :cond_4
    const-string v4, "is_app_link"

    invoke-virtual {v0, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199487
    if-eqz v4, :cond_5

    .line 199488
    const-string v5, "is_app_link"

    invoke-virtual {v6, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199489
    :cond_5
    const-string v4, "ref"

    invoke-virtual {v0, v4}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199490
    if-eqz v4, :cond_6

    .line 199491
    iput-object v4, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 199492
    :cond_6
    const-string v4, "extra_direct_installs_enabled"

    invoke-virtual {v0, v4, v9}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 199493
    const-string v4, "direct_install_intent"

    invoke-virtual {v6, v4, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199494
    :cond_7
    if-eqz p3, :cond_c

    .line 199495
    const-string v4, "sponsored"

    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 199496
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 199497
    invoke-virtual {v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199498
    :goto_2
    const-string v4, "cta_click"

    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 199499
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 199500
    const-string v4, "cta_click"

    invoke-virtual {v6, v4, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199501
    :cond_8
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_9
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 199502
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 199503
    const-string v8, "temporary_parameters"

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 199504
    instance-of v8, v5, LX/0lF;

    if-eqz v8, :cond_b

    .line 199505
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    check-cast v5, LX/0lF;

    invoke-virtual {v6, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_3

    .line 199506
    :cond_a
    invoke-virtual {v6, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 199507
    :cond_b
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_3

    .line 199508
    :cond_c
    move-object v2, v6

    .line 199509
    if-eqz v2, :cond_0

    .line 199510
    const-string v3, "target"

    const-string v1, "access_token"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, "app"

    :goto_4
    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199511
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 199512
    if-eqz v1, :cond_d

    .line 199513
    const-string v3, "dest_uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199514
    :cond_d
    iget-object v0, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$FbrpcIntent;->a:Landroid/net/Uri;

    .line 199515
    if-eqz v0, :cond_14

    .line 199516
    const/4 v8, -0x1

    .line 199517
    new-instance v5, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 199518
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v6

    .line 199519
    if-eqz v6, :cond_13

    .line 199520
    const/4 v1, 0x0

    .line 199521
    :goto_5
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_13

    .line 199522
    const/16 v3, 0x26

    invoke-virtual {v6, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 199523
    if-ne v3, v8, :cond_e

    .line 199524
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    .line 199525
    :cond_e
    const/16 v4, 0x3d

    invoke-virtual {v6, v4, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 199526
    if-eq v4, v8, :cond_f

    if-le v4, v3, :cond_10

    :cond_f
    move v4, v3

    .line 199527
    :cond_10
    add-int/lit8 v7, v4, 0x1

    .line 199528
    if-ge v1, v4, :cond_12

    .line 199529
    invoke-virtual {v6, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 199530
    const-string v1, ""

    .line 199531
    if-ge v7, v3, :cond_11

    .line 199532
    invoke-virtual {v6, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199533
    :cond_11
    invoke-virtual {v5, v4, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 199534
    :cond_12
    add-int/lit8 v1, v3, 0x1

    .line 199535
    goto :goto_5

    .line 199536
    :cond_13
    move-object v1, v5

    .line 199537
    const-string v3, "fbrpc"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199538
    const-string v1, "has_app_link"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199539
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 199540
    const-string v1, "has_app_link"

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 199541
    :cond_14
    iget-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    move-object v0, v0

    .line 199542
    if-nez v0, :cond_1b

    .line 199543
    const/4 v0, 0x0

    .line 199544
    :cond_15
    :goto_6
    move-object v0, v0

    .line 199545
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 199546
    iget-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    move-object v0, v0

    .line 199547
    if-nez v0, :cond_16

    .line 199548
    invoke-static {p0}, LX/0gh;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 199549
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 199550
    :cond_16
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p0, v2}, LX/0gh;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_1

    .line 199551
    :cond_17
    const-string v1, "market"

    goto/16 :goto_4

    .line 199552
    :cond_18
    const-string v1, "sponsored"

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 199553
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 199554
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 199555
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 199556
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_7
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 199557
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 199558
    instance-of p2, v2, LX/0lF;

    if-eqz p2, :cond_19

    .line 199559
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 199560
    :cond_19
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 199561
    :cond_1a
    new-instance v1, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;

    invoke-direct {v1, v3, v4, v5}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;-><init>(ZLandroid/os/Bundle;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 199562
    :cond_1b
    const-string v1, "nf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    const-string v1, "feed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 199563
    :cond_1c
    const-string v1, "native_newsfeed"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 199564
    :cond_1d
    const-string v1, "bookmark"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 199565
    const-string v1, "sidebar_menu"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6
.end method

.method private static a(Landroid/content/pm/PackageInfo;Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageInfo;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 199451
    iget-object v3, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 199452
    if-nez v3, :cond_1

    .line 199453
    :cond_0
    :goto_0
    return v1

    .line 199454
    :cond_1
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 199455
    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v0, v3, v2

    .line 199456
    invoke-virtual {v4}, Ljava/security/MessageDigest;->reset()V

    .line 199457
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 199458
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v6, 0x3

    invoke-static {v0, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    move v0, v1

    .line 199459
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v0, v7, :cond_3

    .line 199460
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 199461
    const/4 v1, 0x1

    goto :goto_0

    .line 199462
    :catch_0
    const-string v0, "Facebook-IntentUriHandler"

    const-string v2, "Failed to instantiate SHA-1 algorithm."

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 199463
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 199464
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 199450
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 199448
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199449
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 199237
    if-eqz p0, :cond_0

    const-string v0, "fbrpc://facebook/nativethirdparty"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 199129
    invoke-static {p0, p1}, LX/17d;->e(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 199130
    if-eqz v0, :cond_0

    .line 199131
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2}, LX/17d;->c(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 199070
    if-eqz p0, :cond_0

    .line 199071
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199072
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)LX/32n;
    .locals 1

    .prologue
    .line 199073
    if-nez p0, :cond_0

    .line 199074
    const/4 v0, 0x0

    .line 199075
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "invalidation_behavior"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/32n;->fromString(Ljava/lang/String;)LX/32n;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199076
    if-eqz p2, :cond_1

    iget-object v0, p2, LX/31z;->appSites:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 199077
    iget-object v0, p2, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32r;

    .line 199078
    iget-object v0, v0, LX/32r;->fallbackUrl:Ljava/lang/String;

    invoke-static {v0}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199079
    invoke-static {p0, v0, p1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199080
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 199081
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;LX/31z;LX/47G;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/17d;Landroid/net/Uri;)LX/31z;
    .locals 4

    .prologue
    .line 199082
    const-string v0, "appsite_data"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199083
    const/4 v1, 0x0

    .line 199084
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 199085
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    const-class v3, LX/31z;

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31z;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199086
    :goto_0
    invoke-direct {p0, v0}, LX/17d;->a(LX/31z;)LX/31z;

    move-result-object v0

    return-object v0

    .line 199087
    :catch_0
    move-exception v0

    .line 199088
    sget-object v2, LX/17d;->a:Ljava/lang/Class;

    const-string v3, "Error parsing appsites"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 199089
    const-string v0, "target_url"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199090
    invoke-static {p0, v0, p1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199091
    :goto_0
    return-object v0

    .line 199092
    :cond_0
    const/4 v0, 0x0

    .line 199093
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fallback_url"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 199094
    add-int/lit8 v0, v0, 0x1

    .line 199095
    if-nez v1, :cond_2

    .line 199096
    const/4 v0, 0x0

    goto :goto_0

    .line 199097
    :cond_2
    invoke-static {p0, v1, p1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v0, v1

    .line 199098
    goto :goto_0
.end method

.method private static f(Landroid/net/Uri;)LX/6Xd;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 199099
    const-string v0, "extra_applink_key"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199100
    if-nez v0, :cond_0

    .line 199101
    const-string v0, "applink_data"

    .line 199102
    :cond_0
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199103
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 199104
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0lC;->a([B)LX/0lF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 199105
    :goto_0
    new-instance v3, LX/6Xd;

    invoke-direct {v3}, LX/6Xd;-><init>()V

    .line 199106
    iput-object v0, v3, LX/6Xd;->f:LX/0lF;

    .line 199107
    if-eqz v0, :cond_4

    const-string v2, "target_url"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 199108
    const-string v2, "target_url"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, LX/6Xd;->e:Ljava/lang/String;

    .line 199109
    :goto_1
    const-string v2, "referer_data_key"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199110
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 199111
    const-string v2, "referer_data"

    .line 199112
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 199113
    :cond_2
    if-eqz v1, :cond_5

    const-string v0, "fb_app_id"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 199114
    const-string v0, "fb_app_id"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->c:Ljava/lang/String;

    .line 199115
    :goto_2
    if-eqz v1, :cond_6

    const-string v0, "fb_access_token"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 199116
    const-string v0, "fb_access_token"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->a:Ljava/lang/String;

    .line 199117
    :goto_3
    if-eqz v1, :cond_7

    const-string v0, "fb_expires_in"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 199118
    const-string v0, "fb_expires_in"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->b:Ljava/lang/String;

    .line 199119
    :goto_4
    if-eqz v1, :cond_8

    const-string v0, "fb_ref"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 199120
    const-string v0, "fb_ref"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->d:Ljava/lang/String;

    .line 199121
    :goto_5
    return-object v3

    .line 199122
    :catch_0
    move-exception v0

    .line 199123
    sget-object v2, LX/17d;->a:Ljava/lang/Class;

    const-string v3, "Error parsing applink"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    .line 199124
    :cond_4
    const-string v2, "target_url"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, LX/6Xd;->e:Ljava/lang/String;

    goto :goto_1

    .line 199125
    :cond_5
    const-string v0, "app_id"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->c:Ljava/lang/String;

    goto :goto_2

    .line 199126
    :cond_6
    const-string v0, "access_token"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->a:Ljava/lang/String;

    goto :goto_3

    .line 199127
    :cond_7
    const-string v0, "expires_in"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->b:Ljava/lang/String;

    goto :goto_4

    .line 199128
    :cond_8
    const-string v0, "ref"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/6Xd;->d:Ljava/lang/String;

    goto :goto_5
.end method

.method private static f(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 199132
    new-instance v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$ChooserActivityIntent;

    const-class v1, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;

    invoke-direct {v0, p0, v1}, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$ChooserActivityIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199133
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 199134
    return-object v0
.end method

.method private static i(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 199135
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199136
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tel:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199137
    :goto_0
    return-object v0

    .line 199138
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 199139
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tel:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 199140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 199141
    iget-object v0, p2, LX/47I;->a:Ljava/lang/String;

    move-object v0, v0

    .line 199142
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 199143
    invoke-static {v4}, LX/17d;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199144
    const/4 v0, 0x0

    .line 199145
    :cond_0
    :goto_0
    return-object v0

    .line 199146
    :cond_1
    const-string v0, "direct_deeplink"

    invoke-static {v4, v0}, LX/17d;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v5

    .line 199147
    const-string v0, "direct_install"

    invoke-static {v4, v0}, LX/17d;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v6

    .line 199148
    const-string v0, "tap_behavior"

    invoke-virtual {v4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6Xh;->fromString(Ljava/lang/String;)LX/6Xh;

    move-result-object v7

    .line 199149
    invoke-static {p0, v4}, LX/17d;->e(LX/17d;Landroid/net/Uri;)LX/31z;

    move-result-object v8

    .line 199150
    if-nez v5, :cond_2

    invoke-virtual {v7}, LX/6Xh;->shouldAllowOpenInNativeAppIfInstalled()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v0, v2

    .line 199151
    :goto_1
    iget-object v3, p2, LX/47I;->d:LX/47G;

    move-object v3, v3

    .line 199152
    invoke-static {p1, v4, v8, v3}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;LX/31z;LX/47G;)Landroid/content/Intent;

    move-result-object v3

    .line 199153
    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    .line 199154
    if-nez v5, :cond_3

    invoke-virtual {v7}, LX/6Xh;->shouldShowInterstitialForOpen()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move-object v0, v3

    .line 199155
    goto :goto_0

    :cond_4
    move v0, v1

    .line 199156
    goto :goto_1

    .line 199157
    :cond_5
    invoke-static {p1, v4}, LX/17d;->f(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 199158
    :cond_6
    if-nez v6, :cond_7

    invoke-virtual {v7}, LX/6Xh;->shouldGoToMarketIfAppNotInstalled()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    move v1, v2

    .line 199159
    :cond_8
    invoke-static {p1, v4, v8}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v0

    .line 199160
    if-eqz v0, :cond_9

    if-eqz v1, :cond_9

    .line 199161
    if-nez v6, :cond_0

    invoke-virtual {v7}, LX/6Xh;->shouldShowInterstitialForInstall()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199162
    invoke-static {p1, v4}, LX/17d;->f(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 199163
    :cond_9
    invoke-static {p1, v4, v8}, LX/17d;->b(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v1

    .line 199164
    if-eqz v1, :cond_a

    .line 199165
    const-string v5, "extra_fallback_intent"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199166
    const-string v2, "embed_app_intent"

    invoke-static {v4, v2}, LX/17d;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 199167
    if-eqz v3, :cond_b

    .line 199168
    const-string v0, "extra_app_intent"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_a
    :goto_2
    move-object v0, v1

    .line 199169
    goto :goto_0

    .line 199170
    :cond_b
    if-eqz v0, :cond_a

    .line 199171
    const-string v2, "extra_install_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 199172
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v0

    .line 199173
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/47H;->a:Ljava/lang/String;

    .line 199174
    move-object v0, v0

    .line 199175
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 199176
    invoke-virtual {p0, p1, v0}, LX/17d;->a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/content/pm/PackageManager;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 199177
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 199178
    if-eqz p4, :cond_0

    invoke-virtual {p5, v1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 199179
    :goto_0
    if-eqz v0, :cond_2

    .line 199180
    invoke-virtual {p0, p1, p4}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 199181
    :goto_1
    return-object v0

    .line 199182
    :cond_0
    if-eqz p3, :cond_1

    .line 199183
    const/4 v0, 0x1

    move-object p4, p3

    goto :goto_0

    :cond_1
    move-object p4, p2

    .line 199184
    goto :goto_0

    .line 199185
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Landroid/net/Uri;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199186
    if-nez p1, :cond_0

    .line 199187
    const/4 v0, 0x0

    .line 199188
    :goto_0
    return-object v0

    .line 199189
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 199190
    invoke-static {p0, p1}, LX/17d;->e(LX/17d;Landroid/net/Uri;)LX/31z;

    move-result-object v0

    .line 199191
    if-eqz v0, :cond_2

    iget-object v2, v0, LX/31z;->appSites:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 199192
    iget-object v0, v0, LX/31z;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32r;

    .line 199193
    iget-object v3, v0, LX/32r;->packageName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 199194
    iget-object v0, v0, LX/32r;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199195
    :cond_2
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199196
    const-string v0, "market_uri"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199197
    if-eqz v0, :cond_3

    .line 199198
    const-string v2, "id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 199200
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 199201
    :cond_3
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 199202
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199203
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 199204
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v1

    .line 199205
    goto :goto_0
.end method

.method public final d(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 199206
    const-string v0, "fbrpc"

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "facebook"

    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "/nativethirdparty"

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199207
    :cond_0
    const/4 v0, 0x0

    .line 199208
    :goto_0
    return-object v0

    .line 199209
    :cond_1
    invoke-static {p0, p2}, LX/17d;->e(LX/17d;Landroid/net/Uri;)LX/31z;

    move-result-object v0

    .line 199210
    invoke-static {p1, p2, v0}, LX/17d;->d(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
