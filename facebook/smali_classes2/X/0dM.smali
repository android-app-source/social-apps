.class public abstract LX/0dM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0dN;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:LX/0Tn;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Tn;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90281
    const-class v0, LX/0dM;

    sput-object v0, LX/0dM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Tn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;",
            "LX/0Tn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90279
    sget-object v0, LX/0dR;->WHOLE:LX/0dR;

    invoke-direct {p0, p1, p2, v0}, LX/0dM;-><init>(LX/0Ot;LX/0Tn;LX/0dR;)V

    .line 90280
    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Tn;LX/0dR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;",
            "LX/0Tn;",
            "LX/0dR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90255
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/0dM;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 90256
    sget-object v0, LX/0dS;->a:[I

    invoke-virtual {p3}, LX/0dR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 90257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 90258
    :pswitch_0
    iput-object p1, p0, LX/0dM;->b:LX/0Ot;

    .line 90259
    iput-object p2, p0, LX/0dM;->c:LX/0Tn;

    .line 90260
    :goto_0
    return-void

    .line 90261
    :pswitch_1
    iput-object p1, p0, LX/0dM;->b:LX/0Ot;

    .line 90262
    iput-object p2, p0, LX/0dM;->e:LX/0Tn;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(LX/0Ot;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90275
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/0dM;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 90276
    iput-object p1, p0, LX/0dM;->b:LX/0Ot;

    .line 90277
    invoke-static {p2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0dM;->d:Ljava/util/Set;

    .line 90278
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2

    .prologue
    .line 90265
    iget-object v0, p0, LX/0dM;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90266
    :goto_0
    return-void

    .line 90267
    :cond_0
    iget-object v0, p0, LX/0dM;->d:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 90268
    iget-object v0, p0, LX/0dM;->d:Ljava/util/Set;

    invoke-interface {p1, v0, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    goto :goto_0

    .line 90269
    :cond_1
    iget-object v0, p0, LX/0dM;->e:LX/0Tn;

    if-eqz v0, :cond_2

    .line 90270
    iget-object v0, p0, LX/0dM;->e:LX/0Tn;

    invoke-interface {p1, v0, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    goto :goto_0

    .line 90271
    :cond_2
    iget-object v0, p0, LX/0dM;->c:LX/0Tn;

    if-eqz v0, :cond_3

    .line 90272
    iget-object v0, p0, LX/0dM;->c:LX/0Tn;

    invoke-interface {p1, v0, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c(LX/0Tn;LX/0dN;)V

    goto :goto_0

    .line 90273
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 1

    .prologue
    .line 90263
    iget-object v0, p0, LX/0dM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/0dM;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;Ljava/lang/Object;)V

    .line 90264
    return-void
.end method

.method public abstract a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Tn;",
            "TT;)V"
        }
    .end annotation
.end method
