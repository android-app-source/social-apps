.class public LX/0VD;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67514
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 67515
    return-void
.end method

.method public static a()Ljava/util/concurrent/ExecutorService;
    .locals 10
    .annotation runtime Lcom/facebook/common/errorreporting/ErrorReportingSingleThreadExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 67516
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, LX/0TO;

    const-string v0, "ErrorReportingThread-"

    sget-object v9, LX/0TP;->FOREGROUND:LX/0TP;

    invoke-direct {v8, v0, v9}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v1
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 67517
    return-void
.end method
