.class public LX/0eI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:LX/0eL;

.field private static volatile i:LX/0eI;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0dN;

.field private final d:LX/00H;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0eM;

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91649
    sget-object v0, LX/0eJ;->c:LX/0Tn;

    sget-object v1, LX/0eJ;->b:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0eI;->a:Ljava/util/Set;

    .line 91650
    new-instance v0, LX/0eK;

    invoke-direct {v0}, LX/0eK;-><init>()V

    sput-object v0, LX/0eI;->h:LX/0eL;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/00H;LX/0Or;LX/0eM;)V
    .locals 3
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/diagnostics/IsDebugLogsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/00H;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0eM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91652
    iput-object p1, p0, LX/0eI;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 91653
    iput-object p2, p0, LX/0eI;->d:LX/00H;

    .line 91654
    iput-object p3, p0, LX/0eI;->e:LX/0Or;

    .line 91655
    iput-object p4, p0, LX/0eI;->f:LX/0eM;

    .line 91656
    new-instance v0, LX/0eO;

    invoke-direct {v0, p0}, LX/0eO;-><init>(LX/0eI;)V

    iput-object v0, p0, LX/0eI;->c:LX/0dN;

    .line 91657
    iget-object v0, p0, LX/0eI;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eI;->a:Ljava/util/Set;

    iget-object v2, p0, LX/0eI;->c:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 91658
    return-void
.end method

.method public static a(LX/0QB;)LX/0eI;
    .locals 7

    .prologue
    .line 91659
    sget-object v0, LX/0eI;->i:LX/0eI;

    if-nez v0, :cond_1

    .line 91660
    const-class v1, LX/0eI;

    monitor-enter v1

    .line 91661
    :try_start_0
    sget-object v0, LX/0eI;->i:LX/0eI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91662
    if-eqz v2, :cond_0

    .line 91663
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91664
    new-instance v6, LX/0eI;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v4, LX/00H;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/00H;

    const/16 v5, 0x1469

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0eM;->a(LX/0QB;)LX/0eM;

    move-result-object v5

    check-cast v5, LX/0eM;

    invoke-direct {v6, v3, v4, p0, v5}, LX/0eI;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/00H;LX/0Or;LX/0eM;)V

    .line 91665
    move-object v0, v6

    .line 91666
    sput-object v0, LX/0eI;->i:LX/0eI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91667
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91668
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91669
    :cond_1
    sget-object v0, LX/0eI;->i:LX/0eI;

    return-object v0

    .line 91670
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91671
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, -0x1

    .line 91672
    iget-object v0, p0, LX/0eI;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0eJ;->c:LX/0Tn;

    const-string v3, "-1"

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91673
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 91674
    :goto_0
    if-eq v0, v1, :cond_1

    .line 91675
    invoke-static {v0}, LX/01m;->a(I)V

    .line 91676
    :goto_1
    invoke-static {v0}, LX/01m;->a(I)V

    .line 91677
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/0eI;->g:Z

    if-nez v0, :cond_3

    .line 91678
    iget-object v0, p0, LX/0eI;->f:LX/0eM;

    sget-object v1, LX/0eI;->h:LX/0eL;

    invoke-virtual {v0, v1}, LX/0eM;->a(LX/0eL;)V

    .line 91679
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0eI;->g:Z

    .line 91680
    :cond_0
    :goto_2
    return-void

    .line 91681
    :catch_0
    move v0, v1

    goto :goto_0

    .line 91682
    :cond_1
    sget-object v0, LX/0eP;->a:[I

    iget-object v1, p0, LX/0eI;->d:LX/00H;

    .line 91683
    iget-object v2, v1, LX/00H;->i:LX/01S;

    move-object v1, v2

    .line 91684
    invoke-virtual {v1}, LX/01S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 91685
    sget-boolean v0, LX/007;->l:Z

    move v0, v0

    .line 91686
    if-nez v0, :cond_2

    .line 91687
    iget-object v0, p0, LX/0eI;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91688
    const/4 v0, 0x4

    goto :goto_1

    .line 91689
    :pswitch_0
    const/4 v0, 0x3

    .line 91690
    goto :goto_1

    .line 91691
    :cond_2
    const/4 v0, 0x5

    goto :goto_1

    .line 91692
    :cond_3
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0eI;->g:Z

    if-eqz v0, :cond_0

    .line 91693
    iget-object v0, p0, LX/0eI;->f:LX/0eM;

    sget-object v1, LX/0eI;->h:LX/0eL;

    invoke-virtual {v0, v1}, LX/0eM;->b(LX/0eL;)V

    .line 91694
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0eI;->g:Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 91695
    invoke-virtual {p0}, LX/0eI;->a()V

    .line 91696
    return-void
.end method
