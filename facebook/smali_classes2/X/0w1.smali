.class public LX/0w1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0w2;


# instance fields
.field public a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "LX/3sU;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/reflect/Method;

.field private c:Ljava/lang/reflect/Method;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 158533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158534
    const/4 v0, 0x0

    iput-object v0, p0, LX/0w1;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 158527
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "dispatchStartTemporaryDetach"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/0w1;->b:Ljava/lang/reflect/Method;

    .line 158528
    const-class v0, Landroid/view/View;

    const-string v1, "dispatchFinishTemporaryDetach"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/0w1;->c:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158529
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0w1;->d:Z

    .line 158530
    return-void

    .line 158531
    :catch_0
    move-exception v0

    .line 158532
    const-string v1, "ViewCompat"

    const-string v2, "Couldn\'t find method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public A(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158526
    const/4 v0, 0x0

    return v0
.end method

.method public B(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158525
    const/4 v0, 0x0

    return v0
.end method

.method public C(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158524
    return-void
.end method

.method public D(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158523
    const/4 v0, 0x0

    return v0
.end method

.method public E(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 158520
    instance-of v0, p1, LX/1O9;

    if-eqz v0, :cond_0

    .line 158521
    check-cast p1, LX/1O9;

    invoke-interface {p1}, LX/1O9;->stopNestedScroll()V

    .line 158522
    :cond_0
    return-void
.end method

.method public F(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158508
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 158509
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G(Landroid/view/View;)F
    .locals 2

    .prologue
    .line 158518
    invoke-virtual {p0, p1}, LX/0w1;->A(Landroid/view/View;)F

    move-result v0

    invoke-virtual {p0, p1}, LX/0w1;->z(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public a(II)I
    .locals 1

    .prologue
    .line 158517
    or-int v0, p1, p2

    return v0
.end method

.method public a(III)I
    .locals 1

    .prologue
    .line 158516
    invoke-static {p1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158515
    const/4 v0, 0x2

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 158514
    const-wide/16 v0, 0xa

    return-wide v0
.end method

.method public a(Landroid/view/View;LX/3sc;)LX/3sc;
    .locals 0

    .prologue
    .line 158513
    return-object p2
.end method

.method public a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158512
    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 158510
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->invalidate(IIII)V

    .line 158511
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 158545
    return-void
.end method

.method public a(Landroid/view/View;LX/0vn;)V
    .locals 0

    .prologue
    .line 158544
    return-void
.end method

.method public a(Landroid/view/View;LX/1uR;)V
    .locals 0

    .prologue
    .line 158570
    return-void
.end method

.method public a(Landroid/view/View;LX/3sp;)V
    .locals 0

    .prologue
    .line 158569
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 158566
    instance-of p0, p1, LX/3s1;

    if-eqz p0, :cond_0

    .line 158567
    check-cast p1, LX/3s1;

    invoke-interface {p1, p2}, LX/3s1;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 158568
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 158565
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    .prologue
    .line 158564
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 158562
    invoke-virtual {p0}, LX/0w1;->a()J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 158563
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 3

    .prologue
    .line 158571
    invoke-virtual {p0}, LX/0w1;->a()J

    move-result-wide v0

    add-long/2addr v0, p3

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 158572
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 158561
    return-void
.end method

.method public a(Landroid/view/ViewGroup;Z)V
    .locals 0

    .prologue
    .line 158560
    return-void
.end method

.method public a(Landroid/view/View;I)Z
    .locals 4

    .prologue
    .line 158551
    instance-of v0, p1, LX/1OA;

    if-eqz v0, :cond_1

    check-cast p1, LX/1OA;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 158552
    invoke-interface {p1}, LX/1OA;->computeHorizontalScrollOffset()I

    move-result v2

    .line 158553
    invoke-interface {p1}, LX/1OA;->computeHorizontalScrollRange()I

    move-result v3

    invoke-interface {p1}, LX/1OA;->computeHorizontalScrollExtent()I

    move-result p0

    sub-int/2addr v3, p0

    .line 158554
    if-nez v3, :cond_2

    .line 158555
    :cond_0
    :goto_0
    move v0, v0

    .line 158556
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 158557
    :cond_2
    if-gez p2, :cond_3

    .line 158558
    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 158559
    :cond_3
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 158464
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;LX/3sc;)LX/3sc;
    .locals 0

    .prologue
    .line 158550
    return-object p2
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158549
    return-void
.end method

.method public b(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 158547
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->setPadding(IIII)V

    .line 158548
    return-void
.end method

.method public b(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 158546
    return-void
.end method

.method public b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158519
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;I)Z
    .locals 4

    .prologue
    .line 158535
    instance-of v0, p1, LX/1OA;

    if-eqz v0, :cond_1

    check-cast p1, LX/1OA;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 158536
    invoke-interface {p1}, LX/1OA;->computeVerticalScrollOffset()I

    move-result v2

    .line 158537
    invoke-interface {p1}, LX/1OA;->computeVerticalScrollRange()I

    move-result v3

    invoke-interface {p1}, LX/1OA;->computeVerticalScrollExtent()I

    move-result p0

    sub-int/2addr v3, p0

    .line 158538
    if-nez v3, :cond_2

    .line 158539
    :cond_0
    :goto_0
    move v0, v0

    .line 158540
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 158541
    :cond_2
    if-gez p2, :cond_3

    .line 158542
    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 158543
    :cond_3
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158445
    return-void
.end method

.method public c(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 158462
    return-void
.end method

.method public c(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 158459
    instance-of v0, p1, LX/1O9;

    if-eqz v0, :cond_0

    .line 158460
    check-cast p1, LX/1O9;

    invoke-interface {p1, p2}, LX/1O9;->setNestedScrollingEnabled(Z)V

    .line 158461
    :cond_0
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158458
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158456
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 158457
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158455
    return-void
.end method

.method public d(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 158454
    return-void
.end method

.method public e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158453
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158452
    return-void
.end method

.method public e(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 158451
    return-void
.end method

.method public f(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158450
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public f(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158449
    return-void
.end method

.method public f(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 158448
    return-void
.end method

.method public g(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158447
    const/4 v0, 0x0

    return v0
.end method

.method public h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158446
    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 158444
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public j(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 158484
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 158485
    if-eqz v1, :cond_0

    .line 158486
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 158487
    :cond_0
    return v0
.end method

.method public k(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158507
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public l(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158506
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public m(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158505
    const/4 v0, 0x0

    return v0
.end method

.method public n(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158504
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public o(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158503
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public final p(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 158496
    iget-boolean v0, p0, LX/0w1;->d:Z

    if-nez v0, :cond_0

    .line 158497
    invoke-direct {p0}, LX/0w1;->b()V

    .line 158498
    :cond_0
    iget-object v0, p0, LX/0w1;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    .line 158499
    :try_start_0
    iget-object v0, p0, LX/0w1;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158500
    :goto_0
    return-void

    .line 158501
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    goto :goto_0

    .line 158502
    :catch_0
    goto :goto_0
.end method

.method public final q(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 158489
    iget-boolean v0, p0, LX/0w1;->d:Z

    if-nez v0, :cond_0

    .line 158490
    invoke-direct {p0}, LX/0w1;->b()V

    .line 158491
    :cond_0
    iget-object v0, p0, LX/0w1;->c:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    .line 158492
    :try_start_0
    iget-object v0, p0, LX/0w1;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158493
    :goto_0
    return-void

    .line 158494
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->onFinishTemporaryDetach()V

    goto :goto_0

    .line 158495
    :catch_0
    goto :goto_0
.end method

.method public r(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158488
    const/4 v0, 0x0

    return v0
.end method

.method public s(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158463
    const/4 v0, 0x0

    return v0
.end method

.method public t(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158483
    const/4 v0, 0x0

    return v0
.end method

.method public u(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158476
    sget-object v0, LX/3s4;->a:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 158477
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string p0, "mMinWidth"

    invoke-virtual {v0, p0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 158478
    sput-object v0, LX/3s4;->a:Ljava/lang/reflect/Field;

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 158479
    :cond_0
    :goto_0
    sget-object v0, LX/3s4;->a:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_1

    .line 158480
    :try_start_1
    sget-object v0, LX/3s4;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 158481
    :goto_1
    move v0, v0

    .line 158482
    return v0

    :catch_0
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_1
    goto :goto_0
.end method

.method public v(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158469
    sget-object v0, LX/3s4;->b:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 158470
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string p0, "mMinHeight"

    invoke-virtual {v0, p0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 158471
    sput-object v0, LX/3s4;->b:Ljava/lang/reflect/Field;

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 158472
    :cond_0
    :goto_0
    sget-object v0, LX/3s4;->b:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_1

    .line 158473
    :try_start_1
    sget-object v0, LX/3s4;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 158474
    :goto_1
    move v0, v0

    .line 158475
    return v0

    :catch_0
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_1
    goto :goto_0
.end method

.method public w(Landroid/view/View;)LX/3sU;
    .locals 1

    .prologue
    .line 158468
    new-instance v0, LX/3sU;

    invoke-direct {v0, p1}, LX/3sU;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public x(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158467
    const/4 v0, 0x0

    return v0
.end method

.method public y(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158466
    return-void
.end method

.method public z(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158465
    const/4 v0, 0x0

    return v0
.end method
