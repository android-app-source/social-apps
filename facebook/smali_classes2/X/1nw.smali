.class public final LX/1nw;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1nu;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>.FbFeedFrescoComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1nu;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1nu;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 317801
    iput-object p1, p0, LX/1nw;->b:LX/1nu;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 317802
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "imageUri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1nw;->c:[Ljava/lang/String;

    .line 317803
    iput v3, p0, LX/1nw;->d:I

    .line 317804
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1nw;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1nw;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1nw;LX/1De;IILcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1nu",
            "<TE;>.FbFeedFrescoComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 317793
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 317794
    iput-object p4, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    .line 317795
    iget-object v0, p0, LX/1nw;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 317796
    return-void
.end method


# virtual methods
.method public final a(LX/1Pp;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317797
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    .line 317798
    return-object p0
.end method

.method public final a(LX/1Up;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Up;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317799
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    .line 317800
    return-object p0
.end method

.method public final a(LX/1f9;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1f9;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317814
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    .line 317815
    return-object p0
.end method

.method public final a(LX/33B;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/33B;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317805
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    .line 317806
    return-object p0
.end method

.method public final a(LX/4Ab;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Ab;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317807
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    .line 317808
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317809
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    .line 317810
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/1nw;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317811
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    .line 317812
    iget-object v0, p0, LX/1nw;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 317813
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317788
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 317789
    iget-object v0, p0, LX/1nw;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 317790
    return-object p0
.end method

.method public final a(Z)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317791
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-boolean p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->e:Z

    .line 317792
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 317758
    invoke-super {p0}, LX/1X5;->a()V

    .line 317759
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    .line 317760
    iget-object v0, p0, LX/1nw;->b:LX/1nu;

    iget-object v0, v0, LX/1nu;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 317761
    return-void
.end method

.method public final b(LX/1Up;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Up;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317762
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    .line 317763
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317764
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    .line 317765
    return-object p0
.end method

.method public final c(F)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317766
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->s:F

    .line 317767
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1nu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 317768
    iget-object v1, p0, LX/1nw;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1nw;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1nw;->d:I

    if-ge v1, v2, :cond_2

    .line 317769
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 317770
    :goto_0
    iget v2, p0, LX/1nw;->d:I

    if-ge v0, v2, :cond_1

    .line 317771
    iget-object v2, p0, LX/1nw;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 317772
    iget-object v2, p0, LX/1nw;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317773
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317774
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317775
    :cond_2
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    .line 317776
    invoke-virtual {p0}, LX/1nw;->a()V

    .line 317777
    return-object v0
.end method

.method public final h(I)LX/1nw;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317778
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    .line 317779
    return-object p0
.end method

.method public final i(I)LX/1nw;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317786
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    .line 317787
    return-object p0
.end method

.method public final j(I)LX/1nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317780
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput p1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->o:I

    .line 317781
    return-object p0
.end method

.method public final k(I)LX/1nw;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317782
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    .line 317783
    return-object p0
.end method

.method public final l(I)LX/1nw;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 317784
    iget-object v0, p0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    .line 317785
    return-object p0
.end method
