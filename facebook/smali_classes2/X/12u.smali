.class public final LX/12u;
.super LX/0Rh;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rh",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final transient a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public final transient b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public transient c:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 175803
    invoke-direct {p0}, LX/0Rh;-><init>()V

    .line 175804
    invoke-static {p1, p2}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 175805
    iput-object p1, p0, LX/12u;->a:Ljava/lang/Object;

    .line 175806
    iput-object p2, p0, LX/12u;->b:Ljava/lang/Object;

    .line 175807
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;LX/0Rh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "LX/0Rh",
            "<TV;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 175808
    invoke-direct {p0}, LX/0Rh;-><init>()V

    .line 175809
    iput-object p1, p0, LX/12u;->a:Ljava/lang/Object;

    .line 175810
    iput-object p2, p0, LX/12u;->b:Ljava/lang/Object;

    .line 175811
    iput-object p3, p0, LX/12u;->c:LX/0Rh;

    .line 175812
    return-void
.end method


# virtual methods
.method public final synthetic a_()LX/0Ri;
    .locals 1

    .prologue
    .line 175814
    invoke-virtual {p0}, LX/12u;->e()LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 175813
    iget-object v0, p0, LX/12u;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 175801
    iget-object v0, p0, LX/12u;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final createEntrySet()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 175802
    iget-object v0, p0, LX/12u;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/12u;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final createKeySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 175800
    iget-object v0, p0, LX/12u;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Rh;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 175796
    iget-object v0, p0, LX/12u;->c:LX/0Rh;

    .line 175797
    if-nez v0, :cond_0

    .line 175798
    new-instance v0, LX/12u;

    iget-object v1, p0, LX/12u;->b:Ljava/lang/Object;

    iget-object v2, p0, LX/12u;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p0}, LX/12u;-><init>(Ljava/lang/Object;Ljava/lang/Object;LX/0Rh;)V

    iput-object v0, p0, LX/12u;->c:LX/0Rh;

    .line 175799
    :cond_0
    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 175795
    iget-object v0, p0, LX/12u;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12u;->b:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 175794
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 175793
    const/4 v0, 0x1

    return v0
.end method
