.class public final LX/1uU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Z2;


# direct methods
.method public constructor <init>(LX/1Z2;)V
    .locals 0

    .prologue
    .line 340884
    iput-object p1, p0, LX/1uU;->a:LX/1Z2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x5330c801

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 340885
    iget-object v1, p0, LX/1uU;->a:LX/1Z2;

    iget-object v1, v1, LX/1Z2;->j:LX/0ad;

    sget-short v2, LX/1aO;->Q:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340886
    iget-object v1, p0, LX/1uU;->a:LX/1Z2;

    iget-object v1, v1, LX/1Z2;->b:LX/1RX;

    invoke-virtual {v1, v3}, LX/1RX;->b(Z)V

    .line 340887
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 340888
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 340889
    iget-object v2, p0, LX/1uU;->a:LX/1Z2;

    iget-object v2, v2, LX/1Z2;->a:LX/1RW;

    invoke-virtual {v2}, LX/1RW;->d()V

    .line 340890
    sget-object v2, LX/8Lr;->DRAFT_FEED_ENTRY_POINT:LX/8Lr;

    invoke-static {v1, v2}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;

    move-result-object v2

    .line 340891
    iget-object v3, p0, LX/1uU;->a:LX/1Z2;

    iget-object v3, v3, LX/1Z2;->l:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 340892
    const v1, -0x55abfe51

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
