.class public interface abstract LX/0h5;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract d_(I)Landroid/view/View;
.end method

.method public abstract setButtonSpecs(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setCustomTitleView(Landroid/view/View;)V
.end method

.method public abstract setHasBackButton(Z)V
.end method

.method public abstract setHasFbLogo(Z)V
.end method

.method public abstract setOnBackPressedListener(LX/63J;)V
.end method

.method public abstract setOnToolbarButtonListener(LX/63W;)V
.end method

.method public abstract setShowDividers(Z)V
.end method

.method public abstract setTitle(I)V
.end method

.method public abstract setTitle(Ljava/lang/String;)V
.end method

.method public abstract setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
.end method
