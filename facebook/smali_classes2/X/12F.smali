.class public abstract LX/12F;
.super LX/12G;
.source ""


# static fields
.field public static final g:[I


# instance fields
.field public final h:LX/12A;

.field public i:[I

.field public j:I

.field public k:LX/4pa;

.field public l:LX/0lc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174975
    sget-object v0, LX/12H;->f:[I

    move-object v0, v0

    .line 174976
    sput-object v0, LX/12F;->g:[I

    return-void
.end method

.method public constructor <init>(LX/12A;ILX/0lD;)V
    .locals 1

    .prologue
    .line 174968
    invoke-direct {p0, p2, p3}, LX/12G;-><init>(ILX/0lD;)V

    .line 174969
    sget-object v0, LX/12F;->g:[I

    iput-object v0, p0, LX/12F;->i:[I

    .line 174970
    sget-object v0, LX/0lY;->a:LX/0lb;

    iput-object v0, p0, LX/12F;->l:LX/0lc;

    .line 174971
    iput-object p1, p0, LX/12F;->h:LX/12A;

    .line 174972
    sget-object v0, LX/0ls;->ESCAPE_NON_ASCII:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174973
    const/16 v0, 0x7f

    invoke-virtual {p0, v0}, LX/0nX;->a(I)LX/0nX;

    .line 174974
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/0nX;
    .locals 0

    .prologue
    .line 174966
    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput p1, p0, LX/12F;->j:I

    .line 174967
    return-object p0
.end method

.method public final a(LX/0lc;)LX/0nX;
    .locals 0

    .prologue
    .line 174977
    iput-object p1, p0, LX/12F;->l:LX/0lc;

    .line 174978
    return-object p0
.end method

.method public final a(LX/4pa;)LX/0nX;
    .locals 1

    .prologue
    .line 174961
    iput-object p1, p0, LX/12F;->k:LX/4pa;

    .line 174962
    if-nez p1, :cond_0

    .line 174963
    sget-object v0, LX/12F;->g:[I

    iput-object v0, p0, LX/12F;->i:[I

    .line 174964
    :goto_0
    return-object p0

    .line 174965
    :cond_0
    invoke-virtual {p1}, LX/4pa;->a()[I

    move-result-object v0

    iput-object v0, p0, LX/12F;->i:[I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 174958
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 174959
    invoke-virtual {p0, p2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 174960
    return-void
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 174957
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/4pl;->a(Ljava/lang/Class;)LX/0ne;

    move-result-object v0

    return-object v0
.end method
