.class public LX/1NI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/Executor;

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final f:LX/03V;

.field private final g:LX/0tX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0zO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/0t2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:I

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>(LX/0tX;LX/03V;LX/0TF;Ljava/util/concurrent/Executor;Ljava/lang/String;LX/0zO;LX/0t2;)V
    .locals 6
    .param p1    # LX/0tX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0zO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0t2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/String;",
            "LX/0zO",
            "<TT;>;",
            "LX/0t2;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 237194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237195
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 237196
    iput-object v3, p0, LX/1NI;->c:Ljava/util/Set;

    .line 237197
    iput-object v2, p0, LX/1NI;->e:Ljava/lang/ref/WeakReference;

    .line 237198
    iput-boolean v0, p0, LX/1NI;->k:Z

    .line 237199
    iput-boolean v0, p0, LX/1NI;->l:Z

    .line 237200
    if-nez p7, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    if-eqz p7, :cond_2

    if-eqz p6, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 237201
    if-eqz p6, :cond_3

    .line 237202
    iget v0, p6, LX/0zO;->v:I

    move v0, v0

    .line 237203
    :goto_0
    iput v0, p0, LX/1NI;->j:I

    .line 237204
    iput-object p3, p0, LX/1NI;->a:LX/0TF;

    .line 237205
    iput-object p1, p0, LX/1NI;->g:LX/0tX;

    .line 237206
    iput-object p4, p0, LX/1NI;->b:Ljava/util/concurrent/Executor;

    .line 237207
    iput-object p5, p0, LX/1NI;->d:Ljava/lang/String;

    .line 237208
    iput-object p2, p0, LX/1NI;->f:LX/03V;

    .line 237209
    if-nez p6, :cond_4

    move-object v0, v2

    :goto_1
    iput-object v0, p0, LX/1NI;->h:LX/0zO;

    .line 237210
    iput-object p7, p0, LX/1NI;->i:LX/0t2;

    .line 237211
    return-void

    .line 237212
    :cond_3
    const/4 v0, -0x1

    goto :goto_0

    .line 237213
    :cond_4
    new-instance v0, LX/0zO;

    invoke-direct {v0, p6}, LX/0zO;-><init>(LX/0zO;)V

    sget-object v2, LX/0zS;->b:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    iget-wide v2, p6, LX/0zO;->c:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/32 v4, 0x93a80

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 237214
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 237215
    move-object v0, v0

    .line 237216
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0zO;->k:Z

    .line 237217
    move-object v0, v0

    .line 237218
    iput-object p0, v0, LX/0zO;->j:LX/1NI;

    .line 237219
    move-object v0, v0

    .line 237220
    goto :goto_1
.end method

.method public static g(LX/1NI;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 237235
    const/4 v1, 0x0

    .line 237236
    monitor-enter p0

    .line 237237
    :try_start_0
    iget-boolean v2, p0, LX/1NI;->k:Z

    if-eqz v2, :cond_1

    .line 237238
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/1NI;->l:Z

    .line 237239
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237240
    if-eqz v0, :cond_0

    .line 237241
    invoke-virtual {p0}, LX/1NI;->b()V

    .line 237242
    :goto_1
    return-void

    .line 237243
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 237244
    :cond_0
    :try_start_2
    invoke-virtual {p0}, LX/1NI;->c()Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 237245
    new-instance v1, LX/34M;

    invoke-direct {v1, p0}, LX/34M;-><init>(LX/1NI;)V

    iget-object v2, p0, LX/1NI;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 237246
    :catch_0
    move-exception v0

    .line 237247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Exception pushing result"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1NI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "LX/1NI",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 237248
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1NI;->c:Ljava/util/Set;

    .line 237249
    monitor-enter p0

    .line 237250
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1NI;->k:Z

    .line 237251
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1NI;->l:Z

    .line 237252
    monitor-exit p0

    .line 237253
    return-object p0

    .line 237254
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 237233
    return-void
.end method

.method public c()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 237234
    iget-object v0, p0, LX/1NI;->g:LX/0tX;

    iget-object v1, p0, LX/1NI;->h:LX/0zO;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 237225
    monitor-enter p0

    .line 237226
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1NI;->k:Z

    .line 237227
    iget-boolean v0, p0, LX/1NI;->l:Z

    .line 237228
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237229
    if-eqz v0, :cond_0

    .line 237230
    invoke-static {p0}, LX/1NI;->g(LX/1NI;)V

    .line 237231
    :cond_0
    return-void

    .line 237232
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 237222
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/1NI;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237223
    monitor-exit p0

    return-void

    .line 237224
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()I
    .locals 1
    .annotation build Lcom/facebook/graphql/executor/iface/GraphQLObservablePusher$SubscriptionStore;
    .end annotation

    .prologue
    .line 237221
    const/4 v0, 0x2

    return v0
.end method
