.class public LX/1Kx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AjG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Ky;

.field public c:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233404
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 233405
    iput-object v0, p0, LX/1Kx;->a:LX/0Ot;

    .line 233406
    new-instance v0, LX/1Ky;

    invoke-direct {v0, p0}, LX/1Ky;-><init>(LX/1Kx;)V

    iput-object v0, p0, LX/1Kx;->b:LX/1Ky;

    .line 233407
    return-void
.end method

.method public static a(LX/0QB;)LX/1Kx;
    .locals 4

    .prologue
    .line 233410
    const-class v1, LX/1Kx;

    monitor-enter v1

    .line 233411
    :try_start_0
    sget-object v0, LX/1Kx;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 233412
    sput-object v2, LX/1Kx;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 233413
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233414
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 233415
    new-instance v3, LX/1Kx;

    invoke-direct {v3}, LX/1Kx;-><init>()V

    .line 233416
    const/16 p0, 0x1c84

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 233417
    iput-object p0, v3, LX/1Kx;->a:LX/0Ot;

    .line 233418
    move-object v0, v3

    .line 233419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 233420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Kx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 233422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 233408
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Kx;->c:LX/0TF;

    .line 233409
    return-void
.end method
