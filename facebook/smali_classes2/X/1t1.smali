.class public LX/1t1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05F;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/credentials/UserTokenCredentials;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/credentials/UserTokenCredentials;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 335734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335735
    iput-object p1, p0, LX/1t1;->a:LX/0Or;

    .line 335736
    iput-object p2, p0, LX/1t1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 335737
    return-void
.end method

.method public static a(LX/0QB;)LX/1t1;
    .locals 3

    .prologue
    .line 335731
    new-instance v1, LX/1t1;

    const/16 v0, 0x178

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v2, v0}, LX/1t1;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 335732
    move-object v0, v1

    .line 335733
    return-object v0
.end method


# virtual methods
.method public final a()LX/06k;
    .locals 2

    .prologue
    .line 335716
    iget-object v0, p0, LX/1t1;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/UserTokenCredentials;

    .line 335717
    if-nez v0, :cond_0

    .line 335718
    sget-object v0, LX/06k;->a:LX/06k;

    .line 335719
    :goto_0
    return-object v0

    .line 335720
    :cond_0
    iget-object v1, v0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    move-object v1, v1

    .line 335721
    iget-object p0, v0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    move-object v0, p0

    .line 335722
    invoke-static {v1, v0}, LX/06k;->a(Ljava/lang/String;Ljava/lang/String;)LX/06k;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 335729
    iget-object v0, p0, LX/1t1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1p8;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 335730
    return-void
.end method

.method public final a(LX/06k;)Z
    .locals 1

    .prologue
    .line 335728
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335727
    const-string v0, ""

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 335726
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 335725
    iget-object v0, p0, LX/1t1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1p8;->a:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 335723
    iget-object v0, p0, LX/1t1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1p8;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 335724
    return-void
.end method
