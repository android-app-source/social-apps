.class public final enum LX/15e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/15e;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/15e;

.field public static final enum APACHE:LX/15e;

.field public static final enum LIGER:LX/15e;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180946
    new-instance v0, LX/15e;

    const-string v1, "APACHE"

    invoke-direct {v0, v1, v2}, LX/15e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/15e;->APACHE:LX/15e;

    .line 180947
    new-instance v0, LX/15e;

    const-string v1, "LIGER"

    invoke-direct {v0, v1, v3}, LX/15e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/15e;->LIGER:LX/15e;

    .line 180948
    const/4 v0, 0x2

    new-array v0, v0, [LX/15e;

    sget-object v1, LX/15e;->APACHE:LX/15e;

    aput-object v1, v0, v2

    sget-object v1, LX/15e;->LIGER:LX/15e;

    aput-object v1, v0, v3

    sput-object v0, LX/15e;->$VALUES:[LX/15e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 180949
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/15e;
    .locals 1

    .prologue
    .line 180950
    const-class v0, LX/15e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/15e;

    return-object v0
.end method

.method public static values()[LX/15e;
    .locals 1

    .prologue
    .line 180951
    sget-object v0, LX/15e;->$VALUES:[LX/15e;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/15e;

    return-object v0
.end method
