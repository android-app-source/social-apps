.class public LX/12L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile i:LX/12L;


# instance fields
.field public final b:LX/0yc;

.field private final c:LX/0tX;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/0Sh;

.field private final f:LX/0yH;

.field public g:LX/127;

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175256
    const-class v0, LX/12L;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/12L;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0yc;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;LX/0yH;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175258
    iput-object p1, p0, LX/12L;->b:LX/0yc;

    .line 175259
    iput-object p2, p0, LX/12L;->c:LX/0tX;

    .line 175260
    iput-object p3, p0, LX/12L;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 175261
    iput-object p4, p0, LX/12L;->e:LX/0Sh;

    .line 175262
    iput-object p5, p0, LX/12L;->f:LX/0yH;

    .line 175263
    return-void
.end method

.method public static a(LX/0QB;)LX/12L;
    .locals 9

    .prologue
    .line 175264
    sget-object v0, LX/12L;->i:LX/12L;

    if-nez v0, :cond_1

    .line 175265
    const-class v1, LX/12L;

    monitor-enter v1

    .line 175266
    :try_start_0
    sget-object v0, LX/12L;->i:LX/12L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175267
    if-eqz v2, :cond_0

    .line 175268
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 175269
    new-instance v3, LX/12L;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v8

    check-cast v8, LX/0yH;

    invoke-direct/range {v3 .. v8}, LX/12L;-><init>(LX/0yc;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;LX/0yH;)V

    .line 175270
    move-object v0, v3

    .line 175271
    sput-object v0, LX/12L;->i:LX/12L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175272
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175273
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175274
    :cond_1
    sget-object v0, LX/12L;->i:LX/12L;

    return-object v0

    .line 175275
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175276
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 175277
    iget-object v0, p0, LX/12L;->f:LX/0yH;

    sget-object v1, LX/0yY;->DIALTONE_TOGGLE_FB4A_SERVER_STICKY:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175278
    :goto_0
    return-void

    .line 175279
    :cond_0
    new-instance v1, LX/4KW;

    invoke-direct {v1}, LX/4KW;-><init>()V

    if-eqz p1, :cond_2

    const-string v0, "free_data_mode"

    .line 175280
    :goto_1
    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175281
    move-object v0, v1

    .line 175282
    new-instance v1, LX/49s;

    invoke-direct {v1}, LX/49s;-><init>()V

    move-object v1, v1

    .line 175283
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/49s;

    .line 175284
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 175285
    iget-object v1, p0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    .line 175286
    iget-object v1, p0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 175287
    :cond_1
    iget-object v1, p0, LX/12L;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 175288
    iget-object v0, p0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/5N9;

    invoke-direct {v1, p0, p1}, LX/5N9;-><init>(LX/12L;Z)V

    iget-object v2, p0, LX/12L;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 175289
    :cond_2
    const-string v0, "paid_data_mode"

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 175290
    iget-object v0, p0, LX/12L;->e:LX/0Sh;

    new-instance v1, Lcom/facebook/dialtone/ZeroToggleStickyModeManager$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/dialtone/ZeroToggleStickyModeManager$2;-><init>(LX/12L;Z)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 175291
    return-void
.end method
