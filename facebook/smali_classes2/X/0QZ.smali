.class public final enum LX/0QZ;
.super LX/0QX;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 58182
    invoke-direct {p0, p1, p2}, LX/0QX;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final defaultEquivalence()LX/0Qj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58183
    sget-object v0, LX/0Qi;->INSTANCE:LX/0Qi;

    move-object v0, v0

    .line 58184
    return-object v0
.end method

.method public final referenceValue(LX/0Qx;LX/0R1;Ljava/lang/Object;I)LX/0Qf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;TV;I)",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58185
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    new-instance v0, LX/4wM;

    iget-object v1, p1, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, v1, p3, p2}, LX/4wM;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4wW;

    iget-object v1, p1, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, v1, p3, p2, p4}, LX/4wW;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;I)V

    goto :goto_0
.end method
