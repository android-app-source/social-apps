.class public final LX/106;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:LX/0h2;


# direct methods
.method public constructor <init>(LX/0h2;)V
    .locals 0

    .prologue
    .line 168359
    iput-object p1, p0, LX/106;->a:LX/0h2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 168360
    iget-object v0, p0, LX/106;->a:LX/0h2;

    .line 168361
    iget-object v1, v0, LX/0h2;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/23i;

    invoke-virtual {v1}, LX/23i;->a()Z

    move-result v1

    move v0, v1

    .line 168362
    if-nez v0, :cond_0

    .line 168363
    iget-object v0, p0, LX/106;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PR;

    .line 168364
    iget-boolean v1, v0, LX/2PR;->p:Z

    if-eqz v1, :cond_1

    .line 168365
    :cond_0
    :goto_0
    iget-object v0, p0, LX/106;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    iget-object v1, p0, LX/106;->a:LX/0h2;

    iget-object v1, v1, LX/0h2;->h:Landroid/content/Context;

    sget-object v2, LX/0ax;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 168366
    const-string v0, "is_from_messenger_button"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168367
    const-string v0, "trigger"

    const-string v2, "tap_title_bar"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168368
    const-string v0, "is_from_fb4a"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168369
    iget-object v0, p0, LX/106;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/106;->a:LX/0h2;

    iget-object v2, v2, LX/0h2;->h:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 168370
    return-void

    .line 168371
    :cond_1
    iget-object v1, v0, LX/2PR;->f:LX/0xB;

    sget-object v2, LX/12j;->INBOX:LX/12j;

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0xB;->a(LX/12j;I)V

    goto :goto_0
.end method
