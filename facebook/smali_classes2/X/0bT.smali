.class public LX/0bT;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "Ljava/lang/Object;",
        "LX/0bY;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bT;


# instance fields
.field private final a:LX/0bW;


# direct methods
.method public constructor <init>(LX/0bW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86984
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 86985
    iput-object p1, p0, LX/0bT;->a:LX/0bW;

    .line 86986
    return-void
.end method

.method public static a(LX/0QB;)LX/0bT;
    .locals 4

    .prologue
    .line 86987
    sget-object v0, LX/0bT;->b:LX/0bT;

    if-nez v0, :cond_1

    .line 86988
    const-class v1, LX/0bT;

    monitor-enter v1

    .line 86989
    :try_start_0
    sget-object v0, LX/0bT;->b:LX/0bT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 86990
    if-eqz v2, :cond_0

    .line 86991
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 86992
    new-instance p0, LX/0bT;

    invoke-static {v0}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v3

    check-cast v3, LX/0bW;

    invoke-direct {p0, v3}, LX/0bT;-><init>(LX/0bW;)V

    .line 86993
    move-object v0, p0

    .line 86994
    sput-object v0, LX/0bT;->b:LX/0bT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86995
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 86996
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86997
    :cond_1
    sget-object v0, LX/0bT;->b:LX/0bT;

    return-object v0

    .line 86998
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 86999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/0b7;)V
    .locals 0

    .prologue
    .line 87000
    check-cast p1, LX/0bY;

    invoke-virtual {p0, p1}, LX/0bT;->a(LX/0bY;)V

    return-void
.end method

.method public final a(LX/0bY;)V
    .locals 2

    .prologue
    .line 87001
    invoke-super {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 87002
    iget-object v0, p0, LX/0bT;->a:LX/0bW;

    invoke-virtual {p1}, LX/0bY;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0bW;->a(Ljava/lang/String;)V

    .line 87003
    return-void
.end method
