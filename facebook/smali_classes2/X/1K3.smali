.class public LX/1K3;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/0fm;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field private final a:LX/1K2;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(LX/1K2;LX/0Ot;)V
    .locals 0
    .param p1    # LX/1K2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1K2;",
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230827
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 230828
    iput-object p2, p0, LX/1K3;->b:LX/0Ot;

    .line 230829
    iput-object p1, p0, LX/1K3;->a:LX/1K2;

    .line 230830
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 230835
    const/16 v0, 0x740

    invoke-static {v0, p1, p1, p3}, LX/2ck;->a(IIILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230836
    :goto_0
    return-void

    .line 230837
    :cond_0
    const-string v0, "privacy_option_to_upsell"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 230838
    iget-object v1, p0, LX/1K3;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Px;

    iget-object v2, p0, LX/1K3;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/1K3;->c:Landroid/view/ViewGroup;

    iget-object v4, p0, LX/1K3;->a:LX/1K2;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/8Px;->a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/1K2;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 230833
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, LX/1K3;->c:Landroid/view/ViewGroup;

    .line 230834
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 230831
    const/4 v0, 0x0

    iput-object v0, p0, LX/1K3;->c:Landroid/view/ViewGroup;

    .line 230832
    return-void
.end method
