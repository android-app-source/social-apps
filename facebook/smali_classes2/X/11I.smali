.class public final enum LX/11I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/11I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/11I;

.field public static final enum AUTHENTICATE:LX/11I;

.field public static final enum BATCH_COMPONENT_FETCH_CONFIGURATION:LX/11I;

.field public static final enum BOOKMARK_SYNC:LX/11I;

.field public static final enum CONFIRM_CONTACTPOINT_PRECONFIRMATION:LX/11I;

.field public static final enum CONFIRM_MESSENGER_ONLY_CODE:LX/11I;

.field public static final enum CONTACT_POINT_SUGGESTIONS:LX/11I;

.field public static final enum CREATE_MESSENGER_ACCOUNT:LX/11I;

.field public static final enum FETCH_ZERO_IP_TEST:LX/11I;

.field public static final enum FETCH_ZERO_MESSAGE_QUOTA_QUERY:LX/11I;

.field public static final enum FETCH_ZERO_TOKEN_QUERY:LX/11I;

.field public static final enum GET_LOGGED_IN_USER_QUERY:LX/11I;

.field public static final enum GET_NOTIFICATION_COUNT:LX/11I;

.field public static final enum GET_NOTIFICATION_URI:LX/11I;

.field public static final enum GK_INFO:LX/11I;

.field public static final enum HEADER_PREFILL_KICKOFF:LX/11I;

.field public static final enum INITIATE_PRECONFIRMATION:LX/11I;

.field public static final enum LOGIN_MESSENGER_CREDS_BYPASS:LX/11I;

.field public static final enum LOGOUT:LX/11I;

.field public static final enum MESSENGER_ONLY_MIGRATE_ACCOUNT:LX/11I;

.field public static final enum MQTT_CONFIG:LX/11I;

.field public static final enum NOTIFICATION_GET_SEEN_STATES:LX/11I;

.field public static final enum REGISTER_ACCOUNT:LX/11I;

.field public static final enum REGISTER_PUSH:LX/11I;

.field public static final enum REQUEST_MESSENGER_ONLY_CODE:LX/11I;

.field public static final enum RESET_PASSWORD_PRECONFIRMATION:LX/11I;

.field public static final enum SESSIONLESS_GK:LX/11I;

.field public static final enum SMS_INVITE:LX/11I;

.field public static final enum SYNC_X_CONFIGS:LX/11I;

.field public static final enum UNREGISTER_PUSH:LX/11I;

.field public static final enum VALIDATE_REGISTRATION_DATA:LX/11I;

.field public static final enum ZERO_IP_TEST_SUBMIT:LX/11I;


# instance fields
.field public final requestNameString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 171901
    new-instance v0, LX/11I;

    const-string v1, "CONFIRM_CONTACTPOINT_PRECONFIRMATION"

    const-string v2, "confirmContactpointPreconfirmation"

    invoke-direct {v0, v1, v4, v2}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->CONFIRM_CONTACTPOINT_PRECONFIRMATION:LX/11I;

    .line 171902
    new-instance v0, LX/11I;

    const-string v1, "INITIATE_PRECONFIRMATION"

    const-string v2, "initiatePreconfirmation"

    invoke-direct {v0, v1, v5, v2}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->INITIATE_PRECONFIRMATION:LX/11I;

    .line 171903
    new-instance v0, LX/11I;

    const-string v1, "REGISTER_ACCOUNT"

    const-string v2, "registerAccount"

    invoke-direct {v0, v1, v6, v2}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->REGISTER_ACCOUNT:LX/11I;

    .line 171904
    new-instance v0, LX/11I;

    const-string v1, "RESET_PASSWORD_PRECONFIRMATION"

    const-string v2, "resetPasswordPreconfirmation"

    invoke-direct {v0, v1, v7, v2}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->RESET_PASSWORD_PRECONFIRMATION:LX/11I;

    .line 171905
    new-instance v0, LX/11I;

    const-string v1, "VALIDATE_REGISTRATION_DATA"

    const-string v2, "validateRegistrationData"

    invoke-direct {v0, v1, v8, v2}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->VALIDATE_REGISTRATION_DATA:LX/11I;

    .line 171906
    new-instance v0, LX/11I;

    const-string v1, "CONTACT_POINT_SUGGESTIONS"

    const/4 v2, 0x5

    const-string v3, "contactPointSuggestions"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->CONTACT_POINT_SUGGESTIONS:LX/11I;

    .line 171907
    new-instance v0, LX/11I;

    const-string v1, "HEADER_PREFILL_KICKOFF"

    const/4 v2, 0x6

    const-string v3, "headerPrefillKickoff"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->HEADER_PREFILL_KICKOFF:LX/11I;

    .line 171908
    new-instance v0, LX/11I;

    const-string v1, "SYNC_X_CONFIGS"

    const/4 v2, 0x7

    const-string v3, "syncXConfigs"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->SYNC_X_CONFIGS:LX/11I;

    .line 171909
    new-instance v0, LX/11I;

    const-string v1, "SESSIONLESS_GK"

    const/16 v2, 0x8

    const-string v3, "fetchSessionlessGKInfo"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->SESSIONLESS_GK:LX/11I;

    .line 171910
    new-instance v0, LX/11I;

    const-string v1, "GK_INFO"

    const/16 v2, 0x9

    const-string v3, "fetchGKInfo"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->GK_INFO:LX/11I;

    .line 171911
    new-instance v0, LX/11I;

    const-string v1, "REGISTER_PUSH"

    const/16 v2, 0xa

    const-string v3, "registerPush"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->REGISTER_PUSH:LX/11I;

    .line 171912
    new-instance v0, LX/11I;

    const-string v1, "UNREGISTER_PUSH"

    const/16 v2, 0xb

    const-string v3, "unregisterPush"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->UNREGISTER_PUSH:LX/11I;

    .line 171913
    new-instance v0, LX/11I;

    const-string v1, "GET_NOTIFICATION_COUNT"

    const/16 v2, 0xc

    const-string v3, "getNotificationCount"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->GET_NOTIFICATION_COUNT:LX/11I;

    .line 171914
    new-instance v0, LX/11I;

    const-string v1, "NOTIFICATION_GET_SEEN_STATES"

    const/16 v2, 0xd

    const-string v3, "graphNotificationGetSeenStates"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->NOTIFICATION_GET_SEEN_STATES:LX/11I;

    .line 171915
    new-instance v0, LX/11I;

    const-string v1, "GET_NOTIFICATION_URI"

    const/16 v2, 0xe

    const-string v3, "getNotificationURI"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->GET_NOTIFICATION_URI:LX/11I;

    .line 171916
    new-instance v0, LX/11I;

    const-string v1, "LOGOUT"

    const/16 v2, 0xf

    const-string v3, "logout"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->LOGOUT:LX/11I;

    .line 171917
    new-instance v0, LX/11I;

    const-string v1, "AUTHENTICATE"

    const/16 v2, 0x10

    const-string v3, "authenticate"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->AUTHENTICATE:LX/11I;

    .line 171918
    new-instance v0, LX/11I;

    const-string v1, "BOOKMARK_SYNC"

    const/16 v2, 0x11

    const-string v3, "bookmarkSync"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->BOOKMARK_SYNC:LX/11I;

    .line 171919
    new-instance v0, LX/11I;

    const-string v1, "MQTT_CONFIG"

    const/16 v2, 0x12

    const-string v3, "getMobileConfig"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->MQTT_CONFIG:LX/11I;

    .line 171920
    new-instance v0, LX/11I;

    const-string v1, "SMS_INVITE"

    const/16 v2, 0x13

    const-string v3, "messenger_invites"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->SMS_INVITE:LX/11I;

    .line 171921
    new-instance v0, LX/11I;

    const-string v1, "MESSENGER_ONLY_MIGRATE_ACCOUNT"

    const/16 v2, 0x14

    const-string v3, "messenger_only_migrate_account"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->MESSENGER_ONLY_MIGRATE_ACCOUNT:LX/11I;

    .line 171922
    new-instance v0, LX/11I;

    const-string v1, "GET_LOGGED_IN_USER_QUERY"

    const/16 v2, 0x15

    const-string v3, "GetLoggedInUserQuery"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->GET_LOGGED_IN_USER_QUERY:LX/11I;

    .line 171923
    new-instance v0, LX/11I;

    const-string v1, "FETCH_ZERO_TOKEN_QUERY"

    const/16 v2, 0x16

    const-string v3, "FetchZeroTokenQuery"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->FETCH_ZERO_TOKEN_QUERY:LX/11I;

    .line 171924
    new-instance v0, LX/11I;

    const-string v1, "FETCH_ZERO_MESSAGE_QUOTA_QUERY"

    const/16 v2, 0x17

    const-string v3, "FetchZeroMessageQuotaQuery"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->FETCH_ZERO_MESSAGE_QUOTA_QUERY:LX/11I;

    .line 171925
    new-instance v0, LX/11I;

    const-string v1, "FETCH_ZERO_IP_TEST"

    const/16 v2, 0x18

    const-string v3, "FetchZeroIPTest"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->FETCH_ZERO_IP_TEST:LX/11I;

    .line 171926
    new-instance v0, LX/11I;

    const-string v1, "ZERO_IP_TEST_SUBMIT"

    const/16 v2, 0x19

    const-string v3, "ZeroIPTestSubmitMutation"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->ZERO_IP_TEST_SUBMIT:LX/11I;

    .line 171927
    new-instance v0, LX/11I;

    const-string v1, "REQUEST_MESSENGER_ONLY_CODE"

    const/16 v2, 0x1a

    const-string v3, "requestMessengerOnlyConfirmationCode"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->REQUEST_MESSENGER_ONLY_CODE:LX/11I;

    .line 171928
    new-instance v0, LX/11I;

    const-string v1, "CONFIRM_MESSENGER_ONLY_CODE"

    const/16 v2, 0x1b

    const-string v3, "confirmMessengerOnlyConfirmationCode"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->CONFIRM_MESSENGER_ONLY_CODE:LX/11I;

    .line 171929
    new-instance v0, LX/11I;

    const-string v1, "LOGIN_MESSENGER_CREDS_BYPASS"

    const/16 v2, 0x1c

    const-string v3, "loginBypassWithMessengerCredentials"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->LOGIN_MESSENGER_CREDS_BYPASS:LX/11I;

    .line 171930
    new-instance v0, LX/11I;

    const-string v1, "CREATE_MESSENGER_ACCOUNT"

    const/16 v2, 0x1d

    const-string v3, "createMessengerOnlyAccount"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->CREATE_MESSENGER_ACCOUNT:LX/11I;

    .line 171931
    new-instance v0, LX/11I;

    const-string v1, "BATCH_COMPONENT_FETCH_CONFIGURATION"

    const/16 v2, 0x1e

    const-string v3, "handleFetchConfiguration"

    invoke-direct {v0, v1, v2, v3}, LX/11I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/11I;->BATCH_COMPONENT_FETCH_CONFIGURATION:LX/11I;

    .line 171932
    const/16 v0, 0x1f

    new-array v0, v0, [LX/11I;

    sget-object v1, LX/11I;->CONFIRM_CONTACTPOINT_PRECONFIRMATION:LX/11I;

    aput-object v1, v0, v4

    sget-object v1, LX/11I;->INITIATE_PRECONFIRMATION:LX/11I;

    aput-object v1, v0, v5

    sget-object v1, LX/11I;->REGISTER_ACCOUNT:LX/11I;

    aput-object v1, v0, v6

    sget-object v1, LX/11I;->RESET_PASSWORD_PRECONFIRMATION:LX/11I;

    aput-object v1, v0, v7

    sget-object v1, LX/11I;->VALIDATE_REGISTRATION_DATA:LX/11I;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/11I;->CONTACT_POINT_SUGGESTIONS:LX/11I;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/11I;->HEADER_PREFILL_KICKOFF:LX/11I;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/11I;->SYNC_X_CONFIGS:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/11I;->SESSIONLESS_GK:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/11I;->GK_INFO:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/11I;->REGISTER_PUSH:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/11I;->UNREGISTER_PUSH:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/11I;->GET_NOTIFICATION_COUNT:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/11I;->NOTIFICATION_GET_SEEN_STATES:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/11I;->GET_NOTIFICATION_URI:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/11I;->LOGOUT:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/11I;->AUTHENTICATE:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/11I;->BOOKMARK_SYNC:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/11I;->MQTT_CONFIG:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/11I;->SMS_INVITE:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/11I;->MESSENGER_ONLY_MIGRATE_ACCOUNT:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/11I;->GET_LOGGED_IN_USER_QUERY:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/11I;->FETCH_ZERO_TOKEN_QUERY:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/11I;->FETCH_ZERO_MESSAGE_QUOTA_QUERY:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/11I;->FETCH_ZERO_IP_TEST:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/11I;->ZERO_IP_TEST_SUBMIT:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/11I;->REQUEST_MESSENGER_ONLY_CODE:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/11I;->CONFIRM_MESSENGER_ONLY_CODE:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/11I;->LOGIN_MESSENGER_CREDS_BYPASS:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/11I;->CREATE_MESSENGER_ACCOUNT:LX/11I;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/11I;->BATCH_COMPONENT_FETCH_CONFIGURATION:LX/11I;

    aput-object v2, v0, v1

    sput-object v0, LX/11I;->$VALUES:[LX/11I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 171898
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 171899
    iput-object p3, p0, LX/11I;->requestNameString:Ljava/lang/String;

    .line 171900
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/11I;
    .locals 1

    .prologue
    .line 171897
    const-class v0, LX/11I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/11I;

    return-object v0
.end method

.method public static values()[LX/11I;
    .locals 1

    .prologue
    .line 171896
    sget-object v0, LX/11I;->$VALUES:[LX/11I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/11I;

    return-object v0
.end method
