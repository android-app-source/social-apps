.class public LX/1h0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1h1;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1h0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295473
    const-class v0, LX/1h0;

    sput-object v0, LX/1h0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 295471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295472
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295469
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 295470
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 295474
    return-object p1
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 0

    .prologue
    .line 295468
    return-object p1
.end method

.method public final a(LX/1hZ;)V
    .locals 0

    .prologue
    .line 295467
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 295465
    sget-object v0, LX/1h0;->a:Ljava/lang/Class;

    const-string v1, "Dummy DefaultOnionRewriter enabled"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 295466
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295463
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 295464
    return-object v0
.end method
