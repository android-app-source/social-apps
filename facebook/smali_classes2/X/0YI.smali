.class public LX/0YI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0YI;


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80873
    const-string v0, "PerfMethodTracePref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/0YI;->a:Landroid/content/SharedPreferences;

    .line 80874
    iput-object p2, p0, LX/0YI;->b:LX/0SG;

    .line 80875
    return-void
.end method

.method public static a(LX/0QB;)LX/0YI;
    .locals 5

    .prologue
    .line 80876
    sget-object v0, LX/0YI;->c:LX/0YI;

    if-nez v0, :cond_1

    .line 80877
    const-class v1, LX/0YI;

    monitor-enter v1

    .line 80878
    :try_start_0
    sget-object v0, LX/0YI;->c:LX/0YI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80879
    if-eqz v2, :cond_0

    .line 80880
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80881
    new-instance p0, LX/0YI;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/0YI;-><init>(Landroid/content/Context;LX/0SG;)V

    .line 80882
    move-object v0, p0

    .line 80883
    sput-object v0, LX/0YI;->c:LX/0YI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80884
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80885
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80886
    :cond_1
    sget-object v0, LX/0YI;->c:LX/0YI;

    return-object v0

    .line 80887
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 80889
    invoke-virtual {p0, p1}, LX/0YI;->c(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80890
    iget-object v0, p0, LX/0YI;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80891
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 80892
    iget-object v0, p0, LX/0YI;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LastRunTime"

    iget-object v2, p0, LX/0YI;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80893
    return-void
.end method

.method public final c(Ljava/io/File;)Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 80894
    iget-object v0, p0, LX/0YI;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
