.class public final LX/1kq;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 310314
    const-class v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    const v0, 0x7dae79f2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchProductionPromptsQuery"

    const-string v6, "6eaa50bbd7e23f6530c950e47449b0c4"

    const-string v7, "viewer"

    const-string v8, "10155213655716729"

    const-string v9, "10155259088206729"

    .line 310315
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 310316
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 310317
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 310286
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 310287
    sparse-switch v0, :sswitch_data_0

    .line 310288
    :goto_0
    return-object p1

    .line 310289
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 310290
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 310291
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 310292
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 310293
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 310294
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 310295
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 310296
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 310297
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 310298
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 310299
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 310300
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 310301
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 310302
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 310303
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 310304
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 310305
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 310306
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 310307
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 310308
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 310309
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 310310
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 310311
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 310312
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 310313
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f5c3d0f -> :sswitch_b
        -0x7b752021 -> :sswitch_0
        -0x759d3f02 -> :sswitch_14
        -0x6b1af908 -> :sswitch_9
        -0x69f19a9a -> :sswitch_5
        -0x6326fdb3 -> :sswitch_7
        -0x5e743804 -> :sswitch_15
        -0x5dc44b76 -> :sswitch_11
        -0x55ff6f9b -> :sswitch_6
        -0x51484e72 -> :sswitch_2
        -0x50cab1c8 -> :sswitch_13
        -0x4eea3afb -> :sswitch_18
        -0x4ae70342 -> :sswitch_12
        -0x41a91745 -> :sswitch_f
        -0x24e1906f -> :sswitch_17
        -0x17e48248 -> :sswitch_16
        -0x8ca6426 -> :sswitch_d
        0x5a7510f -> :sswitch_1
        0xc168ff8 -> :sswitch_4
        0x291d8de0 -> :sswitch_10
        0x34e16755 -> :sswitch_8
        0x3f836ba7 -> :sswitch_a
        0x54df6484 -> :sswitch_e
        0x66d9d3b1 -> :sswitch_3
        0x7c6b80b3 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 310279
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 310280
    :goto_1
    return v0

    .line 310281
    :sswitch_0
    const-string v4, "11"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v4, "8"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "21"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_3
    const-string v4, "23"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 310282
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 310283
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 310284
    :pswitch_2
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 310285
    :pswitch_3
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x38 -> :sswitch_1
        0x620 -> :sswitch_0
        0x63f -> :sswitch_2
        0x641 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
