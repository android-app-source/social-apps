.class public abstract LX/12V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/12V;
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 175477
    iget v1, p0, LX/12V;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 175476
    iget v0, p0, LX/12V;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 175468
    iget v0, p0, LX/12V;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175471
    iget v0, p0, LX/12V;->a:I

    packed-switch v0, :pswitch_data_0

    .line 175472
    const-string v0, "?"

    :goto_0
    return-object v0

    .line 175473
    :pswitch_0
    const-string v0, "ROOT"

    goto :goto_0

    .line 175474
    :pswitch_1
    const-string v0, "ARRAY"

    goto :goto_0

    .line 175475
    :pswitch_2
    const-string v0, "OBJECT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 175470
    iget v0, p0, LX/12V;->b:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 175469
    iget v0, p0, LX/12V;->b:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/12V;->b:I

    goto :goto_0
.end method

.method public abstract h()Ljava/lang/String;
.end method
