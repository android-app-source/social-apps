.class public final LX/1Oc;
.super LX/1OD;
.source ""


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 241083
    iput-object p1, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, LX/1OD;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/v7/widget/RecyclerView;B)V
    .locals 0

    .prologue
    .line 241082
    invoke-direct {p0, p1}, LX/1Oc;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 241076
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_0

    .line 241077
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 241078
    :goto_0
    return-void

    .line 241079
    :cond_0
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    .line 241080
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 241081
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 241065
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 241066
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241067
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241068
    iput-boolean v2, v0, LX/1Ok;->j:Z

    .line 241069
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->H(Landroid/support/v7/widget/RecyclerView;)V

    .line 241070
    :goto_0
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241071
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 241072
    :cond_0
    return-void

    .line 241073
    :cond_1
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241074
    iput-boolean v2, v0, LX/1Ok;->j:Z

    .line 241075
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->H(Landroid/support/v7/widget/RecyclerView;)V

    goto :goto_0
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 241061
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 241062
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0, p1, p2, p3}, LX/1On;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241063
    invoke-direct {p0}, LX/1Oc;->b()V

    .line 241064
    :cond_0
    return-void
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 241040
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 241041
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    const/4 v1, 0x1

    .line 241042
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v0, v3, p1, p2, p3}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241043
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v1, :cond_1

    :goto_0
    move v0, v1

    .line 241044
    if-eqz v0, :cond_0

    .line 241045
    invoke-direct {p0}, LX/1Oc;->b()V

    .line 241046
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    .line 241054
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 241055
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 241056
    iget-object v3, v0, LX/1On;->a:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, p1, p2, v4}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241057
    iget-object v3, v0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_0
    move v0, v1

    .line 241058
    if-eqz v0, :cond_0

    .line 241059
    invoke-direct {p0}, LX/1Oc;->b()V

    .line 241060
    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public final c(II)V
    .locals 4

    .prologue
    .line 241047
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 241048
    iget-object v0, p0, LX/1Oc;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    const/4 v1, 0x1

    .line 241049
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, p2, v3}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241050
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v1, :cond_1

    :goto_0
    move v0, v1

    .line 241051
    if-eqz v0, :cond_0

    .line 241052
    invoke-direct {p0}, LX/1Oc;->b()V

    .line 241053
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
