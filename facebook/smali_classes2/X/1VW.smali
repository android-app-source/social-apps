.class public LX/1VW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/0sX;

.field private final d:LX/0yc;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/32j;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 264650
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    sput-object v0, LX/1VW;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0sX;LX/0yc;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0sX;",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "LX/32j;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264645
    iput-object p1, p0, LX/1VW;->b:LX/0ad;

    .line 264646
    iput-object p2, p0, LX/1VW;->c:LX/0sX;

    .line 264647
    iput-object p3, p0, LX/1VW;->d:LX/0yc;

    .line 264648
    iput-object p4, p0, LX/1VW;->e:LX/0Ot;

    .line 264649
    return-void
.end method

.method public static a(LX/0yc;LX/1f6;Lcom/facebook/common/callercontext/CallerContext;)I
    .locals 1

    .prologue
    .line 264639
    invoke-virtual {p0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1f6;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/0yc;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0yc;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 264640
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0yc;->d()I

    move-result v0

    :goto_1
    return v0

    .line 264641
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 264642
    :cond_1
    iget v0, p1, LX/1f6;->h:I

    move v0, v0

    .line 264643
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/1VW;
    .locals 7

    .prologue
    .line 264628
    const-class v1, LX/1VW;

    monitor-enter v1

    .line 264629
    :try_start_0
    sget-object v0, LX/1VW;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264630
    sput-object v2, LX/1VW;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264631
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264632
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264633
    new-instance v6, LX/1VW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v4

    check-cast v4, LX/0sX;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v5

    check-cast v5, LX/0yc;

    const/16 p0, 0x15e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/1VW;-><init>(LX/0ad;LX/0sX;LX/0yc;LX/0Ot;)V

    .line 264634
    move-object v0, v6

    .line 264635
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264636
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1VW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264637
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;LX/0sX;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 264626
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 264627
    invoke-virtual {p1}, LX/0sX;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/22Q;Lcom/facebook/graphql/model/GraphQLMedia;LX/1f6;LX/1aZ;LX/1Pr;Lcom/facebook/common/callercontext/CallerContext;)LX/1Dg;
    .locals 10
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/22Q;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1f6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1aZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/22Q;",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            "LX/1f6;",
            "LX/1aZ;",
            "TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 264607
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 264608
    invoke-static {p4}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;

    move-result-object v3

    .line 264609
    iget-object v2, p0, LX/1VW;->d:LX/0yc;

    move-object/from16 v0, p8

    invoke-static {v2, p5, v0}, LX/1VW;->a(LX/0yc;LX/1f6;Lcom/facebook/common/callercontext/CallerContext;)I

    move-result v4

    .line 264610
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, LX/0jW;

    move-object/from16 v0, p7

    invoke-interface {v0, p3, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 264611
    iget-object v2, p0, LX/1VW;->f:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 264612
    iget-object v2, p0, LX/1VW;->b:LX/0ad;

    sget-short v6, LX/1xO;->d:S

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, LX/1VW;->f:Ljava/lang/Boolean;

    .line 264613
    :cond_0
    iget-object v2, p0, LX/1VW;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264614
    iget-object v2, p0, LX/1VW;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/32j;

    invoke-virtual {v2, p1}, LX/32j;->c(LX/1De;)LX/32l;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, LX/32l;->a(LX/1aZ;)LX/32l;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/32l;->a(Landroid/graphics/PointF;)LX/32l;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 264615
    if-nez v5, :cond_1

    .line 264616
    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v3

    const/4 v5, -0x1

    invoke-virtual {v3, v5}, LX/1nh;->h(I)LX/1nh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Di;->b(LX/1n6;)LX/1Di;

    .line 264617
    :cond_1
    iget-object v3, p0, LX/1VW;->c:LX/0sX;

    invoke-static {p1, v3, v1}, LX/1VW;->a(Landroid/content/Context;LX/0sX;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-virtual {p5}, LX/1f6;->e()I

    move-result v2

    invoke-interface {v1, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 264618
    :goto_0
    return-object v1

    .line 264619
    :cond_2
    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v2

    const v6, 0x7f0a045d

    invoke-virtual {v2, v6}, LX/1nh;->i(I)LX/1nh;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 264620
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f021af6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 264621
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v7

    new-instance v8, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const/16 v9, 0x3e8

    invoke-direct {v8, v6, v9}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v7, v8}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    .line 264622
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-virtual {v7, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1up;->a(LX/1dc;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/1up;->c(LX/1dc;)LX/1up;

    move-result-object v2

    const v3, 0x7f020aa6

    invoke-virtual {v2, v3}, LX/1up;->j(I)LX/1up;

    move-result-object v2

    sget-object v3, LX/1Up;->h:LX/1Up;

    invoke-virtual {v2, v3}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 264623
    if-nez v5, :cond_3

    .line 264624
    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v3

    const/4 v5, -0x1

    invoke-virtual {v3, v5}, LX/1nh;->h(I)LX/1nh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Di;->b(LX/1n6;)LX/1Di;

    .line 264625
    :cond_3
    iget-object v3, p0, LX/1VW;->c:LX/0sX;

    invoke-static {p1, v3, v1}, LX/1VW;->a(Landroid/content/Context;LX/0sX;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-virtual {p5}, LX/1f6;->e()I

    move-result v2

    invoke-interface {v1, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0
.end method
