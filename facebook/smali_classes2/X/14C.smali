.class public LX/14C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/14B;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/14C;


# instance fields
.field private final a:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178209
    iput-object p1, p0, LX/14C;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 178210
    return-void
.end method

.method public static a(LX/0QB;)LX/14C;
    .locals 4

    .prologue
    .line 178211
    sget-object v0, LX/14C;->b:LX/14C;

    if-nez v0, :cond_1

    .line 178212
    const-class v1, LX/14C;

    monitor-enter v1

    .line 178213
    :try_start_0
    sget-object v0, LX/14C;->b:LX/14C;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178214
    if-eqz v2, :cond_0

    .line 178215
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 178216
    new-instance p0, LX/14C;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3}, LX/14C;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 178217
    move-object v0, p0

    .line 178218
    sput-object v0, LX/14C;->b:LX/14C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178219
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178220
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178221
    :cond_1
    sget-object v0, LX/14C;->b:LX/14C;

    return-object v0

    .line 178222
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 178224
    iget-object v0, p0, LX/14C;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 178225
    invoke-static {p1, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 178226
    :cond_0
    :goto_0
    return-void

    .line 178227
    :cond_1
    iget-object p0, v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    move-object p0, p0

    .line 178228
    if-eqz p0, :cond_0

    .line 178229
    invoke-interface {p0, p1, p2}, LX/1hk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
