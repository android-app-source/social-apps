.class public final LX/1o2;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/1o2;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1o5;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/1o4;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 318248
    const/4 v0, 0x0

    sput-object v0, LX/1o2;->a:LX/1o2;

    .line 318249
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1o2;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 318250
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 318251
    new-instance v0, LX/1o4;

    invoke-direct {v0}, LX/1o4;-><init>()V

    iput-object v0, p0, LX/1o2;->c:LX/1o4;

    .line 318252
    return-void
.end method

.method public static a(LX/1De;II)LX/1o5;
    .locals 2

    .prologue
    .line 318253
    new-instance v0, LX/1o3;

    invoke-direct {v0}, LX/1o3;-><init>()V

    .line 318254
    sget-object v1, LX/1o2;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1o5;

    .line 318255
    if-nez v1, :cond_0

    .line 318256
    new-instance v1, LX/1o5;

    invoke-direct {v1}, LX/1o5;-><init>()V

    .line 318257
    :cond_0
    invoke-static {v1, p0, p1, p2, v0}, LX/1o5;->a$redex0(LX/1o5;LX/1De;IILX/1o3;)V

    .line 318258
    move-object v0, v1

    .line 318259
    return-object v0
.end method

.method public static c(LX/1De;)LX/1o5;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 318260
    invoke-static {p0, v0, v0}, LX/1o2;->a(LX/1De;II)LX/1o5;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/1o2;
    .locals 2

    .prologue
    .line 318261
    const-class v1, LX/1o2;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1o2;->a:LX/1o2;

    if-nez v0, :cond_0

    .line 318262
    new-instance v0, LX/1o2;

    invoke-direct {v0}, LX/1o2;-><init>()V

    sput-object v0, LX/1o2;->a:LX/1o2;

    .line 318263
    :cond_0
    sget-object v0, LX/1o2;->a:LX/1o2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 318264
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 318265
    invoke-static {}, LX/1dS;->b()V

    .line 318266
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x27cdb8b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 318267
    check-cast p6, LX/1o3;

    .line 318268
    iget-object v1, p6, LX/1o3;->a:LX/1dc;

    invoke-static {p1, p3, p4, p5, v1}, LX/1o4;->a(LX/1De;IILX/1no;LX/1dc;)V

    .line 318269
    const/16 v1, 0x1f

    const v2, -0x23a10a2d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 7

    .prologue
    .line 318270
    check-cast p3, LX/1o3;

    .line 318271
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 318272
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 318273
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 318274
    iget-object v2, p3, LX/1o3;->a:LX/1dc;

    iget-object v3, p3, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    move-object v0, p1

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LX/1o4;->a(LX/1De;LX/1Dg;LX/1dc;Landroid/widget/ImageView$ScaleType;LX/1np;LX/1np;LX/1np;)V

    .line 318275
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318276
    check-cast v0, LX/1oA;

    iput-object v0, p3, LX/1o3;->c:LX/1oA;

    .line 318277
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 318278
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318279
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p3, LX/1o3;->d:Ljava/lang/Integer;

    .line 318280
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 318281
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318282
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p3, LX/1o3;->e:Ljava/lang/Integer;

    .line 318283
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 318284
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 318285
    new-instance v0, LX/1oE;

    invoke-direct {v0}, LX/1oE;-><init>()V

    move-object v0, v0

    .line 318286
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 318221
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 318222
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 318223
    const/4 v3, 0x0

    .line 318224
    sget-object v0, LX/03r;->Image:[I

    invoke-virtual {p1, v0, v3}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 318225
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v5

    move v0, v3

    :goto_0
    if-ge v0, v5, :cond_2

    .line 318226
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v6

    .line 318227
    const/16 v7, 0x0

    if-ne v6, v7, :cond_1

    .line 318228
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v7

    invoke-virtual {v4, v6, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    invoke-virtual {v7, v6}, LX/1nm;->h(I)LX/1nm;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    .line 318229
    iput-object v6, v1, LX/1np;->a:Ljava/lang/Object;

    .line 318230
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318231
    :cond_1
    const/16 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 318232
    sget-object v7, LX/1o4;->a:[Landroid/widget/ImageView$ScaleType;

    const/4 p0, -0x1

    invoke-virtual {v4, v6, p0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    aget-object v6, v7, v6

    .line 318233
    iput-object v6, v2, LX/1np;->a:Ljava/lang/Object;

    .line 318234
    goto :goto_1

    .line 318235
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 318236
    check-cast p2, LX/1o3;

    .line 318237
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318238
    if-eqz v0, :cond_3

    .line 318239
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318240
    check-cast v0, LX/1dc;

    iput-object v0, p2, LX/1o3;->a:LX/1dc;

    .line 318241
    :cond_3
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 318242
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318243
    if-eqz v0, :cond_4

    .line 318244
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 318245
    check-cast v0, Landroid/widget/ImageView$ScaleType;

    iput-object v0, p2, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    .line 318246
    :cond_4
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 318247
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 318220
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 3

    .prologue
    .line 318187
    check-cast p1, LX/1o3;

    .line 318188
    check-cast p2, LX/1o3;

    .line 318189
    iget-object v0, p1, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    iget-object v1, p2, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 318190
    iget-object v1, p1, LX/1o3;->a:LX/1dc;

    iget-object v2, p2, LX/1o3;->a:LX/1dc;

    invoke-static {v1, v2}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 318191
    iget-object v2, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 318192
    iget-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 318193
    if-ne v2, p0, :cond_0

    .line 318194
    iget-object v2, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 318195
    check-cast v2, LX/1dc;

    .line 318196
    iget-object p0, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 318197
    check-cast p0, LX/1dc;

    invoke-static {v2, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 318198
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 318199
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 318200
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 318216
    check-cast p3, LX/1o3;

    .line 318217
    check-cast p2, LX/1oE;

    iget-object v0, p3, LX/1o3;->a:LX/1dc;

    iget-object v1, p3, LX/1o3;->c:LX/1oA;

    .line 318218
    invoke-static {p1, v0}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p0, v1}, LX/1oE;->a(Landroid/graphics/drawable/Drawable;LX/1oA;)V

    .line 318219
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 318215
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 318209
    check-cast p3, LX/1o3;

    .line 318210
    check-cast p2, LX/1oE;

    iget-object v0, p3, LX/1o3;->a:LX/1dc;

    .line 318211
    iget-object p0, p2, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    move-object p0, p0

    .line 318212
    invoke-static {p1, p0, v0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318213
    invoke-virtual {p2}, LX/1oE;->a()V

    .line 318214
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 318205
    check-cast p3, LX/1o3;

    .line 318206
    check-cast p2, LX/1oE;

    iget-object v0, p3, LX/1o3;->d:Ljava/lang/Integer;

    iget-object v1, p3, LX/1o3;->e:Ljava/lang/Integer;

    .line 318207
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p2, p0, p1}, LX/1oE;->a(II)V

    .line 318208
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 318204
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 318203
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 318202
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 318201
    const/16 v0, 0x1e

    return v0
.end method
