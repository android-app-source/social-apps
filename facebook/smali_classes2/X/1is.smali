.class public LX/1is;
.super LX/1iY;
.source ""


# instance fields
.field private final a:Lcom/facebook/performancelogger/PerformanceLogger;

.field private b:Z

.field private c:LX/0Yj;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299261
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299262
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1is;->b:Z

    .line 299263
    iput-object p1, p0, LX/1is;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 299264
    return-void
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 2
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299287
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 299288
    iget-boolean v0, p0, LX/1is;->b:Z

    if-nez v0, :cond_0

    .line 299289
    iget-object v0, p0, LX/1is;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/1is;->c:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 299290
    :cond_0
    return-void
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V
    .locals 4

    .prologue
    .line 299278
    invoke-super {p0, p1, p2, p3}, LX/1iY;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V

    .line 299279
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1is;->d:Ljava/lang/String;

    .line 299280
    iget-object v0, p0, LX/1is;->d:Ljava/lang/String;

    invoke-static {v0}, LX/1iH;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1is;->e:Ljava/lang/String;

    .line 299281
    iget-object v0, p0, LX/1is;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 299282
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1is;->b:Z

    .line 299283
    :goto_0
    return-void

    .line 299284
    :cond_0
    new-instance v0, LX/4h4;

    iget-object v1, p0, LX/1is;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v1, v2, v3}, LX/4h4;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;D)V

    .line 299285
    const v1, 0x4a0008

    const-string v2, "CDN_IMAGE_LOAD_LOGGER"

    invoke-virtual {v0, v1, v2}, LX/4h4;->a(ILjava/lang/String;)LX/0Yj;

    move-result-object v0

    iput-object v0, p0, LX/1is;->c:LX/0Yj;

    .line 299286
    iget-object v0, p0, LX/1is;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/1is;->c:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    goto :goto_0
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5

    .prologue
    .line 299265
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 299266
    iget-boolean v0, p0, LX/1is;->b:Z

    if-nez v0, :cond_2

    .line 299267
    iget-object v0, p0, LX/1is;->c:LX/0Yj;

    .line 299268
    iget-object v1, v0, LX/0Yj;->l:Ljava/util/Map;

    move-object v1, v1

    .line 299269
    sget-object v0, LX/JcY;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299270
    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 299271
    if-eqz v0, :cond_0

    .line 299272
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "header."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 299273
    :cond_1
    const-string v0, "url"

    iget-object v2, p0, LX/1is;->d:Ljava/lang/String;

    invoke-static {v2}, LX/1iH;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299274
    const-string v0, "cdn_type"

    iget-object v2, p0, LX/1is;->e:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299275
    iget-object v0, p0, LX/1is;->c:LX/0Yj;

    invoke-virtual {v0, v1}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 299276
    iget-object v0, p0, LX/1is;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/1is;->c:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 299277
    :cond_2
    return-void
.end method
