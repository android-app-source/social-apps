.class public LX/0Qd;
.super Ljava/util/AbstractMap;
.source ""

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final u:LX/0Qf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:I

.field public final c:I

.field public final d:[LX/0Qx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0Qx",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0QX;

.field public final i:LX/0QX;

.field public final j:J

.field public final k:LX/0Ql;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ql",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final l:J

.field public final m:J

.field public final n:J

.field public final o:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/4wi",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final p:LX/0Qn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final q:LX/0QV;

.field public final r:LX/0Qo;

.field public final s:LX/0QP;

.field public final t:LX/0QM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QM",
            "<-TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field public x:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public y:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58330
    const-class v0, LX/0Qd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/0Qd;->a:Ljava/util/logging/Logger;

    .line 58331
    new-instance v0, LX/0Qe;

    invoke-direct {v0}, LX/0Qe;-><init>()V

    sput-object v0, LX/0Qd;->u:LX/0Qf;

    .line 58332
    new-instance v0, LX/0Qg;

    invoke-direct {v0}, LX/0Qg;-><init>()V

    sput-object v0, LX/0Qd;->v:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(LX/0QN;LX/0QM;)V
    .locals 12
    .param p2    # LX/0QM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QN",
            "<-TK;-TV;>;",
            "LX/0QM",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 58333
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 58334
    iget v0, p1, LX/0QN;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x4

    :goto_0
    move v0, v0

    .line 58335
    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/0Qd;->e:I

    .line 58336
    invoke-virtual {p1}, LX/0QN;->h()LX/0QX;

    move-result-object v0

    iput-object v0, p0, LX/0Qd;->h:LX/0QX;

    .line 58337
    invoke-virtual {p1}, LX/0QN;->k()LX/0QX;

    move-result-object v0

    iput-object v0, p0, LX/0Qd;->i:LX/0QX;

    .line 58338
    iget-object v0, p1, LX/0QN;->p:LX/0Qj;

    invoke-virtual {p1}, LX/0QN;->h()LX/0QX;

    move-result-object v1

    invoke-virtual {v1}, LX/0QX;->defaultEquivalence()LX/0Qj;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Qj;

    move-object v0, v0

    .line 58339
    iput-object v0, p0, LX/0Qd;->f:LX/0Qj;

    .line 58340
    iget-object v0, p1, LX/0QN;->q:LX/0Qj;

    invoke-virtual {p1}, LX/0QN;->k()LX/0QX;

    move-result-object v1

    invoke-virtual {v1}, LX/0QX;->defaultEquivalence()LX/0Qj;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Qj;

    move-object v0, v0

    .line 58341
    iput-object v0, p0, LX/0Qd;->g:LX/0Qj;

    .line 58342
    invoke-virtual {p1}, LX/0QN;->e()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Qd;->j:J

    .line 58343
    iget-object v0, p1, LX/0QN;->j:LX/0Ql;

    sget-object v1, LX/0Qk;->INSTANCE:LX/0Qk;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ql;

    move-object v0, v0

    .line 58344
    iput-object v0, p0, LX/0Qd;->k:LX/0Ql;

    .line 58345
    invoke-virtual {p1}, LX/0QN;->m()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Qd;->l:J

    .line 58346
    invoke-virtual {p1}, LX/0QN;->l()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Qd;->m:J

    .line 58347
    invoke-virtual {p1}, LX/0QN;->n()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Qd;->n:J

    .line 58348
    iget-object v0, p1, LX/0QN;->r:LX/0Qn;

    sget-object v1, LX/0Qm;->INSTANCE:LX/0Qm;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Qn;

    move-object v0, v0

    .line 58349
    iput-object v0, p0, LX/0Qd;->p:LX/0Qn;

    .line 58350
    iget-object v0, p0, LX/0Qd;->p:LX/0Qn;

    sget-object v1, LX/0Qm;->INSTANCE:LX/0Qm;

    if-ne v0, v1, :cond_5

    .line 58351
    sget-object v0, LX/0Qd;->v:Ljava/util/Queue;

    move-object v0, v0

    .line 58352
    :goto_1
    iput-object v0, p0, LX/0Qd;->o:Ljava/util/Queue;

    .line 58353
    invoke-virtual {p0}, LX/0Qd;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Qd;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_0
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 58354
    iget-object v1, p1, LX/0QN;->s:LX/0QV;

    if-eqz v1, :cond_e

    .line 58355
    iget-object v1, p1, LX/0QN;->s:LX/0QV;

    .line 58356
    :goto_3
    move-object v0, v1

    .line 58357
    iput-object v0, p0, LX/0Qd;->q:LX/0QV;

    .line 58358
    iget-object v0, p0, LX/0Qd;->h:LX/0QX;

    .line 58359
    invoke-virtual {p0}, LX/0Qd;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, LX/0Qd;->g()Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_1
    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 58360
    invoke-virtual {p0}, LX/0Qd;->e()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, LX/0Qd;->f()Z

    move-result v3

    if-eqz v3, :cond_11

    :cond_2
    const/4 v3, 0x1

    :goto_5
    move v3, v3

    .line 58361
    invoke-static {v0, v1, v3}, LX/0Qo;->getFactory(LX/0QX;ZZ)LX/0Qo;

    move-result-object v0

    iput-object v0, p0, LX/0Qd;->r:LX/0Qo;

    .line 58362
    iget-object v0, p1, LX/0QN;->t:LX/0QR;

    move-object v0, v0

    .line 58363
    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QP;

    iput-object v0, p0, LX/0Qd;->s:LX/0QP;

    .line 58364
    iput-object p2, p0, LX/0Qd;->t:LX/0QM;

    .line 58365
    iget v0, p1, LX/0QN;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_12

    const/16 v0, 0x10

    :goto_6
    move v0, v0

    .line 58366
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 58367
    invoke-virtual {p0}, LX/0Qd;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, LX/0Qd;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 58368
    iget-wide v6, p0, LX/0Qd;->j:J

    long-to-int v1, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_3
    move v1, v2

    move v3, v4

    .line 58369
    :goto_7
    iget v5, p0, LX/0Qd;->e:I

    if-ge v1, v5, :cond_6

    invoke-virtual {p0}, LX/0Qd;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    mul-int/lit8 v5, v1, 0x14

    int-to-long v6, v5

    iget-wide v8, p0, LX/0Qd;->j:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_6

    .line 58370
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 58371
    shl-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 58372
    :cond_5
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto/16 :goto_1

    .line 58373
    :cond_6
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, LX/0Qd;->c:I

    .line 58374
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, LX/0Qd;->b:I

    .line 58375
    new-array v3, v1, [LX/0Qx;

    move-object v3, v3

    .line 58376
    iput-object v3, p0, LX/0Qd;->d:[LX/0Qx;

    .line 58377
    div-int v3, v0, v1

    .line 58378
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_b

    .line 58379
    add-int/lit8 v0, v3, 0x1

    :goto_8
    move v5, v2

    .line 58380
    :goto_9
    if-ge v5, v0, :cond_7

    .line 58381
    shl-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_9

    .line 58382
    :cond_7
    invoke-virtual {p0}, LX/0Qd;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 58383
    iget-wide v2, p0, LX/0Qd;->j:J

    int-to-long v6, v1

    div-long/2addr v2, v6

    add-long/2addr v2, v10

    .line 58384
    iget-wide v6, p0, LX/0Qd;->j:J

    int-to-long v0, v1

    rem-long/2addr v6, v0

    move-wide v0, v2

    .line 58385
    :goto_a
    iget-object v2, p0, LX/0Qd;->d:[LX/0Qx;

    array-length v2, v2

    if-ge v4, v2, :cond_9

    .line 58386
    int-to-long v2, v4

    cmp-long v2, v2, v6

    if-nez v2, :cond_a

    .line 58387
    sub-long v2, v0, v10

    .line 58388
    :goto_b
    iget-object v1, p0, LX/0Qd;->d:[LX/0Qx;

    .line 58389
    iget-object v0, p1, LX/0QN;->t:LX/0QR;

    move-object v0, v0

    .line 58390
    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QP;

    invoke-direct {p0, v5, v2, v3, v0}, LX/0Qd;->a(IJLX/0QP;)LX/0Qx;

    move-result-object v0

    aput-object v0, v1, v4

    .line 58391
    add-int/lit8 v4, v4, 0x1

    move-wide v0, v2

    goto :goto_a

    .line 58392
    :cond_8
    :goto_c
    iget-object v0, p0, LX/0Qd;->d:[LX/0Qx;

    array-length v0, v0

    if-ge v4, v0, :cond_9

    .line 58393
    iget-object v1, p0, LX/0Qd;->d:[LX/0Qx;

    const-wide/16 v2, -0x1

    .line 58394
    iget-object v0, p1, LX/0QN;->t:LX/0QR;

    move-object v0, v0

    .line 58395
    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QP;

    invoke-direct {p0, v5, v2, v3, v0}, LX/0Qd;->a(IJLX/0QP;)LX/0Qx;

    move-result-object v0

    aput-object v0, v1, v4

    .line 58396
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 58397
    :cond_9
    return-void

    :cond_a
    move-wide v2, v0

    goto :goto_b

    :cond_b
    move v0, v3

    goto :goto_8

    :cond_c
    iget v0, p1, LX/0QN;->g:I

    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_e
    if-eqz v0, :cond_f

    .line 58398
    sget-object v1, LX/0QV;->SYSTEM_TICKER:LX/0QV;

    move-object v1, v1

    .line 58399
    goto/16 :goto_3

    :cond_f
    sget-object v1, LX/0QN;->d:LX/0QV;

    goto/16 :goto_3

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_12
    iget v0, p1, LX/0QN;->f:I

    goto/16 :goto_6
.end method

.method private a(IJLX/0QP;)LX/0Qx;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "LX/0QP;",
            ")",
            "LX/0Qx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58400
    new-instance v1, LX/0Qx;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/0Qx;-><init>(LX/0Qd;IJLX/0QP;)V

    return-object v1
.end method

.method public static a(LX/0R1;LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58401
    invoke-interface {p0, p1}, LX/0R1;->setNextInAccessQueue(LX/0R1;)V

    .line 58402
    invoke-interface {p1, p0}, LX/0R1;->setPreviousInAccessQueue(LX/0R1;)V

    .line 58403
    return-void
.end method

.method public static b(LX/0Qd;I)LX/0Qx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Qx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58404
    iget-object v0, p0, LX/0Qd;->d:[LX/0Qx;

    iget v1, p0, LX/0Qd;->c:I

    ushr-int v1, p1, v1

    iget v2, p0, LX/0Qd;->b:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static b(Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TE;>;)",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 58405
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 58406
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 58407
    return-object v0
.end method

.method public static b(LX/0R1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58408
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 58409
    invoke-interface {p0, v0}, LX/0R1;->setNextInAccessQueue(LX/0R1;)V

    .line 58410
    invoke-interface {p0, v0}, LX/0R1;->setPreviousInAccessQueue(LX/0R1;)V

    .line 58411
    return-void
.end method

.method public static b(LX/0R1;LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58412
    invoke-interface {p0, p1}, LX/0R1;->setNextInWriteQueue(LX/0R1;)V

    .line 58413
    invoke-interface {p1, p0}, LX/0R1;->setPreviousInWriteQueue(LX/0R1;)V

    .line 58414
    return-void
.end method

.method public static c(LX/0Qd;Ljava/lang/Object;)I
    .locals 2
    .param p0    # LX/0Qd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58415
    iget-object v0, p0, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v0, p1}, LX/0Qj;->hash(Ljava/lang/Object;)I

    move-result v0

    .line 58416
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v1, v0

    .line 58417
    ushr-int/lit8 p0, v1, 0xa

    xor-int/2addr v1, p0

    .line 58418
    shl-int/lit8 p0, v1, 0x3

    add-int/2addr v1, p0

    .line 58419
    ushr-int/lit8 p0, v1, 0x6

    xor-int/2addr v1, p0

    .line 58420
    shl-int/lit8 p0, v1, 0x2

    shl-int/lit8 p1, v1, 0xe

    add-int/2addr p0, p1

    add-int/2addr v1, p0

    .line 58421
    ushr-int/lit8 p0, v1, 0x10

    xor-int/2addr v1, p0

    move v0, v1

    .line 58422
    return v0
.end method

.method public static c(LX/0R1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58439
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 58440
    invoke-interface {p0, v0}, LX/0R1;->setNextInWriteQueue(LX/0R1;)V

    .line 58441
    invoke-interface {p0, v0}, LX/0R1;->setPreviousInWriteQueue(LX/0R1;)V

    .line 58442
    return-void
.end method

.method private o()Z
    .locals 4

    .prologue
    .line 58423
    iget-wide v0, p0, LX/0Qd;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 4

    .prologue
    .line 58424
    iget-wide v0, p0, LX/0Qd;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/0QM;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/0QM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 58425
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58426
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, LX/0Qx;->a(Ljava/lang/Object;ILX/0QM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 58427
    iget-wide v0, p0, LX/0Qd;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 58428
    iget-object v0, p0, LX/0Qd;->k:LX/0Ql;

    sget-object v1, LX/0Qk;->INSTANCE:LX/0Qk;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0R1;J)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 58429
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58430
    invoke-direct {p0}, LX/0Qd;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LX/0R1;->getAccessTime()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, LX/0Qd;->l:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 58431
    :cond_0
    :goto_0
    return v0

    .line 58432
    :cond_1
    invoke-direct {p0}, LX/0Qd;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, LX/0R1;->getWriteTime()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, LX/0Qd;->m:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 58433
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 58434
    iget-wide v0, p0, LX/0Qd;->n:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clear()V
    .locals 4

    .prologue
    .line 58435
    iget-object v1, p0, LX/0Qd;->d:[LX/0Qx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 58436
    invoke-virtual {v3}, LX/0Qx;->a()V

    .line 58437
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58438
    :cond_0
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58303
    if-nez p1, :cond_0

    .line 58304
    const/4 v0, 0x0

    .line 58305
    :goto_0
    return v0

    .line 58306
    :cond_0
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58307
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0Qx;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 20
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58308
    if-nez p1, :cond_0

    .line 58309
    const/4 v4, 0x0

    .line 58310
    :goto_0
    return v4

    .line 58311
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v4}, LX/0QV;->read()J

    move-result-wide v14

    .line 58312
    move-object/from16 v0, p0

    iget-object v11, v0, LX/0Qd;->d:[LX/0Qx;

    .line 58313
    const-wide/16 v8, -0x1

    .line 58314
    const/4 v4, 0x0

    move v10, v4

    move-wide v12, v8

    :goto_1
    const/4 v4, 0x3

    if-ge v10, v4, :cond_5

    .line 58315
    const-wide/16 v6, 0x0

    .line 58316
    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_4

    aget-object v7, v11, v6

    .line 58317
    iget-object v0, v7, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v17, v0

    .line 58318
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 58319
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0R1;

    :goto_4
    if-eqz v4, :cond_2

    .line 58320
    invoke-virtual {v7, v4, v14, v15}, LX/0Qx;->a(LX/0R1;J)Ljava/lang/Object;

    move-result-object v18

    .line 58321
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Qd;->g:LX/0Qj;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 58322
    const/4 v4, 0x1

    goto :goto_0

    .line 58323
    :cond_1
    invoke-interface {v4}, LX/0R1;->getNext()LX/0R1;

    move-result-object v4

    goto :goto_4

    .line 58324
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 58325
    :cond_3
    iget v4, v7, LX/0Qx;->modCount:I

    int-to-long v4, v4

    add-long/2addr v8, v4

    .line 58326
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    .line 58327
    :cond_4
    cmp-long v4, v8, v12

    if-eqz v4, :cond_5

    .line 58328
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v12, v8

    goto :goto_1

    .line 58329
    :cond_5
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 58233
    invoke-direct {p0}, LX/0Qd;->p()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Qd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 58234
    invoke-direct {p0}, LX/0Qd;->o()Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Not supported."
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 58235
    iget-object v0, p0, LX/0Qd;->y:Ljava/util/Set;

    .line 58236
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4wF;

    invoke-direct {v0, p0, p0}, LX/4wF;-><init>(LX/0Qd;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LX/0Qd;->y:Ljava/util/Set;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 58237
    invoke-direct {p0}, LX/0Qd;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Qd;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 58238
    invoke-direct {p0}, LX/0Qd;->p()Z

    move-result v0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58239
    if-nez p1, :cond_0

    .line 58240
    const/4 v0, 0x0

    .line 58241
    :goto_0
    return-object v0

    .line 58242
    :cond_0
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58243
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0Qx;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 58244
    iget-object v0, p0, LX/0Qd;->h:LX/0QX;

    sget-object v1, LX/0QX;->STRONG:LX/0QX;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 58245
    iget-object v0, p0, LX/0Qd;->i:LX/0QX;

    sget-object v1, LX/0QX;->STRONG:LX/0QX;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 58246
    iget-object v6, p0, LX/0Qd;->d:[LX/0Qx;

    move v0, v1

    move-wide v2, v4

    .line 58247
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 58248
    aget-object v7, v6, v0

    iget v7, v7, LX/0Qx;->count:I

    if-eqz v7, :cond_1

    .line 58249
    :cond_0
    :goto_1
    return v1

    .line 58250
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, LX/0Qx;->modCount:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 58251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58252
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 58253
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 58254
    aget-object v7, v6, v0

    iget v7, v7, LX/0Qx;->count:I

    if-nez v7, :cond_0

    .line 58255
    aget-object v7, v6, v0

    iget v7, v7, LX/0Qx;->modCount:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 58256
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 58257
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 58258
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 58259
    iget-object v0, p0, LX/0Qd;->w:Ljava/util/Set;

    .line 58260
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4wH;

    invoke-direct {v0, p0, p0}, LX/4wH;-><init>(LX/0Qd;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LX/0Qd;->w:Ljava/util/Set;

    goto :goto_0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 58261
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58262
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58263
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58264
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, LX/0Qx;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58265
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 58266
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LX/0Qd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 58267
    :cond_0
    return-void
.end method

.method public final putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 58268
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58269
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58270
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58271
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, LX/0Qx;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 58272
    if-nez p1, :cond_0

    .line 58273
    const/4 v0, 0x0

    .line 58274
    :goto_0
    return-object v0

    .line 58275
    :cond_0
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58276
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0Qx;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58277
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 58278
    :cond_0
    const/4 v0, 0x0

    .line 58279
    :goto_0
    return v0

    .line 58280
    :cond_1
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58281
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, LX/0Qx;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 58282
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58283
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58284
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58285
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, LX/0Qx;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 58286
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58287
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58288
    if-nez p2, :cond_0

    .line 58289
    const/4 v0, 0x0

    .line 58290
    :goto_0
    return v0

    .line 58291
    :cond_0
    invoke-static {p0, p1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v0

    .line 58292
    invoke-static {p0, v0}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, LX/0Qx;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final size()I
    .locals 10

    .prologue
    .line 58293
    const/4 v3, 0x0

    .line 58294
    iget-object v6, p0, LX/0Qd;->d:[LX/0Qx;

    .line 58295
    const-wide/16 v4, 0x0

    move v2, v3

    .line 58296
    :goto_0
    array-length v7, v6

    if-ge v2, v7, :cond_0

    .line 58297
    aget-object v7, v6, v2

    iget v7, v7, LX/0Qx;->count:I

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    int-to-long v8, v7

    add-long/2addr v4, v8

    .line 58298
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 58299
    :cond_0
    move-wide v0, v4

    .line 58300
    invoke-static {v0, v1}, LX/0a4;->b(J)I

    move-result v0

    return v0
.end method

.method public final values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 58301
    iget-object v0, p0, LX/0Qd;->x:Ljava/util/Collection;

    .line 58302
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4wT;

    invoke-direct {v0, p0, p0}, LX/4wT;-><init>(LX/0Qd;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LX/0Qd;->x:Ljava/util/Collection;

    goto :goto_0
.end method
