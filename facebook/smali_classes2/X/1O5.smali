.class public final LX/1O5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/RectF;

.field public final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/drawable/Drawable$Callback;

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:F

.field public j:[I

.field public k:I

.field public l:F

.field public m:F

.field public n:F

.field private o:Z

.field private p:Landroid/graphics/Path;

.field public q:F

.field public r:D

.field public s:I

.field public t:I

.field public u:I

.field public final v:Landroid/graphics/Paint;

.field public w:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable$Callback;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 238676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238677
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/1O5;->a:Landroid/graphics/RectF;

    .line 238678
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/1O5;->b:Landroid/graphics/Paint;

    .line 238679
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/1O5;->c:Landroid/graphics/Paint;

    .line 238680
    iput v1, p0, LX/1O5;->e:F

    .line 238681
    iput v1, p0, LX/1O5;->f:F

    .line 238682
    iput v1, p0, LX/1O5;->g:F

    .line 238683
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, LX/1O5;->h:F

    .line 238684
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, LX/1O5;->i:F

    .line 238685
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/1O5;->v:Landroid/graphics/Paint;

    .line 238686
    iput-object p1, p0, LX/1O5;->d:Landroid/graphics/drawable/Drawable$Callback;

    .line 238687
    iget-object v0, p0, LX/1O5;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 238688
    iget-object v0, p0, LX/1O5;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 238689
    iget-object v0, p0, LX/1O5;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 238690
    iget-object v0, p0, LX/1O5;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 238691
    iget-object v0, p0, LX/1O5;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 238692
    return-void
.end method

.method public static a(LX/1O5;Landroid/graphics/Canvas;FFLandroid/graphics/Rect;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 238659
    iget-boolean v0, p0, LX/1O5;->o:Z

    if-eqz v0, :cond_0

    .line 238660
    iget-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    if-nez v0, :cond_1

    .line 238661
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    .line 238662
    iget-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 238663
    :goto_0
    iget v0, p0, LX/1O5;->i:F

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, LX/1O5;->q:F

    mul-float/2addr v0, v1

    .line 238664
    iget-wide v2, p0, LX/1O5;->r:D

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    float-to-double v4, v1

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 238665
    iget-wide v2, p0, LX/1O5;->r:D

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v4

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v2, v2

    .line 238666
    iget-object v3, p0, LX/1O5;->p:Landroid/graphics/Path;

    invoke-virtual {v3, v6, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 238667
    iget-object v3, p0, LX/1O5;->p:Landroid/graphics/Path;

    iget v4, p0, LX/1O5;->s:I

    int-to-float v4, v4

    iget v5, p0, LX/1O5;->q:F

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238668
    iget-object v3, p0, LX/1O5;->p:Landroid/graphics/Path;

    iget v4, p0, LX/1O5;->s:I

    int-to-float v4, v4

    iget v5, p0, LX/1O5;->q:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, p0, LX/1O5;->t:I

    int-to-float v5, v5

    iget v6, p0, LX/1O5;->q:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238669
    iget-object v3, p0, LX/1O5;->p:Landroid/graphics/Path;

    sub-float v0, v1, v0

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Path;->offset(FF)V

    .line 238670
    iget-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 238671
    iget-object v0, p0, LX/1O5;->c:Landroid/graphics/Paint;

    iget-object v1, p0, LX/1O5;->j:[I

    iget v2, p0, LX/1O5;->k:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 238672
    add-float v0, p2, p3

    const/high16 v1, 0x40a00000    # 5.0f

    sub-float/2addr v0, v1

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 238673
    iget-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    iget-object v1, p0, LX/1O5;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 238674
    :cond_0
    return-void

    .line 238675
    :cond_1
    iget-object v0, p0, LX/1O5;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto :goto_0
.end method

.method public static l(LX/1O5;)V
    .locals 2

    .prologue
    .line 238657
    iget-object v0, p0, LX/1O5;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238658
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 1

    .prologue
    .line 238655
    iput-wide p1, p0, LX/1O5;->r:D

    .line 238656
    return-void
.end method

.method public final a(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 238649
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    .line 238650
    iget-wide v2, p0, LX/1O5;->r:D

    const-wide/16 v4, 0x0

    cmpg-double v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 238651
    :cond_0
    iget v0, p0, LX/1O5;->h:F

    div-float/2addr v0, v6

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 238652
    :goto_0
    iput v0, p0, LX/1O5;->i:F

    .line 238653
    return-void

    .line 238654
    :cond_1
    div-float/2addr v0, v6

    float-to-double v0, v0

    iget-wide v2, p0, LX/1O5;->r:D

    sub-double/2addr v0, v2

    double-to-float v0, v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 238693
    iget-boolean v0, p0, LX/1O5;->o:Z

    if-eq v0, p1, :cond_0

    .line 238694
    iput-boolean p1, p0, LX/1O5;->o:Z

    .line 238695
    invoke-static {p0}, LX/1O5;->l(LX/1O5;)V

    .line 238696
    :cond_0
    return-void
.end method

.method public final a([I)V
    .locals 1
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 238645
    iput-object p1, p0, LX/1O5;->j:[I

    .line 238646
    const/4 v0, 0x0

    .line 238647
    iput v0, p0, LX/1O5;->k:I

    .line 238648
    return-void
.end method

.method public final b(F)V
    .locals 0

    .prologue
    .line 238642
    iput p1, p0, LX/1O5;->e:F

    .line 238643
    invoke-static {p0}, LX/1O5;->l(LX/1O5;)V

    .line 238644
    return-void
.end method

.method public final c(F)V
    .locals 0

    .prologue
    .line 238639
    iput p1, p0, LX/1O5;->f:F

    .line 238640
    invoke-static {p0}, LX/1O5;->l(LX/1O5;)V

    .line 238641
    return-void
.end method

.method public final d(F)V
    .locals 0

    .prologue
    .line 238636
    iput p1, p0, LX/1O5;->g:F

    .line 238637
    invoke-static {p0}, LX/1O5;->l(LX/1O5;)V

    .line 238638
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 238632
    iget v0, p0, LX/1O5;->e:F

    iput v0, p0, LX/1O5;->l:F

    .line 238633
    iget v0, p0, LX/1O5;->f:F

    iput v0, p0, LX/1O5;->m:F

    .line 238634
    iget v0, p0, LX/1O5;->g:F

    iput v0, p0, LX/1O5;->n:F

    .line 238635
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 238625
    iput v0, p0, LX/1O5;->l:F

    .line 238626
    iput v0, p0, LX/1O5;->m:F

    .line 238627
    iput v0, p0, LX/1O5;->n:F

    .line 238628
    invoke-virtual {p0, v0}, LX/1O5;->b(F)V

    .line 238629
    invoke-virtual {p0, v0}, LX/1O5;->c(F)V

    .line 238630
    invoke-virtual {p0, v0}, LX/1O5;->d(F)V

    .line 238631
    return-void
.end method
