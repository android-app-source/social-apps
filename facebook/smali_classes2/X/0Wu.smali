.class public LX/0Wu;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76797
    invoke-direct {p0}, LX/0Q6;-><init>()V

    return-void
.end method

.method public static a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;)LX/2G5;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76798
    invoke-virtual {p0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    .line 76799
    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76800
    new-instance v0, LX/2G5;

    invoke-direct {v0, p1, p2}, LX/2G5;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;)V

    .line 76801
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;LX/0SG;LX/0Or;)LX/2WA;
    .locals 6
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/annotations/DeviceStatusReporterInterval;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/process/ProcessUtil;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Wt;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/2WA;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76802
    invoke-virtual {p0}, LX/0VT;->a()LX/00G;

    move-result-object v1

    .line 76803
    invoke-virtual {v1}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76804
    const/4 v0, 0x0

    .line 76805
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2WA;

    invoke-virtual {v1}, LX/00G;->c()Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/2WA;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;LX/0Wt;LX/0SG;LX/0Or;)V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 76806
    return-void
.end method
