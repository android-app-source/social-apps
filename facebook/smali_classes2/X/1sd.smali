.class public LX/1sd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/pm/PackageManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 335049
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 335050
    const-string v1, "android.permission.INSTALL_PACKAGES"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 335051
    const-string v1, "android.permission.DELETE_PACKAGES"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 335052
    const-string v1, "android.permission.CHANGE_COMPONENT_ENABLED_STATE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 335053
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, LX/1sd;->a:Ljava/util/Set;

    .line 335054
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 335055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335056
    iput-object p1, p0, LX/1sd;->b:Landroid/content/pm/PackageManager;

    .line 335057
    return-void
.end method

.method public static a(LX/1sd;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 335042
    :try_start_0
    iget-object v0, p0, LX/1sd;->b:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 335043
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_0

    .line 335044
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    neg-int v0, v0

    .line 335045
    :goto_0
    return v0

    .line 335046
    :cond_0
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 335047
    :catch_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/pm/PackageInfo;Landroid/content/pm/Signature;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 335038
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 335039
    :cond_0
    :goto_0
    return v0

    .line 335040
    :cond_1
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v0, v1, v0

    .line 335041
    invoke-virtual {p1, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 335048
    sget-object v0, LX/1ZP;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/1sd;->a(LX/1sd;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 335037
    invoke-virtual {p0}, LX/1sd;->d()I

    move-result v0

    if-lt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 335036
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1sd;->a(I)Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 334997
    :try_start_0
    iget-object v1, p0, LX/1sd;->b:Landroid/content/pm/PackageManager;

    sget-object v2, LX/1ZP;->a:Ljava/lang/String;

    const/16 v3, 0xc0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 334998
    :try_start_1
    iget-object v2, p0, LX/1sd;->b:Landroid/content/pm/PackageManager;

    sget-object v3, LX/1ZP;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 334999
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v3, :cond_0

    .line 335000
    const/16 v0, -0x66

    .line 335001
    :goto_0
    return v0

    .line 335002
    :catch_0
    const/16 v0, -0x64

    goto :goto_0

    .line 335003
    :catch_1
    const/16 v0, -0x65

    goto :goto_0

    .line 335004
    :cond_0
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v3, :cond_1

    .line 335005
    const/16 v0, -0x67

    goto :goto_0

    .line 335006
    :cond_1
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 335007
    sget-object v5, LX/1ZP;->n:Landroid/content/pm/Signature;

    invoke-static {v1, v5}, LX/1sd;->a(Landroid/content/pm/PackageInfo;Landroid/content/pm/Signature;)Z

    move-result v6

    .line 335008
    sget-object v5, LX/1ZP;->j:Landroid/content/pm/Signature;

    invoke-static {v1, v5}, LX/1sd;->a(Landroid/content/pm/PackageInfo;Landroid/content/pm/Signature;)Z

    move-result v7

    .line 335009
    sget-object v5, LX/1ZP;->n:Landroid/content/pm/Signature;

    invoke-static {v2, v5}, LX/1sd;->a(Landroid/content/pm/PackageInfo;Landroid/content/pm/Signature;)Z

    move-result v8

    .line 335010
    sget-object v5, LX/1ZP;->j:Landroid/content/pm/Signature;

    invoke-static {v2, v5}, LX/1sd;->a(Landroid/content/pm/PackageInfo;Landroid/content/pm/Signature;)Z

    move-result v9

    .line 335011
    if-nez v8, :cond_8

    if-nez v9, :cond_8

    move v5, v3

    .line 335012
    :goto_1
    if-eqz v6, :cond_9

    if-eqz v8, :cond_9

    .line 335013
    :cond_2
    :goto_2
    move v2, v3

    .line 335014
    if-nez v2, :cond_3

    .line 335015
    const/16 v0, -0x68

    goto :goto_0

    .line 335016
    :cond_3
    sget-object v2, LX/1ZP;->c:Ljava/lang/String;

    sget-object v3, LX/1sd;->a:Ljava/util/Set;

    .line 335017
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 335018
    iget-object v6, p0, LX/1sd;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v4, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 335019
    if-eqz v4, :cond_4

    .line 335020
    const/4 v4, 0x0

    .line 335021
    :goto_3
    move v2, v4

    .line 335022
    if-nez v2, :cond_5

    .line 335023
    const/16 v0, -0x69

    goto :goto_0

    .line 335024
    :cond_5
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const v3, 0x135b5e5

    if-lt v2, v3, :cond_6

    .line 335025
    const/4 v0, 0x1

    .line 335026
    :cond_6
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    .line 335027
    if-eqz v2, :cond_7

    .line 335028
    const-string v3, "com.facebook.appmanager.api.level"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 335029
    :cond_7
    move v0, v0

    .line 335030
    goto :goto_0

    :cond_8
    move v5, v4

    .line 335031
    goto :goto_1

    .line 335032
    :cond_9
    if-eqz v7, :cond_a

    if-nez v9, :cond_2

    .line 335033
    :cond_a
    if-eqz v5, :cond_b

    .line 335034
    if-nez v6, :cond_2

    if-nez v7, :cond_2

    move v3, v4

    goto :goto_2

    :cond_b
    move v3, v4

    .line 335035
    goto :goto_2

    :cond_c
    const/4 v4, 0x1

    goto :goto_3
.end method
