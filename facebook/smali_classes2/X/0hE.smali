.class public interface abstract LX/0hE;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(LX/394;)Ljava/lang/Object;
.end method

.method public abstract a(LX/395;)V
.end method

.method public abstract a()Z
.end method

.method public abstract b()Z
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()Z
.end method

.method public abstract setAllowLooping(Z)V
.end method

.method public abstract setLogEnteringStartEvent(Z)V
.end method

.method public abstract setLogExitingPauseEvent(Z)V
.end method

.method public abstract setNextStoryFinder(LX/0QK;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract setPreviousStoryFinder(LX/0QK;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract setVisibility(I)V
.end method

.method public abstract t_(I)V
.end method
