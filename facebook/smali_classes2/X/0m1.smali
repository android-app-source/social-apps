.class public LX/0m1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public _rootNames:LX/0lj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0lj",
            "<",
            "LX/1Xc;",
            "LX/0lb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lJ;LX/0m4;)LX/0lb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/0m4",
            "<*>;)",
            "LX/0lb;"
        }
    .end annotation

    .prologue
    .line 131539
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131540
    invoke-virtual {p0, v0, p2}, LX/0m1;->a(Ljava/lang/Class;LX/0m4;)LX/0lb;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Class;LX/0m4;)LX/0lb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0m4",
            "<*>;)",
            "LX/0lb;"
        }
    .end annotation

    .prologue
    .line 131541
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/1Xc;

    invoke-direct {v2, p1}, LX/1Xc;-><init>(Ljava/lang/Class;)V

    .line 131542
    iget-object v0, p0, LX/0m1;->_rootNames:LX/0lj;

    if-nez v0, :cond_2

    .line 131543
    new-instance v0, LX/0lj;

    const/16 v1, 0x14

    const/16 v3, 0xc8

    invoke-direct {v0, v1, v3}, LX/0lj;-><init>(II)V

    iput-object v0, p0, LX/0m1;->_rootNames:LX/0lj;

    .line 131544
    :cond_0
    invoke-virtual {p2, p1}, LX/0m4;->c(Ljava/lang/Class;)LX/0lS;

    move-result-object v0

    .line 131545
    invoke-virtual {p2}, LX/0m4;->a()LX/0lU;

    move-result-object v1

    .line 131546
    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    .line 131547
    invoke-virtual {v1, v0}, LX/0lU;->a(LX/0lN;)LX/2Vb;

    move-result-object v0

    .line 131548
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/2Vb;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 131549
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 131550
    :goto_0
    new-instance v0, LX/0lb;

    invoke-direct {v0, v1}, LX/0lb;-><init>(Ljava/lang/String;)V

    .line 131551
    iget-object v1, p0, LX/0m1;->_rootNames:LX/0lj;

    invoke-virtual {v1, v2, v0}, LX/0lj;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131552
    :goto_1
    monitor-exit p0

    return-object v0

    .line 131553
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/0m1;->_rootNames:LX/0lj;

    invoke-virtual {v0, v2}, LX/0lj;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lb;

    .line 131554
    if-eqz v0, :cond_0

    goto :goto_1

    .line 131555
    :cond_3
    iget-object v1, v0, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131556
    move-object v1, v0

    goto :goto_0

    .line 131557
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
