.class public final enum LX/131;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/131;",
        ">;",
        "Ljava/util/concurrent/Executor;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/131;

.field public static final enum INSTANCE:LX/131;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 175968
    new-instance v0, LX/131;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/131;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/131;->INSTANCE:LX/131;

    .line 175969
    const/4 v0, 0x1

    new-array v0, v0, [LX/131;

    sget-object v1, LX/131;->INSTANCE:LX/131;

    aput-object v1, v0, v2

    sput-object v0, LX/131;->$VALUES:[LX/131;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 175967
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/131;
    .locals 1

    .prologue
    .line 175966
    const-class v0, LX/131;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/131;

    return-object v0
.end method

.method public static values()[LX/131;
    .locals 1

    .prologue
    .line 175965
    sget-object v0, LX/131;->$VALUES:[LX/131;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/131;

    return-object v0
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 175962
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 175963
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175964
    const-string v0, "MoreExecutors.directExecutor()"

    return-object v0
.end method
