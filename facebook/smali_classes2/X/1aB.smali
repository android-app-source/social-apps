.class public final LX/1aB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Ra",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Z

.field public final synthetic c:LX/1RZ;

.field public final synthetic d:LX/1RA;


# direct methods
.method public constructor <init>(LX/1RA;IZLX/1RZ;)V
    .locals 0

    .prologue
    .line 276556
    iput-object p1, p0, LX/1aB;->d:LX/1RA;

    iput p2, p0, LX/1aB;->a:I

    iput-boolean p3, p0, LX/1aB;->b:Z

    iput-object p4, p0, LX/1aB;->c:LX/1RZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 276557
    const/4 v5, 0x0

    .line 276558
    iget v0, p0, LX/1aB;->a:I

    if-lez v0, :cond_1

    iget-object v0, p0, LX/1aB;->d:LX/1RA;

    iget-object v0, v0, LX/1RA;->k:LX/0Px;

    iget v1, p0, LX/1aB;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    move-object v4, v0

    .line 276559
    :goto_0
    iget-object v0, p0, LX/1aB;->d:LX/1RA;

    iget-object v0, v0, LX/1RA;->k:LX/0Px;

    iget v1, p0, LX/1aB;->a:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1RZ;

    .line 276560
    iget v0, p0, LX/1aB;->a:I

    iget-object v1, p0, LX/1aB;->d:LX/1RA;

    iget-object v1, v1, LX/1RA;->k:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/1aB;->d:LX/1RA;

    iget-object v0, v0, LX/1RA;->k:LX/0Px;

    iget v1, p0, LX/1aB;->a:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    move-object v7, v0

    .line 276561
    :goto_1
    iget-object v0, p0, LX/1aB;->d:LX/1RA;

    iget-object v0, v0, LX/1RA;->d:LX/1PW;

    if-eqz v4, :cond_3

    .line 276562
    iget-object v1, v4, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v1, v1

    .line 276563
    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 276564
    :goto_2
    iget-object v2, v6, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v2, v2

    .line 276565
    check-cast v2, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v7, :cond_4

    .line 276566
    iget-object v3, v7, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v3, v3

    .line 276567
    check-cast v3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    :goto_3
    if-eqz v4, :cond_5

    .line 276568
    iget-object v8, v4, LX/1RZ;->d:Ljava/lang/Object;

    move-object v4, v8

    .line 276569
    :goto_4
    if-eqz v7, :cond_0

    .line 276570
    iget-object v5, v7, LX/1RZ;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 276571
    :cond_0
    iget-object v7, v6, LX/1RZ;->c:LX/1Rb;

    move-object v6, v7

    .line 276572
    iget-boolean v7, p0, LX/1aB;->b:Z

    invoke-static/range {v0 .. v7}, LX/1Wz;->a(LX/1PW;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)LX/1Q9;

    move-result-object v0

    .line 276573
    iget-object v1, p0, LX/1aB;->c:LX/1RZ;

    iget-object v2, p0, LX/1aB;->d:LX/1RA;

    iget-object v2, v2, LX/1RA;->d:LX/1PW;

    invoke-virtual {v1, v2}, LX/1Ra;->a(LX/1PW;)V

    .line 276574
    iget-object v1, p0, LX/1aB;->d:LX/1RA;

    iget-object v1, v1, LX/1RA;->d:LX/1PW;

    invoke-static {v0, v1}, LX/1Wz;->a(LX/1Q9;LX/1PW;)V

    .line 276575
    iget-object v0, p0, LX/1aB;->c:LX/1RZ;

    return-object v0

    :cond_1
    move-object v4, v5

    .line 276576
    goto :goto_0

    :cond_2
    move-object v7, v5

    .line 276577
    goto :goto_1

    :cond_3
    move-object v1, v5

    .line 276578
    goto :goto_2

    :cond_4
    move-object v3, v5

    goto :goto_3

    :cond_5
    move-object v4, v5

    goto :goto_4
.end method
