.class public LX/0vn;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:LX/0vr;

.field private static final c:Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 158133
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 158134
    new-instance v0, LX/0vo;

    invoke-direct {v0}, LX/0vo;-><init>()V

    sput-object v0, LX/0vn;->b:LX/0vr;

    .line 158135
    :goto_0
    sget-object v0, LX/0vn;->b:LX/0vr;

    invoke-interface {v0}, LX/0vr;->a()Ljava/lang/Object;

    move-result-object v0

    sput-object v0, LX/0vn;->c:Ljava/lang/Object;

    .line 158136
    return-void

    .line 158137
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 158138
    new-instance v0, LX/0vp;

    invoke-direct {v0}, LX/0vp;-><init>()V

    sput-object v0, LX/0vn;->b:LX/0vr;

    goto :goto_0

    .line 158139
    :cond_1
    new-instance v0, LX/0vq;

    invoke-direct {v0}, LX/0vq;-><init>()V

    sput-object v0, LX/0vn;->b:LX/0vr;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 158130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158131
    sget-object v0, LX/0vn;->b:LX/0vr;

    invoke-interface {v0, p0}, LX/0vr;->a(LX/0vn;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0vn;->a:Ljava/lang/Object;

    .line 158132
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)LX/3t3;
    .locals 2

    .prologue
    .line 158129
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/View;)LX/3t3;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 158127
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 158128
    return-void
.end method

.method public a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    .line 158125
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/View;LX/3sp;)V

    .line 158126
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 158123
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158124
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 158122
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 158121
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 158116
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 158119
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158120
    return-void
.end method

.method public d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 158117
    sget-object v0, LX/0vn;->b:LX/0vr;

    sget-object v1, LX/0vn;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vr;->b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158118
    return-void
.end method
