.class public final LX/0zg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "LX/0zg;",
            "LX/0vR;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0zh;


# instance fields
.field public volatile a:LX/0vR;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 167537
    const-class v0, LX/0zg;

    const-class v1, LX/0vR;

    const-string v2, "a"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, LX/0zg;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 167538
    new-instance v0, LX/0zh;

    invoke-direct {v0}, LX/0zh;-><init>()V

    sput-object v0, LX/0zg;->c:LX/0zh;

    return-void
.end method

.method public constructor <init>(LX/0vR;)V
    .locals 0

    .prologue
    .line 167539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167540
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, LX/0zg;->a:LX/0vR;

    .line 167541
    return-void

    .line 167542
    :cond_0
    sget-object p1, LX/0vP;->a:LX/0vQ;

    move-object p1, p1

    .line 167543
    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 167544
    sget-object v0, LX/0zg;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    sget-object v1, LX/0zg;->c:LX/0zh;

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->getAndSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vR;

    .line 167545
    invoke-interface {v0}, LX/0vR;->a()V

    .line 167546
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 167547
    iget-object v0, p0, LX/0zg;->a:LX/0vR;

    sget-object v1, LX/0zg;->c:LX/0zh;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
