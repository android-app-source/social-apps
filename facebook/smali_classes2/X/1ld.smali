.class public final LX/1ld;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1lW;

.field public static final b:LX/1lW;

.field public static final c:LX/1lW;

.field public static final d:LX/1lW;

.field public static final e:LX/1lW;

.field public static final f:LX/1lW;

.field public static final g:LX/1lW;

.field public static final h:LX/1lW;

.field public static final i:LX/1lW;

.field public static j:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "LX/1lW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 312196
    new-instance v0, LX/1lW;

    const-string v1, "JPEG"

    const-string v2, "jpeg"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->a:LX/1lW;

    .line 312197
    new-instance v0, LX/1lW;

    const-string v1, "PNG"

    const-string v2, "png"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->b:LX/1lW;

    .line 312198
    new-instance v0, LX/1lW;

    const-string v1, "GIF"

    const-string v2, "gif"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->c:LX/1lW;

    .line 312199
    new-instance v0, LX/1lW;

    const-string v1, "BMP"

    const-string v2, "bmp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->d:LX/1lW;

    .line 312200
    new-instance v0, LX/1lW;

    const-string v1, "WEBP_SIMPLE"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->e:LX/1lW;

    .line 312201
    new-instance v0, LX/1lW;

    const-string v1, "WEBP_LOSSLESS"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->f:LX/1lW;

    .line 312202
    new-instance v0, LX/1lW;

    const-string v1, "WEBP_EXTENDED"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->g:LX/1lW;

    .line 312203
    new-instance v0, LX/1lW;

    const-string v1, "WEBP_EXTENDED_WITH_ALPHA"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->h:LX/1lW;

    .line 312204
    new-instance v0, LX/1lW;

    const-string v1, "WEBP_ANIMATED"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1ld;->i:LX/1lW;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 312205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312206
    return-void
.end method

.method public static a(LX/1lW;)Z
    .locals 1

    .prologue
    .line 312207
    invoke-static {p0}, LX/1ld;->b(LX/1lW;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1ld;->i:LX/1lW;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1lW;)Z
    .locals 1

    .prologue
    .line 312208
    sget-object v0, LX/1ld;->e:LX/1lW;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1ld;->f:LX/1lW;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1ld;->g:LX/1lW;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1ld;->h:LX/1lW;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
