.class public LX/1FC;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1FG;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1FG;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221715
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1FG;
    .locals 5

    .prologue
    .line 221716
    sget-object v0, LX/1FC;->a:LX/1FG;

    if-nez v0, :cond_1

    .line 221717
    const-class v1, LX/1FC;

    monitor-enter v1

    .line 221718
    :try_start_0
    sget-object v0, LX/1FC;->a:LX/1FG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221719
    if-eqz v2, :cond_0

    .line 221720
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221721
    invoke-static {v0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v3

    check-cast v3, LX/1FB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {v3, v4, p0}, LX/1Aq;->a(LX/1FB;LX/0ad;Landroid/content/Context;)LX/1FG;

    move-result-object v3

    move-object v0, v3

    .line 221722
    sput-object v0, LX/1FC;->a:LX/1FG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221723
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221724
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221725
    :cond_1
    sget-object v0, LX/1FC;->a:LX/1FG;

    return-object v0

    .line 221726
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 221728
    invoke-static {p0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v0

    check-cast v0, LX/1FB;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v0, v1, v2}, LX/1Aq;->a(LX/1FB;LX/0ad;Landroid/content/Context;)LX/1FG;

    move-result-object v0

    return-object v0
.end method
