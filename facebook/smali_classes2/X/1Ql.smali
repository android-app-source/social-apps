.class public final LX/1Ql;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1DS;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;"
        }
    .end annotation
.end field

.field private c:LX/0g1;

.field private d:LX/99b;

.field public e:LX/1DZ;

.field public f:LX/1PW;

.field private g:Z

.field public h:LX/0g8;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:LX/1Jp;


# direct methods
.method public constructor <init>(LX/1DS;LX/0Ot;LX/0g1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;",
            "LX/0g1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245163
    iput-object p1, p0, LX/1Ql;->a:LX/1DS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245164
    iput-object p2, p0, LX/1Ql;->b:LX/0Ot;

    .line 245165
    iput-object p3, p0, LX/1Ql;->c:LX/0g1;

    .line 245166
    sget-object v0, LX/1Qm;->a:LX/1PW;

    iput-object v0, p0, LX/1Ql;->f:LX/1PW;

    .line 245167
    return-void
.end method

.method private a(LX/0g1;LX/1R0;)LX/1R2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1;",
            "LX/1R0",
            "<*TE;>;)",
            "LX/1R2;"
        }
    .end annotation

    .prologue
    .line 245155
    instance-of v0, p1, LX/0g0;

    if-eqz v0, :cond_0

    .line 245156
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->f:LX/1DT;

    iget-object v1, p0, LX/1Ql;->f:LX/1PW;

    check-cast p1, LX/0g0;

    .line 245157
    new-instance p0, LX/1Ru;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {p0, p2, v1, p1, v2}, LX/1Ru;-><init>(LX/1R0;LX/1PW;LX/0g0;LX/03V;)V

    .line 245158
    move-object v0, p0

    .line 245159
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/1Ql;->f:LX/1PW;

    iget-object v0, p0, LX/1Ql;->e:LX/1DZ;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Ql;->e:LX/1DZ;

    .line 245160
    :goto_1
    new-instance v2, LX/1R1;

    invoke-direct {v2, p2, p1, v1, v0}, LX/1R1;-><init>(LX/1R0;LX/0g1;LX/1PW;LX/1DZ;)V

    .line 245161
    move-object v0, v2

    .line 245162
    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->k:LX/1DY;

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Ql;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g8;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245140
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Ql;->g:Z

    .line 245141
    iput-object p1, p0, LX/1Ql;->h:LX/0g8;

    .line 245142
    return-object p0
.end method

.method public final a(LX/1DZ;)LX/1Ql;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DZ;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245153
    iput-object p1, p0, LX/1Ql;->e:LX/1DZ;

    .line 245154
    return-object p0
.end method

.method public final a(LX/1PW;)LX/1Ql;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245151
    iput-object p1, p0, LX/1Ql;->f:LX/1PW;

    .line 245152
    return-object p0
.end method

.method public final a(LX/99g;)LX/1Ql;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/99g;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245149
    new-instance v0, LX/99b;

    iget-object v1, p0, LX/1Ql;->c:LX/0g1;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, LX/99b;-><init>(LX/0g1;LX/99g;Z)V

    iput-object v0, p0, LX/1Ql;->d:LX/99b;

    .line 245150
    return-object p0
.end method

.method public final b(LX/0g8;)LX/1Ql;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g8;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 245145
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->d:LX/0Uh;

    const/16 v1, 0x5d7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245146
    :goto_0
    return-object p0

    .line 245147
    :cond_0
    iput-object p1, p0, LX/1Ql;->h:LX/0g8;

    .line 245148
    iput-boolean v2, p0, LX/1Ql;->m:Z

    goto :goto_0
.end method

.method public final b(LX/99g;)LX/1Ql;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/99g;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245143
    new-instance v0, LX/99b;

    iget-object v1, p0, LX/1Ql;->c:LX/0g1;

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, v2}, LX/99b;-><init>(LX/0g1;LX/99g;Z)V

    iput-object v0, p0, LX/1Ql;->d:LX/99b;

    .line 245144
    return-object p0
.end method

.method public final d()LX/1Rq;
    .locals 11

    .prologue
    .line 245086
    iget-object v0, p0, LX/1Ql;->d:LX/99b;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1Ql;->d:LX/99b;

    move-object v2, v0

    .line 245087
    :goto_0
    const/4 v1, 0x0

    .line 245088
    iget-object v0, p0, LX/1Ql;->f:LX/1PW;

    instance-of v0, v0, LX/1Po;

    if-eqz v0, :cond_7

    .line 245089
    iget-object v0, p0, LX/1Ql;->f:LX/1PW;

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    .line 245090
    if-eqz v0, :cond_7

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v3, LX/1Qt;->FEED:LX/1Qt;

    if-ne v0, v3, :cond_7

    .line 245091
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->h:LX/0pJ;

    invoke-virtual {v0, v1}, LX/0pJ;->b(Z)Z

    move-result v0

    .line 245092
    :goto_1
    move v0, v0

    .line 245093
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ji;

    iget-object v3, p0, LX/1Ql;->b:LX/0Ot;

    iget-object v1, p0, LX/1Ql;->a:LX/1DS;

    iget-object v1, v1, LX/1DS;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Qx;

    iget-object v4, p0, LX/1Ql;->f:LX/1PW;

    .line 245094
    new-instance v5, LX/1kA;

    iget-object v6, v0, LX/1ji;->a:LX/1Da;

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v1, v7}, LX/1Da;->a(LX/0Ot;LX/1Qx;Z)LX/1R0;

    move-result-object v6

    iget-object v7, v0, LX/1ji;->b:LX/0Zb;

    invoke-direct {v5, v6, v4, v7}, LX/1kA;-><init>(LX/1R0;LX/1PW;LX/0Zb;)V

    .line 245095
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v0, LX/1ji;->c:Ljava/lang/ref/WeakReference;

    .line 245096
    move-object v0, v5

    .line 245097
    :goto_2
    invoke-direct {p0, v2, v0}, LX/1Ql;->a(LX/0g1;LX/1R0;)LX/1R2;

    move-result-object v2

    .line 245098
    iget-boolean v0, p0, LX/1Ql;->m:Z

    if-eqz v0, :cond_8

    .line 245099
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->b:LX/1DW;

    iget-object v1, p0, LX/1Ql;->h:LX/0g8;

    .line 245100
    new-instance v4, LX/1T1;

    invoke-static {v0}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v3

    check-cast v3, LX/0Wd;

    invoke-direct {v4, v2, v1, v3}, LX/1T1;-><init>(LX/1R2;LX/0g8;LX/0Wd;)V

    .line 245101
    move-object v0, v4

    .line 245102
    new-instance v1, LX/1T3;

    invoke-direct {v1, v0}, LX/1T3;-><init>(LX/1T1;)V

    .line 245103
    move-object v1, v1

    .line 245104
    iget-object v3, p0, LX/1Ql;->h:LX/0g8;

    invoke-interface {v3, v1}, LX/0g8;->b(LX/0fx;)V

    .line 245105
    :goto_3
    move-object v0, v0

    .line 245106
    invoke-interface {v2}, LX/1R2;->e()V

    .line 245107
    invoke-interface {v0}, LX/1R9;->a()V

    .line 245108
    iget-object v1, p0, LX/1Ql;->a:LX/1DS;

    iget-object v1, v1, LX/1DS;->l:LX/1Du;

    .line 245109
    iget-object v3, p0, LX/1Ql;->a:LX/1DS;

    iget-object v3, v3, LX/1DS;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/1Rn;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Z

    move-result v3

    move v3, v3

    .line 245110
    new-instance v5, LX/1Rp;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v1}, LX/1KB;->a(LX/0QB;)LX/1KB;

    move-result-object v10

    check-cast v10, LX/1KB;

    move-object v6, v2

    move-object v7, v0

    move v8, v3

    invoke-direct/range {v5 .. v10}, LX/1Rp;-><init>(LX/1R2;LX/1R9;ZLX/03V;LX/1KB;)V

    .line 245111
    move-object v1, v5

    .line 245112
    iget-boolean v0, p0, LX/1Ql;->g:Z

    if-eqz v0, :cond_6

    .line 245113
    iget-object v0, p0, LX/1Ql;->h:LX/0g8;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 245114
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->m:LX/1Dv;

    iget-object v3, p0, LX/1Ql;->h:LX/0g8;

    .line 245115
    new-instance v5, LX/1UD;

    const-class v4, LX/1UH;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1UH;

    invoke-direct {v5, v1, v3, v4}, LX/1UD;-><init>(LX/1Rq;LX/0g8;LX/1UH;)V

    .line 245116
    move-object v0, v5

    .line 245117
    :goto_5
    iget-boolean v1, p0, LX/1Ql;->i:Z

    if-eqz v1, :cond_0

    .line 245118
    iget-object v1, p0, LX/1Ql;->a:LX/1DS;

    iget-object v1, v1, LX/1DS;->n:LX/1Dw;

    .line 245119
    new-instance v4, LX/1UK;

    invoke-static {v1}, LX/1Dc;->a(LX/0QB;)LX/1Dc;

    move-result-object v3

    check-cast v3, LX/1Dc;

    invoke-direct {v4, v0, v3}, LX/1UK;-><init>(LX/1Rq;LX/1Dc;)V

    .line 245120
    move-object v0, v4

    .line 245121
    :cond_0
    iget-boolean v1, p0, LX/1Ql;->j:Z

    if-eqz v1, :cond_1

    .line 245122
    iget-object v1, p0, LX/1Ql;->a:LX/1DS;

    iget-object v1, v1, LX/1DS;->o:LX/1Dx;

    .line 245123
    new-instance v4, LX/1UL;

    invoke-static {v1}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v3

    check-cast v3, LX/198;

    invoke-direct {v4, v0, v3}, LX/1UL;-><init>(LX/1Rq;LX/198;)V

    .line 245124
    move-object v0, v4

    .line 245125
    :cond_1
    iget-boolean v1, p0, LX/1Ql;->k:Z

    if-eqz v1, :cond_2

    .line 245126
    instance-of v1, v2, LX/1Ru;

    .line 245127
    iget-object v2, p0, LX/1Ql;->h:LX/0g8;

    .line 245128
    new-instance v3, LX/62M;

    invoke-direct {v3, v2}, LX/62M;-><init>(LX/0g8;)V

    move-object v3, v3

    .line 245129
    iget-object v4, v3, LX/62M;->c:LX/0fx;

    move-object v4, v4

    .line 245130
    invoke-interface {v2, v4}, LX/0g8;->b(LX/0fx;)V

    .line 245131
    move-object v2, v3

    .line 245132
    iget-object v3, p0, LX/1Ql;->n:LX/1Jp;

    invoke-virtual {v3, v2, v0, v1}, LX/1Jp;->a(LX/62M;LX/1Rq;Z)V

    .line 245133
    :cond_2
    return-object v0

    .line 245134
    :cond_3
    iget-object v0, p0, LX/1Ql;->c:LX/0g1;

    move-object v2, v0

    goto/16 :goto_0

    .line 245135
    :cond_4
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v1, v0, LX/1DS;->i:LX/1Da;

    iget-object v3, p0, LX/1Ql;->b:LX/0Ot;

    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qx;

    iget-boolean v4, p0, LX/1Ql;->l:Z

    invoke-virtual {v1, v3, v0, v4}, LX/1Da;->a(LX/0Ot;LX/1Qx;Z)LX/1R0;

    move-result-object v0

    goto/16 :goto_2

    .line 245136
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, LX/1Ql;->a:LX/1DS;

    iget-object v0, v0, LX/1DS;->a:LX/1DV;

    .line 245137
    new-instance v3, LX/1R8;

    invoke-static {v0}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v1

    check-cast v1, LX/0Wd;

    invoke-direct {v3, v2, v1}, LX/1R8;-><init>(LX/1R2;LX/0Wd;)V

    .line 245138
    move-object v0, v3

    .line 245139
    goto/16 :goto_3
.end method

.method public final e()LX/1Qq;
    .locals 2

    .prologue
    .line 245085
    new-instance v0, LX/1Qo;

    invoke-virtual {p0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Qo;-><init>(LX/1Rq;)V

    return-object v0
.end method
