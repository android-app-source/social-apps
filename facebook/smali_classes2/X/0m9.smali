.class public final LX/0m9;
.super LX/0mA;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0mA",
        "<",
        "LX/0m9;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0mC;)V
    .locals 1

    .prologue
    .line 132029
    invoke-direct {p0, p1}, LX/0mA;-><init>(LX/0mC;)V

    .line 132030
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    .line 132031
    return-void
.end method


# virtual methods
.method public final G()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132003
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final H()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 132004
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final I()LX/0m9;
    .locals 5

    .prologue
    .line 132005
    new-instance v1, LX/0m9;

    iget-object v0, p0, LX/0mA;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 132006
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132007
    iget-object v3, v1, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->d()LX/0lF;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 132008
    :cond_0
    return-object v1
.end method

.method public final a(I)LX/0lF;
    .locals 1

    .prologue
    .line 132009
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 132010
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0lF;)LX/0lF;
    .locals 1

    .prologue
    .line 132011
    if-nez p2, :cond_0

    .line 132012
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object p2

    .line 132013
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132014
    return-object p0
.end method

.method public final a(Ljava/lang/String;D)LX/0m9;
    .locals 2

    .prologue
    .line 132015
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2, p3}, LX/0mC;->a(D)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132016
    return-object p0
.end method

.method public final a(Ljava/lang/String;F)LX/0m9;
    .locals 2

    .prologue
    .line 132017
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2}, LX/0mC;->a(F)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132018
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)LX/0m9;
    .locals 2

    .prologue
    .line 132048
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2}, LX/0mC;->a(I)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132049
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)LX/0m9;
    .locals 2

    .prologue
    .line 132019
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2, p3}, LX/0mC;->a(J)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132020
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0m9;
    .locals 2

    .prologue
    .line 132044
    if-nez p2, :cond_0

    .line 132045
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132046
    :goto_0
    return-object p0

    .line 132047
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, LX/0mC;->a(Z)LX/1Xb;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Double;)LX/0m9;
    .locals 4

    .prologue
    .line 132040
    if-nez p2, :cond_0

    .line 132041
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132042
    :goto_0
    return-object p0

    .line 132043
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, LX/0mC;->a(D)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;
    .locals 2

    .prologue
    .line 132036
    if-nez p2, :cond_0

    .line 132037
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132038
    :goto_0
    return-object p0

    .line 132039
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v1}, LX/0mC;->a(F)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;
    .locals 2

    .prologue
    .line 132032
    if-nez p2, :cond_0

    .line 132033
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132034
    :goto_0
    return-object p0

    .line 132035
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, LX/0mC;->a(I)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;
    .locals 4

    .prologue
    .line 132025
    if-nez p2, :cond_0

    .line 132026
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132027
    :goto_0
    return-object p0

    .line 132028
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/0mC;->a(J)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Short;)LX/0m9;
    .locals 2

    .prologue
    .line 132021
    if-nez p2, :cond_0

    .line 132022
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132023
    :goto_0
    return-object p0

    .line 132024
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-static {v1}, LX/0mC;->a(S)LX/0rP;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    .locals 2

    .prologue
    .line 131997
    if-nez p2, :cond_0

    .line 131998
    invoke-virtual {p0, p1}, LX/0m9;->k(Ljava/lang/String;)LX/0m9;

    .line 131999
    :goto_0
    return-object p0

    .line 132000
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)LX/0m9;
    .locals 2

    .prologue
    .line 132001
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2}, LX/0mC;->a(Z)LX/1Xb;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132002
    return-object p0
.end method

.method public final a(Ljava/lang/String;[B)LX/0m9;
    .locals 2

    .prologue
    .line 131930
    if-nez p2, :cond_0

    .line 131931
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131932
    :goto_0
    return-object p0

    .line 131933
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {p2}, LX/0mC;->a([B)LX/4rH;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 131934
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 131935
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 131936
    if-eqz v0, :cond_0

    .line 131937
    :goto_0
    return-object v0

    .line 131938
    :cond_0
    sget-object v0, LX/2AT;->a:LX/2AT;

    move-object v0, v0

    .line 131939
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;LX/0lF;)LX/0lF;
    .locals 1

    .prologue
    .line 131940
    if-nez p2, :cond_0

    .line 131941
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object p2

    .line 131942
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public final c(Ljava/lang/String;LX/0lF;)LX/0lF;
    .locals 1

    .prologue
    .line 131994
    if-nez p2, :cond_0

    .line 131995
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object p2

    .line 131996
    :cond_0
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public final synthetic d()LX/0lF;
    .locals 1

    .prologue
    .line 131943
    invoke-virtual {p0}, LX/0m9;->I()LX/0m9;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 131944
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)LX/0lF;
    .locals 3

    .prologue
    .line 131945
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131946
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131947
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 131948
    :goto_0
    return-object v0

    .line 131949
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0, p1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 131950
    if-eqz v0, :cond_0

    goto :goto_0

    .line 131951
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 131952
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 131953
    :cond_0
    :goto_0
    return v0

    .line 131954
    :cond_1
    if-eqz p1, :cond_0

    .line 131955
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 131956
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    check-cast p1, LX/0m9;

    iget-object v1, p1, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 131957
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 131958
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;)LX/162;
    .locals 2

    .prologue
    .line 131959
    invoke-virtual {p0}, LX/0mA;->K()LX/162;

    move-result-object v0

    .line 131960
    iget-object v1, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131961
    return-object v0
.end method

.method public final j(Ljava/lang/String;)LX/0m9;
    .locals 2

    .prologue
    .line 131962
    invoke-virtual {p0}, LX/0mA;->L()LX/0m9;

    move-result-object v0

    .line 131963
    iget-object v1, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131964
    return-object v0
.end method

.method public final j()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131965
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)LX/0m9;
    .locals 2

    .prologue
    .line 131966
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131967
    return-object p0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 131968
    sget-object v0, LX/0nH;->OBJECT:LX/0nH;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 131969
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 131970
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131971
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 131972
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mB;

    invoke-virtual {v0, p1, p2}, LX/0mB;->serialize(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 131973
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 131974
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 3

    .prologue
    .line 131975
    invoke-virtual {p3, p0, p1}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 131976
    iget-object v0, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131977
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 131978
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mB;

    invoke-virtual {v0, p1, p2}, LX/0mB;->serialize(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 131979
    :cond_0
    invoke-virtual {p3, p0, p1}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    .line 131980
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 131981
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x20

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 131982
    const-string v0, "{"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131983
    const/4 v0, 0x0

    .line 131984
    iget-object v1, p0, LX/0m9;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131985
    if-lez v1, :cond_0

    .line 131986
    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131987
    :cond_0
    add-int/lit8 v2, v1, 0x1

    .line 131988
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v3, v1}, LX/0mD;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 131989
    const/16 v1, 0x3a

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 131990
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 131991
    goto :goto_0

    .line 131992
    :cond_1
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131993
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
