.class public LX/0yO;
.super LX/0yP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0yO;


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yi;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yN;LX/0Uh;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/IsZeroRatingAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0yN;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164674
    invoke-direct/range {p0 .. p5}, LX/0yP;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yN;)V

    .line 164675
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0yO;->a:Ljava/util/Set;

    .line 164676
    iput-object p6, p0, LX/0yO;->b:LX/0Uh;

    .line 164677
    return-void
.end method

.method public static b(LX/0QB;)LX/0yO;
    .locals 10

    .prologue
    .line 164678
    sget-object v0, LX/0yO;->c:LX/0yO;

    if-nez v0, :cond_1

    .line 164679
    const-class v1, LX/0yO;

    monitor-enter v1

    .line 164680
    :try_start_0
    sget-object v0, LX/0yO;->c:LX/0yO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164681
    if-eqz v2, :cond_0

    .line 164682
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164683
    new-instance v3, LX/0yO;

    const/16 v4, 0x2e3

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x13ec

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13f1

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x387

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0yM;->a(LX/0QB;)LX/0yM;

    move-result-object v8

    check-cast v8, LX/0yN;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, LX/0yO;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yN;LX/0Uh;)V

    .line 164684
    move-object v0, v3

    .line 164685
    sput-object v0, LX/0yO;->c:LX/0yO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164686
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164687
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164688
    :cond_1
    sget-object v0, LX/0yO;->c:LX/0yO;

    return-object v0

    .line 164689
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164690
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 3

    .prologue
    .line 164691
    invoke-super {p0, p1, p2}, LX/0yP;->a(Ljava/lang/Throwable;LX/0yi;)V

    .line 164692
    iget-object v0, p0, LX/0yO;->b:LX/0Uh;

    const/16 v1, 0x2f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164693
    :goto_0
    return-void

    .line 164694
    :cond_0
    iget-object v0, p0, LX/0yO;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164695
    sget-object v0, LX/32P;->TOKEN_FETCH_FAILED_RETRY:LX/32P;

    invoke-virtual {p0, p2, v0}, LX/0yP;->a(LX/0yi;LX/32P;)V

    goto :goto_0

    .line 164696
    :cond_1
    iget-object v0, p0, LX/0yO;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 3

    .prologue
    .line 164697
    invoke-super {p0, p1, p2}, LX/0yP;->b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 164698
    iget-object v0, p0, LX/0yO;->b:LX/0Uh;

    const/16 v1, 0x2f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164699
    :goto_0
    return-void

    .line 164700
    :cond_0
    iget-object v0, p0, LX/0yO;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
