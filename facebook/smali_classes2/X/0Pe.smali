.class public LX/0Pe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:I

.field private b:Landroid/os/HandlerThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public c:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Landroid/content/Context;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final f:Ljava/util/Random;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57003
    const-string v0, "loom"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 57004
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56959
    const/16 v0, 0x11

    iput v0, p0, LX/0Pe;->a:I

    .line 56960
    iput-object p1, p0, LX/0Pe;->e:Landroid/content/Context;

    .line 56961
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/0Pe;->f:Ljava/util/Random;

    .line 56962
    new-instance v0, Lcom/facebook/loom/provider/StackFrameThread$1;

    invoke-direct {v0, p0}, Lcom/facebook/loom/provider/StackFrameThread$1;-><init>(LX/0Pe;)V

    iput-object v0, p0, LX/0Pe;->d:Ljava/lang/Runnable;

    .line 56963
    return-void
.end method

.method public static declared-synchronized b(LX/0Pe;II)V
    .locals 6

    .prologue
    .line 56986
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Pe;->b(LX/0Pe;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 56987
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 56988
    :cond_1
    :try_start_1
    const/4 v0, 0x0

    .line 56989
    and-int/lit16 v1, p2, 0x80

    if-eqz v1, :cond_2

    .line 56990
    const/4 v0, 0x3

    .line 56991
    :cond_2
    and-int/lit16 v1, p2, 0x1000

    if-eqz v1, :cond_3

    .line 56992
    or-int/lit8 v0, v0, 0x4

    .line 56993
    :cond_3
    move v0, v0

    .line 56994
    invoke-static {v0}, Lcom/facebook/cpuprofiler/CPUProfiler;->a(I)Z

    move-result v0

    .line 56995
    if-eqz v0, :cond_0

    .line 56996
    if-gtz p1, :cond_4

    .line 56997
    const/16 p1, 0x11

    .line 56998
    :cond_4
    const/4 v0, -0x1

    const/16 v1, 0x3b

    const v2, 0x7c001f

    int-to-long v4, p1

    invoke-static {v0, v1, v2, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 56999
    iput p1, p0, LX/0Pe;->a:I

    .line 57000
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Pe;->g:Z

    .line 57001
    iget-object v0, p0, LX/0Pe;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0Pe;->d:Ljava/lang/Runnable;

    iget-object v2, p0, LX/0Pe;->f:Ljava/util/Random;

    iget v3, p0, LX/0Pe;->a:I

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-long v2, v2

    const v4, -0x3e46cc6

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57002
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/0Pe;)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.os.HandlerThread._Constructor",
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 56980
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Pe;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/cpuprofiler/CPUProfiler;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 56981
    :goto_0
    monitor-exit p0

    return v0

    .line 56982
    :catch_0
    move-exception v0

    .line 56983
    :try_start_1
    const-string v1, "StackFrameThread"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56984
    const/4 v0, 0x0

    goto :goto_0

    .line 56985
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 56974
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Pe;->c:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 56975
    :goto_0
    monitor-exit p0

    return-void

    .line 56976
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CPU Profiler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Pe;->b:Landroid/os/HandlerThread;

    .line 56977
    iget-object v0, p0, LX/0Pe;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 56978
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/0Pe;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0Pe;->c:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 56968
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Pe;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 56969
    :goto_0
    monitor-exit p0

    return-void

    .line 56970
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/0Pe;->g:Z

    .line 56971
    iget-object v0, p0, LX/0Pe;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0Pe;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 56972
    invoke-static {}, Lcom/facebook/cpuprofiler/CPUProfiler;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(II)V
    .locals 3

    .prologue
    .line 56964
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0Pe;->c()V

    .line 56965
    iget-object v0, p0, LX/0Pe;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/loom/provider/StackFrameThread$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/loom/provider/StackFrameThread$2;-><init>(LX/0Pe;II)V

    const v2, -0x731a41b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56966
    monitor-exit p0

    return-void

    .line 56967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
