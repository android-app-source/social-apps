.class public LX/150;
.super LX/151;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/150;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179842
    invoke-direct {p0, p1, p2, p3}, LX/151;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 179843
    return-void
.end method

.method public static a(LX/0QB;)LX/150;
    .locals 6

    .prologue
    .line 179844
    sget-object v0, LX/150;->b:LX/150;

    if-nez v0, :cond_1

    .line 179845
    const-class v1, LX/150;

    monitor-enter v1

    .line 179846
    :try_start_0
    sget-object v0, LX/150;->b:LX/150;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 179847
    if-eqz v2, :cond_0

    .line 179848
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 179849
    new-instance p0, LX/150;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/150;-><init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V

    .line 179850
    move-object v0, p0

    .line 179851
    sput-object v0, LX/150;->b:LX/150;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179852
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 179853
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179854
    :cond_1
    sget-object v0, LX/150;->b:LX/150;

    return-object v0

    .line 179855
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 179856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179857
    sget-object v0, LX/6bV;->a:Ljava/lang/String;

    return-object v0
.end method
