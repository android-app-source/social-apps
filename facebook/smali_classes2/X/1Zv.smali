.class public final LX/1Zv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/io/FileOutputStream;

.field private final b:Ljava/nio/channels/FileLock;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 275644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275645
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    .line 275646
    :try_start_0
    iget-object v0, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 275647
    if-nez v0, :cond_0

    .line 275648
    iget-object v1, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 275649
    :cond_0
    iput-object v0, p0, LX/1Zv;->b:Ljava/nio/channels/FileLock;

    .line 275650
    return-void

    .line 275651
    :catchall_0
    move-exception v0

    .line 275652
    iget-object v1, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 275653
    :try_start_0
    iget-object v0, p0, LX/1Zv;->b:Ljava/nio/channels/FileLock;

    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275654
    iget-object v0, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 275655
    return-void

    .line 275656
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1Zv;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method
