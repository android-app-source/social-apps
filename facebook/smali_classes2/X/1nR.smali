.class public LX/1nR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field private static final m:LX/0Tn;

.field private static final n:LX/0Tn;

.field private static final o:LX/0Tn;

.field private static final p:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 316432
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "growth/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 316433
    sput-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "friendfinder/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->n:LX/0Tn;

    .line 316434
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "ad_campaign/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->o:LX/0Tn;

    .line 316435
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "ci/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->p:LX/0Tn;

    .line 316436
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "user_account_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->a:LX/0Tn;

    .line 316437
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "growth_ci_continuous_sync/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->b:LX/0Tn;

    .line 316438
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "reg_ccu_terms_accepted/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->c:LX/0Tn;

    .line 316439
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "friendable_contacts_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->d:LX/0Tn;

    .line 316440
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "friending_tooltip_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->e:LX/0Tn;

    .line 316441
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "future_friending_add_friend_tooltip_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->f:LX/0Tn;

    .line 316442
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "future_friending_request_sent_tooltip_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->g:LX/0Tn;

    .line 316443
    sget-object v0, LX/1nR;->n:LX/0Tn;

    const-string v1, "legalapproved/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->h:LX/0Tn;

    .line 316444
    sget-object v0, LX/1nR;->n:LX/0Tn;

    const-string v1, "persistent_legal_approved/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->i:LX/0Tn;

    .line 316445
    sget-object v0, LX/1nR;->o:LX/0Tn;

    const-string v1, "is_app_new_install_reported/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->j:LX/0Tn;

    .line 316446
    sget-object v0, LX/1nR;->p:LX/0Tn;

    const-string v1, "findFriendsLegalBarShown/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->k:LX/0Tn;

    .line 316447
    sget-object v0, LX/1nR;->m:LX/0Tn;

    const-string v1, "ccu_interstitial_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1nR;->l:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 316431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 316430
    sget-object v0, LX/1nR;->b:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/0Tn;
    .locals 3

    .prologue
    .line 316448
    sget-object v0, LX/1nR;->i:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 316449
    sget-object v1, LX/1nR;->h:LX/0Tn;

    const/4 p0, 0x0

    .line 316450
    invoke-interface {p1, v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 316451
    if-eqz v2, :cond_0

    .line 316452
    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v1, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 316453
    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, v0, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 316454
    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 316427
    sget-object v0, LX/1nR;->c:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 316429
    sget-object v0, LX/1nR;->d:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static d(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 316428
    sget-object v0, LX/1nR;->l:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
