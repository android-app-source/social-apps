.class public LX/1kp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 310257
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "prompts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 310258
    sput-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_production_prompt_network_fetch_seconds"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->b:LX/0Tn;

    .line 310259
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_media_prompt_dismissed"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->c:LX/0Tn;

    .line 310260
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_media_prompt_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->d:LX/0Tn;

    .line 310261
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "media_prompt_impression_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->e:LX/0Tn;

    .line 310262
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "is_photo_reminder_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->f:LX/0Tn;

    .line 310263
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_prompt_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->g:LX/0Tn;

    .line 310264
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_photo_reminder_prompt_posted"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->h:LX/0Tn;

    .line 310265
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_weekend_photo_reminder_prompt_posted"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->i:LX/0Tn;

    .line 310266
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_storyline_prompt_dismissed_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->j:LX/0Tn;

    .line 310267
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "time_last_photo_clipboard_prompt_posted"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->k:LX/0Tn;

    .line 310268
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "num_days_until_show_photo_reminder_prompt_again"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->l:LX/0Tn;

    .line 310269
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "num_days_until_show_weekend_photo_reminder_prompt_again"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->m:LX/0Tn;

    .line 310270
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "num_days_until_show_clipboard_prompt_again"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->n:LX/0Tn;

    .line 310271
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "num_dismiss_since_last_photo_prompt_post"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->o:LX/0Tn;

    .line 310272
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "num_dismiss_since_last_clipboard_prompt_post"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->p:LX/0Tn;

    .line 310273
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "clipboard_prompt_uri"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->q:LX/0Tn;

    .line 310274
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "clipboard_prompt_expired_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->r:LX/0Tn;

    .line 310275
    sget-object v0, LX/1kp;->a:LX/0Tn;

    const-string v1, "last_prompt_id_hidden"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1kp;->s:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 310276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310277
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310278
    sget-object v0, LX/1kp;->c:LX/0Tn;

    sget-object v1, LX/1kp;->d:LX/0Tn;

    sget-object v2, LX/1kp;->e:LX/0Tn;

    sget-object v3, LX/1kp;->f:LX/0Tn;

    sget-object v4, LX/1kp;->g:LX/0Tn;

    sget-object v5, LX/1kp;->h:LX/0Tn;

    const/16 v6, 0x8

    new-array v6, v6, [LX/0Tn;

    const/4 v7, 0x0

    sget-object v8, LX/1kp;->k:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/1kp;->l:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/1kp;->n:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/1kp;->o:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, LX/1kp;->p:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, LX/1kp;->r:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, LX/1kp;->q:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, LX/1kp;->s:LX/0Tn;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
