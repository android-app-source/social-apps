.class public final LX/14d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/14e;
.implements Lorg/apache/http/HttpEntity;


# static fields
.field public static final a:Lorg/apache/http/message/BasicHeader;


# instance fields
.field public b:LX/4cJ;

.field private final c:LX/0n9;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 179063
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/14d;->a:Lorg/apache/http/message/BasicHeader;

    return-void
.end method

.method public constructor <init>(LX/0n9;)V
    .locals 0

    .prologue
    .line 179055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179056
    iput-object p1, p0, LX/14d;->c:LX/0n9;

    .line 179057
    return-void
.end method

.method public static a(LX/0n9;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 179058
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 179059
    :try_start_0
    invoke-static {}, LX/10F;->a()LX/10F;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, LX/10F;->a(Ljava/io/Writer;LX/0nA;)V

    .line 179060
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 179061
    :catch_0
    move-exception v0

    .line 179062
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Platform error"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 179064
    iget-object v0, p0, LX/14d;->c:LX/0n9;

    invoke-virtual {v0}, LX/0nA;->a()V

    .line 179065
    return-void
.end method

.method public final consumeContent()V
    .locals 0

    .prologue
    .line 179032
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 179051
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 179052
    invoke-virtual {p0, v0}, LX/14d;->writeTo(Ljava/io/OutputStream;)V

    .line 179053
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v1
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 179054
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 179050
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 179049
    sget-object v0, LX/14d;->a:Lorg/apache/http/message/BasicHeader;

    return-object v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 179048
    const/4 v0, 0x0

    return v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 179047
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 179046
    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 7

    .prologue
    .line 179033
    const/4 v0, 0x0

    .line 179034
    instance-of v1, p1, Ljava/io/ByteArrayOutputStream;

    if-nez v1, :cond_0

    instance-of v1, p1, Ljava/io/BufferedOutputStream;

    if-nez v1, :cond_0

    .line 179035
    new-instance v0, Ljava/io/BufferedOutputStream;

    const/16 v1, 0x800

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    move-object p1, v0

    .line 179036
    :cond_0
    new-instance v1, LX/1iJ;

    invoke-direct {v1, p1}, LX/1iJ;-><init>(Ljava/io/OutputStream;)V

    .line 179037
    new-instance v2, LX/1iK;

    invoke-direct {v2, v1}, LX/1iK;-><init>(Ljava/io/OutputStream;)V

    .line 179038
    invoke-static {}, LX/10F;->a()LX/10F;

    move-result-object v3

    iget-object v4, p0, LX/14d;->c:LX/0n9;

    invoke-virtual {v3, v2, v4}, LX/10F;->a(Ljava/io/Writer;LX/0nA;)V

    .line 179039
    if-eqz v0, :cond_1

    .line 179040
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    .line 179041
    :cond_1
    iget-object v0, p0, LX/14d;->b:LX/4cJ;

    if-eqz v0, :cond_2

    .line 179042
    iget-object v0, p0, LX/14d;->b:LX/4cJ;

    .line 179043
    iget-wide v5, v1, LX/1iJ;->a:J

    move-wide v2, v5

    .line 179044
    invoke-interface {v0, v2, v3}, LX/4cJ;->a(J)V

    .line 179045
    :cond_2
    return-void
.end method
