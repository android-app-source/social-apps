.class public abstract LX/1cZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ca;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1ca",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:LX/1cb;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:F
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/1cj",
            "<TT;>;",
            "Ljava/util/concurrent/Executor;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 282223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282224
    iput-object v0, p0, LX/1cZ;->c:Ljava/lang/Object;

    .line 282225
    iput-object v0, p0, LX/1cZ;->d:Ljava/lang/Throwable;

    .line 282226
    const/4 v0, 0x0

    iput v0, p0, LX/1cZ;->e:F

    .line 282227
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cZ;->b:Z

    .line 282228
    sget-object v0, LX/1cb;->IN_PROGRESS:LX/1cb;

    iput-object v0, p0, LX/1cZ;->a:LX/1cb;

    .line 282229
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/1cZ;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 282230
    return-void
.end method

.method private a(LX/1cj;Ljava/util/concurrent/Executor;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cj",
            "<TT;>;",
            "Ljava/util/concurrent/Executor;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 282323
    new-instance v0, Lcom/facebook/datasource/AbstractDataSource$1;

    invoke-direct {v0, p0, p3, p1, p4}, Lcom/facebook/datasource/AbstractDataSource$1;-><init>(LX/1cZ;ZLX/1cj;Z)V

    const v1, -0x7a336bcf

    invoke-static {p2, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 282324
    return-void
.end method

.method private declared-synchronized b(F)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 282317
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/1cZ;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1cZ;->a:LX/1cb;

    sget-object v2, LX/1cb;->IN_PROGRESS:LX/1cb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v2, :cond_1

    .line 282318
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 282319
    :cond_1
    :try_start_1
    iget v1, p0, LX/1cZ;->e:F

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    .line 282320
    iput p1, p0, LX/1cZ;->e:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282321
    const/4 v0, 0x1

    goto :goto_0

    .line 282322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Ljava/lang/Object;Z)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    .prologue
    .line 282297
    const/4 v1, 0x0

    .line 282298
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 282299
    :try_start_1
    iget-boolean v0, p0, LX/1cZ;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1cZ;->a:LX/1cb;

    sget-object v2, LX/1cb;->IN_PROGRESS:LX/1cb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v2, :cond_2

    .line 282300
    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 282301
    if-eqz p1, :cond_1

    .line 282302
    invoke-virtual {p0, p1}, LX/1cZ;->a(Ljava/lang/Object;)V

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 282303
    :cond_2
    if-eqz p2, :cond_3

    .line 282304
    :try_start_3
    sget-object v0, LX/1cb;->SUCCESS:LX/1cb;

    iput-object v0, p0, LX/1cZ;->a:LX/1cb;

    .line 282305
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/1cZ;->e:F

    .line 282306
    :cond_3
    iget-object v0, p0, LX/1cZ;->c:Ljava/lang/Object;

    if-eq v0, p1, :cond_6

    .line 282307
    iget-object v1, p0, LX/1cZ;->c:Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 282308
    :try_start_4
    iput-object p1, p0, LX/1cZ;->c:Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-object p1, v1

    .line 282309
    :goto_1
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 282310
    if-eqz p1, :cond_4

    .line 282311
    invoke-virtual {p0, p1}, LX/1cZ;->a(Ljava/lang/Object;)V

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 282312
    :catchall_0
    move-exception v0

    move-object p1, v1

    :goto_2
    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 282313
    :catchall_1
    move-exception v0

    :goto_3
    if-eqz p1, :cond_5

    .line 282314
    invoke-virtual {p0, p1}, LX/1cZ;->a(Ljava/lang/Object;)V

    :cond_5
    throw v0

    .line 282315
    :catchall_2
    move-exception v0

    move-object p1, v1

    goto :goto_3

    .line 282316
    :catchall_3
    move-exception v0

    goto :goto_2

    :catchall_4
    move-exception v0

    move-object p1, v1

    goto :goto_2

    :cond_6
    move-object p1, v1

    goto :goto_1
.end method

.method private declared-synchronized b(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    .line 282290
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cZ;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1cZ;->a:LX/1cb;

    sget-object v1, LX/1cb;->IN_PROGRESS:LX/1cb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    .line 282291
    :cond_0
    const/4 v0, 0x0

    .line 282292
    :goto_0
    monitor-exit p0

    return v0

    .line 282293
    :cond_1
    :try_start_1
    sget-object v0, LX/1cb;->FAILURE:LX/1cb;

    iput-object v0, p0, LX/1cZ;->a:LX/1cb;

    .line 282294
    iput-object p1, p0, LX/1cZ;->d:Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282295
    const/4 v0, 0x1

    goto :goto_0

    .line 282296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()Z
    .locals 2

    .prologue
    .line 282289
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cZ;->a:LX/1cb;

    sget-object v1, LX/1cb;->FAILURE:LX/1cb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()V
    .locals 5

    .prologue
    .line 282284
    invoke-direct {p0}, LX/1cZ;->h()Z

    move-result v2

    .line 282285
    invoke-direct {p0}, LX/1cZ;->j()Z

    move-result v3

    .line 282286
    iget-object v0, p0, LX/1cZ;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 282287
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/1cj;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v1, v0, v2, v3}, LX/1cZ;->a(LX/1cj;Ljava/util/concurrent/Executor;ZZ)V

    goto :goto_0

    .line 282288
    :cond_0
    return-void
.end method

.method private declared-synchronized j()Z
    .locals 1

    .prologue
    .line 282283
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1cZ;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1cj;Ljava/util/concurrent/Executor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cj",
            "<TT;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282269
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282270
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282271
    monitor-enter p0

    .line 282272
    :try_start_0
    iget-boolean v0, p0, LX/1cZ;->b:Z

    if-eqz v0, :cond_1

    .line 282273
    monitor-exit p0

    .line 282274
    :cond_0
    :goto_0
    return-void

    .line 282275
    :cond_1
    iget-object v0, p0, LX/1cZ;->a:LX/1cb;

    sget-object v1, LX/1cb;->IN_PROGRESS:LX/1cb;

    if-ne v0, v1, :cond_2

    .line 282276
    iget-object v0, p0, LX/1cZ;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 282277
    :cond_2
    invoke-virtual {p0}, LX/1cZ;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, LX/1cZ;->b()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, LX/1cZ;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 282278
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282279
    if-eqz v0, :cond_0

    .line 282280
    invoke-direct {p0}, LX/1cZ;->h()Z

    move-result v0

    invoke-direct {p0}, LX/1cZ;->j()Z

    move-result v1

    invoke-direct {p0, p1, p2, v0, v1}, LX/1cZ;->a(LX/1cj;Ljava/util/concurrent/Executor;ZZ)V

    goto :goto_0

    .line 282281
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 282282
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 282268
    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 282267
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cZ;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(F)Z
    .locals 4

    .prologue
    .line 282260
    invoke-direct {p0, p1}, LX/1cZ;->b(F)Z

    move-result v0

    .line 282261
    if-eqz v0, :cond_0

    .line 282262
    iget-object v1, p0, LX/1cZ;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 282263
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, LX/1cj;

    .line 282264
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/concurrent/Executor;

    .line 282265
    new-instance p1, Lcom/facebook/datasource/AbstractDataSource$2;

    invoke-direct {p1, p0, v2}, Lcom/facebook/datasource/AbstractDataSource$2;-><init>(LX/1cZ;LX/1cj;)V

    const v2, 0x78f73c70

    invoke-static {v1, p1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 282266
    :cond_0
    return v0
.end method

.method public a(Ljava/lang/Object;Z)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    .prologue
    .line 282256
    invoke-direct {p0, p1, p2}, LX/1cZ;->b(Ljava/lang/Object;Z)Z

    move-result v0

    .line 282257
    if-eqz v0, :cond_0

    .line 282258
    invoke-direct {p0}, LX/1cZ;->i()V

    .line 282259
    :cond_0
    return v0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 282252
    invoke-direct {p0, p1}, LX/1cZ;->b(Ljava/lang/Throwable;)Z

    move-result v0

    .line 282253
    if-eqz v0, :cond_0

    .line 282254
    invoke-direct {p0}, LX/1cZ;->i()V

    .line 282255
    :cond_0
    return v0
.end method

.method public final declared-synchronized b()Z
    .locals 2

    .prologue
    .line 282251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cZ;->a:LX/1cb;

    sget-object v1, LX/1cb;->IN_PROGRESS:LX/1cb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .locals 1

    .prologue
    .line 282250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cZ;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282249
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cZ;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/lang/Throwable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282248
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cZ;->d:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()F
    .locals 1

    .prologue
    .line 282247
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1cZ;->e:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 282231
    monitor-enter p0

    .line 282232
    :try_start_0
    iget-boolean v1, p0, LX/1cZ;->b:Z

    if-eqz v1, :cond_0

    .line 282233
    const/4 v0, 0x0

    monitor-exit p0

    .line 282234
    :goto_0
    return v0

    .line 282235
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/1cZ;->b:Z

    .line 282236
    iget-object v1, p0, LX/1cZ;->c:Ljava/lang/Object;

    .line 282237
    const/4 v2, 0x0

    iput-object v2, p0, LX/1cZ;->c:Ljava/lang/Object;

    .line 282238
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 282239
    if-eqz v1, :cond_1

    .line 282240
    invoke-virtual {p0, v1}, LX/1cZ;->a(Ljava/lang/Object;)V

    .line 282241
    :cond_1
    invoke-virtual {p0}, LX/1cZ;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 282242
    invoke-direct {p0}, LX/1cZ;->i()V

    .line 282243
    :cond_2
    monitor-enter p0

    .line 282244
    :try_start_1
    iget-object v1, p0, LX/1cZ;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 282245
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 282246
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
