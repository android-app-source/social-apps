.class public LX/1TO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 251125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251126
    iput-object p1, p0, LX/1TO;->b:LX/0Ot;

    .line 251127
    iput-object p2, p0, LX/1TO;->a:LX/0Ot;

    .line 251128
    return-void
.end method

.method public static a(LX/0QB;)LX/1TO;
    .locals 5

    .prologue
    .line 251114
    const-class v1, LX/1TO;

    monitor-enter v1

    .line 251115
    :try_start_0
    sget-object v0, LX/1TO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 251116
    sput-object v2, LX/1TO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251117
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251118
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 251119
    new-instance v3, LX/1TO;

    const/16 v4, 0x1fa9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x94f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1TO;-><init>(LX/0Ot;LX/0Ot;)V

    .line 251120
    move-object v0, v3

    .line 251121
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 251122
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251123
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 251124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 251111
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 251112
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 251113
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 251108
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    iget-object v1, p0, LX/1TO;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251109
    const-class v0, Lcom/facebook/graphql/model/GraphQLNoContentFeedUnit;

    iget-object v1, p0, LX/1TO;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251110
    return-void
.end method
