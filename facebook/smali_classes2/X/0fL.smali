.class public abstract LX/0fL;
.super LX/0fM;
.source ""

# interfaces
.implements LX/0fN;


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public h:LX/10f;

.field public i:LX/0gc;

.field public j:LX/0gc;

.field public k:Lcom/facebook/ui/drawers/BackStackFragment;

.field public l:Z

.field private final m:Landroid/os/MessageQueue$IdleHandler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106435
    const-class v0, LX/0fL;

    sput-object v0, LX/0fL;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106427
    invoke-direct {p0, p1}, LX/0fM;-><init>(LX/0Sh;)V

    .line 106428
    sget-object v0, LX/10f;->LOAD_WHEN_IDLE_FORCE_WHEN_VISIBLE:LX/10f;

    iput-object v0, p0, LX/0fL;->h:LX/10f;

    .line 106429
    iput-object v1, p0, LX/0fL;->i:LX/0gc;

    .line 106430
    iput-object v1, p0, LX/0fL;->j:LX/0gc;

    .line 106431
    iput-object v1, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106432
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0fL;->l:Z

    .line 106433
    new-instance v0, LX/10g;

    invoke-direct {v0, p0}, LX/10g;-><init>(LX/0fL;)V

    iput-object v0, p0, LX/0fL;->m:Landroid/os/MessageQueue$IdleHandler;

    .line 106434
    return-void
.end method

.method public static s(LX/0fL;)V
    .locals 4

    .prologue
    .line 106413
    iget-boolean v0, p0, LX/0fL;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    if-nez v0, :cond_1

    .line 106414
    :cond_0
    :goto_0
    return-void

    .line 106415
    :cond_1
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    move-object v0, v0

    .line 106416
    invoke-virtual {p0}, LX/0fL;->j()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/drawers/DrawerContentFragment;

    .line 106417
    if-nez v0, :cond_3

    .line 106418
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    move-object v0, v0

    .line 106419
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106420
    invoke-virtual {p0}, LX/0fL;->k()Lcom/facebook/ui/drawers/DrawerContentFragment;

    move-result-object v0

    .line 106421
    iget-object v1, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106422
    iget-object v2, v1, Lcom/facebook/ui/drawers/BackStackFragment;->c:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 106423
    iget-object v2, v1, Lcom/facebook/ui/drawers/BackStackFragment;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    new-instance v3, Lcom/facebook/ui/drawers/BackStackFragment$1;

    invoke-direct {v3, v1}, Lcom/facebook/ui/drawers/BackStackFragment$1;-><init>(Lcom/facebook/ui/drawers/BackStackFragment;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 106424
    :cond_2
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    invoke-static {v1}, Lcom/facebook/ui/drawers/BackStackFragment;->b(Lcom/facebook/ui/drawers/BackStackFragment;)I

    move-result v3

    invoke-virtual {v2, v3, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    .line 106425
    :cond_3
    invoke-virtual {p0, v0}, LX/0fL;->a(Lcom/facebook/ui/drawers/DrawerContentFragment;)V

    .line 106426
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0fL;->l:Z

    goto :goto_0
.end method

.method public static t(LX/0fL;)V
    .locals 2

    .prologue
    .line 106410
    invoke-static {p0}, LX/0fL;->u(LX/0fL;)V

    .line 106411
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, LX/0fL;->m:Landroid/os/MessageQueue$IdleHandler;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 106412
    return-void
.end method

.method public static u(LX/0fL;)V
    .locals 2

    .prologue
    .line 106408
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, LX/0fL;->m:Landroid/os/MessageQueue$IdleHandler;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 106409
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 106356
    new-instance v0, Lcom/facebook/widget/CustomFrameLayout;

    invoke-direct {v0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 106357
    invoke-virtual {p0}, LX/0fL;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setId(I)V

    .line 106358
    return-object v0
.end method

.method public a(LX/0fP;)V
    .locals 1

    .prologue
    .line 106405
    invoke-super {p0, p1}, LX/0fM;->a(LX/0fP;)V

    .line 106406
    invoke-virtual {p1}, LX/0fP;->d()LX/0gc;

    move-result-object v0

    iput-object v0, p0, LX/0fL;->i:LX/0gc;

    .line 106407
    return-void
.end method

.method public a(Lcom/facebook/ui/drawers/DrawerContentFragment;)V
    .locals 0

    .prologue
    .line 106404
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 106395
    invoke-super {p0, p1}, LX/0fM;->a(Z)V

    .line 106396
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/0fL;->h:LX/10f;

    invoke-virtual {v0}, LX/10f;->shouldLoadWhenVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106397
    invoke-static {p0}, LX/0fL;->s(LX/0fL;)V

    .line 106398
    :cond_0
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    if-nez v0, :cond_2

    .line 106399
    :cond_1
    :goto_0
    return-void

    .line 106400
    :cond_2
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    move-object v0, v0

    .line 106401
    invoke-virtual {p0}, LX/0fL;->j()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 106402
    if-eqz v0, :cond_1

    .line 106403
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 106394
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(LX/0fP;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106385
    invoke-static {p0}, LX/0fL;->u(LX/0fL;)V

    .line 106386
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106387
    iget-object v0, p0, LX/0fL;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 106388
    :cond_0
    invoke-super {p0, p1}, LX/0fM;->b(LX/0fP;)V

    .line 106389
    iput-object v2, p0, LX/0fL;->i:LX/0gc;

    .line 106390
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0fL;->l:Z

    .line 106391
    iput-object v2, p0, LX/0fL;->j:LX/0gc;

    .line 106392
    iput-object v2, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106393
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 106381
    invoke-super {p0, p1}, LX/0fM;->b(Z)V

    .line 106382
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/0fL;->h:LX/10f;

    invoke-virtual {v0}, LX/10f;->shouldLoadWhenFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106383
    invoke-static {p0}, LX/0fL;->s(LX/0fL;)V

    .line 106384
    :cond_0
    return-void
.end method

.method public final fW_()V
    .locals 3

    .prologue
    .line 106360
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    if-eqz v0, :cond_0

    .line 106361
    :goto_0
    return-void

    .line 106362
    :cond_0
    invoke-virtual {p0}, LX/0fL;->j()I

    move-result v1

    .line 106363
    iget-object v0, p0, LX/0fL;->i:LX/0gc;

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/drawers/BackStackFragment;

    iput-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106364
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    if-nez v0, :cond_2

    .line 106365
    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 106366
    const-string v2, "argument_stack_container_id"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106367
    new-instance v2, Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-direct {v2}, Lcom/facebook/ui/drawers/BackStackFragment;-><init>()V

    .line 106368
    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 106369
    move-object v0, v2

    .line 106370
    iput-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106371
    iget-object v0, p0, LX/0fL;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 106372
    :cond_1
    :goto_1
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106373
    iput-object p0, v0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    .line 106374
    goto :goto_0

    .line 106375
    :cond_2
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106376
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 106377
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106378
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 106379
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 106380
    iget-object v0, p0, LX/0fL;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0, v1}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-virtual {v0, v1}, LX/0hH;->e(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_1
.end method

.method public final fY_()Z
    .locals 1

    .prologue
    .line 106359
    invoke-virtual {p0}, LX/0fL;->m()Z

    move-result v0

    return v0
.end method

.method public final fZ_()V
    .locals 2

    .prologue
    .line 106351
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    move-object v0, v0

    .line 106352
    invoke-virtual {p0}, LX/0fL;->j()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/drawers/DrawerContentFragment;

    .line 106353
    if-eqz v0, :cond_0

    .line 106354
    invoke-virtual {p0, v0}, LX/0fL;->a(Lcom/facebook/ui/drawers/DrawerContentFragment;)V

    .line 106355
    :cond_0
    return-void
.end method

.method public abstract j()I
.end method

.method public abstract k()Lcom/facebook/ui/drawers/DrawerContentFragment;
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 106346
    iget-object v0, p0, LX/0fL;->j:LX/0gc;

    move-object v0, v0

    .line 106347
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v1

    if-lez v1, :cond_0

    .line 106348
    invoke-virtual {v0}, LX/0gc;->d()V

    .line 106349
    const/4 v0, 0x1

    .line 106350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 106343
    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    .line 106344
    iget-boolean p0, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, p0

    .line 106345
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
