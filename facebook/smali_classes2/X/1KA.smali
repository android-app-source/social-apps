.class public LX/1KA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/195;

.field public final b:LX/1KB;

.field public final c:LX/198;

.field public final d:Landroid/content/res/Resources;

.field public e:Landroid/util/SparseIntArray;

.field public f:I

.field public g:I

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/198;LX/1KB;LX/193;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 231081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231082
    iput-object p1, p0, LX/1KA;->d:Landroid/content/res/Resources;

    .line 231083
    iput-object p2, p0, LX/1KA;->c:LX/198;

    .line 231084
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "news_feed_scroll"

    invoke-virtual {p4, v0, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, LX/1KA;->a:LX/195;

    .line 231085
    iget-object v0, p0, LX/1KA;->a:LX/195;

    invoke-direct {p0}, LX/1KA;->b()LX/1KG;

    move-result-object v1

    .line 231086
    iput-object v1, v0, LX/195;->k:LX/1KG;

    .line 231087
    iput-object p3, p0, LX/1KA;->b:LX/1KB;

    .line 231088
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/1KA;->e:Landroid/util/SparseIntArray;

    .line 231089
    return-void
.end method

.method public static a(LX/0QB;)LX/1KA;
    .locals 7

    .prologue
    .line 231090
    const-class v1, LX/1KA;

    monitor-enter v1

    .line 231091
    :try_start_0
    sget-object v0, LX/1KA;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 231092
    sput-object v2, LX/1KA;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 231093
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231094
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 231095
    new-instance p0, LX/1KA;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v4

    check-cast v4, LX/198;

    invoke-static {v0}, LX/1KB;->a(LX/0QB;)LX/1KB;

    move-result-object v5

    check-cast v5, LX/1KB;

    const-class v6, LX/193;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/193;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1KA;-><init>(Landroid/content/res/Resources;LX/198;LX/1KB;LX/193;)V

    .line 231096
    move-object v0, p0

    .line 231097
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 231098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1KA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 231100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b()LX/1KG;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 231101
    new-instance v0, LX/1KE;

    invoke-direct {v0, p0}, LX/1KE;-><init>(LX/1KA;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 231102
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 231103
    iput p1, p0, LX/1KA;->f:I

    .line 231104
    iget-object v0, p0, LX/1KA;->a:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 231105
    return-void

    .line 231106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
