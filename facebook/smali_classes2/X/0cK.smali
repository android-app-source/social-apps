.class public LX/0cK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/os/Messenger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field public final c:LX/00G;


# direct methods
.method public constructor <init>(Landroid/os/Messenger;ILX/00G;)V
    .locals 0
    .param p1    # Landroid/os/Messenger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 88174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88175
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88176
    iput-object p1, p0, LX/0cK;->a:Landroid/os/Messenger;

    .line 88177
    iput p2, p0, LX/0cK;->b:I

    .line 88178
    iput-object p3, p0, LX/0cK;->c:LX/00G;

    .line 88179
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/0cK;
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 88180
    const-string v0, "key_messenger"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 88181
    const-string v1, "The messenger is not in the bundle passed in"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88182
    const-string v1, "key_pid"

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 88183
    if-ne v1, v2, :cond_0

    .line 88184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The pid is not in the bundle passed in"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88185
    :cond_0
    const-string v2, "key_process_name"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88186
    const-string v3, "The process name is not in the bundle passed in"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88187
    new-instance v3, LX/0cK;

    invoke-static {v2}, LX/00G;->a(Ljava/lang/String;)LX/00G;

    move-result-object v2

    invoke-direct {v3, v0, v1, v2}, LX/0cK;-><init>(Landroid/os/Messenger;ILX/00G;)V

    return-object v3
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 88188
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88189
    const-string v1, "key_messenger"

    iget-object v2, p0, LX/0cK;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 88190
    const-string v1, "key_pid"

    iget v2, p0, LX/0cK;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88191
    const-string v1, "key_process_name"

    iget-object v2, p0, LX/0cK;->c:LX/00G;

    .line 88192
    iget-object p0, v2, LX/00G;->b:Ljava/lang/String;

    move-object v2, p0

    .line 88193
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88194
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88195
    instance-of v1, p1, LX/0cK;

    if-eqz v1, :cond_0

    .line 88196
    iget v1, p0, LX/0cK;->b:I

    check-cast p1, LX/0cK;

    iget v2, p1, LX/0cK;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 88197
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 88198
    iget v0, p0, LX/0cK;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 88199
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "messenger: "

    iget-object v2, p0, LX/0cK;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pid: "

    iget v2, p0, LX/0cK;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "process name:"

    iget-object v2, p0, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
