.class public abstract LX/17C;
.super LX/17D;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/17D",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 189105
    invoke-direct {p0}, LX/17D;-><init>()V

    .line 189106
    iput-object p1, p0, LX/17C;->a:Landroid/os/Handler;

    .line 189107
    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 189108
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, LX/17C;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 189109
    invoke-virtual {p0}, LX/17D;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189110
    const-string v0, "Must not call get() function from this Handler thread. Will deadlock!"

    .line 189111
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 189112
    :cond_0
    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 189113
    invoke-direct {p0}, LX/17C;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189114
    invoke-direct {p0}, LX/17C;->c()V

    .line 189115
    :cond_0
    invoke-super {p0}, LX/17D;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 189116
    invoke-direct {p0}, LX/17C;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189117
    invoke-direct {p0}, LX/17C;->c()V

    .line 189118
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/17D;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
