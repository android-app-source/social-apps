.class public LX/1gQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 294162
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    sput-object v0, LX/1gQ;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;LX/1FN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/1FN",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 294198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294199
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/1gQ;->b:Ljava/lang/Object;

    .line 294200
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FN;

    iput-object v0, p0, LX/1gQ;->d:LX/1FN;

    .line 294201
    const/4 v0, 0x1

    iput v0, p0, LX/1gQ;->c:I

    .line 294202
    invoke-static {p1}, LX/1gQ;->a(Ljava/lang/Object;)V

    .line 294203
    return-void
.end method

.method private static a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 294191
    sget-object v1, LX/1gQ;->a:Ljava/util/Map;

    monitor-enter v1

    .line 294192
    :try_start_0
    sget-object v0, LX/1gQ;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 294193
    if-nez v0, :cond_0

    .line 294194
    sget-object v0, LX/1gQ;->a:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294195
    :goto_0
    monitor-exit v1

    return-void

    .line 294196
    :cond_0
    sget-object v2, LX/1gQ;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 294197
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 294182
    sget-object v1, LX/1gQ;->a:Ljava/util/Map;

    monitor-enter v1

    .line 294183
    :try_start_0
    sget-object v0, LX/1gQ;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 294184
    if-nez v0, :cond_0

    .line 294185
    const-string v0, "SharedReference"

    const-string v2, "No entry in sLiveObjects for value of type %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294186
    :goto_0
    monitor-exit v1

    return-void

    .line 294187
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 294188
    sget-object v0, LX/1gQ;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 294189
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 294190
    :cond_1
    :try_start_1
    sget-object v2, LX/1gQ;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized d(LX/1gQ;)Z
    .locals 1

    .prologue
    .line 294181
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1gQ;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized e(LX/1gQ;)I
    .locals 1

    .prologue
    .line 294204
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1gQ;->f(LX/1gQ;)V

    .line 294205
    iget v0, p0, LX/1gQ;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 294206
    iget v0, p0, LX/1gQ;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1gQ;->c:I

    .line 294207
    iget v0, p0, LX/1gQ;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 294208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 294209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static f(LX/1gQ;)V
    .locals 1

    .prologue
    .line 294177
    if-eqz p0, :cond_1

    invoke-static {p0}, LX/1gQ;->d(LX/1gQ;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 294178
    if-nez v0, :cond_0

    .line 294179
    new-instance v0, LX/46Y;

    invoke-direct {v0}, LX/46Y;-><init>()V

    throw v0

    .line 294180
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 294176
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1gQ;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 294172
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1gQ;->f(LX/1gQ;)V

    .line 294173
    iget v0, p0, LX/1gQ;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1gQ;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294174
    monitor-exit p0

    return-void

    .line 294175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 294163
    invoke-static {p0}, LX/1gQ;->e(LX/1gQ;)I

    move-result v0

    if-nez v0, :cond_0

    .line 294164
    monitor-enter p0

    .line 294165
    :try_start_0
    iget-object v0, p0, LX/1gQ;->b:Ljava/lang/Object;

    .line 294166
    const/4 v1, 0x0

    iput-object v1, p0, LX/1gQ;->b:Ljava/lang/Object;

    .line 294167
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294168
    iget-object v1, p0, LX/1gQ;->d:LX/1FN;

    invoke-interface {v1, v0}, LX/1FN;->a(Ljava/lang/Object;)V

    .line 294169
    invoke-static {v0}, LX/1gQ;->b(Ljava/lang/Object;)V

    .line 294170
    :cond_0
    return-void

    .line 294171
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
