.class public LX/1Sa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile e:LX/1Sa;


# instance fields
.field private final b:LX/1Sb;

.field private final c:LX/15W;

.field private final d:LX/1Sc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 248608
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1Sa;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/1Sb;LX/15W;LX/1Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 248603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248604
    iput-object p1, p0, LX/1Sa;->b:LX/1Sb;

    .line 248605
    iput-object p2, p0, LX/1Sa;->c:LX/15W;

    .line 248606
    iput-object p3, p0, LX/1Sa;->d:LX/1Sc;

    .line 248607
    return-void
.end method

.method public static a(LX/0QB;)LX/1Sa;
    .locals 3

    .prologue
    .line 248593
    sget-object v0, LX/1Sa;->e:LX/1Sa;

    if-nez v0, :cond_1

    .line 248594
    const-class v1, LX/1Sa;

    monitor-enter v1

    .line 248595
    :try_start_0
    sget-object v0, LX/1Sa;->e:LX/1Sa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 248596
    if-eqz v2, :cond_0

    .line 248597
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Sa;->b(LX/0QB;)LX/1Sa;

    move-result-object v0

    sput-object v0, LX/1Sa;->e:LX/1Sa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248598
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 248599
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248600
    :cond_1
    sget-object v0, LX/1Sa;->e:LX/1Sa;

    return-object v0

    .line 248601
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 248602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 3
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 248592
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, -0x3625f733

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 248588
    if-nez p0, :cond_1

    .line 248589
    :cond_0
    :goto_0
    return v0

    .line 248590
    :cond_1
    const v1, -0x3625f733

    invoke-static {p0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 248591
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1Sa;
    .locals 4

    .prologue
    .line 248586
    new-instance v3, LX/1Sa;

    invoke-static {p0}, LX/1Sb;->a(LX/0QB;)LX/1Sb;

    move-result-object v0

    check-cast v0, LX/1Sb;

    invoke-static {p0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v1

    check-cast v1, LX/15W;

    invoke-static {p0}, LX/1Sc;->b(LX/0QB;)LX/1Sc;

    move-result-object v2

    check-cast v2, LX/1Sc;

    invoke-direct {v3, v0, v1, v2}, LX/1Sa;-><init>(LX/1Sb;LX/15W;LX/1Sc;)V

    .line 248587
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;
    .locals 1

    .prologue
    .line 248585
    iget-object v0, p0, LX/1Sa;->b:LX/1Sb;

    invoke-virtual {v0, p1}, LX/1Sb;->a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 248582
    iget-object v0, p0, LX/1Sa;->c:LX/15W;

    sget-object v1, LX/1Sa;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 248583
    const-class v2, LX/0i1;

    const/4 p0, 0x0

    invoke-virtual {v0, p1, v1, v2, p0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 248584
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 248580
    iget-object v0, p0, LX/1Sa;->d:LX/1Sc;

    invoke-virtual {v0, p1}, LX/1Sc;->b(Ljava/lang/String;)V

    .line 248581
    return-void
.end method
