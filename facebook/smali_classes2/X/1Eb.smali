.class public LX/1Eb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0y1;

.field private final b:LX/0u7;

.field public c:F


# direct methods
.method public constructor <init>(LX/0u7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220657
    iput-object p1, p0, LX/1Eb;->b:LX/0u7;

    .line 220658
    return-void
.end method

.method public static e(LX/1Eb;)Z
    .locals 2

    .prologue
    .line 220659
    iget-object v0, p0, LX/1Eb;->a:LX/0y1;

    sget-object v1, LX/0y1;->CHARGING_AC:LX/0y1;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1Eb;->a:LX/0y1;

    sget-object v1, LX/0y1;->CHARGING_USB:LX/0y1;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1Eb;->a:LX/0y1;

    sget-object v1, LX/0y1;->CHARGING_WIRELESS:LX/0y1;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1Eb;->a:LX/0y1;

    sget-object v1, LX/0y1;->FULL:LX/0y1;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 220660
    iget-object v0, p0, LX/1Eb;->b:LX/0u7;

    invoke-virtual {v0}, LX/0u7;->a()F

    move-result v0

    iput v0, p0, LX/1Eb;->c:F

    .line 220661
    iget-object v0, p0, LX/1Eb;->b:LX/0u7;

    invoke-virtual {v0}, LX/0u7;->b()LX/0y1;

    move-result-object v0

    iput-object v0, p0, LX/1Eb;->a:LX/0y1;

    .line 220662
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 220663
    iget v2, p0, LX/1Eb;->c:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 220664
    :cond_0
    :goto_0
    return v0

    .line 220665
    :cond_1
    iget v2, p0, LX/1Eb;->c:F

    const v3, 0x3d4ccccd    # 0.05f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    move v0, v1

    .line 220666
    goto :goto_0

    .line 220667
    :cond_2
    invoke-static {p0}, LX/1Eb;->e(LX/1Eb;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 220668
    iget v2, p0, LX/1Eb;->c:F

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final c()J
    .locals 10

    .prologue
    .line 220669
    const-wide/32 v4, 0x124f80

    const-wide/32 v2, 0x927c0

    const-wide/32 v6, 0x2bf20

    .line 220670
    iget v8, p0, LX/1Eb;->c:F

    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_1

    .line 220671
    :cond_0
    :goto_0
    move-wide v0, v2

    .line 220672
    return-wide v0

    .line 220673
    :cond_1
    iget v8, p0, LX/1Eb;->c:F

    const v9, 0x3d4ccccd    # 0.05f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    move-wide v2, v4

    .line 220674
    goto :goto_0

    .line 220675
    :cond_2
    invoke-static {p0}, LX/1Eb;->e(LX/1Eb;)Z

    move-result v8

    if-eqz v8, :cond_3

    move-wide v2, v6

    .line 220676
    goto :goto_0

    .line 220677
    :cond_3
    iget v8, p0, LX/1Eb;->c:F

    const v9, 0x3e4ccccd    # 0.2f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_4

    move-wide v2, v4

    .line 220678
    goto :goto_0

    .line 220679
    :cond_4
    iget v4, p0, LX/1Eb;->c:F

    const v5, 0x3e99999a    # 0.3f

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    move-wide v2, v6

    .line 220680
    goto :goto_0
.end method
