.class public final enum LX/0oE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0oE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0oE;

.field public static final enum BOOLEAN:LX/0oE;

.field public static final enum DOUBLE:LX/0oE;

.field public static final enum LONG:LX/0oE;

.field public static final enum NULL:LX/0oE;

.field public static final enum STRING:LX/0oE;

.field private static final sValues:[LX/0oE;


# instance fields
.field private final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140007
    new-instance v0, LX/0oE;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v2, v2}, LX/0oE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0oE;->NULL:LX/0oE;

    .line 140008
    new-instance v0, LX/0oE;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v3, v3}, LX/0oE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0oE;->BOOLEAN:LX/0oE;

    .line 140009
    new-instance v0, LX/0oE;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v4, v4}, LX/0oE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0oE;->LONG:LX/0oE;

    .line 140010
    new-instance v0, LX/0oE;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v5, v5}, LX/0oE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0oE;->STRING:LX/0oE;

    .line 140011
    new-instance v0, LX/0oE;

    const-string v1, "DOUBLE"

    invoke-direct {v0, v1, v6, v6}, LX/0oE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0oE;->DOUBLE:LX/0oE;

    .line 140012
    const/4 v0, 0x5

    new-array v0, v0, [LX/0oE;

    sget-object v1, LX/0oE;->NULL:LX/0oE;

    aput-object v1, v0, v2

    sget-object v1, LX/0oE;->BOOLEAN:LX/0oE;

    aput-object v1, v0, v3

    sget-object v1, LX/0oE;->LONG:LX/0oE;

    aput-object v1, v0, v4

    sget-object v1, LX/0oE;->STRING:LX/0oE;

    aput-object v1, v0, v5

    sget-object v1, LX/0oE;->DOUBLE:LX/0oE;

    aput-object v1, v0, v6

    sput-object v0, LX/0oE;->$VALUES:[LX/0oE;

    .line 140013
    invoke-static {}, LX/0oE;->values()[LX/0oE;

    move-result-object v0

    sput-object v0, LX/0oE;->sValues:[LX/0oE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 139998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139999
    iput p3, p0, LX/0oE;->mValue:I

    .line 140000
    return-void
.end method

.method public static valueOf(I)LX/0oE;
    .locals 1

    .prologue
    .line 140001
    if-ltz p0, :cond_0

    sget-object v0, LX/0oE;->sValues:[LX/0oE;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 140002
    sget-object v0, LX/0oE;->sValues:[LX/0oE;

    aget-object v0, v0, p0

    .line 140003
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0oE;->NULL:LX/0oE;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0oE;
    .locals 1

    .prologue
    .line 140004
    const-class v0, LX/0oE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0oE;

    return-object v0
.end method

.method public static values()[LX/0oE;
    .locals 1

    .prologue
    .line 140005
    sget-object v0, LX/0oE;->$VALUES:[LX/0oE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0oE;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 140006
    iget v0, p0, LX/0oE;->mValue:I

    return v0
.end method
