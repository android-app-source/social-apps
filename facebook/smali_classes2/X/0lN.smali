.class public final LX/0lN;
.super LX/0lO;
.source ""


# static fields
.field private static final m:[LX/0lP;


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final c:LX/0lU;

.field public final d:LX/0m5;

.field public final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public f:LX/0lP;

.field public g:Z

.field public h:LX/2Vc;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Vc;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2As;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Am;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129145
    const/4 v0, 0x0

    new-array v0, v0, [LX/0lP;

    sput-object v0, LX/0lN;->m:[LX/0lP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Ljava/util/List;LX/0lU;LX/0m5;LX/0lP;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<*>;>;",
            "LX/0lU;",
            "LX/0m5;",
            "LX/0lP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129146
    invoke-direct {p0}, LX/0lO;-><init>()V

    .line 129147
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lN;->g:Z

    .line 129148
    iput-object p1, p0, LX/0lN;->a:Ljava/lang/Class;

    .line 129149
    iput-object p2, p0, LX/0lN;->b:Ljava/util/List;

    .line 129150
    iput-object p3, p0, LX/0lN;->c:LX/0lU;

    .line 129151
    iput-object p4, p0, LX/0lN;->d:LX/0m5;

    .line 129152
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    .line 129153
    iput-object p5, p0, LX/0lN;->f:LX/0lP;

    .line 129154
    return-void

    .line 129155
    :cond_0
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    iget-object v1, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-interface {v0, v1}, LX/0m5;->d(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lU;",
            "LX/0m5;",
            ")",
            "LX/0lN;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 129156
    new-instance v0, LX/0lN;

    invoke-static {p0, v5}, LX/1Xw;->a(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/0lN;-><init>(Ljava/lang/Class;Ljava/util/List;LX/0lU;LX/0m5;LX/0lP;)V

    return-object v0
.end method

.method private a([Ljava/lang/annotation/Annotation;)LX/0lP;
    .locals 1

    .prologue
    .line 129157
    new-instance v0, LX/0lP;

    invoke-direct {v0}, LX/0lP;-><init>()V

    .line 129158
    invoke-direct {p0, v0, p1}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    .line 129159
    return-object v0
.end method

.method private a(Ljava/lang/reflect/Field;)LX/2Am;
    .locals 2

    .prologue
    .line 129160
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129161
    new-instance v0, LX/2Am;

    invoke-static {}, LX/0lN;->r()LX/0lP;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/2Am;-><init>(Ljava/lang/reflect/Field;LX/0lP;)V

    .line 129162
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2Am;

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-direct {p0, v1}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/2Am;-><init>(Ljava/lang/reflect/Field;LX/0lP;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Method;)LX/2At;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129163
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129164
    new-instance v0, LX/2At;

    invoke-static {}, LX/0lN;->r()LX/0lP;

    move-result-object v1

    invoke-direct {v0, p1, v1, v2}, LX/2At;-><init>(Ljava/lang/reflect/Method;LX/0lP;[LX/0lP;)V

    .line 129165
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2At;

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-direct {p0, v1}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v1

    invoke-direct {v0, p1, v1, v2}, LX/2At;-><init>(Ljava/lang/reflect/Method;LX/0lP;[LX/0lP;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Constructor;Z)LX/2Vc;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<*>;Z)",
            "LX/2Vc;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 129166
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129167
    new-instance v0, LX/2Vc;

    invoke-static {}, LX/0lN;->r()LX/0lP;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    array-length v2, v2

    invoke-static {v2}, LX/0lN;->a(I)[LX/0lP;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/2Vc;-><init>(Ljava/lang/reflect/Constructor;LX/0lP;[LX/0lP;)V

    .line 129168
    :goto_0
    return-object v0

    .line 129169
    :cond_0
    if-eqz p2, :cond_1

    .line 129170
    new-instance v0, LX/2Vc;

    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v2

    invoke-direct {p0, v2}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v2

    invoke-direct {v0, p1, v2, v1}, LX/2Vc;-><init>(Ljava/lang/reflect/Constructor;LX/0lP;[LX/0lP;)V

    goto :goto_0

    .line 129171
    :cond_1
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v0

    .line 129172
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    array-length v2, v2

    .line 129173
    array-length v3, v0

    if-eq v2, v3, :cond_3

    .line 129174
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v3

    .line 129175
    invoke-virtual {v3}, Ljava/lang/Class;->isEnum()Z

    move-result v4

    if-eqz v4, :cond_2

    array-length v4, v0

    add-int/lit8 v4, v4, 0x2

    if-ne v2, v4, :cond_2

    .line 129176
    array-length v1, v0

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [[Ljava/lang/annotation/Annotation;

    .line 129177
    const/4 v3, 0x2

    array-length v4, v0

    invoke-static {v0, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129178
    invoke-direct {p0, v1}, LX/0lN;->a([[Ljava/lang/annotation/Annotation;)[LX/0lP;

    move-result-object v0

    .line 129179
    :goto_1
    if-nez v0, :cond_4

    .line 129180
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Internal error: constructor for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has mismatch: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " parameters; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v1, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sets of annotations"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129181
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Class;->isMemberClass()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 129182
    array-length v3, v0

    add-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_5

    .line 129183
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [[Ljava/lang/annotation/Annotation;

    .line 129184
    const/4 v3, 0x1

    array-length v4, v0

    invoke-static {v0, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129185
    invoke-direct {p0, v1}, LX/0lN;->a([[Ljava/lang/annotation/Annotation;)[LX/0lP;

    move-result-object v0

    goto :goto_1

    .line 129186
    :cond_3
    invoke-direct {p0, v0}, LX/0lN;->a([[Ljava/lang/annotation/Annotation;)[LX/0lP;

    move-result-object v0

    .line 129187
    :cond_4
    new-instance v1, LX/2Vc;

    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v2

    invoke-direct {p0, v2}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, LX/2Vc;-><init>(Ljava/lang/reflect/Constructor;LX/0lP;[LX/0lP;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_1
.end method

.method private a(Ljava/lang/Class;Ljava/util/Map;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2Am;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2Am;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129188
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    .line 129189
    if-eqz v2, :cond_4

    .line 129190
    invoke-direct {p0, v2, p2}, LX/0lN;->a(Ljava/lang/Class;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 129191
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 129192
    invoke-static {v5}, LX/0lN;->b(Ljava/lang/reflect/Field;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 129193
    if-nez v0, :cond_0

    .line 129194
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 129195
    :cond_0
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5}, LX/0lN;->a(Ljava/lang/reflect/Field;)LX/2Am;

    move-result-object v5

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129196
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129197
    :cond_2
    iget-object v1, p0, LX/0lN;->d:LX/0m5;

    if-eqz v1, :cond_3

    .line 129198
    iget-object v1, p0, LX/0lN;->d:LX/0m5;

    invoke-interface {v1, p1}, LX/0m5;->d(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 129199
    if-eqz v1, :cond_3

    .line 129200
    invoke-direct {p0, v2, v1, v0}, LX/0lN;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;)V

    .line 129201
    :cond_3
    :goto_1
    return-object v0

    :cond_4
    move-object v0, p2

    goto :goto_1
.end method

.method private a(LX/0lP;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lP;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 129202
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    if-eqz v0, :cond_0

    .line 129203
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    invoke-interface {v0, p2}, LX/0m5;->d(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/0lN;->a(LX/0lP;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 129204
    :cond_0
    return-void
.end method

.method private a(LX/0lP;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lP;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 129128
    if-nez p3, :cond_1

    .line 129129
    :cond_0
    return-void

    .line 129130
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    .line 129131
    invoke-static {p3, p2}, LX/1Xw;->a(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 129132
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    goto :goto_0
.end method

.method private a(LX/0lP;[Ljava/lang/annotation/Annotation;)V
    .locals 6

    .prologue
    .line 129205
    if-eqz p2, :cond_3

    .line 129206
    const/4 v1, 0x0

    .line 129207
    array-length v2, p2

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p2, v1

    .line 129208
    invoke-direct {p0, v3}, LX/0lN;->a(Ljava/lang/annotation/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129209
    if-nez v0, :cond_0

    .line 129210
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 129211
    :cond_0
    invoke-interface {v3}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129212
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129213
    :cond_1
    invoke-virtual {p1, v3}, LX/0lP;->a(Ljava/lang/annotation/Annotation;)V

    goto :goto_1

    .line 129214
    :cond_2
    if-eqz v0, :cond_3

    .line 129215
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/annotation/Annotation;

    .line 129216
    invoke-direct {p0, p1, v0}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    goto :goto_2

    .line 129217
    :cond_3
    return-void
.end method

.method private a(LX/2An;[Ljava/lang/annotation/Annotation;)V
    .locals 6

    .prologue
    .line 129218
    if-eqz p2, :cond_3

    .line 129219
    const/4 v1, 0x0

    .line 129220
    array-length v2, p2

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p2, v1

    .line 129221
    invoke-direct {p0, v3}, LX/0lN;->a(Ljava/lang/annotation/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129222
    if-nez v0, :cond_0

    .line 129223
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 129224
    :cond_0
    invoke-interface {v3}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129225
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129226
    :cond_1
    invoke-virtual {p1, v3}, LX/2An;->b(Ljava/lang/annotation/Annotation;)V

    goto :goto_1

    .line 129227
    :cond_2
    if-eqz v0, :cond_3

    .line 129228
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/annotation/Annotation;

    .line 129229
    invoke-direct {p0, p1, v0}, LX/0lN;->a(LX/2An;[Ljava/lang/annotation/Annotation;)V

    goto :goto_2

    .line 129230
    :cond_3
    return-void
.end method

.method private a(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2As;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2As;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 129231
    if-eqz p3, :cond_0

    .line 129232
    invoke-direct {p0, p1, p2, p3, p4}, LX/0lN;->b(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V

    .line 129233
    :cond_0
    if-nez p1, :cond_2

    .line 129234
    :cond_1
    return-void

    .line 129235
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 129236
    invoke-static {v4}, LX/0lN;->c(Ljava/lang/reflect/Method;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 129237
    invoke-virtual {p2, v4}, LX/2As;->b(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v5

    .line 129238
    if-nez v5, :cond_4

    .line 129239
    invoke-direct {p0, v4}, LX/0lN;->a(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v5

    .line 129240
    invoke-virtual {p2, v5}, LX/2As;->a(LX/2At;)V

    .line 129241
    invoke-virtual {p4, v4}, LX/2As;->a(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v4

    .line 129242
    if-eqz v4, :cond_3

    .line 129243
    iget-object v6, v4, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v4, v6

    .line 129244
    invoke-direct {p0, v4, v5, v1}, LX/0lN;->a(Ljava/lang/reflect/Method;LX/2At;Z)V

    .line 129245
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129246
    :cond_4
    invoke-direct {p0, v4, v5}, LX/0lN;->a(Ljava/lang/reflect/Method;LX/2At;)V

    .line 129247
    invoke-virtual {v5}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->isInterface()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->isInterface()Z

    move-result v6

    if-nez v6, :cond_3

    .line 129248
    invoke-virtual {v5, v4}, LX/2At;->a(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/2As;->a(LX/2At;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2Am;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129250
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129251
    invoke-static {p2, p1, v0}, LX/1Xw;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    .line 129252
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 129253
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 129254
    invoke-static {v5}, LX/0lN;->b(Ljava/lang/reflect/Field;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129255
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    .line 129256
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Am;

    .line 129257
    if-eqz v0, :cond_1

    .line 129258
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v5

    invoke-direct {p0, v0, v5}, LX/0lN;->b(LX/2An;[Ljava/lang/annotation/Annotation;)V

    .line 129259
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 129260
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/reflect/Constructor;LX/2Vc;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<*>;",
            "LX/2Vc;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 129261
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/0lN;->b(LX/2An;[Ljava/lang/annotation/Annotation;)V

    .line 129262
    if-eqz p3, :cond_1

    .line 129263
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v3

    .line 129264
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 129265
    aget-object v5, v3, v2

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 129266
    invoke-virtual {p2, v2, v7}, LX/2Au;->a(ILjava/lang/annotation/Annotation;)V

    .line 129267
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129268
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 129269
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/reflect/Method;LX/2At;)V
    .locals 1

    .prologue
    .line 129270
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/0lN;->a(LX/2An;[Ljava/lang/annotation/Annotation;)V

    .line 129271
    return-void
.end method

.method private a(Ljava/lang/reflect/Method;LX/2At;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 129272
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/0lN;->b(LX/2An;[Ljava/lang/annotation/Annotation;)V

    .line 129273
    if-eqz p3, :cond_1

    .line 129274
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v3

    .line 129275
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 129276
    aget-object v5, v3, v2

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 129277
    invoke-virtual {p2, v2, v7}, LX/2Au;->a(ILjava/lang/annotation/Annotation;)V

    .line 129278
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129279
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 129280
    :cond_1
    return-void
.end method

.method private final a(Ljava/lang/annotation/Annotation;)Z
    .locals 1

    .prologue
    .line 129281
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    invoke-virtual {v0, p1}, LX/0lU;->a(Ljava/lang/annotation/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(I)[LX/0lP;
    .locals 3

    .prologue
    .line 129282
    if-nez p0, :cond_1

    .line 129283
    sget-object v0, LX/0lN;->m:[LX/0lP;

    .line 129284
    :cond_0
    return-object v0

    .line 129285
    :cond_1
    new-array v0, p0, [LX/0lP;

    .line 129286
    const/4 v1, 0x0

    :goto_0
    if-ge v1, p0, :cond_0

    .line 129287
    invoke-static {}, LX/0lN;->r()LX/0lP;

    move-result-object v2

    aput-object v2, v0, v1

    .line 129288
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a([[Ljava/lang/annotation/Annotation;)[LX/0lP;
    .locals 4

    .prologue
    .line 129289
    array-length v1, p1

    .line 129290
    new-array v2, v1, [LX/0lP;

    .line 129291
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 129292
    aget-object v3, p1, v0

    invoke-direct {p0, v3}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v3

    aput-object v3, v2, v0

    .line 129293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129294
    :cond_0
    return-object v2
.end method

.method public static b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lU;",
            "LX/0m5;",
            ")",
            "LX/0lN;"
        }
    .end annotation

    .prologue
    .line 129295
    new-instance v0, LX/0lN;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/0lN;-><init>(Ljava/lang/Class;Ljava/util/List;LX/0lU;LX/0m5;LX/0lP;)V

    return-object v0
.end method

.method private b(Ljava/lang/reflect/Method;)LX/2At;
    .locals 3

    .prologue
    .line 129296
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129297
    new-instance v0, LX/2At;

    invoke-static {}, LX/0lN;->r()LX/0lP;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    array-length v2, v2

    invoke-static {v2}, LX/0lN;->a(I)[LX/0lP;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/2At;-><init>(Ljava/lang/reflect/Method;LX/0lP;[LX/0lP;)V

    .line 129298
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2At;

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-direct {p0, v1}, LX/0lN;->a([Ljava/lang/annotation/Annotation;)LX/0lP;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v2

    invoke-direct {p0, v2}, LX/0lN;->a([[Ljava/lang/annotation/Annotation;)[LX/0lP;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/2At;-><init>(Ljava/lang/reflect/Method;LX/0lP;[LX/0lP;)V

    goto :goto_0
.end method

.method private b(LX/2An;[Ljava/lang/annotation/Annotation;)V
    .locals 6

    .prologue
    .line 129299
    if-eqz p2, :cond_3

    .line 129300
    const/4 v1, 0x0

    .line 129301
    array-length v2, p2

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p2, v1

    .line 129302
    invoke-direct {p0, v3}, LX/0lN;->a(Ljava/lang/annotation/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129303
    if-nez v0, :cond_0

    .line 129304
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 129305
    :cond_0
    invoke-interface {v3}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129306
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129307
    :cond_1
    invoke-virtual {p1, v3}, LX/2An;->a(Ljava/lang/annotation/Annotation;)V

    goto :goto_1

    .line 129308
    :cond_2
    if-eqz v0, :cond_3

    .line 129309
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/annotation/Annotation;

    .line 129310
    invoke-direct {p0, p1, v0}, LX/0lN;->b(LX/2An;[Ljava/lang/annotation/Annotation;)V

    goto :goto_2

    .line 129311
    :cond_3
    return-void
.end method

.method private b(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2As;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2As;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129134
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129135
    invoke-static {p3, p1, v0}, LX/1Xw;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    .line 129136
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 129137
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 129138
    invoke-static {v4}, LX/0lN;->c(Ljava/lang/reflect/Method;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 129139
    invoke-virtual {p2, v4}, LX/2As;->b(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v5

    .line 129140
    if-eqz v5, :cond_2

    .line 129141
    invoke-direct {p0, v4, v5}, LX/0lN;->a(Ljava/lang/reflect/Method;LX/2At;)V

    .line 129142
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129143
    :cond_2
    invoke-direct {p0, v4}, LX/0lN;->a(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v4

    invoke-virtual {p4, v4}, LX/2As;->a(LX/2At;)V

    goto :goto_1

    .line 129144
    :cond_3
    return-void
.end method

.method private static b(Ljava/lang/reflect/Field;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128961
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->isSynthetic()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128962
    :cond_0
    :goto_0
    return v0

    .line 128963
    :cond_1
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v1

    .line 128964
    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isTransient(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128965
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Ljava/lang/Class;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 128969
    const/4 v3, 0x0

    .line 128970
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    if-nez v0, :cond_1

    move v1, v2

    .line 128971
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v6

    array-length v7, v6

    move v4, v2

    move-object v0, v3

    :goto_1
    if-ge v4, v7, :cond_6

    aget-object v8, v6, v4

    .line 128972
    invoke-virtual {v8}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_2

    .line 128973
    iget-object v3, p0, LX/0lN;->h:LX/2Vc;

    if-eqz v3, :cond_0

    .line 128974
    iget-object v3, p0, LX/0lN;->h:LX/2Vc;

    invoke-direct {p0, v8, v3, v2}, LX/0lN;->a(Ljava/lang/reflect/Constructor;LX/2Vc;Z)V

    .line 128975
    :cond_0
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 128976
    :cond_1
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 128977
    :cond_2
    if-nez v0, :cond_3

    .line 128978
    new-array v3, v1, [LX/2Av;

    move v5, v2

    .line 128979
    :goto_3
    if-ge v5, v1, :cond_4

    .line 128980
    new-instance v9, LX/2Av;

    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vc;

    .line 128981
    iget-object p1, v0, LX/2Vc;->_constructor:Ljava/lang/reflect/Constructor;

    move-object v0, p1

    .line 128982
    invoke-direct {v9, v0}, LX/2Av;-><init>(Ljava/lang/reflect/Constructor;)V

    aput-object v9, v3, v5

    .line 128983
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    :cond_3
    move-object v3, v0

    .line 128984
    :cond_4
    new-instance v5, LX/2Av;

    invoke-direct {v5, v8}, LX/2Av;-><init>(Ljava/lang/reflect/Constructor;)V

    move v0, v2

    .line 128985
    :goto_4
    if-ge v0, v1, :cond_7

    .line 128986
    aget-object v9, v3, v0

    invoke-virtual {v5, v9}, LX/2Av;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 128987
    iget-object v5, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vc;

    const/4 v5, 0x1

    invoke-direct {p0, v8, v0, v5}, LX/0lN;->a(Ljava/lang/reflect/Constructor;LX/2Vc;Z)V

    move-object v0, v3

    .line 128988
    goto :goto_2

    .line 128989
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 128990
    :cond_6
    return-void

    :cond_7
    move-object v0, v3

    goto :goto_2
.end method

.method private static c(Ljava/lang/reflect/Method;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128991
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128992
    :cond_0
    :goto_0
    return v0

    .line 128993
    :cond_1
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->isSynthetic()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/reflect/Method;->isBridge()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128994
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    array-length v1, v1

    .line 128995
    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Ljava/lang/Class;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 128996
    const/4 v0, 0x0

    .line 128997
    iget-object v1, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 128998
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v6

    array-length v7, v6

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_4

    aget-object v8, v6, v2

    .line 128999
    invoke-virtual {v8}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 129000
    invoke-virtual {v8}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_5

    .line 129001
    if-nez v0, :cond_0

    .line 129002
    new-array v1, v5, [LX/2Av;

    move v4, v3

    .line 129003
    :goto_1
    if-ge v4, v5, :cond_1

    .line 129004
    new-instance v9, LX/2Av;

    iget-object v0, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 129005
    iget-object p1, v0, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, p1

    .line 129006
    invoke-direct {v9, v0}, LX/2Av;-><init>(Ljava/lang/reflect/Method;)V

    aput-object v9, v1, v4

    .line 129007
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_0
    move-object v1, v0

    .line 129008
    :cond_1
    new-instance v4, LX/2Av;

    invoke-direct {v4, v8}, LX/2Av;-><init>(Ljava/lang/reflect/Method;)V

    move v0, v3

    .line 129009
    :goto_2
    if-ge v0, v5, :cond_2

    .line 129010
    aget-object v9, v1, v0

    invoke-virtual {v4, v9}, LX/2Av;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 129011
    iget-object v4, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    const/4 v4, 0x1

    invoke-direct {p0, v8, v0, v4}, LX/0lN;->a(Ljava/lang/reflect/Method;LX/2At;Z)V

    .line 129012
    :cond_2
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_0

    .line 129013
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 129014
    :cond_4
    return-void

    :cond_5
    move-object v1, v0

    goto :goto_3
.end method

.method private n()V
    .locals 3

    .prologue
    .line 129015
    new-instance v0, LX/0lP;

    invoke-direct {v0}, LX/0lP;-><init>()V

    iput-object v0, p0, LX/0lN;->f:LX/0lP;

    .line 129016
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-eqz v0, :cond_2

    .line 129017
    iget-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 129018
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    iget-object v1, p0, LX/0lN;->a:Ljava/lang/Class;

    iget-object v2, p0, LX/0lN;->e:Ljava/lang/Class;

    invoke-direct {p0, v0, v1, v2}, LX/0lN;->a(LX/0lP;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 129019
    :cond_0
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    iget-object v1, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    .line 129020
    iget-object v0, p0, LX/0lN;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 129021
    iget-object v2, p0, LX/0lN;->f:LX/0lP;

    invoke-direct {p0, v2, v0}, LX/0lN;->a(LX/0lP;Ljava/lang/Class;)V

    .line 129022
    iget-object v2, p0, LX/0lN;->f:LX/0lP;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-direct {p0, v2, v0}, LX/0lN;->a(LX/0lP;[Ljava/lang/annotation/Annotation;)V

    goto :goto_0

    .line 129023
    :cond_1
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    const-class v1, Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, LX/0lN;->a(LX/0lP;Ljava/lang/Class;)V

    .line 129024
    :cond_2
    return-void
.end method

.method private o()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 129025
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 129026
    array-length v5, v4

    move v3, v1

    move-object v0, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 129027
    invoke-virtual {v6}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_0

    .line 129028
    invoke-direct {p0, v6, v9}, LX/0lN;->a(Ljava/lang/reflect/Constructor;Z)LX/2Vc;

    move-result-object v6

    iput-object v6, p0, LX/0lN;->h:LX/2Vc;

    .line 129029
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 129030
    :cond_0
    if-nez v0, :cond_1

    .line 129031
    new-instance v0, Ljava/util/ArrayList;

    const/16 v7, 0xa

    array-length v8, v4

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 129032
    :cond_1
    invoke-direct {p0, v6, v1}, LX/0lN;->a(Ljava/lang/reflect/Constructor;Z)LX/2Vc;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129033
    :cond_2
    if-nez v0, :cond_6

    .line 129034
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0lN;->i:Ljava/util/List;

    .line 129035
    :goto_2
    iget-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    if-eqz v0, :cond_4

    .line 129036
    iget-object v0, p0, LX/0lN;->h:LX/2Vc;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 129037
    :cond_3
    iget-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    invoke-direct {p0, v0}, LX/0lN;->c(Ljava/lang/Class;)V

    .line 129038
    :cond_4
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-eqz v0, :cond_7

    .line 129039
    iget-object v0, p0, LX/0lN;->h:LX/2Vc;

    if-eqz v0, :cond_5

    .line 129040
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    iget-object v3, p0, LX/0lN;->h:LX/2Vc;

    invoke-virtual {v0, v3}, LX/0lU;->c(LX/2An;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 129041
    iput-object v2, p0, LX/0lN;->h:LX/2Vc;

    .line 129042
    :cond_5
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 129043
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_3
    add-int/lit8 v3, v0, -0x1

    if-ltz v3, :cond_7

    .line 129044
    iget-object v4, p0, LX/0lN;->c:LX/0lU;

    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2An;

    invoke-virtual {v4, v0}, LX/0lU;->c(LX/2An;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 129045
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v3

    goto :goto_3

    .line 129046
    :cond_6
    iput-object v0, p0, LX/0lN;->i:Ljava/util/List;

    goto :goto_2

    .line 129047
    :cond_7
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    move-object v0, v2

    :goto_4
    if-ge v1, v4, :cond_a

    aget-object v2, v3, v1

    .line 129048
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v5

    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 129049
    if-nez v0, :cond_8

    .line 129050
    new-instance v0, Ljava/util/ArrayList;

    const/16 v5, 0x8

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 129051
    :cond_8
    invoke-direct {p0, v2}, LX/0lN;->b(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129052
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 129053
    :cond_a
    if-nez v0, :cond_c

    .line 129054
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0lN;->j:Ljava/util/List;

    .line 129055
    :cond_b
    iput-boolean v9, p0, LX/0lN;->g:Z

    .line 129056
    return-void

    .line 129057
    :cond_c
    iput-object v0, p0, LX/0lN;->j:Ljava/util/List;

    .line 129058
    iget-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    if-eqz v0, :cond_d

    .line 129059
    iget-object v0, p0, LX/0lN;->e:Ljava/lang/Class;

    invoke-direct {p0, v0}, LX/0lN;->d(Ljava/lang/Class;)V

    .line 129060
    :cond_d
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-eqz v0, :cond_b

    .line 129061
    iget-object v0, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_5
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_b

    .line 129062
    iget-object v2, p0, LX/0lN;->c:LX/0lU;

    iget-object v0, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2An;

    invoke-virtual {v2, v0}, LX/0lU;->c(LX/2An;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 129063
    iget-object v0, p0, LX/0lN;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_5

    :cond_e
    move v0, v1

    goto :goto_5

    :cond_f
    move v0, v3

    goto/16 :goto_3
.end method

.method private p()V
    .locals 5

    .prologue
    .line 129064
    new-instance v0, LX/2As;

    invoke-direct {v0}, LX/2As;-><init>()V

    iput-object v0, p0, LX/0lN;->k:LX/2As;

    .line 129065
    new-instance v2, LX/2As;

    invoke-direct {v2}, LX/2As;-><init>()V

    .line 129066
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/0lN;->k:LX/2As;

    iget-object v3, p0, LX/0lN;->e:Ljava/lang/Class;

    invoke-direct {p0, v0, v1, v3, v2}, LX/0lN;->a(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V

    .line 129067
    iget-object v0, p0, LX/0lN;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 129068
    iget-object v1, p0, LX/0lN;->d:LX/0m5;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 129069
    :goto_1
    iget-object v4, p0, LX/0lN;->k:LX/2As;

    invoke-direct {p0, v0, v4, v1, v2}, LX/0lN;->a(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V

    goto :goto_0

    .line 129070
    :cond_0
    iget-object v1, p0, LX/0lN;->d:LX/0m5;

    invoke-interface {v1, v0}, LX/0m5;->d(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    goto :goto_1

    .line 129071
    :cond_1
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    if-eqz v0, :cond_2

    .line 129072
    iget-object v0, p0, LX/0lN;->d:LX/0m5;

    const-class v1, Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/0m5;->d(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 129073
    if-eqz v0, :cond_2

    .line 129074
    iget-object v1, p0, LX/0lN;->a:Ljava/lang/Class;

    iget-object v3, p0, LX/0lN;->k:LX/2As;

    invoke-direct {p0, v1, v3, v0, v2}, LX/0lN;->b(Ljava/lang/Class;LX/2As;Ljava/lang/Class;LX/2As;)V

    .line 129075
    :cond_2
    iget-object v0, p0, LX/0lN;->c:LX/0lU;

    if-eqz v0, :cond_4

    .line 129076
    invoke-virtual {v2}, LX/2As;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 129077
    invoke-virtual {v2}, LX/2As;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 129078
    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 129079
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 129080
    :try_start_0
    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/2At;->n()[Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 129081
    if-eqz v2, :cond_3

    .line 129082
    invoke-direct {p0, v2}, LX/0lN;->a(Ljava/lang/reflect/Method;)LX/2At;

    move-result-object v2

    .line 129083
    iget-object v3, v0, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v3

    .line 129084
    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, LX/0lN;->a(Ljava/lang/reflect/Method;LX/2At;Z)V

    .line 129085
    iget-object v0, p0, LX/0lN;->k:LX/2As;

    invoke-virtual {v0, v2}, LX/2As;->a(LX/2At;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 129086
    :catch_0
    goto :goto_2

    .line 129087
    :cond_4
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 129088
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0lN;->a(Ljava/lang/Class;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 129089
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 129090
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0lN;->l:Ljava/util/List;

    .line 129091
    :goto_0
    return-void

    .line 129092
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/0lN;->l:Ljava/util/List;

    .line 129093
    iget-object v1, p0, LX/0lN;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private static r()LX/0lP;
    .locals 1

    .prologue
    .line 129094
    new-instance v0, LX/0lP;

    invoke-direct {v0}, LX/0lP;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/2At;"
        }
    .end annotation

    .prologue
    .line 129095
    iget-object v0, p0, LX/0lN;->k:LX/2As;

    if-nez v0, :cond_0

    .line 129096
    invoke-direct {p0}, LX/0lN;->p()V

    .line 129097
    :cond_0
    iget-object v0, p0, LX/0lN;->k:LX/2As;

    invoke-virtual {v0, p1, p2}, LX/2As;->a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 128966
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    if-nez v0, :cond_0

    .line 128967
    invoke-direct {p0}, LX/0lN;->n()V

    .line 128968
    :cond_0
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    invoke-virtual {v0, p1}, LX/0lP;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a()Ljava/lang/reflect/AnnotatedElement;
    .locals 1

    .prologue
    .line 129098
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    move-object v0, v0

    .line 129099
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129100
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 129101
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129102
    iget-object v0, p0, LX/0lN;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public final e()LX/0lP;
    .locals 1

    .prologue
    .line 129103
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    if-nez v0, :cond_0

    .line 129104
    invoke-direct {p0}, LX/0lN;->n()V

    .line 129105
    :cond_0
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    return-object v0
.end method

.method public final g()LX/0lQ;
    .locals 1

    .prologue
    .line 129106
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    if-nez v0, :cond_0

    .line 129107
    invoke-direct {p0}, LX/0lN;->n()V

    .line 129108
    :cond_0
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 129109
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    if-nez v0, :cond_0

    .line 129110
    invoke-direct {p0}, LX/0lN;->n()V

    .line 129111
    :cond_0
    iget-object v0, p0, LX/0lN;->f:LX/0lP;

    invoke-virtual {v0}, LX/0lP;->a()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/2Vc;
    .locals 1

    .prologue
    .line 129112
    iget-boolean v0, p0, LX/0lN;->g:Z

    if-nez v0, :cond_0

    .line 129113
    invoke-direct {p0}, LX/0lN;->o()V

    .line 129114
    :cond_0
    iget-object v0, p0, LX/0lN;->h:LX/2Vc;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Vc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129115
    iget-boolean v0, p0, LX/0lN;->g:Z

    if-nez v0, :cond_0

    .line 129116
    invoke-direct {p0}, LX/0lN;->o()V

    .line 129117
    :cond_0
    iget-object v0, p0, LX/0lN;->i:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129118
    iget-boolean v0, p0, LX/0lN;->g:Z

    if-nez v0, :cond_0

    .line 129119
    invoke-direct {p0}, LX/0lN;->o()V

    .line 129120
    :cond_0
    iget-object v0, p0, LX/0lN;->j:Ljava/util/List;

    return-object v0
.end method

.method public final l()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129121
    iget-object v0, p0, LX/0lN;->k:LX/2As;

    if-nez v0, :cond_0

    .line 129122
    invoke-direct {p0}, LX/0lN;->p()V

    .line 129123
    :cond_0
    iget-object v0, p0, LX/0lN;->k:LX/2As;

    return-object v0
.end method

.method public final m()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Am;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129124
    iget-object v0, p0, LX/0lN;->l:Ljava/util/List;

    if-nez v0, :cond_0

    .line 129125
    invoke-direct {p0}, LX/0lN;->q()V

    .line 129126
    :cond_0
    iget-object v0, p0, LX/0lN;->l:Ljava/util/List;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129127
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[AnnotedClass "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lN;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
