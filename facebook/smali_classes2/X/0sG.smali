.class public LX/0sG;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static b:LX/0QA;

.field public static c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0WV;

.field public static e:LX/0s5;

.field public static f:LX/0sK;

.field private static final g:[Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151385
    const-class v0, LX/0sG;

    sput-object v0, LX/0sG;->a:Ljava/lang/Class;

    .line 151386
    invoke-static {}, LX/0sH;->values()[LX/0sH;

    move-result-object v0

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, LX/0sG;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151384
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 151382
    sget-object v0, LX/0sH;->None:LX/0sH;

    invoke-static {p0, p1, v0}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;LX/0sH;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Boolean;LX/0sH;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 151318
    invoke-virtual {p2}, LX/0sH;->ordinal()I

    move-result v0

    mul-int/lit8 v1, v0, 0x2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    move v1, v0

    .line 151319
    sget-object v0, LX/0sG;->g:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 151320
    if-nez v0, :cond_10

    .line 151321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 151322
    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151323
    sget-object v2, LX/0sG;->h:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 151324
    const-string v2, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "FBAN"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {}, LX/0sG;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "FBAV"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p0}, LX/0sG;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "FBBV"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {p0}, LX/0sG;->d(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "FBDM"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    .line 151325
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 151326
    new-instance v7, Landroid/graphics/Point;

    iget v5, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v7, v5, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 151327
    const-string v5, "window"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 151328
    if-eqz v5, :cond_0

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 151329
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 151330
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "{density="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",width="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v7, Landroid/graphics/Point;->x:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "}"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 151331
    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "FBLC"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    .line 151332
    sget-object v5, LX/0sG;->b:LX/0QA;

    if-nez v5, :cond_1

    .line 151333
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    sput-object v5, LX/0sG;->b:LX/0QA;

    .line 151334
    :cond_1
    sget-object v5, LX/0sG;->b:LX/0QA;

    invoke-static {v5}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    .line 151335
    invoke-virtual {v5}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 151336
    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/0sG;->h:Ljava/lang/String;

    .line 151337
    :cond_2
    sget-object v2, LX/0sG;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151338
    sget-object v2, LX/0sG;->f:LX/0sK;

    if-nez v2, :cond_3

    .line 151339
    sget-object v2, LX/0sG;->b:LX/0QA;

    invoke-static {v2}, LX/0sJ;->a(LX/0QB;)LX/0sK;

    move-result-object v2

    check-cast v2, LX/0sK;

    sput-object v2, LX/0sG;->f:LX/0sK;

    .line 151340
    :cond_3
    sget-object v2, LX/0sG;->f:LX/0sK;

    invoke-virtual {v2}, LX/0sK;->c()I

    move-result v2

    .line 151341
    if-nez v2, :cond_4

    .line 151342
    invoke-static {p0}, LX/0sG;->d(Landroid/content/Context;)I

    move-result v2

    .line 151343
    :cond_4
    move v2, v2

    .line 151344
    const-string v3, "%s/%s;"

    const-string v4, "FBRV"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151345
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 151346
    sget-object v2, LX/0sG;->i:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 151347
    const-string v2, "%s/%s;"

    const-string v3, "FB_FW"

    const-string v4, "2"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/0sG;->i:Ljava/lang/String;

    .line 151348
    :cond_5
    sget-object v2, LX/0sG;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151349
    :cond_6
    sget-object v2, LX/0sG;->j:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 151350
    const-string v2, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;"

    const/16 v3, 0xc

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "FBCR"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 151351
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 151352
    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "FBMF"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "FBBD"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    sget-object v5, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "FBPN"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "FBDV"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    .line 151353
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move-object v5, v5

    .line 151354
    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "FBSV"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    .line 151355
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    move-object v5, v5

    .line 151356
    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/0sG;->j:Ljava/lang/String;

    .line 151357
    :cond_7
    sget-object v2, LX/0sG;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151358
    invoke-static {p2}, LX/0sL;->a(Ljava/lang/Object;)V

    .line 151359
    sget-object v2, LX/0sH;->None:LX/0sH;

    if-eq p2, v2, :cond_8

    .line 151360
    const-string v2, "%s/%s;"

    const-string v3, "FBBK"

    invoke-static {v2, v3, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151361
    :cond_8
    sget-object v2, LX/0sG;->b:LX/0QA;

    if-nez v2, :cond_9

    .line 151362
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    sput-object v2, LX/0sG;->b:LX/0QA;

    .line 151363
    :cond_9
    sget-object v2, LX/0sG;->c:LX/0Or;

    if-nez v2, :cond_a

    .line 151364
    sget-object v2, LX/0sG;->b:LX/0QA;

    const/16 v3, 0x15fa

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    sput-object v2, LX/0sG;->c:LX/0Or;

    .line 151365
    :cond_a
    sget-object v2, LX/0sG;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, LX/0sG;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 151366
    if-eqz v2, :cond_c

    .line 151367
    sget-object v3, LX/0sG;->k:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 151368
    const-string v3, "%s/%s;"

    const-string v4, "FBOP"

    invoke-static {v2}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/0sG;->k:Ljava/lang/String;

    .line 151369
    :cond_b
    sget-object v2, LX/0sG;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151370
    :cond_c
    sget-object v2, LX/0sG;->e:LX/0s5;

    if-nez v2, :cond_d

    .line 151371
    sget-object v2, LX/0sG;->b:LX/0QA;

    invoke-static {v2}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v2

    check-cast v2, LX/0s5;

    sput-object v2, LX/0sG;->e:LX/0s5;

    .line 151372
    :cond_d
    sget-object v2, LX/0sG;->e:LX/0s5;

    invoke-virtual {v2}, LX/0s5;->c()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 151373
    const-string v3, "%s/%s;"

    const-string v4, "FBAT"

    sget-object v2, LX/0sG;->e:LX/0s5;

    invoke-virtual {v2}, LX/0s5;->d()Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v2, "2"

    :goto_1
    invoke-static {v3, v4, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151374
    :cond_e
    sget-object v2, LX/0sG;->l:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 151375
    const-string v2, "%s/%s:%s;"

    const-string v3, "FBCA"

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v4}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v5}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/0sG;->l:Ljava/lang/String;

    .line 151376
    :cond_f
    sget-object v2, LX/0sG;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151377
    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151378
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 151379
    sget-object v2, LX/0sG;->g:[Ljava/lang/String;

    aput-object v0, v2, v1

    .line 151380
    :cond_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 151381
    :cond_12
    const-string v2, "1"

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 151387
    invoke-static {p0}, LX/0YN;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151388
    const-string v1, "/"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 151315
    sput-object v1, LX/0sG;->f:LX/0sK;

    .line 151316
    sget-object v0, LX/0sG;->g:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151317
    return-void
.end method

.method private static b(Landroid/content/Context;)LX/0WV;
    .locals 1

    .prologue
    .line 151310
    sget-object v0, LX/0sG;->b:LX/0QA;

    if-nez v0, :cond_0

    .line 151311
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    sput-object v0, LX/0sG;->b:LX/0QA;

    .line 151312
    :cond_0
    sget-object v0, LX/0sG;->d:LX/0WV;

    if-nez v0, :cond_1

    .line 151313
    sget-object v0, LX/0sG;->b:LX/0QA;

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v0

    check-cast v0, LX/0WV;

    sput-object v0, LX/0sG;->d:LX/0WV;

    .line 151314
    :cond_1
    sget-object v0, LX/0sG;->d:LX/0WV;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151308
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 151309
    if-eqz v0, :cond_0

    const-string v0, "AtWorkForAndroid"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "FB4A"

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 151306
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151307
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 151305
    invoke-static {p0}, LX/0sG;->b(Landroid/content/Context;)LX/0WV;

    move-result-object v0

    invoke-virtual {v0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 151304
    invoke-static {p0}, LX/0sG;->b(Landroid/content/Context;)LX/0WV;

    move-result-object v0

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    return v0
.end method
