.class public LX/1bg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1bh;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/1o9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/1bd;

.field private final d:LX/1bZ;

.field private final e:LX/1bh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:I

.field public final h:Ljava/lang/Object;

.field public final i:J


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/1o9;LX/1bd;LX/1bZ;LX/1bh;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p2    # LX/1o9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1bh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280848
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1bg;->a:Ljava/lang/String;

    .line 280849
    iput-object p2, p0, LX/1bg;->b:LX/1o9;

    .line 280850
    iput-object p3, p0, LX/1bg;->c:LX/1bd;

    .line 280851
    iput-object p4, p0, LX/1bg;->d:LX/1bZ;

    .line 280852
    iput-object p5, p0, LX/1bg;->e:LX/1bh;

    .line 280853
    iput-object p6, p0, LX/1bg;->f:Ljava/lang/String;

    .line 280854
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1o9;->hashCode()I

    move-result v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3}, LX/1bd;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/1bg;->d:LX/1bZ;

    iget-object v4, p0, LX/1bg;->e:LX/1bh;

    move-object v5, p6

    const/4 p6, 0x0

    .line 280855
    if-nez v0, :cond_1

    move p1, p6

    :goto_1
    if-nez v1, :cond_2

    move p2, p6

    :goto_2
    if-nez v2, :cond_3

    move p3, p6

    :goto_3
    if-nez v3, :cond_4

    move p4, p6

    :goto_4
    if-nez v4, :cond_5

    move p5, p6

    :goto_5
    if-nez v5, :cond_6

    .line 280856
    :goto_6
    add-int/lit8 v0, p1, 0x1f

    .line 280857
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p2

    .line 280858
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p3

    .line 280859
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p4

    .line 280860
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p5

    .line 280861
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p6

    .line 280862
    move p1, v0

    .line 280863
    move v0, p1

    .line 280864
    iput v0, p0, LX/1bg;->g:I

    .line 280865
    iput-object p7, p0, LX/1bg;->h:Ljava/lang/Object;

    .line 280866
    sget-object v0, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v0, v0

    .line 280867
    invoke-virtual {v0}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1bg;->i:J

    .line 280868
    return-void

    .line 280869
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result p1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result p2

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result p3

    goto :goto_3

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result p4

    goto :goto_4

    :cond_5
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result p5

    goto :goto_5

    :cond_6
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result p6

    goto :goto_6
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280846
    iget-object v0, p0, LX/1bg;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 280845
    invoke-virtual {p0}, LX/1bg;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280839
    instance-of v1, p1, LX/1bg;

    if-nez v1, :cond_1

    .line 280840
    :cond_0
    :goto_0
    return v0

    .line 280841
    :cond_1
    check-cast p1, LX/1bg;

    .line 280842
    iget v1, p0, LX/1bg;->g:I

    iget v2, p1, LX/1bg;->g:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/1bg;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1bg;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bg;->b:LX/1o9;

    iget-object v2, p1, LX/1bg;->b:LX/1o9;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bg;->c:LX/1bd;

    iget-object v2, p1, LX/1bg;->c:LX/1bd;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bg;->d:LX/1bZ;

    iget-object v2, p1, LX/1bg;->d:LX/1bZ;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bg;->e:LX/1bh;

    iget-object v2, p1, LX/1bg;->e:LX/1bh;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bg;->f:Ljava/lang/String;

    iget-object v2, p1, LX/1bg;->f:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 280844
    iget v0, p0, LX/1bg;->g:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 280843
    const/4 v0, 0x0

    const-string v1, "%s_%s_%s_%s_%s_%s_%d"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/1bg;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/1bg;->b:LX/1o9;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/1bg;->c:LX/1bd;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LX/1bg;->d:LX/1bZ;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, LX/1bg;->e:LX/1bh;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, LX/1bg;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget v4, p0, LX/1bg;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
