.class public final LX/0Qx;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;"
    }
.end annotation


# instance fields
.field public final accessQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile count:I

.field public final keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;"
        }
    .end annotation
.end field

.field public final map:LX/0Qd;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qd",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final maxSegmentWeight:J

.field public modCount:I

.field public final readCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final recencyQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final statsCounter:LX/0QP;

.field public volatile table:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public threshold:I

.field public totalWeight:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final writeQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Qd;IJLX/0QP;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qd",
            "<TK;TV;>;IJ",
            "LX/0QP;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59025
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 59026
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0Qx;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 59027
    iput-object p1, p0, LX/0Qx;->map:LX/0Qd;

    .line 59028
    iput-wide p3, p0, LX/0Qx;->maxSegmentWeight:J

    .line 59029
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QP;

    iput-object v0, p0, LX/0Qx;->statsCounter:LX/0QP;

    .line 59030
    invoke-static {p2}, LX/0Qx;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    .line 59031
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/0Qx;->threshold:I

    .line 59032
    iget-object v3, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v3}, LX/0Qd;->b()Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, LX/0Qx;->threshold:I

    int-to-long v3, v3

    iget-wide v5, p0, LX/0Qx;->maxSegmentWeight:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 59033
    iget v3, p0, LX/0Qx;->threshold:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/0Qx;->threshold:I

    .line 59034
    :cond_0
    iput-object v0, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 59035
    invoke-virtual {p1}, LX/0Qd;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, LX/0Qx;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 59036
    invoke-virtual {p1}, LX/0Qd;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_1
    iput-object v1, p0, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 59037
    invoke-virtual {p1}, LX/0Qd;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, LX/0Qx;->recencyQueue:Ljava/util/Queue;

    .line 59038
    invoke-virtual {p1}, LX/0Qd;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, LX/2MP;

    invoke-direct {v0}, LX/2MP;-><init>()V

    :goto_2
    iput-object v0, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    .line 59039
    invoke-virtual {p1}, LX/0Qd;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, LX/0Qy;

    invoke-direct {v0}, LX/0Qy;-><init>()V

    :goto_3
    iput-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    .line 59040
    return-void

    :cond_2
    move-object v0, v1

    .line 59041
    goto :goto_0

    .line 59042
    :cond_3
    sget-object v0, LX/0Qd;->v:Ljava/util/Queue;

    move-object v0, v0

    .line 59043
    goto :goto_1

    .line 59044
    :cond_4
    sget-object v0, LX/0Qd;->v:Ljava/util/Queue;

    move-object v0, v0

    .line 59045
    goto :goto_2

    .line 59046
    :cond_5
    sget-object v0, LX/0Qd;->v:Ljava/util/Queue;

    move-object v0, v0

    .line 59047
    goto :goto_3
.end method

.method private static a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;
    .locals 1
    .param p2    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;TK;I",
            "LX/0Qf",
            "<TK;TV;>;",
            "LX/0rs;",
            ")",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58973
    invoke-static {p0, p3, p5, p6}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    .line 58974
    iget-object v0, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 58975
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 58976
    invoke-interface {p5}, LX/0Qf;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58977
    const/4 v0, 0x0

    invoke-interface {p5, v0}, LX/0Qf;->a(Ljava/lang/Object;)V

    .line 58978
    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, LX/0Qx;->b(LX/0R1;LX/0R1;)LX/0R1;

    move-result-object p1

    goto :goto_0
.end method

.method private static a(LX/0Qx;Ljava/lang/Object;IJ)LX/0R1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "IJ)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58979
    invoke-static {p0, p1, p2}, LX/0Qx;->d(LX/0Qx;Ljava/lang/Object;I)LX/0R1;

    move-result-object v1

    .line 58980
    if-nez v1, :cond_0

    .line 58981
    :goto_0
    return-object v0

    .line 58982
    :cond_0
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v2, v1, p3, p4}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58983
    invoke-direct {p0, p3, p4}, LX/0Qx;->a(J)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 58984
    goto :goto_0
.end method

.method private static a(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;
    .locals 2
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58985
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->r:LX/0Qo;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p2, p3}, LX/0Qo;->newEntry(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0R1;LX/0R1;)LX/0R1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58986
    invoke-interface {p1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 58987
    :cond_0
    :goto_0
    return-object v0

    .line 58988
    :cond_1
    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    .line 58989
    invoke-interface {v1}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v2

    .line 58990
    if-nez v2, :cond_2

    invoke-interface {v1}, LX/0Qf;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 58991
    :cond_2
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->r:LX/0Qo;

    invoke-virtual {v0, p0, p1, p2}, LX/0Qo;->copyEntry(LX/0Qx;LX/0R1;LX/0R1;)LX/0R1;

    move-result-object v0

    .line 58992
    iget-object v3, p0, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, LX/0Qf;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)LX/0Qf;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0R1;->setValueReference(LX/0Qf;)V

    goto :goto_0
.end method

.method private static a(LX/0Qx;Ljava/lang/Object;IZ)LX/0SO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;IZ)",
            "LX/0SO",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58993
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58994
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v2

    .line 58995
    invoke-static {p0, v2, v3}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58996
    iget-object v4, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58997
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 58998
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    move-object v1, v0

    .line 58999
    :goto_0
    if-eqz v1, :cond_3

    .line 59000
    invoke-interface {v1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 59001
    invoke-interface {v1}, LX/0R1;->getHash()I

    move-result v7

    if-ne v7, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v7, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v7, v7, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v7, p1, v6}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 59002
    invoke-interface {v1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v4

    .line 59003
    invoke-interface {v4}, LX/0Qf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    invoke-interface {v1}, LX/0R1;->getWriteTime()J

    move-result-wide v6

    sub-long/2addr v2, v6

    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-wide v6, v0, LX/0Qd;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    .line 59004
    :cond_0
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59005
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 59006
    :cond_1
    :try_start_1
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 59007
    new-instance v0, LX/0SO;

    invoke-direct {v0, v4}, LX/0SO;-><init>(LX/0Qf;)V

    .line 59008
    invoke-interface {v1, v0}, LX/0R1;->setValueReference(LX/0Qf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59009
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59010
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_1

    .line 59011
    :cond_2
    :try_start_2
    invoke-interface {v1}, LX/0R1;->getNext()LX/0R1;

    move-result-object v1

    goto :goto_0

    .line 59012
    :cond_3
    iget v1, p0, LX/0Qx;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Qx;->modCount:I

    .line 59013
    new-instance v1, LX/0SO;

    invoke-direct {v1}, LX/0SO;-><init>()V

    .line 59014
    invoke-static {p0, p1, p2, v0}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v0

    .line 59015
    invoke-interface {v0, v1}, LX/0R1;->setValueReference(LX/0Qf;)V

    .line 59016
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 59017
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59018
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v1

    goto :goto_1

    .line 59019
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59020
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method private static a(LX/0Qx;LX/0R1;Ljava/lang/Object;ILjava/lang/Object;JLX/0QM;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;TK;ITV;J",
            "LX/0QM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 59021
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0R1;->getWriteTime()J

    move-result-wide v0

    sub-long v0, p5, v0

    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    iget-wide v2, v2, LX/0Qd;->n:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v0

    invoke-interface {v0}, LX/0Qf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59022
    const/4 v0, 0x1

    invoke-direct {p0, p2, p3, p7, v0}, LX/0Qx;->a(Ljava/lang/Object;ILX/0QM;Z)Ljava/lang/Object;

    move-result-object v0

    .line 59023
    if-eqz v0, :cond_0

    move-object p4, v0

    .line 59024
    :cond_0
    return-object p4
.end method

.method private static a(LX/0Qx;LX/0R1;Ljava/lang/Object;LX/0Qf;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;TK;",
            "LX/0Qf",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 59048
    invoke-interface {p3}, LX/0Qf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59049
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59050
    :cond_0
    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Recursive load of: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p2, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 59051
    :try_start_0
    invoke-interface {p3}, LX/0Qf;->e()Ljava/lang/Object;

    move-result-object v0

    .line 59052
    if-nez v0, :cond_2

    .line 59053
    new-instance v0, LX/4w9;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/4w9;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59054
    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-interface {v2, v1}, LX/0QP;->b(I)V

    throw v0

    :cond_1
    move v0, v2

    .line 59055
    goto :goto_0

    .line 59056
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v2, v2, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v2}, LX/0QV;->read()J

    move-result-wide v2

    .line 59057
    invoke-static {p0, p1, v2, v3}, LX/0Qx;->b(LX/0Qx;LX/0R1;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59058
    iget-object v2, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-interface {v2, v1}, LX/0QP;->b(I)V

    return-object v0
.end method

.method private static a(LX/0Qx;Ljava/lang/Object;ILX/0SO;LX/0QM;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0SO",
            "<TK;TV;>;",
            "LX/0QM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 59059
    invoke-virtual {p3, p1, p4}, LX/0SO;->a(Ljava/lang/Object;LX/0QM;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 59060
    invoke-virtual {p0, p1, p2, p3, v0}, LX/0Qx;->a(Ljava/lang/Object;ILX/0SO;Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;ILX/0QM;Z)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0QM",
            "<-TK;TV;>;Z)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 59061
    invoke-static {p0, p1, p2, p4}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;IZ)LX/0SO;

    move-result-object v1

    .line 59062
    if-nez v1, :cond_1

    .line 59063
    :cond_0
    :goto_0
    return-object v0

    .line 59064
    :cond_1
    invoke-virtual {v1, p1, p3}, LX/0SO;->a(Ljava/lang/Object;LX/0QM;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 59065
    new-instance v3, Lcom/google/common/cache/LocalCache$Segment$1;

    move-object v4, p0

    move-object v5, p1

    move v6, p2

    move-object v7, v1

    invoke-direct/range {v3 .. v8}, Lcom/google/common/cache/LocalCache$Segment$1;-><init>(LX/0Qx;Ljava/lang/Object;ILX/0SO;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 59066
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 59067
    invoke-interface {v8, v3, v4}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 59068
    move-object v1, v8

    .line 59069
    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59070
    :try_start_0
    invoke-static {v1}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method private static a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 59071
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 59072
    invoke-virtual {p0}, LX/0Qx;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59073
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/0Qx;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59074
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59075
    :cond_0
    return-void

    .line 59076
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    throw v0
.end method

.method private static a(LX/0Qx;LX/0R1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59077
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59078
    :cond_0
    return-void

    .line 59079
    :cond_1
    invoke-static {p0}, LX/0Qx;->k(LX/0Qx;)V

    .line 59080
    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v0

    invoke-interface {v0}, LX/0Qf;->a()I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, LX/0Qx;->maxSegmentWeight:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 59081
    invoke-interface {p1}, LX/0R1;->getHash()I

    move-result v0

    sget-object v1, LX/0rs;->SIZE:LX/0rs;

    invoke-direct {p0, p1, v0, v1}, LX/0Qx;->a(LX/0R1;ILX/0rs;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59082
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59083
    :cond_2
    iget-wide v0, p0, LX/0Qx;->totalWeight:J

    iget-wide v2, p0, LX/0Qx;->maxSegmentWeight:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 59084
    invoke-direct {p0}, LX/0Qx;->l()LX/0R1;

    move-result-object v0

    .line 59085
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v1

    sget-object v2, LX/0rs;->SIZE:LX/0rs;

    invoke-direct {p0, v0, v1, v2}, LX/0Qx;->a(LX/0R1;ILX/0rs;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59086
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a(LX/0Qx;LX/0R1;LX/0rs;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0rs;",
            ")V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59087
    invoke-interface {p1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, LX/0R1;->getHash()I

    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    invoke-static {p0, v0, v1, p2}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    .line 59088
    return-void
.end method

.method private static a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;TK;TV;J)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59089
    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    .line 59090
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->k:LX/0Ql;

    invoke-interface {v0, p2, p3}, LX/0Ql;->weigh(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    .line 59091
    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Weights must be non-negative"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 59092
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->i:LX/0QX;

    invoke-virtual {v0, p0, p1, p3, v2}, LX/0QX;->referenceValue(LX/0Qx;LX/0R1;Ljava/lang/Object;I)LX/0Qf;

    move-result-object v0

    .line 59093
    invoke-interface {p1, v0}, LX/0R1;->setValueReference(LX/0Qf;)V

    .line 59094
    invoke-static {p0}, LX/0Qx;->k(LX/0Qx;)V

    .line 59095
    iget-wide v4, p0, LX/0Qx;->totalWeight:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/0Qx;->totalWeight:J

    .line 59096
    iget-object v4, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v4}, LX/0Qd;->g()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59097
    invoke-interface {p1, p4, p5}, LX/0R1;->setAccessTime(J)V

    .line 59098
    :cond_0
    iget-object v4, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v4}, LX/0Qd;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 59099
    invoke-interface {p1, p4, p5}, LX/0R1;->setWriteTime(J)V

    .line 59100
    :cond_1
    iget-object v4, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v4, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 59101
    iget-object v4, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v4, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 59102
    invoke-interface {v1, p3}, LX/0Qf;->a(Ljava/lang/Object;)V

    .line 59103
    return-void

    .line 59104
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V
    .locals 4
    .param p0    # LX/0Qx;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/0Qf",
            "<TK;TV;>;",
            "LX/0rs;",
            ")V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59105
    iget-wide v0, p0, LX/0Qx;->totalWeight:J

    invoke-interface {p2}, LX/0Qf;->a()I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0Qx;->totalWeight:J

    .line 59106
    invoke-virtual {p3}, LX/0rs;->wasEvicted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59107
    iget-object v0, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-interface {v0}, LX/0QP;->a()V

    .line 59108
    :cond_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->o:Ljava/util/Queue;

    sget-object v1, LX/0Qd;->v:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    .line 59109
    invoke-interface {p2}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 59110
    new-instance v1, LX/4wi;

    invoke-direct {v1, p1, v0, p3}, LX/4wi;-><init>(Ljava/lang/Object;Ljava/lang/Object;LX/0rs;)V

    move-object v0, v1

    .line 59111
    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v1, v1, LX/0Qd;->o:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 59112
    :cond_1
    return-void
.end method

.method private a(LX/0R1;ILX/0rs;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;I",
            "LX/0rs;",
            ")Z"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59113
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 59114
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 59115
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 59116
    :goto_0
    if-eqz v2, :cond_1

    .line 59117
    if-ne v2, p1, :cond_0

    .line 59118
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 59119
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    move-object v0, p0

    move v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 59120
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 59121
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 59122
    iput v1, p0, LX/0Qx;->count:I

    .line 59123
    const/4 v0, 0x1

    .line 59124
    :goto_1
    return v0

    .line 59125
    :cond_0
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;

    move-result-object v2

    goto :goto_0

    .line 59126
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;ILX/0SO;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0SO",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59127
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 59128
    :try_start_0
    iget-object v3, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 59129
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 59130
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    move-object v2, v0

    .line 59131
    :goto_0
    if-eqz v2, :cond_3

    .line 59132
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 59133
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v6, v6, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v6, p1, v5}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 59134
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    .line 59135
    if-ne v5, p3, :cond_1

    .line 59136
    invoke-virtual {p3}, LX/0SO;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59137
    iget-object v0, p3, LX/0SO;->a:LX/0Qf;

    move-object v0, v0

    .line 59138
    invoke-interface {v2, v0}, LX/0R1;->setValueReference(LX/0Qf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59139
    :goto_1
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59140
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 59141
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v2}, LX/0Qx;->b(LX/0R1;LX/0R1;)LX/0R1;

    move-result-object v0

    .line 59142
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 59143
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59144
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0

    .line 59145
    :cond_1
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59146
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v0, v1

    goto :goto_2

    .line 59147
    :cond_2
    :try_start_2
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 59148
    :cond_3
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59149
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Object;ILX/0SO;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0SO",
            "<TK;TV;>;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 59150
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 59151
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v4

    .line 59152
    invoke-static {p0, v4, v5}, LX/0Qx;->d(LX/0Qx;J)V

    .line 59153
    iget v0, p0, LX/0Qx;->count:I

    add-int/lit8 v7, v0, 0x1

    .line 59154
    iget v0, p0, LX/0Qx;->threshold:I

    if-le v7, v0, :cond_0

    .line 59155
    invoke-static {p0}, LX/0Qx;->m(LX/0Qx;)V

    .line 59156
    iget v0, p0, LX/0Qx;->count:I

    add-int/lit8 v7, v0, 0x1

    .line 59157
    :cond_0
    iget-object v8, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 59158
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    .line 59159
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    move-object v1, v0

    .line 59160
    :goto_0
    if-eqz v1, :cond_6

    .line 59161
    invoke-interface {v1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 59162
    invoke-interface {v1}, LX/0R1;->getHash()I

    move-result v10

    if-ne v10, p2, :cond_5

    if-eqz v3, :cond_5

    iget-object v10, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v10, v10, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v10, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 59163
    invoke-interface {v1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v0

    .line 59164
    invoke-interface {v0}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v3

    .line 59165
    if-eq p3, v0, :cond_1

    if-nez v3, :cond_4

    sget-object v8, LX/0Qd;->u:LX/0Qf;

    if-eq v0, v8, :cond_4

    .line 59166
    :cond_1
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 59167
    invoke-virtual {p3}, LX/0SO;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59168
    if-nez v3, :cond_3

    sget-object v0, LX/0rs;->COLLECTED:LX/0rs;

    .line 59169
    :goto_1
    invoke-static {p0, p1, p3, v0}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    .line 59170
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    :cond_2
    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    .line 59171
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 59172
    iput v7, p0, LX/0Qx;->count:I

    .line 59173
    invoke-static {p0, v1}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59174
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59175
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v0, v6

    :goto_2
    return v0

    .line 59176
    :cond_3
    :try_start_1
    sget-object v0, LX/0rs;->REPLACED:LX/0rs;

    goto :goto_1

    .line 59177
    :cond_4
    new-instance v0, LX/4wX;

    const/4 v1, 0x0

    invoke-direct {v0, p4, v1}, LX/4wX;-><init>(Ljava/lang/Object;I)V

    .line 59178
    sget-object v1, LX/0rs;->REPLACED:LX/0rs;

    invoke-static {p0, p1, v0, v1}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59179
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59180
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v0, v2

    goto :goto_2

    .line 59181
    :cond_5
    :try_start_2
    invoke-interface {v1}, LX/0R1;->getNext()LX/0R1;

    move-result-object v1

    goto :goto_0

    .line 59182
    :cond_6
    iget v1, p0, LX/0Qx;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Qx;->modCount:I

    .line 59183
    invoke-static {p0, p1, p2, v0}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    .line 59184
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 59185
    invoke-virtual {v8, v9, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 59186
    iput v7, p0, LX/0Qx;->count:I

    .line 59187
    invoke-static {p0, v1}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 59188
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59189
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v0, v6

    goto :goto_2

    .line 59190
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 59191
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method private b(LX/0R1;LX/0R1;)LX/0R1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59192
    iget v2, p0, LX/0Qx;->count:I

    .line 59193
    invoke-interface {p2}, LX/0R1;->getNext()LX/0R1;

    move-result-object v1

    .line 59194
    :goto_0
    if-eq p1, p2, :cond_1

    .line 59195
    invoke-direct {p0, p1, v1}, LX/0Qx;->a(LX/0R1;LX/0R1;)LX/0R1;

    move-result-object v0

    .line 59196
    if-eqz v0, :cond_0

    move v1, v2

    .line 59197
    :goto_1
    invoke-interface {p1}, LX/0R1;->getNext()LX/0R1;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 59198
    :cond_0
    invoke-direct {p0, p1}, LX/0Qx;->b(LX/0R1;)V

    .line 59199
    add-int/lit8 v0, v2, -0x1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    .line 59200
    :cond_1
    iput v2, p0, LX/0Qx;->count:I

    .line 59201
    return-object v1
.end method

.method private static b(LX/0Qx;Ljava/lang/Object;ILX/0QM;)Ljava/lang/Object;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0QM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 59202
    const/4 v7, 0x0

    .line 59203
    const/4 v6, 0x0

    .line 59204
    const/4 v8, 0x1

    .line 59205
    invoke-virtual/range {p0 .. p0}, LX/0Qx;->lock()V

    .line 59206
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Qx;->map:LX/0Qd;

    iget-object v4, v4, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v4}, LX/0QV;->read()J

    move-result-wide v10

    .line 59207
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, LX/0Qx;->d(LX/0Qx;J)V

    .line 59208
    move-object/from16 v0, p0

    iget v4, v0, LX/0Qx;->count:I

    add-int/lit8 v12, v4, -0x1

    .line 59209
    move-object/from16 v0, p0

    iget-object v13, v0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 59210
    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    and-int v14, p2, v4

    .line 59211
    invoke-virtual {v13, v14}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0R1;

    move-object v5, v4

    .line 59212
    :goto_0
    if-eqz v5, :cond_7

    .line 59213
    invoke-interface {v5}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v15

    .line 59214
    invoke-interface {v5}, LX/0R1;->getHash()I

    move-result v9

    move/from16 v0, p2

    if-ne v9, v0, :cond_3

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0Qx;->map:LX/0Qd;

    iget-object v9, v9, LX/0Qd;->f:LX/0Qj;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v15}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 59215
    invoke-interface {v5}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v9

    .line 59216
    invoke-interface {v9}, LX/0Qf;->c()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 59217
    const/4 v7, 0x0

    move-object v8, v9

    .line 59218
    :goto_1
    if-eqz v7, :cond_6

    .line 59219
    new-instance v6, LX/0SO;

    invoke-direct {v6}, LX/0SO;-><init>()V

    .line 59220
    if-nez v5, :cond_4

    .line 59221
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v4

    .line 59222
    invoke-interface {v4, v6}, LX/0R1;->setValueReference(LX/0Qf;)V

    .line 59223
    invoke-virtual {v13, v14, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v6

    .line 59224
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/0Qx;->unlock()V

    .line 59225
    invoke-static/range {p0 .. p0}, LX/0Qx;->o(LX/0Qx;)V

    .line 59226
    if-eqz v7, :cond_5

    .line 59227
    :try_start_1
    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 59228
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v5, v3}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;ILX/0SO;LX/0QM;)Ljava/lang/Object;

    move-result-object v5

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 59229
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Qx;->statsCounter:LX/0QP;

    const/4 v6, 0x1

    invoke-interface {v4, v6}, LX/0QP;->b(I)V

    move-object v4, v5

    .line 59230
    :goto_3
    return-object v4

    .line 59231
    :cond_0
    :try_start_3
    invoke-interface {v9}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 59232
    if-nez v7, :cond_1

    .line 59233
    sget-object v7, LX/0rs;->COLLECTED:LX/0rs;

    move-object/from16 v0, p0

    invoke-static {v0, v15, v9, v7}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    .line 59234
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v7, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 59235
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v7, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 59236
    move-object/from16 v0, p0

    iput v12, v0, LX/0Qx;->count:I

    move v7, v8

    move-object v8, v9

    .line 59237
    goto :goto_1

    .line 59238
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Qx;->map:LX/0Qd;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v10, v11}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 59239
    sget-object v7, LX/0rs;->EXPIRED:LX/0rs;

    move-object/from16 v0, p0

    invoke-static {v0, v15, v9, v7}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 59240
    :catchall_0
    move-exception v4

    invoke-virtual/range {p0 .. p0}, LX/0Qx;->unlock()V

    .line 59241
    invoke-static/range {p0 .. p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v4

    .line 59242
    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    invoke-static {v0, v5, v10, v11}, LX/0Qx;->c(LX/0Qx;LX/0R1;J)V

    .line 59243
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Qx;->statsCounter:LX/0QP;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/0QP;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 59244
    invoke-virtual/range {p0 .. p0}, LX/0Qx;->unlock()V

    .line 59245
    invoke-static/range {p0 .. p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v4, v7

    goto :goto_3

    .line 59246
    :cond_3
    :try_start_5
    invoke-interface {v5}, LX/0R1;->getNext()LX/0R1;

    move-result-object v5

    goto/16 :goto_0

    .line 59247
    :cond_4
    invoke-interface {v5, v6}, LX/0R1;->setValueReference(LX/0Qf;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v4, v5

    move-object v5, v6

    goto :goto_2

    .line 59248
    :catchall_1
    move-exception v5

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 59249
    :catchall_2
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Qx;->statsCounter:LX/0QP;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/0QP;->b(I)V

    throw v4

    .line 59250
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v4, v1, v8}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;LX/0Qf;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_3

    :cond_6
    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_2

    :cond_7
    move/from16 v17, v8

    move-object v8, v7

    move/from16 v7, v17

    goto/16 :goto_1
.end method

.method private b(J)V
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 59251
    invoke-static {p0}, LX/0Qx;->k(LX/0Qx;)V

    .line 59252
    :cond_0
    iget-object v0, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    if-eqz v0, :cond_1

    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v1, v0, p1, p2}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59253
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v1

    sget-object v2, LX/0rs;->EXPIRED:LX/0rs;

    invoke-direct {p0, v0, v1, v2}, LX/0Qx;->a(LX/0R1;ILX/0rs;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59254
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59255
    :cond_1
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v1, v0, p1, p2}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 59256
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v1

    sget-object v2, LX/0rs;->EXPIRED:LX/0rs;

    invoke-direct {p0, v0, v1, v2}, LX/0Qx;->a(LX/0R1;ILX/0rs;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 59257
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59258
    :cond_2
    return-void
.end method

.method private static b(LX/0Qx;LX/0R1;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 58965
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58966
    invoke-interface {p1, p2, p3}, LX/0R1;->setAccessTime(J)V

    .line 58967
    :cond_0
    iget-object v0, p0, LX/0Qx;->recencyQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 58968
    return-void
.end method

.method private b(LX/0R1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58969
    sget-object v0, LX/0rs;->COLLECTED:LX/0rs;

    invoke-static {p0, p1, v0}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0rs;)V

    .line 58970
    iget-object v0, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 58971
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 58972
    return-void
.end method

.method private static c(LX/0Qx;LX/0R1;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58565
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58566
    invoke-interface {p1, p2, p3}, LX/0R1;->setAccessTime(J)V

    .line 58567
    :cond_0
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 58568
    return-void
.end method

.method private static d(LX/0Qx;Ljava/lang/Object;I)LX/0R1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58658
    iget-object v0, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58659
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    move-object v0, v0

    .line 58660
    :goto_0
    if-eqz v0, :cond_2

    .line 58661
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 58662
    invoke-interface {v0}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 58663
    if-nez v1, :cond_1

    .line 58664
    invoke-direct {p0}, LX/0Qx;->d()V

    .line 58665
    :cond_0
    invoke-interface {v0}, LX/0R1;->getNext()LX/0R1;

    move-result-object v0

    goto :goto_0

    .line 58666
    :cond_1
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v2, v2, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v2, p1, v1}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58667
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 58653
    invoke-virtual {p0}, LX/0Qx;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58654
    :try_start_0
    invoke-direct {p0}, LX/0Qx;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58655
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58656
    :cond_0
    return-void

    .line 58657
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    throw v0
.end method

.method private static d(LX/0Qx;J)V
    .locals 3

    .prologue
    .line 58646
    invoke-virtual {p0}, LX/0Qx;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58647
    :try_start_0
    invoke-direct {p0}, LX/0Qx;->e()V

    .line 58648
    invoke-direct {p0, p1, p2}, LX/0Qx;->b(J)V

    .line 58649
    iget-object v0, p0, LX/0Qx;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58650
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58651
    :cond_0
    return-void

    .line 58652
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    throw v0
.end method

.method private e()V
    .locals 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58626
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58627
    const/4 v0, 0x0

    move v1, v0

    .line 58628
    :goto_0
    iget-object v0, p0, LX/0Qx;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 58629
    check-cast v0, LX/0R1;

    .line 58630
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    .line 58631
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v3

    .line 58632
    invoke-static {v2, v3}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, LX/0Qx;->a(LX/0R1;I)Z

    .line 58633
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    move v1, v0

    .line 58634
    goto :goto_0

    .line 58635
    :cond_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58636
    const/4 v0, 0x0

    move v1, v0

    .line 58637
    :goto_1
    iget-object v0, p0, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 58638
    check-cast v0, LX/0Qf;

    .line 58639
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    .line 58640
    invoke-interface {v0}, LX/0Qf;->b()LX/0R1;

    move-result-object v3

    .line 58641
    invoke-interface {v3}, LX/0R1;->getHash()I

    move-result v4

    .line 58642
    invoke-static {v2, v4}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v5

    invoke-interface {v3}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3, v4, v0}, LX/0Qx;->a(Ljava/lang/Object;ILX/0Qf;)Z

    .line 58643
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    move v1, v0

    .line 58644
    goto :goto_1

    .line 58645
    :cond_1
    return-void
.end method

.method public static k(LX/0Qx;)V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58622
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0Qx;->recencyQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    if-eqz v0, :cond_1

    .line 58623
    iget-object v1, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58624
    iget-object v1, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58625
    :cond_1
    return-void
.end method

.method private l()LX/0R1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58617
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    .line 58618
    invoke-interface {v0}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v2

    invoke-interface {v2}, LX/0Qf;->a()I

    move-result v2

    .line 58619
    if-lez v2, :cond_0

    .line 58620
    return-object v0

    .line 58621
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static m(LX/0Qx;)V
    .locals 11
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 58585
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58586
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 58587
    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_0

    .line 58588
    :goto_0
    return-void

    .line 58589
    :cond_0
    iget v5, p0, LX/0Qx;->count:I

    .line 58590
    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, LX/0Qx;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    .line 58591
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/0Qx;->threshold:I

    .line 58592
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 58593
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    .line 58594
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    .line 58595
    if-eqz v0, :cond_7

    .line 58596
    invoke-interface {v0}, LX/0R1;->getNext()LX/0R1;

    move-result-object v3

    .line 58597
    invoke-interface {v0}, LX/0R1;->getHash()I

    move-result v1

    and-int v2, v1, v10

    .line 58598
    if-nez v3, :cond_2

    .line 58599
    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    .line 58600
    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    .line 58601
    :goto_3
    if-eqz v3, :cond_3

    .line 58602
    invoke-interface {v3}, LX/0R1;->getHash()I

    move-result v1

    and-int/2addr v1, v10

    .line 58603
    if-eq v1, v2, :cond_6

    move-object v2, v3

    .line 58604
    :goto_4
    invoke-interface {v3}, LX/0R1;->getNext()LX/0R1;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    .line 58605
    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    .line 58606
    :goto_5
    if-eq v2, v4, :cond_1

    .line 58607
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v0

    and-int v3, v0, v10

    .line 58608
    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    .line 58609
    invoke-direct {p0, v2, v0}, LX/0Qx;->a(LX/0R1;LX/0R1;)LX/0R1;

    move-result-object v0

    .line 58610
    if-eqz v0, :cond_4

    .line 58611
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    .line 58612
    :goto_6
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    .line 58613
    :cond_4
    invoke-direct {p0, v2}, LX/0Qx;->b(LX/0R1;)V

    .line 58614
    add-int/lit8 v0, v1, -0x1

    goto :goto_6

    .line 58615
    :cond_5
    iput-object v9, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58616
    iput v5, p0, LX/0Qx;->count:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method private static o(LX/0Qx;)V
    .locals 4

    .prologue
    .line 58578
    invoke-virtual {p0}, LX/0Qx;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58579
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    .line 58580
    :goto_0
    iget-object v1, v0, LX/0Qd;->o:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4wi;

    if-eqz v1, :cond_0

    .line 58581
    :try_start_0
    iget-object v2, v0, LX/0Qd;->p:LX/0Qn;

    invoke-interface {v2, v1}, LX/0Qn;->onRemoval(LX/4wi;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58582
    :catch_0
    move-exception v1

    .line 58583
    sget-object v2, LX/0Qd;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p0, "Exception thrown by removal listener"

    invoke-virtual {v2, v3, p0, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 58584
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0R1;J)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58569
    invoke-interface {p1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 58570
    invoke-direct {p0}, LX/0Qx;->d()V

    .line 58571
    :goto_0
    return-object v0

    .line 58572
    :cond_0
    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    invoke-interface {v1}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v1

    .line 58573
    if-nez v1, :cond_1

    .line 58574
    invoke-direct {p0}, LX/0Qx;->d()V

    goto :goto_0

    .line 58575
    :cond_1
    iget-object v2, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v2, p1, p2, p3}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58576
    invoke-direct {p0, p2, p3}, LX/0Qx;->a(J)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 58577
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58553
    :try_start_0
    iget v1, p0, LX/0Qx;->count:I

    if-eqz v1, :cond_2

    .line 58554
    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v1, v1, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v1}, LX/0QV;->read()J

    move-result-wide v6

    .line 58555
    invoke-static {p0, p1, p2, v6, v7}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;IJ)LX/0R1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 58556
    if-nez v2, :cond_0

    .line 58557
    invoke-virtual {p0}, LX/0Qx;->b()V

    :goto_0
    return-object v0

    .line 58558
    :cond_0
    :try_start_1
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    invoke-interface {v1}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v5

    .line 58559
    if-eqz v5, :cond_1

    .line 58560
    invoke-static {p0, v2, v6, v7}, LX/0Qx;->b(LX/0Qx;LX/0R1;J)V

    .line 58561
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v8, v0, LX/0Qd;->t:LX/0QM;

    move-object v1, p0

    move v4, p2

    invoke-static/range {v1 .. v8}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;ILjava/lang/Object;JLX/0QM;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 58562
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    .line 58563
    :cond_1
    :try_start_2
    invoke-direct {p0}, LX/0Qx;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58564
    :cond_2
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->b()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILX/0QM;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0QM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 58695
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58696
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58697
    :try_start_0
    iget v0, p0, LX/0Qx;->count:I

    if-eqz v0, :cond_1

    .line 58698
    invoke-static {p0, p1, p2}, LX/0Qx;->d(LX/0Qx;Ljava/lang/Object;I)LX/0R1;

    move-result-object v2

    .line 58699
    if-eqz v2, :cond_1

    .line 58700
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v6

    .line 58701
    invoke-virtual {p0, v2, v6, v7}, LX/0Qx;->a(LX/0R1;J)Ljava/lang/Object;

    move-result-object v5

    .line 58702
    if-eqz v5, :cond_0

    .line 58703
    invoke-static {p0, v2, v6, v7}, LX/0Qx;->b(LX/0Qx;LX/0R1;J)V

    .line 58704
    iget-object v0, p0, LX/0Qx;->statsCounter:LX/0QP;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0QP;->a(I)V

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v8, p3

    .line 58705
    invoke-static/range {v1 .. v8}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;ILjava/lang/Object;JLX/0QM;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 58706
    invoke-virtual {p0}, LX/0Qx;->b()V

    :goto_0
    return-object v0

    .line 58707
    :cond_0
    :try_start_1
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v0

    .line 58708
    invoke-interface {v0}, LX/0Qf;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58709
    invoke-static {p0, v2, p1, v0}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;LX/0Qf;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 58710
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    .line 58711
    :cond_1
    :try_start_2
    invoke-static {p0, p1, p2, p3}, LX/0Qx;->b(LX/0Qx;Ljava/lang/Object;ILX/0QM;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 58712
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    .line 58713
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 58714
    :try_start_3
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 58715
    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_2

    .line 58716
    new-instance v1, LX/52G;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, LX/52G;-><init>(Ljava/lang/Error;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58717
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->b()V

    throw v0

    .line 58718
    :cond_2
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    .line 58719
    new-instance v1, LX/52T;

    invoke-direct {v1, v0}, LX/52T;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 58720
    :cond_3
    throw v1
.end method

.method public final a(Ljava/lang/Object;ILX/0SO;Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0SO",
            "<TK;TV;>;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 58721
    const/4 v1, 0x0

    .line 58722
    :try_start_0
    invoke-static {p4}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    .line 58723
    if-nez v1, :cond_1

    .line 58724
    new-instance v0, LX/4w9;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/4w9;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58725
    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    .line 58726
    iget-object v1, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-virtual {p3}, LX/0SO;->f()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LX/0QP;->b(J)V

    .line 58727
    invoke-direct {p0, p1, p2, p3}, LX/0Qx;->a(Ljava/lang/Object;ILX/0SO;)Z

    :cond_0
    throw v0

    .line 58728
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-virtual {p3}, LX/0SO;->f()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/0QP;->a(J)V

    .line 58729
    invoke-direct {p0, p1, p2, p3, v1}, LX/0Qx;->a(Ljava/lang/Object;ILX/0SO;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58730
    if-nez v1, :cond_2

    .line 58731
    iget-object v0, p0, LX/0Qx;->statsCounter:LX/0QP;

    invoke-virtual {p3}, LX/0SO;->f()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/0QP;->b(J)V

    .line 58732
    invoke-direct {p0, p1, p2, p3}, LX/0Qx;->a(Ljava/lang/Object;ILX/0SO;)Z

    :cond_2
    return-object v1
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 58733
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58734
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v8

    .line 58735
    invoke-static {p0, v8, v9}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58736
    iget-object v10, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58737
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v11, p2, v0

    .line 58738
    invoke-virtual {v10, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58739
    :goto_0
    if-eqz v2, :cond_3

    .line 58740
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 58741
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v0, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58742
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    .line 58743
    invoke-interface {v5}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 58744
    if-nez v0, :cond_1

    .line 58745
    invoke-interface {v5}, LX/0Qf;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58746
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58747
    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;

    move-object v0, p0

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58748
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58749
    invoke-virtual {v10, v11, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58750
    iput v1, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58751
    :cond_0
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58752
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v7

    :goto_1
    return-object v0

    .line 58753
    :cond_1
    :try_start_1
    iget v1, p0, LX/0Qx;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Qx;->modCount:I

    .line 58754
    sget-object v1, LX/0rs;->REPLACED:LX/0rs;

    invoke-static {p0, p1, v5, v1}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-wide v5, v8

    .line 58755
    invoke-static/range {v1 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58756
    invoke-static {p0, v2}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58757
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58758
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_1

    .line 58759
    :cond_2
    :try_start_2
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58760
    :cond_3
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58761
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v7

    goto :goto_1

    .line 58762
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58763
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 58764
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58765
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v4

    .line 58766
    invoke-static {p0, v4, v5}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58767
    iget v0, p0, LX/0Qx;->count:I

    add-int/lit8 v0, v0, 0x1

    .line 58768
    iget v1, p0, LX/0Qx;->threshold:I

    if-le v0, v1, :cond_0

    .line 58769
    invoke-static {p0}, LX/0Qx;->m(LX/0Qx;)V

    .line 58770
    :cond_0
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58771
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 58772
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    move-object v1, v0

    .line 58773
    :goto_0
    if-eqz v1, :cond_5

    .line 58774
    invoke-interface {v1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 58775
    invoke-interface {v1}, LX/0R1;->getHash()I

    move-result v3

    if-ne v3, p2, :cond_4

    if-eqz v2, :cond_4

    iget-object v3, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v3, v3, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v3, p1, v2}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 58776
    invoke-interface {v1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v0

    .line 58777
    invoke-interface {v0}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 58778
    if-nez v7, :cond_2

    .line 58779
    iget v2, p0, LX/0Qx;->modCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/0Qx;->modCount:I

    .line 58780
    invoke-interface {v0}, LX/0Qf;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58781
    sget-object v2, LX/0rs;->COLLECTED:LX/0rs;

    invoke-static {p0, p1, v0, v2}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 58782
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58783
    iget v0, p0, LX/0Qx;->count:I

    .line 58784
    :goto_1
    iput v0, p0, LX/0Qx;->count:I

    .line 58785
    invoke-static {p0, v1}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58786
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58787
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v6

    :goto_2
    return-object v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 58788
    :try_start_1
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58789
    iget v0, p0, LX/0Qx;->count:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58790
    :cond_2
    if-eqz p4, :cond_3

    .line 58791
    invoke-static {p0, v1, v4, v5}, LX/0Qx;->c(LX/0Qx;LX/0R1;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58792
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58793
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v7

    goto :goto_2

    .line 58794
    :cond_3
    :try_start_2
    iget v2, p0, LX/0Qx;->modCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/0Qx;->modCount:I

    .line 58795
    sget-object v2, LX/0rs;->REPLACED:LX/0rs;

    invoke-static {p0, p1, v0, v2}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 58796
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58797
    invoke-static {p0, v1}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58798
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58799
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v7

    goto :goto_2

    .line 58800
    :cond_4
    :try_start_3
    invoke-interface {v1}, LX/0R1;->getNext()LX/0R1;

    move-result-object v1

    goto :goto_0

    .line 58801
    :cond_5
    iget v1, p0, LX/0Qx;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Qx;->modCount:I

    .line 58802
    invoke-static {p0, p1, p2, v0}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 58803
    invoke-static/range {v0 .. v5}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58804
    invoke-virtual {v7, v8, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58805
    iget v0, p0, LX/0Qx;->count:I

    add-int/lit8 v0, v0, 0x1

    .line 58806
    iput v0, p0, LX/0Qx;->count:I

    .line 58807
    invoke-static {p0, v1}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58808
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58809
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v6

    goto :goto_2

    .line 58810
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58811
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 58812
    iget v0, p0, LX/0Qx;->count:I

    if-eqz v0, :cond_8

    .line 58813
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58814
    :try_start_0
    iget-object v3, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    .line 58815
    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 58816
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    :goto_1
    if-eqz v0, :cond_1

    .line 58817
    invoke-interface {v0}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v4

    invoke-interface {v4}, LX/0Qf;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58818
    sget-object v4, LX/0rs;->EXPLICIT:LX/0rs;

    invoke-static {p0, v0, v4}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0rs;)V

    .line 58819
    :cond_0
    invoke-interface {v0}, LX/0R1;->getNext()LX/0R1;

    move-result-object v0

    goto :goto_1

    .line 58820
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 58821
    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 58822
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58823
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 58824
    :cond_3
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 58825
    :cond_4
    iget-object v0, p0, LX/0Qx;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    .line 58826
    :cond_5
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 58827
    :cond_6
    iget-object v0, p0, LX/0Qx;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    .line 58828
    :cond_7
    iget-object v0, p0, LX/0Qx;->writeQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 58829
    iget-object v0, p0, LX/0Qx;->accessQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 58830
    iget-object v0, p0, LX/0Qx;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 58831
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58832
    const/4 v0, 0x0

    iput v0, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58833
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58834
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    .line 58835
    :cond_8
    return-void

    .line 58836
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58837
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final a(LX/0R1;I)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;I)Z"
        }
    .end annotation

    .prologue
    .line 58838
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58839
    :try_start_0
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58840
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 58841
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58842
    :goto_0
    if-eqz v2, :cond_1

    .line 58843
    if-ne v2, p1, :cond_0

    .line 58844
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58845
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;

    move-object v0, p0

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58846
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58847
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58848
    iput v1, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58849
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58850
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 58851
    :cond_0
    :try_start_1
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58852
    :cond_1
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58853
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x0

    goto :goto_1

    .line 58854
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58855
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILX/0Qf;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0Qf",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58668
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58669
    :try_start_0
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58670
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    .line 58671
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58672
    :goto_0
    if-eqz v2, :cond_4

    .line 58673
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 58674
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v4

    if-ne v4, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v4, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v4, v4, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v4, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 58675
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v4

    .line 58676
    if-ne v4, p3, :cond_2

    .line 58677
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58678
    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58679
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58680
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58681
    iput v1, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58682
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58683
    invoke-virtual {p0}, LX/0Qx;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58684
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    .line 58685
    :cond_2
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58686
    invoke-virtual {p0}, LX/0Qx;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 58687
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_1

    .line 58688
    :cond_3
    :try_start_1
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58689
    :cond_4
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58690
    invoke-virtual {p0}, LX/0Qx;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 58691
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_1

    .line 58692
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58693
    invoke-virtual {p0}, LX/0Qx;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_5

    .line 58694
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    :cond_5
    throw v0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 58856
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58857
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v8

    .line 58858
    invoke-static {p0, v8, v9}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58859
    iget-object v7, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58860
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    .line 58861
    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58862
    :goto_0
    if-eqz v2, :cond_4

    .line 58863
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 58864
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v0, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 58865
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    .line 58866
    invoke-interface {v5}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 58867
    if-nez v0, :cond_1

    .line 58868
    invoke-interface {v5}, LX/0Qf;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58869
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58870
    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;

    move-object v0, p0

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58871
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58872
    invoke-virtual {v7, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58873
    iput v1, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58874
    :cond_0
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58875
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x0

    :goto_1
    return v0

    .line 58876
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v1, v1, LX/0Qd;->g:LX/0Qj;

    invoke-virtual {v1, p3, v0}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58877
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    .line 58878
    sget-object v0, LX/0rs;->REPLACED:LX/0rs;

    invoke-static {p0, p1, v5, v0}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;LX/0Qf;LX/0rs;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p4

    move-wide v5, v8

    .line 58879
    invoke-static/range {v1 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 58880
    invoke-static {p0, v2}, LX/0Qx;->a(LX/0Qx;LX/0R1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58881
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58882
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x1

    goto :goto_1

    .line 58883
    :cond_2
    :try_start_2
    invoke-static {p0, v2, v8, v9}, LX/0Qx;->c(LX/0Qx;LX/0R1;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58884
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58885
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x0

    goto :goto_1

    .line 58886
    :cond_3
    :try_start_3
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58887
    :cond_4
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58888
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    const/4 v0, 0x0

    goto :goto_1

    .line 58889
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58890
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 58891
    iget-object v0, p0, LX/0Qx;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    .line 58892
    invoke-virtual {p0}, LX/0Qx;->c()V

    .line 58893
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 58894
    :try_start_0
    iget v1, p0, LX/0Qx;->count:I

    if-eqz v1, :cond_2

    .line 58895
    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v1, v1, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v1}, LX/0QV;->read()J

    move-result-wide v2

    .line 58896
    invoke-static {p0, p1, p2, v2, v3}, LX/0Qx;->a(LX/0Qx;Ljava/lang/Object;IJ)LX/0R1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 58897
    if-nez v1, :cond_0

    .line 58898
    invoke-virtual {p0}, LX/0Qx;->b()V

    :goto_0
    return v0

    .line 58899
    :cond_0
    :try_start_1
    invoke-interface {v1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v1

    invoke-interface {v1}, LX/0Qf;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 58900
    :cond_1
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LX/0Qx;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->b()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 58901
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58902
    :try_start_0
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    .line 58903
    invoke-static {p0, v0, v1}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58904
    iget-object v8, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58905
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    .line 58906
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58907
    :goto_0
    if-eqz v2, :cond_4

    .line 58908
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 58909
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v0, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 58910
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    .line 58911
    invoke-interface {v5}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 58912
    iget-object v4, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v4, v4, LX/0Qd;->g:LX/0Qj;

    invoke-virtual {v4, p3, v0}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58913
    sget-object v6, LX/0rs;->EXPLICIT:LX/0rs;

    .line 58914
    :goto_1
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    move-object v0, p0

    move v4, p2

    .line 58915
    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58916
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58917
    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58918
    iput v1, p0, LX/0Qx;->count:I

    .line 58919
    sget-object v0, LX/0rs;->EXPLICIT:LX/0rs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v6, v0, :cond_2

    const/4 v0, 0x1

    .line 58920
    :goto_2
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58921
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move v7, v0

    :goto_3
    return v7

    .line 58922
    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v5}, LX/0Qf;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58923
    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 58924
    :cond_1
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58925
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_3

    :cond_2
    move v0, v7

    .line 58926
    goto :goto_2

    .line 58927
    :cond_3
    :try_start_2
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58928
    :cond_4
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58929
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_3

    .line 58930
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58931
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58932
    invoke-virtual {p0}, LX/0Qx;->lock()V

    .line 58933
    :try_start_0
    iget-object v1, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v1, v1, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v1}, LX/0QV;->read()J

    move-result-wide v2

    .line 58934
    invoke-static {p0, v2, v3}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58935
    iget-object v8, p0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 58936
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v9, p2, v1

    .line 58937
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0R1;

    move-object v2, v1

    .line 58938
    :goto_0
    if-eqz v2, :cond_3

    .line 58939
    invoke-interface {v2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 58940
    invoke-interface {v2}, LX/0R1;->getHash()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v4, v4, LX/0Qd;->f:LX/0Qj;

    invoke-virtual {v4, p1, v3}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 58941
    invoke-interface {v2}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    .line 58942
    invoke-interface {v5}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 58943
    if-eqz v7, :cond_0

    .line 58944
    sget-object v6, LX/0rs;->EXPLICIT:LX/0rs;

    .line 58945
    :goto_1
    iget v0, p0, LX/0Qx;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Qx;->modCount:I

    move-object v0, p0

    move v4, p2

    .line 58946
    invoke-static/range {v0 .. v6}, LX/0Qx;->a(LX/0Qx;LX/0R1;LX/0R1;Ljava/lang/Object;ILX/0Qf;LX/0rs;)LX/0R1;

    move-result-object v0

    .line 58947
    iget v1, p0, LX/0Qx;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 58948
    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 58949
    iput v1, p0, LX/0Qx;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58950
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58951
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    move-object v0, v7

    :goto_2
    return-object v0

    .line 58952
    :cond_0
    :try_start_1
    invoke-interface {v5}, LX/0Qf;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 58953
    sget-object v6, LX/0rs;->COLLECTED:LX/0rs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 58954
    :cond_1
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58955
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_2

    .line 58956
    :cond_2
    :try_start_2
    invoke-interface {v2}, LX/0R1;->getNext()LX/0R1;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 58957
    :cond_3
    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58958
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    goto :goto_2

    .line 58959
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0Qx;->unlock()V

    .line 58960
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 58961
    iget-object v0, p0, LX/0Qx;->map:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    .line 58962
    invoke-static {p0, v0, v1}, LX/0Qx;->d(LX/0Qx;J)V

    .line 58963
    invoke-static {p0}, LX/0Qx;->o(LX/0Qx;)V

    .line 58964
    return-void
.end method
