.class public LX/0ZC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/0ZC;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0kc;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83083
    new-instance v0, LX/0ZD;

    invoke-direct {v0}, LX/0ZD;-><init>()V

    sput-object v0, LX/0ZC;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0kc;",
            ">;>;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83080
    iput-object p1, p0, LX/0ZC;->a:LX/0Ot;

    .line 83081
    iput-object p2, p0, LX/0ZC;->b:LX/0Or;

    .line 83082
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZC;
    .locals 5

    .prologue
    .line 83063
    sget-object v0, LX/0ZC;->d:LX/0ZC;

    if-nez v0, :cond_1

    .line 83064
    const-class v1, LX/0ZC;

    monitor-enter v1

    .line 83065
    :try_start_0
    sget-object v0, LX/0ZC;->d:LX/0ZC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83066
    if-eqz v2, :cond_0

    .line 83067
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83068
    new-instance v3, LX/0ZC;

    .line 83069
    new-instance v4, LX/0ZE;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/0ZE;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 83070
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 83071
    const/16 p0, 0xb45

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0ZC;-><init>(LX/0Ot;LX/0Or;)V

    .line 83072
    move-object v0, v3

    .line 83073
    sput-object v0, LX/0ZC;->d:LX/0ZC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83076
    :cond_1
    sget-object v0, LX/0ZC;->d:LX/0ZC;

    return-object v0

    .line 83077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83060
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83061
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 83062
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83039
    const-string v0, "network_log"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 83040
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 83041
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 83042
    iget-object v0, p0, LX/0ZC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kc;

    .line 83043
    invoke-interface {v0}, LX/0kc;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 83044
    :cond_0
    sget-object v0, LX/0ZC;->c:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 83045
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ki;

    .line 83046
    const-string v5, "[%s] %s%n"

    invoke-interface {v0}, LX/0ki;->getStartTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0}, LX/0ki;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83047
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 83048
    :cond_1
    iget-object v0, p0, LX/0ZC;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 83049
    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v1

    invoke-interface {v1}, LX/1hk;->c()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 83050
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83051
    iget-object v0, p0, LX/0ZC;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a()LX/2Ee;

    move-result-object v0

    .line 83052
    const-string v1, "In flight requests: \n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83053
    iget-object v1, v0, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v1, v1

    .line 83054
    invoke-static {v1}, Lcom/facebook/http/common/FbHttpUtils;->a(Ljava/util/ArrayList;)LX/0Px;

    move-result-object v1

    invoke-static {v2, v1}, LX/0ZC;->a(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 83055
    const-string v1, "In queue requests: \n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83056
    iget-object v1, v0, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v0, v1

    .line 83057
    invoke-static {v0}, Lcom/facebook/http/common/FbHttpUtils;->a(Ljava/util/ArrayList;)LX/0Px;

    move-result-object v0

    invoke-static {v2, v0}, LX/0ZC;->a(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 83058
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83059
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
