.class public final LX/12N;
.super LX/12O;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/12O",
        "<",
        "LX/4g4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/128;


# direct methods
.method public constructor <init>(LX/128;)V
    .locals 0

    .prologue
    .line 175374
    iput-object p1, p0, LX/12N;->a:LX/128;

    invoke-direct {p0}, LX/12O;-><init>()V

    return-void
.end method

.method private a(LX/4g4;LX/3J6;)V
    .locals 3

    .prologue
    .line 175375
    iget-object v0, p2, LX/3J6;->i:LX/0gc;

    if-nez v0, :cond_1

    .line 175376
    iget-object v0, p2, LX/3J6;->d:LX/39A;

    invoke-interface {v0}, LX/39A;->a()V

    .line 175377
    :cond_0
    :goto_0
    return-void

    .line 175378
    :cond_1
    iget-object v0, p2, LX/3J6;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 175379
    iget-object v0, p2, LX/3J6;->d:LX/39A;

    invoke-interface {v0}, LX/39A;->a()V

    goto :goto_0

    .line 175380
    :cond_2
    sget-object v0, LX/6YH;->c:[I

    iget-object v1, p1, LX/4g4;->c:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 175381
    iget-object v0, p2, LX/3J6;->d:LX/39A;

    if-eqz v0, :cond_0

    .line 175382
    iget-object v0, p2, LX/3J6;->d:LX/39A;

    invoke-interface {v0}, LX/39A;->a()V

    goto :goto_0

    .line 175383
    :pswitch_0
    sget-object v0, LX/6YH;->b:[I

    iget-object v1, p1, LX/4g4;->d:LX/4g0;

    invoke-virtual {v1}, LX/4g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 175384
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "no failure reason"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175385
    :pswitch_1
    iget-object v0, p0, LX/12N;->a:LX/128;

    iget-object v0, v0, LX/128;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    iget-object v1, p1, LX/4g4;->e:Ljava/lang/Object;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    invoke-interface {v0, p2, v1, v2}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iget-object v1, p2, LX/3J6;->i:LX/0gc;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    iget-object v2, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 175386
    :pswitch_2
    iget-object v0, p0, LX/12N;->a:LX/128;

    iget-object v0, v0, LX/128;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    iget-object v1, p1, LX/4g4;->e:Ljava/lang/Object;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    invoke-interface {v0, p2, v1, v2}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iget-object v1, p2, LX/3J6;->i:LX/0gc;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    iget-object v2, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 175387
    :pswitch_3
    iget-object v0, p0, LX/12N;->a:LX/128;

    iget-object v0, v0, LX/128;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    iget-object v1, p1, LX/4g4;->e:Ljava/lang/Object;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    invoke-interface {v0, p2, v1, v2}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iget-object v1, p2, LX/3J6;->i:LX/0gc;

    iget-object v2, p1, LX/4g4;->a:LX/0yY;

    iget-object v2, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/4g4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175373
    const-class v0, LX/4g4;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 175365
    check-cast p1, LX/4g4;

    .line 175366
    iget-object v0, p0, LX/12N;->a:LX/128;

    iget-object v0, v0, LX/121;->a:Ljava/util/Map;

    iget-object v1, p1, LX/4g4;->a:LX/0yY;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3J6;

    .line 175367
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/3J6;->d:LX/39A;

    if-nez v1, :cond_1

    .line 175368
    :cond_0
    :goto_0
    return-void

    .line 175369
    :cond_1
    sget-object v1, LX/6YH;->a:[I

    iget-object v2, p1, LX/4g4;->b:LX/4g3;

    invoke-virtual {v2}, LX/4g3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 175370
    :pswitch_0
    iget-object v0, v0, LX/3J6;->d:LX/39A;

    iget-object v1, p1, LX/4g4;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/39A;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 175371
    :pswitch_1
    iget-object v0, v0, LX/3J6;->d:LX/39A;

    invoke-interface {v0}, LX/39A;->a()V

    goto :goto_0

    .line 175372
    :pswitch_2
    invoke-direct {p0, p1, v0}, LX/12N;->a(LX/4g4;LX/3J6;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
