.class public abstract LX/11H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 171891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171892
    invoke-virtual {p0, p1, p2, v0, v0}, LX/11H;->b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 171893
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/11H;->b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 171894
    invoke-virtual {p0, p1, p2, p3, p4}, LX/11H;->b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 171895
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, LX/11H;->b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation
.end method
