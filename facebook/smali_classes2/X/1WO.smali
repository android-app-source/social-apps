.class public LX/1WO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Cca;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ccd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 268145
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1WO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ccd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268146
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 268147
    iput-object p1, p0, LX/1WO;->b:LX/0Ot;

    .line 268148
    return-void
.end method

.method public static a(LX/0QB;)LX/1WO;
    .locals 4

    .prologue
    .line 268149
    const-class v1, LX/1WO;

    monitor-enter v1

    .line 268150
    :try_start_0
    sget-object v0, LX/1WO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268151
    sput-object v2, LX/1WO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268154
    new-instance v3, LX/1WO;

    const/16 p0, 0x2f11

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1WO;-><init>(LX/0Ot;)V

    .line 268155
    move-object v0, v3

    .line 268156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 268160
    check-cast p2, LX/Ccb;

    .line 268161
    iget-object v0, p0, LX/1WO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Ccb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p2, LX/Ccb;->b:Landroid/text/style/ClickableSpan;

    .line 268162
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 268163
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 268164
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/14w;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    .line 268165
    new-instance p0, LX/47x;

    invoke-direct {p0, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 268166
    if-eqz v4, :cond_0

    const p2, 0x7f082701

    :goto_0
    move p2, p2

    .line 268167
    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object p0

    const-string p2, "link_hide_photo"

    .line 268168
    if-eqz v4, :cond_1

    const p1, 0x7f082703

    :goto_1
    move v4, p1

    .line 268169
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 p1, 0x21

    invoke-virtual {p0, p2, v4, v1, p1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v4

    .line 268170
    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    move-object v3, v4

    .line 268171
    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b00ca

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0162

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    const v4, 0x7f0b00bf

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b00d2

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    const v4, 0x7f0b00d5

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b00d6

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 268172
    return-object v0

    :cond_0
    const p2, 0x7f082700

    goto :goto_0

    :cond_1
    const p1, 0x7f082702

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268173
    invoke-static {}, LX/1dS;->b()V

    .line 268174
    const/4 v0, 0x0

    return-object v0
.end method
