.class public LX/1rl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1rl;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0W3;


# direct methods
.method public constructor <init>(LX/0ad;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333007
    iput-object p1, p0, LX/1rl;->a:LX/0ad;

    .line 333008
    iput-object p2, p0, LX/1rl;->b:LX/0W3;

    .line 333009
    return-void
.end method

.method public static a(LX/1rl;FJI)F
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 333005
    iget-object v0, p0, LX/1rl;->a:LX/0ad;

    iget-object v1, p0, LX/1rl;->b:LX/0W3;

    invoke-interface {v1, p2, p3, p4}, LX/0W4;->a(JI)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, p1, v1}, LX/0ad;->a(FF)F

    move-result v0

    return v0
.end method

.method public static a(LX/1rl;IJI)I
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 333004
    iget-object v0, p0, LX/1rl;->a:LX/0ad;

    iget-object v1, p0, LX/1rl;->b:LX/0W3;

    invoke-interface {v1, p2, p3, p4}, LX/0W4;->a(JI)I

    move-result v1

    invoke-interface {v0, p1, v1}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public static a(LX/1rl;JJJ)J
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 333003
    iget-object v0, p0, LX/1rl;->a:LX/0ad;

    iget-object v1, p0, LX/1rl;->b:LX/0W3;

    invoke-interface {v1, p3, p4, p5, p6}, LX/0W4;->a(JJ)J

    move-result-wide v2

    invoke-interface {v0, p1, p2, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/1rl;
    .locals 5

    .prologue
    .line 333010
    sget-object v0, LX/1rl;->c:LX/1rl;

    if-nez v0, :cond_1

    .line 333011
    const-class v1, LX/1rl;

    monitor-enter v1

    .line 333012
    :try_start_0
    sget-object v0, LX/1rl;->c:LX/1rl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 333013
    if-eqz v2, :cond_0

    .line 333014
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 333015
    new-instance p0, LX/1rl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/1rl;-><init>(LX/0ad;LX/0W3;)V

    .line 333016
    move-object v0, p0

    .line 333017
    sput-object v0, LX/1rl;->c:LX/1rl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333018
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 333019
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 333020
    :cond_1
    sget-object v0, LX/1rl;->c:LX/1rl;

    return-object v0

    .line 333021
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 333022
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1rl;CJLX/1sR;)LX/1sR;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 332995
    iget-object v0, p0, LX/1rl;->a:LX/0ad;

    invoke-interface {v0, p1, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332996
    if-nez v0, :cond_0

    .line 332997
    iget-object v0, p0, LX/1rl;->b:LX/0W3;

    invoke-interface {v0, p2, p3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332998
    :cond_0
    if-nez v0, :cond_1

    .line 332999
    :goto_0
    return-object p4

    .line 333000
    :cond_1
    invoke-static {v0}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333001
    :try_start_0
    invoke-static {v0}, LX/1sR;->valueOf(Ljava/lang/String;)LX/1sR;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p4

    goto :goto_0

    .line 333002
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/1rl;SJZ)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 332992
    iget-object v0, p0, LX/1rl;->a:LX/0ad;

    iget-object v1, p0, LX/1rl;->b:LX/0W3;

    invoke-interface {v1, p2, p3, p4}, LX/0W4;->a(JZ)Z

    move-result v1

    invoke-interface {v0, p1, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()J
    .locals 8

    .prologue
    .line 332994
    sget-wide v2, LX/1rm;->d:J

    sget-wide v4, LX/0X5;->eL:J

    const-wide/16 v6, 0x7530

    move-object v1, p0

    invoke-static/range {v1 .. v7}, LX/1rl;->a(LX/1rl;JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()J
    .locals 8

    .prologue
    .line 332993
    sget-wide v2, LX/1rm;->f:J

    sget-wide v4, LX/0X5;->eO:J

    const-wide/16 v6, 0x2710

    move-object v1, p0

    invoke-static/range {v1 .. v7}, LX/1rl;->a(LX/1rl;JJJ)J

    move-result-wide v0

    return-wide v0
.end method
