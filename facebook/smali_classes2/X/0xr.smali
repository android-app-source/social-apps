.class public final enum LX/0xr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0xr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0xr;

.field public static final enum BLACK:LX/0xr;

.field public static final enum BOLD:LX/0xr;

.field public static final enum BUILTIN:LX/0xr;

.field public static final enum LIGHT:LX/0xr;

.field public static final enum MEDIUM:LX/0xr;

.field public static final enum REGULAR:LX/0xr;

.field public static final enum THIN:LX/0xr;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 163498
    new-instance v0, LX/0xr;

    const-string v1, "THIN"

    invoke-direct {v0, v1, v3}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->THIN:LX/0xr;

    .line 163499
    new-instance v0, LX/0xr;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v4}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->LIGHT:LX/0xr;

    .line 163500
    new-instance v0, LX/0xr;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v5}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->REGULAR:LX/0xr;

    .line 163501
    new-instance v0, LX/0xr;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v6}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->MEDIUM:LX/0xr;

    .line 163502
    new-instance v0, LX/0xr;

    const-string v1, "BOLD"

    invoke-direct {v0, v1, v7}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->BOLD:LX/0xr;

    .line 163503
    new-instance v0, LX/0xr;

    const-string v1, "BLACK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->BLACK:LX/0xr;

    .line 163504
    new-instance v0, LX/0xr;

    const-string v1, "BUILTIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xr;->BUILTIN:LX/0xr;

    .line 163505
    const/4 v0, 0x7

    new-array v0, v0, [LX/0xr;

    sget-object v1, LX/0xr;->THIN:LX/0xr;

    aput-object v1, v0, v3

    sget-object v1, LX/0xr;->LIGHT:LX/0xr;

    aput-object v1, v0, v4

    sget-object v1, LX/0xr;->REGULAR:LX/0xr;

    aput-object v1, v0, v5

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    aput-object v1, v0, v6

    sget-object v1, LX/0xr;->BOLD:LX/0xr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0xr;->BLACK:LX/0xr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0xr;->BUILTIN:LX/0xr;

    aput-object v2, v0, v1

    sput-object v0, LX/0xr;->$VALUES:[LX/0xr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163508
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromIndex(I)LX/0xr;
    .locals 1

    .prologue
    .line 163509
    invoke-static {}, LX/0xr;->values()[LX/0xr;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0xr;
    .locals 1

    .prologue
    .line 163507
    const-class v0, LX/0xr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0xr;

    return-object v0
.end method

.method public static values()[LX/0xr;
    .locals 1

    .prologue
    .line 163506
    sget-object v0, LX/0xr;->$VALUES:[LX/0xr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0xr;

    return-object v0
.end method
