.class public LX/0tk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0sg;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3HG;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3HL;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0sg;Ljava/util/Set;Ljava/util/Set;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "Ljava/util/Set",
            "<",
            "LX/3HG;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/3HL;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 155562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155563
    iput-object p1, p0, LX/0tk;->a:LX/0sg;

    .line 155564
    iput-object p2, p0, LX/0tk;->b:Ljava/util/Set;

    .line 155565
    iput-object p3, p0, LX/0tk;->c:Ljava/util/Set;

    .line 155566
    iput-object p4, p0, LX/0tk;->d:LX/0Uh;

    .line 155567
    return-void
.end method

.method public static a(LX/0QB;)LX/0tk;
    .locals 1

    .prologue
    .line 155561
    invoke-static {p0}, LX/0tk;->b(LX/0QB;)LX/0tk;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0tk;LX/0Rf;LX/0jT;LX/2lk;)LX/3Bq;
    .locals 3
    .param p0    # LX/0tk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0jT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155482
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155483
    if-eqz p3, :cond_0

    .line 155484
    :goto_0
    invoke-interface {p3, p2}, LX/2lk;->a(LX/0jT;)Z

    .line 155485
    invoke-interface {p3}, LX/2lk;->d()LX/3Bq;

    move-result-object v0

    .line 155486
    invoke-static {p0, v0, p2}, LX/0tk;->a(LX/0tk;LX/3Bq;LX/0jT;)LX/3Bq;

    move-result-object v0

    move-object v1, v0

    .line 155487
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155488
    invoke-interface {v1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v2

    .line 155489
    new-instance v0, LX/4Uh;

    invoke-direct {v0, v1, v2}, LX/4Uh;-><init>(LX/3Bq;Ljava/util/Set;)V

    .line 155490
    :goto_1
    return-object v0

    .line 155491
    :cond_0
    iget-object v0, p0, LX/0tk;->a:LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object p3

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(LX/0tk;LX/3Bq;LX/0jT;)LX/3Bq;
    .locals 8

    .prologue
    .line 155541
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 155542
    const/4 v1, 0x0

    .line 155543
    iget-object v0, p0, LX/0tk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3HG;

    .line 155544
    invoke-interface {v0}, LX/3HG;->a()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 155545
    invoke-interface {v0, p2}, LX/3HG;->a(LX/0jT;)LX/3Bq;

    move-result-object v4

    .line 155546
    if-eqz v4, :cond_0

    .line 155547
    if-nez v1, :cond_6

    .line 155548
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 155549
    :goto_1
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_0

    .line 155550
    :cond_1
    iget-object v0, p0, LX/0tk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3HL;

    .line 155551
    invoke-interface {v0}, LX/3HL;->a()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 155552
    invoke-interface {v0, p2}, LX/3HL;->a(LX/0jT;)LX/4VT;

    move-result-object v0

    .line 155553
    if-eqz v0, :cond_2

    .line 155554
    if-nez v1, :cond_3

    .line 155555
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155556
    :cond_3
    new-instance v4, LX/4VL;

    iget-object v5, p0, LX/0tk;->d:LX/0Uh;

    const/4 v6, 0x1

    new-array v6, v6, [LX/4VT;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-direct {v4, v5, v6}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 155557
    :cond_4
    if-eqz v1, :cond_5

    .line 155558
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155559
    new-instance p1, LX/4Uf;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/3Bq;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Bq;

    invoke-direct {p1, v0}, LX/4Uf;-><init>([LX/3Bq;)V

    .line 155560
    :cond_5
    return-object p1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/0tk;
    .locals 6

    .prologue
    .line 155536
    new-instance v2, LX/0tk;

    invoke-static {p0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v0

    check-cast v0, LX/0sg;

    .line 155537
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/0tl;

    invoke-direct {v4, p0}, LX/0tl;-><init>(LX/0QB;)V

    invoke-direct {v1, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v3, v1

    .line 155538
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v4

    new-instance v5, LX/0tm;

    invoke-direct {v5, p0}, LX/0tm;-><init>(LX/0QB;)V

    invoke-direct {v1, v4, v5}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v1

    .line 155539
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v3, v4, v1}, LX/0tk;-><init>(LX/0sg;Ljava/util/Set;Ljava/util/Set;LX/0Uh;)V

    .line 155540
    return-object v2
.end method


# virtual methods
.method public final a(LX/0jT;LX/0jT;)LX/0jT;
    .locals 4

    .prologue
    .line 155524
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 155525
    iget-object v0, p0, LX/0tk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3HG;

    .line 155526
    invoke-interface {v0}, LX/3HG;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155527
    invoke-interface {v0}, LX/3HG;->b()LX/69p;

    move-result-object v0

    .line 155528
    if-eqz v0, :cond_0

    .line 155529
    invoke-virtual {v0, p1, p2}, LX/69p;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object p2

    .line 155530
    :cond_1
    :goto_0
    return-object p2

    .line 155531
    :cond_2
    iget-object v0, p0, LX/0tk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3HL;

    .line 155532
    invoke-interface {v0}, LX/3HL;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 155533
    invoke-interface {v0}, LX/3HL;->b()LX/69p;

    move-result-object v0

    .line 155534
    if-eqz v0, :cond_3

    .line 155535
    invoke-virtual {v0, p1, p2}, LX/69p;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(LX/399;)LX/3Bq;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 155519
    iget-object v1, p1, LX/399;->c:LX/0jT;

    move-object v1, v1

    .line 155520
    if-eqz v1, :cond_0

    .line 155521
    iget-object v2, p1, LX/399;->b:LX/0Rf;

    move-object v2, v2

    .line 155522
    invoke-static {p0, v2, v1, v0}, LX/0tk;->a(LX/0tk;LX/0Rf;LX/0jT;LX/2lk;)LX/3Bq;

    move-result-object v0

    .line 155523
    :cond_0
    return-object v0
.end method

.method public final a(LX/399;LX/0jT;LX/2lk;)LX/3Bq;
    .locals 2
    .param p1    # LX/399;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155505
    if-nez p2, :cond_0

    .line 155506
    sget-object v0, LX/3Bq;->a:LX/3Bq;

    .line 155507
    :goto_0
    return-object v0

    .line 155508
    :cond_0
    if-eqz p1, :cond_1

    .line 155509
    iget-object v0, p1, LX/399;->b:LX/0Rf;

    move-object v0, v0

    .line 155510
    :goto_1
    if-eqz p1, :cond_2

    .line 155511
    iget-object v1, p1, LX/399;->c:LX/0jT;

    move-object v1, v1

    .line 155512
    if-eqz v1, :cond_2

    .line 155513
    iget-boolean v1, p1, LX/399;->d:Z

    move v1, v1

    .line 155514
    if-eqz v1, :cond_2

    .line 155515
    iget-object v1, p1, LX/399;->c:LX/0jT;

    move-object v1, v1

    .line 155516
    invoke-static {p0, v0, v1, p3}, LX/0tk;->a(LX/0tk;LX/0Rf;LX/0jT;LX/2lk;)LX/3Bq;

    move-result-object v0

    goto :goto_0

    .line 155517
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 155518
    :cond_2
    invoke-static {p0, v0, p2, p3}, LX/0tk;->a(LX/0tk;LX/0Rf;LX/0jT;LX/2lk;)LX/3Bq;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/399;Lcom/facebook/graphql/executor/GraphQLResult;LX/3Bq;)LX/3Bq;
    .locals 2

    .prologue
    .line 155492
    iget-boolean v0, p1, LX/399;->d:Z

    move v0, v0

    .line 155493
    if-eqz v0, :cond_2

    .line 155494
    iget-object v0, p1, LX/399;->c:LX/0jT;

    move-object v0, v0

    .line 155495
    :goto_0
    if-eqz v0, :cond_0

    .line 155496
    instance-of v1, v0, LX/0jT;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 155497
    check-cast v0, LX/0jT;

    invoke-static {p0, p3, v0}, LX/0tk;->a(LX/0tk;LX/3Bq;LX/0jT;)LX/3Bq;

    move-result-object p3

    .line 155498
    :cond_0
    iget-object v0, p1, LX/399;->b:LX/0Rf;

    move-object v0, v0

    .line 155499
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 155500
    invoke-interface {p3}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, v0}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v1

    .line 155501
    new-instance v0, LX/4Uh;

    invoke-direct {v0, p3, v1}, LX/4Uh;-><init>(LX/3Bq;Ljava/util/Set;)V

    move-object p3, v0

    .line 155502
    :cond_1
    return-object p3

    .line 155503
    :cond_2
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 155504
    goto :goto_0
.end method
