.class public LX/1Fq;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/imagepipeline/animated/factory/AnimatedImageFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1GA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 224657
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1GA;
    .locals 3

    .prologue
    .line 224658
    sget-object v0, LX/1Fq;->a:LX/1GA;

    if-nez v0, :cond_1

    .line 224659
    const-class v1, LX/1Fq;

    monitor-enter v1

    .line 224660
    :try_start_0
    sget-object v0, LX/1Fq;->a:LX/1GA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 224661
    if-eqz v2, :cond_0

    .line 224662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 224663
    invoke-static {v0}, LX/1Fr;->a(LX/0QB;)Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    move-result-object p0

    check-cast p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    invoke-static {p0}, LX/1Aq;->a(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1GA;

    move-result-object p0

    move-object v0, p0

    .line 224664
    sput-object v0, LX/1Fq;->a:LX/1GA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224665
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 224666
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 224667
    :cond_1
    sget-object v0, LX/1Fq;->a:LX/1GA;

    return-object v0

    .line 224668
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 224669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224670
    invoke-static {p0}, LX/1Fr;->a(LX/0QB;)Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    invoke-static {v0}, LX/1Aq;->a(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1GA;

    move-result-object v0

    return-object v0
.end method
