.class public final LX/1o8;
.super LX/0Zi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Zi",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 318443
    invoke-direct {p0, p1}, LX/0Zi;-><init>(I)V

    .line 318444
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/1o8;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 318445
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 318446
    invoke-super {p0}, LX/0Zi;->a()Ljava/lang/Object;

    move-result-object v0

    .line 318447
    if-eqz v0, :cond_0

    .line 318448
    iget-object v1, p0, LX/1o8;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 318449
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 318450
    invoke-super {p0, p1}, LX/0Zi;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 318451
    if-eqz v0, :cond_0

    .line 318452
    iget-object v1, p0, LX/1o8;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 318453
    :cond_0
    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 318454
    iget-object v0, p0, LX/1o8;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method
