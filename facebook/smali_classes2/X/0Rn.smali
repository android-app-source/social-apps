.class public final LX/0Rn;
.super LX/0Ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Ro",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:LX/0Rl;

.field public final synthetic c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0Rl;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 60456
    iput-object p1, p0, LX/0Rn;->a:Ljava/util/Set;

    iput-object p2, p0, LX/0Rn;->b:LX/0Rl;

    iput-object p3, p0, LX/0Rn;->c:Ljava/util/Set;

    invoke-direct {p0}, LX/0Ro;-><init>()V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 60457
    iget-object v0, p0, LX/0Rn;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Rn;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 60458
    iget-object v0, p0, LX/0Rn;->c:Ljava/util/Set;

    iget-object v1, p0, LX/0Rn;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60459
    iget-object v0, p0, LX/0Rn;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, LX/0Rn;->b:LX/0Rl;

    invoke-static {v0, v1}, LX/0RZ;->b(Ljava/util/Iterator;LX/0Rl;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 60460
    invoke-virtual {p0}, LX/0Rn;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
