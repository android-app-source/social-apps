.class public LX/16Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public volatile b:Z

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/16R;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "LX/16R;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field public final f:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 185211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185212
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iput-object v0, p0, LX/16Q;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 185213
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/16Q;->c:Ljava/util/Map;

    .line 185214
    invoke-static {}, LX/0RA;->d()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    .line 185215
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/16Q;->b:Z

    .line 185216
    iput-object p2, p0, LX/16Q;->e:Ljava/lang/String;

    .line 185217
    new-instance v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added Reason: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/16Q;->f:Ljava/lang/Throwable;

    .line 185218
    return-void
.end method

.method private declared-synchronized c(LX/16P;I)Z
    .locals 3

    .prologue
    .line 185219
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185220
    iget-object v0, p1, LX/16P;->a:Ljava/lang/String;

    .line 185221
    new-instance v1, LX/16R;

    invoke-direct {v1, p2, p1}, LX/16R;-><init>(ILX/16P;)V

    .line 185222
    iget-object v2, p0, LX/16Q;->c:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185223
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185224
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 185225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/16Q;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 185205
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/16Q;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;

    .line 185206
    if-eqz v0, :cond_0

    .line 185207
    iget-object v1, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185208
    const/4 v0, 0x1

    .line 185209
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 185210
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static h(LX/16Q;)V
    .locals 2

    .prologue
    .line 185203
    iget-boolean v0, p0, LX/16Q;->b:Z

    const-string v1, "Before removing all trigger controllers must be known to be fully restored!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 185204
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 185201
    iget-boolean v0, p0, LX/16Q;->b:Z

    const-string v1, "Trigger %s is not know to be fully restored!"

    iget-object v2, p0, LX/16Q;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 185202
    return-void
.end method

.method public final declared-synchronized a(LX/16P;I)Z
    .locals 2

    .prologue
    .line 185194
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185195
    iget-object v0, p1, LX/16P;->a:Ljava/lang/String;

    .line 185196
    iget-object v1, p0, LX/16Q;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;

    .line 185197
    if-eqz v0, :cond_0

    .line 185198
    invoke-virtual {p0, p1, p2}, LX/16Q;->b(LX/16P;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 185199
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, LX/16Q;->c(LX/16P;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 185200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 185191
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/16Q;->b:Z

    const-string v1, "Before checking contain trigger controllers must be known to be fully restored!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 185192
    iget-object v0, p0, LX/16Q;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 185193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/16P;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 185226
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/16Q;->h(LX/16Q;)V

    .line 185227
    const/4 v0, 0x0

    .line 185228
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 185229
    iget-object v0, v0, LX/16P;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/16Q;->c(LX/16Q;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 185230
    goto :goto_0

    .line 185231
    :cond_0
    monitor-exit p0

    return v1

    .line 185232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/16P;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 185159
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185160
    iget-object v0, p1, LX/16P;->a:Ljava/lang/String;

    .line 185161
    iget-object v2, p0, LX/16Q;->c:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185162
    if-nez v0, :cond_0

    move v0, v1

    .line 185163
    :goto_0
    monitor-exit p0

    return v0

    .line 185164
    :cond_0
    :try_start_1
    iget v2, v0, LX/16R;->a:I

    if-ne v2, p2, :cond_1

    move v0, v1

    .line 185165
    goto :goto_0

    .line 185166
    :cond_1
    iget-object v1, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 185167
    invoke-direct {p0, p1, p2}, LX/16Q;->c(LX/16P;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 185168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 185169
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/16Q;->h(LX/16Q;)V

    .line 185170
    invoke-static {p0, p1}, LX/16Q;->c(LX/16Q;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 185171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 185172
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/16Q;->b:Z

    .line 185173
    return-void
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 185175
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;

    .line 185176
    invoke-virtual {v0}, LX/16R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 185178
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized e()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/16P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 185180
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;

    .line 185181
    iget-object v0, v0, LX/16R;->b:LX/16P;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 185183
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185185
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185186
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 185187
    iget-object v0, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16R;

    .line 185188
    invoke-virtual {v0}, LX/16R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 185190
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[Debug cause: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/16Q;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", currentInterstitials: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185184
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "KnowinglyFullyRestored"

    iget-boolean v2, p0, LX/16Q;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "Trigger"

    iget-object v2, p0, LX/16Q;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "RankedInterstitials"

    iget-object v2, p0, LX/16Q;->d:Ljava/util/SortedSet;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
