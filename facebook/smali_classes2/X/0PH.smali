.class public final enum LX/0PH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0PH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0PH;

.field public static final enum NAME:LX/0PH;

.field public static final enum PHONE_E164:LX/0PH;

.field public static final enum PHONE_LOCAL:LX/0PH;

.field public static final enum PHONE_NATIONAL:LX/0PH;

.field public static final enum PHONE_VERIFIED:LX/0PH;

.field public static final enum USERNAME:LX/0PH;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55606
    new-instance v0, LX/0PH;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v3}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->NAME:LX/0PH;

    .line 55607
    new-instance v0, LX/0PH;

    const-string v1, "PHONE_E164"

    invoke-direct {v0, v1, v4}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->PHONE_E164:LX/0PH;

    .line 55608
    new-instance v0, LX/0PH;

    const-string v1, "PHONE_NATIONAL"

    invoke-direct {v0, v1, v5}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->PHONE_NATIONAL:LX/0PH;

    .line 55609
    new-instance v0, LX/0PH;

    const-string v1, "PHONE_LOCAL"

    invoke-direct {v0, v1, v6}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->PHONE_LOCAL:LX/0PH;

    .line 55610
    new-instance v0, LX/0PH;

    const-string v1, "PHONE_VERIFIED"

    invoke-direct {v0, v1, v7}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->PHONE_VERIFIED:LX/0PH;

    .line 55611
    new-instance v0, LX/0PH;

    const-string v1, "USERNAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0PH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0PH;->USERNAME:LX/0PH;

    .line 55612
    const/4 v0, 0x6

    new-array v0, v0, [LX/0PH;

    sget-object v1, LX/0PH;->NAME:LX/0PH;

    aput-object v1, v0, v3

    sget-object v1, LX/0PH;->PHONE_E164:LX/0PH;

    aput-object v1, v0, v4

    sget-object v1, LX/0PH;->PHONE_NATIONAL:LX/0PH;

    aput-object v1, v0, v5

    sget-object v1, LX/0PH;->PHONE_LOCAL:LX/0PH;

    aput-object v1, v0, v6

    sget-object v1, LX/0PH;->PHONE_VERIFIED:LX/0PH;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0PH;->USERNAME:LX/0PH;

    aput-object v2, v0, v1

    sput-object v0, LX/0PH;->$VALUES:[LX/0PH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55613
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0PH;
    .locals 1

    .prologue
    .line 55614
    const-class v0, LX/0PH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0PH;

    return-object v0
.end method

.method public static values()[LX/0PH;
    .locals 1

    .prologue
    .line 55615
    sget-object v0, LX/0PH;->$VALUES:[LX/0PH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0PH;

    return-object v0
.end method
