.class public LX/0pl;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0pl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Tf;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fM;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fJ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0pm;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/os/Handler;

.field public final h:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0YG",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0YG",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final j:LX/0ad;

.field public final k:LX/0qW;

.field public final l:LX/0pn;

.field private final m:LX/0qX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145234
    const-class v0, LX/0pl;

    sput-object v0, LX/0pl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Tf;LX/0Ot;LX/0Ot;LX/0pm;LX/0Ot;Landroid/os/Handler;LX/0qW;LX/0ad;LX/0pn;LX/0qX;)V
    .locals 2
    .param p1    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tf;",
            "LX/0Ot",
            "<",
            "LX/1fM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1fJ;",
            ">;",
            "LX/0pm;",
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0qW;",
            "LX/0ad;",
            "LX/0pn;",
            "LX/0qX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 145220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145221
    iput-object p8, p0, LX/0pl;->j:LX/0ad;

    .line 145222
    iput-object p1, p0, LX/0pl;->b:LX/0Tf;

    .line 145223
    iput-object p2, p0, LX/0pl;->c:LX/0Ot;

    .line 145224
    iput-object p3, p0, LX/0pl;->d:LX/0Ot;

    .line 145225
    iput-object p4, p0, LX/0pl;->e:LX/0pm;

    .line 145226
    iput-object p5, p0, LX/0pl;->f:LX/0Ot;

    .line 145227
    iput-object p6, p0, LX/0pl;->g:Landroid/os/Handler;

    .line 145228
    iput-object p7, p0, LX/0pl;->k:LX/0qW;

    .line 145229
    iput-object p9, p0, LX/0pl;->l:LX/0pn;

    .line 145230
    iput-object p10, p0, LX/0pl;->m:LX/0qX;

    .line 145231
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0pl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    .line 145232
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0pl;->i:Ljava/util/concurrent/atomic/AtomicReference;

    .line 145233
    return-void
.end method

.method public static a(LX/0QB;)LX/0pl;
    .locals 1

    .prologue
    .line 145208
    invoke-static {p0}, LX/0pl;->b(LX/0QB;)LX/0pl;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/0pl;Lcom/facebook/api/feed/FetchFeedResult;LX/0rl;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            "LX/0rl",
            "<",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145216
    iget-object v0, p0, LX/0pl;->e:LX/0pm;

    invoke-virtual {v0, p1}, LX/0pm;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 145217
    iget-object v0, p0, LX/0pl;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/187;

    invoke-virtual {v0, v1}, LX/187;->c(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 145218
    iget-object v0, p0, LX/0pl;->g:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/data/FeedDataLoaderReranker$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/feed/data/FeedDataLoaderReranker$4;-><init>(LX/0pl;LX/0rl;Lcom/facebook/api/feed/FetchFeedResult;)V

    const v2, -0x7d37b653

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 145219
    return-void
.end method

.method public static b(LX/0QB;)LX/0pl;
    .locals 11

    .prologue
    .line 145214
    new-instance v0, LX/0pl;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, LX/0Tf;

    const/16 v2, 0xf5

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xf4

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0pm;->a(LX/0QB;)LX/0pm;

    move-result-object v4

    check-cast v4, LX/0pm;

    const/16 v5, 0x670

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {p0}, LX/0qW;->a(LX/0QB;)LX/0qW;

    move-result-object v7

    check-cast v7, LX/0qW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v9

    check-cast v9, LX/0pn;

    invoke-static {p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v10

    check-cast v10, LX/0qX;

    invoke-direct/range {v0 .. v10}, LX/0pl;-><init>(LX/0Tf;LX/0Ot;LX/0Ot;LX/0pm;LX/0Ot;Landroid/os/Handler;LX/0qW;LX/0ad;LX/0pn;LX/0qX;)V

    .line 145215
    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 145212
    const/4 v1, 0x1

    move v1, v1

    .line 145213
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0pl;->m:LX/0qX;

    invoke-virtual {v1, v0}, LX/0qX;->c(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final e()Z
    .locals 1

    .prologue
    .line 145211
    invoke-direct {p0}, LX/0pl;->k()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 145210
    invoke-direct {p0}, LX/0pl;->k()Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 145209
    invoke-direct {p0}, LX/0pl;->k()Z

    move-result v0

    return v0
.end method
