.class public LX/1Kh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ki;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MC;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HwR;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HvL;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1MC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HwR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HvL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 232991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232992
    iput-object p1, p0, LX/1Kh;->a:LX/0Ot;

    .line 232993
    iput-object p2, p0, LX/1Kh;->b:LX/0Ot;

    .line 232994
    iput-object p3, p0, LX/1Kh;->c:LX/0Ot;

    .line 232995
    iput-object p4, p0, LX/1Kh;->d:LX/0Ot;

    .line 232996
    iput-object p5, p0, LX/1Kh;->e:LX/0Ot;

    .line 232997
    iput-object p6, p0, LX/1Kh;->f:LX/0Ot;

    .line 232998
    iput-object p7, p0, LX/1Kh;->g:LX/0Ot;

    .line 232999
    return-void
.end method

.method private static a(LX/1Kh;Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 4

    .prologue
    .line 233056
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233057
    :cond_0
    :goto_0
    return-object p1

    .line 233058
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    move v1, v0

    .line 233059
    :goto_1
    iget-object v0, p0, LX/1Kh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HwR;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/HwR;->a(Ljava/lang/String;LX/0Px;Z)LX/7ks;

    move-result-object v0

    .line 233060
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v1

    iget-object v2, v0, LX/7ks;->a:LX/0Px;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233061
    invoke-static {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    iget-object v0, v0, LX/7ks;->a:LX/0Px;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setAttachments(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object p1

    goto :goto_0

    .line 233062
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
    .locals 11

    .prologue
    .line 233036
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233037
    iget-object v0, p0, LX/1Kh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HvL;

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 233038
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233039
    iget-object v3, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233040
    iget-object v3, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233041
    iget-object v3, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v7

    .line 233042
    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 233043
    iget-object v4, v0, LX/HvL;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/74n;

    iget-object v5, v0, LX/HvL;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/75Q;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(LX/74n;LX/75Q;)V

    .line 233044
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .line 233045
    :cond_0
    iget-object v3, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v7

    :goto_1
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 233046
    iget-object v4, v0, LX/HvL;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/74n;

    iget-object v5, v0, LX/HvL;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/75Q;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(LX/74n;LX/75Q;)V

    .line 233047
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    .line 233048
    :cond_1
    iget-object v3, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    new-instance v4, LX/7lP;

    invoke-direct {v4}, LX/7lP;-><init>()V

    .line 233049
    iput-boolean v7, v4, LX/7lP;->a:Z

    .line 233050
    move-object v4, v4

    .line 233051
    iput-boolean v10, v4, LX/7lP;->c:Z

    .line 233052
    move-object v4, v4

    .line 233053
    invoke-virtual {v4}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v4

    iget-object v3, v0, LX/HvL;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->c(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    move-object v0, v3

    .line 233054
    invoke-static {p0, v0}, LX/1Kh;->a(LX/1Kh;Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    .line 233055
    new-instance v1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    iget-object v2, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Ljava/lang/String;)V

    return-object v1
.end method

.method public static b(LX/0QB;)LX/1Kh;
    .locals 8

    .prologue
    .line 233063
    new-instance v0, LX/1Kh;

    const/16 v1, 0x3d7

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x19e4

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x19d7

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2e3

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3bf

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xac0

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/1Kh;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 233064
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 233025
    iget-object v0, p0, LX/1Kh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MC;

    invoke-virtual {v0}, LX/1MC;->a()Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    move-result-object v0

    .line 233026
    if-nez v0, :cond_0

    move-object v0, v1

    .line 233027
    :goto_0
    return-object v0

    .line 233028
    :cond_0
    :try_start_0
    invoke-direct {p0, v0}, LX/1Kh;->a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 233029
    iget-object v0, p0, LX/1Kh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    sget-object v2, LX/0ge;->LOAD_SAVED_SESSION:LX/0ge;

    .line 233030
    iget-object v3, v1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v3, v3

    .line 233031
    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    move-object v0, v1

    .line 233032
    goto :goto_0

    .line 233033
    :catch_0
    move-exception v2

    .line 233034
    iget-object v0, p0, LX/1Kh;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "composer_load_model_from_saved_session_failed"

    const-string v4, "Failed to load the composer model from a saved session"

    invoke-virtual {v0, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 233035
    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233000
    if-nez p2, :cond_0

    .line 233001
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    .line 233002
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->OTHER:LX/2rw;

    if-ne v0, v3, :cond_1

    .line 233003
    iget-object v0, p0, LX/1Kh;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "composer_no_target_type"

    const-string v4, "Composer target data does not have a valid target type specified"

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233004
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    sget-object v3, LX/21D;->INVALID:LX/21D;

    if-eq v0, v3, :cond_6

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 233005
    iget-object v0, p0, LX/1Kh;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v3, LX/7l1;->a:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233006
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233007
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    const-string v3, "It\'s posting as Page but Page name is null or empty."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 233008
    :cond_2
    invoke-static {}, Lcom/facebook/composer/system/model/ComposerModelImpl;->newBuilder()Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    iget-object v0, p0, LX/1Kh;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setConfiguration(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    new-instance v3, LX/7lP;

    invoke-direct {v3}, LX/7lP;-><init>()V

    .line 233009
    iput-boolean v2, v3, LX/7lP;->a:Z

    .line 233010
    move-object v2, v3

    .line 233011
    iput-boolean v1, v2, LX/7lP;->c:Z

    .line 233012
    move-object v1, v2

    .line 233013
    invoke-virtual {v1}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setAttachments(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTargetAlbum(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTaggedUsers(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setMinutiaeObject(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRating()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setRating(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setReferencedStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setStorylineData(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    .line 233014
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 233015
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTextWithEntities(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 233016
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    sget-object v2, LX/2rt;->SELL:LX/2rt;

    if-ne v0, v2, :cond_4

    .line 233017
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 233018
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    sget-object v2, LX/2rt;->LIFE_EVENT:LX/2rt;

    if-ne v0, v2, :cond_5

    .line 233019
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 233020
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    .line 233021
    invoke-static {p0, v0}, LX/1Kh;->a(LX/1Kh;Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    .line 233022
    new-instance v1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Ljava/lang/String;)V

    return-object v1

    :cond_6
    move v0, v2

    .line 233023
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 233024
    goto/16 :goto_1
.end method
