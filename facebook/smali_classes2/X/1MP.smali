.class public LX/1MP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile H:LX/1MP;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:Z

.field private final D:LX/1h4;

.field private final E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WJ;",
            ">;"
        }
    .end annotation
.end field

.field private final F:LX/0Uh;

.field private final G:LX/1hD;

.field private final d:Ljava/lang/Thread;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1gy;

.field private final l:LX/1hE;

.field private final m:Lcom/facebook/proxygen/HTTPThread;

.field private final n:LX/0ad;

.field private final o:Landroid/content/Context;

.field private final p:LX/0oz;

.field private final q:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public r:Lcom/facebook/proxygen/HTTPClient;

.field public s:LX/1hL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final t:LX/03V;

.field private final u:LX/1MW;

.field private final v:LX/1Mg;

.field private final w:LX/1hI;

.field private final x:LX/0YR;

.field private final y:LX/1hK;

.field public final z:LX/1BA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 235247
    const-class v0, LX/1MP;

    sput-object v0, LX/1MP;->a:Ljava/lang/Class;

    .line 235248
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "b-graph.facebook.com"

    aput-object v1, v0, v2

    sput-object v0, LX/1MP;->b:[Ljava/lang/String;

    .line 235249
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    sput-object v0, LX/1MP;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/1MR;LX/1MU;LX/1MW;LX/1Mg;LX/03V;LX/0pZ;LX/0ad;LX/1Mj;Landroid/content/Context;LX/0oz;LX/1Mk;LX/0y2;LX/0Or;LX/0YR;LX/18b;LX/0kb;LX/0Xl;LX/1gy;LX/0Uo;LX/1h1;LX/0Zb;LX/0dx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/1BA;Ljava/util/Set;LX/1h4;LX/1h9;LX/0Ot;LX/1hD;)V
    .locals 22
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldReportFullNetworkState;
        .end annotation
    .end param
    .param p18    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse",
            "ConstructorMayLeakThis",
            "HardcodedIPAddressUse"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1MR;",
            "LX/1MU;",
            "LX/1MW;",
            "Lcom/facebook/http/config/NetworkConfig;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0pZ;",
            "LX/0ad;",
            "LX/1Mj;",
            "Landroid/content/Context;",
            "LX/0oz;",
            "LX/1Mk;",
            "LX/0y2;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0YR;",
            "LX/18b;",
            "LX/0kb;",
            "LX/0Xl;",
            "LX/1gy;",
            "LX/0Uo;",
            "LX/1h1;",
            "LX/0Zb;",
            "LX/0dx;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/http/engine/HttpPushCallback;",
            ">;",
            "LX/1h4;",
            "LX/1h9;",
            "LX/0Ot",
            "<",
            "LX/0WJ;",
            ">;",
            "LX/1hD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235400
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 235401
    new-instance v4, Lcom/facebook/proxygen/HTTPThread;

    invoke-direct {v4}, Lcom/facebook/proxygen/HTTPThread;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    .line 235402
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->s:LX/1hL;

    .line 235403
    :try_start_0
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->F:LX/0Uh;

    .line 235404
    move-object/from16 v0, p26

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->z:LX/1BA;

    .line 235405
    new-instance v4, LX/1hE;

    move-object/from16 v0, p6

    invoke-direct {v4, v0}, LX/1hE;-><init>(LX/03V;)V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->l:LX/1hE;

    .line 235406
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->l:LX/1hE;

    invoke-virtual {v4}, LX/1hE;->init()V

    .line 235407
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->o:Landroid/content/Context;

    .line 235408
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->k:LX/1gy;

    .line 235409
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->j:LX/0Or;

    .line 235410
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->u:LX/1MW;

    .line 235411
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->v:LX/1Mg;

    .line 235412
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->t:LX/03V;

    .line 235413
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->p:LX/0oz;

    .line 235414
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->n:LX/0ad;

    .line 235415
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->x:LX/0YR;

    .line 235416
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 235417
    new-instance v4, Lcom/facebook/http/executors/liger/LigerRequestExecutor$ThreadPriorityRunnable;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/facebook/http/executors/liger/LigerRequestExecutor$ThreadPriorityRunnable;-><init>(LX/1MP;Ljava/lang/Runnable;)V

    const-string v5, "Liger-EventBase"

    const v6, -0x995e30

    invoke-static {v4, v5, v6}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->d:Ljava/lang/Thread;

    .line 235418
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->d:Ljava/lang/Thread;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setPriority(I)V

    .line 235419
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->d:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 235420
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    invoke-virtual {v4}, Lcom/facebook/proxygen/HTTPThread;->waitForInitialization()V

    .line 235421
    new-instance v4, LX/1hI;

    move-object/from16 v0, p12

    move-object/from16 v1, p14

    move-object/from16 v2, p13

    move-object/from16 v3, p8

    invoke-direct {v4, v0, v1, v2, v3}, LX/1hI;-><init>(LX/1Mk;LX/0Or;LX/0y2;LX/0ad;)V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->w:LX/1hI;

    .line 235422
    move-object/from16 v0, p28

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->D:LX/1h4;

    .line 235423
    move-object/from16 v0, p30

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->E:LX/0Ot;

    .line 235424
    move-object/from16 v0, p31

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1MP;->G:LX/1hD;

    .line 235425
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    invoke-virtual {v4}, Lcom/facebook/proxygen/HTTPThread;->getEventBase()Lcom/facebook/proxygen/EventBase;

    move-result-object v4

    invoke-static {v4}, LX/1MR;->a(Lcom/facebook/proxygen/EventBase;)V

    .line 235426
    invoke-direct/range {p0 .. p0}, LX/1MP;->s()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/1MU;->a(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)V

    .line 235427
    invoke-virtual/range {p5 .. p5}, LX/1Mg;->c()Z

    move-result v17

    .line 235428
    invoke-direct/range {p0 .. p0}, LX/1MP;->f()Landroid/util/Pair;

    move-result-object v18

    .line 235429
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget v5, LX/0by;->G:I

    const/16 v6, 0x2710

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, LX/1MP;->A:I

    .line 235430
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget v5, LX/0by;->u:I

    const/16 v6, 0x2710

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, LX/1MP;->B:I

    .line 235431
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget-short v5, LX/0by;->H:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/1MP;->C:Z

    .line 235432
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget-short v5, LX/0by;->x:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v19

    .line 235433
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget-char v5, LX/0by;->w:C

    const-string v6, "31.13.73.1"

    invoke-interface {v4, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 235434
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->n:LX/0ad;

    sget v5, LX/0by;->y:I

    const/16 v6, 0x3e8

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v21

    .line 235435
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->x:LX/0YR;

    invoke-interface {v4}, LX/0YR;->f()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235436
    new-instance v15, LX/1hK;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->x:LX/0YR;

    move-object/from16 v0, p22

    move-object/from16 v1, p11

    move-object/from16 v2, p17

    invoke-direct {v15, v0, v1, v2, v4}, LX/1hK;-><init>(LX/0Zb;LX/0oz;LX/0kb;LX/0YR;)V

    .line 235437
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->F:LX/0Uh;

    const/16 v5, 0x5c7

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v16

    .line 235438
    new-instance v4, LX/1hL;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->o:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    invoke-virtual {v6}, Lcom/facebook/proxygen/HTTPThread;->getEventBase()Lcom/facebook/proxygen/EventBase;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1MP;->x:LX/0YR;

    invoke-interface {v7}, LX/0YR;->c()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1MP;->x:LX/0YR;

    invoke-interface {v8}, LX/0YR;->d()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->x:LX/0YR;

    invoke-interface {v9}, LX/0YR;->e()I

    move-result v9

    move-object/from16 v10, p16

    move-object/from16 v11, p17

    move-object/from16 v12, p18

    move-object/from16 v13, p12

    move-object/from16 v14, p20

    invoke-direct/range {v4 .. v16}, LX/1hL;-><init>(Landroid/content/Context;Lcom/facebook/proxygen/EventBase;IIILX/18b;LX/0kb;LX/0Xl;LX/1Mk;LX/0Uo;Lcom/facebook/proxygen/AnalyticsLogger;Z)V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->s:LX/1hL;

    .line 235439
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->s:LX/1hL;

    invoke-virtual {v4}, LX/1hL;->g()V

    .line 235440
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->x:LX/0YR;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->s:LX/1hL;

    invoke-interface {v4, v5}, LX/0YR;->a(LX/0YR;)V

    .line 235441
    if-eqz v16, :cond_0

    .line 235442
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->s:LX/1hL;

    .line 235443
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->z:LX/1BA;

    new-instance v6, LX/1uC;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v4}, LX/1uC;-><init>(LX/1MP;LX/1hL;)V

    invoke-virtual {v5, v6}, LX/1BA;->a(LX/1iq;)V

    .line 235444
    :cond_0
    new-instance v4, LX/1hK;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->x:LX/0YR;

    move-object/from16 v0, p22

    move-object/from16 v1, p11

    move-object/from16 v2, p17

    invoke-direct {v4, v0, v1, v2, v5}, LX/1hK;-><init>(LX/0Zb;LX/0oz;LX/0kb;LX/0YR;)V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->y:LX/1hK;

    .line 235445
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1MP;->n:LX/0ad;

    sget v7, LX/0by;->F:I

    const/16 v8, 0x2710

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    int-to-double v6, v6

    div-double v6, v4, v6

    .line 235446
    invoke-interface/range {p21 .. p21}, LX/1h1;->a()LX/0Px;

    move-result-object v4

    .line 235447
    invoke-interface/range {p21 .. p21}, LX/1h1;->b()LX/0Px;

    move-result-object v5

    .line 235448
    invoke-static {v4}, LX/1MP;->a(Ljava/util/List;)[Lcom/facebook/proxygen/RewriteRule;

    move-result-object v8

    .line 235449
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 235450
    invoke-direct/range {p0 .. p0}, LX/1MP;->x()V

    .line 235451
    new-instance v5, Lcom/facebook/proxygen/HTTPClient;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->m:Lcom/facebook/proxygen/HTTPThread;

    invoke-virtual {v9}, Lcom/facebook/proxygen/HTTPThread;->getEventBase()Lcom/facebook/proxygen/EventBase;

    move-result-object v9

    invoke-direct {v5, v9}, Lcom/facebook/proxygen/HTTPClient;-><init>(Lcom/facebook/proxygen/EventBase;)V

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setZlibFilter(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v10, v0, LX/1MP;->f:I

    const-string v11, ""

    const-string v12, ""

    invoke-virtual {v5, v9, v10, v11, v12}, Lcom/facebook/proxygen/HTTPClient;->setProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v10, v0, LX/1MP;->h:I

    const-string v11, ""

    const-string v12, ""

    invoke-virtual {v5, v9, v10, v11, v12}, Lcom/facebook/proxygen/HTTPClient;->setSecureProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->i:Ljava/lang/String;

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setBypassProxyDomains(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setProxyFallbackEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->t()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setPersistentSSLCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/facebook/proxygen/HTTPClient;->setIsSandbox(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v9

    if-nez v17, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {v9, v5}, Lcom/facebook/proxygen/HTTPClient;->setHTTPSEnforced(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const-string v9, "adv"

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setHTTPSessionCacheType(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->j()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setMaxIdleHTTPSessions(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->k()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setMaxIdleSPDYSessions(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->l()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setMaxIdleHTTPSessions2G(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->m()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setMaxIdleSPDYSessions2G(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const v9, 0xd6d8

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setIdleTimeoutForUsed(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const v9, 0xd6d8

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setIdleTimeoutForUnused(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->n()Z

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setPerDomainLimitEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->o()Z

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setPerDomainLimitEnabled2G(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->p()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setPreConnects(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->G:LX/1hD;

    iget-object v9, v9, LX/1hD;->a:Ljava/lang/String;

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setActiveDomains([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->G:LX/1hD;

    iget v9, v9, LX/1hD;->b:I

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setMinDomainRefereshInterval(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, LX/1MP;->s()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setPersistentDNSCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const-wide/16 v10, 0x7530

    invoke-virtual {v5, v10, v11}, Lcom/facebook/proxygen/HTTPClient;->setNewConnectionTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const-wide/32 v10, 0xea60

    invoke-virtual {v5, v10, v11}, Lcom/facebook/proxygen/HTTPClient;->setTransactionIdleTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const-wide/32 v10, 0xea60

    invoke-virtual {v5, v10, v11}, Lcom/facebook/proxygen/HTTPClient;->setSessionWriteTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-static {}, LX/1MP;->w()[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setDNSServers([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->n:LX/0ad;

    sget-short v10, LX/0by;->T:S

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, LX/0ad;->a(SZ)Z

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setDNSCacheEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->G:LX/1hD;

    iget-boolean v9, v9, LX/1hD;->n:Z

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setCAresEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->n:LX/0ad;

    sget-object v10, LX/0c0;->Cached:LX/0c0;

    sget-object v11, LX/0c1;->Off:LX/0c1;

    sget v12, LX/0by;->U:I

    const/4 v13, 0x1

    invoke-interface {v9, v10, v11, v12, v13}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setDnsRequestsOutstanding(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setCircularLogSinkEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1MP;->s:LX/1hL;

    invoke-virtual {v5, v9}, Lcom/facebook/proxygen/HTTPClient;->setNetworkStatusMonitor(Lcom/facebook/proxygen/NetworkStatusMonitor;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/facebook/proxygen/HTTPClient;->setRewriteRules([Lcom/facebook/proxygen/RewriteRule;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/facebook/proxygen/HTTPClient;->setRewriteExemptions([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->y:LX/1hK;

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/proxygen/HTTPClient;->setAnalyticsLogger(Lcom/facebook/proxygen/AnalyticsLogger;D)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->n:LX/0ad;

    sget-short v6, LX/0by;->Z:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setTLSCachedInfoEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->v()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setTLSCachedInfoSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setTransportCallbackEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/facebook/proxygen/HTTPClient;->setGatewayPingEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/facebook/proxygen/HTTPClient;->setGatewayPingAddress(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/facebook/proxygen/HTTPClient;->setGatewayPingIntervalMs(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, v18

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lcom/facebook/proxygen/HTTPClient;->setCrossDomainTCPConnsEnabled(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    move-object/from16 v0, v18

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lcom/facebook/proxygen/HTTPClient;->setUpdateDNSAfterTCPReuse(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->x:LX/0YR;

    invoke-interface {v5}, LX/0YR;->e()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAdaptiveConfigInterval(I)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->g()Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAdaptiveConnTOParam(Lcom/facebook/proxygen/AdaptiveIntegerParameters;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->h()Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAdaptiveSessionTOParam(Lcom/facebook/proxygen/AdaptiveIntegerParameters;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->q()Lcom/facebook/proxygen/SSLVerificationSettings;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setSSLVerificationSettings(Lcom/facebook/proxygen/SSLVerificationSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->r()Lcom/facebook/proxygen/ZeroProtocolSettings;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setZeroProtocolSettings(Lcom/facebook/proxygen/ZeroProtocolSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x28000

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/proxygen/HTTPClient;->setFlowControl(ZIZ)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->F:LX/0Uh;

    const/16 v6, 0xe8

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setEnableCachingPushManager(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v5

    invoke-interface/range {p27 .. p27}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v5, v4}, Lcom/facebook/proxygen/HTTPClient;->setPushCallbacks(Lcom/facebook/proxygen/PushCallbacks;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->G:LX/1hD;

    iget-boolean v5, v5, LX/1hD;->c:Z

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setLoadBalancing(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-static/range {p10 .. p10}, LX/02T;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setPreconnectFilePath(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->G:LX/1hD;

    iget-boolean v5, v5, LX/1hD;->j:Z

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAllowPreconnect(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->G:LX/1hD;

    iget-object v5, v5, LX/1hD;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setActiveProbeJson(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    new-instance v5, LX/1hX;

    move-object/from16 v0, p22

    invoke-direct {v5, v0}, LX/1hX;-><init>(LX/0Zb;)V

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAsyncTCPProbeCallback(Lcom/facebook/proxygen/AsyncTCPProbeCallback;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->G:LX/1hD;

    iget-boolean v5, v5, LX/1hD;->l:Z

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAllowBrotli(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1MP;->G:LX/1hD;

    iget-boolean v5, v5, LX/1hD;->m:Z

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setAllowZstd(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, LX/1MP;->u()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setPersistentZstdCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    .line 235452
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0dU;->k:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 235453
    if-eqz v4, :cond_1

    .line 235454
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    invoke-virtual/range {p9 .. p9}, LX/1Mj;->b()[[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setUserInstalledCertificates([[B)Lcom/facebook/proxygen/HTTPClient;

    .line 235455
    :cond_1
    new-instance v4, LX/1hY;

    move-object/from16 v0, p0

    move-object/from16 v1, p21

    invoke-direct {v4, v0, v1}, LX/1hY;-><init>(LX/1MP;LX/1h1;)V

    move-object/from16 v0, p21

    invoke-interface {v0, v4}, LX/1h1;->a(LX/1hZ;)V

    .line 235456
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    invoke-virtual {v4}, Lcom/facebook/proxygen/HTTPClient;->init()V

    .line 235457
    invoke-static {}, LX/1MR;->a()V

    .line 235458
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    move-object/from16 v0, p29

    invoke-virtual {v0, v4}, LX/1h9;->a(Lcom/facebook/proxygen/HTTPClient;)LX/1ha;

    .line 235459
    return-void

    .line 235460
    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_3
    new-instance v4, LX/1hW;

    move-object/from16 v0, p27

    invoke-direct {v4, v0}, LX/1hW;-><init>(Ljava/util/Set;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 235461
    :catch_0
    move-exception v4

    .line 235462
    new-instance v5, LX/6XY;

    invoke-direct {v5, v4}, LX/6XY;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public static a(LX/0QB;)LX/1MP;
    .locals 3

    .prologue
    .line 235463
    sget-object v0, LX/1MP;->H:LX/1MP;

    if-nez v0, :cond_1

    .line 235464
    const-class v1, LX/1MP;

    monitor-enter v1

    .line 235465
    :try_start_0
    sget-object v0, LX/1MP;->H:LX/1MP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235466
    if-eqz v2, :cond_0

    .line 235467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1MP;->b(LX/0QB;)LX/1MP;

    move-result-object v0

    sput-object v0, LX/1MP;->H:LX/1MP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235468
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235469
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235470
    :cond_1
    sget-object v0, LX/1MP;->H:LX/1MP;

    return-object v0

    .line 235471
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 7

    .prologue
    .line 235473
    :try_start_0
    instance-of v1, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v1, :cond_3

    .line 235474
    move-object v0, p0

    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    move-object v1, v0

    .line 235475
    invoke-interface {v1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 235476
    if-eqz v2, :cond_2

    .line 235477
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gez v3, :cond_4

    .line 235478
    :cond_0
    const-string v3, "Transfer-Encoding"

    const-string v4, "chunked"

    invoke-interface {p0, v3, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 235479
    :goto_0
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 235480
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v3

    invoke-interface {p0, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Lorg/apache/http/Header;)V

    .line 235481
    :cond_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 235482
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v3

    invoke-interface {p0, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Lorg/apache/http/Header;)V

    .line 235483
    :cond_2
    new-instance v2, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;

    invoke-direct {v2, v1}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;-><init>(Lorg/apache/http/HttpEntityEnclosingRequest;)V

    move-object v1, v2

    .line 235484
    :goto_1
    invoke-virtual {v1}, Lorg/apache/http/impl/client/RequestWrapper;->resetHeaders()V

    .line 235485
    return-object v1

    .line 235486
    :cond_3
    new-instance v1, Lorg/apache/http/impl/client/RequestWrapper;

    invoke-direct {v1, p0}, Lorg/apache/http/impl/client/RequestWrapper;-><init>(Lorg/apache/http/HttpRequest;)V
    :try_end_0
    .catch Lorg/apache/http/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 235487
    :catch_0
    move-exception v1

    .line 235488
    new-instance v2, Lorg/apache/http/client/ClientProtocolException;

    invoke-direct {v2, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 235489
    :cond_4
    const-string v3, "Content-Length"

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v5

    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0, v3, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/1MP;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 235490
    :try_start_0
    iget-object v0, p0, LX/1MP;->z:LX/1BA;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, LX/1BA;->a(IJ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235491
    :goto_0
    return-void

    .line 235492
    :catch_0
    move-exception v0

    .line 235493
    sget-object v1, LX/1MP;->a:Ljava/lang/Class;

    const-string v2, "Invalid long value: %s for key: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;ILX/5oc;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 235494
    instance-of v2, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v2, :cond_1

    .line 235495
    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 235496
    if-eqz v2, :cond_1

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 235497
    :cond_0
    :goto_0
    return v0

    .line 235498
    :cond_1
    iget-object v2, p2, LX/5oc;->mError:Lcom/facebook/proxygen/HTTPRequestError;

    move-object v2, v2

    .line 235499
    if-eqz v2, :cond_0

    sget-object v3, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->StreamUnacknowledged:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    .line 235500
    iget-object p0, v2, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v2, p0

    .line 235501
    if-ne v3, v2, :cond_0

    if-gt p1, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)[Lcom/facebook/proxygen/RewriteRule;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;)[",
            "Lcom/facebook/proxygen/RewriteRule;"
        }
    .end annotation

    .prologue
    .line 235502
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/proxygen/RewriteRule;

    .line 235503
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 235504
    new-instance v3, Lcom/facebook/proxygen/RewriteRule;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/onion/OnionRewriteRule;

    iget-object v4, v0, Lcom/facebook/http/onion/OnionRewriteRule;->matcher:Ljava/lang/String;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/onion/OnionRewriteRule;

    iget-object v0, v0, Lcom/facebook/http/onion/OnionRewriteRule;->format:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/facebook/proxygen/RewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v1

    .line 235505
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 235506
    :cond_0
    return-object v2
.end method

.method private static b(LX/0QB;)LX/1MP;
    .locals 34

    .prologue
    .line 235507
    new-instance v2, LX/1MP;

    const/16 v3, 0x15f5

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/1MR;->a(LX/0QB;)LX/1MR;

    move-result-object v4

    check-cast v4, LX/1MR;

    invoke-static/range {p0 .. p0}, LX/1MU;->a(LX/0QB;)LX/1MU;

    move-result-object v5

    check-cast v5, LX/1MU;

    invoke-static/range {p0 .. p0}, LX/1MV;->a(LX/0QB;)LX/1MV;

    move-result-object v6

    check-cast v6, LX/1MW;

    invoke-static/range {p0 .. p0}, LX/1Mg;->a(LX/0QB;)LX/1Mg;

    move-result-object v7

    check-cast v7, LX/1Mg;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v9

    check-cast v9, LX/0pZ;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1Mj;->a(LX/0QB;)LX/1Mj;

    move-result-object v11

    check-cast v11, LX/1Mj;

    const-class v12, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v13

    check-cast v13, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1Mk;->a(LX/0QB;)LX/1Mk;

    move-result-object v14

    check-cast v14, LX/1Mk;

    invoke-static/range {p0 .. p0}, LX/0y2;->a(LX/0QB;)LX/0y2;

    move-result-object v15

    check-cast v15, LX/0y2;

    const/16 v16, 0x323

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v17

    check-cast v17, LX/0YR;

    invoke-static/range {p0 .. p0}, LX/18b;->a(LX/0QB;)LX/18b;

    move-result-object v18

    check-cast v18, LX/18b;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v19

    check-cast v19, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v20

    check-cast v20, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/1Mn;->a(LX/0QB;)LX/1gy;

    move-result-object v21

    check-cast v21, LX/1gy;

    invoke-static/range {p0 .. p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v22

    check-cast v22, LX/0Uo;

    invoke-static/range {p0 .. p0}, LX/1gz;->a(LX/0QB;)LX/1h1;

    move-result-object v23

    check-cast v23, LX/1h1;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v24

    check-cast v24, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0dt;->a(LX/0QB;)LX/0dx;

    move-result-object v25

    check-cast v25, LX/0dx;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v26

    check-cast v26, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v27

    check-cast v27, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v28

    check-cast v28, LX/1BA;

    invoke-static/range {p0 .. p0}, LX/1h3;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v29

    invoke-static/range {p0 .. p0}, LX/1h4;->a(LX/0QB;)LX/1h4;

    move-result-object v30

    check-cast v30, LX/1h4;

    const-class v31, LX/1h9;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/1h9;

    const/16 v32, 0x17d

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v32

    invoke-static/range {p0 .. p0}, LX/1hA;->a(LX/0QB;)LX/1hD;

    move-result-object v33

    check-cast v33, LX/1hD;

    invoke-direct/range {v2 .. v33}, LX/1MP;-><init>(LX/0Or;LX/1MR;LX/1MU;LX/1MW;LX/1Mg;LX/03V;LX/0pZ;LX/0ad;LX/1Mj;Landroid/content/Context;LX/0oz;LX/1Mk;LX/0y2;LX/0Or;LX/0YR;LX/18b;LX/0kb;LX/0Xl;LX/1gy;LX/0Uo;LX/1h1;LX/0Zb;LX/0dx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/1BA;Ljava/util/Set;LX/1h4;LX/1h9;LX/0Ot;LX/1hD;)V

    .line 235508
    return-object v2
.end method

.method private b(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 235533
    invoke-virtual {p0}, LX/1MP;->c()V

    .line 235534
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 235535
    const-string v0, "Host"

    invoke-interface {p1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_0

    .line 235536
    const-string v0, "Host"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 235537
    :cond_0
    const-string v0, "User-Agent"

    invoke-interface {p1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_1

    .line 235538
    const-string v2, "User-Agent"

    iget-object v0, p0, LX/1MP;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 235539
    :cond_1
    iget-object v0, p2, LX/15F;->d:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 235540
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235541
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "priority"

    invoke-virtual {v0}, Lcom/facebook/http/interfaces/RequestPriority;->getNumericValue()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 235542
    invoke-static {p1}, LX/1MP;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v9

    .line 235543
    invoke-static {p3}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v8

    .line 235544
    iget-object v0, v8, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 235545
    if-eqz v0, :cond_2

    .line 235546
    iget-object v2, v0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v0, v2

    .line 235547
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235548
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "fb_request_call_path"

    invoke-interface {v2, v3, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 235549
    :cond_2
    new-instance v5, LX/1iQ;

    iget v0, p0, LX/1MP;->A:I

    iget v2, p0, LX/1MP;->B:I

    iget-boolean v3, p0, LX/1MP;->C:Z

    iget-object v4, p0, LX/1MP;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0dU;->n:LX/0Tn;

    invoke-interface {v4, v6, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    invoke-direct {v5, v0, v2, v3, v4}, LX/1iQ;-><init>(IIZZ)V

    .line 235550
    iget-object v0, p0, LX/1MP;->u:LX/1MW;

    iget-object v2, p0, LX/1MP;->p:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v3

    iget-object v6, p0, LX/1MP;->w:LX/1hI;

    iget-object v7, p0, LX/1MP;->x:LX/0YR;

    move-object v2, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v7}, LX/1MW;->create(Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iW;LX/1iQ;LX/1hI;LX/0YR;)LX/6Xa;

    move-result-object v3

    .line 235551
    new-instance v2, Lcom/facebook/proxygen/NativeReadBuffer;

    invoke-direct {v2}, Lcom/facebook/proxygen/NativeReadBuffer;-><init>()V

    .line 235552
    invoke-virtual {v2}, Lcom/facebook/proxygen/NativeReadBuffer;->init()V

    .line 235553
    const/4 v0, 0x1

    new-array v0, v0, [LX/4ia;

    .line 235554
    new-instance v6, LX/4ib;

    invoke-direct {v6}, LX/4ib;-><init>()V

    .line 235555
    aput-object v6, v0, v10

    .line 235556
    new-instance v10, Lcom/facebook/proxygen/TraceEventContext;

    invoke-direct {v10, v0, v5}, Lcom/facebook/proxygen/TraceEventContext;-><init>([LX/4ia;LX/1iR;)V

    .line 235557
    new-instance v0, LX/75y;

    iget-object v4, p0, LX/1MP;->t:LX/03V;

    .line 235558
    iget v5, v10, Lcom/facebook/proxygen/TraceEventContext;->mParentID:I

    move v7, v5

    .line 235559
    iget-object v5, v8, LX/1iV;->a:Ljava/lang/String;

    move-object v8, v5

    .line 235560
    move-object v5, v6

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, LX/75y;-><init>(Ljava/lang/String;LX/161;LX/6Xa;LX/03V;LX/4ib;LX/1iW;ILjava/lang/String;)V

    .line 235561
    new-instance v3, LX/4iW;

    invoke-direct {v3}, LX/4iW;-><init>()V

    .line 235562
    const/4 v1, 0x0

    .line 235563
    iget-object v4, p4, LX/1iW;->k:LX/2BD;

    move-object v4, v4

    .line 235564
    if-eqz v4, :cond_3

    .line 235565
    new-instance v1, LX/6XX;

    invoke-direct {v1, v4}, LX/6XX;-><init>(LX/2BD;)V

    .line 235566
    :cond_3
    new-instance v4, Lcom/facebook/proxygen/JniHandler;

    invoke-direct {v4, v3, v0, v1}, Lcom/facebook/proxygen/JniHandler;-><init>(LX/4iW;Lcom/facebook/proxygen/HTTPResponseHandler;Lcom/facebook/proxygen/HTTPTransportCallback;)V

    .line 235567
    iget-object v1, p0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    invoke-virtual {v1, v4, v2, v10}, Lcom/facebook/proxygen/HTTPClient;->make(Lcom/facebook/proxygen/JniHandler;Lcom/facebook/proxygen/NativeReadBuffer;Lcom/facebook/proxygen/TraceEventContext;)V

    .line 235568
    invoke-virtual {v3, v9}, LX/4iW;->executeWithDefragmentation(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 235569
    new-instance v2, LX/6XW;

    invoke-direct {v2, v3}, LX/6XW;-><init>(LX/4iW;)V

    .line 235570
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15I;

    iput-object v1, p2, LX/15F;->c:LX/15I;

    .line 235571
    instance-of v1, p1, Lorg/apache/http/client/methods/AbortableHttpRequest;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 235572
    check-cast v1, Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-interface {v1, v2}, Lorg/apache/http/client/methods/AbortableHttpRequest;->setReleaseTrigger(Lorg/apache/http/conn/ConnectionReleaseTrigger;)V

    .line 235573
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 235574
    invoke-virtual {v3}, LX/4iW;->cancel()V

    .line 235575
    :cond_4
    invoke-static {v0}, LX/75y;->waitForHeaders(LX/75y;)V

    .line 235576
    iget-object v1, v0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235577
    iget-object v1, v0, LX/75y;->mResponse:Lorg/apache/http/HttpResponse;

    move-object v0, v1

    .line 235578
    return-object v0
.end method

.method private f()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 235509
    iget-object v0, p0, LX/1MP;->n:LX/0ad;

    sget-short v1, LX/0by;->I:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->J:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/facebook/proxygen/AdaptiveIntegerParameters;
    .locals 10

    .prologue
    const/16 v9, 0x1e

    .line 235510
    const/4 v0, 0x0

    .line 235511
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-char v2, LX/0by;->a:C

    const-string v3, "disabled"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235512
    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/0by;->b:I

    invoke-interface {v2, v3, v4, v5, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 235513
    const-string v2, "latency_based"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 235514
    new-instance v0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/0by;->c:I

    invoke-interface {v1, v2, v3, v4, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v6, LX/0by;->g:I

    invoke-interface {v2, v3, v4, v6, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    iget-object v3, p0, LX/1MP;->n:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/0by;->h:I

    invoke-interface {v3, v4, v6, v7, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    iget-object v4, p0, LX/1MP;->n:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget v8, LX/0by;->i:I

    invoke-interface {v4, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;-><init>(IIIII)V

    .line 235515
    new-instance v1, Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    invoke-direct {v1, v0}, Lcom/facebook/proxygen/AdaptiveIntegerParameters;-><init>(Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;)V

    move-object v0, v1

    .line 235516
    :cond_0
    :goto_0
    return-object v0

    .line 235517
    :cond_1
    const-string v2, "network_type_based"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235518
    new-instance v0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/0by;->j:I

    invoke-interface {v1, v2, v3, v4, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v6, LX/0by;->f:I

    invoke-interface {v2, v3, v4, v6, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    iget-object v3, p0, LX/1MP;->n:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/0by;->e:I

    invoke-interface {v3, v4, v6, v7, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    iget-object v4, p0, LX/1MP;->n:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget v8, LX/0by;->d:I

    invoke-interface {v4, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;-><init>(IIIII)V

    .line 235519
    new-instance v1, Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    invoke-direct {v1, v0}, Lcom/facebook/proxygen/AdaptiveIntegerParameters;-><init>(Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private h()Lcom/facebook/proxygen/AdaptiveIntegerParameters;
    .locals 10

    .prologue
    const/16 v9, 0x3c

    .line 235520
    const/4 v0, 0x0

    .line 235521
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-char v2, LX/0by;->k:C

    const-string v3, "disabled"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235522
    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/0by;->l:I

    invoke-interface {v2, v3, v4, v5, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 235523
    const-string v2, "latency_based"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 235524
    new-instance v0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/0by;->m:I

    invoke-interface {v1, v2, v3, v4, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v6, LX/0by;->q:I

    invoke-interface {v2, v3, v4, v6, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    iget-object v3, p0, LX/1MP;->n:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/0by;->r:I

    invoke-interface {v3, v4, v6, v7, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    iget-object v4, p0, LX/1MP;->n:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget v8, LX/0by;->s:I

    invoke-interface {v4, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;-><init>(IIIII)V

    .line 235525
    new-instance v1, Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    invoke-direct {v1, v0}, Lcom/facebook/proxygen/AdaptiveIntegerParameters;-><init>(Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;)V

    move-object v0, v1

    .line 235526
    :cond_0
    :goto_0
    return-object v0

    .line 235527
    :cond_1
    const-string v2, "network_type_based"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235528
    new-instance v0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/0by;->t:I

    invoke-interface {v1, v2, v3, v4, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v6, LX/0by;->p:I

    invoke-interface {v2, v3, v4, v6, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    iget-object v3, p0, LX/1MP;->n:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/0by;->o:I

    invoke-interface {v3, v4, v6, v7, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    iget-object v4, p0, LX/1MP;->n:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget v8, LX/0by;->n:I

    invoke-interface {v4, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;-><init>(IIIII)V

    .line 235529
    new-instance v1, Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    invoke-direct {v1, v0}, Lcom/facebook/proxygen/AdaptiveIntegerParameters;-><init>(Lcom/facebook/proxygen/AdaptiveIntegerParameters$NetworkTypeBasedParameter;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private j()I
    .locals 1

    .prologue
    .line 235530
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget v0, v0, LX/1hD;->e:I

    goto :goto_0
.end method

.method private k()I
    .locals 1

    .prologue
    .line 235531
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget v0, v0, LX/1hD;->f:I

    goto :goto_0
.end method

.method private l()I
    .locals 1

    .prologue
    .line 235399
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget v0, v0, LX/1hD;->h:I

    goto :goto_0
.end method

.method private m()I
    .locals 1

    .prologue
    .line 235532
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget v0, v0, LX/1hD;->i:I

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 235246
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget-boolean v0, v0, LX/1hD;->d:Z

    goto :goto_0
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 235250
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget-boolean v0, v0, LX/1hD;->g:Z

    goto :goto_0
.end method

.method private p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235251
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget-object v0, v0, LX/1hD;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1MP;->G:LX/1hD;

    iget-object v0, v0, LX/1hD;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method private q()Lcom/facebook/proxygen/SSLVerificationSettings;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 235252
    new-instance v0, LX/1hU;

    invoke-direct {v0}, LX/1hU;-><init>()V

    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->X:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 235253
    iput-boolean v1, v0, LX/1hU;->enableTimestampVerification:Z

    .line 235254
    move-object v0, v0

    .line 235255
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->Y:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 235256
    iput-boolean v1, v0, LX/1hU;->enforceCertKeyLengthVerification:Z

    .line 235257
    move-object v0, v0

    .line 235258
    iget-object v1, p0, LX/1MP;->k:LX/1gy;

    iget-wide v2, v1, LX/1gy;->c:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 235259
    iput-wide v2, v0, LX/1hU;->trustedReferenceTimestamp:J

    .line 235260
    move-object v0, v0

    .line 235261
    new-instance v7, Lcom/facebook/proxygen/SSLVerificationSettings;

    iget-boolean v8, v0, LX/1hU;->enableTimestampVerification:Z

    iget-boolean v9, v0, LX/1hU;->enforceCertKeyLengthVerification:Z

    iget-wide v10, v0, LX/1hU;->trustedReferenceTimestamp:J

    invoke-direct/range {v7 .. v11}, Lcom/facebook/proxygen/SSLVerificationSettings;-><init>(ZZJ)V

    move-object v0, v7

    .line 235262
    return-object v0
.end method

.method private r()Lcom/facebook/proxygen/ZeroProtocolSettings;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 235263
    iget-object v0, p0, LX/1MP;->n:LX/0ad;

    sget-char v1, LX/0by;->ac:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 235264
    new-instance v1, LX/1hV;

    invoke-direct {v1}, LX/1hV;-><init>()V

    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-short v3, LX/0by;->ad:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 235265
    iput-boolean v2, v1, LX/1hV;->enabled:Z

    .line 235266
    move-object v1, v1

    .line 235267
    iget-object v2, p0, LX/1MP;->n:LX/0ad;

    sget-short v3, LX/0by;->ae:S

    invoke-interface {v2, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 235268
    iput-boolean v2, v1, LX/1hV;->enforceExpiration:Z

    .line 235269
    move-object v1, v1

    .line 235270
    iput-object v0, v1, LX/1hV;->aeads:[Ljava/lang/String;

    .line 235271
    move-object v0, v1

    .line 235272
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-char v2, LX/0by;->af:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235273
    iput-object v1, v0, LX/1hV;->hostnamePolicy:Ljava/lang/String;

    .line 235274
    move-object v0, v0

    .line 235275
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->aj:S

    invoke-interface {v1, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 235276
    iput-boolean v1, v0, LX/1hV;->zeroRttEnabled:Z

    .line 235277
    move-object v0, v0

    .line 235278
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->ah:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 235279
    iput-boolean v1, v0, LX/1hV;->retryEnabled:Z

    .line 235280
    move-object v0, v0

    .line 235281
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget v2, LX/0by;->ai:I

    invoke-interface {v1, v2, v4}, LX/0ad;->a(II)I

    move-result v1

    .line 235282
    iput v1, v0, LX/1hV;->tlsFallback:I

    .line 235283
    move-object v0, v0

    .line 235284
    iget-object v1, p0, LX/1MP;->n:LX/0ad;

    sget-short v2, LX/0by;->ag:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235285
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/1MP;->o:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "fbzeroscfg.store"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235286
    iput-boolean v5, v0, LX/1hV;->persistentCacheEnabled:Z

    .line 235287
    move-object v2, v0

    .line 235288
    new-instance v3, LX/1hJ;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x1e

    .line 235289
    iput v1, v3, LX/1hJ;->cacheCapacity:I

    .line 235290
    move-object v1, v3

    .line 235291
    const/16 v3, 0x96

    .line 235292
    iput v3, v1, LX/1hJ;->syncInterval:I

    .line 235293
    move-object v1, v1

    .line 235294
    invoke-virtual {v1}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v1

    .line 235295
    iput-object v1, v2, LX/1hV;->cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 235296
    :cond_0
    invoke-virtual {v0}, LX/1hV;->build()Lcom/facebook/proxygen/ZeroProtocolSettings;

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/facebook/proxygen/PersistentSSLCacheSettings;
    .locals 3

    .prologue
    .line 235297
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1MP;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fbdns.store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235298
    new-instance v1, LX/1hJ;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v0, 0xc8

    .line 235299
    iput v0, v1, LX/1hJ;->cacheCapacity:I

    .line 235300
    move-object v0, v1

    .line 235301
    const/16 v1, 0x96

    .line 235302
    iput v1, v0, LX/1hJ;->syncInterval:I

    .line 235303
    move-object v0, v0

    .line 235304
    invoke-virtual {v0}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v0

    .line 235305
    return-object v0
.end method

.method private t()Lcom/facebook/proxygen/PersistentSSLCacheSettings;
    .locals 3

    .prologue
    .line 235306
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1MP;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fbtlsx.store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235307
    new-instance v1, LX/1hJ;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x32

    .line 235308
    iput v0, v1, LX/1hJ;->cacheCapacity:I

    .line 235309
    move-object v0, v1

    .line 235310
    const/16 v1, 0x96

    .line 235311
    iput v1, v0, LX/1hJ;->syncInterval:I

    .line 235312
    move-object v0, v0

    .line 235313
    const/4 v1, 0x1

    .line 235314
    iput-boolean v1, v0, LX/1hJ;->enableCrossDomainTickets:Z

    .line 235315
    move-object v0, v0

    .line 235316
    invoke-virtual {v0}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v0

    .line 235317
    return-object v0
.end method

.method private u()Lcom/facebook/proxygen/PersistentSSLCacheSettings;
    .locals 3

    .prologue
    .line 235318
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1MP;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fbzstd.store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235319
    new-instance v1, LX/1hJ;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x64

    .line 235320
    iput v0, v1, LX/1hJ;->cacheCapacity:I

    .line 235321
    move-object v0, v1

    .line 235322
    const/16 v1, 0x96

    .line 235323
    iput v1, v0, LX/1hJ;->syncInterval:I

    .line 235324
    move-object v0, v0

    .line 235325
    invoke-virtual {v0}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v0

    .line 235326
    return-object v0
.end method

.method private v()Lcom/facebook/proxygen/PersistentSSLCacheSettings;
    .locals 3

    .prologue
    .line 235327
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1MP;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fbtlscachedinfo.store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235328
    new-instance v1, LX/1hJ;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x28

    .line 235329
    iput v0, v1, LX/1hJ;->cacheCapacity:I

    .line 235330
    move-object v0, v1

    .line 235331
    const/16 v1, 0x96

    .line 235332
    iput v1, v0, LX/1hJ;->syncInterval:I

    .line 235333
    move-object v0, v0

    .line 235334
    invoke-virtual {v0}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v0

    return-object v0
.end method

.method private static w()[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 235335
    new-array v0, v1, [Ljava/lang/String;

    .line 235336
    const-string v2, "net.dns1"

    invoke-static {v2}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 235337
    const-string v3, "net.dns2"

    invoke-static {v3}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235338
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 235339
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    aput-object v2, v0, v1

    aput-object v3, v0, v5

    .line 235340
    :cond_0
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_3

    .line 235341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":53"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 235342
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235343
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 235344
    new-array v0, v5, [Ljava/lang/String;

    aput-object v2, v0, v1

    goto :goto_0

    .line 235345
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 235346
    new-array v0, v5, [Ljava/lang/String;

    aput-object v3, v0, v1

    goto :goto_0

    .line 235347
    :cond_3
    return-object v0
.end method

.method private x()V
    .locals 5

    .prologue
    const/high16 v4, 0x10000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235348
    iget-object v0, p0, LX/1MP;->v:LX/1Mg;

    .line 235349
    iget-object v3, v0, LX/1Mg;->j:Lorg/apache/http/HttpHost;

    move-object v0, v3

    .line 235350
    if-eqz v0, :cond_1

    .line 235351
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1MP;->e:Ljava/lang/String;

    .line 235352
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v1

    iput v1, p0, LX/1MP;->f:I

    .line 235353
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1MP;->g:Ljava/lang/String;

    .line 235354
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v0

    iput v0, p0, LX/1MP;->h:I

    .line 235355
    const-string v0, ""

    iput-object v0, p0, LX/1MP;->i:Ljava/lang/String;

    .line 235356
    :cond_0
    :goto_0
    return-void

    .line 235357
    :cond_1
    const-string v0, "http.proxyHost"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1MP;->e:Ljava/lang/String;

    .line 235358
    const-string v0, "https.proxyHost"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1MP;->g:Ljava/lang/String;

    .line 235359
    const-string v0, "http.nonProxyHosts"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1MP;->i:Ljava/lang/String;

    .line 235360
    const-string v0, "http.proxyPort"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235361
    const-string v3, "https.proxyPort"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235362
    if-eqz v0, :cond_4

    .line 235363
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1MP;->f:I

    .line 235364
    iget v0, p0, LX/1MP;->f:I

    if-ltz v0, :cond_3

    iget v0, p0, LX/1MP;->f:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-ge v0, v4, :cond_3

    move v0, v1

    .line 235365
    :goto_1
    if-nez v0, :cond_2

    .line 235366
    const-string v0, ""

    iput-object v0, p0, LX/1MP;->e:Ljava/lang/String;

    .line 235367
    iput v2, p0, LX/1MP;->f:I

    .line 235368
    :cond_2
    if-eqz v3, :cond_6

    .line 235369
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1MP;->h:I

    .line 235370
    iget v0, p0, LX/1MP;->h:I

    if-ltz v0, :cond_5

    iget v0, p0, LX/1MP;->h:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    if-ge v0, v4, :cond_5

    .line 235371
    :goto_2
    if-nez v1, :cond_0

    .line 235372
    const-string v0, ""

    iput-object v0, p0, LX/1MP;->g:Ljava/lang/String;

    .line 235373
    iput v2, p0, LX/1MP;->h:I

    goto :goto_0

    :cond_3
    move v0, v2

    .line 235374
    goto :goto_1

    :cond_4
    move v0, v2

    .line 235375
    goto :goto_1

    .line 235376
    :catch_0
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    .line 235377
    goto :goto_2

    :cond_6
    move v1, v2

    .line 235378
    goto :goto_2

    .line 235379
    :catch_1
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 235380
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 235381
    iget-object v2, p0, LX/1MP;->D:LX/1h4;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s://%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x1

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    .line 235382
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235383
    iget-object v3, v2, LX/1h4;->e:LX/0ad;

    sget-short v4, LX/0by;->V:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 235384
    :cond_0
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 235385
    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, LX/1MP;->b(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch LX/5oc; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 235386
    :catch_0
    move-exception v1

    .line 235387
    invoke-static {p1, v0, v1}, LX/1MP;->a(Lorg/apache/http/client/methods/HttpUriRequest;ILX/5oc;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 235388
    throw v1

    .line 235389
    :cond_1
    iget-object v3, v2, LX/1h4;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    invoke-virtual {v3, v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 235390
    iget-object v3, v2, LX/1h4;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/http/executors/liger/MostRecentHostsStorage$1;

    invoke-direct {v4, v2, v1}, Lcom/facebook/http/executors/liger/MostRecentHostsStorage$1;-><init>(LX/1h4;Ljava/lang/String;)V

    const v5, -0x2120d05c

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 235391
    iget-object v1, p0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    monitor-enter v1

    .line 235392
    :try_start_0
    invoke-direct {p0}, LX/1MP;->x()V

    .line 235393
    iget-object v0, p0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    iget-object v2, p0, LX/1MP;->e:Ljava/lang/String;

    iget v3, p0, LX/1MP;->f:I

    const-string v4, ""

    const-string v5, ""

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v0

    iget-object v2, p0, LX/1MP;->g:Ljava/lang/String;

    iget v3, p0, LX/1MP;->h:I

    const-string v4, ""

    const-string v5, ""

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/proxygen/HTTPClient;->setSecureProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v0

    iget-object v2, p0, LX/1MP;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/proxygen/HTTPClient;->setBypassProxyDomains(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v0

    iget-object v2, p0, LX/1MP;->v:LX/1Mg;

    .line 235394
    iget-boolean v3, v2, LX/1Mg;->h:Z

    move v2, v3

    .line 235395
    invoke-virtual {v0, v2}, Lcom/facebook/proxygen/HTTPClient;->setIsSandbox(Z)Lcom/facebook/proxygen/HTTPClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/proxygen/HTTPClient;->reInitializeIfNeeded()Z

    .line 235396
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 235397
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235398
    const-string v0, "Liger"

    return-object v0
.end method
