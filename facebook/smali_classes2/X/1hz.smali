.class public LX/1hz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "Deprecated"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/1hz;


# instance fields
.field private final b:I

.field private final c:Lorg/apache/http/params/HttpParams;

.field private final d:LX/1i4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297436
    const-class v0, LX/1hz;

    sput-object v0, LX/1hz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lorg/apache/http/params/HttpParams;LX/1i4;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/http/annotations/MaxRedirects;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297432
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1hz;->b:I

    .line 297433
    iput-object p2, p0, LX/1hz;->c:Lorg/apache/http/params/HttpParams;

    .line 297434
    iput-object p3, p0, LX/1hz;->d:LX/1i4;

    .line 297435
    return-void
.end method

.method public static a(LX/0QB;)LX/1hz;
    .locals 6

    .prologue
    .line 297416
    sget-object v0, LX/1hz;->e:LX/1hz;

    if-nez v0, :cond_1

    .line 297417
    const-class v1, LX/1hz;

    monitor-enter v1

    .line 297418
    :try_start_0
    sget-object v0, LX/1hz;->e:LX/1hz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297419
    if-eqz v2, :cond_0

    .line 297420
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297421
    new-instance p0, LX/1hz;

    .line 297422
    invoke-static {}, LX/14a;->b()Ljava/lang/Integer;

    move-result-object v3

    move-object v3, v3

    .line 297423
    check-cast v3, Ljava/lang/Integer;

    invoke-static {v0}, LX/1i0;->b(LX/0QB;)Lorg/apache/http/params/HttpParams;

    move-result-object v4

    check-cast v4, Lorg/apache/http/params/HttpParams;

    invoke-static {v0}, LX/1i4;->a(LX/0QB;)LX/1i4;

    move-result-object v5

    check-cast v5, LX/1i4;

    invoke-direct {p0, v3, v4, v5}, LX/1hz;-><init>(Ljava/lang/Integer;Lorg/apache/http/params/HttpParams;LX/1i4;)V

    .line 297424
    move-object v0, p0

    .line 297425
    sput-object v0, LX/1hz;->e:LX/1hz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297426
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297427
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297428
    :cond_1
    sget-object v0, LX/1hz;->e:LX/1hz;

    return-object v0

    .line 297429
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([Lorg/apache/http/Header;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/RedirectHandler;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    .prologue
    .line 297437
    :try_start_0
    invoke-interface {p3, p1, p2}, Lorg/apache/http/client/RedirectHandler;->getLocationURI(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Ljava/net/URI;

    move-result-object v0

    .line 297438
    invoke-virtual {v0}, Ljava/net/URI;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_0

    .line 297439
    new-instance v0, Lorg/apache/http/ProtocolException;

    const-string v1, "The specified URI must be absolute"

    invoke-direct {v0, v1}, Lorg/apache/http/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/apache/http/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297440
    :catch_0
    move-exception v0

    .line 297441
    new-instance v1, Lorg/apache/http/client/ClientProtocolException;

    invoke-direct {v1, v0}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 297442
    :cond_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 297443
    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 297444
    array-length v3, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object p1, p0, v1

    .line 297445
    sget-object p2, LX/1AJ;->a:Ljava/util/Set;

    invoke-interface {p1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 297446
    invoke-virtual {v2, p1}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Lorg/apache/http/Header;)V

    .line 297447
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297448
    :cond_2
    move-object v0, v2

    .line 297449
    return-object v0
.end method

.method private static a(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    .line 297413
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 297414
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 297415
    :cond_0
    return-void
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 0

    .prologue
    .line 297411
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    .line 297412
    return-void
.end method

.method private static a(LX/1hz;Lorg/apache/http/HttpRequest;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 297391
    new-instance v0, Lorg/apache/http/impl/client/ClientParamsStack;

    iget-object v1, p0, LX/1hz;->c:Lorg/apache/http/params/HttpParams;

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2, v3}, Lorg/apache/http/impl/client/ClientParamsStack;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/params/HttpParams;Lorg/apache/http/params/HttpParams;Lorg/apache/http/params/HttpParams;)V

    .line 297392
    invoke-static {v0}, Lorg/apache/http/client/params/HttpClientParams;->isRedirecting(Lorg/apache/http/params/HttpParams;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;LX/1MQ;Lorg/apache/http/client/RedirectHandler;Lorg/apache/http/protocol/HttpContext;LX/0am;LX/2BD;)Lorg/apache/http/HttpResponse;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/facebook/http/interfaces/HttpRequestState;",
            "LX/1MQ;",
            "Lorg/apache/http/client/RedirectHandler;",
            "Lorg/apache/http/protocol/HttpContext;",
            "LX/0am",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;>;",
            "LX/2BD;",
            ")",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    .prologue
    .line 297393
    const/4 v0, 0x0

    move v7, v0

    move-object v1, p1

    :goto_0
    iget v0, p0, LX/1hz;->b:I

    if-gt v7, v0, :cond_8

    .line 297394
    sget-object v8, LX/03R;->UNSET:LX/03R;

    .line 297395
    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v3, p5}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 297396
    iget-object v0, p0, LX/1hz;->d:LX/1i4;

    move-object v2, p2

    move-object v4, p3

    move-object v5, p6

    move-object/from16 v6, p7

    invoke-virtual/range {v0 .. v6}, LX/1i4;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1MQ;LX/0am;LX/2BD;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 297397
    :try_start_0
    invoke-static {p0, v1}, LX/1hz;->a(LX/1hz;Lorg/apache/http/HttpRequest;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p4, v4, v3}, Lorg/apache/http/client/RedirectHandler;->isRedirectRequested(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 297398
    :try_start_1
    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 297399
    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    invoke-static {v0, v4, v3, p4}, LX/1hz;->a([Lorg/apache/http/Header;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/RedirectHandler;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object p1

    .line 297400
    invoke-static {v1, p1}, LX/1hz;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/methods/HttpUriRequest;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 297401
    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 297402
    :cond_0
    invoke-static {v4}, LX/1hz;->a(Lorg/apache/http/HttpResponse;)V

    .line 297403
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move-object v1, p1

    goto :goto_0

    .line 297404
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 297405
    :cond_3
    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, LX/03R;->asBoolean()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 297406
    :cond_4
    invoke-static {v4}, LX/1hz;->a(Lorg/apache/http/HttpResponse;)V

    :cond_5
    return-object v4

    .line 297407
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 297408
    :cond_6
    invoke-static {v4}, LX/1hz;->a(Lorg/apache/http/HttpResponse;)V

    :cond_7
    throw v0

    .line 297409
    :cond_8
    new-instance v0, LX/4bZ;

    invoke-direct {v0}, LX/4bZ;-><init>()V

    throw v0

    .line 297410
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
