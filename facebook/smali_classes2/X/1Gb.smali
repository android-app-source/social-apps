.class public LX/1Gb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Gb;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Ga;",
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Ga;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225776
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Gb;->a:Ljava/util/Map;

    .line 225777
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Gb;->b:Ljava/util/Map;

    .line 225778
    iget-object v0, p0, LX/1Gb;->b:Ljava/util/Map;

    sget-object v1, LX/1Ga;->CACHE:LX/1Ga;

    const-string v2, "image"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225779
    iget-object v0, p0, LX/1Gb;->a:Ljava/util/Map;

    sget-object v1, LX/1Ga;->CACHE:LX/1Ga;

    new-instance v2, LX/1Gc;

    invoke-direct {v2, p0, p1}, LX/1Gc;-><init>(LX/1Gb;Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225780
    iget-object v0, p0, LX/1Gb;->b:Ljava/util/Map;

    sget-object v1, LX/1Ga;->FILES:LX/1Ga;

    const-string v2, "image_main"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225781
    iget-object v0, p0, LX/1Gb;->a:Ljava/util/Map;

    sget-object v1, LX/1Ga;->FILES:LX/1Ga;

    new-instance v2, LX/1Ge;

    invoke-direct {v2, p0, p1}, LX/1Ge;-><init>(LX/1Gb;Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225782
    return-void
.end method

.method public static a(LX/0QB;)LX/1Gb;
    .locals 4

    .prologue
    .line 225783
    sget-object v0, LX/1Gb;->c:LX/1Gb;

    if-nez v0, :cond_1

    .line 225784
    const-class v1, LX/1Gb;

    monitor-enter v1

    .line 225785
    :try_start_0
    sget-object v0, LX/1Gb;->c:LX/1Gb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225786
    if-eqz v2, :cond_0

    .line 225787
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225788
    new-instance p0, LX/1Gb;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/1Gb;-><init>(Landroid/content/Context;)V

    .line 225789
    move-object v0, p0

    .line 225790
    sput-object v0, LX/1Gb;->c:LX/1Gb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225791
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225792
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225793
    :cond_1
    sget-object v0, LX/1Gb;->c:LX/1Gb;

    return-object v0

    .line 225794
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225795
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Ga;)LX/1Gd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ga;",
            ")",
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225774
    iget-object v0, p0, LX/1Gb;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gd;

    return-object v0
.end method

.method public final b(LX/1Ga;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 225773
    iget-object v0, p0, LX/1Gb;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
