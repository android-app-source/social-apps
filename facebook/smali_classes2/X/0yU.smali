.class public LX/0yU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0yN;

.field private c:LX/0yV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164803
    const-class v0, LX/0yU;

    sput-object v0, LX/0yU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0yN;LX/0yV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164791
    iput-object p1, p0, LX/0yU;->b:LX/0yN;

    .line 164792
    iput-object p2, p0, LX/0yU;->c:LX/0yV;

    .line 164793
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;)LX/0Rf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164794
    monitor-enter p0

    .line 164795
    :try_start_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164796
    :try_start_1
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164797
    iget-object v1, p0, LX/0yU;->c:LX/0yV;

    invoke-virtual {v1, p1}, LX/0yV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 164798
    invoke-static {v1}, LX/0yY;->fromStrings(Ljava/lang/Iterable;)LX/0Rf;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 164799
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 164800
    :catch_0
    move-exception v1

    .line 164801
    :try_start_2
    sget-object v2, LX/0yU;->a:Ljava/lang/Class;

    const-string v3, "Error de-serializing enabled UI features - %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 164802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/0QB;)LX/0yU;
    .locals 3

    .prologue
    .line 164788
    new-instance v2, LX/0yU;

    invoke-static {p0}, LX/0yM;->a(LX/0QB;)LX/0yM;

    move-result-object v0

    check-cast v0, LX/0yN;

    invoke-static {p0}, LX/0yV;->b(LX/0QB;)LX/0yV;

    move-result-object v1

    check-cast v1, LX/0yV;

    invoke-direct {v2, v0, v1}, LX/0yU;-><init>(LX/0yN;LX/0yV;)V

    .line 164789
    return-object v2
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164784
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yU;->b:LX/0yN;

    const-string v1, ""

    invoke-interface {v0, p1, v1}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164785
    invoke-direct {p0, v0}, LX/0yU;->b(Ljava/lang/String;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 164786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/0yY;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 164787
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yU;->c:LX/0yV;

    invoke-static {p1}, LX/0yY;->toStrings(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yV;->a(LX/0Px;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
