.class public LX/0tJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0tJ;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154295
    iput-object p1, p0, LX/0tJ;->a:LX/0ad;

    .line 154296
    return-void
.end method

.method public static a(LX/0QB;)LX/0tJ;
    .locals 4

    .prologue
    .line 154297
    sget-object v0, LX/0tJ;->b:LX/0tJ;

    if-nez v0, :cond_1

    .line 154298
    const-class v1, LX/0tJ;

    monitor-enter v1

    .line 154299
    :try_start_0
    sget-object v0, LX/0tJ;->b:LX/0tJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154300
    if-eqz v2, :cond_0

    .line 154301
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154302
    new-instance p0, LX/0tJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/0tJ;-><init>(LX/0ad;)V

    .line 154303
    move-object v0, p0

    .line 154304
    sput-object v0, LX/0tJ;->b:LX/0tJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154305
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154306
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154307
    :cond_1
    sget-object v0, LX/0tJ;->b:LX/0tJ;

    return-object v0

    .line 154308
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154309
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 154310
    iget-object v0, p0, LX/0tJ;->a:LX/0ad;

    sget-short v1, LX/0wn;->O:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 154311
    iget-object v0, p0, LX/0tJ;->a:LX/0ad;

    sget v1, LX/0wn;->P:I

    const/16 v2, 0xfa0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 154312
    iget-object v0, p0, LX/0tJ;->a:LX/0ad;

    sget v1, LX/0wn;->R:I

    const/16 v2, 0x2ee

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method
