.class public final enum LX/1rF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1rF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1rF;

.field public static final enum BACKGROUNDED:LX/1rF;

.field public static final enum CLOCK_CHANGE:LX/1rF;

.field public static final enum DATASAVINGS:LX/1rF;

.field public static final enum DIALTONE:LX/1rF;

.field public static final enum FOREGROUNDED:LX/1rF;

.field public static final enum IGNORE:LX/1rF;

.field public static final enum LOGIN:LX/1rF;

.field public static final enum USER_ACTION:LX/1rF;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 331914
    new-instance v0, LX/1rF;

    const-string v1, "FOREGROUNDED"

    invoke-direct {v0, v1, v3}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->FOREGROUNDED:LX/1rF;

    .line 331915
    new-instance v0, LX/1rF;

    const-string v1, "USER_ACTION"

    invoke-direct {v0, v1, v4}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->USER_ACTION:LX/1rF;

    .line 331916
    new-instance v0, LX/1rF;

    const-string v1, "BACKGROUNDED"

    invoke-direct {v0, v1, v5}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->BACKGROUNDED:LX/1rF;

    .line 331917
    new-instance v0, LX/1rF;

    const-string v1, "CLOCK_CHANGE"

    invoke-direct {v0, v1, v6}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->CLOCK_CHANGE:LX/1rF;

    .line 331918
    new-instance v0, LX/1rF;

    const-string v1, "IGNORE"

    invoke-direct {v0, v1, v7}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->IGNORE:LX/1rF;

    .line 331919
    new-instance v0, LX/1rF;

    const-string v1, "LOGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->LOGIN:LX/1rF;

    .line 331920
    new-instance v0, LX/1rF;

    const-string v1, "DIALTONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->DIALTONE:LX/1rF;

    .line 331921
    new-instance v0, LX/1rF;

    const-string v1, "DATASAVINGS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1rF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rF;->DATASAVINGS:LX/1rF;

    .line 331922
    const/16 v0, 0x8

    new-array v0, v0, [LX/1rF;

    sget-object v1, LX/1rF;->FOREGROUNDED:LX/1rF;

    aput-object v1, v0, v3

    sget-object v1, LX/1rF;->USER_ACTION:LX/1rF;

    aput-object v1, v0, v4

    sget-object v1, LX/1rF;->BACKGROUNDED:LX/1rF;

    aput-object v1, v0, v5

    sget-object v1, LX/1rF;->CLOCK_CHANGE:LX/1rF;

    aput-object v1, v0, v6

    sget-object v1, LX/1rF;->IGNORE:LX/1rF;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1rF;->LOGIN:LX/1rF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1rF;->DIALTONE:LX/1rF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1rF;->DATASAVINGS:LX/1rF;

    aput-object v2, v0, v1

    sput-object v0, LX/1rF;->$VALUES:[LX/1rF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 331923
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1rF;
    .locals 1

    .prologue
    .line 331924
    const-class v0, LX/1rF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1rF;

    return-object v0
.end method

.method public static values()[LX/1rF;
    .locals 1

    .prologue
    .line 331925
    sget-object v0, LX/1rF;->$VALUES:[LX/1rF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1rF;

    return-object v0
.end method
