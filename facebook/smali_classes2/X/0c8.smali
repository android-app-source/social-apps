.class public LX/0c8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0c8;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0VT;

.field private final c:LX/03V;

.field private final d:Landroid/os/HandlerThread;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0bD;


# direct methods
.method public constructor <init>(LX/0Or;LX/0VT;LX/03V;Landroid/os/HandlerThread;LX/0Or;LX/0bD;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/process/MyProcessId;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/os/HandlerThread;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0bD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87819
    iput-object p1, p0, LX/0c8;->a:LX/0Or;

    .line 87820
    iput-object p2, p0, LX/0c8;->b:LX/0VT;

    .line 87821
    iput-object p3, p0, LX/0c8;->c:LX/03V;

    .line 87822
    iput-object p4, p0, LX/0c8;->d:Landroid/os/HandlerThread;

    .line 87823
    iput-object p5, p0, LX/0c8;->e:LX/0Or;

    .line 87824
    iput-object p6, p0, LX/0c8;->f:LX/0bD;

    .line 87825
    return-void
.end method

.method public static a(LX/0QB;)LX/0c8;
    .locals 10

    .prologue
    .line 87826
    sget-object v0, LX/0c8;->g:LX/0c8;

    if-nez v0, :cond_1

    .line 87827
    const-class v1, LX/0c8;

    monitor-enter v1

    .line 87828
    :try_start_0
    sget-object v0, LX/0c8;->g:LX/0c8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87829
    if-eqz v2, :cond_0

    .line 87830
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87831
    new-instance v3, LX/0c8;

    const/16 v4, 0x15ce

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v5

    check-cast v5, LX/0VT;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0Zq;->a(LX/0QB;)Landroid/os/HandlerThread;

    move-result-object v7

    check-cast v7, Landroid/os/HandlerThread;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0bD;->a(LX/0QB;)LX/0bD;

    move-result-object v9

    check-cast v9, LX/0bD;

    invoke-direct/range {v3 .. v9}, LX/0c8;-><init>(LX/0Or;LX/0VT;LX/03V;Landroid/os/HandlerThread;LX/0Or;LX/0bD;)V

    .line 87832
    move-object v0, v3

    .line 87833
    sput-object v0, LX/0c8;->g:LX/0c8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87834
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87835
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87836
    :cond_1
    sget-object v0, LX/0c8;->g:LX/0c8;

    return-object v0

    .line 87837
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Xl;Z)LX/0cJ;
    .locals 10

    .prologue
    .line 87839
    new-instance v0, LX/0cI;

    iget-object v3, p0, LX/0c8;->a:LX/0Or;

    iget-object v4, p0, LX/0c8;->b:LX/0VT;

    iget-object v5, p0, LX/0c8;->c:LX/03V;

    iget-object v6, p0, LX/0c8;->f:LX/0bD;

    iget-object v7, p0, LX/0c8;->d:Landroid/os/HandlerThread;

    iget-object v8, p0, LX/0c8;->e:LX/0Or;

    move-object v1, p1

    move-object v2, p2

    move v9, p3

    invoke-direct/range {v0 .. v9}, LX/0cI;-><init>(Ljava/lang/String;LX/0Xl;LX/0Or;LX/0VT;LX/03V;LX/0bD;Landroid/os/HandlerThread;LX/0Or;Z)V

    return-object v0
.end method
