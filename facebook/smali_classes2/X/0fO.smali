.class public LX/0fO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oj;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0om;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 107892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107893
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 107894
    iput-object v0, p0, LX/0fO;->a:LX/0Ot;

    .line 107895
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 107896
    iput-object v0, p0, LX/0fO;->b:LX/0Ot;

    .line 107897
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 107898
    iput-object v0, p0, LX/0fO;->c:LX/0Ot;

    .line 107899
    return-void
.end method

.method public static a(LX/0QB;)LX/0fO;
    .locals 1

    .prologue
    .line 107891
    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0fO;
    .locals 4

    .prologue
    .line 107868
    new-instance v0, LX/0fO;

    invoke-direct {v0}, LX/0fO;-><init>()V

    .line 107869
    const/16 v1, 0xa89

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xa87

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xa88

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 107870
    iput-object v1, v0, LX/0fO;->a:LX/0Ot;

    iput-object v2, v0, LX/0fO;->b:LX/0Ot;

    iput-object v3, v0, LX/0fO;->c:LX/0Ot;

    .line 107871
    return-object v0
.end method

.method public static y(LX/0fO;)LX/0i8;
    .locals 1

    .prologue
    .line 107882
    iget-object v0, p0, LX/0fO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oj;

    invoke-virtual {v0}, LX/0oj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107883
    iget-object v0, p0, LX/0fO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i8;

    .line 107884
    :goto_0
    return-object v0

    .line 107885
    :cond_0
    iget-object v0, p0, LX/0fO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0om;

    invoke-virtual {v0}, LX/0om;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107886
    iget-object v0, p0, LX/0fO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i8;

    goto :goto_0

    .line 107887
    :cond_1
    iget-object v0, p0, LX/0fO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oo;

    invoke-virtual {v0}, LX/0oo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107888
    iget-object v0, p0, LX/0fO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i8;

    goto :goto_0

    .line 107889
    :cond_2
    iget-object v0, p0, LX/0fO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i8;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 107881
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->b()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 107880
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->c()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 107879
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->g()Z

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107890
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 107878
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->l()Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 107877
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->m()Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 107876
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->n()Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 107875
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->o()Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 107874
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->p()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 107873
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->q()Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 107872
    invoke-static {p0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->s()Z

    move-result v0

    return v0
.end method
