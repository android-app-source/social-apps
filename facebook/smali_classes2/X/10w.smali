.class public final LX/10w;
.super LX/0rP;
.source ""


# instance fields
.field public final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 169537
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput-wide p1, p0, LX/10w;->a:J

    return-void
.end method

.method public static b(J)LX/10w;
    .locals 2

    .prologue
    .line 169536
    new-instance v0, LX/10w;

    invoke-direct {v0, p0, p1}, LX/10w;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 169535
    iget-wide v0, p0, LX/10w;->a:J

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169534
    iget-wide v0, p0, LX/10w;->a:J

    invoke-static {v0, v1}, LX/13I;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 169533
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 4

    .prologue
    .line 169532
    iget-wide v0, p0, LX/10w;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 169531
    sget-object v0, LX/16L;->LONG:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 169517
    if-ne p1, p0, :cond_1

    .line 169518
    :cond_0
    :goto_0
    return v0

    .line 169519
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 169520
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 169521
    goto :goto_0

    .line 169522
    :cond_3
    check-cast p1, LX/10w;

    iget-wide v2, p1, LX/10w;->a:J

    iget-wide v4, p0, LX/10w;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 169530
    iget-wide v0, p0, LX/10w;->a:J

    long-to-int v0, v0

    iget-wide v2, p0, LX/10w;->a:J

    const/16 v1, 0x20

    shr-long/2addr v2, v1

    long-to-int v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 169528
    iget-wide v0, p0, LX/10w;->a:J

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(J)V

    .line 169529
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 169527
    iget-wide v0, p0, LX/10w;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 2

    .prologue
    .line 169526
    iget-wide v0, p0, LX/10w;->a:J

    long-to-int v0, v0

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 169525
    iget-wide v0, p0, LX/10w;->a:J

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 169524
    iget-wide v0, p0, LX/10w;->a:J

    long-to-double v0, v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 169523
    iget-wide v0, p0, LX/10w;->a:J

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method
