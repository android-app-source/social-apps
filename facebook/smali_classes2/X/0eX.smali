.class public LX/0eX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final c:Ljava/nio/charset/Charset;

.field public static final synthetic m:Z


# instance fields
.field public a:Ljava/nio/ByteBuffer;

.field public b:I

.field public d:I

.field public e:[I

.field public f:I

.field public g:Z

.field public h:I

.field public i:[I

.field public j:I

.field public k:I

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92108
    const-class v0, LX/0eX;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/0eX;->m:Z

    .line 92109
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/0eX;->c:Ljava/nio/charset/Charset;

    return-void

    .line 92110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92111
    const/16 v0, 0x400

    invoke-direct {p0, v0}, LX/0eX;-><init>(I)V

    .line 92112
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 92113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92114
    iput v0, p0, LX/0eX;->d:I

    .line 92115
    const/4 v1, 0x0

    iput-object v1, p0, LX/0eX;->e:[I

    .line 92116
    iput v2, p0, LX/0eX;->f:I

    .line 92117
    iput-boolean v2, p0, LX/0eX;->g:Z

    .line 92118
    const/16 v1, 0x10

    new-array v1, v1, [I

    iput-object v1, p0, LX/0eX;->i:[I

    .line 92119
    iput v2, p0, LX/0eX;->j:I

    .line 92120
    iput v2, p0, LX/0eX;->k:I

    .line 92121
    iput-boolean v2, p0, LX/0eX;->l:Z

    .line 92122
    if-gtz p1, :cond_0

    move p1, v0

    .line 92123
    :cond_0
    iput p1, p0, LX/0eX;->b:I

    .line 92124
    invoke-static {p1}, LX/0eX;->d(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    .line 92125
    return-void
.end method

.method private static b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 92126
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    .line 92127
    const/high16 v1, -0x40000000    # -2.0f

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    .line 92128
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: cannot grow buffer beyond 2 gigabytes."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 92129
    :cond_0
    shl-int/lit8 v1, v0, 0x1

    .line 92130
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 92131
    invoke-static {v1}, LX/0eX;->d(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 92132
    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 92133
    invoke-virtual {v2, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 92134
    return-object v2
.end method

.method private b(S)V
    .locals 2

    .prologue
    .line 92135
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92136
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 92137
    return-void
.end method

.method private static d(I)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 92138
    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 92139
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 92140
    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 92141
    iget-boolean v0, p0, LX/0eX;->g:Z

    if-eqz v0, :cond_0

    .line 92142
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: object serialization must not be nested."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 92143
    :cond_0
    return-void
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 92171
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 92144
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    invoke-direct {p0, p1}, LX/0eX;->f(I)V

    return-void
.end method

.method private h(I)V
    .locals 2

    .prologue
    .line 92145
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 92146
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: struct must be serialized inline."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 92147
    :cond_0
    return-void
.end method

.method public static i(LX/0eX;I)V
    .locals 2

    .prologue
    .line 92148
    iget-object v0, p0, LX/0eX;->e:[I

    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v1

    aput v1, v0, p1

    .line 92149
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 92150
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LX/0eX;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 92151
    sget-object v0, LX/0eX;->c:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 92152
    invoke-virtual {p0, v4}, LX/0eX;->a(B)V

    .line 92153
    array-length v1, v0

    invoke-virtual {p0, v2, v1, v2}, LX/0eX;->a(III)V

    .line 92154
    iget-object v1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eX;->b:I

    array-length v3, v0

    sub-int/2addr v2, v3

    iput v2, p0, LX/0eX;->b:I

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 92155
    iget-object v1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    array-length v2, v0

    invoke-virtual {v1, v0, v4, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 92156
    invoke-virtual {p0}, LX/0eX;->b()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/nio/ByteBuffer;)LX/0eX;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92157
    iput-object p1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    .line 92158
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 92159
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 92160
    const/4 v0, 0x1

    iput v0, p0, LX/0eX;->d:I

    .line 92161
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iput v0, p0, LX/0eX;->b:I

    .line 92162
    iput v2, p0, LX/0eX;->f:I

    .line 92163
    iput-boolean v2, p0, LX/0eX;->g:Z

    .line 92164
    iput v2, p0, LX/0eX;->h:I

    .line 92165
    iput v2, p0, LX/0eX;->j:I

    .line 92166
    iput v2, p0, LX/0eX;->k:I

    .line 92167
    return-object p0
.end method

.method public final a(B)V
    .locals 2

    .prologue
    .line 92168
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92169
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 92170
    return-void
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 92106
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p1, p2}, Ljava/nio/ByteBuffer;->putDouble(ID)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 92107
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putFloat(IF)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 92019
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92020
    sget-boolean v0, LX/0eX;->m:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v0

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 92021
    :cond_0
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x4

    .line 92022
    invoke-direct {p0, v0}, LX/0eX;->f(I)V

    .line 92023
    return-void
.end method

.method public final a(IBI)V
    .locals 1

    .prologue
    .line 92024
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    :cond_0
    invoke-virtual {p0, p2}, LX/0eX;->a(B)V

    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final a(IFD)V
    .locals 3

    .prologue
    .line 92025
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    float-to-double v0, p2

    cmpl-double v0, v0, p3

    if-eqz v0, :cond_1

    .line 92026
    :cond_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    invoke-virtual {p0, p2}, LX/0eX;->a(F)V

    .line 92027
    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 92028
    iget v0, p0, LX/0eX;->d:I

    if-le p1, v0, :cond_0

    iput p1, p0, LX/0eX;->d:I

    .line 92029
    :cond_0
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LX/0eX;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    xor-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    .line 92030
    :goto_0
    iget v1, p0, LX/0eX;->b:I

    add-int v2, v0, p1

    add-int/2addr v2, p2

    if-ge v1, v2, :cond_1

    .line 92031
    iget-object v1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    .line 92032
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-static {v2}, LX/0eX;->b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    .line 92033
    iget v2, p0, LX/0eX;->b:I

    iget-object v3, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    sub-int v1, v3, v1

    add-int/2addr v1, v2

    iput v1, p0, LX/0eX;->b:I

    goto :goto_0

    .line 92034
    :cond_1
    const/4 v2, 0x0

    .line 92035
    move v1, v2

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v3, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget p1, p0, LX/0eX;->b:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, LX/0eX;->b:I

    invoke-virtual {v3, p1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 92036
    :cond_2
    return-void
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 92037
    invoke-direct {p0}, LX/0eX;->f()V

    .line 92038
    iput p2, p0, LX/0eX;->k:I

    .line 92039
    const/4 v0, 0x4

    mul-int v1, p1, p2

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92040
    mul-int v0, p1, p2

    invoke-virtual {p0, p3, v0}, LX/0eX;->a(II)V

    .line 92041
    return-void
.end method

.method public final a(IJJ)V
    .locals 2

    .prologue
    .line 92042
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    cmp-long v0, p2, p4

    if-eqz v0, :cond_1

    .line 92043
    :cond_0
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92044
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1, p2, p3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 92045
    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final a(ISI)V
    .locals 1

    .prologue
    .line 92046
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    :cond_0
    invoke-direct {p0, p2}, LX/0eX;->b(S)V

    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final a(IZZ)V
    .locals 2

    .prologue
    .line 92047
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    .line 92048
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92049
    iget-object v1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v0, p0, LX/0eX;->b:I

    add-int/lit8 p3, v0, -0x1

    iput p3, p0, LX/0eX;->b:I

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {v1, p3, v0}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 92050
    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 92051
    iget v0, p0, LX/0eX;->k:I

    invoke-direct {p0, v0}, LX/0eX;->f(I)V

    .line 92052
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92099
    invoke-direct {p0}, LX/0eX;->f()V

    .line 92100
    iget-object v0, p0, LX/0eX;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0eX;->e:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    :cond_0
    new-array v0, p1, [I

    iput-object v0, p0, LX/0eX;->e:[I

    .line 92101
    :cond_1
    iput p1, p0, LX/0eX;->f:I

    .line 92102
    iget-object v0, p0, LX/0eX;->e:[I

    iget v1, p0, LX/0eX;->f:I

    invoke-static {v0, v2, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 92103
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0eX;->g:Z

    .line 92104
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v0

    iput v0, p0, LX/0eX;->h:I

    .line 92105
    return-void
.end method

.method public final b(III)V
    .locals 1

    .prologue
    .line 92053
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    :cond_0
    invoke-direct {p0, p2}, LX/0eX;->g(I)V

    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 92054
    iget-object v0, p0, LX/0eX;->e:[I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0eX;->g:Z

    if-nez v0, :cond_1

    .line 92055
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: endObject called without startObject"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 92056
    :cond_1
    invoke-direct {p0, v1}, LX/0eX;->g(I)V

    .line 92057
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v3

    .line 92058
    iget v0, p0, LX/0eX;->f:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 92059
    iget-object v0, p0, LX/0eX;->e:[I

    aget v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0eX;->e:[I

    aget v0, v0, v2

    sub-int v0, v3, v0

    :goto_1
    int-to-short v0, v0

    .line 92060
    invoke-direct {p0, v0}, LX/0eX;->b(S)V

    .line 92061
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 92062
    goto :goto_1

    .line 92063
    :cond_3
    iget v0, p0, LX/0eX;->h:I

    sub-int v0, v3, v0

    int-to-short v0, v0

    invoke-direct {p0, v0}, LX/0eX;->b(S)V

    .line 92064
    iget v0, p0, LX/0eX;->f:I

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x2

    int-to-short v0, v0

    invoke-direct {p0, v0}, LX/0eX;->b(S)V

    move v0, v1

    .line 92065
    :goto_2
    iget v2, p0, LX/0eX;->j:I

    if-ge v0, v2, :cond_8

    .line 92066
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    iget-object v4, p0, LX/0eX;->i:[I

    aget v4, v4, v0

    sub-int v4, v2, v4

    .line 92067
    iget v5, p0, LX/0eX;->b:I

    .line 92068
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v6

    .line 92069
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v2

    if-ne v6, v2, :cond_5

    .line 92070
    const/4 v2, 0x2

    :goto_3
    if-ge v2, v6, :cond_4

    .line 92071
    iget-object v7, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    add-int v8, v4, v2

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v7

    iget-object v8, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    add-int v9, v5, v2

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v8

    if-ne v7, v8, :cond_5

    .line 92072
    add-int/lit8 v2, v2, 0x2

    goto :goto_3

    .line 92073
    :cond_4
    iget-object v2, p0, LX/0eX;->i:[I

    aget v0, v2, v0

    .line 92074
    :goto_4
    if-eqz v0, :cond_6

    .line 92075
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    sub-int/2addr v2, v3

    iput v2, p0, LX/0eX;->b:I

    .line 92076
    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, LX/0eX;->b:I

    sub-int/2addr v0, v3

    invoke-virtual {v2, v4, v0}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 92077
    :goto_5
    iput-boolean v1, p0, LX/0eX;->g:Z

    .line 92078
    return v3

    .line 92079
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 92080
    :cond_6
    iget v0, p0, LX/0eX;->j:I

    iget-object v2, p0, LX/0eX;->i:[I

    array-length v2, v2

    if-ne v0, v2, :cond_7

    iget-object v0, p0, LX/0eX;->i:[I

    iget v2, p0, LX/0eX;->j:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, LX/0eX;->i:[I

    .line 92081
    :cond_7
    iget-object v0, p0, LX/0eX;->i:[I

    iget v2, p0, LX/0eX;->j:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/0eX;->j:I

    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v4

    aput v4, v0, v2

    .line 92082
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget-object v2, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v0, v2, v4}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 92083
    iget v0, p0, LX/0eX;->d:I

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/0eX;->a(II)V

    .line 92084
    invoke-virtual {p0, p1}, LX/0eX;->a(I)V

    .line 92085
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0eX;->b:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 92086
    return-void
.end method

.method public final c(III)V
    .locals 1

    .prologue
    .line 92087
    iget-boolean v0, p0, LX/0eX;->l:Z

    if-nez v0, :cond_0

    if-eq p2, p3, :cond_1

    :cond_0
    invoke-virtual {p0, p2}, LX/0eX;->a(I)V

    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    :cond_1
    return-void
.end method

.method public final d()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 92088
    iget-object v0, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final d(III)V
    .locals 0

    .prologue
    .line 92089
    if-eq p2, p3, :cond_0

    .line 92090
    invoke-direct {p0, p2}, LX/0eX;->h(I)V

    .line 92091
    invoke-static {p0, p1}, LX/0eX;->i(LX/0eX;I)V

    .line 92092
    :cond_0
    return-void
.end method

.method public final e()[B
    .locals 4

    .prologue
    .line 92093
    iget v0, p0, LX/0eX;->b:I

    iget-object v1, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget v2, p0, LX/0eX;->b:I

    sub-int/2addr v1, v2

    .line 92094
    new-array v2, v1, [B

    .line 92095
    iget-object v3, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 92096
    iget-object v3, p0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 92097
    move-object v0, v2

    .line 92098
    return-object v0
.end method
