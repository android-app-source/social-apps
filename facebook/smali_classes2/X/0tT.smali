.class public LX/0tT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154656
    iput-object p1, p0, LX/0tT;->a:LX/0W3;

    .line 154657
    return-void
.end method

.method public static a(LX/0QB;)LX/0tT;
    .locals 1

    .prologue
    .line 154658
    invoke-static {p0}, LX/0tT;->b(LX/0QB;)LX/0tT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0tT;
    .locals 2

    .prologue
    .line 154659
    new-instance v1, LX/0tT;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-direct {v1, v0}, LX/0tT;-><init>(LX/0W3;)V

    .line 154660
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 154661
    iget-object v0, p0, LX/0tT;->a:LX/0W3;

    sget-wide v2, LX/0X5;->hw:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 154662
    invoke-virtual {p0}, LX/0tT;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tT;->a:LX/0W3;

    sget-wide v2, LX/0X5;->hz:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
