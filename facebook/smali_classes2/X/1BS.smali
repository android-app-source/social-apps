.class public LX/1BS;
.super LX/1BT;
.source ""


# instance fields
.field public final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "LX/2xn;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/2xn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x19

    .line 213166
    invoke-direct {p0}, LX/1BT;-><init>()V

    .line 213167
    new-instance v0, LX/0aq;

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1BS;->a:LX/0aq;

    .line 213168
    new-instance v0, LX/0aq;

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1BS;->b:LX/0aq;

    return-void
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 213169
    iget-object v0, p0, LX/1BS;->a:LX/0aq;

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xn;

    .line 213170
    if-nez v0, :cond_0

    .line 213171
    :goto_0
    return-void

    .line 213172
    :cond_0
    iget-object v1, p0, LX/1BS;->b:LX/0aq;

    invoke-virtual {v1, p3, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213173
    invoke-static {v0, p1}, LX/2xp;->a(LX/2xn;LX/1bf;)V

    goto :goto_0
.end method

.method public final a(LX/1bf;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 1

    .prologue
    .line 213174
    iget-object v0, p0, LX/1BS;->b:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213175
    return-void
.end method

.method public final a(LX/1bf;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 213176
    iget-object v0, p0, LX/1BS;->b:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213177
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213178
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213179
    :cond_0
    :goto_0
    return-void

    .line 213180
    :cond_1
    iget-object v0, p0, LX/1BS;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xn;

    .line 213181
    if-eqz v0, :cond_0

    .line 213182
    invoke-static {v0, p2, p3}, LX/2xp;->a(LX/2xn;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 213183
    iget-object v0, p0, LX/1BS;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
