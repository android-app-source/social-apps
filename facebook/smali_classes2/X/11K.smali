.class public LX/11K;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 171948
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 171949
    return-void
.end method

.method public static a(LX/0s1;)LX/0s9;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171947
    invoke-interface {p0}, LX/0s1;->a()LX/0s9;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/config/server/IsInternalPrefsEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171946
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/config/server/IsRedirectToSandboxEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171945
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0dU;->r:LX/0Tn;

    const-string v1, "facebook.com"

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "facebook.com"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Boolean;LX/00H;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/config/server/IsInternalPrefsEnabled;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/config/server/ShouldUsePreferredConfig;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171942
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171943
    iget-object v0, p1, LX/00H;->i:LX/01S;

    move-object v0, v0

    .line 171944
    sget-object v1, LX/01S;->PUBLIC:LX/01S;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0s1;)LX/0s9;
    .locals 1
    .annotation runtime Lcom/facebook/http/annotations/ProductionPlatformAppHttpConfig;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171937
    invoke-interface {p0}, LX/0s1;->b()LX/0s9;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0s1;)LX/0s9;
    .locals 1
    .annotation runtime Lcom/facebook/http/annotations/BootstrapPlatformAppHttpConfig;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171941
    invoke-interface {p0}, LX/0s1;->c()LX/0s9;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0s1;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171940
    invoke-interface {p0}, LX/0s1;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0s1;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/http/annotations/ShortUserAgentString;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 171939
    invoke-interface {p0}, LX/0s1;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 171938
    return-void
.end method
