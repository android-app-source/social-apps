.class public LX/0Sg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile C:LX/0Sg;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private B:LX/0Vq;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public final b:LX/0Sh;

.field private final c:Landroid/os/Handler;

.field private final d:LX/0Sy;

.field private final e:LX/0SG;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ks;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final p:Ljava/util/concurrent/locks/ReentrantLock;

.field public final q:Ljava/util/concurrent/locks/Condition;

.field private final r:Ljava/util/concurrent/locks/Condition;

.field private final s:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final t:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/0Vb",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final u:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private volatile v:LX/0T7;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "writes guarded by mLock"
    .end annotation
.end field

.field private w:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private x:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private y:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private z:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61875
    const-class v0, LX/0Sg;

    sput-object v0, LX/0Sg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;Landroid/os/Handler;LX/0Sy;LX/0SG;LX/0Or;)V
    .locals 6
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsAppStartupDone;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v4, 0x8

    .line 61876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61877
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61878
    iput-object v0, p0, LX/0Sg;->g:LX/0Ot;

    .line 61879
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61880
    iput-object v0, p0, LX/0Sg;->h:LX/0Ot;

    .line 61881
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61882
    iput-object v0, p0, LX/0Sg;->i:LX/0Ot;

    .line 61883
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61884
    iput-object v0, p0, LX/0Sg;->j:LX/0Ot;

    .line 61885
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61886
    iput-object v0, p0, LX/0Sg;->k:LX/0Ot;

    .line 61887
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61888
    iput-object v0, p0, LX/0Sg;->l:LX/0Ot;

    .line 61889
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61890
    iput-object v0, p0, LX/0Sg;->m:LX/0Ot;

    .line 61891
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61892
    iput-object v0, p0, LX/0Sg;->n:LX/0Ot;

    .line 61893
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 61894
    iput-object v0, p0, LX/0Sg;->o:LX/0Ot;

    .line 61895
    sget-object v0, LX/0T7;->START:LX/0T7;

    iput-object v0, p0, LX/0Sg;->v:LX/0T7;

    .line 61896
    iput-object p1, p0, LX/0Sg;->b:LX/0Sh;

    .line 61897
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0x64

    new-instance v2, LX/0T8;

    invoke-direct {v2}, LX/0T8;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    .line 61898
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    .line 61899
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    .line 61900
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    .line 61901
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, LX/0Sg;->r:Ljava/util/concurrent/locks/Condition;

    .line 61902
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0Sg;->s:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 61903
    iput-object p2, p0, LX/0Sg;->c:Landroid/os/Handler;

    .line 61904
    iput-object p3, p0, LX/0Sg;->d:LX/0Sy;

    .line 61905
    iput-object p4, p0, LX/0Sg;->e:LX/0SG;

    .line 61906
    iput-object p5, p0, LX/0Sg;->f:LX/0Or;

    .line 61907
    const-string v0, "AppChoreographer Stage"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v4, v5, v0, v1}, LX/018;->b(JLjava/lang/String;I)V

    .line 61908
    const-string v0, "AppChoreographer Stage"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sget-object v2, LX/0T7;->START:LX/0T7;

    invoke-virtual {v2}, LX/0T7;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5, v0, v1, v2}, LX/018;->a(JLjava/lang/String;ILjava/lang/String;)V

    .line 61909
    return-void
.end method

.method public static a(LX/0QB;)LX/0Sg;
    .locals 13

    .prologue
    .line 61910
    sget-object v0, LX/0Sg;->C:LX/0Sg;

    if-nez v0, :cond_1

    .line 61911
    const-class v1, LX/0Sg;

    monitor-enter v1

    .line 61912
    :try_start_0
    sget-object v0, LX/0Sg;->C:LX/0Sg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 61913
    if-eqz v2, :cond_0

    .line 61914
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 61915
    new-instance v3, LX/0Sg;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v6

    check-cast v6, LX/0Sy;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const/16 v8, 0x1495

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/0Sg;-><init>(LX/0Sh;Landroid/os/Handler;LX/0Sy;LX/0SG;LX/0Or;)V

    .line 61916
    const/16 v4, 0x245

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2db

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x240

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xf11

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x140d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1430

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x259

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/00H;

    invoke-interface {v0, v11}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xac0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    .line 61917
    iput-object v4, v3, LX/0Sg;->g:LX/0Ot;

    iput-object v5, v3, LX/0Sg;->h:LX/0Ot;

    iput-object v6, v3, LX/0Sg;->i:LX/0Ot;

    iput-object v7, v3, LX/0Sg;->j:LX/0Ot;

    iput-object v8, v3, LX/0Sg;->k:LX/0Ot;

    iput-object v9, v3, LX/0Sg;->l:LX/0Ot;

    iput-object v10, v3, LX/0Sg;->m:LX/0Ot;

    iput-object v11, v3, LX/0Sg;->n:LX/0Ot;

    iput-object v12, v3, LX/0Sg;->o:LX/0Ot;

    .line 61918
    move-object v0, v3

    .line 61919
    sput-object v0, LX/0Sg;->C:LX/0Sg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61920
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 61921
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 61922
    :cond_1
    sget-object v0, LX/0Sg;->C:LX/0Sg;

    return-object v0

    .line 61923
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 61924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/Runnable;LX/0VZ;Ljava/lang/String;)LX/0Va;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LX/0VZ;",
            "Ljava/lang/String;",
            ")",
            "LX/0Va",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61925
    new-instance v0, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$5;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$5;-><init>(LX/0Sg;LX/0VZ;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 61926
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Va;->a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/0Va;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/0Va",
            "<TT;>;",
            "LX/0VZ;",
            "Ljava/util/concurrent/ExecutorService;",
            ")",
            "LX/0Va",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 61927
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61928
    :try_start_0
    new-instance v0, LX/0Vb;

    iget-object v1, p0, LX/0Sg;->s:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v5

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/0Vb;-><init>(LX/0Va;LX/0VZ;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;I)V

    .line 61929
    iget-object v1, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 61930
    new-instance v0, LX/0Vc;

    invoke-direct {v0, p0}, LX/0Vc;-><init>(LX/0Sg;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 61931
    const/4 v0, 0x0

    invoke-direct {p0, p3, p1, v0}, LX/0Sg;->a(LX/0VZ;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61932
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61933
    :cond_0
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object p2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private a(LX/0Vm;)Ljava/util/concurrent/ExecutorService;
    .locals 3

    .prologue
    .line 61934
    sget-object v0, LX/0Vl;->b:[I

    invoke-virtual {p1}, LX/0Vm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 61935
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown thread type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61936
    :pswitch_0
    iget-object v0, p0, LX/0Sg;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    .line 61937
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/0Sg;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0Sg;LX/0T7;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61938
    iget-object v2, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61939
    const-wide/16 v2, 0x8

    :try_start_0
    const-string v4, "AppChoreographer Stage"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {p1}, LX/0T7;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, LX/018;->a(JLjava/lang/String;ILjava/lang/String;)V

    .line 61940
    sget-object v2, LX/0Vl;->a:[I

    iget-object v3, p0, LX/0Sg;->v:LX/0T7;

    invoke-virtual {v3}, LX/0T7;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 61941
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61942
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 61943
    :pswitch_0
    :try_start_1
    sget-object v2, LX/0T7;->APPLICATION_INITIALIZING:LX/0T7;

    if-ne p1, v2, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 61944
    :goto_1
    const-string v0, "AppChoreographer moving from %s to %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/0Sg;->v:LX/0T7;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, LX/0PR;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61945
    iput-object p1, p0, LX/0Sg;->v:LX/0T7;

    .line 61946
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61947
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61948
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 61949
    goto :goto_0

    .line 61950
    :pswitch_1
    :try_start_2
    sget-object v2, LX/0T7;->APPLICATION_LOADING:LX/0T7;

    if-ne p1, v2, :cond_1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_3

    .line 61951
    :pswitch_2
    sget-object v2, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    if-ne p1, v2, :cond_2

    :goto_4
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 61952
    const-wide/16 v0, 0x8

    const-string v2, "AppChoreographer Stage"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/018;->c(JLjava/lang/String;I)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 61953
    goto :goto_4

    .line 61954
    :pswitch_3
    iget-object v0, p0, LX/0Sg;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "DefaultAppChoreographer_Already_Loaded"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AppChoreographer already loaded. Requested stage = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61955
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(LX/0VZ;Ljava/lang/String;Z)Z
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61956
    sget-object v2, LX/0Vl;->a:[I

    iget-object v3, p0, LX/0Sg;->v:LX/0T7;

    invoke-virtual {v3}, LX/0T7;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    .line 61957
    :cond_1
    :goto_0
    return v0

    .line 61958
    :pswitch_1
    sget-object v2, LX/0VZ;->STARTUP_INITIALIZATION:LX/0VZ;

    if-ne p1, v2, :cond_0

    goto :goto_0

    .line 61959
    :pswitch_2
    invoke-virtual {p1}, LX/0VZ;->ordinal()I

    move-result v2

    sget-object v3, LX/0VZ;->APPLICATION_LOADING:LX/0VZ;

    invoke-virtual {v3}, LX/0VZ;->ordinal()I

    move-result v3

    if-gt v2, v3, :cond_0

    goto :goto_0

    .line 61960
    :pswitch_3
    invoke-virtual {p1}, LX/0VZ;->ordinal()I

    move-result v2

    sget-object v3, LX/0VZ;->APPLICATION_LOADING:LX/0VZ;

    invoke-virtual {v3}, LX/0VZ;->ordinal()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 61961
    invoke-static {p0}, LX/0Sg;->l(LX/0Sg;)V

    .line 61962
    iget-object v2, p0, LX/0Sg;->d:LX/0Sy;

    invoke-virtual {v2}, LX/0Sy;->b()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 61963
    goto :goto_0

    .line 61964
    :cond_3
    const/4 v5, 0x1

    .line 61965
    iget-object v4, p0, LX/0Sg;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ks;

    .line 61966
    iget-boolean v6, v4, LX/0ks;->b:Z

    if-nez v6, :cond_4

    iget-boolean v6, v4, LX/0ks;->a:Z

    if-eqz v6, :cond_b

    :cond_4
    const/4 v6, 0x1

    :goto_1
    move v4, v6

    .line 61967
    if-nez v4, :cond_5

    iget-object v4, p0, LX/0Sg;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->l()Z

    move-result v4

    if-nez v4, :cond_8

    :cond_5
    move v4, v5

    .line 61968
    :goto_2
    move v2, v4

    .line 61969
    if-nez v2, :cond_6

    move v0, v1

    .line 61970
    goto :goto_0

    .line 61971
    :cond_6
    sget-object v2, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    if-eq p1, v2, :cond_1

    .line 61972
    iget-boolean v0, p0, LX/0Sg;->x:Z

    if-eqz v0, :cond_7

    .line 61973
    iget-object v0, p0, LX/0Sg;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 61974
    :cond_7
    iget-object v0, p0, LX/0Sg;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move v0, v1

    .line 61975
    goto :goto_0

    .line 61976
    :cond_8
    iget-object v4, p0, LX/0Sg;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v6

    iget-object v4, p0, LX/0Sg;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Uo;

    .line 61977
    iget-wide v10, v4, LX/0Uo;->J:J

    move-wide v8, v10

    .line 61978
    sub-long/2addr v6, v8

    .line 61979
    const-wide/32 v8, 0xea60

    cmp-long v4, v6, v8

    if-gez v4, :cond_9

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-gez v4, :cond_a

    :cond_9
    move v4, v5

    .line 61980
    goto :goto_2

    .line 61981
    :cond_a
    const/4 v4, 0x0

    goto :goto_2

    :cond_b
    const/4 v6, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static h(LX/0Sg;)V
    .locals 2

    .prologue
    .line 61982
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61983
    :try_start_0
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61984
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61985
    return-void

    .line 61986
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private static i(LX/0Sg;)V
    .locals 8
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 61832
    iget-object v0, p0, LX/0Sg;->v:LX/0T7;

    sget-object v3, LX/0T7;->APPLICATION_LOADING:LX/0T7;

    if-eq v0, v3, :cond_1

    .line 61833
    :cond_0
    :goto_0
    return-void

    .line 61834
    :cond_1
    invoke-static {p0}, LX/0Sg;->l(LX/0Sg;)V

    .line 61835
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61836
    iget-boolean v0, p0, LX/0Sg;->w:Z

    if-eqz v0, :cond_2

    .line 61837
    const-string v0, "AppChoreographer: Advancing to loaded because UI is no longer loading"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    move v0, v1

    .line 61838
    :goto_1
    if-eqz v0, :cond_4

    .line 61839
    sget-object v0, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    invoke-static {p0, v0}, LX/0Sg;->a(LX/0Sg;LX/0T7;)V

    goto :goto_0

    .line 61840
    :cond_2
    sget-object v3, LX/03R;->YES:LX/03R;

    iget-object v0, p0, LX/0Sg;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->k()LX/03R;

    move-result-object v0

    if-ne v3, v0, :cond_3

    .line 61841
    const-string v0, "AppChoreographer: Advancing to loaded because app is backgrounded"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    .line 61842
    :cond_3
    iget-object v0, p0, LX/0Sg;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    .line 61843
    const-string v0, "AppChoreographer: Advancing to loaded because exceeded time threshold"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    .line 61844
    :cond_4
    iget-object v0, p0, LX/0Sg;->c:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public static j(LX/0Sg;)V
    .locals 5

    .prologue
    .line 61845
    iget-object v0, p0, LX/0Sg;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 61846
    :goto_0
    const/4 v1, 0x0

    .line 61847
    :try_start_0
    invoke-static {}, LX/00m;->c()V

    .line 61848
    invoke-static {p0}, LX/0Sg;->k(LX/0Sg;)LX/0Vb;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 61849
    :try_start_1
    iget-object v0, p0, LX/0Sg;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    .line 61850
    iget-object v2, v0, LX/00H;->j:LX/01T;

    move-object v0, v2

    .line 61851
    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/0Sg;->v:LX/0T7;

    sget-object v2, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    if-ne v0, v2, :cond_0

    iget-object v0, v1, LX/0Vb;->b:LX/0VZ;

    invoke-virtual {v0}, LX/0VZ;->ordinal()I

    move-result v0

    sget-object v2, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    invoke-virtual {v2}, LX/0VZ;->ordinal()I

    move-result v2

    if-lt v0, v2, :cond_0

    iget-object v0, p0, LX/0Sg;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x12f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Sg;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kd;

    invoke-virtual {v0}, LX/0kd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61852
    const-string v0, "AppChoreographer-waitForStartup"

    const v2, -0x2056087e

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_1 .. :try_end_1} :catch_1

    .line 61853
    :try_start_2
    iget-object v0, p0, LX/0Sg;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kd;

    invoke-virtual {v0}, LX/0kd;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61854
    const v0, -0x459aa990

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V

    .line 61855
    :cond_0
    iget-object v0, v1, LX/0Vb;->c:Ljava/util/concurrent/ExecutorService;

    iget-object v2, v1, LX/0Vb;->a:LX/0Va;

    const v3, 0x4f162a91

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    .line 61856
    const v2, -0x68f8200f

    :try_start_4
    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_4 .. :try_end_4} :catch_1

    .line 61857
    :try_start_5
    iget-object v0, p0, LX/0Sg;->c:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 61858
    :catch_0
    move-exception v0

    .line 61859
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 61860
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 61861
    :catchall_0
    move-exception v0

    const v2, -0x62b366bb

    :try_start_6
    invoke-static {v2}, LX/02m;->a(I)V

    throw v0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_6 .. :try_end_6} :catch_1

    .line 61862
    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 61863
    :goto_1
    if-eqz v0, :cond_3

    .line 61864
    iget-object v1, v0, LX/0Vb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v0, v0, LX/0Vb;->d:Ljava/lang/String;

    move-object v1, v0

    .line 61865
    :goto_2
    iget-object v0, p0, LX/0Sg;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "fb_task_description"

    invoke-virtual {v0, v3, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 61866
    :goto_3
    invoke-static {v2}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 61867
    :catch_2
    move-exception v3

    .line 61868
    :try_start_7
    iget-object v0, v1, LX/0Vb;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, LX/0Vb;->d:Ljava/lang/String;

    move-object v2, v0

    .line 61869
    :goto_4
    iget-object v0, p0, LX/0Sg;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "fb_task_description"

    invoke-virtual {v0, v4, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 61870
    invoke-static {v3}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 61871
    :cond_1
    const-string v0, "Null description"
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_7 .. :try_end_7} :catch_1

    move-object v2, v0

    goto :goto_4

    .line 61872
    :cond_2
    const-string v0, "Null description"

    move-object v1, v0

    goto :goto_2

    .line 61873
    :cond_3
    iget-object v0, p0, LX/0Sg;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fb_task_description"

    const-string v3, "Null task"

    invoke-virtual {v0, v1, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 61874
    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_1
.end method

.method private static k(LX/0Sg;)LX/0Vb;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vb",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 61808
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61809
    :goto_0
    :try_start_0
    iget-boolean v0, p0, LX/0Sg;->A:Z

    if-eqz v0, :cond_2

    .line 61810
    iget-object v0, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 61811
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61812
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vb;

    .line 61813
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/0Vb;->a:LX/0Va;

    invoke-virtual {v0}, LX/0Va;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61814
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 61815
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 61816
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/0Sg;->A:Z

    .line 61817
    :cond_2
    iget-object v0, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vb;

    .line 61818
    if-eqz v0, :cond_4

    .line 61819
    iget-object v1, v0, LX/0Vb;->a:LX/0Va;

    invoke-virtual {v1}, LX/0Va;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 61820
    iget-object v0, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    goto :goto_0

    .line 61821
    :cond_3
    iget v1, p0, LX/0Sg;->z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Sg;->z:I

    .line 61822
    iget-object v1, v0, LX/0Vb;->b:LX/0VZ;

    iget-object v0, v0, LX/0Vb;->d:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, LX/0Sg;->a(LX/0VZ;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 61823
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Sg;->x:Z

    .line 61824
    iget-object v0, p0, LX/0Sg;->t:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61825
    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    .line 61826
    :cond_4
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, LX/0Sg;->x:Z

    .line 61827
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Sg;->y:Z

    .line 61828
    iget-object v0, p0, LX/0Sg;->r:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 61829
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    .line 61830
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Sg;->y:Z

    .line 61831
    iget-object v0, p0, LX/0Sg;->r:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static l(LX/0Sg;)V
    .locals 10
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 61799
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61800
    :cond_0
    return-void

    .line 61801
    :cond_1
    iget-object v0, p0, LX/0Sg;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 61802
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 61803
    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61804
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 61805
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v2, v6

    const-wide/32 v8, 0xea60

    cmp-long v1, v6, v8

    if-ltz v1, :cond_2

    .line 61806
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 61807
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            "LX/0VZ;",
            "LX/0Vm;",
            ")",
            "LX/0Va",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 61796
    invoke-direct {p0, p2, p3, p1}, LX/0Sg;->a(Ljava/lang/Runnable;LX/0VZ;Ljava/lang/String;)LX/0Va;

    move-result-object v0

    .line 61797
    invoke-direct {p0, p4}, LX/0Sg;->a(LX/0Vm;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 61798
    invoke-direct {p0, p1, v0, p3, v1}, LX/0Sg;->a(Ljava/lang/String;LX/0Va;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            "LX/0VZ;",
            "Ljava/util/concurrent/ExecutorService;",
            ")",
            "LX/0Va",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 61794
    invoke-direct {p0, p2, p3, p1}, LX/0Sg;->a(Ljava/lang/Runnable;LX/0VZ;Ljava/lang/String;)LX/0Va;

    move-result-object v0

    .line 61795
    invoke-direct {p0, p1, v0, p3, p4}, LX/0Sg;->a(Ljava/lang/String;LX/0Va;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 61786
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61787
    :try_start_0
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    iget-object v1, p0, LX/0Sg;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61788
    new-instance v1, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$7;-><init>(LX/0Sg;Lcom/google/common/util/concurrent/ListenableFuture;)V

    iget-object v0, p0, LX/0Sg;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-interface {p1, v1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 61789
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Sg;->w:Z

    .line 61790
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61791
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61792
    return-void

    .line 61793
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 61778
    iget-object v1, p0, LX/0Sg;->v:LX/0T7;

    sget-object v2, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    if-ne v1, v2, :cond_0

    .line 61779
    :goto_0
    return v0

    .line 61780
    :cond_0
    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61781
    :try_start_0
    invoke-static {p0}, LX/0Sg;->i(LX/0Sg;)V

    .line 61782
    iget-object v1, p0, LX/0Sg;->v:LX/0T7;

    sget-object v2, LX/0T7;->APPLICATION_LOADED:LX/0T7;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 61783
    :goto_1
    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 61784
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 61785
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 61770
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61771
    :try_start_0
    iget-object v0, p0, LX/0Sg;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 61772
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61773
    invoke-static {p0}, LX/0Sg;->i(LX/0Sg;)V

    .line 61774
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61775
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61776
    return-void

    .line 61777
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 61745
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61746
    :try_start_0
    invoke-static {p0}, LX/0Sg;->l(LX/0Sg;)V

    .line 61747
    iget-object v0, p0, LX/0Sg;->u:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 61748
    :goto_0
    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    .line 61749
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 61750
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 61760
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61761
    :try_start_0
    new-instance v0, LX/0Vp;

    invoke-direct {v0, p0}, LX/0Vp;-><init>(LX/0Sg;)V

    iput-object v0, p0, LX/0Sg;->B:LX/0Vq;

    .line 61762
    iget-object v0, p0, LX/0Sg;->d:LX/0Sy;

    iget-object v1, p0, LX/0Sg;->B:LX/0Vq;

    invoke-virtual {v0, v1}, LX/0Sy;->a(LX/0Vq;)V

    .line 61763
    sget-object v0, LX/0T7;->APPLICATION_INITIALIZING:LX/0T7;

    invoke-static {p0, v0}, LX/0Sg;->a(LX/0Sg;LX/0T7;)V

    .line 61764
    new-instance v0, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$2;

    invoke-direct {v0, p0}, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$2;-><init>(LX/0Sg;)V

    const-string v1, "AppChoreographer"

    const v2, -0x9b6e802

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    .line 61765
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 61766
    iget-object v0, p0, LX/0Sg;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$3;

    invoke-direct {v1, p0}, Lcom/facebook/common/appchoreographer/DefaultAppChoreographer$3;-><init>(LX/0Sg;)V

    const v2, -0x7c0fb35a

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61767
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61768
    return-void

    .line 61769
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final f()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 61751
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 61752
    :try_start_0
    invoke-static {p0}, LX/0Sg;->i(LX/0Sg;)V

    .line 61753
    iget-object v0, p0, LX/0Sg;->v:LX/0T7;

    sget-object v1, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    if-ne v0, v1, :cond_0

    .line 61754
    iget-boolean v0, p0, LX/0Sg;->x:Z

    if-nez v0, :cond_0

    .line 61755
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Sg;->x:Z

    .line 61756
    iget-object v0, p0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61757
    :cond_0
    iget-object v0, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61758
    return-void

    .line 61759
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
