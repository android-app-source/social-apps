.class public LX/15d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Z

.field private final b:LX/0Uh;

.field public final c:LX/0ad;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;LX/0wq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180942
    iget-boolean v0, p3, LX/0wq;->Q:Z

    iput-boolean v0, p0, LX/15d;->a:Z

    .line 180943
    iput-object p1, p0, LX/15d;->b:LX/0Uh;

    .line 180944
    iput-object p2, p0, LX/15d;->c:LX/0ad;

    .line 180945
    return-void
.end method

.method public static b(LX/0QB;)LX/15d;
    .locals 4

    .prologue
    .line 180939
    new-instance v3, LX/15d;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v2

    check-cast v2, LX/0wq;

    invoke-direct {v3, v0, v1, v2}, LX/15d;-><init>(LX/0Uh;LX/0ad;LX/0wq;)V

    .line 180940
    return-object v3
.end method

.method public static e(LX/15d;)Z
    .locals 3

    .prologue
    .line 180936
    iget-object v0, p0, LX/15d;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 180937
    iget-object v0, p0, LX/15d;->b:LX/0Uh;

    const/16 v1, 0x34d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/15d;->d:Ljava/lang/Boolean;

    .line 180938
    :cond_0
    iget-object v0, p0, LX/15d;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 180932
    iget-object v1, p0, LX/15d;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 180933
    invoke-static {p0}, LX/15d;->e(LX/15d;)Z

    move-result v1

    .line 180934
    iget-boolean v2, p0, LX/15d;->a:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/15d;->c:LX/0ad;

    sget-short v2, LX/BSC;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/15d;->e:Ljava/lang/Boolean;

    .line 180935
    :cond_2
    iget-object v0, p0, LX/15d;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 180929
    iget-object v0, p0, LX/15d;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 180930
    iget-object v0, p0, LX/15d;->c:LX/0ad;

    sget-short v1, LX/BSC;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/15d;->h:Ljava/lang/Boolean;

    .line 180931
    :cond_0
    iget-object v0, p0, LX/15d;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
