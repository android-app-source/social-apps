.class public LX/1Tn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:[LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 254622
    const/16 v0, 0xd

    new-array v0, v0, [LX/1Cz;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->a:LX/1To;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->a:LX/1Tp;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->a:LX/1Tq;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->b:LX/1Cz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->a:LX/1Cz;

    aput-object v2, v0, v1

    sput-object v0, LX/1Tn;->a:[LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254620
    iput-object p1, p0, LX/1Tn;->b:LX/0Ot;

    .line 254621
    return-void
.end method

.method public static a(LX/0QB;)LX/1Tn;
    .locals 4

    .prologue
    .line 254605
    const-class v1, LX/1Tn;

    monitor-enter v1

    .line 254606
    :try_start_0
    sget-object v0, LX/1Tn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254607
    sput-object v2, LX/1Tn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254608
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254609
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254610
    new-instance v3, LX/1Tn;

    const/16 p0, 0x21b9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Tn;-><init>(LX/0Ot;)V

    .line 254611
    move-object v0, v3

    .line 254612
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254613
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Tn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254614
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 4

    .prologue
    .line 254623
    sget-object v1, LX/1Tn;->a:[LX/1Cz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 254624
    invoke-static {p1, v3}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 254625
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254626
    :cond_0
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 0

    .prologue
    .line 254618
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 254616
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    iget-object v1, p0, LX/1Tn;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 254617
    return-void
.end method
