.class public LX/10i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Xp;

.field private final b:LX/10k;

.field public final c:LX/10k;

.field public final d:LX/10k;

.field public final e:LX/10k;

.field public f:LX/10b;


# direct methods
.method public constructor <init>(LX/10b;LX/0Xp;LX/10k;LX/10k;LX/10k;LX/10k;)V
    .locals 0
    .param p1    # LX/10b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/10k;
        .annotation runtime Lcom/facebook/divebar/FavoritesDivebar;
        .end annotation
    .end param
    .param p4    # LX/10k;
        .annotation runtime Lcom/facebook/divebar/NowDivebar;
        .end annotation
    .end param
    .param p5    # LX/10k;
        .annotation runtime Lcom/facebook/divebar/SnacksDivebar;
        .end annotation
    .end param
    .param p6    # LX/10k;
        .annotation runtime Lcom/facebook/divebar/InspirationCameraDivebar;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169252
    iput-object p1, p0, LX/10i;->f:LX/10b;

    .line 169253
    iput-object p2, p0, LX/10i;->a:LX/0Xp;

    .line 169254
    iput-object p3, p0, LX/10i;->b:LX/10k;

    .line 169255
    iput-object p4, p0, LX/10i;->c:LX/10k;

    .line 169256
    iput-object p5, p0, LX/10i;->d:LX/10k;

    .line 169257
    iput-object p6, p0, LX/10i;->e:LX/10k;

    .line 169258
    return-void
.end method

.method public static c(LX/10i;)LX/10k;
    .locals 2

    .prologue
    .line 169259
    iget-object v0, p0, LX/10i;->d:LX/10k;

    iget-object v1, p0, LX/10i;->f:LX/10b;

    invoke-interface {v0, v1}, LX/10k;->a(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169260
    iget-object v0, p0, LX/10i;->d:LX/10k;

    .line 169261
    :goto_0
    return-object v0

    .line 169262
    :cond_0
    iget-object v0, p0, LX/10i;->c:LX/10k;

    iget-object v1, p0, LX/10i;->f:LX/10b;

    invoke-interface {v0, v1}, LX/10k;->a(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169263
    iget-object v0, p0, LX/10i;->c:LX/10k;

    goto :goto_0

    .line 169264
    :cond_1
    iget-object v0, p0, LX/10i;->e:LX/10k;

    iget-object v1, p0, LX/10i;->f:LX/10b;

    invoke-interface {v0, v1}, LX/10k;->a(LX/10b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169265
    iget-object v0, p0, LX/10i;->e:LX/10k;

    goto :goto_0

    .line 169266
    :cond_2
    iget-object v0, p0, LX/10i;->b:LX/10k;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0gs;)V
    .locals 2

    .prologue
    .line 169267
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 169268
    const-string v1, "com.facebook.orca.threadview.DIVEBAR_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169269
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 169270
    iget-object v1, p0, LX/10i;->a:LX/0Xp;

    invoke-virtual {v1, v0}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 169271
    return-void
.end method
