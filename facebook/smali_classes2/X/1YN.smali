.class public LX/1YN;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fs;


# instance fields
.field private final a:LX/1YO;


# direct methods
.method public constructor <init>(LX/1YO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273461
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 273462
    iput-object p1, p0, LX/1YN;->a:LX/1YO;

    .line 273463
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 273464
    iget-object v0, p0, LX/1YN;->a:LX/1YO;

    const-class v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 273465
    invoke-static {v0}, LX/1YO;->a(LX/1YO;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 273466
    :goto_0
    return-void

    .line 273467
    :cond_0
    iget-object p0, v0, LX/1YO;->b:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 273468
    iget-object p0, v0, LX/1YO;->c:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1YR;

    .line 273469
    invoke-interface {p0}, LX/1YR;->c()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 273470
    invoke-interface {p0, p3}, LX/1YR;->a(LX/1PW;)V

    .line 273471
    iget-object p2, v0, LX/1YO;->d:Ljava/util/Set;

    invoke-interface {p2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 273472
    :cond_2
    iget-object p0, v0, LX/1YO;->b:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 273473
    iget-object v0, p0, LX/1YN;->a:LX/1YO;

    const-class v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 273474
    invoke-static {v0}, LX/1YO;->a(LX/1YO;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 273475
    :cond_0
    :goto_0
    return-void

    .line 273476
    :cond_1
    iget-object p0, v0, LX/1YO;->b:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 273477
    iget-object p0, v0, LX/1YO;->b:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 273478
    iget-object p0, v0, LX/1YO;->d:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1YR;

    .line 273479
    invoke-interface {p0}, LX/1YR;->b()V

    goto :goto_1

    .line 273480
    :cond_2
    iget-object p0, v0, LX/1YO;->d:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 273481
    goto :goto_0
.end method
