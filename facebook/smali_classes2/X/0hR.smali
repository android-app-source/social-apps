.class public final enum LX/0hR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0hR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0hR;

.field public static final enum ASCENDING:LX/0hR;

.field public static final enum DESCENDING:LX/0hR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117662
    new-instance v0, LX/0hR;

    const-string v1, "ASCENDING"

    invoke-direct {v0, v1, v2}, LX/0hR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0hR;->ASCENDING:LX/0hR;

    .line 117663
    new-instance v0, LX/0hR;

    const-string v1, "DESCENDING"

    invoke-direct {v0, v1, v3}, LX/0hR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0hR;->DESCENDING:LX/0hR;

    .line 117664
    const/4 v0, 0x2

    new-array v0, v0, [LX/0hR;

    sget-object v1, LX/0hR;->ASCENDING:LX/0hR;

    aput-object v1, v0, v2

    sget-object v1, LX/0hR;->DESCENDING:LX/0hR;

    aput-object v1, v0, v3

    sput-object v0, LX/0hR;->$VALUES:[LX/0hR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 117665
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0hR;
    .locals 1

    .prologue
    .line 117666
    const-class v0, LX/0hR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0hR;

    return-object v0
.end method

.method public static values()[LX/0hR;
    .locals 1

    .prologue
    .line 117667
    sget-object v0, LX/0hR;->$VALUES:[LX/0hR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0hR;

    return-object v0
.end method
