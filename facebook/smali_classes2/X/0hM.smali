.class public LX/0hM;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Tn;

.field public static final B:LX/0Tn;

.field public static final C:LX/0Tn;

.field public static final D:LX/0Tn;

.field public static final E:LX/0Tn;

.field public static final F:LX/0Tn;

.field public static final G:LX/0Tn;

.field public static final H:LX/0Tn;

.field public static final I:LX/0Tn;

.field public static final J:LX/0Tn;

.field public static final K:LX/0Tn;

.field public static final L:LX/0Tn;

.field public static final M:LX/0Tn;

.field public static final N:LX/0Tn;

.field public static final O:LX/0Tn;

.field public static final P:LX/0Tn;

.field public static final Q:LX/0Tn;

.field public static final R:LX/0Tn;

.field public static final S:LX/0Tn;

.field public static final T:LX/0Tn;

.field public static final U:LX/0Tn;

.field public static final V:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 115826
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "notifications/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 115827
    sput-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "last_updated_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->b:LX/0Tn;

    .line 115828
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "last_clicked_notif_cache_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->c:LX/0Tn;

    .line 115829
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "count_before_last_update"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->d:LX/0Tn;

    .line 115830
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "count_after_last_update"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->e:LX/0Tn;

    .line 115831
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "added_to_db_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->f:LX/0Tn;

    .line 115832
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "full_screen_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->g:LX/0Tn;

    .line 115833
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "half_screen_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->h:LX/0Tn;

    .line 115834
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "ringtone_registered"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->i:LX/0Tn;

    .line 115835
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "clear_push_notif_pref"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->j:LX/0Tn;

    .line 115836
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "notifs"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->k:LX/0Tn;

    .line 115837
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "lockscreen_on"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->l:LX/0Tn;

    .line 115838
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "vibrate"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->m:LX/0Tn;

    .line 115839
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "use_led"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->n:LX/0Tn;

    .line 115840
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "ringtone_sound"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->o:LX/0Tn;

    .line 115841
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "ringtone"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->p:LX/0Tn;

    .line 115842
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "wall_posts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->q:LX/0Tn;

    .line 115843
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "messages"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->r:LX/0Tn;

    .line 115844
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "comments"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->s:LX/0Tn;

    .line 115845
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "friend_requests"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->t:LX/0Tn;

    .line 115846
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "friend_confirmations"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->u:LX/0Tn;

    .line 115847
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "photo_tags"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->v:LX/0Tn;

    .line 115848
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "event_invites"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->w:LX/0Tn;

    .line 115849
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "app_requests"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->x:LX/0Tn;

    .line 115850
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "groups"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->y:LX/0Tn;

    .line 115851
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "place_tips"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->z:LX/0Tn;

    .line 115852
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "preview"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->A:LX/0Tn;

    .line 115853
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "polling_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->B:LX/0Tn;

    .line 115854
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "last_notifications_sync_new_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->C:LX/0Tn;

    .line 115855
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "last_notifications_sync_full_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->D:LX/0Tn;

    .line 115856
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "lockscreen_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->E:LX/0Tn;

    .line 115857
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "lockscreen_show_facebook"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->F:LX/0Tn;

    .line 115858
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "lockscreen_light_up_screen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->G:LX/0Tn;

    .line 115859
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "pending_jewel_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->H:LX/0Tn;

    .line 115860
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "pending_friend_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->I:LX/0Tn;

    .line 115861
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "inline_notification_nux_hidden"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->J:LX/0Tn;

    .line 115862
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "inline_notification_nux_first_seen_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->K:LX/0Tn;

    .line 115863
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "sync_local_preference_settings"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->L:LX/0Tn;

    .line 115864
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "notification_tab_image_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->M:LX/0Tn;

    .line 115865
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "notifications_logged_out_push_enabled/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->N:LX/0Tn;

    .line 115866
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "notifications_logged_out_push_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->O:LX/0Tn;

    .line 115867
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "after_login_redirect_to_notifications"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->P:LX/0Tn;

    .line 115868
    sget-object v0, LX/0hM;->a:LX/0Tn;

    const-string v1, "logged_out_push_logging_out_prefix/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 115869
    sput-object v0, LX/0hM;->Q:LX/0Tn;

    const-string v1, "set_up_after_login_redirect_to_notifications"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->R:LX/0Tn;

    .line 115870
    sget-object v0, LX/0hM;->Q:LX/0Tn;

    const-string v1, "notifications_logged_out_push_target_uid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->S:LX/0Tn;

    .line 115871
    sget-object v0, LX/0hM;->Q:LX/0Tn;

    const-string v1, "notifications_logged_out_push_ndid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->T:LX/0Tn;

    .line 115872
    sget-object v0, LX/0hM;->Q:LX/0Tn;

    const-string v1, "notifications_logged_out_push_landing_experience"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->U:LX/0Tn;

    .line 115873
    sget-object v0, LX/0hM;->Q:LX/0Tn;

    const-string v1, "notifications_logged_out_push_current_uid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0hM;->V:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 115875
    sget-object v0, LX/0hM;->N:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 115876
    sget-object v0, LX/0hM;->O:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
