.class public LX/11k;
.super LX/11l;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/11k;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173131
    invoke-direct {p0}, LX/11l;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/11k;
    .locals 3

    .prologue
    .line 173132
    sget-object v0, LX/11k;->a:LX/11k;

    if-nez v0, :cond_1

    .line 173133
    const-class v1, LX/11k;

    monitor-enter v1

    .line 173134
    :try_start_0
    sget-object v0, LX/11k;->a:LX/11k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 173135
    if-eqz v2, :cond_0

    .line 173136
    :try_start_1
    new-instance v0, LX/11k;

    invoke-direct {v0}, LX/11k;-><init>()V

    .line 173137
    move-object v0, v0

    .line 173138
    sput-object v0, LX/11k;->a:LX/11k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173139
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 173140
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 173141
    :cond_1
    sget-object v0, LX/11k;->a:LX/11k;

    return-object v0

    .line 173142
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 173143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;)V
    .locals 4

    .prologue
    .line 173144
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 173145
    :cond_0
    const-class v0, LX/11l;

    const-string v1, "event/Observers cannot be null"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 173146
    :goto_0
    return-void

    .line 173147
    :cond_1
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 173148
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 173149
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    .line 173150
    iget-object v3, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 173151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 173152
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
