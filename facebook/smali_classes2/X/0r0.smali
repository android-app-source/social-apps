.class public LX/0r0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0r1;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148859
    sget-object v0, LX/0r1;->NOT_INITIALIZED:LX/0r1;

    iput-object v0, p0, LX/0r0;->a:LX/0r1;

    .line 148860
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 148852
    iget-object v0, p0, LX/0r0;->a:LX/0r1;

    sget-object v1, LX/0r1;->INITIALIZED:LX/0r1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 148854
    iget-object v0, p0, LX/0r0;->a:LX/0r1;

    sget-object v1, LX/0r1;->NOT_INITIALIZED:LX/0r1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 148855
    sget-object v0, LX/0r1;->INITIAL_FETCH_STARTED:LX/0r1;

    iput-object v0, p0, LX/0r0;->a:LX/0r1;

    .line 148856
    return-void

    .line 148857
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 148853
    iget-object v0, p0, LX/0r0;->a:LX/0r1;

    sget-object v1, LX/0r1;->INITIAL_FETCH_STARTED:LX/0r1;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/0r0;->a:LX/0r1;

    sget-object v1, LX/0r1;->INITIAL_HEAD_LOAD_COMPLETE:LX/0r1;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/0r0;->a:LX/0r1;

    sget-object v1, LX/0r1;->INITIAL_TAIL_LOAD_COMPLETE:LX/0r1;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
