.class public LX/15j;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181513
    const-class v0, LX/15j;

    sput-object v0, LX/15j;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 0
    .param p2    # LX/0Tn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 181520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181521
    iput-object p1, p0, LX/15j;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 181522
    iput-object p2, p0, LX/15j;->c:LX/0Tn;

    .line 181523
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 181517
    iget-object v0, p0, LX/15j;->c:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    .line 181518
    iget-object v0, p0, LX/15j;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/15j;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 181519
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 181516
    iget-object v0, p0, LX/15j;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/15j;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 181514
    iget-object v0, p0, LX/15j;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/15j;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 181515
    return-void
.end method
