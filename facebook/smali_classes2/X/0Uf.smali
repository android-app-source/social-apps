.class public final LX/0Uf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/io/File;

.field private b:Ljava/io/RandomAccessFile;

.field private c:Ljava/nio/channels/FileLock;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 66553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66554
    iput-object p1, p0, LX/0Uf;->a:Ljava/io/File;

    .line 66555
    return-void
.end method

.method public static b(Ljava/io/File;Ljava/io/RandomAccessFile;)V
    .locals 5

    .prologue
    .line 66565
    :try_start_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66566
    :goto_0
    return-void

    .line 66567
    :catch_0
    move-exception v0

    .line 66568
    sget-object v1, LX/0Ua;->a:Ljava/lang/String;

    const-string v2, "Cannot close file %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 66569
    iget-object v1, p0, LX/0Uf;->a:Ljava/io/File;

    .line 66570
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v2, v1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66571
    :goto_0
    move-object v1, v2

    .line 66572
    if-nez v1, :cond_0

    .line 66573
    :goto_1
    return v0

    .line 66574
    :cond_0
    iget-object v2, p0, LX/0Uf;->a:Ljava/io/File;

    .line 66575
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 66576
    :try_start_1
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 66577
    :goto_2
    move-object v2, v3

    .line 66578
    if-nez v2, :cond_1

    .line 66579
    iget-object v2, p0, LX/0Uf;->a:Ljava/io/File;

    invoke-static {v2, v1}, LX/0Uf;->b(Ljava/io/File;Ljava/io/RandomAccessFile;)V

    goto :goto_1

    .line 66580
    :cond_1
    iput-object v1, p0, LX/0Uf;->b:Ljava/io/RandomAccessFile;

    .line 66581
    iput-object v2, p0, LX/0Uf;->c:Ljava/nio/channels/FileLock;

    .line 66582
    const/4 v0, 0x1

    goto :goto_1

    .line 66583
    :catch_0
    move-exception v2

    .line 66584
    sget-object v3, LX/0Ua;->a:Ljava/lang/String;

    const-string v4, "Cannot create file %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66585
    const/4 v2, 0x0

    goto :goto_0

    .line 66586
    :catch_1
    move-exception v3

    .line 66587
    sget-object v4, LX/0Ua;->a:Ljava/lang/String;

    const-string v5, "Cannot acquire a lock to file %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v4, v3, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66588
    invoke-static {v2, v1}, LX/0Uf;->b(Ljava/io/File;Ljava/io/RandomAccessFile;)V

    .line 66589
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 66556
    iget-object v0, p0, LX/0Uf;->c:Ljava/nio/channels/FileLock;

    .line 66557
    iget-object v1, p0, LX/0Uf;->b:Ljava/io/RandomAccessFile;

    .line 66558
    iput-object v2, p0, LX/0Uf;->c:Ljava/nio/channels/FileLock;

    .line 66559
    iput-object v2, p0, LX/0Uf;->b:Ljava/io/RandomAccessFile;

    .line 66560
    :try_start_0
    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66561
    :goto_0
    iget-object v0, p0, LX/0Uf;->a:Ljava/io/File;

    invoke-static {v0, v1}, LX/0Uf;->b(Ljava/io/File;Ljava/io/RandomAccessFile;)V

    .line 66562
    return-void

    .line 66563
    :catch_0
    move-exception v0

    .line 66564
    sget-object v2, LX/0Ua;->a:Ljava/lang/String;

    const-string v3, "Cannot release a lock to file %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/0Uf;->a:Ljava/io/File;

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
