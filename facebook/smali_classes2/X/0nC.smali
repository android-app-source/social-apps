.class public final LX/0nC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0nD;


# static fields
.field private static a:LX/0nC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()LX/0nC;
    .locals 2

    .prologue
    .line 134947
    const-class v1, LX/0nC;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0nC;->a:LX/0nC;

    if-nez v0, :cond_0

    .line 134948
    new-instance v0, LX/0nC;

    invoke-direct {v0}, LX/0nC;-><init>()V

    sput-object v0, LX/0nC;->a:LX/0nC;

    .line 134949
    :cond_0
    sget-object v0, LX/0nC;->a:LX/0nC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 134950
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 134940
    invoke-virtual {p0, v3}, Ljava/io/Writer;->write(I)V

    .line 134941
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 134942
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 134943
    invoke-static {p0, v2}, LX/10H;->a(Ljava/io/Writer;C)V

    .line 134944
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134945
    :cond_0
    invoke-virtual {p0, v3}, Ljava/io/Writer;->write(I)V

    .line 134946
    return-void
.end method

.method private a(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134923
    if-nez p3, :cond_0

    .line 134924
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 134925
    :goto_0
    return-void

    .line 134926
    :cond_0
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 134927
    check-cast p3, Ljava/lang/String;

    invoke-static {p1, p3}, LX/0nC;->a(Ljava/io/Writer;Ljava/lang/String;)V

    goto :goto_0

    .line 134928
    :cond_1
    instance-of v0, p3, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 134929
    check-cast p3, Ljava/lang/Number;

    .line 134930
    invoke-static {}, LX/10I;->a()LX/10I;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, LX/10I;->a(Ljava/io/Writer;Ljava/lang/Number;)V

    .line 134931
    goto :goto_0

    .line 134932
    :cond_2
    instance-of v0, p3, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 134933
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "true"

    :goto_1
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "false"

    goto :goto_1

    .line 134934
    :cond_4
    instance-of v0, p3, LX/0nA;

    if-eqz v0, :cond_5

    .line 134935
    check-cast p3, LX/0nA;

    invoke-direct {p0, p1, p3}, LX/0nC;->c(Ljava/io/Writer;LX/0nA;)V

    goto :goto_0

    .line 134936
    :cond_5
    const-string v0, ""

    .line 134937
    if-eqz p2, :cond_6

    .line 134938
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " (found in key \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134939
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not supported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static b(LX/0nC;Ljava/io/Writer;LX/0n9;)V
    .locals 4

    .prologue
    .line 134913
    iget v0, p2, LX/0n9;->c:I

    move v1, v0

    .line 134914
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 134915
    if-lez v0, :cond_0

    .line 134916
    const/16 v2, 0x2c

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(I)V

    .line 134917
    :cond_0
    invoke-virtual {p2, v0}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 134918
    invoke-static {p1, v2}, LX/0nC;->a(Ljava/io/Writer;Ljava/lang/String;)V

    .line 134919
    const/16 v3, 0x3a

    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(I)V

    .line 134920
    invoke-virtual {p2, v0}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, p1, v2, v3}, LX/0nC;->a(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134921
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134922
    :cond_1
    return-void
.end method

.method public static b(LX/0nC;Ljava/io/Writer;LX/0zL;)V
    .locals 4

    .prologue
    .line 134878
    invoke-virtual {p2}, LX/0zL;->j()I

    move-result v1

    .line 134879
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 134880
    if-lez v0, :cond_0

    .line 134881
    const/16 v2, 0x2c

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(I)V

    .line 134882
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v0}, LX/0zL;->b(I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, p1, v2, v3}, LX/0nC;->a(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134883
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134884
    :cond_1
    return-void
.end method

.method private c(Ljava/io/Writer;LX/0nA;)V
    .locals 5

    .prologue
    const/16 v3, 0x22

    .line 134900
    const-class v0, LX/0nC;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/0nA;->b(Ljava/lang/Class;I)I

    move-result v0

    .line 134901
    invoke-virtual {p2, p0}, LX/0nA;->b(LX/0nD;)LX/0nD;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 134902
    const-class v2, LX/0nC;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 134903
    invoke-virtual {p2, p1, p0}, LX/0nA;->a(Ljava/io/Writer;LX/0nD;)V

    .line 134904
    :goto_0
    return-void

    .line 134905
    :cond_0
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    .line 134906
    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(I)V

    .line 134907
    new-instance v0, LX/10H;

    invoke-direct {v0, p1}, LX/10H;-><init>(Ljava/io/Writer;)V

    .line 134908
    :try_start_0
    invoke-virtual {p2, v0, p0}, LX/0nA;->a(Ljava/io/Writer;LX/0nD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134909
    invoke-virtual {v0}, LX/10H;->flush()V

    .line 134910
    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    .line 134911
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/10H;->flush()V

    throw v1

    .line 134912
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported encoder="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", flags="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " combination"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public final a(Ljava/io/Writer;LX/0nA;)V
    .locals 1

    .prologue
    .line 134889
    instance-of v0, p2, LX/0n9;

    if-eqz v0, :cond_0

    .line 134890
    check-cast p2, LX/0n9;

    .line 134891
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 134892
    invoke-static {p0, p1, p2}, LX/0nC;->b(LX/0nC;Ljava/io/Writer;LX/0n9;)V

    .line 134893
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 134894
    :goto_0
    return-void

    .line 134895
    :cond_0
    check-cast p2, LX/0zL;

    .line 134896
    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 134897
    invoke-static {p0, p1, p2}, LX/0nC;->b(LX/0nC;Ljava/io/Writer;LX/0zL;)V

    .line 134898
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 134899
    goto :goto_0
.end method

.method public final b(Ljava/io/Writer;LX/0nA;)V
    .locals 1

    .prologue
    .line 134885
    instance-of v0, p2, LX/0n9;

    if-eqz v0, :cond_0

    .line 134886
    check-cast p2, LX/0n9;

    invoke-static {p0, p1, p2}, LX/0nC;->b(LX/0nC;Ljava/io/Writer;LX/0n9;)V

    .line 134887
    :goto_0
    return-void

    .line 134888
    :cond_0
    check-cast p2, LX/0zL;

    invoke-static {p0, p1, p2}, LX/0nC;->b(LX/0nC;Ljava/io/Writer;LX/0zL;)V

    goto :goto_0
.end method
