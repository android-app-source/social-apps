.class public LX/0tx;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/contextual/ContextualConfigFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0ud;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155818
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0ud;
    .locals 4

    .prologue
    .line 155819
    sget-object v0, LX/0tx;->a:LX/0ud;

    if-nez v0, :cond_1

    .line 155820
    const-class v1, LX/0tx;

    monitor-enter v1

    .line 155821
    :try_start_0
    sget-object v0, LX/0tx;->a:LX/0ud;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155822
    if-eqz v2, :cond_0

    .line 155823
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155824
    invoke-static {v0}, LX/0ty;->a(LX/0QB;)LX/0u0;

    move-result-object v3

    check-cast v3, LX/0u0;

    invoke-static {v0}, LX/0ua;->a(LX/0QB;)LX/0uc;

    move-result-object p0

    check-cast p0, LX/0uc;

    invoke-static {v3, p0}, LX/0tv;->a(LX/0u0;LX/0uc;)LX/0ud;

    move-result-object v3

    move-object v0, v3

    .line 155825
    sput-object v0, LX/0tx;->a:LX/0ud;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155826
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155827
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155828
    :cond_1
    sget-object v0, LX/0tx;->a:LX/0ud;

    return-object v0

    .line 155829
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155830
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155831
    invoke-static {p0}, LX/0ty;->a(LX/0QB;)LX/0u0;

    move-result-object v0

    check-cast v0, LX/0u0;

    invoke-static {p0}, LX/0ua;->a(LX/0QB;)LX/0uc;

    move-result-object v1

    check-cast v1, LX/0uc;

    invoke-static {v0, v1}, LX/0tv;->a(LX/0u0;LX/0uc;)LX/0ud;

    move-result-object v0

    return-object v0
.end method
