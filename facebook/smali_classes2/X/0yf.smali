.class public final LX/0yf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0yL;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0yL;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 165794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165795
    iput-object p1, p0, LX/0yf;->a:LX/0QB;

    .line 165796
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0yL;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 165797
    new-instance v0, LX/0yf;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0yf;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 165798
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 165799
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0yf;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 165800
    packed-switch p2, :pswitch_data_0

    .line 165801
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165802
    :pswitch_0
    invoke-static {p1}, LX/168;->a(LX/0QB;)LX/168;

    move-result-object v0

    .line 165803
    :goto_0
    return-object v0

    .line 165804
    :pswitch_1
    new-instance v1, LX/1Ye;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0x1aa1

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/1Ye;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;)V

    .line 165805
    move-object v0, v1

    .line 165806
    goto :goto_0

    .line 165807
    :pswitch_2
    invoke-static {p1}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 165808
    const/4 v0, 0x3

    return v0
.end method
