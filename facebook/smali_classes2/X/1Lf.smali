.class public LX/1Lf;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Lg;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Lg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 234044
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Lg;
    .locals 5

    .prologue
    .line 234045
    sget-object v0, LX/1Lf;->a:LX/1Lg;

    if-nez v0, :cond_1

    .line 234046
    const-class v1, LX/1Lf;

    monitor-enter v1

    .line 234047
    :try_start_0
    sget-object v0, LX/1Lf;->a:LX/1Lg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234048
    if-eqz v2, :cond_0

    .line 234049
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 234050
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v4, 0x135d

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x12e7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v3, v4, p0}, LX/19Y;->a(LX/03V;LX/0Ot;LX/0Ot;)LX/1Lg;

    move-result-object v3

    move-object v0, v3

    .line 234051
    sput-object v0, LX/1Lf;->a:LX/1Lg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234052
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234053
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234054
    :cond_1
    sget-object v0, LX/1Lf;->a:LX/1Lg;

    return-object v0

    .line 234055
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 234057
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    const/16 v1, 0x135d

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x12e7

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/19Y;->a(LX/03V;LX/0Ot;LX/0Ot;)LX/1Lg;

    move-result-object v0

    return-object v0
.end method
