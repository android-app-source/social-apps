.class public LX/1Lu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1Lu;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field public final b:LX/0ad;

.field public final c:LX/0So;

.field private final d:Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7PP;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0So;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 234435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234436
    new-instance v0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;-><init>(LX/1Lu;)V

    iput-object v0, p0, LX/1Lu;->d:Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;

    .line 234437
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Lu;->e:Ljava/util/List;

    .line 234438
    iput-object p1, p0, LX/1Lu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 234439
    iput-object p2, p0, LX/1Lu;->b:LX/0ad;

    .line 234440
    iput-object p3, p0, LX/1Lu;->c:LX/0So;

    .line 234441
    return-void
.end method

.method public static a(LX/0QB;)LX/1Lu;
    .locals 6

    .prologue
    .line 234408
    sget-object v0, LX/1Lu;->g:LX/1Lu;

    if-nez v0, :cond_1

    .line 234409
    const-class v1, LX/1Lu;

    monitor-enter v1

    .line 234410
    :try_start_0
    sget-object v0, LX/1Lu;->g:LX/1Lu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234411
    if-eqz v2, :cond_0

    .line 234412
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 234413
    new-instance p0, LX/1Lu;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/1Lu;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0So;)V

    .line 234414
    move-object v0, p0

    .line 234415
    sput-object v0, LX/1Lu;->g:LX/1Lu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234416
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234417
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234418
    :cond_1
    sget-object v0, LX/1Lu;->g:LX/1Lu;

    return-object v0

    .line 234419
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/1Lu;)V
    .locals 7

    .prologue
    .line 234431
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lu;->f:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 234432
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 234433
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1Lu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/1Lu;->d:Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/1Lu;->f:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/1Lu;)V
    .locals 2

    .prologue
    .line 234442
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Lu;->f:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 234443
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 234444
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1Lu;->f:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 234445
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Lu;->f:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234446
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 234424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lu;->b:LX/0ad;

    sget-short v1, LX/7Or;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 234425
    :goto_0
    monitor-exit p0

    return-object p1

    .line 234426
    :cond_0
    :try_start_1
    new-instance v0, LX/7PP;

    invoke-direct {v0, p0, p1}, LX/7PP;-><init>(LX/1Lu;Ljava/io/OutputStream;)V

    .line 234427
    iget-object v1, p0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234428
    invoke-static {p0}, LX/1Lu;->a(LX/1Lu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object p1, v0

    .line 234429
    goto :goto_0

    .line 234430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 234421
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234422
    monitor-exit p0

    return-void

    .line 234423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
