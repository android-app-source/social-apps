.class public LX/0yw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0yw;


# instance fields
.field private final a:LX/0yx;

.field public final b:LX/0yy;

.field public final c:LX/0z0;

.field private final d:LX/0z1;

.field private final e:LX/0z2;

.field private final f:LX/0SG;

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:LX/0z4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0z4",
            "<",
            "LX/6VO;",
            ">;"
        }
    .end annotation
.end field

.field private i:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0yx;LX/0yy;LX/0z0;LX/0z1;LX/0z2;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166474
    iput-object p1, p0, LX/0yw;->a:LX/0yx;

    .line 166475
    iput-object p2, p0, LX/0yw;->b:LX/0yy;

    .line 166476
    iput-object p3, p0, LX/0yw;->c:LX/0z0;

    .line 166477
    iput-object p4, p0, LX/0yw;->d:LX/0z1;

    .line 166478
    iput-object p5, p0, LX/0yw;->e:LX/0z2;

    .line 166479
    iput-object p6, p0, LX/0yw;->f:LX/0SG;

    .line 166480
    return-void
.end method

.method public static a(LX/0QB;)LX/0yw;
    .locals 10

    .prologue
    .line 166420
    sget-object v0, LX/0yw;->j:LX/0yw;

    if-nez v0, :cond_1

    .line 166421
    const-class v1, LX/0yw;

    monitor-enter v1

    .line 166422
    :try_start_0
    sget-object v0, LX/0yw;->j:LX/0yw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166423
    if-eqz v2, :cond_0

    .line 166424
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 166425
    new-instance v3, LX/0yw;

    invoke-static {v0}, LX/0yx;->a(LX/0QB;)LX/0yx;

    move-result-object v4

    check-cast v4, LX/0yx;

    .line 166426
    new-instance v5, LX/0yy;

    invoke-direct {v5}, LX/0yy;-><init>()V

    .line 166427
    move-object v5, v5

    .line 166428
    move-object v5, v5

    .line 166429
    check-cast v5, LX/0yy;

    .line 166430
    new-instance v6, LX/0z0;

    invoke-direct {v6}, LX/0z0;-><init>()V

    .line 166431
    move-object v6, v6

    .line 166432
    move-object v6, v6

    .line 166433
    check-cast v6, LX/0z0;

    const-class v7, LX/0z1;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0z1;

    const-class v8, LX/0z2;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/0z2;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-direct/range {v3 .. v9}, LX/0yw;-><init>(LX/0yx;LX/0yy;LX/0z0;LX/0z1;LX/0z2;LX/0SG;)V

    .line 166434
    move-object v0, v3

    .line 166435
    sput-object v0, LX/0yw;->j:LX/0yw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166436
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166437
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166438
    :cond_1
    sget-object v0, LX/0yw;->j:LX/0yw;

    return-object v0

    .line 166439
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;)V
    .locals 6

    .prologue
    .line 166446
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0yw;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 166447
    :goto_0
    monitor-exit p0

    return-void

    .line 166448
    :cond_0
    :try_start_1
    iget v0, p1, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventRecycleListSize:I

    move v0, v0

    .line 166449
    new-instance v1, LX/0z3;

    invoke-direct {v1, v0}, LX/0z3;-><init>(I)V

    .line 166450
    move-object v0, v1

    .line 166451
    iget-object v1, p0, LX/0yw;->e:LX/0z2;

    .line 166452
    iget v2, p1, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventQueueSize:I

    move v2, v2

    .line 166453
    iget v3, p1, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventAgeSec:I

    move v3, v3

    .line 166454
    new-instance v5, LX/0z4;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v4, v2, v3}, LX/0z4;-><init>(LX/0SG;II)V

    .line 166455
    move-object v1, v5

    .line 166456
    iput-object v1, p0, LX/0yw;->h:LX/0z4;

    .line 166457
    iget-object v1, p0, LX/0yw;->h:LX/0z4;

    .line 166458
    iput-object v0, v1, LX/0z4;->f:LX/0z3;

    .line 166459
    iget-object v1, p0, LX/0yw;->h:LX/0z4;

    new-instance v2, LX/0z5;

    invoke-direct {v2, p0}, LX/0z5;-><init>(LX/0yw;)V

    invoke-virtual {v1, v2}, LX/0z4;->a(LX/0yz;)V

    .line 166460
    iget-object v1, p0, LX/0yw;->h:LX/0z4;

    iget-object v2, p0, LX/0yw;->b:LX/0yy;

    invoke-virtual {v1, v2}, LX/0z4;->a(LX/0yz;)V

    .line 166461
    iget-object v1, p0, LX/0yw;->h:LX/0z4;

    iget-object v2, p0, LX/0yw;->c:LX/0z0;

    invoke-virtual {v1, v2}, LX/0z4;->a(LX/0yz;)V

    .line 166462
    iget-object v1, p0, LX/0yw;->c:LX/0z0;

    .line 166463
    iget v2, p1, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mVideoPlayCountThresholdSec:I

    move v2, v2

    .line 166464
    iput v2, v1, LX/0z0;->a:I

    .line 166465
    iget-object v1, p0, LX/0yw;->a:LX/0yx;

    .line 166466
    iput-object p1, v1, LX/0yx;->d:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    .line 166467
    iput-object v0, v1, LX/0yx;->e:LX/0z3;

    .line 166468
    iget-object v2, v1, LX/0yx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 166469
    iget-object v0, p0, LX/0yw;->a:LX/0yx;

    .line 166470
    iget-object v1, v0, LX/0yx;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 166471
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yw;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 166481
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0yw;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()J
    .locals 2

    .prologue
    .line 166445
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0yw;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 166441
    iget-object v0, p0, LX/0yw;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 166442
    monitor-enter p0

    .line 166443
    :try_start_0
    iput-wide v0, p0, LX/0yw;->i:J

    .line 166444
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
