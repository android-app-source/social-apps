.class public final LX/15y;
.super LX/12V;
.source ""


# instance fields
.field public final c:LX/15y;

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public g:LX/15y;


# direct methods
.method private constructor <init>(LX/15y;III)V
    .locals 1

    .prologue
    .line 183187
    invoke-direct {p0}, LX/12V;-><init>()V

    .line 183188
    const/4 v0, 0x0

    iput-object v0, p0, LX/15y;->g:LX/15y;

    .line 183189
    iput p2, p0, LX/15y;->a:I

    .line 183190
    iput-object p1, p0, LX/15y;->c:LX/15y;

    .line 183191
    iput p3, p0, LX/15y;->d:I

    .line 183192
    iput p4, p0, LX/15y;->e:I

    .line 183193
    const/4 v0, -0x1

    iput v0, p0, LX/15y;->b:I

    .line 183194
    return-void
.end method

.method public static a(II)LX/15y;
    .locals 3

    .prologue
    .line 183202
    new-instance v0, LX/15y;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0, p1}, LX/15y;-><init>(LX/15y;III)V

    return-object v0
.end method

.method private a(III)V
    .locals 1

    .prologue
    .line 183196
    iput p1, p0, LX/15y;->a:I

    .line 183197
    const/4 v0, -0x1

    iput v0, p0, LX/15y;->b:I

    .line 183198
    iput p2, p0, LX/15y;->d:I

    .line 183199
    iput p3, p0, LX/15y;->e:I

    .line 183200
    const/4 v0, 0x0

    iput-object v0, p0, LX/15y;->f:Ljava/lang/String;

    .line 183201
    return-void
.end method

.method public static i()LX/15y;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 183195
    new-instance v0, LX/15y;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v2, v3}, LX/15y;-><init>(LX/15y;III)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()LX/12V;
    .locals 1

    .prologue
    .line 183185
    iget-object v0, p0, LX/15y;->c:LX/15y;

    move-object v0, v0

    .line 183186
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)LX/28G;
    .locals 6

    .prologue
    .line 183203
    new-instance v0, LX/28G;

    const-wide/16 v2, -0x1

    iget v4, p0, LX/15y;->d:I

    iget v5, p0, LX/15y;->e:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/28G;-><init>(Ljava/lang/Object;JII)V

    return-object v0
.end method

.method public final b(II)LX/15y;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 183180
    iget-object v0, p0, LX/15y;->g:LX/15y;

    .line 183181
    if-nez v0, :cond_0

    .line 183182
    new-instance v0, LX/15y;

    invoke-direct {v0, p0, v1, p1, p2}, LX/15y;-><init>(LX/15y;III)V

    iput-object v0, p0, LX/15y;->g:LX/15y;

    .line 183183
    :goto_0
    return-object v0

    .line 183184
    :cond_0
    invoke-direct {v0, v1, p1, p2}, LX/15y;->a(III)V

    goto :goto_0
.end method

.method public final c(II)LX/15y;
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 183158
    iget-object v0, p0, LX/15y;->g:LX/15y;

    .line 183159
    if-nez v0, :cond_0

    .line 183160
    new-instance v0, LX/15y;

    invoke-direct {v0, p0, v1, p1, p2}, LX/15y;-><init>(LX/15y;III)V

    iput-object v0, p0, LX/15y;->g:LX/15y;

    .line 183161
    :goto_0
    return-object v0

    .line 183162
    :cond_0
    invoke-direct {v0, v1, p1, p2}, LX/15y;->a(III)V

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183179
    iget-object v0, p0, LX/15y;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 183177
    iget v0, p0, LX/12V;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15y;->b:I

    .line 183178
    iget v1, p0, LX/12V;->a:I

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x22

    .line 183163
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 183164
    iget v1, p0, LX/12V;->a:I

    packed-switch v1, :pswitch_data_0

    .line 183165
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 183166
    :pswitch_0
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 183167
    :pswitch_1
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183168
    invoke-virtual {p0}, LX/12V;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 183169
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 183170
    :pswitch_2
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183171
    iget-object v1, p0, LX/15y;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 183172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183173
    iget-object v1, p0, LX/15y;->f:Ljava/lang/String;

    invoke-static {v0, v1}, LX/12H;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 183174
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183175
    :goto_1
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 183176
    :cond_0
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
