.class public LX/1Cc;
.super LX/1Cd;
.source ""

# interfaces
.implements LX/1Ce;
.implements LX/0hk;
.implements LX/1Cf;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public A:I

.field public B:Z

.field public C:Z

.field private final a:Ljava/lang/Runnable;

.field private final b:Ljava/lang/Runnable;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1DS;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1Ck;

.field public final i:LX/1Cg;

.field public final j:Landroid/os/Handler;

.field public final k:LX/1Cj;

.field private final l:LX/1Ci;

.field public m:LX/1Qq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:LX/1Cn;

.field private final o:LX/0SG;

.field private final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final q:LX/0ad;

.field public r:Z

.field public s:Z

.field private t:Z

.field private u:J

.field private v:Z

.field public w:Ljava/lang/String;

.field public final x:LX/1Co;

.field private final y:LX/1Cp;

.field private final z:LX/0qb;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1Cg;LX/1Ci;LX/1Cj;LX/1Cn;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ck;LX/0ad;LX/0Sg;LX/1Co;LX/1Cp;LX/0qb;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1DS;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;",
            ">;",
            "LX/1Cg;",
            "LX/1Ci;",
            "LX/1Cj;",
            "LX/1Cn;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1Ck;",
            "LX/0ad;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/1Co;",
            "LX/1Cp;",
            "LX/0qb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216641
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 216642
    new-instance v2, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$1;

    invoke-direct {v2, p0}, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$1;-><init>(LX/1Cc;)V

    iput-object v2, p0, LX/1Cc;->a:Ljava/lang/Runnable;

    .line 216643
    new-instance v2, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$2;

    invoke-direct {v2, p0}, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$2;-><init>(LX/1Cc;)V

    iput-object v2, p0, LX/1Cc;->b:Ljava/lang/Runnable;

    .line 216644
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, LX/1Cc;->j:Landroid/os/Handler;

    .line 216645
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/1Cc;->r:Z

    .line 216646
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/1Cc;->s:Z

    .line 216647
    const/4 v2, 0x0

    iput v2, p0, LX/1Cc;->A:I

    .line 216648
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/1Cc;->B:Z

    .line 216649
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/1Cc;->C:Z

    .line 216650
    iput-object p1, p0, LX/1Cc;->c:LX/0Ot;

    .line 216651
    iput-object p2, p0, LX/1Cc;->d:LX/0Ot;

    .line 216652
    iput-object p9, p0, LX/1Cc;->h:LX/1Ck;

    .line 216653
    iput-object p3, p0, LX/1Cc;->i:LX/1Cg;

    .line 216654
    iput-object p5, p0, LX/1Cc;->k:LX/1Cj;

    .line 216655
    iput-object p4, p0, LX/1Cc;->l:LX/1Ci;

    .line 216656
    iput-object p6, p0, LX/1Cc;->n:LX/1Cn;

    .line 216657
    iput-object p7, p0, LX/1Cc;->o:LX/0SG;

    .line 216658
    iput-object p8, p0, LX/1Cc;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 216659
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1Cc;->q:LX/0ad;

    .line 216660
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1Cc;->x:LX/1Co;

    .line 216661
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Cc;->y:LX/1Cp;

    .line 216662
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Cc;->z:LX/0qb;

    .line 216663
    new-instance v2, LX/1Cq;

    invoke-direct {v2}, LX/1Cq;-><init>()V

    iput-object v2, p0, LX/1Cc;->e:LX/1Cq;

    .line 216664
    new-instance v2, LX/1Cq;

    invoke-direct {v2}, LX/1Cq;-><init>()V

    iput-object v2, p0, LX/1Cc;->f:LX/1Cq;

    .line 216665
    new-instance v2, LX/1Cq;

    invoke-direct {v2}, LX/1Cq;-><init>()V

    iput-object v2, p0, LX/1Cc;->g:LX/1Cq;

    .line 216666
    iget-object v2, p0, LX/1Cc;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1Cr;->b:LX/0Tn;

    invoke-direct {p0}, LX/1Cc;->h()J

    move-result-wide v4

    neg-long v4, v4

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    iput-wide v2, p0, LX/1Cc;->u:J

    .line 216667
    const-string v2, "Holding off Good Morning fetch for coldstart"

    new-instance v3, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$3;

    invoke-direct {v3, p0}, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$3;-><init>(LX/1Cc;)V

    sget-object v4, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    sget-object v5, LX/0Vm;->UI:LX/0Vm;

    move-object/from16 v0, p11

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 216668
    return-void
.end method

.method public static a(LX/1Cc;LX/0gf;)V
    .locals 5

    .prologue
    .line 216632
    iget-object v0, p0, LX/1Cc;->o:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 216633
    iget-boolean v2, p0, LX/1Cc;->v:Z

    if-eqz v2, :cond_1

    .line 216634
    iget-wide v2, p0, LX/1Cc;->u:J

    sub-long/2addr v0, v2

    invoke-direct {p0}, LX/1Cc;->h()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    .line 216635
    iget-object v1, v0, LX/1Co;->a:LX/0Uh;

    const/16 v2, 0x4e3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 216636
    if-eqz v0, :cond_2

    sget-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-ne p1, v0, :cond_2

    .line 216637
    :cond_0
    iget-object v0, p0, LX/1Cc;->h:LX/1Ck;

    const-string v1, "FetchPinnedUnits"

    new-instance v2, LX/3it;

    invoke-direct {v2, p0, p1}, LX/3it;-><init>(LX/1Cc;LX/0gf;)V

    new-instance v3, LX/3iu;

    invoke-direct {v3, p0}, LX/3iu;-><init>(LX/1Cc;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 216638
    :cond_1
    :goto_0
    return-void

    .line 216639
    :cond_2
    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    invoke-virtual {v0}, LX/1Co;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216640
    invoke-direct {p0}, LX/1Cc;->e()V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Calendar;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v3, 0x12

    const/16 v2, 0xc

    .line 216630
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 216631
    const-string v1, "morning"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    if-lt v0, v2, :cond_2

    :cond_0
    const-string v1, "afternoon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-lt v0, v2, :cond_1

    if-lt v0, v3, :cond_2

    :cond_1
    const-string v1, "evening"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-lt v0, v3, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1Cc;
    .locals 15

    .prologue
    .line 216625
    new-instance v0, LX/1Cc;

    const/16 v1, 0x6be

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x897

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/1Cg;->b(LX/0QB;)LX/1Cg;

    move-result-object v3

    check-cast v3, LX/1Cg;

    const-class v4, LX/1Ci;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1Ci;

    .line 216626
    new-instance v8, LX/1Cj;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lB;

    invoke-direct {v8, v5, v6, v7}, LX/1Cj;-><init>(LX/0tX;LX/1Ck;LX/0lB;)V

    .line 216627
    move-object v5, v8

    .line 216628
    check-cast v5, LX/1Cj;

    invoke-static {p0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v6

    check-cast v6, LX/1Cn;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v11

    check-cast v11, LX/0Sg;

    invoke-static {p0}, LX/1Co;->b(LX/0QB;)LX/1Co;

    move-result-object v12

    check-cast v12, LX/1Co;

    invoke-static {p0}, LX/1Cp;->a(LX/0QB;)LX/1Cp;

    move-result-object v13

    check-cast v13, LX/1Cp;

    invoke-static {p0}, LX/0qb;->a(LX/0QB;)LX/0qb;

    move-result-object v14

    check-cast v14, LX/0qb;

    invoke-direct/range {v0 .. v14}, LX/1Cc;-><init>(LX/0Ot;LX/0Ot;LX/1Cg;LX/1Ci;LX/1Cj;LX/1Cn;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ck;LX/0ad;LX/0Sg;LX/1Co;LX/1Cp;LX/0qb;)V

    .line 216629
    return-object v0
.end method

.method public static b(LX/1Cc;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 216601
    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1Cc;->x:LX/1Co;

    invoke-virtual {v0}, LX/1Co;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 216602
    :goto_0
    if-eqz v0, :cond_4

    .line 216603
    iget-object v2, p0, LX/1Cc;->f:LX/1Cq;

    .line 216604
    iput-object p1, v2, LX/1Cq;->a:Ljava/lang/Object;

    .line 216605
    :goto_1
    iput-boolean v1, p0, LX/1Cc;->r:Z

    .line 216606
    iput-object p2, p0, LX/1Cc;->w:Ljava/lang/String;

    .line 216607
    iget-object v1, p0, LX/1Cc;->y:LX/1Cp;

    if-eqz v1, :cond_1

    .line 216608
    iget-object v1, p0, LX/1Cc;->y:LX/1Cp;

    const/4 v3, 0x0

    .line 216609
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 216610
    const-string v2, "daily_dialogue_lightweight_unit_id"

    const/4 p1, 0x0

    invoke-virtual {v4, v2, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 216611
    if-nez v2, :cond_0

    .line 216612
    const-string v2, "daily_dialogue_lightweight_unit_type"

    const/4 p1, 0x0

    invoke-virtual {v4, v2, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 216613
    :cond_0
    :goto_2
    move-object v2, v2

    .line 216614
    iput-object v2, v1, LX/1Cp;->a:Ljava/lang/String;

    .line 216615
    :cond_1
    if-eqz v0, :cond_5

    .line 216616
    invoke-direct {p0}, LX/1Cc;->e()V

    .line 216617
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v1

    .line 216618
    goto :goto_0

    .line 216619
    :cond_4
    iget-object v2, p0, LX/1Cc;->e:LX/1Cq;

    .line 216620
    iput-object p1, v2, LX/1Cq;->a:Ljava/lang/Object;

    .line 216621
    goto :goto_1

    .line 216622
    :cond_5
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 216623
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_3

    .line 216624
    :catch_0
    move-object v2, v3

    goto :goto_2
.end method

.method private c(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 216588
    instance-of v2, p1, LX/1Rk;

    if-nez v2, :cond_1

    .line 216589
    :cond_0
    :goto_0
    return-void

    .line 216590
    :cond_1
    check-cast p1, LX/1Rk;

    invoke-virtual {p1}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 216591
    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    if-eqz v2, :cond_0

    .line 216592
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    move-object v0, v3

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 216593
    const-string v2, "daily_dialogue_lightweight_unit_type"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "daily_dialogue_lightweight_unit_type"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "SIMPLE_GREETING"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move-object v2, v3

    .line 216594
    check-cast v2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, v3

    check-cast v2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, v3

    check-cast v2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 216595
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    .line 216596
    iget-object v4, p0, LX/1Cc;->o:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 216597
    check-cast v3, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 216598
    invoke-static {v3, v2}, LX/1Cc;->a(Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 216599
    iget-object v2, p0, LX/1Cc;->n:LX/1Cn;

    sget-object v3, LX/3lw;->GOODWILL_DD_GREETING_MISMATCH:LX/3lw;

    invoke-virtual {v2, v3}, LX/1Cn;->a(LX/3lw;)V

    goto/16 :goto_0

    .line 216600
    :catch_0
    goto/16 :goto_0
.end method

.method private e()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 216578
    iget-object v0, p0, LX/1Cc;->z:LX/0qb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cc;->y:LX/1Cp;

    if-eqz v0, :cond_0

    .line 216579
    iget-object v0, p0, LX/1Cc;->z:LX/0qb;

    iget-object v1, p0, LX/1Cc;->y:LX/1Cp;

    sget-object v2, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v0, v1, v2}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Cc;->f:LX/1Cq;

    invoke-virtual {v0}, LX/1Cq;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 216580
    iget-object v0, p0, LX/1Cc;->e:LX/1Cq;

    iget-object v1, p0, LX/1Cc;->f:LX/1Cq;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1Cq;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 216581
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 216582
    :goto_0
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216583
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 216584
    :cond_0
    return-void

    .line 216585
    :cond_1
    iget-object v0, p0, LX/1Cc;->e:LX/1Cq;

    const/4 v1, 0x0

    .line 216586
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 216587
    goto :goto_0
.end method

.method public static f(LX/1Cc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216565
    iget-object v0, p0, LX/1Cc;->y:LX/1Cp;

    if-eqz v0, :cond_0

    .line 216566
    iget-object v0, p0, LX/1Cc;->y:LX/1Cp;

    .line 216567
    iput-object v1, v0, LX/1Cp;->a:Ljava/lang/String;

    .line 216568
    :cond_0
    iget-object v0, p0, LX/1Cc;->e:LX/1Cq;

    invoke-virtual {v0}, LX/1Cq;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 216569
    iget-object v0, p0, LX/1Cc;->e:LX/1Cq;

    .line 216570
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 216571
    iget-object v0, p0, LX/1Cc;->f:LX/1Cq;

    .line 216572
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 216573
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    if-eqz v0, :cond_1

    .line 216574
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 216575
    :cond_1
    iget-object v0, p0, LX/1Cc;->j:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216576
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Cc;->r:Z

    .line 216577
    return-void
.end method

.method private h()J
    .locals 3

    .prologue
    .line 216516
    iget-object v0, p0, LX/1Cc;->q:LX/0ad;

    sget v1, LX/1Cs;->b:F

    const v2, 0x47a8c000    # 86400.0f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    .line 216517
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 216550
    iget-object v0, p0, LX/1Cc;->o:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Cc;->u:J

    .line 216551
    iget-object v0, p0, LX/1Cc;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1Cr;->b:LX/0Tn;

    iget-wide v2, p0, LX/1Cc;->u:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 216552
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 216553
    const/4 v0, 0x0

    .line 216554
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 216555
    iget-object v2, p0, LX/1Cc;->x:LX/1Co;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/1Cc;->x:LX/1Co;

    invoke-virtual {v2}, LX/1Co;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "daily_dialogue_lightweight_unit_type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "daily_dialogue_lightweight_unit_type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "lw_ptr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 216556
    const/4 v0, 0x1

    .line 216557
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 216558
    iget-object v0, p0, LX/1Cc;->g:LX/1Cq;

    .line 216559
    iput-object p1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 216560
    iget-boolean v0, p0, LX/1Cc;->B:Z

    if-nez v0, :cond_1

    .line 216561
    :goto_1
    return-void

    .line 216562
    :catch_0
    iget-object v1, p0, LX/1Cc;->n:LX/1Cn;

    sget-object v2, LX/3lw;->GOODWILL_PTR_UNIT_DECODE_FAILED:LX/3lw;

    invoke-virtual {v1, v2}, LX/1Cn;->a(LX/3lw;)V

    goto :goto_0

    .line 216563
    :cond_1
    invoke-static {p0, p1, p2}, LX/1Cc;->b(LX/1Cc;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    goto :goto_1

    .line 216564
    :cond_2
    invoke-static {p0}, LX/1Cc;->f(LX/1Cc;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 216541
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 216542
    iget-boolean v0, p0, LX/1Cc;->r:Z

    if-nez v0, :cond_0

    .line 216543
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Cc;->s:Z

    .line 216544
    iget-object v0, p0, LX/1Cc;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$5;

    invoke-direct {v1, p0}, Lcom/facebook/goodwill/dailydialogue/adapter/DailyDialogueInjectedFeedAdapter$5;-><init>(LX/1Cc;)V

    .line 216545
    iget-object v5, p0, LX/1Cc;->q:LX/0ad;

    sget v6, LX/1Cs;->c:F

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-interface {v5, v6, v7}, LX/0ad;->a(FF)F

    move-result v5

    .line 216546
    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v6

    float-to-long v5, v5

    move-wide v2, v5

    .line 216547
    const v4, -0x2dfef9da

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 216548
    invoke-direct {p0, p1}, LX/1Cc;->c(Ljava/lang/Object;)V

    .line 216549
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 216537
    iget-boolean v0, p0, LX/1Cc;->v:Z

    if-nez v0, :cond_0

    .line 216538
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Cc;->v:Z

    .line 216539
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1Cc;->a(LX/1Cc;LX/0gf;)V

    .line 216540
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216533
    iput-boolean v1, p0, LX/1Cc;->s:Z

    .line 216534
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    invoke-interface {v0, v1}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 216535
    iget-object v0, p0, LX/1Cc;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216536
    :cond_0
    return-void
.end method

.method public final c(LX/0g8;)LX/1Cw;
    .locals 10

    .prologue
    .line 216525
    iget-object v0, p0, LX/1Cc;->l:LX/1Ci;

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v1

    iget-object v2, p0, LX/1Cc;->a:Ljava/lang/Runnable;

    iget-object v3, p0, LX/1Cc;->b:Ljava/lang/Runnable;

    .line 216526
    new-instance v4, LX/1SU;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    const-class v6, LX/1SV;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1SV;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, LX/1SU;-><init>(Landroid/content/Context;LX/1SV;LX/1PY;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 216527
    move-object v1, v4

    .line 216528
    iget-object v0, p0, LX/1Cc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1DS;

    iget-object v2, p0, LX/1Cc;->d:LX/0Ot;

    iget-object v3, p0, LX/1Cc;->e:LX/1Cq;

    invoke-virtual {v0, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 216529
    iput-object v1, v0, LX/1Ql;->f:LX/1PW;

    .line 216530
    move-object v0, v0

    .line 216531
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/1Cc;->m:LX/1Qq;

    .line 216532
    iget-object v0, p0, LX/1Cc;->m:LX/1Qq;

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 216521
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Cc;->t:Z

    .line 216522
    iget-object v0, p0, LX/1Cc;->z:LX/0qb;

    invoke-virtual {v0}, LX/0qb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cc;->z:LX/0qb;

    iget-object v1, p0, LX/1Cc;->y:LX/1Cp;

    sget-object v2, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v0, v1, v2}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Cc;->e:LX/1Cq;

    invoke-virtual {v0}, LX/1Cq;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 216523
    invoke-static {p0}, LX/1Cc;->f(LX/1Cc;)V

    .line 216524
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 216518
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Cc;->t:Z

    .line 216519
    iget-object v0, p0, LX/1Cc;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216520
    return-void
.end method
