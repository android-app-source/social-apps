.class public LX/1ML;
.super LX/0SQ;
.source ""

# interfaces
.implements LX/1MM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0SQ",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;",
        "LX/1MM",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 235184
    invoke-direct {p0}, LX/0SQ;-><init>()V

    return-void
.end method

.method public static immediateFailedFuture(Ljava/lang/Throwable;)LX/1ML;
    .locals 1

    .prologue
    .line 235190
    new-instance v0, LX/1ML;

    invoke-direct {v0}, LX/1ML;-><init>()V

    .line 235191
    invoke-virtual {v0, p0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    .line 235192
    return-object v0
.end method

.method public static immediateFuture(Lcom/facebook/fbservice/service/OperationResult;)LX/1ML;
    .locals 1

    .prologue
    .line 235187
    new-instance v0, LX/1ML;

    invoke-direct {v0}, LX/1ML;-><init>()V

    .line 235188
    invoke-virtual {v0, p0}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 235189
    return-object v0
.end method


# virtual methods
.method public cancelOperation()Z
    .locals 1

    .prologue
    .line 235186
    const/4 v0, 0x0

    return v0
.end method

.method public updatePriority(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0

    .prologue
    .line 235185
    return-void
.end method
