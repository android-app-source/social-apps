.class public LX/1Q4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pi;


# static fields
.field public static final a:[I


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/22C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 244287
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, LX/1Q4;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244344
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Q4;->b:Ljava/util/Set;

    .line 244345
    return-void
.end method

.method public static a(LX/0QB;)LX/1Q4;
    .locals 1

    .prologue
    .line 244340
    new-instance v0, LX/1Q4;

    invoke-direct {v0}, LX/1Q4;-><init>()V

    .line 244341
    move-object v0, v0

    .line 244342
    return-object v0
.end method

.method public static b(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 244338
    sget-object v1, LX/1Q4;->a:[I

    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 244339
    sget-object v1, LX/1Q4;->a:[I

    aget v1, v1, v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    if-ge v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/view/View;)LX/0g8;
    .locals 2

    .prologue
    .line 244330
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 244331
    :goto_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 244332
    check-cast v0, Landroid/view/View;

    .line 244333
    invoke-static {v0}, LX/62E;->a(Landroid/view/View;)LX/0g8;

    move-result-object v1

    .line 244334
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 244335
    :goto_1
    return-object v0

    .line 244336
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 244337
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 244328
    iget-object v0, p0, LX/1Q4;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 244329
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 5

    .prologue
    .line 244290
    iget-object v0, p0, LX/1Q4;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22C;

    .line 244291
    iget-object v1, v0, LX/22C;->b:Landroid/view/View;

    move-object v1, v1

    .line 244292
    instance-of v1, v1, LX/1dh;

    if-eqz v1, :cond_0

    .line 244293
    iget-object v1, v0, LX/22C;->b:Landroid/view/View;

    move-object v1, v1

    .line 244294
    check-cast v1, LX/1dh;

    .line 244295
    invoke-interface {v1}, LX/1dh;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/22C;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {p1, v2}, LX/34p;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, LX/1Q4;->b(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, LX/1Q4;->d(Landroid/view/View;)LX/0g8;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 244296
    :cond_1
    :goto_0
    move-object v1, v0

    .line 244297
    if-eqz v1, :cond_3

    .line 244298
    iget-object v0, v1, LX/22C;->b:Landroid/view/View;

    move-object v0, v0

    .line 244299
    const/4 v2, 0x1

    .line 244300
    sget-object v3, LX/1Q4;->a:[I

    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 244301
    invoke-static {v0}, LX/1Q4;->d(Landroid/view/View;)LX/0g8;

    move-result-object v3

    .line 244302
    sget-object p0, LX/1Q4;->a:[I

    aget p0, p0, v2

    if-lez p0, :cond_2

    if-eqz v3, :cond_8

    invoke-interface {v3}, LX/0g8;->f()Z

    move-result v3

    if-nez v3, :cond_8

    :cond_2
    :goto_1
    move v0, v2

    .line 244303
    if-eqz v0, :cond_4

    .line 244304
    :cond_3
    :goto_2
    return-void

    .line 244305
    :cond_4
    iget-object v0, v1, LX/22C;->b:Landroid/view/View;

    move-object v0, v0

    .line 244306
    invoke-static {v0}, LX/1Q4;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 244307
    iget-object v0, v1, LX/22C;->b:Landroid/view/View;

    move-object v0, v0

    .line 244308
    invoke-static {v0}, LX/1Q4;->d(Landroid/view/View;)LX/0g8;

    move-result-object v2

    .line 244309
    if-eqz v2, :cond_5

    .line 244310
    invoke-interface {v2, v0}, LX/0g8;->c(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 p0, 0x40a00000    # 5.0f

    invoke-static {v4, p0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0g8;->d(II)V

    .line 244311
    :cond_5
    iget-object v0, v1, LX/22C;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 244312
    iget-object v2, v1, LX/22C;->b:Landroid/view/View;

    move-object v2, v2

    .line 244313
    new-instance v3, LX/0hs;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 p0, 0x2

    invoke-direct {v3, v4, p0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 244314
    invoke-interface {p1}, LX/34p;->jy_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 244315
    invoke-interface {p1}, LX/34p;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 244316
    sget-object v4, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v4}, LX/0ht;->a(LX/3AV;)V

    .line 244317
    const/4 v4, -0x1

    .line 244318
    iput v4, v3, LX/0hs;->t:I

    .line 244319
    new-instance v4, LX/99O;

    invoke-direct {v4, p1, v0}, LX/99O;-><init>(LX/34p;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v3, v4}, LX/0hs;->a(LX/5Od;)V

    .line 244320
    invoke-static {v2}, LX/1Q4;->d(Landroid/view/View;)LX/0g8;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 244321
    invoke-static {v2}, LX/1Q4;->d(Landroid/view/View;)LX/0g8;

    move-result-object v4

    .line 244322
    new-instance p0, LX/99P;

    invoke-direct {p0, v3}, LX/99P;-><init>(LX/0hs;)V

    invoke-interface {v4, p0}, LX/0g8;->b(LX/0fx;)V

    .line 244323
    :cond_6
    move-object v2, v3

    .line 244324
    invoke-interface {p1, v2}, LX/34p;->a(LX/0hs;)V

    .line 244325
    iget-object v0, v1, LX/22C;->b:Landroid/view/View;

    move-object v0, v0

    .line 244326
    check-cast v0, LX/1dh;

    invoke-interface {v0, v2}, LX/1dh;->a(LX/0hs;)V

    .line 244327
    iget-object v0, v1, LX/22C;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {p1, v0}, LX/34p;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 244288
    iget-object v0, p0, LX/1Q4;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 244289
    return-void
.end method
