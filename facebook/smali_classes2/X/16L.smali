.class public final enum LX/16L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/16L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/16L;

.field public static final enum BIG_DECIMAL:LX/16L;

.field public static final enum BIG_INTEGER:LX/16L;

.field public static final enum DOUBLE:LX/16L;

.field public static final enum FLOAT:LX/16L;

.field public static final enum INT:LX/16L;

.field public static final enum LONG:LX/16L;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 184936
    new-instance v0, LX/16L;

    const-string v1, "INT"

    invoke-direct {v0, v1, v3}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->INT:LX/16L;

    new-instance v0, LX/16L;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v4}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->LONG:LX/16L;

    new-instance v0, LX/16L;

    const-string v1, "BIG_INTEGER"

    invoke-direct {v0, v1, v5}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->BIG_INTEGER:LX/16L;

    new-instance v0, LX/16L;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v6}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->FLOAT:LX/16L;

    new-instance v0, LX/16L;

    const-string v1, "DOUBLE"

    invoke-direct {v0, v1, v7}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->DOUBLE:LX/16L;

    new-instance v0, LX/16L;

    const-string v1, "BIG_DECIMAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/16L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/16L;->BIG_DECIMAL:LX/16L;

    .line 184937
    const/4 v0, 0x6

    new-array v0, v0, [LX/16L;

    sget-object v1, LX/16L;->INT:LX/16L;

    aput-object v1, v0, v3

    sget-object v1, LX/16L;->LONG:LX/16L;

    aput-object v1, v0, v4

    sget-object v1, LX/16L;->BIG_INTEGER:LX/16L;

    aput-object v1, v0, v5

    sget-object v1, LX/16L;->FLOAT:LX/16L;

    aput-object v1, v0, v6

    sget-object v1, LX/16L;->DOUBLE:LX/16L;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/16L;->BIG_DECIMAL:LX/16L;

    aput-object v2, v0, v1

    sput-object v0, LX/16L;->$VALUES:[LX/16L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 184939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/16L;
    .locals 1

    .prologue
    .line 184940
    const-class v0, LX/16L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/16L;

    return-object v0
.end method

.method public static values()[LX/16L;
    .locals 1

    .prologue
    .line 184938
    sget-object v0, LX/16L;->$VALUES:[LX/16L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/16L;

    return-object v0
.end method
