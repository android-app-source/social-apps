.class public LX/1Rk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/graphql/model/HasFeedEdge;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public final a:LX/1RA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1RA",
            "<TT;*>;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:J

.field public final d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

.field public final e:LX/0jW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Z


# direct methods
.method public constructor <init>(LX/1RA;IJLcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/0jW;Z)V
    .locals 1
    .param p5    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RA",
            "<TT;*>;IJ",
            "Lcom/facebook/graphql/model/FeedEdge;",
            "LX/0jW;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 246725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246726
    iput-object p1, p0, LX/1Rk;->a:LX/1RA;

    .line 246727
    iput p2, p0, LX/1Rk;->b:I

    .line 246728
    iput-wide p3, p0, LX/1Rk;->c:J

    .line 246729
    iput-object p5, p0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 246730
    iput-object p6, p0, LX/1Rk;->e:LX/0jW;

    .line 246731
    iput-boolean p7, p0, LX/1Rk;->f:Z

    .line 246732
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 246733
    iget-object v0, p0, LX/1Rk;->e:LX/0jW;

    instance-of v0, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    .line 246734
    iget-object v0, p0, LX/1Rk;->e:LX/0jW;

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 246735
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/1Rb;
    .locals 2

    .prologue
    .line 246736
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    iget v1, p0, LX/1Rk;->b:I

    invoke-virtual {v0, v1}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246737
    instance-of v0, p1, LX/1Rk;

    if-nez v0, :cond_1

    .line 246738
    :cond_0
    :goto_0
    return v2

    .line 246739
    :cond_1
    check-cast p1, LX/1Rk;

    .line 246740
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 246741
    iget-object v3, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v3

    .line 246742
    iget-object v3, p0, LX/1Rk;->a:LX/1RA;

    .line 246743
    iget-object v4, v3, LX/1RA;->b:Ljava/lang/Object;

    move-object v3, v4

    .line 246744
    if-ne v0, v3, :cond_2

    move v0, v1

    .line 246745
    :goto_1
    iget-boolean v3, p0, LX/1Rk;->f:Z

    if-eqz v3, :cond_3

    .line 246746
    if-eqz v0, :cond_0

    iget v0, p1, LX/1Rk;->b:I

    iget v3, p0, LX/1Rk;->b:I

    if-ne v0, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 246747
    goto :goto_1

    :cond_3
    move v2, v0

    .line 246748
    goto :goto_0
.end method

.method public final f()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 246749
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 246750
    iget-object p0, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 246751
    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 246752
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 246753
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 246754
    if-nez v0, :cond_0

    .line 246755
    const/4 v0, -0x1

    .line 246756
    :goto_0
    return v0

    .line 246757
    :cond_0
    iget-boolean v0, p0, LX/1Rk;->f:Z

    if-eqz v0, :cond_1

    .line 246758
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 246759
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 246760
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/1Rk;->b:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 246761
    :cond_1
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 246762
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 246763
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
