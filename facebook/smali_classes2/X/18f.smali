.class public abstract LX/18f;
.super LX/0Xt;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Xt",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final transient b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;+",
            "LX/0Py",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final transient c:I


# direct methods
.method public constructor <init>(LX/0P1;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;+",
            "LX/0Py",
            "<TV;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 206903
    invoke-direct {p0}, LX/0Xt;-><init>()V

    .line 206904
    iput-object p1, p0, LX/18f;->b:LX/0P1;

    .line 206905
    iput p2, p0, LX/18f;->c:I

    .line 206906
    return-void
.end method

.method public static c(LX/0Xu;)LX/18f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Xu",
            "<+TK;+TV;>;)",
            "LX/18f",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 206907
    instance-of v0, p0, LX/18f;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 206908
    check-cast v0, LX/18f;

    .line 206909
    invoke-virtual {v0}, LX/18f;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 206910
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/3CE;->b(LX/0Xu;)LX/3CE;

    move-result-object v0

    goto :goto_0
.end method

.method public static c()LX/2Q8;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/2Q8",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 206911
    new-instance v0, LX/2Q8;

    invoke-direct {v0}, LX/2Q8;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Xu;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<+TK;+TV;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206912
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Iterable",
            "<+TV;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206913
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206914
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 206915
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    move-object v0, v0

    .line 206916
    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 206923
    invoke-virtual {p0, p1}, LX/18f;->h(Ljava/lang/Object;)LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206917
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206918
    invoke-virtual {p0, p1}, LX/18f;->i(Ljava/lang/Object;)LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 206919
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final e()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 206920
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 206921
    iget v0, p0, LX/18f;->c:I

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 206922
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206886
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final g(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 206902
    if-eqz p1, :cond_0

    invoke-super {p0, p1}, LX/0Xt;->g(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract h(Ljava/lang/Object;)LX/0Py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation
.end method

.method public i(Ljava/lang/Object;)LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206887
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic i()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 206888
    invoke-virtual {p0}, LX/18f;->x()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 206889
    invoke-virtual {p0}, LX/18f;->y()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 206890
    invoke-virtual {p0}, LX/18f;->v()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 206891
    invoke-virtual {p0}, LX/18f;->w()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 206892
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final o()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 206893
    new-instance v0, LX/4yM;

    invoke-direct {v0, p0}, LX/4yM;-><init>(LX/18f;)V

    return-object v0
.end method

.method public final synthetic p()Ljava/util/Set;
    .locals 1

    .prologue
    .line 206894
    invoke-virtual {p0}, LX/18f;->e()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/1M1;
    .locals 1

    .prologue
    .line 206895
    invoke-super {p0}, LX/0Xt;->q()LX/1M1;

    move-result-object v0

    check-cast v0, LX/4yO;

    return-object v0
.end method

.method public final r()LX/1M1;
    .locals 1

    .prologue
    .line 206896
    new-instance v0, LX/4yP;

    invoke-direct {v0, p0}, LX/4yP;-><init>(LX/18f;)V

    return-object v0
.end method

.method public final s()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 206897
    new-instance v0, LX/4yQ;

    invoke-direct {v0, p0}, LX/4yQ;-><init>(LX/18f;)V

    return-object v0
.end method

.method public v()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 206898
    invoke-super {p0}, LX/0Xt;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, LX/0Py;

    return-object v0
.end method

.method public final w()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 206899
    new-instance v0, LX/4yK;

    invoke-direct {v0, p0}, LX/4yK;-><init>(LX/18f;)V

    return-object v0
.end method

.method public final x()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 206900
    invoke-super {p0}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, LX/0Py;

    return-object v0
.end method

.method public final y()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 206901
    new-instance v0, LX/4yL;

    invoke-direct {v0, p0}, LX/4yL;-><init>(LX/18f;)V

    return-object v0
.end method
