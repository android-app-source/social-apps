.class public LX/1Hw;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 227954
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 227955
    return-void
.end method

.method public static a(LX/1Hn;)LX/1Hz;
    .locals 2
    .annotation build Lcom/facebook/crypto/module/FixedKey;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 227956
    new-instance v0, LX/1Hc;

    sget-object v1, LX/1Hy;->KEY_128:LX/1Hy;

    invoke-direct {v0, v1}, LX/1Hc;-><init>(LX/1Hy;)V

    .line 227957
    invoke-virtual {p0, v0}, LX/1Ho;->b(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/1Hn;)LX/1Hz;
    .locals 2
    .annotation build Lcom/facebook/crypto/module/SharedPrefsKey;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 227958
    new-instance v0, LX/1Hx;

    sget-object v1, LX/1Hy;->KEY_128:LX/1Hy;

    invoke-direct {v0, p0, v1}, LX/1Hx;-><init>(Landroid/content/Context;LX/1Hy;)V

    .line 227959
    invoke-virtual {p1, v0}, LX/1Ho;->b(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1Hn;LX/0WP;LX/03V;LX/1BA;LX/0Or;)LX/1IW;
    .locals 6
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Hn;",
            "LX/0WP;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1IW;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 227960
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227961
    new-instance v2, LX/1I5;

    invoke-direct {v2, p1, p0, v0, p2}, LX/1I5;-><init>(LX/0WP;LX/1Ho;Ljava/lang/String;LX/03V;)V

    .line 227962
    invoke-virtual {p0, v2}, LX/1Ho;->c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v1

    .line 227963
    new-instance v0, LX/1IW;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/1IW;-><init>(LX/1Hz;LX/1I5;LX/03V;LX/1BA;LX/0Or;)V

    return-object v0
.end method

.method public static b(LX/1Hn;)LX/1Hz;
    .locals 2
    .annotation build Lcom/facebook/crypto/module/FixedKey256;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 227964
    new-instance v0, LX/1Hc;

    sget-object v1, LX/1Hy;->KEY_256:LX/1Hy;

    invoke-direct {v0, v1}, LX/1Hc;-><init>(LX/1Hy;)V

    .line 227965
    invoke-virtual {p0, v0}, LX/1Ho;->c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 227966
    return-void
.end method
