.class public LX/19k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field public l:I


# direct methods
.method public constructor <init>(ZZIIIIIIIII)V
    .locals 0

    .prologue
    .line 208512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208513
    iput-boolean p1, p0, LX/19k;->a:Z

    .line 208514
    iput-boolean p2, p0, LX/19k;->b:Z

    .line 208515
    iput p3, p0, LX/19k;->c:I

    .line 208516
    iput p4, p0, LX/19k;->d:I

    .line 208517
    iput p5, p0, LX/19k;->e:I

    .line 208518
    iput p6, p0, LX/19k;->f:I

    .line 208519
    iput p7, p0, LX/19k;->g:I

    .line 208520
    iput p8, p0, LX/19k;->h:I

    .line 208521
    iput p9, p0, LX/19k;->i:I

    .line 208522
    iput p10, p0, LX/19k;->j:I

    .line 208523
    iput p11, p0, LX/19k;->k:I

    .line 208524
    iput p11, p0, LX/19k;->l:I

    .line 208525
    return-void
.end method

.method public static a(LX/19k;LX/0p3;)Z
    .locals 4
    .param p0    # LX/19k;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 208502
    if-eqz p1, :cond_0

    iget-boolean v1, p0, LX/19k;->a:Z

    if-nez v1, :cond_1

    .line 208503
    :cond_0
    :goto_0
    return v0

    .line 208504
    :cond_1
    iget v1, p0, LX/19k;->l:I

    .line 208505
    sget-object v2, LX/2xk;->a:[I

    invoke-virtual {p1}, LX/0p3;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 208506
    iget v2, p0, LX/19k;->k:I

    iput v2, p0, LX/19k;->l:I

    .line 208507
    :goto_1
    iget v2, p0, LX/19k;->l:I

    if-eq v2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 208508
    :pswitch_0
    iget v2, p0, LX/19k;->c:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208509
    :pswitch_1
    iget v2, p0, LX/19k;->d:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208510
    :pswitch_2
    iget v2, p0, LX/19k;->e:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208511
    :pswitch_3
    iget v2, p0, LX/19k;->f:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/19k;LX/1jR;)Z
    .locals 4
    .param p0    # LX/19k;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 208492
    if-eqz p1, :cond_0

    iget-boolean v1, p0, LX/19k;->b:Z

    if-nez v1, :cond_1

    .line 208493
    :cond_0
    :goto_0
    return v0

    .line 208494
    :cond_1
    iget v1, p0, LX/19k;->l:I

    .line 208495
    sget-object v2, LX/2xk;->b:[I

    invoke-virtual {p1}, LX/1jR;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 208496
    iget v2, p0, LX/19k;->k:I

    iput v2, p0, LX/19k;->l:I

    .line 208497
    :goto_1
    iget v2, p0, LX/19k;->l:I

    if-eq v2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 208498
    :pswitch_0
    iget v2, p0, LX/19k;->g:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208499
    :pswitch_1
    iget v2, p0, LX/19k;->h:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208500
    :pswitch_2
    iget v2, p0, LX/19k;->i:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    .line 208501
    :pswitch_3
    iget v2, p0, LX/19k;->j:I

    iput v2, p0, LX/19k;->l:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 208491
    iget v0, p0, LX/19k;->l:I

    return v0
.end method
