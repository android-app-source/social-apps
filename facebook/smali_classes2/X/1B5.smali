.class public LX/1B5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile z:LX/1B5;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g4;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1BK;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Zb;

.field private final e:LX/1BA;

.field private final f:LX/17Q;

.field public final g:LX/0lB;

.field public final h:LX/03V;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0kL;

.field public final o:LX/0SG;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3E1;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0pe;

.field public final r:LX/0W3;

.field private s:Z

.field private t:Z

.field public u:J

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/logging/VpvWaterfallImpression;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field public x:J

.field public y:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0lB;LX/03V;LX/0Zb;LX/1BA;LX/17Q;LX/0Or;LX/0kL;LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/0Ot;LX/0pe;LX/0W3;LX/0Ot;LX/0Uh;)V
    .locals 4
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsVpvWaterfallLoggingEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsVpvWaterfallAddTrackingEnabled;
        .end annotation
    .end param
    .param p14    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1g4;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;",
            ">;",
            "LX/0lB;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0kL;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/3E1;",
            ">;",
            "LX/0pe;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/1BK;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 212239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212240
    iput-object p1, p0, LX/1B5;->a:LX/0Ot;

    .line 212241
    iput-object p2, p0, LX/1B5;->b:LX/0Ot;

    .line 212242
    iput-object p5, p0, LX/1B5;->d:LX/0Zb;

    .line 212243
    iput-object p6, p0, LX/1B5;->e:LX/1BA;

    .line 212244
    iput-object p7, p0, LX/1B5;->f:LX/17Q;

    .line 212245
    iput-object p8, p0, LX/1B5;->k:LX/0Or;

    .line 212246
    iput-object p9, p0, LX/1B5;->n:LX/0kL;

    .line 212247
    iput-object p10, p0, LX/1B5;->o:LX/0SG;

    .line 212248
    iget-object v2, p0, LX/1B5;->o:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/1B5;->u:J

    .line 212249
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/1B5;->v:Ljava/util/List;

    .line 212250
    iput-object p11, p0, LX/1B5;->l:LX/0Or;

    .line 212251
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1B5;->m:LX/0Or;

    .line 212252
    iput-object p3, p0, LX/1B5;->g:LX/0lB;

    .line 212253
    iput-object p4, p0, LX/1B5;->h:LX/03V;

    .line 212254
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1B5;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 212255
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1B5;->p:LX/0Ot;

    .line 212256
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1B5;->q:LX/0pe;

    .line 212257
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1B5;->j:Landroid/os/Handler;

    .line 212258
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1B5;->r:LX/0W3;

    .line 212259
    const/4 v2, -0x1

    iput v2, p0, LX/1B5;->w:I

    .line 212260
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/1B5;->x:J

    .line 212261
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1B5;->c:LX/0Ot;

    .line 212262
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1B5;->y:LX/0Uh;

    .line 212263
    invoke-virtual {p0}, LX/1B5;->a()V

    .line 212264
    return-void
.end method

.method public static a(LX/0QB;)LX/1B5;
    .locals 3

    .prologue
    .line 212229
    sget-object v0, LX/1B5;->z:LX/1B5;

    if-nez v0, :cond_1

    .line 212230
    const-class v1, LX/1B5;

    monitor-enter v1

    .line 212231
    :try_start_0
    sget-object v0, LX/1B5;->z:LX/1B5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212232
    if-eqz v2, :cond_0

    .line 212233
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1B5;->b(LX/0QB;)LX/1B5;

    move-result-object v0

    sput-object v0, LX/1B5;->z:LX/1B5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212234
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212235
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212236
    :cond_1
    sget-object v0, LX/1B5;->z:LX/1B5;

    return-object v0

    .line 212237
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1B5;LX/1g1;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)LX/1g1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1g1;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "J",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/1g1;"
        }
    .end annotation

    .prologue
    .line 212214
    iput-wide p3, p1, LX/1g1;->f:J

    .line 212215
    move-object v0, p1

    .line 212216
    iput-object p5, v0, LX/1g1;->k:Ljava/lang/String;

    .line 212217
    move-object v0, v0

    .line 212218
    iput-object p6, v0, LX/1g1;->r:LX/0P1;

    .line 212219
    move-object v0, v0

    .line 212220
    invoke-static {p2}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v1

    .line 212221
    iput v1, v0, LX/1g1;->n:I

    .line 212222
    move-object v0, v0

    .line 212223
    invoke-static {p2}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    .line 212224
    iput-object v1, v0, LX/1g1;->o:Ljava/lang/String;

    .line 212225
    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212226
    iget-object v0, p0, LX/1B5;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BK;

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v0

    .line 212227
    iput v0, p1, LX/1g1;->q:I

    .line 212228
    :cond_0
    return-object p1
.end method

.method public static a(LX/1B5;LX/1g1;)V
    .locals 3

    .prologue
    .line 212196
    const-string v0, "FeedUnitImpressionLoggerController.logImpression"

    const v1, 0x33689fe1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 212197
    :try_start_0
    sget-object v0, LX/1fy;->BEFORE_VALIDATION:LX/1fy;

    invoke-direct {p0, p1, v0}, LX/1B5;->a(LX/1g1;LX/1fy;)V

    .line 212198
    const/4 v1, 0x0

    .line 212199
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    move v0, v1

    .line 212200
    :goto_0
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212201
    if-nez v0, :cond_0

    .line 212202
    const v0, 0x62e48267

    invoke-static {v0}, LX/02m;->a(I)V

    .line 212203
    :goto_1
    return-void

    .line 212204
    :cond_0
    :try_start_1
    sget-object v0, LX/1fy;->BEFORE_SENT_TO_QUEUE:LX/1fy;

    invoke-direct {p0, p1, v0}, LX/1B5;->a(LX/1g1;LX/1fy;)V

    .line 212205
    invoke-direct {p0, p1}, LX/1B5;->b(LX/1g1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212206
    const v0, -0x41874649

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, -0x1188d7d5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 212207
    :cond_1
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    instance-of v0, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v0, :cond_3

    .line 212208
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    .line 212209
    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 212210
    goto :goto_0

    .line 212211
    :cond_3
    iget-object v0, p1, LX/1g1;->a:LX/1g2;

    sget-object v2, LX/1g2;->NON_SPONSORED_IMPRESSION:LX/1g2;

    if-eq v0, v2, :cond_4

    iget-object v0, p1, LX/1g1;->a:LX/1g2;

    sget-object v2, LX/1g2;->SPONSORED_IMPRESSION:LX/1g2;

    if-eq v0, v2, :cond_4

    invoke-virtual {p1}, LX/1g1;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 212212
    goto :goto_0

    .line 212213
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/1B5;Lcom/facebook/graphql/model/Sponsorable;LX/3EA;)V
    .locals 4

    .prologue
    .line 212176
    const-string v0, "FeedUnitImpressionLoggerController.logSponsoredImpressed"

    const v1, 0x324796f7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 212177
    :try_start_0
    invoke-static {p1}, LX/18J;->a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;

    move-result-object v1

    .line 212178
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212179
    sget-object v0, LX/3EB;->a:[I

    invoke-virtual {p2}, LX/3EA;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 212180
    :goto_0
    instance-of v0, v1, Lcom/facebook/graphql/model/SponsoredImpression;

    if-eqz v0, :cond_1

    sget-object v0, LX/1g2;->SPONSORED_IMPRESSION:LX/1g2;

    .line 212181
    :goto_1
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v2

    .line 212182
    new-instance v3, LX/1g1;

    invoke-direct {v3, v0, p1, v2}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    .line 212183
    iput-object v1, v3, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 212184
    move-object v0, v3

    .line 212185
    iput-object p2, v0, LX/1g1;->d:LX/3EA;

    .line 212186
    move-object v0, v0

    .line 212187
    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212188
    :cond_0
    const v0, 0x527730e5

    invoke-static {v0}, LX/02m;->a(I)V

    .line 212189
    return-void

    .line 212190
    :cond_1
    :try_start_1
    sget-object v0, LX/1g2;->ORGANIC_IMPRESSION:LX/1g2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 212191
    :catchall_0
    move-exception v0

    const v1, 0x5d04a4cd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 212192
    :pswitch_0
    sget-object v0, LX/183;->PENDING:LX/183;

    iput-object v0, v1, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    goto :goto_0

    .line 212193
    :pswitch_1
    sget-object v0, LX/183;->PENDING:LX/183;

    iput-object v0, v1, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    goto :goto_0

    .line 212194
    :pswitch_2
    sget-object v0, LX/183;->PENDING:LX/183;

    iput-object v0, v1, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    .line 212195
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/1g1;LX/1fy;)V
    .locals 2

    .prologue
    .line 212169
    iget-object v0, p1, LX/1g1;->a:LX/1g2;

    move-object v0, v0

    .line 212170
    sget-object v1, LX/1g2;->VIEWPORT_DURATION_IMPRESSION:LX/1g2;

    if-ne v0, v1, :cond_0

    .line 212171
    iget-object v0, p1, LX/1g1;->k:Ljava/lang/String;

    move-object v0, v0

    .line 212172
    const-string v1, "native_newsfeed"

    if-eq v0, v1, :cond_1

    .line 212173
    :cond_0
    :goto_0
    return-void

    .line 212174
    :cond_1
    iget-object v0, p1, LX/1g1;->e:LX/162;

    move-object v0, v0

    .line 212175
    invoke-virtual {p0, v0, p2}, LX/1B5;->a(LX/162;LX/1fy;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1B5;
    .locals 22

    .prologue
    .line 212002
    new-instance v2, LX/1B5;

    const/16 v3, 0x687

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x688

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v8

    check-cast v8, LX/1BA;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v9

    check-cast v9, LX/17Q;

    const/16 v10, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0x14b3

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x14b2

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0kY;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v16

    check-cast v16, Landroid/os/Handler;

    const/16 v17, 0x5d5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0pe;->a(LX/0QB;)LX/0pe;

    move-result-object v18

    check-cast v18, LX/0pe;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v19

    check-cast v19, LX/0W3;

    const/16 v20, 0x6ab

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    invoke-direct/range {v2 .. v21}, LX/1B5;-><init>(LX/0Ot;LX/0Ot;LX/0lB;LX/03V;LX/0Zb;LX/1BA;LX/17Q;LX/0Or;LX/0kL;LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/0Ot;LX/0pe;LX/0W3;LX/0Ot;LX/0Uh;)V

    .line 212003
    return-object v2
.end method

.method private b(LX/1g1;)V
    .locals 12

    .prologue
    .line 212018
    iget-object v0, p1, LX/1g1;->e:LX/162;

    move-object v1, v0

    .line 212019
    iget-object v0, p1, LX/1g1;->a:LX/1g2;

    move-object v0, v0

    .line 212020
    sget-object v2, LX/1g3;->a:[I

    invoke-virtual {v0}, LX/1g2;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 212021
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 212022
    :pswitch_0
    iget-object v0, p0, LX/1B5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1g4;

    .line 212023
    iget-object v1, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 212024
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 212025
    iget v2, p1, LX/1g1;->g:I

    move v2, v2

    .line 212026
    invoke-virtual {v0, v1, v2}, LX/1g4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 212027
    :goto_0
    return-void

    .line 212028
    :pswitch_1
    invoke-direct {p0, p1}, LX/1B5;->d(LX/1g1;)V

    .line 212029
    iget-object v0, p0, LX/1B5;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E1;

    .line 212030
    iget-object v2, p1, LX/1g1;->k:Ljava/lang/String;

    move-object v2, v2

    .line 212031
    iget v3, p1, LX/1g1;->i:I

    move v3, v3

    .line 212032
    iget v4, p1, LX/1g1;->n:I

    move v4, v4

    .line 212033
    iget-object v5, p1, LX/1g1;->o:Ljava/lang/String;

    move-object v5, v5

    .line 212034
    iget-object v6, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v6, v6

    .line 212035
    check-cast v6, Lcom/facebook/graphql/model/FeedUnit;

    .line 212036
    iget-object v7, p1, LX/1g1;->r:LX/0P1;

    move-object v7, v7

    .line 212037
    invoke-virtual/range {v0 .. v7}, LX/3E1;->a(LX/0lF;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;LX/0P1;)V

    goto :goto_0

    .line 212038
    :pswitch_2
    iget-object v0, p1, LX/1g1;->k:Ljava/lang/String;

    move-object v0, v0

    .line 212039
    const-string v2, "native_newsfeed"

    if-ne v0, v2, :cond_0

    .line 212040
    sget-object v0, LX/1fy;->BEFORE_SENT_TO_MARAUDER:LX/1fy;

    invoke-virtual {p0, v1, v0}, LX/1B5;->a(LX/162;LX/1fy;)V

    .line 212041
    :cond_0
    iget-object v0, p0, LX/1B5;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E1;

    .line 212042
    iget-wide v10, p1, LX/1g1;->f:J

    move-wide v2, v10

    .line 212043
    iget-object v4, p1, LX/1g1;->k:Ljava/lang/String;

    move-object v4, v4

    .line 212044
    iget-object v5, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v5, v5

    .line 212045
    check-cast v5, Lcom/facebook/graphql/model/FeedUnit;

    .line 212046
    iget v6, p1, LX/1g1;->n:I

    move v6, v6

    .line 212047
    iget-object v7, p1, LX/1g1;->o:Ljava/lang/String;

    move-object v7, v7

    .line 212048
    iget v8, p1, LX/1g1;->q:I

    move v8, v8

    .line 212049
    iget-object v9, p1, LX/1g1;->r:LX/0P1;

    move-object v9, v9

    .line 212050
    invoke-virtual/range {v0 .. v9}, LX/3E1;->a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;ILX/0P1;)V

    goto :goto_0

    .line 212051
    :pswitch_3
    iget-wide v10, p1, LX/1g1;->f:J

    move-wide v2, v10

    .line 212052
    iget v0, p1, LX/1g1;->g:I

    move v0, v0

    .line 212053
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 212054
    const/4 v4, 0x0

    .line 212055
    :goto_1
    move-object v0, v4

    .line 212056
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 212057
    :pswitch_4
    iget-object v0, p0, LX/1B5;->y:LX/0Uh;

    const/16 v1, 0x520

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 212058
    iget-object v0, p1, LX/1g1;->b:Ljava/lang/Object;

    move-object v1, v0

    .line 212059
    instance-of v0, v1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 212060
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-nez v2, :cond_1

    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v2, :cond_7

    :cond_1
    move v2, v4

    .line 212061
    :goto_2
    move v0, v2

    .line 212062
    if-eqz v0, :cond_3

    .line 212063
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v0, 0x0

    .line 212064
    if-nez v1, :cond_e

    .line 212065
    :cond_2
    :goto_3
    move v0, v0

    .line 212066
    iput v0, p1, LX/1g1;->q:I

    .line 212067
    :cond_3
    invoke-direct {p0, p1}, LX/1B5;->d(LX/1g1;)V

    .line 212068
    iget-object v0, p0, LX/1B5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(LX/1g1;)V

    .line 212069
    iget-object v0, p0, LX/1B5;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->u:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 212070
    if-eqz v0, :cond_4

    .line 212071
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 212072
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1B5;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1B5;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 212073
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, v1

    .line 212074
    if-eqz v0, :cond_4

    .line 212075
    iget-object v0, p0, LX/1B5;->n:LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p1}, LX/1g1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 212076
    :cond_4
    goto/16 :goto_0

    .line 212077
    :pswitch_5
    iget-object v0, p1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    move-object v0, v0

    .line 212078
    iget-object v2, p1, LX/1g1;->d:LX/3EA;

    move-object v2, v2

    .line 212079
    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/BaseImpression;->b(LX/3EA;)V

    .line 212080
    iget-object v2, p1, LX/1g1;->d:LX/3EA;

    move-object v2, v2

    .line 212081
    iget v3, p1, LX/1g1;->g:I

    move v3, v3

    .line 212082
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "organic_impression"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "io"

    sget-object v4, LX/3EA;->ORIGINAL:LX/3EA;

    if-ne v2, v4, :cond_f

    const-string v4, "1"

    :goto_4
    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "isv"

    sget-object v4, LX/3EA;->VIEWABILITY:LX/3EA;

    if-ne v2, v4, :cond_10

    const-string v4, "1"

    :goto_5
    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "scroll_index"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object v1, v4

    .line 212083
    iget-object v2, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 212084
    iget-object v1, p1, LX/1g1;->d:LX/3EA;

    move-object v1, v1

    .line 212085
    const/4 v2, 0x1

    iget-object v3, p0, LX/1B5;->o:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/model/BaseImpression;->a(LX/3EA;ZJ)V

    goto/16 :goto_0

    .line 212086
    :pswitch_6
    iget v0, p1, LX/1g1;->g:I

    move v0, v0

    .line 212087
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 212088
    const/4 v2, 0x0

    .line 212089
    :goto_6
    move-object v0, v2

    .line 212090
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212091
    :pswitch_7
    iget v0, p1, LX/1g1;->j:I

    move v0, v0

    .line 212092
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 212093
    const/4 v2, 0x0

    .line 212094
    :goto_7
    move-object v0, v2

    .line 212095
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212096
    :pswitch_8
    iget v0, p1, LX/1g1;->l:I

    move v0, v0

    .line 212097
    iget-boolean v2, p1, LX/1g1;->m:Z

    move v2, v2

    .line 212098
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 212099
    const/4 v3, 0x0

    .line 212100
    :goto_8
    move-object v0, v3

    .line 212101
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212102
    :pswitch_9
    iget-object v0, p1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    move-object v0, v0

    .line 212103
    iget-wide v10, v0, Lcom/facebook/graphql/model/BaseImpression;->h:J

    move-wide v2, v10

    .line 212104
    iget-object v0, p1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    move-object v0, v0

    .line 212105
    iget-object v4, v0, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    move-object v0, v4

    .line 212106
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 212107
    const/4 v4, 0x0

    .line 212108
    :goto_9
    move-object v0, v4

    .line 212109
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212110
    :pswitch_a
    iget v0, p1, LX/1g1;->p:I

    move v0, v0

    .line 212111
    if-ltz v0, :cond_5

    .line 212112
    iget v0, p1, LX/1g1;->p:I

    move v0, v0

    .line 212113
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 212114
    const/4 v2, 0x0

    .line 212115
    :goto_a
    move-object v0, v2

    .line 212116
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212117
    :cond_5
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 212118
    const/4 v0, 0x0

    .line 212119
    :goto_b
    move-object v0, v0

    .line 212120
    iget-object v1, p0, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 212121
    :cond_6
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "viewport_visible_duration"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "vpvd_time_delta"

    invoke-virtual {v4, v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "scroll_index"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 212122
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212123
    move-object v4, v4

    .line 212124
    goto/16 :goto_1

    .line 212125
    :cond_7
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_d

    .line 212126
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 212127
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    .line 212128
    if-nez v7, :cond_8

    move v2, v3

    .line 212129
    goto/16 :goto_2

    .line 212130
    :cond_8
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v3

    :goto_c
    if-ge v6, v8, :cond_d

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 212131
    if-eqz v2, :cond_c

    .line 212132
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2, v5, v9}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v5

    if-eqz v5, :cond_9

    move v2, v4

    .line 212133
    goto/16 :goto_2

    .line 212134
    :cond_9
    invoke-static {v2}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_a

    move v2, v4

    .line 212135
    goto/16 :goto_2

    .line 212136
    :cond_a
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2, v5}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 212137
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v9

    .line 212138
    if-eqz v9, :cond_c

    .line 212139
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v3

    :goto_d
    if-ge v5, v10, :cond_c

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 212140
    invoke-static {v2}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_b

    move v2, v4

    .line 212141
    goto/16 :goto_2

    .line 212142
    :cond_b
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_d

    .line 212143
    :cond_c
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_c

    :cond_d
    move v2, v3

    .line 212144
    goto/16 :goto_2

    .line 212145
    :cond_e
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    .line 212146
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 212147
    iget-object v0, p0, LX/1B5;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BK;

    invoke-virtual {v0, v2}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_3

    :cond_f
    const-string v4, "0"

    goto/16 :goto_4

    :cond_10
    const-string v4, "0"

    goto/16 :goto_5

    .line 212148
    :cond_11
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "non_viewability"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "scroll_index"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 212149
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212150
    move-object v2, v2

    .line 212151
    goto/16 :goto_6

    .line 212152
    :cond_12
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "feed_story_height"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "height"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 212153
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212154
    move-object v2, v2

    .line 212155
    goto/16 :goto_7

    .line 212156
    :cond_13
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "feed_story_seen"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "tracking"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "seen_height"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "fully_seen"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "native_newsfeed"

    .line 212157
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212158
    move-object v3, v3

    .line 212159
    goto/16 :goto_8

    :cond_14
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "percent_feed_unit_seen"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "viewport_entry_ts"

    invoke-virtual {v4, v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "ts_percent_in_viewport"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 212160
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212161
    move-object v4, v4

    .line 212162
    goto/16 :goto_9

    :cond_15
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "feed_unit_full_view_debug"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "partial_full_view_type"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 212163
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212164
    move-object v2, v2

    .line 212165
    goto/16 :goto_a

    :cond_16
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "feed_unit_full_view"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "native_newsfeed"

    .line 212166
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 212167
    move-object v0, v0

    .line 212168
    goto/16 :goto_b

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private declared-synchronized d(LX/1g1;)V
    .locals 6

    .prologue
    .line 212004
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/1g3;->a:[I

    .line 212005
    iget-object v1, p1, LX/1g1;->a:LX/1g2;

    move-object v1, v1

    .line 212006
    invoke-virtual {v1}, LX/1g2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 212007
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212008
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 212009
    :pswitch_1
    :try_start_1
    sget-object v0, LX/3EC;->a:LX/0Tn;

    move-object v1, v0

    .line 212010
    :goto_0
    iget-object v0, p0, LX/1B5;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 212011
    if-nez v0, :cond_0

    .line 212012
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x40e1ffe000000000L    # 36863.0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    shl-int/lit8 v0, v0, 0x10

    .line 212013
    :cond_0
    iget-object v2, p0, LX/1B5;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v2, v1, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 212014
    iput v0, p1, LX/1g1;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212015
    monitor-exit p0

    return-void

    .line 212016
    :pswitch_2
    :try_start_2
    sget-object v0, LX/3EC;->b:LX/0Tn;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 212017
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 212265
    iget-object v0, p0, LX/1B5;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B5;->s:Z

    .line 212266
    iget-object v0, p0, LX/1B5;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B5;->t:Z

    .line 212267
    return-void
.end method

.method public final a(LX/162;LX/1fy;)V
    .locals 13
    .param p1    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 211925
    iget-boolean v0, p0, LX/1B5;->s:Z

    if-nez v0, :cond_0

    .line 211926
    :goto_0
    return-void

    .line 211927
    :cond_0
    iget-object v3, p0, LX/1B5;->v:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 211928
    iget v7, p0, LX/1B5;->w:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    .line 211929
    iget-object v7, p0, LX/1B5;->r:LX/0W3;

    sget-wide v9, LX/0X5;->hJ:J

    const/16 v8, 0x32

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JI)I

    move-result v7

    iput v7, p0, LX/1B5;->w:I

    .line 211930
    :cond_1
    iget v7, p0, LX/1B5;->w:I

    move v4, v7

    .line 211931
    if-ge v3, v4, :cond_3

    iget-object v3, p0, LX/1B5;->o:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, p0, LX/1B5;->u:J

    sub-long/2addr v3, v5

    .line 211932
    iget-wide v7, p0, LX/1B5;->x:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    .line 211933
    iget-object v7, p0, LX/1B5;->r:LX/0W3;

    sget-wide v9, LX/0X5;->hK:J

    const-wide/32 v11, 0x2bf20

    invoke-interface {v7, v9, v10, v11, v12}, LX/0W4;->a(JJ)J

    move-result-wide v7

    iput-wide v7, p0, LX/1B5;->x:J

    .line 211934
    :cond_2
    iget-wide v7, p0, LX/1B5;->x:J

    move-wide v5, v7

    .line 211935
    cmp-long v3, v3, v5

    if-lez v3, :cond_6

    :cond_3
    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 211936
    if-eqz v0, :cond_4

    .line 211937
    iget-object v0, p0, LX/1B5;->v:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 211938
    iget-object v1, p0, LX/1B5;->j:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;-><init>(LX/1B5;LX/0Px;)V

    const v0, 0x26a722df

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 211939
    iget-object v0, p0, LX/1B5;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 211940
    :cond_4
    iget-object v0, p0, LX/1B5;->v:Ljava/util/List;

    new-instance v1, Lcom/facebook/feed/logging/VpvWaterfallImpression;

    iget-boolean v2, p0, LX/1B5;->t:Z

    if-eqz v2, :cond_5

    :goto_2
    invoke-virtual {p2}, LX/1fy;->getCode()I

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/facebook/feed/logging/VpvWaterfallImpression;-><init>(LX/162;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211941
    iget-object v0, p0, LX/1B5;->o:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1B5;->u:J

    goto :goto_0

    .line 211942
    :cond_5
    const/4 p1, 0x0

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "J",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211943
    if-eqz p1, :cond_0

    .line 211944
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 211945
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 211946
    :goto_0
    new-instance v2, LX/1g1;

    sget-object v1, LX/1g2;->VIEWPORT_DURATION_IMPRESSION:LX/1g2;

    iget-object v3, p0, LX/1B5;->q:LX/0pe;

    invoke-virtual {v3, p1}, LX/0pe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-direct {v2, v1, v0, v3}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v1 .. v7}, LX/1B5;->a(LX/1B5;LX/1g1;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)LX/1g1;

    move-result-object v0

    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211947
    return-void

    .line 211948
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211949
    if-eqz p1, :cond_1

    .line 211950
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 211951
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 211952
    :goto_0
    new-instance v1, LX/1g1;

    sget-object v2, LX/1g2;->VIEWPORT_IMPRESSION:LX/1g2;

    iget-object v3, p0, LX/1B5;->q:LX/0pe;

    invoke-virtual {v3, p1}, LX/0pe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    iget-object v2, p0, LX/1B5;->o:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 211953
    iput-wide v2, v1, LX/1g1;->h:J

    .line 211954
    move-object v1, v1

    .line 211955
    iput-object p2, v1, LX/1g1;->k:Ljava/lang/String;

    .line 211956
    move-object v1, v1

    .line 211957
    iput-object p3, v1, LX/1g1;->r:LX/0P1;

    .line 211958
    move-object v1, v1

    .line 211959
    if-eqz p1, :cond_0

    .line 211960
    invoke-static {v0}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v2

    .line 211961
    iput v2, v1, LX/1g1;->n:I

    .line 211962
    invoke-static {v0}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 211963
    iput-object v0, v1, LX/1g1;->o:Ljava/lang/String;

    .line 211964
    :cond_0
    iget-object v0, p0, LX/1B5;->e:LX/1BA;

    const v2, 0x710001

    invoke-virtual {v0, v2}, LX/1BA;->a(I)V

    .line 211965
    invoke-static {p0, v1}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211966
    return-void

    .line 211967
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;LX/AkR;)V
    .locals 3

    .prologue
    .line 211968
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v0

    .line 211969
    new-instance v1, LX/1g1;

    sget-object v2, LX/1g2;->SPONSORED_FULL_VIEW_IMPRESSION:LX/1g2;

    invoke-direct {v1, v2, p1, v0}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 211970
    iput-object v0, v1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 211971
    move-object v0, v1

    .line 211972
    invoke-virtual {p2}, LX/AkR;->ordinal()I

    move-result v1

    .line 211973
    iput v1, v0, LX/1g1;->p:I

    .line 211974
    move-object v0, v0

    .line 211975
    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211976
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStorySet;JLjava/lang/String;LX/0P1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            "J",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211977
    new-instance v2, LX/1g1;

    sget-object v0, LX/1g2;->VIEWPORT_DURATION_IMPRESSION:LX/1g2;

    iget-object v1, p0, LX/1B5;->q:LX/0pe;

    .line 211978
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211979
    iget-object v3, v1, LX/0pe;->a:LX/0pf;

    invoke-virtual {v3, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/1g0;

    move-result-object v3

    .line 211980
    iget-object v4, v3, LX/1g0;->n:LX/162;

    move-object v4, v4

    .line 211981
    if-nez v4, :cond_0

    .line 211982
    invoke-static {p1}, LX/1fz;->b(LX/16h;)LX/162;

    move-result-object v4

    iget-object v5, v1, LX/0pe;->b:LX/0pg;

    invoke-virtual {v5}, LX/0pg;->a()LX/0Rf;

    move-result-object v5

    iget-object v6, v1, LX/0pe;->c:LX/0lB;

    invoke-static {v4, v5, v6}, LX/1BX;->a(LX/162;LX/0Rf;LX/0lB;)LX/162;

    move-result-object v4

    .line 211983
    iput-object v4, v3, LX/1g0;->n:LX/162;

    .line 211984
    :cond_0
    iget-object v4, v3, LX/1g0;->n:LX/162;

    move-object v3, v4

    .line 211985
    move-object v1, v3

    .line 211986
    invoke-direct {v2, v0, p1, v1}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/1B5;->a(LX/1B5;LX/1g1;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)LX/1g1;

    move-result-object v0

    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211987
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/Sponsorable;J)V
    .locals 5

    .prologue
    .line 211988
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v0

    .line 211989
    new-instance v1, LX/1g1;

    sget-object v2, LX/1g2;->SPONSORED_DURATION_IMPRESSION:LX/1g2;

    invoke-direct {v1, v2, p1, v0}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    .line 211990
    iput-wide p2, v1, LX/1g1;->f:J

    .line 211991
    move-object v0, v1

    .line 211992
    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211993
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    .line 211994
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v0

    .line 211995
    new-instance v1, LX/1g1;

    sget-object v2, LX/1g2;->SPONSORED_FULL_VIEW_IMPRESSION:LX/1g2;

    invoke-direct {v1, v2, p1, v0}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 211996
    iput-object v0, v1, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 211997
    move-object v0, v1

    .line 211998
    invoke-static {p0, v0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211999
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/Sponsorable;)V
    .locals 1

    .prologue
    .line 212000
    sget-object v0, LX/3EA;->VIEWABILITY:LX/3EA;

    invoke-static {p0, p1, v0}, LX/1B5;->a(LX/1B5;Lcom/facebook/graphql/model/Sponsorable;LX/3EA;)V

    .line 212001
    return-void
.end method
