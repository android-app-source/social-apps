.class public LX/1nG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 315620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315621
    return-void
.end method

.method private static a(LX/1yB;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1yB;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 315622
    invoke-interface {p0}, LX/1yB;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1yB;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/1yB;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 315623
    sget-object v1, LX/0ax;->dq:Ljava/lang/String;

    invoke-interface {p0}, LX/1yB;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 315624
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 315625
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1nG;
    .locals 1

    .prologue
    .line 315626
    new-instance v0, LX/1nG;

    invoke-direct {v0}, LX/1nG;-><init>()V

    .line 315627
    move-object v0, v0

    .line 315628
    return-object v0
.end method

.method public static varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/infer/annotation/Strict;
        value = "Related to HighPri NPE #5736396"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 315629
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 315630
    :cond_0
    :goto_0
    return-object v0

    .line 315631
    :cond_1
    sparse-switch p0, :sswitch_data_0

    move-object v1, v0

    .line 315632
    :goto_1
    if-eqz v1, :cond_0

    .line 315633
    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315634
    :sswitch_0
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    goto :goto_1

    .line 315635
    :sswitch_1
    sget-object v1, LX/0ax;->aE:Ljava/lang/String;

    goto :goto_1

    .line 315636
    :sswitch_2
    sget-object v1, LX/0ax;->C:Ljava/lang/String;

    goto :goto_1

    .line 315637
    :sswitch_3
    sget-object v1, LX/0ax;->B:Ljava/lang/String;

    goto :goto_1

    .line 315638
    :sswitch_4
    sget-object v1, LX/0ax;->bV:Ljava/lang/String;

    goto :goto_1

    .line 315639
    :sswitch_5
    sget-object v1, LX/0ax;->bs:Ljava/lang/String;

    goto :goto_1

    .line 315640
    :sswitch_6
    sget-object v1, LX/0ax;->bB:Ljava/lang/String;

    goto :goto_1

    .line 315641
    :sswitch_7
    sget-object v1, LX/0ax;->ek:Ljava/lang/String;

    goto :goto_1

    .line 315642
    :sswitch_8
    sget-object v1, LX/0ax;->dr:Ljava/lang/String;

    goto :goto_1

    .line 315643
    :sswitch_9
    sget-object v1, LX/0ax;->do:Ljava/lang/String;

    goto :goto_1

    .line 315644
    :sswitch_a
    sget-object v1, LX/0ax;->eU:Ljava/lang/String;

    goto :goto_1

    .line 315645
    :sswitch_b
    sget-object v1, LX/0ax;->dt:Ljava/lang/String;

    goto :goto_1

    .line 315646
    :sswitch_c
    sget-object v1, LX/0ax;->bw:Ljava/lang/String;

    goto :goto_1

    .line 315647
    :sswitch_d
    sget-object v1, LX/0ax;->hn:Ljava/lang/String;

    goto :goto_1

    .line 315648
    :sswitch_e
    sget-object v1, LX/0ax;->fM:Ljava/lang/String;

    goto :goto_1

    .line 315649
    :sswitch_f
    sget-object v1, LX/0ax;->gU:Ljava/lang/String;

    goto :goto_1

    .line 315650
    :sswitch_10
    sget-object v1, LX/0ax;->hX:Ljava/lang/String;

    goto :goto_1

    .line 315651
    :sswitch_11
    sget-object v1, LX/0ax;->eI:Ljava/lang/String;

    goto :goto_1

    .line 315652
    :sswitch_12
    sget-object v1, LX/0ax;->fv:Ljava/lang/String;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7333ac54 -> :sswitch_b
        -0x4e6785e3 -> :sswitch_f
        -0x3ff252d0 -> :sswitch_7
        -0x1c0bce95 -> :sswitch_10
        -0x7d2175f -> :sswitch_11
        0x252412 -> :sswitch_6
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x3c68e4f -> :sswitch_5
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x4984e12 -> :sswitch_4
        0x499e8e7 -> :sswitch_1
        0x4c808d5 -> :sswitch_9
        0x4ed245b -> :sswitch_a
        0x64687ce -> :sswitch_c
        0xa7c5482 -> :sswitch_e
        0xe198c7c -> :sswitch_8
        0x23637cfe -> :sswitch_f
        0x5fcedbf5 -> :sswitch_d
        0x78cca0e3 -> :sswitch_12
    .end sparse-switch
.end method

.method public static final varargs a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/infer/annotation/Strict;
        value = "Related to HighPri NPE #5736396"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 315653
    if-nez p0, :cond_0

    .line 315654
    const/4 v0, 0x0

    .line 315655
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    invoke-static {v0, p1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1yA;)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const v6, 0x25d6af

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 315656
    invoke-static {p1}, LX/1nG;->a(LX/1yB;)LX/0am;

    move-result-object v1

    .line 315657
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315658
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 315659
    :cond_0
    :goto_0
    return-object v0

    .line 315660
    :cond_1
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 315661
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-ne v1, v2, :cond_2

    .line 315662
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315663
    :cond_2
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x7333ac54

    if-ne v1, v2, :cond_3

    .line 315664
    invoke-interface {p1}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 315665
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {p1}, LX/1yA;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315666
    :cond_3
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x64687ce

    if-ne v1, v2, :cond_4

    .line 315667
    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 315668
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315669
    :cond_4
    invoke-interface {p1}, LX/1yA;->n()LX/1yP;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, LX/1yA;->n()LX/1yP;

    move-result-object v0

    invoke-interface {v0}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 315670
    new-array v0, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1yA;->n()LX/1yP;

    move-result-object v1

    invoke-interface {v1}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v6, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315671
    :cond_5
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v6, :cond_6

    .line 315672
    new-array v0, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v6, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315673
    :cond_6
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x5fcedbf5

    if-ne v0, v1, :cond_7

    .line 315674
    const v0, 0x5fcedbf5

    new-array v1, v5, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {p1}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315675
    :cond_7
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x252412

    if-ne v0, v1, :cond_8

    .line 315676
    const v0, 0x252412

    new-array v1, v5, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {p1}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315677
    :cond_8
    invoke-interface {p1}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-interface {p1}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/infer/annotation/Strict;
        value = "Related to HighPri NPE #5736396"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 315678
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    invoke-static {p1, v0}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/infer/annotation/Strict;
        value = "Related to HighPri NPE #5736396"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 315679
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0xe9bddb6

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1eaef984

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x4dba1a9d

    if-ne v0, v1, :cond_2

    .line 315680
    :cond_0
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315681
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 315682
    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315683
    sget-object v0, LX/0ax;->do:Ljava/lang/String;

    invoke-static {p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 315684
    :cond_1
    :goto_0
    return-object p3

    .line 315685
    :cond_2
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4c808d5

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_5

    .line 315686
    :cond_3
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 315687
    const/4 p3, 0x0

    goto :goto_0

    .line 315688
    :cond_4
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 315689
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315690
    invoke-virtual {p0, p1, v0}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 315691
    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0xa7c5482

    if-ne v0, v1, :cond_6

    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 315692
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 315693
    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315694
    invoke-virtual {p0, p1, p2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 315695
    :cond_6
    invoke-virtual {p0, p1, p2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/facebook/infer/annotation/Strict;
        value = "Related to HighPri NPE #5736396"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v3, 0x25d6af

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315696
    if-nez p1, :cond_0

    .line 315697
    const/4 v0, 0x0

    .line 315698
    :goto_0
    return-object v0

    .line 315699
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 315700
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 315701
    :cond_1
    invoke-static {p1}, LX/1nG;->a(LX/1yB;)LX/0am;

    move-result-object v0

    .line 315702
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 315703
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 315704
    :sswitch_0
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->c()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$EmployerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 315705
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->c()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$EmployerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$EmployerModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315706
    :sswitch_1
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$WorkProjectModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 315707
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$WorkProjectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$WorkProjectModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315708
    :sswitch_2
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->an_()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolClassModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 315709
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->an_()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolClassModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolClassModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315710
    :sswitch_3
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->e()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 315711
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->e()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$SchoolModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315712
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;->am_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7443c132 -> :sswitch_2
        -0x693f1dce -> :sswitch_1
        0x4799e77b -> :sswitch_0
        0x4b57f312 -> :sswitch_3
    .end sparse-switch
.end method
