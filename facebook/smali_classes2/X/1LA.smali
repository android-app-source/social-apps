.class public LX/1LA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1LA;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AjS;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14w;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AjS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/14w;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233622
    iput-object p1, p0, LX/1LA;->a:LX/0Ot;

    .line 233623
    iput-object p2, p0, LX/1LA;->b:LX/0Ot;

    .line 233624
    iput-object p3, p0, LX/1LA;->c:LX/0Ot;

    .line 233625
    return-void
.end method

.method public static a(LX/0QB;)LX/1LA;
    .locals 6

    .prologue
    .line 233626
    sget-object v0, LX/1LA;->d:LX/1LA;

    if-nez v0, :cond_1

    .line 233627
    const-class v1, LX/1LA;

    monitor-enter v1

    .line 233628
    :try_start_0
    sget-object v0, LX/1LA;->d:LX/1LA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 233629
    if-eqz v2, :cond_0

    .line 233630
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 233631
    new-instance v3, LX/1LA;

    const/16 v4, 0x1c87

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb23

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x978

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/1LA;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 233632
    move-object v0, v3

    .line 233633
    sput-object v0, LX/1LA;->d:LX/1LA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233634
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 233635
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233636
    :cond_1
    sget-object v0, LX/1LA;->d:LX/1LA;

    return-object v0

    .line 233637
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 233638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 233594
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 233595
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 233596
    iget-object v1, p0, LX/1LA;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    const/4 v2, 0x0

    .line 233597
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 233598
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-static {v3}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 233599
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233600
    :cond_0
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    if-eqz v3, :cond_1

    .line 233601
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    iget-object v5, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->m:LX/Aod;

    invoke-virtual {v3, v5}, LX/9g2;->b(LX/9g5;)V

    .line 233602
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    invoke-virtual {v3}, LX/9g2;->c()V

    .line 233603
    const/4 v3, 0x0

    iput-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    .line 233604
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 233605
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/1LA;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/14w;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 233606
    iget-object v0, p0, LX/1LA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AjS;

    const/4 v1, 0x0

    .line 233607
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 233608
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v3, p2}, LX/AjS;->a(LX/AjS;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)LX/0Px;

    move-result-object v6

    .line 233609
    new-instance v3, LX/AjR;

    invoke-direct {v3, v0}, LX/AjR;-><init>(LX/AjS;)V

    move-object v9, v3

    .line 233610
    move-object v3, v0

    move-object v4, p1

    move-object v5, p2

    move v7, v1

    move-object v8, p3

    invoke-static/range {v3 .. v9}, LX/AjS;->a(LX/AjS;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;LX/0Px;ZLcom/facebook/common/callercontext/CallerContext;LX/2h0;)V

    .line 233611
    :cond_3
    return-void

    .line 233612
    :cond_4
    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 233613
    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 233614
    invoke-static {v3}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 233615
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 233616
    :cond_6
    const-class v3, LX/23W;

    new-instance v5, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v3, v5}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v5

    .line 233617
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/23T;

    sget-object v6, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5, v6}, LX/23T;->a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    .line 233618
    new-instance v3, LX/Aod;

    invoke-direct {v3, v1, v0, v2}, LX/Aod;-><init>(Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;Lcom/facebook/graphql/model/GraphQLStory;LX/Aoe;)V

    iput-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->m:LX/Aod;

    .line 233619
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    iget-object v5, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->m:LX/Aod;

    invoke-virtual {v3, v5}, LX/9g2;->a(LX/9g5;)V

    .line 233620
    iget-object v3, v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->l:LX/9g2;

    const/16 v5, 0xa

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, LX/9g2;->a(ILX/0am;)V

    goto/16 :goto_0
.end method
