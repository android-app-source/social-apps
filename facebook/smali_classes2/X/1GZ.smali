.class public LX/1GZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/1Ga;

.field private static volatile f:LX/1GZ;


# instance fields
.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/1Gb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 225748
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "cache_dir_expriment_migrator"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 225749
    sput-object v0, LX/1GZ;->a:LX/0Tn;

    const-string v1, "last_known_loc"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1GZ;->b:LX/0Tn;

    .line 225750
    sget-object v0, LX/1Ga;->CACHE:LX/1Ga;

    sput-object v0, LX/1GZ;->c:LX/1Ga;

    return-void
.end method

.method public constructor <init>(LX/1Gb;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225752
    iput-object p1, p0, LX/1GZ;->e:LX/1Gb;

    .line 225753
    iput-object p2, p0, LX/1GZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 225754
    return-void
.end method

.method public static a(LX/0QB;)LX/1GZ;
    .locals 5

    .prologue
    .line 225755
    sget-object v0, LX/1GZ;->f:LX/1GZ;

    if-nez v0, :cond_1

    .line 225756
    const-class v1, LX/1GZ;

    monitor-enter v1

    .line 225757
    :try_start_0
    sget-object v0, LX/1GZ;->f:LX/1GZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225758
    if-eqz v2, :cond_0

    .line 225759
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225760
    new-instance p0, LX/1GZ;

    invoke-static {v0}, LX/1Gb;->a(LX/0QB;)LX/1Gb;

    move-result-object v3

    check-cast v3, LX/1Gb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/1GZ;-><init>(LX/1Gb;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 225761
    move-object v0, p0

    .line 225762
    sput-object v0, LX/1GZ;->f:LX/1GZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225763
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225764
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225765
    :cond_1
    sget-object v0, LX/1GZ;->f:LX/1GZ;

    return-object v0

    .line 225766
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225767
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
