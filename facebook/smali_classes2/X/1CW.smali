.class public LX/1CW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216338
    const-class v0, LX/1CW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1CW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216335
    iput-object p1, p0, LX/1CW;->b:Landroid/content/Context;

    .line 216336
    iput-object p2, p0, LX/1CW;->c:LX/03V;

    .line 216337
    return-void
.end method

.method public static a(II)I
    .locals 0

    .prologue
    .line 216308
    sparse-switch p0, :sswitch_data_0

    .line 216309
    :goto_0
    return p1

    .line 216310
    :sswitch_0
    const p1, 0x7f08005f

    goto :goto_0

    .line 216311
    :sswitch_1
    const p1, 0x7f080060

    goto :goto_0

    .line 216312
    :sswitch_2
    const p1, 0x7f080046

    goto :goto_0

    .line 216313
    :sswitch_3
    const p1, 0x7f080047

    goto :goto_0

    .line 216314
    :sswitch_4
    const p1, 0x7f080048

    goto :goto_0

    .line 216315
    :sswitch_5
    const p1, 0x7f080053

    goto :goto_0

    .line 216316
    :sswitch_6
    const p1, 0x7f080056

    goto :goto_0

    .line 216317
    :sswitch_7
    const p1, 0x7f080057

    goto :goto_0

    .line 216318
    :sswitch_8
    const p1, 0x7f080058

    goto :goto_0

    .line 216319
    :sswitch_9
    const p1, 0x7f080058

    goto :goto_0

    .line 216320
    :sswitch_a
    const p1, 0x7f080058

    goto :goto_0

    .line 216321
    :sswitch_b
    const p1, 0x7f080058

    goto :goto_0

    .line 216322
    :sswitch_c
    const p1, 0x7f080058

    goto :goto_0

    .line 216323
    :sswitch_d
    const p1, 0x7f080059

    goto :goto_0

    .line 216324
    :sswitch_e
    const p1, 0x7f08005a

    goto :goto_0

    .line 216325
    :sswitch_f
    const p1, 0x7f08005a

    goto :goto_0

    .line 216326
    :sswitch_10
    const p1, 0x7f08005a

    goto :goto_0

    .line 216327
    :sswitch_11
    const p1, 0x7f08005a

    goto :goto_0

    .line 216328
    :sswitch_12
    const p1, 0x7f08005a

    goto :goto_0

    .line 216329
    :sswitch_13
    const p1, 0x7f08005b

    goto :goto_0

    .line 216330
    :sswitch_14
    const p1, 0x7f08005c

    goto :goto_0

    .line 216331
    :sswitch_15
    const p1, 0x7f08005d

    goto :goto_0

    .line 216332
    :sswitch_16
    const p1, 0x7f08005e

    goto :goto_0

    .line 216333
    :sswitch_17
    const p1, 0x7f08003e

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x141 -> :sswitch_2
        0x14c -> :sswitch_3
        0x170 -> :sswitch_5
        0x1fa -> :sswitch_4
        0x208 -> :sswitch_7
        0x209 -> :sswitch_8
        0x20a -> :sswitch_d
        0x20b -> :sswitch_e
        0x20c -> :sswitch_9
        0x20d -> :sswitch_f
        0x20e -> :sswitch_13
        0x20f -> :sswitch_14
        0x210 -> :sswitch_15
        0x211 -> :sswitch_a
        0x212 -> :sswitch_10
        0x213 -> :sswitch_11
        0x214 -> :sswitch_b
        0x215 -> :sswitch_c
        0x217 -> :sswitch_12
        0x218 -> :sswitch_16
        0x323 -> :sswitch_17
        0x189117 -> :sswitch_6
    .end sparse-switch
.end method

.method public static a(LX/0QB;)LX/1CW;
    .locals 1

    .prologue
    .line 216339
    invoke-static {p0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/http/protocol/ApiErrorResult;)Z
    .locals 2

    .prologue
    .line 216307
    invoke-virtual {p0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0x170

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1CW;
    .locals 3

    .prologue
    .line 216305
    new-instance v2, LX/1CW;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/1CW;-><init>(Landroid/content/Context;LX/03V;)V

    .line 216306
    return-object v2
.end method

.method public static final b(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 216297
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 216298
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v2, :cond_1

    .line 216299
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 216300
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 216301
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/1CW;->a(Lcom/facebook/http/protocol/ApiErrorResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 216302
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 216303
    goto :goto_0

    :cond_1
    move v0, v1

    .line 216304
    goto :goto_0
.end method

.method public static c(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 3

    .prologue
    .line 216290
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 216291
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non-API error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 216292
    iget-object v2, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v2, v2

    .line 216293
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 216294
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 216295
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    return-object v0

    .line 216296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 216271
    const/4 v0, 0x0

    .line 216272
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 216273
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v1, v2, :cond_3

    .line 216274
    invoke-static {p1}, LX/1CW;->c(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 216275
    invoke-static {v1}, LX/1CW;->a(Lcom/facebook/http/protocol/ApiErrorResult;)Z

    move-result v2

    .line 216276
    if-eqz p2, :cond_0

    if-nez v2, :cond_0

    .line 216277
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/4 v3, -0x1

    .line 216278
    invoke-static {v0, v3}, LX/1CW;->a(II)I

    move-result v2

    .line 216279
    if-ne v2, v3, :cond_4

    .line 216280
    iget-object v2, p0, LX/1CW;->c:LX/03V;

    sget-object v3, LX/1CW;->a:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "No error message for error code "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216281
    const/4 v2, 0x0

    .line 216282
    :goto_0
    move-object v0, v2

    .line 216283
    :cond_0
    if-nez v0, :cond_1

    .line 216284
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    .line 216285
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    .line 216286
    iget-object v0, p0, LX/1CW;->b:Landroid/content/Context;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216287
    :cond_2
    return-object v0

    .line 216288
    :cond_3
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_1

    .line 216289
    iget-object v0, p0, LX/1CW;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v3, p0, LX/1CW;->b:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
