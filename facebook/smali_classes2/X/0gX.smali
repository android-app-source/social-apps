.class public LX/0gX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0TF;

.field private static volatile e:LX/0gX;


# instance fields
.field private final c:LX/1fS;

.field private final d:LX/1fb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112215
    const-class v0, LX/0gX;

    sput-object v0, LX/0gX;->a:Ljava/lang/Class;

    .line 112216
    new-instance v0, LX/1fR;

    invoke-direct {v0}, LX/1fR;-><init>()V

    sput-object v0, LX/0gX;->b:LX/0TF;

    return-void
.end method

.method public constructor <init>(LX/1fS;LX/1fb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 112217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112218
    iput-object p1, p0, LX/0gX;->c:LX/1fS;

    .line 112219
    iput-object p2, p0, LX/0gX;->d:LX/1fb;

    .line 112220
    return-void
.end method

.method public static a(LX/0QB;)LX/0gX;
    .locals 5

    .prologue
    .line 112221
    sget-object v0, LX/0gX;->e:LX/0gX;

    if-nez v0, :cond_1

    .line 112222
    const-class v1, LX/0gX;

    monitor-enter v1

    .line 112223
    :try_start_0
    sget-object v0, LX/0gX;->e:LX/0gX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 112224
    if-eqz v2, :cond_0

    .line 112225
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 112226
    new-instance p0, LX/0gX;

    invoke-static {v0}, LX/1fS;->a(LX/0QB;)LX/1fS;

    move-result-object v3

    check-cast v3, LX/1fS;

    invoke-static {v0}, LX/1fb;->b(LX/0QB;)LX/1fb;

    move-result-object v4

    check-cast v4, LX/1fb;

    invoke-direct {p0, v3, v4}, LX/0gX;-><init>(LX/1fS;LX/1fb;)V

    .line 112227
    move-object v0, p0

    .line 112228
    sput-object v0, LX/0gX;->e:LX/0gX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112229
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 112230
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 112231
    :cond_1
    sget-object v0, LX/0gX;->e:LX/0gX;

    return-object v0

    .line 112232
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 112233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gV;)LX/0gM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0gV",
            "<TT;>;)",
            "LX/0gM;"
        }
    .end annotation

    .prologue
    .line 112234
    sget-object v0, LX/0gX;->b:LX/0TF;

    invoke-virtual {p0, p1, v0}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0gV;LX/0TF;)LX/0gM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0gV",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;)",
            "LX/0gM;"
        }
    .end annotation

    .prologue
    .line 112235
    new-instance v0, LX/026;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    .line 112236
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112237
    invoke-virtual {p0, v0}, LX/0gX;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 112238
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2o1;

    .line 112239
    iget-object v1, v0, LX/2o1;->b:LX/31B;

    move-object v1, v1

    .line 112240
    if-eqz v1, :cond_0

    .line 112241
    iget-object v1, v0, LX/2o1;->b:LX/31B;

    move-object v0, v1

    .line 112242
    throw v0

    .line 112243
    :cond_0
    iget-object v1, v0, LX/2o1;->a:LX/0gM;

    move-object v0, v1

    .line 112244
    return-object v0
.end method

.method public final a(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "Ljava/util/Map",
            "<",
            "LX/0gV",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;>;)",
            "Ljava/util/Map",
            "<",
            "LX/0gV;",
            "LX/2o1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112245
    new-instance v2, LX/026;

    invoke-direct {v2}, LX/026;-><init>()V

    .line 112246
    iget-object v0, p0, LX/0gX;->c:LX/1fS;

    invoke-virtual {v0, p1}, LX/1fS;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 112247
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112248
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2o1;

    .line 112249
    iget-object p1, v1, LX/2o1;->a:LX/0gM;

    move-object v1, p1

    .line 112250
    if-eqz v1, :cond_0

    .line 112251
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 112252
    iget-object v0, p0, LX/0gX;->d:LX/1fb;

    .line 112253
    const-string p1, "graphql_subscriptions_subscribe"

    invoke-static {v0, p1, v1}, LX/1fb;->a(LX/1fb;Ljava/lang/String;LX/0gM;)V

    .line 112254
    goto :goto_0

    .line 112255
    :cond_1
    return-object v2
.end method

.method public final a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0gM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112256
    iget-object v0, p0, LX/0gX;->c:LX/1fS;

    invoke-virtual {v0, p1}, LX/1fS;->a(Ljava/util/Set;)V

    .line 112257
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gM;

    .line 112258
    iget-object v2, p0, LX/0gX;->d:LX/1fb;

    .line 112259
    const-string p1, "graphql_subscriptions_unsubscribe"

    invoke-static {v2, p1, v0}, LX/1fb;->a(LX/1fb;Ljava/lang/String;LX/0gM;)V

    .line 112260
    goto :goto_0

    .line 112261
    :cond_0
    return-void
.end method
