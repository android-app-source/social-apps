.class public LX/0V3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0V3;


# instance fields
.field public final a:LX/0V2;

.field public final b:LX/0V6;

.field public final c:LX/0V8;

.field private final d:LX/0SG;

.field public final e:LX/03V;

.field public final f:LX/0VP;

.field private final g:LX/0VQ;

.field private final h:LX/0Uo;


# direct methods
.method public constructor <init>(LX/0V2;LX/0V6;LX/0V8;LX/0SG;LX/03V;LX/0VP;LX/0VQ;LX/0Uo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67347
    iput-object p3, p0, LX/0V3;->c:LX/0V8;

    .line 67348
    iput-object p1, p0, LX/0V3;->a:LX/0V2;

    .line 67349
    iput-object p2, p0, LX/0V3;->b:LX/0V6;

    .line 67350
    iput-object p4, p0, LX/0V3;->d:LX/0SG;

    .line 67351
    iput-object p5, p0, LX/0V3;->e:LX/03V;

    .line 67352
    iput-object p6, p0, LX/0V3;->f:LX/0VP;

    .line 67353
    iput-object p7, p0, LX/0V3;->g:LX/0VQ;

    .line 67354
    iput-object p8, p0, LX/0V3;->h:LX/0Uo;

    .line 67355
    return-void
.end method

.method public static a(LX/0QB;)LX/0V3;
    .locals 12

    .prologue
    .line 67356
    sget-object v0, LX/0V3;->i:LX/0V3;

    if-nez v0, :cond_1

    .line 67357
    const-class v1, LX/0V3;

    monitor-enter v1

    .line 67358
    :try_start_0
    sget-object v0, LX/0V3;->i:LX/0V3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67359
    if-eqz v2, :cond_0

    .line 67360
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67361
    new-instance v3, LX/0V3;

    invoke-static {v0}, LX/0V2;->a(LX/0QB;)LX/0V2;

    move-result-object v4

    check-cast v4, LX/0V2;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v5

    check-cast v5, LX/0V6;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v6

    check-cast v6, LX/0V8;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v9

    check-cast v9, LX/0VP;

    invoke-static {v0}, LX/0VQ;->a(LX/0QB;)LX/0VQ;

    move-result-object v10

    check-cast v10, LX/0VQ;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v11

    check-cast v11, LX/0Uo;

    invoke-direct/range {v3 .. v11}, LX/0V3;-><init>(LX/0V2;LX/0V6;LX/0V8;LX/0SG;LX/03V;LX/0VP;LX/0VQ;LX/0Uo;)V

    .line 67362
    move-object v0, v3

    .line 67363
    sput-object v0, LX/0V3;->i:LX/0V3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67364
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67365
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67366
    :cond_1
    sget-object v0, LX/0V3;->i:LX/0V3;

    return-object v0

    .line 67367
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0V3;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 67307
    :try_start_0
    iget-object v0, p0, LX/0V3;->e:LX/03V;

    const-string v1, "hprof"

    const-string v2, "Started"

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 67308
    const-string v0, "%s_%d"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    iget-object v2, p0, LX/0V3;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 67309
    const-string v1, "%s/dump_%s.hprof"

    invoke-static {v1, p2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67310
    iget-object v2, p0, LX/0V3;->g:LX/0VQ;

    iget-object v3, p0, LX/0V3;->h:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->j()Z

    move-result v3

    iget-object v4, p0, LX/0V3;->h:LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->m()Z

    move-result v4

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 67311
    sget-boolean v6, LX/0VQ;->e:Z

    if-eqz v6, :cond_1

    .line 67312
    :cond_0
    :goto_0
    invoke-static {v1}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 67313
    iget-object v1, p0, LX/0V3;->e:LX/03V;

    const-string v2, "hprof"

    const-string v3, "Success"

    invoke-virtual {v1, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 67314
    iget-object v1, p0, LX/0V3;->e:LX/03V;

    const-string v2, "hprof_id"

    invoke-virtual {v1, v2, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67315
    :goto_1
    return-void

    .line 67316
    :catch_0
    move-exception v0

    .line 67317
    const-string v1, "MemoryDumper"

    const-string v2, "IOException trying to write Hprof dump"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67318
    iget-object v1, p0, LX/0V3;->e:LX/03V;

    const-string v2, "hprof"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 67319
    :cond_1
    :try_start_1
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67320
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 67321
    :try_start_2
    const-string v6, ""

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 67322
    const-string v5, "Warning@storeDumpMetadata Throwable .getClass().getName() returned a empty string"

    invoke-static {v5, v7}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 67323
    :catch_1
    move-exception v5

    .line 67324
    const-string v6, "Warning@storeDumpMetadata invalid arguments while writing "

    invoke-static {v6, v5}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 67325
    :cond_2
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 67326
    :try_start_3
    const-string v6, "Dump ID"

    invoke-virtual {v7, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v8, "Dump cause"

    invoke-virtual {v6, v8, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v8, "app_version_name"

    iget-object p2, v2, LX/0VQ;->f:LX/0WV;

    invoke-virtual {p2}, LX/0WV;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, v8, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v8, "app_version_code"

    iget-object p2, v2, LX/0VQ;->f:LX/0WV;

    invoke-virtual {p2}, LX/0WV;->b()I

    move-result p2

    invoke-virtual {v6, v8, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v8, "Is Backgrounded"

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, v8, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v8, "Was Ever Foregrounded"

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, v8, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move v6, v5

    .line 67327
    :goto_2
    const/4 v8, 0x3

    if-ge v6, v8, :cond_3

    .line 67328
    sget-object v5, LX/0VQ;->c:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    sget-object v8, LX/0VQ;->a:LX/0Tn;

    invoke-virtual {v8}, LX/0To;->a()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-static {v0}, LX/0VQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v8

    invoke-virtual {v8}, LX/0To;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v5, v8, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v5

    .line 67329
    if-nez v5, :cond_3

    .line 67330
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 67331
    :cond_3
    if-nez v5, :cond_0

    .line 67332
    const-string v5, "Error@storeMetadata metadata didn\'t commit even after 3 retries"

    const/4 v6, 0x0

    invoke-static {v5, v6}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 67333
    :catch_2
    move-exception v5

    .line 67334
    const-string v6, "Error@storeDumpMetadata(): Unable to put metadata to JSON "

    invoke-static {v6, v5}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public static b(LX/0V3;Ljava/lang/String;)V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 67335
    iget-object v0, p0, LX/0V3;->f:LX/0VP;

    iget-object v1, p0, LX/0V3;->f:LX/0VP;

    invoke-virtual {v1}, LX/0VP;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2Hr;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1, v2}, LX/0VP;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    .line 67336
    if-eqz v0, :cond_2

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 67337
    if-eqz v0, :cond_0

    .line 67338
    :goto_1
    return-void

    .line 67339
    :cond_0
    iget-object v3, p0, LX/0V3;->b:LX/0V6;

    .line 67340
    iget-wide v9, v3, LX/0V6;->e:J

    move-wide v3, v9

    .line 67341
    iget-object v5, p0, LX/0V3;->c:LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v5, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v5

    .line 67342
    const-wide/16 v7, 0x3

    mul-long/2addr v3, v7

    cmp-long v3, v5, v3

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 67343
    if-nez v0, :cond_1

    .line 67344
    iget-object v0, p0, LX/0V3;->e:LX/03V;

    const-string v1, "hprof"

    const-string v2, "Failed - not enough free space"

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 67345
    :cond_1
    iget-object v0, p0, LX/0V3;->f:LX/0VP;

    invoke-virtual {v0}, LX/0VP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0V3;->a(LX/0V3;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method
