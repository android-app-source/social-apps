.class public final LX/13y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/140;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/140;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 177841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177842
    iput-object p1, p0, LX/13y;->a:LX/0QB;

    .line 177843
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 177844
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/13y;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 177845
    packed-switch p2, :pswitch_data_0

    .line 177846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177847
    :pswitch_0
    new-instance v1, LX/13z;

    invoke-static {p1}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-direct {v1, v0}, LX/13z;-><init>(LX/0tK;)V

    .line 177848
    move-object v0, v1

    .line 177849
    :goto_0
    return-object v0

    .line 177850
    :pswitch_1
    new-instance v1, LX/142;

    invoke-static {p1}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-direct {v1, v0}, LX/142;-><init>(LX/0tK;)V

    .line 177851
    move-object v0, v1

    .line 177852
    goto :goto_0

    .line 177853
    :pswitch_2
    new-instance v0, LX/143;

    const/16 v1, 0x1483

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/143;-><init>(LX/0Or;)V

    .line 177854
    move-object v0, v0

    .line 177855
    goto :goto_0

    .line 177856
    :pswitch_3
    new-instance v0, LX/144;

    const/16 v1, 0x14d1

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/144;-><init>(LX/0Or;)V

    .line 177857
    move-object v0, v0

    .line 177858
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 177859
    const/4 v0, 0x4

    return v0
.end method
