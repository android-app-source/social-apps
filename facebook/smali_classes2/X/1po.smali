.class public final enum LX/1po;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1po;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1po;

.field public static final enum PHOTO:LX/1po;

.field public static final enum UNKNOWN:LX/1po;

.field public static final enum VIDEO:LX/1po;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 329751
    new-instance v0, LX/1po;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/1po;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1po;->PHOTO:LX/1po;

    .line 329752
    new-instance v0, LX/1po;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/1po;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1po;->VIDEO:LX/1po;

    .line 329753
    new-instance v0, LX/1po;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/1po;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1po;->UNKNOWN:LX/1po;

    .line 329754
    const/4 v0, 0x3

    new-array v0, v0, [LX/1po;

    sget-object v1, LX/1po;->PHOTO:LX/1po;

    aput-object v1, v0, v2

    sget-object v1, LX/1po;->VIDEO:LX/1po;

    aput-object v1, v0, v3

    sget-object v1, LX/1po;->UNKNOWN:LX/1po;

    aput-object v1, v0, v4

    sput-object v0, LX/1po;->$VALUES:[LX/1po;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 329755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1po;
    .locals 1

    .prologue
    .line 329756
    const-class v0, LX/1po;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1po;

    return-object v0
.end method

.method public static values()[LX/1po;
    .locals 1

    .prologue
    .line 329757
    sget-object v0, LX/1po;->$VALUES:[LX/1po;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1po;

    return-object v0
.end method
