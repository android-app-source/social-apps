.class public final LX/0pp;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 146477
    new-instance v0, LX/0U1;

    const-string v1, "feed_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->a:LX/0U1;

    .line 146478
    new-instance v0, LX/0U1;

    const-string v1, "fetched_at"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->b:LX/0U1;

    .line 146479
    new-instance v0, LX/0U1;

    const-string v1, "cursor"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->c:LX/0U1;

    .line 146480
    new-instance v0, LX/0U1;

    const-string v1, "dedup_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->d:LX/0U1;

    .line 146481
    new-instance v0, LX/0U1;

    const-string v1, "sort_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->e:LX/0U1;

    .line 146482
    new-instance v0, LX/0U1;

    const-string v1, "ranking_weight"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->f:LX/0U1;

    .line 146483
    new-instance v0, LX/0U1;

    const-string v1, "features_meta"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->g:LX/0U1;

    .line 146484
    new-instance v0, LX/0U1;

    const-string v1, "disallow_first"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->h:LX/0U1;

    .line 146485
    new-instance v0, LX/0U1;

    const-string v1, "seen_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->i:LX/0U1;

    .line 146486
    new-instance v0, LX/0U1;

    const-string v1, "image_seen_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->j:LX/0U1;

    .line 146487
    new-instance v0, LX/0U1;

    const-string v1, "see_first_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->k:LX/0U1;

    .line 146488
    new-instance v0, LX/0U1;

    const-string v1, "row_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->l:LX/0U1;

    .line 146489
    new-instance v0, LX/0U1;

    const-string v1, "story_data"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->m:LX/0U1;

    .line 146490
    new-instance v0, LX/0U1;

    const-string v1, "story_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->n:LX/0U1;

    .line 146491
    new-instance v0, LX/0U1;

    const-string v1, "cache_file_path"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->o:LX/0U1;

    .line 146492
    new-instance v0, LX/0U1;

    const-string v1, "cache_file_offset"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->p:LX/0U1;

    .line 146493
    new-instance v0, LX/0U1;

    const-string v1, "cache_file_data_length"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->q:LX/0U1;

    .line 146494
    new-instance v0, LX/0U1;

    const-string v1, "unit_position_in_flatbuffer"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->r:LX/0U1;

    .line 146495
    new-instance v0, LX/0U1;

    const-string v1, "mutation_data"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->s:LX/0U1;

    .line 146496
    new-instance v0, LX/0U1;

    const-string v1, "extra_data"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->t:LX/0U1;

    .line 146497
    new-instance v0, LX/0U1;

    const-string v1, "mutation_supported"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->u:LX/0U1;

    .line 146498
    new-instance v0, LX/0U1;

    const-string v1, "client_sort_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->v:LX/0U1;

    .line 146499
    new-instance v0, LX/0U1;

    const-string v1, "client_cursor"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->w:LX/0U1;

    .line 146500
    new-instance v0, LX/0U1;

    const-string v1, "cache_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->x:LX/0U1;

    .line 146501
    new-instance v0, LX/0U1;

    const-string v1, "story_ranking_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->y:LX/0U1;

    .line 146502
    new-instance v0, LX/0U1;

    const-string v1, "story_bump_reason"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0pp;->z:LX/0U1;

    return-void
.end method
