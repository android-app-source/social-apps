.class public LX/0Si;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Si;


# instance fields
.field private a:LX/0Sk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sk;)V
    .locals 0
    .param p1    # LX/0Sk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 62042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62043
    iput-object p1, p0, LX/0Si;->a:LX/0Sk;

    .line 62044
    return-void
.end method

.method public static a(LX/0QB;)LX/0Si;
    .locals 4

    .prologue
    .line 62054
    sget-object v0, LX/0Si;->b:LX/0Si;

    if-nez v0, :cond_1

    .line 62055
    const-class v1, LX/0Si;

    monitor-enter v1

    .line 62056
    :try_start_0
    sget-object v0, LX/0Si;->b:LX/0Si;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62057
    if-eqz v2, :cond_0

    .line 62058
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62059
    new-instance p0, LX/0Si;

    invoke-static {v0}, LX/0Sk;->a(LX/0QB;)LX/0Sk;

    move-result-object v3

    check-cast v3, LX/0Sk;

    invoke-direct {p0, v3}, LX/0Si;-><init>(LX/0Sk;)V

    .line 62060
    move-object v0, p0

    .line 62061
    sput-object v0, LX/0Si;->b:LX/0Si;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62062
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62063
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62064
    :cond_1
    sget-object v0, LX/0Si;->b:LX/0Si;

    return-object v0

    .line 62065
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 62053
    iget-object v0, p0, LX/0Si;->a:LX/0Sk;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Si;->a:LX/0Sk;

    invoke-virtual {v0}, LX/0Sk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;
    .locals 2

    .prologue
    .line 62067
    const-wide/16 v0, 0x80

    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62068
    new-instance v0, LX/44O;

    invoke-direct {v0, p1, p2}, LX/44O;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62069
    :goto_0
    return-object v0

    .line 62070
    :cond_0
    invoke-direct {p0}, LX/0Si;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62071
    new-instance v0, LX/22g;

    iget-object v1, p0, LX/0Si;->a:LX/0Sk;

    invoke-direct {v0, v1, p1, p2}, LX/22g;-><init>(LX/0Sk;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 62072
    :cond_1
    sget-object v0, LX/1mY;->a:LX/0cV;

    goto :goto_0
.end method

.method public final a(LX/0cT;)Ljava/util/List;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 62046
    iget-object v0, p0, LX/0Si;->a:LX/0Sk;

    if-nez v0, :cond_0

    .line 62047
    const/4 v0, 0x0

    .line 62048
    :goto_0
    return-object v0

    .line 62049
    :cond_0
    new-instance v1, LX/0cU;

    invoke-direct {v1, p0, p1}, LX/0cU;-><init>(LX/0Si;LX/0cT;)V

    .line 62050
    iget-object v0, p0, LX/0Si;->a:LX/0Sk;

    invoke-virtual {v0, v1}, LX/0Sk;->a(LX/0XB;)V

    .line 62051
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 62052
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 62045
    const-wide/16 v0, 0x80

    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/0Si;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
