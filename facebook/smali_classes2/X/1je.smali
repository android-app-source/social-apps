.class public LX/1je;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jf;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/0pJ;

.field private b:LX/0xv;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0qZ;

.field private final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private f:J

.field private g:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300740
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1je;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0pJ;LX/0Ot;LX/0qZ;LX/0SG;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pJ;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0qZ;",
            "LX/0SG;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 300733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300734
    iput-object p1, p0, LX/1je;->a:LX/0pJ;

    .line 300735
    iput-object p2, p0, LX/1je;->c:LX/0Ot;

    .line 300736
    iput-object p3, p0, LX/1je;->d:LX/0qZ;

    .line 300737
    iput-object p4, p0, LX/1je;->g:LX/0SG;

    .line 300738
    iput-object p5, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 300739
    return-void
.end method

.method public static a(LX/0QB;)LX/1je;
    .locals 13

    .prologue
    .line 300704
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 300705
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 300706
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 300707
    if-nez v1, :cond_0

    .line 300708
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300709
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 300710
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 300711
    sget-object v1, LX/1je;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 300712
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 300713
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 300714
    :cond_1
    if-nez v1, :cond_4

    .line 300715
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 300716
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 300717
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 300718
    new-instance v7, LX/1je;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/0qZ;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/0qZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v12

    check-cast v12, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v7 .. v12}, LX/1je;-><init>(LX/0pJ;LX/0Ot;LX/0qZ;LX/0SG;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 300719
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300720
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 300721
    if-nez v1, :cond_2

    .line 300722
    sget-object v0, LX/1je;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1je;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 300723
    :goto_1
    if-eqz v0, :cond_3

    .line 300724
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 300725
    :goto_3
    check-cast v0, LX/1je;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 300726
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 300727
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 300728
    :catchall_1
    move-exception v0

    .line 300729
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 300730
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 300731
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 300732
    :cond_2
    :try_start_8
    sget-object v0, LX/1je;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1je;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a()V
    .locals 6

    .prologue
    .line 300678
    const-string v0, "FreshFeedStoryRanker.init"

    const v1, 0x628d67dd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300679
    :try_start_0
    iget-object v0, p0, LX/1je;->a:LX/0pJ;

    const-string v1, "{\"ctr_multiply_values\" : {\"base_values\" : { \"weight_final\" : \"1\", \"seen\" : {\"viewed\" : \"-10000\"}, \"!fresh\" : {\"viewed\" : \"-2000\"}}},\"ctr_value_features\": {\"seen\": \"client_has_seen\", \"fresh\": \"cur_client_story_age_ms < 540001\"}}"

    invoke-virtual {v0, v1}, LX/0pJ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 300680
    iget-object v1, p0, LX/1je;->a:LX/0pJ;

    const/4 v2, 0x0

    .line 300681
    sget-wide v3, LX/0X5;->dy:J

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5, v2}, LX/0pK;->a(JILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 300682
    iget-object v2, p0, LX/1je;->d:LX/0qZ;

    invoke-virtual {v2, v0, v1}, LX/0qZ;->a(Ljava/lang/String;Ljava/lang/String;)LX/0xv;

    move-result-object v0

    iput-object v0, p0, LX/1je;->b:LX/0xv;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300683
    const v0, 0x4552d91e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300684
    :goto_0
    return-void

    .line 300685
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 300686
    :try_start_1
    iget-object v0, p0, LX/1je;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FreshFeedStoryRanker"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 300687
    iget-object v0, p0, LX/1je;->d:LX/0qZ;

    const-string v1, "{\"ctr_multiply_values\" : {\"base_values\" : { \"weight_final\" : \"1\", \"seen\" : {\"viewed\" : \"-10000\"}, \"!fresh\" : {\"viewed\" : \"-2000\"}}},\"ctr_value_features\": {\"seen\": \"client_has_seen\", \"fresh\": \"cur_client_story_age_ms < 540001\"}}"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0qZ;->a(Ljava/lang/String;Ljava/lang/String;)LX/0xv;

    move-result-object v0

    iput-object v0, p0, LX/1je;->b:LX/0xv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300688
    const v0, 0x11929894

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x2da7ad10

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v6, 0x3d

    const/4 v5, 0x2

    const v4, 0xa00a5

    .line 300689
    iget-object v0, p0, LX/1je;->b:LX/0xv;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1je;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1je;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 300690
    :cond_0
    invoke-direct {p0}, LX/1je;->a()V

    .line 300691
    iget-object v0, p0, LX/1je;->b:LX/0xv;

    const/4 v1, 0x1

    .line 300692
    iput-boolean v1, v0, LX/0xv;->n:Z

    .line 300693
    iget-object v0, p0, LX/1je;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1je;->f:J

    .line 300694
    :cond_1
    const-string v0, "FreshFeedStoryRanker.rerank"

    const v1, 0x5cda898b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300695
    iget-object v0, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 300696
    :try_start_0
    iget-object v0, p0, LX/1je;->b:LX/0xv;

    invoke-virtual {v0, p1}, LX/0xv;->a(Ljava/util/List;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 300697
    iget-object v1, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 300698
    iget-object v1, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v6, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 300699
    const v0, -0x2406ff18

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300700
    return-void

    .line 300701
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 300702
    iget-object v1, p0, LX/1je;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x0

    invoke-interface {v1, v4, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 300703
    const v1, -0x68b8dc7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
