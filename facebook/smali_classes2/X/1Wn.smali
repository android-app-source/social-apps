.class public LX/1Wn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/39b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Wn",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/39b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269583
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 269584
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Wn;->b:LX/0Zi;

    .line 269585
    iput-object p1, p0, LX/1Wn;->a:LX/0Ot;

    .line 269586
    return-void
.end method

.method public static a(LX/0QB;)LX/1Wn;
    .locals 4

    .prologue
    .line 269572
    const-class v1, LX/1Wn;

    monitor-enter v1

    .line 269573
    :try_start_0
    sget-object v0, LX/1Wn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269574
    sput-object v2, LX/1Wn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269577
    new-instance v3, LX/1Wn;

    const/16 p0, 0x8b1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Wn;-><init>(LX/0Ot;)V

    .line 269578
    move-object v0, v3

    .line 269579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Wn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;LX/20K;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/20K;",
            ")",
            "LX/1dQ",
            "<",
            "LX/48J;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269571
    const v0, 0x45d367d4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;LX/20b;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/20b;",
            ")",
            "LX/1dQ",
            "<",
            "LX/48J;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269570
    const v0, 0x3361a0d3

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269569
    const v0, -0x14ac399a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269568
    const v0, -0x1a74f343

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269463
    const v0, -0x1a74f0d7

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 269565
    check-cast p2, LX/39W;

    .line 269566
    iget-object v0, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/39b;

    iget-object v2, p2, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/39W;->b:LX/1Po;

    iget-object v4, p2, LX/39W;->c:LX/1Wk;

    iget v5, p2, LX/39W;->d:I

    iget v6, p2, LX/39W;->e:I

    iget-object v7, p2, LX/39W;->f:LX/1zt;

    iget-object v8, p2, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v9, p2, LX/39W;->h:LX/20K;

    iget-object v10, p2, LX/39W;->i:LX/39f;

    iget-object v11, p2, LX/39W;->j:LX/20b;

    iget-object v12, p2, LX/39W;->k:LX/20b;

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, LX/39b;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1Wk;IILX/1zt;Lcom/facebook/feed/rows/core/props/FeedProps;LX/20K;LX/39f;LX/20b;LX/20b;)LX/1Dg;

    move-result-object v0

    .line 269567
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 269516
    invoke-static {}, LX/1dS;->b()V

    .line 269517
    iget v1, p1, LX/1dQ;->b:I

    .line 269518
    sparse-switch v1, :sswitch_data_0

    .line 269519
    :goto_0
    return-object v0

    .line 269520
    :sswitch_0
    check-cast p2, LX/48J;

    .line 269521
    iget-object v1, p2, LX/48J;->a:Landroid/view/View;

    iget-object v2, p2, LX/48J;->b:Landroid/view/MotionEvent;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, LX/20b;

    .line 269522
    iget-object p1, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 269523
    invoke-virtual {v0, v1, v2}, LX/20b;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    move p1, p1

    .line 269524
    move v0, p1

    .line 269525
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 269526
    :sswitch_1
    check-cast p2, LX/48J;

    .line 269527
    iget-object v1, p2, LX/48J;->a:Landroid/view/View;

    iget-object v2, p2, LX/48J;->b:Landroid/view/MotionEvent;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, LX/20K;

    .line 269528
    iget-object p1, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 269529
    invoke-virtual {v0, v1, v2}, LX/20K;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 269530
    const/4 p1, 0x1

    move p1, p1

    .line 269531
    move v0, p1

    .line 269532
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 269533
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 269534
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 269535
    check-cast v2, LX/39W;

    .line 269536
    iget-object v3, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/39b;

    iget-object v5, v2, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, v2, LX/39W;->m:LX/20Z;

    iget-object v7, v2, LX/39W;->i:LX/39f;

    iget-object v8, v2, LX/39W;->l:LX/21H;

    iget-object v9, v2, LX/39W;->f:LX/1zt;

    iget-object p1, v2, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v1

    const/4 p0, 0x1

    const/4 v1, 0x0

    .line 269537
    invoke-virtual {v5, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    sget-object p2, LX/1zt;->c:LX/1zt;

    if-ne v9, p2, :cond_1

    .line 269538
    :cond_0
    :goto_1
    if-eqz p0, :cond_3

    iget-object p2, v3, LX/39b;->d:LX/1zf;

    invoke-virtual {p2}, LX/1zf;->e()LX/1zt;

    move-result-object p2

    .line 269539
    :goto_2
    sget-object v2, LX/20X;->LIKE:LX/20X;

    invoke-interface {v6, v4, v2}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 269540
    invoke-static {v7, v1, p2}, LX/39f;->a$redex0(LX/39f;ILX/1zt;)V

    .line 269541
    invoke-virtual {v8}, LX/21H;->c()Z

    move-result v1

    if-nez v1, :cond_4

    .line 269542
    :goto_3
    goto :goto_0

    .line 269543
    :sswitch_3
    check-cast p2, LX/3Ae;

    .line 269544
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 269545
    check-cast v2, LX/39W;

    .line 269546
    iget-object v3, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v2, LX/39W;->m:LX/20Z;

    .line 269547
    sget-object p0, LX/20X;->COMMENT:LX/20X;

    invoke-interface {v3, v1, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 269548
    goto/16 :goto_0

    .line 269549
    :sswitch_4
    check-cast p2, LX/3Ae;

    .line 269550
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 269551
    check-cast v2, LX/39W;

    .line 269552
    iget-object v3, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v2, LX/39W;->m:LX/20Z;

    .line 269553
    sget-object p0, LX/20X;->SHARE:LX/20X;

    invoke-interface {v3, v1, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 269554
    goto/16 :goto_0

    :cond_1
    move p0, v1

    .line 269555
    goto :goto_1

    .line 269556
    :cond_2
    iget-object p2, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 269557
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result p2

    if-eqz p2, :cond_0

    move p0, v1

    goto :goto_1

    .line 269558
    :cond_3
    sget-object p2, LX/1zt;->c:LX/1zt;

    goto :goto_2

    .line 269559
    :cond_4
    if-eqz p0, :cond_5

    .line 269560
    invoke-static {v7, p2}, LX/39f;->a$redex0(LX/39f;LX/1zt;)V

    goto :goto_3

    .line 269561
    :cond_5
    iget-object p2, v7, LX/39f;->j:Ljava/lang/Runnable;

    if-eqz p2, :cond_6

    .line 269562
    iget-object p2, v7, LX/39f;->a:LX/39b;

    iget-object p2, p2, LX/39b;->k:Landroid/os/Handler;

    iget-object p0, v7, LX/39f;->j:Ljava/lang/Runnable;

    invoke-static {p2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 269563
    :cond_6
    const/4 p2, 0x0

    iput-object p2, v7, LX/39f;->j:Ljava/lang/Runnable;

    .line 269564
    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x1a74f343 -> :sswitch_3
        -0x1a74f0d7 -> :sswitch_4
        -0x14ac399a -> :sswitch_2
        0x3361a0d3 -> :sswitch_0
        0x45d367d4 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 269504
    check-cast p1, LX/39W;

    .line 269505
    check-cast p2, LX/39W;

    .line 269506
    iget v0, p1, LX/39W;->e:I

    iput v0, p2, LX/39W;->e:I

    .line 269507
    iget-object v0, p1, LX/39W;->f:LX/1zt;

    iput-object v0, p2, LX/39W;->f:LX/1zt;

    .line 269508
    iget-object v0, p1, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p2, LX/39W;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269509
    iget-object v0, p1, LX/39W;->h:LX/20K;

    iput-object v0, p2, LX/39W;->h:LX/20K;

    .line 269510
    iget-object v0, p1, LX/39W;->i:LX/39f;

    iput-object v0, p2, LX/39W;->i:LX/39f;

    .line 269511
    iget-object v0, p1, LX/39W;->j:LX/20b;

    iput-object v0, p2, LX/39W;->j:LX/20b;

    .line 269512
    iget-object v0, p1, LX/39W;->k:LX/20b;

    iput-object v0, p2, LX/39W;->k:LX/20b;

    .line 269513
    iget-object v0, p1, LX/39W;->l:LX/21H;

    iput-object v0, p2, LX/39W;->l:LX/21H;

    .line 269514
    iget-object v0, p1, LX/39W;->m:LX/20Z;

    iput-object v0, p2, LX/39W;->m:LX/20Z;

    .line 269515
    return-void
.end method

.method public final c(LX/1De;)LX/39X;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1Wn",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 269496
    new-instance v1, LX/39W;

    invoke-direct {v1, p0}, LX/39W;-><init>(LX/1Wn;)V

    .line 269497
    iget-object v2, p0, LX/1Wn;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/39X;

    .line 269498
    if-nez v2, :cond_0

    .line 269499
    new-instance v2, LX/39X;

    invoke-direct {v2, p0}, LX/39X;-><init>(LX/1Wn;)V

    .line 269500
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/39X;->a$redex0(LX/39X;LX/1De;IILX/39W;)V

    .line 269501
    move-object v1, v2

    .line 269502
    move-object v0, v1

    .line 269503
    return-object v0
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 11

    .prologue
    .line 269465
    check-cast p2, LX/39W;

    .line 269466
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 269467
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 269468
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 269469
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 269470
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 269471
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 269472
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 269473
    iget-object v0, p0, LX/1Wn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/39b;

    iget-object v2, p2, LX/39W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/39W;->b:LX/1Po;

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, LX/39b;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 269474
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269475
    check-cast v0, LX/20K;

    iput-object v0, p2, LX/39W;->h:LX/20K;

    .line 269476
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 269477
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269478
    check-cast v0, LX/39f;

    iput-object v0, p2, LX/39W;->i:LX/39f;

    .line 269479
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 269480
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269481
    check-cast v0, LX/20b;

    iput-object v0, p2, LX/39W;->j:LX/20b;

    .line 269482
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 269483
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269484
    check-cast v0, LX/20b;

    iput-object v0, p2, LX/39W;->k:LX/20b;

    .line 269485
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 269486
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269487
    check-cast v0, LX/21H;

    iput-object v0, p2, LX/39W;->l:LX/21H;

    .line 269488
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 269489
    iget-object v0, v9, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269490
    check-cast v0, LX/20Z;

    iput-object v0, p2, LX/39W;->m:LX/20Z;

    .line 269491
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 269492
    iget-object v0, v10, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 269493
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/39W;->e:I

    .line 269494
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 269495
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 269464
    const/4 v0, 0x1

    return v0
.end method
