.class public abstract LX/0Za;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zb;


# instance fields
.field private final a:Ljava/util/Random;

.field private final b:LX/0Zm;

.field public final c:Lcom/facebook/analytics/AnalyticsStats;

.field private final d:LX/0aE;


# direct methods
.method public constructor <init>(Ljava/util/Random;LX/0Zm;Lcom/facebook/analytics/AnalyticsStats;)V
    .locals 1

    .prologue
    .line 83697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83698
    new-instance v0, LX/0aE;

    invoke-direct {v0, p0}, LX/0aE;-><init>(LX/0Za;)V

    iput-object v0, p0, LX/0Za;->d:LX/0aE;

    .line 83699
    iput-object p1, p0, LX/0Za;->a:Ljava/util/Random;

    .line 83700
    iput-object p2, p0, LX/0Za;->b:LX/0Zm;

    .line 83701
    iput-object p3, p0, LX/0Za;->c:Lcom/facebook/analytics/AnalyticsStats;

    .line 83702
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)LX/0oG;
    .locals 4

    .prologue
    .line 83679
    iget-object v0, p0, LX/0Za;->b:LX/0Zm;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 83680
    invoke-static {}, LX/0i9;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/0mr;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v2

    .line 83681
    :cond_1
    :goto_0
    move v0, v1

    .line 83682
    iget-object v1, p0, LX/0Za;->b:LX/0Zm;

    const/4 v2, 0x1

    .line 83683
    if-gtz v0, :cond_5

    .line 83684
    const/4 v2, 0x0

    .line 83685
    :cond_2
    :goto_1
    move v1, v2

    .line 83686
    new-instance v2, LX/0oG;

    iget-object v3, p0, LX/0Za;->d:LX/0aE;

    invoke-direct {v2, p1, v1, v0, v3}, LX/0oG;-><init>(Ljava/lang/String;ZILX/0aE;)V

    return-object v2

    .line 83687
    :cond_3
    iget-object v1, v0, LX/0Zm;->d:LX/0XZ;

    invoke-virtual {v1, p1, v3, v3}, LX/0XZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 83688
    if-gez v1, :cond_1

    .line 83689
    if-eqz p2, :cond_4

    move v1, v2

    .line 83690
    goto :goto_0

    .line 83691
    :cond_4
    const/16 v1, 0xc8

    move v1, v1

    .line 83692
    goto :goto_0

    .line 83693
    :cond_5
    if-eq v0, v2, :cond_2

    .line 83694
    iget-object v2, v1, LX/0Zm;->d:LX/0XZ;

    .line 83695
    iget-object v1, v2, LX/0XZ;->d:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    move v2, v1

    .line 83696
    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final c(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 83671
    if-ne p2, v0, :cond_1

    .line 83672
    :cond_0
    :goto_0
    return v0

    .line 83673
    :cond_1
    iget-object v1, p0, LX/0Za;->a:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    rem-int/2addr v1, p2

    if-eqz v1, :cond_2

    .line 83674
    const/4 v0, 0x0

    goto :goto_0

    .line 83675
    :cond_2
    instance-of v1, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v1, :cond_3

    .line 83676
    check-cast p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sampling_frequency"

    invoke-virtual {p1, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 83677
    :cond_3
    instance-of v1, p1, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    if-eqz v1, :cond_0

    .line 83678
    check-cast p1, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    const-string v1, "sampling_frequency"

    invoke-virtual {p1, v1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;I)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    goto :goto_0
.end method
