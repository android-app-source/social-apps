.class public final LX/1nf;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Z

.field private static b:LX/1nf;

.field public static final c:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1nh;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "Landroid/graphics/drawable/ColorDrawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 317239
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/1nf;->a:Z

    .line 317240
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1nf;->c:LX/0Zi;

    .line 317241
    sget-boolean v0, LX/1nf;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, LX/0Zi;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    :goto_1
    sput-object v0, LX/1nf;->d:LX/0Zk;

    return-void

    .line 317242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 317243
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 317244
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 317245
    return-void
.end method

.method public static declared-synchronized a()LX/1nf;
    .locals 2

    .prologue
    .line 317246
    const-class v1, LX/1nf;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1nf;->b:LX/1nf;

    if-nez v0, :cond_0

    .line 317247
    new-instance v0, LX/1nf;

    invoke-direct {v0}, LX/1nf;-><init>()V

    sput-object v0, LX/1nf;->b:LX/1nf;

    .line 317248
    :cond_0
    sget-object v0, LX/1nf;->b:LX/1nf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 317249
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1De;)LX/1nh;
    .locals 2

    .prologue
    .line 317250
    new-instance v0, LX/1ng;

    invoke-direct {v0}, LX/1ng;-><init>()V

    .line 317251
    sget-object v1, LX/1nf;->c:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nh;

    .line 317252
    if-nez v1, :cond_0

    .line 317253
    new-instance v1, LX/1nh;

    invoke-direct {v1}, LX/1nh;-><init>()V

    .line 317254
    :cond_0
    invoke-virtual {v1, p0, v0}, LX/1nh;->a(LX/1De;LX/1ng;)V

    .line 317255
    move-object v0, v1

    .line 317256
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 317257
    const/4 v1, 0x0

    .line 317258
    sget-boolean v0, LX/1nf;->a:Z

    if-eqz v0, :cond_0

    .line 317259
    sget-object v0, LX/1nf;->d:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    move-object v1, v0

    .line 317260
    :cond_0
    if-nez v1, :cond_1

    .line 317261
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    move-object v0, p2

    check-cast v0, LX/1ng;

    iget v0, v0, LX/1ng;->a:I

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object v0, v1

    .line 317262
    :goto_0
    check-cast p2, LX/1ng;

    iget v1, p2, LX/1ng;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 317263
    return-object v0

    :cond_1
    move-object v0, p2

    .line 317264
    check-cast v0, LX/1ng;

    iget v0, v0, LX/1ng;->a:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 1

    .prologue
    .line 317265
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 317266
    sget-boolean v0, LX/1nf;->a:Z

    if-eqz v0, :cond_0

    .line 317267
    sget-object v0, LX/1nf;->d:LX/0Zk;

    check-cast p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-interface {v0, p2}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 317268
    :cond_0
    return-void
.end method
