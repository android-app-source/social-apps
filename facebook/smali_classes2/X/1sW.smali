.class public LX/1sW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1sX;


# instance fields
.field private final a:LX/1sY;

.field private final b:LX/1wi;


# direct methods
.method public constructor <init>(LX/1sY;LX/1wi;)V
    .locals 0

    .prologue
    .line 334956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334957
    iput-object p1, p0, LX/1sW;->a:LX/1sY;

    .line 334958
    iput-object p2, p0, LX/1sW;->b:LX/1wi;

    .line 334959
    return-void
.end method


# virtual methods
.method public final a(LX/EeS;LX/EeX;)V
    .locals 7

    .prologue
    .line 334931
    iget-object v0, p0, LX/1sW;->b:LX/1wi;

    iget-object v1, p0, LX/1sW;->a:LX/1sY;

    const/4 p1, 0x6

    const/4 p0, 0x0

    .line 334932
    iget-object v2, p2, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, p1}, LX/3CW;->c(II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x7

    invoke-static {v2, v3}, LX/3CW;->c(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 334933
    :cond_0
    new-instance v2, LX/2HB;

    iget-object v3, v0, LX/1wi;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 334934
    iget-object v3, v0, LX/1wi;->c:Landroid/content/res/Resources;

    const v4, 0x7f08366b

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, LX/1wi;->d:Ljava/lang/String;

    aput-object v6, v5, p0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 334935
    iget-object v3, p2, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, p1}, LX/3CW;->c(II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 334936
    const v3, 0x7f0217fe

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    .line 334937
    iget-object v3, v0, LX/1wi;->c:Landroid/content/res/Resources;

    const v4, 0x7f08366c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 334938
    iget-object v3, p2, LX/EeX;->operationUuid:Ljava/lang/String;

    .line 334939
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v1, LX/1sY;->a:Landroid/content/Context;

    const-class v6, Lcom/facebook/appupdate/AppUpdateService;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334940
    const-string v5, "start_install"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334941
    const-string v5, "operation_uuid"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334942
    iget-object v5, v1, LX/1sY;->a:Landroid/content/Context;

    invoke-static {v3}, LX/1sY;->a(Ljava/lang/String;)I

    move-result v3

    const/high16 v6, 0x8000000

    invoke-static {v5, v3, v4, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object v3, v3

    .line 334943
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 334944
    :goto_0
    iget-object v3, v0, LX/1wi;->b:Landroid/app/NotificationManager;

    const-string v4, "selfupdate_notification"

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v3, v4, p0, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 334945
    :goto_1
    return-void

    .line 334946
    :cond_1
    const v3, 0x7f0217fd

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    .line 334947
    iget-object v3, v0, LX/1wi;->c:Landroid/content/res/Resources;

    const v4, 0x7f08366d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 334948
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v1, LX/1sY;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/appupdate/AppUpdateService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334949
    const-string v4, "restart_download"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334950
    const-string v4, "operation_uuid"

    iget-object v5, p2, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334951
    iget-object v4, v1, LX/1sY;->a:Landroid/content/Context;

    iget-object v5, p2, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-static {v5}, LX/1sY;->a(Ljava/lang/String;)I

    move-result v5

    const/high16 v6, 0x8000000

    invoke-static {v4, v5, v3, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 334952
    move-object v3, v3

    .line 334953
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 334954
    goto :goto_0

    .line 334955
    :cond_2
    invoke-virtual {v0}, LX/1wi;->b()V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 334930
    const/4 v0, 0x0

    return v0
.end method
