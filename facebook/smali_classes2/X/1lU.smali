.class public final LX/1lU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/1RN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1lT;


# direct methods
.method public constructor <init>(LX/1lT;)V
    .locals 0

    .prologue
    .line 311965
    iput-object p1, p0, LX/1lU;->a:LX/1lT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 311959
    check-cast p1, LX/1RN;

    check-cast p2, LX/1RN;

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    .line 311960
    iget-object v0, p1, LX/1RN;->b:LX/1lP;

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 311961
    :goto_0
    iget-object v4, p1, LX/1RN;->b:LX/1lP;

    if-nez v4, :cond_1

    .line 311962
    :goto_1
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0

    .line 311963
    :cond_0
    iget-object v0, p1, LX/1RN;->b:LX/1lP;

    iget-wide v0, v0, LX/1lP;->d:D

    goto :goto_0

    .line 311964
    :cond_1
    iget-object v2, p2, LX/1RN;->b:LX/1lP;

    iget-wide v2, v2, LX/1lP;->d:D

    goto :goto_1
.end method
