.class public LX/1Io;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0Tn;

.field private final b:LX/0Tn;

.field private final c:LX/0Tn;

.field private final d:LX/0Tn;

.field private final e:LX/0Tn;

.field private final f:LX/0Tn;

.field private final g:LX/0Tn;

.field private final h:LX/0Tn;

.field private final i:LX/0Tn;

.field private final j:LX/0Tn;

.field public final k:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229243
    iput-object p1, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 229244
    sget-object v0, LX/1Ip;->c:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "EFFICIENCY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->a:LX/0Tn;

    .line 229245
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_URI"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->b:LX/0Tn;

    .line 229246
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_CONTENT_LENGTH"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->c:LX/0Tn;

    .line 229247
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_FETCH_TIME_MS"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->d:LX/0Tn;

    .line 229248
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_FIRST_UI_TIME_MS"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->e:LX/0Tn;

    .line 229249
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_IS_PREFETCH"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->f:LX/0Tn;

    .line 229250
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_IS_CANCELLATION_REQUESTED"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->g:LX/0Tn;

    .line 229251
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_FETCHER_CALLING_CLASS"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->h:LX/0Tn;

    .line 229252
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_FETCHER_ANALYTICS_TAG"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->i:LX/0Tn;

    .line 229253
    iget-object v0, p0, LX/1Io;->a:LX/0Tn;

    const-string v1, "KEY_FETCHER_FEATURE_TAG"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/1Io;->j:LX/0Tn;

    .line 229254
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1fm;J)LX/1fm;
    .locals 4

    .prologue
    .line 229255
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229256
    iget-object v0, p1, LX/1fm;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/1Io;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229257
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->e:LX/0Tn;

    invoke-interface {v0, v1, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 229258
    invoke-virtual {p0}, LX/1Io;->b()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 229259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;IJZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/1fm;
    .locals 3

    .prologue
    .line 229260
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229261
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->b:LX/0Tn;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->c:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->d:LX/0Tn;

    invoke-interface {v0, v1, p3, p4}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->f:LX/0Tn;

    invoke-interface {v0, v1, p5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->g:LX/0Tn;

    invoke-interface {v0, v1, p6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->h:LX/0Tn;

    invoke-interface {v0, v1, p7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->i:LX/0Tn;

    invoke-interface {v0, v1, p8}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->j:LX/0Tn;

    invoke-interface {v0, v1, p9}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 229262
    invoke-virtual {p0}, LX/1Io;->b()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 229263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0am;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "LX/1fm;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, -0x1

    .line 229264
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229265
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/1Io;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229266
    if-nez v0, :cond_0

    .line 229267
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 229268
    :goto_0
    monitor-exit p0

    return-object v0

    .line 229269
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/1Io;->e:LX/0Tn;

    const-wide/16 v4, -0x1

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 229270
    new-instance v1, LX/1fm;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, LX/1Io;->c:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, p0, LX/1Io;->d:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v0, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    cmp-long v0, v6, v10

    if-nez v0, :cond_1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    :goto_1
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v7, p0, LX/1Io;->f:LX/0Tn;

    const/4 v8, 0x0

    invoke-interface {v0, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v7

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v8, p0, LX/1Io;->g:LX/0Tn;

    const/4 v9, 0x0

    invoke-interface {v0, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v8

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v9, p0, LX/1Io;->h:LX/0Tn;

    const/4 v10, 0x0

    invoke-interface {v0, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v10, p0, LX/1Io;->i:LX/0Tn;

    const/4 v11, 0x0

    invoke-interface {v0, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v11, p0, LX/1Io;->j:LX/0Tn;

    const/4 v12, 0x0

    invoke-interface {v0, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, LX/1fm;-><init>(Landroid/net/Uri;IJLX/0am;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    goto :goto_1

    .line 229271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 229272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229273
    iget-object v0, p0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/1Io;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229274
    monitor-exit p0

    return-void

    .line 229275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
