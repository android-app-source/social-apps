.class public final LX/1XE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1WE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Landroid/graphics/Typeface;

.field public g:Landroid/text/Layout$Alignment;

.field public h:Z

.field public i:LX/1xz;

.field public j:I

.field public k:I

.field public l:I

.field public m:F

.field public final synthetic n:LX/1WE;


# direct methods
.method public constructor <init>(LX/1WE;)V
    .locals 1

    .prologue
    .line 270716
    iput-object p1, p0, LX/1XE;->n:LX/1WE;

    .line 270717
    move-object v0, p1

    .line 270718
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270719
    sget v0, LX/1XF;->b:I

    iput v0, p0, LX/1XE;->c:I

    .line 270720
    sget-object v0, LX/1XF;->e:Ljava/lang/Integer;

    iput-object v0, p0, LX/1XE;->d:Ljava/lang/Integer;

    .line 270721
    sget-object v0, LX/1XF;->f:Ljava/lang/Integer;

    iput-object v0, p0, LX/1XE;->e:Ljava/lang/Integer;

    .line 270722
    sget-object v0, LX/1XF;->c:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/1XE;->f:Landroid/graphics/Typeface;

    .line 270723
    sget-object v0, LX/1XF;->d:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/1XE;->g:Landroid/text/Layout$Alignment;

    .line 270724
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1XE;->h:Z

    .line 270725
    sget v0, LX/1XF;->g:I

    iput v0, p0, LX/1XE;->l:I

    .line 270726
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/1XE;->m:F

    .line 270727
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270728
    const-string v0, "ContentTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270729
    if-ne p0, p1, :cond_1

    .line 270730
    :cond_0
    :goto_0
    return v0

    .line 270731
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270732
    goto :goto_0

    .line 270733
    :cond_3
    check-cast p1, LX/1XE;

    .line 270734
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270735
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270736
    if-eq v2, v3, :cond_0

    .line 270737
    iget-object v2, p0, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270738
    goto :goto_0

    .line 270739
    :cond_5
    iget-object v2, p1, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 270740
    :cond_6
    iget-object v2, p0, LX/1XE;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1XE;->b:LX/1Pn;

    iget-object v3, p1, LX/1XE;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 270741
    goto :goto_0

    .line 270742
    :cond_8
    iget-object v2, p1, LX/1XE;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 270743
    :cond_9
    iget v2, p0, LX/1XE;->c:I

    iget v3, p1, LX/1XE;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 270744
    goto :goto_0

    .line 270745
    :cond_a
    iget-object v2, p0, LX/1XE;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/1XE;->d:Ljava/lang/Integer;

    iget-object v3, p1, LX/1XE;->d:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 270746
    goto :goto_0

    .line 270747
    :cond_c
    iget-object v2, p1, LX/1XE;->d:Ljava/lang/Integer;

    if-nez v2, :cond_b

    .line 270748
    :cond_d
    iget-object v2, p0, LX/1XE;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/1XE;->e:Ljava/lang/Integer;

    iget-object v3, p1, LX/1XE;->e:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 270749
    goto :goto_0

    .line 270750
    :cond_f
    iget-object v2, p1, LX/1XE;->e:Ljava/lang/Integer;

    if-nez v2, :cond_e

    .line 270751
    :cond_10
    iget-object v2, p0, LX/1XE;->f:Landroid/graphics/Typeface;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/1XE;->f:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/1XE;->f:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 270752
    goto :goto_0

    .line 270753
    :cond_12
    iget-object v2, p1, LX/1XE;->f:Landroid/graphics/Typeface;

    if-nez v2, :cond_11

    .line 270754
    :cond_13
    iget-object v2, p0, LX/1XE;->g:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/1XE;->g:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/1XE;->g:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 270755
    goto/16 :goto_0

    .line 270756
    :cond_15
    iget-object v2, p1, LX/1XE;->g:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_14

    .line 270757
    :cond_16
    iget-boolean v2, p0, LX/1XE;->h:Z

    iget-boolean v3, p1, LX/1XE;->h:Z

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 270758
    goto/16 :goto_0

    .line 270759
    :cond_17
    iget-object v2, p0, LX/1XE;->i:LX/1xz;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/1XE;->i:LX/1xz;

    iget-object v3, p1, LX/1XE;->i:LX/1xz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 270760
    goto/16 :goto_0

    .line 270761
    :cond_19
    iget-object v2, p1, LX/1XE;->i:LX/1xz;

    if-nez v2, :cond_18

    .line 270762
    :cond_1a
    iget v2, p0, LX/1XE;->j:I

    iget v3, p1, LX/1XE;->j:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 270763
    goto/16 :goto_0

    .line 270764
    :cond_1b
    iget v2, p0, LX/1XE;->k:I

    iget v3, p1, LX/1XE;->k:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 270765
    goto/16 :goto_0

    .line 270766
    :cond_1c
    iget v2, p0, LX/1XE;->l:I

    iget v3, p1, LX/1XE;->l:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 270767
    goto/16 :goto_0

    .line 270768
    :cond_1d
    iget v2, p0, LX/1XE;->m:F

    iget v3, p1, LX/1XE;->m:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 270769
    goto/16 :goto_0
.end method
