.class public LX/1Aa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1AX;

.field public final b:LX/1AY;

.field private final c:Landroid/os/Handler;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TV;",
            "LX/2oV",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zh",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Runnable;

.field public final h:LX/0ad;

.field public i:Z

.field private j:Z

.field public k:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:I

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zg;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/autoplay/VideoDisplayedCoordinator$VideoVisibilityChangedListener",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1AX;ZZLX/1AY;Landroid/os/Handler;LX/15L;LX/0ad;)V
    .locals 3
    .param p1    # LX/1AX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0xc8

    .line 210676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210677
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    .line 210678
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1Aa;->f:Ljava/util/Set;

    .line 210679
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Aa;->j:Z

    .line 210680
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210681
    iput v2, p0, LX/1Aa;->o:I

    .line 210682
    iput-object p4, p0, LX/1Aa;->b:LX/1AY;

    .line 210683
    iput-object p5, p0, LX/1Aa;->c:Landroid/os/Handler;

    .line 210684
    iput-object p1, p0, LX/1Aa;->a:LX/1AX;

    .line 210685
    iput-object p7, p0, LX/1Aa;->h:LX/0ad;

    .line 210686
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1Aa;->e:Ljava/util/List;

    .line 210687
    iget-object v0, p0, LX/1Aa;->h:LX/0ad;

    sget v1, LX/0ws;->eA:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1Aa;->o:I

    .line 210688
    new-instance v0, Lcom/facebook/feed/autoplay/VideoDisplayedCoordinator$1;

    invoke-direct {v0, p0, p3}, Lcom/facebook/feed/autoplay/VideoDisplayedCoordinator$1;-><init>(LX/1Aa;Z)V

    iput-object v0, p0, LX/1Aa;->g:Ljava/lang/Runnable;

    .line 210689
    if-eqz p2, :cond_0

    .line 210690
    iget-object v0, p6, LX/15L;->b:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210691
    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1Aa;->q:Ljava/util/List;

    .line 210692
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1Aa;->p:Ljava/util/List;

    .line 210693
    return-void

    .line 210694
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Aa;->j:Z

    goto :goto_0
.end method

.method public static a$redex0(LX/1Aa;J)V
    .locals 3

    .prologue
    .line 210605
    iget-object v0, p0, LX/1Aa;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/1Aa;->g:Ljava/lang/Runnable;

    const v2, -0x1ea61b43

    invoke-static {v0, v1, p1, p2, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 210606
    return-void
.end method

.method public static g(LX/1Aa;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 210673
    iget-object v0, p0, LX/1Aa;->l:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 210674
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1Aa;->l:Ljava/util/Set;

    .line 210675
    :cond_0
    iget-object v0, p0, LX/1Aa;->l:Ljava/util/Set;

    return-object v0
.end method

.method public static g(LX/1Aa;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 210668
    iget-object v0, p0, LX/1Aa;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 210669
    if-eqz v0, :cond_0

    .line 210670
    iget-object v0, p0, LX/1Aa;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D4B;

    .line 210671
    invoke-virtual {v0, p1}, LX/D4B;->b(Landroid/view/View;)V

    goto :goto_0

    .line 210672
    :cond_0
    return-void
.end method

.method public static declared-synchronized h(LX/1Aa;Landroid/view/View;)V
    .locals 2
    .param p0    # LX/1Aa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 210658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    iget-object v1, p0, LX/1Aa;->k:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oV;

    .line 210659
    if-eqz v0, :cond_0

    .line 210660
    iget-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    invoke-static {p0, v0}, LX/1Aa;->i(LX/1Aa;Landroid/view/View;)V

    .line 210661
    :cond_0
    iput-object p1, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210662
    if-eqz p1, :cond_1

    .line 210663
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oV;

    .line 210664
    if-eqz v0, :cond_1

    .line 210665
    invoke-virtual {v0, p1}, LX/2oV;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210666
    :cond_1
    monitor-exit p0

    return-void

    .line 210667
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static i(LX/1Aa;)V
    .locals 2

    .prologue
    .line 210656
    iget v0, p0, LX/1Aa;->o:I

    int-to-long v0, v0

    invoke-static {p0, v0, v1}, LX/1Aa;->a$redex0(LX/1Aa;J)V

    .line 210657
    return-void
.end method

.method private static i(LX/1Aa;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 210652
    iget-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 210653
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oV;

    invoke-virtual {v0, p1}, LX/2oV;->a(Ljava/lang/Object;)V

    .line 210654
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210655
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 210647
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Aa;->j:Z

    .line 210648
    iget-boolean v0, p0, LX/1Aa;->i:Z

    if-nez v0, :cond_0

    .line 210649
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Aa;->i:Z

    .line 210650
    int-to-long v0, p1

    invoke-static {p0, v0, v1}, LX/1Aa;->a$redex0(LX/1Aa;J)V

    .line 210651
    :cond_0
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 2

    .prologue
    .line 210695
    iget-boolean v0, p0, LX/1Aa;->i:Z

    if-eqz v0, :cond_1

    .line 210696
    iget-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 210697
    iget-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210698
    iget-object v1, p0, LX/1Aa;->k:Landroid/view/View;

    if-ne v1, v0, :cond_0

    .line 210699
    iget-object v1, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2oV;

    invoke-virtual {v1, v0, p1}, LX/2oV;->a(Ljava/lang/Object;LX/04g;)V

    .line 210700
    const/4 v1, 0x0

    iput-object v1, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210701
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Aa;->i:Z

    .line 210702
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Aa;->j:Z

    .line 210703
    return-void
.end method

.method public final a(LX/7zg;)V
    .locals 1

    .prologue
    .line 210645
    iget-object v0, p0, LX/1Aa;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210646
    return-void
.end method

.method public final a(LX/7zh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7zh",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 210643
    iget-object v0, p0, LX/1Aa;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210644
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 210637
    monitor-enter p0

    .line 210638
    :try_start_0
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210639
    invoke-static {p0, p1}, LX/1Aa;->i(LX/1Aa;Landroid/view/View;)V

    .line 210640
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210641
    :cond_0
    invoke-static {p0, p1}, LX/1Aa;->g(LX/1Aa;Landroid/view/View;)V

    .line 210642
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/View;LX/2oV;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/2oV",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 210622
    monitor-enter p0

    .line 210623
    :try_start_0
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210624
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oV;

    .line 210625
    iget-object v1, v0, LX/2oV;->a:Ljava/lang/String;

    move-object v0, v1

    .line 210626
    iget-object v1, p2, LX/2oV;->a:Ljava/lang/String;

    move-object v1, v1

    .line 210627
    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210628
    invoke-static {p0, p1}, LX/1Aa;->i(LX/1Aa;Landroid/view/View;)V

    .line 210629
    :cond_0
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210630
    iget-object v0, p0, LX/1Aa;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zg;

    .line 210631
    invoke-interface {v0, p2}, LX/7zg;->a(LX/2oV;)V

    goto :goto_0

    .line 210632
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210633
    iget-boolean v0, p0, LX/1Aa;->i:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/1Aa;->j:Z

    if-nez v0, :cond_2

    .line 210634
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Aa;->i:Z

    .line 210635
    invoke-static {p0}, LX/1Aa;->i(LX/1Aa;)V

    .line 210636
    :cond_2
    return-void
.end method

.method public final b(LX/7zh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7zh",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 210620
    iget-object v0, p0, LX/1Aa;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 210621
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 210609
    monitor-enter p0

    .line 210610
    :try_start_0
    iget-object v0, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 210611
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 210612
    check-cast v0, Landroid/view/View;

    .line 210613
    invoke-static {p0, v0}, LX/1Aa;->i(LX/1Aa;Landroid/view/View;)V

    .line 210614
    iget-object v4, p0, LX/1Aa;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210615
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 210616
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210617
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Aa;->k:Landroid/view/View;

    .line 210618
    return-void

    .line 210619
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 210607
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0}, LX/1Aa;->a(LX/04g;)V

    .line 210608
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 210603
    iget v0, p0, LX/1Aa;->o:I

    invoke-virtual {p0, v0}, LX/1Aa;->a(I)V

    .line 210604
    return-void
.end method
