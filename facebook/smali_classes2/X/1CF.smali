.class public LX/1CF;
.super LX/14l;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/1CH;


# instance fields
.field public c:LX/1CH;

.field public d:LX/0qm;

.field private final e:LX/0Sh;

.field public final f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field public final g:LX/03V;

.field private final h:LX/0SG;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3H7;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/15W;

.field private final k:LX/0ad;

.field private final l:LX/1CJ;

.field public final m:LX/0qn;

.field private final n:LX/1CK;

.field private final o:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final p:LX/1CL;

.field private final q:LX/0fO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215879
    const-class v0, LX/1CF;

    sput-object v0, LX/1CF;->a:Ljava/lang/Class;

    .line 215880
    new-instance v0, LX/1CG;

    invoke-direct {v0}, LX/1CG;-><init>()V

    sput-object v0, LX/1CF;->b:LX/1CH;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Sh;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/03V;LX/0SG;LX/0Ot;LX/15W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1CJ;LX/0qn;LX/1CK;LX/1CL;LX/0fO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/3H7;",
            ">;",
            "LX/15W;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ad;",
            "LX/1CJ;",
            "LX/0qn;",
            "LX/1CK;",
            "LX/1CL;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215863
    invoke-static {}, LX/1CF;->e()Landroid/content/IntentFilter;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/14l;-><init>(Landroid/content/Context;Landroid/content/IntentFilter;)V

    .line 215864
    iput-object p2, p0, LX/1CF;->e:LX/0Sh;

    .line 215865
    iput-object p3, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 215866
    iput-object p4, p0, LX/1CF;->g:LX/03V;

    .line 215867
    sget-object v0, LX/1CF;->b:LX/1CH;

    iput-object v0, p0, LX/1CF;->c:LX/1CH;

    .line 215868
    iput-object p5, p0, LX/1CF;->h:LX/0SG;

    .line 215869
    iput-object p6, p0, LX/1CF;->i:LX/0Ot;

    .line 215870
    iput-object p7, p0, LX/1CF;->j:LX/15W;

    .line 215871
    iput-object p8, p0, LX/1CF;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 215872
    iput-object p9, p0, LX/1CF;->k:LX/0ad;

    .line 215873
    iput-object p10, p0, LX/1CF;->l:LX/1CJ;

    .line 215874
    iput-object p11, p0, LX/1CF;->m:LX/0qn;

    .line 215875
    iput-object p12, p0, LX/1CF;->n:LX/1CK;

    .line 215876
    iput-object p13, p0, LX/1CF;->p:LX/1CL;

    .line 215877
    iput-object p14, p0, LX/1CF;->q:LX/0fO;

    .line 215878
    return-void
.end method

.method public static a(LX/0QB;)LX/1CF;
    .locals 1

    .prologue
    .line 215692
    invoke-static {p0}, LX/1CF;->b(LX/0QB;)LX/1CF;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1CF;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 215844
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 215845
    iput-object v2, v4, LX/3dM;->D:Ljava/lang/String;

    .line 215846
    move-object v2, v4

    .line 215847
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, LX/3dM;->c(Z)LX/3dM;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/3dM;->g(Z)LX/3dM;

    move-result-object v2

    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 215848
    iput-object v2, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 215849
    move-object v2, v3

    .line 215850
    sget-object v3, LX/0SF;->a:LX/0SF;

    move-object v3, v3

    .line 215851
    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    .line 215852
    iput-wide v4, v2, LX/23u;->G:J

    .line 215853
    move-object v2, v2

    .line 215854
    iput-object p3, v2, LX/23u;->T:Ljava/lang/String;

    .line 215855
    move-object v2, v2

    .line 215856
    invoke-static {p1, p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    .line 215857
    iput-object v3, v2, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 215858
    move-object v2, v2

    .line 215859
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object v0, v2

    .line 215860
    invoke-static {p0, p2, p3, v0}, LX/1CF;->a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 215861
    iget-object v1, p0, LX/1CF;->c:LX/1CH;

    invoke-interface {v1, v0}, LX/1CH;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 215862
    return-void
.end method

.method private static a(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 215817
    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p1, p2}, LX/0qm;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 215818
    if-nez v1, :cond_0

    .line 215819
    :goto_0
    return-void

    .line 215820
    :cond_0
    const-string v0, "com.facebook.STREAM_PUBLISH_MEDIA_PROCESSING_COMPLETE"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 215821
    if-eqz p3, :cond_4

    .line 215822
    invoke-static {v1, p3, p2}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 215823
    iget-object v0, p0, LX/1CF;->l:LX/1CJ;

    iget-object v4, p0, LX/1CF;->l:LX/1CJ;

    invoke-virtual {v4, v1}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v1

    .line 215824
    iget-object v4, v0, LX/1CJ;->a:LX/0aq;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result p4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {v4, p4, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215825
    iget-object v1, p0, LX/1CF;->m:LX/0qn;

    if-eqz v2, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    :goto_1
    invoke-virtual {v1, v3, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215826
    iget-object v0, p0, LX/1CF;->k:LX/0ad;

    sget-short v1, LX/1RY;->x:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215827
    iget-object v0, p0, LX/1CF;->n:LX/1CK;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v3, v5, v1}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 215828
    :cond_1
    invoke-static {p0, p1, p2, v3}, LX/1CF;->a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 215829
    if-eqz v2, :cond_2

    invoke-static {p3}, LX/17E;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "own_timeline"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215830
    iget-object v0, p0, LX/1CF;->p:LX/1CL;

    .line 215831
    iget-object v1, v0, LX/1CL;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    const-string v2, "176016256154136"

    .line 215832
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 215833
    move-object v1, v1

    .line 215834
    iget-object v2, v0, LX/1CL;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0gt;->a(Landroid/content/Context;)V

    .line 215835
    :cond_2
    iget-object v0, p0, LX/1CF;->c:LX/1CH;

    invoke-interface {v0, v3}, LX/1CH;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 215836
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_1

    .line 215837
    :cond_4
    invoke-static {v1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v1}, LX/17E;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v1}, LX/17E;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v1}, LX/17E;->p(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 215838
    :cond_5
    iget-object v0, p0, LX/1CF;->m:LX/0qn;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215839
    invoke-static {p0, v1, p1, p2}, LX/1CF;->a(LX/1CF;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 215840
    :cond_6
    invoke-static {v1}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 215841
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non-album story was not pre-fetched! requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", postId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215842
    :cond_7
    iget-object v0, p0, LX/1CF;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3H7;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/3H7;->a(Ljava/lang/String;LX/0rS;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 215843
    iget-object v2, p0, LX/1CF;->e:LX/0Sh;

    new-instance v3, LX/99x;

    invoke-direct {v3, p0, v1, p1, p2}, LX/99x;-><init>(LX/1CF;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0
.end method

.method public static a(LX/1CF;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 215815
    iget-object v1, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v1, p1}, LX/0qm;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215816
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v1, p2}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 215751
    const-string v0, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215752
    const-string v0, "extra_feed_story"

    invoke-static {p4, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 215753
    invoke-static {p0, p2, p3}, LX/1CF;->a(LX/1CF;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 215754
    iget-object v1, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v1, p2}, LX/0qm;->a(Ljava/lang/String;)V

    .line 215755
    iget-object v1, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v1, p3}, LX/0qm;->b(Ljava/lang/String;)Z

    .line 215756
    :cond_0
    invoke-static {p0, p2, p3, v0}, LX/1CF;->a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    :cond_1
    :goto_0
    move v0, v6

    .line 215757
    :goto_1
    return v0

    .line 215758
    :cond_2
    if-nez v0, :cond_0

    .line 215759
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pending story is null with request id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 215760
    goto :goto_1

    .line 215761
    :cond_3
    const-string v0, "com.facebook.STREAM_PUBLISH_PROGRESS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 215762
    invoke-static {p0, p2, p3}, LX/1CF;->a(LX/1CF;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 215763
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    const-string v2, "Pending story doesn\'t exist with request id %s (publish_begin)"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 215764
    goto :goto_1

    .line 215765
    :cond_4
    iget-object v0, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 215766
    const-string v2, "extra_percent_progress"

    invoke-virtual {p4, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 215767
    if-eqz v0, :cond_1

    .line 215768
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 215769
    iget-object v3, p0, LX/1CF;->m:LX/0qn;

    invoke-virtual {v3, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v3, v4, :cond_5

    .line 215770
    iget-object v3, p0, LX/1CF;->m:LX/0qn;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v3, v2, v4}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215771
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v2

    if-nez v2, :cond_6

    .line 215772
    iget-object v2, p0, LX/1CF;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v6}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 215773
    :cond_6
    iget-object v2, p0, LX/1CF;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 215774
    iget-object v4, v0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v4}, LX/4Zi;->d()Z

    move-result v4

    if-nez v4, :cond_e

    .line 215775
    :goto_2
    goto :goto_0

    .line 215776
    :cond_7
    const-string v0, "com.facebook.STREAM_PUBLISH_RESTART"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 215777
    iget-object v0, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 215778
    if-eqz v0, :cond_1

    .line 215779
    iget-object v1, p0, LX/1CF;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 215780
    if-eqz v6, :cond_f

    iget v1, v0, Lcom/facebook/composer/publish/common/PendingStory;->c:I

    .line 215781
    :goto_3
    iget-object v4, v0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    .line 215782
    invoke-virtual {v4}, LX/4Zi;->a()V

    .line 215783
    const/4 v5, 0x1

    iput-boolean v5, v4, LX/4Zi;->k:Z

    .line 215784
    invoke-virtual {v4, v2, v3, v1}, LX/4Zi;->a(JI)V

    .line 215785
    goto/16 :goto_0

    .line 215786
    :cond_8
    const-string v0, "graphql_story"

    invoke-static {p4, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 215787
    const-string v0, "extra_result"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v0

    .line 215788
    invoke-static {p0, p2, p3}, LX/1CF;->a(LX/1CF;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 215789
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    const-string v2, "Pending story doesn\'t exist with request id %s (publish_fail)"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 215790
    goto/16 :goto_1

    .line 215791
    :cond_9
    if-nez p3, :cond_d

    .line 215792
    sget-object v2, LX/7m7;->EXCEPTION:LX/7m7;

    if-ne v0, v2, :cond_c

    .line 215793
    const-string v0, "extra_error_details"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 215794
    if-nez v0, :cond_a

    .line 215795
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    .line 215796
    :cond_a
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215797
    iget-object v2, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v2, p2, p3}, LX/0qm;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 215798
    if-nez v3, :cond_10

    .line 215799
    :cond_b
    :goto_4
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    const-string v2, "Pending story doesn\'t exist with request id %s (id is null)"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 215800
    goto/16 :goto_1

    .line 215801
    :cond_c
    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p2}, LX/0qm;->a(Ljava/lang/String;)V

    .line 215802
    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p3}, LX/0qm;->b(Ljava/lang/String;)Z

    goto :goto_4

    .line 215803
    :cond_d
    const-string v0, "extra_target_type"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p1

    .line 215804
    invoke-static/range {v0 .. v5}, LX/1CF;->a(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 215805
    :cond_e
    iget-object v4, v0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    const/16 v5, 0x3de

    iget p0, v0, Lcom/facebook/composer/publish/common/PendingStory;->c:I

    invoke-static {p0, v1}, Ljava/lang/Math;->max(II)I

    move-result p0

    invoke-static {v5, p0}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, v2, v3, v5}, LX/4Zi;->c(JI)V

    goto/16 :goto_2

    .line 215806
    :cond_f
    const/16 v1, 0x320

    goto/16 :goto_3

    .line 215807
    :cond_10
    iget-object v2, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v4

    .line 215808
    if-eqz v4, :cond_b

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 215809
    iget-boolean v2, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->isVideoTranscodingError:Z

    if-eqz v2, :cond_11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 215810
    :goto_5
    iget-object v5, p0, LX/1CF;->m:LX/0qn;

    invoke-virtual {v5, v3, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215811
    iget-object v2, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setErrorDetails(Lcom/facebook/composer/publish/common/ErrorDetails;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    .line 215812
    invoke-static {v2, v3, v5, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 215813
    goto :goto_4

    .line 215814
    :cond_11
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_5
.end method

.method public static a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 215748
    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p2}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p3}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215749
    :goto_0
    return-void

    .line 215750
    :cond_0
    iget-object v0, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v0, p1, p3}, LX/0qm;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1CF;
    .locals 15

    .prologue
    .line 215746
    new-instance v0, LX/1CF;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x1296

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v7

    check-cast v7, LX/15W;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/1CJ;->a(LX/0QB;)LX/1CJ;

    move-result-object v10

    check-cast v10, LX/1CJ;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v11

    check-cast v11, LX/0qn;

    invoke-static {p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v12

    check-cast v12, LX/1CK;

    invoke-static {p0}, LX/1CL;->b(LX/0QB;)LX/1CL;

    move-result-object v13

    check-cast v13, LX/1CL;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v14

    check-cast v14, LX/0fO;

    invoke-direct/range {v0 .. v14}, LX/1CF;-><init>(Landroid/content/Context;LX/0Sh;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/03V;LX/0SG;LX/0Ot;LX/15W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1CJ;LX/0qn;LX/1CK;LX/1CL;LX/0fO;)V

    .line 215747
    return-object v0
.end method

.method private static e()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 215740
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 215741
    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215742
    const-string v1, "com.facebook.STREAM_PUBLISH_PROGRESS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215743
    const-string v1, "com.facebook.STREAM_PUBLISH_RESTART"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215744
    const-string v1, "com.facebook.STREAM_PUBLISH_MEDIA_PROCESSING_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 215745
    return-object v0
.end method


# virtual methods
.method public final a(LX/1CH;LX/0qm;)V
    .locals 11

    .prologue
    .line 215723
    iput-object p1, p0, LX/1CF;->c:LX/1CH;

    .line 215724
    iput-object p2, p0, LX/1CF;->d:LX/0qm;

    .line 215725
    invoke-super {p0}, LX/14l;->a()V

    .line 215726
    iget-object v0, p0, LX/1CF;->k:LX/0ad;

    sget-short v1, LX/1EB;->an:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1CF;->q:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215727
    :cond_0
    iget-object v3, p0, LX/1CF;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v5

    .line 215728
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 215729
    :cond_1
    return-void

    .line 215730
    :cond_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215731
    iget-object v7, p0, LX/1CF;->c:LX/1CH;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v9

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-interface {v7, v9, v10, v8}, LX/1CH;->a(JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 215732
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v7

    .line 215733
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v8

    .line 215734
    invoke-static {p0, v8, v7}, LX/1CF;->a(LX/1CF;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 215735
    iget-object v9, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v9, v8}, LX/0qm;->a(Ljava/lang/String;)V

    .line 215736
    iget-object v9, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v9, v7}, LX/0qm;->b(Ljava/lang/String;)Z

    .line 215737
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    invoke-static {p0, v8, v7, v9}, LX/1CF;->a$redex0(LX/1CF;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 215738
    iget-object v7, p0, LX/1CF;->c:LX/1CH;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1CH;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 215739
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 215696
    iget-object v0, p0, LX/1CF;->j:LX/15W;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->POST_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 215697
    const-class v2, LX/0i1;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 215698
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 215699
    const-string v0, "extra_request_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 215700
    if-nez v4, :cond_1

    .line 215701
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    const-string v1, "Story published does not have a request id"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 215702
    :cond_0
    :goto_0
    return-void

    .line 215703
    :cond_1
    const-string v0, "extra_legacy_api_post_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215704
    const-string v0, "extra_feed_story"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 215705
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 215706
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215707
    :cond_2
    iget-object v2, p0, LX/1CF;->d:LX/0qm;

    invoke-virtual {v2, v4, v1}, LX/0qm;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 215708
    if-eqz v2, :cond_3

    move-object v0, v2

    .line 215709
    :cond_3
    if-nez v0, :cond_4

    .line 215710
    sget-object v0, LX/1CF;->a:Ljava/lang/Class;

    const-string v1, "No story available to show. Action:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 215711
    :cond_4
    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 215712
    iget-object v2, p0, LX/1CF;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 215713
    iget-object v2, p0, LX/1CF;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v5, LX/0dp;->o:LX/0Tn;

    invoke-interface {v2, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 215714
    invoke-static {v0}, LX/17E;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 215715
    iget-object v2, p0, LX/1CF;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v5, LX/0dp;->s:LX/0Tn;

    invoke-interface {v2, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 215716
    :cond_5
    const-string v2, "extra_target_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p2, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 215717
    iget-object v2, p0, LX/1CF;->c:LX/1CH;

    invoke-interface {v2, v6, v7, v0}, LX/1CH;->a(JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215718
    invoke-direct {p0, v3, v4, v1, p2}, LX/1CF;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v1

    .line 215719
    if-nez v1, :cond_6

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 215720
    iget-object v0, p0, LX/1CF;->c:LX/1CH;

    invoke-interface {v0}, LX/1CH;->a()V

    goto/16 :goto_0

    .line 215721
    :cond_6
    if-eqz v1, :cond_0

    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215722
    iget-object v1, p0, LX/1CF;->c:LX/1CH;

    invoke-interface {v1, v0}, LX/1CH;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 215693
    invoke-super {p0}, LX/14l;->b()V

    .line 215694
    sget-object v0, LX/1CF;->b:LX/1CH;

    iput-object v0, p0, LX/1CF;->c:LX/1CH;

    .line 215695
    return-void
.end method
