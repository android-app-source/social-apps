.class public LX/1FQ;
.super LX/1FR;
.source ""

# interfaces
.implements LX/1FS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1FR",
        "<[B>;",
        "Lcom/facebook/imagepipeline/memory/ByteArrayPool;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final g:[I


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;LX/1F0;)V
    .locals 4

    .prologue
    .line 222302
    invoke-direct {p0, p1, p2, p3}, LX/1FR;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    .line 222303
    iget-object v1, p2, LX/1F7;->c:Landroid/util/SparseIntArray;

    .line 222304
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/1FQ;->g:[I

    .line 222305
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 222306
    iget-object v2, p0, LX/1FQ;->g:[I

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    aput v3, v2, v0

    .line 222307
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222308
    :cond_0
    invoke-virtual {p0}, LX/1FR;->a()V

    .line 222309
    return-void
.end method


# virtual methods
.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 222310
    new-array v0, p1, [B

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 222311
    check-cast p1, [B

    .line 222312
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222313
    return-void
.end method

.method public final c(I)I
    .locals 4

    .prologue
    .line 222314
    if-gtz p1, :cond_0

    .line 222315
    new-instance v0, LX/4eK;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4eK;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 222316
    :cond_0
    iget-object v2, p0, LX/1FQ;->g:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v0, v2, v1

    .line 222317
    if-lt v0, p1, :cond_2

    move p1, v0

    .line 222318
    :cond_1
    return p1

    .line 222319
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 222320
    check-cast p1, [B

    .line 222321
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222322
    array-length v0, p1

    return v0
.end method

.method public final d(I)I
    .locals 0

    .prologue
    .line 222323
    return p1
.end method
