.class public LX/0Yq;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadSuperClassSQLiteOpenHelper.SharedSQLiteOpenHelper"
    }
.end annotation


# instance fields
.field private final a:LX/0Yr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILandroid/database/DatabaseErrorHandler;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "LX/0Tw;",
            ">;I",
            "Landroid/database/DatabaseErrorHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82509
    const/4 v3, 0x0

    const/16 v4, 0xc8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)V

    .line 82510
    new-instance v0, LX/0Yr;

    invoke-direct {v0, p3, p4, p1}, LX/0Yr;-><init>(Ljava/util/List;ILandroid/content/Context;)V

    iput-object v0, p0, LX/0Yq;->a:LX/0Yr;

    .line 82511
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 82497
    const-string v0, "CREATE TABLE _shared_version (name TEXT PRIMARY KEY, version INTEGER)"

    const p0, 0x74d008ca

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x66c3cba7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 82498
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 82507
    iget-object v0, p0, LX/0Yq;->a:LX/0Yr;

    invoke-virtual {v0, p1}, LX/0Yr;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 82508
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 82499
    iget-object v0, p0, LX/0Yq;->a:LX/0Yr;

    .line 82500
    const/16 v1, 0xc8

    if-ge p2, v1, :cond_0

    .line 82501
    const-string v1, "CREATE TABLE _shared_version (name TEXT PRIMARY KEY, version INTEGER)"

    const v2, 0x796aed36

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x45f9cef2

    invoke-static {v1}, LX/03h;->a(I)V

    .line 82502
    iget-object v1, v0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    iget-object v1, v0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tw;

    .line 82503
    iget-object p3, v1, LX/0Tw;->a:Ljava/lang/String;

    move-object v1, p3

    .line 82504
    invoke-static {p1, v1, p2}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 82505
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 82506
    :cond_0
    return-void
.end method
