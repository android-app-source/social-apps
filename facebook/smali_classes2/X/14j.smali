.class public final enum LX/14j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/14j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/14j;

.field public static final enum ACTIVE:LX/14j;

.field public static final enum BACKGROUNDED:LX/14j;

.field public static final enum FOREGROUNDED:LX/14j;

.field public static final enum INIT:LX/14j;

.field public static final enum LAUNCH:LX/14j;

.field public static final enum RESIGN:LX/14j;


# instance fields
.field private stateName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179153
    new-instance v0, LX/14j;

    const-string v1, "INIT"

    const-string v2, "init"

    invoke-direct {v0, v1, v4, v2}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->INIT:LX/14j;

    .line 179154
    new-instance v0, LX/14j;

    const-string v1, "LAUNCH"

    const-string v2, "launch"

    invoke-direct {v0, v1, v5, v2}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->LAUNCH:LX/14j;

    .line 179155
    new-instance v0, LX/14j;

    const-string v1, "ACTIVE"

    const-string v2, "active"

    invoke-direct {v0, v1, v6, v2}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->ACTIVE:LX/14j;

    .line 179156
    new-instance v0, LX/14j;

    const-string v1, "RESIGN"

    const-string v2, "resign"

    invoke-direct {v0, v1, v7, v2}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->RESIGN:LX/14j;

    .line 179157
    new-instance v0, LX/14j;

    const-string v1, "FOREGROUNDED"

    const-string v2, "foreground"

    invoke-direct {v0, v1, v8, v2}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->FOREGROUNDED:LX/14j;

    .line 179158
    new-instance v0, LX/14j;

    const-string v1, "BACKGROUNDED"

    const/4 v2, 0x5

    const-string v3, "background"

    invoke-direct {v0, v1, v2, v3}, LX/14j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/14j;->BACKGROUNDED:LX/14j;

    .line 179159
    const/4 v0, 0x6

    new-array v0, v0, [LX/14j;

    sget-object v1, LX/14j;->INIT:LX/14j;

    aput-object v1, v0, v4

    sget-object v1, LX/14j;->LAUNCH:LX/14j;

    aput-object v1, v0, v5

    sget-object v1, LX/14j;->ACTIVE:LX/14j;

    aput-object v1, v0, v6

    sget-object v1, LX/14j;->RESIGN:LX/14j;

    aput-object v1, v0, v7

    sget-object v1, LX/14j;->FOREGROUNDED:LX/14j;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/14j;->BACKGROUNDED:LX/14j;

    aput-object v2, v0, v1

    sput-object v0, LX/14j;->$VALUES:[LX/14j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 179160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 179161
    iput-object p3, p0, LX/14j;->stateName:Ljava/lang/String;

    .line 179162
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/14j;
    .locals 1

    .prologue
    .line 179152
    const-class v0, LX/14j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/14j;

    return-object v0
.end method

.method public static values()[LX/14j;
    .locals 1

    .prologue
    .line 179151
    sget-object v0, LX/14j;->$VALUES:[LX/14j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/14j;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179150
    iget-object v0, p0, LX/14j;->stateName:Ljava/lang/String;

    return-object v0
.end method
