.class public LX/18y;
.super Landroid/widget/FrameLayout;
.source ""


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 207113
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 207114
    return-void
.end method

.method public static a(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 207115
    instance-of v0, p0, LX/0h0;

    if-eqz v0, :cond_0

    .line 207116
    check-cast p0, LX/0h0;

    .line 207117
    const/4 v0, 0x0

    invoke-interface {p0, v0}, LX/0h0;->setSaveFromParentEnabledCompat(Z)V

    .line 207118
    invoke-interface {p0}, LX/0h0;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    .line 207119
    :goto_0
    return-object v0

    .line 207120
    :cond_0
    new-instance v0, LX/18y;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/18y;-><init>(Landroid/content/Context;)V

    .line 207121
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 207122
    if-eqz v1, :cond_1

    .line 207123
    invoke-virtual {v0, v1}, LX/18y;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207124
    :cond_1
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 207125
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207126
    invoke-virtual {v0, p0}, LX/18y;->addView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207127
    invoke-virtual {p0, p1}, LX/18y;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 207128
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207129
    invoke-virtual {p0, p1}, LX/18y;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 207130
    return-void
.end method
