.class public LX/1RO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1RJ;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AyS;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AzD;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1bQ;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axw;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BMP;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B5l;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1RP;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 246145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246146
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246147
    iput-object v0, p0, LX/1RO;->b:LX/0Ot;

    .line 246148
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246149
    iput-object v0, p0, LX/1RO;->c:LX/0Ot;

    .line 246150
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246151
    iput-object v0, p0, LX/1RO;->e:LX/0Ot;

    .line 246152
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246153
    iput-object v0, p0, LX/1RO;->f:LX/0Ot;

    .line 246154
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246155
    iput-object v0, p0, LX/1RO;->g:LX/0Ot;

    .line 246156
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 246157
    iput-object v0, p0, LX/1RO;->h:LX/0Ot;

    .line 246158
    new-instance v0, LX/1RP;

    invoke-direct {v0, p0}, LX/1RP;-><init>(LX/1RO;)V

    iput-object v0, p0, LX/1RO;->i:LX/1RP;

    .line 246159
    return-void
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 3

    .prologue
    .line 246142
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246143
    instance-of v1, v0, LX/Ayb;

    const-string v2, "Bug in framework."

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 246144
    new-instance v1, LX/AyT;

    check-cast v0, LX/Ayb;

    invoke-direct {v1, p0, v0}, LX/AyT;-><init>(LX/1RO;LX/Ayb;)V

    return-object v1
.end method

.method public final a(LX/1RN;)V
    .locals 0

    .prologue
    .line 246141
    return-void
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 4
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246124
    iget-object v0, p0, LX/1RO;->i:LX/1RP;

    .line 246125
    iput-object p2, v0, LX/1RP;->b:LX/1RN;

    .line 246126
    check-cast p1, LX/2xq;

    .line 246127
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    iget-object v1, p0, LX/1RO;->i:LX/1RP;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246128
    iget-object v0, p1, LX/2xq;->c:LX/AkM;

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    .line 246129
    iget-object v1, p1, LX/2xq;->b:LX/AkL;

    check-cast v1, LX/AyT;

    .line 246130
    iget-object v2, v1, LX/AyT;->b:Landroid/net/Uri;

    iget-object v3, v1, LX/AyT;->c:Ljava/lang/String;

    iget-object v1, v1, LX/AyT;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/text/SpannableStringBuilder;)V

    .line 246131
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v1

    .line 246132
    instance-of v0, v1, LX/Ayb;

    const-string v2, "Didn\'t get a souvenir prompt object"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move-object v0, v1

    .line 246133
    check-cast v0, LX/Ayb;

    .line 246134
    iget-boolean v2, v0, LX/Ayb;->b:Z

    move v2, v2

    .line 246135
    if-nez v2, :cond_0

    .line 246136
    iget-object v2, p0, LX/1RO;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AzD;

    invoke-virtual {v2, v0}, LX/AzD;->b(LX/Ayb;)V

    .line 246137
    const/4 v2, 0x1

    .line 246138
    iput-boolean v2, v0, LX/Ayb;->b:Z

    .line 246139
    iget-object v0, p0, LX/1RO;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axw;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Axw;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 246140
    :cond_0
    return-void
.end method

.method public final a(ZLX/1RN;)V
    .locals 0

    .prologue
    .line 246160
    return-void
.end method

.method public final b(LX/1RN;)V
    .locals 0

    .prologue
    .line 246123
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 246118
    iget-object v0, p0, LX/1RO;->i:LX/1RP;

    .line 246119
    iput-object v1, v0, LX/1RP;->b:LX/1RN;

    .line 246120
    check-cast p1, LX/2xq;

    .line 246121
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246122
    return-void
.end method

.method public final c(LX/1RN;)V
    .locals 0

    .prologue
    .line 246117
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 246115
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246116
    instance-of v0, v0, LX/Ayb;

    return v0
.end method
