.class public final LX/1A2;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 209970
    new-instance v0, LX/0su;

    sget-object v1, LX/1A3;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/1A2;->a:LX/0sv;

    .line 209971
    sget-object v0, LX/1A3;->a:LX/0U1;

    sget-object v1, LX/1A3;->b:LX/0U1;

    sget-object v2, LX/1A3;->c:LX/0U1;

    sget-object v3, LX/1A3;->d:LX/0U1;

    sget-object v4, LX/1A3;->e:LX/0U1;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/1A2;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 209977
    const-string v0, "saved_video_stories"

    sget-object v1, LX/1A2;->b:LX/0Px;

    sget-object v2, LX/1A2;->a:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 209978
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 209972
    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    .line 209973
    const-string v0, "ALTER TABLE saved_video_stories ADD COLUMN last_updated LONG"

    const v1, -0x198e11fa

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x18f54e70

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209974
    :cond_0
    const/4 v0, 0x3

    if-ge p2, v0, :cond_1

    .line 209975
    const-string v0, "ALTER TABLE saved_video_stories ADD COLUMN seen_state TEXT"

    const v1, -0x6d4a095b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x26b54dd6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209976
    :cond_1
    return-void
.end method
