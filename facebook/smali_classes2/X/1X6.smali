.class public final LX/1X6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1Ua;

.field public final c:I

.field public final d:I

.field public final e:LX/1X9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ua;)V
    .locals 1

    .prologue
    .line 270475
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 270476
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Ua;",
            ")V"
        }
    .end annotation

    .prologue
    .line 270477
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 270478
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Ua;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 270479
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    .line 270480
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V
    .locals 0
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1X9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Ua;",
            "II",
            "LX/1X9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 270481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270482
    iput-object p5, p0, LX/1X6;->e:LX/1X9;

    .line 270483
    iput-object p1, p0, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 270484
    iput-object p2, p0, LX/1X6;->b:LX/1Ua;

    .line 270485
    iput p3, p0, LX/1X6;->c:I

    .line 270486
    iput p4, p0, LX/1X6;->d:I

    .line 270487
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Ua;",
            "LX/1X9;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 270488
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    .line 270489
    return-void
.end method
