.class public LX/0w3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Landroid/graphics/Rect;

.field private static h:LX/0Xm;


# instance fields
.field private final b:I

.field private c:I

.field private d:Z

.field public e:I

.field public f:Z

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158584
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/0w3;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 158607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158608
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/0w3;->g:I

    .line 158609
    const v0, 0x7f0b01ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/0w3;->b:I

    .line 158610
    invoke-direct {p0, p1}, LX/0w3;->a(Landroid/content/res/Resources;)V

    .line 158611
    return-void
.end method

.method public static a(LX/0QB;)LX/0w3;
    .locals 4

    .prologue
    .line 158612
    const-class v1, LX/0w3;

    monitor-enter v1

    .line 158613
    :try_start_0
    sget-object v0, LX/0w3;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 158614
    sput-object v2, LX/0w3;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 158615
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158616
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 158617
    new-instance p0, LX/0w3;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/0w3;-><init>(Landroid/content/res/Resources;)V

    .line 158618
    move-object v0, p0

    .line 158619
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 158620
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0w3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158621
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 158622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 158605
    const v0, 0x7f0b01ae

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/0w3;->e:I

    .line 158606
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158585
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 158586
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 158587
    iget v4, p0, LX/0w3;->g:I

    if-eq v0, v4, :cond_0

    .line 158588
    iput v0, p0, LX/0w3;->g:I

    .line 158589
    invoke-direct {p0, v3}, LX/0w3;->a(Landroid/content/res/Resources;)V

    .line 158590
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 158591
    iget v4, p0, LX/0w3;->c:I

    if-ne v0, v4, :cond_1

    iget-boolean v4, p0, LX/0w3;->d:Z

    if-eqz v4, :cond_2

    .line 158592
    :cond_1
    sget-object v4, LX/0w3;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v4}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 158593
    sget-object v4, LX/0w3;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-lez v4, :cond_5

    .line 158594
    iput v0, p0, LX/0w3;->c:I

    .line 158595
    sget-object v4, LX/0w3;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-le v0, v4, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/0w3;->d:Z

    .line 158596
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 158597
    sget-object v3, LX/0w3;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v3

    .line 158598
    iget v3, p0, LX/0w3;->b:I

    if-le v0, v3, :cond_4

    :goto_1
    iput-boolean v1, p0, LX/0w3;->f:Z

    .line 158599
    iget-boolean v1, p0, LX/0w3;->f:Z

    if-eqz v1, :cond_2

    .line 158600
    iput v0, p0, LX/0w3;->e:I

    .line 158601
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 158602
    goto :goto_0

    :cond_4
    move v1, v2

    .line 158603
    goto :goto_1

    .line 158604
    :cond_5
    iput-boolean v2, p0, LX/0w3;->f:Z

    goto :goto_2
.end method
