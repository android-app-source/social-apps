.class public final LX/1Op;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Oq;


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 242352
    iput-object p1, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(LX/1lw;)V
    .locals 4

    .prologue
    .line 242358
    iget v0, p1, LX/1lw;->a:I

    packed-switch v0, :pswitch_data_0

    .line 242359
    :goto_0
    return-void

    .line 242360
    :pswitch_0
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-virtual {v0, v1, v2}, LX/1OR;->a(II)V

    goto :goto_0

    .line 242361
    :pswitch_1
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-virtual {v0, v1, v2}, LX/1OR;->b(II)V

    goto :goto_0

    .line 242362
    :pswitch_2
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, LX/1lw;->b:I

    iget v3, p1, LX/1lw;->d:I

    invoke-virtual {v0, v1, v2, v3}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    .line 242363
    :pswitch_3
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-virtual {v0, v1, v2}, LX/1OR;->c(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(I)LX/1a1;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 242353
    iget-object v1, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/RecyclerView;->a(IZ)LX/1a1;

    move-result-object v1

    .line 242354
    if-nez v1, :cond_1

    .line 242355
    :cond_0
    :goto_0
    return-object v0

    .line 242356
    :cond_1
    iget-object v2, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    iget-object v3, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2, v3}, LX/1Os;->c(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 242357
    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 242348
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 242349
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 242350
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-static {v0, p2}, LX/1Ok;->a(LX/1Ok;I)I

    .line 242351
    return-void
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 242364
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->a(IILjava/lang/Object;)V

    .line 242365
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 242366
    return-void
.end method

.method public final a(LX/1lw;)V
    .locals 0

    .prologue
    .line 242346
    invoke-direct {p0, p1}, LX/1Op;->c(LX/1lw;)V

    .line 242347
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 242343
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 242344
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 242345
    return-void
.end method

.method public final b(LX/1lw;)V
    .locals 0

    .prologue
    .line 242335
    invoke-direct {p0, p1}, LX/1Op;->c(LX/1lw;)V

    .line 242336
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 242340
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->e(II)V

    .line 242341
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 242342
    return-void
.end method

.method public final d(II)V
    .locals 2

    .prologue
    .line 242337
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->d(II)V

    .line 242338
    iget-object v0, p0, LX/1Op;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 242339
    return-void
.end method
