.class public LX/1fU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/1fU;


# instance fields
.field public final a:LX/1fX;

.field public final b:LX/1fY;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/308;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Uh;

.field public e:Z

.field public f:Z

.field public g:LX/1tH;

.field public h:Z


# direct methods
.method public constructor <init>(LX/1fX;LX/0Uh;)V
    .locals 2
    .param p1    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 291748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291749
    new-instance v0, LX/1fY;

    invoke-direct {v0}, LX/1fY;-><init>()V

    iput-object v0, p0, LX/1fU;->b:LX/1fY;

    .line 291750
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1fU;->c:Ljava/util/List;

    .line 291751
    iput-boolean v1, p0, LX/1fU;->h:Z

    .line 291752
    iput-object p1, p0, LX/1fU;->a:LX/1fX;

    .line 291753
    iput-object p2, p0, LX/1fU;->d:LX/0Uh;

    .line 291754
    return-void
.end method

.method public static a(LX/0QB;)LX/1fU;
    .locals 5

    .prologue
    .line 291755
    sget-object v0, LX/1fU;->i:LX/1fU;

    if-nez v0, :cond_1

    .line 291756
    const-class v1, LX/1fU;

    monitor-enter v1

    .line 291757
    :try_start_0
    sget-object v0, LX/1fU;->i:LX/1fU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291758
    if-eqz v2, :cond_0

    .line 291759
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291760
    new-instance p0, LX/1fU;

    invoke-static {v0}, LX/1fV;->b(LX/0QB;)LX/1fW;

    move-result-object v3

    check-cast v3, LX/1fX;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/1fU;-><init>(LX/1fX;LX/0Uh;)V

    .line 291761
    move-object v0, p0

    .line 291762
    sput-object v0, LX/1fU;->i:LX/1fU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291763
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291764
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291765
    :cond_1
    sget-object v0, LX/1fU;->i:LX/1fU;

    return-object v0

    .line 291766
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291767
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1se;)Lcom/facebook/push/mqtt/ipc/SubscribeTopic;
    .locals 3

    .prologue
    .line 291768
    new-instance v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    .line 291769
    iget-object v1, p0, LX/1se;->a:Ljava/lang/String;

    move-object v1, v1

    .line 291770
    iget v2, p0, LX/1se;->b:I

    move v2, v2

    .line 291771
    invoke-direct {v0, v1, v2}, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/1se;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/SubscribeTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291772
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 291773
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1se;

    .line 291774
    invoke-static {v0}, LX/1fU;->a(LX/1se;)Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291775
    :cond_0
    return-object v1
.end method

.method private static a(Ljava/util/List;LX/768;)Ljava/util/List;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/308;",
            ">;",
            "LX/768;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291776
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 291777
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/308;

    .line 291778
    new-instance v3, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;

    .line 291779
    iget-object v4, v0, LX/308;->a:LX/1se;

    move-object v4, v4

    .line 291780
    invoke-static {v4}, LX/1fU;->a(LX/1se;)Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    move-result-object v4

    .line 291781
    iget-object p0, v0, LX/308;->b:Ljava/lang/String;

    move-object p0, p0

    .line 291782
    invoke-direct {v3, v4, p0, p1}, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;-><init>(Lcom/facebook/push/mqtt/ipc/SubscribeTopic;Ljava/lang/String;LX/768;)V

    move-object v0, v3

    .line 291783
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291784
    :cond_0
    return-object v1
.end method

.method public static b(LX/1fU;)V
    .locals 3

    .prologue
    .line 291785
    iget-object v0, p0, LX/1fU;->g:LX/1tH;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/1fU;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1fU;->e:Z

    if-eqz v0, :cond_0

    .line 291786
    iget-object v0, p0, LX/1fU;->g:LX/1tH;

    iget-object v1, p0, LX/1fU;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1fU;->a(Ljava/util/List;LX/768;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1tH;->a(Ljava/util/List;)V

    .line 291787
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1fU;->f:Z

    .line 291788
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/1se;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/1se;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 291789
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 291790
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 291791
    iget-object v2, p0, LX/1fU;->a:LX/1fX;

    new-instance v3, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;-><init>(LX/1fU;LX/0Px;LX/0Px;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
