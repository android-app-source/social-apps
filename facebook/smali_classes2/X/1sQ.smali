.class public LX/1sQ;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 334732
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 334733
    return-void
.end method

.method public static a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/03V;)LX/2D5;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "LX/2Fw;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6Z6;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2D4;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/2D5;"
        }
    .end annotation

    .prologue
    .line 334734
    invoke-static/range {p0 .. p7}, LX/2D2;->a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/03V;)LX/2D5;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)Lcom/facebook/location/BaseFbLocationManager;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "LX/2vg;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6Z8;",
            ">;",
            "LX/0Or",
            "<",
            "LX/35T;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "Lcom/facebook/location/FbLocationManager;"
        }
    .end annotation

    .prologue
    .line 334735
    invoke-static/range {p0 .. p8}, LX/2vc;->a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)Lcom/facebook/location/BaseFbLocationManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 334736
    return-void
.end method
