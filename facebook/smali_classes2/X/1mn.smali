.class public interface abstract LX/1mn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<YogaNodeType::",
        "LX/1mn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/facebook/csslayout/YogaEdge;)F
.end method

.method public abstract a()V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(LX/1mn;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TYogaNodeType;I)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaAlign;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaDirection;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaEdge;F)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaFlexDirection;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaJustify;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaMeasureFunction;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaOverflow;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaPositionType;)V
.end method

.method public abstract a(Lcom/facebook/csslayout/YogaWrap;)V
.end method

.method public abstract a(Ljava/lang/Object;)V
.end method

.method public abstract b()I
.end method

.method public abstract b(I)LX/1mn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TYogaNodeType;"
        }
    .end annotation
.end method

.method public abstract b(F)V
.end method

.method public abstract b(Lcom/facebook/csslayout/YogaAlign;)V
.end method

.method public abstract b(Lcom/facebook/csslayout/YogaEdge;F)V
.end method

.method public abstract c(I)LX/1mn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TYogaNodeType;"
        }
    .end annotation
.end method

.method public abstract c()V
.end method

.method public abstract c(F)V
.end method

.method public abstract c(Lcom/facebook/csslayout/YogaAlign;)V
.end method

.method public abstract c(Lcom/facebook/csslayout/YogaEdge;F)V
.end method

.method public abstract d(F)V
.end method

.method public abstract d(Lcom/facebook/csslayout/YogaEdge;F)V
.end method

.method public abstract d()Z
.end method

.method public abstract e(F)V
.end method

.method public abstract f(F)V
.end method

.method public abstract g()V
.end method

.method public abstract g(F)V
.end method

.method public abstract h()Lcom/facebook/csslayout/YogaDirection;
.end method

.method public abstract h(F)V
.end method

.method public abstract i()F
.end method

.method public abstract i(F)V
.end method

.method public abstract j()F
.end method

.method public abstract j(F)V
.end method

.method public abstract k()F
.end method

.method public abstract l()F
.end method

.method public abstract m()F
.end method

.method public abstract n()F
.end method

.method public abstract o()Lcom/facebook/csslayout/YogaDirection;
.end method

.method public abstract q()Ljava/lang/Object;
.end method

.method public abstract r()LX/1mn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TYogaNodeType;"
        }
    .end annotation
.end method
