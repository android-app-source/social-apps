.class public LX/16z;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public static final b:Lcom/facebook/graphql/model/GraphQLEntity;

.field public static final c:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

.field public static final d:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

.field public static final e:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

.field public static final f:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

.field public static final g:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

.field public static final h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public static final i:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1W5;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

.field public static final m:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

.field public static final n:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 188080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x285feb

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, LX/16z;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188081
    const-string v0, "0"

    const v1, 0x4c808d5

    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    sput-object v0, LX/16z;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188082
    new-instance v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;-><init>()V

    sput-object v0, LX/16z;->c:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 188083
    new-instance v0, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;-><init>()V

    sput-object v0, LX/16z;->d:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 188084
    new-instance v0, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;-><init>()V

    sput-object v0, LX/16z;->e:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 188085
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;-><init>()V

    sput-object v0, LX/16z;->f:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 188086
    new-instance v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;-><init>()V

    sput-object v0, LX/16z;->g:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 188087
    const-string v0, ""

    invoke-static {v0}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    sput-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 188088
    new-instance v0, LX/177;

    invoke-direct {v0}, LX/177;-><init>()V

    sput-object v0, LX/16z;->i:Ljava/util/Comparator;

    .line 188089
    new-instance v0, LX/178;

    invoke-direct {v0}, LX/178;-><init>()V

    sput-object v0, LX/16z;->j:Ljava/util/Comparator;

    .line 188090
    new-instance v0, LX/179;

    invoke-direct {v0}, LX/179;-><init>()V

    sput-object v0, LX/16z;->k:Ljava/util/Comparator;

    .line 188091
    new-instance v0, Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;-><init>()V

    sput-object v0, LX/16z;->l:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 188092
    new-instance v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;-><init>()V

    sput-object v0, LX/16z;->m:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 188093
    new-instance v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;-><init>()V

    sput-object v0, LX/16z;->n:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 188078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188079
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)F
    .locals 2

    .prologue
    .line 188077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 1

    .prologue
    .line 188076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188071
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 188072
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 188073
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 188074
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 188075
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/16h;LX/16h;)LX/162;
    .locals 2

    .prologue
    .line 188066
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 188067
    invoke-interface {p0}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 188068
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 188069
    invoke-interface {p1}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 188070
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 188060
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 188061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 188062
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188063
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    .line 188064
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 188065
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)LX/1yL;
    .locals 3

    .prologue
    .line 188059
    new-instance v0, LX/1yL;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1yL;-><init>(II)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntityAtRange;)LX/1yL;
    .locals 3

    .prologue
    .line 188006
    new-instance v0, LX/1yL;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1yL;-><init>(II)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLocation;)Landroid/location/Location;
    .locals 4

    .prologue
    .line 188054
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 188055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 188056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 188057
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
    .locals 1

    .prologue
    .line 188051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    if-nez v0, :cond_0

    .line 188052
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 188053
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;I)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 188049
    invoke-static {p0}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 188050
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 2

    .prologue
    .line 188046
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188047
    :cond_0
    const/4 v0, 0x0

    .line 188048
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 2

    .prologue
    .line 188039
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 188040
    iput-object p0, v0, LX/170;->o:Ljava/lang/String;

    .line 188041
    move-object v0, v0

    .line 188042
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 188043
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188044
    move-object v0, v0

    .line 188045
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 1

    .prologue
    .line 188033
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 188034
    iput-object p0, v0, LX/170;->o:Ljava/lang/String;

    .line 188035
    move-object v0, v0

    .line 188036
    iput-object p1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188037
    move-object v0, v0

    .line 188038
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 1

    .prologue
    .line 188025
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 188026
    iput-object p0, v0, LX/170;->o:Ljava/lang/String;

    .line 188027
    move-object v0, v0

    .line 188028
    iput-object p1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188029
    move-object v0, v0

    .line 188030
    iput-object p2, v0, LX/170;->Y:Ljava/lang/String;

    .line 188031
    move-object v0, v0

    .line 188032
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;
    .locals 2

    .prologue
    .line 188015
    new-instance v0, LX/4W6;

    invoke-direct {v0}, LX/4W6;-><init>()V

    .line 188016
    iput-object p0, v0, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188017
    move-object v0, v0

    .line 188018
    iget v1, p1, LX/1yL;->a:I

    move v1, v1

    .line 188019
    iput v1, v0, LX/4W6;->d:I

    .line 188020
    move-object v0, v0

    .line 188021
    iget v1, p1, LX/1yL;->b:I

    move v1, v1

    .line 188022
    iput v1, v0, LX/4W6;->c:I

    .line 188023
    move-object v0, v0

    .line 188024
    invoke-virtual {v0}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 188007
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 188008
    iput-object p0, v0, LX/2dc;->h:Ljava/lang/String;

    .line 188009
    move-object v0, v0

    .line 188010
    iput p1, v0, LX/2dc;->i:I

    .line 188011
    move-object v0, v0

    .line 188012
    iput p2, v0, LX/2dc;->c:I

    .line 188013
    move-object v0, v0

    .line 188014
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 188105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 188106
    if-nez v0, :cond_0

    .line 188107
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    .line 188108
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 188095
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    .line 188096
    iput-object p0, v0, LX/17L;->f:Ljava/lang/String;

    .line 188097
    move-object v0, v0

    .line 188098
    iput-object p1, v0, LX/17L;->c:Ljava/lang/String;

    .line 188099
    move-object v0, v0

    .line 188100
    iput-boolean p2, v0, LX/17L;->e:Z

    .line 188101
    move-object v0, v0

    .line 188102
    iput-boolean p3, v0, LX/17L;->d:Z

    .line 188103
    move-object v0, v0

    .line 188104
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStructuredSurvey;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;
    .locals 3

    .prologue
    .line 188225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 188226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 188227
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188228
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    .line 188229
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188230
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 188177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 188178
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 188179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 188180
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 188181
    new-instance v7, LX/4W6;

    invoke-direct {v7}, LX/4W6;-><init>()V

    .line 188182
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188183
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    iput-object v8, v7, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188184
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v8

    iput v8, v7, LX/4W6;->c:I

    .line 188185
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v8

    iput v8, v7, LX/4W6;->d:I

    .line 188186
    invoke-static {v7, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 188187
    move-object v7, v7

    .line 188188
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v0

    add-int/2addr v0, v3

    .line 188189
    iput v0, v7, LX/4W6;->d:I

    .line 188190
    move-object v0, v7

    .line 188191
    invoke-virtual {v0}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188192
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 188193
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 188194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 188195
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    :goto_1
    if-ge v2, v7, :cond_1

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageAtRange;

    .line 188196
    new-instance v8, LX/4Wv;

    invoke-direct {v8}, LX/4Wv;-><init>()V

    .line 188197
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188198
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v9

    iput-object v9, v8, LX/4Wv;->b:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 188199
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->j()I

    move-result v9

    iput v9, v8, LX/4Wv;->c:I

    .line 188200
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->k()I

    move-result v9

    iput v9, v8, LX/4Wv;->d:I

    .line 188201
    invoke-static {v8, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 188202
    move-object v8, v8

    .line 188203
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->k()I

    move-result v0

    add-int/2addr v0, v3

    .line 188204
    iput v0, v8, LX/4Wv;->d:I

    .line 188205
    move-object v0, v8

    .line 188206
    invoke-virtual {v0}, LX/4Wv;->a()Lcom/facebook/graphql/model/GraphQLImageAtRange;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188207
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 188208
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 188209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 188210
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    :goto_2
    if-ge v1, v7, :cond_2

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    .line 188211
    new-instance v8, LX/4Vo;

    invoke-direct {v8}, LX/4Vo;-><init>()V

    .line 188212
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188213
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->a()I

    move-result v9

    iput v9, v8, LX/4Vo;->b:I

    .line 188214
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v9

    iput v9, v8, LX/4Vo;->c:I

    .line 188215
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v9

    iput v9, v8, LX/4Vo;->d:I

    .line 188216
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v9

    iput-object v9, v8, LX/4Vo;->e:LX/0Px;

    .line 188217
    invoke-static {v8, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 188218
    move-object v8, v8

    .line 188219
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v0

    add-int/2addr v0, v3

    .line 188220
    iput v0, v8, LX/4Vo;->d:I

    .line 188221
    move-object v0, v8

    .line 188222
    invoke-virtual {v0}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188223
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 188224
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v5, v2}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 188173
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 188174
    iput-object p0, v0, LX/173;->f:Ljava/lang/String;

    .line 188175
    move-object v0, v0

    .line 188176
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageAtRange;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;"
        }
    .end annotation

    .prologue
    .line 188160
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 188161
    iput-object p0, v0, LX/173;->f:Ljava/lang/String;

    .line 188162
    move-object v0, v0

    .line 188163
    invoke-static {p2}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 188164
    iput-object v1, v0, LX/173;->c:LX/0Px;

    .line 188165
    move-object v0, v0

    .line 188166
    invoke-static {p1}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 188167
    iput-object v1, v0, LX/173;->e:LX/0Px;

    .line 188168
    move-object v0, v0

    .line 188169
    invoke-static {p3}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 188170
    iput-object v1, v0, LX/173;->b:LX/0Px;

    .line 188171
    move-object v0, v0

    .line 188172
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;",
            "Ljava/lang/Iterable",
            "<+",
            "LX/2qo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 188152
    if-nez p0, :cond_0

    move-object v0, v1

    .line 188153
    :goto_0
    return-object v0

    .line 188154
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qo;

    .line 188155
    invoke-interface {v0}, LX/2qo;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 188156
    const/4 v1, 0x0

    invoke-interface {v0}, LX/2qo;->n_()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 188157
    invoke-interface {v0}, LX/2qo;->a()I

    move-result v0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v0

    .line 188158
    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 188159
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 188150
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 188151
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 188145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    .line 188146
    if-nez v0, :cond_0

    .line 188147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 188148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    .line 188149
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLName;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 188231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    .line 188232
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLName;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLName;->m_()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    .line 188233
    if-nez v0, :cond_0

    move-object v3, v4

    .line 188234
    :goto_0
    move-object v1, v3

    .line 188235
    move-object v0, v1

    .line 188236
    return-object v0

    .line 188237
    :cond_0
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNamePart;

    .line 188238
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNamePart;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 188239
    const/4 v4, 0x0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNamePart;->n_()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v4

    .line 188240
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNamePart;->a()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v3

    .line 188241
    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    move-object v3, v4

    .line 188242
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 188142
    if-nez p0, :cond_0

    .line 188143
    const-string v0, "null"

    .line 188144
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "startCursor"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "endCursor"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "hasPreviousPage"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "hasNextPage"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/479",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 188130
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 188131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188132
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 188133
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 188134
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v1

    .line 188135
    :goto_1
    new-instance v8, LX/479;

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntityAtRange;)LX/1yL;

    move-result-object v0

    invoke-direct {v8, v0, v1}, LX/479;-><init>(LX/1yL;Ljava/lang/Object;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188136
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 188137
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 188138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v1, v3

    :goto_2
    if-ge v1, v6, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    .line 188139
    new-instance v3, LX/479;

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)LX/1yL;

    move-result-object v0

    invoke-direct {v3, v0, v2}, LX/479;-><init>(LX/1yL;Ljava/lang/Object;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 188141
    :cond_1
    return-object v5

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V
    .locals 1

    .prologue
    .line 188127
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Z)V

    .line 188128
    return-void

    .line 188129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 188123
    if-nez p0, :cond_1

    .line 188124
    :cond_0
    :goto_0
    return v0

    .line 188125
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v1

    .line 188126
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 188116
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    .line 188117
    :goto_0
    return v0

    .line 188118
    :cond_1
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 188119
    invoke-virtual {p1, v0}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188120
    const/4 v0, 0x1

    goto :goto_0

    .line 188121
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 188122
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 188111
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 188112
    :cond_0
    :goto_0
    return v0

    .line 188113
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 188114
    goto :goto_0

    .line 188115
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;)Z
    .locals 1

    .prologue
    .line 188110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProfile;)Z
    .locals 1

    .prologue
    .line 188109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 1

    .prologue
    .line 188058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z
    .locals 2

    .prologue
    .line 188094
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNKONWN:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)Z
    .locals 1

    .prologue
    .line 187907
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLSeenByConnection;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSeenByConnection;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->j()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    .line 187946
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 187947
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->j()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 1

    .prologue
    .line 187941
    invoke-static {p0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 187942
    if-nez v0, :cond_0

    .line 187943
    const/4 v0, 0x0

    .line 187944
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 1

    .prologue
    .line 187940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 187938
    if-nez p0, :cond_1

    .line 187939
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLImage;)Z
    .locals 2

    .prologue
    .line 187937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 187936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x22a42d2a    # -9.8999738E17f

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 1

    .prologue
    .line 187935
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 2

    .prologue
    .line 187932
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187933
    :cond_0
    const/4 v0, 0x0

    .line 187934
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 187931
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLProfile;)Z
    .locals 1

    .prologue
    .line 187930
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187927
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->k()Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->k()Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v0

    .line 187929
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 187926
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/16z;->v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLPage;)Z
    .locals 1

    .prologue
    .line 187925
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 187918
    if-nez p0, :cond_0

    .line 187919
    const/4 v0, 0x0

    .line 187920
    :goto_0
    return v0

    .line 187921
    :cond_0
    invoke-static {p0}, LX/16z;->s(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 187922
    invoke-static {p0}, LX/16z;->u(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a()I

    move-result v0

    goto :goto_0

    .line 187923
    :cond_1
    invoke-static {p0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 187924
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 187910
    invoke-static {p0}, LX/16z;->s(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187911
    invoke-static {p0}, LX/16z;->v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    .line 187912
    if-nez v1, :cond_1

    .line 187913
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    return v0

    .line 187914
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 187915
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v1

    .line 187916
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 187917
    :cond_3
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_1
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187909
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 187908
    invoke-static {p0}, LX/16z;->s(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/16z;->u(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187906
    invoke-static {p0}, LX/16z;->s(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/16z;->u(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->j()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 187978
    if-nez p0, :cond_1

    .line 187979
    :cond_0
    :goto_0
    return-object v0

    .line 187980
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 187981
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 187982
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 187983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 188005
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 187997
    if-nez p0, :cond_1

    .line 187998
    :cond_0
    :goto_0
    return v0

    .line 187999
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 188000
    goto :goto_0

    .line 188001
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 188002
    goto :goto_0

    .line 188003
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 188004
    goto :goto_0
.end method

.method public static j(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .locals 1

    .prologue
    .line 187994
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 187995
    sget-object v0, LX/16z;->c:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 187996
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 187993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->a()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 187990
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16z;->t(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187991
    invoke-static {p0}, LX/16z;->t(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;->a()I

    move-result v0

    .line 187992
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 187989
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLLocation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 187986
    invoke-static {p0}, LX/16z;->v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 187987
    sget-object v0, LX/16z;->f:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 187988
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/16z;->v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 187984
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 187985
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLLocation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 1

    .prologue
    .line 187948
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 187949
    :cond_0
    sget-object v0, LX/16z;->m:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 187950
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 187977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x25d6af

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static o(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 1

    .prologue
    .line 187976
    invoke-static {p0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v0

    return v0
.end method

.method public static p(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 1

    .prologue
    .line 187975
    invoke-static {p0}, LX/16z;->q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v0

    return v0
.end method

.method public static q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .locals 1

    .prologue
    .line 187972
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 187973
    :cond_0
    sget-object v0, LX/16z;->n:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 187974
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 187962
    if-nez p0, :cond_0

    move v0, v1

    .line 187963
    :goto_0
    return v0

    .line 187964
    :cond_0
    invoke-static {p0}, LX/207;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    .line 187965
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    .line 187966
    goto :goto_0

    .line 187967
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 187968
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v0

    if-lez v0, :cond_3

    .line 187969
    const/4 v0, 0x1

    goto :goto_0

    .line 187970
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 187971
    goto :goto_0
.end method

.method private static s(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 187961
    invoke-static {p0}, LX/16z;->v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static t(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;
    .locals 1

    .prologue
    .line 187957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    .line 187958
    if-nez v0, :cond_0

    .line 187959
    sget-object v0, LX/16z;->e:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 187960
    :cond_0
    return-object v0
.end method

.method private static u(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLCommentsConnection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 187954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 187955
    sget-object v0, LX/16z;->l:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 187956
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    goto :goto_0
.end method

.method private static v(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187951
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 187953
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
