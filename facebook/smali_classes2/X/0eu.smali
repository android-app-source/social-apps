.class public LX/0eu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final i:LX/0ou;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ou",
            "<",
            "LX/0eu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/49P;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/Object;

.field public e:J

.field public f:J

.field private g:J

.field private h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 104371
    new-instance v0, LX/0oq;

    const-class v1, LX/0eu;

    .line 104372
    sget-object v2, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v2, v2

    .line 104373
    invoke-direct {v0, v1, v2}, LX/0oq;-><init>(Ljava/lang/Class;LX/0So;)V

    new-instance v1, LX/49O;

    const-class v2, LX/0eu;

    invoke-direct {v1, v2}, LX/49O;-><init>(Ljava/lang/Class;)V

    .line 104374
    iput-object v1, v0, LX/0oq;->f:LX/0ot;

    .line 104375
    move-object v0, v0

    .line 104376
    invoke-virtual {v0}, LX/0oq;->a()LX/0ou;

    move-result-object v0

    sput-object v0, LX/0eu;->i:LX/0ou;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104370
    return-void
.end method

.method public static a(LX/0eu;)LX/0eu;
    .locals 8

    .prologue
    .line 104367
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    move-object v0, v0

    .line 104368
    sget-object v1, LX/49P;->START:LX/49P;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/49P;->STOP:LX/49P;

    :goto_0
    iget v1, p0, LX/0eu;->b:I

    iget-object v2, p0, LX/0eu;->c:Ljava/lang/String;

    iget-object v3, p0, LX/0eu;->d:[Ljava/lang/Object;

    iget-wide v4, p0, LX/0eu;->e:J

    iget-wide v6, p0, LX/0eu;->g:J

    invoke-static/range {v0 .. v7}, LX/0eu;->a(LX/49P;ILjava/lang/String;[Ljava/lang/Object;JJ)LX/0eu;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, LX/49P;->STOP_ASYNC:LX/49P;

    goto :goto_0
.end method

.method public static a(LX/49P;ILjava/lang/String;[Ljava/lang/Object;JJ)LX/0eu;
    .locals 4

    .prologue
    .line 104357
    sget-object v0, LX/0eu;->i:LX/0ou;

    invoke-virtual {v0}, LX/0ou;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eu;

    .line 104358
    iput-object p0, v0, LX/0eu;->a:LX/49P;

    .line 104359
    iput p1, v0, LX/0eu;->b:I

    .line 104360
    iput-object p3, v0, LX/0eu;->d:[Ljava/lang/Object;

    .line 104361
    iput-object p2, v0, LX/0eu;->c:Ljava/lang/String;

    .line 104362
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/0eu;->g:J

    .line 104363
    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v2

    iput-wide v2, v0, LX/0eu;->e:J

    .line 104364
    iput-wide p4, v0, LX/0eu;->f:J

    .line 104365
    iput-wide p6, v0, LX/0eu;->h:J

    .line 104366
    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v10, 0x1

    const-wide/16 v8, 0x3e8

    .line 104353
    long-to-float v0, p0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v0, v0

    .line 104354
    div-long v2, v0, v8

    const-wide/16 v4, 0x3c

    rem-long/2addr v2, v4

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 104355
    rem-long/2addr v0, v8

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 104356
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v6, 0x64

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    add-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 104341
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 104342
    long-to-float v1, p0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-long v2, v1

    .line 104343
    const-wide/16 v4, 0xa

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 104344
    const-string v1, "____"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104345
    :cond_0
    :goto_0
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 104346
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104347
    :cond_1
    const-wide/16 v4, 0x64

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 104348
    const-string v1, "___"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 104349
    :cond_2
    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 104350
    const-string v1, "__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 104351
    :cond_3
    const-wide/16 v4, 0x2710

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 104352
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 104304
    sget-object v0, LX/0eu;->i:LX/0ou;

    invoke-virtual {v0, p0}, LX/0ou;->a(Ljava/lang/Object;)V

    .line 104305
    return-void
.end method

.method public final a(Ljava/lang/StringBuilder;JJI)V
    .locals 8

    .prologue
    const-wide/32 v6, 0xf4240

    .line 104317
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    .line 104318
    const-string v0, "-----"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104319
    :goto_0
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104320
    iget-wide v0, p0, LX/0eu;->e:J

    sub-long/2addr v0, p2

    div-long/2addr v0, v6

    invoke-static {v0, v1}, LX/0eu;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104321
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    sget-object v1, LX/49P;->START:LX/49P;

    if-ne v0, v1, :cond_2

    .line 104322
    const-string v0, " Start    ...     ...   "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104323
    :cond_0
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p6, :cond_6

    .line 104324
    const-string v1, "|  "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104325
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 104326
    :cond_1
    iget-wide v0, p0, LX/0eu;->e:J

    sub-long/2addr v0, p4

    div-long/2addr v0, v6

    invoke-static {v0, v1}, LX/0eu;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 104327
    :cond_2
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    sget-object v1, LX/49P;->START_ASYNC:LX/49P;

    if-ne v0, v1, :cond_3

    .line 104328
    const-string v0, " AStart   ...     ...   "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 104329
    :cond_3
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    sget-object v1, LX/49P;->STOP:LX/49P;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, LX/0eu;->a:LX/49P;

    sget-object v1, LX/49P;->STOP_ASYNC:LX/49P;

    if-ne v0, v1, :cond_5

    .line 104330
    :cond_4
    const-string v0, " Done "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104331
    iget-wide v0, p0, LX/0eu;->e:J

    iget-wide v2, p0, LX/0eu;->f:J

    sub-long/2addr v0, v2

    .line 104332
    iget-wide v2, p0, LX/0eu;->g:J

    iget-wide v4, p0, LX/0eu;->h:J

    sub-long/2addr v2, v4

    .line 104333
    div-long/2addr v0, v6

    invoke-static {v0, v1}, LX/0eu;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104334
    const-string v0, " ms "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104335
    invoke-static {v2, v3}, LX/0eu;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104336
    const-string v0, " ms "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 104337
    :cond_5
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    sget-object v1, LX/49P;->SPAWN:LX/49P;

    if-eq v0, v1, :cond_0

    .line 104338
    const-string v0, " Comment  ...     ...   "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 104339
    :cond_6
    invoke-virtual {p0}, LX/0eu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104340
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 104316
    iget v0, p0, LX/0eu;->b:I

    return v0
.end method

.method public final c()LX/49P;
    .locals 1

    .prologue
    .line 104315
    iget-object v0, p0, LX/0eu;->a:LX/49P;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 104308
    iget-object v0, p0, LX/0eu;->d:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 104309
    :try_start_0
    iget-object v0, p0, LX/0eu;->c:Ljava/lang/String;

    iget-object v1, p0, LX/0eu;->d:[Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0eu;->c:Ljava/lang/String;

    .line 104310
    const/4 v0, 0x0

    iput-object v0, p0, LX/0eu;->d:[Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104311
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0eu;->c:Ljava/lang/String;

    return-object v0

    .line 104312
    :catch_0
    move-exception v0

    .line 104313
    const-string v1, "TraceEvent"

    const-string v2, "Bad format string"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104314
    iput-object v3, p0, LX/0eu;->d:[Ljava/lang/Object;

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 104307
    iget-wide v0, p0, LX/0eu;->e:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104306
    invoke-virtual {p0}, LX/0eu;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
