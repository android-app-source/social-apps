.class public final LX/1rK;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/0yI;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1rK;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Landroid/os/Handler;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332009
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 332010
    iput-object p2, p0, LX/1rK;->a:LX/0Ot;

    .line 332011
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1rK;->b:Z

    .line 332012
    return-void
.end method

.method public static a(LX/0QB;)LX/1rK;
    .locals 6

    .prologue
    .line 332013
    sget-object v0, LX/1rK;->c:LX/1rK;

    if-nez v0, :cond_1

    .line 332014
    const-class v1, LX/1rK;

    monitor-enter v1

    .line 332015
    :try_start_0
    sget-object v0, LX/1rK;->c:LX/1rK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332016
    if-eqz v2, :cond_0

    .line 332017
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332018
    new-instance v4, LX/1rK;

    const/16 v3, 0x13f6

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x245

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {v4, v5, p0, v3}, LX/1rK;-><init>(LX/0Ot;LX/0Ot;Landroid/os/Handler;)V

    .line 332019
    move-object v0, v4

    .line 332020
    sput-object v0, LX/1rK;->c:LX/1rK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332021
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332022
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332023
    :cond_1
    sget-object v0, LX/1rK;->c:LX/1rK;

    return-object v0

    .line 332024
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 332026
    check-cast p3, LX/0yI;

    .line 332027
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 332028
    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 332029
    iget-object v0, p0, LX/1rK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 332030
    invoke-virtual {p3}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n()V

    .line 332031
    :cond_0
    :goto_0
    return-void

    .line 332032
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1rK;->b:Z

    goto :goto_0

    .line 332033
    :cond_2
    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 332034
    iget-boolean v0, p0, LX/1rK;->b:Z

    if-eqz v0, :cond_3

    .line 332035
    invoke-virtual {p3}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n()V

    .line 332036
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1rK;->b:Z

    goto :goto_0

    .line 332037
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j()V

    goto :goto_0

    .line 332038
    :cond_4
    const-string v1, "com.facebook.zero.ZERO_HEADER_REFRESH_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332039
    sget-object v0, LX/32P;->HEADERS_REFRESH:LX/32P;

    .line 332040
    invoke-virtual {p3, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/32P;)V

    .line 332041
    goto :goto_0
.end method
