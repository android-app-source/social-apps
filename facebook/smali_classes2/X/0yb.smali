.class public LX/0yb;
.super LX/0yc;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile E:LX/0yb;

.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Landroid/support/v4/app/DialogFragment;

.field public B:Z

.field private final C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/0lC;

.field public final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fJ;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pl;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pm;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0dO;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12L;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0ZA;

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5N4;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5N7;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0W3;

.field private final w:LX/0yd;

.field public x:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/13j;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0yL;",
            ">;>;"
        }
    .end annotation
.end field

.field private z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 165394
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.facebook.katana.activity.media.ViewVideoActivity"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.facebook.photos.albums.video.VideoAlbumLaunchPlayerActivity"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.facebook.photos.taggablegallery.ProductionVideoGalleryActivity"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0yb;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Z9;LX/0yd;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0W3;LX/0Ot;LX/0lC;)V
    .locals 2
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
        .end annotation
    .end param
    .param p8    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneManualSwitcherFeatureAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Z9;",
            "LX/0yd;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0yL;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5N4;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0fJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/12L;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0dO;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5N7;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 165317
    invoke-direct {p0}, LX/0yc;-><init>()V

    .line 165318
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0yb;->B:Z

    .line 165319
    iput-object p1, p0, LX/0yb;->b:Landroid/content/Context;

    .line 165320
    iput-object p7, p0, LX/0yb;->p:LX/0Or;

    .line 165321
    iput-object p2, p0, LX/0yb;->r:LX/0ZA;

    .line 165322
    iput-object p3, p0, LX/0yb;->w:LX/0yd;

    .line 165323
    iput-object p4, p0, LX/0yb;->y:LX/0Ot;

    .line 165324
    iput-object p5, p0, LX/0yb;->g:LX/0Ot;

    .line 165325
    iput-object p6, p0, LX/0yb;->k:LX/0Ot;

    .line 165326
    iput-object p8, p0, LX/0yb;->c:LX/0Ot;

    .line 165327
    iput-object p9, p0, LX/0yb;->d:LX/0Ot;

    .line 165328
    iput-object p10, p0, LX/0yb;->e:LX/0Ot;

    .line 165329
    iput-object p11, p0, LX/0yb;->s:LX/0Ot;

    .line 165330
    iput-object p12, p0, LX/0yb;->f:LX/0Ot;

    .line 165331
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0yb;->h:LX/0Ot;

    .line 165332
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0yb;->l:LX/0Ot;

    .line 165333
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0yb;->m:LX/0Ot;

    .line 165334
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0yb;->j:LX/0Ot;

    .line 165335
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/0yb;->x:LX/0am;

    .line 165336
    iput-object p13, p0, LX/0yb;->o:LX/0Ot;

    .line 165337
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0yb;->i:LX/0Ot;

    .line 165338
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0yb;->n:LX/0Ot;

    .line 165339
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0yb;->q:LX/0Or;

    .line 165340
    move-object/from16 v0, p21

    iput-object v0, p0, LX/0yb;->t:LX/0Ot;

    .line 165341
    move-object/from16 v0, p22

    iput-object v0, p0, LX/0yb;->u:LX/0Ot;

    .line 165342
    move-object/from16 v0, p23

    iput-object v0, p0, LX/0yb;->v:LX/0W3;

    .line 165343
    move-object/from16 v0, p24

    iput-object v0, p0, LX/0yb;->C:LX/0Ot;

    .line 165344
    move-object/from16 v0, p25

    iput-object v0, p0, LX/0yb;->D:LX/0lC;

    .line 165345
    return-void
.end method

.method public static A(LX/0yb;)Z
    .locals 3

    .prologue
    .line 165346
    iget-object v0, p0, LX/0yb;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x4b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static C(LX/0yb;)V
    .locals 5

    .prologue
    .line 165347
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_PHOTOCAP_ERROR:LX/0yY;

    const-string v2, ""

    iget-object v3, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0805f6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/2nT;->SPINNER:LX/2nT;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/2nT;)LX/121;

    .line 165348
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 165349
    if-nez v1, :cond_0

    .line 165350
    invoke-static {p0}, LX/0yb;->D(LX/0yb;)V

    .line 165351
    :goto_0
    return-void

    .line 165352
    :cond_0
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v2, LX/0yY;->DIALTONE_PHOTOCAP_ERROR:LX/0yY;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    goto :goto_0
.end method

.method public static D(LX/0yb;)V
    .locals 3

    .prologue
    .line 165353
    iget-object v0, p0, LX/0yb;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "dialtone"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "currentAcitvity is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165354
    return-void
.end method

.method public static a(LX/0QB;)LX/0yb;
    .locals 3

    .prologue
    .line 165355
    sget-object v0, LX/0yb;->E:LX/0yb;

    if-nez v0, :cond_1

    .line 165356
    const-class v1, LX/0yb;

    monitor-enter v1

    .line 165357
    :try_start_0
    sget-object v0, LX/0yb;->E:LX/0yb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 165358
    if-eqz v2, :cond_0

    .line 165359
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0yb;->b(LX/0QB;)LX/0yb;

    move-result-object v0

    sput-object v0, LX/0yb;->E:LX/0yb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165360
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 165361
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 165362
    :cond_1
    sget-object v0, LX/0yb;->E:LX/0yb;

    return-object v0

    .line 165363
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 165364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 165365
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 165366
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 165367
    move-object v1, v0

    .line 165368
    const-string v0, "ref"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165369
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165370
    iget-object v0, p0, LX/0yb;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 165371
    return-void
.end method

.method private a(Ljava/lang/String;LX/49w;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 5

    .prologue
    .line 165372
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 165373
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 165374
    move-object v1, v0

    .line 165375
    const-string v0, "whitelist_type"

    invoke-virtual {p2}, LX/49w;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165376
    const-string v0, "whitelisted_element"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165377
    if-eqz p4, :cond_0

    .line 165378
    const-string v0, "whitelisted_callercontext"

    .line 165379
    iget-object v2, p4, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 165380
    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165381
    :cond_0
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165382
    iget-object v0, p0, LX/0yb;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 165383
    return-void
.end method

.method private static b(LX/0QB;)LX/0yb;
    .locals 28

    .prologue
    .line 165384
    new-instance v2, LX/0yb;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0Z9;->a(LX/0QB;)LX/0Z9;

    move-result-object v4

    check-cast v4, LX/0Z9;

    invoke-static/range {p0 .. p0}, LX/0yd;->a(LX/0QB;)LX/0yd;

    move-result-object v5

    check-cast v5, LX/0yd;

    invoke-static/range {p0 .. p0}, LX/0yf;->a(LX/0QB;)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbc

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x312

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x1c6

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1ce

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1a9f

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xc16

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xbd8

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x4e4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x4ed

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x12b1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0xf9a

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x4e8

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x4e7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x1486

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    const/16 v23, 0x1aa0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0xac0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v25

    check-cast v25, LX/0W3;

    const/16 v26, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v27

    check-cast v27, LX/0lC;

    invoke-direct/range {v2 .. v27}, LX/0yb;-><init>(Landroid/content/Context;LX/0Z9;LX/0yd;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0W3;LX/0Ot;LX/0lC;)V

    .line 165385
    return-object v2
.end method

.method public static b(LX/0yb;Z)V
    .locals 2

    .prologue
    .line 165386
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0df;->K:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 165387
    return-void
.end method

.method public static c(LX/0yb;Z)V
    .locals 2

    .prologue
    .line 165388
    invoke-direct {p0}, LX/0yb;->y()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yL;

    .line 165389
    invoke-interface {v0}, LX/0yL;->ig_()V

    goto :goto_0

    .line 165390
    :cond_0
    return-void
.end method

.method public static d(LX/0yb;Z)V
    .locals 2

    .prologue
    .line 165391
    invoke-direct {p0}, LX/0yb;->y()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yL;

    .line 165392
    invoke-interface {v0, p1}, LX/0yL;->e_(Z)V

    goto :goto_0

    .line 165393
    :cond_0
    return-void
.end method

.method public static f(LX/0yb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 165403
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 165404
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 165405
    move-object v1, v0

    .line 165406
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165407
    iget-object v0, p0, LX/0yb;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 165408
    return-void
.end method

.method private w()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 165395
    iget-object v1, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v1}, LX/0ZA;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165396
    :goto_0
    return v0

    .line 165397
    :cond_0
    const/4 v2, 0x0

    .line 165398
    iget-object v1, p0, LX/0yb;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0dO;

    sget-object v3, LX/0yY;->DIALTONE_STICKY_MODE:LX/0yY;

    invoke-virtual {v1, v3}, LX/0dP;->a(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0df;->K:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 165399
    if-eqz v1, :cond_1

    .line 165400
    iget-object v1, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v1}, LX/0ZA;->a()Z

    .line 165401
    const-string v1, "dialtone_explicitly_entered"

    const-string v2, "dialtone_sticky"

    invoke-static {p0, v1, v2}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 165402
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private declared-synchronized y()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0yL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165633
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yb;->z:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 165634
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/0yb;->z:Ljava/util/Set;

    .line 165635
    iget-object v1, p0, LX/0yb;->z:Ljava/util/Set;

    iget-object v0, p0, LX/0yb;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 165636
    :cond_0
    iget-object v0, p0, LX/0yb;->z:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 165637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(FFLX/396;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    .line 165515
    iget-object v0, p0, LX/0yb;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5N7;

    .line 165516
    iget-boolean v1, p3, LX/396;->d:Z

    if-eqz v1, :cond_1

    .line 165517
    invoke-static {v0}, LX/5N7;->b(LX/5N7;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 165518
    :cond_0
    :goto_0
    move-object v0, v1

    .line 165519
    return-object v0

    .line 165520
    :cond_1
    const/high16 p0, 0x40000000    # 2.0f

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 165521
    iget-object v1, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 165522
    const v2, 0x7f03041a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 165523
    const v2, 0x7f0d0c97

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 165524
    const v3, 0x7f0d0c98

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 165525
    const v4, 0x7f0d0c99

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 165526
    iget-object v5, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v5}, LX/0yc;->o()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 165527
    iget-object v5, v0, LX/5N7;->f:Landroid/content/Context;

    const v6, 0x7f0805fd

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 165528
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165529
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165530
    :cond_2
    sget-object v5, LX/5N6;->a:[I

    iget-object v6, p3, LX/396;->c:LX/397;

    invoke-virtual {v6}, LX/397;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 165531
    :cond_3
    iget-object v2, v0, LX/5N7;->a:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    .line 165532
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, LX/5N7;->a:Landroid/graphics/Bitmap;

    .line 165533
    :cond_4
    iget-object v1, v0, LX/5N7;->a:Landroid/graphics/Bitmap;

    :goto_1
    move-object v1, v1

    .line 165534
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v2, p2

    if-lez v2, :cond_0

    .line 165535
    :cond_5
    const/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 165536
    iget-object v1, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 165537
    const v2, 0x7f03041a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 165538
    const v2, 0x7f0d0c97

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 165539
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165540
    const v2, 0x7f0d0c98

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 165541
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165542
    const v2, 0x7f0d0c99

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 165543
    sget-object v3, LX/5N6;->a:[I

    iget-object v4, p3, LX/396;->c:LX/397;

    invoke-virtual {v4}, LX/397;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 165544
    :cond_6
    :goto_2
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_3
    move-object v1, v1

    .line 165545
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v2, p1

    if-lez v2, :cond_0

    invoke-static {v0}, LX/5N7;->b(LX/5N7;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    .line 165546
    :pswitch_0
    iget-object v2, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->k()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 165547
    iget v2, p3, LX/396;->b:I

    if-lez v2, :cond_7

    .line 165548
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v3, 0x7f080602

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p3, LX/396;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165549
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165550
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165551
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 165552
    :cond_7
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v5, 0x7f0805fc

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165553
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165554
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165555
    iget-object v2, v0, LX/5N7;->h:LX/1pl;

    .line 165556
    iget v3, v2, LX/1pl;->g:I

    move v2, v3

    .line 165557
    if-lez v2, :cond_8

    .line 165558
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0f002c

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165559
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165560
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165561
    :cond_8
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 165562
    :cond_9
    iget-object v2, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->o()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 165563
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v3, 0x7f080606

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165564
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165565
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165566
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 165567
    :cond_a
    iget v2, p3, LX/396;->b:I

    if-lez v2, :cond_3

    .line 165568
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v3, 0x7f080602

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p3, LX/396;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165569
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165570
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165571
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 165572
    :pswitch_1
    iget-object v5, v0, LX/5N7;->b:Landroid/graphics/Bitmap;

    if-nez v5, :cond_c

    .line 165573
    iget-object v5, v0, LX/5N7;->f:Landroid/content/Context;

    const v6, 0x7f0805fe

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 165574
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165575
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165576
    iget-object v5, v0, LX/5N7;->f:Landroid/content/Context;

    const v6, 0x7f080605

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 165577
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165578
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165579
    const v5, 0x7f020a20

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 165580
    iget-object v5, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v5}, LX/0yc;->p()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 165581
    iget-object v5, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a02d5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 165582
    iget-object v6, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00d5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 165583
    invoke-virtual {v3, p0, v8, v8, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 165584
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a00d5

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 165585
    invoke-virtual {v4, p0, v8, v8, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 165586
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v3, v5, :cond_d

    .line 165587
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f020b3f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 165588
    :goto_4
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 165589
    :cond_b
    :goto_5
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, LX/5N7;->b:Landroid/graphics/Bitmap;

    .line 165590
    :cond_c
    iget-object v1, v0, LX/5N7;->b:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 165591
    :cond_d
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f020b3f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 165592
    :cond_e
    iget-object v2, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->o()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 165593
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v5, 0x7f080600

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165594
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165595
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165596
    iget-object v2, v0, LX/5N7;->f:Landroid/content/Context;

    const v3, 0x7f080607

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165597
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165598
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 165599
    :pswitch_2
    iget-object v3, v0, LX/5N7;->c:Landroid/graphics/Bitmap;

    if-nez v3, :cond_f

    .line 165600
    iget-object v3, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->o()Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v5, 0x7f080608

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 165601
    :goto_6
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165602
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165603
    const v3, 0x7f0208f8

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 165604
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, LX/5N7;->c:Landroid/graphics/Bitmap;

    .line 165605
    :cond_f
    iget-object v1, v0, LX/5N7;->c:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 165606
    :cond_10
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v5, 0x7f080604

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    .line 165607
    :pswitch_3
    iget-object v3, v0, LX/5N7;->d:Landroid/graphics/Bitmap;

    if-nez v3, :cond_11

    .line 165608
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080603

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 165609
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165610
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165611
    invoke-static {v0, v1}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, LX/5N7;->d:Landroid/graphics/Bitmap;

    .line 165612
    :cond_11
    iget-object v1, v0, LX/5N7;->d:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 165613
    :pswitch_4
    iget-object v3, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->o()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080608

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 165614
    :goto_7
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165615
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 165616
    :cond_12
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080604

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 165617
    :pswitch_5
    iget-object v3, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->o()Z

    move-result v3

    if-eqz v3, :cond_13

    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080607

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 165618
    :goto_8
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165619
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 165620
    :cond_13
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080605

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    .line 165621
    :pswitch_6
    iget-object v3, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->k()Z

    move-result v3

    if-eqz v3, :cond_14

    iget-object v3, v0, LX/5N7;->h:LX/1pl;

    .line 165622
    iget v4, v3, LX/1pl;->g:I

    move v3, v4

    .line 165623
    if-lez v3, :cond_14

    .line 165624
    iget-object v3, v0, LX/5N7;->h:LX/1pl;

    .line 165625
    iget v4, v3, LX/1pl;->g:I

    move v3, v4

    .line 165626
    iget-object v4, v0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f002c

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 165627
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165628
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 165629
    :cond_14
    iget-object v3, v0, LX/5N7;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->o()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 165630
    iget-object v3, v0, LX/5N7;->f:Landroid/content/Context;

    const v4, 0x7f080606

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p3, LX/396;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 165631
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165632
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(LX/0yL;)V
    .locals 1

    .prologue
    .line 165512
    if-nez p1, :cond_0

    .line 165513
    :goto_0
    return-void

    .line 165514
    :cond_0
    invoke-direct {p0}, LX/0yb;->y()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(LX/13j;)V
    .locals 1

    .prologue
    .line 165510
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0yb;->x:LX/0am;

    .line 165511
    return-void
.end method

.method public final a(LX/15i;I)V
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateQuota"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 165508
    iget-object v0, p0, LX/0yb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pl;

    invoke-virtual {v0, p1, p2}, LX/1pl;->b(LX/15i;I)V

    .line 165509
    return-void
.end method

.method public final a(LX/32P;)V
    .locals 3

    .prologue
    .line 165506
    iget-object v0, p0, LX/0yb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 165507
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 165497
    iget-object v0, p0, LX/0yb;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5N4;

    sget-object v1, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 165498
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 165499
    const-string v2, "tabbar_target_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165500
    const-string v0, "extra_launch_uri"

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165501
    const-string v0, "POP_TO_ROOT"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165502
    iget-object v0, p0, LX/0yb;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fJ;

    invoke-interface {v0}, LX/0fJ;->a()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 165503
    iget-object v0, p0, LX/0yb;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fJ;

    invoke-interface {v0, v1}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 165504
    iget-object v0, p0, LX/0yb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 165505
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 165496
    iget-object v0, p0, LX/0yb;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final a(LX/0yL;Landroid/net/Uri;Z)Z
    .locals 7
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165476
    invoke-virtual {p0}, LX/0yc;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 165477
    const-string v1, "dialtone_photocapping_degraded_image_click"

    invoke-static {p0, v1}, LX/0yb;->f(LX/0yb;Ljava/lang/String;)V

    .line 165478
    if-nez p2, :cond_1

    .line 165479
    invoke-static {p0}, LX/0yb;->C(LX/0yb;)V

    .line 165480
    :goto_0
    const/4 v0, 0x1

    .line 165481
    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0, p3}, LX/0yc;->a(Z)Z

    move-result v0

    goto :goto_1

    .line 165482
    :cond_1
    iget-object v1, p0, LX/0yb;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pm;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165483
    new-instance v3, LX/4GL;

    invoke-direct {v3}, LX/4GL;-><init>()V

    .line 165484
    const-string v4, "url"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165485
    move-object v3, v3

    .line 165486
    new-instance v4, LX/49j;

    invoke-direct {v4}, LX/49j;-><init>()V

    move-object v4, v4

    .line 165487
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/49j;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 165488
    iget-object v4, v1, LX/1pm;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v4, v3

    .line 165489
    new-instance v5, LX/5My;

    invoke-direct {v5, p0, p1, p2}, LX/5My;-><init>(LX/0yb;LX/0yL;Landroid/net/Uri;)V

    .line 165490
    iget-object v1, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    sget-object v2, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v6, LX/2nT;->SPINNER:LX/2nT;

    invoke-virtual/range {v1 .. v6}, LX/121;->a(LX/0yY;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;

    .line 165491
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v2

    .line 165492
    if-nez v2, :cond_2

    .line 165493
    invoke-static {p0}, LX/0yb;->D(LX/0yb;)V

    goto :goto_0

    .line 165494
    :cond_2
    iget-object v1, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    sget-object v3, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v2, v6}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    iput-object v1, p0, LX/0yb;->A:Landroid/support/v4/app/DialogFragment;

    .line 165495
    iget-object v1, p0, LX/0yb;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 165427
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 165428
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 165429
    :goto_0
    if-eqz v0, :cond_0

    const-string v3, "start"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "start"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 165430
    :cond_1
    const-string v0, "ref"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 165431
    const-string v0, "dialtone_explicitly_entered"

    invoke-static {p0, v0, v3}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    .line 165432
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 165433
    iget-boolean p2, v0, LX/13j;->g:Z

    move v0, p2

    .line 165434
    if-eqz v0, :cond_4

    .line 165435
    :cond_2
    invoke-virtual {p0, v3}, LX/0yc;->a(Ljava/lang/String;)Z

    .line 165436
    invoke-virtual {p0, p1}, LX/0yc;->a(Landroid/content/Context;)V

    :goto_1
    move v0, v2

    .line 165437
    :goto_2
    return v0

    .line 165438
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 165439
    :cond_4
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 165440
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 165441
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 165442
    iget-object v0, p0, LX/0yb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 165443
    goto :goto_2

    .line 165444
    :cond_6
    if-eqz v0, :cond_7

    const-string v3, "switch_to_dialtone"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    :cond_7
    const-string v3, "switch_to_dialtone"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 165445
    :cond_8
    const-string v0, "ref"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165446
    const-string v0, "switched_to_dialtone_via_intent"

    invoke-static {p0, v0, v1}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    .line 165447
    invoke-virtual {p0, v1}, LX/0yc;->a(Ljava/lang/String;)Z

    .line 165448
    if-nez v1, :cond_9

    iget-object v0, p0, LX/0yb;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dO;

    invoke-virtual {v0}, LX/0dO;->c()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, LX/0yb;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dO;

    sget-object v3, LX/0yY;->OPTIN_GROUP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v3}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 165449
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v3, LX/0dQ;->r:LX/0Tn;

    invoke-interface {v0, v3, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 165450
    :cond_9
    invoke-virtual {p0, p1}, LX/0yc;->a(Landroid/content/Context;)V

    .line 165451
    if-eqz v1, :cond_a

    const-string v0, "force_switch_to_dialtone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 165452
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->D:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 165453
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.facebook.zero.ACTION_ZERO_UPDATE_STATUS"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165454
    const-string v0, "zero_status_to_update"

    const-string v3, "force_switch_to_dialtone"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165455
    iget-object v0, p0, LX/0yb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 165456
    :cond_a
    iget-object v0, p0, LX/0yb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-string v1, "com.facebook.zero.ACTION_ZERO_INTERSTITIAL_REFRESH"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    move v0, v2

    .line 165457
    goto/16 :goto_2

    .line 165458
    :cond_b
    if-eqz v0, :cond_c

    const-string v3, "open_zb_portal"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    :cond_c
    const-string v3, "open_zb_portal"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 165459
    :cond_d
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v0

    .line 165460
    if-nez v0, :cond_13

    .line 165461
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 165462
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    new-instance v1, LX/5Mx;

    invoke-direct {v1, p0}, LX/5Mx;-><init>(LX/0yb;)V

    .line 165463
    iget-object p0, v0, LX/13j;->j:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165464
    :cond_e
    :goto_3
    move v0, v2

    .line 165465
    goto/16 :goto_2

    .line 165466
    :cond_f
    if-eqz v0, :cond_10

    const-string v2, "switch_to_full_fb"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    :cond_10
    const-string v0, "switch_to_full_fb"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 165467
    :cond_11
    const-string v0, "switched_to_full_fb_via_intent"

    const-string v2, ""

    invoke-static {p0, v0, v2}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    .line 165468
    const-string v0, "switch_to_full_fb"

    invoke-virtual {p0, v0}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 165469
    const-string v0, "ref"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165470
    if-eqz v0, :cond_12

    const-string v2, "dialtone_settings_screen"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 165471
    iget-object v0, p0, LX/0yb;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x87

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 165472
    if-eqz v0, :cond_12

    .line 165473
    invoke-virtual {p0, p1}, LX/0yc;->a(Landroid/content/Context;)V

    :cond_12
    move v0, v1

    .line 165474
    goto/16 :goto_2

    .line 165475
    :cond_13
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0yc;->a(Z)Z

    goto :goto_3
.end method

.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 5

    .prologue
    .line 165409
    iget-object v0, p0, LX/0yb;->w:LX/0yd;

    .line 165410
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/0yd;->b:LX/0ye;

    sget-object v3, LX/49z;->URI:LX/49z;

    invoke-virtual {v2, v3}, LX/0ye;->a(LX/49z;)LX/0Rf;

    move-result-object v2

    invoke-static {v1, v2}, LX/0yd;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    .line 165411
    const-string v1, "efg"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165412
    if-nez v1, :cond_5

    move v1, v2

    .line 165413
    :goto_0
    move v1, v1

    .line 165414
    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 165415
    if-nez v0, :cond_1

    iget-object v0, p0, LX/0yb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pl;

    invoke-virtual {v0, p1}, LX/1pl;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 165416
    :goto_2
    if-eqz v0, :cond_2

    .line 165417
    const-string v1, "dialtone_whitelisted_impression"

    sget-object v2, LX/49w;->URI:LX/49w;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, p2}, LX/0yb;->a(Ljava/lang/String;LX/49w;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 165418
    :cond_2
    return v0

    .line 165419
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 165420
    :cond_5
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 165421
    iget-object v1, v0, LX/0yd;->c:LX/0lC;

    const-class v4, Ljava/util/Map;

    invoke-virtual {v1, v3, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 165422
    const-string v3, "dtw"

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    goto :goto_0

    .line 165423
    :catch_0
    move v1, v2

    goto :goto_0

    .line 165424
    :catch_1
    move v1, v2

    goto :goto_0

    .line 165425
    :catch_2
    move v1, v2

    goto :goto_0

    .line 165426
    :catch_3
    move v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 3

    .prologue
    .line 165294
    iget-object v0, p0, LX/0yb;->w:LX/0yd;

    .line 165295
    iget-object v1, v0, LX/0yd;->b:LX/0ye;

    sget-object v2, LX/49z;->PHOTO:LX/49z;

    invoke-virtual {v1, v2}, LX/0ye;->a(LX/49z;)LX/0Rf;

    move-result-object v1

    invoke-static {p1, v1}, LX/0yd;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v1

    move v0, v1

    .line 165296
    if-eqz v0, :cond_0

    .line 165297
    const-string v1, "dialtone_whitelisted_impression"

    sget-object v2, LX/49w;->FEATURE_TAG:LX/49w;

    invoke-direct {p0, v1, v2, p1, p2}, LX/0yb;->a(Ljava/lang/String;LX/49w;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 165298
    :cond_0
    return v0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 165299
    invoke-virtual {p0}, LX/0yc;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165300
    iget-object v0, p0, LX/0yb;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "dialtone"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gatekeeper check failed"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165301
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->b()Z

    move v0, v1

    .line 165302
    :goto_0
    return v0

    .line 165303
    :cond_0
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 165304
    goto :goto_0

    .line 165305
    :cond_1
    const/4 v1, 0x1

    .line 165306
    invoke-static {p0, v1}, LX/0yb;->c(LX/0yb;Z)V

    .line 165307
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 165308
    iget-boolean v2, v0, LX/13j;->g:Z

    move v0, v2

    .line 165309
    if-eqz v0, :cond_4

    :cond_2
    const-string v0, "dialtone_enabled_cold"

    :goto_1
    if-eqz p1, :cond_5

    :goto_2
    invoke-static {p0, v0, p1}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    .line 165310
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->a()Z

    .line 165311
    invoke-static {p0, v1}, LX/0yb;->b(LX/0yb;Z)V

    .line 165312
    if-eqz p2, :cond_3

    .line 165313
    iget-object v0, p0, LX/0yb;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12L;

    invoke-virtual {v0, v1}, LX/12L;->a(Z)V

    .line 165314
    :cond_3
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->c()Z

    move-result v0

    invoke-static {p0, v0}, LX/0yb;->d(LX/0yb;Z)V

    .line 165315
    const/4 v0, 0x1

    goto :goto_0

    .line 165316
    :cond_4
    const-string v0, "dialtone_enabled_warm"

    goto :goto_1

    :cond_5
    const-string p1, ""

    goto :goto_2
.end method

.method public final a(Z)Z
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 165222
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 165223
    invoke-virtual {p0}, LX/0yc;->o()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 165224
    :cond_0
    :goto_0
    move v0, v0

    .line 165225
    if-eqz v0, :cond_1

    .line 165226
    const-string v0, "flex_photo_upgrade_without_interstitial"

    invoke-virtual {p0, v0}, LX/0yc;->b(Ljava/lang/String;)Z

    move v0, v3

    .line 165227
    :goto_1
    return v0

    .line 165228
    :cond_1
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 165229
    if-nez v1, :cond_2

    .line 165230
    invoke-static {p0}, LX/0yb;->D(LX/0yb;)V

    move v0, v2

    .line 165231
    goto :goto_1

    .line 165232
    :cond_2
    instance-of v0, v1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_8

    .line 165233
    const/4 v11, 0x1

    .line 165234
    iget-boolean v0, p0, LX/0yb;->B:Z

    if-eqz v0, :cond_e

    .line 165235
    :goto_2
    invoke-virtual {p0}, LX/0yc;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 165236
    sget-object v0, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    :goto_3
    move-object v2, v0

    .line 165237
    :goto_4
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v4}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 165238
    invoke-virtual {v2}, LX/0yY;->toString()Ljava/lang/String;

    move-result-object v0

    .line 165239
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "dialtone_upgrade_dialog_impression"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "dialtone"

    .line 165240
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 165241
    move-object v2, v1

    .line 165242
    const-string v1, "product_name"

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165243
    const-string v1, "is_photo"

    invoke-virtual {v2, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165244
    const-string v4, "carrier_id"

    iget-object v1, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v1, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 165245
    iget-object v1, p0, LX/0yb;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 165246
    invoke-virtual {p0}, LX/0yc;->i()V

    move v0, v3

    .line 165247
    goto :goto_1

    .line 165248
    :cond_3
    invoke-virtual {p0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165249
    sget-object v0, LX/0yY;->FLEX_PLUS:LX/0yY;

    goto :goto_3

    .line 165250
    :cond_4
    if-eqz p1, :cond_6

    .line 165251
    invoke-static {p0}, LX/0yb;->A(LX/0yb;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    :goto_5
    move-object v2, v0

    goto :goto_4

    :cond_5
    sget-object v0, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    goto :goto_5

    .line 165252
    :cond_6
    invoke-static {p0}, LX/0yb;->A(LX/0yb;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    goto :goto_3

    :cond_7
    sget-object v0, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    goto :goto_3

    .line 165253
    :cond_8
    iget-object v0, p0, LX/0yb;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "dialtone"

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Attempting to show upgrade dialog but current activity is not a fragment activity: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 165254
    goto/16 :goto_1

    .line 165255
    :cond_9
    invoke-virtual {p0}, LX/0yc;->t()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p0}, LX/0yc;->u()Z

    move-result v4

    if-nez v4, :cond_0

    .line 165256
    :cond_a
    invoke-static {p0}, LX/0yb;->A(LX/0yb;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 165257
    if-eqz p1, :cond_b

    invoke-virtual {p0}, LX/0yc;->q()Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_b
    if-nez p1, :cond_c

    invoke-virtual {p0}, LX/0yc;->r()Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_c
    move v0, v1

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 165258
    goto/16 :goto_0

    .line 165259
    :cond_e
    new-instance v2, LX/5Mz;

    invoke-direct {v2, p0}, LX/5Mz;-><init>(LX/0yb;)V

    .line 165260
    iget-object v0, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0805f1

    new-array v6, v11, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/0df;->j:LX/0Tn;

    iget-object v9, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f08062c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 165261
    invoke-static {p0}, LX/0yb;->A(LX/0yb;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 165262
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v5, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    iget-object v6, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0805ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6, v4, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 165263
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v5, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    iget-object v6, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0805ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6, v4, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 165264
    :goto_6
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v4, LX/0yY;->FLEX_PLUS:LX/0yY;

    iget-object v5, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0805f0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0805f2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 165265
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v4, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    iget-object v5, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0805f4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0805f5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 165266
    iput-boolean v11, p0, LX/0yb;->B:Z

    goto/16 :goto_2

    .line 165267
    :cond_f
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v5, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    iget-object v6, p0, LX/0yb;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0805ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6, v4, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    goto :goto_6
.end method

.method public final b(LX/0yL;)V
    .locals 1

    .prologue
    .line 165219
    if-nez p1, :cond_0

    .line 165220
    :goto_0
    return-void

    .line 165221
    :cond_0
    invoke-direct {p0}, LX/0yb;->y()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 165216
    invoke-virtual {p0}, LX/0yc;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165217
    const/4 v0, 0x0

    .line 165218
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/0yb;->w()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 4

    .prologue
    .line 165212
    iget-object v0, p0, LX/0yb;->w:LX/0yd;

    invoke-virtual {v0, p1}, LX/0yd;->b(Landroid/net/Uri;)Z

    move-result v0

    .line 165213
    if-eqz v0, :cond_0

    .line 165214
    const-string v1, "dialtone_whitelisted_impression"

    sget-object v2, LX/49w;->IMAGE_SIZE:LX/49w;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, p2}, LX/0yb;->a(Ljava/lang/String;LX/49w;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 165215
    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/String;Z)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165199
    invoke-direct {p0}, LX/0yb;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165200
    const/4 v0, 0x0

    .line 165201
    :goto_0
    return v0

    .line 165202
    :cond_0
    const/4 v1, 0x0

    .line 165203
    invoke-static {p0, v1}, LX/0yb;->c(LX/0yb;Z)V

    .line 165204
    const-string v0, "dialtone_disabled"

    if-eqz p1, :cond_2

    :goto_1
    invoke-static {p0, v0, p1}, LX/0yb;->a(LX/0yb;Ljava/lang/String;Ljava/lang/String;)V

    .line 165205
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->b()Z

    .line 165206
    invoke-static {p0, v1}, LX/0yb;->b(LX/0yb;Z)V

    .line 165207
    if-eqz p2, :cond_1

    .line 165208
    iget-object v0, p0, LX/0yb;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12L;

    invoke-virtual {v0, v1}, LX/12L;->a(Z)V

    .line 165209
    :cond_1
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->c()Z

    move-result v0

    invoke-static {p0, v0}, LX/0yb;->d(LX/0yb;Z)V

    .line 165210
    const/4 v0, 0x1

    goto :goto_0

    .line 165211
    :cond_2
    const-string p1, ""

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 165196
    iget-object v0, p0, LX/0yb;->w:LX/0yd;

    .line 165197
    iget-object v1, v0, LX/0yd;->b:LX/0ye;

    sget-object p0, LX/49z;->VIDEO:LX/49z;

    invoke-virtual {v1, p0}, LX/0ye;->a(LX/49z;)LX/0Rf;

    move-result-object v1

    invoke-static {p1, v1}, LX/0yd;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v1

    move v0, v1

    .line 165198
    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 165193
    iget-object v0, p0, LX/0yb;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5N7;

    .line 165194
    iget-object v1, v0, LX/5N7;->f:Landroid/content/Context;

    const/high16 p0, 0x43300000    # 176.0f

    invoke-static {v1, p0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    move v0, v1

    .line 165195
    return v0
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 165188
    iget-object v0, p0, LX/0yb;->w:LX/0yd;

    .line 165189
    iget-object v1, v0, LX/0yd;->b:LX/0ye;

    sget-object v2, LX/49z;->FACEWEB:LX/49z;

    invoke-virtual {v1, v2}, LX/0ye;->a(LX/49z;)LX/0Rf;

    move-result-object v1

    invoke-static {p1, v1}, LX/0yd;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v1

    move v0, v1

    .line 165190
    if-eqz v0, :cond_0

    .line 165191
    const-string v1, "dialtone_whitelisted_impression"

    sget-object v2, LX/49w;->FACEWEB:LX/49w;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, p1, v3}, LX/0yb;->a(Ljava/lang/String;LX/49w;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 165192
    :cond_0
    return v0
.end method

.method public final e()Landroid/app/Activity;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 165182
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165183
    iget-object v0, p0, LX/0yb;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "dialtone"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "trying to switch to dialtone without any activity present!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 165184
    const/4 v0, 0x0

    .line 165185
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 165186
    iget-object v1, v0, LX/13j;->f:Landroid/app/Activity;

    move-object v0, v1

    .line 165187
    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 165169
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    invoke-virtual {v0}, LX/13j;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165170
    :cond_0
    :goto_0
    return-void

    .line 165171
    :cond_1
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v3

    .line 165172
    if-nez v3, :cond_2

    .line 165173
    invoke-static {p0}, LX/0yb;->D(LX/0yb;)V

    goto :goto_0

    .line 165174
    :cond_2
    new-instance v4, Landroid/content/Intent;

    const-class v0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    invoke-direct {v4, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165175
    const/high16 v0, 0x4000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 165176
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 165177
    const-string v0, "dialtone_wrong_carrier_flag"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165178
    :goto_2
    iget-object v0, p0, LX/0yb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0x3e8

    invoke-interface {v0, v4, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 165179
    :sswitch_0
    const-string v5, "unsupported_carrier"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v5, "unsupported_category"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v5, "not_in_region"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    .line 165180
    :pswitch_0
    const-string v0, "dialtone_wrong_carrier_flag"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 165181
    :pswitch_1
    const-string v0, "dialtone_not_in_region_flag"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x323f931e -> :sswitch_2
        0x3369a2ee -> :sswitch_0
        0x3c7c77c8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 165164
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165165
    iget-object v0, p0, LX/0yb;->r:LX/0ZA;

    invoke-virtual {v0}, LX/0ZA;->c()Z

    move-result v0

    .line 165166
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 165167
    iget-boolean p0, v0, LX/13j;->h:Z

    move v0, p0

    .line 165168
    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 165156
    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    invoke-virtual {v0}, LX/13j;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165157
    :cond_0
    :goto_0
    return-void

    .line 165158
    :cond_1
    invoke-virtual {p0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 165159
    if-nez v1, :cond_2

    .line 165160
    invoke-static {p0}, LX/0yb;->D(LX/0yb;)V

    goto :goto_0

    .line 165161
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165162
    const/high16 v0, 0x4000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 165163
    iget-object v0, p0, LX/0yb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x3e8

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 165153
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165154
    :goto_0
    return-void

    .line 165155
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0yb;->d(LX/0yb;Z)V

    goto :goto_0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 165268
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->v:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 165269
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/0dQ;->v:LX/0Tn;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 165270
    return-void
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 165271
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 165293
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 165272
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_FEED_CAPPING:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 165273
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 165274
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->FLEX_PLUS:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 165275
    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DATA_SAVING_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 165276
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->VIDEO_SCREENCAP:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 165277
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 165278
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 165279
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 165280
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->DIALTONE_SHOW_INTERSTITIAL_WITH_CAP:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 165281
    iget-object v0, p0, LX/0yb;->v:LX/0W3;

    sget-wide v2, LX/0X5;->bZ:J

    const/4 v4, 0x5

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v2

    .line 165282
    iget-object v0, p0, LX/0yb;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dQ;->v:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 165283
    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final v()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165284
    iget-object v0, p0, LX/0yb;->v:LX/0W3;

    sget-wide v2, LX/0X5;->ca:J

    const-string v1, "FETCH_DID_NOT_RETURN_RESULTS"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165285
    const/4 v2, 0x0

    .line 165286
    const-string v1, "FETCH_DID_NOT_RETURN_RESULTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 165287
    :try_start_0
    iget-object v1, p0, LX/0yb;->D:LX/0lC;

    new-instance v3, LX/5N0;

    invoke-direct {v3, p0}, LX/5N0;-><init>(LX/0yb;)V

    invoke-virtual {v1, v0, v3}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165288
    :goto_0
    if-nez v0, :cond_0

    .line 165289
    sget-object v0, LX/0yb;->a:Ljava/util/List;

    .line 165290
    :cond_0
    return-object v0

    .line 165291
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 165292
    iget-object v0, p0, LX/0yb;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "getBlacklistedIntents"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method
