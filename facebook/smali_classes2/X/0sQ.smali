.class public LX/0sQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private final c:LX/0So;

.field public final d:Ljava/util/Random;

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Zb;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Ljava/util/Random;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151909
    iput-object p2, p0, LX/0sQ;->a:LX/0Zb;

    .line 151910
    iput-object p3, p0, LX/0sQ;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 151911
    iput-object p1, p0, LX/0sQ;->e:LX/0ad;

    .line 151912
    iput-object p4, p0, LX/0sQ;->c:LX/0So;

    .line 151913
    iput-object p5, p0, LX/0sQ;->d:Ljava/util/Random;

    .line 151914
    sput-object p0, LX/0sR;->a:LX/0sQ;

    .line 151915
    return-void
.end method

.method public static b(LX/0QB;)LX/0sQ;
    .locals 6

    .prologue
    .line 151916
    new-instance v0, LX/0sQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v5

    check-cast v5, Ljava/util/Random;

    invoke-direct/range {v0 .. v5}, LX/0sQ;-><init>(LX/0ad;LX/0Zb;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Ljava/util/Random;)V

    .line 151917
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 151918
    invoke-virtual {p0}, LX/0sQ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151919
    iget-object v6, p0, LX/0sQ;->e:LX/0ad;

    sget v7, LX/16l;->c:I

    const/16 v8, 0xa

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    int-to-long v6, v6

    .line 151920
    iget-object v8, p0, LX/0sQ;->d:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextLong()J

    move-result-wide v8

    rem-long v6, v8, v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    const/4 v6, 0x1

    :goto_0
    move v0, v6

    .line 151921
    if-eqz v0, :cond_2

    .line 151922
    instance-of v0, p3, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v0, :cond_3

    .line 151923
    check-cast p3, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v1

    .line 151924
    :goto_1
    new-instance v2, LX/49J;

    iget-object v0, p0, LX/0sQ;->c:LX/0So;

    instance-of v3, v1, LX/15i;

    invoke-direct {v2, p1, p2, v0, v3}, LX/49J;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0So;Z)V

    .line 151925
    instance-of v0, v1, LX/15w;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 151926
    check-cast v0, LX/15w;

    invoke-virtual {v0}, LX/15w;->j()LX/12V;

    move-result-object v0

    invoke-static {v0}, LX/0sR;->a(LX/12V;)Ljava/lang/String;

    move-result-object v0

    .line 151927
    if-eqz v0, :cond_1

    .line 151928
    iget-object v3, v2, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 151929
    :cond_0
    iget-object v3, v2, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151930
    :cond_1
    invoke-static {v1, v2}, LX/0sR;->a(Ljava/lang/Object;LX/49J;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151931
    new-instance v0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;

    iget-object v3, p0, LX/0sQ;->a:LX/0Zb;

    invoke-direct {v0, v3, v2, v1}, Lcom/facebook/debug/fieldusage/FieldUsageReporter;-><init>(LX/0Zb;LX/49J;Ljava/lang/Object;)V

    .line 151932
    iget-object v1, p0, LX/0sQ;->e:LX/0ad;

    sget v2, LX/16l;->a:I

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    .line 151933
    if-lez v1, :cond_2

    .line 151934
    iget-object v2, p0, LX/0sQ;->b:Ljava/util/concurrent/ScheduledExecutorService;

    int-to-long v4, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v0, v4, v5, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 151935
    :cond_2
    return-void

    :cond_3
    move-object v1, p3

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 151936
    iget-object v1, p0, LX/0sQ;->e:LX/0ad;

    sget v2, LX/16l;->a:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
