.class public final LX/0q7;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/0pE;

.field private final b:LX/0mU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/2u6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0pE;Landroid/os/Looper;LX/0mU;)V
    .locals 0
    .param p3    # LX/0mU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147108
    iput-object p1, p0, LX/0q7;->a:LX/0pE;

    .line 147109
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 147110
    iput-object p3, p0, LX/0q7;->b:LX/0mU;

    .line 147111
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 147103
    iget-object v0, p0, LX/0q7;->b:LX/0mU;

    if-eqz v0, :cond_0

    .line 147104
    const-string v0, "doWaitForWriteBlockRelease"

    const v1, -0x364e7efb

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147105
    iget-object v0, p0, LX/0q7;->b:LX/0mU;

    invoke-virtual {v0}, LX/0mU;->a()V

    .line 147106
    const v0, -0x23c2cdd8

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147107
    :cond_0
    return-void
.end method

.method private static b(LX/0q7;)LX/2DB;
    .locals 12

    .prologue
    .line 147082
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->e:LX/2DB;

    if-nez v0, :cond_1

    .line 147083
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, v1, LX/0pE;->c:LX/0q6;

    .line 147084
    iget-object v2, v1, LX/0q6;->a:Landroid/content/Context;

    invoke-static {v2}, LX/2Cp;->a(Landroid/content/Context;)LX/2Cp;

    move-result-object v2

    .line 147085
    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v3

    .line 147086
    invoke-static {v2}, LX/2Cp;->b(LX/2Cp;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move v3, v4

    .line 147087
    move v2, v3

    .line 147088
    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/2D7;->a(Z)LX/2D7;

    move-result-object v8

    .line 147089
    iget-object v2, v1, LX/0q6;->a:Landroid/content/Context;

    iget-object v3, v1, LX/0q6;->c:Ljava/lang/String;

    .line 147090
    new-instance v4, Ljava/io/File;

    .line 147091
    const-string v5, "analytics"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    move-object v5, v5

    .line 147092
    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v9, v4

    .line 147093
    new-instance v11, LX/2DB;

    new-instance v2, LX/2DC;

    iget-object v3, v1, LX/0q6;->d:LX/0me;

    invoke-interface {v3}, LX/0me;->a()I

    move-result v3

    iget-object v4, v1, LX/0q6;->e:LX/0me;

    invoke-interface {v4}, LX/0me;->a()I

    move-result v4

    iget-object v5, v1, LX/0q6;->f:LX/0mm;

    iget-object v6, v1, LX/0q6;->g:LX/0Zh;

    .line 147094
    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v7

    .line 147095
    if-nez v7, :cond_0

    .line 147096
    const-string v7, "unknown"

    .line 147097
    :cond_0
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v9, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v7, v10

    .line 147098
    move-object v7, v7

    .line 147099
    invoke-direct/range {v2 .. v8}, LX/2DC;-><init>(IILX/0mm;LX/0Zh;Ljava/io/File;LX/2D7;)V

    new-instance v3, LX/2DG;

    iget-object v4, v1, LX/0q6;->a:Landroid/content/Context;

    iget v5, v1, LX/0q6;->b:I

    new-instance v6, LX/2DI;

    iget-object v7, v1, LX/0q6;->h:LX/0pC;

    invoke-direct {v6, v9, v7}, LX/2DI;-><init>(Ljava/io/File;LX/0pC;)V

    iget-object v7, v1, LX/0q6;->i:LX/0ma;

    iget-object v8, v1, LX/0q6;->j:Ljava/lang/Class;

    iget-object v9, v1, LX/0q6;->k:LX/0mg;

    invoke-interface {v9}, LX/0mg;->a()LX/0mi;

    move-result-object v9

    iget-object v10, v1, LX/0q6;->k:LX/0mg;

    invoke-interface {v10}, LX/0mg;->b()LX/0mi;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/2DG;-><init>(Landroid/content/Context;ILX/2DI;LX/0ma;Ljava/lang/Class;LX/0mi;LX/0mi;)V

    invoke-direct {v11, v2, v3}, LX/2DB;-><init>(LX/2DD;LX/2DH;)V

    move-object v1, v11

    .line 147100
    iput-object v1, v0, LX/0pE;->e:LX/2DB;

    .line 147101
    :cond_1
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->e:LX/2DB;

    return-object v0

    .line 147102
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private b(LX/0q8;)V
    .locals 2

    .prologue
    .line 147112
    const-string v0, "doBootstrapNewSession"

    const v1, 0x26c8840d

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147113
    :try_start_0
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->d:LX/0mk;

    invoke-virtual {p1, v0}, LX/0q8;->a(LX/0mk;)LX/2u6;

    move-result-object v0

    iput-object v0, p0, LX/0q7;->c:LX/2u6;

    .line 147114
    invoke-static {p0}, LX/0q7;->b(LX/0q7;)LX/2DB;

    move-result-object v0

    iget-object v1, p0, LX/0q7;->c:LX/2u6;

    invoke-virtual {v0, v1}, LX/2DB;->a(LX/2u6;)V

    .line 147115
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    if-eqz v0, :cond_0

    .line 147116
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    iget-object v1, p0, LX/0q7;->c:LX/2u6;

    invoke-virtual {v0, v1}, LX/2DB;->a(LX/2u6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147117
    :cond_0
    const v0, -0x73e22a92

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147118
    return-void

    .line 147119
    :catchall_0
    move-exception v0

    const v1, -0x57c88328

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private b(LX/2u6;)V
    .locals 2

    .prologue
    .line 147074
    const-string v0, "doStartNewSession"

    const v1, -0x2af4b8d8

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147075
    :try_start_0
    iput-object p1, p0, LX/0q7;->c:LX/2u6;

    .line 147076
    invoke-static {p0}, LX/0q7;->b(LX/0q7;)LX/2DB;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2DB;->b(LX/2u6;)V

    .line 147077
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    if-eqz v0, :cond_0

    .line 147078
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    iget-object v1, p0, LX/0q7;->c:LX/2u6;

    invoke-virtual {v0, v1}, LX/2DB;->b(LX/2u6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147079
    :cond_0
    const v0, 0x13641296

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147080
    return-void

    .line 147081
    :catchall_0
    move-exception v0

    const v1, -0x2edac57f

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147067
    const-string v0, "doUserLogout"

    const v1, -0x75a671a9

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147068
    :try_start_0
    invoke-static {p0}, LX/0q7;->b(LX/0q7;)LX/2DB;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2DB;->a(Ljava/lang/String;)V

    .line 147069
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    if-eqz v0, :cond_0

    .line 147070
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    invoke-virtual {v0, p1}, LX/2DB;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147071
    :cond_0
    const v0, 0x7fd1886b

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147072
    return-void

    .line 147073
    :catchall_0
    move-exception v0

    const v1, 0x5eeef14a

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private c(LX/0n9;)V
    .locals 8

    .prologue
    .line 147041
    const-string v0, "doWrite"

    const v1, 0x47bfe7d7

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147042
    :try_start_0
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->b:LX/0mI;

    if-eqz v0, :cond_0

    .line 147043
    const-string v0, "eventListener"

    const v1, -0x67649586

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 147044
    :try_start_1
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->b:LX/0mI;

    invoke-interface {v0}, LX/0mI;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147045
    const v0, -0x4e6edd97

    :try_start_2
    invoke-static {v0}, LX/03q;->a(I)V

    .line 147046
    :cond_0
    const-string v0, "writeToDisk"

    const v1, -0x111e321e

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147047
    :try_start_3
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->e:LX/2DB;

    invoke-virtual {v0, p1}, LX/2DB;->a(LX/0nA;)V
    :try_end_3
    .catch LX/3zv; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 147048
    const v0, -0x1891cb47

    :try_start_4
    invoke-static {v0}, LX/03q;->a(I)V

    .line 147049
    invoke-virtual {p1}, LX/0nA;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 147050
    :goto_0
    const v0, 0x205d0874

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147051
    return-void

    .line 147052
    :catchall_0
    move-exception v0

    const v1, 0x43cd8aa

    :try_start_5
    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 147053
    :catchall_1
    move-exception v0

    const v1, -0x19b6a494

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0

    .line 147054
    :catch_0
    :goto_1
    :try_start_6
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    if-nez v0, :cond_1

    .line 147055
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, v1, LX/0pE;->c:LX/0q6;

    .line 147056
    new-instance v2, LX/2DB;

    new-instance v3, LX/3zy;

    iget-object v4, v1, LX/0q6;->d:LX/0me;

    invoke-interface {v4}, LX/0me;->a()I

    move-result v4

    iget-object v5, v1, LX/0q6;->e:LX/0me;

    invoke-interface {v5}, LX/0me;->a()I

    move-result v5

    iget-object v6, v1, LX/0q6;->f:LX/0mm;

    iget-object v7, v1, LX/0q6;->g:LX/0Zh;

    invoke-direct {v3, v4, v5, v6, v7}, LX/3zy;-><init>(IILX/0mm;LX/0Zh;)V

    new-instance v4, LX/406;

    iget-object v5, v1, LX/0q6;->a:Landroid/content/Context;

    iget-object v6, v1, LX/0q6;->g:LX/0Zh;

    iget-object v7, v1, LX/0q6;->h:LX/0pC;

    invoke-direct {v4, v5, v6, v7}, LX/406;-><init>(Landroid/content/Context;LX/0Zh;LX/0pC;)V

    invoke-direct {v2, v3, v4}, LX/2DB;-><init>(LX/2DD;LX/2DH;)V

    move-object v1, v2

    .line 147057
    iput-object v1, v0, LX/0pE;->f:LX/2DB;

    .line 147058
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    iget-object v1, p0, LX/0q7;->c:LX/2u6;

    invoke-virtual {v0, v1}, LX/2DB;->a(LX/2u6;)V

    .line 147059
    :cond_1
    iget-object v0, p0, LX/0q7;->a:LX/0pE;

    iget-object v0, v0, LX/0pE;->f:LX/2DB;

    move-object v0, v0

    .line 147060
    invoke-virtual {v0, p1}, LX/2DB;->a(LX/0nA;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 147061
    const v0, 0x7d3fbff1

    :try_start_7
    invoke-static {v0}, LX/03q;->a(I)V

    .line 147062
    invoke-virtual {p1}, LX/0nA;->a()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 147063
    :catch_1
    move-exception v0

    .line 147064
    :try_start_8
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 147065
    :catchall_2
    move-exception v0

    const v1, 0x83786c0

    :try_start_9
    invoke-static {v1}, LX/03q;->a(I)V

    .line 147066
    invoke-virtual {p1}, LX/0nA;->a()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catch_2
    goto :goto_1
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 147022
    const-string v0, "handleMessage"

    const v1, -0x43624556

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 147023
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 147024
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147025
    :catchall_0
    move-exception v0

    const v1, -0x7c3360a6

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0

    .line 147026
    :pswitch_0
    :try_start_1
    invoke-direct {p0}, LX/0q7;->a()V

    .line 147027
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0n9;

    invoke-direct {p0, v0}, LX/0q7;->c(LX/0n9;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147028
    :goto_0
    const v0, -0x1715ccfa

    invoke-static {v0}, LX/03q;->a(I)V

    .line 147029
    return-void

    .line 147030
    :pswitch_1
    :try_start_2
    invoke-direct {p0}, LX/0q7;->a()V

    .line 147031
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0q8;

    invoke-direct {p0, v0}, LX/0q7;->b(LX/0q8;)V

    goto :goto_0

    .line 147032
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/2u6;

    invoke-direct {p0, v0}, LX/0q7;->b(LX/2u6;)V

    goto :goto_0

    .line 147033
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LX/0q7;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 147034
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/40B;

    .line 147035
    iget-object v1, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, v1, LX/0pE;->g:LX/40D;

    if-eqz v1, :cond_0

    .line 147036
    iget-object v1, p0, LX/0q7;->a:LX/0pE;

    iget-object v1, v1, LX/0pE;->g:LX/40D;

    .line 147037
    iget-object v2, v0, LX/40B;->a:Ljava/lang/String;

    move-object v2, v2

    .line 147038
    iget-object p1, v0, LX/40B;->b:Ljava/util/Map;

    move-object p1, p1

    .line 147039
    iget-object p0, v1, LX/40D;->a:LX/4lQ;

    invoke-virtual {p0, v2, p1}, LX/4lQ;->a(Ljava/lang/String;Ljava/util/Map;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147040
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
