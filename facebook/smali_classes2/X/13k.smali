.class public LX/13k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/13k;


# instance fields
.field public a:Landroid/app/Activity;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177185
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/13k;->b:Z

    .line 177186
    return-void
.end method

.method public static a(LX/0QB;)LX/13k;
    .locals 3

    .prologue
    .line 177187
    sget-object v0, LX/13k;->c:LX/13k;

    if-nez v0, :cond_1

    .line 177188
    const-class v1, LX/13k;

    monitor-enter v1

    .line 177189
    :try_start_0
    sget-object v0, LX/13k;->c:LX/13k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177190
    if-eqz v2, :cond_0

    .line 177191
    :try_start_1
    new-instance v0, LX/13k;

    invoke-direct {v0}, LX/13k;-><init>()V

    .line 177192
    move-object v0, v0

    .line 177193
    sput-object v0, LX/13k;->c:LX/13k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177194
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177195
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177196
    :cond_1
    sget-object v0, LX/13k;->c:LX/13k;

    return-object v0

    .line 177197
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
