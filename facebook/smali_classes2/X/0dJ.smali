.class public LX/0dJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0dJ;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0dM;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0dM;",
            ">;>;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90206
    iput-object p1, p0, LX/0dJ;->a:LX/0Ot;

    .line 90207
    iput-object p2, p0, LX/0dJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90208
    return-void
.end method

.method public static a(LX/0QB;)LX/0dJ;
    .locals 5

    .prologue
    .line 90189
    sget-object v0, LX/0dJ;->c:LX/0dJ;

    if-nez v0, :cond_1

    .line 90190
    const-class v1, LX/0dJ;

    monitor-enter v1

    .line 90191
    :try_start_0
    sget-object v0, LX/0dJ;->c:LX/0dJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90192
    if-eqz v2, :cond_0

    .line 90193
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90194
    new-instance v4, LX/0dJ;

    .line 90195
    new-instance v3, LX/0dK;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0dK;-><init>(LX/0QB;)V

    move-object v3, v3

    .line 90196
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v3, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v3

    move-object p0, v3

    .line 90197
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, p0, v3}, LX/0dJ;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 90198
    move-object v0, v4

    .line 90199
    sput-object v0, LX/0dJ;->c:LX/0dJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90200
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90201
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90202
    :cond_1
    sget-object v0, LX/0dJ;->c:LX/0dJ;

    return-object v0

    .line 90203
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90204
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 90209
    const-string v0, "INeedInitForSharedPrefsListenerRegister-RegisterListeners"

    const v1, 0x67e970bb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 90210
    :try_start_0
    invoke-static {p0}, LX/0dJ;->b(LX/0dJ;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dM;

    .line 90211
    iget-object v2, p0, LX/0dJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0, v2}, LX/0dM;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90212
    :catchall_0
    move-exception v0

    const v1, -0x1fba67e5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, 0x43bd873b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 90213
    return-void
.end method

.method private static b(LX/0dJ;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0dM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90186
    const-string v0, "INeedInitForSharedPrefsListenerRegister-RegisterListeners#construct"

    const v1, 0x8adf1ca

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 90187
    :try_start_0
    iget-object v0, p0, LX/0dJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90188
    const v1, -0x6c0e197e

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x7cb76669

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 90184
    invoke-direct {p0}, LX/0dJ;->a()V

    .line 90185
    return-void
.end method
