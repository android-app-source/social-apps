.class public final LX/0gE;
.super LX/0gF;
.source ""


# instance fields
.field public a:LX/0wJ;

.field public final synthetic b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

.field public final c:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/0gc;)V
    .locals 2

    .prologue
    .line 111652
    iput-object p1, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 111653
    invoke-direct {p0, p2}, LX/0gF;-><init>(LX/0gc;)V

    .line 111654
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/0gE;->c:LX/0Ri;

    .line 111655
    new-instance v0, LX/0wJ;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/0gE;->a:LX/0wJ;

    .line 111656
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 111663
    iget-object v0, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GdQ;

    .line 111664
    iget-object p0, v0, LX/GdQ;->c:Ljava/lang/String;

    move-object v0, p0

    .line 111665
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 111661
    iget-object v1, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, p0, LX/0gE;->c:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;)I

    move-result v0

    .line 111662
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x2

    :cond_0
    return v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 111666
    iget-object v0, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GdQ;

    .line 111667
    iget-object v1, v0, LX/GdQ;->b:Ljava/lang/String;

    move-object v1, v1

    .line 111668
    iget-object v2, p0, LX/0gE;->c:LX/0Ri;

    invoke-interface {v2, v1}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 111669
    iget-object v2, p0, LX/0gE;->c:LX/0Ri;

    invoke-virtual {v0}, LX/GdQ;->c()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-interface {v2, v1, v0}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111670
    :cond_0
    iget-object v0, p0, LX/0gE;->c:LX/0Ri;

    invoke-interface {v0, v1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 111660
    iget-object v0, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 111657
    iget-object v0, p0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GdQ;

    .line 111658
    iget-object p0, v0, LX/GdQ;->b:Ljava/lang/String;

    move-object v0, p0

    .line 111659
    return-object v0
.end method
