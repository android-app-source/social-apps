.class public LX/1lg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 312239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312240
    return-void
.end method

.method public static a(Ljava/io/InputStream;)I
    .locals 15

    .prologue
    const/4 v0, 0x0

    .line 312241
    :try_start_0
    const/4 v5, 0x2

    const/4 v3, 0x0

    .line 312242
    const/16 v2, 0xe1

    const/16 v10, 0xff

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 312243
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312244
    :cond_0
    :goto_0
    invoke-static {p0, v7, v8}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v9

    if-ne v9, v10, :cond_9

    move v9, v10

    .line 312245
    :goto_1
    if-ne v9, v10, :cond_1

    .line 312246
    invoke-static {p0, v7, v8}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v9

    goto :goto_1

    .line 312247
    :cond_1
    const/16 v11, 0xc0

    if-ne v2, v11, :cond_6

    .line 312248
    packed-switch v9, :pswitch_data_0

    .line 312249
    :pswitch_0
    const/4 v11, 0x0

    :goto_2
    move v11, v11

    .line 312250
    if-eqz v11, :cond_6

    .line 312251
    :cond_2
    :goto_3
    move v2, v7

    .line 312252
    if-eqz v2, :cond_5

    .line 312253
    invoke-static {p0, v5, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    .line 312254
    const/4 v4, 0x6

    if-le v2, v4, :cond_5

    .line 312255
    const/4 v4, 0x4

    invoke-static {p0, v4, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v4

    .line 312256
    add-int/lit8 v2, v2, -0x4

    .line 312257
    invoke-static {p0, v5, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v5

    .line 312258
    add-int/lit8 v2, v2, -0x2

    .line 312259
    const v6, 0x45786966

    if-ne v4, v6, :cond_5

    if-nez v5, :cond_5

    .line 312260
    :goto_4
    move v1, v2

    .line 312261
    if-nez v1, :cond_3

    .line 312262
    :goto_5
    return v0

    .line 312263
    :cond_3
    const/4 v2, 0x0

    .line 312264
    new-instance v3, LX/4fQ;

    invoke-direct {v3}, LX/4fQ;-><init>()V

    .line 312265
    const v10, 0x49492a00    # 823968.0f

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v5, 0x0

    .line 312266
    if-gt v1, v9, :cond_d

    .line 312267
    :goto_6
    move v4, v5

    .line 312268
    iget v5, v3, LX/4fQ;->c:I

    add-int/lit8 v5, v5, -0x8

    .line 312269
    if-eqz v4, :cond_4

    if-le v5, v4, :cond_a

    .line 312270
    :cond_4
    :goto_7
    move v0, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 312271
    goto :goto_5

    .line 312272
    :catch_0
    goto :goto_5

    :cond_5
    :try_start_1
    move v2, v3

    goto :goto_4

    .line 312273
    :cond_6
    if-eq v9, v2, :cond_2

    .line 312274
    const/16 v11, 0xd8

    if-eq v9, v11, :cond_0

    if-eq v9, v7, :cond_0

    .line 312275
    const/16 v11, 0xd9

    if-eq v9, v11, :cond_7

    const/16 v11, 0xda

    if-ne v9, v11, :cond_8

    :cond_7
    move v7, v8

    .line 312276
    goto :goto_3

    .line 312277
    :cond_8
    const/4 v9, 0x2

    invoke-static {p0, v9, v8}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    .line 312278
    int-to-long v11, v9

    invoke-virtual {p0, v11, v12}, Ljava/io/InputStream;->skip(J)J

    goto :goto_0

    :cond_9
    move v7, v8

    .line 312279
    goto :goto_3

    .line 312280
    :pswitch_1
    const/4 v11, 0x1

    goto :goto_2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 312281
    :cond_a
    int-to-long v6, v5

    invoke-virtual {p0, v6, v7}, Ljava/io/InputStream;->skip(J)J

    .line 312282
    sub-int v2, v4, v5

    .line 312283
    iget-boolean v4, v3, LX/4fQ;->a:Z

    const/16 v5, 0x112

    const/4 v14, 0x2

    const/4 v8, 0x0

    .line 312284
    const/16 v9, 0xe

    if-ge v2, v9, :cond_12

    .line 312285
    :cond_b
    :goto_8
    move v2, v8

    .line 312286
    iget-boolean v3, v3, LX/4fQ;->a:Z

    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 312287
    const/16 v5, 0xa

    if-ge v2, v5, :cond_14

    .line 312288
    :cond_c
    :goto_9
    move v2, v4

    .line 312289
    goto :goto_7

    .line 312290
    :cond_d
    invoke-static {p0, v8, v5}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v4

    iput v4, v3, LX/4fQ;->b:I

    .line 312291
    add-int/lit8 v6, v1, -0x4

    .line 312292
    iget v4, v3, LX/4fQ;->b:I

    if-eq v4, v10, :cond_e

    iget v4, v3, LX/4fQ;->b:I

    const v7, 0x4d4d002a    # 2.14958752E8f

    if-eq v4, v7, :cond_e

    .line 312293
    sget-object v4, LX/1li;->a:Ljava/lang/Class;

    const-string v6, "Invalid TIFF header"

    invoke-static {v4, v6}, LX/03J;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_6

    .line 312294
    :cond_e
    iget v4, v3, LX/4fQ;->b:I

    if-ne v4, v10, :cond_10

    const/4 v4, 0x1

    :goto_a
    iput-boolean v4, v3, LX/4fQ;->a:Z

    .line 312295
    iget-boolean v4, v3, LX/4fQ;->a:Z

    invoke-static {p0, v8, v4}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v4

    iput v4, v3, LX/4fQ;->c:I

    .line 312296
    add-int/lit8 v4, v6, -0x4

    .line 312297
    iget v6, v3, LX/4fQ;->c:I

    if-lt v6, v9, :cond_f

    iget v6, v3, LX/4fQ;->c:I

    add-int/lit8 v6, v6, -0x8

    if-le v6, v4, :cond_11

    .line 312298
    :cond_f
    sget-object v4, LX/1li;->a:Ljava/lang/Class;

    const-string v6, "Invalid offset"

    invoke-static {v4, v6}, LX/03J;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_10
    move v4, v5

    .line 312299
    goto :goto_a

    :cond_11
    move v5, v4

    .line 312300
    goto/16 :goto_6

    .line 312301
    :cond_12
    invoke-static {p0, v14, v4}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v9

    .line 312302
    add-int/lit8 v10, v2, -0x2

    move v11, v10

    .line 312303
    :goto_b
    add-int/lit8 v10, v9, -0x1

    if-lez v9, :cond_b

    const/16 v9, 0xc

    if-lt v11, v9, :cond_b

    .line 312304
    invoke-static {p0, v14, v4}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v12

    .line 312305
    add-int/lit8 v9, v11, -0x2

    .line 312306
    if-ne v12, v5, :cond_13

    move v8, v9

    .line 312307
    goto :goto_8

    .line 312308
    :cond_13
    const-wide/16 v12, 0xa

    invoke-virtual {p0, v12, v13}, Ljava/io/InputStream;->skip(J)J

    .line 312309
    add-int/lit8 v9, v9, -0xa

    move v11, v9

    move v9, v10

    .line 312310
    goto :goto_b

    .line 312311
    :cond_14
    invoke-static {p0, v7, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v5

    .line 312312
    const/4 v6, 0x3

    if-ne v5, v6, :cond_c

    .line 312313
    const/4 v5, 0x4

    invoke-static {p0, v5, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v5

    .line 312314
    const/4 v6, 0x1

    if-ne v5, v6, :cond_c

    .line 312315
    invoke-static {p0, v7, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    move-result v4

    .line 312316
    invoke-static {p0, v7, v3}, LX/1lh;->a(Ljava/io/InputStream;IZ)I

    goto :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0xc0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
