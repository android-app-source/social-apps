.class public final enum LX/0mt;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0m7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0mt;",
        ">;",
        "LX/0m7;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0mt;

.field public static final enum CLOSE_CLOSEABLE:LX/0mt;

.field public static final enum EAGER_SERIALIZER_FETCH:LX/0mt;

.field public static final enum FAIL_ON_EMPTY_BEANS:LX/0mt;

.field public static final enum FLUSH_AFTER_WRITE_VALUE:LX/0mt;

.field public static final enum INDENT_OUTPUT:LX/0mt;

.field public static final enum ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

.field public static final enum WRAP_EXCEPTIONS:LX/0mt;

.field public static final enum WRAP_ROOT_VALUE:LX/0mt;

.field public static final enum WRITE_BIGDECIMAL_AS_PLAIN:LX/0mt;

.field public static final enum WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS:LX/0mt;

.field public static final enum WRITE_DATES_AS_TIMESTAMPS:LX/0mt;

.field public static final enum WRITE_DATE_KEYS_AS_TIMESTAMPS:LX/0mt;

.field public static final enum WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mt;

.field public static final enum WRITE_EMPTY_JSON_ARRAYS:LX/0mt;

.field public static final enum WRITE_ENUMS_USING_INDEX:LX/0mt;

.field public static final enum WRITE_ENUMS_USING_TO_STRING:LX/0mt;

.field public static final enum WRITE_NULL_MAP_VALUES:LX/0mt;

.field public static final enum WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;


# instance fields
.field private final _defaultState:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133144
    new-instance v0, LX/0mt;

    const-string v1, "WRAP_ROOT_VALUE"

    invoke-direct {v0, v1, v3, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRAP_ROOT_VALUE:LX/0mt;

    .line 133145
    new-instance v0, LX/0mt;

    const-string v1, "INDENT_OUTPUT"

    invoke-direct {v0, v1, v4, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->INDENT_OUTPUT:LX/0mt;

    .line 133146
    new-instance v0, LX/0mt;

    const-string v1, "FAIL_ON_EMPTY_BEANS"

    invoke-direct {v0, v1, v5, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->FAIL_ON_EMPTY_BEANS:LX/0mt;

    .line 133147
    new-instance v0, LX/0mt;

    const-string v1, "WRAP_EXCEPTIONS"

    invoke-direct {v0, v1, v6, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRAP_EXCEPTIONS:LX/0mt;

    .line 133148
    new-instance v0, LX/0mt;

    const-string v1, "CLOSE_CLOSEABLE"

    invoke-direct {v0, v1, v7, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->CLOSE_CLOSEABLE:LX/0mt;

    .line 133149
    new-instance v0, LX/0mt;

    const-string v1, "FLUSH_AFTER_WRITE_VALUE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->FLUSH_AFTER_WRITE_VALUE:LX/0mt;

    .line 133150
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_DATES_AS_TIMESTAMPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_DATES_AS_TIMESTAMPS:LX/0mt;

    .line 133151
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_DATE_KEYS_AS_TIMESTAMPS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_DATE_KEYS_AS_TIMESTAMPS:LX/0mt;

    .line 133152
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS:LX/0mt;

    .line 133153
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_ENUMS_USING_TO_STRING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_ENUMS_USING_TO_STRING:LX/0mt;

    .line 133154
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_ENUMS_USING_INDEX"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_ENUMS_USING_INDEX:LX/0mt;

    .line 133155
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_NULL_MAP_VALUES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    .line 133156
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_EMPTY_JSON_ARRAYS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_EMPTY_JSON_ARRAYS:LX/0mt;

    .line 133157
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    .line 133158
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_BIGDECIMAL_AS_PLAIN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_BIGDECIMAL_AS_PLAIN:LX/0mt;

    .line 133159
    new-instance v0, LX/0mt;

    const-string v1, "WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mt;

    .line 133160
    new-instance v0, LX/0mt;

    const-string v1, "ORDER_MAP_ENTRIES_BY_KEYS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

    .line 133161
    new-instance v0, LX/0mt;

    const-string v1, "EAGER_SERIALIZER_FETCH"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v4}, LX/0mt;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mt;->EAGER_SERIALIZER_FETCH:LX/0mt;

    .line 133162
    const/16 v0, 0x12

    new-array v0, v0, [LX/0mt;

    sget-object v1, LX/0mt;->WRAP_ROOT_VALUE:LX/0mt;

    aput-object v1, v0, v3

    sget-object v1, LX/0mt;->INDENT_OUTPUT:LX/0mt;

    aput-object v1, v0, v4

    sget-object v1, LX/0mt;->FAIL_ON_EMPTY_BEANS:LX/0mt;

    aput-object v1, v0, v5

    sget-object v1, LX/0mt;->WRAP_EXCEPTIONS:LX/0mt;

    aput-object v1, v0, v6

    sget-object v1, LX/0mt;->CLOSE_CLOSEABLE:LX/0mt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0mt;->FLUSH_AFTER_WRITE_VALUE:LX/0mt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0mt;->WRITE_DATES_AS_TIMESTAMPS:LX/0mt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0mt;->WRITE_DATE_KEYS_AS_TIMESTAMPS:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0mt;->WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0mt;->WRITE_ENUMS_USING_TO_STRING:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0mt;->WRITE_ENUMS_USING_INDEX:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0mt;->WRITE_NULL_MAP_VALUES:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0mt;->WRITE_EMPTY_JSON_ARRAYS:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0mt;->WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0mt;->WRITE_BIGDECIMAL_AS_PLAIN:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0mt;->WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0mt;->ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0mt;->EAGER_SERIALIZER_FETCH:LX/0mt;

    aput-object v2, v0, v1

    sput-object v0, LX/0mt;->$VALUES:[LX/0mt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 133141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133142
    iput-boolean p3, p0, LX/0mt;->_defaultState:Z

    .line 133143
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0mt;
    .locals 1

    .prologue
    .line 133140
    const-class v0, LX/0mt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0mt;

    return-object v0
.end method

.method public static values()[LX/0mt;
    .locals 1

    .prologue
    .line 133137
    sget-object v0, LX/0mt;->$VALUES:[LX/0mt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0mt;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 133139
    iget-boolean v0, p0, LX/0mt;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 2

    .prologue
    .line 133138
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0mt;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method
