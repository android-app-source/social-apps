.class public LX/0gy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:[LX/0cQ;

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0cQ;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/0gy;


# instance fields
.field private a:LX/0gw;

.field public b:LX/0hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 114448
    const/4 v0, 0x6

    new-array v0, v0, [LX/0cQ;

    const/4 v1, 0x0

    sget-object v2, LX/0cQ;->FRIEND_REQUESTS_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRIENDING_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/0cQ;->BOOKMARKS_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/0cQ;->THREAD_LIST_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/0cQ;->BOOKMARKS_SECTION_FRAGMENT:LX/0cQ;

    aput-object v2, v0, v1

    sput-object v0, LX/0gy;->c:[LX/0cQ;

    .line 114449
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, LX/0gy;->c:[LX/0cQ;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/0gy;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0gw;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 114444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114445
    iput-object p1, p0, LX/0gy;->a:LX/0gw;

    .line 114446
    iput-object p2, p0, LX/0gy;->b:LX/0hB;

    .line 114447
    return-void
.end method

.method public static a(LX/0QB;)LX/0gy;
    .locals 5

    .prologue
    .line 114367
    sget-object v0, LX/0gy;->e:LX/0gy;

    if-nez v0, :cond_1

    .line 114368
    const-class v1, LX/0gy;

    monitor-enter v1

    .line 114369
    :try_start_0
    sget-object v0, LX/0gy;->e:LX/0gy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 114370
    if-eqz v2, :cond_0

    .line 114371
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 114372
    new-instance p0, LX/0gy;

    invoke-static {v0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v3

    check-cast v3, LX/0gw;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-direct {p0, v3, v4}, LX/0gy;-><init>(LX/0gw;LX/0hB;)V

    .line 114373
    move-object v0, p0

    .line 114374
    sput-object v0, LX/0gy;->e:LX/0gy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114375
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 114376
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 114377
    :cond_1
    sget-object v0, LX/0gy;->e:LX/0gy;

    return-object v0

    .line 114378
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 114379
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0gy;I)LX/0gz;
    .locals 4

    .prologue
    .line 114431
    new-instance v0, LX/0gz;

    invoke-direct {v0}, LX/0gz;-><init>()V

    .line 114432
    iget-object v1, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    .line 114433
    const/high16 v2, 0x42000000    # 32.0f

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 114434
    const/high16 v3, 0x44320000    # 712.0f

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 114435
    mul-int/lit8 v3, v2, 0x2

    sub-int v3, p1, v3

    .line 114436
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 114437
    iput v1, v0, LX/0gz;->a:I

    .line 114438
    iget v1, v0, LX/0gz;->a:I

    sub-int v1, v3, v1

    .line 114439
    div-int/lit8 v3, v1, 0x2

    add-int/2addr v3, v2

    .line 114440
    iput v3, v0, LX/0gz;->c:I

    .line 114441
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 114442
    iput v1, v0, LX/0gz;->d:I

    .line 114443
    return-object v0
.end method

.method public static b(LX/0gy;I)LX/0gz;
    .locals 6

    .prologue
    .line 114414
    new-instance v0, LX/0gz;

    invoke-direct {v0}, LX/0gz;-><init>()V

    .line 114415
    iget-object v1, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    .line 114416
    const/high16 v2, 0x42000000    # 32.0f

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 114417
    const/high16 v3, 0x41000000    # 8.0f

    mul-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 114418
    const/high16 v4, 0x43940000    # 296.0f

    mul-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 114419
    const/high16 v5, 0x44320000    # 712.0f

    mul-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 114420
    iput v4, v0, LX/0gz;->b:I

    .line 114421
    iput v3, v0, LX/0gz;->d:I

    .line 114422
    mul-int/lit8 v5, v2, 0x2

    sub-int v5, p1, v5

    sub-int v3, v5, v3

    sub-int/2addr v3, v4

    .line 114423
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 114424
    iput v1, v0, LX/0gz;->a:I

    .line 114425
    iget v1, v0, LX/0gz;->a:I

    sub-int v1, v3, v1

    .line 114426
    div-int/lit8 v3, v1, 0x2

    add-int/2addr v3, v2

    .line 114427
    iput v3, v0, LX/0gz;->c:I

    .line 114428
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 114429
    iput v1, v0, LX/0gz;->e:I

    .line 114430
    return-object v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 114413
    iget-object v0, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    iget-object v1, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0cQ;Landroid/content/Context;Landroid/support/v4/widget/SwipeRefreshLayout;I)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 114394
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 114395
    const v2, 0x7f0a0148

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 114396
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 114397
    invoke-static {p3, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 114398
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 114399
    invoke-virtual {p2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v3, p4, v2, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 114400
    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 114401
    invoke-virtual {p0, p1}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v6

    .line 114402
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 114403
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v3, v1

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 114404
    iget v2, v6, LX/0gz;->c:I

    move v2, v2

    .line 114405
    iget v3, v6, LX/0gz;->d:I

    move v4, v3

    .line 114406
    move v3, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 114407
    invoke-static {p3, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 114408
    iget v0, v6, LX/0gz;->c:I

    move v0, v0

    .line 114409
    invoke-virtual {p3}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v1

    .line 114410
    iget v2, v6, LX/0gz;->d:I

    move v2, v2

    .line 114411
    invoke-virtual {p3}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setPadding(IIII)V

    .line 114412
    return-void
.end method

.method public final a(LX/0cQ;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 114392
    iget-object v1, p0, LX/0gy;->a:LX/0gw;

    invoke-virtual {v1}, LX/0gw;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 114393
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, LX/0gy;->d:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/0gz;
    .locals 2

    .prologue
    .line 114388
    iget-object v0, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 114389
    invoke-direct {p0}, LX/0gy;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114390
    invoke-static {p0, v0}, LX/0gy;->a(LX/0gy;I)LX/0gz;

    move-result-object v0

    .line 114391
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v0}, LX/0gy;->b(LX/0gy;I)LX/0gz;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/0cQ;)LX/0gz;
    .locals 2
    .param p1    # LX/0cQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114381
    iget-object v0, p0, LX/0gy;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v1

    .line 114382
    invoke-virtual {p0, p1}, LX/0gy;->a(LX/0cQ;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/0gy;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 114383
    :goto_0
    if-eqz v0, :cond_1

    .line 114384
    invoke-static {p0, v1}, LX/0gy;->b(LX/0gy;I)LX/0gz;

    move-result-object v0

    .line 114385
    :goto_1
    return-object v0

    .line 114386
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 114387
    :cond_1
    invoke-static {p0, v1}, LX/0gy;->a(LX/0gy;I)LX/0gz;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 114380
    sget-object v0, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {p0, v0}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v0

    iget v0, v0, LX/0gz;->a:I

    return v0
.end method
