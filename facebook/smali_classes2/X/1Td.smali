.class public LX/1Td;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 253210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253211
    iput-object p1, p0, LX/1Td;->a:LX/0Ot;

    .line 253212
    iput-object p2, p0, LX/1Td;->b:LX/0Ot;

    .line 253213
    iput-object p3, p0, LX/1Td;->c:LX/0Ot;

    .line 253214
    return-void
.end method

.method public static a(LX/0QB;)LX/1Td;
    .locals 6

    .prologue
    .line 253215
    const-class v1, LX/1Td;

    monitor-enter v1

    .line 253216
    :try_start_0
    sget-object v0, LX/1Td;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 253217
    sput-object v2, LX/1Td;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 253218
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253219
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 253220
    new-instance v3, LX/1Td;

    const/16 v4, 0x20f8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x20f9

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x2123

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/1Td;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 253221
    move-object v0, v3

    .line 253222
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 253223
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Td;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253224
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 253225
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 253226
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 0

    .prologue
    .line 253227
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 253228
    const-class v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    iget-object v1, p0, LX/1Td;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253229
    const-class v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    iget-object v1, p0, LX/1Td;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253230
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    iget-object v1, p0, LX/1Td;->c:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253231
    return-void
.end method
