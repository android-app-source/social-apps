.class public LX/0Zl;
.super LX/0Zm;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0Zl;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;LX/0a8;LX/0XZ;)V
    .locals 6
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "LX/0XZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83823
    const/16 v4, 0x482

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/0Zm;-><init>(LX/0Or;LX/0Uh;LX/0a8;ILX/0XZ;)V

    .line 83824
    return-void
.end method

.method public static a(LX/0QB;)LX/0Zl;
    .locals 7

    .prologue
    .line 83825
    sget-object v0, LX/0Zl;->a:LX/0Zl;

    if-nez v0, :cond_1

    .line 83826
    const-class v1, LX/0Zl;

    monitor-enter v1

    .line 83827
    :try_start_0
    sget-object v0, LX/0Zl;->a:LX/0Zl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83828
    if-eqz v2, :cond_0

    .line 83829
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83830
    new-instance v6, LX/0Zl;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v4

    check-cast v4, LX/0a8;

    invoke-static {v0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object v5

    check-cast v5, LX/0XZ;

    invoke-direct {v6, p0, v3, v4, v5}, LX/0Zl;-><init>(LX/0Or;LX/0Uh;LX/0a8;LX/0XZ;)V

    .line 83831
    move-object v0, v6

    .line 83832
    sput-object v0, LX/0Zl;->a:LX/0Zl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83833
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83834
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83835
    :cond_1
    sget-object v0, LX/0Zl;->a:LX/0Zl;

    return-object v0

    .line 83836
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
