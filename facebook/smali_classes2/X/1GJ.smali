.class public LX/1GJ;
.super LX/1GK;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1GJ;


# instance fields
.field private final a:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1FO;

.field private final e:LX/0W3;


# direct methods
.method public constructor <init>(LX/1GA;LX/1FG;Lcom/facebook/ui/images/webp/AnimatedImageDecoder;LX/1FO;LX/0Or;LX/0Or;LX/0W3;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/images/abtest/newpipeline/IsNewWepImagesEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/images/abtest/newpipeline/IsNewGifImagesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedImageFactory;",
            "LX/1FG;",
            "Lcom/facebook/ui/images/webp/AnimatedImageDecoder;",
            "LX/1FO;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225180
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, p1, p2, v0}, LX/1GK;-><init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;)V

    .line 225181
    iput-object p3, p0, LX/1GJ;->a:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    .line 225182
    iput-object p5, p0, LX/1GJ;->b:LX/0Or;

    .line 225183
    iput-object p6, p0, LX/1GJ;->c:LX/0Or;

    .line 225184
    iput-object p4, p0, LX/1GJ;->d:LX/1FO;

    .line 225185
    iput-object p7, p0, LX/1GJ;->e:LX/0W3;

    .line 225186
    return-void
.end method

.method private static a(LX/1bZ;)I
    .locals 1

    .prologue
    .line 225187
    instance-of v0, p0, LX/4eB;

    if-eqz v0, :cond_0

    .line 225188
    check-cast p0, LX/4eB;

    iget v0, p0, LX/4eB;->g:I

    .line 225189
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1GJ;
    .locals 11

    .prologue
    .line 225190
    sget-object v0, LX/1GJ;->f:LX/1GJ;

    if-nez v0, :cond_1

    .line 225191
    const-class v1, LX/1GJ;

    monitor-enter v1

    .line 225192
    :try_start_0
    sget-object v0, LX/1GJ;->f:LX/1GJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225193
    if-eqz v2, :cond_0

    .line 225194
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225195
    new-instance v3, LX/1GJ;

    invoke-static {v0}, LX/1Fq;->a(LX/0QB;)LX/1GA;

    move-result-object v4

    check-cast v4, LX/1GA;

    invoke-static {v0}, LX/1FC;->a(LX/0QB;)LX/1FG;

    move-result-object v5

    check-cast v5, LX/1FG;

    invoke-static {v0}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->a(LX/0QB;)Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    move-result-object v6

    check-cast v6, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    invoke-static {v0}, LX/1GM;->a(LX/0QB;)LX/1FO;

    move-result-object v7

    check-cast v7, LX/1FO;

    const/16 v8, 0x158d

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x158c

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    invoke-direct/range {v3 .. v10}, LX/1GJ;-><init>(LX/1GA;LX/1FG;Lcom/facebook/ui/images/webp/AnimatedImageDecoder;LX/1FO;LX/0Or;LX/0Or;LX/0W3;)V

    .line 225196
    move-object v0, v3

    .line 225197
    sput-object v0, LX/1GJ;->f:LX/1GJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225198
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225199
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225200
    :cond_1
    sget-object v0, LX/1GJ;->f:LX/1GJ;

    return-object v0

    .line 225201
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225202
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1bZ;I)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 225203
    if-eqz p1, :cond_0

    move v0, v1

    .line 225204
    :goto_0
    if-eqz v0, :cond_2

    .line 225205
    if-ne p1, v1, :cond_1

    .line 225206
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 225207
    :goto_1
    return-object v0

    .line 225208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 225209
    :cond_1
    if-ne p1, v2, :cond_2

    .line 225210
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 225211
    :cond_2
    instance-of v0, p0, LX/4eB;

    if-eqz v0, :cond_3

    .line 225212
    check-cast p0, LX/4eB;

    iget-object v0, p0, LX/4eB;->h:Ljava/lang/Integer;

    goto :goto_1

    .line 225213
    :cond_3
    sget-object v0, LX/4eC;->a:Ljava/lang/Integer;

    goto :goto_1
.end method

.method private d(LX/1FL;LX/1bZ;)LX/1ln;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 225214
    :try_start_0
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result-object v2

    .line 225215
    if-nez v2, :cond_0

    .line 225216
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 225217
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 225218
    iget-object v1, p0, LX/1GJ;->d:LX/1FO;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v3

    invoke-virtual {v1, v3}, LX/1FO;->a(I)LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v3

    .line 225219
    :try_start_2
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 225220
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v6

    invoke-virtual {v0, v4, v1, v5, v6}, LX/1FK;->a(I[BII)V

    .line 225221
    iget-object v0, p0, LX/1GJ;->a:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    invoke-static {p2}, LX/1GJ;->a(LX/1bZ;)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->a([BIII)LX/4nB;

    move-result-object v0

    .line 225222
    iget-object v1, v0, LX/4nB;->e:LX/0Px;

    move-object v1, v1

    .line 225223
    invoke-static {}, LX/1FH;->a()LX/1FI;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1FI;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 225224
    :try_start_3
    iget-boolean v1, p2, LX/1bZ;->e:Z

    if-eqz v1, :cond_1

    .line 225225
    new-instance v1, LX/1ll;

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    sget-object v5, LX/1lk;->a:LX/1lk;

    const/4 v6, 0x0

    invoke-direct {v1, v0, v5, v6}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 225226
    :try_start_4
    invoke-static {v4}, LX/1FJ;->a(Ljava/lang/Iterable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 225227
    :try_start_5
    invoke-virtual {v3}, LX/1FJ;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 225228
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    .line 225229
    :cond_1
    :try_start_6
    iget-object v1, v0, LX/4nB;->f:LX/0Px;

    move-object v1, v1

    .line 225230
    new-instance v0, LX/1q7;

    invoke-direct {v0, v4, v1}, LX/1q7;-><init>(Ljava/util/List;Ljava/util/List;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 225231
    :try_start_7
    invoke-static {v4}, LX/1FJ;->a(Ljava/lang/Iterable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 225232
    :try_start_8
    invoke-virtual {v3}, LX/1FJ;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 225233
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 225234
    :catchall_0
    move-exception v0

    :try_start_9
    invoke-static {v4}, LX/1FJ;->a(Ljava/lang/Iterable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 225235
    :catchall_1
    move-exception v0

    :try_start_a
    invoke-virtual {v3}, LX/1FJ;->close()V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 225236
    :catchall_2
    move-exception v0

    move-object v1, v2

    :goto_1
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    :catchall_3
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1FL;LX/1bZ;)LX/1ln;
    .locals 4

    .prologue
    .line 225237
    iget-object v0, p0, LX/1GJ;->e:LX/0W3;

    sget-wide v2, LX/0X5;->eX:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    invoke-static {p2, v0}, LX/1GJ;->a(LX/1bZ;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 225238
    iget-object v0, p0, LX/1GJ;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225239
    invoke-super {p0, p1, p2}, LX/1GK;->a(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    .line 225240
    :goto_0
    return-object v0

    .line 225241
    :pswitch_0
    invoke-super {p0, p1, p2}, LX/1GK;->a(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    goto :goto_0

    .line 225242
    :pswitch_1
    new-instance v0, LX/1q8;

    invoke-direct {v0, p1}, LX/1q8;-><init>(LX/1FL;)V

    goto :goto_0

    .line 225243
    :cond_0
    new-instance v0, LX/1q8;

    invoke-direct {v0, p1}, LX/1q8;-><init>(LX/1FL;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1FL;LX/1bZ;)LX/1ln;
    .locals 4

    .prologue
    .line 225244
    iget-object v0, p0, LX/1GJ;->e:LX/0W3;

    sget-wide v2, LX/0X5;->eY:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    invoke-static {p2, v0}, LX/1GJ;->a(LX/1bZ;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 225245
    iget-object v0, p0, LX/1GJ;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225246
    invoke-super {p0, p1, p2}, LX/1GK;->c(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    .line 225247
    :goto_0
    return-object v0

    .line 225248
    :pswitch_0
    invoke-super {p0, p1, p2}, LX/1GK;->c(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    goto :goto_0

    .line 225249
    :pswitch_1
    invoke-direct {p0, p1, p2}, LX/1GJ;->d(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    goto :goto_0

    .line 225250
    :cond_0
    invoke-direct {p0, p1, p2}, LX/1GJ;->d(LX/1FL;LX/1bZ;)LX/1ln;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
