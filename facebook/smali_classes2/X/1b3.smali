.class public LX/1b3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279113
    return-void
.end method

.method public static a(LX/0QB;)LX/1b3;
    .locals 1

    .prologue
    .line 279111
    invoke-static {p0}, LX/1b3;->b(LX/0QB;)LX/1b3;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0c1;)Z
    .locals 4

    .prologue
    .line 279110
    iget-object v0, p0, LX/1b3;->c:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/AVs;->a:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, p1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/1b3;
    .locals 4

    .prologue
    .line 279106
    new-instance v3, LX/1b3;

    invoke-direct {v3}, LX/1b3;-><init>()V

    .line 279107
    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    check-cast v0, LX/1b4;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    .line 279108
    iput-object v0, v3, LX/1b3;->a:LX/1b4;

    iput-object v1, v3, LX/1b3;->b:LX/0iA;

    iput-object v2, v3, LX/1b3;->c:LX/0ad;

    .line 279109
    return-object v3
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 279103
    iget-object v0, p0, LX/1b3;->a:LX/1b4;

    .line 279104
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x35a

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 279105
    return v0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 279098
    iget-object v0, p0, LX/1b3;->b:LX/0iA;

    sget-object v1, LX/3EJ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3EJ;

    invoke-virtual {v0, v1, v2}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 279102
    invoke-direct {p0}, LX/1b3;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/1b3;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0c1;->Off:LX/0c1;

    invoke-direct {p0, v0}, LX/1b3;->a(LX/0c1;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 279099
    invoke-direct {p0}, LX/1b3;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/1b3;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 279100
    :cond_0
    :goto_0
    return-void

    .line 279101
    :cond_1
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-direct {p0, v0}, LX/1b3;->a(LX/0c1;)Z

    goto :goto_0
.end method
