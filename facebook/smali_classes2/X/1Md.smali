.class public abstract LX/1Md;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/1Me;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 235818
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 235819
    const-string v0, "com.facebook.push.mqtt.ipc.MqttChannelStateListener"

    invoke-virtual {p0, p0, v0}, LX/1Md;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 235820
    return-void
.end method

.method public static a(Landroid/os/IBinder;)LX/1Me;
    .locals 2

    .prologue
    .line 235821
    if-nez p0, :cond_0

    .line 235822
    const/4 v0, 0x0

    .line 235823
    :goto_0
    return-object v0

    .line 235824
    :cond_0
    const-string v0, "com.facebook.push.mqtt.ipc.MqttChannelStateListener"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 235825
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/1Me;

    if-eqz v1, :cond_1

    .line 235826
    check-cast v0, LX/1Me;

    goto :goto_0

    .line 235827
    :cond_1
    new-instance v0, LX/767;

    invoke-direct {v0, p0}, LX/767;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 235828
    return-object p0
.end method

.method public final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 235829
    sparse-switch p1, :sswitch_data_0

    .line 235830
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 235831
    :sswitch_0
    const-string v0, "com.facebook.push.mqtt.ipc.MqttChannelStateListener"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 235832
    goto :goto_0

    .line 235833
    :sswitch_1
    const-string v0, "com.facebook.push.mqtt.ipc.MqttChannelStateListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 235834
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 235835
    sget-object v0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    .line 235836
    :goto_1
    invoke-virtual {p0, v0}, LX/1Md;->a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V

    move v0, v1

    .line 235837
    goto :goto_0

    .line 235838
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
