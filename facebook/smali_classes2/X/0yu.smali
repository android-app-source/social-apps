.class public LX/0yu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final d:Landroid/net/Uri;

.field private static final e:[Ljava/lang/String;

.field public static final f:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field public c:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 166345
    const-class v0, LX/0yu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0yu;->a:Ljava/lang/String;

    .line 166346
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/0yu;->d:Landroid/net/Uri;

    .line 166347
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "date_added"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "media_type"

    aput-object v1, v0, v5

    sput-object v0, LX/0yu;->e:[Ljava/lang/String;

    .line 166348
    const-string v0, "%s = %d AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "_data"

    aput-object v2, v1, v5

    const-string v2, "_data"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0yu;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166321
    iput-object p1, p0, LX/0yu;->c:LX/0SG;

    .line 166322
    iput-object p2, p0, LX/0yu;->b:Landroid/content/Context;

    .line 166323
    return-void
.end method

.method public static a(LX/0yu;)J
    .locals 8

    .prologue
    const-wide/16 v6, -0x3e8

    .line 166326
    iget-object v0, p0, LX/0yu;->b:Landroid/content/Context;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 166327
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    move-wide v0, v6

    .line 166328
    :goto_0
    return-wide v0

    .line 166329
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0yu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/0yu;->d:Landroid/net/Uri;

    sget-object v2, LX/0yu;->e:[Ljava/lang/String;

    .line 166330
    sget-object v3, LX/0yu;->f:Ljava/lang/String;

    move-object v3, v3

    .line 166331
    const/4 v4, 0x0

    const-string v5, "date_added DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 166332
    if-nez v2, :cond_1

    move-wide v0, v6

    .line 166333
    goto :goto_0

    .line 166334
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, -0x3e8

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    .line 166335
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 166336
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 166337
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 166338
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    goto :goto_0

    .line 166339
    :catch_0
    move-exception v0

    .line 166340
    sget-object v1, LX/0yu;->a:Ljava/lang/String;

    const-string v2, "Security Exception:"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v6

    .line 166341
    goto :goto_0

    .line 166342
    :catch_1
    move-exception v0

    .line 166343
    sget-object v1, LX/0yu;->a:Ljava/lang/String;

    const-string v2, "Runtime exception:"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v6

    .line 166344
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0yu;
    .locals 3

    .prologue
    .line 166324
    new-instance v2, LX/0yu;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/0yu;-><init>(LX/0SG;Landroid/content/Context;)V

    .line 166325
    return-object v2
.end method
