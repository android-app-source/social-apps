.class public LX/1YO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/03R;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1YR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1YR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/03R;)V
    .locals 1
    .param p2    # LX/03R;
        .annotation runtime Lcom/facebook/feed/annotations/IsMenuButtonTooltipTriggerManagerEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1YR;",
            ">;",
            "LX/03R;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273483
    iput-object p1, p0, LX/1YO;->c:Ljava/util/Set;

    .line 273484
    iput-object p2, p0, LX/1YO;->a:LX/03R;

    .line 273485
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1YO;->b:Ljava/util/Set;

    .line 273486
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1YO;->d:Ljava/util/Set;

    .line 273487
    return-void
.end method

.method public static a(LX/0QB;)LX/1YO;
    .locals 6

    .prologue
    .line 273488
    const-class v1, LX/1YO;

    monitor-enter v1

    .line 273489
    :try_start_0
    sget-object v0, LX/1YO;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273490
    sput-object v2, LX/1YO;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273491
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273492
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273493
    new-instance v4, LX/1YO;

    .line 273494
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/1YP;

    invoke-direct {p0, v0}, LX/1YP;-><init>(LX/0QB;)V

    invoke-direct {v3, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v3

    .line 273495
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 p0, 0x299

    invoke-virtual {v3, p0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    move-object v3, v3

    .line 273496
    check-cast v3, LX/03R;

    invoke-direct {v4, v5, v3}, LX/1YO;-><init>(Ljava/util/Set;LX/03R;)V

    .line 273497
    move-object v0, v4

    .line 273498
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1YO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1YO;)Z
    .locals 2

    .prologue
    .line 273502
    iget-object v0, p0, LX/1YO;->a:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
