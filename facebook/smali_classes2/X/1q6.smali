.class public LX/1q6;
.super LX/0qg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/0ad;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 330048
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1q6;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 330044
    invoke-direct {p0}, LX/0qg;-><init>()V

    .line 330045
    const/4 v0, 0x0

    iput-object v0, p0, LX/1q6;->b:Ljava/lang/String;

    .line 330046
    iput-object p1, p0, LX/1q6;->a:LX/0ad;

    .line 330047
    return-void
.end method

.method public static a(LX/0QB;)LX/1q6;
    .locals 7

    .prologue
    .line 330015
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 330016
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 330017
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 330018
    if-nez v1, :cond_0

    .line 330019
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330020
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 330021
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 330022
    sget-object v1, LX/1q6;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 330023
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 330024
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 330025
    :cond_1
    if-nez v1, :cond_4

    .line 330026
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 330027
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 330028
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 330029
    new-instance p0, LX/1q6;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {p0, v1}, LX/1q6;-><init>(LX/0ad;)V

    .line 330030
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 330031
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 330032
    if-nez v1, :cond_2

    .line 330033
    sget-object v0, LX/1q6;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q6;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 330034
    :goto_1
    if-eqz v0, :cond_3

    .line 330035
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 330036
    :goto_3
    check-cast v0, LX/1q6;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 330037
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 330038
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 330039
    :catchall_1
    move-exception v0

    .line 330040
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 330041
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 330042
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 330043
    :cond_2
    :try_start_8
    sget-object v0, LX/1q6;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q6;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0rH;)LX/0qi;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 330010
    iget-object v1, p0, LX/1q6;->a:LX/0ad;

    sget-short v2, LX/0rI;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330011
    iget-object v0, p0, LX/1q6;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    .line 330012
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/1q6;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330014
    const-string v0, "prompt"

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0rH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330013
    const/4 v0, 0x1

    new-array v0, v0, [LX/0rH;

    const/4 v1, 0x0

    sget-object v2, LX/0rH;->NEWS_FEED:LX/0rH;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    return-object v0
.end method
