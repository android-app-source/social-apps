.class public LX/0SH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SI;


# static fields
.field public static final a:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60956
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v0

    const-string v1, "0"

    .line 60957
    iput-object v1, v0, LX/0SK;->a:Ljava/lang/String;

    .line 60958
    move-object v0, v0

    .line 60959
    const-string v1, ""

    .line 60960
    iput-object v1, v0, LX/0SK;->b:Ljava/lang/String;

    .line 60961
    move-object v0, v0

    .line 60962
    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    sput-object v0, LX/0SH;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 60963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60964
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 60949
    sget-object v0, LX/0SH;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 60955
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    .locals 1

    .prologue
    .line 60954
    sget-object v0, LX/1mW;->a:LX/1mW;

    return-object v0
.end method

.method public final b()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 60965
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 60953
    sget-object v0, LX/0SH;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final d()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 60952
    sget-object v0, LX/0SH;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final e()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 60951
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 60950
    return-void
.end method
