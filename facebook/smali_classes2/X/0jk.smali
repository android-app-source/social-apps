.class public LX/0jk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0jk;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0jl;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0jl;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 124630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124631
    iput-object p2, p0, LX/0jk;->a:LX/0Ot;

    .line 124632
    iput-object p1, p0, LX/0jk;->b:LX/0SG;

    .line 124633
    return-void
.end method

.method public static a(LX/0QB;)LX/0jk;
    .locals 6

    .prologue
    .line 124634
    sget-object v0, LX/0jk;->c:LX/0jk;

    if-nez v0, :cond_1

    .line 124635
    const-class v1, LX/0jk;

    monitor-enter v1

    .line 124636
    :try_start_0
    sget-object v0, LX/0jk;->c:LX/0jk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 124637
    if-eqz v2, :cond_0

    .line 124638
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 124639
    new-instance v4, LX/0jk;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    .line 124640
    new-instance v5, LX/0zl;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v5, p0}, LX/0zl;-><init>(LX/0QB;)V

    move-object v5, v5

    .line 124641
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v5, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v5

    move-object v5, v5

    .line 124642
    invoke-direct {v4, v3, v5}, LX/0jk;-><init>(LX/0SG;LX/0Ot;)V

    .line 124643
    move-object v0, v4

    .line 124644
    sput-object v0, LX/0jk;->c:LX/0jk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124645
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 124646
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 124647
    :cond_1
    sget-object v0, LX/0jk;->c:LX/0jk;

    return-object v0

    .line 124648
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 124649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 124650
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 124651
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 124652
    iget-object v0, p0, LX/0jk;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 124653
    iget-object v0, p0, LX/0jk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jl;

    .line 124654
    invoke-interface {v0, v2, v3}, LX/0jl;->b(J)V

    goto :goto_0

    .line 124655
    :cond_0
    return-void
.end method
