.class public LX/1KD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/19F;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/19D;

.field private final b:Landroid/view/Choreographer;

.field private final c:LX/03V;

.field public final d:Ljava/lang/Runnable;

.field private final e:Ljava/lang/reflect/Method;

.field private final f:Ljava/lang/reflect/Method;

.field private final g:Ljava/lang/reflect/Method;

.field public h:J

.field public i:J

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(LX/19D;Landroid/view/Choreographer;LX/03V;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 231318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231319
    iput-wide v0, p0, LX/1KD;->h:J

    .line 231320
    iput-wide v0, p0, LX/1KD;->i:J

    .line 231321
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1KD;->j:Z

    .line 231322
    iput-object p1, p0, LX/1KD;->a:LX/19D;

    .line 231323
    iput-object p2, p0, LX/1KD;->b:Landroid/view/Choreographer;

    .line 231324
    iput-object p3, p0, LX/1KD;->c:LX/03V;

    .line 231325
    :try_start_0
    const-class v0, Landroid/view/Choreographer;

    const-string v1, "postCallback"

    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Class;

    const/4 p2, 0x0

    sget-object p3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p3, p1, p2

    const/4 p2, 0x1

    const-class p3, Ljava/lang/Runnable;

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-class p3, Ljava/lang/Object;

    aput-object p3, p1, p2

    invoke-virtual {v0, v1, p1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 231326
    :goto_0
    move-object v0, v0

    .line 231327
    iput-object v0, p0, LX/1KD;->e:Ljava/lang/reflect/Method;

    .line 231328
    :try_start_1
    const-class v0, Landroid/view/Choreographer;

    const-string v1, "removeCallbacks"

    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Class;

    const/4 p2, 0x0

    sget-object p3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p3, p1, p2

    const/4 p2, 0x1

    const-class p3, Ljava/lang/Runnable;

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-class p3, Ljava/lang/Object;

    aput-object p3, p1, p2

    invoke-virtual {v0, v1, p1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 231329
    :goto_1
    move-object v0, v0

    .line 231330
    iput-object v0, p0, LX/1KD;->f:Ljava/lang/reflect/Method;

    .line 231331
    :try_start_2
    const-class v0, Landroid/view/Choreographer;

    const-string v1, "getFrameTime"

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Class;

    invoke-virtual {v0, v1, p1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 231332
    :goto_2
    move-object v0, v0

    .line 231333
    iput-object v0, p0, LX/1KD;->g:Ljava/lang/reflect/Method;

    .line 231334
    new-instance v0, Lcom/facebook/debug/fps/TouchCallbackTimingSource$1;

    invoke-direct {v0, p0}, Lcom/facebook/debug/fps/TouchCallbackTimingSource$1;-><init>(LX/1KD;)V

    move-object v0, v0

    .line 231335
    iput-object v0, p0, LX/1KD;->d:Ljava/lang/Runnable;

    .line 231336
    return-void

    .line 231337
    :catch_0
    move-exception v0

    .line 231338
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    .line 231339
    const/4 v0, 0x0

    goto :goto_0

    .line 231340
    :catch_1
    move-exception v0

    .line 231341
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    .line 231342
    const/4 v0, 0x0

    goto :goto_1

    .line 231343
    :catch_2
    move-exception v0

    .line 231344
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    .line 231345
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(LX/1KD;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 231315
    iget-object v0, p0, LX/1KD;->c:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Choreographer reflection failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 231316
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1KD;->k:Z

    .line 231317
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 231311
    if-eqz p1, :cond_0

    iget-boolean v0, p0, LX/1KD;->j:Z

    if-nez v0, :cond_0

    .line 231312
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1KD;->h:J

    .line 231313
    :cond_0
    iput-boolean p1, p0, LX/1KD;->j:Z

    .line 231314
    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 231309
    iget-object v0, p0, LX/1KD;->e:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/1KD;->b:Landroid/view/Choreographer;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 231310
    return-void
.end method

.method private c(Ljava/lang/Runnable;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 231346
    :try_start_0
    iget-object v0, p0, LX/1KD;->f:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/1KD;->b:Landroid/view/Choreographer;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 231347
    :goto_0
    return-void

    .line 231348
    :catch_0
    move-exception v0

    .line 231349
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 231306
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1KD;->a(Z)V

    .line 231307
    iget-object v0, p0, LX/1KD;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/1KD;->a(Ljava/lang/Runnable;)V

    .line 231308
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 231301
    iget-boolean v0, p0, LX/1KD;->k:Z

    if-eqz v0, :cond_0

    .line 231302
    :goto_0
    return-void

    .line 231303
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, LX/1KD;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 231304
    :catch_0
    move-exception v0

    .line 231305
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 231298
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1KD;->a(Z)V

    .line 231299
    iget-object v0, p0, LX/1KD;->d:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, LX/1KD;->c(Ljava/lang/Runnable;)V

    .line 231300
    return-void
.end method

.method public final c()J
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 231293
    :try_start_0
    iget-object v0, p0, LX/1KD;->g:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/1KD;->b:Landroid/view/Choreographer;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 231294
    :goto_0
    return-wide v0

    .line 231295
    :catch_0
    move-exception v0

    .line 231296
    invoke-static {p0, v0}, LX/1KD;->a(LX/1KD;Ljava/lang/Exception;)V

    .line 231297
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
