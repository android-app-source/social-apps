.class public final LX/0lt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0lt;


# instance fields
.field public b:LX/0lt;

.field public final c:Z

.field public final d:Z

.field public e:[Ljava/lang/String;

.field public f:[LX/0lu;

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field public final l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131120
    new-instance v0, LX/0lt;

    invoke-direct {v0}, LX/0lt;-><init>()V

    sput-object v0, LX/0lt;->a:LX/0lt;

    .line 131121
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 131112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131113
    iput-boolean v0, p0, LX/0lt;->d:Z

    .line 131114
    iput-boolean v0, p0, LX/0lt;->c:Z

    .line 131115
    iput-boolean v0, p0, LX/0lt;->k:Z

    .line 131116
    iput v1, p0, LX/0lt;->l:I

    .line 131117
    iput v1, p0, LX/0lt;->j:I

    .line 131118
    const/16 v0, 0x40

    invoke-direct {p0, v0}, LX/0lt;->b(I)V

    .line 131119
    return-void
.end method

.method private constructor <init>(LX/0lt;ZZ[Ljava/lang/String;[LX/0lu;III)V
    .locals 2

    .prologue
    .line 131098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131099
    iput-object p1, p0, LX/0lt;->b:LX/0lt;

    .line 131100
    iput-boolean p2, p0, LX/0lt;->d:Z

    .line 131101
    iput-boolean p3, p0, LX/0lt;->c:Z

    .line 131102
    iput-object p4, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131103
    iput-object p5, p0, LX/0lt;->f:[LX/0lu;

    .line 131104
    iput p6, p0, LX/0lt;->g:I

    .line 131105
    iput p7, p0, LX/0lt;->l:I

    .line 131106
    array-length v0, p4

    .line 131107
    invoke-static {v0}, LX/0lt;->c(I)I

    move-result v1

    iput v1, p0, LX/0lt;->h:I

    .line 131108
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0lt;->i:I

    .line 131109
    iput p8, p0, LX/0lt;->j:I

    .line 131110
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lt;->k:Z

    .line 131111
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 131092
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 131093
    iget v1, p0, LX/0lt;->l:I

    .line 131094
    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    move v1, v4

    :goto_0
    if-ge v1, v3, :cond_0

    .line 131095
    mul-int/lit8 v0, v0, 0x21

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    add-int/2addr v2, v0

    .line 131096
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 131097
    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private a([CI)I
    .locals 4

    .prologue
    .line 131087
    iget v1, p0, LX/0lt;->l:I

    .line 131088
    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ge v1, p2, :cond_0

    .line 131089
    mul-int/lit8 v0, v0, 0x21

    aget-char v2, p1, v1

    add-int/2addr v2, v0

    .line 131090
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 131091
    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static a()LX/0lt;
    .locals 4

    .prologue
    .line 131084
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 131085
    long-to-int v2, v0

    const/16 v3, 0x20

    ushr-long/2addr v0, v3

    long-to-int v0, v0

    add-int/2addr v0, v2

    or-int/lit8 v0, v0, 0x1

    .line 131086
    invoke-static {v0}, LX/0lt;->a(I)LX/0lt;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)LX/0lt;
    .locals 1

    .prologue
    .line 131083
    sget-object v0, LX/0lt;->a:LX/0lt;

    invoke-direct {v0, p0}, LX/0lt;->d(I)LX/0lt;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0lt;)V
    .locals 2

    .prologue
    .line 131063
    iget v0, p1, LX/0lt;->g:I

    move v0, v0

    .line 131064
    const/16 v1, 0x2ee0

    if-gt v0, v1, :cond_0

    iget v0, p1, LX/0lt;->j:I

    const/16 v1, 0x3f

    if-le v0, v1, :cond_2

    .line 131065
    :cond_0
    monitor-enter p0

    .line 131066
    const/16 v0, 0x40

    :try_start_0
    invoke-direct {p0, v0}, LX/0lt;->b(I)V

    .line 131067
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lt;->k:Z

    .line 131068
    monitor-exit p0

    .line 131069
    :cond_1
    :goto_0
    return-void

    .line 131070
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 131071
    :cond_2
    iget v0, p1, LX/0lt;->g:I

    move v0, v0

    .line 131072
    iget v1, p0, LX/0lt;->g:I

    move v1, v1

    .line 131073
    if-le v0, v1, :cond_1

    .line 131074
    monitor-enter p0

    .line 131075
    :try_start_1
    iget-object v0, p1, LX/0lt;->e:[Ljava/lang/String;

    iput-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131076
    iget-object v0, p1, LX/0lt;->f:[LX/0lu;

    iput-object v0, p0, LX/0lt;->f:[LX/0lu;

    .line 131077
    iget v0, p1, LX/0lt;->g:I

    iput v0, p0, LX/0lt;->g:I

    .line 131078
    iget v0, p1, LX/0lt;->h:I

    iput v0, p0, LX/0lt;->h:I

    .line 131079
    iget v0, p1, LX/0lt;->i:I

    iput v0, p0, LX/0lt;->i:I

    .line 131080
    iget v0, p1, LX/0lt;->j:I

    iput v0, p0, LX/0lt;->j:I

    .line 131081
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lt;->k:Z

    .line 131082
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131122
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131123
    shr-int/lit8 v0, p1, 0x1

    new-array v0, v0, [LX/0lu;

    iput-object v0, p0, LX/0lt;->f:[LX/0lu;

    .line 131124
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, LX/0lt;->i:I

    .line 131125
    iput v1, p0, LX/0lt;->g:I

    .line 131126
    iput v1, p0, LX/0lt;->j:I

    .line 131127
    invoke-static {p1}, LX/0lt;->c(I)I

    move-result v0

    iput v0, p0, LX/0lt;->h:I

    .line 131128
    return-void
.end method

.method private static c(I)I
    .locals 1

    .prologue
    .line 131062
    shr-int/lit8 v0, p0, 0x2

    sub-int v0, p0, v0

    return v0
.end method

.method private d(I)LX/0lt;
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 131061
    new-instance v0, LX/0lt;

    const/4 v1, 0x0

    iget-object v4, p0, LX/0lt;->e:[Ljava/lang/String;

    iget-object v5, p0, LX/0lt;->f:[LX/0lu;

    iget v6, p0, LX/0lt;->g:I

    iget v8, p0, LX/0lt;->j:I

    move v3, v2

    move v7, p1

    invoke-direct/range {v0 .. v8}, LX/0lt;-><init>(LX/0lt;ZZ[Ljava/lang/String;[LX/0lu;III)V

    return-object v0
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 130949
    ushr-int/lit8 v0, p1, 0xf

    add-int/2addr v0, p1

    .line 130950
    iget v1, p0, LX/0lt;->i:I

    and-int/2addr v0, v1

    return v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131052
    iget-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131053
    array-length v1, v0

    .line 131054
    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131055
    iget-object v2, p0, LX/0lt;->e:[Ljava/lang/String;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131056
    iget-object v0, p0, LX/0lt;->f:[LX/0lu;

    .line 131057
    array-length v1, v0

    .line 131058
    new-array v2, v1, [LX/0lu;

    iput-object v2, p0, LX/0lt;->f:[LX/0lu;

    .line 131059
    iget-object v2, p0, LX/0lt;->f:[LX/0lu;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131060
    return-void
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 131051
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Longest collision chain in symbol table (of size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/0lt;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") now exceeds maximum, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- suspect a DoS attack based on hash collisions"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private g()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 131004
    iget-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    array-length v4, v0

    .line 131005
    add-int v0, v4, v4

    .line 131006
    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    .line 131007
    iput v2, p0, LX/0lt;->g:I

    .line 131008
    iget-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 131009
    iget-object v0, p0, LX/0lt;->f:[LX/0lu;

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 131010
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0lt;->k:Z

    .line 131011
    :cond_0
    return-void

    .line 131012
    :cond_1
    iget-object v5, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131013
    iget-object v6, p0, LX/0lt;->f:[LX/0lu;

    .line 131014
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 131015
    shr-int/lit8 v1, v0, 0x1

    new-array v1, v1, [LX/0lu;

    iput-object v1, p0, LX/0lt;->f:[LX/0lu;

    .line 131016
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LX/0lt;->i:I

    .line 131017
    invoke-static {v0}, LX/0lt;->c(I)I

    move-result v0

    iput v0, p0, LX/0lt;->h:I

    move v3, v2

    move v0, v2

    move v1, v2

    .line 131018
    :goto_0
    if-ge v3, v4, :cond_4

    .line 131019
    aget-object v7, v5, v3

    .line 131020
    if-eqz v7, :cond_2

    .line 131021
    add-int/lit8 v1, v1, 0x1

    .line 131022
    invoke-direct {p0, v7}, LX/0lt;->a(Ljava/lang/String;)I

    move-result v8

    invoke-direct {p0, v8}, LX/0lt;->e(I)I

    move-result v8

    .line 131023
    iget-object v9, p0, LX/0lt;->e:[Ljava/lang/String;

    aget-object v9, v9, v8

    if-nez v9, :cond_3

    .line 131024
    iget-object v9, p0, LX/0lt;->e:[Ljava/lang/String;

    aput-object v7, v9, v8

    .line 131025
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 131026
    :cond_3
    shr-int/lit8 v8, v8, 0x1

    .line 131027
    new-instance v9, LX/0lu;

    iget-object v10, p0, LX/0lt;->f:[LX/0lu;

    aget-object v10, v10, v8

    invoke-direct {v9, v7, v10}, LX/0lu;-><init>(Ljava/lang/String;LX/0lu;)V

    .line 131028
    iget-object v7, p0, LX/0lt;->f:[LX/0lu;

    aput-object v9, v7, v8

    .line 131029
    iget v7, v9, LX/0lu;->c:I

    move v7, v7

    .line 131030
    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    .line 131031
    :cond_4
    shr-int/lit8 v4, v4, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 131032
    :goto_2
    if-ge v3, v4, :cond_7

    .line 131033
    aget-object v0, v6, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    .line 131034
    :goto_3
    if-eqz v1, :cond_6

    .line 131035
    add-int/lit8 v2, v2, 0x1

    .line 131036
    iget-object v5, v1, LX/0lu;->a:Ljava/lang/String;

    move-object v5, v5

    .line 131037
    invoke-direct {p0, v5}, LX/0lt;->a(Ljava/lang/String;)I

    move-result v7

    invoke-direct {p0, v7}, LX/0lt;->e(I)I

    move-result v7

    .line 131038
    iget-object v8, p0, LX/0lt;->e:[Ljava/lang/String;

    aget-object v8, v8, v7

    if-nez v8, :cond_5

    .line 131039
    iget-object v8, p0, LX/0lt;->e:[Ljava/lang/String;

    aput-object v5, v8, v7

    .line 131040
    :goto_4
    iget-object v5, v1, LX/0lu;->b:LX/0lu;

    move-object v1, v5

    .line 131041
    goto :goto_3

    .line 131042
    :cond_5
    shr-int/lit8 v7, v7, 0x1

    .line 131043
    new-instance v8, LX/0lu;

    iget-object v9, p0, LX/0lt;->f:[LX/0lu;

    aget-object v9, v9, v7

    invoke-direct {v8, v5, v9}, LX/0lu;-><init>(Ljava/lang/String;LX/0lu;)V

    .line 131044
    iget-object v5, p0, LX/0lt;->f:[LX/0lu;

    aput-object v8, v5, v7

    .line 131045
    iget v5, v8, LX/0lu;->c:I

    move v5, v5

    .line 131046
    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4

    .line 131047
    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    .line 131048
    :cond_7
    iput v1, p0, LX/0lt;->j:I

    .line 131049
    iget v0, p0, LX/0lt;->g:I

    if-eq v2, v0, :cond_0

    .line 131050
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Internal error on SymbolTable.rehash(): had "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/0lt;->g:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " entries; now have "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(ZZ)LX/0lt;
    .locals 9

    .prologue
    .line 130995
    monitor-enter p0

    .line 130996
    :try_start_0
    iget-object v4, p0, LX/0lt;->e:[Ljava/lang/String;

    .line 130997
    iget-object v5, p0, LX/0lt;->f:[LX/0lu;

    .line 130998
    iget v6, p0, LX/0lt;->g:I

    .line 130999
    iget v7, p0, LX/0lt;->l:I

    .line 131000
    iget v8, p0, LX/0lt;->j:I

    .line 131001
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131002
    new-instance v0, LX/0lt;

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v8}, LX/0lt;-><init>(LX/0lt;ZZ[Ljava/lang/String;[LX/0lu;III)V

    return-object v0

    .line 131003
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a([CIII)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xff

    .line 130957
    if-gtz p3, :cond_1

    .line 130958
    const-string v1, ""

    .line 130959
    :cond_0
    :goto_0
    return-object v1

    .line 130960
    :cond_1
    iget-boolean v0, p0, LX/0lt;->d:Z

    if-nez v0, :cond_2

    .line 130961
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 130962
    :cond_2
    invoke-direct {p0, p4}, LX/0lt;->e(I)I

    move-result v1

    .line 130963
    iget-object v0, p0, LX/0lt;->e:[Ljava/lang/String;

    aget-object v2, v0, v1

    .line 130964
    if-eqz v2, :cond_6

    .line 130965
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, p3, :cond_5

    .line 130966
    const/4 v0, 0x0

    .line 130967
    :cond_3
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int v4, p2, v0

    aget-char v4, p1, v4

    if-ne v3, v4, :cond_4

    .line 130968
    add-int/lit8 v0, v0, 0x1

    if-lt v0, p3, :cond_3

    .line 130969
    :cond_4
    if-ne v0, p3, :cond_5

    move-object v1, v2

    .line 130970
    goto :goto_0

    .line 130971
    :cond_5
    iget-object v0, p0, LX/0lt;->f:[LX/0lu;

    shr-int/lit8 v2, v1, 0x1

    aget-object v0, v0, v2

    .line 130972
    if-eqz v0, :cond_6

    .line 130973
    invoke-virtual {v0, p1, p2, p3}, LX/0lu;->a([CII)Ljava/lang/String;

    move-result-object v0

    .line 130974
    if-eqz v0, :cond_6

    move-object v1, v0

    .line 130975
    goto :goto_0

    .line 130976
    :cond_6
    iget-boolean v0, p0, LX/0lt;->k:Z

    if-nez v0, :cond_8

    .line 130977
    invoke-direct {p0}, LX/0lt;->f()V

    .line 130978
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0lt;->k:Z

    move v0, v1

    .line 130979
    :goto_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    .line 130980
    iget-boolean v2, p0, LX/0lt;->c:Z

    if-eqz v2, :cond_7

    .line 130981
    sget-object v2, LX/16J;->a:LX/16J;

    invoke-virtual {v2, v1}, LX/16J;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 130982
    :cond_7
    iget v2, p0, LX/0lt;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/0lt;->g:I

    .line 130983
    iget-object v2, p0, LX/0lt;->e:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    .line 130984
    iget-object v2, p0, LX/0lt;->e:[Ljava/lang/String;

    aput-object v1, v2, v0

    goto :goto_0

    .line 130985
    :cond_8
    iget v0, p0, LX/0lt;->g:I

    iget v2, p0, LX/0lt;->h:I

    if-lt v0, v2, :cond_a

    .line 130986
    invoke-direct {p0}, LX/0lt;->g()V

    .line 130987
    invoke-direct {p0, p1, p3}, LX/0lt;->a([CI)I

    move-result v0

    invoke-direct {p0, v0}, LX/0lt;->e(I)I

    move-result v0

    goto :goto_1

    .line 130988
    :cond_9
    shr-int/lit8 v0, v0, 0x1

    .line 130989
    new-instance v2, LX/0lu;

    iget-object v3, p0, LX/0lt;->f:[LX/0lu;

    aget-object v3, v3, v0

    invoke-direct {v2, v1, v3}, LX/0lu;-><init>(Ljava/lang/String;LX/0lu;)V

    .line 130990
    iget-object v3, p0, LX/0lt;->f:[LX/0lu;

    aput-object v2, v3, v0

    .line 130991
    iget v0, v2, LX/0lu;->c:I

    move v0, v0

    .line 130992
    iget v2, p0, LX/0lt;->j:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0lt;->j:I

    .line 130993
    iget v0, p0, LX/0lt;->j:I

    if-le v0, v5, :cond_0

    .line 130994
    invoke-direct {p0, v5}, LX/0lt;->f(I)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 130951
    iget-boolean v0, p0, LX/0lt;->k:Z

    move v0, v0

    .line 130952
    if-nez v0, :cond_1

    .line 130953
    :cond_0
    :goto_0
    return-void

    .line 130954
    :cond_1
    iget-object v0, p0, LX/0lt;->b:LX/0lt;

    if-eqz v0, :cond_0

    .line 130955
    iget-object v0, p0, LX/0lt;->b:LX/0lt;

    invoke-direct {v0, p0}, LX/0lt;->a(LX/0lt;)V

    .line 130956
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lt;->k:Z

    goto :goto_0
.end method
