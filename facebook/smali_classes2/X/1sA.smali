.class public final LX/1sA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1sB;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 333761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333762
    iput-object v0, p0, LX/1sA;->a:LX/0Ot;

    .line 333763
    iput-object v0, p0, LX/1sA;->b:LX/0Ot;

    .line 333764
    iput-object v0, p0, LX/1sA;->c:LX/0Ot;

    .line 333765
    iput-object v0, p0, LX/1sA;->d:LX/0Ot;

    .line 333766
    iput-object v0, p0, LX/1sA;->e:LX/0Ot;

    .line 333767
    iput-object v0, p0, LX/1sA;->f:LX/0Ot;

    .line 333768
    iput-object v0, p0, LX/1sA;->g:LX/0Ot;

    .line 333769
    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestStatefulActionListComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/partdefinition/FriendRequestUnitComponentSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsActionListComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionImageBlockUnitComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsVerticalListUnitComponentGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333794
    iput-object p1, p0, LX/1sA;->a:LX/0Ot;

    .line 333795
    iput-object p2, p0, LX/1sA;->b:LX/0Ot;

    .line 333796
    iput-object p3, p0, LX/1sA;->c:LX/0Ot;

    .line 333797
    iput-object p4, p0, LX/1sA;->d:LX/0Ot;

    .line 333798
    iput-object p5, p0, LX/1sA;->e:LX/0Ot;

    .line 333799
    iput-object p6, p0, LX/1sA;->f:LX/0Ot;

    .line 333800
    iput-object p7, p0, LX/1sA;->g:LX/0Ot;

    .line 333801
    return-void
.end method

.method public static a(LX/0QB;)LX/1sA;
    .locals 11

    .prologue
    .line 333782
    const-class v1, LX/1sA;

    monitor-enter v1

    .line 333783
    :try_start_0
    sget-object v0, LX/1sA;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 333784
    sput-object v2, LX/1sA;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 333785
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333786
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 333787
    new-instance v3, LX/1sA;

    const/16 v4, 0x224f

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2250

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2ae4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ae8

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ae9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2aeb

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1280

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/1sA;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 333788
    move-object v0, v3

    .line 333789
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 333790
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1sA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333791
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 333792
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333779
    iget-object v0, p0, LX/1sA;->h:LX/0Px;

    if-nez v0, :cond_0

    .line 333780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_STATEFUL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_WIDE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v10

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1sA;->h:LX/0Px;

    .line 333781
    :cond_0
    iget-object v0, p0, LX/1sA;->h:LX/0Px;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 333770
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 333771
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 333772
    :sswitch_0
    iget-object v0, p0, LX/1sA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333773
    :sswitch_1
    iget-object v0, p0, LX/1sA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333774
    :sswitch_2
    iget-object v0, p0, LX/1sA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333775
    :sswitch_3
    iget-object v0, p0, LX/1sA;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333776
    :sswitch_4
    iget-object v0, p0, LX/1sA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333777
    :sswitch_5
    iget-object v0, p0, LX/1sA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 333778
    :sswitch_6
    iget-object v0, p0, LX/1sA;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_3
        0x8 -> :sswitch_6
        0x13 -> :sswitch_3
        0x14 -> :sswitch_6
        0x26 -> :sswitch_2
        0x2e -> :sswitch_4
        0x59 -> :sswitch_2
        0xee -> :sswitch_1
        0xef -> :sswitch_1
        0xfe -> :sswitch_0
    .end sparse-switch
.end method
