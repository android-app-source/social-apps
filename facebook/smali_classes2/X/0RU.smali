.class public final LX/0RU;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0RG;


# direct methods
.method public constructor <init>(LX/0RG;)V
    .locals 0

    .prologue
    .line 60030
    iput-object p1, p0, LX/0RU;->a:LX/0RG;

    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 60031
    iget-object v0, p0, LX/0RU;->a:LX/0RG;

    iget-object v0, v0, LX/0RG;->d:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    invoke-virtual {v0}, LX/0S7;->d()Landroid/content/Context;

    move-result-object v0

    .line 60032
    if-nez v0, :cond_0

    .line 60033
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 60034
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 60035
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v1

    .line 60036
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0SD;->a(B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60037
    new-instance v0, LX/4fr;

    const-string v1, "Should not call getContext in singleton creation. Can lead to memory leaks."

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60038
    :cond_1
    return-object v0
.end method
