.class public final LX/1Xb;
.super LX/0mE;
.source ""


# static fields
.field public static final a:LX/1Xb;

.field public static final b:LX/1Xb;


# instance fields
.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 271388
    new-instance v0, LX/1Xb;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1Xb;-><init>(Z)V

    sput-object v0, LX/1Xb;->a:LX/1Xb;

    .line 271389
    new-instance v0, LX/1Xb;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1Xb;-><init>(Z)V

    sput-object v0, LX/1Xb;->b:LX/1Xb;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 271387
    invoke-direct {p0}, LX/0mE;-><init>()V

    iput-boolean p1, p0, LX/1Xb;->c:Z

    return-void
.end method

.method public static b(Z)LX/1Xb;
    .locals 1

    .prologue
    .line 271369
    if-eqz p0, :cond_0

    sget-object v0, LX/1Xb;->a:LX/1Xb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1Xb;->b:LX/1Xb;

    goto :goto_0
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271386
    iget-boolean v0, p0, LX/1Xb;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public final F()Z
    .locals 1

    .prologue
    .line 271385
    iget-boolean v0, p0, LX/1Xb;->c:Z

    return v0
.end method

.method public final a(D)D
    .locals 2

    .prologue
    .line 271384
    iget-boolean v0, p0, LX/1Xb;->c:Z

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)J
    .locals 2

    .prologue
    .line 271383
    iget-boolean v0, p0, LX/1Xb;->c:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 271382
    iget-boolean v0, p0, LX/1Xb;->c:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 271381
    iget-boolean v0, p0, LX/1Xb;->c:Z

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 271380
    iget-boolean v0, p0, LX/1Xb;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 271374
    if-ne p1, p0, :cond_1

    .line 271375
    :cond_0
    :goto_0
    return v0

    .line 271376
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 271377
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 271378
    goto :goto_0

    .line 271379
    :cond_3
    iget-boolean v2, p0, LX/1Xb;->c:Z

    check-cast p1, LX/1Xb;

    iget-boolean v3, p1, LX/1Xb;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 271373
    sget-object v0, LX/0nH;->BOOLEAN:LX/0nH;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 271371
    iget-boolean v0, p0, LX/1Xb;->c:Z

    invoke-virtual {p1, v0}, LX/0nX;->a(Z)V

    .line 271372
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 271370
    iget-boolean v0, p0, LX/1Xb;->c:Z

    return v0
.end method
