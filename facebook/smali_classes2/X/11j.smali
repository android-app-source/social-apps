.class public LX/11j;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/common/idleexecutor/IdleExecutor;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Wd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 173130
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Wd;
    .locals 4

    .prologue
    .line 173118
    sget-object v0, LX/11j;->a:LX/0Wd;

    if-nez v0, :cond_1

    .line 173119
    const-class v1, LX/11j;

    monitor-enter v1

    .line 173120
    :try_start_0
    sget-object v0, LX/11j;->a:LX/0Wd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 173121
    if-eqz v2, :cond_0

    .line 173122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 173123
    invoke-static {v0}, LX/0Wb;->a(LX/0QB;)LX/0Wb;

    move-result-object v3

    check-cast v3, LX/0Wb;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p0}, LX/0Wc;->b(LX/0Wb;Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v3

    move-object v0, v3

    .line 173124
    sput-object v0, LX/11j;->a:LX/0Wd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173125
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 173126
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 173127
    :cond_1
    sget-object v0, LX/11j;->a:LX/0Wd;

    return-object v0

    .line 173128
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 173129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 173117
    invoke-static {p0}, LX/0Wb;->a(LX/0QB;)LX/0Wb;

    move-result-object v0

    check-cast v0, LX/0Wb;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1}, LX/0Wc;->b(LX/0Wb;Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    return-object v0
.end method
