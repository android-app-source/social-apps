.class public LX/1h4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/1h4;


# instance fields
.field private final b:LX/1h5;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/03V;

.field public final e:LX/0ad;

.field private final f:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295484
    const-class v0, LX/1h4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1h4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1h5;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/03V;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 295485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295486
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LX/1h4;->f:Ljava/lang/ThreadLocal;

    .line 295487
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/1h4;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 295488
    iput-object p1, p0, LX/1h4;->b:LX/1h5;

    .line 295489
    iput-object p2, p0, LX/1h4;->c:Ljava/util/concurrent/ExecutorService;

    .line 295490
    iput-object p3, p0, LX/1h4;->e:LX/0ad;

    .line 295491
    iput-object p4, p0, LX/1h4;->d:LX/03V;

    .line 295492
    return-void
.end method

.method public static a(LX/0QB;)LX/1h4;
    .locals 7

    .prologue
    .line 295493
    sget-object v0, LX/1h4;->h:LX/1h4;

    if-nez v0, :cond_1

    .line 295494
    const-class v1, LX/1h4;

    monitor-enter v1

    .line 295495
    :try_start_0
    sget-object v0, LX/1h4;->h:LX/1h4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 295496
    if-eqz v2, :cond_0

    .line 295497
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 295498
    new-instance p0, LX/1h4;

    invoke-static {v0}, LX/1h5;->a(LX/0QB;)LX/1h5;

    move-result-object v3

    check-cast v3, LX/1h5;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1h4;-><init>(LX/1h5;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/03V;)V

    .line 295499
    move-object v0, p0

    .line 295500
    sput-object v0, LX/1h4;->h:LX/1h4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295501
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 295502
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 295503
    :cond_1
    sget-object v0, LX/1h4;->h:LX/1h4;

    return-object v0

    .line 295504
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 295505
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/1h4;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 295506
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1h4;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 295507
    if-nez v0, :cond_0

    .line 295508
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    move-object v1, v0

    .line 295509
    :goto_0
    sget-object v0, LX/1h8;->b:LX/0U1;

    .line 295510
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 295511
    invoke-virtual {v1, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295512
    :try_start_1
    iget-object v0, p0, LX/1h4;->b:LX/1h5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 295513
    :try_start_2
    const-string v2, "most_recent_hosts_table"

    const/4 v3, 0x0

    const/4 v4, 0x5

    const v5, 0x53c274e2

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v2, 0xd6b6c57

    invoke-static {v2}, LX/03h;->a(I)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295514
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/1h4;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 295515
    monitor-exit p0

    return-void

    .line 295516
    :catch_0
    :try_start_4
    const-string v2, "most_recent_hosts_table"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 295517
    :catch_1
    move-exception v0

    .line 295518
    :try_start_5
    iget-object v2, p0, LX/1h4;->d:LX/03V;

    sget-object v3, LX/1h4;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 295519
    :try_start_6
    iget-object v0, p0, LX/1h4;->b:LX/1h5;

    invoke-virtual {v0}, LX/0Tr;->f()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    .line 295520
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 295521
    iget-object v0, p0, LX/1h4;->e:LX/0ad;

    sget-short v1, LX/0by;->V:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295522
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 295523
    :goto_0
    return-object v0

    .line 295524
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1h4;->b:LX/1h5;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 295525
    const-string v1, "most_recent_hosts_table"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/1h8;->b:LX/0U1;

    .line 295526
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 295527
    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/1h8;->a:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 295528
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 295529
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295530
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 295531
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    .line 295532
    :catch_0
    move-exception v0

    .line 295533
    iget-object v1, p0, LX/1h4;->d:LX/03V;

    sget-object v2, LX/1h4;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 295534
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 295535
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method
