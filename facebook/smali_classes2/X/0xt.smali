.class public final enum LX/0xt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0xt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0xt;

.field public static final enum BANDWIDTH_METER:LX/0xt;

.field public static final enum DATA_CONNECTION_MANAGER:LX/0xt;

.field public static final enum NETWORK_INFO:LX/0xt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163547
    new-instance v0, LX/0xt;

    const-string v1, "BANDWIDTH_METER"

    invoke-direct {v0, v1, v2}, LX/0xt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xt;->BANDWIDTH_METER:LX/0xt;

    .line 163548
    new-instance v0, LX/0xt;

    const-string v1, "DATA_CONNECTION_MANAGER"

    invoke-direct {v0, v1, v3}, LX/0xt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xt;->DATA_CONNECTION_MANAGER:LX/0xt;

    .line 163549
    new-instance v0, LX/0xt;

    const-string v1, "NETWORK_INFO"

    invoke-direct {v0, v1, v4}, LX/0xt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xt;->NETWORK_INFO:LX/0xt;

    .line 163550
    const/4 v0, 0x3

    new-array v0, v0, [LX/0xt;

    sget-object v1, LX/0xt;->BANDWIDTH_METER:LX/0xt;

    aput-object v1, v0, v2

    sget-object v1, LX/0xt;->DATA_CONNECTION_MANAGER:LX/0xt;

    aput-object v1, v0, v3

    sget-object v1, LX/0xt;->NETWORK_INFO:LX/0xt;

    aput-object v1, v0, v4

    sput-object v0, LX/0xt;->$VALUES:[LX/0xt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/0xt;
    .locals 1

    .prologue
    .line 163542
    :try_start_0
    invoke-static {p0}, LX/0xt;->valueOf(Ljava/lang/String;)LX/0xt;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 163543
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/0xt;->BANDWIDTH_METER:LX/0xt;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0xt;
    .locals 1

    .prologue
    .line 163545
    const-class v0, LX/0xt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0xt;

    return-object v0
.end method

.method public static values()[LX/0xt;
    .locals 1

    .prologue
    .line 163544
    sget-object v0, LX/0xt;->$VALUES:[LX/0xt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0xt;

    return-object v0
.end method
