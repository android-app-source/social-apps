.class public LX/1Yu;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fs;


# instance fields
.field private final a:LX/1B1;

.field private final b:LX/0bH;

.field private final c:LX/0jU;

.field private final d:LX/1Vt;

.field private e:LX/1Pf;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1B1;LX/0bH;LX/0jU;LX/1Vt;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 274135
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 274136
    iput-object p1, p0, LX/1Yu;->a:LX/1B1;

    .line 274137
    iput-object p2, p0, LX/1Yu;->b:LX/0bH;

    .line 274138
    iput-object p3, p0, LX/1Yu;->c:LX/0jU;

    .line 274139
    iput-object p4, p0, LX/1Yu;->d:LX/1Vt;

    .line 274140
    const/4 v0, 0x5

    new-array v0, v0, [LX/0b2;

    new-instance v1, LX/1Yv;

    invoke-direct {v1, p0}, LX/1Yv;-><init>(LX/1Yu;)V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, LX/1Yw;

    invoke-direct {v2, p0}, LX/1Yw;-><init>(LX/1Yu;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/1Yx;

    invoke-direct {v2, p0}, LX/1Yx;-><init>(LX/1Yu;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LX/1Yz;

    invoke-direct {v2, p0}, LX/1Yz;-><init>(LX/1Yu;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, LX/1Z0;

    invoke-direct {v2, p0}, LX/1Z0;-><init>(LX/1Yu;)V

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, LX/1B1;->a([LX/0b2;)V

    .line 274141
    return-void
.end method

.method public static a$redex0(LX/1Yu;Ljava/lang/String;)V
    .locals 4
    .param p0    # LX/1Yu;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 274123
    iget-object v0, p0, LX/1Yu;->e:LX/1Pf;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 274124
    :cond_0
    :goto_0
    return-void

    .line 274125
    :cond_1
    iget-object v0, p0, LX/1Yu;->c:LX/0jU;

    invoke-virtual {v0, p1}, LX/0jU;->b(Ljava/lang/String;)Lcom/facebook/api/story/FetchSingleStoryResult;

    move-result-object v0

    .line 274126
    if-eqz v0, :cond_0

    .line 274127
    iget-object v1, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 274128
    invoke-static {v1}, LX/1Vt;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274129
    new-instance v0, LX/CAS;

    invoke-direct {v0, v1}, LX/CAS;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 274130
    iget-object v2, p0, LX/1Yu;->e:LX/1Pf;

    invoke-interface {v2, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAT;

    .line 274131
    iget-boolean v2, v0, LX/CAT;->a:Z

    move v2, v2

    .line 274132
    if-nez v2, :cond_0

    .line 274133
    iput-boolean v3, v0, LX/CAT;->a:Z

    .line 274134
    iget-object v0, p0, LX/1Yu;->e:LX/1Pf;

    new-array v2, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 274121
    iput-object p3, p0, LX/1Yu;->e:LX/1Pf;

    .line 274122
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 274142
    iget-object v0, p0, LX/1Yu;->a:LX/1B1;

    iget-object v1, p0, LX/1Yu;->b:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 274143
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 274119
    iget-object v0, p0, LX/1Yu;->a:LX/1B1;

    iget-object v1, p0, LX/1Yu;->b:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 274120
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 274117
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Yu;->e:LX/1Pf;

    .line 274118
    return-void
.end method
