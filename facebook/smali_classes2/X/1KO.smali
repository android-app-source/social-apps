.class public LX/1KO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1KO;


# instance fields
.field public a:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/MotionEvent;

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 231470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231471
    const/4 v0, 0x0

    iput-object v0, p0, LX/1KO;->c:Landroid/view/MotionEvent;

    .line 231472
    return-void
.end method

.method public static a(LX/0QB;)LX/1KO;
    .locals 5

    .prologue
    .line 231473
    sget-object v0, LX/1KO;->f:LX/1KO;

    if-nez v0, :cond_1

    .line 231474
    const-class v1, LX/1KO;

    monitor-enter v1

    .line 231475
    :try_start_0
    sget-object v0, LX/1KO;->f:LX/1KO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 231476
    if-eqz v2, :cond_0

    .line 231477
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 231478
    new-instance p0, LX/1KO;

    invoke-direct {p0}, LX/1KO;-><init>()V

    .line 231479
    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    .line 231480
    iput-object v3, p0, LX/1KO;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v4, p0, LX/1KO;->b:LX/0Uo;

    .line 231481
    move-object v0, p0

    .line 231482
    sput-object v0, LX/1KO;->f:LX/1KO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 231484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 231485
    :cond_1
    sget-object v0, LX/1KO;->f:LX/1KO;

    return-object v0

    .line 231486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 231487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
