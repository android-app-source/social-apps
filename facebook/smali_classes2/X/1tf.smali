.class public LX/1tf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05R;


# instance fields
.field private final a:LX/1sj;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1sj;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1sj;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336657
    iput-object p1, p0, LX/1tf;->a:LX/1sj;

    .line 336658
    iput-object p2, p0, LX/1tf;->b:LX/0Or;

    .line 336659
    return-void
.end method

.method private static a([B)LX/2ZA;
    .locals 4

    .prologue
    .line 336660
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 336661
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 336662
    new-instance v2, LX/1sr;

    invoke-direct {v2, v1}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 336663
    const/4 v1, 0x0

    .line 336664
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 336665
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 336666
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_1

    .line 336667
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 336668
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 336669
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 p0, 0xb

    if-ne v3, p0, :cond_0

    .line 336670
    invoke-virtual {v0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 336671
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 336672
    :cond_1
    invoke-virtual {v0}, LX/1su;->e()V

    .line 336673
    new-instance v2, LX/2ZA;

    invoke-direct {v2, v1}, LX/2ZA;-><init>(Ljava/lang/String;)V

    .line 336674
    move-object v0, v2

    .line 336675
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/2ZA;)[B
    .locals 2

    .prologue
    .line 336676
    new-instance v0, LX/1so;

    new-instance v1, LX/1sp;

    invoke-direct {v1}, LX/1sp;-><init>()V

    invoke-direct {v0, v1}, LX/1so;-><init>(LX/1sq;)V

    .line 336677
    invoke-virtual {v0, p0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 336678
    const-string v0, "/t_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336679
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 336680
    :goto_0
    return-object v0

    .line 336681
    :cond_0
    :try_start_0
    invoke-static {p2}, LX/1tf;->a([B)LX/2ZA;

    move-result-object v1

    .line 336682
    iget-object v0, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    .line 336683
    :cond_1
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0

    .line 336684
    :cond_2
    iget-object v0, p0, LX/1tf;->a:LX/1sj;

    iget-object v2, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 336685
    invoke-static {v0}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v2

    .line 336686
    const-string v3, "op"

    const-string v4, "mqtt_publish_received"

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336687
    const-string v3, "service"

    const-string v4, "receiver_mqtt_client"

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336688
    const-string v3, "appfg"

    iget-object v4, p0, LX/1tf;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336689
    iget-object v3, p0, LX/1tf;->a:LX/1sj;

    sget-object v4, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v3, v0, v4, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336690
    invoke-static {v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    .line 336691
    invoke-virtual {v2}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v3

    .line 336692
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v4, v5, :cond_3

    iget-object v1, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 336693
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0

    .line 336694
    :cond_3
    invoke-static {v2}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 336695
    iget-object v4, p0, LX/1tf;->a:LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v4, v2, v5, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336696
    new-instance v1, LX/2ZA;

    invoke-direct {v1, v3}, LX/2ZA;-><init>(Ljava/lang/String;)V

    .line 336697
    invoke-static {v1}, LX/1tf;->a(LX/2ZA;)[B

    move-result-object v1

    .line 336698
    const/4 v3, 0x0

    const/4 v4, 0x0

    array-length v5, v1

    invoke-static {v1, v3, p2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336699
    iget-object v1, p0, LX/1tf;->a:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 336700
    :catch_0
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 336701
    check-cast p1, Lcom/facebook/fbtrace/FbTraceNode;

    .line 336702
    invoke-static {p1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 336703
    const-string v2, "success"

    if-eqz p2, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-interface {v1, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336704
    if-eqz p3, :cond_0

    .line 336705
    const-string v0, "error_code"

    invoke-interface {v1, v0, p3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336706
    :cond_0
    iget-object v0, p0, LX/1tf;->a:LX/1sj;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, p1, v2, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336707
    return-void

    .line 336708
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;[B)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 336709
    const-string v0, "/t_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336710
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 336711
    :goto_0
    return-object v0

    .line 336712
    :cond_0
    :try_start_0
    invoke-static {p2}, LX/1tf;->a([B)LX/2ZA;

    move-result-object v1

    .line 336713
    iget-object v0, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    .line 336714
    :cond_1
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0

    .line 336715
    :cond_2
    iget-object v0, p0, LX/1tf;->a:LX/1sj;

    iget-object v2, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 336716
    invoke-static {v0}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v2

    .line 336717
    const-string v3, "op"

    const-string v4, "mqtt_publish_send"

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336718
    const-string v3, "service"

    const-string v4, "sender_mqtt_client"

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336719
    iget-object v3, p0, LX/1tf;->a:LX/1sj;

    sget-object v4, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v3, v0, v4, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336720
    invoke-static {v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    .line 336721
    invoke-virtual {v2}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v3

    .line 336722
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v1, v1, LX/2ZA;->traceInfo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v4, v1, :cond_3

    .line 336723
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0

    .line 336724
    :cond_3
    invoke-static {v2}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 336725
    const-string v4, "op"

    const-string v5, "proxygen_publish_send"

    invoke-interface {v1, v4, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336726
    iget-object v4, p0, LX/1tf;->a:LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v4, v2, v5, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336727
    new-instance v1, LX/2ZA;

    invoke-direct {v1, v3}, LX/2ZA;-><init>(Ljava/lang/String;)V

    .line 336728
    invoke-static {v1}, LX/1tf;->a(LX/2ZA;)[B

    move-result-object v1

    .line 336729
    const/4 v2, 0x0

    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v1, v2, p2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 336730
    :catch_0
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 336731
    check-cast p1, Lcom/facebook/fbtrace/FbTraceNode;

    .line 336732
    invoke-static {p1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 336733
    const-string v2, "success"

    if-eqz p2, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-interface {v1, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336734
    if-eqz p3, :cond_0

    .line 336735
    const-string v0, "error_code"

    invoke-interface {v1, v0, p3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336736
    :cond_0
    iget-object v0, p0, LX/1tf;->a:LX/1sj;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, p1, v2, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 336737
    return-void

    .line 336738
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method
