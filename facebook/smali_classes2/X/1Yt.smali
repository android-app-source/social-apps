.class public LX/1Yt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "LX/1Yt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 274095
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    invoke-direct {p0, v0}, LX/1Yt;-><init>(LX/0tf;)V

    .line 274096
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 274115
    new-instance v0, LX/0tf;

    invoke-direct {v0, p1}, LX/0tf;-><init>(I)V

    invoke-direct {p0, v0}, LX/1Yt;-><init>(LX/0tf;)V

    .line 274116
    return-void
.end method

.method private constructor <init>(LX/0tf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tf",
            "<",
            "LX/1Yt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274113
    iput-object p1, p0, LX/1Yt;->a:LX/0tf;

    .line 274114
    return-void
.end method

.method public constructor <init>([J)V
    .locals 4

    .prologue
    .line 274107
    new-instance v0, LX/0tf;

    array-length v1, p1

    invoke-direct {v0, v1}, LX/0tf;-><init>(I)V

    invoke-direct {p0, v0}, LX/1Yt;-><init>(LX/0tf;)V

    .line 274108
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 274109
    aget-wide v2, p1, v0

    invoke-virtual {p0, v2, v3}, LX/1Yt;->a(J)V

    .line 274110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274111
    :cond_0
    return-void
.end method

.method public static a(I)LX/1Yt;
    .locals 1

    .prologue
    .line 274106
    new-instance v0, LX/1Yt;

    invoke-direct {v0, p0}, LX/1Yt;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 274104
    iget-object v0, p0, LX/1Yt;->a:LX/0tf;

    invoke-virtual {v0, p1, p2, p0}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 274105
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 274103
    iget-object v0, p0, LX/1Yt;->a:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()[J
    .locals 6

    .prologue
    .line 274097
    iget-object v0, p0, LX/1Yt;->a:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->a()I

    move-result v1

    .line 274098
    new-array v2, v1, [J

    .line 274099
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 274100
    iget-object v3, p0, LX/1Yt;->a:LX/0tf;

    invoke-virtual {v3, v0}, LX/0tf;->b(I)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 274101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274102
    :cond_0
    return-object v2
.end method
