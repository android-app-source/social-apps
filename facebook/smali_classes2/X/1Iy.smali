.class public final LX/1Iy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0r4;


# instance fields
.field public final synthetic a:LX/1Iv;


# direct methods
.method public constructor <init>(LX/1Iv;)V
    .locals 0

    .prologue
    .line 229509
    iput-object p1, p0, LX/1Iy;->a:LX/1Iv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 229488
    iget-object v0, p0, LX/1Iy;->a:LX/1Iv;

    invoke-virtual {v0}, LX/1Iv;->m()V

    .line 229489
    return-void
.end method

.method public final b()J
    .locals 7

    .prologue
    .line 229490
    iget-object v0, p0, LX/1Iy;->a:LX/1Iv;

    invoke-static {v0}, LX/1Iv;->z(LX/1Iv;)Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Iy;->a:LX/1Iv;

    invoke-static {v0}, LX/1Iv;->z(LX/1Iv;)Lcom/facebook/common/perftest/PerfTestConfig;

    .line 229491
    sget-wide v4, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->e:J

    move-wide v0, v4

    .line 229492
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 229493
    iget-object v0, p0, LX/1Iy;->a:LX/1Iv;

    invoke-static {v0}, LX/1Iv;->z(LX/1Iv;)Lcom/facebook/common/perftest/PerfTestConfig;

    .line 229494
    sget-wide v4, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->e:J

    move-wide v0, v4

    .line 229495
    :goto_0
    return-wide v0

    .line 229496
    :cond_0
    iget-object v0, p0, LX/1Iy;->a:LX/1Iv;

    iget-object v0, v0, LX/1Iv;->i:LX/0pJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0pJ;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229497
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/1Iy;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->i:LX/0pJ;

    sget v2, LX/1Iv;->a:I

    .line 229498
    sget-wide v4, LX/0X5;->dl:J

    const/4 v6, 0x7

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 229499
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0

    .line 229500
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/1Iy;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->i:LX/0pJ;

    sget v2, LX/1Iv;->a:I

    .line 229501
    sget-wide v4, LX/0X5;->dx:J

    const/16 v6, 0x9

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 229502
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()J
    .locals 7

    .prologue
    .line 229503
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/1Iy;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->i:LX/0pJ;

    const/4 v2, 0x0

    .line 229504
    sget-wide v4, LX/0X5;->dm:J

    const/16 v6, 0x8

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 229505
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 7

    .prologue
    .line 229506
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/1Iy;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->i:LX/0pJ;

    sget v2, LX/1Iv;->a:I

    .line 229507
    sget-wide v4, LX/0X5;->dk:J

    const/4 v6, 0x6

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 229508
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
