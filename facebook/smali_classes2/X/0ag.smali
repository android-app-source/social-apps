.class public LX/0ag;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/nio/ByteBuffer;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field public final f:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;Z)V
    .locals 3

    .prologue
    .line 85418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85419
    iput-object p1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    .line 85420
    if-eqz p2, :cond_0

    .line 85421
    invoke-direct {p0}, LX/0ag;->c()V

    .line 85422
    :cond_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 85423
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, LX/0ag;->f:I

    .line 85424
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->f:I

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    iput v0, p0, LX/0ag;->b:I

    .line 85425
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->f:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    iput v0, p0, LX/0ag;->c:I

    .line 85426
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->f:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    iput v0, p0, LX/0ag;->d:I

    .line 85427
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->f:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    iput v0, p0, LX/0ag;->e:I

    .line 85428
    return-void
.end method

.method public static a()LX/0ag;
    .locals 3

    .prologue
    .line 85415
    :try_start_0
    new-instance v0, LX/0ag;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/26v;->a(Ljava/lang/Iterable;Z)[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/0ag;-><init>(Ljava/nio/ByteBuffer;Z)V
    :try_end_0
    .catch LX/5ol; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 85416
    :catch_0
    move-exception v0

    .line 85417
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(LX/0aa;)LX/0ag;
    .locals 3

    .prologue
    .line 85411
    invoke-interface {p0}, LX/0aa;->d()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 85412
    :try_start_0
    new-instance v1, LX/0ag;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/0ag;-><init>(Ljava/nio/ByteBuffer;Z)V
    :try_end_0
    .catch LX/5ol; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 85413
    :catch_0
    move-exception v0

    .line 85414
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private declared-synchronized c()V
    .locals 5

    .prologue
    .line 85323
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 85324
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 85325
    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    .line 85326
    new-instance v1, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "index.bin is too small to verify: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes less than expected: 12"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85328
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 85329
    const v2, -0x5314ff4

    if-eq v1, v2, :cond_1

    .line 85330
    new-instance v0, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected magic: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Expected: -87117812"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85331
    :cond_1
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 85332
    const v2, 0x20151009

    if-eq v1, v2, :cond_2

    .line 85333
    new-instance v0, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Expected: 538251273"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85334
    :cond_2
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 85335
    if-eq v0, v1, :cond_3

    .line 85336
    new-instance v2, LX/5ol;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected index.bin size: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Expected: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85337
    :cond_3
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 85396
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->b:I

    const/4 v2, -0x1

    invoke-static {v0, v1, p1, v2}, LX/0ah;->b(Ljava/nio/ByteBuffer;III)I

    move-result v0

    .line 85397
    if-ne v0, v3, :cond_0

    .line 85398
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No experiment index found for slot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85400
    :cond_0
    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 85393
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->f:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 85394
    monitor-exit p0

    return v0

    .line 85395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 85401
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0ag;->f:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 85402
    if-ne v1, v0, :cond_1

    .line 85403
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 85404
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0ag;->e:I

    invoke-static {v2, v3, v1}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v1

    .line 85405
    iget-object v2, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v3, 0x1

    invoke-static {v2, v1, v3, p2}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/String;)I

    move-result v2

    .line 85406
    if-eq v2, v0, :cond_0

    .line 85407
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v3, 0x2

    invoke-static {v0, v1, v3}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85408
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v1, v0, v2}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85409
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-static {v1, v0, v2, v3}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 85410
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/26y;)V
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85355
    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85356
    :try_start_1
    iget v2, p0, LX/0ag;->c:I

    if-nez v2, :cond_1

    .line 85357
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 85358
    :cond_0
    monitor-exit p0

    return-void

    .line 85359
    :cond_1
    :try_start_2
    iget-object v2, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0ag;->c:I

    invoke-static {v2, v3}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 85360
    iget-object v2, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, LX/0ag;->e:I

    invoke-static {v2, v4}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v2

    .line 85361
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 85362
    if-ne v3, v2, :cond_2

    :goto_0
    :try_start_3
    invoke-static {v0}, LX/0Tp;->a(Z)V

    move v2, v1

    .line 85363
    :goto_1
    if-ge v2, v3, :cond_0

    .line 85364
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 85365
    :try_start_4
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, LX/0ag;->c:I

    invoke-static {v0, v4, v2}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85366
    iget-object v4, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v4, v0}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v4

    .line 85367
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v5, p0, LX/0ag;->e:I

    invoke-static {v0, v5, v2}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85368
    iget-object v5, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v6, 0x1

    invoke-static {v5, v0, v6}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v5

    .line 85369
    if-nez v5, :cond_3

    .line 85370
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No parameters found for experiment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85371
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 85372
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85373
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_2
    move v0, v1

    .line 85374
    goto :goto_0

    .line 85375
    :cond_3
    :try_start_8
    iget-object v6, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v6, v5}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v6

    .line 85376
    iget-object v7, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v8, 0x2

    invoke-static {v7, v0, v8}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v7

    .line 85377
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move v0, v1

    .line 85378
    :goto_2
    if-ge v0, v6, :cond_5

    .line 85379
    :try_start_9
    monitor-enter p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 85380
    :try_start_a
    iget-object v8, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v8, v5, v0}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v8

    .line 85381
    iget-object v9, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v9, v7, v0}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v9

    .line 85382
    iget-object v10, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v10, v8}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v8

    .line 85383
    iget-object v10, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v11, 0x1

    const/4 v12, -0x1

    invoke-static {v10, v9, v11, v12}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v10

    .line 85384
    iget-object v11, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    const/4 v12, 0x2

    const/4 v13, -0x1

    invoke-static {v11, v9, v12, v13}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v9

    .line 85385
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 85386
    if-nez v0, :cond_4

    .line 85387
    :try_start_b
    invoke-interface {p1, v4, v10}, LX/26y;->a(Ljava/lang/String;I)V

    .line 85388
    :cond_4
    const/4 v11, 0x3

    if-gt v0, v11, :cond_6

    const/4 v11, 0x1

    :goto_3
    move v11, v11

    .line 85389
    invoke-interface {p1, v8, v10, v9, v11}, LX/26y;->a(Ljava/lang/String;IIZ)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 85390
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 85391
    :catchall_3
    move-exception v0

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 85392
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_6
    const/4 v11, 0x0

    goto :goto_3
.end method

.method public final declared-synchronized b()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85349
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    .line 85350
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 85351
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 85352
    move-object v0, v1

    .line 85353
    new-instance v1, LX/2VY;

    invoke-direct {v1, p0, v0}, LX/2VY;-><init>(LX/0ag;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 85354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 85346
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->c:I

    invoke-static {v0, v1, p1}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85347
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    invoke-static {v1, v0}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 85348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 85343
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0ag;->f:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 85344
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 85345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 85338
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ag;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ag;->d:I

    const/4 v2, -0x1

    invoke-static {v0, v1, p1, v2}, LX/0ah;->b(Ljava/nio/ByteBuffer;III)I

    move-result v0

    .line 85339
    if-ne v0, v3, :cond_0

    .line 85340
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No first slot found for experiment index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85342
    :cond_0
    monitor-exit p0

    return v0
.end method
