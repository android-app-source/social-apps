.class public LX/0yt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yn;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:J

.field private static volatile g:LX/0yt;


# instance fields
.field private final b:LX/0yu;

.field private final c:LX/0SG;

.field private d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 166276
    const-wide/16 v0, 0x9

    sput-wide v0, LX/0yt;->a:J

    return-void
.end method

.method public constructor <init>(LX/0yu;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166278
    iput-object p1, p0, LX/0yt;->b:LX/0yu;

    .line 166279
    iput-object p2, p0, LX/0yt;->c:LX/0SG;

    .line 166280
    return-void
.end method

.method public static a(LX/0QB;)LX/0yt;
    .locals 5

    .prologue
    .line 166281
    sget-object v0, LX/0yt;->g:LX/0yt;

    if-nez v0, :cond_1

    .line 166282
    const-class v1, LX/0yt;

    monitor-enter v1

    .line 166283
    :try_start_0
    sget-object v0, LX/0yt;->g:LX/0yt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166284
    if-eqz v2, :cond_0

    .line 166285
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 166286
    new-instance p0, LX/0yt;

    invoke-static {v0}, LX/0yu;->b(LX/0QB;)LX/0yu;

    move-result-object v3

    check-cast v3, LX/0yu;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/0yt;-><init>(LX/0yu;LX/0SG;)V

    .line 166287
    move-object v0, p0

    .line 166288
    sput-object v0, LX/0yt;->g:LX/0yt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166289
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166290
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166291
    :cond_1
    sget-object v0, LX/0yt;->g:LX/0yt;

    return-object v0

    .line 166292
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 11

    .prologue
    .line 166294
    monitor-enter p0

    .line 166295
    :try_start_0
    iget-object v0, p0, LX/0yt;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/0yt;->d:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x2bf20

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 166296
    monitor-exit p0

    .line 166297
    :goto_0
    return-void

    .line 166298
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 166299
    iget-object v0, p0, LX/0yt;->b:LX/0yu;

    sget-wide v2, LX/0yt;->a:J

    const/4 v4, 0x0

    .line 166300
    invoke-static {v0}, LX/0yu;->a(LX/0yu;)J

    move-result-wide v6

    .line 166301
    const-wide/16 v8, -0x3e8

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 166302
    :cond_1
    :goto_1
    move v0, v4

    .line 166303
    monitor-enter p0

    .line 166304
    :try_start_1
    iget-boolean v1, p0, LX/0yt;->f:Z

    if-eq v1, v0, :cond_2

    .line 166305
    iput-boolean v0, p0, LX/0yt;->f:Z

    .line 166306
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yt;->e:Z

    .line 166307
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 166308
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 166309
    :cond_3
    iget-object v5, v0, LX/0yu;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v8

    sub-long v6, v8, v6

    const-wide/32 v8, 0xea60

    div-long/2addr v6, v8

    .line 166310
    cmp-long v5, v6, v2

    if-gtz v5, :cond_1

    const/4 v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 166311
    return-void
.end method

.method public final a(LX/0y8;)V
    .locals 2

    .prologue
    .line 166312
    invoke-direct {p0}, LX/0yt;->c()V

    .line 166313
    monitor-enter p0

    .line 166314
    :try_start_0
    iget-boolean v0, p0, LX/0yt;->f:Z

    invoke-virtual {p1, v0}, LX/0y8;->b(Z)V

    .line 166315
    iget-object v0, p0, LX/0yt;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0yt;->d:J

    .line 166316
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0yt;->e:Z

    .line 166317
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0yl;)V
    .locals 0

    .prologue
    .line 166318
    return-void
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 166319
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0yt;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
