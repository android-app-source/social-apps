.class public LX/1fJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/1fJ;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0Sy;

.field private final d:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/1fL;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCommands"
    .end annotation
.end field

.field private f:Z

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 291288
    const-class v0, LX/1fJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1fJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Sy;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 291280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291281
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    .line 291282
    iput-boolean v1, p0, LX/1fJ;->e:Z

    .line 291283
    iput-boolean v1, p0, LX/1fJ;->f:Z

    .line 291284
    iput-boolean v1, p0, LX/1fJ;->g:Z

    .line 291285
    iput-object p1, p0, LX/1fJ;->b:Ljava/util/concurrent/ExecutorService;

    .line 291286
    iput-object p2, p0, LX/1fJ;->c:LX/0Sy;

    .line 291287
    return-void
.end method

.method public static a(LX/0QB;)LX/1fJ;
    .locals 5

    .prologue
    .line 291267
    sget-object v0, LX/1fJ;->h:LX/1fJ;

    if-nez v0, :cond_1

    .line 291268
    const-class v1, LX/1fJ;

    monitor-enter v1

    .line 291269
    :try_start_0
    sget-object v0, LX/1fJ;->h:LX/1fJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291270
    if-eqz v2, :cond_0

    .line 291271
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291272
    new-instance p0, LX/1fJ;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v4

    check-cast v4, LX/0Sy;

    invoke-direct {p0, v3, v4}, LX/1fJ;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Sy;)V

    .line 291273
    move-object v0, p0

    .line 291274
    sput-object v0, LX/1fJ;->h:LX/1fJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291275
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291276
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291277
    :cond_1
    sget-object v0, LX/1fJ;->h:LX/1fJ;

    return-object v0

    .line 291278
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCommands"
    .end annotation

    .prologue
    .line 291262
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1fJ;->e:Z

    .line 291263
    :try_start_0
    iget-object v0, p0, LX/1fJ;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/api/feedcache/db/service/FeedDbCommandExecutor$1;

    invoke-direct {v1, p0}, Lcom/facebook/api/feedcache/db/service/FeedDbCommandExecutor$1;-><init>(LX/1fJ;)V

    const v2, -0x6816205b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291264
    :goto_0
    return-void

    .line 291265
    :catch_0
    move-exception v0

    .line 291266
    sget-object v1, LX/1fJ;->a:Ljava/lang/String;

    const-string v2, "scheduleService interrupted"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static d(LX/1fJ;)V
    .locals 4

    .prologue
    .line 291289
    :goto_0
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291290
    :try_start_0
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291291
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1fJ;->e:Z

    .line 291292
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    const v2, -0x3e8f6ea4

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 291293
    monitor-exit v1

    return-void

    .line 291294
    :cond_0
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fL;

    .line 291295
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291296
    const-string v1, "FeedDbMutationService(%s)"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x76af6e77

    invoke-static {v1, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 291297
    :try_start_1
    invoke-virtual {v0}, LX/1fL;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 291298
    const v0, 0x418bf8be

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 291299
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 291300
    :catchall_1
    move-exception v0

    const v1, 0x2ba86bfc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 291259
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291260
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1fJ;->g:Z

    .line 291261
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/1fL;)V
    .locals 2

    .prologue
    .line 291251
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291252
    :try_start_0
    iget-boolean v0, p0, LX/1fJ;->g:Z

    if-eqz v0, :cond_0

    .line 291253
    monitor-exit v1

    .line 291254
    :goto_0
    return-void

    .line 291255
    :cond_0
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 291256
    iget-boolean v0, p0, LX/1fJ;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/1fJ;->f:Z

    if-eqz v0, :cond_1

    .line 291257
    invoke-direct {p0}, LX/1fJ;->c()V

    .line 291258
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 291242
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291243
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/1fJ;->g:Z

    .line 291244
    :goto_0
    iget-boolean v0, p0, LX/1fJ;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 291245
    :try_start_1
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    const-wide/16 v2, 0x2710

    const v4, 0x3f6f1f2

    invoke-static {v0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291246
    :try_start_2
    monitor-exit v1

    .line 291247
    :goto_1
    return-void

    .line 291248
    :catch_0
    move-exception v0

    .line 291249
    sget-object v2, LX/1fJ;->a:Ljava/lang/String;

    const-string v3, "stopService interrupted"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 291250
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final b(LX/1fL;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 291226
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291227
    :try_start_0
    iget-boolean v2, p0, LX/1fJ;->f:Z

    if-nez v2, :cond_0

    .line 291228
    monitor-exit v1

    .line 291229
    :goto_0
    return v0

    .line 291230
    :cond_0
    iget-boolean v2, p0, LX/1fJ;->g:Z

    if-eqz v2, :cond_1

    .line 291231
    monitor-exit v1

    goto :goto_0

    .line 291232
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 291233
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 291234
    iget-boolean v0, p0, LX/1fJ;->e:Z

    if-nez v0, :cond_2

    .line 291235
    invoke-direct {p0}, LX/1fJ;->c()V

    .line 291236
    :cond_2
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 291237
    iget-object v1, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 291238
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/1fJ;->f:Z

    .line 291239
    iget-boolean v0, p0, LX/1fJ;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1fJ;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291240
    invoke-direct {p0}, LX/1fJ;->c()V

    .line 291241
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
