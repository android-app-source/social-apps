.class public abstract LX/0Oo;
.super LX/0On;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54368
    invoke-direct {p0}, LX/0On;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 54384
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/0Oo;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, LX/0Oo;->a:LX/0Uh;

    .line 54385
    return-void
.end method

.method public final b()Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54369
    invoke-virtual {p0}, LX/0Oo;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/2BO;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54370
    :cond_0
    :goto_0
    return v0

    .line 54371
    :cond_1
    iget-object v2, p0, LX/0Oo;->a:LX/0Uh;

    const/16 v3, 0x7

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 54372
    if-eqz v2, :cond_2

    invoke-virtual {p0}, LX/0Oo;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 54373
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 54374
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 54375
    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v7

    .line 54376
    array-length v8, v7

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_4

    aget-object v9, v7, v5

    .line 54377
    sget-object v10, LX/2fu;->a:Ljava/util/Set;

    invoke-interface {v10, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 54378
    const/16 v10, 0x40

    :try_start_0
    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 54379
    iget-object v10, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v10, :cond_3

    iget-object v10, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v10, v10

    if-ne v10, v3, :cond_3

    sget-object v10, LX/44q;->a:Ljava/util/Set;

    iget-object v9, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 p0, 0x0

    aget-object v9, v9, p0

    invoke-virtual {v9}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v9

    invoke-static {v9}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v10, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-eqz v9, :cond_3

    .line 54380
    :goto_2
    move v2, v3

    .line 54381
    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 54382
    :catch_0
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    move v3, v4

    .line 54383
    goto :goto_2
.end method
