.class public final enum LX/0mq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0mq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0mq;

.field public static final enum CLIENT_EVENT:LX/0mq;

.field public static final enum EXPERIMENT:LX/0mq;


# instance fields
.field private final mExtraJsonKey:Ljava/lang/String;

.field private final mProtocolValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 133126
    new-instance v0, LX/0mq;

    const-string v1, "CLIENT_EVENT"

    const-string v2, "client_event"

    const-string v3, "extra"

    invoke-direct {v0, v1, v4, v2, v3}, LX/0mq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0mq;->CLIENT_EVENT:LX/0mq;

    .line 133127
    new-instance v0, LX/0mq;

    const-string v1, "EXPERIMENT"

    const-string v2, "experiment"

    const-string v3, "result"

    invoke-direct {v0, v1, v5, v2, v3}, LX/0mq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0mq;->EXPERIMENT:LX/0mq;

    .line 133128
    const/4 v0, 0x2

    new-array v0, v0, [LX/0mq;

    sget-object v1, LX/0mq;->CLIENT_EVENT:LX/0mq;

    aput-object v1, v0, v4

    sget-object v1, LX/0mq;->EXPERIMENT:LX/0mq;

    aput-object v1, v0, v5

    sput-object v0, LX/0mq;->$VALUES:[LX/0mq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133123
    iput-object p3, p0, LX/0mq;->mProtocolValue:Ljava/lang/String;

    .line 133124
    iput-object p4, p0, LX/0mq;->mExtraJsonKey:Ljava/lang/String;

    .line 133125
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0mq;
    .locals 1

    .prologue
    .line 133117
    const-class v0, LX/0mq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0mq;

    return-object v0
.end method

.method public static values()[LX/0mq;
    .locals 1

    .prologue
    .line 133121
    sget-object v0, LX/0mq;->$VALUES:[LX/0mq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0mq;

    return-object v0
.end method


# virtual methods
.method public final getExtraJsonKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133120
    iget-object v0, p0, LX/0mq;->mExtraJsonKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getProtocolValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133119
    iget-object v0, p0, LX/0mq;->mProtocolValue:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133118
    iget-object v0, p0, LX/0mq;->mProtocolValue:Ljava/lang/String;

    return-object v0
.end method
