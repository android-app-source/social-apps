.class public LX/1Jx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public b:LX/1Jw;

.field public c:LX/1Jw;


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 230759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230760
    iput-boolean p1, p0, LX/1Jx;->a:Z

    .line 230761
    return-void
.end method


# virtual methods
.method public final b(LX/1Jw;LX/1Jw;)V
    .locals 0
    .param p1    # LX/1Jw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Jw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 230762
    iput-object p1, p0, LX/1Jx;->b:LX/1Jw;

    .line 230763
    iput-object p2, p0, LX/1Jx;->c:LX/1Jw;

    .line 230764
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230765
    instance-of v1, p1, LX/1Jx;

    if-nez v1, :cond_1

    .line 230766
    :cond_0
    :goto_0
    return v0

    .line 230767
    :cond_1
    check-cast p1, LX/1Jx;

    .line 230768
    iget-object v1, p0, LX/1Jx;->b:LX/1Jw;

    move-object v1, v1

    .line 230769
    iget-object v2, p1, LX/1Jx;->b:LX/1Jw;

    move-object v2, v2

    .line 230770
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230771
    iget-object v1, p0, LX/1Jx;->c:LX/1Jw;

    move-object v1, v1

    .line 230772
    iget-object v2, p1, LX/1Jx;->c:LX/1Jw;

    move-object v2, v2

    .line 230773
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 230774
    const/4 v0, 0x0

    .line 230775
    iget-object v1, p0, LX/1Jx;->b:LX/1Jw;

    if-eqz v1, :cond_0

    .line 230776
    iget-object v0, p0, LX/1Jx;->b:LX/1Jw;

    invoke-virtual {v0}, LX/1Jw;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 230777
    :cond_0
    iget-object v1, p0, LX/1Jx;->c:LX/1Jw;

    if-eqz v1, :cond_1

    .line 230778
    iget-object v1, p0, LX/1Jx;->c:LX/1Jw;

    invoke-virtual {v1}, LX/1Jw;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 230779
    :cond_1
    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method
