.class public final LX/1iJ;
.super Ljava/io/FilterOutputStream;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation


# instance fields
.field public a:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 297738
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 297739
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 297743
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 297744
    return-void
.end method

.method public final write(I)V
    .locals 4

    .prologue
    .line 297745
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 297746
    iget-wide v0, p0, LX/1iJ;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1iJ;->a:J

    .line 297747
    return-void
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 297740
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 297741
    iget-wide v0, p0, LX/1iJ;->a:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1iJ;->a:J

    .line 297742
    return-void
.end method
