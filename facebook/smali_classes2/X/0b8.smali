.class public final LX/0b8;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
        "LX/BaK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/BaK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86640
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86641
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86642
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 86643
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    check-cast p2, LX/BaK;

    .line 86644
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86645
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v0, v1

    .line 86646
    if-nez v0, :cond_1

    .line 86647
    :cond_0
    :goto_0
    return-void

    .line 86648
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 86649
    iget-object v1, p2, LX/BaK;->a:LX/1EY;

    .line 86650
    iget-object v2, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v2, v2

    .line 86651
    invoke-virtual {v1, v2}, LX/1EY;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 86652
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86653
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86654
    iget-object v1, p2, LX/BaK;->b:LX/1EW;

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 86655
    iget p0, v1, LX/1EW;->e:I

    sub-int/2addr p0, v2

    iput p0, v1, LX/1EW;->e:I

    .line 86656
    iget-object p0, v1, LX/1EW;->c:Landroid/view/View;

    if-nez p0, :cond_2

    .line 86657
    :goto_1
    goto :goto_0

    .line 86658
    :cond_2
    iget-object p0, v1, LX/1EW;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f082abc

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/1EW;->c(LX/1EW;Ljava/lang/String;)V

    .line 86659
    iget-object p0, v1, LX/1EW;->d:LX/4nm;

    const p1, 0x7f082abd

    new-instance v2, LX/BaJ;

    invoke-direct {v2, v1, v0}, LX/BaJ;-><init>(LX/1EW;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v2}, LX/4nm;->a(ILandroid/view/View$OnClickListener;)LX/4nm;

    .line 86660
    iget-object p0, v1, LX/1EW;->d:LX/4nm;

    invoke-virtual {p0}, LX/4nm;->a()V

    goto :goto_1
.end method
