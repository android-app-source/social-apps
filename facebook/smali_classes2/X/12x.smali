.class public LX/12x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SetRepeatingUse",
        "AlarmManagerUse"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/12x;


# instance fields
.field private final a:Landroid/app/AlarmManager;

.field private final b:LX/12z;


# direct methods
.method public constructor <init>(Landroid/app/AlarmManager;LX/12z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175913
    iput-object p1, p0, LX/12x;->a:Landroid/app/AlarmManager;

    .line 175914
    iput-object p2, p0, LX/12x;->b:LX/12z;

    .line 175915
    return-void
.end method

.method public static a(LX/0QB;)LX/12x;
    .locals 5

    .prologue
    .line 175882
    sget-object v0, LX/12x;->c:LX/12x;

    if-nez v0, :cond_1

    .line 175883
    const-class v1, LX/12x;

    monitor-enter v1

    .line 175884
    :try_start_0
    sget-object v0, LX/12x;->c:LX/12x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175885
    if-eqz v2, :cond_0

    .line 175886
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 175887
    new-instance p0, LX/12x;

    invoke-static {v0}, LX/12y;->b(LX/0QB;)Landroid/app/AlarmManager;

    move-result-object v3

    check-cast v3, Landroid/app/AlarmManager;

    invoke-static {v0}, LX/12z;->a(LX/0QB;)LX/12z;

    move-result-object v4

    check-cast v4, LX/12z;

    invoke-direct {p0, v3, v4}, LX/12x;-><init>(Landroid/app/AlarmManager;LX/12z;)V

    .line 175888
    move-object v0, p0

    .line 175889
    sput-object v0, LX/12x;->c:LX/12x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175890
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175891
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175892
    :cond_1
    sget-object v0, LX/12x;->c:LX/12x;

    return-object v0

    .line 175893
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175894
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(IJJLandroid/app/PendingIntent;)V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetInexactRepeatingArgs"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 175903
    cmp-long v0, p2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "The alarm trigger time must be > 0"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 175904
    cmp-long v0, p4, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v2, "Alarm interval must be > 0"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 175905
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_3

    .line 175906
    iget-object v0, p0, LX/12x;->b:LX/12z;

    iget-object v7, p0, LX/12x;->a:Landroid/app/AlarmManager;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    invoke-virtual/range {v0 .. v7}, LX/12z;->a(IJJLandroid/app/PendingIntent;Landroid/app/AlarmManager;)V

    .line 175907
    :goto_2
    if-eqz v8, :cond_0

    .line 175908
    iget-object v0, p0, LX/12x;->a:Landroid/app/AlarmManager;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 175909
    :cond_0
    return-void

    :cond_1
    move v0, v8

    .line 175910
    goto :goto_0

    :cond_2
    move v0, v8

    .line 175911
    goto :goto_1

    :cond_3
    move v8, v1

    goto :goto_2
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 175901
    iget-object v0, p0, LX/12x;->a:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 175902
    return-void
.end method

.method public final b(IJLandroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 175899
    iget-object v0, p0, LX/12x;->a:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 175900
    return-void
.end method

.method public final c(IJLandroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 175895
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 175896
    iget-object v0, p0, LX/12x;->a:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 175897
    :goto_0
    return-void

    .line 175898
    :cond_0
    iget-object v0, p0, LX/12x;->a:Landroid/app/AlarmManager;

    invoke-static {v0, p1, p2, p3, p4}, LX/30B;->a(Landroid/app/AlarmManager;IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method
