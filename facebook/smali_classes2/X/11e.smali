.class public LX/11e;
.super Ljava/util/AbstractCollection;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractCollection",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final d:LX/11e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xs",
            "<TK;TV;>.WrappedCollection;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Collection;LX/11e;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/11e;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Collection",
            "<TV;>;",
            "LX/0Xs",
            "<TK;TV;>.WrappedCollection;)V"
        }
    .end annotation

    .prologue
    .line 172734
    iput-object p1, p0, LX/11e;->f:LX/0Xs;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 172735
    iput-object p2, p0, LX/11e;->b:Ljava/lang/Object;

    .line 172736
    iput-object p3, p0, LX/11e;->c:Ljava/util/Collection;

    .line 172737
    iput-object p4, p0, LX/11e;->d:LX/11e;

    .line 172738
    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/11e;->e:Ljava/util/Collection;

    .line 172739
    return-void

    .line 172740
    :cond_0
    iget-object v0, p4, LX/11e;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 172741
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 172811
    iget-object v0, p0, LX/11e;->d:LX/11e;

    if-eqz v0, :cond_0

    .line 172812
    iget-object v0, p0, LX/11e;->d:LX/11e;

    invoke-virtual {v0}, LX/11e;->a()V

    .line 172813
    iget-object v0, p0, LX/11e;->d:LX/11e;

    .line 172814
    iget-object v1, v0, LX/11e;->c:Ljava/util/Collection;

    move-object v0, v1

    .line 172815
    iget-object v1, p0, LX/11e;->e:Ljava/util/Collection;

    if-eq v0, v1, :cond_1

    .line 172816
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 172817
    :cond_0
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172818
    iget-object v0, p0, LX/11e;->f:LX/0Xs;

    iget-object v0, v0, LX/0Xs;->a:Ljava/util/Map;

    iget-object v1, p0, LX/11e;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 172819
    if-eqz v0, :cond_1

    .line 172820
    iput-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    .line 172821
    :cond_1
    return-void
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 172803
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172804
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    .line 172805
    iget-object v1, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 172806
    if-eqz v1, :cond_0

    .line 172807
    iget-object v2, p0, LX/11e;->f:LX/0Xs;

    invoke-static {v2}, LX/0Xs;->c(LX/0Xs;)I

    .line 172808
    if-eqz v0, :cond_0

    .line 172809
    invoke-virtual {p0}, LX/11e;->d()V

    .line 172810
    :cond_0
    return v1
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 172793
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172794
    const/4 v0, 0x0

    .line 172795
    :cond_0
    :goto_0
    return v0

    .line 172796
    :cond_1
    invoke-virtual {p0}, LX/11e;->size()I

    move-result v1

    .line 172797
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    .line 172798
    if-eqz v0, :cond_0

    .line 172799
    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 172800
    iget-object v3, p0, LX/11e;->f:LX/0Xs;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, LX/0Xs;->a(LX/0Xs;I)I

    .line 172801
    if-nez v1, :cond_0

    .line 172802
    invoke-virtual {p0}, LX/11e;->d()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 172788
    iget-object v0, p0, LX/11e;->d:LX/11e;

    if-eqz v0, :cond_1

    .line 172789
    iget-object v0, p0, LX/11e;->d:LX/11e;

    invoke-virtual {v0}, LX/11e;->b()V

    .line 172790
    :cond_0
    :goto_0
    return-void

    .line 172791
    :cond_1
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172792
    iget-object v0, p0, LX/11e;->f:LX/0Xs;

    iget-object v0, v0, LX/0Xs;->a:Ljava/util/Map;

    iget-object v1, p0, LX/11e;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 172782
    invoke-virtual {p0}, LX/11e;->size()I

    move-result v0

    .line 172783
    if-nez v0, :cond_0

    .line 172784
    :goto_0
    return-void

    .line 172785
    :cond_0
    iget-object v1, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    .line 172786
    iget-object v1, p0, LX/11e;->f:LX/0Xs;

    invoke-static {v1, v0}, LX/0Xs;->b(LX/0Xs;I)I

    .line 172787
    invoke-virtual {p0}, LX/11e;->b()V

    goto :goto_0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 172780
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172781
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 172778
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172779
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 172822
    iget-object v0, p0, LX/11e;->d:LX/11e;

    if-eqz v0, :cond_0

    .line 172823
    iget-object v0, p0, LX/11e;->d:LX/11e;

    invoke-virtual {v0}, LX/11e;->d()V

    .line 172824
    :goto_0
    return-void

    .line 172825
    :cond_0
    iget-object v0, p0, LX/11e;->f:LX/0Xs;

    iget-object v0, v0, LX/0Xs;->a:Ljava/util/Map;

    iget-object v1, p0, LX/11e;->b:Ljava/lang/Object;

    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 172773
    if-ne p1, p0, :cond_0

    .line 172774
    const/4 v0, 0x1

    .line 172775
    :goto_0
    return v0

    .line 172776
    :cond_0
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172777
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 172771
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172772
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->hashCode()I

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 172769
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172770
    new-instance v0, LX/11f;

    invoke-direct {v0, p0}, LX/11f;-><init>(LX/11e;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 172763
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172764
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 172765
    if-eqz v0, :cond_0

    .line 172766
    iget-object v1, p0, LX/11e;->f:LX/0Xs;

    invoke-static {v1}, LX/0Xs;->b(LX/0Xs;)I

    .line 172767
    invoke-virtual {p0}, LX/11e;->b()V

    .line 172768
    :cond_0
    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 172754
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172755
    const/4 v0, 0x0

    .line 172756
    :cond_0
    :goto_0
    return v0

    .line 172757
    :cond_1
    invoke-virtual {p0}, LX/11e;->size()I

    move-result v1

    .line 172758
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    .line 172759
    if-eqz v0, :cond_0

    .line 172760
    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 172761
    iget-object v3, p0, LX/11e;->f:LX/0Xs;

    sub-int v1, v2, v1

    invoke-static {v3, v1}, LX/0Xs;->a(LX/0Xs;I)I

    .line 172762
    invoke-virtual {p0}, LX/11e;->b()V

    goto :goto_0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 172746
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172747
    invoke-virtual {p0}, LX/11e;->size()I

    move-result v0

    .line 172748
    iget-object v1, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->retainAll(Ljava/util/Collection;)Z

    move-result v1

    .line 172749
    if-eqz v1, :cond_0

    .line 172750
    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 172751
    iget-object v3, p0, LX/11e;->f:LX/0Xs;

    sub-int v0, v2, v0

    invoke-static {v3, v0}, LX/0Xs;->a(LX/0Xs;I)I

    .line 172752
    invoke-virtual {p0}, LX/11e;->b()V

    .line 172753
    :cond_0
    return v1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 172744
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172745
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172742
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172743
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
