.class public LX/0nW;
.super LX/0nX;
.source ""


# static fields
.field public static final b:I


# instance fields
.field public c:LX/0lD;

.field public d:I

.field public e:Z

.field public f:LX/4s0;

.field public g:LX/4s0;

.field public h:I

.field public i:LX/12U;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136207
    invoke-static {}, LX/0lr;->collectDefaults()I

    move-result v0

    sput v0, LX/0nW;->b:I

    return-void
.end method

.method public constructor <init>(LX/0lD;)V
    .locals 1

    .prologue
    .line 136163
    invoke-direct {p0}, LX/0nX;-><init>()V

    .line 136164
    iput-object p1, p0, LX/0nW;->c:LX/0lD;

    .line 136165
    sget v0, LX/0nW;->b:I

    iput v0, p0, LX/0nW;->d:I

    .line 136166
    invoke-static {}, LX/12U;->i()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136167
    new-instance v0, LX/4s0;

    invoke-direct {v0}, LX/4s0;-><init>()V

    iput-object v0, p0, LX/0nW;->g:LX/4s0;

    iput-object v0, p0, LX/0nW;->f:LX/4s0;

    .line 136168
    const/4 v0, 0x0

    iput v0, p0, LX/0nW;->h:I

    .line 136169
    return-void
.end method

.method private a(LX/15z;)V
    .locals 2

    .prologue
    .line 136170
    iget-object v0, p0, LX/0nW;->g:LX/4s0;

    iget v1, p0, LX/0nW;->h:I

    invoke-virtual {v0, v1, p1}, LX/4s0;->a(ILX/15z;)LX/4s0;

    move-result-object v0

    .line 136171
    if-nez v0, :cond_0

    .line 136172
    iget v0, p0, LX/0nW;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0nW;->h:I

    .line 136173
    :goto_0
    return-void

    .line 136174
    :cond_0
    iput-object v0, p0, LX/0nW;->g:LX/4s0;

    .line 136175
    const/4 v0, 0x1

    iput v0, p0, LX/0nW;->h:I

    goto :goto_0
.end method

.method private a(LX/15z;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 136176
    iget-object v0, p0, LX/0nW;->g:LX/4s0;

    iget v1, p0, LX/0nW;->h:I

    invoke-virtual {v0, v1, p1, p2}, LX/4s0;->a(ILX/15z;Ljava/lang/Object;)LX/4s0;

    move-result-object v0

    .line 136177
    if-nez v0, :cond_0

    .line 136178
    iget v0, p0, LX/0nW;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0nW;->h:I

    .line 136179
    :goto_0
    return-void

    .line 136180
    :cond_0
    iput-object v0, p0, LX/0nW;->g:LX/4s0;

    .line 136181
    const/4 v0, 0x1

    iput v0, p0, LX/0nW;->h:I

    goto :goto_0
.end method

.method private b(LX/0lD;)LX/15w;
    .locals 2

    .prologue
    .line 136182
    new-instance v0, LX/4rz;

    iget-object v1, p0, LX/0nW;->f:LX/4s0;

    invoke-direct {v0, v1, p1}, LX/4rz;-><init>(LX/4s0;LX/0lD;)V

    return-object v0
.end method

.method private c(LX/15w;)V
    .locals 3

    .prologue
    .line 136183
    sget-object v0, LX/4ry;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 136184
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Internal error: should never end up through this code path"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136185
    :pswitch_0
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 136186
    :goto_0
    return-void

    .line 136187
    :pswitch_1
    invoke-virtual {p0}, LX/0nX;->g()V

    goto :goto_0

    .line 136188
    :pswitch_2
    invoke-virtual {p0}, LX/0nX;->d()V

    goto :goto_0

    .line 136189
    :pswitch_3
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0

    .line 136190
    :pswitch_4
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 136191
    :pswitch_5
    invoke-virtual {p1}, LX/15w;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136192
    invoke-virtual {p1}, LX/15w;->p()[C

    move-result-object v0

    invoke-virtual {p1}, LX/15w;->r()I

    move-result v1

    invoke-virtual {p1}, LX/15w;->q()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/0nX;->a([CII)V

    goto :goto_0

    .line 136193
    :cond_0
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 136194
    :pswitch_6
    sget-object v0, LX/4ry;->b:[I

    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v1

    invoke-virtual {v1}, LX/16L;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 136195
    invoke-virtual {p1}, LX/15w;->y()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(J)V

    goto :goto_0

    .line 136196
    :pswitch_7
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->b(I)V

    goto :goto_0

    .line 136197
    :pswitch_8
    invoke-virtual {p1}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/math/BigInteger;)V

    goto :goto_0

    .line 136198
    :pswitch_9
    sget-object v0, LX/4ry;->b:[I

    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v1

    invoke-virtual {v1}, LX/16L;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 136199
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(D)V

    goto :goto_0

    .line 136200
    :pswitch_a
    invoke-virtual {p1}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 136201
    :pswitch_b
    invoke-virtual {p1}, LX/15w;->A()F

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(F)V

    goto :goto_0

    .line 136202
    :pswitch_c
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0nX;->a(Z)V

    goto/16 :goto_0

    .line 136203
    :pswitch_d
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0nX;->a(Z)V

    goto/16 :goto_0

    .line 136204
    :pswitch_e
    invoke-virtual {p0}, LX/0nX;->h()V

    goto/16 :goto_0

    .line 136205
    :pswitch_f
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private static k()V
    .locals 2

    .prologue
    .line 136206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Called operation not supported for TokenBuffer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 136157
    iget-object v0, p0, LX/0nW;->c:LX/0lD;

    return-object v0
.end method

.method public final a(LX/0nW;)LX/0nW;
    .locals 2

    .prologue
    .line 136208
    invoke-virtual {p1}, LX/0nW;->i()LX/15w;

    move-result-object v0

    .line 136209
    :goto_0
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 136210
    invoke-direct {p0, v0}, LX/0nW;->c(LX/15w;)V

    goto :goto_0

    .line 136211
    :cond_0
    return-object p0
.end method

.method public final a(LX/0lD;)LX/0nX;
    .locals 0

    .prologue
    .line 136212
    iput-object p1, p0, LX/0nW;->c:LX/0lD;

    .line 136213
    return-object p0
.end method

.method public final a(LX/15w;)LX/15w;
    .locals 3

    .prologue
    .line 136214
    new-instance v0, LX/4rz;

    iget-object v1, p0, LX/0nW;->f:LX/4s0;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4rz;-><init>(LX/4s0;LX/0lD;)V

    .line 136215
    invoke-virtual {p1}, LX/15w;->k()LX/28G;

    move-result-object v1

    .line 136216
    iput-object v1, v0, LX/4rz;->h:LX/28G;

    .line 136217
    return-object v0
.end method

.method public final a(C)V
    .locals 0

    .prologue
    .line 136220
    invoke-static {}, LX/0nW;->k()V

    .line 136221
    return-void
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 136218
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136219
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 136303
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136304
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 136301
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136302
    return-void
.end method

.method public final a(LX/0lG;)V
    .locals 1

    .prologue
    .line 136299
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136300
    return-void
.end method

.method public final a(LX/0ln;[BII)V
    .locals 2

    .prologue
    .line 136295
    new-array v0, p4, [B

    .line 136296
    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136297
    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 136298
    return-void
.end method

.method public final a(LX/0nX;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 136235
    iget-object v3, p0, LX/0nW;->f:LX/4s0;

    .line 136236
    const/4 v1, -0x1

    move v0, v1

    move-object v1, v3

    .line 136237
    :goto_0
    add-int/lit8 v0, v0, 0x1

    const/16 v3, 0x10

    if-lt v0, v3, :cond_c

    .line 136238
    iget-object v0, v1, LX/4s0;->a:LX/4s0;

    move-object v0, v0

    .line 136239
    if-eqz v0, :cond_b

    move v1, v2

    move-object v3, v0

    .line 136240
    :goto_1
    invoke-virtual {v3, v1}, LX/4s0;->a(I)LX/15z;

    move-result-object v0

    .line 136241
    if-eqz v0, :cond_b

    .line 136242
    sget-object v4, LX/4ry;->a:[I

    invoke-virtual {v0}, LX/15z;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 136243
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Internal error: should never end up through this code path"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136244
    :pswitch_0
    invoke-virtual {p1}, LX/0nX;->f()V

    move v0, v1

    move-object v1, v3

    .line 136245
    goto :goto_0

    .line 136246
    :pswitch_1
    invoke-virtual {p1}, LX/0nX;->g()V

    move v0, v1

    move-object v1, v3

    .line 136247
    goto :goto_0

    .line 136248
    :pswitch_2
    invoke-virtual {p1}, LX/0nX;->d()V

    move v0, v1

    move-object v1, v3

    .line 136249
    goto :goto_0

    .line 136250
    :pswitch_3
    invoke-virtual {p1}, LX/0nX;->e()V

    move v0, v1

    move-object v1, v3

    .line 136251
    goto :goto_0

    .line 136252
    :pswitch_4
    invoke-virtual {v3, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 136253
    instance-of v4, v0, LX/0lc;

    if-eqz v4, :cond_0

    .line 136254
    check-cast v0, LX/0lc;

    invoke-virtual {p1, v0}, LX/0nX;->b(LX/0lc;)V

    :goto_2
    move v0, v1

    move-object v1, v3

    .line 136255
    goto :goto_0

    .line 136256
    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    move v0, v1

    move-object v1, v3

    .line 136257
    goto :goto_0

    .line 136258
    :pswitch_5
    invoke-virtual {v3, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 136259
    instance-of v4, v0, LX/0lc;

    if-eqz v4, :cond_1

    .line 136260
    check-cast v0, LX/0lc;

    invoke-virtual {p1, v0}, LX/0nX;->c(LX/0lc;)V

    goto :goto_2

    .line 136261
    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    move v0, v1

    move-object v1, v3

    .line 136262
    goto :goto_0

    .line 136263
    :pswitch_6
    invoke-virtual {v3, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 136264
    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 136265
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    goto :goto_2

    .line 136266
    :cond_2
    instance-of v4, v0, Ljava/math/BigInteger;

    if-eqz v4, :cond_3

    .line 136267
    check-cast v0, Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/math/BigInteger;)V

    goto :goto_2

    .line 136268
    :cond_3
    instance-of v4, v0, Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 136269
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, LX/0nX;->a(J)V

    goto :goto_2

    .line 136270
    :cond_4
    instance-of v4, v0, Ljava/lang/Short;

    if-eqz v4, :cond_5

    .line 136271
    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, LX/0nX;->a(S)V

    goto :goto_2

    .line 136272
    :cond_5
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    move v0, v1

    move-object v1, v3

    .line 136273
    goto/16 :goto_0

    .line 136274
    :pswitch_7
    invoke-virtual {v3, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 136275
    instance-of v4, v0, Ljava/lang/Double;

    if-eqz v4, :cond_6

    .line 136276
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    goto :goto_2

    .line 136277
    :cond_6
    instance-of v4, v0, Ljava/math/BigDecimal;

    if-eqz v4, :cond_7

    .line 136278
    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/math/BigDecimal;)V

    goto/16 :goto_2

    .line 136279
    :cond_7
    instance-of v4, v0, Ljava/lang/Float;

    if-eqz v4, :cond_8

    .line 136280
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v0}, LX/0nX;->a(F)V

    goto/16 :goto_2

    .line 136281
    :cond_8
    if-nez v0, :cond_9

    .line 136282
    invoke-virtual {p1}, LX/0nX;->h()V

    goto/16 :goto_2

    .line 136283
    :cond_9
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    .line 136284
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 136285
    :cond_a
    new-instance v1, LX/4pY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized value type for VALUE_NUMBER_FLOAT: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", can not serialize"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136286
    :pswitch_8
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0nX;->a(Z)V

    move v0, v1

    move-object v1, v3

    .line 136287
    goto/16 :goto_0

    .line 136288
    :pswitch_9
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    move v0, v1

    move-object v1, v3

    .line 136289
    goto/16 :goto_0

    .line 136290
    :pswitch_a
    invoke-virtual {p1}, LX/0nX;->h()V

    move v0, v1

    move-object v1, v3

    .line 136291
    goto/16 :goto_0

    .line 136292
    :pswitch_b
    invoke-virtual {v3, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    move v0, v1

    move-object v1, v3

    .line 136293
    goto/16 :goto_0

    .line 136294
    :cond_b
    return-void

    :cond_c
    move-object v3, v1

    move v1, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 136233
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136234
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136230
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136231
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    invoke-virtual {v0, p1}, LX/12U;->a(Ljava/lang/String;)I

    .line 136232
    return-void
.end method

.method public final a(Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 136226
    if-nez p1, :cond_0

    .line 136227
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 136228
    :goto_0
    return-void

    .line 136229
    :cond_0
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 136222
    if-nez p1, :cond_0

    .line 136223
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 136224
    :goto_0
    return-void

    .line 136225
    :cond_0
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(S)V
    .locals 2

    .prologue
    .line 136158
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136159
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 136160
    if-eqz p1, :cond_0

    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    :goto_0
    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136161
    return-void

    .line 136162
    :cond_0
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    goto :goto_0
.end method

.method public final a([CII)V
    .locals 1

    .prologue
    .line 136068
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 136069
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 136070
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136071
    return-void
.end method

.method public final b(LX/0lc;)V
    .locals 2

    .prologue
    .line 136072
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136073
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12U;->a(Ljava/lang/String;)I

    .line 136074
    return-void
.end method

.method public final b(LX/15w;)V
    .locals 2

    .prologue
    .line 136075
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 136076
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_0

    .line 136077
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136078
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 136079
    :cond_0
    sget-object v1, LX/4ry;->a:[I

    invoke-virtual {v0}, LX/15z;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 136080
    :pswitch_0
    invoke-direct {p0, p1}, LX/0nW;->c(LX/15w;)V

    .line 136081
    :goto_0
    return-void

    .line 136082
    :pswitch_1
    invoke-virtual {p0}, LX/0nX;->d()V

    .line 136083
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_1

    .line 136084
    invoke-virtual {p0, p1}, LX/0nW;->b(LX/15w;)V

    goto :goto_1

    .line 136085
    :cond_1
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0

    .line 136086
    :pswitch_2
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 136087
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v1, :cond_2

    .line 136088
    invoke-virtual {p0, p1}, LX/0nW;->b(LX/15w;)V

    goto :goto_2

    .line 136089
    :cond_2
    invoke-virtual {p0}, LX/0nX;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136090
    if-nez p1, :cond_0

    .line 136091
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 136092
    :goto_0
    return-void

    .line 136093
    :cond_0
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b([CII)V
    .locals 0

    .prologue
    .line 136094
    invoke-static {}, LX/0nW;->k()V

    .line 136095
    return-void
.end method

.method public final c()LX/0nX;
    .locals 0

    .prologue
    .line 136096
    return-object p0
.end method

.method public final c(LX/0lc;)V
    .locals 1

    .prologue
    .line 136097
    if-nez p1, :cond_0

    .line 136098
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 136099
    :goto_0
    return-void

    .line 136100
    :cond_0
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136101
    invoke-static {}, LX/0nW;->k()V

    .line 136102
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 136103
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0nW;->e:Z

    .line 136104
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 136105
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136106
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    invoke-virtual {v0}, LX/12U;->j()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136107
    return-void
.end method

.method public final d(LX/0lc;)V
    .locals 0

    .prologue
    .line 136108
    invoke-static {}, LX/0nW;->k()V

    .line 136109
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136066
    invoke-static {}, LX/0nW;->k()V

    .line 136067
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 136110
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136111
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136112
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 136113
    if-eqz v0, :cond_0

    .line 136114
    iput-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136115
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136116
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    invoke-direct {p0, v0, p1}, LX/0nW;->a(LX/15z;Ljava/lang/Object;)V

    .line 136117
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 136118
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136119
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    invoke-virtual {v0}, LX/12U;->k()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136120
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 136121
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 136122
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136123
    iget-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136124
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 136125
    if-eqz v0, :cond_0

    .line 136126
    iput-object v0, p0, LX/0nW;->i:LX/12U;

    .line 136127
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 136128
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    invoke-direct {p0, v0}, LX/0nW;->a(LX/15z;)V

    .line 136129
    return-void
.end method

.method public final i()LX/15w;
    .locals 1

    .prologue
    .line 136130
    iget-object v0, p0, LX/0nW;->c:LX/0lD;

    invoke-direct {p0, v0}, LX/0nW;->b(LX/0lD;)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/15z;
    .locals 2

    .prologue
    .line 136131
    iget-object v0, p0, LX/0nW;->f:LX/4s0;

    if-eqz v0, :cond_0

    .line 136132
    iget-object v0, p0, LX/0nW;->f:LX/4s0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4s0;->a(I)LX/15z;

    move-result-object v0

    .line 136133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x64

    .line 136134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 136135
    const-string v0, "[TokenBuffer: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136136
    invoke-virtual {p0}, LX/0nW;->i()LX/15w;

    move-result-object v2

    .line 136137
    const/4 v0, 0x0

    .line 136138
    :goto_0
    :try_start_0
    invoke-virtual {v2}, LX/15w;->c()LX/15z;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 136139
    if-nez v3, :cond_1

    .line 136140
    if-lt v0, v5, :cond_0

    .line 136141
    const-string v2, " ... (truncated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v0, v0, -0x64

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " entries)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136142
    :cond_0
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136143
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 136144
    :cond_1
    if-ge v0, v5, :cond_3

    .line 136145
    if-lez v0, :cond_2

    .line 136146
    :try_start_1
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136147
    :cond_2
    invoke-virtual {v3}, LX/15z;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136148
    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v3, v4, :cond_3

    .line 136149
    const/16 v3, 0x28

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136150
    invoke-virtual {v2}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136151
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 136152
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 136153
    goto :goto_0

    .line 136154
    :catch_0
    move-exception v0

    .line 136155
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 136156
    sget-object v0, LX/4pz;->VERSION:LX/0ne;

    return-object v0
.end method
