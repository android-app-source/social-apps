.class public LX/13s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/13s;


# instance fields
.field private final a:LX/0Zb;

.field public final b:Landroid/view/WindowManager;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2G1;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/13t;

.field public final e:LX/0gh;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/view/WindowManager;LX/0Ot;LX/13t;LX/0gh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Landroid/view/WindowManager;",
            "LX/0Ot",
            "<",
            "LX/2G1;",
            ">;",
            "LX/13t;",
            "LX/0gh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177523
    iput-object p1, p0, LX/13s;->a:LX/0Zb;

    .line 177524
    iput-object p2, p0, LX/13s;->b:Landroid/view/WindowManager;

    .line 177525
    iput-object p3, p0, LX/13s;->c:LX/0Ot;

    .line 177526
    iput-object p4, p0, LX/13s;->d:LX/13t;

    .line 177527
    iput-object p5, p0, LX/13s;->e:LX/0gh;

    .line 177528
    return-void
.end method

.method public static a(LX/0QB;)LX/13s;
    .locals 9

    .prologue
    .line 177495
    sget-object v0, LX/13s;->f:LX/13s;

    if-nez v0, :cond_1

    .line 177496
    const-class v1, LX/13s;

    monitor-enter v1

    .line 177497
    :try_start_0
    sget-object v0, LX/13s;->f:LX/13s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177498
    if-eqz v2, :cond_0

    .line 177499
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177500
    new-instance v3, LX/13s;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    const/16 v6, 0xd1

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v7

    check-cast v7, LX/13t;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v8

    check-cast v8, LX/0gh;

    invoke-direct/range {v3 .. v8}, LX/13s;-><init>(LX/0Zb;Landroid/view/WindowManager;LX/0Ot;LX/13t;LX/0gh;)V

    .line 177501
    move-object v0, v3

    .line 177502
    sput-object v0, LX/13s;->f:LX/13s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177503
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177504
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177505
    :cond_1
    sget-object v0, LX/13s;->f:LX/13s;

    return-object v0

    .line 177506
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177507
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 177508
    iget-object v0, p0, LX/13s;->e:LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->b(Landroid/app/Activity;)V

    .line 177509
    instance-of v0, p1, LX/13v;

    if-nez v0, :cond_0

    .line 177510
    iget-object v0, p0, LX/13s;->e:LX/0gh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 177511
    :cond_0
    iget-object v0, p0, LX/13s;->d:LX/13t;

    invoke-virtual {v0, p1}, LX/13t;->a(Landroid/content/Context;)V

    .line 177512
    iget-object v0, p0, LX/13s;->d:LX/13t;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 177513
    iget-object v2, v0, LX/13t;->i:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_1

    iget-object v2, v0, LX/13t;->h:LX/0Yb;

    if-nez v2, :cond_1

    .line 177514
    new-instance v2, LX/0Yd;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    new-instance p1, LX/158;

    invoke-direct {p1, v0}, LX/158;-><init>(LX/13t;)V

    invoke-direct {v2, v3, p1}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v2, v0, LX/13t;->i:Landroid/content/BroadcastReceiver;

    .line 177515
    iget-object v2, v0, LX/13t;->i:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string p1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 177516
    iget-object v2, v0, LX/13t;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 177517
    new-instance v3, LX/15J;

    invoke-direct {v3, v0}, LX/15J;-><init>(LX/13t;)V

    .line 177518
    iget-object v2, v0, LX/13t;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    sget-object p1, LX/0oz;->a:Ljava/lang/String;

    invoke-interface {v2, p1, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, v0, LX/13t;->h:LX/0Yb;

    .line 177519
    iget-object v2, v0, LX/13t;->h:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 177520
    :cond_1
    iget-object v0, p0, LX/13s;->e:LX/0gh;

    iget-object v1, p0, LX/13s;->b:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(IZ)V

    .line 177521
    return-void
.end method
