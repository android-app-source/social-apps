.class public LX/1rc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/1rc;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332748
    const-class v0, LX/1rc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1rc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332745
    iput-object p1, p0, LX/1rc;->b:Landroid/content/Context;

    .line 332746
    iput-object p2, p0, LX/1rc;->c:LX/03V;

    .line 332747
    return-void
.end method

.method public static a(LX/0QB;)LX/1rc;
    .locals 5

    .prologue
    .line 332731
    sget-object v0, LX/1rc;->d:LX/1rc;

    if-nez v0, :cond_1

    .line 332732
    const-class v1, LX/1rc;

    monitor-enter v1

    .line 332733
    :try_start_0
    sget-object v0, LX/1rc;->d:LX/1rc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332734
    if-eqz v2, :cond_0

    .line 332735
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332736
    new-instance p0, LX/1rc;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/1rc;-><init>(Landroid/content/Context;LX/03V;)V

    .line 332737
    move-object v0, p0

    .line 332738
    sput-object v0, LX/1rc;->d:LX/1rc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332739
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332740
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332741
    :cond_1
    sget-object v0, LX/1rc;->d:LX/1rc;

    return-object v0

    .line 332742
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1rc;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 332730
    iget-object v0, p0, LX/1rc;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 332729
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 332704
    :try_start_0
    iget-object v0, p0, LX/1rc;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.wifi"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 332705
    :goto_0
    return v0

    .line 332706
    :catch_0
    move-exception v0

    .line 332707
    iget-object v1, p0, LX/1rc;->c:LX/03V;

    sget-object v2, LX/1rc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332708
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 332725
    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {p0, v0}, LX/1rc;->a(LX/1rc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332726
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {p0, v0}, LX/1rc;->a(LX/1rc;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v0}, LX/1rc;->a(LX/1rc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 332727
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 332728
    if-eqz v0, :cond_1

    const-string v0, "android.permission.CHANGE_WIFI_STATE"

    invoke-static {p0, v0}, LX/1rc;->a(LX/1rc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 332719
    :try_start_0
    iget-object v0, p0, LX/1rc;->b:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 332720
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 332721
    :goto_0
    return v0

    .line 332722
    :catch_0
    move-exception v0

    .line 332723
    iget-object v1, p0, LX/1rc;->c:LX/03V;

    sget-object v2, LX/1rc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332724
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/03R;
    .locals 3

    .prologue
    .line 332709
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 332710
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 332711
    :goto_0
    return-object v0

    .line 332712
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1rc;->b:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 332713
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 332714
    :goto_1
    move v0, v0

    .line 332715
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    goto :goto_0

    .line 332716
    :catch_0
    move-exception v0

    .line 332717
    iget-object v1, p0, LX/1rc;->c:LX/03V;

    sget-object v2, LX/1rc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332718
    const/4 v0, 0x0

    goto :goto_1
.end method
