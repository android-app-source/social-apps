.class public final LX/1XD;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1VH;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1XC;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 270712
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 270713
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "delegate"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "storyRenderContext"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1XD;->b:[Ljava/lang/String;

    .line 270714
    iput v3, p0, LX/1XD;->c:I

    .line 270715
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1XD;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1XD;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1XD;LX/1De;IILX/1XC;)V
    .locals 1

    .prologue
    .line 270705
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 270706
    iput-object p4, p0, LX/1XD;->a:LX/1XC;

    .line 270707
    iget-object v0, p0, LX/1XD;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 270708
    return-void
.end method


# virtual methods
.method public final a(LX/1EO;)LX/1XD;
    .locals 2

    .prologue
    .line 270702
    iget-object v0, p0, LX/1XD;->a:LX/1XC;

    iput-object p1, v0, LX/1XC;->c:LX/1EO;

    .line 270703
    iget-object v0, p0, LX/1XD;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 270704
    return-object p0
.end method

.method public final a(LX/1X1;)LX/1XD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/1XD;"
        }
    .end annotation

    .prologue
    .line 270709
    iget-object v0, p0, LX/1XD;->a:LX/1XC;

    iput-object p1, v0, LX/1XC;->a:LX/1X1;

    .line 270710
    iget-object v0, p0, LX/1XD;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 270711
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1XD;"
        }
    .end annotation

    .prologue
    .line 270685
    iget-object v0, p0, LX/1XD;->a:LX/1XC;

    iput-object p1, v0, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 270686
    iget-object v0, p0, LX/1XD;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 270687
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 270688
    invoke-super {p0}, LX/1X5;->a()V

    .line 270689
    const/4 v0, 0x0

    iput-object v0, p0, LX/1XD;->a:LX/1XC;

    .line 270690
    sget-object v0, LX/1VH;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 270691
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1VH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 270692
    iget-object v1, p0, LX/1XD;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1XD;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1XD;->c:I

    if-ge v1, v2, :cond_2

    .line 270693
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 270694
    :goto_0
    iget v2, p0, LX/1XD;->c:I

    if-ge v0, v2, :cond_1

    .line 270695
    iget-object v2, p0, LX/1XD;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270696
    iget-object v2, p0, LX/1XD;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270697
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270698
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270699
    :cond_2
    iget-object v0, p0, LX/1XD;->a:LX/1XC;

    .line 270700
    invoke-virtual {p0}, LX/1XD;->a()V

    .line 270701
    return-object v0
.end method
