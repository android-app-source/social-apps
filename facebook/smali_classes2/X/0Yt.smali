.class public final LX/0Yt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0VI;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0VI;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 82650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82651
    iput-object p1, p0, LX/0Yt;->a:LX/0QB;

    .line 82652
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 82653
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0Yt;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 82654
    packed-switch p2, :pswitch_data_0

    .line 82655
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82656
    :pswitch_0
    invoke-static {p1}, LX/0Yu;->a(LX/0QB;)LX/0Yu;

    move-result-object v0

    .line 82657
    :goto_0
    return-object v0

    .line 82658
    :pswitch_1
    invoke-static {p1}, LX/0Yv;->a(LX/0QB;)LX/0Yv;

    move-result-object v0

    goto :goto_0

    .line 82659
    :pswitch_2
    invoke-static {p1}, LX/0Yx;->a(LX/0QB;)LX/0Yx;

    move-result-object v0

    goto :goto_0

    .line 82660
    :pswitch_3
    new-instance v0, LX/0Yy;

    invoke-direct {v0}, LX/0Yy;-><init>()V

    .line 82661
    move-object v0, v0

    .line 82662
    move-object v0, v0

    .line 82663
    goto :goto_0

    .line 82664
    :pswitch_4
    new-instance v1, LX/0Yz;

    .line 82665
    new-instance v0, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    new-instance p2, LX/0Z0;

    invoke-direct {p2, p1}, LX/0Z0;-><init>(LX/0QB;)V

    invoke-direct {v0, p0, p2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object p0, v0

    .line 82666
    invoke-static {p1}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    invoke-direct {v1, p0, v0}, LX/0Yz;-><init>(Ljava/util/Set;Ljava/util/Random;)V

    .line 82667
    move-object v0, v1

    .line 82668
    goto :goto_0

    .line 82669
    :pswitch_5
    new-instance v0, LX/0Z7;

    const/16 v1, 0x3d6

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Z7;-><init>(LX/0Ot;)V

    .line 82670
    move-object v0, v0

    .line 82671
    goto :goto_0

    .line 82672
    :pswitch_6
    new-instance v1, LX/0Z8;

    invoke-static {p1}, LX/0Z9;->b(LX/0QB;)LX/0Z9;

    move-result-object v0

    check-cast v0, LX/0Z9;

    invoke-direct {v1, v0}, LX/0Z8;-><init>(LX/0Z9;)V

    .line 82673
    move-object v0, v1

    .line 82674
    goto :goto_0

    .line 82675
    :pswitch_7
    invoke-static {p1}, LX/0ZB;->a(LX/0QB;)LX/0ZB;

    move-result-object v0

    goto :goto_0

    .line 82676
    :pswitch_8
    invoke-static {p1}, LX/0ZC;->a(LX/0QB;)LX/0ZC;

    move-result-object v0

    goto :goto_0

    .line 82677
    :pswitch_9
    new-instance v0, LX/0ZF;

    const/16 v1, 0xdf4

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ZF;-><init>(LX/0Ot;)V

    .line 82678
    move-object v0, v0

    .line 82679
    goto :goto_0

    .line 82680
    :pswitch_a
    new-instance v0, LX/0ZG;

    const/16 v1, 0xca0

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ZG;-><init>(LX/0Ot;)V

    .line 82681
    move-object v0, v0

    .line 82682
    goto :goto_0

    .line 82683
    :pswitch_b
    new-instance v1, LX/0ZH;

    invoke-static {p1}, LX/0ZJ;->a(LX/0QB;)LX/0ZJ;

    move-result-object v0

    check-cast v0, LX/0ZJ;

    invoke-direct {v1, v0}, LX/0ZH;-><init>(LX/0ZJ;)V

    .line 82684
    move-object v0, v1

    .line 82685
    goto :goto_0

    .line 82686
    :pswitch_c
    new-instance v0, LX/0ZM;

    const/16 v1, 0x1010

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ZM;-><init>(LX/0Ot;)V

    .line 82687
    move-object v0, v0

    .line 82688
    goto/16 :goto_0

    .line 82689
    :pswitch_d
    new-instance v0, LX/0ZN;

    invoke-direct {v0}, LX/0ZN;-><init>()V

    .line 82690
    move-object v0, v0

    .line 82691
    move-object v0, v0

    .line 82692
    goto/16 :goto_0

    .line 82693
    :pswitch_e
    new-instance v0, LX/0ZO;

    const/16 v1, 0x101f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ZO;-><init>(LX/0Ot;)V

    .line 82694
    move-object v0, v0

    .line 82695
    goto/16 :goto_0

    .line 82696
    :pswitch_f
    new-instance v1, LX/0ZP;

    invoke-static {p1}, LX/0ZR;->a(LX/0QB;)LX/0ZR;

    move-result-object v0

    check-cast v0, LX/0ZR;

    invoke-direct {v1, v0}, LX/0ZP;-><init>(LX/0ZR;)V

    .line 82697
    move-object v0, v1

    .line 82698
    goto/16 :goto_0

    .line 82699
    :pswitch_10
    new-instance v1, LX/0ZS;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/0ZS;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 82700
    move-object v0, v1

    .line 82701
    goto/16 :goto_0

    .line 82702
    :pswitch_11
    invoke-static {p1}, LX/0Zc;->a(LX/0QB;)LX/0Zc;

    move-result-object v0

    goto/16 :goto_0

    .line 82703
    :pswitch_12
    new-instance v0, LX/0Zf;

    const/16 v1, 0x132a

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x132e

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/0Zf;-><init>(LX/0Ot;LX/0Ot;)V

    .line 82704
    move-object v0, v0

    .line 82705
    goto/16 :goto_0

    .line 82706
    :pswitch_13
    new-instance v0, LX/0Zg;

    const/16 v1, 0x133a

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Zg;-><init>(LX/0Ot;)V

    .line 82707
    move-object v0, v0

    .line 82708
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 82709
    const/16 v0, 0x14

    return v0
.end method
