.class public LX/0aF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0aG;


# instance fields
.field private final mDefaultBlueServiceOperationProvider:LX/0aH;

.field private final mViewerContextManager:LX/0SI;


# direct methods
.method public constructor <init>(LX/0SI;LX/0aH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84432
    iput-object p1, p0, LX/0aF;->mViewerContextManager:LX/0SI;

    .line 84433
    iput-object p2, p0, LX/0aF;->mDefaultBlueServiceOperationProvider:LX/0aH;

    .line 84434
    return-void
.end method

.method public static createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;
    .locals 3

    .prologue
    .line 84429
    new-instance v2, LX/0aF;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    const-class v1, LX/0aH;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/0aH;

    invoke-direct {v2, v0, v1}, LX/0aF;-><init>(LX/0SI;LX/0aH;)V

    .line 84430
    return-object v2
.end method

.method public static getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;
    .locals 1

    .prologue
    .line 84428
    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic newInstance(Ljava/lang/String;Landroid/os/Bundle;)LX/1MF;
    .locals 2

    .prologue
    .line 84426
    sget-object v0, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0aF;->newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MG;

    move-result-object v0

    move-object v0, v0

    .line 84427
    return-object v0
.end method

.method public bridge synthetic newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;)LX/1MF;
    .locals 1

    .prologue
    .line 84417
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/0aF;->newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MG;

    move-result-object v0

    move-object v0, v0

    .line 84418
    return-object v0
.end method

.method public bridge synthetic newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MF;
    .locals 1
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84425
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0aF;->newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MG;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newInstance(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)LX/1MF;
    .locals 1
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84423
    sget-object v0, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0, p1, p2, v0, p3}, LX/0aF;->newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MG;

    move-result-object v0

    move-object v0, v0

    .line 84424
    return-object v0
.end method

.method public newInstance(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MG;
    .locals 6
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84419
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84420
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84421
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84422
    iget-object v0, p0, LX/0aF;->mDefaultBlueServiceOperationProvider:LX/0aH;

    iget-object v5, p0, LX/0aF;->mViewerContextManager:LX/0SI;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/0aH;->get(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;LX/0SI;)LX/1MG;

    move-result-object v0

    return-object v0
.end method
