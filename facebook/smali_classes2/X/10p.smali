.class public abstract LX/10p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/10b;

.field public b:LX/10e;

.field public c:LX/10e;

.field public d:Z


# direct methods
.method public constructor <init>(LX/10b;)V
    .locals 2

    .prologue
    .line 169438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169439
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/10p;->d:Z

    .line 169440
    iput-object p1, p0, LX/10p;->a:LX/10b;

    .line 169441
    sget-object v0, LX/10q;->a:[I

    invoke-virtual {p1}, LX/10b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169442
    :goto_0
    return-void

    .line 169443
    :pswitch_0
    sget-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    iput-object v0, p0, LX/10p;->b:LX/10e;

    .line 169444
    sget-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    iput-object v0, p0, LX/10p;->c:LX/10e;

    goto :goto_0

    .line 169445
    :pswitch_1
    sget-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    iput-object v0, p0, LX/10p;->b:LX/10e;

    .line 169446
    sget-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    iput-object v0, p0, LX/10p;->c:LX/10e;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/10e;)LX/A7U;
    .locals 1

    .prologue
    .line 169447
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    if-ne p0, v0, :cond_0

    .line 169448
    sget-object v0, LX/A7U;->CLOSED:LX/A7U;

    .line 169449
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/A7U;->OPENED:LX/A7U;

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/A7U;)V
.end method

.method public abstract b(LX/A7U;)V
.end method

.method public abstract c(LX/A7U;)V
.end method
