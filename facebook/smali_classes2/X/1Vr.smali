.class public LX/1Vr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/17T;

.field private final c:LX/1Vq;

.field private final d:LX/0s6;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17T;LX/0s6;LX/1Vq;)V
    .locals 0
    .param p4    # LX/1Vq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266509
    iput-object p1, p0, LX/1Vr;->a:Lcom/facebook/content/SecureContextHelper;

    .line 266510
    iput-object p2, p0, LX/1Vr;->b:LX/17T;

    .line 266511
    iput-object p4, p0, LX/1Vr;->c:LX/1Vq;

    .line 266512
    iput-object p3, p0, LX/1Vr;->d:LX/0s6;

    .line 266513
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 266527
    iget-object v0, p0, LX/1Vr;->b:LX/17T;

    iget-object v1, p0, LX/1Vr;->c:LX/1Vq;

    invoke-interface {v1}, LX/1Vq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 266528
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 266521
    iget-object v0, p0, LX/1Vr;->b:LX/17T;

    invoke-virtual {v0, p1}, LX/17T;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 266522
    if-nez v0, :cond_0

    .line 266523
    invoke-virtual {p0, p1, p2}, LX/1Vr;->b(Ljava/lang/String;Landroid/content/Context;)V

    .line 266524
    :goto_0
    return-void

    .line 266525
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 266526
    iget-object v1, p0, LX/1Vr;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 266519
    iget-object v1, p0, LX/1Vr;->d:LX/0s6;

    iget-object v2, p0, LX/1Vr;->c:LX/1Vq;

    invoke-interface {v2}, LX/1Vq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 266520
    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 266514
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266515
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 266516
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 266517
    iget-object v1, p0, LX/1Vr;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 266518
    return-void
.end method
