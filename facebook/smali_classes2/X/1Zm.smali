.class public LX/1Zm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/util/UUID;

.field public static final c:Ljava/util/UUID;

.field private static volatile p:LX/1Zm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/244;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/Context;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/246;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Zl;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/bluetooth/BluetoothAdapter;

.field private j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/net/nsd/NsdManager;

.field private m:Landroid/net/nsd/NsdManager$DiscoveryListener;

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275425
    const-class v0, LX/1Zm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Zm;->a:Ljava/lang/String;

    .line 275426
    const-string v0, "3e96cbfe-1bde-4cf9-aafc-a0dbc4efa17a"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, LX/1Zm;->b:Ljava/util/UUID;

    .line 275427
    const-string v0, "f3f3df09-7292-4fe9-916a-b9d288827011"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, LX/1Zm;->c:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/244;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 275411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275412
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Zm;->g:Ljava/util/Map;

    .line 275413
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Zm;->h:Ljava/util/Set;

    .line 275414
    iput-object v1, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    .line 275415
    iput-object v1, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 275416
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Zm;->k:Ljava/util/Set;

    .line 275417
    iput-object v1, p0, LX/1Zm;->l:Landroid/net/nsd/NsdManager;

    .line 275418
    iput-object v1, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    .line 275419
    iput-boolean v2, p0, LX/1Zm;->n:Z

    .line 275420
    iput-boolean v2, p0, LX/1Zm;->o:Z

    .line 275421
    iput-object p2, p0, LX/1Zm;->d:LX/0Ot;

    .line 275422
    iput-object p3, p0, LX/1Zm;->e:LX/0Ot;

    .line 275423
    iput-object p1, p0, LX/1Zm;->f:Landroid/content/Context;

    .line 275424
    return-void
.end method

.method public static a(LX/0QB;)LX/1Zm;
    .locals 6

    .prologue
    .line 275398
    sget-object v0, LX/1Zm;->p:LX/1Zm;

    if-nez v0, :cond_1

    .line 275399
    const-class v1, LX/1Zm;

    monitor-enter v1

    .line 275400
    :try_start_0
    sget-object v0, LX/1Zm;->p:LX/1Zm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 275401
    if-eqz v2, :cond_0

    .line 275402
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 275403
    new-instance v4, LX/1Zm;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0x4da

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xac0

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/1Zm;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V

    .line 275404
    move-object v0, v4

    .line 275405
    sput-object v0, LX/1Zm;->p:LX/1Zm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 275407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 275408
    :cond_1
    sget-object v0, LX/1Zm;->p:LX/1Zm;

    return-object v0

    .line 275409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 275410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/1Zn;)V
    .locals 6

    .prologue
    .line 275384
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1Zm;->f:Landroid/content/Context;

    iget-object v0, p0, LX/1Zm;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 275385
    sget-object v2, LX/1Zo;->a:[I

    invoke-virtual {p1}, LX/1Zn;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 275386
    :cond_0
    :goto_0
    move v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275387
    if-nez v0, :cond_1

    .line 275388
    :goto_1
    monitor-exit p0

    return-void

    .line 275389
    :cond_1
    :try_start_1
    sget-object v0, LX/1Zo;->a:[I

    invoke-virtual {p1}, LX/1Zn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 275390
    :pswitch_0
    invoke-direct {p0}, LX/1Zm;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 275391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 275392
    :pswitch_1
    :try_start_2
    invoke-direct {p0}, LX/1Zm;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 275393
    :pswitch_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v2, v5, :cond_2

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v5, "android.hardware.bluetooth_le"

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 275394
    :goto_2
    const/16 v5, 0x324

    invoke-virtual {v0, v5, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 275395
    if-eqz v2, :cond_0

    if-eqz v5, :cond_0

    move v4, v3

    goto :goto_0

    :cond_2
    move v2, v4

    .line 275396
    goto :goto_2

    .line 275397
    :pswitch_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v2, v5, :cond_0

    move v4, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(LX/1Zm;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 275364
    iget-object v0, p0, LX/1Zm;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/244;

    new-instance v1, LX/245;

    invoke-direct {v1, p0, p1}, LX/245;-><init>(LX/1Zm;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 275365
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275366
    :cond_0
    :goto_0
    return-void

    .line 275367
    :cond_1
    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 275368
    array-length v3, v2

    const/4 v4, 0x3

    if-lt v3, v4, :cond_2

    const/4 v3, 0x0

    aget-object v3, v2, v3

    const-string v4, "fbsdk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 275369
    :cond_2
    invoke-virtual {v1, p0}, LX/245;->a(LX/246;)V

    goto :goto_0

    .line 275370
    :cond_3
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    .line 275371
    if-nez v2, :cond_4

    .line 275372
    invoke-virtual {v1, p0}, LX/245;->a(LX/246;)V

    goto :goto_0

    .line 275373
    :cond_4
    sget-object v3, LX/246;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 275374
    invoke-virtual {v1, p0}, LX/245;->a(LX/246;)V

    goto :goto_0

    .line 275375
    :cond_5
    new-instance v3, LX/247;

    invoke-direct {v3}, LX/247;-><init>()V

    move-object v3, v3

    .line 275376
    const-string v4, "shortCode"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 275377
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 275378
    iget-object v4, v3, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 275379
    const/4 p0, 0x1

    .line 275380
    iput-boolean p0, v4, LX/0gW;->l:Z

    .line 275381
    iget-object v4, v0, LX/244;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 275382
    new-instance v4, LX/248;

    invoke-direct {v4, v0}, LX/248;-><init>(LX/244;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 275383
    new-instance v3, LX/249;

    invoke-direct {v3, v1}, LX/249;-><init>(LX/245;)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public static declared-synchronized b(LX/1Zm;LX/1Zn;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation

    .prologue
    .line 275354
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/1Zo;->a:[I

    invoke-virtual {p1}, LX/1Zn;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 275355
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 275356
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    if-eqz v0, :cond_0

    .line 275357
    iget-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 275358
    invoke-direct {p0}, LX/1Zm;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 275359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 275360
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/1Zm;->o:Z

    .line 275361
    iget-boolean v0, p0, LX/1Zm;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/1Zm;->n:Z

    if-eqz v0, :cond_0

    .line 275362
    iget-object v0, p0, LX/1Zm;->l:Landroid/net/nsd/NsdManager;

    iget-object v1, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    invoke-virtual {v0, v1}, Landroid/net/nsd/NsdManager;->stopServiceDiscovery(Landroid/net/nsd/NsdManager$DiscoveryListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 275363
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized c()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation

    .prologue
    .line 275344
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    if-nez v0, :cond_0

    .line 275345
    new-instance v0, LX/DAv;

    invoke-direct {v0, p0}, LX/DAv;-><init>(LX/1Zm;)V

    iput-object v0, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 275346
    iget-object v0, p0, LX/1Zm;->f:Landroid/content/Context;

    const-string v1, "bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 275347
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    .line 275348
    iget-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 275349
    iget-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275350
    iget-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/util/UUID;

    const/4 v2, 0x0

    sget-object v3, LX/1Zm;->b:Ljava/util/UUID;

    aput-object v3, v1, v2

    iget-object v2, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->startLeScan([Ljava/util/UUID;Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275351
    :cond_0
    monitor-exit p0

    return-void

    .line 275352
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 275353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 275338
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    if-nez v0, :cond_0

    .line 275339
    new-instance v0, LX/1Zq;

    invoke-direct {v0, p0}, LX/1Zq;-><init>(LX/1Zm;)V

    iput-object v0, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    .line 275340
    iget-object v0, p0, LX/1Zm;->f:Landroid/content/Context;

    const-string v1, "servicediscovery"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/nsd/NsdManager;

    iput-object v0, p0, LX/1Zm;->l:Landroid/net/nsd/NsdManager;

    .line 275341
    iget-object v0, p0, LX/1Zm;->l:Landroid/net/nsd/NsdManager;

    const-string v1, "_fb._tcp."

    const/4 v2, 0x1

    iget-object v3, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/nsd/NsdManager;->discoverServices(Ljava/lang/String;ILandroid/net/nsd/NsdManager$DiscoveryListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275342
    :cond_0
    monitor-exit p0

    return-void

    .line 275343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e$redex0(LX/1Zm;)V
    .locals 1

    .prologue
    .line 275334
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/1Zm;->m:Landroid/net/nsd/NsdManager$DiscoveryListener;

    .line 275335
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Zm;->l:Landroid/net/nsd/NsdManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275336
    monitor-exit p0

    return-void

    .line 275337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 1

    .prologue
    .line 275329
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/1Zm;->j:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 275330
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Zm;->i:Landroid/bluetooth/BluetoothAdapter;

    .line 275331
    iget-object v0, p0, LX/1Zm;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275332
    monitor-exit p0

    return-void

    .line 275333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/1Zm;)V
    .locals 3

    .prologue
    .line 275308
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Zm;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zl;

    .line 275309
    iget-object v2, p0, LX/1Zm;->g:Ljava/util/Map;

    invoke-interface {v0, v2}, LX/1Zl;->a(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 275310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 275311
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1Zl;)Z
    .locals 3

    .prologue
    .line 275320
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Zm;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 275321
    if-eqz v0, :cond_0

    .line 275322
    iget-object v1, p0, LX/1Zm;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 275323
    sget-object v1, LX/1Zn;->NSD:LX/1Zn;

    invoke-direct {p0, v1}, LX/1Zm;->a(LX/1Zn;)V

    .line 275324
    sget-object v1, LX/1Zn;->BLE:LX/1Zn;

    invoke-direct {p0, v1}, LX/1Zm;->a(LX/1Zn;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275325
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 275326
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/1Zm;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 275327
    iget-object v1, p0, LX/1Zm;->g:Ljava/util/Map;

    invoke-interface {p1, v1}, LX/1Zl;->a(Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 275328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1Zl;)Z
    .locals 2

    .prologue
    .line 275312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Zm;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 275313
    if-eqz v0, :cond_0

    .line 275314
    iget-object v1, p0, LX/1Zm;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275315
    sget-object v1, LX/1Zn;->NSD:LX/1Zn;

    invoke-static {p0, v1}, LX/1Zm;->b(LX/1Zm;LX/1Zn;)V

    .line 275316
    sget-object v1, LX/1Zn;->BLE:LX/1Zn;

    invoke-static {p0, v1}, LX/1Zm;->b(LX/1Zm;LX/1Zn;)V

    .line 275317
    iget-object v1, p0, LX/1Zm;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275318
    :cond_0
    monitor-exit p0

    return v0

    .line 275319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
