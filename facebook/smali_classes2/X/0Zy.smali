.class public LX/0Zy;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84243
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 84244
    return-void
.end method

.method public static a(Landroid/os/Handler;)LX/0Tf;
    .locals 1
    .param p0    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 84245
    new-instance v0, LX/0Td;

    invoke-direct {v0, p0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method public static a(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 84246
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method public static a(LX/0Zr;)Landroid/os/Looper;
    .locals 2
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 84247
    const-string v0, "BackgroundBroadcastHandler"

    sget-object v1, LX/0TP;->NORMAL:LX/0TP;

    invoke-virtual {p0, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 84248
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 84249
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 84250
    return-void
.end method
