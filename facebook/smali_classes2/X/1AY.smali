.class public LX/1AY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public final b:LX/0gp;


# direct methods
.method public constructor <init>(LX/0gp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 210595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210596
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    .line 210597
    iput-object p1, p0, LX/1AY;->b:LX/0gp;

    .line 210598
    return-void
.end method

.method public static a(LX/0QB;)LX/1AY;
    .locals 1

    .prologue
    .line 210594
    invoke-static {p0}, LX/1AY;->b(LX/0QB;)LX/1AY;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7zf;)Z
    .locals 1

    .prologue
    .line 210593
    sget-object v0, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/7zf;->VISIBLE:LX/7zf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 210583
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_1

    .line 210584
    :cond_0
    :goto_0
    return v0

    .line 210585
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 210586
    :goto_1
    if-eqz v1, :cond_4

    .line 210587
    instance-of v2, v1, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_2

    instance-of v2, v1, Landroid/widget/AdapterView;

    if-nez v2, :cond_2

    instance-of v2, v1, LX/3nq;

    if-eqz v2, :cond_3

    .line 210588
    :cond_2
    const/4 v1, 0x1

    .line 210589
    :goto_2
    move v1, v1

    .line 210590
    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 210591
    :cond_3
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 210592
    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/1AY;
    .locals 2

    .prologue
    .line 210581
    new-instance v1, LX/1AY;

    invoke-static {p0}, LX/0gp;->a(LX/0QB;)LX/0gp;

    move-result-object v0

    check-cast v0, LX/0gp;

    invoke-direct {v1, v0}, LX/1AY;-><init>(LX/0gp;)V

    .line 210582
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/7zf;
    .locals 1

    .prologue
    .line 210542
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1AY;->a(Landroid/view/View;Z)LX/7zf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Z)LX/7zf;
    .locals 3

    .prologue
    .line 210544
    invoke-virtual {p0, p1}, LX/1AY;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210545
    sget-object v0, LX/7zf;->OFFSCREEN:LX/7zf;

    .line 210546
    :goto_0
    return-object v0

    .line 210547
    :cond_0
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 210548
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 210549
    iget-object v1, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 210550
    iget-object v1, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 210551
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v0, v0

    .line 210552
    const v1, 0x3f666666    # 0.9f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 210553
    sget-object v0, LX/7zf;->NOT_VISIBLE:LX/7zf;

    goto :goto_0

    .line 210554
    :cond_1
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 210555
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 210556
    if-eqz p2, :cond_3

    .line 210557
    iget-object v1, p0, LX/1AY;->b:LX/0gp;

    .line 210558
    invoke-virtual {v1}, LX/0gp;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/0gp;->d:LX/0iW;

    if-nez v2, :cond_6

    .line 210559
    :cond_2
    const/4 v2, 0x0

    .line 210560
    :goto_1
    move v1, v2

    .line 210561
    iget-object v2, p0, LX/1AY;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    .line 210562
    if-lez v1, :cond_3

    .line 210563
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sub-int/2addr v0, v1

    .line 210564
    :cond_3
    iget-object v1, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 210565
    iget-object v1, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 210566
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v0, v0

    .line 210567
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 210568
    sget-object v0, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    goto :goto_0

    .line 210569
    :cond_4
    const v1, 0x3ee66666    # 0.45f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 210570
    sget-object v0, LX/7zf;->NOT_VISIBLE:LX/7zf;

    goto :goto_0

    .line 210571
    :cond_5
    sget-object v0, LX/7zf;->VISIBLE:LX/7zf;

    goto :goto_0

    .line 210572
    :cond_6
    iget-boolean v2, v1, LX/0gp;->h:Z

    if-eqz v2, :cond_7

    .line 210573
    iget-object v2, v1, LX/0gp;->d:LX/0iW;

    .line 210574
    iget p2, v2, LX/0iW;->g:I

    move v2, p2

    .line 210575
    goto :goto_1

    .line 210576
    :cond_7
    iget-object v2, v1, LX/0gp;->d:LX/0iW;

    .line 210577
    iget p2, v2, LX/0iW;->g:I

    move v2, p2

    .line 210578
    iget-object p2, v1, LX/0gp;->d:LX/0iW;

    .line 210579
    iget v1, p2, LX/0iW;->h:I

    if-lez v1, :cond_8

    iget v1, p2, LX/0iW;->h:I

    :goto_2
    move p2, v1

    .line 210580
    sub-int/2addr v2, p2

    goto :goto_1

    :cond_8
    iget v1, p2, LX/0iW;->g:I

    goto :goto_2
.end method

.method public final b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 210543
    iget-object v0, p0, LX/1AY;->a:Landroid/graphics/Rect;

    invoke-static {p1, v0}, LX/1AY;->a(Landroid/view/View;Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method
