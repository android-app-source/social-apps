.class public LX/0yQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0yQ;


# instance fields
.field private final a:LX/0Uh;

.field private b:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164738
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yQ;->b:Z

    .line 164739
    iput-object p1, p0, LX/0yQ;->a:LX/0Uh;

    .line 164740
    return-void
.end method

.method public static a(LX/0QB;)LX/0yQ;
    .locals 4

    .prologue
    .line 164741
    sget-object v0, LX/0yQ;->c:LX/0yQ;

    if-nez v0, :cond_1

    .line 164742
    const-class v1, LX/0yQ;

    monitor-enter v1

    .line 164743
    :try_start_0
    sget-object v0, LX/0yQ;->c:LX/0yQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164744
    if-eqz v2, :cond_0

    .line 164745
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164746
    new-instance p0, LX/0yQ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/0yQ;-><init>(LX/0Uh;)V

    .line 164747
    move-object v0, p0

    .line 164748
    sput-object v0, LX/0yQ;->c:LX/0yQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164749
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164750
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164751
    :cond_1
    sget-object v0, LX/0yQ;->c:LX/0yQ;

    return-object v0

    .line 164752
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 164754
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0yQ;->b:Z

    .line 164755
    return-void
.end method

.method public final declared-synchronized a(Lorg/apache/http/HttpRequest;)V
    .locals 3

    .prologue
    .line 164756
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yQ;->a:LX/0Uh;

    const/16 v1, 0x683

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164757
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 164758
    iget-boolean v1, p0, LX/0yQ;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "^https?://(b-)?((api|api2|z-m-api|b-api|api-read)|(graph|graph2|z-m-graph|b-graph))\\.([0-9a-zA-Z\\.-]*)?facebook\\.com.*$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "method/mobile.zeroCampaign"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164759
    const-string v0, "X-ZERO-STATE"

    const-string v1, "unknown"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164760
    :cond_0
    monitor-exit p0

    return-void

    .line 164761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
