.class public LX/1dt;
.super LX/1SX;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final C:LX/0Sh;

.field public final D:LX/17Q;

.field public final E:LX/0Zb;

.field public final F:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final G:LX/14w;

.field public final H:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final I:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field public final L:LX/1eA;

.field public final M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Q1;",
            ">;"
        }
    .end annotation
.end field

.field private final N:LX/1Sp;

.field private final f:LX/1Kf;

.field private final g:LX/13l;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1du;

.field private j:LX/1e9;

.field public final k:LX/03V;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/1JC;

.field private final n:LX/1VF;

.field public final o:LX/1dz;

.field public final p:LX/1e0;

.field public final q:LX/0Uh;

.field public final r:LX/1e4;

.field public final s:LX/0ad;

.field public final t:LX/1e5;

.field public final u:LX/1e7;

.field public final v:LX/1e6;

.field public final w:LX/1e8;

.field private final x:Ljava/lang/String;

.field public final y:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public final z:LX/0bH;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 286778
    const-class v0, LX/1dt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1dt;->a:Ljava/lang/String;

    .line 286779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const/4 v6, 0x7

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const/4 v7, 0x0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_RESEARCH_POLLS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC_MISCLASSIFICATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1dt;->b:LX/0Rf;

    .line 286780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1dt;->c:LX/0Rf;

    .line 286781
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1dt;->d:LX/0Rf;

    .line 286782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1dt;->e:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Sh;LX/0bH;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0Or;LX/0SG;LX/13l;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/1du;LX/03V;LX/0ad;LX/0Or;LX/0Ot;LX/1JC;LX/1VF;LX/1Pf;LX/1dz;LX/1e0;LX/0Uh;LX/0qn;LX/1e4;LX/1e5;LX/1e6;LX/0W3;LX/0Ot;LX/1e7;LX/1e8;Ljava/lang/String;LX/1Sl;LX/0tX;LX/1Sm;LX/1e9;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/1eA;LX/0Ot;LX/0wL;)V
    .locals 42
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedPrivacyEditingEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsGroupCommerceNewDeleteInterceptEnabled;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p25    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p35    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p47    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6G2;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/13l;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/1du;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/1JC;",
            "LX/1VF;",
            "LX/1Pf;",
            "LX/1dz;",
            "LX/1e0;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0qn;",
            "Lcom/facebook/privacy/ui/PrivacyScopeResourceResolver;",
            "LX/1e5;",
            "LX/1e6;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/1e7;",
            "LX/1e8;",
            "Ljava/lang/String;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/1e9;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/1eA;",
            "LX/0Ot",
            "<",
            "LX/6Q1;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 286783
    move-object/from16 v2, p0

    move-object/from16 v3, p15

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p19

    move-object/from16 v9, p20

    move-object/from16 v10, p21

    move-object/from16 v11, p22

    move-object/from16 v12, p23

    move-object/from16 v13, p8

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move-object/from16 v18, p7

    move-object/from16 v19, p12

    move-object/from16 v20, p13

    move-object/from16 v21, p14

    move-object/from16 v22, p16

    move-object/from16 v23, p17

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p30

    move-object/from16 v29, p31

    move-object/from16 v30, p35

    move-object/from16 v31, p39

    move-object/from16 v32, p43

    move-object/from16 v33, p44

    move-object/from16 v34, p55

    move-object/from16 v35, p48

    move-object/from16 v36, p49

    move-object/from16 v37, p50

    move-object/from16 v38, p52

    move-object/from16 v39, p53

    move-object/from16 v40, p54

    move-object/from16 v41, p60

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 286784
    new-instance v2, LX/1eB;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/1eB;-><init>(LX/1dt;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/1dt;->N:LX/1Sp;

    .line 286785
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->I:LX/0Or;

    .line 286786
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->H:LX/0Or;

    .line 286787
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->z:LX/0bH;

    .line 286788
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->A:LX/0Or;

    .line 286789
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->B:LX/0Or;

    .line 286790
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->C:LX/0Sh;

    .line 286791
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->D:LX/17Q;

    .line 286792
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->E:LX/0Zb;

    .line 286793
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->F:LX/0Or;

    .line 286794
    move-object/from16 v0, p27

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->G:LX/14w;

    .line 286795
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->f:LX/1Kf;

    .line 286796
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->g:LX/13l;

    .line 286797
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->h:LX/0Or;

    .line 286798
    move-object/from16 v0, p28

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->i:LX/1du;

    .line 286799
    move-object/from16 v0, p51

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->j:LX/1e9;

    .line 286800
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Z)V

    .line 286801
    move-object/from16 v0, p29

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->k:LX/03V;

    .line 286802
    move-object/from16 v0, p32

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->l:LX/0Ot;

    .line 286803
    move-object/from16 v0, p33

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->m:LX/1JC;

    .line 286804
    move-object/from16 v0, p34

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->n:LX/1VF;

    .line 286805
    move-object/from16 v0, p36

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->o:LX/1dz;

    .line 286806
    move-object/from16 v0, p37

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->p:LX/1e0;

    .line 286807
    move-object/from16 v0, p38

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->q:LX/0Uh;

    .line 286808
    move-object/from16 v0, p40

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->r:LX/1e4;

    .line 286809
    move-object/from16 v0, p30

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->s:LX/0ad;

    .line 286810
    move-object/from16 v0, p41

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->t:LX/1e5;

    .line 286811
    move-object/from16 v0, p45

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->u:LX/1e7;

    .line 286812
    move-object/from16 v0, p42

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->v:LX/1e6;

    .line 286813
    move-object/from16 v0, p46

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->w:LX/1e8;

    .line 286814
    move-object/from16 v0, p47

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->x:Ljava/lang/String;

    .line 286815
    move-object/from16 v0, p55

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->y:LX/0Or;

    .line 286816
    move-object/from16 v0, p56

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->J:LX/0Ot;

    .line 286817
    move-object/from16 v0, p57

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->K:LX/0Ot;

    .line 286818
    move-object/from16 v0, p58

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->L:LX/1eA;

    .line 286819
    move-object/from16 v0, p59

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dt;->M:LX/0Ot;

    .line 286820
    const-string v2, "native_newsfeed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 286821
    const-string v2, "native_story"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 286822
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1dt;->N:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 286823
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 286824
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 286825
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286826
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    .line 286827
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLName;)Ljava/lang/String;

    move-result-object v0

    .line 286828
    :goto_0
    const v1, 0x7f08106e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 286829
    invoke-interface {p2, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v6

    .line 286830
    instance-of v0, v6, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 286831
    check-cast v0, LX/3Ai;

    .line 286832
    sget-object v1, LX/Bo5;->a:[I

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLActor;->F()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 286833
    const v1, 0x7f081071

    :goto_1
    move v1, v1

    .line 286834
    invoke-virtual {v0, v1}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 286835
    :cond_0
    new-instance v0, LX/BoB;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/BoB;-><init>(LX/1dt;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286836
    const v0, 0x7f0219e3

    move v0, v0

    .line 286837
    invoke-virtual {p0, v6, v0, v5}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 286838
    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, p3, v0, p4, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 286839
    return-void

    .line 286840
    :cond_1
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286841
    :pswitch_0
    const v1, 0x7f08106f

    goto :goto_1

    .line 286842
    :pswitch_1
    const v1, 0x7f081070

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Landroid/view/Menu;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLExploreFeed;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/Menu;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLExploreFeed;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 286843
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v0

    .line 286844
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286845
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 286846
    const v0, 0x7f081072

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 286847
    invoke-interface {p2, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    .line 286848
    instance-of v0, v7, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v7

    .line 286849
    check-cast v0, LX/3Ai;

    const v1, 0x7f081073

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 286850
    :cond_0
    new-instance v0, LX/BoD;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, LX/BoD;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLExploreFeed;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286851
    const v0, 0x7f0208cf

    move v0, v0

    .line 286852
    invoke-virtual {p0, v7, v0, v6}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 286853
    invoke-interface {v7}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, p4, v0, p3, v8}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 286854
    return-void
.end method

.method public static a$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286855
    sget-object v0, LX/Al1;->UN_SEE_FIRST:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v4

    .line 286856
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286857
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_1

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 286858
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->K()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 286859
    invoke-direct/range {v0 .. v5}, LX/1dt;->a(Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 286860
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 286861
    :cond_1
    return-void
.end method

.method public static b$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286862
    sget-object v0, LX/Al1;->HIDE_TOPIC_FROM_USER:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 286863
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286864
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;->a()LX/0Px;

    move-result-object v7

    .line 286865
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 286866
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/1dt;->a(Landroid/content/Context;Landroid/view/Menu;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLExploreFeed;)V

    .line 286867
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 286868
    :cond_0
    return-void
.end method

.method public static d(LX/1dt;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286869
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286870
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286871
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 286872
    invoke-static {v0}, LX/1dt;->l(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 286873
    invoke-static {p2}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 286874
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    .line 286875
    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    .line 286876
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286877
    const-string v2, "xout_menu_opened"

    .line 286878
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 286879
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 286880
    move-object v2, v2

    .line 286881
    move-object v2, v2

    .line 286882
    iget-object v3, p0, LX/1dt;->E:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 286883
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286884
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 286885
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 286886
    const v2, 0x7f08111e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 286887
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 286888
    sget-object v2, LX/Al1;->WHY_AM_I_SEEING_THIS:LX/Al1;

    invoke-virtual {v2}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v2

    .line 286889
    invoke-interface {p1, v1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 286890
    new-instance v3, LX/Bo3;

    invoke-direct {v3, p0, p2, v2, p3}, LX/Bo3;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/view/View;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286891
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, p2, v3, v2, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 286892
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 286893
    iget-boolean v2, v0, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    move v0, v2

    .line 286894
    if-eqz v0, :cond_3

    const v0, 0x7f020ac1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 286895
    :cond_0
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286896
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 286897
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object v1, v0

    .line 286898
    check-cast v1, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 286899
    iget-boolean v3, v1, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    move v3, v3

    .line 286900
    if-eqz v3, :cond_4

    const v1, 0x7f081121

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 286901
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 286902
    if-eqz v3, :cond_5

    sget-object v1, LX/Al1;->MARK_AS_USEFUL:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v1

    .line 286903
    :goto_2
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    .line 286904
    const v3, 0x7f020238

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 286905
    new-instance v3, LX/Bo4;

    invoke-direct {v3, p0, p2, v1, v0}, LX/Bo4;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286906
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p0, p2, v0, v1, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 286907
    :cond_1
    const/4 v9, 0x0

    .line 286908
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 286909
    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    .line 286910
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 286911
    iget-object v4, p0, LX/1dt;->q:LX/0Uh;

    const/16 v5, 0x40e

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_7

    .line 286912
    :cond_2
    :goto_3
    move-object v6, v3

    .line 286913
    if-nez v6, :cond_6

    .line 286914
    :goto_4
    return-void

    .line 286915
    :cond_3
    const v0, 0x7f0208ed

    goto :goto_0

    .line 286916
    :cond_4
    const v1, 0x7f08111f

    goto :goto_1

    .line 286917
    :cond_5
    sget-object v1, LX/Al1;->THIS_AD_IS_USEFUL:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 286918
    :cond_6
    sget-object v2, LX/Al1;->MORE_ABOUT_THIS_APP:LX/Al1;

    invoke-virtual {v2}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v5

    .line 286919
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08114d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, v6, LX/47G;->a:Ljava/lang/String;

    aput-object v7, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 286920
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v8

    .line 286921
    new-instance v2, LX/Bo2;

    move-object v3, p0

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, LX/Bo2;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/47G;Landroid/view/View;)V

    invoke-interface {v8, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286922
    invoke-interface {v8}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {p0, p2, v2, v5, v9}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 286923
    const v2, 0x7f0208ed

    invoke-interface {v8, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_4

    .line 286924
    :cond_7
    instance-of v4, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_2

    .line 286925
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286926
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    .line 286927
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 286928
    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v3

    goto :goto_3
.end method

.method public static synthetic d(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 286929
    invoke-static {p1}, LX/1dt;->l(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method

.method public static final j(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 286930
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 286931
    const/4 v0, 0x0

    .line 286932
    :goto_0
    return v0

    :cond_0
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_0
.end method

.method public static k(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 286752
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    move v0, v1

    .line 286753
    :goto_0
    return v0

    .line 286754
    :cond_0
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286755
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 286756
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, LX/1dt;->x:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286757
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 286758
    goto :goto_0
.end method

.method private static l(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 286933
    instance-of v1, p0, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v1, :cond_0

    .line 286934
    check-cast p0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {p0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 286935
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286936
    iget-boolean v2, v1, Lcom/facebook/graphql/model/SponsoredImpression;->v:Z

    move v1, v2

    .line 286937
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 286938
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 286759
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286760
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    .line 286761
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_1

    .line 286762
    :cond_0
    :goto_0
    return-void

    .line 286763
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v3

    .line 286764
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 286765
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v5

    .line 286766
    sget-object v6, LX/1dt;->d:LX/0Rf;

    invoke-virtual {v6, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 286767
    invoke-virtual {p0, p1, p2, v0, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V

    .line 286768
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 286769
    :cond_3
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 286770
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    .line 286771
    sget-object v5, LX/1dt;->d:LX/0Rf;

    invoke-virtual {v5, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 286772
    invoke-virtual {p0, p1, p2, v0, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V

    .line 286773
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 286774
    :cond_5
    iget-object v0, p0, LX/1dt;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "573681459506672"

    .line 286775
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 286776
    move-object v0, v0

    .line 286777
    invoke-virtual {v0}, LX/0gt;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286939
    invoke-super {p0, p1, p2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 286940
    iget-object v0, p0, LX/1dt;->g:LX/13l;

    if-eqz v0, :cond_0

    .line 286941
    iget-object v0, p0, LX/1dt;->g:LX/13l;

    sget-object v1, LX/04g;->BY_DIALOG:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 286942
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 286684
    invoke-virtual {p0, p1}, LX/1SX;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 286685
    invoke-static {p1}, LX/0sa;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286686
    iget-object v1, p0, LX/1dt;->j:LX/1e9;

    .line 286687
    iget-object v2, v1, LX/1e9;->a:LX/1Nq;

    const-string v3, ""

    invoke-static {v3}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 286688
    :cond_0
    iget-object v1, p0, LX/1dt;->f:LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 v4, 0x6de

    const-class v0, Landroid/app/Activity;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v3, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 286689
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 286690
    invoke-interface {p1}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 286691
    :goto_0
    iget-object v0, p3, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 286692
    iget-object v1, p0, LX/1dt;->i:LX/1du;

    invoke-interface {p1}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/1du;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 286693
    return-void

    .line 286694
    :cond_0
    iget-object v0, p0, LX/1dt;->i:LX/1du;

    invoke-interface {p1}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    new-instance v3, LX/BoE;

    invoke-direct {v3, p0, p1}, LX/BoE;-><init>(LX/1dt;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1du;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)LX/BoM;
    .locals 2

    .prologue
    .line 286695
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v0, :cond_0

    .line 286696
    new-instance v0, LX/BoM;

    invoke-direct {v0, p0}, LX/BoM;-><init>(LX/1dt;)V

    .line 286697
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286698
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286699
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286700
    iget-object v1, p0, LX/1dt;->z:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, p1}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 286701
    iget-object v1, p0, LX/1dt;->z:LX/0bH;

    new-instance v2, LX/1YM;

    invoke-direct {v2}, LX/1YM;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 286702
    iget-object v1, p0, LX/1dt;->A:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dv;

    invoke-virtual {v1, v0}, LX/1dv;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 286703
    iget-object v2, p0, LX/1dt;->C:LX/0Sh;

    new-instance v3, LX/Bo9;

    invoke-direct {v3, p0, v0, p2}, LX/Bo9;-><init>(LX/1dt;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 286704
    return-void
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286705
    iget-object v0, p0, LX/1dt;->h:LX/0Or;

    return-object v0
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)LX/9lZ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")",
            "LX/9lZ;"
        }
    .end annotation

    .prologue
    .line 286706
    new-instance v0, LX/Boc;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-direct {v0, p0, p1, v1}, LX/Boc;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 286707
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 286708
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1wE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;

    move-result-object v0

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286709
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":OrganicHScroll"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286710
    :goto_0
    return-object v0

    .line 286711
    :cond_0
    invoke-static {p1}, LX/1wF;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286712
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":SharedAd"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 286713
    goto :goto_0
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286714
    new-instance v0, LX/0ju;

    invoke-direct {v0, p2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08104e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08105c

    new-instance v2, LX/Bo6;

    invoke-direct {v2, p0, p1, p2}, LX/Bo6;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0810d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 286715
    return-void
.end method

.method public d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 286716
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286717
    new-instance v0, LX/Bod;

    invoke-direct {v0, p0}, LX/Bod;-><init>(LX/1dt;)V

    .line 286718
    :goto_0
    return-object v0

    .line 286719
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    if-eqz v0, :cond_2

    .line 286720
    :cond_1
    new-instance v0, LX/BoP;

    invoke-direct {v0, p0}, LX/BoP;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286721
    :cond_2
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_4

    .line 286722
    invoke-static {p1}, LX/1wF;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 286723
    new-instance v0, LX/Bof;

    invoke-direct {v0, p0}, LX/Bof;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286724
    :cond_3
    new-instance v0, LX/1wG;

    invoke-direct {v0, p0}, LX/1wG;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286725
    :cond_4
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_5

    .line 286726
    new-instance v0, LX/BoG;

    invoke-direct {v0, p0}, LX/BoG;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286727
    :cond_5
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    if-eqz v0, :cond_6

    .line 286728
    new-instance v0, LX/BoH;

    invoke-direct {v0, p0}, LX/BoH;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286729
    :cond_6
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v0, :cond_7

    .line 286730
    new-instance v0, LX/BoV;

    invoke-direct {v0, p0}, LX/BoV;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286731
    :cond_7
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_8

    .line 286732
    new-instance v0, LX/BoX;

    invoke-direct {v0, p0}, LX/BoX;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286733
    :cond_8
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    if-eqz v0, :cond_9

    .line 286734
    new-instance v0, LX/BoW;

    invoke-direct {v0, p0}, LX/BoW;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286735
    :cond_9
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    if-eqz v0, :cond_a

    .line 286736
    new-instance v0, LX/BoZ;

    invoke-direct {v0, p0}, LX/BoZ;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286737
    :cond_a
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    if-eqz v0, :cond_b

    .line 286738
    new-instance v0, LX/Bog;

    invoke-direct {v0, p0}, LX/Bog;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286739
    :cond_b
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    if-eqz v0, :cond_c

    .line 286740
    new-instance v0, LX/Bob;

    invoke-direct {v0, p0}, LX/Bob;-><init>(LX/1dt;)V

    goto :goto_0

    .line 286741
    :cond_c
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    if-eqz v0, :cond_d

    .line 286742
    new-instance v0, LX/BoJ;

    invoke-direct {v0, p0}, LX/BoJ;-><init>(LX/1dt;)V

    goto/16 :goto_0

    .line 286743
    :cond_d
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v0, :cond_e

    move-object v0, v1

    .line 286744
    goto/16 :goto_0

    .line 286745
    :cond_e
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    if-eqz v0, :cond_f

    .line 286746
    new-instance v0, LX/Boa;

    invoke-direct {v0, p0}, LX/Boa;-><init>(LX/1dt;)V

    goto/16 :goto_0

    .line 286747
    :cond_f
    instance-of v0, p1, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v0, :cond_10

    .line 286748
    new-instance v0, LX/BoH;

    invoke-direct {v0, p0}, LX/BoH;-><init>(LX/1dt;)V

    goto/16 :goto_0

    :cond_10
    move-object v0, v1

    .line 286749
    goto/16 :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 286750
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 286751
    const/4 v0, 0x1

    return v0
.end method
