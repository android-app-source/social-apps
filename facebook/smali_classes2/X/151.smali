.class public abstract LX/151;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/152;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/ContentResolver;

.field private final d:LX/03V;

.field public final e:LX/0So;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/5Qs;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/5Qt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179916
    new-instance v0, LX/152;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-direct {v0, v2, v2, v1, v2}, LX/152;-><init>(ZZLX/03R;Z)V

    sput-object v0, LX/151;->a:LX/152;

    .line 179917
    const-class v0, LX/151;

    sput-object v0, LX/151;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/03V;LX/0So;)V
    .locals 1

    .prologue
    .line 179918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179919
    iput-object p1, p0, LX/151;->c:Landroid/content/ContentResolver;

    .line 179920
    iput-object p2, p0, LX/151;->d:LX/03V;

    .line 179921
    iput-object p3, p0, LX/151;->e:LX/0So;

    .line 179922
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/151;->f:Ljava/util/Map;

    .line 179923
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/151;->g:Ljava/util/Map;

    .line 179924
    return-void
.end method

.method public static b(LX/151;Ljava/lang/String;)LX/152;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 179890
    sget-object v8, LX/03R;->UNSET:LX/03R;

    .line 179891
    :try_start_0
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 179892
    const-string v0, "userId"

    invoke-virtual {v3, v0, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 179893
    iget-object v0, p0, LX/151;->c:Landroid/content/ContentResolver;

    invoke-virtual {p0}, LX/151;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 179894
    if-eqz v5, :cond_9

    move v1, v7

    move-object v2, v8

    move v0, v7

    move v3, v7

    .line 179895
    :goto_0
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 179896
    const/4 v3, 0x0

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    sget-object v4, LX/5Qr;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-le v3, v4, :cond_1

    move v3, v6

    .line 179897
    :goto_1
    if-eqz v3, :cond_2

    move v0, v6

    .line 179898
    :cond_0
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move v4, v3

    move v3, v0

    .line 179899
    :goto_2
    new-instance v0, LX/152;

    invoke-direct {v0, v4, v3, v2, v1}, LX/152;-><init>(ZZLX/03R;Z)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 179900
    :goto_3
    return-object v0

    :cond_1
    move v3, v7

    .line 179901
    goto :goto_1

    .line 179902
    :cond_2
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-lt v4, v9, :cond_8

    .line 179903
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sget-object v4, LX/5Qr;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v0, v4, :cond_4

    move v0, v6

    :goto_4
    move v4, v0

    .line 179904
    :goto_5
    invoke-interface {v5}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-lt v0, v10, :cond_3

    .line 179905
    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sget-object v2, LX/5Qr;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v0, v2, :cond_5

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_6
    move-object v2, v0

    .line 179906
    :cond_3
    invoke-interface {v5}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    const/4 v8, 0x4

    if-lt v0, v8, :cond_7

    .line 179907
    const/4 v0, 0x3

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sget-object v1, LX/5Qr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_6

    move v0, v6

    :goto_7
    move v1, v0

    move v0, v4

    goto :goto_0

    :cond_4
    move v0, v7

    .line 179908
    goto :goto_4

    .line 179909
    :cond_5
    sget-object v0, LX/03R;->NO:LX/03R;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_6

    :cond_6
    move v0, v7

    .line 179910
    goto :goto_7

    .line 179911
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 179912
    :catch_0
    new-instance v0, LX/152;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-direct {v0, v7, v7, v1, v7}, LX/152;-><init>(ZZLX/03R;Z)V

    goto :goto_3

    .line 179913
    :catch_1
    move-exception v0

    .line 179914
    iget-object v1, p0, LX/151;->d:LX/03V;

    const-string v2, "BASE_APP_USER_STATUS_PROVIDER"

    const-string v3, "Exception in BaseAppUserStatusProvider"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 179915
    new-instance v0, LX/152;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-direct {v0, v7, v7, v1, v7}, LX/152;-><init>(ZZLX/03R;Z)V

    goto :goto_3

    :cond_7
    move v0, v4

    goto/16 :goto_0

    :cond_8
    move v4, v0

    goto :goto_5

    :cond_9
    move v1, v7

    move-object v2, v8

    move v3, v7

    move v4, v7

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/152;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 179858
    iget-object v0, p0, LX/151;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 179859
    iget-object v1, p0, LX/151;->f:Ljava/util/Map;

    monitor-enter v1

    .line 179860
    :try_start_0
    iget-object v0, p0, LX/151;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qs;

    .line 179861
    if-eqz v0, :cond_0

    iget-wide v4, v0, LX/5Qs;->b:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 179862
    iget-object v0, p0, LX/151;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179863
    const/4 v0, 0x0

    .line 179864
    :cond_0
    if-eqz v0, :cond_1

    .line 179865
    iget-object v0, v0, LX/5Qs;->a:LX/152;

    monitor-exit v1

    .line 179866
    :goto_0
    return-object v0

    .line 179867
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179868
    iget-object v1, p0, LX/151;->g:Ljava/util/Map;

    monitor-enter v1

    .line 179869
    :try_start_1
    iget-object v0, p0, LX/151;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qt;

    .line 179870
    if-nez v0, :cond_2

    .line 179871
    new-instance v0, LX/5Qt;

    new-instance v4, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;

    invoke-direct {v4, p0, p1}, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;-><init>(LX/151;Ljava/lang/String;)V

    const v5, 0x39fda7b1

    invoke-static {v4, v5}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v4

    invoke-direct {v0, v4, v2, v3}, LX/5Qt;-><init>(Ljava/lang/Thread;J)V

    .line 179872
    iget-object v4, p0, LX/151;->g:Ljava/util/Map;

    invoke-interface {v4, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179873
    iget-object v4, v0, LX/5Qt;->a:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 179874
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 179875
    const-wide/16 v4, 0xbb8

    iget-wide v6, v0, LX/5Qt;->b:J

    sub-long/2addr v2, v6

    sub-long v2, v4, v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 179876
    cmp-long v1, v2, v8

    if-lez v1, :cond_3

    .line 179877
    :try_start_2
    iget-object v0, v0, LX/5Qt;->a:Ljava/lang/Thread;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 179878
    :cond_3
    :goto_1
    iget-object v1, p0, LX/151;->f:Ljava/util/Map;

    monitor-enter v1

    .line 179879
    :try_start_3
    iget-object v0, p0, LX/151;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qs;

    .line 179880
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 179881
    if-eqz v0, :cond_4

    .line 179882
    iget-object v0, v0, LX/5Qs;->a:LX/152;

    goto :goto_0

    .line 179883
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 179884
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 179885
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 179886
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 179887
    :cond_4
    iget-object v0, p0, LX/151;->d:LX/03V;

    const-string v1, "BASE_APP_USER_STATUS_PROVIDER Default status returned"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve status from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/151;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179888
    sget-object v0, LX/151;->b:Ljava/lang/Class;

    const-string v1, "Remote app took too long to respond, using default status."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 179889
    sget-object v0, LX/151;->a:LX/152;

    goto/16 :goto_0
.end method

.method public abstract a()Ljava/lang/String;
.end method
