.class public abstract LX/0aT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0YZ;"
    }
.end annotation


# instance fields
.field private final mBroadcastLogic:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 84795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84796
    iput-object p1, p0, LX/0aT;->mBroadcastLogic:LX/0Ot;

    .line 84797
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x2c17e0b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 84798
    iget-object v1, p0, LX/0aT;->mBroadcastLogic:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1}, LX/0aT;->onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V

    .line 84799
    const/16 v1, 0x27

    const v2, 0x5c546977

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public abstract onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "TT;)V"
        }
    .end annotation
.end method
