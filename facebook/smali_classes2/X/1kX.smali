.class public LX/1kX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ry;


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1kY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1kZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1vS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 309935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309936
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309937
    iget-object v0, p0, LX/1kX;->a:LX/0TD;

    new-instance v1, LX/1vT;

    invoke-direct {v1, p0}, LX/1vT;-><init>(LX/1kX;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309938
    invoke-virtual {p0}, LX/1kX;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309939
    const/4 v0, 0x0

    iput-object v0, p0, LX/1kX;->d:LX/1vS;

    .line 309940
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 309941
    invoke-virtual {p0}, LX/1kX;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309942
    const/4 v0, 0x0

    iput-object v0, p0, LX/1kX;->d:LX/1vS;

    .line 309943
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 309944
    iget-object v0, p0, LX/1kX;->c:LX/1kZ;

    .line 309945
    iget-object p0, v0, LX/1kZ;->b:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    move v0, p0

    .line 309946
    return v0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309947
    const-class v0, LX/1vS;

    return-object v0
.end method
