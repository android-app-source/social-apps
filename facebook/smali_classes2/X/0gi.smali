.class public final enum LX/0gi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0gi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0gi;

.field public static final enum DEFAULT_DIVEBAR:LX/0gi;

.field public static final enum INSPIRATION_CAMERA_DIVEBAR:LX/0gi;

.field public static final enum NOW_SIDEBAR:LX/0gi;

.field public static final enum SNACKS_DIVEBAR:LX/0gi;

.field public static final enum UNKOWN:LX/0gi;


# instance fields
.field private final mAnalyticsTag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 113319
    new-instance v0, LX/0gi;

    const-string v1, "DEFAULT_DIVEBAR"

    const-string v2, "divebar"

    invoke-direct {v0, v1, v3, v2}, LX/0gi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gi;->DEFAULT_DIVEBAR:LX/0gi;

    .line 113320
    new-instance v0, LX/0gi;

    const-string v1, "NOW_SIDEBAR"

    const-string v2, "now_presence"

    invoke-direct {v0, v1, v4, v2}, LX/0gi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gi;->NOW_SIDEBAR:LX/0gi;

    .line 113321
    new-instance v0, LX/0gi;

    const-string v1, "SNACKS_DIVEBAR"

    const-string v2, "snacks_divebar"

    invoke-direct {v0, v1, v5, v2}, LX/0gi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gi;->SNACKS_DIVEBAR:LX/0gi;

    .line 113322
    new-instance v0, LX/0gi;

    const-string v1, "INSPIRATION_CAMERA_DIVEBAR"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v6, v2}, LX/0gi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gi;->INSPIRATION_CAMERA_DIVEBAR:LX/0gi;

    .line 113323
    new-instance v0, LX/0gi;

    const-string v1, "UNKOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v7, v2}, LX/0gi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gi;->UNKOWN:LX/0gi;

    .line 113324
    const/4 v0, 0x5

    new-array v0, v0, [LX/0gi;

    sget-object v1, LX/0gi;->DEFAULT_DIVEBAR:LX/0gi;

    aput-object v1, v0, v3

    sget-object v1, LX/0gi;->NOW_SIDEBAR:LX/0gi;

    aput-object v1, v0, v4

    sget-object v1, LX/0gi;->SNACKS_DIVEBAR:LX/0gi;

    aput-object v1, v0, v5

    sget-object v1, LX/0gi;->INSPIRATION_CAMERA_DIVEBAR:LX/0gi;

    aput-object v1, v0, v6

    sget-object v1, LX/0gi;->UNKOWN:LX/0gi;

    aput-object v1, v0, v7

    sput-object v0, LX/0gi;->$VALUES:[LX/0gi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 113325
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113326
    iput-object p3, p0, LX/0gi;->mAnalyticsTag:Ljava/lang/String;

    .line 113327
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0gi;
    .locals 1

    .prologue
    .line 113328
    const-class v0, LX/0gi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0gi;

    return-object v0
.end method

.method public static values()[LX/0gi;
    .locals 1

    .prologue
    .line 113329
    sget-object v0, LX/0gi;->$VALUES:[LX/0gi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0gi;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113330
    iget-object v0, p0, LX/0gi;->mAnalyticsTag:Ljava/lang/String;

    return-object v0
.end method
