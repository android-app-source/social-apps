.class public LX/0yg;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 165816
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 165817
    return-void
.end method

.method public static a(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165815
    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsUserEligibleForDialtone;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165814
    invoke-virtual {p0}, LX/0yc;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165813
    invoke-virtual {p0}, LX/0yc;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoCapFeatureEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165812
    invoke-virtual {p0}, LX/0yc;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneFeedCapFeatureEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165809
    invoke-virtual {p0}, LX/0yc;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/0yc;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneFacewebFeatureEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 165811
    invoke-virtual {p0}, LX/0yc;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 165810
    return-void
.end method
