.class public abstract LX/0On;
.super Landroid/content/ContentProvider;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54315
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 54316
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x200

    .line 54302
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54303
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 54304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54305
    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 54306
    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 54307
    invoke-virtual {p0}, LX/0On;->c()V

    .line 54308
    invoke-virtual {p0}, LX/0On;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54309
    invoke-static {}, LX/2BO;->a()V

    .line 54310
    :cond_0
    return-void
.end method

.method private static i()V
    .locals 2

    .prologue
    .line 54311
    const-wide/16 v0, 0x200

    invoke-static {v0, v1}, LX/018;->a(J)V

    .line 54312
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method public abstract a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method public a(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 1

    .prologue
    .line 54313
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 54314
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method public abstract a(Landroid/net/Uri;)Ljava/lang/String;
.end method

.method public a()V
    .locals 0

    .prologue
    .line 54317
    return-void
.end method

.method public a(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 54318
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method public final applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 54319
    const-string v0, "applyBatch"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54320
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54321
    invoke-virtual {p0, p1}, LX/0On;->a(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54322
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public b(Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 54323
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 54324
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 54297
    const/4 v0, 0x0

    return v0
.end method

.method public final bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 1

    .prologue
    .line 54298
    const-string v0, "bulkInsert"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54299
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54300
    invoke-virtual {p0, p1, p2}, LX/0On;->a(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 54301
    invoke-static {}, LX/0On;->i()V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 54215
    iget-object v1, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 54216
    :try_start_0
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54217
    invoke-virtual {p0}, LX/0On;->a()V

    .line 54218
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 54219
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 54220
    const-string v0, "call"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54221
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54222
    const/4 v0, 0x0

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54223
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54224
    const-string v0, "delete"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54225
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54226
    invoke-virtual {p0, p1, p2, p3}, LX/0On;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 54227
    invoke-static {}, LX/0On;->i()V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 54228
    const-string v0, "getStreamTypes"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54229
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54230
    const/4 v0, 0x0

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54231
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54232
    const-string v0, "getType"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54233
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54234
    invoke-virtual {p0, p1}, LX/0On;->a(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54235
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54236
    const-string v0, "insert"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54237
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54238
    invoke-virtual {p0, p1, p2}, LX/0On;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54239
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final isTemporary()Z
    .locals 1

    .prologue
    .line 54240
    const-string v0, "isTemporary"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54241
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54242
    invoke-super {p0}, Landroid/content/ContentProvider;->isTemporary()Z

    move-result v0

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54243
    invoke-static {}, LX/0On;->i()V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 54244
    const-string v0, "onConfigurationChanged"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54245
    :try_start_0
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 54246
    invoke-static {}, LX/0On;->i()V

    .line 54247
    :goto_0
    return-void

    .line 54248
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/0On;->h()V

    .line 54249
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onConfigurationChanged(Landroid/content/res/Configuration;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54250
    invoke-static {}, LX/0On;->i()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final onCreate()Z
    .locals 1

    .prologue
    .line 54295
    const-string v0, "onCreate"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54296
    invoke-static {}, LX/0On;->i()V

    const/4 v0, 0x1

    return v0
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 54251
    const-string v0, "onLowMemory"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54252
    :try_start_0
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 54253
    invoke-static {}, LX/0On;->i()V

    .line 54254
    :goto_0
    return-void

    .line 54255
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/0On;->h()V

    .line 54256
    invoke-super {p0}, Landroid/content/ContentProvider;->onLowMemory()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54257
    invoke-static {}, LX/0On;->i()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final onTrimMemory(I)V
    .locals 1

    .prologue
    .line 54258
    const-string v0, "onTrimMemory"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54259
    :try_start_0
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 54260
    invoke-static {}, LX/0On;->i()V

    .line 54261
    :goto_0
    return-void

    .line 54262
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/0On;->h()V

    .line 54263
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onTrimMemory(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54264
    invoke-static {}, LX/0On;->i()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 54265
    const-string v0, "openAssetFile"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54266
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54267
    invoke-virtual {p0, p1, p2}, LX/0On;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54268
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 54269
    const-string v0, "openFile"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54270
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54271
    invoke-virtual {p0, p1, p2}, LX/0On;->b(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54272
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 54273
    const-string v0, "openTypedAssetFile"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54274
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54275
    invoke-virtual {p0, p1}, LX/0On;->b(Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54276
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 54277
    const-string v0, "query"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54278
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54279
    invoke-virtual/range {p0 .. p5}, LX/0On;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54280
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 2
    .param p2    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/os/CancellationSignal;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 54281
    const-string v0, "query"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54282
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54283
    invoke-virtual/range {p0 .. p5}, LX/0On;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54284
    invoke-static {}, LX/0On;->i()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final shutdown()V
    .locals 1

    .prologue
    .line 54285
    const-string v0, "shutdown"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54286
    :try_start_0
    iget-object v0, p0, LX/0On;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 54287
    invoke-static {}, LX/0On;->i()V

    .line 54288
    :goto_0
    return-void

    .line 54289
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/0On;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54290
    invoke-static {}, LX/0On;->i()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54291
    const-string v0, "update"

    invoke-direct {p0, v0}, LX/0On;->a(Ljava/lang/String;)V

    .line 54292
    :try_start_0
    invoke-direct {p0}, LX/0On;->h()V

    .line 54293
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0On;->a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 54294
    invoke-static {}, LX/0On;->i()V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {}, LX/0On;->i()V

    throw v0
.end method
