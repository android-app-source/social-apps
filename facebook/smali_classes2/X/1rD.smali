.class public LX/1rD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jl;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1rD;


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1qi;

.field private e:J

.field public volatile f:J


# direct methods
.method public constructor <init>(LX/0SG;LX/1qi;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/1qi;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331869
    iput-object p1, p0, LX/1rD;->a:LX/0SG;

    .line 331870
    iput-object p3, p0, LX/1rD;->b:LX/0Ot;

    .line 331871
    iput-object p4, p0, LX/1rD;->c:LX/0Ot;

    .line 331872
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1rD;->f:J

    .line 331873
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1rD;->e:J

    .line 331874
    iput-object p2, p0, LX/1rD;->d:LX/1qi;

    .line 331875
    return-void
.end method

.method public static a(LX/0QB;)LX/1rD;
    .locals 7

    .prologue
    .line 331881
    sget-object v0, LX/1rD;->g:LX/1rD;

    if-nez v0, :cond_1

    .line 331882
    const-class v1, LX/1rD;

    monitor-enter v1

    .line 331883
    :try_start_0
    sget-object v0, LX/1rD;->g:LX/1rD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331884
    if-eqz v2, :cond_0

    .line 331885
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331886
    new-instance v5, LX/1rD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/1qi;->a(LX/0QB;)LX/1qi;

    move-result-object v4

    check-cast v4, LX/1qi;

    const/16 v6, 0xbc

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xc9

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, LX/1rD;-><init>(LX/0SG;LX/1qi;LX/0Ot;LX/0Ot;)V

    .line 331887
    move-object v0, v5

    .line 331888
    sput-object v0, LX/1rD;->g:LX/1rD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331889
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331890
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331891
    :cond_1
    sget-object v0, LX/1rD;->g:LX/1rD;

    return-object v0

    .line 331892
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 331876
    iget-object v0, p0, LX/1rD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v0

    .line 331877
    if-eqz v0, :cond_0

    .line 331878
    invoke-virtual {v0}, LX/148;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331879
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 331880
    :cond_0
    return-object p1
.end method

.method private static a(JJLX/1t7;)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 331861
    iget-wide v2, p4, LX/1t7;->a:J

    .line 331862
    iget-wide v4, p4, LX/1t7;->b:J

    .line 331863
    sub-long v6, p2, p0

    cmp-long v1, v6, v2

    if-ltz v1, :cond_1

    .line 331864
    :cond_0
    :goto_0
    return v0

    .line 331865
    :cond_1
    rem-long v6, p0, v2

    sub-long/2addr v4, v6

    add-long/2addr v4, v2

    rem-long v2, v4, v2

    add-long/2addr v2, p0

    .line 331866
    cmp-long v1, v2, p2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(JLX/1t7;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 331867
    iget-wide v0, p2, LX/1t7;->b:J

    iget-wide v2, p2, LX/1t7;->a:J

    rem-long v2, p0, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, 0x3e8

    .line 331852
    div-long v0, p1, v4

    .line 331853
    iget-wide v2, p0, LX/1rD;->e:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 331854
    :cond_0
    :goto_0
    return-void

    .line 331855
    :cond_1
    iget-wide v2, p0, LX/1rD;->e:J

    mul-long/2addr v2, v4

    .line 331856
    iput-wide v0, p0, LX/1rD;->e:J

    .line 331857
    iget-object v4, p0, LX/1rD;->d:LX/1qi;

    invoke-virtual {v4}, LX/1qi;->a()LX/1t7;

    move-result-object v4

    .line 331858
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/1t7;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0, v1, v4}, LX/1rD;->a(JLX/1t7;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331859
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "immediate_active_seconds"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "activity_time"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "last_activity_time"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "last_foreground_time"

    iget-wide v2, p0, LX/1rD;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "upload_this_event_now"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 331860
    iget-object v1, p0, LX/1rD;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {p0, v0}, LX/1rD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 331851
    iget-object v0, p0, LX/1rD;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 331849
    invoke-direct {p0, p1, p2}, LX/1rD;->c(J)V

    .line 331850
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 331842
    iget-object v0, p0, LX/1rD;->d:LX/1qi;

    invoke-virtual {v0}, LX/1qi;->a()LX/1t7;

    move-result-object v0

    .line 331843
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1t7;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2, p3, p4, p5, v0}, LX/1rD;->a(JJLX/1t7;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 331844
    :cond_0
    :goto_0
    return-void

    .line 331845
    :cond_1
    mul-long v0, p2, v2

    .line 331846
    mul-long/2addr v2, p4

    .line 331847
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "immediate_active_seconds"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "event"

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "start_time"

    invoke-virtual {v4, v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "end_time"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "upload_this_event_now"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 331848
    iget-object v1, p0, LX/1rD;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {p0, v0}, LX/1rD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 331840
    invoke-direct {p0, p1, p2}, LX/1rD;->c(J)V

    .line 331841
    return-void
.end method
