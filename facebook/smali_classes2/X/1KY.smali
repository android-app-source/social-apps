.class public LX/1KY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1KY;


# instance fields
.field public b:Z

.field public c:J

.field public d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232797
    new-instance v0, LX/1KY;

    invoke-direct {v0}, LX/1KY;-><init>()V

    sput-object v0, LX/1KY;->a:LX/1KY;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 232796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 232790
    iget-wide v0, p0, LX/1KY;->c:J

    int-to-long v2, p1

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/1KY;->c:J

    .line 232791
    iget-wide v0, p0, LX/1KY;->d:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1KY;->d:J

    .line 232792
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 232793
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232794
    const-string v1, "warm sched:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/1KY;->c:J

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sF:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/1KY;->d:J

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232795
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
