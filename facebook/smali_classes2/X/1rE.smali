.class public final LX/1rE;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/168;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1rE;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/168;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331907
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 331908
    return-void
.end method

.method public static a(LX/0QB;)LX/1rE;
    .locals 4

    .prologue
    .line 331894
    sget-object v0, LX/1rE;->a:LX/1rE;

    if-nez v0, :cond_1

    .line 331895
    const-class v1, LX/1rE;

    monitor-enter v1

    .line 331896
    :try_start_0
    sget-object v0, LX/1rE;->a:LX/1rE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331897
    if-eqz v2, :cond_0

    .line 331898
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331899
    new-instance v3, LX/1rE;

    const/16 p0, 0xce

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1rE;-><init>(LX/0Ot;)V

    .line 331900
    move-object v0, v3

    .line 331901
    sput-object v0, LX/1rE;->a:LX/1rE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331902
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331903
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331904
    :cond_1
    sget-object v0, LX/1rE;->a:LX/1rE;

    return-object v0

    .line 331905
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 331909
    check-cast p3, LX/168;

    .line 331910
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331911
    sget-object v0, LX/1rF;->FOREGROUNDED:LX/1rF;

    iget-object v1, p3, LX/168;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p3, v0, v2, v3}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 331912
    :goto_0
    return-void

    .line 331913
    :cond_0
    sget-object v0, LX/1rF;->BACKGROUNDED:LX/1rF;

    iget-object v1, p3, LX/168;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p3, v0, v2, v3}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    goto :goto_0
.end method
