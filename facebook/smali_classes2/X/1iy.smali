.class public LX/1iy;
.super LX/1iY;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0yR;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0yR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299494
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299495
    iput-object p1, p0, LX/1iy;->a:LX/0Uh;

    .line 299496
    iput-object p2, p0, LX/1iy;->b:LX/0yR;

    .line 299497
    return-void
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 3
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299489
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 299490
    iget-object v0, p0, LX/1iy;->a:LX/0Uh;

    const/16 v1, 0x687

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 299491
    :cond_0
    :goto_0
    return-void

    .line 299492
    :cond_1
    const-string v0, "X-ZERO-STATE"

    invoke-interface {p2, v0}, Lorg/apache/http/HttpRequest;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299493
    iget-object v0, p0, LX/1iy;->b:LX/0yR;

    invoke-virtual {v0}, LX/0yR;->a()V

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 299469
    invoke-super {p0, p1, p2}, LX/1iY;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 299470
    iget-object v1, p0, LX/1iy;->a:LX/0Uh;

    const/16 v2, 0x687

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 299471
    :cond_0
    :goto_0
    return-void

    .line 299472
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 299473
    sget-object v1, LX/1uT;->a:LX/0Px;

    move-object v3, v1

    .line 299474
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299475
    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 299476
    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 299477
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 299478
    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299479
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 299480
    :cond_3
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 299481
    const-string v0, "http.request"

    invoke-interface {p2, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/HttpRequest;

    .line 299482
    if-eqz v0, :cond_0

    const-string v1, "X-ZERO-STATE"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpRequest;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299483
    iget-object v0, p0, LX/1iy;->b:LX/0yR;

    invoke-virtual {v0}, LX/0yR;->a()V

    goto :goto_0

    .line 299484
    :cond_4
    iget-object v0, p0, LX/1iy;->b:LX/0yR;

    .line 299485
    iget-object v1, v0, LX/0yR;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yT;

    .line 299486
    iget-object v0, v1, LX/0yT;->a:LX/0yI;

    invoke-virtual {v0, v2}, LX/0yI;->a(Ljava/util/Map;)V

    .line 299487
    goto :goto_2

    .line 299488
    :cond_5
    goto :goto_0
.end method
