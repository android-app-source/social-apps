.class public final LX/1cA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1c8;


# instance fields
.field public final synthetic a:LX/1bo;


# direct methods
.method public constructor <init>(LX/1bo;)V
    .locals 0

    .prologue
    .line 281574
    iput-object p1, p0, LX/1cA;->a:LX/1bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ln;)Z
    .locals 1

    .prologue
    .line 281589
    const/4 v0, 0x1

    return v0
.end method

.method public final b(LX/1ln;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 281575
    instance-of v0, p1, LX/1ll;

    if-eqz v0, :cond_2

    .line 281576
    check-cast p1, LX/1ll;

    .line 281577
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, LX/1cA;->a:LX/1bo;

    iget-object v0, v0, LX/1bo;->b:Landroid/content/res/Resources;

    invoke-virtual {p1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 281578
    iget v0, p1, LX/1ll;->d:I

    move v0, v0

    .line 281579
    if-eqz v0, :cond_0

    .line 281580
    iget v0, p1, LX/1ll;->d:I

    move v0, v0

    .line 281581
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 281582
    :goto_0
    return-object v0

    .line 281583
    :cond_1
    new-instance v0, LX/4AT;

    .line 281584
    iget v2, p1, LX/1ll;->d:I

    move v2, v2

    .line 281585
    invoke-direct {v0, v1, v2}, LX/4AT;-><init>(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    .line 281586
    :cond_2
    iget-object v0, p0, LX/1cA;->a:LX/1bo;

    iget-object v0, v0, LX/1bo;->c:LX/1c3;

    if-eqz v0, :cond_3

    .line 281587
    iget-object v0, p0, LX/1cA;->a:LX/1bo;

    iget-object v0, v0, LX/1bo;->c:LX/1c3;

    invoke-virtual {v0, p1}, LX/1c3;->a(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 281588
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
