.class public LX/1sf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0VT;

.field private final c:LX/0WV;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0VT;LX/0WV;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/selfupdate/annotations/IsSelfUpdateEnabledGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "LX/0WV;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 335071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335072
    iput-object p1, p0, LX/1sf;->a:Landroid/content/Context;

    .line 335073
    iput-object p2, p0, LX/1sf;->b:LX/0VT;

    .line 335074
    iput-object p3, p0, LX/1sf;->c:LX/0WV;

    .line 335075
    iput-object p4, p0, LX/1sf;->d:LX/0Or;

    .line 335076
    iput-object p5, p0, LX/1sf;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 335077
    return-void
.end method

.method public static b(LX/0QB;)LX/1sf;
    .locals 6

    .prologue
    .line 335078
    new-instance v0, LX/1sf;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v2

    check-cast v2, LX/0VT;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v3

    check-cast v3, LX/0WV;

    const/16 v4, 0x156f

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v5}, LX/1sf;-><init>(Landroid/content/Context;LX/0VT;LX/0WV;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 335079
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 335080
    iget-object v0, p0, LX/1sf;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 335081
    if-nez v0, :cond_0

    move v0, v1

    .line 335082
    :goto_0
    return v0

    .line 335083
    :cond_0
    iget-object v0, p0, LX/1sf;->c:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->d()Z

    move-result v0

    .line 335084
    iget-object v2, p0, LX/1sf;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->D:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 335085
    iget-object v3, p0, LX/1sf;->b:LX/0VT;

    iget-object v4, p0, LX/1sf;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0VT;->b(Ljava/lang/String;)Z

    move-result v3

    .line 335086
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    if-eqz v3, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 335087
    :try_start_0
    iget-object v2, p0, LX/1sf;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "install_non_market_apps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 335088
    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    .line 335089
    :catch_0
    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 335090
    goto :goto_0
.end method
