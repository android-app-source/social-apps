.class public final LX/0zH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/0h2;


# direct methods
.method public constructor <init>(LX/0h2;)V
    .locals 0

    .prologue
    .line 166775
    iput-object p1, p0, LX/0zH;->a:LX/0h2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x2a12f90b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 166776
    iget-object v0, p0, LX/0zH;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i0;

    .line 166777
    iget-object v1, v0, LX/0i0;->b:LX/0iU;

    .line 166778
    invoke-virtual {v1}, LX/0iV;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166779
    sget-object v0, LX/10Q;->HIDDEN:LX/10Q;

    invoke-static {v1, v0}, LX/0iU;->a$redex0(LX/0iU;LX/10Q;)V

    .line 166780
    :cond_0
    iget-object v0, p0, LX/0zH;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_search_bar"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 166781
    const/4 v0, 0x0

    .line 166782
    iget-object v1, p0, LX/0zH;->a:LX/0h2;

    iget-object v1, v1, LX/0h2;->r:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->h()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 166783
    iget-object v0, p0, LX/0zH;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->h()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v1, v0

    .line 166784
    :goto_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 166785
    const-string v0, "title_bar_primary_button_spec"

    iget-object v4, p0, LX/0zH;->a:LX/0h2;

    iget-object v4, v4, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getPrimaryButtonSpec()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 166786
    const-string v0, "title_bar_secondary_button_spec"

    iget-object v4, p0, LX/0zH;->a:LX/0h2;

    iget-object v4, v4, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getSecondaryButtonSpec()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 166787
    const-string v0, "is_awareness_unit_eligible_intent_flag"

    sget-object v4, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v5, p0, LX/0zH;->a:LX/0h2;

    iget-object v5, v5, LX/0h2;->r:LX/0fd;

    invoke-virtual {v5}, LX/0fd;->b()Lcom/facebook/apptab/state/TabTag;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166788
    iget-object v0, p0, LX/0zH;->a:LX/0h2;

    iget-object v0, v0, LX/0h2;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FiT;

    sget-object v4, LX/3zg;->MAINTAB:LX/3zg;

    invoke-virtual {v0, v1, v4, v3}, LX/FiT;->a(Landroid/support/v4/app/Fragment;LX/3zg;Landroid/os/Bundle;)V

    .line 166789
    const v0, 0x7142f894

    invoke-static {v6, v6, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
