.class public LX/1cQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1FQ;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/1GL;

.field private final d:LX/1Gv;

.field private final e:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z

.field public final g:Z

.field private final h:Z


# direct methods
.method public constructor <init>(LX/1FQ;Ljava/util/concurrent/Executor;LX/1GL;LX/1Gv;ZZZLX/1cF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/memory/ByteArrayPool;",
            "Ljava/util/concurrent/Executor;",
            "LX/1GL;",
            "Lcom/facebook/imagepipeline/decoder/ProgressiveJpegConfig;",
            "ZZZ",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 282026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282027
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FQ;

    iput-object v0, p0, LX/1cQ;->a:LX/1FQ;

    .line 282028
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/1cQ;->b:Ljava/util/concurrent/Executor;

    .line 282029
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GL;

    iput-object v0, p0, LX/1cQ;->c:LX/1GL;

    .line 282030
    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gv;

    iput-object v0, p0, LX/1cQ;->d:LX/1Gv;

    .line 282031
    iput-boolean p5, p0, LX/1cQ;->f:Z

    .line 282032
    iput-boolean p6, p0, LX/1cQ;->g:Z

    .line 282033
    invoke-static {p8}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/1cQ;->e:LX/1cF;

    .line 282034
    iput-boolean p7, p0, LX/1cQ;->h:Z

    .line 282035
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282036
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 282037
    iget-object v1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 282038
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282039
    new-instance v0, LX/4er;

    iget-boolean v1, p0, LX/1cQ;->h:Z

    invoke-direct {v0, p0, p1, p2, v1}, LX/4er;-><init>(LX/1cQ;LX/1cd;LX/1cW;Z)V

    .line 282040
    :goto_0
    iget-object v1, p0, LX/1cQ;->e:LX/1cF;

    invoke-interface {v1, v0, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 282041
    return-void

    .line 282042
    :cond_0
    new-instance v4, LX/1eQ;

    iget-object v0, p0, LX/1cQ;->a:LX/1FQ;

    invoke-direct {v4, v0}, LX/1eQ;-><init>(LX/1FQ;)V

    .line 282043
    new-instance v0, LX/1eR;

    iget-object v5, p0, LX/1cQ;->d:LX/1Gv;

    iget-boolean v6, p0, LX/1cQ;->h:Z

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, LX/1eR;-><init>(LX/1cQ;LX/1cd;LX/1cW;LX/1eQ;LX/1Gv;Z)V

    goto :goto_0
.end method
