.class public final LX/0e3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/48R;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/48R;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 91417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91418
    iput-object p1, p0, LX/0e3;->a:LX/0QB;

    .line 91419
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 91416
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0e3;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 91420
    packed-switch p2, :pswitch_data_0

    .line 91421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91422
    :pswitch_0
    new-instance p0, LX/5Mw;

    invoke-static {p1}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v0, v1}, LX/5Mw;-><init>(LX/0yc;Lcom/facebook/content/SecureContextHelper;)V

    .line 91423
    move-object v0, p0

    .line 91424
    :goto_0
    return-object v0

    .line 91425
    :pswitch_1
    new-instance v2, LX/IXC;

    invoke-static {p1}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {p1}, LX/IXG;->a(LX/0QB;)LX/IXG;

    move-result-object v5

    check-cast v5, LX/IXG;

    const/16 v6, 0x1032

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xac0

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3231

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p1}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v9

    check-cast v9, LX/0yH;

    invoke-direct/range {v2 .. v9}, LX/IXC;-><init>(LX/11i;LX/0So;LX/IXG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0yH;)V

    .line 91426
    move-object v0, v2

    .line 91427
    goto :goto_0

    .line 91428
    :pswitch_2
    new-instance v2, LX/K5K;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/K8G;->b(LX/0QB;)LX/K8G;

    move-result-object v4

    check-cast v4, LX/K8G;

    invoke-static {p1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lB;

    invoke-static {p1}, LX/K7A;->a(LX/0QB;)LX/K7A;

    move-result-object v8

    check-cast v8, LX/K7A;

    invoke-direct/range {v2 .. v8}, LX/K5K;-><init>(Lcom/facebook/content/SecureContextHelper;LX/K8G;LX/0Sh;LX/0So;LX/0lB;LX/K7A;)V

    .line 91429
    move-object v0, v2

    .line 91430
    goto :goto_0

    .line 91431
    :pswitch_3
    invoke-static {p1}, LX/D37;->b(LX/0QB;)LX/D37;

    move-result-object v0

    goto :goto_0

    .line 91432
    :pswitch_4
    new-instance v2, LX/7VC;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/1ow;->c(LX/0QB;)LX/1ow;

    move-result-object v4

    check-cast v4, LX/1oy;

    const/16 v5, 0x14d1

    invoke-static {p1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 91433
    new-instance v6, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/63q;

    invoke-direct {v8, p1}, LX/63q;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 91434
    invoke-static {p1}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v7

    check-cast v7, LX/0yH;

    invoke-direct/range {v2 .. v7}, LX/7VC;-><init>(Landroid/content/Context;LX/1oy;LX/0Or;Ljava/util/Set;LX/0yH;)V

    .line 91435
    move-object v0, v2

    .line 91436
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 91415
    const/4 v0, 0x5

    return v0
.end method
