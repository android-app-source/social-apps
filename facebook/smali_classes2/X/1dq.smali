.class public LX/1dq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 286594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286595
    iput p1, p0, LX/1dq;->a:I

    .line 286596
    iput p2, p0, LX/1dq;->b:I

    .line 286597
    iput p3, p0, LX/1dq;->c:I

    .line 286598
    iput p4, p0, LX/1dq;->d:I

    .line 286599
    return-void
.end method

.method private static a(Landroid/content/res/Resources;IILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 286603
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne p3, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 286604
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    aput-object p3, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    aput-object p3, v0, v1

    .line 286605
    new-instance v1, LX/82e;

    invoke-direct {v1, v0}, LX/82e;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 286606
    move-object v0, v1

    .line 286607
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 286600
    if-lez p2, :cond_0

    .line 286601
    iget v0, p0, LX/1dq;->b:I

    iget v1, p0, LX/1dq;->d:I

    invoke-static {p1, v0, v1, p3}, LX/1dq;->a(Landroid/content/res/Resources;IILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 286602
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, LX/1dq;->a:I

    iget v1, p0, LX/1dq;->c:I

    invoke-static {p1, v0, v1, p3}, LX/1dq;->a(Landroid/content/res/Resources;IILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
