.class public LX/0mU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0mU;


# instance fields
.field public final a:Ljava/util/concurrent/CountDownLatch;

.field private final b:LX/0Sg;


# direct methods
.method public constructor <init>(LX/0Sg;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 132564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132565
    iput-object p1, p0, LX/0mU;->b:LX/0Sg;

    .line 132566
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/0mU;->a:Ljava/util/concurrent/CountDownLatch;

    .line 132567
    return-void
.end method

.method public static a(LX/0QB;)LX/0mU;
    .locals 4

    .prologue
    .line 132568
    sget-object v0, LX/0mU;->c:LX/0mU;

    if-nez v0, :cond_1

    .line 132569
    const-class v1, LX/0mU;

    monitor-enter v1

    .line 132570
    :try_start_0
    sget-object v0, LX/0mU;->c:LX/0mU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132571
    if-eqz v2, :cond_0

    .line 132572
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132573
    new-instance p0, LX/0mU;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v3

    check-cast v3, LX/0Sg;

    invoke-direct {p0, v3}, LX/0mU;-><init>(LX/0Sg;)V

    .line 132574
    move-object v0, p0

    .line 132575
    sput-object v0, LX/0mU;->c:LX/0mU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132576
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132577
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132578
    :cond_1
    sget-object v0, LX/0mU;->c:LX/0mU;

    return-object v0

    .line 132579
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 132581
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mU;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 132582
    iget-object v0, p0, LX/0mU;->b:LX/0Sg;

    const-string v1, "ColdStartCompletedLock-waitForInitialized"

    new-instance v2, Lcom/facebook/analytics2/loggermodule/ColdStartCompletedLock$1;

    invoke-direct {v2, p0}, Lcom/facebook/analytics2/loggermodule/ColdStartCompletedLock$1;-><init>(LX/0mU;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 132583
    iget-object v0, p0, LX/0mU;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/CountDownLatch;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132584
    :cond_0
    monitor-exit p0

    return-void

    .line 132585
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
