.class public final LX/0tm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/3HL;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/3HL;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 155649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155650
    iput-object p1, p0, LX/0tm;->a:LX/0QB;

    .line 155651
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155648
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0tm;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155584
    packed-switch p2, :pswitch_data_0

    .line 155585
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155586
    :pswitch_0
    new-instance v1, LX/3PK;

    const-class v0, LX/3PL;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PL;

    invoke-direct {v1, v0}, LX/3PK;-><init>(LX/3PL;)V

    .line 155587
    move-object v0, v1

    .line 155588
    :goto_0
    return-object v0

    .line 155589
    :pswitch_1
    new-instance v1, LX/3PM;

    const-class v0, LX/3PN;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PN;

    invoke-direct {v1, v0}, LX/3PM;-><init>(LX/3PN;)V

    .line 155590
    move-object v0, v1

    .line 155591
    goto :goto_0

    .line 155592
    :pswitch_2
    invoke-static {p1}, LX/3HK;->b(LX/0QB;)LX/3HK;

    move-result-object v0

    goto :goto_0

    .line 155593
    :pswitch_3
    new-instance v1, LX/3PO;

    const-class v0, LX/3PP;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PP;

    invoke-direct {v1, v0}, LX/3PO;-><init>(LX/3PP;)V

    .line 155594
    move-object v0, v1

    .line 155595
    goto :goto_0

    .line 155596
    :pswitch_4
    new-instance v1, LX/3PQ;

    const-class v0, LX/3PR;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PR;

    invoke-direct {v1, v0}, LX/3PQ;-><init>(LX/3PR;)V

    .line 155597
    move-object v0, v1

    .line 155598
    goto :goto_0

    .line 155599
    :pswitch_5
    new-instance v1, LX/3Kj;

    const-class v0, LX/3Ki;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Ki;

    invoke-direct {v1, v0}, LX/3Kj;-><init>(LX/3Ki;)V

    .line 155600
    move-object v0, v1

    .line 155601
    goto :goto_0

    .line 155602
    :pswitch_6
    new-instance v1, LX/3PS;

    const-class v0, LX/3PT;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PT;

    invoke-direct {v1, v0}, LX/3PS;-><init>(LX/3PT;)V

    .line 155603
    move-object v0, v1

    .line 155604
    goto :goto_0

    .line 155605
    :pswitch_7
    new-instance v1, LX/3PU;

    const-class v0, LX/3PV;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PV;

    invoke-direct {v1, v0}, LX/3PU;-><init>(LX/3PV;)V

    .line 155606
    move-object v0, v1

    .line 155607
    goto :goto_0

    .line 155608
    :pswitch_8
    new-instance v1, LX/3PW;

    const-class v0, LX/3PX;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PX;

    invoke-direct {v1, v0}, LX/3PW;-><init>(LX/3PX;)V

    .line 155609
    move-object v0, v1

    .line 155610
    goto :goto_0

    .line 155611
    :pswitch_9
    new-instance v1, LX/3PY;

    const-class v0, LX/3PZ;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PZ;

    invoke-direct {v1, v0}, LX/3PY;-><init>(LX/3PZ;)V

    .line 155612
    move-object v0, v1

    .line 155613
    goto :goto_0

    .line 155614
    :pswitch_a
    new-instance v1, LX/3Pa;

    const-class v0, LX/3Pb;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pb;

    invoke-direct {v1, v0}, LX/3Pa;-><init>(LX/3Pb;)V

    .line 155615
    move-object v0, v1

    .line 155616
    goto/16 :goto_0

    .line 155617
    :pswitch_b
    new-instance v1, LX/3Pc;

    const-class v0, LX/3Pd;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pd;

    invoke-direct {v1, v0}, LX/3Pc;-><init>(LX/3Pd;)V

    .line 155618
    move-object v0, v1

    .line 155619
    goto/16 :goto_0

    .line 155620
    :pswitch_c
    new-instance v1, LX/3Pe;

    const-class v0, LX/3PT;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PT;

    invoke-direct {v1, v0}, LX/3Pe;-><init>(LX/3PT;)V

    .line 155621
    move-object v0, v1

    .line 155622
    goto/16 :goto_0

    .line 155623
    :pswitch_d
    new-instance v1, LX/3Pf;

    const-class v0, LX/3Pg;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pg;

    invoke-direct {v1, v0}, LX/3Pf;-><init>(LX/3Pg;)V

    .line 155624
    move-object v0, v1

    .line 155625
    goto/16 :goto_0

    .line 155626
    :pswitch_e
    new-instance v1, LX/3Ph;

    const-class v0, LX/3Pg;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pg;

    invoke-direct {v1, v0}, LX/3Ph;-><init>(LX/3Pg;)V

    .line 155627
    move-object v0, v1

    .line 155628
    goto/16 :goto_0

    .line 155629
    :pswitch_f
    new-instance v1, LX/3Pi;

    const-class v0, LX/3Pj;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pj;

    invoke-direct {v1, v0}, LX/3Pi;-><init>(LX/3Pj;)V

    .line 155630
    move-object v0, v1

    .line 155631
    goto/16 :goto_0

    .line 155632
    :pswitch_10
    new-instance v1, LX/3Pk;

    const-class v0, LX/3Pl;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pl;

    invoke-direct {v1, v0}, LX/3Pk;-><init>(LX/3Pl;)V

    .line 155633
    move-object v0, v1

    .line 155634
    goto/16 :goto_0

    .line 155635
    :pswitch_11
    new-instance v1, LX/3Pm;

    const-class v0, LX/3Pn;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pn;

    invoke-direct {v1, v0}, LX/3Pm;-><init>(LX/3Pn;)V

    .line 155636
    const-class v0, LX/3Pn;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pn;

    .line 155637
    iput-object v0, v1, LX/3Pm;->a:LX/3Pn;

    .line 155638
    move-object v0, v1

    .line 155639
    goto/16 :goto_0

    .line 155640
    :pswitch_12
    new-instance v1, LX/3Po;

    const-class v0, LX/3Pp;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Pp;

    invoke-direct {v1, v0}, LX/3Po;-><init>(LX/3Pp;)V

    .line 155641
    move-object v0, v1

    .line 155642
    goto/16 :goto_0

    .line 155643
    :pswitch_13
    new-instance v0, LX/3Pq;

    invoke-direct {v0}, LX/3Pq;-><init>()V

    .line 155644
    move-object v0, v0

    .line 155645
    move-object v0, v0

    .line 155646
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 155647
    const/16 v0, 0x14

    return v0
.end method
