.class public LX/1We;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1We;


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1Wi;",
            "LX/1Wj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Wg;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269271
    invoke-virtual {p1}, LX/1Wg;->d()Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, LX/1We;->a:Ljava/util/EnumMap;

    .line 269272
    return-void
.end method

.method public static a(LX/0QB;)LX/1We;
    .locals 5

    .prologue
    .line 269274
    sget-object v0, LX/1We;->b:LX/1We;

    if-nez v0, :cond_1

    .line 269275
    const-class v1, LX/1We;

    monitor-enter v1

    .line 269276
    :try_start_0
    sget-object v0, LX/1We;->b:LX/1We;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 269277
    if-eqz v2, :cond_0

    .line 269278
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 269279
    new-instance v4, LX/1We;

    .line 269280
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object p0

    check-cast p0, LX/1V7;

    invoke-static {v3, p0}, LX/1Wf;->a(Landroid/content/res/Resources;LX/1V7;)LX/1Wg;

    move-result-object v3

    move-object v3, v3

    .line 269281
    check-cast v3, LX/1Wg;

    invoke-direct {v4, v3}, LX/1We;-><init>(LX/1Wg;)V

    .line 269282
    move-object v0, v4

    .line 269283
    sput-object v0, LX/1We;->b:LX/1We;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269284
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 269285
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 269286
    :cond_1
    sget-object v0, LX/1We;->b:LX/1We;

    return-object v0

    .line 269287
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 269288
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Wi;)LX/1Wj;
    .locals 1

    .prologue
    .line 269273
    iget-object v0, p0, LX/1We;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Wj;

    return-object v0
.end method
