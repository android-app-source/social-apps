.class public LX/1Rx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ry;


# instance fields
.field public a:LX/0P1;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "LX/1Ry;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;

.field private final c:LX/1E1;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Ry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0ad;LX/1E1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1Ry;",
            ">;",
            "LX/0ad;",
            "LX/1E1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247263
    iput-object p1, p0, LX/1Rx;->d:Ljava/util/Set;

    .line 247264
    iput-object p2, p0, LX/1Rx;->b:LX/0ad;

    .line 247265
    iput-object p3, p0, LX/1Rx;->c:LX/1E1;

    .line 247266
    return-void
.end method

.method private declared-synchronized c()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 247267
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 247268
    :goto_0
    monitor-exit p0

    return-void

    .line 247269
    :cond_0
    :try_start_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 247270
    iget-object v0, p0, LX/1Rx;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ry;

    .line 247271
    invoke-interface {v0}, LX/1Ry;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 247272
    invoke-interface {v0}, LX/1Ry;->b()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 247273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 247274
    :cond_2
    :try_start_2
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/1Rx;->a:LX/0P1;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 247275
    invoke-direct {p0}, LX/1Rx;->c()V

    .line 247276
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    new-instance v1, LX/1ke;

    invoke-direct {v1, p0, p1}, LX/1ke;-><init>(LX/1Rx;Z)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 247277
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/1l5;

    invoke-direct {v1, p0}, LX/1l5;-><init>(LX/1Rx;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247278
    invoke-direct {p0}, LX/1Rx;->c()V

    .line 247279
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 247280
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ry;

    invoke-interface {v0, p1}, LX/1Ry;->a(Ljava/lang/Class;)V

    .line 247281
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 247282
    invoke-direct {p0}, LX/1Rx;->c()V

    .line 247283
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 247284
    iget-object v0, p0, LX/1Rx;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ry;

    invoke-interface {v0, p1, p2, p3}, LX/1Ry;->a(Ljava/lang/Class;Ljava/lang/String;Z)V

    .line 247285
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 247286
    iget-object v1, p0, LX/1Rx;->c:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1Rx;->b:LX/0ad;

    sget-short v2, LX/1RY;->t:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247287
    const-class v0, LX/1kK;

    return-object v0
.end method
