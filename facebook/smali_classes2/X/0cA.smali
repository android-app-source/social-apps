.class public LX/0cA;
.super LX/0Q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Q0",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87857
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/0cA;-><init>(I)V

    .line 87858
    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 87859
    invoke-direct {p0, p1}, LX/0Q0;-><init>(I)V

    .line 87860
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Iterable;)LX/0P7;
    .locals 1

    .prologue
    .line 87861
    invoke-virtual {p0, p1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/Iterator;)LX/0P7;
    .locals 1

    .prologue
    .line 87863
    invoke-virtual {p0, p1}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 87862
    invoke-virtual {p0, p1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a()LX/0Py;
    .locals 1

    .prologue
    .line 87855
    invoke-virtual {p0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)LX/0Q0;
    .locals 1

    .prologue
    .line 87856
    invoke-virtual {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 87843
    invoke-virtual {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    return-object v0
.end method

.method public b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87844
    iget v0, p0, LX/0Q0;->b:I

    iget-object v1, p0, LX/0Q0;->a:[Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 87845
    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v1

    iput v1, p0, LX/0cA;->b:I

    .line 87846
    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)LX/0cA;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0cA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87847
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Iterable;)LX/0P7;

    .line 87848
    return-object p0
.end method

.method public b(Ljava/util/Iterator;)LX/0cA;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LX/0cA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87849
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/util/Iterator;)LX/0P7;

    .line 87850
    return-object p0
.end method

.method public varargs b([Ljava/lang/Object;)LX/0cA;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LX/0cA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87851
    invoke-super {p0, p1}, LX/0Q0;->a([Ljava/lang/Object;)LX/0P7;

    .line 87852
    return-object p0
.end method

.method public c(Ljava/lang/Object;)LX/0cA;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/0cA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87853
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Object;)LX/0Q0;

    .line 87854
    return-object p0
.end method
