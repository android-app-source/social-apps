.class public LX/0Q7;
.super LX/0Px;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Px",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final transient b:I

.field private final transient c:I

.field private final transient d:[Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57789
    new-instance v0, LX/0Q7;

    sget-object v1, LX/0P8;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, LX/0Q7;-><init>([Ljava/lang/Object;)V

    sput-object v0, LX/0Q7;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>([Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 57787
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, LX/0Q7;-><init>([Ljava/lang/Object;II)V

    .line 57788
    return-void
.end method

.method private constructor <init>([Ljava/lang/Object;II)V
    .locals 0

    .prologue
    .line 57782
    invoke-direct {p0}, LX/0Px;-><init>()V

    .line 57783
    iput p2, p0, LX/0Q7;->b:I

    .line 57784
    iput p3, p0, LX/0Q7;->c:I

    .line 57785
    iput-object p1, p0, LX/0Q7;->d:[Ljava/lang/Object;

    .line 57786
    return-void
.end method


# virtual methods
.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 3

    .prologue
    .line 57780
    iget-object v0, p0, LX/0Q7;->d:[Ljava/lang/Object;

    iget v1, p0, LX/0Q7;->b:I

    iget v2, p0, LX/0Q7;->c:I

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57781
    iget v0, p0, LX/0Q7;->c:I

    add-int/2addr v0, p2

    return v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 57790
    iget v0, p0, LX/0Q7;->c:I

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 57791
    iget-object v0, p0, LX/0Q7;->d:[Ljava/lang/Object;

    iget v1, p0, LX/0Q7;->b:I

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 2

    .prologue
    .line 57779
    iget v0, p0, LX/0Q7;->c:I

    iget-object v1, p0, LX/0Q7;->d:[Ljava/lang/Object;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final listIterator(I)LX/0Rb;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Rb",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57778
    iget-object v0, p0, LX/0Q7;->d:[Ljava/lang/Object;

    iget v1, p0, LX/0Q7;->b:I

    iget v2, p0, LX/0Q7;->c:I

    invoke-static {v0, v1, v2, p1}, LX/0RZ;->a([Ljava/lang/Object;III)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 57777
    invoke-virtual {p0, p1}, LX/0Q7;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 57776
    iget v0, p0, LX/0Q7;->c:I

    return v0
.end method

.method public final subListUnchecked(II)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57775
    new-instance v0, LX/0Q7;

    iget-object v1, p0, LX/0Q7;->d:[Ljava/lang/Object;

    iget v2, p0, LX/0Q7;->b:I

    add-int/2addr v2, p1

    sub-int v3, p2, p1

    invoke-direct {v0, v1, v2, v3}, LX/0Q7;-><init>([Ljava/lang/Object;II)V

    return-object v0
.end method
