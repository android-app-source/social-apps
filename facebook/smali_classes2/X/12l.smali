.class public final LX/12l;
.super LX/12m;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/12m",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private final map:LX/0P1;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 175729
    invoke-direct {p0}, LX/12m;-><init>()V

    .line 175730
    iput-object p1, p0, LX/12l;->map:LX/0P1;

    .line 175731
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TK;"
        }
    .end annotation

    .prologue
    .line 175738
    iget-object v0, p0, LX/12l;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 175737
    iget-object v0, p0, LX/12l;->map:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 175736
    const/4 v0, 0x1

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 175735
    iget-object v0, p0, LX/12l;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keyIterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 175734
    invoke-virtual {p0}, LX/12l;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 175733
    iget-object v0, p0, LX/12l;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    return v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "serialization"
    .end annotation

    .prologue
    .line 175732
    new-instance v0, LX/4yH;

    iget-object v1, p0, LX/12l;->map:LX/0P1;

    invoke-direct {v0, v1}, LX/4yH;-><init>(LX/0P1;)V

    return-object v0
.end method
