.class public LX/19d;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Z


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0So;

.field public final d:LX/19e;

.field public final e:LX/19g;

.field public final f:LX/19f;

.field private final g:LX/19h;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208166
    const/4 v0, 0x0

    sput-boolean v0, LX/19d;->a:Z

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0So;LX/19e;LX/19f;LX/19g;LX/19h;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;",
            "LX/0So;",
            "LX/19e;",
            "LX/19f;",
            "LX/19g;",
            "LX/19h;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208168
    iput-object p1, p0, LX/19d;->b:LX/0Ot;

    .line 208169
    iput-object p2, p0, LX/19d;->c:LX/0So;

    .line 208170
    iput-object p3, p0, LX/19d;->d:LX/19e;

    .line 208171
    iput-object p4, p0, LX/19d;->f:LX/19f;

    .line 208172
    iput-object p5, p0, LX/19d;->e:LX/19g;

    .line 208173
    iput-object p6, p0, LX/19d;->g:LX/19h;

    .line 208174
    iput-object p7, p0, LX/19d;->h:LX/0Ot;

    .line 208175
    return-void
.end method

.method public static a(LX/0QB;)LX/19d;
    .locals 1

    .prologue
    .line 208176
    invoke-static {p0}, LX/19d;->b(LX/0QB;)LX/19d;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/19d;
    .locals 8

    .prologue
    .line 208177
    new-instance v0, LX/19d;

    const/16 v1, 0x1335

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {p0}, LX/19e;->b(LX/0QB;)LX/19e;

    move-result-object v3

    check-cast v3, LX/19e;

    invoke-static {p0}, LX/19f;->a(LX/0QB;)LX/19f;

    move-result-object v4

    check-cast v4, LX/19f;

    invoke-static {p0}, LX/19g;->a(LX/0QB;)LX/19g;

    move-result-object v5

    check-cast v5, LX/19g;

    invoke-static {p0}, LX/19h;->a(LX/0QB;)LX/19h;

    move-result-object v6

    check-cast v6, LX/19h;

    const/16 v7, 0x259

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/19d;-><init>(LX/0Ot;LX/0So;LX/19e;LX/19f;LX/19g;LX/19h;LX/0Ot;)V

    .line 208178
    return-object v0
.end method


# virtual methods
.method public final a(LX/2p7;)LX/2p8;
    .locals 4

    .prologue
    .line 208179
    new-instance v0, LX/2p8;

    .line 208180
    sget-object v1, LX/2p9;->a:[I

    invoke-virtual {p1}, LX/2p7;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 208181
    iget-object v1, p0, LX/19d;->d:LX/19e;

    :goto_0
    move-object v1, v1

    .line 208182
    iget-object v2, p0, LX/19d;->g:LX/19h;

    iget-object v3, p0, LX/19d;->h:LX/0Ot;

    invoke-direct {v0, v1, v2, v3}, LX/2p8;-><init>(LX/19e;LX/19h;LX/0Ot;)V

    return-object v0

    .line 208183
    :pswitch_0
    iget-object v1, p0, LX/19d;->f:LX/19f;

    goto :goto_0

    .line 208184
    :pswitch_1
    iget-object v1, p0, LX/19d;->e:LX/19g;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
