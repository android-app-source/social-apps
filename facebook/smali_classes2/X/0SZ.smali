.class public final enum LX/0SZ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0R1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0SZ;",
        ">;",
        "LX/0R1",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0SZ;

.field public static final enum INSTANCE:LX/0SZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61469
    new-instance v0, LX/0SZ;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/0SZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    .line 61470
    const/4 v0, 0x1

    new-array v0, v0, [LX/0SZ;

    sget-object v1, LX/0SZ;->INSTANCE:LX/0SZ;

    aput-object v1, v0, v2

    sput-object v0, LX/0SZ;->$VALUES:[LX/0SZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61468
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0SZ;
    .locals 1

    .prologue
    .line 61467
    const-class v0, LX/0SZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0SZ;

    return-object v0
.end method

.method public static values()[LX/0SZ;
    .locals 1

    .prologue
    .line 61466
    sget-object v0, LX/0SZ;->$VALUES:[LX/0SZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0SZ;

    return-object v0
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 61465
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getHash()I
    .locals 1

    .prologue
    .line 61464
    const/4 v0, 0x0

    return v0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61463
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNext()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61462
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61461
    return-object p0
.end method

.method public final getNextInWriteQueue()LX/0R1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61460
    return-object p0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61471
    return-object p0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61450
    return-object p0
.end method

.method public final getValueReference()LX/0Qf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61452
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 61451
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final setAccessTime(J)V
    .locals 0

    .prologue
    .line 61453
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61454
    return-void
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61455
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61456
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61457
    return-void
.end method

.method public final setValueReference(LX/0Qf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61458
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 0

    .prologue
    .line 61459
    return-void
.end method
