.class public final enum LX/0yF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yF;

.field public static final enum BALANCED_POWER_AND_ACCURACY:LX/0yF;

.field public static final enum HIGH_ACCURACY:LX/0yF;

.field public static final enum LOW_POWER:LX/0yF;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164228
    new-instance v0, LX/0yF;

    const-string v1, "LOW_POWER"

    invoke-direct {v0, v1, v2}, LX/0yF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yF;->LOW_POWER:LX/0yF;

    .line 164229
    new-instance v0, LX/0yF;

    const-string v1, "BALANCED_POWER_AND_ACCURACY"

    invoke-direct {v0, v1, v3}, LX/0yF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yF;->BALANCED_POWER_AND_ACCURACY:LX/0yF;

    .line 164230
    new-instance v0, LX/0yF;

    const-string v1, "HIGH_ACCURACY"

    invoke-direct {v0, v1, v4}, LX/0yF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    .line 164231
    const/4 v0, 0x3

    new-array v0, v0, [LX/0yF;

    sget-object v1, LX/0yF;->LOW_POWER:LX/0yF;

    aput-object v1, v0, v2

    sget-object v1, LX/0yF;->BALANCED_POWER_AND_ACCURACY:LX/0yF;

    aput-object v1, v0, v3

    sget-object v1, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    aput-object v1, v0, v4

    sput-object v0, LX/0yF;->$VALUES:[LX/0yF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 164236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/0yF;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 164234
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0yF;->valueOf(Ljava/lang/String;)LX/0yF;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 164235
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yF;
    .locals 1

    .prologue
    .line 164233
    const-class v0, LX/0yF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yF;

    return-object v0
.end method

.method public static values()[LX/0yF;
    .locals 1

    .prologue
    .line 164232
    sget-object v0, LX/0yF;->$VALUES:[LX/0yF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yF;

    return-object v0
.end method
