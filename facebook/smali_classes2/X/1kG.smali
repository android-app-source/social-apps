.class public LX/1kG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/1E1;

.field private final e:LX/0ad;

.field public final f:LX/1bQ;

.field public g:LX/AlX;

.field public final h:LX/1kH;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 309309
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Count(1)"

    aput-object v2, v0, v1

    sput-object v0, LX/1kG;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1E1;LX/1bQ;LX/0ad;LX/1kH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309302
    iput-object p1, p0, LX/1kG;->b:Landroid/content/Context;

    .line 309303
    iput-object p2, p0, LX/1kG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 309304
    iput-object p3, p0, LX/1kG;->d:LX/1E1;

    .line 309305
    iput-object p4, p0, LX/1kG;->f:LX/1bQ;

    .line 309306
    iput-object p5, p0, LX/1kG;->e:LX/0ad;

    .line 309307
    iput-object p6, p0, LX/1kG;->h:LX/1kH;

    .line 309308
    return-void
.end method

.method public static a(LX/0QB;)LX/1kG;
    .locals 10

    .prologue
    .line 309290
    const-class v1, LX/1kG;

    monitor-enter v1

    .line 309291
    :try_start_0
    sget-object v0, LX/1kG;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 309292
    sput-object v2, LX/1kG;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 309293
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309294
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 309295
    new-instance v3, LX/1kG;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v6

    check-cast v6, LX/1E1;

    invoke-static {v0}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object v7

    check-cast v7, LX/1bQ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/1kH;->a(LX/0QB;)LX/1kH;

    move-result-object v9

    check-cast v9, LX/1kH;

    invoke-direct/range {v3 .. v9}, LX/1kG;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1E1;LX/1bQ;LX/0ad;LX/1kH;)V

    .line 309296
    move-object v0, v3

    .line 309297
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 309298
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1kG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309299
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 309300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Ri;)Z
    .locals 2

    .prologue
    .line 309287
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kJ;

    .line 309288
    iget-object v1, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, v1

    .line 309289
    invoke-virtual {v0}, LX/1lR;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLX/1po;)I
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 309280
    invoke-static {p3}, LX/1kH;->a(LX/1po;)Ljava/lang/String;

    move-result-object v3

    .line 309281
    iget-object v0, p0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/1kH;->a:Landroid/net/Uri;

    sget-object v2, LX/1kG;->a:[Ljava/lang/String;

    const-string v5, "%s AND %s > %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v6

    const/4 v3, 0x1

    const-string v8, "date_added"

    aput-object v8, v7, v3

    const/4 v3, 0x2

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, p1, p2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v5, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 309282
    if-nez v1, :cond_0

    move v0, v6

    .line 309283
    :goto_0
    return v0

    .line 309284
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 309285
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 309286
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final b()J
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 309271
    iget-object v0, p0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/1kH;->a:Landroid/net/Uri;

    sget-object v2, LX/1kH;->b:[Ljava/lang/String;

    .line 309272
    const/4 v3, 0x0

    invoke-static {v3}, LX/1kH;->a(LX/1po;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 309273
    const/4 v4, 0x0

    const-string v5, "date_added DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 309274
    if-nez v2, :cond_0

    .line 309275
    :goto_0
    return-wide v6

    .line 309276
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309277
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 309278
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 309279
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    goto :goto_0

    :cond_1
    move-wide v0, v6

    goto :goto_1
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 309257
    iget-object v0, p0, LX/1kG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1kp;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 309258
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 309259
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 309260
    iget-object v1, p0, LX/1kG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 309261
    sget-object v4, LX/1kp;->d:LX/0Tn;

    invoke-interface {v1, v4, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 309262
    sget-object v2, LX/1kp;->e:LX/0Tn;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 309263
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 309264
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 309267
    iget-object v0, p0, LX/1kG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 309268
    sget-object v1, LX/1kp;->e:LX/0Tn;

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 309269
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 309270
    return-void
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 309266
    iget-object v0, p0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 3

    .prologue
    .line 309265
    iget-object v0, p0, LX/1kG;->e:LX/0ad;

    sget-short v1, LX/32h;->k:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1kG;->f()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/1kG;->f()I

    move-result v0

    goto :goto_0
.end method
