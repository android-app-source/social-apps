.class public final LX/0SC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/0SC;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0QB;

.field private c:LX/0SD;

.field private d:Ljava/lang/Byte;

.field private e:LX/0RF;

.field private f:LX/0S7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60889
    new-instance v0, Ljava/util/ArrayDeque;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    sput-object v0, LX/0SC;->b:Ljava/util/ArrayDeque;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0QB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 60891
    if-nez p0, :cond_0

    if-nez p1, :cond_1

    .line 60892
    :cond_0
    const/4 v1, 0x0

    .line 60893
    :goto_0
    return-object v1

    .line 60894
    :cond_1
    sget-object v1, LX/0SC;->b:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 60895
    :try_start_0
    sget-object v0, LX/0SC;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SC;

    .line 60896
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60897
    if-nez v0, :cond_2

    .line 60898
    new-instance v0, LX/0SC;

    invoke-direct {v0}, LX/0SC;-><init>()V

    move-object v1, v0

    .line 60899
    :goto_1
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v0

    iput-object v0, v1, LX/0SC;->c:LX/0SD;

    .line 60900
    iget-object v0, v1, LX/0SC;->c:LX/0SD;

    .line 60901
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/0SD;->b(B)B

    move-result p0

    move v0, p0

    .line 60902
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    iput-object v0, v1, LX/0SC;->d:Ljava/lang/Byte;

    .line 60903
    const-class v0, LX/0RF;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RF;

    iput-object v0, v1, LX/0SC;->e:LX/0RF;

    .line 60904
    iget-object v0, v1, LX/0SC;->e:LX/0RF;

    invoke-virtual {v0}, LX/0RF;->enterScope()LX/0S7;

    move-result-object v0

    iput-object v0, v1, LX/0SC;->f:LX/0S7;

    .line 60905
    invoke-interface {p1}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    iput-object v0, v1, LX/0SC;->a:LX/0QB;

    goto :goto_0

    .line 60906
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60907
    iput-object v2, p0, LX/0SC;->a:LX/0QB;

    .line 60908
    iget-object v0, p0, LX/0SC;->f:LX/0S7;

    if-eqz v0, :cond_0

    .line 60909
    iget-object v0, p0, LX/0SC;->f:LX/0S7;

    invoke-static {v0}, LX/0RF;->a(LX/0S7;)V

    .line 60910
    iput-object v2, p0, LX/0SC;->f:LX/0S7;

    .line 60911
    :cond_0
    iput-object v2, p0, LX/0SC;->e:LX/0RF;

    .line 60912
    iget-object v0, p0, LX/0SC;->d:Ljava/lang/Byte;

    if-eqz v0, :cond_1

    .line 60913
    iget-object v0, p0, LX/0SC;->c:LX/0SD;

    iget-object v1, p0, LX/0SC;->d:Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    .line 60914
    iput-byte v1, v0, LX/0SD;->a:B

    .line 60915
    iput-object v2, p0, LX/0SC;->d:Ljava/lang/Byte;

    .line 60916
    :cond_1
    iput-object v2, p0, LX/0SC;->c:LX/0SD;

    .line 60917
    sget-object v1, LX/0SC;->b:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 60918
    :try_start_0
    sget-object v0, LX/0SC;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p0}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    .line 60919
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
