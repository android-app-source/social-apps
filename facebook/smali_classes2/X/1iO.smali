.class public final LX/1iO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 298355
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1iO;-><init>(II)V

    .line 298356
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 298357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298358
    iput p1, p0, LX/1iO;->a:I

    .line 298359
    iput p2, p0, LX/1iO;->b:I

    .line 298360
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 298361
    if-ne p0, p1, :cond_1

    .line 298362
    :cond_0
    :goto_0
    return v0

    .line 298363
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, LX/1iO;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 298364
    goto :goto_0

    .line 298365
    :cond_3
    check-cast p1, LX/1iO;

    .line 298366
    iget v2, p0, LX/1iO;->a:I

    iget v3, p1, LX/1iO;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/1iO;->b:I

    iget v3, p1, LX/1iO;->b:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 298367
    iget v0, p0, LX/1iO;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/1iO;->b:I

    add-int/2addr v0, v1

    return v0
.end method
