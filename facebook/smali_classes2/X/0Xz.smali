.class public final LX/0Xz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Y4;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Y4;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 79887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79888
    iput-object p1, p0, LX/0Xz;->a:LX/0QB;

    .line 79889
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0Y4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79890
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/0Xz;

    invoke-direct {v2, p0}, LX/0Xz;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79891
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0Xz;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79892
    packed-switch p2, :pswitch_data_0

    .line 79893
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79894
    :pswitch_0
    new-instance v0, LX/0Y5;

    invoke-direct {v0}, LX/0Y5;-><init>()V

    .line 79895
    const/16 v1, 0x245

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 79896
    iput-object v1, v0, LX/0Y5;->b:LX/0Ot;

    .line 79897
    move-object v0, v0

    .line 79898
    :goto_0
    return-object v0

    .line 79899
    :pswitch_1
    invoke-static {p1}, LX/0Y6;->a(LX/0QB;)LX/0Y7;

    move-result-object v0

    goto :goto_0

    .line 79900
    :pswitch_2
    invoke-static {p1}, LX/0Y9;->a(LX/0QB;)LX/0Y9;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 79901
    const/4 v0, 0x3

    return v0
.end method
