.class public LX/1EW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1EX;

.field public c:Landroid/view/View;

.field public d:LX/4nm;

.field public e:I

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 219993
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1EW;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1EX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219995
    iput-object p1, p0, LX/1EW;->a:Landroid/content/Context;

    .line 219996
    iput-object p2, p0, LX/1EW;->b:LX/1EX;

    .line 219997
    return-void
.end method

.method public static a(LX/0QB;)LX/1EW;
    .locals 8

    .prologue
    .line 219998
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 219999
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 220000
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 220001
    if-nez v1, :cond_0

    .line 220002
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220003
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 220004
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 220005
    sget-object v1, LX/1EW;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 220006
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 220007
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 220008
    :cond_1
    if-nez v1, :cond_4

    .line 220009
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 220010
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 220011
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 220012
    new-instance p0, LX/1EW;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/1EX;->b(LX/0QB;)LX/1EX;

    move-result-object v7

    check-cast v7, LX/1EX;

    invoke-direct {p0, v1, v7}, LX/1EW;-><init>(Landroid/content/Context;LX/1EX;)V

    .line 220013
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 220014
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 220015
    if-nez v1, :cond_2

    .line 220016
    sget-object v0, LX/1EW;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EW;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 220017
    :goto_1
    if-eqz v0, :cond_3

    .line 220018
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 220019
    :goto_3
    check-cast v0, LX/1EW;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 220020
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 220021
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 220022
    :catchall_1
    move-exception v0

    .line 220023
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 220024
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 220025
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 220026
    :cond_2
    :try_start_8
    sget-object v0, LX/1EW;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EW;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/1EW;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 219983
    iget-object v0, p0, LX/1EW;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 219984
    const/4 v0, 0x0

    .line 219985
    :goto_0
    return v0

    .line 219986
    :cond_0
    invoke-static {p0, p1}, LX/1EW;->c(LX/1EW;Ljava/lang/String;)V

    .line 219987
    iget-object v0, p0, LX/1EW;->d:LX/4nm;

    invoke-virtual {v0}, LX/4nm;->a()V

    .line 219988
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/1EW;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 219989
    iget-object v0, p0, LX/1EW;->d:LX/4nm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EW;->d:LX/4nm;

    invoke-virtual {v0}, LX/4nm;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219990
    iget-object v0, p0, LX/1EW;->d:LX/4nm;

    invoke-virtual {v0}, LX/4nm;->b()V

    .line 219991
    :cond_0
    iget-object v0, p0, LX/1EW;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, LX/4nm;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/4nm;

    move-result-object v0

    iget-object v1, p0, LX/1EW;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/4nm;->a(I)LX/4nm;

    move-result-object v0

    iput-object v0, p0, LX/1EW;->d:LX/4nm;

    .line 219992
    return-void
.end method
