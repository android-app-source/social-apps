.class public LX/0rT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0rS;

.field public b:Lcom/facebook/api/feedtype/FeedType;

.field public c:I

.field public d:J

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:LX/0gf;

.field public j:Z

.field public k:LX/0rU;

.field public l:Lcom/facebook/api/feed/FeedFetchContext;

.field public m:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/feed/Vpv;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Lcom/facebook/http/interfaces/RequestPriority;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149601
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0rT;->r:Z

    .line 149602
    sget-object v0, LX/0gf;->UNKNOWN:LX/0gf;

    iput-object v0, p0, LX/0rT;->i:LX/0gf;

    .line 149603
    sget-object v0, LX/0rU;->UNSET:LX/0rU;

    iput-object v0, p0, LX/0rT;->k:LX/0rU;

    .line 149604
    sget-object v0, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    iput-object v0, p0, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 149605
    return-void
.end method


# virtual methods
.method public final a(LX/0gf;)LX/0rT;
    .locals 1

    .prologue
    .line 149606
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gf;

    iput-object v0, p0, LX/0rT;->i:LX/0gf;

    .line 149607
    return-object p0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;
    .locals 4

    .prologue
    .line 149608
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 149609
    iput-object v0, p0, LX/0rT;->f:Ljava/lang/String;

    .line 149610
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 149611
    iput-object v0, p0, LX/0rT;->g:Ljava/lang/String;

    .line 149612
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v0, v0

    .line 149613
    iput-object v0, p0, LX/0rT;->a:LX/0rS;

    .line 149614
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 149615
    iput-object v0, p0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 149616
    iget v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v0, v0

    .line 149617
    iput v0, p0, LX/0rT;->c:I

    .line 149618
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 149619
    iput-object v0, p0, LX/0rT;->i:LX/0gf;

    .line 149620
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v0, v0

    .line 149621
    iput-boolean v0, p0, LX/0rT;->j:Z

    .line 149622
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v0, v0

    .line 149623
    iput-object v0, p0, LX/0rT;->k:LX/0rU;

    .line 149624
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v0, v0

    .line 149625
    iput-object v0, p0, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 149626
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 149627
    iput-object v0, p0, LX/0rT;->h:Ljava/lang/String;

    .line 149628
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 149629
    iput-object v0, p0, LX/0rT;->m:Lcom/facebook/common/callercontext/CallerContext;

    .line 149630
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v0, v0

    .line 149631
    iput-object v0, p0, LX/0rT;->n:LX/0Px;

    .line 149632
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v0, v0

    .line 149633
    iput-object v0, p0, LX/0rT;->o:LX/0Px;

    .line 149634
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    move v0, v0

    .line 149635
    iput-boolean v0, p0, LX/0rT;->p:Z

    .line 149636
    iget-wide v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->n:J

    move-wide v0, v2

    .line 149637
    iput-wide v0, p0, LX/0rT;->d:J

    .line 149638
    iget-wide v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    move-wide v0, v2

    .line 149639
    iput-wide v0, p0, LX/0rT;->e:J

    .line 149640
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v0, v0

    .line 149641
    iput-boolean v0, p0, LX/0rT;->r:Z

    .line 149642
    return-object p0
.end method

.method public final t()Lcom/facebook/api/feed/FetchFeedParams;
    .locals 1

    .prologue
    .line 149643
    iget-object v0, p0, LX/0rT;->a:LX/0rS;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149644
    iget v0, p0, LX/0rT;->c:I

    move v0, v0

    .line 149645
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 149646
    iget-object v0, p0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149647
    iget-object v0, p0, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149648
    new-instance v0, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-direct {v0, p0}, Lcom/facebook/api/feed/FetchFeedParams;-><init>(LX/0rT;)V

    return-object v0

    .line 149649
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
