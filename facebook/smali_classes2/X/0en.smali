.class public LX/0en;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0en;


# instance fields
.field private final a:Lcom/facebook/common/file/FileNativeLibrary;


# direct methods
.method public constructor <init>(Lcom/facebook/common/file/FileNativeLibrary;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 104106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104107
    iput-object p1, p0, LX/0en;->a:Lcom/facebook/common/file/FileNativeLibrary;

    .line 104108
    return-void
.end method

.method public static a(LX/0QB;)LX/0en;
    .locals 4

    .prologue
    .line 104093
    sget-object v0, LX/0en;->b:LX/0en;

    if-nez v0, :cond_1

    .line 104094
    const-class v1, LX/0en;

    monitor-enter v1

    .line 104095
    :try_start_0
    sget-object v0, LX/0en;->b:LX/0en;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 104096
    if-eqz v2, :cond_0

    .line 104097
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 104098
    new-instance p0, LX/0en;

    invoke-static {v0}, Lcom/facebook/common/file/FileNativeLibrary;->a(LX/0QB;)Lcom/facebook/common/file/FileNativeLibrary;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/file/FileNativeLibrary;

    invoke-direct {p0, v3}, LX/0en;-><init>(Lcom/facebook/common/file/FileNativeLibrary;)V

    .line 104099
    move-object v0, p0

    .line 104100
    sput-object v0, LX/0en;->b:LX/0en;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104101
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 104102
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104103
    :cond_1
    sget-object v0, LX/0en;->b:LX/0en;

    return-object v0

    .line 104104
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 104105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 104077
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104078
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 104079
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 104080
    new-instance v3, Ljava/io/BufferedInputStream;

    const/16 v4, 0x400

    invoke-direct {v3, v2, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104081
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {p0, p2, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 104082
    new-instance v4, Ljava/io/BufferedOutputStream;

    const/16 v5, 0x400

    invoke-direct {v4, v2, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 104083
    :try_start_2
    invoke-static {v3, v4}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 104084
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104085
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 104086
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :goto_0
    move-object v1, v0

    .line 104087
    :cond_0
    return-object v1

    .line 104088
    :catchall_0
    move-exception v2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 104089
    :catchall_1
    move-exception v2

    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    throw v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 104090
    :catch_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104091
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    move-object v0, v1

    .line 104092
    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 104076
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 104075
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/net/URI;)Ljava/io/File;
    .locals 1

    .prologue
    .line 104109
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    return-object v0
.end method

.method public static a(Ljava/io/File;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 104074
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static a(Ljava/io/File;ILjava/lang/String;J)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 104056
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104057
    :cond_0
    return v1

    .line 104058
    :cond_1
    const/4 v0, 0x0

    .line 104059
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 104060
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 104061
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 104062
    if-eqz v3, :cond_0

    .line 104063
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 104064
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_5

    .line 104065
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 104066
    if-lez p1, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ne p1, v7, :cond_5

    .line 104067
    :cond_3
    if-eqz v0, :cond_4

    .line 104068
    invoke-virtual {v0, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 104069
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 104070
    :cond_4
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v6, v6, p3

    if-ltz v6, :cond_5

    .line 104071
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 104072
    const/4 v1, 0x1

    .line 104073
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/io/File;)Ljava/util/jar/JarFile;
    .locals 1

    .prologue
    .line 104055
    new-instance v0, Ljava/util/jar/JarFile;

    invoke-direct {v0, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 104050
    :try_start_0
    new-instance v0, LX/3AR;

    invoke-direct {v0, p0, p1}, LX/3AR;-><init>(LX/0en;Ljava/io/InputStream;)V

    .line 104051
    const/4 v1, 0x0

    new-array v1, v1, [LX/3AS;

    invoke-static {p2, v1}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1vI;->a(LX/3AU;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104052
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 104053
    return-void

    .line 104054
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public final a(Ljava/io/File;LX/0Rl;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "LX/0Rl",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 104044
    new-instance v1, LX/0fY;

    invoke-direct {v1, p0, p2}, LX/0fY;-><init>(LX/0en;LX/0Rl;)V

    invoke-virtual {p1, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 104045
    if-nez v3, :cond_1

    .line 104046
    :cond_0
    return v0

    .line 104047
    :cond_1
    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    .line 104048
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    and-int/2addr v2, v0

    .line 104049
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public final d(Ljava/io/File;)J
    .locals 13

    .prologue
    .line 104028
    iget-object v0, p0, LX/0en;->a:Lcom/facebook/common/file/FileNativeLibrary;

    invoke-virtual {v0}, LX/03m;->T_()Z

    move-result v0

    if-nez v0, :cond_4

    .line 104029
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 104030
    invoke-virtual {v6, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104031
    const-wide/16 v2, 0x0

    move-wide v4, v2

    .line 104032
    :goto_0
    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 104033
    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 104034
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    array-length v8, v7

    const/4 v2, 0x0

    move v12, v2

    move-wide v2, v4

    move v4, v12

    :goto_1
    if-ge v4, v8, :cond_2

    aget-object v5, v7, v4

    .line 104035
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 104036
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 104037
    invoke-virtual {v6, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104038
    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 104039
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 104040
    invoke-virtual {v5}, Ljava/io/File;->length()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    add-long/2addr v2, v10

    goto :goto_2

    :cond_2
    move-wide v4, v2

    .line 104041
    goto :goto_0

    .line 104042
    :cond_3
    move-wide v0, v4

    .line 104043
    :goto_3
    return-wide v0

    :cond_4
    iget-object v0, p0, LX/0en;->a:Lcom/facebook/common/file/FileNativeLibrary;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/file/FileNativeLibrary;->nativeGetFolderSize(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_3

    :catch_0
    goto :goto_2
.end method
