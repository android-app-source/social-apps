.class public final LX/1I7;
.super LX/1I8;
.source ""


# direct methods
.method private constructor <init>(LX/1I9;Ljava/lang/Character;)V
    .locals 2
    .param p2    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228300
    invoke-direct {p0, p1, p2}, LX/1I8;-><init>(LX/1I9;Ljava/lang/Character;)V

    .line 228301
    iget-object v0, p1, LX/1I9;->f:[C

    array-length v0, v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 228302
    return-void

    .line 228303
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
    .locals 2
    .param p3    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228304
    new-instance v0, LX/1I9;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/1I9;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0, p3}, LX/1I7;-><init>(LX/1I9;Ljava/lang/Character;)V

    .line 228305
    return-void
.end method


# virtual methods
.method public final a([BLjava/lang/CharSequence;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 228306
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228307
    invoke-virtual {p0}, LX/1I8;->a()LX/1IA;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1IA;->trimTrailingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 228308
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1I9;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 228309
    new-instance v0, LX/51z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid input length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/51z;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v0

    .line 228310
    :goto_0
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 228311
    iget-object v2, p0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v4, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {v2, v0}, LX/1I9;->a(C)I

    move-result v0

    shl-int/lit8 v2, v0, 0x12

    .line 228312
    iget-object v5, p0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v0, v3, 0x1

    invoke-interface {v4, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v5, v3}, LX/1I9;->a(C)I

    move-result v3

    shl-int/lit8 v3, v3, 0xc

    or-int/2addr v2, v3

    .line 228313
    add-int/lit8 v3, v1, 0x1

    ushr-int/lit8 v5, v2, 0x10

    int-to-byte v5, v5

    aput-byte v5, p1, v1

    .line 228314
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 228315
    iget-object v5, p0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v1, v0, 0x1

    invoke-interface {v4, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {v5, v0}, LX/1I9;->a(C)I

    move-result v0

    shl-int/lit8 v0, v0, 0x6

    or-int v5, v2, v0

    .line 228316
    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v0, v5, 0x8

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 228317
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 228318
    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v4, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {v3, v1}, LX/1I9;->a(C)I

    move-result v1

    or-int v3, v5, v1

    .line 228319
    add-int/lit8 v1, v2, 0x1

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    goto :goto_0

    .line 228320
    :cond_1
    return v1

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_0
.end method

.method public final a(LX/1I9;Ljava/lang/Character;)LX/1I6;
    .locals 1
    .param p2    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228321
    new-instance v0, LX/1I7;

    invoke-direct {v0, p1, p2}, LX/1I7;-><init>(LX/1I9;Ljava/lang/Character;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Appendable;[BII)V
    .locals 5

    .prologue
    .line 228322
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228323
    add-int v0, p3, p4

    array-length v1, p2

    invoke-static {p3, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    move v0, p4

    move v1, p3

    .line 228324
    :goto_0
    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    .line 228325
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p2, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v1

    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p2, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    .line 228326
    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    ushr-int/lit8 v4, v2, 0x12

    invoke-virtual {v3, v4}, LX/1I9;->a(I)C

    move-result v3

    invoke-interface {p1, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228327
    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    ushr-int/lit8 v4, v2, 0xc

    and-int/lit8 v4, v4, 0x3f

    invoke-virtual {v3, v4}, LX/1I9;->a(I)C

    move-result v3

    invoke-interface {p1, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228328
    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    ushr-int/lit8 v4, v2, 0x6

    and-int/lit8 v4, v4, 0x3f

    invoke-virtual {v3, v4}, LX/1I9;->a(I)C

    move-result v3

    invoke-interface {p1, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228329
    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    and-int/lit8 v2, v2, 0x3f

    invoke-virtual {v3, v2}, LX/1I9;->a(I)C

    move-result v2

    invoke-interface {p1, v2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228330
    add-int/lit8 v0, v0, -0x3

    goto :goto_0

    .line 228331
    :cond_0
    add-int v0, p3, p4

    if-ge v1, v0, :cond_1

    .line 228332
    add-int v0, p3, p4

    sub-int/2addr v0, v1

    invoke-virtual {p0, p1, p2, v1, v0}, LX/1I8;->b(Ljava/lang/Appendable;[BII)V

    .line 228333
    :cond_1
    return-void
.end method
