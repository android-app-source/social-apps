.class public LX/0TJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.util.concurrent.ThreadPoolExecutor._Constructor"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0TJ;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:LX/0Sj;


# direct methods
.method public constructor <init>(LX/0TL;LX/0Sj;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 62790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62791
    iput-object p2, p0, LX/0TJ;->e:LX/0Sj;

    .line 62792
    invoke-virtual {p1}, LX/0TL;->d()I

    move-result v0

    .line 62793
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LX/0TJ;->a:I

    .line 62794
    mul-int/lit8 v1, v0, 0x2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LX/0TJ;->b:I

    .line 62795
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LX/0TJ;->c:I

    .line 62796
    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0TJ;->d:I

    .line 62797
    return-void
.end method

.method public static a(LX/0QB;)LX/0TJ;
    .locals 5

    .prologue
    .line 62777
    sget-object v0, LX/0TJ;->f:LX/0TJ;

    if-nez v0, :cond_1

    .line 62778
    const-class v1, LX/0TJ;

    monitor-enter v1

    .line 62779
    :try_start_0
    sget-object v0, LX/0TJ;->f:LX/0TJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62780
    if-eqz v2, :cond_0

    .line 62781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62782
    new-instance p0, LX/0TJ;

    invoke-static {v0}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v3

    check-cast v3, LX/0TL;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v4

    check-cast v4, LX/0Sj;

    invoke-direct {p0, v3, v4}, LX/0TJ;-><init>(LX/0TL;LX/0Sj;)V

    .line 62783
    move-object v0, p0

    .line 62784
    sput-object v0, LX/0TJ;->f:LX/0TJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62785
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62786
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62787
    :cond_1
    sget-object v0, LX/0TJ;->f:LX/0TJ;

    return-object v0

    .line 62788
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .prologue
    .line 62776
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Fg_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/0TP;->FOREGROUND:LX/0TP;

    invoke-direct {v8, v0, v2}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    move v2, p1

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .prologue
    .line 62775
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "P["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0, p2}, LX/0TO;-><init>(Ljava/lang/String;I)V

    move v2, p1

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 62774
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Ug_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/0TP;->URGENT:LX/0TP;

    invoke-direct {v8, v0, v3}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    move v3, v2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0Xg;
    .locals 10

    .prologue
    .line 62798
    new-instance v1, LX/0Xg;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, LX/0TO;

    sget-object v0, LX/0TP;->NORMAL:LX/0TP;

    invoke-direct {v8, p2, v0}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v9, p0, LX/0TJ;->e:LX/0Sj;

    move v2, p1

    move v3, p1

    invoke-direct/range {v1 .. v9}, LX/0Xg;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    check-cast v0, LX/0Xg;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    .prologue
    .line 62773
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    iget v2, p0, LX/0TJ;->a:I

    const v3, 0x7fffffff

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "Bg_"

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v9, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-direct {v8, v0, v9}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    .prologue
    .line 62772
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    iget v2, p0, LX/0TJ;->b:I

    const v3, 0x7fffffff

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "Norm_"

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v9, LX/0TP;->NORMAL:LX/0TP;

    invoke-direct {v8, v0, v9}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    .prologue
    .line 62771
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    iget v2, p0, LX/0TJ;->b:I

    const v3, 0x7fffffff

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v8, LX/0TO;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "NormN_"

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v9, LX/0TP;->NORMAL_NEW:LX/0TP;

    invoke-direct {v8, v0, v9}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1

    .prologue
    .line 62770
    iget v0, p0, LX/0TJ;->c:I

    invoke-static {p1, v0}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    .prologue
    .line 62767
    iget v0, p0, LX/0TJ;->d:I

    .line 62768
    new-instance v2, Ljava/util/concurrent/ThreadPoolExecutor;

    const v4, 0x7fffffff

    const-wide/16 v5, 0xf

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v8}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v9, LX/0TO;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Ff_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/0TP;->URGENT:LX/0TP;

    invoke-direct {v9, v1, v3}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    move v3, v0

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-static {v2}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    move-object v0, v1

    .line 62769
    return-object v0
.end method

.method public final g(Ljava/lang/String;)LX/0UB;
    .locals 4

    .prologue
    .line 62766
    new-instance v0, LX/0UB;

    const/4 v1, 0x1

    new-instance v2, LX/0TO;

    sget-object v3, LX/0TP;->NORMAL:LX/0TP;

    invoke-direct {v2, p1, v3}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v3, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct {v0, v1, v2, v3}, LX/0UB;-><init>(ILjava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v0}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    check-cast v0, LX/0UB;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)LX/0UB;
    .locals 4

    .prologue
    .line 62765
    new-instance v0, LX/0UB;

    const/4 v1, 0x1

    new-instance v2, LX/0TO;

    sget-object v3, LX/0TP;->URGENT:LX/0TP;

    invoke-direct {v2, p1, v3}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v3, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct {v0, v1, v2, v3}, LX/0UB;-><init>(ILjava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v0}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    check-cast v0, LX/0UB;

    return-object v0
.end method
