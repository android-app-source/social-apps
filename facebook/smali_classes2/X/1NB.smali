.class public LX/1NB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field public volatile b:J

.field public volatile c:J

.field public volatile d:LX/1NE;

.field public e:Z

.field public final f:LX/1ND;

.field public final synthetic g:LX/0tc;


# direct methods
.method public constructor <init>(LX/0tc;LX/1NC;)V
    .locals 1

    .prologue
    .line 237009
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1NB;-><init>(LX/0tc;LX/1ND;Z)V

    .line 237010
    return-void
.end method

.method public constructor <init>(LX/0tc;LX/1ND;Z)V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 236995
    iput-object p1, p0, LX/1NB;->g:LX/0tc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236996
    iput-wide v2, p0, LX/1NB;->b:J

    .line 236997
    iput-wide v2, p0, LX/1NB;->c:J

    .line 236998
    sget-object v0, LX/1NE;->NETWORK:LX/1NE;

    iput-object v0, p0, LX/1NB;->d:LX/1NE;

    .line 236999
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1NB;->e:Z

    .line 237000
    iput-object p2, p0, LX/1NB;->f:LX/1ND;

    .line 237001
    if-nez p3, :cond_0

    .line 237002
    iget-object v1, p1, LX/0tc;->f:Ljava/util/List;

    monitor-enter v1

    .line 237003
    :try_start_0
    iget-wide v2, p1, LX/0tc;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p1, LX/0tc;->e:J

    iput-wide v2, p0, LX/1NB;->a:J

    .line 237004
    iget-object v0, p1, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237005
    monitor-exit v1

    .line 237006
    :goto_0
    return-void

    .line 237007
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 237008
    :cond_0
    iput-wide v2, p0, LX/1NB;->a:J

    goto :goto_0
.end method

.method private a()LX/1NB;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 236980
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v2

    .line 236981
    :try_start_0
    iget-object v0, p0, LX/1NB;->d:LX/1NE;

    sget-object v3, LX/1NE;->NETWORK:LX/1NE;

    if-ne v0, v3, :cond_1

    .line 236982
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-wide v4, v0, LX/0tc;->b:J

    iput-wide v4, p0, LX/1NB;->b:J

    .line 236983
    :goto_0
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    .line 236984
    if-ne v0, p0, :cond_3

    .line 236985
    monitor-exit v2

    move-object v0, v1

    .line 236986
    :goto_1
    return-object v0

    .line 236987
    :cond_1
    iget-object v0, p0, LX/1NB;->d:LX/1NE;

    sget-object v3, LX/1NE;->MEMORY_CACHE:LX/1NE;

    if-ne v0, v3, :cond_2

    .line 236988
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-wide v4, v0, LX/0tc;->d:J

    iput-wide v4, p0, LX/1NB;->b:J

    goto :goto_0

    .line 236989
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 236990
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-wide v4, v0, LX/0tc;->c:J

    iput-wide v4, p0, LX/1NB;->b:J

    goto :goto_0

    .line 236991
    :cond_3
    iget-object v4, v0, LX/1NB;->f:LX/1ND;

    invoke-direct {p0, v4}, LX/1NB;->a(LX/1ND;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/1NB;->f:LX/1ND;

    invoke-interface {v4, p0}, LX/1ND;->a(LX/1NB;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 236992
    monitor-exit v2

    goto :goto_1

    .line 236993
    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 236994
    goto :goto_1
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLResult;LX/1NB;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "LX/1NB;",
            ")",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 236961
    const-string v0, "GraphQLQueryScheduler.updateResultFromBlocker"

    const v3, 0x734d18b8

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 236962
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v0

    .line 236963
    iget-object v0, p1, LX/1NB;->f:LX/1ND;

    invoke-interface {v0, p0}, LX/1ND;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 236964
    sget-object v4, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-ne v0, v4, :cond_2

    .line 236965
    iget-object v0, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v0

    .line 236966
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-eq v4, v0, :cond_0

    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-ne v4, v0, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    const-string v2, "Unexpected blocker condition. Result model = %s, freshness = %s"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-virtual {v4}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-static {v1, v2, v5}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 236967
    iget-object v0, p1, LX/1NB;->f:LX/1ND;

    invoke-interface {v0, p0}, LX/1ND;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 236968
    sget-object v1, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eq v0, v1, :cond_4

    .line 236969
    invoke-static {v0}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    sget-object v1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    .line 236970
    iput-object v1, v0, LX/1lO;->b:LX/0ta;

    .line 236971
    move-object v0, v0

    .line 236972
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 236973
    :cond_2
    :goto_1
    const v1, 0x453dab45

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 236974
    :cond_3
    :try_start_1
    const-string v0, "NULL"

    goto :goto_0

    .line 236975
    :cond_4
    invoke-static {p0}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    sget-object v1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    .line 236976
    iput-object v1, v0, LX/1lO;->b:LX/0ta;

    .line 236977
    move-object v0, v0

    .line 236978
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 236979
    :catchall_0
    move-exception v0

    const v1, 0xbe06046

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(LX/1ND;)Z
    .locals 3

    .prologue
    .line 236957
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    invoke-interface {v0}, LX/1ND;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 236958
    invoke-interface {p1}, LX/1ND;->a()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236959
    const/4 v0, 0x1

    .line 236960
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 11
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 236938
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 236939
    const-wide/16 v2, -0x1

    .line 236940
    const/4 v0, 0x0

    .line 236941
    iget-wide v6, p0, LX/1NB;->c:J

    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-wide v8, v1, LX/0tc;->b:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_6

    move v1, v0

    .line 236942
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 236943
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    .line 236944
    iget-wide v6, v0, LX/1NB;->c:J

    iget-wide v8, p0, LX/1NB;->b:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    iget-wide v6, v0, LX/1NB;->c:J

    iget-wide v8, p0, LX/1NB;->c:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    iget-wide v6, v0, LX/1NB;->c:J

    iget-wide v8, p1, Lcom/facebook/graphql/executor/GraphQLResult;->c:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    instance-of v5, v0, LX/37X;

    if-eqz v5, :cond_0

    iget-wide v6, v0, LX/1NB;->a:J

    iget-wide v8, p1, Lcom/facebook/graphql/executor/GraphQLResult;->b:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    :cond_0
    iget-wide v6, p0, LX/1NB;->c:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    instance-of v5, v0, LX/37X;

    if-eqz v5, :cond_5

    .line 236945
    :cond_1
    iget-object v2, v0, LX/1NB;->f:LX/1ND;

    invoke-direct {p0, v2}, LX/1NB;->a(LX/1ND;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236946
    invoke-static {p1, v0}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/1NB;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    .line 236947
    add-int/lit8 v1, v1, 0x1

    .line 236948
    :cond_2
    iget-wide v2, v0, LX/1NB;->c:J

    move v0, v1

    :goto_1
    move v1, v0

    .line 236949
    goto :goto_0

    :cond_3
    move v5, v1

    move-object v0, p1

    .line 236950
    :goto_2
    const-wide/16 v6, -0x1

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 236951
    invoke-static {v0}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    .line 236952
    iput-wide v2, v0, LX/1lO;->h:J

    .line 236953
    move-object v0, v0

    .line 236954
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    move-object v6, v0

    .line 236955
    :goto_3
    const/16 v3, 0x9a

    const-string v4, "apply_later_finished_count"

    move-object v0, p2

    move v1, p3

    move v2, p4

    invoke-static/range {v0 .. v5}, LX/0tc;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;IISLjava/lang/String;I)V

    .line 236956
    return-object v6

    :cond_4
    move-object v6, v0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v5, v0

    move-object v0, p1

    goto :goto_2
.end method


# virtual methods
.method public a(LX/1NE;)LX/1NB;
    .locals 1

    .prologue
    .line 236936
    iput-object p1, p0, LX/1NB;->d:LX/1NE;

    .line 236937
    invoke-direct {p0}, LX/1NB;->a()LX/1NB;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 236935
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 10
    .param p2    # Lcom/facebook/quicklog/QuickPerformanceLogger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 236803
    if-eqz p1, :cond_0

    .line 236804
    iget-boolean v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    move v1, v1

    .line 236805
    if-eqz v1, :cond_0

    .line 236806
    iget-object v1, p0, LX/1NB;->f:LX/1ND;

    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ND;->a(Ljava/util/Set;)LX/1ND;

    .line 236807
    :cond_0
    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-object v1, v1, LX/0tc;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 236808
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/1NB;->e:Z

    .line 236809
    iget-object v4, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v4

    .line 236810
    :try_start_0
    iget-object v1, p0, LX/1NB;->d:LX/1NE;

    sget-object v2, LX/1NE;->NETWORK:LX/1NE;

    if-ne v1, v2, :cond_1

    .line 236811
    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-wide v2, v1, LX/0tc;->b:J

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    iput-wide v2, v1, LX/0tc;->b:J

    .line 236812
    :cond_1
    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-wide v2, v1, LX/0tc;->b:J

    iput-wide v2, p0, LX/1NB;->c:J

    .line 236813
    if-eqz p1, :cond_8

    .line 236814
    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-object v1, v1, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    move v1, v0

    move-object v2, p1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    .line 236815
    iget-wide v6, v0, LX/1NB;->c:J

    iget-wide v8, p0, LX/1NB;->b:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_7

    .line 236816
    iget-object v6, v0, LX/1NB;->f:LX/1ND;

    invoke-direct {p0, v6}, LX/1NB;->a(LX/1ND;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 236817
    invoke-static {v2, v0}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/1NB;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 236818
    iget-object v6, v0, LX/1NB;->f:LX/1ND;

    instance-of v6, v6, LX/3Bw;

    move v0, v6

    .line 236819
    if-eqz v0, :cond_2

    .line 236820
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 236821
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move-object v1, v2

    :goto_1
    move-object v2, v1

    move v1, v0

    .line 236822
    goto :goto_0

    :cond_3
    move v0, v3

    move-object p1, v2

    .line 236823
    :goto_2
    iget-object v2, p0, LX/1NB;->d:LX/1NE;

    sget-object v3, LX/1NE;->NETWORK:LX/1NE;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 236824
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236825
    :cond_4
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 236826
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 236827
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 236828
    :cond_5
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 236829
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 236830
    :cond_6
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    const v3, 0x186ab0d6

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 236831
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236832
    const/16 v3, 0x99

    const-string v4, "start_callback_count"

    add-int v5, v1, v0

    move-object v0, p2

    move v1, p3

    move v2, p4

    invoke-static/range {v0 .. v5}, LX/0tc;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;IISLjava/lang/String;I)V

    .line 236833
    return-object p1

    .line 236834
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_8
    move v1, v0

    goto :goto_2
.end method

.method public final a(I)V
    .locals 10

    .prologue
    .line 236911
    iget-object v0, p0, LX/1NB;->d:LX/1NE;

    sget-object v1, LX/1NE;->NETWORK:LX/1NE;

    if-ne v0, v1, :cond_0

    .line 236912
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->i:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 236913
    :goto_0
    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-object v1, v1, LX/0tc;->h:LX/0t5;

    iget-object v2, p0, LX/1NB;->f:LX/1ND;

    invoke-interface {v2}, LX/1ND;->a()LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0}, LX/0t5;->a(ILjava/util/Set;I)V

    .line 236914
    :cond_0
    invoke-virtual {p0}, LX/1NB;->b()V

    .line 236915
    iget-object v4, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v4

    .line 236916
    :try_start_0
    iget-object v0, p0, LX/1NB;->d:LX/1NE;

    sget-object v1, LX/1NE;->NETWORK:LX/1NE;

    if-ne v0, v1, :cond_1

    .line 236917
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-wide v2, v1, LX/0tc;->c:J

    iget-wide v6, p0, LX/1NB;->c:J

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, v0, LX/0tc;->c:J

    .line 236918
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v1, p0, LX/1NB;->g:LX/0tc;

    iget-wide v2, v1, LX/0tc;->d:J

    iget-wide v6, p0, LX/1NB;->c:J

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, v0, LX/0tc;->d:J

    .line 236919
    :cond_1
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 236920
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 236921
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 236922
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    const v1, 0x1ee7117

    invoke-static {v0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 236923
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-wide v0, v0, LX/0tc;->c:J

    .line 236924
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    iget-object v2, v2, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    .line 236925
    iget-wide v6, v0, LX/1NB;->b:J

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    .line 236926
    iget-wide v0, v0, LX/1NB;->b:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 236927
    goto :goto_1

    .line 236928
    :cond_3
    const/4 v0, 0x3

    goto :goto_0

    .line 236929
    :cond_4
    :goto_2
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_5

    .line 236930
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 236931
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 236932
    :cond_5
    :goto_3
    :try_start_1
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    iget-wide v0, v0, LX/1NB;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_6

    .line 236933
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 236934
    :cond_6
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 14
    .param p2    # Lcom/facebook/quicklog/QuickPerformanceLogger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 236891
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 236892
    invoke-static/range {p0 .. p4}, LX/1NB;->c(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 236893
    const-wide/16 v0, -0x1

    .line 236894
    const/4 v5, 0x0

    move-wide v12, v0

    move-object v1, v2

    move-wide v2, v12

    .line 236895
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 236896
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NB;

    .line 236897
    instance-of v4, v0, LX/37X;

    if-eqz v4, :cond_4

    iget-wide v8, v0, LX/1NB;->a:J

    iget-wide v10, v1, Lcom/facebook/graphql/executor/GraphQLResult;->b:J

    cmp-long v4, v8, v10

    if-lez v4, :cond_4

    .line 236898
    invoke-virtual {v1}, Lcom/facebook/graphql/executor/GraphQLResult;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 236899
    iget-object v4, v0, LX/1NB;->f:LX/1ND;

    invoke-direct {p0, v4}, LX/1NB;->a(LX/1ND;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 236900
    iget-object v4, v0, LX/1NB;->f:LX/1ND;

    invoke-interface {v4, v1}, LX/1ND;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 236901
    invoke-virtual {v4}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    sget-object v7, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eq v1, v7, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 236902
    add-int/lit8 v5, v5, 0x1

    move-object v1, v4

    .line 236903
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/fbservice/results/BaseResult;->getFreshness()LX/0ta;

    move-result-object v4

    sget-object v7, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-eq v4, v7, :cond_4

    .line 236904
    iget-wide v2, v0, LX/1NB;->a:J

    move-wide v12, v2

    move-object v2, v1

    move-wide v0, v12

    :goto_2
    move-wide v12, v0

    move-object v1, v2

    move-wide v2, v12

    .line 236905
    goto :goto_0

    .line 236906
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 236907
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-eqz v0, :cond_3

    .line 236908
    invoke-static {v1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/1lO;->b(J)LX/1lO;

    move-result-object v0

    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    move-object v6, v1

    .line 236909
    :goto_3
    const/16 v3, 0x98

    const-string v4, "apply_all_ops_count"

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static/range {v0 .. v5}, LX/0tc;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;IISLjava/lang/String;I)V

    .line 236910
    return-object v6

    :cond_3
    move-object v6, v1

    goto :goto_3

    :cond_4
    move-wide v12, v2

    move-object v2, v1

    move-wide v0, v12

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 236887
    iget-boolean v0, p0, LX/1NB;->e:Z

    if-eqz v0, :cond_0

    .line 236888
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 236889
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1NB;->e:Z

    .line 236890
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 236886
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, LX/1NB;->b(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 236872
    const/4 v1, 0x0

    .line 236873
    :try_start_0
    invoke-virtual {p0}, LX/1NB;->b()V

    .line 236874
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 236875
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 236876
    :try_start_2
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    const v3, 0x29495ea4

    invoke-static {v0, v3}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 236877
    :catch_0
    const/4 v1, 0x1

    .line 236878
    goto :goto_0

    .line 236879
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236880
    if-eqz v1, :cond_1

    .line 236881
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 236882
    :cond_1
    return-void

    .line 236883
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 236884
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_2

    .line 236885
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_2
    throw v0
.end method

.method public final d()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 236854
    :try_start_0
    iget-boolean v2, p0, LX/1NB;->e:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 236855
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 236856
    :try_start_1
    iget-object v3, p0, LX/1NB;->d:LX/1NE;

    sget-object v4, LX/1NE;->NETWORK:LX/1NE;

    if-ne v3, v4, :cond_0

    .line 236857
    iget-object v3, p0, LX/1NB;->g:LX/0tc;

    iget-object v4, p0, LX/1NB;->g:LX/0tc;

    iget-wide v4, v4, LX/0tc;->d:J

    iget-wide v6, p0, LX/1NB;->c:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, v3, LX/0tc;->d:J

    .line 236858
    :cond_0
    iget-object v3, p0, LX/1NB;->g:LX/0tc;

    iget-object v3, v3, LX/0tc;->m:Ljava/util/LinkedList;

    invoke-virtual {v3, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 236859
    iget-object v3, p0, LX/1NB;->g:LX/0tc;

    const v4, -0x360a0fcc    # -2014726.5f

    invoke-static {v3, v4}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 236860
    :goto_1
    iget-object v3, p0, LX/1NB;->g:LX/0tc;

    iget-object v3, v3, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-eq v3, p0, :cond_2

    .line 236861
    :try_start_2
    iget-object v3, p0, LX/1NB;->g:LX/0tc;

    const v4, 0x147abb17

    invoke-static {v3, v4}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 236862
    :catch_0
    move v1, v0

    .line 236863
    goto :goto_1

    :cond_1
    move v2, v1

    .line 236864
    goto :goto_0

    .line 236865
    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236866
    if-eqz v1, :cond_3

    .line 236867
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 236868
    :cond_3
    return-void

    .line 236869
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 236870
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_4

    .line 236871
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_4
    throw v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 236852
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/1NB;->a(I)V

    .line 236853
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 236835
    const/4 v1, 0x0

    .line 236836
    :try_start_0
    iget-object v2, p0, LX/1NB;->g:LX/0tc;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 236837
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    instance-of v0, v0, LX/3Bw;

    if-eqz v0, :cond_4

    .line 236838
    iget-object v0, p0, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/3Bw;

    iget-boolean v0, v0, LX/3Bw;->b:Z

    if-eqz v0, :cond_4

    .line 236839
    const/4 v0, 0x1

    .line 236840
    :goto_1
    move v0, v0

    .line 236841
    if-nez v0, :cond_1

    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->l:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    iget-object v0, v0, LX/0tc;->f:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 236842
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/1NB;->g:LX/0tc;

    const v3, 0x7048ff76

    invoke-static {v0, v3}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 236843
    :catch_0
    const/4 v1, 0x1

    .line 236844
    goto :goto_0

    .line 236845
    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236846
    if-eqz v1, :cond_2

    .line 236847
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 236848
    :cond_2
    return-void

    .line 236849
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 236850
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3

    .line 236851
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_3
    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
