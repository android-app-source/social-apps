.class public LX/0Tk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Tl;


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field private final b:LX/0Tq;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Random;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferencesDbUpgradeStep;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63518
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    const-string v1, "version"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0Tk;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Tq;LX/0Ot;Ljava/util/Random;LX/0Or;)V
    .locals 0
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tq;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferencesDbUpgradeStep;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63520
    iput-object p1, p0, LX/0Tk;->b:LX/0Tq;

    .line 63521
    iput-object p2, p0, LX/0Tk;->c:LX/0Ot;

    .line 63522
    iput-object p3, p0, LX/0Tk;->d:Ljava/util/Random;

    .line 63523
    iput-object p4, p0, LX/0Tk;->e:LX/0Or;

    .line 63524
    return-void
.end method

.method private b(Ljava/util/Map;)V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63525
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63526
    :cond_0
    :goto_0
    return-void

    .line 63527
    :cond_1
    sget-object v0, LX/0Tk;->a:LX/0Tn;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 63528
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 63529
    :goto_1
    const/4 v1, 0x2

    move v2, v1

    .line 63530
    if-eq v0, v2, :cond_0

    .line 63531
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v3

    .line 63532
    iget-object v1, p0, LX/0Tk;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/87d;

    .line 63533
    iget v5, v1, LX/87d;->d:I

    move v5, v5

    .line 63534
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5, v1}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_2

    .line 63535
    :cond_2
    move-object v3, v3

    .line 63536
    move v1, v0

    .line 63537
    :goto_3
    if-ge v1, v2, :cond_9

    .line 63538
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87d;

    .line 63539
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 63540
    iget-object v5, v0, LX/87d;->a:Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-interface {v5}, Lcom/facebook/gk/store/GatekeeperWriter;->e()LX/2LD;

    move-result-object v8

    .line 63541
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 63542
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Tn;

    .line 63543
    iget-object v10, v0, LX/87d;->c:LX/0Tn;

    invoke-virtual {v6, v10}, LX/0To;->a(LX/0To;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 63544
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/Boolean;

    if-eqz v10, :cond_4

    .line 63545
    iget-object v10, v0, LX/87d;->c:LX/0Tn;

    invoke-virtual {v6, v10}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v10

    .line 63546
    iget-object v11, v0, LX/87d;->b:LX/0Uh;

    invoke-virtual {v11, v10}, LX/0Uh;->b(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 63547
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static {v5}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v5

    invoke-interface {v8, v10, v5}, LX/2LD;->a(Ljava/lang/String;LX/03R;)LX/2LD;

    .line 63548
    :cond_4
    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 63549
    :cond_5
    invoke-interface {v8}, LX/2LD;->a()V

    .line 63550
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Tn;

    .line 63551
    invoke-interface {p1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 63552
    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p0, v5, v7}, LX/0Tk;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 63553
    invoke-interface {v7}, Ljava/util/Set;->size()I

    .line 63554
    goto/16 :goto_4

    .line 63555
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 63556
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 63557
    :cond_9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 63558
    sget-object v1, LX/0Tk;->a:LX/0Tn;

    .line 63559
    const/4 v2, 0x2

    move v2, v2

    .line 63560
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63561
    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 63562
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0Tk;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 63563
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 63564
    :try_start_0
    const-string v0, "FbSharedPreferencesDbStorage.queryDb"

    const v1, -0x3559fb07    # -5440124.5f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 63565
    :try_start_1
    iget-object v0, p0, LX/0Tk;->b:LX/0Tq;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "preferences"

    sget-object v2, LX/0be;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 63566
    const v0, 0x41c0766a

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 63567
    if-nez v1, :cond_2

    .line 63568
    iget-object v0, p0, LX/0Tk;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FbSharedPreferencesDbStorage_NULL_CURSOR"

    const-string v3, "Null cursor."

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 63569
    if-eqz v1, :cond_0

    .line 63570
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 63571
    :cond_0
    :goto_0
    return-void

    .line 63572
    :catchall_0
    move-exception v0

    const v1, 0x142b8ab6

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 63573
    :catchall_1
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_1

    .line 63574
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 63575
    :cond_2
    :try_start_4
    const-string v0, "FbSharedPreferencesDbStorage.loadPrefsFromCursor"

    const v2, -0x277301a4

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 63576
    :try_start_5
    invoke-static {v1, p1}, LX/0bf;->a(Landroid/database/Cursor;Ljava/util/Map;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 63577
    const v0, 0x64462675

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 63578
    if-eqz v1, :cond_3

    .line 63579
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 63580
    :cond_3
    invoke-direct {p0, p1}, LX/0Tk;->b(Ljava/util/Map;)V

    goto :goto_0

    .line 63581
    :catchall_2
    move-exception v0

    const v2, 0xdfa8bb0

    :try_start_7
    invoke-static {v2}, LX/02m;->a(I)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 63582
    :catchall_3
    move-exception v0

    goto :goto_1
.end method

.method public final a(Ljava/util/Map;Ljava/util/Collection;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/0Tn;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63583
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63584
    :cond_0
    return-void

    .line 63585
    :cond_1
    iget-object v0, p0, LX/0Tk;->d:Ljava/util/Random;

    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-gtz v0, :cond_5

    move v0, v1

    .line 63586
    :goto_0
    iget-object v3, p0, LX/0Tk;->b:LX/0Tq;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    move v5, v2

    move v3, v2

    move-object v2, v4

    .line 63587
    :goto_1
    const/16 v7, 0xa

    if-ge v5, v7, :cond_7

    .line 63588
    const-string v2, "FbSharedPreferencesDbStorage.writePrefChangesAttempt"

    const v7, 0x1a2f94dd

    invoke-static {v2, v7}, LX/02m;->a(Ljava/lang/String;I)V

    .line 63589
    const v2, 0x6490b62c

    invoke-static {v6, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 63590
    if-nez v3, :cond_3

    .line 63591
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 63592
    :cond_2
    move v3, v1

    .line 63593
    :cond_3
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 63594
    :cond_4
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63595
    const v0, 0x4923839e    # 669753.9f

    invoke-static {v6, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 63596
    const v0, -0x7dd156b0

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v4

    .line 63597
    :goto_2
    if-eqz v0, :cond_0

    .line 63598
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5
    move v0, v2

    .line 63599
    goto :goto_0

    .line 63600
    :catch_0
    move-exception v2

    .line 63601
    if-eqz v0, :cond_6

    .line 63602
    :try_start_1
    const-string v7, "Writing preferences failed."

    .line 63603
    const-string v8, "FbSharedPreferencesDbStorage_PROVIDER_SQLITE_EXCEPTION"

    .line 63604
    const-string v9, "Attempt #%d. %s."

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v10, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 63605
    invoke-static {v8, v9}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v9

    const/4 v10, 0x1

    .line 63606
    iput v10, v9, LX/0VK;->e:I

    .line 63607
    move-object v9, v9

    .line 63608
    iput-object v2, v9, LX/0VK;->c:Ljava/lang/Throwable;

    .line 63609
    move-object v9, v9

    .line 63610
    invoke-virtual {v9}, LX/0VK;->g()LX/0VG;

    move-result-object v10

    .line 63611
    iget-object v9, p0, LX/0Tk;->c:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-virtual {v9, v10}, LX/03V;->a(LX/0VG;)V

    .line 63612
    :cond_6
    const-wide/16 v9, 0x1e
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63613
    :goto_3
    const v7, 0x21d0e52b

    invoke-static {v6, v7}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 63614
    const v7, 0x6bda05a

    invoke-static {v7}, LX/02m;->a(I)V

    .line 63615
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 63616
    :catchall_0
    move-exception v0

    const v1, 0x386131a6

    invoke-static {v6, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 63617
    const v1, 0x3abdce04

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_7
    move-object v0, v2

    goto :goto_2

    .line 63618
    :cond_8
    :try_start_3
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 63619
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 63620
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Tn;

    .line 63621
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 63622
    invoke-static {v8, v7, v2}, LX/0bf;->a(Landroid/content/ContentValues;LX/0Tn;Ljava/lang/Object;)V

    .line 63623
    const-string v2, "preferences"

    const/4 v7, 0x0

    const v10, -0x6854f844

    invoke-static {v10}, LX/03h;->a(I)V

    invoke-virtual {v6, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, 0x508bf7e4

    invoke-static {v2}, LX/03h;->a(I)V

    goto :goto_4
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63624
    :cond_9
    :try_start_4
    const/4 v2, 0x1

    new-array v7, v2, [Ljava/lang/String;

    .line 63625
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Tn;

    .line 63626
    const/4 v9, 0x0

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v9

    .line 63627
    const-string v2, "preferences"

    const-string v9, "key = ?"

    invoke-virtual {v6, v2, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 63628
    :catch_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_3
.end method
