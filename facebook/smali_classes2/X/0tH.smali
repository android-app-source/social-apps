.class public LX/0tH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0tH;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0ad;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154251
    iput-object p1, p0, LX/0tH;->a:Landroid/content/Context;

    .line 154252
    iput-object p2, p0, LX/0tH;->b:LX/0ad;

    .line 154253
    iput-object p3, p0, LX/0tH;->c:LX/0Uh;

    .line 154254
    return-void
.end method

.method public static a(LX/0QB;)LX/0tH;
    .locals 6

    .prologue
    .line 154255
    sget-object v0, LX/0tH;->d:LX/0tH;

    if-nez v0, :cond_1

    .line 154256
    const-class v1, LX/0tH;

    monitor-enter v1

    .line 154257
    :try_start_0
    sget-object v0, LX/0tH;->d:LX/0tH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154258
    if-eqz v2, :cond_0

    .line 154259
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154260
    new-instance p0, LX/0tH;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/0tH;-><init>(Landroid/content/Context;LX/0ad;LX/0Uh;)V

    .line 154261
    move-object v0, p0

    .line 154262
    sput-object v0, LX/0tH;->d:LX/0tH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154263
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154264
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154265
    :cond_1
    sget-object v0, LX/0tH;->d:LX/0tH;

    return-object v0

    .line 154266
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 3

    .prologue
    .line 154268
    iget-object v0, p0, LX/0tH;->b:LX/0ad;

    sget-short v1, LX/1sg;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 154269
    iget-object v0, p0, LX/0tH;->b:LX/0ad;

    sget-short v1, LX/1sg;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 154270
    iget-object v0, p0, LX/0tH;->b:LX/0ad;

    sget-short v1, LX/1sg;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final f()LX/1zu;
    .locals 2

    .prologue
    .line 154271
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/0tH;->a:Landroid/content/Context;

    invoke-static {v0}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x7de

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 154272
    if-eqz v0, :cond_0

    sget-object v0, LX/1zu;->VECTOR_ANIMATED_ALWAYS:LX/1zu;

    :goto_1
    return-object v0

    :cond_0
    sget-object v0, LX/1zu;->IMAGE:LX/1zu;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
