.class public LX/0eE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0eF;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0eE;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0W9;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91603
    iput-object p1, p0, LX/0eE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 91604
    iput-object p2, p0, LX/0eE;->b:LX/0W9;

    .line 91605
    return-void
.end method

.method public static a(LX/0QB;)LX/0eE;
    .locals 5

    .prologue
    .line 91588
    sget-object v0, LX/0eE;->c:LX/0eE;

    if-nez v0, :cond_1

    .line 91589
    const-class v1, LX/0eE;

    monitor-enter v1

    .line 91590
    :try_start_0
    sget-object v0, LX/0eE;->c:LX/0eE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91591
    if-eqz v2, :cond_0

    .line 91592
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91593
    new-instance p0, LX/0eE;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {p0, v3, v4}, LX/0eE;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0W9;)V

    .line 91594
    move-object v0, p0

    .line 91595
    sput-object v0, LX/0eE;->c:LX/0eE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91596
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91597
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91598
    :cond_1
    sget-object v0, LX/0eE;->c:LX/0eE;

    return-object v0

    .line 91599
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 91601
    const-string v0, "device"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 91587
    iget-object v0, p0, LX/0eE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eC;->b:LX/0Tn;

    const-string v2, "device"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Locale;
    .locals 3

    .prologue
    .line 91578
    iget-object v0, p0, LX/0eE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91579
    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v0

    .line 91580
    :goto_0
    return-object v0

    .line 91581
    :cond_0
    invoke-direct {p0}, LX/0eE;->c()Ljava/lang/String;

    move-result-object v0

    .line 91582
    invoke-static {v0}, LX/0eE;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91583
    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0

    .line 91584
    :cond_1
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 91585
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91586
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 91577
    invoke-direct {p0}, LX/0eE;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0eE;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
