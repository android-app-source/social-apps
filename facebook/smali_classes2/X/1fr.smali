.class public LX/1fr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 292385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292386
    instance-of v0, p0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    .line 292387
    check-cast p0, Lcom/facebook/graphql/model/FeedUnit;

    .line 292388
    :goto_0
    return-object p0

    .line 292389
    :cond_0
    instance-of v0, p0, LX/0jQ;

    if-eqz v0, :cond_1

    .line 292390
    check-cast p0, LX/0jQ;

    invoke-interface {p0}, LX/0jQ;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    goto :goto_0

    .line 292391
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292392
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_0

    .line 292393
    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 292394
    :goto_0
    return-object p0

    .line 292395
    :cond_0
    instance-of v0, p0, LX/1Rk;

    if-eqz v0, :cond_1

    .line 292396
    check-cast p0, LX/1Rk;

    .line 292397
    iget-object v0, p0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object p0, v0

    .line 292398
    goto :goto_0

    .line 292399
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
