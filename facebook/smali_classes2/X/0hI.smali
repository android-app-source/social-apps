.class public LX/0hI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0hI;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 115634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115635
    const/4 v0, -0x1

    iput v0, p0, LX/0hI;->b:I

    .line 115636
    iput-object p1, p0, LX/0hI;->a:Landroid/content/res/Resources;

    .line 115637
    return-void
.end method

.method public static a(LX/0QB;)LX/0hI;
    .locals 4

    .prologue
    .line 115658
    sget-object v0, LX/0hI;->c:LX/0hI;

    if-nez v0, :cond_1

    .line 115659
    const-class v1, LX/0hI;

    monitor-enter v1

    .line 115660
    :try_start_0
    sget-object v0, LX/0hI;->c:LX/0hI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 115661
    if-eqz v2, :cond_0

    .line 115662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 115663
    new-instance p0, LX/0hI;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/0hI;-><init>(Landroid/content/res/Resources;)V

    .line 115664
    move-object v0, p0

    .line 115665
    sput-object v0, LX/0hI;->c:LX/0hI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115666
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 115667
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 115668
    :cond_1
    sget-object v0, LX/0hI;->c:LX/0hI;

    return-object v0

    .line 115669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 115670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/Window;)I
    .locals 4

    .prologue
    .line 115638
    iget v0, p0, LX/0hI;->b:I

    if-lez v0, :cond_0

    .line 115639
    iget v0, p0, LX/0hI;->b:I

    .line 115640
    :goto_0
    return v0

    .line 115641
    :cond_0
    iget-object v0, p0, LX/0hI;->a:Landroid/content/res/Resources;

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 115642
    if-lez v0, :cond_1

    .line 115643
    iget-object v1, p0, LX/0hI;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 115644
    if-lez v0, :cond_1

    .line 115645
    iput v0, p0, LX/0hI;->b:I

    .line 115646
    iget v0, p0, LX/0hI;->b:I

    goto :goto_0

    .line 115647
    :cond_1
    if-eqz p1, :cond_2

    .line 115648
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 115649
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 115650
    iget v1, v0, Landroid/graphics/Rect;->top:I

    if-lez v1, :cond_2

    .line 115651
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, LX/0hI;->b:I

    .line 115652
    iget v0, p0, LX/0hI;->b:I

    goto :goto_0

    .line 115653
    :cond_2
    const/high16 v0, 0x41c80000    # 25.0f

    .line 115654
    iget-object v1, p0, LX/0hI;->a:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 115655
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    move v0, v1

    .line 115656
    iput v0, p0, LX/0hI;->b:I

    .line 115657
    iget v0, p0, LX/0hI;->b:I

    goto :goto_0
.end method
