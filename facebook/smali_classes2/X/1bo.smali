.class public LX/1bo;
.super LX/1bp;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1bp",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/1c3;

.field private final d:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "LX/1c8;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/1bh;

.field private g:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public h:Z

.field private final i:LX/1c8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281090
    const-class v0, LX/1bo;

    sput-object v0, LX/1bo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1br;LX/1c3;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;LX/1c9;)V
    .locals 1
    .param p10    # LX/1c9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1br;",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            "Ljava/util/concurrent/Executor;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Ljava/lang/Object;",
            "LX/1c9",
            "<",
            "LX/1c8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281081
    invoke-direct {p0, p2, p4, p7, p9}, LX/1bp;-><init>(LX/1br;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Object;)V

    .line 281082
    new-instance v0, LX/1cA;

    invoke-direct {v0, p0}, LX/1cA;-><init>(LX/1bo;)V

    iput-object v0, p0, LX/1bo;->i:LX/1c8;

    .line 281083
    iput-object p1, p0, LX/1bo;->b:Landroid/content/res/Resources;

    .line 281084
    iput-object p3, p0, LX/1bo;->c:LX/1c3;

    .line 281085
    iput-object p5, p0, LX/1bo;->e:LX/1Fh;

    .line 281086
    iput-object p8, p0, LX/1bo;->f:LX/1bh;

    .line 281087
    iput-object p10, p0, LX/1bo;->d:LX/1c9;

    .line 281088
    invoke-direct {p0, p6}, LX/1bo;->a(LX/1Gd;)V

    .line 281089
    return-void
.end method

.method private a(LX/1Gd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 281078
    iput-object p1, p0, LX/1bo;->g:LX/1Gd;

    .line 281079
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1bo;->a(LX/1ln;)V

    .line 281080
    return-void
.end method

.method private a(LX/1ln;)V
    .locals 3
    .param p1    # LX/1ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281059
    iget-boolean v0, p0, LX/1bo;->h:Z

    if-nez v0, :cond_1

    .line 281060
    :cond_0
    :goto_0
    return-void

    .line 281061
    :cond_1
    iget-object v0, p0, LX/1bp;->j:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 281062
    if-nez v0, :cond_2

    .line 281063
    new-instance v0, LX/4AS;

    invoke-direct {v0}, LX/4AS;-><init>()V

    .line 281064
    invoke-virtual {p0, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 281065
    :cond_2
    instance-of v1, v0, LX/4AS;

    if-eqz v1, :cond_0

    .line 281066
    check-cast v0, LX/4AS;

    .line 281067
    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    move-object v1, v1

    .line 281068
    invoke-virtual {v0, v1}, LX/4AS;->a(Ljava/lang/String;)V

    .line 281069
    if-eqz p1, :cond_3

    .line 281070
    invoke-virtual {p1}, LX/1ln;->g()I

    move-result v1

    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v2

    .line 281071
    iput v1, v0, LX/4AS;->b:I

    .line 281072
    iput v2, v0, LX/4AS;->c:I

    .line 281073
    invoke-virtual {v0}, LX/4AS;->invalidateSelf()V

    .line 281074
    invoke-virtual {p1}, LX/1ln;->b()I

    move-result v1

    .line 281075
    iput v1, v0, LX/4AS;->d:I

    .line 281076
    goto :goto_0

    .line 281077
    :cond_3
    invoke-virtual {v0}, LX/4AS;->a()V

    goto :goto_0
.end method


# virtual methods
.method public a(LX/1FJ;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 281046
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->b(Z)V

    .line 281047
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 281048
    invoke-direct {p0, v0}, LX/1bo;->a(LX/1ln;)V

    .line 281049
    iget-object v1, p0, LX/1bo;->d:LX/1c9;

    if-eqz v1, :cond_1

    .line 281050
    iget-object v1, p0, LX/1bo;->d:LX/1c9;

    invoke-virtual {v1}, LX/1c9;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1c8;

    .line 281051
    invoke-interface {v1, v0}, LX/1c8;->a(LX/1ln;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 281052
    invoke-interface {v1, v0}, LX/1c8;->b(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 281053
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 281054
    :goto_0
    return-object v0

    .line 281055
    :cond_1
    iget-object v1, p0, LX/1bo;->i:LX/1c8;

    invoke-interface {v1, v0}, LX/1c8;->b(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 281056
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 281057
    goto :goto_0

    .line 281058
    :cond_2
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized image class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 281045
    check-cast p1, LX/1FJ;

    invoke-virtual {p0, p1}, LX/1bo;->a(LX/1FJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281041
    const/4 v0, 0x0

    invoke-static {p0, p2, p4, v0}, LX/1bp;->a(LX/1bp;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 281042
    invoke-direct {p0, p1}, LX/1bo;->a(LX/1Gd;)V

    .line 281043
    iput-object p3, p0, LX/1bo;->f:LX/1bh;

    .line 281044
    return-void
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 281091
    check-cast p1, LX/1FJ;

    .line 281092
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->b(Z)V

    .line 281093
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    return-object v0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281038
    instance-of v0, p1, LX/2dJ;

    if-eqz v0, :cond_0

    .line 281039
    check-cast p1, LX/2dJ;

    invoke-interface {p1}, LX/2dJ;->a()V

    .line 281040
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281036
    check-cast p1, LX/1FJ;

    .line 281037
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1FJ;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281033
    check-cast p1, LX/1FJ;

    .line 281034
    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    .line 281035
    return-void
.end method

.method public o()LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 281031
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281032
    iget-object v0, p0, LX/1bo;->g:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;

    return-object v0
.end method

.method public final p()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 281021
    const/4 v1, 0x0

    .line 281022
    iget-object v0, p0, LX/1bo;->e:LX/1Fh;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1bo;->f:LX/1bh;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 281023
    :goto_0
    return-object v0

    .line 281024
    :cond_1
    iget-object v0, p0, LX/1bo;->e:LX/1Fh;

    iget-object v2, p0, LX/1bo;->f:LX/1bh;

    invoke-interface {v0, v2}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v2

    .line 281025
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->d()LX/1lk;

    move-result-object v0

    .line 281026
    iget-boolean p0, v0, LX/1lk;->d:Z

    move v0, p0

    .line 281027
    if-nez v0, :cond_2

    .line 281028
    invoke-virtual {v2}, LX/1FJ;->close()V

    move-object v0, v1

    .line 281029
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 281030
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 281020
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "super"

    invoke-super {p0}, LX/1bp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "dataSourceSupplier"

    iget-object v2, p0, LX/1bo;->g:LX/1Gd;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
