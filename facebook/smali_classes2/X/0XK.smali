.class public final enum LX/0XK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0XK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0XK;

.field public static final enum CONNECTED:LX/0XK;

.field public static final enum NO_CONNECTION:LX/0XK;

.field public static final enum UNSET:LX/0XK;


# instance fields
.field public final dbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 78139
    new-instance v0, LX/0XK;

    const-string v1, "UNSET"

    const-string v2, "UNSET"

    invoke-direct {v0, v1, v3, v2}, LX/0XK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0XK;->UNSET:LX/0XK;

    .line 78140
    new-instance v0, LX/0XK;

    const-string v1, "CONNECTED"

    const-string v2, "CONNECTED"

    invoke-direct {v0, v1, v4, v2}, LX/0XK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0XK;->CONNECTED:LX/0XK;

    .line 78141
    new-instance v0, LX/0XK;

    const-string v1, "NO_CONNECTION"

    const-string v2, "NO_CONNECTION"

    invoke-direct {v0, v1, v5, v2}, LX/0XK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0XK;->NO_CONNECTION:LX/0XK;

    .line 78142
    const/4 v0, 0x3

    new-array v0, v0, [LX/0XK;

    sget-object v1, LX/0XK;->UNSET:LX/0XK;

    aput-object v1, v0, v3

    sget-object v1, LX/0XK;->CONNECTED:LX/0XK;

    aput-object v1, v0, v4

    sget-object v1, LX/0XK;->NO_CONNECTION:LX/0XK;

    aput-object v1, v0, v5

    sput-object v0, LX/0XK;->$VALUES:[LX/0XK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78153
    iput-object p3, p0, LX/0XK;->dbValue:Ljava/lang/String;

    .line 78154
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)LX/0XK;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78145
    if-nez p0, :cond_1

    .line 78146
    sget-object v0, LX/0XK;->UNSET:LX/0XK;

    .line 78147
    :cond_0
    :goto_0
    return-object v0

    .line 78148
    :cond_1
    invoke-static {}, LX/0XK;->values()[LX/0XK;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 78149
    iget-object v4, v0, LX/0XK;->dbValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 78150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 78151
    :cond_2
    sget-object v0, LX/0XK;->UNSET:LX/0XK;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0XK;
    .locals 1

    .prologue
    .line 78144
    const-class v0, LX/0XK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0XK;

    return-object v0
.end method

.method public static values()[LX/0XK;
    .locals 1

    .prologue
    .line 78143
    sget-object v0, LX/0XK;->$VALUES:[LX/0XK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0XK;

    return-object v0
.end method
