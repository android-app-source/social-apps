.class public LX/1Yg;
.super LX/1Yh;
.source ""


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:LX/1OP;

.field private final f:LX/1Yt;

.field private final g:LX/1Yk;

.field private h:LX/0Sh;

.field private i:LX/19P;

.field private j:LX/0wq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273884
    const-class v0, LX/1Yg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Yg;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0g8;LX/1OP;LX/1Yk;LX/0Sh;LX/19P;LX/1AP;LX/0wq;)V
    .locals 10
    .param p1    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1OP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273876
    move-object/from16 v0, p7

    iget v3, v0, LX/0wq;->u:I

    sget-object v4, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    sget-object v5, LX/1Yj;->ALL_ONSCREEN_AND_OFFSCREEN:LX/1Yj;

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v9}, LX/1Yh;-><init>(LX/0g8;ILX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V

    .line 273877
    new-instance v1, LX/1Yt;

    invoke-direct {v1}, LX/1Yt;-><init>()V

    iput-object v1, p0, LX/1Yg;->f:LX/1Yt;

    .line 273878
    iput-object p3, p0, LX/1Yg;->g:LX/1Yk;

    .line 273879
    iput-object p2, p0, LX/1Yg;->e:LX/1OP;

    .line 273880
    iput-object p4, p0, LX/1Yg;->h:LX/0Sh;

    .line 273881
    iput-object p5, p0, LX/1Yg;->i:LX/19P;

    .line 273882
    move-object/from16 v0, p7

    iput-object v0, p0, LX/1Yg;->j:LX/0wq;

    .line 273883
    return-void
.end method

.method public static a$redex0(LX/1Yg;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 273839
    if-nez p1, :cond_1

    .line 273840
    :cond_0
    return-void

    .line 273841
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 273842
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p0, v0}, LX/1Yg;->a$redex0(LX/1Yg;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 273843
    :cond_2
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 273844
    invoke-static {p0, v0}, LX/1Yg;->a$redex0(LX/1Yg;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 273845
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 273846
    :cond_3
    invoke-static {p1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273847
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 273848
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 273849
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 273850
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const p1, 0x4ed245b

    if-ne v4, p1, :cond_a

    .line 273851
    :goto_2
    move-object v3, v3

    .line 273852
    if-eqz v3, :cond_4

    .line 273853
    iget-object v0, p0, LX/1Yg;->j:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->I:Z

    if-nez v0, :cond_5

    iget-object v0, p0, LX/1Yg;->j:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->K:Z

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    .line 273854
    :goto_3
    iget-object v4, p0, LX/1Yg;->j:LX/0wq;

    invoke-virtual {v4}, LX/0wq;->e()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 273855
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, LX/1Yg;->j:LX/0wq;

    invoke-virtual {v4}, LX/0wq;->g()Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1Yg;->j:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->C:Z

    if-eqz v0, :cond_4

    .line 273856
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    .line 273857
    iget-object v0, p0, LX/1Yg;->i:LX/19P;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v3

    invoke-virtual {v0, v4, v3}, LX/19P;->a(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_6
    move v0, v1

    .line 273858
    goto :goto_3

    .line 273859
    :cond_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, LX/1Yg;->j:LX/0wq;

    invoke-virtual {v4}, LX/0wq;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, LX/1Yg;->j:LX/0wq;

    invoke-virtual {v4}, LX/0wq;->f()Z

    move-result v4

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/1Yg;->j:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->C:Z

    if-eqz v0, :cond_8

    .line 273860
    iget-object v0, p0, LX/1Yg;->i:LX/19P;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v3

    invoke-virtual {v0, v4, v3}, LX/19P;->a(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 273861
    :cond_8
    iget-object v0, p0, LX/1Yg;->g:LX/1Yk;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    .line 273862
    if-nez v3, :cond_b

    .line 273863
    :cond_9
    :goto_4
    goto/16 :goto_1

    :cond_a
    const/4 v3, 0x0

    goto :goto_2

    .line 273864
    :cond_b
    iget-object v4, v0, LX/1Yk;->c:LX/0aq;

    invoke-virtual {v4, v3}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/engine/VideoPlayerParams;

    .line 273865
    if-eqz v4, :cond_9

    .line 273866
    invoke-static {v4}, LX/1Yk;->b(Lcom/facebook/video/engine/VideoPlayerParams;)Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v5

    .line 273867
    if-nez v5, :cond_d

    const/4 v5, 0x0

    :goto_5
    move-object v5, v5

    .line 273868
    if-eqz v5, :cond_9

    .line 273869
    iget-object v4, v0, LX/1Yk;->h:LX/0wq;

    iget-boolean v4, v4, LX/0wq;->y:Z

    if-eqz v4, :cond_c

    iget-object v4, v0, LX/1Yk;->e:LX/0aq;

    invoke-virtual {v4, v5}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_9

    .line 273870
    :cond_c
    iget-object v4, v0, LX/1Yk;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7K3;

    .line 273871
    iget-object v6, v4, LX/7K3;->f:LX/7K2;

    invoke-virtual {v6, v5}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 273872
    :goto_6
    goto :goto_4

    :cond_d
    iget-object v5, v5, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    goto :goto_5

    .line 273873
    :cond_e
    :try_start_0
    new-instance p1, LX/7K5;

    iget-object v6, v4, LX/7K3;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/MediaPlayer;

    iget-object v0, v4, LX/7K3;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, v4, LX/7K3;->c:LX/0So;

    invoke-direct {p1, v6, v0, v5, v3}, LX/7K5;-><init>(Landroid/media/MediaPlayer;Landroid/content/Context;Landroid/net/Uri;LX/0So;)V

    .line 273874
    iget-object v6, v4, LX/7K3;->f:LX/7K2;

    invoke-virtual {v6, v5, p1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 273875
    :catch_0
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v5, v6, p1

    goto :goto_6
.end method

.method private d(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 273814
    iget-object v2, p0, LX/1Yg;->e:LX/1OP;

    invoke-interface {v2}, LX/1OP;->ij_()I

    move-result v2

    if-ge p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 273815
    :cond_0
    :goto_0
    return-wide v0

    .line 273816
    :cond_1
    iget-object v2, p0, LX/1Yg;->e:LX/1OP;

    invoke-interface {v2, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 273817
    if-eqz v2, :cond_0

    .line 273818
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 273831
    iget-object v0, p0, LX/1Yg;->e:LX/1OP;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 273832
    :cond_0
    :goto_0
    return-void

    .line 273833
    :cond_1
    iget-object v0, p0, LX/1Yg;->e:LX/1OP;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 273834
    iget-object v1, p0, LX/1Yg;->f:LX/1Yt;

    invoke-direct {p0, p1}, LX/1Yg;->d(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/1Yt;->a(J)V

    .line 273835
    invoke-static {v0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 273836
    if-eqz v0, :cond_0

    .line 273837
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 273838
    iget-object v1, p0, LX/1Yg;->h:LX/0Sh;

    new-instance v2, Lcom/facebook/feed/ui/feedprefetch/VideoPrepareViewPreloader$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/ui/feedprefetch/VideoPrepareViewPreloader$1;-><init>(LX/1Yg;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    .line 273824
    iget-object v0, p0, LX/1Yg;->f:LX/1Yt;

    invoke-direct {p0, p1}, LX/1Yg;->d(I)J

    move-result-wide v2

    .line 273825
    iget-object v1, v0, LX/1Yt;->a:LX/0tf;

    .line 273826
    iget-boolean p0, v1, LX/0tf;->b:Z

    if-eqz p0, :cond_0

    .line 273827
    invoke-static {v1}, LX/0tf;->d(LX/0tf;)V

    .line 273828
    :cond_0
    iget-object p0, v1, LX/0tf;->c:[J

    iget p1, v1, LX/0tf;->e:I

    invoke-static {p0, p1, v2, v3}, LX/01M;->a([JIJ)I

    move-result p0

    move v1, p0

    .line 273829
    if-ltz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 273830
    return v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 273819
    iget-object v0, p0, LX/1Yg;->j:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->w:Z

    if-eqz v0, :cond_0

    .line 273820
    iget-object v0, p0, LX/1Yg;->f:LX/1Yt;

    invoke-direct {p0, p1}, LX/1Yg;->d(I)J

    move-result-wide v2

    .line 273821
    iget-object v1, v0, LX/1Yt;->a:LX/0tf;

    .line 273822
    invoke-virtual {v1, v2, v3}, LX/0tf;->b(J)V

    .line 273823
    :cond_0
    return-void
.end method
