.class public LX/1CU;
.super LX/1CV;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/1CW;

.field public final d:Landroid/content/res/Resources;

.field public final e:LX/0kL;

.field public final f:LX/1CX;

.field public final g:LX/0bH;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:LX/0qq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216248
    const-class v0, LX/1CU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1CU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0bH;LX/03V;LX/1CW;Landroid/content/res/Resources;LX/1CX;LX/0kL;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1CW;",
            "Landroid/content/res/Resources;",
            "LX/1CX;",
            "LX/0kL;",
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216260
    invoke-direct {p0}, LX/1CV;-><init>()V

    .line 216261
    iput-object p2, p0, LX/1CU;->b:LX/03V;

    .line 216262
    iput-object p3, p0, LX/1CU;->c:LX/1CW;

    .line 216263
    iput-object p4, p0, LX/1CU;->d:Landroid/content/res/Resources;

    .line 216264
    iput-object p6, p0, LX/1CU;->e:LX/0kL;

    .line 216265
    iput-object p5, p0, LX/1CU;->f:LX/1CX;

    .line 216266
    iput-object p1, p0, LX/1CU;->g:LX/0bH;

    .line 216267
    iput-object p7, p0, LX/1CU;->h:LX/0Ot;

    .line 216268
    return-void
.end method

.method public static a(LX/0QB;)LX/1CU;
    .locals 9

    .prologue
    .line 216257
    new-instance v1, LX/1CU;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v2

    check-cast v2, LX/0bH;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v4

    check-cast v4, LX/1CW;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v6

    check-cast v6, LX/1CX;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    const/16 v8, 0x1a39

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, LX/1CU;-><init>(LX/0bH;LX/03V;LX/1CW;Landroid/content/res/Resources;LX/1CX;LX/0kL;LX/0Ot;)V

    .line 216258
    move-object v0, v1

    .line 216259
    return-object v0
.end method


# virtual methods
.method public final a(LX/0qq;)V
    .locals 0

    .prologue
    .line 216255
    iput-object p1, p0, LX/1CU;->j:LX/0qq;

    .line 216256
    return-void
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 216249
    check-cast p1, LX/1Nc;

    .line 216250
    iget-object v0, p0, LX/1CU;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/967;

    .line 216251
    iget-object v1, p1, LX/1Nc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 216252
    iget-boolean v2, p1, LX/1Nc;->h:Z

    move v2, v2

    .line 216253
    const-string v3, "newsfeed_story_notify_me"

    const-string v4, "native_newsfeed"

    new-instance v5, LX/DBn;

    invoke-direct {v5, p0}, LX/DBn;-><init>(LX/1CU;)V

    invoke-virtual/range {v0 .. v5}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    .line 216254
    return-void
.end method
