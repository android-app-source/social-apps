.class public LX/1Qu;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Qx;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Qx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 245284
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Qx;
    .locals 5

    .prologue
    .line 245286
    sget-object v0, LX/1Qu;->a:LX/1Qx;

    if-nez v0, :cond_1

    .line 245287
    const-class v1, LX/1Qu;

    monitor-enter v1

    .line 245288
    :try_start_0
    sget-object v0, LX/1Qu;->a:LX/1Qx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 245289
    if-eqz v2, :cond_0

    .line 245290
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 245291
    const/16 v3, 0x5d1

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v3, 0x5d0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v4, p0, v3}, LX/1Qv;->a(LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;)LX/1Qx;

    move-result-object v3

    move-object v0, v3

    .line 245292
    sput-object v0, LX/1Qu;->a:LX/1Qx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245293
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 245294
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 245295
    :cond_1
    sget-object v0, LX/1Qu;->a:LX/1Qx;

    return-object v0

    .line 245296
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 245297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 245285
    const/16 v0, 0x5d1

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v0, 0x5d0

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v1, v2, v0}, LX/1Qv;->a(LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;)LX/1Qx;

    move-result-object v0

    return-object v0
.end method
