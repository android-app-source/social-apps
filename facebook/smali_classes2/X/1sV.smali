.class public LX/1sV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334900
    iput-object p1, p0, LX/1sV;->a:Landroid/content/Context;

    .line 334901
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "[B>;>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 334902
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 334903
    new-instance v0, LX/2ze;

    iget-object v1, p0, LX/1sV;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2ze;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/2ze;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 334904
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 334905
    :try_start_0
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "app_updates"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 334906
    :try_start_1
    const-string v0, "id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 334907
    const-string v2, "data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 334908
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 334909
    new-instance v3, Landroid/util/Pair;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 334910
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334911
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 334912
    :cond_3
    return-object v9

    .line 334913
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 334914
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 334915
    new-instance v0, LX/2ze;

    iget-object v1, p0, LX/1sV;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2ze;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/2ze;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 334916
    :cond_0
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "app_updates"

    const-string v2, "id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 334917
    return-void
.end method

.method public final a(J[B)V
    .locals 7

    .prologue
    .line 334918
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 334919
    new-instance v0, LX/2ze;

    iget-object v1, p0, LX/1sV;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2ze;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/2ze;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 334920
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 334921
    const-string v1, "id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 334922
    const-string v1, "data"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 334923
    iget-object v1, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const v2, -0x4da9ce03

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 334924
    :try_start_0
    iget-object v1, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "app_updates"

    const-string v3, "id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 334925
    iget-object v1, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "app_updates"

    const/4 v3, 0x0

    const v4, -0x73f9fced

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0xa25cc47

    invoke-static {v0}, LX/03h;->a(I)V

    .line 334926
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334927
    iget-object v0, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const v1, 0x72d34652

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 334928
    return-void

    .line 334929
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const v2, -0x3480249e    # -1.6767842E7f

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
