.class public LX/1RX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1RX;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246392
    iput-object p1, p0, LX/1RX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 246393
    return-void
.end method

.method public static a(LX/0QB;)LX/1RX;
    .locals 4

    .prologue
    .line 246378
    sget-object v0, LX/1RX;->b:LX/1RX;

    if-nez v0, :cond_1

    .line 246379
    const-class v1, LX/1RX;

    monitor-enter v1

    .line 246380
    :try_start_0
    sget-object v0, LX/1RX;->b:LX/1RX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 246381
    if-eqz v2, :cond_0

    .line 246382
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 246383
    new-instance p0, LX/1RX;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/1RX;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 246384
    move-object v0, p0

    .line 246385
    sput-object v0, LX/1RX;->b:LX/1RX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246386
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 246387
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 246388
    :cond_1
    sget-object v0, LX/1RX;->b:LX/1RX;

    return-object v0

    .line 246389
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 246390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 246372
    iget-object v0, p0, LX/1RX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1aM;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 246373
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 246377
    iget-object v0, p0, LX/1RX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1aM;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 246375
    iget-object v0, p0, LX/1RX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1aM;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 246376
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 246374
    iget-object v0, p0, LX/1RX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1aM;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
