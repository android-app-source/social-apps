.class public LX/14D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/14D;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/14E;

.field public final d:LX/03V;

.field private final e:LX/14F;

.field private final f:Lcom/facebook/common/perftest/PerfTestConfig;

.field private g:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178411
    const-class v0, LX/14D;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/14D;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/14E;LX/03V;LX/14F;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178413
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/14D;->g:LX/03R;

    .line 178414
    iput-object p1, p0, LX/14D;->b:LX/0ad;

    .line 178415
    iput-object p2, p0, LX/14D;->c:LX/14E;

    .line 178416
    iput-object p3, p0, LX/14D;->d:LX/03V;

    .line 178417
    iput-object p4, p0, LX/14D;->e:LX/14F;

    .line 178418
    iput-object p5, p0, LX/14D;->f:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 178419
    return-void
.end method

.method public static a(LX/0QB;)LX/14D;
    .locals 9

    .prologue
    .line 178420
    sget-object v0, LX/14D;->h:LX/14D;

    if-nez v0, :cond_1

    .line 178421
    const-class v1, LX/14D;

    monitor-enter v1

    .line 178422
    :try_start_0
    sget-object v0, LX/14D;->h:LX/14D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178423
    if-eqz v2, :cond_0

    .line 178424
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 178425
    new-instance v3, LX/14D;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    .line 178426
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    invoke-static {}, LX/0eG;->a()LX/14E;

    move-result-object v5

    move-object v5, v5

    .line 178427
    check-cast v5, LX/14E;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/14F;->b(LX/0QB;)LX/14F;

    move-result-object v7

    check-cast v7, LX/14F;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v8

    check-cast v8, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct/range {v3 .. v8}, LX/14D;-><init>(LX/0ad;LX/14E;LX/03V;LX/14F;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 178428
    move-object v0, v3

    .line 178429
    sput-object v0, LX/14D;->h:LX/14D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178430
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178431
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178432
    :cond_1
    sget-object v0, LX/14D;->h:LX/14D;

    return-object v0

    .line 178433
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/15e;
    .locals 5

    .prologue
    .line 178435
    sget-object v0, LX/15e;->APACHE:LX/15e;

    .line 178436
    const/4 v1, 0x0

    .line 178437
    :try_start_0
    const-string v2, "org.apache.http.client.methods.HttpUriRequest"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 178438
    const-string v3, "getAllHeaders"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 178439
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 178440
    if-nez v1, :cond_1

    .line 178441
    :cond_0
    :goto_1
    return-object v0

    .line 178442
    :cond_1
    iget-object v1, p0, LX/14D;->e:LX/14F;

    invoke-virtual {v1}, LX/14F;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178443
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178444
    sget-boolean v0, LX/0as;->b:Z

    if-eqz v0, :cond_4

    .line 178445
    const-string v0, "liger_engine_enabled"

    .line 178446
    :goto_2
    move-object v0, v0

    .line 178447
    const-string v1, "liger_engine_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178448
    sget-object v0, LX/15e;->LIGER:LX/15e;

    goto :goto_1

    .line 178449
    :cond_2
    const/4 v1, 0x1

    move v0, v1

    .line 178450
    if-eqz v0, :cond_3

    .line 178451
    sget-object v0, LX/15e;->LIGER:LX/15e;

    goto :goto_1

    .line 178452
    :cond_3
    sget-object v0, LX/15e;->APACHE:LX/15e;

    goto :goto_1

    .line 178453
    :catch_0
    move-exception v2

    .line 178454
    iget-object v3, p0, LX/14D;->d:LX/03V;

    sget-object v4, LX/14D;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 178455
    :cond_4
    sget-boolean v0, LX/0as;->c:Z

    if-eqz v0, :cond_5

    .line 178456
    const-string v0, "okhttp_engine_enabled"

    goto :goto_2

    .line 178457
    :cond_5
    const-string v0, "no_valid_engine"

    goto :goto_2
.end method
