.class public LX/0Ul;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0Ul;


# instance fields
.field public a:J

.field public final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 66775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66776
    iput-object p1, p0, LX/0Ul;->b:LX/0So;

    .line 66777
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ul;
    .locals 4

    .prologue
    .line 66778
    sget-object v0, LX/0Ul;->c:LX/0Ul;

    if-nez v0, :cond_1

    .line 66779
    const-class v1, LX/0Ul;

    monitor-enter v1

    .line 66780
    :try_start_0
    sget-object v0, LX/0Ul;->c:LX/0Ul;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 66781
    if-eqz v2, :cond_0

    .line 66782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 66783
    new-instance p0, LX/0Ul;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/0Ul;-><init>(LX/0So;)V

    .line 66784
    move-object v0, p0

    .line 66785
    sput-object v0, LX/0Ul;->c:LX/0Ul;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66786
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 66787
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66788
    :cond_1
    sget-object v0, LX/0Ul;->c:LX/0Ul;

    return-object v0

    .line 66789
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 66790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
