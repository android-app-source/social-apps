.class public LX/1JG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1JG;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0SG;

.field public final c:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229952
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1JG;->a:Ljava/util/List;

    .line 229953
    iput-object p1, p0, LX/1JG;->b:LX/0SG;

    .line 229954
    iput-object p2, p0, LX/1JG;->c:LX/0Sh;

    .line 229955
    return-void
.end method

.method public static a(LX/0QB;)LX/1JG;
    .locals 5

    .prologue
    .line 229928
    sget-object v0, LX/1JG;->d:LX/1JG;

    if-nez v0, :cond_1

    .line 229929
    const-class v1, LX/1JG;

    monitor-enter v1

    .line 229930
    :try_start_0
    sget-object v0, LX/1JG;->d:LX/1JG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 229931
    if-eqz v2, :cond_0

    .line 229932
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 229933
    new-instance p0, LX/1JG;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/1JG;-><init>(LX/0SG;LX/0Sh;)V

    .line 229934
    move-object v0, p0

    .line 229935
    sput-object v0, LX/1JG;->d:LX/1JG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229936
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 229937
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229938
    :cond_1
    sget-object v0, LX/1JG;->d:LX/1JG;

    return-object v0

    .line 229939
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 229940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 229941
    iget-object v0, p0, LX/1JG;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 229942
    const-wide/16 v2, 0x3e8

    mul-long v4, p1, v2

    .line 229943
    iget-object v0, p0, LX/1JG;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 229944
    iget-object v0, p0, LX/1JG;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 229945
    :goto_0
    if-ge v2, v3, :cond_1

    .line 229946
    iget-object v0, p0, LX/1JG;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v6, v8

    cmp-long v0, v8, v4

    if-gtz v0, :cond_0

    .line 229947
    sub-int v0, v3, v2

    .line 229948
    :goto_1
    return v0

    .line 229949
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 229950
    goto :goto_1
.end method
