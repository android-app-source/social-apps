.class public LX/1af;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ag;


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/content/res/Resources;

.field public c:LX/4Ab;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/1ar;

.field public final e:LX/1ap;

.field public final f:LX/1ah;


# direct methods
.method public constructor <init>(LX/1Uo;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 278010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278011
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/1af;->a:Landroid/graphics/drawable/Drawable;

    .line 278012
    iget-object v0, p1, LX/1Uo;->c:Landroid/content/res/Resources;

    move-object v0, v0

    .line 278013
    iput-object v0, p0, LX/1af;->b:Landroid/content/res/Resources;

    .line 278014
    iget-object v0, p1, LX/1Uo;->u:LX/4Ab;

    move-object v0, v0

    .line 278015
    iput-object v0, p0, LX/1af;->c:LX/4Ab;

    .line 278016
    new-instance v0, LX/1ah;

    iget-object v2, p0, LX/1af;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v2}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, LX/1af;->f:LX/1ah;

    .line 278017
    iget-object v0, p1, LX/1Uo;->s:Ljava/util/List;

    move-object v0, v0

    .line 278018
    if-eqz v0, :cond_1

    .line 278019
    iget-object v0, p1, LX/1Uo;->s:Ljava/util/List;

    move-object v0, v0

    .line 278020
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 278021
    :goto_0
    iget-object v2, p1, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    move-object v2, v2

    .line 278022
    if-eqz v2, :cond_2

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    .line 278023
    add-int/lit8 v2, v0, 0x6

    .line 278024
    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    .line 278025
    iget-object v4, p1, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    move-object v4, v4

    .line 278026
    invoke-direct {p0, v4, v10}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v2, v3

    .line 278027
    iget-object v4, p1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    move-object v4, v4

    .line 278028
    iget-object v5, p1, LX/1Uo;->g:LX/1Up;

    move-object v5, v5

    .line 278029
    invoke-direct {p0, v4, v5}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v2, v1

    .line 278030
    const/4 v4, 0x2

    iget-object v5, p0, LX/1af;->f:LX/1ah;

    .line 278031
    iget-object v6, p1, LX/1Uo;->n:LX/1Up;

    move-object v6, v6

    .line 278032
    iget-object v7, p1, LX/1Uo;->p:Landroid/graphics/PointF;

    move-object v7, v7

    .line 278033
    iget-object v8, p1, LX/1Uo;->o:Landroid/graphics/Matrix;

    move-object v8, v8

    .line 278034
    iget-object v9, p1, LX/1Uo;->q:Landroid/graphics/ColorFilter;

    move-object v9, v9

    .line 278035
    invoke-virtual {v5, v9}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 278036
    invoke-static {v5, v6, v7}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/1Up;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 278037
    if-eqz v11, :cond_0

    if-nez v8, :cond_6

    .line 278038
    :cond_0
    :goto_2
    move-object v11, v11

    .line 278039
    move-object v5, v11

    .line 278040
    aput-object v5, v2, v4

    .line 278041
    const/4 v4, 0x3

    .line 278042
    iget-object v5, p1, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    move-object v5, v5

    .line 278043
    iget-object v6, p1, LX/1Uo;->m:LX/1Up;

    move-object v6, v6

    .line 278044
    invoke-direct {p0, v5, v6}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v2, v4

    .line 278045
    const/4 v4, 0x4

    .line 278046
    iget-object v5, p1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    move-object v5, v5

    .line 278047
    iget-object v6, p1, LX/1Uo;->i:LX/1Up;

    move-object v6, v6

    .line 278048
    invoke-direct {p0, v5, v6}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v2, v4

    .line 278049
    const/4 v4, 0x5

    .line 278050
    iget-object v5, p1, LX/1Uo;->j:Landroid/graphics/drawable/Drawable;

    move-object v5, v5

    .line 278051
    iget-object v6, p1, LX/1Uo;->k:LX/1Up;

    move-object v6, v6

    .line 278052
    invoke-direct {p0, v5, v6}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v2, v4

    .line 278053
    if-lez v0, :cond_5

    .line 278054
    iget-object v0, p1, LX/1Uo;->s:Ljava/util/List;

    move-object v0, v0

    .line 278055
    if-eqz v0, :cond_3

    .line 278056
    iget-object v0, p1, LX/1Uo;->s:Ljava/util/List;

    move-object v0, v0

    .line 278057
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 278058
    add-int/lit8 v1, v3, 0x1

    add-int/lit8 v3, v3, 0x6

    invoke-direct {p0, v0, v10}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aput-object v0, v2, v3

    move v3, v1

    .line 278059
    goto :goto_3

    :cond_1
    move v0, v1

    .line 278060
    goto/16 :goto_0

    :cond_2
    move v2, v3

    .line 278061
    goto/16 :goto_1

    :cond_3
    move v3, v1

    .line 278062
    :cond_4
    iget-object v0, p1, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 278063
    if-eqz v0, :cond_5

    .line 278064
    add-int/lit8 v0, v3, 0x6

    .line 278065
    iget-object v1, p1, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 278066
    invoke-direct {p0, v1, v10}, LX/1af;->e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v2, v0

    .line 278067
    :cond_5
    new-instance v0, LX/1ap;

    invoke-direct {v0, v2}, LX/1ap;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, LX/1af;->e:LX/1ap;

    .line 278068
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    .line 278069
    iget v1, p1, LX/1Uo;->d:I

    move v1, v1

    .line 278070
    invoke-virtual {v0, v1}, LX/1ap;->c(I)V

    .line 278071
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    iget-object v1, p0, LX/1af;->c:LX/4Ab;

    invoke-static {v0, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/4Ab;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278072
    new-instance v1, LX/1ar;

    invoke-direct {v1, v0}, LX/1ar;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, LX/1af;->d:LX/1ar;

    .line 278073
    iget-object v0, p0, LX/1af;->d:LX/1ar;

    invoke-virtual {v0}, LX/1ar;->mutate()Landroid/graphics/drawable/Drawable;

    .line 278074
    invoke-direct {p0}, LX/1af;->h()V

    .line 278075
    return-void

    :cond_6
    new-instance v5, LX/1fi;

    invoke-direct {v5, v11, v8}, LX/1fi;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V

    move-object v11, v5

    goto/16 :goto_2
.end method

.method private a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 278113
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0, v2}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 278114
    if-nez v1, :cond_0

    .line 278115
    :goto_0
    return-void

    .line 278116
    :cond_0
    const v0, 0x3f7fbe77    # 0.999f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    .line 278117
    instance-of v0, v1, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 278118
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 278119
    :cond_1
    invoke-direct {p0, v2}, LX/1af;->d(I)V

    .line 278120
    :goto_1
    const v0, 0x461c4000    # 10000.0f

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0

    .line 278121
    :cond_2
    instance-of v0, v1, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 278122
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 278123
    :cond_3
    invoke-direct {p0, v2}, LX/1af;->c(I)V

    goto :goto_1
.end method

.method public static a(LX/1af;ILandroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278124
    if-nez p2, :cond_0

    .line 278125
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/1aq;->a(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 278126
    :goto_0
    return-void

    .line 278127
    :cond_0
    iget-object v0, p0, LX/1af;->c:LX/4Ab;

    iget-object v1, p0, LX/1af;->b:Landroid/content/res/Resources;

    invoke-static {p2, v0, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278128
    invoke-static {p0, p1}, LX/1af;->e(LX/1af;I)LX/1ai;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 278129
    if-ltz p1, :cond_0

    .line 278130
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    .line 278131
    const/4 v1, 0x0

    iput v1, v0, LX/1ap;->a:I

    .line 278132
    iget-object v1, v0, LX/1ap;->g:[Z

    const/4 p0, 0x1

    aput-boolean p0, v1, p1

    .line 278133
    invoke-virtual {v0}, LX/1ap;->invalidateSelf()V

    .line 278134
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 278135
    if-ltz p1, :cond_0

    .line 278136
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    const/4 p0, 0x0

    .line 278137
    iput p0, v0, LX/1ap;->a:I

    .line 278138
    iget-object v1, v0, LX/1ap;->g:[Z

    aput-boolean p0, v1, p1

    .line 278139
    invoke-virtual {v0}, LX/1ap;->invalidateSelf()V

    .line 278140
    :cond_0
    return-void
.end method

.method public static e(LX/1af;I)LX/1ai;
    .locals 3

    .prologue
    .line 278141
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    const/4 v2, 0x1

    const/4 p0, 0x0

    .line 278142
    if-ltz p1, :cond_3

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 278143
    iget-object v1, v0, LX/1aq;->d:[LX/1ai;

    array-length v1, v1

    if-ge p1, v1, :cond_4

    :goto_1
    invoke-static {v2}, LX/03g;->a(Z)V

    .line 278144
    iget-object v1, v0, LX/1aq;->d:[LX/1ai;

    aget-object v1, v1, p1

    if-nez v1, :cond_0

    .line 278145
    iget-object v1, v0, LX/1aq;->d:[LX/1ai;

    .line 278146
    new-instance v2, LX/1fh;

    invoke-direct {v2, v0, p1}, LX/1fh;-><init>(LX/1aq;I)V

    move-object v2, v2

    .line 278147
    aput-object v2, v1, p1

    .line 278148
    :cond_0
    iget-object v1, v0, LX/1aq;->d:[LX/1ai;

    aget-object v1, v1, p1

    move-object v0, v1

    .line 278149
    invoke-interface {v0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, LX/1fi;

    if-eqz v1, :cond_1

    .line 278150
    invoke-interface {v0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/1fi;

    .line 278151
    :cond_1
    invoke-interface {v0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, LX/1ao;

    if-eqz v1, :cond_2

    .line 278152
    invoke-interface {v0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/1ao;

    .line 278153
    :cond_2
    return-object v0

    :cond_3
    move v1, p0

    .line 278154
    goto :goto_0

    :cond_4
    move v2, p0

    .line 278155
    goto :goto_1
.end method

.method private e(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 278156
    iget-object v0, p0, LX/1af;->c:LX/4Ab;

    iget-object v1, p0, LX/1af;->b:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278157
    invoke-static {v0, p2}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278158
    return-object v0
.end method

.method public static f(LX/1af;I)LX/1ao;
    .locals 2

    .prologue
    .line 278100
    invoke-static {p0, p1}, LX/1af;->e(LX/1af;I)LX/1ai;

    move-result-object v0

    .line 278101
    instance-of v1, v0, LX/1ao;

    if-eqz v1, :cond_0

    .line 278102
    check-cast v0, LX/1ao;

    .line 278103
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, LX/1Up;->a:LX/1Up;

    .line 278104
    sget-object p0, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, p0}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 278105
    invoke-static {p0, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 278106
    invoke-interface {v0, p0}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 278107
    const-string p1, "Parent has no child drawable!"

    invoke-static {p0, p1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278108
    check-cast p0, LX/1ao;

    move-object v0, p0

    .line 278109
    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 278159
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    if-eqz v0, :cond_0

    .line 278160
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->b()V

    .line 278161
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    .line 278162
    const/4 v1, 0x0

    iput v1, v0, LX/1ap;->a:I

    .line 278163
    iget-object v1, v0, LX/1ap;->g:[Z

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([ZZ)V

    .line 278164
    invoke-virtual {v0}, LX/1ap;->invalidateSelf()V

    .line 278165
    invoke-direct {p0}, LX/1af;->i()V

    .line 278166
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1af;->c(I)V

    .line 278167
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->f()V

    .line 278168
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->c()V

    .line 278169
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 278170
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1af;->d(I)V

    .line 278171
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/1af;->d(I)V

    .line 278172
    const/4 v0, 0x3

    invoke-direct {p0, v0}, LX/1af;->d(I)V

    .line 278173
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/1af;->d(I)V

    .line 278174
    const/4 v0, 0x5

    invoke-direct {p0, v0}, LX/1af;->d(I)V

    .line 278175
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278176
    iget-object v0, p0, LX/1af;->d:LX/1ar;

    return-object v0
.end method

.method public final a(FZ)V
    .locals 2

    .prologue
    .line 278177
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 278178
    :goto_0
    return-void

    .line 278179
    :cond_0
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->b()V

    .line 278180
    invoke-direct {p0, p1}, LX/1af;->a(F)V

    .line 278181
    if-eqz p2, :cond_1

    .line 278182
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->f()V

    .line 278183
    :cond_1
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->c()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 278184
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0, p1}, LX/1ap;->c(I)V

    .line 278185
    return-void
.end method

.method public final a(LX/1Up;)V
    .locals 1

    .prologue
    .line 278186
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278187
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ao;->a(LX/1Up;)V

    .line 278188
    return-void
.end method

.method public final a(LX/4Ab;)V
    .locals 7
    .param p1    # LX/4Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278189
    iput-object p1, p0, LX/1af;->c:LX/4Ab;

    .line 278190
    iget-object v0, p0, LX/1af;->d:LX/1ar;

    iget-object v1, p0, LX/1af;->c:LX/4Ab;

    .line 278191
    invoke-interface {v0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 278192
    if-eqz v1, :cond_4

    .line 278193
    iget-object v3, v1, LX/4Ab;->a:LX/4Aa;

    move-object v3, v3

    .line 278194
    sget-object p1, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    if-ne v3, p1, :cond_4

    .line 278195
    instance-of v3, v2, LX/1oL;

    if-eqz v3, :cond_3

    .line 278196
    check-cast v2, LX/1oL;

    .line 278197
    invoke-static {v2, v1}, LX/1an;->a(LX/1oM;LX/4Ab;)V

    .line 278198
    iget v3, v1, LX/4Ab;->d:I

    move v3, v3

    .line 278199
    invoke-virtual {v2, v3}, LX/1oL;->a(I)V

    .line 278200
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v1}, LX/1aq;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 278201
    invoke-static {p0, v0}, LX/1af;->e(LX/1af;I)LX/1ai;

    move-result-object v1

    iget-object v2, p0, LX/1af;->c:LX/4Ab;

    iget-object v3, p0, LX/1af;->b:Landroid/content/res/Resources;

    .line 278202
    invoke-static {v1}, LX/1an;->a(LX/1ai;)LX/1ai;

    move-result-object v5

    .line 278203
    invoke-interface {v5}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 278204
    if-eqz v2, :cond_6

    .line 278205
    iget-object v6, v2, LX/4Ab;->a:LX/4Aa;

    move-object v6, v6

    .line 278206
    sget-object p1, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    if-ne v6, p1, :cond_6

    .line 278207
    instance-of v6, v4, LX/1oM;

    if-eqz v6, :cond_5

    .line 278208
    check-cast v4, LX/1oM;

    .line 278209
    invoke-static {v4, v2}, LX/1an;->a(LX/1oM;LX/4Ab;)V

    .line 278210
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 278211
    :cond_2
    return-void

    .line 278212
    :cond_3
    sget-object v2, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v2}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 278213
    invoke-static {v2, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/4Ab;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 278214
    invoke-interface {v0, v2}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 278215
    :cond_4
    instance-of v3, v2, LX/1oL;

    if-eqz v3, :cond_0

    .line 278216
    check-cast v2, LX/1oL;

    .line 278217
    sget-object v3, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 278218
    invoke-interface {v0, v2}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 278219
    sget-object v2, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0

    .line 278220
    :cond_5
    if-eqz v4, :cond_1

    .line 278221
    sget-object v6, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {v5, v6}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 278222
    invoke-static {v4, v2, v3}, LX/1an;->b(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 278223
    invoke-interface {v5, v4}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 278224
    :cond_6
    instance-of v5, v4, LX/1oM;

    if-eqz v5, :cond_1

    .line 278225
    check-cast v4, LX/1oM;

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 278226
    invoke-interface {v4, v6}, LX/1oM;->a(Z)V

    .line 278227
    invoke-interface {v4, v5}, LX/1oM;->a(F)V

    .line 278228
    invoke-interface {v4, v6, v5}, LX/1oM;->a(IF)V

    .line 278229
    invoke-interface {v4, v5}, LX/1oM;->b(F)V

    .line 278230
    goto :goto_2
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 278231
    iget-object v0, p0, LX/1af;->f:LX/1ah;

    invoke-virtual {v0, p1}, LX/1ah;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 278232
    return-void
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 278110
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278111
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ao;->a(Landroid/graphics/PointF;)V

    .line 278112
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 277975
    iget-object v0, p0, LX/1af;->f:LX/1ah;

    invoke-virtual {v0, p1}, LX/1ah;->b(Landroid/graphics/RectF;)V

    .line 277976
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277977
    iget-object v0, p0, LX/1af;->d:LX/1ar;

    .line 277978
    iput-object p1, v0, LX/1ar;->a:Landroid/graphics/drawable/Drawable;

    .line 277979
    invoke-virtual {v0}, LX/1ar;->invalidateSelf()V

    .line 277980
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;FZ)V
    .locals 2

    .prologue
    .line 277981
    iget-object v0, p0, LX/1af;->c:LX/4Ab;

    iget-object v1, p0, LX/1af;->b:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 277982
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 277983
    iget-object v1, p0, LX/1af;->f:LX/1ah;

    invoke-virtual {v1, v0}, LX/1ah;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 277984
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->b()V

    .line 277985
    invoke-direct {p0}, LX/1af;->i()V

    .line 277986
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/1af;->c(I)V

    .line 277987
    invoke-direct {p0, p2}, LX/1af;->a(F)V

    .line 277988
    if-eqz p3, :cond_0

    .line 277989
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->f()V

    .line 277990
    :cond_0
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->c()V

    .line 277991
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;LX/1Up;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 277992
    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 277993
    invoke-static {p0, v0}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ao;->a(LX/1Up;)V

    .line 277994
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 277995
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->b()V

    .line 277996
    invoke-direct {p0}, LX/1af;->i()V

    .line 277997
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0, v1}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 277998
    invoke-direct {p0, v1}, LX/1af;->c(I)V

    .line 277999
    :goto_0
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->c()V

    .line 278000
    return-void

    .line 278001
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1af;->c(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 278002
    iget-object v0, p0, LX/1af;->f:LX/1ah;

    iget-object v1, p0, LX/1af;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1ah;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 278003
    invoke-direct {p0}, LX/1af;->h()V

    .line 278004
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 278005
    iget-object v0, p0, LX/1af;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 278006
    return-void
.end method

.method public final b(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 278007
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278008
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ao;->a(Landroid/graphics/PointF;)V

    .line 278009
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277973
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 277974
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;LX/1Up;)V
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 278076
    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 278077
    invoke-static {p0, v0}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ao;->a(LX/1Up;)V

    .line 278078
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 278079
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->b()V

    .line 278080
    invoke-direct {p0}, LX/1af;->i()V

    .line 278081
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0, v1}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 278082
    invoke-direct {p0, v1}, LX/1af;->c(I)V

    .line 278083
    :goto_0
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->c()V

    .line 278084
    return-void

    .line 278085
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1af;->c(I)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 278086
    iget-object v0, p0, LX/1af;->e:LX/1ap;

    .line 278087
    iget p0, v0, LX/1ap;->b:I

    move v0, p0

    .line 278088
    return v0
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278089
    const/4 v0, 0x5

    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 278090
    return-void
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278091
    const/4 v0, 0x4

    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 278092
    return-void
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278093
    const/4 v0, 0x3

    invoke-static {p0, v0, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 278094
    return-void
.end method

.method public final f(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278095
    const/4 v0, 0x0

    .line 278096
    if-ltz v0, :cond_0

    add-int/lit8 v1, v0, 0x6

    iget-object v2, p0, LX/1af;->e:LX/1ap;

    invoke-virtual {v2}, LX/1aq;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "The given index does not correspond to an overlay image."

    invoke-static {v1, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 278097
    add-int/lit8 v1, v0, 0x6

    invoke-static {p0, v1, p1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 278098
    return-void

    .line 278099
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
