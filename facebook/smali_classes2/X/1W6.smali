.class public LX/1W6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1W6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267465
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 267466
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1W6;->b:LX/0Zi;

    .line 267467
    iput-object p1, p0, LX/1W6;->a:LX/0Ot;

    .line 267468
    return-void
.end method

.method public static a(LX/0QB;)LX/1W6;
    .locals 4

    .prologue
    .line 267469
    const-class v1, LX/1W6;

    monitor-enter v1

    .line 267470
    :try_start_0
    sget-object v0, LX/1W6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267471
    sput-object v2, LX/1W6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267472
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267473
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267474
    new-instance v3, LX/1W6;

    const/16 p0, 0x1d46

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1W6;-><init>(LX/0Ot;)V

    .line 267475
    move-object v0, v3

    .line 267476
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267477
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1W6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267478
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 267480
    check-cast p2, LX/BtD;

    .line 267481
    iget-object v0, p0, LX/1W6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;

    iget-object v1, p2, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BtD;->b:LX/1Pn;

    iget-boolean v3, p2, LX/BtD;->c:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Z)LX/1Dg;

    move-result-object v0

    .line 267482
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 267483
    invoke-static {}, LX/1dS;->b()V

    .line 267484
    const/4 v0, 0x0

    return-object v0
.end method
