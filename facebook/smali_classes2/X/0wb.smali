.class public LX/0wb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CallbackClass:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TCallbackClass;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<TCallbackClass;>;"
        }
    .end annotation
.end field

.field private b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<TCallbackClass;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 160245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160246
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0wb;->a:LX/0UE;

    .line 160247
    const/4 v0, 0x0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;

    .line 160248
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TCallbackClass;>;"
        }
    .end annotation

    .prologue
    .line 160237
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wb;->b:LX/0Rf;

    if-nez v0, :cond_0

    .line 160238
    iget-object v0, p0, LX/0wb;->a:LX/0UE;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;

    .line 160239
    :cond_0
    iget-object v0, p0, LX/0wb;->b:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 160240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCallbackClass;)V"
        }
    .end annotation

    .prologue
    .line 160241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wb;->a:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 160242
    const/4 v0, 0x0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160243
    monitor-exit p0

    return-void

    .line 160244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 160233
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wb;->a:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 160234
    const/4 v0, 0x0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160235
    monitor-exit p0

    return-void

    .line 160236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCallbackClass;)V"
        }
    .end annotation

    .prologue
    .line 160225
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wb;->a:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->remove(Ljava/lang/Object;)Z

    .line 160226
    const/4 v0, 0x0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160227
    monitor-exit p0

    return-void

    .line 160228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TCallbackClass;>;"
        }
    .end annotation

    .prologue
    .line 160229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wb;->b:LX/0Rf;

    if-nez v0, :cond_0

    .line 160230
    iget-object v0, p0, LX/0wb;->a:LX/0UE;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0wb;->b:LX/0Rf;

    .line 160231
    :cond_0
    iget-object v0, p0, LX/0wb;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 160232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
