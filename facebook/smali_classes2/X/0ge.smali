.class public final enum LX/0ge;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ge;

.field public static final enum CHECKIN_PREVIEW_ADD_PHOTO_CLICKED:LX/0ge;

.field public static final enum CHECKIN_PREVIEW_DISMISSED:LX/0ge;

.field public static final enum CHECKIN_PREVIEW_SEEN:LX/0ge;

.field public static final enum COMPOSER_ACTION_ITEM_CLICK:LX/0ge;

.field public static final enum COMPOSER_ACTIVITY_RESULT:LX/0ge;

.field public static final enum COMPOSER_ADD_LINK_ATTACHMENT:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_AFTER_TAG_PEOPLE:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_CANCEL:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_CLICK:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_FAILURE:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_READY:LX/0ge;

.field public static final enum COMPOSER_ADD_LOCATION_REMOVE:LX/0ge;

.field public static final enum COMPOSER_ATTACH_MEDIA_CLICK:LX/0ge;

.field public static final enum COMPOSER_ATTACH_MOVIE:LX/0ge;

.field public static final enum COMPOSER_ATTACH_MOVIE_CANCEL:LX/0ge;

.field public static final enum COMPOSER_ATTACH_MOVIE_FAILURE:LX/0ge;

.field public static final enum COMPOSER_ATTACH_MOVIE_REMOVE:LX/0ge;

.field public static final enum COMPOSER_ATTACH_PHOTO:LX/0ge;

.field public static final enum COMPOSER_ATTACH_PHOTO_CANCEL:LX/0ge;

.field public static final enum COMPOSER_ATTACH_PHOTO_FAILURE:LX/0ge;

.field public static final enum COMPOSER_ATTACH_PHOTO_READY:LX/0ge;

.field public static final enum COMPOSER_ATTACH_PHOTO_REMOVE:LX/0ge;

.field public static final enum COMPOSER_CANCELLED:LX/0ge;

.field public static final enum COMPOSER_CANCEL_PRIVACY:LX/0ge;

.field public static final enum COMPOSER_CHANGE_PRIVACY:LX/0ge;

.field public static final enum COMPOSER_COLLAGE_CLICKED:LX/0ge;

.field public static final enum COMPOSER_DELETE_ALL_PENDING_STORIES:LX/0ge;

.field public static final enum COMPOSER_DELETE_ALL_PENDING_STORIES_FAILED:LX/0ge;

.field public static final enum COMPOSER_DISCARD_DIALOG_DISMISSED:LX/0ge;

.field public static final enum COMPOSER_DISCARD_DIALOG_DISPLAYED:LX/0ge;

.field public static final enum COMPOSER_EDIT_ADD_MINUTIAE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_ADD_PLACE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_ADD_WITH_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_CHANGE_MINUTIAE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_CHANGE_PLACE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_CHANGE_WITH_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_REMOVE_MINUTIAE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_REMOVE_PLACE_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_REMOVE_WITH_TAG:LX/0ge;

.field public static final enum COMPOSER_EDIT_TAGS:LX/0ge;

.field public static final enum COMPOSER_ENTRY:LX/0ge;

.field public static final enum COMPOSER_EXIT_FAILURE:LX/0ge;

.field public static final enum COMPOSER_FACECAST_CLICK:LX/0ge;

.field public static final enum COMPOSER_FIRST_CHARACTER_TYPED:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_CANCEL:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_CLICK:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_FAILURE:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_READY:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_REMOVE:LX/0ge;

.field public static final enum COMPOSER_FRIEND_TAG_SUGGESTIONS_SHOWN:LX/0ge;

.field public static final enum COMPOSER_IMPLICIT_LOCATION_CLICK:LX/0ge;

.field public static final enum COMPOSER_IMPLICIT_LOCATION_REMOVED:LX/0ge;

.field public static final enum COMPOSER_INIT:LX/0ge;

.field public static final enum COMPOSER_MAIN_TAB_ACTIVITY_RESULT:LX/0ge;

.field public static final enum COMPOSER_MEDIA_PICKER_SELECTION_CHANGE:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_CANCEL:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_CLICK:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_ICON_CLICKED:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_ICON_PICKER_CANCEL:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_ICON_PICKER_UPDATE:LX/0ge;

.field public static final enum COMPOSER_MINUTIAE_REMOVE:LX/0ge;

.field public static final enum COMPOSER_OPENED_TARGET_SELECTOR:LX/0ge;

.field public static final enum COMPOSER_OPEN_PICKER_FROM_EXIT_CONFIRMATION:LX/0ge;

.field public static final enum COMPOSER_OPEN_PRIVACY:LX/0ge;

.field public static final enum COMPOSER_PAUSE:LX/0ge;

.field public static final enum COMPOSER_PHOTO_THUMBNAIL_CLICKED:LX/0ge;

.field public static final enum COMPOSER_PICKS_PRIVACY_FROM_TYPEAHEAD_FILTER:LX/0ge;

.field public static final enum COMPOSER_POST:LX/0ge;

.field public static final enum COMPOSER_POST_CANCEL:LX/0ge;

.field public static final enum COMPOSER_POST_COMPLETED:LX/0ge;

.field public static final enum COMPOSER_POST_COMPOSITION_CANCEL:LX/0ge;

.field public static final enum COMPOSER_POST_COMPOSITION_CLICK:LX/0ge;

.field public static final enum COMPOSER_POST_FAILURE:LX/0ge;

.field public static final enum COMPOSER_POST_RETRY:LX/0ge;

.field public static final enum COMPOSER_POST_SUCCESS:LX/0ge;

.field public static final enum COMPOSER_PRIVACY_READY:LX/0ge;

.field public static final enum COMPOSER_PUBLISH_FLOW:LX/0ge;

.field public static final enum COMPOSER_PUBLISH_MODE_SELECTOR_CLICK:LX/0ge;

.field public static final enum COMPOSER_PUBLISH_START:LX/0ge;

.field public static final enum COMPOSER_REMOVE_LINK_ATTACHMENT:LX/0ge;

.field public static final enum COMPOSER_REMOVE_PHOTO:LX/0ge;

.field public static final enum COMPOSER_REMOVE_VIDEO:LX/0ge;

.field public static final enum COMPOSER_SELECTABLE_PRIVACY_PILL_CLICKED:LX/0ge;

.field public static final enum COMPOSER_SELECT_ALBUM:LX/0ge;

.field public static final enum COMPOSER_SELECT_ALBUM_CANCEL:LX/0ge;

.field public static final enum COMPOSER_SELECT_ALBUM_CHOOSE:LX/0ge;

.field public static final enum COMPOSER_SELECT_ALBUM_CLICK:LX/0ge;

.field public static final enum COMPOSER_SELECT_ALBUM_NEW:LX/0ge;

.field public static final enum COMPOSER_TAG_EXPANSION_PILL_CLICKED:LX/0ge;

.field public static final enum COMPOSER_TEXT_PASTED:LX/0ge;

.field public static final enum COMPOSER_TEXT_READY:LX/0ge;

.field public static final enum COMPOSER_TEXT_START:LX/0ge;

.field public static final enum COMPOSER_TRANSLITERATE_CLICK:LX/0ge;

.field public static final enum COMPOSER_VIDEO_TAG_CLICK:LX/0ge;

.field public static final enum COMPOSER_VIDEO_THUMBNAIL_CLICKED:LX/0ge;

.field public static final enum DISCARD_SESSION:LX/0ge;

.field public static final enum LOAD_SAVED_SESSION:LX/0ge;

.field public static final enum MEME_BUSTING_SENTRY_WARNING_ACK:LX/0ge;

.field public static final enum MEME_BUSTING_SENTRY_WARNING_CANCEL:LX/0ge;

.field public static final enum MEME_BUSTING_SENTRY_WARNING_RECEIVED:LX/0ge;

.field public static final enum PUBLISH_MODE_OPTION_SELECTED:LX/0ge;

.field public static final enum SAVE_SESSION:LX/0ge;

.field public static final enum TEXT_ONLY_PLACE_POSTED:LX/0ge;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 112736
    new-instance v0, LX/0ge;

    const-string v1, "CHECKIN_PREVIEW_ADD_PHOTO_CLICKED"

    const-string v2, "checkin_preview_add_photo_clicked"

    invoke-direct {v0, v1, v4, v2}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->CHECKIN_PREVIEW_ADD_PHOTO_CLICKED:LX/0ge;

    .line 112737
    new-instance v0, LX/0ge;

    const-string v1, "CHECKIN_PREVIEW_DISMISSED"

    const-string v2, "checkin_preview_dismissed"

    invoke-direct {v0, v1, v5, v2}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->CHECKIN_PREVIEW_DISMISSED:LX/0ge;

    .line 112738
    new-instance v0, LX/0ge;

    const-string v1, "CHECKIN_PREVIEW_SEEN"

    const-string v2, "checkin_preview_seen"

    invoke-direct {v0, v1, v6, v2}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->CHECKIN_PREVIEW_SEEN:LX/0ge;

    .line 112739
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ACTION_ITEM_CLICK"

    const-string v2, "composer_action_item_click"

    invoke-direct {v0, v1, v7, v2}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ACTION_ITEM_CLICK:LX/0ge;

    .line 112740
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ACTIVITY_RESULT"

    const-string v2, "composer_activity_result"

    invoke-direct {v0, v1, v8, v2}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ACTIVITY_RESULT:LX/0ge;

    .line 112741
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LINK_ATTACHMENT"

    const/4 v2, 0x5

    const-string v3, "add_link_attachment"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LINK_ATTACHMENT:LX/0ge;

    .line 112742
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION"

    const/4 v2, 0x6

    const-string v3, "composer_add_location"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION:LX/0ge;

    .line 112743
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_AFTER_TAG_PEOPLE"

    const/4 v2, 0x7

    const-string v3, "composer_add_location_after_tag_people"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_AFTER_TAG_PEOPLE:LX/0ge;

    .line 112744
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_CANCEL"

    const/16 v2, 0x8

    const-string v3, "composer_add_location_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_CANCEL:LX/0ge;

    .line 112745
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_CLICK"

    const/16 v2, 0x9

    const-string v3, "composer_add_location_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_CLICK:LX/0ge;

    .line 112746
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_FAILURE"

    const/16 v2, 0xa

    const-string v3, "composer_add_location_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_FAILURE:LX/0ge;

    .line 112747
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_READY"

    const/16 v2, 0xb

    const-string v3, "composer_add_location_ready"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_READY:LX/0ge;

    .line 112748
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ADD_LOCATION_REMOVE"

    const/16 v2, 0xc

    const-string v3, "composer_add_location_remove"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_REMOVE:LX/0ge;

    .line 112749
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_MEDIA_CLICK"

    const/16 v2, 0xd

    const-string v3, "intent_media"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_MEDIA_CLICK:LX/0ge;

    .line 112750
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_MOVIE"

    const/16 v2, 0xe

    const-string v3, "composer_attach_movie"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_MOVIE:LX/0ge;

    .line 112751
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_MOVIE_CANCEL"

    const/16 v2, 0xf

    const-string v3, "composer_attach_movie_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_MOVIE_CANCEL:LX/0ge;

    .line 112752
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_MOVIE_FAILURE"

    const/16 v2, 0x10

    const-string v3, "composer_attach_movie_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_MOVIE_FAILURE:LX/0ge;

    .line 112753
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_MOVIE_REMOVE"

    const/16 v2, 0x11

    const-string v3, "composer_attach_movie_remove"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_MOVIE_REMOVE:LX/0ge;

    .line 112754
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_PHOTO"

    const/16 v2, 0x12

    const-string v3, "composer_attach_photo"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_PHOTO:LX/0ge;

    .line 112755
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_PHOTO_CANCEL"

    const/16 v2, 0x13

    const-string v3, "composer_attach_photo_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_PHOTO_CANCEL:LX/0ge;

    .line 112756
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_PHOTO_FAILURE"

    const/16 v2, 0x14

    const-string v3, "composer_attach_photo_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_PHOTO_FAILURE:LX/0ge;

    .line 112757
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_PHOTO_READY"

    const/16 v2, 0x15

    const-string v3, "composer_attach_photo_ready"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_PHOTO_READY:LX/0ge;

    .line 112758
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ATTACH_PHOTO_REMOVE"

    const/16 v2, 0x16

    const-string v3, "composer_attach_photo_remove"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ATTACH_PHOTO_REMOVE:LX/0ge;

    .line 112759
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_CANCELLED"

    const/16 v2, 0x17

    const-string v3, "composer_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_CANCELLED:LX/0ge;

    .line 112760
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_CANCEL_PRIVACY"

    const/16 v2, 0x18

    const-string v3, "composer_cancel_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_CANCEL_PRIVACY:LX/0ge;

    .line 112761
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_CHANGE_PRIVACY"

    const/16 v2, 0x19

    const-string v3, "composer_change_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_CHANGE_PRIVACY:LX/0ge;

    .line 112762
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_COLLAGE_CLICKED"

    const/16 v2, 0x1a

    const-string v3, "composer_collage_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_COLLAGE_CLICKED:LX/0ge;

    .line 112763
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_DELETE_ALL_PENDING_STORIES"

    const/16 v2, 0x1b

    const-string v3, "composer_delete_all_pending_stories"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_DELETE_ALL_PENDING_STORIES:LX/0ge;

    .line 112764
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_DELETE_ALL_PENDING_STORIES_FAILED"

    const/16 v2, 0x1c

    const-string v3, "composer_delete_all_pending_stories_failed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_DELETE_ALL_PENDING_STORIES_FAILED:LX/0ge;

    .line 112765
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_DISCARD_DIALOG_DISMISSED"

    const/16 v2, 0x1d

    const-string v3, "composer_discard_dialog_dismissed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISMISSED:LX/0ge;

    .line 112766
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_DISCARD_DIALOG_DISPLAYED"

    const/16 v2, 0x1e

    const-string v3, "composer_discard_dialog_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISPLAYED:LX/0ge;

    .line 112767
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_ADD_MINUTIAE_TAG"

    const/16 v2, 0x1f

    const-string v3, "composer_edit_add_minutiae_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_ADD_MINUTIAE_TAG:LX/0ge;

    .line 112768
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_ADD_PLACE_TAG"

    const/16 v2, 0x20

    const-string v3, "composer_edit_add_place_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_ADD_PLACE_TAG:LX/0ge;

    .line 112769
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_ADD_WITH_TAG"

    const/16 v2, 0x21

    const-string v3, "composer_edit_add_with_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_ADD_WITH_TAG:LX/0ge;

    .line 112770
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_CHANGE_MINUTIAE_TAG"

    const/16 v2, 0x22

    const-string v3, "composer_edit_change_minutiae_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_CHANGE_MINUTIAE_TAG:LX/0ge;

    .line 112771
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_CHANGE_PLACE_TAG"

    const/16 v2, 0x23

    const-string v3, "composer_edit_change_place_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_CHANGE_PLACE_TAG:LX/0ge;

    .line 112772
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_CHANGE_WITH_TAG"

    const/16 v2, 0x24

    const-string v3, "composer_edit_change_with_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_CHANGE_WITH_TAG:LX/0ge;

    .line 112773
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_REMOVE_MINUTIAE_TAG"

    const/16 v2, 0x25

    const-string v3, "composer_edit_remove_minutiae_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_REMOVE_MINUTIAE_TAG:LX/0ge;

    .line 112774
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_REMOVE_PLACE_TAG"

    const/16 v2, 0x26

    const-string v3, "composer_edit_remove_place_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_REMOVE_PLACE_TAG:LX/0ge;

    .line 112775
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_REMOVE_WITH_TAG"

    const/16 v2, 0x27

    const-string v3, "composer_edit_remove_with_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_REMOVE_WITH_TAG:LX/0ge;

    .line 112776
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EDIT_TAGS"

    const/16 v2, 0x28

    const-string v3, "composer_edit_tags"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EDIT_TAGS:LX/0ge;

    .line 112777
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_ENTRY"

    const/16 v2, 0x29

    const-string v3, "composer_entry"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_ENTRY:LX/0ge;

    .line 112778
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_EXIT_FAILURE"

    const/16 v2, 0x2a

    const-string v3, "composer_exit_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_EXIT_FAILURE:LX/0ge;

    .line 112779
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FACECAST_CLICK"

    const/16 v2, 0x2b

    const-string v3, "composer_facecast_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FACECAST_CLICK:LX/0ge;

    .line 112780
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FIRST_CHARACTER_TYPED"

    const/16 v2, 0x2c

    const-string v3, "composer_first_character_typed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FIRST_CHARACTER_TYPED:LX/0ge;

    .line 112781
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG"

    const/16 v2, 0x2d

    const-string v3, "composer_friend_tag"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG:LX/0ge;

    .line 112782
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_CANCEL"

    const/16 v2, 0x2e

    const-string v3, "composer_friend_tag_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_CANCEL:LX/0ge;

    .line 112783
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_CLICK"

    const/16 v2, 0x2f

    const-string v3, "composer_friend_tag_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_CLICK:LX/0ge;

    .line 112784
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_FAILURE"

    const/16 v2, 0x30

    const-string v3, "composer_friend_tag_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_FAILURE:LX/0ge;

    .line 112785
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_READY"

    const/16 v2, 0x31

    const-string v3, "composer_friend_tag_ready"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_READY:LX/0ge;

    .line 112786
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_REMOVE"

    const/16 v2, 0x32

    const-string v3, "composer_friend_tag_remove"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_REMOVE:LX/0ge;

    .line 112787
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_FRIEND_TAG_SUGGESTIONS_SHOWN"

    const/16 v2, 0x33

    const-string v3, "composer_friend_tag_suggestions_shown"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_SUGGESTIONS_SHOWN:LX/0ge;

    .line 112788
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_IMPLICIT_LOCATION_CLICK"

    const/16 v2, 0x34

    const-string v3, "composer_implicit_location_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_IMPLICIT_LOCATION_CLICK:LX/0ge;

    .line 112789
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_IMPLICIT_LOCATION_REMOVED"

    const/16 v2, 0x35

    const-string v3, "composer_implicit_location_removed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_IMPLICIT_LOCATION_REMOVED:LX/0ge;

    .line 112790
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_INIT"

    const/16 v2, 0x36

    const-string v3, "composer_init"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_INIT:LX/0ge;

    .line 112791
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MAIN_TAB_ACTIVITY_RESULT"

    const/16 v2, 0x37

    const-string v3, "composer_main_tab_activity_result"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MAIN_TAB_ACTIVITY_RESULT:LX/0ge;

    .line 112792
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MEDIA_PICKER_SELECTION_CHANGE"

    const/16 v2, 0x38

    const-string v3, "composer_media_picker_selection_change"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MEDIA_PICKER_SELECTION_CHANGE:LX/0ge;

    .line 112793
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE"

    const/16 v2, 0x39

    const-string v3, "composer_minutiae"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE:LX/0ge;

    .line 112794
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_CANCEL"

    const/16 v2, 0x3a

    const-string v3, "composer_minutiae_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_CANCEL:LX/0ge;

    .line 112795
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_CLICK"

    const/16 v2, 0x3b

    const-string v3, "composer_minutiae_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_CLICK:LX/0ge;

    .line 112796
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_ICON_CLICKED"

    const/16 v2, 0x3c

    const-string v3, "composer_minutiae_icon_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_ICON_CLICKED:LX/0ge;

    .line 112797
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_ICON_PICKER_CANCEL"

    const/16 v2, 0x3d

    const-string v3, "composer_minutiae_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_CANCEL:LX/0ge;

    .line 112798
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_ICON_PICKER_UPDATE"

    const/16 v2, 0x3e

    const-string v3, "composer_minutiae_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_UPDATE:LX/0ge;

    .line 112799
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_MINUTIAE_REMOVE"

    const/16 v2, 0x3f

    const-string v3, "composer_minutiae_remove"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_MINUTIAE_REMOVE:LX/0ge;

    .line 112800
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_OPENED_TARGET_SELECTOR"

    const/16 v2, 0x40

    const-string v3, "composer_opened_target_selector"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_OPENED_TARGET_SELECTOR:LX/0ge;

    .line 112801
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_OPEN_PICKER_FROM_EXIT_CONFIRMATION"

    const/16 v2, 0x41

    const-string v3, "composer_open_picker_from_exit_confirmation"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_OPEN_PICKER_FROM_EXIT_CONFIRMATION:LX/0ge;

    .line 112802
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_OPEN_PRIVACY"

    const/16 v2, 0x42

    const-string v3, "composer_open_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_OPEN_PRIVACY:LX/0ge;

    .line 112803
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PAUSE"

    const/16 v2, 0x43

    const-string v3, "composer_pause"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PAUSE:LX/0ge;

    .line 112804
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PHOTO_THUMBNAIL_CLICKED"

    const/16 v2, 0x44

    const-string v3, "clicked_on_photo_thumbnail"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PHOTO_THUMBNAIL_CLICKED:LX/0ge;

    .line 112805
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PICKS_PRIVACY_FROM_TYPEAHEAD_FILTER"

    const/16 v2, 0x45

    const-string v3, "composer_picks_privacy_from_typeahead_filter"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PICKS_PRIVACY_FROM_TYPEAHEAD_FILTER:LX/0ge;

    .line 112806
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST"

    const/16 v2, 0x46

    const-string v3, "composer_post"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST:LX/0ge;

    .line 112807
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_CANCEL"

    const/16 v2, 0x47

    const-string v3, "composer_post_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_CANCEL:LX/0ge;

    .line 112808
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_COMPLETED"

    const/16 v2, 0x48

    const-string v3, "composer_post_completed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_COMPLETED:LX/0ge;

    .line 112809
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_COMPOSITION_CANCEL"

    const/16 v2, 0x49

    const-string v3, "post_composition_closed"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_COMPOSITION_CANCEL:LX/0ge;

    .line 112810
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_COMPOSITION_CLICK"

    const/16 v2, 0x4a

    const-string v3, "post_composition_opened"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_COMPOSITION_CLICK:LX/0ge;

    .line 112811
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_FAILURE"

    const/16 v2, 0x4b

    const-string v3, "composer_post_failure"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_FAILURE:LX/0ge;

    .line 112812
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_RETRY"

    const/16 v2, 0x4c

    const-string v3, "composer_post_retry"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_RETRY:LX/0ge;

    .line 112813
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_POST_SUCCESS"

    const/16 v2, 0x4d

    const-string v3, "composer_post_success"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_POST_SUCCESS:LX/0ge;

    .line 112814
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PRIVACY_READY"

    const/16 v2, 0x4e

    const-string v3, "composer_privacy_ready"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PRIVACY_READY:LX/0ge;

    .line 112815
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PUBLISH_MODE_SELECTOR_CLICK"

    const/16 v2, 0x4f

    const-string v3, "post_options_picker_started"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PUBLISH_MODE_SELECTOR_CLICK:LX/0ge;

    .line 112816
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PUBLISH_START"

    const/16 v2, 0x50

    const-string v3, "composer_publish_start"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PUBLISH_START:LX/0ge;

    .line 112817
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_PUBLISH_FLOW"

    const/16 v2, 0x51

    const-string v3, "composer_publish_flow"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_PUBLISH_FLOW:LX/0ge;

    .line 112818
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_REMOVE_LINK_ATTACHMENT"

    const/16 v2, 0x52

    const-string v3, "remove_link_attachment"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_REMOVE_LINK_ATTACHMENT:LX/0ge;

    .line 112819
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_REMOVE_PHOTO"

    const/16 v2, 0x53

    const-string v3, "remove_photo"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_REMOVE_PHOTO:LX/0ge;

    .line 112820
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_REMOVE_VIDEO"

    const/16 v2, 0x54

    const-string v3, "remove_video"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_REMOVE_VIDEO:LX/0ge;

    .line 112821
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECTABLE_PRIVACY_PILL_CLICKED"

    const/16 v2, 0x55

    const-string v3, "composer_selectable_privacy_pill_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECTABLE_PRIVACY_PILL_CLICKED:LX/0ge;

    .line 112822
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECT_ALBUM"

    const/16 v2, 0x56

    const-string v3, "composer_select_album"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECT_ALBUM:LX/0ge;

    .line 112823
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECT_ALBUM_CANCEL"

    const/16 v2, 0x57

    const-string v3, "composer_select_album_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECT_ALBUM_CANCEL:LX/0ge;

    .line 112824
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECT_ALBUM_CHOOSE"

    const/16 v2, 0x58

    const-string v3, "composer_select_album_choose"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECT_ALBUM_CHOOSE:LX/0ge;

    .line 112825
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECT_ALBUM_CLICK"

    const/16 v2, 0x59

    const-string v3, "composer_select_album_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECT_ALBUM_CLICK:LX/0ge;

    .line 112826
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_SELECT_ALBUM_NEW"

    const/16 v2, 0x5a

    const-string v3, "composer_select_album_new"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_SELECT_ALBUM_NEW:LX/0ge;

    .line 112827
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_TAG_EXPANSION_PILL_CLICKED"

    const/16 v2, 0x5b

    const-string v3, "composer_tag_expansion_pill_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_TAG_EXPANSION_PILL_CLICKED:LX/0ge;

    .line 112828
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_TEXT_PASTED"

    const/16 v2, 0x5c

    const-string v3, "composer_text_pasted"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_TEXT_PASTED:LX/0ge;

    .line 112829
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_TEXT_READY"

    const/16 v2, 0x5d

    const-string v3, "composer_text_ready"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_TEXT_READY:LX/0ge;

    .line 112830
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_TEXT_START"

    const/16 v2, 0x5e

    const-string v3, "composer_text_start"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_TEXT_START:LX/0ge;

    .line 112831
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_TRANSLITERATE_CLICK"

    const/16 v2, 0x5f

    const-string v3, "composer_transliterate_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_TRANSLITERATE_CLICK:LX/0ge;

    .line 112832
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_VIDEO_TAG_CLICK"

    const/16 v2, 0x60

    const-string v3, "composer_video_tag_click"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_VIDEO_TAG_CLICK:LX/0ge;

    .line 112833
    new-instance v0, LX/0ge;

    const-string v1, "COMPOSER_VIDEO_THUMBNAIL_CLICKED"

    const/16 v2, 0x61

    const-string v3, "clicked_on_video_thumbnail"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->COMPOSER_VIDEO_THUMBNAIL_CLICKED:LX/0ge;

    .line 112834
    new-instance v0, LX/0ge;

    const-string v1, "DISCARD_SESSION"

    const/16 v2, 0x62

    const-string v3, "discard_draft"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->DISCARD_SESSION:LX/0ge;

    .line 112835
    new-instance v0, LX/0ge;

    const-string v1, "LOAD_SAVED_SESSION"

    const/16 v2, 0x63

    const-string v3, "load_draft"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->LOAD_SAVED_SESSION:LX/0ge;

    .line 112836
    new-instance v0, LX/0ge;

    const-string v1, "MEME_BUSTING_SENTRY_WARNING_ACK"

    const/16 v2, 0x64

    const-string v3, "meme_busting_sentry_warning_ack"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_ACK:LX/0ge;

    .line 112837
    new-instance v0, LX/0ge;

    const-string v1, "MEME_BUSTING_SENTRY_WARNING_CANCEL"

    const/16 v2, 0x65

    const-string v3, "meme_busting_sentry_warning_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_CANCEL:LX/0ge;

    .line 112838
    new-instance v0, LX/0ge;

    const-string v1, "MEME_BUSTING_SENTRY_WARNING_RECEIVED"

    const/16 v2, 0x66

    const-string v3, "meme_busting_sentry_warning_received"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_RECEIVED:LX/0ge;

    .line 112839
    new-instance v0, LX/0ge;

    const-string v1, "PUBLISH_MODE_OPTION_SELECTED"

    const/16 v2, 0x67

    const-string v3, "post_options_picker_selected_post_option"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->PUBLISH_MODE_OPTION_SELECTED:LX/0ge;

    .line 112840
    new-instance v0, LX/0ge;

    const-string v1, "SAVE_SESSION"

    const/16 v2, 0x68

    const-string v3, "save_draft"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->SAVE_SESSION:LX/0ge;

    .line 112841
    new-instance v0, LX/0ge;

    const-string v1, "TEXT_ONLY_PLACE_POSTED"

    const/16 v2, 0x69

    const-string v3, "text_only_place_posted"

    invoke-direct {v0, v1, v2, v3}, LX/0ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ge;->TEXT_ONLY_PLACE_POSTED:LX/0ge;

    .line 112842
    const/16 v0, 0x6a

    new-array v0, v0, [LX/0ge;

    sget-object v1, LX/0ge;->CHECKIN_PREVIEW_ADD_PHOTO_CLICKED:LX/0ge;

    aput-object v1, v0, v4

    sget-object v1, LX/0ge;->CHECKIN_PREVIEW_DISMISSED:LX/0ge;

    aput-object v1, v0, v5

    sget-object v1, LX/0ge;->CHECKIN_PREVIEW_SEEN:LX/0ge;

    aput-object v1, v0, v6

    sget-object v1, LX/0ge;->COMPOSER_ACTION_ITEM_CLICK:LX/0ge;

    aput-object v1, v0, v7

    sget-object v1, LX/0ge;->COMPOSER_ACTIVITY_RESULT:LX/0ge;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0ge;->COMPOSER_ADD_LINK_ATTACHMENT:LX/0ge;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION:LX/0ge;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_AFTER_TAG_PEOPLE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_READY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_REMOVE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MEDIA_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MOVIE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MOVIE_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MOVIE_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MOVIE_REMOVE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_READY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_REMOVE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0ge;->COMPOSER_CANCELLED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0ge;->COMPOSER_CANCEL_PRIVACY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0ge;->COMPOSER_CHANGE_PRIVACY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/0ge;->COMPOSER_COLLAGE_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/0ge;->COMPOSER_DELETE_ALL_PENDING_STORIES:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/0ge;->COMPOSER_DELETE_ALL_PENDING_STORIES_FAILED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISMISSED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISPLAYED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/0ge;->COMPOSER_EDIT_ADD_MINUTIAE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/0ge;->COMPOSER_EDIT_ADD_PLACE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/0ge;->COMPOSER_EDIT_ADD_WITH_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/0ge;->COMPOSER_EDIT_CHANGE_MINUTIAE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/0ge;->COMPOSER_EDIT_CHANGE_PLACE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/0ge;->COMPOSER_EDIT_CHANGE_WITH_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/0ge;->COMPOSER_EDIT_REMOVE_MINUTIAE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/0ge;->COMPOSER_EDIT_REMOVE_PLACE_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/0ge;->COMPOSER_EDIT_REMOVE_WITH_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/0ge;->COMPOSER_EDIT_TAGS:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/0ge;->COMPOSER_ENTRY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/0ge;->COMPOSER_EXIT_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/0ge;->COMPOSER_FACECAST_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/0ge;->COMPOSER_FIRST_CHARACTER_TYPED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_READY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_REMOVE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_SUGGESTIONS_SHOWN:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/0ge;->COMPOSER_IMPLICIT_LOCATION_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/0ge;->COMPOSER_IMPLICIT_LOCATION_REMOVED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/0ge;->COMPOSER_INIT:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/0ge;->COMPOSER_MAIN_TAB_ACTIVITY_RESULT:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/0ge;->COMPOSER_MEDIA_PICKER_SELECTION_CHANGE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_ICON_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_UPDATE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_REMOVE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/0ge;->COMPOSER_OPENED_TARGET_SELECTOR:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/0ge;->COMPOSER_OPEN_PICKER_FROM_EXIT_CONFIRMATION:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/0ge;->COMPOSER_OPEN_PRIVACY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/0ge;->COMPOSER_PAUSE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/0ge;->COMPOSER_PHOTO_THUMBNAIL_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/0ge;->COMPOSER_PICKS_PRIVACY_FROM_TYPEAHEAD_FILTER:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/0ge;->COMPOSER_POST:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/0ge;->COMPOSER_POST_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/0ge;->COMPOSER_POST_COMPLETED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/0ge;->COMPOSER_POST_COMPOSITION_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/0ge;->COMPOSER_POST_COMPOSITION_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/0ge;->COMPOSER_POST_FAILURE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/0ge;->COMPOSER_POST_RETRY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/0ge;->COMPOSER_POST_SUCCESS:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/0ge;->COMPOSER_PRIVACY_READY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/0ge;->COMPOSER_PUBLISH_MODE_SELECTOR_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/0ge;->COMPOSER_PUBLISH_START:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/0ge;->COMPOSER_PUBLISH_FLOW:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/0ge;->COMPOSER_REMOVE_LINK_ATTACHMENT:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/0ge;->COMPOSER_REMOVE_PHOTO:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/0ge;->COMPOSER_REMOVE_VIDEO:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/0ge;->COMPOSER_SELECTABLE_PRIVACY_PILL_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/0ge;->COMPOSER_SELECT_ALBUM:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/0ge;->COMPOSER_SELECT_ALBUM_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/0ge;->COMPOSER_SELECT_ALBUM_CHOOSE:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/0ge;->COMPOSER_SELECT_ALBUM_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, LX/0ge;->COMPOSER_SELECT_ALBUM_NEW:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, LX/0ge;->COMPOSER_TAG_EXPANSION_PILL_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, LX/0ge;->COMPOSER_TEXT_PASTED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, LX/0ge;->COMPOSER_TEXT_READY:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, LX/0ge;->COMPOSER_TEXT_START:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, LX/0ge;->COMPOSER_TRANSLITERATE_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, LX/0ge;->COMPOSER_VIDEO_TAG_CLICK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, LX/0ge;->COMPOSER_VIDEO_THUMBNAIL_CLICKED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, LX/0ge;->DISCARD_SESSION:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, LX/0ge;->LOAD_SAVED_SESSION:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_ACK:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_CANCEL:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_RECEIVED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, LX/0ge;->PUBLISH_MODE_OPTION_SELECTED:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, LX/0ge;->SAVE_SESSION:LX/0ge;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, LX/0ge;->TEXT_ONLY_PLACE_POSTED:LX/0ge;

    aput-object v2, v0, v1

    sput-object v0, LX/0ge;->$VALUES:[LX/0ge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112731
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112732
    iput-object p3, p0, LX/0ge;->name:Ljava/lang/String;

    .line 112733
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ge;
    .locals 1

    .prologue
    .line 112735
    const-class v0, LX/0ge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ge;

    return-object v0
.end method

.method public static values()[LX/0ge;
    .locals 1

    .prologue
    .line 112734
    sget-object v0, LX/0ge;->$VALUES:[LX/0ge;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ge;

    return-object v0
.end method
