.class public LX/14x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/14x;


# instance fields
.field private final a:LX/14z;

.field private final b:LX/0s6;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/150;


# direct methods
.method public constructor <init>(LX/14z;LX/0s6;LX/0Or;LX/150;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14z;",
            "LX/0s6;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/150;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179789
    iput-object p1, p0, LX/14x;->a:LX/14z;

    .line 179790
    iput-object p2, p0, LX/14x;->b:LX/0s6;

    .line 179791
    iput-object p3, p0, LX/14x;->c:LX/0Or;

    .line 179792
    iput-object p4, p0, LX/14x;->d:LX/150;

    .line 179793
    return-void
.end method

.method public static a(LX/0QB;)LX/14x;
    .locals 7

    .prologue
    .line 179775
    sget-object v0, LX/14x;->e:LX/14x;

    if-nez v0, :cond_1

    .line 179776
    const-class v1, LX/14x;

    monitor-enter v1

    .line 179777
    :try_start_0
    sget-object v0, LX/14x;->e:LX/14x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 179778
    if-eqz v2, :cond_0

    .line 179779
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 179780
    new-instance v6, LX/14x;

    invoke-static {v0}, LX/14y;->a(LX/0QB;)LX/14z;

    move-result-object v3

    check-cast v3, LX/14z;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/150;->a(LX/0QB;)LX/150;

    move-result-object v5

    check-cast v5, LX/150;

    invoke-direct {v6, v3, v4, p0, v5}, LX/14x;-><init>(LX/14z;LX/0s6;LX/0Or;LX/150;)V

    .line 179781
    move-object v0, v6

    .line 179782
    sput-object v0, LX/14x;->e:LX/14x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179783
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 179784
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179785
    :cond_1
    sget-object v0, LX/14x;->e:LX/14x;

    return-object v0

    .line 179786
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 179787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 179774
    invoke-virtual {p0}, LX/14x;->e()Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 179794
    invoke-virtual {p0}, LX/14x;->c()Ljava/lang/String;

    move-result-object v1

    .line 179795
    if-eqz v1, :cond_0

    .line 179796
    invoke-static {v1, p1}, LX/14z;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 179797
    :cond_0
    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 179772
    invoke-virtual {p0}, LX/14x;->e()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 179773
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179764
    invoke-virtual {p0}, LX/14x;->e()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 179765
    if-eqz v0, :cond_0

    .line 179766
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 179767
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 179769
    iget-object v0, p0, LX/14x;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 179770
    iget-object v1, p0, LX/14x;->d:LX/150;

    invoke-virtual {v1, v0}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 179771
    iget-boolean v0, v0, LX/152;->a:Z

    return v0
.end method

.method public final e()Landroid/content/pm/PackageInfo;
    .locals 3

    .prologue
    .line 179768
    iget-object v0, p0, LX/14x;->b:LX/0s6;

    invoke-static {}, LX/2g8;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method
