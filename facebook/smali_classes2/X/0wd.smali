.class public LX/0wd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:I


# instance fields
.field public b:LX/0wT;

.field public c:Z

.field public final d:Ljava/lang/String;

.field public final e:LX/0we;

.field private final f:LX/0we;

.field private final g:LX/0we;

.field private h:D

.field public i:D

.field public j:Z

.field public k:D

.field public l:D

.field private m:LX/0wb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0wb",
            "<",
            "LX/0xi;",
            ">;"
        }
    .end annotation
.end field

.field private n:D

.field private final o:LX/0wW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160391
    const/4 v0, 0x0

    sput v0, LX/0wd;->a:I

    return-void
.end method

.method public constructor <init>(LX/0wW;)V
    .locals 4
    .param p1    # LX/0wW;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    .line 160349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160350
    new-instance v0, LX/0we;

    invoke-direct {v0}, LX/0we;-><init>()V

    iput-object v0, p0, LX/0wd;->e:LX/0we;

    .line 160351
    new-instance v0, LX/0we;

    invoke-direct {v0}, LX/0we;-><init>()V

    iput-object v0, p0, LX/0wd;->f:LX/0we;

    .line 160352
    new-instance v0, LX/0we;

    invoke-direct {v0}, LX/0we;-><init>()V

    iput-object v0, p0, LX/0wd;->g:LX/0we;

    .line 160353
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0wd;->j:Z

    .line 160354
    iput-wide v2, p0, LX/0wd;->k:D

    .line 160355
    iput-wide v2, p0, LX/0wd;->l:D

    .line 160356
    new-instance v0, LX/0wb;

    invoke-direct {v0}, LX/0wb;-><init>()V

    iput-object v0, p0, LX/0wd;->m:LX/0wb;

    .line 160357
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0wd;->n:D

    .line 160358
    const-string v0, "Spring cannot be created outside of a SpringSystem"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160359
    iput-object p1, p0, LX/0wd;->o:LX/0wW;

    .line 160360
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spring:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, LX/0wd;->a:I

    add-int/lit8 v2, v1, 0x1

    sput v2, LX/0wd;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0wd;->d:Ljava/lang/String;

    .line 160361
    return-void
.end method

.method private h(D)V
    .locals 11

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 160388
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iget-object v1, p0, LX/0wd;->e:LX/0we;

    iget-wide v2, v1, LX/0we;->a:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/0wd;->f:LX/0we;

    iget-wide v4, v1, LX/0we;->a:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/0we;->a:D

    .line 160389
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iget-object v1, p0, LX/0wd;->e:LX/0we;

    iget-wide v2, v1, LX/0we;->b:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/0wd;->f:LX/0we;

    iget-wide v4, v1, LX/0we;->b:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/0we;->b:D

    .line 160390
    return-void
.end method

.method private l()Z
    .locals 4

    .prologue
    .line 160387
    iget-wide v0, p0, LX/0wd;->h:D

    iget-wide v2, p0, LX/0wd;->i:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p0}, LX/0wd;->d()D

    move-result-wide v0

    iget-wide v2, p0, LX/0wd;->i:D

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    iget-wide v0, p0, LX/0wd;->h:D

    iget-wide v2, p0, LX/0wd;->i:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    invoke-virtual {p0}, LX/0wd;->d()D

    move-result-wide v0

    iget-wide v2, p0, LX/0wd;->i:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(D)LX/0wd;
    .locals 3

    .prologue
    .line 160382
    iput-wide p1, p0, LX/0wd;->h:D

    .line 160383
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iput-wide p1, v0, LX/0we;->a:D

    .line 160384
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    .line 160385
    invoke-interface {v0, p0}, LX/0xi;->a(LX/0wd;)V

    goto :goto_0

    .line 160386
    :cond_0
    return-object p0
.end method

.method public final a(LX/0wT;)LX/0wd;
    .locals 1

    .prologue
    .line 160380
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wT;

    iput-object v0, p0, LX/0wd;->b:LX/0wT;

    .line 160381
    return-object p0
.end method

.method public final a(LX/0xi;)LX/0wd;
    .locals 1

    .prologue
    .line 160377
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160378
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0, p1}, LX/0wb;->a(Ljava/lang/Object;)V

    .line 160379
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 160374
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->b()V

    .line 160375
    iget-object v0, p0, LX/0wd;->o:LX/0wW;

    invoke-virtual {v0, p0}, LX/0wW;->a(LX/0wd;)V

    .line 160376
    return-void
.end method

.method public final b(D)LX/0wd;
    .locals 3

    .prologue
    .line 160365
    iget-wide v0, p0, LX/0wd;->i:D

    cmpl-double v0, v0, p1

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160366
    :cond_0
    return-object p0

    .line 160367
    :cond_1
    invoke-virtual {p0}, LX/0wd;->d()D

    move-result-wide v0

    iput-wide v0, p0, LX/0wd;->h:D

    .line 160368
    iput-wide p1, p0, LX/0wd;->i:D

    .line 160369
    iget-object v0, p0, LX/0wd;->o:LX/0wW;

    .line 160370
    iget-object v1, p0, LX/0wd;->d:Ljava/lang/String;

    move-object v1, v1

    .line 160371
    invoke-virtual {v0, v1}, LX/0wW;->a(Ljava/lang/String;)V

    .line 160372
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    .line 160373
    invoke-interface {v0, p0}, LX/0xi;->d(LX/0wd;)V

    goto :goto_0
.end method

.method public final b(LX/0xi;)LX/0wd;
    .locals 1

    .prologue
    .line 160362
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160363
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0, p1}, LX/0wb;->b(Ljava/lang/Object;)V

    .line 160364
    return-object p0
.end method

.method public final c(D)LX/0wd;
    .locals 3

    .prologue
    .line 160275
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iput-wide p1, v0, LX/0we;->b:D

    .line 160276
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    .line 160277
    iget-object v0, p0, LX/0wd;->o:LX/0wW;

    .line 160278
    iget-object v1, p0, LX/0wd;->d:Ljava/lang/String;

    move-object v1, v1

    .line 160279
    invoke-virtual {v0, v1}, LX/0wW;->a(Ljava/lang/String;)V

    .line 160280
    :cond_0
    return-object p0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 160281
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iget-wide v0, v0, LX/0we;->a:D

    return-wide v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 160282
    iget-wide v0, p0, LX/0wd;->i:D

    return-wide v0
.end method

.method public final f(D)V
    .locals 33
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 160283
    invoke-virtual/range {p0 .. p0}, LX/0wd;->i()Z

    move-result v10

    .line 160284
    if-eqz v10, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0wd;->j:Z

    if-eqz v2, :cond_1

    .line 160285
    :cond_0
    return-void

    .line 160286
    :cond_1
    const-wide v2, 0x3fb0624dd2f1a9fcL    # 0.064

    cmpl-double v2, p1, v2

    if-lez v2, :cond_2

    .line 160287
    const-wide p1, 0x3fb0624dd2f1a9fcL    # 0.064

    .line 160288
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->n:D

    add-double v2, v2, p1

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/0wd;->n:D

    .line 160289
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->b:LX/0wT;

    iget-wide v12, v2, LX/0wT;->b:D

    .line 160290
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->b:LX/0wT;

    iget-wide v14, v2, LX/0wT;->a:D

    .line 160291
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->e:LX/0we;

    iget-wide v8, v2, LX/0we;->a:D

    .line 160292
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->e:LX/0we;

    iget-wide v6, v2, LX/0we;->b:D

    .line 160293
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->g:LX/0we;

    iget-wide v4, v2, LX/0we;->a:D

    .line 160294
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->g:LX/0we;

    iget-wide v2, v2, LX/0we;->b:D

    .line 160295
    :goto_0
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0wd;->n:D

    move-wide/from16 v16, v0

    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v11, v16, v18

    if-ltz v11, :cond_4

    .line 160296
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->n:D

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    sub-double v2, v2, v16

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/0wd;->n:D

    .line 160297
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->n:D

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v2, v2, v16

    if-gez v2, :cond_3

    .line 160298
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->f:LX/0we;

    iput-wide v8, v2, LX/0we;->a:D

    .line 160299
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->f:LX/0we;

    iput-wide v6, v2, LX/0we;->b:D

    .line 160300
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->i:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v12

    mul-double v4, v14, v6

    sub-double v16, v2, v4

    .line 160301
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, v6

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 160302
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v4, v4, v16

    const-wide/high16 v18, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v18

    add-double v18, v6, v4

    .line 160303
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/0wd;->i:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v12

    mul-double v4, v14, v18

    sub-double v20, v2, v4

    .line 160304
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v18

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 160305
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v4, v4, v20

    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v22

    add-double v22, v6, v4

    .line 160306
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/0wd;->i:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v12

    mul-double v4, v14, v22

    sub-double v24, v2, v4

    .line 160307
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v22

    add-double v4, v8, v2

    .line 160308
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v24

    add-double/2addr v2, v6

    .line 160309
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0wd;->i:D

    move-wide/from16 v26, v0

    sub-double v26, v26, v4

    mul-double v26, v26, v12

    mul-double v28, v14, v2

    sub-double v26, v26, v28

    .line 160310
    const-wide v28, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    add-double v18, v18, v22

    mul-double v18, v18, v30

    add-double v18, v18, v6

    add-double v18, v18, v2

    mul-double v18, v18, v28

    .line 160311
    const-wide v22, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    add-double v20, v20, v24

    mul-double v20, v20, v28

    add-double v16, v16, v20

    add-double v16, v16, v26

    mul-double v16, v16, v22

    .line 160312
    const-wide v20, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v18, v18, v20

    add-double v8, v8, v18

    .line 160313
    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v16, v16, v18

    add-double v6, v6, v16

    goto/16 :goto_0

    .line 160314
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, LX/0wd;->g:LX/0we;

    iput-wide v4, v11, LX/0we;->a:D

    .line 160315
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0wd;->g:LX/0we;

    iput-wide v2, v4, LX/0we;->b:D

    .line 160316
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->e:LX/0we;

    iput-wide v8, v2, LX/0we;->a:D

    .line 160317
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->e:LX/0we;

    iput-wide v6, v2, LX/0we;->b:D

    .line 160318
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->n:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 160319
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->n:D

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    div-double/2addr v2, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LX/0wd;->h(D)V

    .line 160320
    :cond_5
    invoke-virtual/range {p0 .. p0}, LX/0wd;->i()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0wd;->c:Z

    if-eqz v2, :cond_b

    invoke-direct/range {p0 .. p0}, LX/0wd;->l()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 160321
    :cond_6
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/0wd;->i:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/0wd;->h:D

    .line 160322
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->e:LX/0we;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/0wd;->i:D

    iput-wide v4, v2, LX/0we;->a:D

    .line 160323
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    .line 160324
    const/4 v2, 0x1

    .line 160325
    :goto_1
    const/4 v3, 0x0

    .line 160326
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0wd;->j:Z

    if-eqz v4, :cond_a

    .line 160327
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, LX/0wd;->j:Z

    .line 160328
    const/4 v3, 0x1

    move v4, v3

    .line 160329
    :goto_2
    const/4 v3, 0x0

    .line 160330
    if-eqz v2, :cond_7

    .line 160331
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/0wd;->j:Z

    .line 160332
    const/4 v2, 0x1

    move v3, v2

    .line 160333
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v2}, LX/0wb;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0xi;

    .line 160334
    if-eqz v4, :cond_9

    .line 160335
    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/0xi;->c(LX/0wd;)V

    .line 160336
    :cond_9
    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/0xi;->a(LX/0wd;)V

    .line 160337
    if-eqz v3, :cond_8

    .line 160338
    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/0xi;->b(LX/0wd;)V

    goto :goto_3

    :cond_a
    move v4, v3

    goto :goto_2

    :cond_b
    move v2, v10

    goto :goto_1
.end method

.method public final g(D)Z
    .locals 7

    .prologue
    .line 160339
    invoke-virtual {p0}, LX/0wd;->d()D

    move-result-wide v0

    sub-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 160340
    iget-wide v5, p0, LX/0wd;->l:D

    move-wide v2, v5

    .line 160341
    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 8

    .prologue
    .line 160342
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iget-wide v0, v0, LX/0we;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, LX/0wd;->k:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/0wd;->e:LX/0we;

    .line 160343
    iget-wide v4, p0, LX/0wd;->i:D

    iget-wide v6, v0, LX/0we;->a:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    move-wide v0, v4

    .line 160344
    iget-wide v2, p0, LX/0wd;->l:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()LX/0wd;
    .locals 4

    .prologue
    .line 160345
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    iget-wide v0, v0, LX/0we;->a:D

    iput-wide v0, p0, LX/0wd;->i:D

    .line 160346
    iget-object v0, p0, LX/0wd;->g:LX/0we;

    iget-object v1, p0, LX/0wd;->e:LX/0we;

    iget-wide v2, v1, LX/0we;->a:D

    iput-wide v2, v0, LX/0we;->a:D

    .line 160347
    iget-object v0, p0, LX/0wd;->e:LX/0we;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, LX/0we;->b:D

    .line 160348
    return-object p0
.end method

.method public final k()LX/0wd;
    .locals 1

    .prologue
    .line 160273
    iget-object v0, p0, LX/0wd;->m:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->b()V

    .line 160274
    return-object p0
.end method
