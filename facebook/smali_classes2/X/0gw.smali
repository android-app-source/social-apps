.class public LX/0gw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0gw;


# instance fields
.field private final a:Ljava/lang/Boolean;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 114232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114233
    iput-object p1, p0, LX/0gw;->a:Ljava/lang/Boolean;

    .line 114234
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0gw;->b:Z

    .line 114235
    return-void
.end method

.method public static a(LX/0QB;)LX/0gw;
    .locals 5

    .prologue
    .line 114219
    sget-object v0, LX/0gw;->c:LX/0gw;

    if-nez v0, :cond_1

    .line 114220
    const-class v1, LX/0gw;

    monitor-enter v1

    .line 114221
    :try_start_0
    sget-object v0, LX/0gw;->c:LX/0gw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 114222
    if-eqz v2, :cond_0

    .line 114223
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 114224
    new-instance p0, LX/0gw;

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-direct {p0, v3, v4}, LX/0gw;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 114225
    move-object v0, p0

    .line 114226
    sput-object v0, LX/0gw;->c:LX/0gw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114227
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 114228
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 114229
    :cond_1
    sget-object v0, LX/0gw;->c:LX/0gw;

    return-object v0

    .line 114230
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 114231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 114218
    iget-object v0, p0, LX/0gw;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 114214
    invoke-virtual {p0}, LX/0gw;->a()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 114217
    invoke-virtual {p0}, LX/0gw;->b()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 114216
    invoke-virtual {p0}, LX/0gw;->b()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 114215
    invoke-virtual {p0}, LX/0gw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0gw;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
