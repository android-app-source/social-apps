.class public interface abstract LX/0QB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QC;


# virtual methods
.method public abstract getApplicationInjector()LX/0QA;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getBinders()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getInjectorThreadStack()LX/0S7;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getProcessIdentifier()I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getScopeAwareInjector()LX/0R6;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getScopeUnawareInjector()LX/0QD;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
