.class public abstract LX/0Vx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2WA;

.field public b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 1
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75178
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 75179
    iput-object p1, p0, LX/0Vx;->a:LX/2WA;

    .line 75180
    return-void
.end method

.method private declared-synchronized e()V
    .locals 9

    .prologue
    .line 75216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Vx;->a:LX/2WA;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Vx;->a:LX/2WA;

    invoke-virtual {p0}, LX/0Vx;->a()Ljava/lang/String;

    move-result-object v1

    .line 75217
    iget-object v3, v0, LX/2WA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/2WA;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-object v5, v0, LX/2WA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0, v1}, LX/2WA;->b(LX/2WA;Ljava/lang/String;)LX/0Tn;

    move-result-object v6

    const-wide/16 v7, 0x0

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    sub-long v5, v3, v5

    iget-object v3, v0, LX/2WA;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v5, v3

    if-lez v3, :cond_1

    iget-object v3, v0, LX/2WA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0, v1}, LX/2WA;->c(LX/2WA;Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 75218
    if-eqz v0, :cond_0

    .line 75219
    iget-object v0, p0, LX/0Vx;->a:LX/2WA;

    invoke-virtual {p0}, LX/0Vx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/0Vx;->c()LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    .line 75220
    iget-object v3, v0, LX/2WA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-static {v0, v1}, LX/2WA;->c(LX/2WA;Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-static {v0, v1}, LX/2WA;->b(LX/2WA;Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    iget-object v5, v0, LX/2WA;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 75221
    invoke-virtual {p0}, LX/0Vx;->W_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75222
    :cond_0
    monitor-exit p0

    return-void

    .line 75223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized W_()V
    .locals 1

    .prologue
    .line 75213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75214
    monitor-exit p0

    return-void

    .line 75215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public declared-synchronized a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 75207
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0Vx;->c()LX/0lF;

    move-result-object v0

    .line 75208
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-lez v1, :cond_0

    .line 75209
    invoke-virtual {p0}, LX/0Vx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 75210
    :cond_0
    invoke-virtual {p0}, LX/0Vx;->W_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75211
    monitor-exit p0

    return-void

    .line 75212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75205
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75206
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 75195
    :cond_0
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75196
    if-nez v0, :cond_2

    .line 75197
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75198
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 75199
    :goto_0
    if-eqz v0, :cond_0

    .line 75200
    invoke-direct {p0}, LX/0Vx;->e()V

    .line 75201
    return-void

    .line 75202
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 75203
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 75204
    iget-object v2, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 75186
    :cond_0
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75187
    if-nez v0, :cond_2

    .line 75188
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75189
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 75190
    :goto_0
    if-eqz v0, :cond_0

    .line 75191
    invoke-direct {p0}, LX/0Vx;->e()V

    .line 75192
    return-void

    .line 75193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 75194
    :cond_2
    iget-object v1, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()LX/0lF;
    .locals 8

    .prologue
    .line 75181
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 75182
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 75183
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 75184
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    goto :goto_0

    .line 75185
    :cond_1
    return-object v2
.end method
