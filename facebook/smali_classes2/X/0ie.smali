.class public LX/0ie;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0ie;


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 121548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121549
    iput-object p1, p0, LX/0ie;->a:LX/0if;

    .line 121550
    return-void
.end method

.method public static a(LX/0QB;)LX/0ie;
    .locals 4

    .prologue
    .line 121535
    sget-object v0, LX/0ie;->b:LX/0ie;

    if-nez v0, :cond_1

    .line 121536
    const-class v1, LX/0ie;

    monitor-enter v1

    .line 121537
    :try_start_0
    sget-object v0, LX/0ie;->b:LX/0ie;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121538
    if-eqz v2, :cond_0

    .line 121539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121540
    new-instance p0, LX/0ie;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/0ie;-><init>(LX/0if;)V

    .line 121541
    move-object v0, p0

    .line 121542
    sput-object v0, LX/0ie;->b:LX/0ie;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121543
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121544
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121545
    :cond_1
    sget-object v0, LX/0ie;->b:LX/0ie;

    return-object v0

    .line 121546
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 121529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121530
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121531
    const-string v1, "\":\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121532
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121533
    iget-object v1, p0, LX/0ie;->a:LX/0if;

    sget-object v2, LX/0ig;->ar:LX/0ih;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 121534
    return-void
.end method


# virtual methods
.method public final a(LX/21C;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 8

    .prologue
    .line 121505
    iget-object v0, p0, LX/0ie;->a:LX/0if;

    sget-object v1, LX/0ig;->ar:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 121506
    const-string v0, "notification_source"

    invoke-virtual {p1}, LX/21C;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121507
    sget-object v0, LX/21C;->JEWEL:LX/21C;

    if-ne p1, v0, :cond_1

    .line 121508
    const-string v0, "notification_position"

    .line 121509
    iget v1, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    move v1, v1

    .line 121510
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121511
    const-string v0, "seen_state"

    .line 121512
    iget-object v1, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    move-object v1, v1

    .line 121513
    invoke-static {p0, v0, v1}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121514
    const-string v0, "logging_data"

    .line 121515
    iget-object v1, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    move-object v1, v1

    .line 121516
    invoke-static {p0, v0, v1}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121517
    :cond_0
    :goto_0
    return-void

    .line 121518
    :cond_1
    sget-object v0, LX/21C;->PUSH:LX/21C;

    if-ne p1, v0, :cond_0

    .line 121519
    const-string v2, "alert_id"

    .line 121520
    iget-wide v6, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    move-wide v4, v6

    .line 121521
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121522
    const-string v2, "content_id"

    .line 121523
    iget-object v3, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    move-object v3, v3

    .line 121524
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121525
    const-string v2, "notif_type"

    .line 121526
    iget-object v3, p2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a:Ljava/lang/String;

    move-object v3, v3

    .line 121527
    invoke-static {p0, v2, v3}, LX/0ie;->b(LX/0ie;Ljava/lang/String;Ljava/lang/String;)V

    .line 121528
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 121499
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "action_type"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "event_privacy_type"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 121500
    iget-object v1, p0, LX/0ie;->a:LX/0if;

    sget-object v2, LX/0ig;->ar:LX/0ih;

    sget-object v3, LX/8D1;->EVENT_RSVP:LX/8D1;

    invoke-virtual {v3}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121501
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 121502
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "action_type"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 121503
    iget-object v1, p0, LX/0ie;->a:LX/0if;

    sget-object v2, LX/0ig;->ar:LX/0ih;

    sget-object v3, LX/8D1;->FRIENDING:LX/8D1;

    invoke-virtual {v3}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121504
    return-void
.end method
