.class public LX/12e;
.super LX/0hi;
.source ""


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wO;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wP;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iD;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wa;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1wO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175663
    invoke-direct {p0}, LX/0hi;-><init>()V

    .line 175664
    iput-object p1, p0, LX/12e;->a:LX/0Ot;

    .line 175665
    iput-object p2, p0, LX/12e;->b:LX/0Ot;

    .line 175666
    iput-object p3, p0, LX/12e;->c:LX/0Ot;

    .line 175667
    iput-object p4, p0, LX/12e;->d:LX/0Ot;

    .line 175668
    iput-object p5, p0, LX/12e;->e:LX/0Ot;

    .line 175669
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 175656
    iput-object v0, p0, LX/12e;->a:LX/0Ot;

    .line 175657
    iput-object v0, p0, LX/12e;->b:LX/0Ot;

    .line 175658
    iput-object v0, p0, LX/12e;->c:LX/0Ot;

    .line 175659
    iput-object v0, p0, LX/12e;->d:LX/0Ot;

    .line 175660
    iput-object v0, p0, LX/12e;->e:LX/0Ot;

    .line 175661
    invoke-super {p0}, LX/0hi;->a()V

    .line 175662
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 11
    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 175541
    :try_start_0
    const-string v0, "FbMainTabFragmentAppEnteredLeftControllerCallbacksDispatcher.onUserEnteredApp"

    const v1, 0x55950208

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175542
    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wO;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 175543
    :try_start_1
    const-string v0, "FbMainTabFragmentVideoFetchController"

    const v1, -0x2890256f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175544
    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wO;

    invoke-virtual {v0}, LX/1wO;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175545
    const v0, -0x31023f95

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175546
    :cond_0
    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 175547
    :try_start_3
    const-string v0, "FbMainTabFragmentInterstitialStartHelperController"

    const v1, 0x5a3e6d7d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175548
    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wP;

    .line 175549
    iget-object v1, v0, LX/1wP;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15W;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v1, p1, v2}, LX/15W;->a(Landroid/app/Activity;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 175550
    const v0, -0x2a49aceb

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175551
    :cond_1
    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 175552
    :try_start_5
    const-string v0, "FbMainTabFragmentJewelController"

    const v1, -0x132f1ed7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175553
    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    .line 175554
    iget-object v2, v0, LX/0iD;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0xC;

    invoke-virtual {v2}, LX/0xC;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 175555
    iget-object v2, v0, LX/0iD;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iE;

    invoke-interface {v2}, LX/0iE;->a()V

    .line 175556
    iget-object v2, v0, LX/0iD;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iE;

    invoke-interface {v2}, LX/0iE;->d()V

    .line 175557
    iget-object v2, v0, LX/0iD;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2t4;

    .line 175558
    iget-object v3, v2, LX/2t4;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2t4;->b:LX/0Tn;

    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 175559
    iget-object v5, v2, LX/2t4;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 175560
    const-wide/32 v7, 0x36ee80

    sub-long v7, v5, v7

    cmp-long v3, v7, v3

    if-gtz v3, :cond_8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 175561
    :cond_2
    :goto_0
    const v0, 0x16c48920

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175562
    :cond_3
    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wa;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v0

    if-eqz v0, :cond_5

    .line 175563
    :try_start_7
    const-string v0, "FbMainTabFragmentSelfUpdateController"

    const v1, -0x165fec7b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175564
    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wa;

    .line 175565
    iget-object v2, v0, LX/1wa;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1sf;

    invoke-virtual {v2}, LX/1sf;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 175566
    iget-object v2, v0, LX/1wa;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjp;

    const-wide/16 v6, -0x1

    const/4 v10, 0x0

    .line 175567
    iget-boolean v4, v2, LX/Fjp;->q:Z

    if-nez v4, :cond_9

    .line 175568
    :cond_4
    :goto_1
    iget-object v2, v0, LX/1wa;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1wb;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LX/1wb;->a(Landroid/app/Activity;Z)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 175569
    const v0, -0x7c22236c

    :try_start_8
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175570
    :cond_5
    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wv;

    invoke-virtual {v0}, LX/1wv;->a()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-eqz v0, :cond_7

    .line 175571
    :try_start_9
    const-string v0, "FbMainTabFragmentSurveyController"

    const v1, -0x7088bc41

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175572
    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wv;

    .line 175573
    instance-of v1, p1, LX/0f7;

    if-nez v1, :cond_e
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 175574
    :cond_6
    :goto_2
    const v0, 0x361e63e8

    :try_start_a
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 175575
    :cond_7
    const v0, -0xf2b3bc3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 175576
    return-void

    .line 175577
    :catchall_0
    move-exception v0

    const v1, 0x65adf92b

    :try_start_b
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 175578
    :catchall_1
    move-exception v0

    const v1, -0x4e6a8b14

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175579
    :catchall_2
    move-exception v0

    const v1, 0x3f9a2ef0

    :try_start_c
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175580
    :catchall_3
    move-exception v0

    const v1, -0x449987a3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175581
    :catchall_4
    move-exception v0

    const v1, -0x2457d1d5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175582
    :catchall_5
    move-exception v0

    const v1, -0x54cb6793

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 175583
    :cond_8
    :try_start_d
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "marketplace_impression"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 175584
    const-string v4, "previousActionsCount"

    iget-object v7, v2, LX/2t4;->f:LX/0xB;

    sget-object v8, LX/12j;->MARKETPLACE:LX/12j;

    invoke-virtual {v7, v8}, LX/0xB;->a(LX/12j;)I

    move-result v7

    invoke-virtual {v3, v4, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 175585
    const-string v4, "uiComponent"

    const-string v7, "app_tab"

    invoke-virtual {v3, v4, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 175586
    const-string v4, "surface"

    const-string v7, "app_tab"

    invoke-virtual {v3, v4, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 175587
    const-string v4, "marketplace"

    .line 175588
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 175589
    iget-object v4, v2, LX/2t4;->c:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 175590
    invoke-virtual {v3}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 175591
    iget-object v3, v2, LX/2t4;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2t4;->b:LX/0Tn;

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto/16 :goto_0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 175592
    :cond_9
    :try_start_e
    iget-object v4, v2, LX/Fjp;->d:LX/0YO;

    invoke-virtual {v4}, LX/0Uq;->b()V

    .line 175593
    iget-object v4, v2, LX/Fjp;->c:LX/0cZ;

    invoke-virtual {v4}, LX/0Uq;->b()V

    .line 175594
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->n:LX/0Tn;

    invoke-interface {v4, v5, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 175595
    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    .line 175596
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->d:LX/0Tn;

    invoke-interface {v4, v5, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 175597
    iget-object v5, v2, LX/Fjp;->g:LX/0WV;

    invoke-virtual {v5}, LX/0WV;->b()I

    move-result v5

    if-lt v5, v4, :cond_b

    .line 175598
    iget-object v5, v2, LX/Fjp;->r:LX/0lC;

    invoke-virtual {v5}, LX/0lC;->e()LX/0m9;

    move-result-object v5

    .line 175599
    const-string v6, "current_version"

    iget-object v7, v2, LX/Fjp;->g:LX/0WV;

    invoke-virtual {v7}, LX/0WV;->b()I

    move-result v7

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 175600
    const-string v6, "downloaded_version"

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 175601
    iget-object v4, v2, LX/Fjp;->j:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Fjr;

    const-string v6, "scheduleservice_newer_version_running"

    invoke-virtual {v4, v6, v5}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V

    .line 175602
    :cond_a
    :goto_3
    iget-object v4, v2, LX/Fjp;->i:LX/EmQ;

    invoke-virtual {v4}, LX/EmQ;->a()V

    .line 175603
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->b:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 175604
    iget-object v6, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/Fjg;->c:LX/0Tn;

    const-wide/32 v8, 0x2932e00

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 175605
    iget-object v8, v2, LX/Fjp;->e:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 175606
    sub-long v4, v8, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    .line 175607
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/Fjg;->b:LX/0Tn;

    invoke-interface {v4, v5, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 175608
    invoke-virtual {v2, v10}, LX/Fjp;->a(Z)V

    goto/16 :goto_1

    .line 175609
    :cond_b
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->r:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 175610
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 175611
    const-string v4, "emptyOrNull"

    move-object v5, v4

    .line 175612
    :goto_4
    iget-object v4, v2, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Fjj;

    const-string v6, "selfupdate_start_showing_activity"

    const-string v7, "source"

    invoke-static {v7, v5}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 175613
    iget-object v4, v2, LX/Fjp;->j:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Fjr;

    invoke-virtual {v4}, LX/Fjr;->a()V

    goto :goto_3

    .line 175614
    :cond_c
    const/4 v5, 0x1

    if-ne v4, v5, :cond_a

    .line 175615
    iget-object v4, v2, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->g:LX/0Tn;

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 175616
    cmp-long v6, v4, v6

    if-eqz v6, :cond_a

    .line 175617
    iget-object v6, v2, LX/Fjp;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/selfupdate/SelfUpdateManager$1;

    invoke-direct {v7, v2, v4, v5}, Lcom/facebook/selfupdate/SelfUpdateManager$1;-><init>(LX/Fjp;J)V

    const v4, 0x4f14a94f

    invoke-static {v6, v7, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_3

    :cond_d
    move-object v5, v4

    goto :goto_4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :cond_e
    move-object v1, p1

    .line 175618
    check-cast v1, LX/0f7;

    .line 175619
    invoke-interface {v1}, LX/0f7;->q()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 175620
    iget-object v1, v0, LX/1wv;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gt;

    .line 175621
    const-string v2, "1565141090400626"

    .line 175622
    iput-object v2, v1, LX/0gt;->a:Ljava/lang/String;

    .line 175623
    invoke-virtual {v1, p1}, LX/0gt;->a(Landroid/content/Context;)V

    goto/16 :goto_2
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 175624
    :try_start_0
    const-string v0, "FbMainTabFragmentAppEnteredLeftControllerCallbacksDispatcher.onUserLeftApp"

    const v1, 0x6dab75c1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175625
    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wO;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 175626
    :try_start_1
    const-string v0, "FbMainTabFragmentVideoFetchController"

    const v1, -0x5ab6a121

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175627
    iget-object v0, p0, LX/12e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wO;

    .line 175628
    iget-object v1, v0, LX/1wO;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xX;

    sget-object p1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v1, p1}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 175629
    iget-object v1, v0, LX/1wO;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hn;

    invoke-virtual {v1}, LX/0hn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175630
    :cond_0
    const v0, -0x18c726c3

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175631
    :cond_1
    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 175632
    :try_start_3
    const-string v0, "FbMainTabFragmentInterstitialStartHelperController"

    const v1, 0x62f1c3d5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175633
    iget-object v0, p0, LX/12e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 175634
    const v0, 0x415f6718

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175635
    :cond_2
    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 175636
    :try_start_5
    const-string v0, "FbMainTabFragmentJewelController"

    const v1, 0x134109bb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175637
    iget-object v0, p0, LX/12e;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    .line 175638
    iget-object v1, v0, LX/0iD;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iE;

    invoke-interface {v1}, LX/0iE;->e()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 175639
    const v0, -0x32b651e3

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175640
    :cond_3
    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wa;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 175641
    :try_start_7
    const-string v0, "FbMainTabFragmentSelfUpdateController"

    const v1, 0x554762c9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175642
    iget-object v0, p0, LX/12e;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 175643
    const v0, 0x8ed097e

    :try_start_8
    invoke-static {v0}, LX/02m;->a(I)V

    .line 175644
    :cond_4
    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wv;

    invoke-virtual {v0}, LX/1wv;->a()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-eqz v0, :cond_5

    .line 175645
    :try_start_9
    const-string v0, "FbMainTabFragmentSurveyController"

    const v1, -0x209c38b0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175646
    iget-object v0, p0, LX/12e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 175647
    const v0, -0x7ffb8c55

    :try_start_a
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 175648
    :cond_5
    const v0, -0x50783780

    invoke-static {v0}, LX/02m;->a(I)V

    .line 175649
    return-void

    .line 175650
    :catchall_0
    move-exception v0

    const v1, -0xf6afa7f

    :try_start_b
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 175651
    :catchall_1
    move-exception v0

    const v1, 0x371a6d4c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175652
    :catchall_2
    move-exception v0

    const v1, 0x64cd3959

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175653
    :catchall_3
    move-exception v0

    const v1, 0x79f09008

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175654
    :catchall_4
    move-exception v0

    const v1, -0xda6849c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 175655
    :catchall_5
    move-exception v0

    const v1, 0x14cbeac5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
