.class public final enum LX/0wr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wr;

.field public static final enum HTTP:LX/0wr;

.field public static final enum HTTP_1RT_INTERCEPTING:LX/0wr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 160990
    new-instance v0, LX/0wr;

    const-string v1, "HTTP"

    invoke-direct {v0, v1, v2}, LX/0wr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0wr;->HTTP:LX/0wr;

    .line 160991
    new-instance v0, LX/0wr;

    const-string v1, "HTTP_1RT_INTERCEPTING"

    invoke-direct {v0, v1, v3}, LX/0wr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0wr;->HTTP_1RT_INTERCEPTING:LX/0wr;

    .line 160992
    const/4 v0, 0x2

    new-array v0, v0, [LX/0wr;

    sget-object v1, LX/0wr;->HTTP:LX/0wr;

    aput-object v1, v0, v2

    sget-object v1, LX/0wr;->HTTP_1RT_INTERCEPTING:LX/0wr;

    aput-object v1, v0, v3

    sput-object v0, LX/0wr;->$VALUES:[LX/0wr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 160984
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/0wr;
    .locals 5

    .prologue
    .line 160985
    invoke-static {}, LX/0wr;->values()[LX/0wr;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 160986
    invoke-virtual {v0}, LX/0wr;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 160987
    :goto_1
    return-object v0

    .line 160988
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160989
    :cond_1
    sget-object v0, LX/0wr;->HTTP:LX/0wr;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wr;
    .locals 1

    .prologue
    .line 160983
    const-class v0, LX/0wr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wr;

    return-object v0
.end method

.method public static values()[LX/0wr;
    .locals 1

    .prologue
    .line 160982
    sget-object v0, LX/0wr;->$VALUES:[LX/0wr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wr;

    return-object v0
.end method
