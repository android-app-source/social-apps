.class public LX/1TF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 249951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249952
    iput-object p1, p0, LX/1TF;->a:LX/0Ot;

    .line 249953
    return-void
.end method

.method public static a(LX/0QB;)LX/1TF;
    .locals 4

    .prologue
    .line 249935
    const-class v1, LX/1TF;

    monitor-enter v1

    .line 249936
    :try_start_0
    sget-object v0, LX/1TF;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 249937
    sput-object v2, LX/1TF;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249938
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249939
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 249940
    new-instance v3, LX/1TF;

    const/16 p0, 0x896

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1TF;-><init>(LX/0Ot;)V

    .line 249941
    move-object v0, v3

    .line 249942
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 249943
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249944
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 249945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 249954
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 249948
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 249949
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 249950
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 249946
    const-class v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    iget-object v1, p0, LX/1TF;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 249947
    return-void
.end method
