.class public abstract LX/121;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0yY;",
            "LX/3J6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 173834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173835
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/121;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(LX/121;LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/39A;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/google/common/util/concurrent/ListenableFuture;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173822
    new-instance v0, LX/3J6;

    invoke-direct {v0, p0}, LX/3J6;-><init>(LX/121;)V

    .line 173823
    iput-object p1, v0, LX/3J6;->a:LX/0yY;

    .line 173824
    iput-object p2, v0, LX/3J6;->b:Ljava/lang/String;

    .line 173825
    iput-object p3, v0, LX/3J6;->c:Ljava/lang/String;

    .line 173826
    iput-object p4, v0, LX/3J6;->d:LX/39A;

    .line 173827
    iput-object p6, v0, LX/3J6;->e:Ljava/lang/String;

    .line 173828
    iput-object p7, v0, LX/3J6;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 173829
    iput-object p8, v0, LX/3J6;->g:LX/0TF;

    .line 173830
    iput-object p5, v0, LX/3J6;->h:Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    .line 173831
    iput-object p9, v0, LX/3J6;->j:LX/2nT;

    .line 173832
    iget-object v1, p0, LX/121;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173833
    return-object p0
.end method

.method public static a(LX/0gc;LX/0yY;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 173820
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 173821
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;
    .locals 11

    .prologue
    .line 173817
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 173818
    invoke-virtual {p0}, LX/121;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v10, LX/2nT;->DEFAULT:LX/2nT;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v0

    move-object v8, v7

    move-object v9, v7

    invoke-static/range {v1 .. v10}, LX/121;->a(LX/121;LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;

    move-result-object v1

    move-object v0, v1

    .line 173819
    return-object v0
.end method

.method public final a(LX/0yY;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 173816
    invoke-virtual {p0}, LX/121;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-static/range {v0 .. v9}, LX/121;->a(LX/121;LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/2nT;)LX/121;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 173815
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, p4

    invoke-static/range {v0 .. v9}, LX/121;->a(LX/121;LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 173814
    sget-object v9, LX/2nT;->DEFAULT:LX/2nT;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-static/range {v0 .. v9}, LX/121;->a(LX/121;LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;LX/2nT;)LX/121;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;
    .locals 3
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 173789
    iget-object v0, p0, LX/121;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3J6;

    .line 173790
    if-nez v0, :cond_0

    move-object v0, v1

    .line 173791
    :goto_0
    return-object v0

    .line 173792
    :cond_0
    iput-object p2, v0, LX/3J6;->i:LX/0gc;

    .line 173793
    iget-object v2, v0, LX/3J6;->j:LX/2nT;

    invoke-virtual {p0, v2, p1}, LX/121;->a(LX/2nT;LX/0yY;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173794
    invoke-virtual {p0}, LX/121;->a()V

    .line 173795
    invoke-static {p2, p1}, LX/121;->a(LX/0gc;LX/0yY;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 173796
    invoke-virtual {p0, v0, p3, p1}, LX/121;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 173797
    invoke-virtual {p2}, LX/0gc;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173798
    iget-object v1, p1, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 173799
    :cond_1
    const-string v0, "ZeroDialogController"

    const-string v2, "Attempting to show fragment after onSaveInstanceState() has been called"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 173800
    goto :goto_0

    .line 173801
    :cond_2
    iget-object v0, v0, LX/3J6;->d:LX/39A;

    .line 173802
    if-nez v0, :cond_3

    move-object v0, v1

    .line 173803
    goto :goto_0

    .line 173804
    :cond_3
    invoke-interface {v0, p3}, LX/39A;->a(Ljava/lang/Object;)V

    :cond_4
    move-object v0, v1

    .line 173805
    goto :goto_0
.end method

.method public abstract a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a()V
.end method

.method public final a(LX/0yY;LX/0gc;)V
    .locals 1

    .prologue
    .line 173812
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 173813
    return-void
.end method

.method public final a(LX/0yY;LX/39A;)V
    .locals 1

    .prologue
    .line 173807
    if-eqz p2, :cond_0

    .line 173808
    iget-object v0, p0, LX/121;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3J6;

    .line 173809
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/3J6;->d:LX/39A;

    if-ne v0, p2, :cond_0

    .line 173810
    iget-object v0, p0, LX/121;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173811
    :cond_0
    return-void
.end method

.method public final a(LX/0yY;)Z
    .locals 1

    .prologue
    .line 173806
    sget-object v0, LX/2nT;->DEFAULT:LX/2nT;

    invoke-virtual {p0, v0, p1}, LX/121;->a(LX/2nT;LX/0yY;)Z

    move-result v0

    return v0
.end method

.method public abstract a(LX/2nT;LX/0yY;)Z
.end method

.method public abstract b()Ljava/lang/String;
.end method
