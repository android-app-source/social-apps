.class public LX/0Se;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Se;


# instance fields
.field public a:LX/0Sf;


# direct methods
.method public constructor <init>(LX/0Sf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 61535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61536
    iput-object p1, p0, LX/0Se;->a:LX/0Sf;

    .line 61537
    return-void
.end method

.method public static a(LX/0QB;)LX/0Se;
    .locals 4

    .prologue
    .line 61538
    sget-object v0, LX/0Se;->b:LX/0Se;

    if-nez v0, :cond_1

    .line 61539
    const-class v1, LX/0Se;

    monitor-enter v1

    .line 61540
    :try_start_0
    sget-object v0, LX/0Se;->b:LX/0Se;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 61541
    if-eqz v2, :cond_0

    .line 61542
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 61543
    new-instance p0, LX/0Se;

    invoke-static {v0}, LX/0Sf;->b(LX/0QB;)LX/0Sf;

    move-result-object v3

    check-cast v3, LX/0Sf;

    invoke-direct {p0, v3}, LX/0Se;-><init>(LX/0Sf;)V

    .line 61544
    move-object v0, p0

    .line 61545
    sput-object v0, LX/0Se;->b:LX/0Se;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 61547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 61548
    :cond_1
    sget-object v0, LX/0Se;->b:LX/0Se;

    return-object v0

    .line 61549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 61550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
