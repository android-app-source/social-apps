.class public LX/1VB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:LX/1Uc;

.field public static final b:LX/1Ub;


# instance fields
.field private c:Landroid/content/res/Resources;

.field private d:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 257449
    new-instance v0, LX/1Ue;

    invoke-direct {v0, v2, v2}, LX/1Ue;-><init>(FF)V

    sput-object v0, LX/1VB;->a:LX/1Uc;

    .line 257450
    new-instance v0, LX/1Ub;

    sget-object v1, LX/1VB;->a:LX/1Uc;

    invoke-direct {v0, v2, v2, v2, v1}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    sput-object v0, LX/1VB;->b:LX/1Ub;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 257405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257406
    iput-object p1, p0, LX/1VB;->c:Landroid/content/res/Resources;

    .line 257407
    return-void
.end method


# virtual methods
.method public final e()F
    .locals 3

    .prologue
    .line 257448
    iget-object v0, p0, LX/1VB;->c:Landroid/content/res/Resources;

    iget-object v1, p0, LX/1VB;->c:Landroid/content/res/Resources;

    const v2, 0x7f0b00d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final declared-synchronized h()Ljava/util/EnumMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257408
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    if-nez v0, :cond_0

    .line 257409
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/1UZ;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    .line 257410
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->LEGACY_DEFAULT:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, LX/1Ue;

    const/high16 v7, 0x41400000    # 12.0f

    const/high16 v8, 0x41400000    # 12.0f

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257411
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->STORY_EDGE:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, LX/1Ue;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257412
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257413
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257414
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257415
    const/high16 v4, 0x40c00000    # 6.0f

    move v4, v4

    .line 257416
    const/4 v5, 0x0

    new-instance v6, LX/1Ue;

    const/high16 v7, 0x41400000    # 12.0f

    const/high16 v8, 0x41400000    # 12.0f

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257417
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->DEFAULT_TEXT:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257418
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257419
    const/high16 v4, 0x40000000    # 2.0f

    move v4, v4

    .line 257420
    const/4 v5, 0x0

    new-instance v6, LX/1Ue;

    const/high16 v7, 0x41400000    # 12.0f

    const/high16 v8, 0x41400000    # 12.0f

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257421
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257422
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257423
    const/high16 v4, 0x40c00000    # 6.0f

    move v4, v4

    .line 257424
    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257425
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->DEFAULT_HEADER:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257426
    const/high16 v3, 0x40000000    # 2.0f

    move v3, v3

    .line 257427
    const/high16 v4, 0x40000000    # 2.0f

    move v4, v4

    .line 257428
    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257429
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->DEFAULT_TEXT_HEADER:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/4 v3, 0x0

    .line 257430
    const/high16 v4, 0x40000000    # 2.0f

    move v4, v4

    .line 257431
    const/high16 v5, -0x3ec00000    # -12.0f

    new-instance v6, LX/1Ue;

    const/high16 v7, 0x41400000    # 12.0f

    const/high16 v8, 0x41400000    # 12.0f

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257432
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->SUBSTORY_HEADER:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257433
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257434
    const/high16 v4, 0x40000000    # 2.0f

    move v4, v4

    .line 257435
    const/4 v5, 0x0

    new-instance v6, LX/1Ue;

    const/high16 v7, 0x41400000    # 12.0f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257436
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->ATTACHMENT_TOP:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257437
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257438
    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257439
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->ATTACHMENT_BOTTOM:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/4 v3, 0x0

    .line 257440
    const/high16 v4, 0x40c00000    # 6.0f

    move v4, v4

    .line 257441
    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257442
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;

    sget-object v1, LX/1UZ;->SHARED_ATTACHMENT:LX/1UZ;

    new-instance v2, LX/1Ub;

    .line 257443
    const/high16 v3, 0x40c00000    # 6.0f

    move v3, v3

    .line 257444
    const/high16 v4, 0x40c00000    # 6.0f

    move v4, v4

    .line 257445
    const/4 v5, 0x0

    sget-object v6, LX/1VB;->a:LX/1Uc;

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257446
    :cond_0
    iget-object v0, p0, LX/1VB;->d:Ljava/util/EnumMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 257447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
