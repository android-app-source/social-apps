.class public final LX/0ln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _name:Ljava/lang/String;

.field public final transient a:Z

.field public final transient b:C

.field public final transient c:I

.field private final transient d:[I

.field private final transient e:[C

.field private final transient f:[B


# direct methods
.method public constructor <init>(LX/0ln;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 130758
    iget-boolean v3, p1, LX/0ln;->a:Z

    iget-char v4, p1, LX/0ln;->b:C

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/0ln;-><init>(LX/0ln;Ljava/lang/String;ZCI)V

    .line 130759
    return-void
.end method

.method public constructor <init>(LX/0ln;Ljava/lang/String;ZCI)V
    .locals 4

    .prologue
    const/16 v1, 0x40

    const/4 v3, 0x0

    .line 130743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130744
    const/16 v0, 0x80

    new-array v0, v0, [I

    iput-object v0, p0, LX/0ln;->d:[I

    .line 130745
    new-array v0, v1, [C

    iput-object v0, p0, LX/0ln;->e:[C

    .line 130746
    new-array v0, v1, [B

    iput-object v0, p0, LX/0ln;->f:[B

    .line 130747
    iput-object p2, p0, LX/0ln;->_name:Ljava/lang/String;

    .line 130748
    iget-object v0, p1, LX/0ln;->f:[B

    .line 130749
    iget-object v1, p0, LX/0ln;->f:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130750
    iget-object v0, p1, LX/0ln;->e:[C

    .line 130751
    iget-object v1, p0, LX/0ln;->e:[C

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130752
    iget-object v0, p1, LX/0ln;->d:[I

    .line 130753
    iget-object v1, p0, LX/0ln;->d:[I

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130754
    iput-boolean p3, p0, LX/0ln;->a:Z

    .line 130755
    iput-char p4, p0, LX/0ln;->b:C

    .line 130756
    iput p5, p0, LX/0ln;->c:I

    .line 130757
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZCI)V
    .locals 5

    .prologue
    const/16 v2, 0x40

    const/4 v0, 0x0

    .line 130722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130723
    const/16 v1, 0x80

    new-array v1, v1, [I

    iput-object v1, p0, LX/0ln;->d:[I

    .line 130724
    new-array v1, v2, [C

    iput-object v1, p0, LX/0ln;->e:[C

    .line 130725
    new-array v1, v2, [B

    iput-object v1, p0, LX/0ln;->f:[B

    .line 130726
    iput-object p1, p0, LX/0ln;->_name:Ljava/lang/String;

    .line 130727
    iput-boolean p3, p0, LX/0ln;->a:Z

    .line 130728
    iput-char p4, p0, LX/0ln;->b:C

    .line 130729
    iput p5, p0, LX/0ln;->c:I

    .line 130730
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 130731
    if-eq v1, v2, :cond_0

    .line 130732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Base64Alphabet length must be exactly 64 (was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130733
    :cond_0
    iget-object v2, p0, LX/0ln;->e:[C

    invoke-virtual {p2, v0, v1, v2, v0}, Ljava/lang/String;->getChars(II[CI)V

    .line 130734
    iget-object v2, p0, LX/0ln;->d:[I

    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 130735
    :goto_0
    if-ge v0, v1, :cond_1

    .line 130736
    iget-object v2, p0, LX/0ln;->e:[C

    aget-char v2, v2, v0

    .line 130737
    iget-object v3, p0, LX/0ln;->f:[B

    int-to-byte v4, v2

    aput-byte v4, v3, v0

    .line 130738
    iget-object v3, p0, LX/0ln;->d:[I

    aput v0, v3, v2

    .line 130739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130740
    :cond_1
    if-eqz p3, :cond_2

    .line 130741
    iget-object v0, p0, LX/0ln;->d:[I

    const/4 v1, -0x2

    aput v1, v0, p4

    .line 130742
    :cond_2
    return-void
.end method

.method private a(CILjava/lang/String;)V
    .locals 2

    .prologue
    .line 130710
    const/16 v0, 0x20

    if-gt p1, v0, :cond_1

    .line 130711
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal white space character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: can only used between units"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130712
    :goto_0
    if-eqz p3, :cond_0

    .line 130713
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130714
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130715
    :cond_1
    invoke-virtual {p0, p1}, LX/0ln;->a(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130716
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected padding character (\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130717
    iget-char v1, p0, LX/0ln;->b:C

    move v1, v1

    .line 130718
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: padding only legal as 3rd or 4th character"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 130719
    :cond_2
    invoke-static {p1}, Ljava/lang/Character;->isDefined(C)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130720
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 130721
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (code 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;I)V
    .locals 2

    .prologue
    .line 130705
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0x12

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130706
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0xc

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130707
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130708
    iget-object v0, p0, LX/0ln;->e:[C

    and-int/lit8 v1, p2, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130709
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;II)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 130696
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0x12

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130697
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0xc

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130698
    iget-boolean v0, p0, LX/0ln;->a:Z

    if-eqz v0, :cond_2

    .line 130699
    if-ne p3, v2, :cond_1

    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130700
    iget-char v0, p0, LX/0ln;->b:C

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130701
    :cond_0
    :goto_1
    return-void

    .line 130702
    :cond_1
    iget-char v0, p0, LX/0ln;->b:C

    goto :goto_0

    .line 130703
    :cond_2
    if-ne p3, v2, :cond_0

    .line 130704
    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v1, p2, 0x6

    and-int/lit8 v1, v1, 0x3f

    aget-char v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 130606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected end-of-String in base64 content"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(II[BI)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 130686
    add-int/lit8 v0, p4, 0x1

    iget-object v1, p0, LX/0ln;->f:[B

    shr-int/lit8 v2, p1, 0x12

    and-int/lit8 v2, v2, 0x3f

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 130687
    add-int/lit8 v2, v0, 0x1

    iget-object v1, p0, LX/0ln;->f:[B

    shr-int/lit8 v3, p1, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v1, v1, v3

    aput-byte v1, p3, v0

    .line 130688
    iget-boolean v0, p0, LX/0ln;->a:Z

    if-eqz v0, :cond_1

    .line 130689
    iget-char v0, p0, LX/0ln;->b:C

    int-to-byte v1, v0

    .line 130690
    add-int/lit8 v3, v2, 0x1

    if-ne p2, v4, :cond_0

    iget-object v0, p0, LX/0ln;->f:[B

    shr-int/lit8 v4, p1, 0x6

    and-int/lit8 v4, v4, 0x3f

    aget-byte v0, v0, v4

    :goto_0
    aput-byte v0, p3, v2

    .line 130691
    add-int/lit8 v0, v3, 0x1

    aput-byte v1, p3, v3

    .line 130692
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 130693
    goto :goto_0

    .line 130694
    :cond_1
    if-ne p2, v4, :cond_2

    .line 130695
    add-int/lit8 v0, v2, 0x1

    iget-object v1, p0, LX/0ln;->f:[B

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v1, v1, v3

    aput-byte v1, p3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final a(II[CI)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 130677
    add-int/lit8 v0, p4, 0x1

    iget-object v1, p0, LX/0ln;->e:[C

    shr-int/lit8 v2, p1, 0x12

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    aput-char v1, p3, p4

    .line 130678
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, LX/0ln;->e:[C

    shr-int/lit8 v3, p1, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-char v2, v2, v3

    aput-char v2, p3, v0

    .line 130679
    iget-boolean v0, p0, LX/0ln;->a:Z

    if-eqz v0, :cond_1

    .line 130680
    add-int/lit8 v2, v1, 0x1

    if-ne p2, v4, :cond_0

    iget-object v0, p0, LX/0ln;->e:[C

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-char v0, v0, v3

    :goto_0
    aput-char v0, p3, v1

    .line 130681
    add-int/lit8 v0, v2, 0x1

    iget-char v1, p0, LX/0ln;->b:C

    aput-char v1, p3, v2

    .line 130682
    :goto_1
    return v0

    .line 130683
    :cond_0
    iget-char v0, p0, LX/0ln;->b:C

    goto :goto_0

    .line 130684
    :cond_1
    if-ne p2, v4, :cond_2

    .line 130685
    add-int/lit8 v0, v1, 0x1

    iget-object v2, p0, LX/0ln;->e:[C

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-char v2, v2, v3

    aput-char v2, p3, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(I[BI)I
    .locals 4

    .prologue
    .line 130672
    add-int/lit8 v0, p3, 0x1

    iget-object v1, p0, LX/0ln;->f:[B

    shr-int/lit8 v2, p1, 0x12

    and-int/lit8 v2, v2, 0x3f

    aget-byte v1, v1, v2

    aput-byte v1, p2, p3

    .line 130673
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, LX/0ln;->f:[B

    shr-int/lit8 v3, p1, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p2, v0

    .line 130674
    add-int/lit8 v0, v1, 0x1

    iget-object v2, p0, LX/0ln;->f:[B

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p2, v1

    .line 130675
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, LX/0ln;->f:[B

    and-int/lit8 v3, p1, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p2, v0

    .line 130676
    return v1
.end method

.method public final a(I[CI)I
    .locals 4

    .prologue
    .line 130667
    add-int/lit8 v0, p3, 0x1

    iget-object v1, p0, LX/0ln;->e:[C

    shr-int/lit8 v2, p1, 0x12

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    aput-char v1, p2, p3

    .line 130668
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, LX/0ln;->e:[C

    shr-int/lit8 v3, p1, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-char v2, v2, v3

    aput-char v2, p2, v0

    .line 130669
    add-int/lit8 v0, v1, 0x1

    iget-object v2, p0, LX/0ln;->e:[C

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-char v2, v2, v3

    aput-char v2, p2, v1

    .line 130670
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, LX/0ln;->e:[C

    and-int/lit8 v3, p1, 0x3f

    aget-char v2, v2, v3

    aput-char v2, p2, v0

    .line 130671
    return v1
.end method

.method public final a([BZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x22

    .line 130577
    array-length v3, p1

    .line 130578
    shr-int/lit8 v0, v3, 0x2

    add-int/2addr v0, v3

    shr-int/lit8 v1, v3, 0x3

    add-int/2addr v0, v1

    .line 130579
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 130580
    if-eqz p2, :cond_0

    .line 130581
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130582
    :cond_0
    iget v0, p0, LX/0ln;->c:I

    move v0, v0

    .line 130583
    shr-int/lit8 v1, v0, 0x2

    .line 130584
    const/4 v0, 0x0

    .line 130585
    add-int/lit8 v5, v3, -0x3

    move v2, v1

    .line 130586
    :goto_0
    if-gt v0, v5, :cond_2

    .line 130587
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x8

    .line 130588
    add-int/lit8 v6, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 130589
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v6, 0x1

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v0, v6

    .line 130590
    invoke-direct {p0, v4, v0}, LX/0ln;->a(Ljava/lang/StringBuilder;I)V

    .line 130591
    add-int/lit8 v0, v2, -0x1

    if-gtz v0, :cond_1

    .line 130592
    const/16 v0, 0x5c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130593
    const/16 v0, 0x6e

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130594
    iget v0, p0, LX/0ln;->c:I

    move v0, v0

    .line 130595
    shr-int/lit8 v0, v0, 0x2

    :cond_1
    move v2, v0

    move v0, v1

    .line 130596
    goto :goto_0

    .line 130597
    :cond_2
    sub-int v1, v3, v0

    .line 130598
    if-lez v1, :cond_4

    .line 130599
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x10

    .line 130600
    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    .line 130601
    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    .line 130602
    :cond_3
    invoke-direct {p0, v4, v0, v1}, LX/0ln;->a(Ljava/lang/StringBuilder;II)V

    .line 130603
    :cond_4
    if-eqz p2, :cond_5

    .line 130604
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130605
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/2SG;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x0

    const/4 v8, -0x2

    const/4 v7, 0x0

    .line 130607
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    move v0, v2

    .line 130608
    :goto_0
    if-ge v0, v3, :cond_3

    .line 130609
    :goto_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 130610
    if-ge v1, v3, :cond_3

    .line 130611
    const/16 v4, 0x20

    if-le v0, v4, :cond_e

    .line 130612
    invoke-virtual {p0, v0}, LX/0ln;->b(C)I

    move-result v4

    .line 130613
    if-gez v4, :cond_0

    .line 130614
    invoke-direct {p0, v0, v2, v7}, LX/0ln;->a(CILjava/lang/String;)V

    .line 130615
    :cond_0
    if-lt v1, v3, :cond_1

    .line 130616
    invoke-static {}, LX/0ln;->d()V

    .line 130617
    :cond_1
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 130618
    invoke-virtual {p0, v1}, LX/0ln;->b(C)I

    move-result v5

    .line 130619
    if-gez v5, :cond_2

    .line 130620
    const/4 v6, 0x1

    invoke-direct {p0, v1, v6, v7}, LX/0ln;->a(CILjava/lang/String;)V

    .line 130621
    :cond_2
    shl-int/lit8 v1, v4, 0x6

    or-int/2addr v1, v5

    .line 130622
    if-lt v0, v3, :cond_5

    .line 130623
    iget-boolean v4, p0, LX/0ln;->a:Z

    move v4, v4

    .line 130624
    if-nez v4, :cond_4

    .line 130625
    shr-int/lit8 v0, v1, 0x4

    .line 130626
    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 130627
    :cond_3
    :goto_2
    return-void

    .line 130628
    :cond_4
    invoke-static {}, LX/0ln;->d()V

    .line 130629
    :cond_5
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 130630
    invoke-virtual {p0, v0}, LX/0ln;->b(C)I

    move-result v5

    .line 130631
    if-gez v5, :cond_9

    .line 130632
    if-eq v5, v8, :cond_6

    .line 130633
    const/4 v5, 0x2

    invoke-direct {p0, v0, v5, v7}, LX/0ln;->a(CILjava/lang/String;)V

    .line 130634
    :cond_6
    if-lt v4, v3, :cond_7

    .line 130635
    invoke-static {}, LX/0ln;->d()V

    .line 130636
    :cond_7
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 130637
    invoke-virtual {p0, v4}, LX/0ln;->a(C)Z

    move-result v5

    if-nez v5, :cond_8

    .line 130638
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "expected padding character \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130639
    iget-char v6, p0, LX/0ln;->b:C

    move v6, v6

    .line 130640
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v9, v5}, LX/0ln;->a(CILjava/lang/String;)V

    .line 130641
    :cond_8
    shr-int/lit8 v1, v1, 0x4

    .line 130642
    invoke-virtual {p2, v1}, LX/2SG;->a(I)V

    goto/16 :goto_0

    .line 130643
    :cond_9
    shl-int/lit8 v0, v1, 0x6

    or-int v1, v0, v5

    .line 130644
    if-lt v4, v3, :cond_b

    .line 130645
    iget-boolean v0, p0, LX/0ln;->a:Z

    move v0, v0

    .line 130646
    if-nez v0, :cond_a

    .line 130647
    shr-int/lit8 v0, v1, 0x2

    .line 130648
    invoke-virtual {p2, v0}, LX/2SG;->b(I)V

    goto :goto_2

    .line 130649
    :cond_a
    invoke-static {}, LX/0ln;->d()V

    .line 130650
    :cond_b
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 130651
    invoke-virtual {p0, v4}, LX/0ln;->b(C)I

    move-result v5

    .line 130652
    if-gez v5, :cond_d

    .line 130653
    if-eq v5, v8, :cond_c

    .line 130654
    invoke-direct {p0, v4, v9, v7}, LX/0ln;->a(CILjava/lang/String;)V

    .line 130655
    :cond_c
    shr-int/lit8 v1, v1, 0x2

    .line 130656
    invoke-virtual {p2, v1}, LX/2SG;->b(I)V

    goto/16 :goto_0

    .line 130657
    :cond_d
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v1, v5

    .line 130658
    invoke-virtual {p2, v1}, LX/2SG;->c(I)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public final a(C)Z
    .locals 1

    .prologue
    .line 130659
    iget-char v0, p0, LX/0ln;->b:C

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 130660
    iget-char v0, p0, LX/0ln;->b:C

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(C)I
    .locals 1

    .prologue
    .line 130661
    const/16 v0, 0x7f

    if-gt p1, v0, :cond_0

    iget-object v0, p0, LX/0ln;->d:[I

    aget v0, v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 130662
    const/16 v0, 0x7f

    if-gt p1, v0, :cond_0

    iget-object v0, p0, LX/0ln;->d:[I

    aget v0, v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 130663
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 130664
    iget-object v0, p0, LX/0ln;->_name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130665
    iget-object v0, p0, LX/0ln;->_name:Ljava/lang/String;

    invoke-static {v0}, LX/0lm;->a(Ljava/lang/String;)LX/0ln;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130666
    iget-object v0, p0, LX/0ln;->_name:Ljava/lang/String;

    return-object v0
.end method
