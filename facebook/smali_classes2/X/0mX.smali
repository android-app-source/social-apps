.class public LX/0mX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0mI;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/0mI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132617
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/0mX;->a:LX/0mY;

    .line 132618
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 132619
    iget-object v0, p0, LX/0mX;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 132620
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 132621
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mI;

    invoke-interface {v0}, LX/0mI;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132622
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132623
    :cond_0
    iget-object v0, p0, LX/0mX;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 132624
    return-void

    .line 132625
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0mX;->a:LX/0mY;

    invoke-virtual {v1}, LX/0mY;->b()V

    throw v0
.end method

.method public final a(LX/0mI;)V
    .locals 1

    .prologue
    .line 132626
    iget-object v0, p0, LX/0mX;->a:LX/0mY;

    invoke-virtual {v0, p1}, LX/0mY;->a(Ljava/lang/Object;)V

    .line 132627
    return-void
.end method
