.class public LX/1cW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1bf;

.field public final b:Ljava/lang/String;

.field public final c:LX/1BV;

.field public final d:Ljava/lang/Object;

.field public final e:LX/1bY;

.field private f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:LX/1bc;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1bf;Ljava/lang/String;LX/1BV;Ljava/lang/Object;LX/1bY;ZZLX/1bc;)V
    .locals 1

    .prologue
    .line 282162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282163
    iput-object p1, p0, LX/1cW;->a:LX/1bf;

    .line 282164
    iput-object p2, p0, LX/1cW;->b:Ljava/lang/String;

    .line 282165
    iput-object p3, p0, LX/1cW;->c:LX/1BV;

    .line 282166
    iput-object p4, p0, LX/1cW;->d:Ljava/lang/Object;

    .line 282167
    iput-object p5, p0, LX/1cW;->e:LX/1bY;

    .line 282168
    iput-boolean p6, p0, LX/1cW;->f:Z

    .line 282169
    iput-object p8, p0, LX/1cW;->g:LX/1bc;

    .line 282170
    iput-boolean p7, p0, LX/1cW;->h:Z

    .line 282171
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cW;->i:Z

    .line 282172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1cW;->j:Ljava/util/List;

    .line 282173
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 282104
    if-nez p0, :cond_1

    .line 282105
    :cond_0
    return-void

    .line 282106
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cg;

    .line 282107
    invoke-virtual {v0}, LX/1cg;->b()V

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)V
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 282158
    if-nez p0, :cond_1

    .line 282159
    :cond_0
    return-void

    .line 282160
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cg;

    .line 282161
    invoke-virtual {v0}, LX/1cg;->c()V

    goto :goto_0
.end method

.method public static c(Ljava/util/List;)V
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 282154
    if-nez p0, :cond_1

    .line 282155
    :cond_0
    return-void

    .line 282156
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cg;

    .line 282157
    invoke-virtual {v0}, LX/1cg;->d()V

    goto :goto_0
.end method

.method private declared-synchronized j()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282148
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cW;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 282149
    const/4 v0, 0x0

    .line 282150
    :goto_0
    monitor-exit p0

    return-object v0

    .line 282151
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1cW;->i:Z

    .line 282152
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1cW;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/1bf;
    .locals 1

    .prologue
    .line 282147
    iget-object v0, p0, LX/1cW;->a:LX/1bf;

    return-object v0
.end method

.method public final declared-synchronized a(LX/1bc;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bc;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cW;->g:LX/1bc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 282142
    const/4 v0, 0x0

    .line 282143
    :goto_0
    monitor-exit p0

    return-object v0

    .line 282144
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/1cW;->g:LX/1bc;

    .line 282145
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1cW;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282135
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cW;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 282136
    const/4 v0, 0x0

    .line 282137
    :goto_0
    monitor-exit p0

    return-object v0

    .line 282138
    :cond_0
    :try_start_1
    iput-boolean p1, p0, LX/1cW;->f:Z

    .line 282139
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1cW;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/1cg;)V
    .locals 2

    .prologue
    .line 282125
    const/4 v0, 0x0

    .line 282126
    monitor-enter p0

    .line 282127
    :try_start_0
    iget-object v1, p0, LX/1cW;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282128
    iget-boolean v1, p0, LX/1cW;->i:Z

    if-eqz v1, :cond_0

    .line 282129
    const/4 v0, 0x1

    .line 282130
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282131
    if-eqz v0, :cond_1

    .line 282132
    invoke-virtual {p1}, LX/1cg;->a()V

    .line 282133
    :cond_1
    return-void

    .line 282134
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282124
    iget-object v0, p0, LX/1cW;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized b(Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 282118
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cW;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 282119
    const/4 v0, 0x0

    .line 282120
    :goto_0
    monitor-exit p0

    return-object v0

    .line 282121
    :cond_0
    :try_start_1
    iput-boolean p1, p0, LX/1cW;->h:Z

    .line 282122
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1cW;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()LX/1BV;
    .locals 1

    .prologue
    .line 282117
    iget-object v0, p0, LX/1cW;->c:LX/1BV;

    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 282116
    iget-object v0, p0, LX/1cW;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 282115
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cW;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()LX/1bc;
    .locals 1

    .prologue
    .line 282114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cW;->g:LX/1bc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Z
    .locals 1

    .prologue
    .line 282113
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cW;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 282108
    invoke-direct {p0}, LX/1cW;->j()Ljava/util/List;

    move-result-object v0

    .line 282109
    if-nez v0, :cond_1

    .line 282110
    :cond_0
    return-void

    .line 282111
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1cg;

    .line 282112
    invoke-virtual {v1}, LX/1cg;->a()V

    goto :goto_0
.end method
