.class public LX/1ev;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Landroid/graphics/Rect;

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/0sd;

.field private final c:LX/0rq;

.field private final d:LX/1f0;

.field private final e:LX/1f1;

.field private final f:LX/0tK;

.field private final g:I

.field private final h:LX/1f2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 289294
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, LX/1ev;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(LX/0sd;LX/0rq;LX/1f0;LX/1f1;LX/0tK;Landroid/content/res/Resources;LX/1f2;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289296
    iput-object p1, p0, LX/1ev;->b:LX/0sd;

    .line 289297
    iput-object p2, p0, LX/1ev;->c:LX/0rq;

    .line 289298
    iput-object p3, p0, LX/1ev;->d:LX/1f0;

    .line 289299
    iput-object p4, p0, LX/1ev;->e:LX/1f1;

    .line 289300
    iput-object p5, p0, LX/1ev;->f:LX/0tK;

    .line 289301
    iput-object p7, p0, LX/1ev;->h:LX/1f2;

    .line 289302
    const v0, 0x7f0b0034

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/1ev;->g:I

    .line 289303
    return-void
.end method

.method public static a(LX/0QB;)LX/1ev;
    .locals 11

    .prologue
    .line 289304
    const-class v1, LX/1ev;

    monitor-enter v1

    .line 289305
    :try_start_0
    sget-object v0, LX/1ev;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 289306
    sput-object v2, LX/1ev;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 289307
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289308
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 289309
    new-instance v3, LX/1ev;

    invoke-static {v0}, LX/0sb;->b(LX/0QB;)LX/0sd;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    .line 289310
    new-instance v6, LX/1f0;

    invoke-direct {v6}, LX/1f0;-><init>()V

    .line 289311
    move-object v6, v6

    .line 289312
    move-object v6, v6

    .line 289313
    check-cast v6, LX/1f0;

    invoke-static {v0}, LX/1f1;->a(LX/0QB;)LX/1f1;

    move-result-object v7

    check-cast v7, LX/1f1;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v8

    check-cast v8, LX/0tK;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1f2;->b(LX/0QB;)LX/1f2;

    move-result-object v10

    check-cast v10, LX/1f2;

    invoke-direct/range {v3 .. v10}, LX/1ev;-><init>(LX/0sd;LX/0rq;LX/1f0;LX/1f1;LX/0tK;Landroid/content/res/Resources;LX/1f2;)V

    .line 289314
    move-object v0, v3

    .line 289315
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 289316
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ev;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289317
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 289318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/graphics/Rect;ILcom/facebook/graphql/model/GraphQLImage;)LX/1f4;
    .locals 10

    .prologue
    .line 289319
    invoke-static {p2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289320
    int-to-float v0, p1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 289321
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289322
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 289323
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v8, v1

    .line 289324
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v9, v0

    .line 289325
    :goto_0
    new-instance v0, LX/1f3;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    iget v3, p0, Landroid/graphics/Rect;->left:I

    iget v4, p0, Landroid/graphics/Rect;->top:I

    iget v5, p0, Landroid/graphics/Rect;->right:I

    iget v6, p0, Landroid/graphics/Rect;->bottom:I

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v9}, LX/1f3;-><init>(IIIIIIIII)V

    .line 289326
    const/4 v5, 0x0

    .line 289327
    iget v2, v0, LX/1f3;->h:I

    .line 289328
    iget v3, v0, LX/1f3;->i:I

    .line 289329
    iget v1, v0, LX/1f3;->a:I

    int-to-float v1, v1

    iget v4, v0, LX/1f3;->b:I

    int-to-float v4, v4

    div-float/2addr v1, v4

    .line 289330
    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_2

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_4

    .line 289331
    :cond_2
    new-instance v1, LX/1f4;

    invoke-direct {v1, v2, v3, v5, v5}, LX/1f4;-><init>(IIII)V

    .line 289332
    :goto_1
    move-object v0, v1

    .line 289333
    return-object v0

    .line 289334
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    .line 289335
    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, LX/1f0;->a(IIIZ)I

    move-result v2

    move v9, v2

    .line 289336
    move v8, p1

    .line 289337
    goto :goto_0

    .line 289338
    :cond_4
    iget v1, v0, LX/1f3;->h:I

    iget v2, v0, LX/1f3;->c:I

    sub-int/2addr v1, v2

    iget v2, v0, LX/1f3;->e:I

    sub-int/2addr v1, v2

    iget v2, v0, LX/1f3;->g:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 289339
    const v2, 0x3f2aaaab

    div-float v2, v1, v2

    iget v3, v0, LX/1f3;->i:I

    iget v4, v0, LX/1f3;->d:I

    sub-int/2addr v3, v4

    iget v4, v0, LX/1f3;->f:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 289340
    iget v3, v0, LX/1f3;->a:I

    int-to-float v3, v3

    div-float v3, v1, v3

    iget v4, v0, LX/1f3;->b:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 289341
    float-to-int v1, v1

    iget v3, v0, LX/1f3;->c:I

    add-int/2addr v1, v3

    iget v3, v0, LX/1f3;->e:I

    add-int/2addr v3, v1

    .line 289342
    iget v1, v0, LX/1f3;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, v0, LX/1f3;->d:I

    add-int/2addr v1, v2

    iget v2, v0, LX/1f3;->f:I

    add-int/2addr v2, v1

    .line 289343
    iget v4, v0, LX/1f3;->g:I

    .line 289344
    new-instance v1, LX/1f4;

    invoke-direct {v1, v3, v2, v4, v4}, LX/1f4;-><init>(IIII)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;
    .locals 4

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 289345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 289346
    iget-object v0, p0, LX/1ev;->f:LX/0tK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0tK;->b(Z)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;
    .locals 4

    .prologue
    .line 289347
    invoke-static {p1, p3}, LX/1f1;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 289348
    invoke-static {p2, p3, v1}, LX/1ev;->a(Landroid/graphics/Rect;ILcom/facebook/graphql/model/GraphQLImage;)LX/1f4;

    move-result-object v2

    .line 289349
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 289350
    new-instance v3, LX/1f5;

    invoke-direct {v3}, LX/1f5;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    .line 289351
    :goto_0
    iput-object v0, v3, LX/1f5;->a:Ljava/lang/String;

    .line 289352
    move-object v0, v3

    .line 289353
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    .line 289354
    iput v3, v0, LX/1f5;->b:I

    .line 289355
    move-object v0, v0

    .line 289356
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    .line 289357
    iput v3, v0, LX/1f5;->c:I

    .line 289358
    move-object v0, v0

    .line 289359
    iget v3, v2, LX/1f4;->a:I

    .line 289360
    iput v3, v0, LX/1f5;->d:I

    .line 289361
    move-object v0, v0

    .line 289362
    iget v2, v2, LX/1f4;->b:I

    .line 289363
    iput v2, v0, LX/1f5;->e:I

    .line 289364
    move-object v0, v0

    .line 289365
    iget-object v2, p0, LX/1ev;->h:LX/1f2;

    invoke-virtual {v2, p1, v1, v0}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;)LX/1bf;

    .line 289366
    invoke-virtual {v0}, LX/1f5;->a()LX/1f6;

    move-result-object v0

    return-object v0

    .line 289367
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 289368
    iget-object v0, p0, LX/1ev;->b:LX/0sd;

    .line 289369
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 289370
    mul-int/lit8 v1, p1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;
    .locals 3

    .prologue
    .line 289371
    invoke-direct {p0}, LX/1ev;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289372
    iget-object v0, p0, LX/1ev;->b:LX/0sd;

    .line 289373
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 289374
    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, p2, v0}, LX/1ev;->b(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    .line 289375
    iget v1, v0, LX/1f6;->h:I

    move v1, v1

    .line 289376
    iget v2, p0, LX/1ev;->g:I

    mul-int/lit8 v2, v2, 0x3c

    if-ge v1, v2, :cond_0

    .line 289377
    iget v1, p0, LX/1ev;->g:I

    mul-int/lit8 v1, v1, 0x3c

    int-to-float v1, v1

    .line 289378
    iget v2, v0, LX/1f6;->h:I

    move v2, v2

    .line 289379
    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 289380
    int-to-float v2, p3

    .line 289381
    iget p3, v0, LX/1f6;->g:I

    move v0, p3

    .line 289382
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    .line 289383
    invoke-direct {p0, p1, p2, v0}, LX/1ev;->b(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    .line 289384
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, LX/1ev;->b(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;
    .locals 3

    .prologue
    .line 289385
    invoke-direct {p0}, LX/1ev;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289386
    sget-object v0, LX/1ev;->a:Landroid/graphics/Rect;

    iget-object v1, p0, LX/1ev;->b:LX/0sd;

    .line 289387
    iget v2, v1, LX/0sd;->c:I

    move v1, v2

    .line 289388
    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    .line 289389
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1ev;->a:Landroid/graphics/Rect;

    iget-object v1, p0, LX/1ev;->b:LX/0sd;

    .line 289390
    iget v2, v1, LX/0sd;->c:I

    move v1, v2

    .line 289391
    invoke-virtual {p0, p1, v0, v1}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 289392
    invoke-virtual {p0, v4}, LX/1ev;->a(I)I

    move-result v1

    .line 289393
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 289394
    invoke-static {v2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289395
    int-to-float v0, v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 289396
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289397
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 289398
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 289399
    :goto_0
    new-instance v3, LX/1f5;

    invoke-direct {v3}, LX/1f5;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    .line 289400
    iput v4, v3, LX/1f5;->b:I

    .line 289401
    move-object v3, v3

    .line 289402
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    .line 289403
    iput v4, v3, LX/1f5;->c:I

    .line 289404
    move-object v3, v3

    .line 289405
    iput v1, v3, LX/1f5;->d:I

    .line 289406
    move-object v1, v3

    .line 289407
    iput v0, v1, LX/1f5;->e:I

    .line 289408
    move-object v0, v1

    .line 289409
    iget-object v1, p0, LX/1ev;->h:LX/1f2;

    invoke-virtual {v1, p1, v2, v0}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;)LX/1bf;

    .line 289410
    invoke-virtual {v0}, LX/1f5;->a()LX/1f6;

    move-result-object v0

    return-object v0

    .line 289411
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    invoke-static {v1, v0, v3, v4}, LX/1f0;->a(IIIZ)I

    move-result v0

    goto :goto_0
.end method
