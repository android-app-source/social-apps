.class public LX/1YF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Landroid/util/SparseIntArray;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 273431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273432
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/1YF;->a:Landroid/util/SparseArray;

    .line 273433
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/1YF;->b:Landroid/util/SparseIntArray;

    .line 273434
    const/4 v0, 0x0

    iput v0, p0, LX/1YF;->c:I

    return-void
.end method

.method private b(I)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273424
    iget-object v0, p0, LX/1YF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 273425
    if-nez v0, :cond_0

    .line 273426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 273427
    iget-object v1, p0, LX/1YF;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 273428
    iget-object v1, p0, LX/1YF;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_0

    .line 273429
    iget-object v1, p0, LX/1YF;->b:Landroid/util/SparseIntArray;

    const/4 v2, 0x5

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 273430
    :cond_0
    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 273422
    iget-object v0, p0, LX/1YF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 273423
    return-void
.end method


# virtual methods
.method public final a(I)LX/1a1;
    .locals 3

    .prologue
    .line 273416
    iget-object v0, p0, LX/1YF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 273417
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273418
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 273419
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 273420
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 273421
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 273414
    iget v0, p0, LX/1YF;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1YF;->c:I

    .line 273415
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 273393
    iget-object v0, p0, LX/1YF;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 273394
    iget-object v0, p0, LX/1YF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 273395
    if-eqz v0, :cond_0

    .line 273396
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 273397
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 273398
    :cond_0
    return-void
.end method

.method public final a(LX/1OM;LX/1OM;Z)V
    .locals 1

    .prologue
    .line 273399
    if-eqz p1, :cond_0

    .line 273400
    invoke-virtual {p0}, LX/1YF;->b()V

    .line 273401
    :cond_0
    if-nez p3, :cond_1

    iget v0, p0, LX/1YF;->c:I

    if-nez v0, :cond_1

    .line 273402
    invoke-direct {p0}, LX/1YF;->c()V

    .line 273403
    :cond_1
    if-eqz p2, :cond_2

    .line 273404
    invoke-virtual {p0}, LX/1YF;->a()V

    .line 273405
    :cond_2
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 3

    .prologue
    .line 273406
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 273407
    invoke-direct {p0, v0}, LX/1YF;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 273408
    iget-object v2, p0, LX/1YF;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v0, v2, :cond_0

    .line 273409
    :goto_0
    return-void

    .line 273410
    :cond_0
    invoke-virtual {p1}, LX/1a1;->u()V

    .line 273411
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 273412
    iget v0, p0, LX/1YF;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1YF;->c:I

    .line 273413
    return-void
.end method
