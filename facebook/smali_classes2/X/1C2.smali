.class public LX/1C2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final G:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile J:LX/1C2;


# instance fields
.field private final A:Z

.field public final B:Z

.field public final C:Z

.field private final D:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/093;",
            ">;"
        }
    .end annotation
.end field

.field private final F:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/1CB;

.field private I:LX/0wp;

.field private final a:LX/0Zb;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/13u;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rD;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0yx;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1C3;

.field private final j:LX/1C4;

.field private final k:LX/04B;

.field public final l:LX/1CE;

.field private final m:LX/1C7;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0oz;

.field private final p:LX/1CC;

.field private final q:LX/0ad;

.field private final r:LX/0Uh;

.field private final s:LX/04J;

.field private final t:LX/04K;

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/1CD;

.field private final w:Landroid/media/AudioManager;

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3nl;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 215035
    sget-object v0, LX/04A;->VIDEO_DISPLAYED:LX/04A;

    iget-object v0, v0, LX/04A;->value:Ljava/lang/String;

    sget-object v1, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    sget-object v2, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    sget-object v3, LX/04A;->VIDEO_START:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    sget-object v4, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    sget-object v5, LX/04A;->VIDEO_ENTERED_HD:LX/04A;

    iget-object v5, v5, LX/04A;->value:Ljava/lang/String;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, LX/04A;->VIDEO_EXITED_HD:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/04A;->VIDEO_PAUSE:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/04A;->VIDEO_COMPLETE:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1C2;->G:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Ot;LX/03V;LX/13u;LX/0Ot;LX/0yx;LX/1C3;LX/1C4;LX/04B;LX/0Sh;LX/0So;LX/0W3;LX/0Or;LX/0Or;LX/1C7;Ljava/lang/String;LX/0Ot;LX/0oz;LX/1CC;LX/0ad;LX/0Uh;LX/04J;LX/04K;LX/0Ot;LX/1CD;Landroid/media/AudioManager;LX/0Ot;)V
    .locals 8
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/video/engine/IsVideoPlayerDebugEnabled;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/video/engine/VideoLoggingLevel;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/13u;",
            "LX/0Ot",
            "<",
            "LX/1rD;",
            ">;",
            "LX/0yx;",
            "LX/1C3;",
            "LX/1C4;",
            "LX/04B;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0So;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1C7;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;",
            "LX/0oz;",
            "LX/1CC;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/04J;",
            "LX/04K;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;",
            "LX/1CD;",
            "Landroid/media/AudioManager;",
            "LX/0Ot",
            "<",
            "LX/3nl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215108
    const-string v1, "video"

    iput-object v1, p0, LX/1C2;->y:Ljava/lang/String;

    .line 215109
    const-string v1, "too_many_events"

    iput-object v1, p0, LX/1C2;->z:Ljava/lang/String;

    .line 215110
    iput-object p1, p0, LX/1C2;->a:LX/0Zb;

    .line 215111
    iput-object p2, p0, LX/1C2;->b:LX/0Ot;

    .line 215112
    iput-object p3, p0, LX/1C2;->c:LX/03V;

    .line 215113
    iput-object p4, p0, LX/1C2;->d:LX/13u;

    .line 215114
    iput-object p5, p0, LX/1C2;->e:LX/0Ot;

    .line 215115
    iput-object p6, p0, LX/1C2;->f:LX/0yx;

    .line 215116
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1C2;->g:LX/0Or;

    .line 215117
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1C2;->h:LX/0Or;

    .line 215118
    iput-object p7, p0, LX/1C2;->i:LX/1C3;

    .line 215119
    move-object/from16 v0, p8

    iput-object v0, p0, LX/1C2;->j:LX/1C4;

    .line 215120
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1C2;->k:LX/04B;

    .line 215121
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1C2;->m:LX/1C7;

    .line 215122
    new-instance v1, LX/1CE;

    iget-object v4, p0, LX/1C2;->c:LX/03V;

    const/16 v5, 0xa

    const/16 v6, 0x64

    move-object/from16 v2, p10

    move-object/from16 v3, p11

    move-object/from16 v7, p12

    invoke-direct/range {v1 .. v7}, LX/1CE;-><init>(LX/0Sh;LX/0So;LX/03V;IILX/0W3;)V

    iput-object v1, p0, LX/1C2;->l:LX/1CE;

    .line 215123
    new-instance v1, LX/0aq;

    const/16 v2, 0xc8

    invoke-direct {v1, v2}, LX/0aq;-><init>(I)V

    iput-object v1, p0, LX/1C2;->D:LX/0aq;

    .line 215124
    new-instance v1, LX/0aq;

    const/16 v2, 0xc8

    invoke-direct {v1, v2}, LX/0aq;-><init>(I)V

    iput-object v1, p0, LX/1C2;->E:LX/0aq;

    .line 215125
    new-instance v1, LX/0aq;

    const/16 v2, 0xc8

    invoke-direct {v1, v2}, LX/0aq;-><init>(I)V

    iput-object v1, p0, LX/1C2;->F:LX/0aq;

    .line 215126
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1C2;->n:LX/0Ot;

    .line 215127
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1C2;->o:LX/0oz;

    .line 215128
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1C2;->p:LX/1CC;

    .line 215129
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1C2;->q:LX/0ad;

    .line 215130
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1C2;->r:LX/0Uh;

    .line 215131
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1C2;->s:LX/04J;

    .line 215132
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1C2;->t:LX/04K;

    .line 215133
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1C2;->u:LX/0Ot;

    .line 215134
    move-object/from16 v0, p25

    iput-object v0, p0, LX/1C2;->v:LX/1CD;

    .line 215135
    move-object/from16 v0, p26

    iput-object v0, p0, LX/1C2;->w:Landroid/media/AudioManager;

    .line 215136
    move-object/from16 v0, p27

    iput-object v0, p0, LX/1C2;->x:LX/0Ot;

    .line 215137
    iget-object v1, p0, LX/1C2;->r:LX/0Uh;

    sget v2, LX/19n;->p:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1C2;->A:Z

    .line 215138
    iget-object v1, p0, LX/1C2;->r:LX/0Uh;

    const/16 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1C2;->B:Z

    .line 215139
    iget-object v1, p0, LX/1C2;->r:LX/0Uh;

    const/16 v2, 0x51f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1C2;->C:Z

    .line 215140
    move-object/from16 v0, p16

    invoke-virtual {p0, v0}, LX/1C2;->a(Ljava/lang/String;)V

    .line 215141
    return-void
.end method

.method public static a(LX/0QB;)LX/1C2;
    .locals 3

    .prologue
    .line 215097
    sget-object v0, LX/1C2;->J:LX/1C2;

    if-nez v0, :cond_1

    .line 215098
    const-class v1, LX/1C2;

    monitor-enter v1

    .line 215099
    :try_start_0
    sget-object v0, LX/1C2;->J:LX/1C2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 215100
    if-eqz v2, :cond_0

    .line 215101
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1C2;->b(LX/0QB;)LX/1C2;

    move-result-object v0

    sput-object v0, LX/1C2;->J:LX/1C2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215102
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 215103
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215104
    :cond_1
    sget-object v0, LX/1C2;->J:LX/1C2;

    return-object v0

    .line 215105
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 215106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;
    .locals 9
    .param p12    # LX/09M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215083
    if-eqz p2, :cond_2

    iget-object v2, p2, LX/04G;->value:Ljava/lang/String;

    .line 215084
    :goto_0
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->IS_ABR_ENABLED:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-static {p4}, LX/1C2;->a(I)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p14

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    int-to-float v5, p6

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    const-string v5, "logVideoUnPausedEvent"

    move-object/from16 v0, p9

    invoke-static {p0, p5, v0, v5}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v5, v0, LX/04H;->eligibility:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 215085
    iget-boolean v4, p0, LX/1C2;->B:Z

    if-eqz v4, :cond_0

    .line 215086
    sget-object v4, LX/04F;->CURRENT_VOLUME:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-static {p0}, LX/1C2;->e(LX/1C2;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215087
    :cond_0
    move-object/from16 v0, p12

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V

    .line 215088
    move-object/from16 v0, p13

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 215089
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215090
    iget-object v4, p0, LX/1C2;->v:LX/1CD;

    invoke-static {v3, v4}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1CD;)V

    .line 215091
    iget-object v4, p0, LX/1C2;->i:LX/1C3;

    move-object/from16 v0, p7

    move-object/from16 v1, p9

    invoke-virtual {v4, v0, p5, v2, v1}, LX/1C3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 215092
    iget-object v2, p0, LX/1C2;->p:LX/1CC;

    invoke-virtual {v2}, LX/1CC;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215093
    iget-object v2, p0, LX/1C2;->t:LX/04K;

    invoke-virtual {v2}, LX/04K;->d()V

    .line 215094
    :cond_1
    if-eqz p13, :cond_3

    invoke-interface/range {p13 .. p13}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v6, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v4, p7

    move-object v5, p1

    move-object/from16 v7, p8

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2

    .line 215095
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 215096
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/04D;LX/04G;)LX/1C2;
    .locals 2
    .param p1    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/04D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215068
    invoke-static {p2}, LX/1C2;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215069
    :goto_0
    return-object p0

    .line 215070
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215071
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 215072
    sget-object v0, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215073
    :cond_1
    invoke-direct {p0, p1, p2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215074
    invoke-direct {p0, p1, p2}, LX/1C2;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215075
    invoke-direct {p0, p1, p2}, LX/1C2;->d(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215076
    if-eqz p3, :cond_2

    .line 215077
    invoke-static {p1, p3}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 215078
    :cond_2
    if-eqz p4, :cond_3

    .line 215079
    sget-object v0, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p4, LX/04G;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215080
    :cond_3
    iget-object v0, p0, LX/1C2;->j:LX/1C4;

    invoke-virtual {v0, p1}, LX/1C6;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215081
    iget-object v0, p0, LX/1C2;->m:LX/1C7;

    invoke-virtual {v0, p1}, LX/1C6;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215082
    invoke-static {p0, p1}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;
    .locals 8
    .param p1    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/04D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215067
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;Z)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;Z)LX/1C2;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/04D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/04G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215047
    invoke-static {p2}, LX/1C2;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215048
    :goto_0
    return-object p0

    .line 215049
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215050
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 215051
    sget-object v0, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215052
    :cond_1
    if-eqz p3, :cond_4

    .line 215053
    sget-object v0, LX/04F;->TRACKING_PARAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215054
    :goto_1
    invoke-virtual {p1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215055
    invoke-direct {p0, p1, p2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215056
    invoke-direct {p0, p1, p2}, LX/1C2;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215057
    invoke-direct {p0, p1, p2}, LX/1C2;->d(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 215058
    if-eqz p5, :cond_2

    .line 215059
    invoke-static {p1, p5}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 215060
    :cond_2
    if-eqz p6, :cond_3

    .line 215061
    sget-object v0, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p6, LX/04G;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215062
    :cond_3
    iget-object v0, p0, LX/1C2;->j:LX/1C4;

    invoke-virtual {v0, p1}, LX/1C6;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215063
    iget-object v0, p0, LX/1C2;->m:LX/1C7;

    invoke-virtual {v0, p1}, LX/1C6;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215064
    invoke-direct {p0, p1, p7}, LX/1C2;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)LX/1C2;

    move-result-object p0

    goto :goto_0

    .line 215065
    :cond_4
    const-string v0, "Missing tracking codes: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 215066
    iget-object v1, p0, LX/1C2;->c:LX/03V;

    sget-object v2, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 215046
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 215038
    invoke-static {p2}, LX/1C2;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215039
    :goto_0
    return-object p2

    .line 215040
    :cond_0
    invoke-static {p1}, LX/1C2;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p2, p1

    .line 215041
    goto :goto_0

    .line 215042
    :cond_1
    iget-object v0, p0, LX/1C2;->c:LX/03V;

    if-eqz v0, :cond_2

    .line 215043
    const-string v0, "from %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 215044
    iget-object v1, p0, LX/1C2;->c:LX/03V;

    sget-object v2, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215045
    :cond_2
    const/4 p2, 0x0

    goto :goto_0
.end method

.method private static a(LX/0JG;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2
    .param p0    # LX/0JG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 214923
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/0JG;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214924
    sget-object v0, LX/04F;->EXTERNAL_LOG_TYPE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/0JG;->value:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->EXTERNAL_LOG_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214925
    :cond_0
    return-void
.end method

.method private static a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 6

    .prologue
    .line 214816
    sget-object v0, LX/04F;->DATA_CONNECTION_QUALITY:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214817
    sget-object v0, LX/04F;->FB_BANDWIDTH_ESTIMATE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->f()D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214818
    sget-object v0, LX/04F;->DATA_LATENCY_QUALITY:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->e()LX/0p3;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214819
    sget-object v0, LX/04F;->FB_LATENCY_ESTIMATE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->k()D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214820
    sget-object v0, LX/04F;->VIDEO_BANDWIDTH_ESTIMATE:LX/04F;

    iget-object v1, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v0, p0, LX/1C2;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1m0;

    .line 214821
    iget-object v2, v0, LX/1m0;->o:LX/1m5;

    move-object v0, v2

    .line 214822
    invoke-interface {v0}, LX/04m;->a()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214823
    return-void
.end method

.method private static a(LX/1C2;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 215026
    if-nez p1, :cond_1

    .line 215027
    :cond_0
    :goto_0
    return-void

    .line 215028
    :cond_1
    iget-object v0, p0, LX/1C2;->E:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/093;

    .line 215029
    if-eqz v0, :cond_0

    .line 215030
    iget-object v1, v0, LX/093;->e:LX/03R;

    move-object v1, v1

    .line 215031
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v1, v2, :cond_0

    .line 215032
    sget-object v1, LX/04F;->VR_CAST_BUTTON_DISPLAYED:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    .line 215033
    iget-object v2, v0, LX/093;->e:LX/03R;

    move-object v0, v2

    .line 215034
    invoke-virtual {p2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;I)V
    .locals 4

    .prologue
    .line 215022
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 215023
    if-eqz v0, :cond_0

    .line 215024
    :goto_1
    return-void

    .line 215025
    :cond_0
    sget-object v0, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    int-to-float v1, p1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V
    .locals 2

    .prologue
    .line 215018
    if-nez p1, :cond_0

    .line 215019
    :goto_0
    return-void

    .line 215020
    :cond_0
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p1, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215021
    sget-object v0, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p1, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V
    .locals 2

    .prologue
    .line 215004
    if-eqz p1, :cond_4

    .line 215005
    sget-object v0, LX/04F;->IS_LIVE_STREAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->a()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215006
    sget-object v0, LX/04F;->IS_BROADCAST:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->b()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215007
    invoke-interface {p1}, LX/098;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/098;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215008
    sget-object v0, LX/04F;->BROADCAST_STATUS:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215009
    :cond_0
    invoke-interface {p1}, LX/098;->d()LX/19o;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 215010
    sget-object v0, LX/04F;->PROJECTION:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->d()LX/19o;

    move-result-object v1

    invoke-virtual {v1}, LX/19o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215011
    :cond_1
    invoke-interface {p1}, LX/098;->f()LX/03z;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 215012
    sget-object v0, LX/04F;->AUDIO_CHANNEL_LAYOUT:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->f()LX/03z;

    move-result-object v1

    invoke-virtual {v1}, LX/03z;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215013
    :cond_2
    invoke-interface {p1}, LX/098;->h()I

    move-result v0

    .line 215014
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 215015
    sget-object v1, LX/04F;->STORY_POSITION:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215016
    :cond_3
    sget-object v0, LX/04F;->IS_SPHERICAL_FALLBACK:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p1}, LX/098;->e()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215017
    :cond_4
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V
    .locals 8

    .prologue
    .line 214985
    if-nez p1, :cond_0

    .line 214986
    :goto_0
    return-void

    .line 214987
    :cond_0
    invoke-virtual {p1}, LX/09M;->f()LX/09O;

    move-result-object v0

    .line 214988
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 214989
    :try_start_0
    sget-object v2, LX/04F;->STALL_RECORD_TIME:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    .line 214990
    iget-wide v6, v0, LX/09O;->a:J

    move-wide v4, v6

    .line 214991
    invoke-virtual {p0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214992
    sget-object v2, LX/04F;->STALL_TIME:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    .line 214993
    iget v3, v0, LX/09O;->c:F

    move v3, v3

    .line 214994
    float-to-double v4, v3

    invoke-virtual {p0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214995
    iget v2, v0, LX/09O;->b:I

    move v2, v2

    .line 214996
    sget-object v3, LX/04F;->STALL_COUNT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214997
    if-lez v2, :cond_1

    .line 214998
    invoke-static {p0, v0, v1}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09O;Lorg/json/JSONObject;)V

    .line 214999
    :cond_1
    iget v2, v0, LX/09O;->k:F

    move v0, v2

    .line 215000
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    .line 215001
    sget-object v2, LX/04F;->TIME_UNTIL_INTERRUPT:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    float-to-double v4, v0

    invoke-virtual {p0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215002
    :cond_2
    sget-object v0, LX/04F;->STALL_INFO_JSON:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 215003
    :catch_0
    goto :goto_0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09O;Lorg/json/JSONObject;)V
    .locals 11

    .prologue
    .line 214934
    sget-object v0, LX/04F;->AVERAGE_STALL_TIME:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1}, LX/09O;->j()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214935
    sget-object v0, LX/04F;->FIRST_STALL_START_POSITION:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214936
    iget-object v1, p1, LX/09O;->d:LX/09N;

    move-object v1, v1

    .line 214937
    iget-wide v5, v1, LX/09N;->a:J

    move-wide v2, v5

    .line 214938
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214939
    sget-object v0, LX/04F;->FIRST_STALL_TIME:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214940
    iget-object v1, p1, LX/09O;->d:LX/09N;

    move-object v1, v1

    .line 214941
    invoke-virtual {v1}, LX/09N;->b()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214942
    sget-object v0, LX/04F;->LAST_STALL_START_POSITION:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214943
    iget-object v1, p1, LX/09O;->e:LX/09N;

    move-object v1, v1

    .line 214944
    iget-wide v5, v1, LX/09N;->a:J

    move-wide v2, v5

    .line 214945
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214946
    sget-object v0, LX/04F;->LAST_STALL_TIME:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214947
    iget-object v1, p1, LX/09O;->e:LX/09N;

    move-object v1, v1

    .line 214948
    invoke-virtual {v1}, LX/09N;->b()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214949
    sget-object v0, LX/04F;->MAX_STALL_START_POSITION:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214950
    iget-object v1, p1, LX/09O;->f:LX/09N;

    move-object v1, v1

    .line 214951
    iget-wide v5, v1, LX/09N;->a:J

    move-wide v2, v5

    .line 214952
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214953
    sget-object v0, LX/04F;->MAX_STALL_TIME:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214954
    iget-object v1, p1, LX/09O;->f:LX/09N;

    move-object v1, v1

    .line 214955
    invoke-virtual {v1}, LX/09N;->b()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214956
    sget-object v0, LX/04F;->RECENT_STALLS_TOTAL_COUNT:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214957
    iget-object v1, p1, LX/09O;->g:LX/0Px;

    move-object v1, v1

    .line 214958
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214959
    sget-object v0, LX/04F;->RECENT_STALLS_TOTAL_DURATION:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214960
    iget v1, p1, LX/09O;->h:F

    move v1, v1

    .line 214961
    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214962
    sget-object v0, LX/04F;->RECENT_STALLS_TOTAL_DURATION_TRIMMED:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214963
    iget v1, p1, LX/09O;->i:F

    move v1, v1

    .line 214964
    float-to-double v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214965
    sget-object v0, LX/04F;->IS_STALLING:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    .line 214966
    iget-boolean v1, p1, LX/09O;->j:Z

    move v1, v1

    .line 214967
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214968
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 214969
    iget-object v0, p1, LX/09O;->g:LX/0Px;

    move-object v3, v0

    .line 214970
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09N;

    .line 214971
    iget-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    if-nez v5, :cond_0

    .line 214972
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    iput-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    .line 214973
    iget-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    const-string v6, "Position"

    .line 214974
    iget-wide v9, v0, LX/09N;->a:J

    move-wide v7, v9

    .line 214975
    invoke-virtual {v5, v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 214976
    iget-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    const-string v6, "Time"

    .line 214977
    iget-wide v9, v0, LX/09N;->b:J

    move-wide v7, v9

    .line 214978
    invoke-virtual {v5, v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 214979
    iget-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    const-string v6, "Duration"

    invoke-virtual {v0}, LX/09N;->b()F

    move-result v7

    float-to-double v7, v7

    invoke-virtual {v5, v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 214980
    :cond_0
    iget-object v5, v0, LX/09N;->d:Lorg/json/JSONObject;

    move-object v0, v5

    .line 214981
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 214982
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 214983
    :cond_1
    sget-object v0, LX/04F;->RECENT_STALLS_DETAIL:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 214984
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214928
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0u7;

    .line 214929
    invoke-virtual {v0}, LX/0u7;->a()F

    move-result v1

    .line 214930
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 214931
    sget-object v2, LX/04F;->BATTERY_LEVEL:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214932
    :cond_0
    sget-object v1, LX/04F;->BATTERY_STATE:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0}, LX/0u7;->b()LX/0y1;

    move-result-object v0

    invoke-virtual {v0}, LX/0y1;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214933
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1CD;)V
    .locals 2

    .prologue
    .line 214926
    sget-object v0, LX/04F;->HEADSET_STATE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1}, LX/1CD;->a()LX/45B;

    move-result-object v1

    iget-object v1, v1, LX/45B;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214927
    return-void
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215149
    if-nez p2, :cond_1

    .line 215150
    :cond_0
    :goto_0
    return-void

    .line 215151
    :cond_1
    iget-object v0, p0, LX/1C2;->E:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/093;

    .line 215152
    if-eqz v0, :cond_0

    .line 215153
    invoke-virtual {v0}, LX/093;->c()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0AR;I)V
    .locals 3

    .prologue
    .line 215217
    sget-object v0, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->IS_ABR_ENABLED:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-static {p3}, LX/1C2;->a(I)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215218
    if-eqz p2, :cond_0

    .line 215219
    sget-object v0, LX/04F;->STREAM_REPRESENTATION_ID:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, p2, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215220
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V
    .locals 13

    .prologue
    .line 215221
    if-nez p1, :cond_1

    .line 215222
    :cond_0
    :goto_0
    return-void

    .line 215223
    :cond_1
    const/4 v0, 0x0

    .line 215224
    :try_start_0
    new-instance v1, Ljava/lang/ProcessBuilder;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "cat"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "/sys/class/thermal/thermal_zone0/temp"

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 215225
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 215226
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 215227
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 215228
    if-eqz v1, :cond_3

    .line 215229
    sget-object v2, LX/04F;->CPU_TEMPERATURE:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    const-wide v11, 0x408f400000000000L    # 1000.0

    .line 215230
    const-wide/16 v7, 0x0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 215231
    :try_start_2
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    move-result-wide v7

    .line 215232
    :goto_1
    cmpl-double v9, v7, v11

    if-lez v9, :cond_2

    div-double/2addr v7, v11

    :cond_2
    move-wide v4, v7

    .line 215233
    invoke-virtual {p0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 215234
    :cond_3
    if-eqz v0, :cond_0

    .line 215235
    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    .line 215236
    :catch_0
    :goto_2
    if-eqz v0, :cond_0

    .line 215237
    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    .line 215238
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v1, :cond_4

    .line 215239
    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    :cond_4
    throw v0

    .line 215240
    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    :catch_1
    goto :goto_2

    :catch_2
    goto :goto_1
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 215241
    if-le p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 215242
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215243
    if-nez p2, :cond_0

    .line 215244
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 215245
    goto :goto_0

    .line 215246
    :cond_1
    iget-object v2, p0, LX/1C2;->D:LX/0aq;

    monitor-enter v2

    .line 215247
    :try_start_0
    iget-object v3, p0, LX/1C2;->D:LX/0aq;

    invoke-virtual {v3, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    .line 215248
    :goto_1
    if-eqz p3, :cond_2

    .line 215249
    iget-object v1, p0, LX/1C2;->D:LX/0aq;

    invoke-virtual {v1, p1, p1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215250
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 215251
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 215252
    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/1C2;
    .locals 30

    .prologue
    .line 215253
    new-instance v2, LX/1C2;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 v4, 0x97

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {p0 .. p0}, LX/13u;->a(LX/0QB;)LX/13u;

    move-result-object v6

    check-cast v6, LX/13u;

    const/16 v7, 0xb5

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0yx;->a(LX/0QB;)LX/0yx;

    move-result-object v8

    check-cast v8, LX/0yx;

    invoke-static/range {p0 .. p0}, LX/1C3;->a(LX/0QB;)LX/1C3;

    move-result-object v9

    check-cast v9, LX/1C3;

    invoke-static/range {p0 .. p0}, LX/1C4;->a(LX/0QB;)LX/1C4;

    move-result-object v10

    check-cast v10, LX/1C4;

    invoke-static/range {p0 .. p0}, LX/04B;->a(LX/0QB;)LX/04B;

    move-result-object v11

    check-cast v11, LX/04B;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v13

    check-cast v13, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v14

    check-cast v14, LX/0W3;

    const/16 v15, 0x12e4

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0x1593

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/1C7;->a(LX/0QB;)LX/1C7;

    move-result-object v17

    check-cast v17, LX/1C7;

    invoke-static/range {p0 .. p0}, LX/1C9;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const/16 v19, 0x1358

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v20

    check-cast v20, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1CC;->a(LX/0QB;)LX/1CC;

    move-result-object v21

    check-cast v21, LX/1CC;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v22

    check-cast v22, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v23

    check-cast v23, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v24

    check-cast v24, LX/04J;

    invoke-static/range {p0 .. p0}, LX/04K;->a(LX/0QB;)LX/04K;

    move-result-object v25

    check-cast v25, LX/04K;

    const/16 v26, 0x292

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/1CD;->a(LX/0QB;)LX/1CD;

    move-result-object v27

    check-cast v27, LX/1CD;

    invoke-static/range {p0 .. p0}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v28

    check-cast v28, Landroid/media/AudioManager;

    const/16 v29, 0x138f

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    invoke-direct/range {v2 .. v29}, LX/1C2;-><init>(LX/0Zb;LX/0Ot;LX/03V;LX/13u;LX/0Ot;LX/0yx;LX/1C3;LX/1C4;LX/04B;LX/0Sh;LX/0So;LX/0W3;LX/0Or;LX/0Or;LX/1C7;Ljava/lang/String;LX/0Ot;LX/0oz;LX/1CC;LX/0ad;LX/0Uh;LX/04J;LX/04K;LX/0Ot;LX/1CD;Landroid/media/AudioManager;LX/0Ot;)V

    .line 215254
    return-object v2
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)LX/1C2;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 215255
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215256
    const-string v0, "video"

    .line 215257
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 215258
    iget-object v0, p0, LX/1C2;->k:LX/04B;

    .line 215259
    sget-object v2, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 215260
    if-eqz v3, :cond_0

    sget-object v2, LX/04B;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 215261
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1C2;->p:LX/1CC;

    invoke-virtual {v0, p1}, LX/1CC;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215262
    invoke-direct {p0, p1}, LX/1C2;->e(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215263
    iget-object v0, p0, LX/1C2;->q:LX/0ad;

    sget-short v2, LX/37A;->f:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215264
    iput-boolean v4, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->k:Z

    .line 215265
    :cond_1
    iget-object v0, p0, LX/1C2;->r:LX/0Uh;

    sget v2, LX/19n;->h:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215266
    sget-object v0, LX/04F;->SEQUENCE_NUMBER:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215267
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 215268
    :goto_1
    sget-object v2, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 215269
    const/16 v3, 0x3e8

    if-le v0, v3, :cond_2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 215270
    iget-object v0, p0, LX/1C2;->c:LX/03V;

    const-string v2, "too_many_events"

    const-string v3, "Too many events for same video: (%s)"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215271
    :cond_2
    iget-object v0, p0, LX/1C2;->d:LX/13u;

    invoke-virtual {v0, p1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215272
    sget-object v0, LX/04F;->IS_LIVE_STREAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 215273
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1C2;->r:LX/0Uh;

    sget v2, LX/19n;->l:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 215274
    iget-object v0, p0, LX/1C2;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 215275
    :goto_2
    invoke-direct {p0, p1}, LX/1C2;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215276
    iget-object v6, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v6, v6

    .line 215277
    sget-object v7, LX/04A;->VIDEO_PAUSE:LX/04A;

    iget-object v7, v7, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    sget-object v7, LX/04A;->VIDEO_COMPLETE:LX/04A;

    iget-object v7, v7, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 215278
    :goto_3
    iget-object v0, p0, LX/1C2;->H:LX/1CB;

    sget-object v1, LX/1CB;->NONE:LX/1CB;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, LX/1C2;->H:LX/1CB;

    sget-object v1, LX/1CB;->ONLY_CORE_ANALYTICS_EVENTS:LX/1CB;

    if-ne v0, v1, :cond_c

    sget-object v0, LX/1C2;->G:Ljava/util/Set;

    .line 215279
    iget-object v1, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 215280
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 215281
    :cond_3
    :goto_4
    return-object p0

    .line 215282
    :catch_0
    move v0, v1

    goto :goto_1

    .line 215283
    :cond_4
    if-eqz p2, :cond_5

    .line 215284
    iget-object v0, p0, LX/1C2;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_2

    .line 215285
    :cond_5
    iget-object v0, p0, LX/1C2;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_2

    .line 215286
    :cond_6
    sget-object v2, LX/04B;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 215287
    invoke-static {v0, v3, p1}, LX/04B;->a(LX/04B;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215288
    :cond_7
    iget-object v2, v0, LX/04B;->g:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 215289
    :goto_5
    if-eqz v2, :cond_0

    .line 215290
    invoke-static {v0, p1, v3}, LX/04B;->a(LX/04B;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 215291
    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    .line 215292
    :cond_9
    :try_start_1
    sget-object v6, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v6, v6, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 215293
    sget-object v8, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v8, v8, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 215294
    sub-double v6, v8, v6

    double-to-int v6, v6

    .line 215295
    iget-object v7, p0, LX/1C2;->f:LX/0yx;

    .line 215296
    invoke-static {v7}, LX/0yx;->b(LX/0yx;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, v7, LX/0yx;->d:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    .line 215297
    iget-boolean v9, v8, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mShouldReportVideoPlay:Z

    move v8, v9

    .line 215298
    if-nez v8, :cond_b
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 215299
    :cond_a
    :goto_6
    goto :goto_3

    .line 215300
    :catch_1
    move-exception v6

    .line 215301
    const-string v7, "VideoLoggingUtils"

    const-string v8, "Failed to report UIH video play event"

    invoke-static {v7, v8, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 215302
    :cond_b
    :try_start_2
    iget-object v8, v7, LX/0yx;->e:LX/0z3;

    invoke-virtual {v8}, LX/0z3;->a()LX/6VO;

    move-result-object v8

    .line 215303
    sget-object v9, LX/14q;->VIDEO_PLAY:LX/14q;

    .line 215304
    iput-object v9, v8, LX/6VO;->a:LX/14q;

    .line 215305
    iput v6, v8, LX/6VO;->c:I

    .line 215306
    invoke-static {v7, v8}, LX/0yx;->a(LX/0yx;LX/6VO;)V

    goto :goto_6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 215307
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 215308
    const-string v1, "VideoLoggingUtils: VIDEO EVENT \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215309
    const-string v1, "VideoLoggingUtils: ____________________________\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215310
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 215311
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 215312
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video_id"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215313
    const-string v1, "VideoLoggingUtils: ----------------------------\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215314
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Time:        "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "video_time_position"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(Cur) | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video_last_start_time_position"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(LST) | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video_seek_source_time_position"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(Source)\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215315
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Play_Reason: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "video_play_reason"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215316
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Player:      "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "player"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215317
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Channel_Session: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "video_chaining_session_id"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215318
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Channel_Depth: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "video_chaining_depth_level"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215319
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Player_Orig: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "player_origin"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215320
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      Player_Suborig: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "player_suborigin"

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215321
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoLoggingUtils:      All:         "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4
.end method

.method private static b(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 215322
    if-nez p2, :cond_1

    .line 215323
    :cond_0
    :goto_0
    return-void

    .line 215324
    :cond_1
    iget-object v0, p0, LX/1C2;->E:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/093;

    .line 215325
    if-eqz v0, :cond_0

    .line 215326
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 215327
    const-string v2, "autoplay_fb_bandwidth"

    iget-wide v3, v0, LX/093;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215328
    const-string v2, "autoplay_video_bandwidth"

    iget-wide v3, v0, LX/093;->g:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215329
    const-string v2, "autoplay_time_since_last_fb_bandwidth_ms"

    iget-wide v3, v0, LX/093;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215330
    const-string v2, "autoplay_time_since_last_connectivity_change_ms"

    iget-wide v3, v0, LX/093;->i:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215331
    iget-object v2, v0, LX/093;->j:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/093;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 215332
    const-string v2, "autoplay_connection_oracle_model_desc"

    iget-object v3, v0, LX/093;->j:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215333
    const-string v2, "autoplay_connection_oracle_bw_est"

    iget v3, v0, LX/093;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215334
    :cond_2
    move-object v0, v1

    .line 215335
    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 8

    .prologue
    .line 215206
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 215207
    sget-object v1, LX/04A;->VIDEO_PAUSE:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/04A;->VIDEO_COMPLETE:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215208
    :cond_0
    sget-object v0, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 215209
    sget-object v0, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    move-wide v4, v2

    move-wide v2, v0

    .line 215210
    :goto_0
    iget-object v0, p0, LX/1C2;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rD;

    invoke-virtual {v0}, LX/1rD;->a()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    long-to-double v6, v0

    .line 215211
    sub-double v0, v2, v4

    sub-double v2, v6, v0

    .line 215212
    iget-object v0, p0, LX/1C2;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rD;

    const-string v1, "video"

    double-to-long v2, v2

    double-to-long v4, v6

    invoke-virtual/range {v0 .. v5}, LX/1rD;->a(Ljava/lang/String;JJ)V

    .line 215213
    :cond_1
    return-void

    .line 215214
    :cond_2
    sget-object v1, LX/04A;->VIDEO_SEEK:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215215
    sget-object v0, LX/04F;->SEEK_SOURCE_POSITION_PARAM:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    move-wide v2, v0

    move-wide v4, v0

    .line 215216
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 215336
    if-nez p0, :cond_1

    .line 215337
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;
    .locals 11
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215188
    const-string v2, "logVideoStartEvent"

    move-object/from16 v0, p5

    move-object/from16 v1, p11

    invoke-static {p0, v0, v1, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 215189
    if-eqz p2, :cond_2

    iget-object v2, p2, LX/04G;->value:Ljava/lang/String;

    .line 215190
    :goto_0
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/04A;->VIDEO_START:LX/04A;

    iget-object v5, v5, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->IS_ABR_ENABLED:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    invoke-static {p4}, LX/1C2;->a(I)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p6

    int-to-float v6, v0

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->PREVIOUS_VIDEO_ID:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p13

    iget-object v6, v0, LX/04H;->eligibility:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    .line 215191
    iget-boolean v4, p0, LX/1C2;->B:Z

    if-eqz v4, :cond_0

    .line 215192
    sget-object v4, LX/04F;->CURRENT_VOLUME:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-static {p0}, LX/1C2;->e(LX/1C2;)I

    move-result v5

    invoke-virtual {v10, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215193
    :cond_0
    move-object/from16 v0, p15

    invoke-static {v10, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 215194
    move-object/from16 v0, p8

    invoke-static {p0, v0, v10}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215195
    move-object/from16 v0, p14

    invoke-static {v10, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V

    .line 215196
    invoke-static {p0, v10}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215197
    iget-object v4, p0, LX/1C2;->v:LX/1CD;

    invoke-static {v10, v4}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1CD;)V

    .line 215198
    iget-object v4, p0, LX/1C2;->i:LX/1C3;

    move-object/from16 v0, p8

    move-object/from16 v1, p5

    invoke-virtual {v4, v0, v1, v2, v3}, LX/1C3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 215199
    iget-object v2, p0, LX/1C2;->p:LX/1CC;

    invoke-virtual {v2}, LX/1CC;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215200
    iget-object v2, p0, LX/1C2;->t:LX/04K;

    invoke-virtual {v2}, LX/04K;->d()V

    .line 215201
    :cond_1
    move-object/from16 v0, p8

    invoke-static {p0, v10, v0}, LX/1C2;->b(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p15

    .line 215202
    invoke-static/range {v2 .. v9}, LX/1C2;->c(LX/1C2;LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 215203
    if-eqz p15, :cond_3

    invoke-interface/range {p15 .. p15}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v6, 0x1

    :goto_1
    move-object v2, p0

    move-object v3, v10

    move-object/from16 v4, p8

    move-object v5, p1

    move-object/from16 v7, p9

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2

    .line 215204
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 215205
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static c(LX/1C2;LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 215182
    if-eqz p7, :cond_0

    invoke-interface {p7}, LX/098;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215183
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_SPHERICAL_FALLBACK_ENTERED:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 215184
    invoke-static {v1, p7}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 215185
    if-eqz p7, :cond_1

    invoke-interface {p7}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p5

    move-object v3, p1

    move-object v5, p6

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object p0

    .line 215186
    :cond_0
    return-object p0

    .line 215187
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private c(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 215176
    if-nez p2, :cond_0

    .line 215177
    :goto_0
    return-void

    .line 215178
    :cond_0
    iget-object v0, p0, LX/1C2;->F:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 215179
    if-nez v0, :cond_1

    .line 215180
    iget-object v0, p0, LX/1C2;->F:LX/0aq;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-virtual {v0, p2, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215181
    :cond_1
    sget-object v0, LX/04F;->SEQUENCE_NUMBER:LX/04F;

    iget-object v1, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v0, p0, LX/1C2;->F:LX/0aq;

    invoke-virtual {v0, p2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;
    .locals 1

    .prologue
    .line 215175
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1C2;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215170
    if-nez p2, :cond_1

    .line 215171
    :cond_0
    :goto_0
    return-void

    .line 215172
    :cond_1
    iget-object v0, p0, LX/1C2;->s:LX/04J;

    invoke-virtual {v0, p2}, LX/04J;->a(Ljava/lang/String;)LX/0P1;

    move-result-object v0

    .line 215173
    if-eqz v0, :cond_0

    .line 215174
    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 215148
    if-eqz p0, :cond_0

    const-string v0, "GIF:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(LX/1C2;)I
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 215167
    iget-object v0, p0, LX/1C2;->w:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 215168
    iget-object v1, p0, LX/1C2;->w:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 215169
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method

.method private e(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 215156
    iget-object v0, p0, LX/1C2;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nl;

    .line 215157
    iget-object v1, v0, LX/3nl;->b:LX/376;

    .line 215158
    iget-boolean p0, v1, LX/376;->e:Z

    move v1, p0

    .line 215159
    if-nez v1, :cond_1

    .line 215160
    const/4 v1, 0x0

    .line 215161
    :goto_0
    move-object v0, v1

    .line 215162
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215163
    :goto_1
    return-void

    .line 215164
    :cond_0
    sget-object v1, LX/04F;->WATCH_AND_GO_SESSION_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    :cond_1
    iget-object v1, v0, LX/3nl;->b:LX/376;

    .line 215165
    iget-object p0, v1, LX/376;->d:Ljava/lang/String;

    move-object v1, p0

    .line 215166
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/04G;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/0lF;II)LX/1C2;
    .locals 7

    .prologue
    .line 215154
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_SPATIAL_AUDIO_BUFFER_UNDERRUN:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->BUFFER_UNDERRUN_COUNT:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 215155
    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move-object v5, p2

    move-object v6, p1

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/04G;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/0lF;IIJ)LX/1C2;
    .locals 10

    .prologue
    .line 215036
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_FRAMES_DROPPED:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->FRAME_DROP_COUNT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->FRAME_DROP_ELAPSED_TIME:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-wide/from16 v0, p8

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 215037
    const/4 v6, 0x0

    move-object v2, p0

    move-object v4, p4

    move-object v5, p5

    move-object v7, p2

    move-object v8, p1

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;)LX/1C2;
    .locals 4

    .prologue
    .line 215142
    sget-object v0, LX/04A;->VIDEO_SURFACE_UPDATED:LX/04A;

    iget-object v1, v0, LX/04A;->value:Ljava/lang/String;

    .line 215143
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/04G;->value:Ljava/lang/String;

    .line 215144
    :goto_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-static {p0, p4, v1}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 215145
    invoke-static {v0, p3}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 215146
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0

    .line 215147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/04G;Ljava/lang/String;LX/0lF;Ljava/lang/String;LX/04D;Z)LX/1C2;
    .locals 7

    .prologue
    .line 214813
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JC;->CHROMECAST_CAST_DISCONNECTED:LX/0JC;

    iget-object v1, v1, LX/0JC;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0JB;->DISCONNECT_REASON_CODE:LX/0JB;

    iget-object v1, v1, LX/0JB;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p4

    move-object v3, p3

    move v4, p6

    move-object v5, p5

    move-object v6, p1

    .line 214814
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/04G;Ljava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;ILX/098;)LX/1C2;
    .locals 4

    .prologue
    .line 214806
    sget-object v0, LX/04A;->VIDEO_DISCONTINUED:LX/04A;

    iget-object v1, v0, LX/04A;->value:Ljava/lang/String;

    .line 214807
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/04G;->value:Ljava/lang/String;

    .line 214808
    :goto_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-static {p0, p5, v1}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p7

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 214809
    invoke-static {v0, p4}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 214810
    invoke-static {v0, p8}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214811
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0

    .line 214812
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/093;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)LX/1C2;
    .locals 13

    .prologue
    .line 214791
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_DISPLAYED:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p5

    iget-object v4, v0, LX/04H;->eligibility:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-virtual {p1}, LX/093;->c()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    .line 214792
    move-object/from16 v0, p6

    invoke-static {v12, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214793
    invoke-static {p0, v12}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214794
    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    .line 214795
    iget-object v2, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->g()LX/0am;

    move-result-object v3

    .line 214796
    iget-object v2, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->d()LX/0am;

    move-result-object v10

    .line 214797
    iget-object v2, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->f()D

    move-result-wide v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iget-object v2, p0, LX/1C2;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1m0;

    invoke-virtual {v2}, LX/1m0;->c()LX/04m;

    move-result-object v2

    invoke-interface {v2}, LX/04m;->a()J

    move-result-wide v6

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :goto_0
    invoke-virtual {v10}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v10}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    :goto_1
    move-object v3, p1

    invoke-virtual/range {v3 .. v11}, LX/093;->a(JJJJ)V

    .line 214798
    iget-object v2, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214799
    iget-object v2, p0, LX/1C2;->o:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1C2;->o:LX/0oz;

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    invoke-virtual {v3, v4, v5}, LX/0oz;->a(D)I

    move-result v3

    invoke-virtual {p1, v2, v3}, LX/093;->a(Ljava/lang/String;I)V

    .line 214800
    :cond_0
    iget-object v2, p0, LX/1C2;->E:LX/0aq;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, p1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214801
    :cond_1
    iget-object v2, p0, LX/1C2;->p:LX/1CC;

    invoke-virtual {v2}, LX/1CC;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214802
    iget-object v2, p0, LX/1C2;->t:LX/04K;

    invoke-virtual {v2}, LX/04K;->b()V

    .line 214803
    :cond_2
    if-eqz p6, :cond_5

    invoke-interface/range {p6 .. p6}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v6, 0x1

    :goto_2
    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, v12

    move-object/from16 v4, p3

    move-object v5, p2

    move-object/from16 v7, p4

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2

    .line 214804
    :cond_3
    const-wide/16 v8, -0x1

    goto :goto_0

    :cond_4
    const-wide/16 v10, -0x1

    goto :goto_1

    .line 214805
    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public final a(LX/0JO;IZ)LX/1C2;
    .locals 2

    .prologue
    .line 214789
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/0JO;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0JN;->POOL_COUNT:LX/0JN;

    iget-object v1, v1, LX/0JN;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/0JN;->IS_360_VIDEO:LX/0JN;

    iget-object v1, v1, LX/0JN;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 214790
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;
    .locals 9
    .param p10    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0JG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "LX/04G;",
            "LX/04G;",
            "Ljava/lang/String;",
            "LX/04D;",
            "Ljava/lang/String;",
            "II",
            "LX/098;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "LX/0JG;",
            "Ljava/lang/String;",
            ")",
            "LX/1C2;"
        }
    .end annotation

    .prologue
    .line 214772
    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    if-eq p2, v2, :cond_1

    const/4 v2, 0x1

    .line 214773
    :goto_0
    if-eqz v2, :cond_2

    .line 214774
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 214775
    const-string v2, "content_id"

    invoke-interface {v3, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214776
    iget-object v2, p0, LX/1C2;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gh;

    const-string v4, "video"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5, v3}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 214777
    :goto_1
    invoke-static {p6}, LX/1C2;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 214778
    iget-object v2, p0, LX/1C2;->c:LX/03V;

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    const-string v4, "Invalid player format change reason (%s)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p6, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214779
    :cond_0
    if-eqz p3, :cond_3

    iget-object v2, p3, LX/04G;->value:Ljava/lang/String;

    .line 214780
    :goto_2
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_PLAYER_FORMAT_CHANGED:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->PREVIOUS_PLAYER_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p7

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p8

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214781
    move-object/from16 v0, p9

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214782
    invoke-static {p0, p4, v3}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214783
    move-object/from16 v0, p11

    move-object/from16 v1, p12

    invoke-static {v0, v1, v3}, LX/1C2;->a(LX/0JG;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214784
    if-eqz p9, :cond_4

    invoke-interface/range {p9 .. p9}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v6, 0x1

    :goto_3
    move-object v2, p0

    move-object v4, p4

    move-object v5, p1

    move-object v7, p5

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2

    .line 214785
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 214786
    :cond_2
    iget-object v2, p0, LX/1C2;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gh;

    const-string v3, "video"

    invoke-virtual {v2, v3}, LX/0gh;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 214787
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 214788
    :cond_4
    const/4 v6, 0x0

    goto :goto_3
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 214769
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_VOLUME_DECREASE:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->CURRENT_VOLUME:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214770
    invoke-static {v1, p8}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214771
    if-eqz p8, :cond_0

    invoke-interface {p8}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p6

    move-object v3, p1

    move-object v5, p7

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/098;)LX/1C2;
    .locals 9

    .prologue
    .line 214763
    const-string v2, "logVideoPlayedForThreeSeconds"

    move-object/from16 v0, p8

    invoke-static {p0, v0, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 214764
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_PLAYED_FOR_THREE_SECONDS:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p5

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214765
    move-object/from16 v0, p10

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214766
    invoke-static {p0, p6, v3}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214767
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214768
    if-eqz p10, :cond_0

    invoke-interface/range {p10 .. p10}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v2, p0

    move-object v4, p6

    move-object v5, p1

    move-object/from16 v7, p7

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;Z)LX/1C2;
    .locals 7

    .prologue
    const/high16 v4, 0x447a0000    # 1000.0f

    .line 214761
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_SEEK:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->SEEK_SOURCE_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    div-float/2addr v2, v4

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p5

    div-float/2addr v2, v4

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    iget-object v2, v2, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p6

    move-object v3, p1

    move v4, p8

    move-object v5, p7

    move-object v6, p2

    .line 214762
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;
    .locals 1

    .prologue
    .line 214759
    iget-object v0, p0, LX/1C2;->i:LX/1C3;

    invoke-virtual {v0, p8}, LX/1C3;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214760
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual/range {p0 .. p15}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 214686
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_MUTED:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214687
    invoke-static {v1, p7}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214688
    if-eqz p7, :cond_0

    invoke-interface {p7}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p5

    move-object v3, p1

    move-object v5, p6

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;Ljava/lang/String;LX/098;Z)LX/1C2;
    .locals 8
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 214746
    const/4 v1, 0x0

    invoke-direct {p0, p6, p4, v1}, LX/1C2;->a(Ljava/lang/String;IZ)Z

    move-result v1

    .line 214747
    if-eqz v1, :cond_1

    sget-object v1, LX/0JV;->STARTED:LX/0JV;

    .line 214748
    :goto_0
    const-string v2, "logVideoStartRequested"

    move-object/from16 v0, p9

    invoke-static {p0, p3, v0, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 214749
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_ENCODE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->PREVIOUS_VIDEO_ID:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p8

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->REQUESTED_STATE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    iget-object v1, v1, LX/0JV;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v3, v0, LX/04H;->eligibility:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->DASH_MANIFEST_AVAILABLE:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 214750
    move-object/from16 v0, p13

    invoke-static {v2, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214751
    invoke-static {p0, v2}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214752
    invoke-static {v2, p4}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;I)V

    .line 214753
    iget-object v1, p0, LX/1C2;->p:LX/1CC;

    invoke-virtual {v1}, LX/1CC;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214754
    iget-object v1, p0, LX/1C2;->t:LX/04K;

    invoke-virtual {v1}, LX/04K;->c()V

    .line 214755
    :cond_0
    invoke-static {p0, v2, p6}, LX/1C2;->b(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 214756
    if-eqz p13, :cond_2

    invoke-interface/range {p13 .. p13}, LX/098;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v5, 0x1

    :goto_1
    move-object v1, p0

    move-object v3, p6

    move-object v4, p1

    move-object v6, p7

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v1

    return-object v1

    .line 214757
    :cond_1
    sget-object v1, LX/0JV;->UNPAUSED:LX/0JV;

    goto/16 :goto_0

    .line 214758
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;IIILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;
    .locals 9
    .param p12    # LX/09M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 214735
    iget-object v2, p0, LX/1C2;->i:LX/1C3;

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1C3;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 214736
    :goto_0
    return-object p0

    .line 214737
    :cond_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_COMPLETE:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p14

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p6

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p7

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "logVideoCompleteEvent"

    move-object/from16 v0, p10

    invoke-static {p0, v4, v0, v5}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214738
    invoke-static {v3, p3, p4, p5}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0AR;I)V

    .line 214739
    iget-object v2, p0, LX/1C2;->u:LX/0Ot;

    invoke-static {v3, v2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0Ot;)V

    .line 214740
    iget-boolean v2, p0, LX/1C2;->A:Z

    invoke-static {v3, v2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V

    .line 214741
    move-object/from16 v0, p13

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214742
    move-object/from16 v0, p12

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V

    .line 214743
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214744
    iget-object v2, p0, LX/1C2;->i:LX/1C3;

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1C3;->a(Ljava/lang/String;)Z

    .line 214745
    if-eqz p13, :cond_1

    invoke-interface/range {p13 .. p13}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v4, p8

    move-object v5, p1

    move-object/from16 v7, p9

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object p0

    goto/16 :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;
    .locals 1

    .prologue
    .line 214733
    iget-object v0, p0, LX/1C2;->i:LX/1C3;

    invoke-virtual {v0, p9}, LX/1C3;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214734
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual/range {p0 .. p15}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;
    .locals 9

    .prologue
    .line 214730
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_PLAYER_PAUSE:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p5

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p6

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    const-string v4, "logVideoPlayerPauseEvent"

    move-object/from16 v0, p9

    invoke-static {p0, p4, v0, v4}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214731
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    move-object v2, p0

    move-object/from16 v4, p7

    move-object v5, p1

    move/from16 v6, p10

    move-object/from16 v7, p8

    move-object v8, p2

    .line 214732
    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;ZLjava/lang/String;)LX/1C2;
    .locals 9

    .prologue
    .line 214726
    const-string v2, "logVideoPlayerStopEvent"

    move-object/from16 v0, p9

    invoke-static {p0, p4, v0, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 214727
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_PLAYER_STOP:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    int-to-float v5, p5

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    int-to-float v5, p6

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214728
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    move-object v2, p0

    move-object/from16 v4, p7

    move-object v5, p1

    move/from16 v6, p10

    move-object/from16 v7, p8

    move-object v8, p2

    .line 214729
    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;
    .locals 7

    .prologue
    .line 214723
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_PLAYER_UNPAUSE:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p5

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    const-string v2, "logVideoPlayerUnPausedEvent"

    invoke-static {p0, p4, p8, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214724
    invoke-static {p0, v1}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    move-object v0, p0

    move-object v2, p6

    move-object v3, p1

    move/from16 v4, p9

    move-object v5, p7

    move-object v6, p2

    .line 214725
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;)LX/1C2;
    .locals 8
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 214709
    const/4 v1, 0x0

    invoke-direct {p0, p7, p5, v1}, LX/1C2;->a(Ljava/lang/String;IZ)Z

    move-result v1

    .line 214710
    if-eqz v1, :cond_0

    sget-object v1, LX/0JV;->STARTED:LX/0JV;

    .line 214711
    :goto_0
    const-string v2, "logVideoStartCancelled"

    move-object/from16 v0, p10

    invoke-static {p0, p4, v0, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 214712
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    iget-object v4, v4, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->PREVIOUS_VIDEO_ID:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->REQUESTED_STATE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    iget-object v1, v1, LX/0JV;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 214713
    iget-object v1, p0, LX/1C2;->u:LX/0Ot;

    invoke-static {v2, v1}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0Ot;)V

    .line 214714
    iget-boolean v1, p0, LX/1C2;->A:Z

    invoke-static {v2, v1}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V

    .line 214715
    move-object/from16 v0, p13

    invoke-static {v2, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214716
    move-object/from16 v0, p12

    invoke-static {v2, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V

    .line 214717
    invoke-static {p0, v2}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214718
    iget-object v1, p0, LX/1C2;->i:LX/1C3;

    invoke-virtual {v1, p7}, LX/1C3;->b(Ljava/lang/String;)V

    .line 214719
    invoke-static {p0, v2, p7}, LX/1C2;->b(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 214720
    if-eqz p13, :cond_1

    invoke-interface/range {p13 .. p13}, LX/098;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v5, 0x1

    :goto_1
    move-object v1, p0

    move-object v3, p7

    move-object v4, p1

    move-object/from16 v6, p8

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v1

    return-object v1

    .line 214721
    :cond_0
    sget-object v1, LX/0JV;->UNPAUSED:LX/0JV;

    goto/16 :goto_0

    .line 214722
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(LX/0lF;Ljava/lang/String;ILX/04D;LX/04G;ZZ)LX/1C2;
    .locals 7

    .prologue
    .line 214707
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_WATCH_AND_BROWSE_DISMISS_VIDEO_PLAYER:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_IS_PLAYING:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_IS_PLAY_COMPLETED:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214708
    const/4 v4, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;Ljava/lang/String;LX/04D;LX/04G;Z)LX/1C2;
    .locals 7

    .prologue
    .line 214705
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/04C;->VIDEO_CHAINING_IMPRESSION:LX/04C;

    iget-object v0, v0, LX/04C;->value:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move v4, p5

    move-object v5, p3

    move-object v6, p4

    .line 214706
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/04G;Ljava/lang/String;ILjava/lang/String;IILX/04D;Ljava/lang/String;LX/0AR;LX/0AR;LX/0JM;Z)LX/1C2;
    .locals 10

    .prologue
    .line 214689
    iget-object v2, p0, LX/1C2;->I:LX/0wp;

    if-nez v2, :cond_0

    .line 214690
    iget-object v2, p0, LX/1C2;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0wp;

    iput-object v2, p0, LX/1C2;->I:LX/0wp;

    .line 214691
    :cond_0
    sget-object v2, LX/09L;->DASH:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LX/09L;->DASH_LIVE:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 214692
    :goto_0
    return-object p0

    .line 214693
    :cond_1
    sget-object v2, LX/09L;->DASH_LIVE:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 214694
    const/4 v9, 0x1

    .line 214695
    :goto_1
    sget-object v2, LX/04A;->VIDEO_REPRESENTATION_ENDED:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    .line 214696
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->IS_ABR_ENABLED:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-static {p4}, LX/1C2;->a(I)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    sget-object v4, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-static {p0, v0, v2}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p6

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p7

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->STREAM_REPRESENTATION_EVENT_SOURCE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p12

    iget-object v4, v0, LX/0JM;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214697
    sget-object v2, LX/09L;->DASH_LIVE:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214698
    sget-object v2, LX/04F;->IS_LIVE_STREAM:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214699
    :cond_2
    if-eqz p10, :cond_3

    .line 214700
    sget-object v2, LX/04F;->STREAM_REPRESENTATION_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    iget-object v4, v0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v4, LX/04F;->STREAM_VIDEO_WIDTH:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    iget v5, v0, LX/0AR;->f:I

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v4, LX/04F;->STREAM_VIDEO_HEIGHT:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    iget v5, v0, LX/0AR;->g:I

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v4, LX/04F;->STREAM_BITRATE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    iget v5, v0, LX/0AR;->c:I

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v4, LX/04F;->STREAM_MIME_TYPE:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p10

    iget-object v5, v0, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214701
    :cond_3
    if-eqz p11, :cond_4

    .line 214702
    sget-object v2, LX/04F;->STREAM_NEXT_REPRESENTATION_ID:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v4, v0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214703
    :cond_4
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214704
    iget-object v5, p1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-boolean v6, p1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    move-object v2, p0

    move-object v4, p5

    move-object/from16 v7, p8

    move-object v8, p2

    invoke-direct/range {v2 .. v9}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;Z)LX/1C2;

    move-result-object p0

    goto/16 :goto_0

    :cond_5
    move/from16 v9, p13

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;JIIIIFILX/04D;)LX/1C2;
    .locals 4

    .prologue
    .line 214855
    const/16 v0, 0x5a

    if-gt p4, v0, :cond_0

    const/16 v0, -0x5a

    if-ge p4, v0, :cond_2

    .line 214856
    :cond_0
    const/4 v0, 0x1

    .line 214857
    :goto_0
    move v0, v0

    .line 214858
    int-to-float v1, p4

    .line 214859
    const/high16 p4, 0x42b40000    # 90.0f

    cmpl-float p4, v1, p4

    if-lez p4, :cond_3

    .line 214860
    const/high16 p4, 0x43340000    # 180.0f

    sub-float v1, p4, v1

    .line 214861
    :cond_1
    :goto_1
    move v1, v1

    .line 214862
    float-to-int v1, v1

    move v1, v1

    .line 214863
    int-to-float v2, p5

    invoke-static {v2, v0}, LX/7Cq;->a(FZ)F

    move-result v2

    float-to-int v2, v2

    move v0, v2

    .line 214864
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0JL;->SPHERICAL_VIDEO_VIEWPORT_CHANGE:LX/0JL;

    iget-object v3, v3, LX/0JL;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/0JJ;->SPHERICAL_VIEWPORT_YAW_ANGLE:LX/0JJ;

    iget-object v3, v3, LX/0JJ;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/0JJ;->SPHERICAL_VIEWPORT_ROLL_ANGLE:LX/0JJ;

    iget-object v2, v2, LX/0JJ;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/0JJ;->SPHERICAL_VIEWPORT_PITCH_ANGLE:LX/0JJ;

    iget-object v2, v2, LX/0JJ;->value:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/0JJ;->SPHERICAL_LAST_DRAG_TIMESTAMP:LX/0JJ;

    iget-object v1, v1, LX/0JJ;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/0JJ;->SPHERICAL_FIELD_OF_VIEW_VERTICAL:LX/0JJ;

    iget-object v1, v1, LX/0JJ;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/0JJ;->SPHERICAL_VIDEO_ASPECT_RATIO:LX/0JJ;

    iget-object v1, v1, LX/0JJ;->value:Ljava/lang/String;

    float-to-double v2, p8

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p10, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p10, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 214865
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 214866
    :cond_3
    const/high16 p4, -0x3d4c0000    # -90.0f

    cmpg-float p4, v1, p4

    if-gez p4, :cond_1

    .line 214867
    const/high16 p4, -0x3ccc0000    # -180.0f

    sub-float v1, p4, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;
    .locals 7

    .prologue
    .line 214921
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_ENTERED_HD:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p5

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p6

    move-object v5, p3

    move-object v6, p4

    .line 214922
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/1C2;
    .locals 9

    .prologue
    .line 214919
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_QUALITY_SELECTED:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    int-to-float v4, p5

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->SELECTED_QUALITY:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->PREVIOUS_SELECTED_QUALITY:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p8

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->PRESELECTED_QUALITY:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p6

    move-object v7, p3

    move-object v8, p4

    .line 214920
    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v2

    return-object v2
.end method

.method public final a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;ZLjava/lang/String;)LX/1C2;
    .locals 7

    .prologue
    .line 214917
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_QUALITY_SELECTOR_TAPPED:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->QUALITY_SELECTOR_SURFACE:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p5

    move-object v5, p3

    move-object v6, p4

    .line 214918
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 214911
    if-eqz p1, :cond_0

    sget-object v0, LX/04A;->VIDEO_HEADSET_CONNECTED:LX/04A;

    iget-object v0, v0, LX/04A;->value:Ljava/lang/String;

    .line 214912
    :goto_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v0, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p5

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214913
    invoke-static {v1, p8}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214914
    if-eqz p8, :cond_1

    invoke-interface {p8}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    :goto_1
    move-object v0, p0

    move-object v2, p6

    move-object v3, p2

    move-object v5, p7

    move-object v6, p3

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0

    .line 214915
    :cond_0
    sget-object v0, LX/04A;->VIDEO_HEADSET_DISCONNECTED:LX/04A;

    iget-object v0, v0, LX/04A;->value:Ljava/lang/String;

    goto :goto_0

    .line 214916
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 214908
    :try_start_0
    invoke-static {p1}, LX/1CB;->valueOf(Ljava/lang/String;)LX/1CB;

    move-result-object v0

    iput-object v0, p0, LX/1C2;->H:LX/1CB;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214909
    :goto_0
    return-void

    .line 214910
    :catch_0
    sget-object v0, LX/1CB;->NONE:LX/1CB;

    iput-object v0, p0, LX/1C2;->H:LX/1CB;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/04D;LX/098;LX/098;LX/0lF;LX/04G;)V
    .locals 7

    .prologue
    .line 214904
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_IN_TRANSITION:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->OLD_BROADCAST_STATUS:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p3}, LX/098;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->BROADCAST_STATUS:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-interface {p4}, LX/098;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214905
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p1

    move-object v3, p5

    move-object v5, p2

    move-object v6, p6

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 214906
    return-void

    .line 214907
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 214901
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/2oE;->a(Landroid/net/Uri;)LX/2oE;

    move-result-object v0

    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 214902
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/video/engine/VideoDataSource;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 214903
    return-void
.end method

.method public final a(Ljava/lang/String;LX/04G;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/04G;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;",
            "Ljava/lang/String;",
            "LX/04D;",
            "Ljava/lang/String;",
            "LX/098;",
            "Ljava/lang/Exception;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214869
    if-nez p9, :cond_0

    .line 214870
    const-string v0, "VideoLoggingUtils"

    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214871
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 214872
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 214873
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 214874
    :cond_0
    const-string v0, "VideoLoggingUtils"

    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, p9, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 214875
    :cond_1
    if-eqz p2, :cond_6

    iget-object v0, p2, LX/04G;->value:Ljava/lang/String;

    .line 214876
    :goto_2
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->URL:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logVideoException"

    invoke-static {p0, v3, p5, v4}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->PRODUCT:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->DEVICE:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->BOARD:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->MANUFACTURER:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->BRAND:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->MODEL:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214877
    if-eqz p10, :cond_7

    .line 214878
    sget-object v2, LX/04F;->STACK_TRACE:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214879
    :cond_2
    :goto_3
    invoke-static {v1, p8}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214880
    invoke-static {v1, p6}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04D;)V

    .line 214881
    invoke-static {p0, v1}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    .line 214882
    sget-object v2, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214883
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214884
    iget-object v1, p0, LX/1C2;->l:LX/1CE;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 214885
    iget-object v2, v1, LX/1CE;->j:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v1, LX/1CE;->j:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v2, v3

    .line 214886
    :goto_4
    iget-object p0, v1, LX/1CE;->k:Ljava/lang/String;

    if-eqz p0, :cond_9

    iget-object p0, v1, LX/1CE;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_9

    .line 214887
    :goto_5
    if-nez v2, :cond_3

    if-eqz v3, :cond_4

    .line 214888
    :cond_3
    sget-object v2, LX/7K6;->ID_CHANGED:LX/7K6;

    invoke-static {v1, v4, v2}, LX/1CE;->a(LX/1CE;ZLX/7K6;)V

    .line 214889
    :cond_4
    iput-object p3, v1, LX/1CE;->j:Ljava/lang/String;

    .line 214890
    iput-object v0, v1, LX/1CE;->k:Ljava/lang/String;

    .line 214891
    iget-object v2, v1, LX/1CE;->g:Ljava/lang/Throwable;

    if-nez v2, :cond_5

    .line 214892
    iput-object p9, v1, LX/1CE;->g:Ljava/lang/Throwable;

    .line 214893
    :cond_5
    iget-object v2, v1, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214894
    invoke-static {v1}, LX/1CE;->a$redex0(LX/1CE;)V

    .line 214895
    return-void

    .line 214896
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 214897
    :cond_7
    if-eqz p9, :cond_2

    .line 214898
    sget-object v2, LX/04F;->STACK_TRACE:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-static {p9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_3

    :cond_8
    move v2, v4

    .line 214899
    goto :goto_4

    :cond_9
    move v3, v4

    .line 214900
    goto :goto_5
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 214868
    iget-object v0, p0, LX/1C2;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 214815
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1C2;->a(Ljava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method public final b()LX/1C2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 214853
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0AA;->VIDEO_PLAYER_SERVICE_UNAVAILABLE:LX/0AA;

    iget-object v1, v1, LX/0AA;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 214854
    invoke-static {p0, v0, v2, v2, v2}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 214850
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_VOLUME_INCREASE:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->CURRENT_VOLUME:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214851
    invoke-static {v1, p8}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214852
    if-eqz p8, :cond_0

    invoke-interface {p8}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p6

    move-object v3, p1

    move-object v5, p7

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;
    .locals 18
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 214846
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p8

    move/from16 v2, p6

    invoke-direct {v0, v1, v2, v3}, LX/1C2;->a(Ljava/lang/String;IZ)Z

    move-result v3

    .line 214847
    if-eqz v3, :cond_0

    .line 214848
    invoke-direct/range {p0 .. p15}, LX/1C2;->c(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    move-result-object v3

    .line 214849
    :goto_0
    return-object v3

    :cond_0
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p7

    invoke-direct/range {v3 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    move-result-object v3

    goto :goto_0
.end method

.method public final b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;
    .locals 7

    .prologue
    .line 214841
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_UNMUTED:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 214842
    iget-boolean v0, p0, LX/1C2;->B:Z

    if-eqz v0, :cond_0

    .line 214843
    sget-object v0, LX/04F;->CURRENT_VOLUME:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-static {p0}, LX/1C2;->e(LX/1C2;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214844
    :cond_0
    invoke-static {v1, p7}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214845
    if-eqz p7, :cond_1

    invoke-interface {p7}, LX/098;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move-object v2, p5

    move-object v3, p1

    move-object v5, p6

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;
    .locals 9

    .prologue
    .line 214830
    if-eqz p6, :cond_0

    sget-object v2, LX/04g;->BY_DEBUG_SILENT:LX/04g;

    iget-object v2, v2, LX/04g;->value:Ljava/lang/String;

    if-ne p6, v2, :cond_0

    .line 214831
    :goto_0
    return-object p0

    .line 214832
    :cond_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/04A;->VIDEO_PAUSE:LX/04A;

    iget-object v3, v3, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/04F;->STREAMING_FORMAT:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->STREAM_TYPE:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p15

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p7

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move/from16 v0, p8

    int-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    const-string v4, "logVideoPausedEvent"

    move-object/from16 v0, p11

    invoke-static {p0, p6, v0, v4}, LX/1C2;->a(LX/1C2;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/04F;->PLAYER_VERSION:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 214833
    invoke-static {v3, p3, p4, p5}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0AR;I)V

    .line 214834
    iget-object v2, p0, LX/1C2;->u:LX/0Ot;

    invoke-static {v3, v2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0Ot;)V

    .line 214835
    iget-boolean v2, p0, LX/1C2;->A:Z

    invoke-static {v3, v2}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V

    .line 214836
    move-object/from16 v0, p14

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/098;)V

    .line 214837
    move-object/from16 v0, p13

    invoke-static {v3, v0}, LX/1C2;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/09M;)V

    .line 214838
    invoke-static {p0, v3}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 214839
    iget-object v2, p0, LX/1C2;->i:LX/1C3;

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/1C3;->a(Ljava/lang/String;)Z

    .line 214840
    if-eqz p14, :cond_1

    invoke-interface/range {p14 .. p14}, LX/098;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v4, p9

    move-object v5, p1

    move-object/from16 v7, p10

    move-object v8, p2

    invoke-static/range {v2 .. v8}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object p0

    goto/16 :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;LX/04D;)LX/1C2;
    .locals 3

    .prologue
    .line 214828
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JK;->VIEWPORT_ROTATED:LX/0JK;

    iget-object v1, v1, LX/0JK;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 214829
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;
    .locals 7

    .prologue
    .line 214826
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/04A;->VIDEO_EXITED_HD:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    int-to-float v2, p5

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p6

    move-object v5, p3

    move-object v6, p4

    .line 214827
    invoke-static/range {v0 .. v6}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;LX/04D;)LX/1C2;
    .locals 3

    .prologue
    .line 214824
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0JK;->VIEWPORT_ZOOMED:LX/0JK;

    iget-object v1, v1, LX/0JK;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 214825
    invoke-static {p0, v0}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    move-result-object v0

    return-object v0
.end method
