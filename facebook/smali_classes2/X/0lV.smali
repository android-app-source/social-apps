.class public final LX/0lV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lW;
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonAutoDetect;
    creatorVisibility = .enum LX/0lX;->ANY:LX/0lX;
    fieldVisibility = .enum LX/0lX;->PUBLIC_ONLY:LX/0lX;
    getterVisibility = .enum LX/0lX;->PUBLIC_ONLY:LX/0lX;
    isGetterVisibility = .enum LX/0lX;->PUBLIC_ONLY:LX/0lX;
    setterVisibility = .enum LX/0lX;->ANY:LX/0lX;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0lW",
        "<",
        "LX/0lV;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/0lV;

.field private static final serialVersionUID:J = -0x622bae41c348ab43L


# instance fields
.field public final _creatorMinLevel:LX/0lX;

.field public final _fieldMinLevel:LX/0lX;

.field public final _getterMinLevel:LX/0lX;

.field public final _isGetterMinLevel:LX/0lX;

.field public final _setterMinLevel:LX/0lX;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 129960
    new-instance v1, LX/0lV;

    const-class v0, LX/0lV;

    const-class v2, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;

    invoke-direct {v1, v0}, LX/0lV;-><init>(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)V

    sput-object v1, LX/0lV;->a:LX/0lV;

    return-void
.end method

.method private constructor <init>(LX/0lX;)V
    .locals 1

    .prologue
    .line 129912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129913
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_0

    .line 129914
    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v0, v0, LX/0lV;->_getterMinLevel:LX/0lX;

    iput-object v0, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    .line 129915
    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v0, v0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    iput-object v0, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    .line 129916
    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v0, v0, LX/0lV;->_setterMinLevel:LX/0lX;

    iput-object v0, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    .line 129917
    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v0, v0, LX/0lV;->_creatorMinLevel:LX/0lX;

    iput-object v0, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    .line 129918
    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v0, v0, LX/0lV;->_fieldMinLevel:LX/0lX;

    iput-object v0, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    .line 129919
    :goto_0
    return-void

    .line 129920
    :cond_0
    iput-object p1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    .line 129921
    iput-object p1, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    .line 129922
    iput-object p1, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    .line 129923
    iput-object p1, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    .line 129924
    iput-object p1, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    goto :goto_0
.end method

.method private constructor <init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V
    .locals 0

    .prologue
    .line 129925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129926
    iput-object p1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    .line 129927
    iput-object p2, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    .line 129928
    iput-object p3, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    .line 129929
    iput-object p4, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    .line 129930
    iput-object p5, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    .line 129931
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)V
    .locals 1

    .prologue
    .line 129932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129933
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->getterVisibility()LX/0lX;

    move-result-object v0

    iput-object v0, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    .line 129934
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->isGetterVisibility()LX/0lX;

    move-result-object v0

    iput-object v0, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    .line 129935
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->setterVisibility()LX/0lX;

    move-result-object v0

    iput-object v0, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    .line 129936
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->creatorVisibility()LX/0lX;

    move-result-object v0

    iput-object v0, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    .line 129937
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->fieldVisibility()LX/0lX;

    move-result-object v0

    iput-object v0, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    .line 129938
    return-void
.end method

.method private a(Ljava/lang/reflect/Field;)Z
    .locals 1

    .prologue
    .line 129939
    iget-object v0, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-virtual {v0, p1}, LX/0lX;->isVisible(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/reflect/Member;)Z
    .locals 1

    .prologue
    .line 129940
    iget-object v0, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    invoke-virtual {v0, p1}, LX/0lX;->isVisible(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/reflect/Method;)Z
    .locals 1

    .prologue
    .line 129941
    iget-object v0, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    invoke-virtual {v0, p1}, LX/0lX;->isVisible(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method private b(LX/0np;LX/0lX;)LX/0lV;
    .locals 2

    .prologue
    .line 129898
    sget-object v0, LX/0nq;->a:[I

    invoke-virtual {p1}, LX/0np;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 129899
    :goto_0
    return-object p0

    .line 129900
    :pswitch_0
    invoke-direct {p0, p2}, LX/0lV;->g(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    .line 129901
    :pswitch_1
    invoke-direct {p0, p2}, LX/0lV;->i(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    .line 129902
    :pswitch_2
    invoke-direct {p0, p2}, LX/0lV;->j(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    .line 129903
    :pswitch_3
    invoke-direct {p0, p2}, LX/0lV;->k(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    .line 129904
    :pswitch_4
    invoke-direct {p0, p2}, LX/0lV;->h(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    .line 129905
    :pswitch_5
    invoke-static {p2}, LX/0lV;->f(LX/0lX;)LX/0lV;

    move-result-object p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private b(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)LX/0lV;
    .locals 2

    .prologue
    .line 129942
    if-eqz p1, :cond_0

    .line 129943
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->getterVisibility()LX/0lX;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0lV;->g(LX/0lX;)LX/0lV;

    move-result-object v0

    .line 129944
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->isGetterVisibility()LX/0lX;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0lV;->h(LX/0lX;)LX/0lV;

    move-result-object v0

    .line 129945
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->setterVisibility()LX/0lX;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0lV;->i(LX/0lX;)LX/0lV;

    move-result-object v0

    .line 129946
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->creatorVisibility()LX/0lX;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0lV;->j(LX/0lX;)LX/0lV;

    move-result-object v0

    .line 129947
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;->fieldVisibility()LX/0lX;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0lV;->k(LX/0lX;)LX/0lV;

    move-result-object p0

    .line 129948
    :cond_0
    return-object p0
.end method

.method private b(Ljava/lang/reflect/Method;)Z
    .locals 1

    .prologue
    .line 129949
    iget-object v0, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    invoke-virtual {v0, p1}, LX/0lX;->isVisible(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/reflect/Method;)Z
    .locals 1

    .prologue
    .line 129950
    iget-object v0, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    invoke-virtual {v0, p1}, LX/0lX;->isVisible(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method private static f(LX/0lX;)LX/0lV;
    .locals 1

    .prologue
    .line 129951
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p0, v0, :cond_0

    .line 129952
    sget-object v0, LX/0lV;->a:LX/0lV;

    .line 129953
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0lV;

    invoke-direct {v0, p0}, LX/0lV;-><init>(LX/0lX;)V

    goto :goto_0
.end method

.method private g(LX/0lX;)LX/0lV;
    .locals 6

    .prologue
    .line 129954
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v1, v0, LX/0lV;->_getterMinLevel:LX/0lX;

    .line 129955
    :goto_0
    iget-object v0, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    if-ne v0, v1, :cond_0

    .line 129956
    :goto_1
    return-object p0

    :cond_0
    new-instance v0, LX/0lV;

    iget-object v2, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    iget-object v3, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    iget-object v4, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    iget-object v5, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-direct/range {v0 .. v5}, LX/0lV;-><init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V

    move-object p0, v0

    goto :goto_1

    :cond_1
    move-object v1, p1

    goto :goto_0
.end method

.method private h(LX/0lX;)LX/0lV;
    .locals 6

    .prologue
    .line 129957
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v2, v0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    .line 129958
    :goto_0
    iget-object v0, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    if-ne v0, v2, :cond_0

    .line 129959
    :goto_1
    return-object p0

    :cond_0
    new-instance v0, LX/0lV;

    iget-object v1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    iget-object v3, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    iget-object v4, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    iget-object v5, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-direct/range {v0 .. v5}, LX/0lV;-><init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V

    move-object p0, v0

    goto :goto_1

    :cond_1
    move-object v2, p1

    goto :goto_0
.end method

.method private i(LX/0lX;)LX/0lV;
    .locals 6

    .prologue
    .line 129906
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v3, v0, LX/0lV;->_setterMinLevel:LX/0lX;

    .line 129907
    :goto_0
    iget-object v0, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    if-ne v0, v3, :cond_0

    .line 129908
    :goto_1
    return-object p0

    :cond_0
    new-instance v0, LX/0lV;

    iget-object v1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    iget-object v2, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    iget-object v4, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    iget-object v5, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-direct/range {v0 .. v5}, LX/0lV;-><init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V

    move-object p0, v0

    goto :goto_1

    :cond_1
    move-object v3, p1

    goto :goto_0
.end method

.method private j(LX/0lX;)LX/0lV;
    .locals 6

    .prologue
    .line 129909
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v4, v0, LX/0lV;->_creatorMinLevel:LX/0lX;

    .line 129910
    :goto_0
    iget-object v0, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    if-ne v0, v4, :cond_0

    .line 129911
    :goto_1
    return-object p0

    :cond_0
    new-instance v0, LX/0lV;

    iget-object v1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    iget-object v2, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    iget-object v3, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    iget-object v5, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-direct/range {v0 .. v5}, LX/0lV;-><init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V

    move-object p0, v0

    goto :goto_1

    :cond_1
    move-object v4, p1

    goto :goto_0
.end method

.method private k(LX/0lX;)LX/0lV;
    .locals 6

    .prologue
    .line 129878
    sget-object v0, LX/0lX;->DEFAULT:LX/0lX;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0lV;->a:LX/0lV;

    iget-object v5, v0, LX/0lV;->_fieldMinLevel:LX/0lX;

    .line 129879
    :goto_0
    iget-object v0, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    if-ne v0, v5, :cond_0

    .line 129880
    :goto_1
    return-object p0

    :cond_0
    new-instance v0, LX/0lV;

    iget-object v1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    iget-object v2, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    iget-object v3, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    iget-object v4, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    invoke-direct/range {v0 .. v5}, LX/0lV;-><init>(LX/0lX;LX/0lX;LX/0lX;LX/0lX;LX/0lX;)V

    move-object p0, v0

    goto :goto_1

    :cond_1
    move-object v5, p1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129881
    invoke-direct {p0, p1}, LX/0lV;->g(LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/0np;LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129882
    invoke-direct {p0, p1, p2}, LX/0lV;->b(LX/0np;LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)LX/0lW;
    .locals 1

    .prologue
    .line 129883
    invoke-direct {p0, p1}, LX/0lV;->b(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Am;)Z
    .locals 1

    .prologue
    .line 129884
    iget-object v0, p1, LX/2Am;->a:Ljava/lang/reflect/Field;

    move-object v0, v0

    .line 129885
    invoke-direct {p0, v0}, LX/0lV;->a(Ljava/lang/reflect/Field;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/2An;)Z
    .locals 1

    .prologue
    .line 129886
    invoke-virtual {p1}, LX/2An;->j()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0lV;->a(Ljava/lang/reflect/Member;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/2At;)Z
    .locals 1

    .prologue
    .line 129887
    iget-object v0, p1, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 129888
    invoke-direct {p0, v0}, LX/0lV;->a(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129889
    invoke-direct {p0, p1}, LX/0lV;->h(LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2At;)Z
    .locals 1

    .prologue
    .line 129890
    iget-object v0, p1, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 129891
    invoke-direct {p0, v0}, LX/0lV;->b(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c(LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129892
    invoke-direct {p0, p1}, LX/0lV;->i(LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/2At;)Z
    .locals 1

    .prologue
    .line 129893
    iget-object v0, p1, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 129894
    invoke-direct {p0, v0}, LX/0lV;->c(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method

.method public final synthetic d(LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129895
    invoke-direct {p0, p1}, LX/0lV;->j(LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(LX/0lX;)LX/0lW;
    .locals 1

    .prologue
    .line 129896
    invoke-direct {p0, p1}, LX/0lV;->k(LX/0lX;)LX/0lV;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129897
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Visibility: getter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lV;->_getterMinLevel:LX/0lX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isGetter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0lV;->_isGetterMinLevel:LX/0lX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", setter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0lV;->_setterMinLevel:LX/0lX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creator: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0lV;->_creatorMinLevel:LX/0lX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", field: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0lV;->_fieldMinLevel:LX/0lX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
