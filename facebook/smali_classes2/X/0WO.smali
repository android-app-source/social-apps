.class public final LX/0WO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Ljava/util/concurrent/Executor;

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 76152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76153
    const/4 v0, 0x0

    iput v0, p0, LX/0WO;->c:I

    .line 76154
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0WO;->a:Landroid/content/Context;

    .line 76155
    return-void
.end method


# virtual methods
.method public final a()LX/0WP;
    .locals 7

    .prologue
    .line 76142
    new-instance v0, LX/0WP;

    iget-object v1, p0, LX/0WO;->b:Ljava/util/concurrent/Executor;

    .line 76143
    if-eqz v1, :cond_0

    :goto_0
    move-object v1, v1

    .line 76144
    iget-object v2, p0, LX/0WO;->a:Landroid/content/Context;

    .line 76145
    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v3

    .line 76146
    if-eqz v3, :cond_1

    .line 76147
    :goto_1
    new-instance v4, Ljava/io/File;

    const-string v5, "light_prefs"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 76148
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 76149
    move-object v2, v4

    .line 76150
    iget v3, p0, LX/0WO;->c:I

    invoke-direct {v0, v1, v2, v3}, LX/0WP;-><init>(Ljava/util/concurrent/Executor;Ljava/io/File;I)V

    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    goto :goto_0

    .line 76151
    :cond_1
    const-string v3, "default"

    goto :goto_1
.end method
