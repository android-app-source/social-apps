.class public final enum LX/1g2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1g2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1g2;

.field public static final enum FEED_UNIT_HEIGHT:LX/1g2;

.field public static final enum FEED_UNIT_SEEN:LX/1g2;

.field public static final enum FEED_UNIT_STORY_MSG_LENGTH:LX/1g2;

.field public static final enum NON_SPONSORED_IMPRESSION:LX/1g2;

.field public static final enum NON_VIEWABILITY_IMPRESSION:LX/1g2;

.field public static final enum ORGANIC_IMPRESSION:LX/1g2;

.field public static final enum SPONSORED_DURATION_IMPRESSION:LX/1g2;

.field public static final enum SPONSORED_FULL_VIEW_IMPRESSION:LX/1g2;

.field public static final enum SPONSORED_IMPRESSION:LX/1g2;

.field public static final enum SPONSORED_PERCENT_UNIT_SEEN:LX/1g2;

.field public static final enum VIEWPORT_DURATION_IMPRESSION:LX/1g2;

.field public static final enum VIEWPORT_IMPRESSION:LX/1g2;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 292704
    new-instance v0, LX/1g2;

    const-string v1, "VIEWPORT_IMPRESSION"

    invoke-direct {v0, v1, v3}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->VIEWPORT_IMPRESSION:LX/1g2;

    .line 292705
    new-instance v0, LX/1g2;

    const-string v1, "VIEWPORT_DURATION_IMPRESSION"

    invoke-direct {v0, v1, v4}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->VIEWPORT_DURATION_IMPRESSION:LX/1g2;

    .line 292706
    new-instance v0, LX/1g2;

    const-string v1, "SPONSORED_DURATION_IMPRESSION"

    invoke-direct {v0, v1, v5}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->SPONSORED_DURATION_IMPRESSION:LX/1g2;

    .line 292707
    new-instance v0, LX/1g2;

    const-string v1, "SPONSORED_IMPRESSION"

    invoke-direct {v0, v1, v6}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->SPONSORED_IMPRESSION:LX/1g2;

    .line 292708
    new-instance v0, LX/1g2;

    const-string v1, "ORGANIC_IMPRESSION"

    invoke-direct {v0, v1, v7}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->ORGANIC_IMPRESSION:LX/1g2;

    .line 292709
    new-instance v0, LX/1g2;

    const-string v1, "NON_VIEWABILITY_IMPRESSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->NON_VIEWABILITY_IMPRESSION:LX/1g2;

    .line 292710
    new-instance v0, LX/1g2;

    const-string v1, "NON_SPONSORED_IMPRESSION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->NON_SPONSORED_IMPRESSION:LX/1g2;

    .line 292711
    new-instance v0, LX/1g2;

    const-string v1, "FEED_UNIT_HEIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->FEED_UNIT_HEIGHT:LX/1g2;

    .line 292712
    new-instance v0, LX/1g2;

    const-string v1, "FEED_UNIT_SEEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->FEED_UNIT_SEEN:LX/1g2;

    .line 292713
    new-instance v0, LX/1g2;

    const-string v1, "FEED_UNIT_STORY_MSG_LENGTH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->FEED_UNIT_STORY_MSG_LENGTH:LX/1g2;

    .line 292714
    new-instance v0, LX/1g2;

    const-string v1, "SPONSORED_PERCENT_UNIT_SEEN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->SPONSORED_PERCENT_UNIT_SEEN:LX/1g2;

    .line 292715
    new-instance v0, LX/1g2;

    const-string v1, "SPONSORED_FULL_VIEW_IMPRESSION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1g2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1g2;->SPONSORED_FULL_VIEW_IMPRESSION:LX/1g2;

    .line 292716
    const/16 v0, 0xc

    new-array v0, v0, [LX/1g2;

    sget-object v1, LX/1g2;->VIEWPORT_IMPRESSION:LX/1g2;

    aput-object v1, v0, v3

    sget-object v1, LX/1g2;->VIEWPORT_DURATION_IMPRESSION:LX/1g2;

    aput-object v1, v0, v4

    sget-object v1, LX/1g2;->SPONSORED_DURATION_IMPRESSION:LX/1g2;

    aput-object v1, v0, v5

    sget-object v1, LX/1g2;->SPONSORED_IMPRESSION:LX/1g2;

    aput-object v1, v0, v6

    sget-object v1, LX/1g2;->ORGANIC_IMPRESSION:LX/1g2;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1g2;->NON_VIEWABILITY_IMPRESSION:LX/1g2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1g2;->NON_SPONSORED_IMPRESSION:LX/1g2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1g2;->FEED_UNIT_HEIGHT:LX/1g2;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1g2;->FEED_UNIT_SEEN:LX/1g2;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1g2;->FEED_UNIT_STORY_MSG_LENGTH:LX/1g2;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1g2;->SPONSORED_PERCENT_UNIT_SEEN:LX/1g2;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1g2;->SPONSORED_FULL_VIEW_IMPRESSION:LX/1g2;

    aput-object v2, v0, v1

    sput-object v0, LX/1g2;->$VALUES:[LX/1g2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 292717
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1g2;
    .locals 1

    .prologue
    .line 292718
    const-class v0, LX/1g2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1g2;

    return-object v0
.end method

.method public static values()[LX/1g2;
    .locals 1

    .prologue
    .line 292719
    sget-object v0, LX/1g2;->$VALUES:[LX/1g2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1g2;

    return-object v0
.end method
