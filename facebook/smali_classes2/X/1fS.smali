.class public final LX/1fS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/1fS;


# instance fields
.field private final b:LX/1fU;

.field private final c:LX/0sO;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2o0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0lp;

.field private f:Ljava/util/concurrent/ScheduledExecutorService;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1se;",
            "LX/2nz",
            "<+",
            "LX/0jT;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1se;",
            "LX/2nz",
            "<+",
            "LX/0jT;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final i:LX/0ad;

.field public final j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final k:LX/0Uo;

.field private final l:LX/03V;

.field public final m:LX/0W3;

.field public n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 291747
    const-class v0, LX/1fS;

    sput-object v0, LX/1fS;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1fU;LX/0sO;LX/0Or;LX/0lp;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0Xl;LX/0Uo;LX/03V;LX/0W3;)V
    .locals 2
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1fU;",
            "LX/0sO;",
            "LX/0Or",
            "<",
            "LX/2o0;",
            ">;",
            "LX/0lp;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0ad;",
            "LX/0Xl;",
            "LX/0Uo;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 291730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291731
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1fS;->g:Ljava/util/Map;

    .line 291732
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1fS;->h:Ljava/util/Map;

    .line 291733
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/1fS;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 291734
    iput-object p1, p0, LX/1fS;->b:LX/1fU;

    .line 291735
    iput-object p2, p0, LX/1fS;->c:LX/0sO;

    .line 291736
    iput-object p3, p0, LX/1fS;->d:LX/0Or;

    .line 291737
    iput-object p4, p0, LX/1fS;->e:LX/0lp;

    .line 291738
    iput-object p6, p0, LX/1fS;->i:LX/0ad;

    .line 291739
    iput-object p5, p0, LX/1fS;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 291740
    iput-object p8, p0, LX/1fS;->k:LX/0Uo;

    .line 291741
    iput-object p9, p0, LX/1fS;->l:LX/03V;

    .line 291742
    iput-object p10, p0, LX/1fS;->m:LX/0W3;

    .line 291743
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/1fS;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 291744
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1fS;->o:Ljava/util/Set;

    .line 291745
    invoke-interface {p7}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p1, LX/1fa;

    invoke-direct {p1, p0}, LX/1fa;-><init>(LX/1fS;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 291746
    return-void
.end method

.method public static a(LX/0QB;)LX/1fS;
    .locals 14

    .prologue
    .line 291717
    sget-object v0, LX/1fS;->p:LX/1fS;

    if-nez v0, :cond_1

    .line 291718
    const-class v1, LX/1fS;

    monitor-enter v1

    .line 291719
    :try_start_0
    sget-object v0, LX/1fS;->p:LX/1fS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291720
    if-eqz v2, :cond_0

    .line 291721
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291722
    new-instance v3, LX/1fS;

    invoke-static {v0}, LX/1fU;->a(LX/0QB;)LX/1fU;

    move-result-object v4

    check-cast v4, LX/1fU;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v5

    check-cast v5, LX/0sO;

    const/16 v6, 0xb20

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v7

    check-cast v7, LX/0lp;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v11

    check-cast v11, LX/0Uo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v13

    check-cast v13, LX/0W3;

    invoke-direct/range {v3 .. v13}, LX/1fS;-><init>(LX/1fU;LX/0sO;LX/0Or;LX/0lp;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/0Xl;LX/0Uo;LX/03V;LX/0W3;)V

    .line 291723
    move-object v0, v3

    .line 291724
    sput-object v0, LX/1fS;->p:LX/1fS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291725
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291726
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291727
    :cond_1
    sget-object v0, LX/1fS;->p:LX/1fS;

    return-object v0

    .line 291728
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291729
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/1se;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291711
    monitor-enter p0

    .line 291712
    :try_start_0
    iget-object v0, p0, LX/1fS;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 291713
    iget-object v1, p0, LX/1fS;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 291714
    monitor-exit p0

    .line 291715
    return-object v0

    .line 291716
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Ljava/lang/String;)LX/1se;
    .locals 2

    .prologue
    .line 291710
    new-instance v0, LX/1se;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/1se;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static c(LX/1fS;)V
    .locals 3

    .prologue
    .line 291706
    iget-object v0, p0, LX/1fS;->b:LX/1fU;

    .line 291707
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 291708
    invoke-direct {p0}, LX/1fS;->b()LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1fU;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 291709
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "Ljava/util/Map",
            "<",
            "LX/0gV",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;>;)",
            "Ljava/util/Map",
            "<",
            "LX/0gV",
            "<TT;>;",
            "LX/2o1",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 291668
    new-instance v6, LX/026;

    const/16 v0, 0x1e

    invoke-direct {v6, v0}, LX/026;-><init>(I)V

    .line 291669
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 291670
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 291671
    iget-object v1, p0, LX/1fS;->k:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291672
    iget-object v2, p0, LX/1fS;->l:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "GraphQLMQTTSubscriptionInBackground-"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gV;

    .line 291673
    iget-object v4, v1, LX/0gW;->f:Ljava/lang/String;

    move-object v1, v4

    .line 291674
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GraphQl Subscription in background after ms: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/1fS;->k:LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->q()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 291675
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gV;

    .line 291676
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0TF;

    .line 291677
    const-string v0, "client_subscription_id"

    sget-object v1, LX/0sO;->b:Ljava/util/concurrent/Callable;

    .line 291678
    iget-object v4, v2, LX/0gW;->e:LX/0w7;

    move-object v4, v4

    .line 291679
    iget-object v5, v2, LX/0gV;->f:Ljava/lang/String;

    move-object v5, v5

    .line 291680
    invoke-static {v4, v5, v0, v1}, LX/0sO;->a(LX/0w7;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 291681
    const-string v0, "graphqlsubscriptions"

    .line 291682
    iget-object v1, v2, LX/0gW;->h:Ljava/lang/String;

    move-object v1, v1

    .line 291683
    iget-object v4, v2, LX/0gV;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v4, v4

    .line 291684
    if-eqz v4, :cond_3

    .line 291685
    iget-boolean v5, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v5, v5

    .line 291686
    if-eqz v5, :cond_3

    .line 291687
    iget-object v5, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v5

    .line 291688
    :goto_1
    move-object v4, v4

    .line 291689
    :try_start_0
    iget-object v5, v2, LX/0gW;->e:LX/0w7;

    move-object v5, v5

    .line 291690
    invoke-static {v5}, LX/0sO;->a(LX/0w7;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 291691
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "/graphql/3/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291692
    invoke-static {v0}, LX/1fS;->b(Ljava/lang/String;)LX/1se;

    move-result-object v1

    .line 291693
    new-instance v0, LX/2nz;

    iget-object v4, p0, LX/1fS;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2o0;

    iget-object v5, p0, LX/1fS;->e:LX/0lp;

    invoke-direct/range {v0 .. v5}, LX/2nz;-><init>(LX/1se;LX/0gV;LX/0TF;LX/2o0;LX/0lp;)V

    .line 291694
    monitor-enter p0

    .line 291695
    :try_start_1
    iget-object v3, p0, LX/1fS;->g:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291696
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291697
    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 291698
    new-instance v1, LX/2o1;

    invoke-direct {v1, v0}, LX/2o1;-><init>(LX/0gM;)V

    invoke-interface {v6, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 291699
    :catch_0
    move-exception v0

    .line 291700
    new-instance v1, LX/31B;

    const-string v3, "Could not build param set"

    invoke-direct {v1, v3, v0}, LX/31B;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291701
    new-instance v0, LX/2o1;

    invoke-direct {v0, v1}, LX/2o1;-><init>(LX/31B;)V

    invoke-interface {v6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 291702
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 291703
    :cond_1
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 291704
    iget-object v0, p0, LX/1fS;->b:LX/1fU;

    invoke-direct {p0}, LX/1fS;->b()LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, LX/1fU;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 291705
    :cond_2
    return-object v6

    :cond_3
    const-string v4, "0"

    goto/16 :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0gM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291604
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 291605
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gM;

    move-object v1, v0

    .line 291606
    check-cast v1, LX/2nz;

    .line 291607
    iget-object v4, v1, LX/2nz;->a:LX/1se;

    move-object v1, v4

    .line 291608
    invoke-interface {v0}, LX/0gM;->c()LX/0gV;

    move-result-object v0

    .line 291609
    iget-object v4, v0, LX/0gW;->b:Ljava/lang/String;

    move-object v4, v4

    .line 291610
    monitor-enter p0

    .line 291611
    :try_start_0
    iget-object v0, p0, LX/1fS;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nz;

    .line 291612
    if-eqz v0, :cond_1

    .line 291613
    iget-object v5, p0, LX/1fS;->i:LX/0ad;

    sget-short v6, LX/2o2;->a:S

    const/4 p1, 0x0

    invoke-interface {v5, v6, p1}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 291614
    if-eqz v5, :cond_2

    .line 291615
    iget-object v7, p0, LX/1fS;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v7

    if-nez v7, :cond_0

    .line 291616
    iget-object v7, p0, LX/1fS;->m:LX/0W3;

    sget-wide v9, LX/0X5;->dE:J

    const-string v8, ""

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 291617
    iget-object v8, p0, LX/1fS;->o:Ljava/util/Set;

    const-string v9, ","

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 291618
    :cond_0
    iget-object v7, p0, LX/1fS;->o:Ljava/util/Set;

    invoke-interface {v7, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    move v4, v7

    .line 291619
    if-eqz v4, :cond_2

    .line 291620
    iget-object v4, p0, LX/1fS;->h:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291621
    iget-object v0, p0, LX/1fS;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291622
    iget-object v0, p0, LX/1fS;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/graphql/mqtt/GraphQLMQTTSubscriptionConnector$1;

    invoke-direct {v1, p0}, Lcom/facebook/graphql/mqtt/GraphQLMQTTSubscriptionConnector$1;-><init>(LX/1fS;)V

    .line 291623
    iget-object v4, p0, LX/1fS;->i:LX/0ad;

    sget v5, LX/2o2;->b:I

    const/16 v6, 0x3c

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    move v4, v4

    .line 291624
    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 291625
    :cond_1
    :goto_1
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 291626
    :cond_2
    :try_start_1
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 291627
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 291628
    iget-object v0, p0, LX/1fS;->b:LX/1fU;

    .line 291629
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 291630
    invoke-virtual {v0, v1, v2}, LX/1fU;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 291631
    :cond_4
    return-void
.end method

.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 8

    .prologue
    .line 291632
    const-string v0, "/graphql"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 291633
    :goto_0
    return-void

    .line 291634
    :cond_0
    monitor-enter p0

    .line 291635
    :try_start_0
    iget-object v0, p0, LX/1fS;->g:Ljava/util/Map;

    invoke-static {p1}, LX/1fS;->b(Ljava/lang/String;)LX/1se;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nz;

    .line 291636
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291637
    if-nez v0, :cond_1

    .line 291638
    sget-object v0, LX/1fS;->a:Ljava/lang/Class;

    const-string v1, "GraphQL Subscription over MQTT got unexpected payload on pattern %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 291639
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 291640
    :cond_1
    iget-object v1, v0, LX/2nz;->d:LX/2o0;

    move-object v1, v1

    .line 291641
    const/4 p4, 0x2

    const/4 p3, 0x1

    const p1, 0x590012

    .line 291642
    sget-object v2, LX/2o0;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v3

    .line 291643
    iget-object v2, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, p1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 291644
    iget-object v2, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "byte_array_payload_size"

    array-length v5, p2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, p1, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 291645
    invoke-interface {v0}, LX/0gM;->d()LX/0TF;

    move-result-object v4

    .line 291646
    :try_start_2
    invoke-interface {v0}, LX/0gM;->c()LX/0gV;

    move-result-object v2

    .line 291647
    iget-object v5, v1, LX/2o0;->c:LX/0lp;

    invoke-virtual {v5, p2}, LX/0lp;->a([B)LX/15w;

    move-result-object v5

    .line 291648
    iget-object v6, v1, LX/2o0;->b:LX/0lC;

    invoke-virtual {v5, v6}, LX/15w;->a(LX/0lD;)V

    .line 291649
    iget-object v6, v1, LX/2o0;->d:LX/0sO;

    .line 291650
    iget-object v7, v2, LX/0gW;->f:Ljava/lang/String;

    move-object v7, v7

    .line 291651
    const/4 p0, 0x1

    invoke-virtual {v6, v7, p0, v5}, LX/0sO;->a(Ljava/lang/String;ILX/15w;)LX/15w;

    .line 291652
    iget-object v6, v2, LX/0gW;->a:LX/0w5;

    move-object v2, v6

    .line 291653
    invoke-virtual {v2, v5}, LX/0w5;->a(LX/15w;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jT;

    .line 291654
    iget-object v5, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x590012

    const/16 v7, 0xb1

    invoke-interface {v5, v6, v3, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 291655
    iget-object v5, v1, LX/2o0;->f:LX/1fb;

    .line 291656
    const-string v6, "graphql_subscriptions_receive"

    invoke-static {v5, v6, v0}, LX/1fb;->a(LX/1fb;Ljava/lang/String;LX/0gM;)V

    .line 291657
    iget-object v5, v1, LX/2o0;->e:LX/0ti;

    invoke-virtual {v5, v2}, LX/0ti;->a(LX/0jT;)V

    .line 291658
    iget-object v5, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x590012

    const/16 v7, 0x13

    invoke-interface {v5, v6, v3, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 291659
    iget-object v5, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x590012

    const/4 v7, 0x2

    invoke-interface {v5, v6, v3, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 291660
    invoke-interface {v4, v2}, LX/0TF;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 291661
    :goto_1
    goto/16 :goto_0

    .line 291662
    :catch_0
    move-exception v2

    .line 291663
    iget-object v5, v1, LX/2o0;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x3

    invoke-interface {v5, p1, v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 291664
    sget-object v3, LX/2o0;->a:Ljava/lang/Class;

    const-string v5, "GraphQL Subscription payload received but handling failed for query %s using %s"

    new-array v6, p4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v0}, LX/0gM;->c()LX/0gV;

    move-result-object p0

    .line 291665
    iget-object p2, p0, LX/0gW;->f:Ljava/lang/String;

    move-object p0, p2

    .line 291666
    aput-object p0, v6, v7

    invoke-interface {v0}, LX/0gM;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, p3

    invoke-static {v3, v2, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291667
    invoke-interface {v4, v2}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
