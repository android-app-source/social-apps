.class public final enum LX/0pD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0pD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0pD;

.field public static final enum HIGH:LX/0pD;

.field public static final enum NORMAL:LX/0pD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143987
    new-instance v0, LX/0pD;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/0pD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0pD;->NORMAL:LX/0pD;

    .line 143988
    new-instance v0, LX/0pD;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v3}, LX/0pD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0pD;->HIGH:LX/0pD;

    .line 143989
    const/4 v0, 0x2

    new-array v0, v0, [LX/0pD;

    sget-object v1, LX/0pD;->NORMAL:LX/0pD;

    aput-object v1, v0, v2

    sget-object v1, LX/0pD;->HIGH:LX/0pD;

    aput-object v1, v0, v3

    sput-object v0, LX/0pD;->$VALUES:[LX/0pD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 143990
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0pD;
    .locals 1

    .prologue
    .line 143991
    const-class v0, LX/0pD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0pD;

    return-object v0
.end method

.method public static values()[LX/0pD;
    .locals 1

    .prologue
    .line 143992
    sget-object v0, LX/0pD;->$VALUES:[LX/0pD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0pD;

    return-object v0
.end method
