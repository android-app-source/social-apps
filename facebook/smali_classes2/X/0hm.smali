.class public LX/0hm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/os/Handler;

.field public h:LX/0f7;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 118324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118325
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 118326
    iput-object v0, p0, LX/0hm;->c:LX/0Ot;

    .line 118327
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 118328
    iput-object v0, p0, LX/0hm;->d:LX/0Ot;

    .line 118329
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 118330
    iput-object v0, p0, LX/0hm;->e:LX/0Ot;

    .line 118331
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 118332
    iput-object v0, p0, LX/0hm;->f:LX/0Ot;

    .line 118333
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/0hm;->g:Landroid/os/Handler;

    .line 118334
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 118355
    iget-object v0, p0, LX/0hm;->h:LX/0f7;

    if-nez v0, :cond_1

    .line 118356
    :cond_0
    :goto_0
    return-void

    .line 118357
    :cond_1
    iget-object v0, p0, LX/0hm;->h:LX/0f7;

    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    .line 118358
    if-eqz v0, :cond_0

    .line 118359
    iget-object v0, p0, LX/0hm;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 118360
    sget-object v1, LX/2ns;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 118361
    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object p0, LX/2ns;->b:LX/0Tn;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2, p0, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 118362
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 118352
    instance-of v0, p1, LX/0f7;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 118353
    check-cast p1, LX/0f7;

    iput-object p1, p0, LX/0hm;->h:LX/0f7;

    .line 118354
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 118351
    iget-object v0, p0, LX/0hm;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 118338
    invoke-direct {p0}, LX/0hm;->f()V

    .line 118339
    iget-object v2, p0, LX/0hm;->h:LX/0f7;

    if-nez v2, :cond_2

    .line 118340
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0hm;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 118341
    iget-object v0, p0, LX/0hm;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    invoke-virtual {v0}, LX/0hn;->e()V

    .line 118342
    :cond_1
    return-void

    .line 118343
    :cond_2
    iget-object v2, p0, LX/0hm;->h:LX/0f7;

    sget-object v3, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v3

    .line 118344
    if-eqz v3, :cond_0

    .line 118345
    iget-object v2, p0, LX/0hm;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iA;

    .line 118346
    const-string v4, "4111"

    const-class v5, LX/2nt;

    invoke-virtual {v2, v4, v5}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/2nt;

    .line 118347
    if-eqz v2, :cond_0

    .line 118348
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v2, LX/2nt;->c:Ljava/lang/ref/WeakReference;

    .line 118349
    new-instance v2, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentVideoHomeController$1;

    invoke-direct {v2, p0}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentVideoHomeController$1;-><init>(LX/0hm;)V

    .line 118350
    iget-object v3, p0, LX/0hm;->g:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    const v6, -0x6273dea6

    invoke-static {v3, v2, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 118335
    iget-object v0, p0, LX/0hm;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118336
    iget-object v0, p0, LX/0hm;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    invoke-virtual {v0}, LX/0hn;->f()V

    .line 118337
    :cond_0
    return-void
.end method
