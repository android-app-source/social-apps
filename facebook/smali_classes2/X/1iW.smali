.class public LX/1iW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UsingDefaultJsonDeserializer"
    }
.end annotation


# instance fields
.field public a:J

.field public b:LX/03R;

.field public final bytesReadByApp:LX/1iX;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bytes_read_by_app"
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public d:LX/03R;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:LX/2BD;

.field public final requestBodyBytes:LX/1iX;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "request_body"
    .end annotation
.end field

.field public final requestHeaderBytes:LX/1iX;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "request_header"
    .end annotation
.end field

.field public final responseBodyBytes:LX/1iX;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "response_body"
    .end annotation
.end field

.field public final responseHeaderBytes:LX/1iX;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "response_header"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0p7;LX/0So;LX/2BD;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298469
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/1iW;->b:LX/03R;

    .line 298470
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/1iW;->d:LX/03R;

    .line 298471
    iput-object v1, p0, LX/1iW;->e:Ljava/lang/String;

    .line 298472
    iput-object v1, p0, LX/1iW;->f:Ljava/lang/String;

    .line 298473
    iput-object v1, p0, LX/1iW;->g:Ljava/lang/String;

    .line 298474
    iput-object v1, p0, LX/1iW;->h:Ljava/lang/String;

    .line 298475
    iput-object v1, p0, LX/1iW;->i:Ljava/lang/String;

    .line 298476
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1iW;->j:Z

    .line 298477
    iput-object v1, p0, LX/1iW;->k:LX/2BD;

    .line 298478
    new-instance v0, LX/1iX;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1iX;-><init>(LX/0am;)V

    iput-object v0, p0, LX/1iW;->bytesReadByApp:LX/1iX;

    .line 298479
    new-instance v0, LX/1iX;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1iX;-><init>(LX/0am;)V

    iput-object v0, p0, LX/1iW;->requestBodyBytes:LX/1iX;

    .line 298480
    new-instance v0, LX/1iX;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1iX;-><init>(LX/0am;)V

    iput-object v0, p0, LX/1iW;->requestHeaderBytes:LX/1iX;

    .line 298481
    new-instance v0, LX/1iX;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1iX;-><init>(LX/0am;)V

    iput-object v0, p0, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 298482
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1iW;->c:Ljava/lang/String;

    .line 298483
    new-instance v0, LX/1iX;

    new-instance v1, LX/1hg;

    invoke-direct {v1, p2, p3}, LX/1hg;-><init>(LX/0p7;LX/0So;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1iX;-><init>(LX/0am;)V

    iput-object v0, p0, LX/1iW;->responseBodyBytes:LX/1iX;

    .line 298484
    iput-object p4, p0, LX/1iW;->k:LX/2BD;

    .line 298485
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 298466
    iput-object p1, p0, LX/1iW;->e:Ljava/lang/String;

    .line 298467
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 298449
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, LX/1iW;->b:LX/03R;

    .line 298450
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 298464
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/1iW;->d:LX/03R;

    .line 298465
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298463
    iget-object v0, p0, LX/1iW;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298462
    iget-object v0, p0, LX/1iW;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298461
    iget-object v0, p0, LX/1iW;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 298458
    iget-object v0, p0, LX/1iW;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298459
    const/4 v0, 0x0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/1iW;->d:LX/03R;

    .line 298460
    :cond_0
    return-void
.end method

.method public final i()J
    .locals 6

    .prologue
    .line 298453
    iget-object v0, p0, LX/1iW;->requestHeaderBytes:LX/1iX;

    .line 298454
    iget-wide v4, v0, LX/1iX;->a:J

    move-wide v0, v4

    .line 298455
    iget-object v2, p0, LX/1iW;->requestBodyBytes:LX/1iX;

    .line 298456
    iget-wide v4, v2, LX/1iX;->a:J

    move-wide v2, v4

    .line 298457
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298452
    iget-object v0, p0, LX/1iW;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 298451
    iget-wide v0, p0, LX/1iW;->a:J

    return-wide v0
.end method
