.class public LX/1nD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/1nD;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field public final f:LX/0Uh;

.field public final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 315284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/16 v6, 0xd

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v9

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v10

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v11

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v12

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v13

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->TRENDING_FINITE_SERP_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/16 v7, 0xb

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1nD;->a:LX/0Rf;

    .line 315285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/4 v6, 0x6

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v9

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v10

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v11

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v12

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v7, v6, v13

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1nD;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0ad;LX/0Uh;)V
    .locals 5
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/search/abtest/gk/TrendingNewsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 315292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315293
    iput-object p1, p0, LX/1nD;->c:LX/0Or;

    .line 315294
    iput-object p2, p0, LX/1nD;->d:LX/0Or;

    .line 315295
    iput-object p3, p0, LX/1nD;->e:LX/0ad;

    .line 315296
    iput-object p4, p0, LX/1nD;->f:LX/0Uh;

    .line 315297
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 315298
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v0, p0, LX/1nD;->e:LX/0ad;

    const-string v3, "content"

    invoke-static {v0, v3}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315299
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v0, p0, LX/1nD;->e:LX/0ad;

    const-string v3, "content"

    invoke-static {v0, v3}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_ENTITIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315303
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_ENTITIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_ENTITIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315305
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_ENTITIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315306
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v0, p0, LX/1nD;->e:LX/0ad;

    sget-short v3, LX/100;->H:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315307
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_PLACES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_PANDORA_PHOTOS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_PANDORA_PHOTOS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315310
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315311
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315313
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315315
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 315316
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/1nD;->g:LX/0P1;

    .line 315317
    return-void

    .line 315318
    :cond_0
    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 315319
    :cond_1
    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    goto/16 :goto_1

    .line 315320
    :cond_2
    sget-object v0, LX/0cQ;->SEARCH_RESULTS_ENTITIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)LX/1nD;
    .locals 7

    .prologue
    .line 315321
    sget-object v0, LX/1nD;->h:LX/1nD;

    if-nez v0, :cond_1

    .line 315322
    const-class v1, LX/1nD;

    monitor-enter v1

    .line 315323
    :try_start_0
    sget-object v0, LX/1nD;->h:LX/1nD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 315324
    if-eqz v2, :cond_0

    .line 315325
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 315326
    new-instance v5, LX/1nD;

    const/16 v3, 0xc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x156c

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v6, p0, v3, v4}, LX/1nD;-><init>(LX/0Or;LX/0Or;LX/0ad;LX/0Uh;)V

    .line 315327
    move-object v0, v5

    .line 315328
    sput-object v0, LX/1nD;->h:LX/1nD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315329
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 315330
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 315331
    :cond_1
    sget-object v0, LX/1nD;->h:LX/1nD;

    return-object v0

    .line 315332
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 315333
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 315334
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/1nD;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "target_fragment"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "typeahead_session_id"

    iget-object v3, p2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "candidate_session_id"

    iget-object v3, p2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "display_style"

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_title"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_function"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "query_vertical"

    invoke-virtual {v0, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "graph_search_keyword_type"

    invoke-virtual {v0, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "source"

    if-eqz p8, :cond_0

    invoke-virtual {p8}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "exact_match"

    if-eqz p9, :cond_2

    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/1nD;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;
    .locals 8
    .param p2    # LX/8ci;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 315335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0, p1}, LX/7BG;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 315336
    const-string v3, "content"

    if-nez p4, :cond_0

    sget-object v5, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    :goto_0
    const/4 v6, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    .line 315337
    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315338
    return-object v0

    :cond_0
    move-object v5, p4

    .line 315339
    goto :goto_0
.end method

.method private static a(LX/1nD;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 315340
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/1nD;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 315341
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_DISCOVERY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315342
    const-string v1, "extra_events_discovery_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315343
    const-string v1, "extra_events_discovery_subtitle"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315344
    const-string v1, "extra_is_from_search"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315345
    const-string v1, "extra_search_location"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315346
    const-string v1, "extra_is_nearby_search"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315347
    const-string v1, "extra_search_time_range"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315348
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 315349
    const-string v0, "target_fragment"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 315350
    sget-object v1, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z
    .locals 1

    .prologue
    .line 315351
    sget-object v0, LX/1nD;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static b(LX/1nD;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)I
    .locals 3

    .prologue
    .line 315352
    sget-object v0, LX/1nD;->b:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315353
    iget-object v0, p0, LX/1nD;->g:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 315354
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1nD;->e:LX/0ad;

    sget-short v1, LX/100;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/1nD;->g:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;
    .locals 10
    .param p6    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 315355
    invoke-static {p1}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315356
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported display style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 315357
    :cond_0
    invoke-static {p0, p1}, LX/1nD;->b(LX/1nD;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)I

    move-result v1

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object/from16 v2, p8

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v8, p5

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    .line 315358
    if-eqz p6, :cond_1

    .line 315359
    const-string v1, "results_query_role"

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315360
    :cond_1
    const-string v1, "results_query_type"

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315361
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/0Px;LX/0Px;Z)Landroid/content/Intent;
    .locals 3
    .param p6    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 315286
    invoke-virtual/range {p0 .. p8}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "preloaded_story_ids"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p10, v0}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "open_filter_dialog"

    invoke-virtual {v0, v1, p11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 315287
    const-string v1, "filters"

    invoke-static {v0, v1, p9}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 315288
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315289
    invoke-static {p1}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315290
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported display style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 315291
    :cond_0
    invoke-static {p0, p1}, LX/1nD;->b(LX/1nD;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)I

    move-result v1

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object/from16 v2, p6

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v8, p5

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/0Px;)Landroid/content/Intent;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 315258
    sget-object v1, LX/0cQ;->SEARCH_RESULTS_EXPLORE_HOST_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v2

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object/from16 v3, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v9, p5

    invoke-static/range {v1 .. v10}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v1

    .line 315259
    const-string v2, "graph_search_explore_tabs"

    move-object/from16 v0, p7

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 315260
    return-object v1
.end method

.method public final a(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 315261
    iget-object v0, p0, LX/1nD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 315262
    if-eqz v0, :cond_0

    .line 315263
    const-string v0, "keywords_topic_trending(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 315264
    :goto_0
    const-string v3, "news_v2"

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v0, p0

    move-object v1, p3

    move-object v4, p4

    move-object v5, p1

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 315265
    :cond_0
    const-string v0, "stories-topic(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 315266
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/Boolean;LX/103;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "LX/8ci;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "Ljava/lang/Boolean;",
            "LX/103;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 315267
    sget-object v1, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/4 v8, 0x0

    move-object v1, p0

    move-object/from16 v3, p6

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p7

    invoke-static/range {v1 .. v10}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "graph_search_scoped_entity_type"

    move-object/from16 v0, p8

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "graph_search_scoped_entity_id"

    move-object/from16 v0, p9

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "graph_search_scoped_entity_name"

    move-object/from16 v0, p10

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "graph_search_query_modifiers"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315268
    if-eqz p10, :cond_0

    iget-object v0, p0, LX/1nD;->e:LX/0ad;

    sget-short v1, LX/100;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 315269
    :goto_0
    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v4, p9

    move/from16 v5, p11

    .line 315270
    invoke-static/range {v0 .. v5}, LX/1nD;->a(LX/1nD;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 315271
    :goto_1
    return-object v0

    .line 315272
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 315273
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {p0, v0}, LX/1nD;->b(LX/1nD;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)I

    move-result v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object/from16 v2, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v8, p4

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "results_query_role"

    invoke-virtual {p5}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/Boolean;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315274
    sget-object v0, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p5

    move-object/from16 v3, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v8, p4

    move-object/from16 v9, p7

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/Boolean;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315275
    sget-object v0, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v1

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v2, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    move-object/from16 v9, p7

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/search/logging/api/SearchTypeaheadSession;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 315276
    invoke-static {p0, p2, p5, p6, p7}, LX/1nD;->a(LX/1nD;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "place_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ranking_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315277
    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315278
    const-string v0, "keywords_global_share(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v2, v4

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 315279
    const-string v6, "content"

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move-object v8, p4

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/search/logging/api/SearchTypeaheadSession;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 315280
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 315281
    sget-object v0, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315282
    const-string v0, "keywords_pulse_topic(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v2, v4

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 315283
    const-string v6, "content"

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move-object v8, p4

    invoke-static/range {v0 .. v9}, LX/1nD;->a(LX/1nD;ILcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
