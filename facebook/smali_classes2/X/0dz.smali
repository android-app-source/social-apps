.class public LX/0dz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;
.implements LX/0dN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile r:LX/0dz;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qw;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0W9;

.field private final e:LX/03V;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Vt;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0TD;

.field private final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0e5;

.field public final l:LX/0e8;

.field private final m:LX/0WJ;

.field private final n:LX/0Uo;

.field private final o:Landroid/os/Handler;

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91191
    const-class v0, LX/0dz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0dz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0W9;LX/03V;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0TD;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0e5;LX/0e8;LX/0WJ;LX/0Uo;)V
    .locals 2
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1qw;",
            ">;",
            "LX/0W9;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0Vt;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0TD;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0e5;",
            "LX/0e8;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91240
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0dz;->o:Landroid/os/Handler;

    .line 91241
    const/4 v0, 0x0

    iput-object v0, p0, LX/0dz;->p:Ljava/lang/String;

    .line 91242
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0dz;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 91243
    iput-object p1, p0, LX/0dz;->b:Landroid/content/Context;

    .line 91244
    iput-object p2, p0, LX/0dz;->c:LX/0Ot;

    .line 91245
    iput-object p3, p0, LX/0dz;->d:LX/0W9;

    .line 91246
    iput-object p4, p0, LX/0dz;->e:LX/03V;

    .line 91247
    iput-object p5, p0, LX/0dz;->f:LX/0Ot;

    .line 91248
    iput-object p6, p0, LX/0dz;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 91249
    iput-object p7, p0, LX/0dz;->h:LX/0TD;

    .line 91250
    iput-object p8, p0, LX/0dz;->i:Lcom/facebook/content/SecureContextHelper;

    .line 91251
    iput-object p9, p0, LX/0dz;->j:LX/0Or;

    .line 91252
    iput-object p10, p0, LX/0dz;->k:LX/0e5;

    .line 91253
    iput-object p11, p0, LX/0dz;->l:LX/0e8;

    .line 91254
    iput-object p12, p0, LX/0dz;->m:LX/0WJ;

    .line 91255
    iput-object p13, p0, LX/0dz;->n:LX/0Uo;

    .line 91256
    iget-object v0, p0, LX/0dz;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eC;->b:LX/0Tn;

    invoke-interface {v0, v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 91257
    return-void
.end method

.method public static a(LX/0QB;)LX/0dz;
    .locals 3

    .prologue
    .line 91229
    sget-object v0, LX/0dz;->r:LX/0dz;

    if-nez v0, :cond_1

    .line 91230
    const-class v1, LX/0dz;

    monitor-enter v1

    .line 91231
    :try_start_0
    sget-object v0, LX/0dz;->r:LX/0dz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91232
    if-eqz v2, :cond_0

    .line 91233
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0dz;->b(LX/0QB;)LX/0dz;

    move-result-object v0

    sput-object v0, LX/0dz;->r:LX/0dz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91234
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91235
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91236
    :cond_1
    sget-object v0, LX/0dz;->r:LX/0dz;

    return-object v0

    .line 91237
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0dz;
    .locals 14

    .prologue
    .line 91224
    new-instance v0, LX/0dz;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xc6b

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x10c5

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0xb83

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    .line 91225
    new-instance v12, LX/0e5;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v11

    check-cast v11, Landroid/telephony/TelephonyManager;

    invoke-direct {v12, v10, v11}, LX/0e5;-><init>(LX/0W9;Landroid/telephony/TelephonyManager;)V

    .line 91226
    move-object v10, v12

    .line 91227
    check-cast v10, LX/0e5;

    invoke-static {p0}, LX/0e8;->a(LX/0QB;)LX/0e8;

    move-result-object v11

    check-cast v11, LX/0e8;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v12

    check-cast v12, LX/0WJ;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v13

    check-cast v13, LX/0Uo;

    invoke-direct/range {v0 .. v13}, LX/0dz;-><init>(Landroid/content/Context;LX/0Ot;LX/0W9;LX/03V;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0TD;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0e5;LX/0e8;LX/0WJ;LX/0Uo;)V

    .line 91228
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 91221
    iget-object v0, p0, LX/0dz;->l:LX/0e8;

    .line 91222
    invoke-virtual {v0}, LX/0e8;->c()LX/0Py;

    move-result-object v1

    iget-object p0, v0, LX/0e8;->a:LX/0W9;

    invoke-virtual {p0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/0e8;->a(LX/0Py;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 91223
    return-object v0
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 0

    .prologue
    .line 91219
    invoke-virtual {p0}, LX/0dz;->e()V

    .line 91220
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91216
    iget-object v0, p0, LX/0dz;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0eC;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 91217
    invoke-virtual {p0}, LX/0dz;->e()V

    .line 91218
    return-void
.end method

.method public final c()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91215
    iget-object v0, p0, LX/0dz;->l:LX/0e8;

    invoke-virtual {v0}, LX/0e8;->c()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 91214
    iget-object v0, p0, LX/0dz;->h:LX/0TD;

    new-instance v1, Lcom/facebook/languages/switcher/LanguageSwitcher$1;

    invoke-direct {v1, p0}, Lcom/facebook/languages/switcher/LanguageSwitcher$1;-><init>(LX/0dz;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 91194
    iget-object v0, p0, LX/0dz;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91195
    iget-object v0, p0, LX/0dz;->e:LX/03V;

    sget-object v1, LX/0dz;->a:Ljava/lang/String;

    const-string v2, "LanguageSwitcher.updateAppLocale called before shared prefs initialized."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91196
    :cond_0
    :goto_0
    return-void

    .line 91197
    :cond_1
    iget-object v0, p0, LX/0dz;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    .line 91198
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91199
    iget-object v0, p0, LX/0dz;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    .line 91200
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91201
    iget-object v0, p0, LX/0dz;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vt;

    invoke-virtual {v0, v1}, LX/0Vt;->a(Ljava/util/Locale;)V

    .line 91202
    iget-object v0, p0, LX/0dz;->l:LX/0e8;

    invoke-virtual {v0, v1}, LX/0e8;->a(Ljava/util/Locale;)V

    .line 91203
    iget-object v0, p0, LX/0dz;->e:LX/03V;

    const-string v1, "app_locale"

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 91204
    iget-object v0, p0, LX/0dz;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0dz;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91205
    iget-object v0, p0, LX/0dz;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qw;

    .line 91206
    iget-object v1, v0, LX/1qw;->b:LX/0eF;

    invoke-interface {v1}, LX/0eF;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, LX/1qw;->a(LX/1qw;Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 91207
    iput-object v0, p0, LX/0dz;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 91208
    iget-object v0, p0, LX/0dz;->m:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91209
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/0dz;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/languages/switcher/LanguageSwitchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91210
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 91211
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 91212
    iget-object v1, p0, LX/0dz;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/0dz;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91213
    :cond_2
    iput-object v3, p0, LX/0dz;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 91192
    invoke-virtual {p0}, LX/0dz;->e()V

    .line 91193
    return-void
.end method
