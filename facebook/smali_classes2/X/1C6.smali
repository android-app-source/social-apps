.class public abstract LX/1C6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/04A;",
            "Ljava/util/Set",
            "<",
            "LX/04A;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0So;

.field private final b:J

.field private c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/7KB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 215386
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 215387
    sput-object v0, LX/1C6;->d:Ljava/util/Map;

    const/4 v1, 0x0

    sget-object v2, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215388
    sget-object v0, LX/1C6;->d:Ljava/util/Map;

    sget-object v1, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    sget-object v2, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    sget-object v3, LX/04A;->VIDEO_START:LX/04A;

    sget-object v4, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    invoke-static {v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215389
    sget-object v0, LX/1C6;->d:Ljava/util/Map;

    sget-object v1, LX/04A;->VIDEO_START:LX/04A;

    sget-object v2, LX/04A;->VIDEO_PAUSE:LX/04A;

    sget-object v3, LX/04A;->VIDEO_COMPLETE:LX/04A;

    invoke-static {v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215390
    sget-object v0, LX/1C6;->d:Ljava/util/Map;

    sget-object v1, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    sget-object v2, LX/04A;->VIDEO_PAUSE:LX/04A;

    sget-object v3, LX/04A;->VIDEO_COMPLETE:LX/04A;

    invoke-static {v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215391
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 215392
    sget-object v1, LX/1C6;->d:Ljava/util/Map;

    sget-object v2, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215393
    sget-object v1, LX/1C6;->d:Ljava/util/Map;

    sget-object v2, LX/04A;->VIDEO_PAUSE:LX/04A;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215394
    sget-object v1, LX/1C6;->d:Ljava/util/Map;

    sget-object v2, LX/04A;->VIDEO_COMPLETE:LX/04A;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215395
    return-void
.end method

.method public constructor <init>(LX/0So;JI)V
    .locals 2

    .prologue
    .line 215396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215397
    iput-object p1, p0, LX/1C6;->a:LX/0So;

    .line 215398
    new-instance v0, LX/0aq;

    invoke-direct {v0, p4}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1C6;->c:LX/0aq;

    .line 215399
    iput-wide p2, p0, LX/1C6;->b:J

    .line 215400
    return-void
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/7KA;
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 215401
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 215402
    invoke-static {v0}, LX/04A;->asEvent(Ljava/lang/String;)LX/04A;

    move-result-object v1

    .line 215403
    sget-object v0, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215404
    sget-object v2, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 215405
    sget-object v3, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v3, v3, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 215406
    sget-object v4, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 215407
    sget-object v4, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v4, v4, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 215408
    if-nez v3, :cond_0

    const-wide/16 v4, -0x1

    .line 215409
    :goto_0
    if-nez v0, :cond_1

    move-object v7, v6

    .line 215410
    :goto_1
    if-nez v2, :cond_2

    .line 215411
    :goto_2
    new-instance v0, LX/7KA;

    iget-object v2, p0, LX/1C6;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct/range {v0 .. v9}, LX/7KA;-><init>(LX/04A;JJLX/04g;LX/04g;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 215412
    :cond_0
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v10

    double-to-long v4, v4

    goto :goto_0

    .line 215413
    :cond_1
    invoke-static {v0}, LX/04g;->asEventTriggerType(Ljava/lang/String;)LX/04g;

    move-result-object v7

    goto :goto_1

    .line 215414
    :cond_2
    invoke-static {v2}, LX/04g;->asEventTriggerType(Ljava/lang/String;)LX/04g;

    move-result-object v6

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/7KB;
    .locals 6

    .prologue
    .line 215415
    iget-object v0, p0, LX/1C6;->c:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7KB;

    .line 215416
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/1C6;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, v0, LX/7KB;->b:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, LX/1C6;->b:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 215417
    iget-object v0, p0, LX/1C6;->c:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215418
    const/4 v0, 0x0

    .line 215419
    :cond_0
    if-nez v0, :cond_1

    .line 215420
    new-instance v0, LX/7KB;

    iget-object v1, p0, LX/1C6;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, LX/7KB;-><init>(Ljava/lang/String;J)V

    .line 215421
    iget-object v1, p0, LX/1C6;->c:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215422
    :cond_1
    return-object v0
.end method

.method public abstract a(LX/7KB;)V
.end method

.method public a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 4

    .prologue
    .line 215423
    invoke-virtual {p0}, LX/1C6;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 215424
    :cond_0
    :goto_0
    return-void

    .line 215425
    :cond_1
    sget-object v0, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215426
    if-eqz v0, :cond_0

    .line 215427
    invoke-virtual {p0, v0}, LX/1C6;->a(Ljava/lang/String;)LX/7KB;

    move-result-object v1

    .line 215428
    invoke-direct {p0, p1}, LX/1C6;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/7KA;

    move-result-object v2

    .line 215429
    iget-object v3, v1, LX/7KB;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215430
    sget-object v3, LX/1C6;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    iget-object p1, v2, LX/7KA;->a:LX/04A;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    move v3, v3

    .line 215431
    if-nez v3, :cond_3

    .line 215432
    :goto_1
    iget-boolean v2, v1, LX/7KB;->d:Z

    move v2, v2

    .line 215433
    if-nez v2, :cond_2

    .line 215434
    invoke-virtual {p0, v1}, LX/1C6;->a(LX/7KB;)V

    .line 215435
    :cond_2
    iget-boolean v2, v1, LX/7KB;->e:Z

    move v1, v2

    .line 215436
    if-eqz v1, :cond_0

    .line 215437
    iget-object v1, p0, LX/1C6;->c:LX/0aq;

    invoke-virtual {v1, v0}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 215438
    :cond_3
    sget-object v3, LX/1C6;->d:Ljava/util/Map;

    iget-object p1, v1, LX/7KB;->f:LX/04A;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 215439
    iget-object p1, v2, LX/7KA;->a:LX/04A;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    move v3, v3

    .line 215440
    iput-boolean v3, v1, LX/7KB;->d:Z

    .line 215441
    iget-object v3, v2, LX/7KA;->a:LX/04A;

    iput-object v3, v1, LX/7KB;->f:LX/04A;

    .line 215442
    sget-object v3, LX/1C6;->d:Ljava/util/Map;

    iget-object p1, v2, LX/7KA;->a:LX/04A;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    move v3, v3

    .line 215443
    iput-boolean v3, v1, LX/7KB;->e:Z

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 215444
    const/4 v0, 0x0

    return v0
.end method
