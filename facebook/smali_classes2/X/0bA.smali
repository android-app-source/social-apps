.class public final LX/0bA;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;",
        "LX/ALu;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/ALu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86736
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86737
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86745
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 86738
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;

    check-cast p2, LX/ALu;

    .line 86739
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86740
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v0, v1

    .line 86741
    if-nez v0, :cond_1

    .line 86742
    :cond_0
    :goto_0
    return-void

    .line 86743
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86744
    iget-object v1, p2, LX/ALu;->a:LX/AKO;

    invoke-virtual {v1, v0}, LX/AKO;->a(Lcom/facebook/audience/model/UploadShot;)V

    goto :goto_0
.end method
