.class public final LX/1eM;
.super LX/1cg;
.source ""


# instance fields
.field public final synthetic a:Landroid/util/Pair;

.field public final synthetic b:LX/1eL;


# direct methods
.method public constructor <init>(LX/1eL;Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 288147
    iput-object p1, p0, LX/1eM;->b:LX/1eL;

    iput-object p2, p0, LX/1eM;->a:Landroid/util/Pair;

    invoke-direct {p0}, LX/1cg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 288129
    iget-object v4, p0, LX/1eM;->b:LX/1eL;

    monitor-enter v4

    .line 288130
    :try_start_0
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    iget-object v1, v1, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v2, p0, LX/1eM;->a:Landroid/util/Pair;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    move-result v5

    .line 288131
    if-eqz v5, :cond_3

    .line 288132
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    iget-object v1, v1, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288133
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    iget-object v1, v1, LX/1eL;->f:LX/1cW;

    move-object v2, v0

    move-object v3, v1

    move-object v1, v0

    .line 288134
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288135
    invoke-static {v2}, LX/1cW;->a(Ljava/util/List;)V

    .line 288136
    invoke-static {v1}, LX/1cW;->c(Ljava/util/List;)V

    .line 288137
    invoke-static {v0}, LX/1cW;->b(Ljava/util/List;)V

    .line 288138
    if-eqz v3, :cond_0

    .line 288139
    invoke-virtual {v3}, LX/1cW;->i()V

    .line 288140
    :cond_0
    if-eqz v5, :cond_1

    .line 288141
    iget-object v0, p0, LX/1eM;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/1cd;

    invoke-virtual {v0}, LX/1cd;->b()V

    .line 288142
    :cond_1
    return-void

    .line 288143
    :cond_2
    :try_start_1
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v1}, LX/1eL;->b(LX/1eL;)Ljava/util/List;

    move-result-object v3

    .line 288144
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v1}, LX/1eL;->f$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v2

    .line 288145
    iget-object v1, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v1}, LX/1eL;->d$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v1

    move-object v6, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v6

    goto :goto_0

    .line 288146
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 288127
    iget-object v0, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v0}, LX/1eL;->b(LX/1eL;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/1cW;->a(Ljava/util/List;)V

    .line 288128
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 288125
    iget-object v0, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v0}, LX/1eL;->d$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/1cW;->b(Ljava/util/List;)V

    .line 288126
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 288123
    iget-object v0, p0, LX/1eM;->b:LX/1eL;

    invoke-static {v0}, LX/1eL;->f$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/1cW;->c(Ljava/util/List;)V

    .line 288124
    return-void
.end method
