.class public LX/1fb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 291826
    const-class v0, LX/1fb;

    sput-object v0, LX/1fb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291828
    iput-object p1, p0, LX/1fb;->b:LX/0Zb;

    .line 291829
    return-void
.end method

.method public static a(LX/1fb;Ljava/lang/String;LX/0gM;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 291830
    :try_start_0
    iget-object v0, p0, LX/1fb;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 291831
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291832
    const-string v1, "subscription"

    invoke-interface {p2}, LX/0gM;->c()LX/0gV;

    move-result-object v2

    .line 291833
    iget-object p0, v2, LX/0gW;->b:Ljava/lang/String;

    move-object v2, p0

    .line 291834
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291835
    const-string v1, "handle_type"

    invoke-interface {p2}, LX/0gM;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291836
    invoke-interface {p2}, LX/0gM;->f()LX/0lF;

    move-result-object v1

    .line 291837
    if-eqz v1, :cond_0

    .line 291838
    const-string v1, "query_params"

    invoke-interface {p2}, LX/0gM;->f()LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 291839
    :cond_0
    invoke-virtual {v0}, LX/0oG;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291840
    :cond_1
    :goto_0
    return-void

    .line 291841
    :catch_0
    move-exception v0

    .line 291842
    sget-object v1, LX/1fb;->a:Ljava/lang/Class;

    const-string v2, "GraphQL Subscription analytics encountered an error."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1fb;
    .locals 2

    .prologue
    .line 291843
    new-instance v1, LX/1fb;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/1fb;-><init>(LX/0Zb;)V

    .line 291844
    return-object v1
.end method
