.class public LX/1ad;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 277856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Uo;Landroid/content/Context;Landroid/util/AttributeSet;)LX/1Uo;
    .locals 13
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 277857
    if-eqz p2, :cond_20

    .line 277858
    sget-object v1, LX/1ae;->GenericDraweeHierarchy:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 277859
    :try_start_0
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v9

    move v7, v4

    move v1, v0

    move v2, v0

    move v3, v0

    move v5, v4

    .line 277860
    :goto_0
    if-ge v7, v9, :cond_19

    .line 277861
    invoke-virtual {v8, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v10

    .line 277862
    const/16 v11, 0xb

    if-ne v10, v11, :cond_1

    .line 277863
    invoke-static {v8, v10}, LX/1ad;->a(Landroid/content/res/TypedArray;I)LX/1Up;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 277864
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 277865
    :cond_1
    const/16 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 277866
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 277867
    iput-object v10, p0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277868
    goto :goto_1

    .line 277869
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 277870
    :cond_2
    :try_start_1
    const/16 v11, 0xe

    if-ne v10, v11, :cond_3

    .line 277871
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    goto :goto_1

    .line 277872
    :cond_3
    const/16 v11, 0x8

    if-ne v10, v11, :cond_4

    .line 277873
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 277874
    iput-object v10, p0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 277875
    goto :goto_1

    .line 277876
    :cond_4
    const/16 v11, 0x0

    if-ne v10, v11, :cond_5

    .line 277877
    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    .line 277878
    iput v10, p0, LX/1Uo;->d:I

    .line 277879
    goto :goto_1

    .line 277880
    :cond_5
    const/16 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 277881
    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v10

    .line 277882
    iput v10, p0, LX/1Uo;->e:F

    .line 277883
    goto :goto_1

    .line 277884
    :cond_6
    const/16 v11, 0x3

    if-ne v10, v11, :cond_7

    .line 277885
    invoke-static {v8, v10}, LX/1ad;->a(Landroid/content/res/TypedArray;I)LX/1Up;

    move-result-object v10

    .line 277886
    iput-object v10, p0, LX/1Uo;->g:LX/1Up;

    .line 277887
    goto :goto_1

    .line 277888
    :cond_7
    const/16 v11, 0x4

    if-ne v10, v11, :cond_8

    .line 277889
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 277890
    iput-object v10, p0, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 277891
    goto :goto_1

    .line 277892
    :cond_8
    const/16 v11, 0x5

    if-ne v10, v11, :cond_9

    .line 277893
    invoke-static {v8, v10}, LX/1ad;->a(Landroid/content/res/TypedArray;I)LX/1Up;

    move-result-object v10

    .line 277894
    iput-object v10, p0, LX/1Uo;->i:LX/1Up;

    .line 277895
    goto :goto_1

    .line 277896
    :cond_9
    const/16 v11, 0x6

    if-ne v10, v11, :cond_a

    .line 277897
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 277898
    iput-object v10, p0, LX/1Uo;->j:Landroid/graphics/drawable/Drawable;

    .line 277899
    goto :goto_1

    .line 277900
    :cond_a
    const/16 v11, 0x7

    if-ne v10, v11, :cond_b

    .line 277901
    invoke-static {v8, v10}, LX/1ad;->a(Landroid/content/res/TypedArray;I)LX/1Up;

    move-result-object v10

    .line 277902
    iput-object v10, p0, LX/1Uo;->k:LX/1Up;

    .line 277903
    goto :goto_1

    .line 277904
    :cond_b
    const/16 v11, 0x9

    if-ne v10, v11, :cond_c

    .line 277905
    invoke-static {v8, v10}, LX/1ad;->a(Landroid/content/res/TypedArray;I)LX/1Up;

    move-result-object v10

    .line 277906
    iput-object v10, p0, LX/1Uo;->m:LX/1Up;

    .line 277907
    goto/16 :goto_1

    .line 277908
    :cond_c
    const/16 v11, 0xa

    if-ne v10, v11, :cond_d

    .line 277909
    invoke-virtual {v8, v10, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    goto/16 :goto_1

    .line 277910
    :cond_d
    const/16 v11, 0xc

    if-ne v10, v11, :cond_e

    .line 277911
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 277912
    iput-object v10, p0, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    .line 277913
    goto/16 :goto_1

    .line 277914
    :cond_e
    const/16 v11, 0xd

    if-ne v10, v11, :cond_f

    .line 277915
    invoke-static {p1, v8, v10}, LX/1ad;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    goto/16 :goto_1

    .line 277916
    :cond_f
    const/16 v11, 0xf

    if-ne v10, v11, :cond_10

    .line 277917
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v10

    .line 277918
    iput-boolean v10, v11, LX/4Ab;->b:Z

    .line 277919
    goto/16 :goto_1

    .line 277920
    :cond_10
    const/16 v11, 0x10

    if-ne v10, v11, :cond_11

    .line 277921
    invoke-virtual {v8, v10, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    goto/16 :goto_1

    .line 277922
    :cond_11
    const/16 v11, 0x11

    if-ne v10, v11, :cond_12

    .line 277923
    invoke-virtual {v8, v10, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    goto/16 :goto_1

    .line 277924
    :cond_12
    const/16 v11, 0x12

    if-ne v10, v11, :cond_13

    .line 277925
    invoke-virtual {v8, v10, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    goto/16 :goto_1

    .line 277926
    :cond_13
    const/16 v11, 0x14

    if-ne v10, v11, :cond_14

    .line 277927
    invoke-virtual {v8, v10, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    goto/16 :goto_1

    .line 277928
    :cond_14
    const/16 v11, 0x13

    if-ne v10, v11, :cond_15

    .line 277929
    invoke-virtual {v8, v10, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    goto/16 :goto_1

    .line 277930
    :cond_15
    const/16 v11, 0x15

    if-ne v10, v11, :cond_16

    .line 277931
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    invoke-virtual {v11, v10}, LX/4Ab;->a(I)LX/4Ab;

    goto/16 :goto_1

    .line 277932
    :cond_16
    const/16 v11, 0x16

    if-ne v10, v11, :cond_17

    .line 277933
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v11, v10}, LX/4Ab;->c(F)LX/4Ab;

    goto/16 :goto_1

    .line 277934
    :cond_17
    const/16 v11, 0x17

    if-ne v10, v11, :cond_18

    .line 277935
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    .line 277936
    iput v10, v11, LX/4Ab;->f:I

    .line 277937
    goto/16 :goto_1

    .line 277938
    :cond_18
    const/16 v11, 0x18

    if-ne v10, v11, :cond_0

    .line 277939
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v11, v10}, LX/4Ab;->d(F)LX/4Ab;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 277940
    :cond_19
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 277941
    :goto_2
    iget-object v7, p0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    move-object v7, v7

    .line 277942
    if-eqz v7, :cond_1a

    if-lez v5, :cond_1a

    .line 277943
    new-instance v7, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 277944
    iget-object v8, p0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    move-object v8, v8

    .line 277945
    invoke-direct {v7, v8, v5}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 277946
    iput-object v7, p0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 277947
    :cond_1a
    if-lez v4, :cond_1b

    .line 277948
    invoke-static {p0}, LX/1ad;->a(LX/1Uo;)LX/4Ab;

    move-result-object v7

    if-eqz v3, :cond_1c

    int-to-float v3, v4

    move v5, v3

    :goto_3
    if-eqz v2, :cond_1d

    int-to-float v2, v4

    move v3, v2

    :goto_4
    if-eqz v0, :cond_1e

    int-to-float v0, v4

    move v2, v0

    :goto_5
    if-eqz v1, :cond_1f

    int-to-float v0, v4

    :goto_6
    invoke-virtual {v7, v5, v3, v2, v0}, LX/4Ab;->a(FFFF)LX/4Ab;

    .line 277949
    :cond_1b
    return-object p0

    :cond_1c
    move v5, v6

    .line 277950
    goto :goto_3

    :cond_1d
    move v3, v6

    goto :goto_4

    :cond_1e
    move v2, v6

    goto :goto_5

    :cond_1f
    move v0, v6

    goto :goto_6

    :cond_20
    move v1, v0

    move v2, v0

    move v3, v0

    move v5, v4

    goto :goto_2
.end method

.method private static a(Landroid/content/res/TypedArray;I)LX/1Up;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 277951
    const/4 v0, -0x2

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 277952
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "XML attribute not specified!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277953
    :pswitch_0
    const/4 v0, 0x0

    .line 277954
    :goto_0
    return-object v0

    .line 277955
    :pswitch_1
    sget-object v0, LX/1Up;->a:LX/1Up;

    goto :goto_0

    .line 277956
    :pswitch_2
    sget-object v0, LX/1Up;->b:LX/1Up;

    goto :goto_0

    .line 277957
    :pswitch_3
    sget-object v0, LX/1Up;->c:LX/1Up;

    goto :goto_0

    .line 277958
    :pswitch_4
    sget-object v0, LX/1Up;->d:LX/1Up;

    goto :goto_0

    .line 277959
    :pswitch_5
    sget-object v0, LX/1Up;->e:LX/1Up;

    goto :goto_0

    .line 277960
    :pswitch_6
    sget-object v0, LX/1Up;->f:LX/1Up;

    goto :goto_0

    .line 277961
    :pswitch_7
    sget-object v0, LX/1Up;->g:LX/1Up;

    goto :goto_0

    .line 277962
    :pswitch_8
    sget-object v0, LX/1Up;->h:LX/1Up;

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static a(LX/1Uo;)LX/4Ab;
    .locals 1

    .prologue
    .line 277963
    iget-object v0, p0, LX/1Uo;->u:LX/4Ab;

    move-object v0, v0

    .line 277964
    if-nez v0, :cond_0

    .line 277965
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    .line 277966
    iput-object v0, p0, LX/1Uo;->u:LX/4Ab;

    .line 277967
    :cond_0
    iget-object v0, p0, LX/1Uo;->u:LX/4Ab;

    move-object v0, v0

    .line 277968
    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 277969
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 277970
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
