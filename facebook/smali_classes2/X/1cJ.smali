.class public LX/1cJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1HY;

.field private final b:LX/1HY;

.field public final c:LX/1Ao;

.field public final d:LX/1IZ;

.field private final e:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1HY;LX/1HY;LX/1Ao;LX/1IZ;LX/1cF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HY;",
            "LX/1HY;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1IZ;",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281802
    iput-object p1, p0, LX/1cJ;->a:LX/1HY;

    .line 281803
    iput-object p2, p0, LX/1cJ;->b:LX/1HY;

    .line 281804
    iput-object p3, p0, LX/1cJ;->c:LX/1Ao;

    .line 281805
    iput-object p4, p0, LX/1cJ;->d:LX/1IZ;

    .line 281806
    iput-object p5, p0, LX/1cJ;->e:LX/1cF;

    .line 281807
    return-void
.end method

.method private a(LX/1cd;LX/1cW;Z)LX/1ex;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "Z)",
            "LX/1ex",
            "<",
            "LX/1FL;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281808
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v0

    .line 281809
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v2, v0

    .line 281810
    new-instance v0, LX/4f8;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, LX/4f8;-><init>(LX/1cJ;LX/1BV;Ljava/lang/String;LX/1cd;LX/1cW;Z)V

    return-object v0
.end method

.method public static a(LX/1BV;Ljava/lang/String;ZZ)Ljava/util/Map;
    .locals 4
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1BV;",
            "Ljava/lang/String;",
            "ZZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281811
    invoke-interface {p0, p1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281812
    const/4 v0, 0x0

    .line 281813
    :goto_0
    return-object v0

    .line 281814
    :cond_0
    if-eqz p2, :cond_1

    .line 281815
    const-string v0, "cached_value_found"

    const-string v1, "true"

    const-string v2, "cached_value_used_as_last"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 281816
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 281817
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281818
    invoke-interface {p0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281819
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    move-object v0, p0

    .line 281820
    goto :goto_0

    .line 281821
    :cond_1
    const-string v0, "cached_value_found"

    const-string v1, "false"

    invoke-static {v0, v1}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/concurrent/atomic/AtomicBoolean;LX/1cW;)V
    .locals 1

    .prologue
    .line 281822
    new-instance v0, LX/35z;

    invoke-direct {v0, p0, p1}, LX/35z;-><init>(LX/1cJ;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {p2, v0}, LX/1cW;->a(LX/1cg;)V

    .line 281823
    return-void
.end method

.method private static a(LX/3BC;LX/1o9;)Z
    .locals 2

    .prologue
    .line 281824
    iget v0, p0, LX/3BC;->b:I

    move v0, v0

    .line 281825
    iget v1, p1, LX/1o9;->a:I

    if-lt v0, v1, :cond_0

    .line 281826
    iget v0, p0, LX/3BC;->c:I

    move v0, v0

    .line 281827
    iget v1, p1, LX/1o9;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/3BC;LX/3BC;LX/1o9;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 281828
    if-nez p1, :cond_1

    .line 281829
    :cond_0
    :goto_0
    return v0

    .line 281830
    :cond_1
    invoke-static {p1, p2}, LX/1cJ;->a(LX/3BC;LX/1o9;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 281831
    iget v2, p0, LX/3BC;->b:I

    move v2, v2

    .line 281832
    iget v3, p1, LX/3BC;->b:I

    move v3, v3

    .line 281833
    if-ge v2, v3, :cond_2

    invoke-static {p0, p2}, LX/1cJ;->a(LX/3BC;LX/1o9;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 281834
    :cond_3
    iget v2, p0, LX/3BC;->b:I

    move v2, v2

    .line 281835
    iget v3, p1, LX/3BC;->b:I

    move v3, v3

    .line 281836
    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/1cJ;LX/1cd;LX/1cW;LX/1ny;Ljava/util/List;LX/1bf;LX/1o9;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "LX/1ny;",
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;",
            "LX/1bf;",
            "LX/1o9;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/1eg;"
        }
    .end annotation

    .prologue
    .line 281837
    invoke-virtual {p5}, LX/1bf;->a()LX/1bb;

    move-result-object v1

    sget-object v2, LX/1bb;->SMALL:LX/1bb;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/1cJ;->b:LX/1HY;

    move-object v2, v1

    .line 281838
    :goto_0
    invoke-virtual {p2}, LX/1cW;->d()Ljava/lang/Object;

    move-result-object v7

    .line 281839
    const/4 v5, 0x0

    .line 281840
    const/4 v4, 0x0

    .line 281841
    const/4 v1, 0x0

    move v6, v1

    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_1

    .line 281842
    invoke-interface {p4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3BC;

    .line 281843
    iget-object v3, p0, LX/1cJ;->c:LX/1Ao;

    invoke-virtual {v1}, LX/3BC;->a()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v3, v8, v7}, LX/1Ao;->a(Landroid/net/Uri;Ljava/lang/Object;)LX/1bh;

    move-result-object v3

    .line 281844
    invoke-virtual {v2, v3}, LX/1HY;->a(LX/1bh;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 281845
    move-object/from16 v0, p6

    invoke-static {v1, v5, v0}, LX/1cJ;->a(LX/3BC;LX/3BC;LX/1o9;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v9, v3

    move-object v3, v1

    move-object v1, v9

    .line 281846
    :goto_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-object v5, v3

    move-object v4, v1

    goto :goto_1

    .line 281847
    :cond_0
    iget-object v1, p0, LX/1cJ;->a:LX/1HY;

    move-object v2, v1

    goto :goto_0

    .line 281848
    :cond_1
    if-nez v4, :cond_3

    .line 281849
    const/4 v1, 0x0

    invoke-static {v1}, LX/1eg;->a(Ljava/lang/Object;)LX/1eg;

    move-result-object v1

    .line 281850
    :cond_2
    const/4 v2, 0x0

    move v9, v2

    move-object v2, v1

    move v1, v9

    .line 281851
    :goto_3
    invoke-direct {p0, p1, p2, v1}, LX/1cJ;->a(LX/1cd;LX/1cW;Z)LX/1ex;

    move-result-object v1

    .line 281852
    invoke-virtual {v2, v1}, LX/1eg;->a(LX/1ex;)LX/1eg;

    move-result-object v1

    return-object v1

    .line 281853
    :cond_3
    move-object/from16 v0, p7

    invoke-virtual {v2, v4, v0}, LX/1HY;->a(LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v1

    .line 281854
    invoke-virtual {p3}, LX/1ny;->c()Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p6

    invoke-static {v5, v0}, LX/1cJ;->a(LX/3BC;LX/1o9;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move v9, v2

    move-object v2, v1

    move v1, v9

    goto :goto_3

    :cond_4
    move-object v1, v4

    move-object v3, v5

    goto :goto_2
.end method

.method public static b(LX/1cJ;LX/1cd;LX/1cW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281855
    iget-object v0, p0, LX/1cJ;->e:LX/1cF;

    new-instance v1, LX/1qc;

    invoke-direct {v1, p0, p1, p2}, LX/1qc;-><init>(LX/1cJ;LX/1cd;LX/1cW;)V

    invoke-interface {v0, v1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 281856
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281857
    invoke-virtual/range {p2 .. p2}, LX/1cW;->a()LX/1bf;

    move-result-object v7

    .line 281858
    invoke-virtual {v7}, LX/1bf;->f()LX/1o9;

    move-result-object v8

    .line 281859
    invoke-virtual {v7}, LX/1bf;->c()LX/1ny;

    move-result-object v5

    .line 281860
    invoke-virtual {v7}, LX/1bf;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v8, :cond_0

    iget v2, v8, LX/1o9;->b:I

    if-lez v2, :cond_0

    iget v2, v8, LX/1o9;->a:I

    if-lez v2, :cond_0

    if-nez v5, :cond_1

    .line 281861
    :cond_0
    invoke-static/range {p0 .. p2}, LX/1cJ;->b(LX/1cJ;LX/1cd;LX/1cW;)V

    .line 281862
    :goto_0
    return-void

    .line 281863
    :cond_1
    invoke-virtual/range {p2 .. p2}, LX/1cW;->c()LX/1BV;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, LX/1cW;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MediaVariationsFallbackProducer"

    invoke-interface {v2, v3, v4}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281864
    new-instance v9, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v9, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 281865
    invoke-virtual {v5}, LX/1ny;->b()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 281866
    invoke-virtual {v5}, LX/1ny;->b()Ljava/util/List;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-static/range {v2 .. v9}, LX/1cJ;->a$redex0(LX/1cJ;LX/1cd;LX/1cW;LX/1ny;Ljava/util/List;LX/1bf;LX/1o9;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    .line 281867
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v9, v1}, LX/1cJ;->a(Ljava/util/concurrent/atomic/AtomicBoolean;LX/1cW;)V

    goto :goto_0

    .line 281868
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1cJ;->d:LX/1IZ;

    invoke-virtual {v5}, LX/1ny;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1IZ;->a(Ljava/lang/String;)LX/1eg;

    move-result-object v2

    .line 281869
    new-instance v10, LX/35x;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object v14, v5

    move-object v15, v7

    move-object/from16 v16, v8

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v17}, LX/35x;-><init>(LX/1cJ;LX/1cd;LX/1cW;LX/1ny;LX/1bf;LX/1o9;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v2, v10}, LX/1eg;->a(LX/1ex;)LX/1eg;

    goto :goto_1
.end method
