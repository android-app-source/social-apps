.class public LX/0xG;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yH;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11u;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11v;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/120;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/widget/CustomViewPager;

.field public g:Landroid/view/View;

.field public h:LX/12S;

.field public i:LX/12I;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 162283
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 162284
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162285
    iput-object v0, p0, LX/0xG;->a:LX/0Ot;

    .line 162286
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162287
    iput-object v0, p0, LX/0xG;->b:LX/0Ot;

    .line 162288
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162289
    iput-object v0, p0, LX/0xG;->c:LX/0Ot;

    .line 162290
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162291
    iput-object v0, p0, LX/0xG;->d:LX/0Ot;

    .line 162292
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162293
    iput-object v0, p0, LX/0xG;->e:LX/0Ot;

    .line 162294
    return-void
.end method

.method public static a(LX/0xG;LX/0yY;)Z
    .locals 1

    .prologue
    .line 162295
    iget-object v0, p0, LX/0xG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    .line 162296
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/0xG;ILX/0yY;)V
    .locals 4

    .prologue
    .line 162297
    invoke-static {p0, p2}, LX/0xG;->a(LX/0xG;LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162298
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {p2, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162299
    iget-object v0, p0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->b()V

    .line 162300
    :cond_0
    :goto_0
    invoke-static {p0, p2}, LX/0xG;->a(LX/0xG;LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 162301
    :cond_1
    :goto_1
    return-void

    .line 162302
    :cond_2
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p2, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162303
    iget-object v0, p0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    iget-object v1, p0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/120;

    invoke-virtual {v0, v1}, LX/11u;->a(LX/120;)V

    goto :goto_0

    .line 162304
    :cond_3
    iget-object v0, p0, LX/0xG;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 162305
    iget-object v0, p0, LX/0xG;->g:Landroid/view/View;

    iget-object v1, p0, LX/0xG;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/0xG;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, LX/0xG;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 162306
    iget-object v0, p0, LX/0xG;->f:Lcom/facebook/widget/CustomViewPager;

    new-instance v1, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentZeroRatingController$3;

    invoke-direct {v1, p0}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentZeroRatingController$3;-><init>(LX/0xG;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 162307
    iget-object v0, p0, LX/0xG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11v;

    iget-object v1, p0, LX/0xG;->h:LX/12S;

    .line 162308
    iput-object v1, v0, LX/11v;->p:LX/12S;

    .line 162309
    move-object v0, v0

    .line 162310
    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    .line 162311
    iput-object v1, v0, LX/11v;->q:LX/0yY;

    .line 162312
    iget-object v0, p0, LX/0xG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11v;

    invoke-virtual {v0}, LX/11v;->a()V

    .line 162313
    iget-object v0, p0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/120;

    iget-object v1, p0, LX/0xG;->i:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->a(LX/12I;)V

    .line 162314
    iget-object v0, p0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/120;

    invoke-virtual {v0}, LX/120;->a()V

    .line 162315
    iget-object v0, p0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->b()V

    .line 162316
    iget-object v0, p0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->c()V

    .line 162317
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 162318
    iget-object v0, p0, LX/0xG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11v;

    invoke-virtual {v0}, LX/11v;->b()V

    .line 162319
    iget-object v0, p0, LX/0xG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11v;

    const/4 v1, 0x0

    .line 162320
    iput-object v1, v0, LX/11v;->p:LX/12S;

    .line 162321
    iget-object v0, p0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/120;

    invoke-virtual {v0}, LX/120;->b()V

    .line 162322
    iget-object v0, p0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/120;

    iget-object v1, p0, LX/0xG;->i:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->b(LX/12I;)V

    .line 162323
    iget-object v0, p0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->e()V

    .line 162324
    return-void
.end method
