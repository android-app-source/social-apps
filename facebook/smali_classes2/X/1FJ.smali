.class public final LX/1FJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1FJ;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:Z


# instance fields
.field private final d:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:LX/1gQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1gQ",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222126
    const-class v0, LX/1FJ;

    sput-object v0, LX/1FJ;->a:Ljava/lang/Class;

    .line 222127
    new-instance v0, LX/1H0;

    invoke-direct {v0}, LX/1H0;-><init>()V

    sput-object v0, LX/1FJ;->b:LX/1FN;

    return-void
.end method

.method private constructor <init>(LX/1gQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1gQ",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 222120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222121
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1FJ;->f:Z

    .line 222122
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1gQ;

    iput-object v0, p0, LX/1FJ;->g:LX/1gQ;

    .line 222123
    invoke-virtual {p1}, LX/1gQ;->b()V

    .line 222124
    invoke-static {}, LX/1FJ;->f()Ljava/lang/Throwable;

    move-result-object v0

    iput-object v0, p0, LX/1FJ;->d:Ljava/lang/Throwable;

    .line 222125
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;LX/1FN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/1FN",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 222115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222116
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1FJ;->f:Z

    .line 222117
    new-instance v0, LX/1gQ;

    invoke-direct {v0, p1, p2}, LX/1gQ;-><init>(Ljava/lang/Object;LX/1FN;)V

    iput-object v0, p0, LX/1FJ;->g:LX/1gQ;

    .line 222118
    invoke-static {}, LX/1FJ;->f()Ljava/lang/Throwable;

    move-result-object v0

    iput-object v0, p0, LX/1FJ;->d:Ljava/lang/Throwable;

    .line 222119
    return-void
.end method

.method public static a(Ljava/io/Closeable;)LX/1FJ;
    .locals 2
    .param p0    # Ljava/io/Closeable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Closeable;",
            ">(TT;)",
            "LX/1FJ",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222112
    if-nez p0, :cond_0

    .line 222113
    const/4 v0, 0x0

    .line 222114
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1FJ;

    sget-object v1, LX/1FJ;->b:LX/1FN;

    invoke-direct {v0, p0, v1}, LX/1FJ;-><init>(Ljava/lang/Object;LX/1FN;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;LX/1FN;)LX/1FJ;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "LX/1FN",
            "<TT;>;)",
            "LX/1FJ",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222109
    if-nez p0, :cond_0

    .line 222110
    const/4 v0, 0x0

    .line 222111
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1FJ;

    invoke-direct {v0, p0, p1}, LX/1FJ;-><init>(Ljava/lang/Object;LX/1FN;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<",
            "LX/1FJ",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 222102
    if-nez p0, :cond_0

    .line 222103
    const/4 v0, 0x0

    .line 222104
    :goto_0
    return-object v0

    .line 222105
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 222106
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 222107
    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 222108
    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;)V
    .locals 2
    .param p0    # Ljava/lang/Iterable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "LX/1FJ",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 222098
    if-eqz p0, :cond_0

    .line 222099
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 222100
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 222101
    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 222096
    sput-boolean p0, LX/1FJ;->c:Z

    .line 222097
    return-void
.end method

.method public static a(LX/1FJ;)Z
    .locals 1
    .param p0    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 222095
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1FJ;)LX/1FJ;
    .locals 1
    .param p0    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1FJ",
            "<TT;>;)",
            "LX/1FJ",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222128
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/1FJ;->c()LX/1FJ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1FJ;)V
    .locals 0
    .param p0    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 222092
    if-eqz p0, :cond_0

    .line 222093
    invoke-virtual {p0}, LX/1FJ;->close()V

    .line 222094
    :cond_0
    return-void
.end method

.method private static f()Ljava/lang/Throwable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222089
    sget-boolean v0, LX/1FJ;->c:Z

    if-eqz v0, :cond_0

    .line 222090
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 222091
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 222086
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1FJ;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222087
    iget-object v0, p0, LX/1FJ;->g:LX/1gQ;

    invoke-virtual {v0}, LX/1gQ;->a()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 222088
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 222082
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/1FJ;->f()Ljava/lang/Throwable;

    move-result-object v0

    iput-object v0, p0, LX/1FJ;->e:Ljava/lang/Throwable;

    .line 222083
    invoke-virtual {p0}, LX/1FJ;->d()Z

    move-result v0

    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222084
    new-instance v0, LX/1FJ;

    iget-object v1, p0, LX/1FJ;->g:LX/1gQ;

    invoke-direct {v0, v1}, LX/1FJ;-><init>(LX/1gQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 222085
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 222079
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/1FJ;->f()Ljava/lang/Throwable;

    move-result-object v0

    iput-object v0, p0, LX/1FJ;->e:Ljava/lang/Throwable;

    .line 222080
    invoke-virtual {p0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/1FJ;

    iget-object v1, p0, LX/1FJ;->g:LX/1gQ;

    invoke-direct {v0, v1}, LX/1FJ;-><init>(LX/1gQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 222081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 222078
    invoke-virtual {p0}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 222070
    monitor-enter p0

    .line 222071
    :try_start_0
    iget-boolean v0, p0, LX/1FJ;->f:Z

    if-eqz v0, :cond_0

    .line 222072
    monitor-exit p0

    .line 222073
    :goto_0
    return-void

    .line 222074
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1FJ;->f:Z

    .line 222075
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222076
    iget-object v0, p0, LX/1FJ;->g:LX/1gQ;

    invoke-virtual {v0}, LX/1gQ;->c()V

    goto :goto_0

    .line 222077
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 222069
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1FJ;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()I
    .locals 1

    .prologue
    .line 222049
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1FJ;->g:LX/1gQ;

    invoke-virtual {v0}, LX/1gQ;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 222050
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 222051
    :try_start_1
    iget-boolean v0, p0, LX/1FJ;->f:Z

    if-eqz v0, :cond_0

    .line 222052
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222053
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 222054
    :goto_0
    return-void

    .line 222055
    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 222056
    :try_start_3
    sget-boolean v0, LX/1FJ;->c:Z

    if-eqz v0, :cond_4

    .line 222057
    iget-object v0, p0, LX/1FJ;->e:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1FJ;->e:Ljava/lang/Throwable;

    move-object v1, v0

    .line 222058
    :goto_1
    iget-object v0, p0, LX/1FJ;->e:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    const-string v0, "Finalized without closing! Cause is clone location."

    .line 222059
    :goto_2
    const-string v2, "UnclosedReference"

    .line 222060
    sget-object v3, LX/03J;->a:LX/03G;

    const/4 v4, 0x6

    invoke-interface {v3, v4}, LX/03G;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222061
    sget-object v3, LX/03J;->a:LX/03G;

    invoke-interface {v3, v2, v0, v1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 222062
    :cond_1
    :goto_3
    invoke-virtual {p0}, LX/1FJ;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 222063
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    .line 222064
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 222065
    :catchall_1
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0

    .line 222066
    :cond_2
    :try_start_6
    iget-object v0, p0, LX/1FJ;->d:Ljava/lang/Throwable;

    move-object v1, v0

    goto :goto_1

    .line 222067
    :cond_3
    const-string v0, "Finalized without closing! Cause is obtain location."

    goto :goto_2

    .line 222068
    :cond_4
    sget-object v0, LX/1FJ;->a:Ljava/lang/Class;

    const-string v1, "Finalized without closing: %x %x (type = %s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/1FJ;->g:LX/1gQ;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/1FJ;->g:LX/1gQ;

    invoke-virtual {v4}, LX/1gQ;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3
.end method
