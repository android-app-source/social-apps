.class public final enum LX/0pL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0pL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0pL;

.field public static final enum DISK_AND_MEMORY_CACHE:LX/0pL;

.field public static final enum MEMORY_ONLY_CACHE:LX/0pL;

.field public static final enum NO_CACHE:LX/0pL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144321
    new-instance v0, LX/0pL;

    const-string v1, "DISK_AND_MEMORY_CACHE"

    invoke-direct {v0, v1, v2}, LX/0pL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    .line 144322
    new-instance v0, LX/0pL;

    const-string v1, "MEMORY_ONLY_CACHE"

    invoke-direct {v0, v1, v3}, LX/0pL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0pL;->MEMORY_ONLY_CACHE:LX/0pL;

    .line 144323
    new-instance v0, LX/0pL;

    const-string v1, "NO_CACHE"

    invoke-direct {v0, v1, v4}, LX/0pL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0pL;->NO_CACHE:LX/0pL;

    .line 144324
    const/4 v0, 0x3

    new-array v0, v0, [LX/0pL;

    sget-object v1, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    aput-object v1, v0, v2

    sget-object v1, LX/0pL;->MEMORY_ONLY_CACHE:LX/0pL;

    aput-object v1, v0, v3

    sget-object v1, LX/0pL;->NO_CACHE:LX/0pL;

    aput-object v1, v0, v4

    sput-object v0, LX/0pL;->$VALUES:[LX/0pL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 144320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0pL;
    .locals 1

    .prologue
    .line 144318
    const-class v0, LX/0pL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0pL;

    return-object v0
.end method

.method public static values()[LX/0pL;
    .locals 1

    .prologue
    .line 144319
    sget-object v0, LX/0pL;->$VALUES:[LX/0pL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0pL;

    return-object v0
.end method
