.class public LX/1Bf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/1Bf;


# instance fields
.field private final b:LX/0ad;

.field private final c:Landroid/content/Context;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/Runnable;

.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213687
    const-class v0, LX/1Bf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Bf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;Landroid/content/Context;LX/0Or;)V
    .locals 2
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213689
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Bf;->f:J

    .line 213690
    iput-object p1, p0, LX/1Bf;->b:LX/0ad;

    .line 213691
    iput-object p2, p0, LX/1Bf;->c:Landroid/content/Context;

    .line 213692
    iput-object p3, p0, LX/1Bf;->d:LX/0Or;

    .line 213693
    new-instance v0, Lcom/facebook/browser/liteclient/fallback/BrowserLiteIntentServiceHelperSelector$1;

    invoke-direct {v0, p0}, Lcom/facebook/browser/liteclient/fallback/BrowserLiteIntentServiceHelperSelector$1;-><init>(LX/1Bf;)V

    iput-object v0, p0, LX/1Bf;->e:Ljava/lang/Runnable;

    .line 213694
    return-void
.end method

.method public static a(LX/0QB;)LX/1Bf;
    .locals 6

    .prologue
    .line 213695
    sget-object v0, LX/1Bf;->g:LX/1Bf;

    if-nez v0, :cond_1

    .line 213696
    const-class v1, LX/1Bf;

    monitor-enter v1

    .line 213697
    :try_start_0
    sget-object v0, LX/1Bf;->g:LX/1Bf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213698
    if-eqz v2, :cond_0

    .line 213699
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213700
    new-instance v5, LX/1Bf;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 p0, 0x2fd

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/1Bf;-><init>(LX/0ad;Landroid/content/Context;LX/0Or;)V

    .line 213701
    move-object v0, v5

    .line 213702
    sput-object v0, LX/1Bf;->g:LX/1Bf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213703
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213704
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213705
    :cond_1
    sget-object v0, LX/1Bf;->g:LX/1Bf;

    return-object v0

    .line 213706
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/1Bf;)Z
    .locals 3

    .prologue
    .line 213708
    iget-object v0, p0, LX/1Bf;->b:LX/0ad;

    sget-short v1, LX/1Bm;->C:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static d(LX/1Bf;)Landroid/os/Bundle;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213683
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 213684
    iget-object v0, p0, LX/1Bf;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213685
    const-string v0, "BrowserLiteIntent.EXTRA_LOGCAT"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213686
    :cond_0
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 213675
    invoke-static {p0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v0

    invoke-static {p0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/049;->f(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 213676
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V
    .locals 2

    .prologue
    .line 213681
    invoke-static {p0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v0

    invoke-static {p0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/049;->a(Landroid/content/Context;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;ZLandroid/os/Bundle;)V

    .line 213682
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 213679
    iget-object v0, p0, LX/1Bf;->c:Landroid/content/Context;

    invoke-static {p0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v1

    invoke-static {p0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/049;->e(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 213680
    return-void
.end method

.method public final b(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 213677
    invoke-static {p0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v0

    invoke-static {p1, v0, p2}, LX/049;->b(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 213678
    return-void
.end method
