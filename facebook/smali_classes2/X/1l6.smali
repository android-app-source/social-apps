.class public LX/1l6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private a:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 311049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311050
    const/4 v0, 0x1

    iput v0, p0, LX/1l6;->a:I

    .line 311051
    return-void
.end method

.method public static a()LX/1l6;
    .locals 1

    .prologue
    .line 311048
    new-instance v0, LX/1l6;

    invoke-direct {v0}, LX/1l6;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(I)LX/1l6;
    .locals 1

    .prologue
    .line 311039
    iget v0, p0, LX/1l6;->a:I

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p1

    iput v0, p0, LX/1l6;->a:I

    .line 311040
    return-object p0
.end method

.method public final a(J)LX/1l6;
    .locals 3

    .prologue
    .line 311047
    const/16 v0, 0x20

    ushr-long v0, p1, v0

    xor-long/2addr v0, p1

    long-to-int v0, v0

    invoke-virtual {p0, v0}, LX/1l6;->a(I)LX/1l6;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)LX/1l6;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 311043
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 311044
    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {p0, v1}, LX/1l6;->a(I)LX/1l6;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 311045
    goto :goto_0

    .line 311046
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final a(Z)LX/1l6;
    .locals 1

    .prologue
    .line 311042
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/1l6;->a(I)LX/1l6;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 311041
    iget v0, p0, LX/1l6;->a:I

    return v0
.end method
