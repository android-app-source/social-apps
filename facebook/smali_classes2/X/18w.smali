.class public LX/18w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 207104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 207105
    const-class v0, LX/18x;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18x;

    .line 207106
    if-eqz v0, :cond_0

    .line 207107
    invoke-interface {v0}, LX/18x;->a()Landroid/view/View;

    move-result-object v0

    .line 207108
    :goto_0
    return-object v0

    .line 207109
    :cond_0
    const-class v0, Landroid/app/Activity;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 207110
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 207111
    invoke-static {p0}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 207112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x1020002

    goto :goto_0
.end method
