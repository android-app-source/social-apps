.class public LX/0WM;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0TD;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0TD;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76113
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0TD;
    .locals 5

    .prologue
    .line 76114
    sget-object v0, LX/0WM;->a:LX/0TD;

    if-nez v0, :cond_1

    .line 76115
    const-class v1, LX/0WM;

    monitor-enter v1

    .line 76116
    :try_start_0
    sget-object v0, LX/0WM;->a:LX/0TD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76117
    if-eqz v2, :cond_0

    .line 76118
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76119
    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v3

    check-cast v3, LX/0TJ;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v4

    check-cast v4, LX/0Sj;

    invoke-static {v0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object p0

    check-cast p0, LX/0TR;

    invoke-static {v3, v4, p0}, LX/0Su;->a(LX/0TJ;LX/0Sj;LX/0TR;)LX/0TD;

    move-result-object v3

    move-object v0, v3

    .line 76120
    sput-object v0, LX/0WM;->a:LX/0TD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76121
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76122
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76123
    :cond_1
    sget-object v0, LX/0WM;->a:LX/0TD;

    return-object v0

    .line 76124
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 76126
    invoke-static {p0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v0

    check-cast v0, LX/0TJ;

    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v1

    check-cast v1, LX/0Sj;

    invoke-static {p0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v2

    check-cast v2, LX/0TR;

    invoke-static {v0, v1, v2}, LX/0Su;->a(LX/0TJ;LX/0Sj;LX/0TR;)LX/0TD;

    move-result-object v0

    return-object v0
.end method
