.class public LX/0z4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feed/data/freshfeed/uih/SlidingWindowEvent;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:I

.field private final c:I

.field private final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0yz",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public f:LX/0z3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/data/freshfeed/uih/SlidingWindowEventQueueRecycler",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;II)V
    .locals 1
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166590
    iput-object p1, p0, LX/0z4;->a:LX/0SG;

    .line 166591
    iput p2, p0, LX/0z4;->b:I

    .line 166592
    iput p3, p0, LX/0z4;->c:I

    .line 166593
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, p2}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, LX/0z4;->d:Ljava/util/Queue;

    .line 166594
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0z4;->e:Ljava/util/List;

    .line 166595
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 8

    .prologue
    .line 166574
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0z4;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 166575
    :cond_0
    monitor-exit p0

    return-void

    .line 166576
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0z4;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget v2, p0, LX/0z4;->c:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long v2, v0, v2

    .line 166577
    iget-object v0, p0, LX/0z4;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 166578
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166579
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VO;

    .line 166580
    iget-object v4, p0, LX/0z4;->d:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    iget v5, p0, LX/0z4;->b:I

    if-ge v4, v5, :cond_3

    .line 166581
    iget-wide v6, v0, LX/6VO;->b:J

    move-wide v4, v6

    .line 166582
    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    .line 166583
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 166584
    iget-object v4, p0, LX/0z4;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0yz;

    .line 166585
    invoke-interface {v4, v0}, LX/0yz;->b(LX/6VO;)V

    goto :goto_1

    .line 166586
    :cond_4
    iget-object v4, p0, LX/0z4;->f:LX/0z3;

    if-eqz v4, :cond_2

    .line 166587
    iget-object v4, p0, LX/0z4;->f:LX/0z3;

    invoke-virtual {v4, v0}, LX/0z3;->a(LX/6VO;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166588
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0yz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yz",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 166564
    iget-object v0, p0, LX/0z4;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166565
    return-void
.end method

.method public final declared-synchronized a(LX/6VO;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 166566
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0z4;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 166567
    iput-wide v0, p1, LX/6VO;->b:J

    .line 166568
    invoke-virtual {p0}, LX/0z4;->a()V

    .line 166569
    iget-object v0, p0, LX/0z4;->d:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 166570
    iget-object v0, p0, LX/0z4;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yz;

    .line 166571
    invoke-interface {v0, p1}, LX/0yz;->a(LX/6VO;)V

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166572
    :cond_0
    monitor-exit p0

    return-void

    .line 166573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
