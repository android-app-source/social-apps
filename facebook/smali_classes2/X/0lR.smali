.class public LX/0lR;
.super LX/0lS;
.source ""


# instance fields
.field public final b:LX/0m4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0m4",
            "<*>;"
        }
    .end annotation
.end field

.field public final c:LX/0lU;

.field public final d:LX/0lN;

.field public e:LX/1Y3;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/4qt;

.field public h:LX/2At;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/2An;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2At;

.field public l:LX/2An;


# direct methods
.method private constructor <init>(LX/0m4;LX/0lJ;LX/0lN;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0lN;",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129462
    invoke-direct {p0, p2}, LX/0lS;-><init>(LX/0lJ;)V

    .line 129463
    iput-object p1, p0, LX/0lR;->b:LX/0m4;

    .line 129464
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/0lR;->c:LX/0lU;

    .line 129465
    iput-object p3, p0, LX/0lR;->d:LX/0lN;

    .line 129466
    iput-object p4, p0, LX/0lR;->f:Ljava/util/List;

    .line 129467
    return-void

    .line 129468
    :cond_0
    invoke-virtual {p1}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(LX/2Al;)V
    .locals 4

    .prologue
    .line 129410
    iget-object v0, p1, LX/2Al;->a:LX/0m4;

    move-object v0, v0

    .line 129411
    iget-object v1, p1, LX/2Al;->c:LX/0lJ;

    move-object v1, v1

    .line 129412
    iget-object v2, p1, LX/2Al;->d:LX/0lN;

    move-object v2, v2

    .line 129413
    invoke-virtual {p1}, LX/2Al;->d()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, LX/0lR;-><init>(LX/0m4;LX/0lJ;LX/0lN;Ljava/util/List;)V

    .line 129414
    invoke-virtual {p1}, LX/2Al;->j()LX/4qt;

    move-result-object v0

    iput-object v0, p0, LX/0lR;->g:LX/4qt;

    .line 129415
    return-void
.end method

.method public static a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0lN;",
            ")",
            "LX/0lR;"
        }
    .end annotation

    .prologue
    .line 129416
    new-instance v0, LX/0lR;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, p1, p2, v1}, LX/0lR;-><init>(LX/0m4;LX/0lJ;LX/0lN;Ljava/util/List;)V

    return-object v0
.end method

.method public static a(LX/2Al;)LX/0lR;
    .locals 2

    .prologue
    .line 129417
    new-instance v0, LX/0lR;

    invoke-direct {v0, p0}, LX/0lR;-><init>(LX/2Al;)V

    .line 129418
    invoke-virtual {p0}, LX/2Al;->h()LX/2At;

    move-result-object v1

    iput-object v1, v0, LX/0lR;->h:LX/2At;

    .line 129419
    iget-object v1, p0, LX/2Al;->m:Ljava/util/HashSet;

    move-object v1, v1

    .line 129420
    iput-object v1, v0, LX/0lR;->j:Ljava/util/Set;

    .line 129421
    iget-object v1, p0, LX/2Al;->n:Ljava/util/LinkedHashMap;

    move-object v1, v1

    .line 129422
    iput-object v1, v0, LX/0lR;->i:Ljava/util/Map;

    .line 129423
    invoke-virtual {p0}, LX/2Al;->f()LX/2At;

    move-result-object v1

    iput-object v1, v0, LX/0lR;->k:LX/2At;

    .line 129424
    return-object v0
.end method

.method private a(Ljava/lang/Object;)LX/1Xr;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 129425
    if-nez p1, :cond_0

    move-object p1, v0

    .line 129426
    :goto_0
    return-object p1

    .line 129427
    :cond_0
    instance-of v1, p1, LX/1Xr;

    if-eqz v1, :cond_1

    .line 129428
    check-cast p1, LX/1Xr;

    goto :goto_0

    .line 129429
    :cond_1
    instance-of v1, p1, Ljava/lang/Class;

    if-nez v1, :cond_2

    .line 129430
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Converter definition of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected type Converter or Class<Converter> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129431
    :cond_2
    check-cast p1, Ljava/lang/Class;

    .line 129432
    const-class v1, LX/1Xq;

    if-eq p1, v1, :cond_3

    const-class v1, LX/1Xp;

    if-ne p1, v1, :cond_4

    :cond_3
    move-object p1, v0

    .line 129433
    goto :goto_0

    .line 129434
    :cond_4
    const-class v1, LX/1Xr;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 129435
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected Class<Converter>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129436
    :cond_5
    iget-object v1, p0, LX/0lR;->b:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->l()LX/4py;

    move-result-object v1

    .line 129437
    if-nez v1, :cond_7

    .line 129438
    :goto_1
    if-nez v0, :cond_6

    .line 129439
    iget-object v0, p0, LX/0lR;->b:LX/0m4;

    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p1, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Xr;

    :cond_6
    move-object p1, v0

    .line 129440
    goto :goto_0

    .line 129441
    :cond_7
    const/4 v0, 0x0

    move-object v0, v0

    .line 129442
    goto :goto_1
.end method

.method private a(LX/2At;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 129443
    invoke-virtual {p1}, LX/2At;->o()Ljava/lang/Class;

    move-result-object v2

    .line 129444
    invoke-virtual {p0}, LX/0lS;->b()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 129445
    :cond_0
    :goto_0
    return v0

    .line 129446
    :cond_1
    iget-object v2, p0, LX/0lR;->c:LX/0lU;

    invoke-virtual {v2, p1}, LX/0lU;->w(LX/0lO;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 129447
    goto :goto_0

    .line 129448
    :cond_2
    const-string v2, "valueOf"

    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 129449
    goto :goto_0
.end method

.method public static b(LX/2Al;)LX/0lR;
    .locals 2

    .prologue
    .line 129450
    new-instance v0, LX/0lR;

    invoke-direct {v0, p0}, LX/0lR;-><init>(LX/2Al;)V

    .line 129451
    invoke-virtual {p0}, LX/2Al;->f()LX/2At;

    move-result-object v1

    iput-object v1, v0, LX/0lR;->k:LX/2At;

    .line 129452
    invoke-virtual {p0}, LX/2Al;->g()LX/2An;

    move-result-object v1

    iput-object v1, v0, LX/0lR;->l:LX/2An;

    .line 129453
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/reflect/Type;)LX/0lJ;
    .locals 1

    .prologue
    .line 129454
    if-nez p1, :cond_0

    .line 129455
    const/4 v0, 0x0

    .line 129456
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0lS;->f()LX/1Y3;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Y3;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0nr;)LX/0nr;
    .locals 2

    .prologue
    .line 129495
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129496
    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1, p1}, LX/0lU;->a(LX/0lO;LX/0nr;)LX/0nr;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/2At;"
        }
    .end annotation

    .prologue
    .line 129457
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, p1, p2}, LX/0lN;->a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4pN;)LX/4pN;
    .locals 2

    .prologue
    .line 129458
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-eqz v0, :cond_0

    .line 129459
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->e(LX/0lO;)LX/4pN;

    move-result-object v0

    .line 129460
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 129461
    :cond_0
    return-object p1
.end method

.method public final a(Z)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 129392
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->i()LX/2Vc;

    move-result-object v0

    .line 129393
    if-nez v0, :cond_0

    .line 129394
    const/4 v0, 0x0

    .line 129395
    :goto_0
    return-object v0

    .line 129396
    :cond_0
    if-eqz p1, :cond_1

    .line 129397
    invoke-virtual {v0}, LX/2An;->k()V

    .line 129398
    :cond_1
    :try_start_0
    iget-object v1, v0, LX/2Vc;->_constructor:Ljava/lang/reflect/Constructor;

    move-object v0, v1

    .line 129399
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 129400
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 129401
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_1

    .line 129402
    :cond_2
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 129403
    :cond_3
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 129404
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to instantiate bean of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/0lR;->d:LX/0lN;

    .line 129405
    iget-object p0, v3, LX/0lN;->a:Ljava/lang/Class;

    move-object v3, p0

    .line 129406
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 129407
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final varargs a([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 129469
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vc;

    .line 129470
    invoke-virtual {v0}, LX/2Vc;->g()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    .line 129471
    invoke-virtual {v0, v2}, LX/2Vc;->a(I)Ljava/lang/Class;

    move-result-object v4

    .line 129472
    array-length v5, p1

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, p1, v1

    .line 129473
    if-ne v6, v4, :cond_1

    .line 129474
    iget-object v1, v0, LX/2Vc;->_constructor:Ljava/lang/reflect/Constructor;

    move-object v0, v1

    .line 129475
    :goto_1
    return-object v0

    .line 129476
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129477
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 129478
    iget-object v0, p0, LX/0lR;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 129479
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129480
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Aq;

    .line 129481
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129482
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 129483
    const/4 v0, 0x1

    .line 129484
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs b([Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 129485
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 129486
    invoke-direct {p0, v0}, LX/0lR;->a(LX/2At;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129487
    invoke-virtual {v0, v2}, LX/2At;->a(I)Ljava/lang/Class;

    move-result-object v4

    .line 129488
    array-length v5, p1

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, p1, v1

    .line 129489
    invoke-virtual {v4, v6}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 129490
    iget-object v1, v0, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v1

    .line 129491
    :goto_1
    return-object v0

    .line 129492
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129493
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()LX/0lN;
    .locals 1

    .prologue
    .line 129494
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    return-object v0
.end method

.method public final d()LX/4qt;
    .locals 1

    .prologue
    .line 129408
    iget-object v0, p0, LX/0lR;->g:LX/4qt;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 129409
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->h()Z

    move-result v0

    return v0
.end method

.method public final f()LX/1Y3;
    .locals 3

    .prologue
    .line 129342
    iget-object v0, p0, LX/0lR;->e:LX/1Y3;

    if-nez v0, :cond_0

    .line 129343
    new-instance v0, LX/1Y3;

    iget-object v1, p0, LX/0lR;->b:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->n()LX/0li;

    move-result-object v1

    iget-object v2, p0, LX/0lS;->a:LX/0lJ;

    invoke-direct {v0, v1, v2}, LX/1Y3;-><init>(LX/0li;LX/0lJ;)V

    iput-object v0, p0, LX/0lR;->e:LX/1Y3;

    .line 129344
    :cond_0
    iget-object v0, p0, LX/0lR;->e:LX/1Y3;

    return-object v0
.end method

.method public final g()LX/0lQ;
    .locals 1

    .prologue
    .line 129345
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->g()LX/0lQ;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129346
    iget-object v0, p0, LX/0lR;->f:Ljava/util/List;

    return-object v0
.end method

.method public final i()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2An;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129347
    const/4 v1, 0x0

    .line 129348
    iget-object v0, p0, LX/0lR;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Aq;

    .line 129349
    invoke-virtual {v0}, LX/2Aq;->n()LX/2An;

    move-result-object v3

    .line 129350
    if-eqz v3, :cond_0

    .line 129351
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    invoke-virtual {v0, v3}, LX/0lU;->a(LX/2An;)LX/4pn;

    move-result-object v4

    .line 129352
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/4pn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129353
    if-nez v1, :cond_2

    .line 129354
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 129355
    :goto_1
    iget-object v1, v4, LX/4pn;->b:Ljava/lang/String;

    move-object v1, v1

    .line 129356
    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 129357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Multiple back-reference properties with name \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129358
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final j()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129359
    iget-object v0, p0, LX/0lR;->j:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 129360
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 129361
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lR;->j:Ljava/util/Set;

    goto :goto_0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Vc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129362
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->j()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129363
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->k()Ljava/util/List;

    move-result-object v0

    .line 129364
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129365
    :goto_0
    return-object v0

    .line 129366
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 129367
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 129368
    invoke-direct {p0, v0}, LX/0lR;->a(LX/2At;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 129369
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 129370
    goto :goto_0
.end method

.method public final m()LX/2Vc;
    .locals 1

    .prologue
    .line 129371
    iget-object v0, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->i()LX/2Vc;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/2An;
    .locals 3

    .prologue
    .line 129372
    iget-object v0, p0, LX/0lR;->l:LX/2An;

    if-eqz v0, :cond_0

    .line 129373
    iget-object v0, p0, LX/0lR;->l:LX/2An;

    invoke-virtual {v0}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v0

    .line 129374
    const-class v1, Ljava/util/Map;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid \'any-getter\' annotation on method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0lR;->l:LX/2An;

    invoke-virtual {v2}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(): return type is not instance of java.util.Map"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129376
    :cond_0
    iget-object v0, p0, LX/0lR;->l:LX/2An;

    return-object v0
.end method

.method public final o()LX/2At;
    .locals 4

    .prologue
    .line 129377
    iget-object v0, p0, LX/0lR;->h:LX/2At;

    if-eqz v0, :cond_0

    .line 129378
    iget-object v0, p0, LX/0lR;->h:LX/2At;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2At;->a(I)Ljava/lang/Class;

    move-result-object v0

    .line 129379
    const-class v1, Ljava/lang/String;

    if-eq v0, v1, :cond_0

    const-class v1, Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 129380
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid \'any-setter\' annotation on method "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/0lR;->h:LX/2At;

    invoke-virtual {v3}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(): first argument not of type String or Object, but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129381
    :cond_0
    iget-object v0, p0, LX/0lR;->h:LX/2At;

    return-object v0
.end method

.method public final p()LX/2At;
    .locals 1

    .prologue
    .line 129382
    iget-object v0, p0, LX/0lR;->k:LX/2At;

    return-object v0
.end method

.method public final q()LX/1Xr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129383
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129384
    const/4 v0, 0x0

    .line 129385
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->m(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0lR;->a(Ljava/lang/Object;)LX/1Xr;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()LX/1Xr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129386
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-nez v0, :cond_0

    .line 129387
    const/4 v0, 0x0

    .line 129388
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->u(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0lR;->a(Ljava/lang/Object;)LX/1Xr;

    move-result-object v0

    goto :goto_0
.end method

.method public final s()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/2An;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129389
    iget-object v0, p0, LX/0lR;->i:Ljava/util/Map;

    return-object v0
.end method

.method public final t()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129390
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->j(LX/0lN;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final u()LX/2zN;
    .locals 2

    .prologue
    .line 129391
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lR;->c:LX/0lU;

    iget-object v1, p0, LX/0lR;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->k(LX/0lN;)LX/2zN;

    move-result-object v0

    goto :goto_0
.end method
