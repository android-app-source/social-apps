.class public LX/1ra;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile p:LX/1ra;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0SG;

.field public final c:LX/0So;

.field private final d:LX/1rb;

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/0So;LX/1rb;)V
    .locals 4
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    const/high16 v0, -0x80000000

    .line 332609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332610
    iput-wide v2, p0, LX/1ra;->e:J

    .line 332611
    iput-wide v2, p0, LX/1ra;->f:J

    .line 332612
    iput-wide v2, p0, LX/1ra;->g:J

    .line 332613
    iput-wide v2, p0, LX/1ra;->h:J

    .line 332614
    iput v0, p0, LX/1ra;->i:I

    .line 332615
    iput v0, p0, LX/1ra;->j:I

    .line 332616
    iput v0, p0, LX/1ra;->k:I

    .line 332617
    iput v0, p0, LX/1ra;->l:I

    .line 332618
    iput v0, p0, LX/1ra;->m:I

    .line 332619
    iput v0, p0, LX/1ra;->n:I

    .line 332620
    iput v0, p0, LX/1ra;->o:I

    .line 332621
    iput-object p1, p0, LX/1ra;->a:LX/0Zb;

    .line 332622
    iput-object p2, p0, LX/1ra;->b:LX/0SG;

    .line 332623
    iput-object p3, p0, LX/1ra;->c:LX/0So;

    .line 332624
    iput-object p4, p0, LX/1ra;->d:LX/1rb;

    .line 332625
    return-void
.end method

.method private static a(Ljava/util/List;J)LX/0lF;
    .locals 7
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;J)",
            "LX/0lF;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 332626
    if-nez p0, :cond_0

    .line 332627
    const/4 v0, 0x0

    .line 332628
    :goto_0
    return-object v0

    .line 332629
    :cond_0
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 332630
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/wifiscan/WifiScanResult;

    .line 332631
    invoke-static {v0, p1, p2}, LX/2zX;->a(Lcom/facebook/wifiscan/WifiScanResult;J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, LX/162;->b(J)LX/162;

    goto :goto_1

    .line 332632
    :cond_1
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->c()LX/0m9;

    move-result-object v0

    .line 332633
    const-string v2, "ages_ms"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0
.end method

.method public static a(LX/1ra;Ljava/lang/String;)LX/0oG;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    .line 332647
    iget-object v0, p0, LX/1ra;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 332648
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 332649
    const/4 v0, 0x0

    .line 332650
    :cond_0
    :goto_0
    return-object v0

    .line 332651
    :cond_1
    const-string v1, "foreground_location"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 332652
    const-string v1, "device_status"

    iget-object v2, p0, LX/1ra;->d:LX/1rb;

    .line 332653
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->c()LX/0m9;

    move-result-object v3

    .line 332654
    const-string v6, "location_status"

    invoke-static {v2}, LX/1rb;->b(LX/1rb;)LX/0lF;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 332655
    const-string v6, "wifi_status"

    .line 332656
    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-virtual {v7}, LX/0mC;->c()LX/0m9;

    move-result-object v7

    .line 332657
    const-string v8, "available"

    iget-object v9, v2, LX/1rb;->b:LX/1rc;

    invoke-virtual {v9}, LX/1rc;->a()Z

    move-result v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 332658
    const-string v8, "has_permission"

    iget-object v9, v2, LX/1rb;->b:LX/1rc;

    invoke-virtual {v9}, LX/1rc;->c()Z

    move-result v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 332659
    const-string v8, "supports_timestamps"

    invoke-static {}, LX/1rc;->b()Z

    move-result v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 332660
    const-string v8, "enabled"

    iget-object v9, v2, LX/1rb;->b:LX/1rc;

    invoke-virtual {v9}, LX/1rc;->d()Z

    move-result v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 332661
    const-string v8, "can_always_scan"

    .line 332662
    iget-object v9, v2, LX/1rb;->b:LX/1rc;

    invoke-virtual {v9}, LX/1rc;->e()LX/03R;

    move-result-object v9

    .line 332663
    sget-object p1, LX/03R;->UNSET:LX/03R;

    if-ne v9, p1, :cond_3

    .line 332664
    const-string v9, "unsupported"

    .line 332665
    :goto_1
    move-object v9, v9

    .line 332666
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 332667
    move-object v7, v7

    .line 332668
    invoke-virtual {v3, v6, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 332669
    move-object v2, v3

    .line 332670
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332671
    iget-wide v2, p0, LX/1ra;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 332672
    const-string v1, "session_id"

    iget-wide v2, p0, LX/1ra;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332673
    :cond_2
    iget-wide v2, p0, LX/1ra;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 332674
    const-string v1, "request_id"

    iget-wide v2, p0, LX/1ra;->f:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    goto :goto_0

    :cond_3
    invoke-virtual {v9}, LX/03R;->asBoolean()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v9

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/1ra;
    .locals 7

    .prologue
    .line 332634
    sget-object v0, LX/1ra;->p:LX/1ra;

    if-nez v0, :cond_1

    .line 332635
    const-class v1, LX/1ra;

    monitor-enter v1

    .line 332636
    :try_start_0
    sget-object v0, LX/1ra;->p:LX/1ra;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332637
    if-eqz v2, :cond_0

    .line 332638
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332639
    new-instance p0, LX/1ra;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/1rb;->a(LX/0QB;)LX/1rb;

    move-result-object v6

    check-cast v6, LX/1rb;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1ra;-><init>(LX/0Zb;LX/0SG;LX/0So;LX/1rb;)V

    .line 332640
    move-object v0, p0

    .line 332641
    sput-object v0, LX/1ra;->p:LX/1ra;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332642
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332643
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332644
    :cond_1
    sget-object v0, LX/1ra;->p:LX/1ra;

    return-object v0

    .line 332645
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1ra;LX/0oG;LX/2TS;JJ)V
    .locals 8
    .param p1    # LX/0oG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 332562
    iget-wide v0, p0, LX/1ra;->g:J

    sub-long v0, p3, v0

    .line 332563
    const-string v2, "scan_duration_ms"

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332564
    if-nez p2, :cond_0

    .line 332565
    :goto_0
    return-void

    .line 332566
    :cond_0
    const-string v0, "location_manager_result"

    iget-object v1, p2, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v1, v1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    .line 332567
    if-nez v1, :cond_3

    .line 332568
    const/4 v3, 0x0

    .line 332569
    :goto_1
    move-object v1, v3

    .line 332570
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332571
    const-string v0, "wifi_scan_result"

    iget-object v1, p2, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v1, v1, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    invoke-static {v1, p5, p6}, LX/1ra;->a(Ljava/util/List;J)LX/0lF;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332572
    const-string v0, "connected_wifi_result"

    iget-object v1, p2, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v1, v1, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 332573
    if-nez v1, :cond_4

    .line 332574
    const/4 v2, 0x0

    .line 332575
    :goto_2
    move-object v1, v2

    .line 332576
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332577
    const-string v0, "location_manager_throwable"

    iget-object v1, p2, LX/2TS;->b:LX/2Tl;

    iget-object v1, v1, LX/2Tl;->a:Ljava/lang/Throwable;

    .line 332578
    if-nez v1, :cond_5

    .line 332579
    const/4 v2, 0x0

    .line 332580
    :cond_1
    :goto_3
    move-object v1, v2

    .line 332581
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332582
    const-string v0, "wifi_scan_throwable"

    iget-object v1, p2, LX/2TS;->b:LX/2Tl;

    iget-object v1, v1, LX/2Tl;->b:Ljava/lang/Throwable;

    .line 332583
    if-nez v1, :cond_7

    .line 332584
    const/4 v2, 0x0

    .line 332585
    :cond_2
    :goto_4
    move-object v1, v2

    .line 332586
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    goto :goto_0

    .line 332587
    :cond_3
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->c()LX/0m9;

    move-result-object v4

    .line 332588
    const-string v3, "age_ms"

    invoke-static {v1, p5, p6, p3, p4}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;JJ)J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 332589
    const-string v5, "accuracy_meters"

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v4, v5, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    move-object v3, v4

    .line 332590
    goto :goto_1

    .line 332591
    :cond_4
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->c()LX/0m9;

    move-result-object v2

    .line 332592
    const-string v3, "is_connected"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    goto :goto_2

    .line 332593
    :cond_5
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->c()LX/0m9;

    move-result-object v2

    .line 332594
    const-string v3, "class"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 332595
    instance-of v3, v1, LX/2Ce;

    if-eqz v3, :cond_6

    .line 332596
    check-cast v1, LX/2Ce;

    .line 332597
    const-string v3, "location_manager_exception_type"

    iget-object v4, v1, LX/2Ce;->type:LX/2sk;

    invoke-virtual {v4}, LX/2sk;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_3

    .line 332598
    :cond_6
    instance-of v3, v1, Ljava/util/concurrent/ExecutionException;

    if-eqz v3, :cond_1

    .line 332599
    check-cast v1, Ljava/util/concurrent/ExecutionException;

    .line 332600
    const-string v3, "execution_exception_cause_class"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_3

    .line 332601
    :cond_7
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->c()LX/0m9;

    move-result-object v2

    .line 332602
    const-string v3, "class"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 332603
    instance-of v3, v1, LX/2x2;

    if-eqz v3, :cond_8

    .line 332604
    check-cast v1, LX/2x2;

    .line 332605
    const-string v3, "wifi_scan_operation_exception_type"

    iget-object v4, v1, LX/2x2;->type:LX/2x3;

    invoke-virtual {v4}, LX/2x3;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_4

    .line 332606
    :cond_8
    instance-of v3, v1, Ljava/util/concurrent/ExecutionException;

    if-eqz v3, :cond_2

    .line 332607
    check-cast v1, Ljava/util/concurrent/ExecutionException;

    .line 332608
    const-string v3, "execution_exception_cause_class"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_4
.end method

.method public static a(LX/1ra;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332557
    iget-wide v2, p0, LX/1ra;->e:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v2, v0

    .line 332558
    :goto_0
    if-ne v2, p1, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 332559
    return-void

    :cond_0
    move v2, v1

    .line 332560
    goto :goto_0

    :cond_1
    move v0, v1

    .line 332561
    goto :goto_1
.end method

.method public static b(LX/1ra;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332497
    iget-wide v2, p0, LX/1ra;->f:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v2, v0

    .line 332498
    :goto_0
    if-ne v2, p1, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 332499
    return-void

    :cond_0
    move v2, v1

    .line 332500
    goto :goto_0

    :cond_1
    move v0, v1

    .line 332501
    goto :goto_1
.end method

.method public static c(LX/1ra;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332502
    iget-wide v2, p0, LX/1ra;->g:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v2, v0

    .line 332503
    :goto_0
    if-ne v2, p1, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 332504
    return-void

    :cond_0
    move v2, v1

    .line 332505
    goto :goto_0

    :cond_1
    move v0, v1

    .line 332506
    goto :goto_1
.end method

.method public static d(LX/1ra;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332507
    iget-wide v2, p0, LX/1ra;->h:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v2, v0

    .line 332508
    :goto_0
    if-ne v2, p1, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 332509
    return-void

    :cond_0
    move v2, v1

    .line 332510
    goto :goto_0

    :cond_1
    move v0, v1

    .line 332511
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2TS;)V
    .locals 10
    .param p1    # LX/2TS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v0, 0x1

    .line 332512
    invoke-static {p0, v0}, LX/1ra;->a(LX/1ra;Z)V

    .line 332513
    invoke-static {p0, v0}, LX/1ra;->b(LX/1ra;Z)V

    .line 332514
    invoke-static {p0, v0}, LX/1ra;->c(LX/1ra;Z)V

    .line 332515
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1ra;->d(LX/1ra;Z)V

    .line 332516
    iget v0, p0, LX/1ra;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1ra;->k:I

    .line 332517
    const-string v0, "fgl_scan_fail"

    invoke-static {p0, v0}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    .line 332518
    if-eqz v2, :cond_0

    .line 332519
    iget-object v0, p0, LX/1ra;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 332520
    iget-object v0, p0, LX/1ra;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 332521
    iget-wide v0, p0, LX/1ra;->f:J

    sub-long v0, v4, v0

    .line 332522
    const-string v3, "request_duration_ms"

    invoke-virtual {v2, v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-object v1, p0

    move-object v3, p1

    .line 332523
    invoke-static/range {v1 .. v7}, LX/1ra;->a(LX/1ra;LX/0oG;LX/2TS;JJ)V

    .line 332524
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 332525
    :cond_0
    iput-wide v8, p0, LX/1ra;->g:J

    .line 332526
    iput-wide v8, p0, LX/1ra;->f:J

    .line 332527
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v1, 0x1

    .line 332528
    invoke-static {p0, v1}, LX/1ra;->a(LX/1ra;Z)V

    .line 332529
    invoke-static {p0, v1}, LX/1ra;->b(LX/1ra;Z)V

    .line 332530
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1ra;->c(LX/1ra;Z)V

    .line 332531
    invoke-static {p0, v1}, LX/1ra;->d(LX/1ra;Z)V

    .line 332532
    iget v0, p0, LX/1ra;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1ra;->n:I

    .line 332533
    const-string v0, "fgl_write_fail"

    invoke-static {p0, v0}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 332534
    if-eqz v0, :cond_1

    .line 332535
    iget-object v1, p0, LX/1ra;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 332536
    iget-wide v4, p0, LX/1ra;->h:J

    sub-long v4, v2, v4

    .line 332537
    iget-wide v6, p0, LX/1ra;->f:J

    sub-long/2addr v2, v6

    .line 332538
    const-string v1, "write_duration_ms"

    invoke-virtual {v0, v1, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332539
    const-string v1, "request_duration_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332540
    const-string v1, "write_throwable"

    .line 332541
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->c()LX/0m9;

    move-result-object v2

    .line 332542
    const-string v3, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 332543
    instance-of v3, p1, LX/4Ua;

    if-eqz v3, :cond_2

    .line 332544
    check-cast p1, LX/4Ua;

    .line 332545
    iget-object v3, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v3, :cond_0

    .line 332546
    const-string v3, "graphql_error_code"

    iget-object v4, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v4, v4, Lcom/facebook/graphql/error/GraphQLError;->code:I

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 332547
    :cond_0
    :goto_0
    move-object v2, v2

    .line 332548
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332549
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 332550
    :cond_1
    iput-wide v8, p0, LX/1ra;->h:J

    .line 332551
    iput-wide v8, p0, LX/1ra;->g:J

    .line 332552
    iput-wide v8, p0, LX/1ra;->f:J

    .line 332553
    return-void

    .line 332554
    :cond_2
    instance-of v3, p1, Ljava/util/concurrent/ExecutionException;

    if-eqz v3, :cond_0

    .line 332555
    check-cast p1, Ljava/util/concurrent/ExecutionException;

    .line 332556
    const-string v3, "execution_exception_cause_class"

    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0
.end method
