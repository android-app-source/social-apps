.class public final LX/1jg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public f:Z

.field public g:Z

.field public h:D

.field public i:D

.field public j:I

.field public k:I

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJDDIILjava/lang/String;)V
    .locals 4

    .prologue
    .line 300741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300742
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1jg;->g:Z

    .line 300743
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "cursor is null"

    invoke-static {v1, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 300744
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "sortKey is null"

    invoke-static {v1, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 300745
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    const-string v2, "dedupKey is null"

    invoke-static {v1, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 300746
    iput-object p1, p0, LX/1jg;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300747
    iput-object p2, p0, LX/1jg;->b:Ljava/lang/String;

    .line 300748
    iput-object p3, p0, LX/1jg;->c:Ljava/lang/String;

    .line 300749
    iput-object p4, p0, LX/1jg;->d:Ljava/lang/String;

    .line 300750
    iput-boolean p5, p0, LX/1jg;->f:Z

    .line 300751
    iput-wide p6, p0, LX/1jg;->e:J

    .line 300752
    iput-wide p8, p0, LX/1jg;->h:D

    .line 300753
    iput-wide p10, p0, LX/1jg;->i:D

    .line 300754
    move/from16 v0, p12

    iput v0, p0, LX/1jg;->j:I

    .line 300755
    move/from16 v0, p13

    iput v0, p0, LX/1jg;->k:I

    .line 300756
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1jg;->l:Ljava/lang/String;

    .line 300757
    return-void

    .line 300758
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 300759
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 300760
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method
