.class public LX/13B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176090
    iput-object p1, p0, LX/13B;->a:LX/0Zb;

    .line 176091
    return-void
.end method

.method public static a(LX/13B;Ljava/lang/String;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 176084
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "search_awareness_unit_impression"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176085
    const-string v1, "template"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176086
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176087
    iget-object v1, p0, LX/13B;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176088
    return-void
.end method

.method public static b(LX/0QB;)LX/13B;
    .locals 2

    .prologue
    .line 176082
    new-instance v1, LX/13B;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/13B;-><init>(LX/0Zb;)V

    .line 176083
    return-object v1
.end method

.method public static b(LX/13B;Ljava/lang/String;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 176077
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "search_awareness_unit_dismissed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176078
    const-string v1, "template"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176079
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176080
    iget-object v1, p0, LX/13B;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176081
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 176043
    const-string v0, "TUTORIAL_NUX"

    const-string v1, "card_shown"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/13B;->a(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176044
    return-void
.end method

.method public final a(LX/CwT;)V
    .locals 1

    .prologue
    .line 176074
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 176075
    invoke-virtual {p0, p1, v0}, LX/13B;->b(LX/CwT;LX/0P1;)V

    .line 176076
    return-void
.end method

.method public final a(LX/CwT;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 176060
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 176061
    invoke-virtual {v0, p2}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 176062
    const-string v1, "unit_id"

    .line 176063
    iget-object v2, p1, LX/CwT;->a:LX/A0Z;

    move-object v2, v2

    .line 176064
    invoke-interface {v2}, LX/A0Z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 176065
    const-string v1, "root_unit_id"

    .line 176066
    iget-object v2, p1, LX/CwT;->f:Ljava/lang/String;

    move-object v2, v2

    .line 176067
    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 176068
    const-string v1, "test_name"

    .line 176069
    iget-object v2, p1, LX/CwT;->d:Ljava/lang/String;

    move-object v2, v2

    .line 176070
    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 176071
    iget-object v1, p1, LX/CwT;->a:LX/A0Z;

    move-object v1, v1

    .line 176072
    invoke-interface {v1}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/13B;->a(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176073
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 176058
    const-string v0, "GROUPS_PILL"

    const-string v1, "was_clicked"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/13B;->b(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176059
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 176056
    const-string v0, "SPOTLIGHT"

    const-string v1, "card_shown"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/13B;->a(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176057
    return-void
.end method

.method public final b(LX/CwT;LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 176047
    iget-object v0, p1, LX/CwT;->a:LX/A0Z;

    move-object v0, v0

    .line 176048
    invoke-interface {v0}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v1

    const-string v2, "unit_id"

    .line 176049
    iget-object v3, p1, LX/CwT;->a:LX/A0Z;

    move-object v3, v3

    .line 176050
    invoke-interface {v3}, LX/A0Z;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "root_unit_id"

    .line 176051
    iget-object v3, p1, LX/CwT;->f:Ljava/lang/String;

    move-object v3, v3

    .line 176052
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "test_name"

    .line 176053
    iget-object v3, p1, LX/CwT;->d:Ljava/lang/String;

    move-object v3, v3

    .line 176054
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/13B;->b(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176055
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 176045
    const-string v0, "TUTORIAL_NUX"

    const-string v1, "action"

    invoke-static {v1, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/13B;->b(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 176046
    return-void
.end method
