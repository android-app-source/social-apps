.class public final LX/0bc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/0bd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87086
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0bc;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87085
    return-void
.end method

.method public static declared-synchronized a()V
    .locals 8

    .prologue
    .line 87087
    const-class v2, LX/0bc;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/0bc;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 87088
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87089
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bd;

    .line 87090
    iget-object v1, v0, LX/0bd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/sqlite/SQLiteDatabase;

    .line 87091
    if-nez v1, :cond_1

    .line 87092
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87093
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 87094
    :cond_1
    :try_start_1
    iget-wide v4, v0, LX/0bd;->b:J

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    .line 87095
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 87096
    invoke-virtual {v0}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-lez v0, :cond_0

    .line 87097
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 87098
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87099
    :cond_2
    monitor-exit v2

    return-void
.end method

.method public static declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 87081
    const-class v1, LX/0bc;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0bc;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/0bd;

    invoke-direct {v3, p0}, LX/0bd;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87082
    monitor-exit v1

    return-void

    .line 87083
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 87071
    const-class v1, LX/0bc;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0bc;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87072
    monitor-exit v1

    return-void

    .line 87073
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 87074
    const-class v1, LX/0bc;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0bc;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bd;

    .line 87075
    if-eqz v0, :cond_0

    .line 87076
    sget-object v2, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v2, v2

    .line 87077
    invoke-virtual {v2}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/0bd;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87078
    :goto_0
    monitor-exit v1

    return-void

    .line 87079
    :cond_0
    :try_start_1
    sget-object v0, LX/0Tr;->a:Ljava/lang/Class;

    const-string v2, "Database was not found"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87080
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
