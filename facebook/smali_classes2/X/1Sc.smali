.class public LX/1Sc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Sd;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(LX/1Sd;Ljava/util/concurrent/ExecutorService;LX/0SG;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 248737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248738
    iput-object p1, p0, LX/1Sc;->a:LX/1Sd;

    .line 248739
    iput-object p2, p0, LX/1Sc;->b:Ljava/util/concurrent/ExecutorService;

    .line 248740
    iput-object p3, p0, LX/1Sc;->c:LX/0SG;

    .line 248741
    return-void
.end method

.method public static a(LX/0QB;)LX/1Sc;
    .locals 1

    .prologue
    .line 248736
    invoke-static {p0}, LX/1Sc;->b(LX/0QB;)LX/1Sc;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 248716
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 248717
    const-string v2, "section_name"

    invoke-virtual {v1, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248718
    const-string v2, "node_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248719
    const-string v2, "time_saved_ms"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 248720
    const-string v2, "group_title"

    const-string v3, "TODAY"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248721
    const-string v2, "picture_uri"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248722
    const-string v2, "title"

    invoke-virtual {v1, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248723
    const-string v2, "type"

    invoke-virtual {v1, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248724
    const-string v2, "playable_url"

    invoke-virtual {v1, v2, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248725
    const-string v2, "is_playable"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248726
    const-string v2, "media_content_size"

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 248727
    const-string v2, "section_name_server"

    invoke-virtual {v1, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248728
    const-string v2, "saved_state"

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248729
    const-string v2, "source_story_id"

    invoke-virtual {v1, v2, p10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248730
    const-string v2, "source_container_category"

    const-string v3, "POST"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248731
    const-string v2, "is_download_client"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248732
    const-string v2, "source_actor_name"

    invoke-virtual {v1, v2, p11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248733
    const-string v2, "source_actor_short_name"

    move-object/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248734
    const-string v2, "source_actor_picture_uri"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248735
    return-object v1
.end method

.method public static a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 248714
    iget-object v6, p0, LX/1Sc;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;-><init>(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    const v1, 0x22700a56

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 248715
    return-void
.end method

.method public static a(LX/1Sc;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 248712
    iget-object v0, p0, LX/1Sc;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$1;-><init>(LX/1Sc;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/Object;)V

    const v2, 0x148a6074

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 248713
    return-void
.end method

.method public static b(LX/0QB;)LX/1Sc;
    .locals 4

    .prologue
    .line 248710
    new-instance v3, LX/1Sc;

    invoke-static {p0}, LX/1Sd;->a(LX/0QB;)LX/1Sd;

    move-result-object v0

    check-cast v0, LX/1Sd;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-direct {v3, v0, v1, v2}, LX/1Sc;-><init>(LX/1Sd;Ljava/util/concurrent/ExecutorService;LX/0SG;)V

    .line 248711
    return-object v3
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 248742
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "section_name"

    aput-object v1, v0, v3

    const-string v1, "saved_state"

    aput-object v1, v0, v4

    const-string v1, "time_saved_ms"

    aput-object v1, v0, v5

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "ARCHIVED"

    aput-object v2, v1, v3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, LX/1Sc;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p0, p1, p2, v0, v1}, LX/1Sc;->a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248743
    return-void
.end method

.method public final a(JLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248708
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "section_name"

    aput-object v1, v0, v2

    const-string v1, "saved_state"

    aput-object v1, v0, v3

    const-string v1, "time_saved_ms"

    aput-object v1, v0, v4

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p3, v1, v2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, LX/1Sc;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, p1, p2, v0, v1}, LX/1Sc;->a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248709
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 248695
    iget-object v0, p0, LX/1Sc;->a:LX/1Sd;

    invoke-virtual {v0}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 248696
    const v0, -0x30dcd1be

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248697
    :try_start_0
    const-string v0, "instant_article_id=?"

    .line 248698
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 248699
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 248700
    const-string v4, "instant_article_last_read_block_id"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248701
    const-string v4, "item"

    invoke-virtual {v1, v4, v3, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 248702
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248703
    const v0, 0x7dc77cd7

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248704
    sget-object v0, LX/AUM;->a:LX/AUM;

    move-object v0, v0

    .line 248705
    const-class v1, LX/BNx;

    invoke-virtual {v0, v1}, LX/AUM;->a(Ljava/lang/Object;)V

    .line 248706
    return-void

    .line 248707
    :catchall_0
    move-exception v0

    const v2, -0x4e0b70d6

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 248641
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Sc;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 248642
    const-string v2, "VIDEOS"

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    invoke-static/range {v2 .. v15}, LX/1Sc;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v16

    .line 248643
    const-string v2, "ALL"

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    invoke-static/range {v2 .. v15}, LX/1Sc;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 248644
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1Sc;->a:LX/1Sd;

    invoke-virtual {v3}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 248645
    const v4, 0x5ee41925

    invoke-static {v3, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248646
    :try_start_0
    const-string v4, "node_id=? AND section_name=?"

    .line 248647
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    const-string v7, "ARCHIVED"

    aput-object v7, v5, v6

    .line 248648
    const-string v6, "item"

    invoke-virtual {v3, v6, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 248649
    const-string v4, "node_id=?"

    .line 248650
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    .line 248651
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 248652
    const-string v7, "is_download_client"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248653
    const-string v7, "item"

    invoke-virtual {v3, v7, v6, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 248654
    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 248655
    const-string v4, "item"

    const/4 v5, 0x0

    const/4 v6, 0x4

    const v7, -0x1cb07de6

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v3, v4, v5, v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v2, 0x6786d485

    invoke-static {v2}, LX/03h;->a(I)V

    .line 248656
    const-string v2, "item"

    const/4 v4, 0x0

    const/4 v5, 0x4

    const v6, 0x23581e46

    invoke-static {v6}, LX/03h;->a(I)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v4, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v2, -0x24b51fdb

    invoke-static {v2}, LX/03h;->a(I)V

    .line 248657
    :cond_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248658
    const v2, 0x4b132f4

    invoke-static {v3, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248659
    invoke-static {}, LX/AUM;->a()LX/AUM;

    move-result-object v2

    const-class v3, LX/BNx;

    invoke-virtual {v2, v3}, LX/AUM;->a(Ljava/lang/Object;)V

    .line 248660
    return-void

    .line 248661
    :catchall_0
    move-exception v2

    const v4, -0x7ac95580

    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 248693
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "is_deleted_client"

    aput-object v1, v0, v3

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, p1, v0, v1}, LX/1Sc;->a(LX/1Sc;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248694
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 248680
    iget-object v0, p0, LX/1Sc;->a:LX/1Sd;

    invoke-virtual {v0}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 248681
    const v0, 0x5f5282c9

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248682
    :try_start_0
    const-string v0, "node_id=?"

    .line 248683
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 248684
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 248685
    const-string v4, "is_download_client"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248686
    const-string v4, "item"

    invoke-virtual {v1, v4, v3, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 248687
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248688
    const v0, -0x1f744765

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248689
    sget-object v0, LX/AUM;->a:LX/AUM;

    move-object v0, v0

    .line 248690
    const-class v1, LX/BNx;

    invoke-virtual {v0, v1}, LX/AUM;->a(Ljava/lang/Object;)V

    .line 248691
    return-void

    .line 248692
    :catchall_0
    move-exception v0

    const v2, 0x2658c138

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 248664
    iget-object v0, p0, LX/1Sc;->a:LX/1Sd;

    invoke-virtual {v0}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 248665
    const v1, 0x75a72480

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248666
    const-string v9, ""

    .line 248667
    :try_start_0
    const-string v3, "node_id=?"

    .line 248668
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 248669
    const-string v1, "item"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 248670
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248671
    const-string v1, "source_actor_name"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 248672
    :goto_0
    const v3, -0xa53fa81

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248673
    if-eqz v2, :cond_0

    .line 248674
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 248675
    :cond_0
    return-object v1

    .line 248676
    :catchall_0
    move-exception v1

    move-object v2, v10

    :goto_1
    const v3, 0x433c5116

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248677
    if-eqz v2, :cond_1

    .line 248678
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v1

    .line 248679
    :catchall_1
    move-exception v1

    goto :goto_1

    :cond_2
    move-object v1, v9

    goto :goto_0
.end method

.method public final e(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 248662
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "is_item_viewed"

    aput-object v1, v0, v3

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, p1, p2, v0, v1}, LX/1Sc;->a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248663
    return-void
.end method
