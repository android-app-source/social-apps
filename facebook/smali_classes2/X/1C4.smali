.class public LX/1C4;
.super LX/1C5;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile c:LX/1C4;


# instance fields
.field private final b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215360
    const-class v0, LX/1C4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1C4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215361
    invoke-direct {p0, p1, p2}, LX/1C5;-><init>(LX/0So;LX/03V;)V

    .line 215362
    iput-object p2, p0, LX/1C4;->b:LX/03V;

    .line 215363
    return-void
.end method

.method public static a(LX/0QB;)LX/1C4;
    .locals 5

    .prologue
    .line 215364
    sget-object v0, LX/1C4;->c:LX/1C4;

    if-nez v0, :cond_1

    .line 215365
    const-class v1, LX/1C4;

    monitor-enter v1

    .line 215366
    :try_start_0
    sget-object v0, LX/1C4;->c:LX/1C4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 215367
    if-eqz v2, :cond_0

    .line 215368
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 215369
    new-instance p0, LX/1C4;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/1C4;-><init>(LX/0So;LX/03V;)V

    .line 215370
    move-object v0, p0

    .line 215371
    sput-object v0, LX/1C4;->c:LX/1C4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215372
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 215373
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215374
    :cond_1
    sget-object v0, LX/1C4;->c:LX/1C4;

    return-object v0

    .line 215375
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 215376
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7KB;)V
    .locals 4

    .prologue
    .line 215377
    iget-object v0, p0, LX/1C4;->b:LX/03V;

    sget-object v1, LX/1C4;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid video-session:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/7KB;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215378
    return-void
.end method
