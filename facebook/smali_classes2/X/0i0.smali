.class public LX/0i0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0i0;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0iU;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iU;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0iU;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 119400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119401
    iput-object p1, p0, LX/0i0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 119402
    iput-object p2, p0, LX/0i0;->b:LX/0iU;

    .line 119403
    iput-object p3, p0, LX/0i0;->c:LX/0Or;

    .line 119404
    return-void
.end method

.method public static a(LX/0QB;)LX/0i0;
    .locals 7

    .prologue
    .line 119384
    sget-object v0, LX/0i0;->d:LX/0i0;

    if-nez v0, :cond_1

    .line 119385
    const-class v1, LX/0i0;

    monitor-enter v1

    .line 119386
    :try_start_0
    sget-object v0, LX/0i0;->d:LX/0i0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 119387
    if-eqz v2, :cond_0

    .line 119388
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 119389
    new-instance v5, LX/0i0;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 119390
    new-instance p0, LX/0iU;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v4, v6}, LX/0iU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/res/Resources;)V

    .line 119391
    move-object v4, p0

    .line 119392
    check-cast v4, LX/0iU;

    const/16 v6, 0xb

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct {v5, v3, v4, v6}, LX/0i0;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iU;LX/0Or;)V

    .line 119393
    move-object v0, v5

    .line 119394
    sput-object v0, LX/0i0;->d:LX/0i0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119395
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 119396
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119397
    :cond_1
    sget-object v0, LX/0i0;->d:LX/0i0;

    return-object v0

    .line 119398
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 119399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static f(LX/0i0;)LX/10S;
    .locals 3

    .prologue
    .line 119383
    iget-object v0, p0, LX/0i0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/10R;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 119382
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 119381
    invoke-static {p0}, LX/0i0;->f(LX/0i0;)LX/10S;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 119368
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 119380
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119371
    const-string v0, "AppTabInterstitialController.setup"

    const v1, 0x65492a0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 119372
    :try_start_0
    invoke-static {p0}, LX/0i0;->f(LX/0i0;)LX/10S;

    move-result-object v0

    sget-object v1, LX/10S;->ELIGIBLE:LX/10S;

    if-ne v0, v1, :cond_0

    .line 119373
    iget-object v0, p0, LX/0i0;->b:LX/0iU;

    .line 119374
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v0, LX/0iV;->c:Landroid/view/ViewGroup;

    .line 119375
    if-eqz p2, :cond_0

    .line 119376
    iput-object p2, v0, LX/0iV;->b:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119377
    :cond_0
    const v0, -0x21e96eb9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 119378
    return-void

    .line 119379
    :catchall_0
    move-exception v0

    const v1, -0x32252139

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119370
    const-string v0, "1862"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119369
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    iget-object v0, p0, LX/0i0;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;Ljava/lang/String;)V

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CONFIGURATION_CHANGED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    iget-object v0, p0, LX/0i0;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
