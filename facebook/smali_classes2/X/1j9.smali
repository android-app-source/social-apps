.class public final LX/1j9;
.super LX/0eW;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadArgumentPlacement",
        "BadClosingBracePlacement",
        "YodaConditions"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300028
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 3

    .prologue
    .line 300027
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 3

    .prologue
    .line 300026
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 3

    .prologue
    .line 300025
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 3

    .prologue
    .line 300024
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 300023
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 3

    .prologue
    .line 300022
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final g()J
    .locals 3

    .prologue
    .line 300021
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final h()J
    .locals 3

    .prologue
    .line 300014
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 3

    .prologue
    .line 300020
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()J
    .locals 3

    .prologue
    .line 300019
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final k()J
    .locals 3

    .prologue
    .line 300018
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 3

    .prologue
    .line 300017
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 3

    .prologue
    .line 300016
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 3

    .prologue
    .line 300015
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
