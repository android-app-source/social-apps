.class public LX/1Os;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ou;

.field public final b:LX/1Ov;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ou;)V
    .locals 1

    .prologue
    .line 242511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242512
    iput-object p1, p0, LX/1Os;->a:LX/1Ou;

    .line 242513
    new-instance v0, LX/1Ov;

    invoke-direct {v0}, LX/1Ov;-><init>()V

    iput-object v0, p0, LX/1Os;->b:LX/1Ov;

    .line 242514
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Os;->c:Ljava/util/List;

    .line 242515
    return-void
.end method

.method public static e(LX/1Os;I)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 242499
    if-gez p1, :cond_1

    move v0, v1

    .line 242500
    :cond_0
    :goto_0
    return v0

    .line 242501
    :cond_1
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0}, LX/1Ou;->a()I

    move-result v2

    move v0, p1

    .line 242502
    :goto_1
    if-ge v0, v2, :cond_3

    .line 242503
    iget-object v3, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v3, v0}, LX/1Ov;->d(I)I

    move-result v3

    .line 242504
    sub-int v3, v0, v3

    sub-int v3, p1, v3

    .line 242505
    if-nez v3, :cond_2

    .line 242506
    :goto_2
    iget-object v1, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1, v0}, LX/1Ov;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242507
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 242508
    :cond_2
    add-int/2addr v0, v3

    .line 242509
    goto :goto_1

    :cond_3
    move v0, v1

    .line 242510
    goto :goto_0
.end method

.method private f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 242496
    iget-object v0, p0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242497
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0, p1}, LX/1Ou;->c(Landroid/view/View;)V

    .line 242498
    return-void
.end method

.method public static g(LX/1Os;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 242492
    iget-object v0, p0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242493
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0, p1}, LX/1Ou;->d(Landroid/view/View;)V

    .line 242494
    const/4 v0, 0x1

    .line 242495
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V
    .locals 2

    .prologue
    .line 242484
    if-gez p2, :cond_1

    .line 242485
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0}, LX/1Ou;->a()I

    move-result v0

    .line 242486
    :goto_0
    iget-object v1, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1, v0, p4}, LX/1Ov;->a(IZ)V

    .line 242487
    if-eqz p4, :cond_0

    .line 242488
    invoke-direct {p0, p1}, LX/1Os;->f(Landroid/view/View;)V

    .line 242489
    :cond_0
    iget-object v1, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1, p1, v0, p3}, LX/1Ou;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 242490
    return-void

    .line 242491
    :cond_1
    invoke-static {p0, p2}, LX/1Os;->e(LX/1Os;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;IZ)V
    .locals 2

    .prologue
    .line 242476
    if-gez p2, :cond_1

    .line 242477
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0}, LX/1Ou;->a()I

    move-result v0

    .line 242478
    :goto_0
    iget-object v1, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1, v0, p3}, LX/1Ov;->a(IZ)V

    .line 242479
    if-eqz p3, :cond_0

    .line 242480
    invoke-direct {p0, p1}, LX/1Os;->f(Landroid/view/View;)V

    .line 242481
    :cond_0
    iget-object v1, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1, p1, v0}, LX/1Ou;->a(Landroid/view/View;I)V

    .line 242482
    return-void

    .line 242483
    :cond_1
    invoke-static {p0, p2}, LX/1Os;->e(LX/1Os;I)I

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 242516
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0}, LX/1Ou;->a()I

    move-result v0

    iget-object v1, p0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final b(Landroid/view/View;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 242471
    iget-object v1, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1, p1}, LX/1Ou;->a(Landroid/view/View;)I

    move-result v1

    .line 242472
    if-ne v1, v0, :cond_1

    .line 242473
    :cond_0
    :goto_0
    return v0

    .line 242474
    :cond_1
    iget-object v2, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v2, v1}, LX/1Ov;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 242475
    iget-object v0, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v0, v1}, LX/1Ov;->d(I)I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 242459
    invoke-static {p0, p1}, LX/1Os;->e(LX/1Os;I)I

    move-result v0

    .line 242460
    iget-object v1, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1, v0}, LX/1Ou;->b(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 242470
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0}, LX/1Ou;->a()I

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 242469
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0, p1}, LX/1Ou;->b(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 242468
    iget-object v0, p0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 242462
    iget-object v0, p0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v0, p1}, LX/1Ou;->a(Landroid/view/View;)I

    move-result v0

    .line 242463
    if-gez v0, :cond_0

    .line 242464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "view is not a child, cannot hide "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242465
    :cond_0
    iget-object v1, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1, v0}, LX/1Ov;->a(I)V

    .line 242466
    invoke-direct {p0, p1}, LX/1Os;->f(Landroid/view/View;)V

    .line 242467
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 242461
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1}, LX/1Ov;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hidden list:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
