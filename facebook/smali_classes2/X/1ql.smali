.class public final LX/1ql;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Runnable;

.field public final synthetic b:LX/1r1;

.field private final c:Landroid/os/PowerManager$WakeLock;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private f:I

.field public final g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:J

.field private n:J


# direct methods
.method public constructor <init>(LX/1r1;Landroid/os/PowerManager$WakeLock;ILjava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 331437
    iput-object p1, p0, LX/1ql;->b:LX/1r1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331438
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ql;->h:Z

    .line 331439
    iput-boolean v1, p0, LX/1ql;->i:Z

    .line 331440
    iput v1, p0, LX/1ql;->l:I

    .line 331441
    iput-wide v2, p0, LX/1ql;->m:J

    .line 331442
    iput-wide v2, p0, LX/1ql;->n:J

    .line 331443
    new-instance v0, Lcom/facebook/common/wakelock/FbWakeLockManager$WakeLock$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/wakelock/FbWakeLockManager$WakeLock$1;-><init>(LX/1ql;)V

    iput-object v0, p0, LX/1ql;->a:Ljava/lang/Runnable;

    .line 331444
    iput-object p2, p0, LX/1ql;->c:Landroid/os/PowerManager$WakeLock;

    .line 331445
    iput p3, p0, LX/1ql;->g:I

    .line 331446
    iput-object p4, p0, LX/1ql;->d:Ljava/lang/String;

    .line 331447
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1ql;->k:Ljava/util/Map;

    .line 331448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1ql;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1ql;->e:Ljava/lang/String;

    .line 331449
    return-void
.end method

.method public static declared-synchronized k(LX/1ql;)V
    .locals 1

    .prologue
    .line 331382
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331383
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ql;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331384
    :goto_0
    monitor-exit p0

    return-void

    .line 331385
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/1ql;->h:Z

    if-eqz v0, :cond_1

    .line 331386
    :goto_1
    iget v0, p0, LX/1ql;->f:I

    if-lez v0, :cond_2

    .line 331387
    invoke-virtual {p0}, LX/1ql;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 331388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 331389
    :cond_1
    :try_start_2
    invoke-virtual {p0}, LX/1ql;->d()V

    .line 331390
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ql;->i:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 331434
    iget-boolean v0, p0, LX/1ql;->i:Z

    if-eqz v0, :cond_0

    .line 331435
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "WakeLock already disposed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331436
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 331423
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1ql;->l()V

    .line 331424
    iget-boolean v0, p0, LX/1ql;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/1ql;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/1ql;->f:I

    if-nez v0, :cond_2

    .line 331425
    :cond_0
    iget v0, p0, LX/1ql;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1ql;->l:I

    .line 331426
    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 331427
    iget-object v0, p0, LX/1ql;->b:LX/1r1;

    iget-object v0, v0, LX/1r1;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1ql;->m:J

    .line 331428
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ql;->j:Z

    .line 331429
    :cond_2
    iget-object v0, p0, LX/1ql;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 331430
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_3

    .line 331431
    iget-object v0, p0, LX/1ql;->b:LX/1r1;

    iget-object v0, v0, LX/1r1;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/1ql;->a:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331432
    :cond_3
    monitor-exit p0

    return-void

    .line 331433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 331416
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    const-string v1, "only add to extra if the wakelock is being held"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 331417
    iget-object v0, p0, LX/1ql;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 331418
    if-nez v0, :cond_0

    .line 331419
    iget-object v0, p0, LX/1ql;->k:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331420
    :goto_0
    monitor-exit p0

    return-void

    .line 331421
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/1ql;->k:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 331422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 331410
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 331411
    :try_start_0
    iget-boolean v0, p0, LX/1ql;->h:Z

    const-string v1, "Wake lock cannot go from non-refcounted to refcounted"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 331412
    :cond_0
    iput-boolean p1, p0, LX/1ql;->h:Z

    .line 331413
    iget-object v0, p0, LX/1ql;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331414
    monitor-exit p0

    return-void

    .line 331415
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331450
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ql;->k:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 331407
    monitor-enter p0

    const-wide/16 v0, -0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, LX/1ql;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331408
    monitor-exit p0

    return-void

    .line 331409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 6

    .prologue
    .line 331398
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1ql;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 331399
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 331400
    :cond_1
    :try_start_1
    iget-boolean v0, p0, LX/1ql;->h:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/1ql;->f:I

    if-eqz v0, :cond_0

    .line 331401
    :cond_2
    iget-object v0, p0, LX/1ql;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 331402
    iget-boolean v0, p0, LX/1ql;->h:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/1ql;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1ql;->f:I

    if-nez v0, :cond_0

    .line 331403
    :cond_3
    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 331404
    iget-wide v0, p0, LX/1ql;->n:J

    iget-object v2, p0, LX/1ql;->b:LX/1r1;

    iget-object v2, v2, LX/1r1;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/1ql;->m:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1ql;->n:J

    .line 331405
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ql;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 331406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 331397
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1ql;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 331396
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1ql;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()J
    .locals 4

    .prologue
    .line 331392
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331393
    iget-wide v0, p0, LX/1ql;->n:J

    iget-object v2, p0, LX/1ql;->b:LX/1r1;

    iget-object v2, v2, LX/1r1;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/1ql;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    .line 331394
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/1ql;->n:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 331395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()I
    .locals 1

    .prologue
    .line 331391
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1ql;->l:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
