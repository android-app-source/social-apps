.class public LX/1dL;
.super LX/1dK;
.source ""


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field public e:Landroid/graphics/Rect;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 285153
    invoke-direct {p0}, LX/1dK;-><init>()V

    .line 285154
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1dL;->a:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 285152
    iget v0, p0, LX/1dL;->d:I

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285151
    iget-object v0, p0, LX/1dL;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/1dc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285149
    iput-object p1, p0, LX/1dL;->b:LX/1dc;

    .line 285150
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 285147
    iput-object p1, p0, LX/1dL;->f:Ljava/lang/String;

    .line 285148
    return-void
.end method

.method public final b(IIII)V
    .locals 1

    .prologue
    .line 285143
    iget-object v0, p0, LX/1dL;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 285144
    invoke-static {}, LX/1cy;->n()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/1dL;->e:Landroid/graphics/Rect;

    .line 285145
    :cond_0
    iget-object v0, p0, LX/1dL;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 285146
    return-void
.end method

.method public final b(LX/1dc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285124
    iput-object p1, p0, LX/1dL;->c:LX/1dc;

    .line 285125
    return-void
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 285155
    const/4 v0, 0x0

    .line 285156
    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    if-nez v1, :cond_2

    .line 285157
    :cond_0
    :goto_0
    move v0, v0

    .line 285158
    if-nez v0, :cond_1

    .line 285159
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 285160
    :goto_1
    return-void

    .line 285161
    :cond_1
    invoke-virtual {p0, p1}, LX/1dK;->a(Landroid/graphics/Rect;)V

    .line 285162
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 285163
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 285164
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 285165
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-nez v1, :cond_3

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_3

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-nez v1, :cond_3

    iget-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(IIII)V
    .locals 1

    .prologue
    .line 285141
    iget-object v0, p0, LX/1dL;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 285142
    return-void
.end method

.method public final g(I)V
    .locals 0

    .prologue
    .line 285139
    iput p1, p0, LX/1dL;->d:I

    .line 285140
    return-void
.end method

.method public final w()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 285129
    invoke-super {p0}, LX/1dK;->w()V

    .line 285130
    iget-object v0, p0, LX/1dL;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 285131
    iput-object v1, p0, LX/1dL;->b:LX/1dc;

    .line 285132
    iput-object v1, p0, LX/1dL;->c:LX/1dc;

    .line 285133
    const/4 v0, 0x0

    iput v0, p0, LX/1dL;->d:I

    .line 285134
    iput-object v1, p0, LX/1dL;->f:Ljava/lang/String;

    .line 285135
    iget-object v0, p0, LX/1dL;->e:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 285136
    iget-object v0, p0, LX/1dL;->e:Landroid/graphics/Rect;

    invoke-static {v0}, LX/1cy;->a(Landroid/graphics/Rect;)V

    .line 285137
    iput-object v1, p0, LX/1dL;->e:Landroid/graphics/Rect;

    .line 285138
    :cond_0
    return-void
.end method

.method public final x()LX/1dc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285128
    iget-object v0, p0, LX/1dL;->b:LX/1dc;

    return-object v0
.end method

.method public final y()LX/1dc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285127
    iget-object v0, p0, LX/1dL;->c:LX/1dc;

    return-object v0
.end method

.method public final z()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 285126
    iget-object v0, p0, LX/1dL;->a:Landroid/graphics/Rect;

    return-object v0
.end method
