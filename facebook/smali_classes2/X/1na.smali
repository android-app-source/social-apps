.class public final LX/1na;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/1na;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1ne;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/1nc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 316828
    const/4 v0, 0x0

    sput-object v0, LX/1na;->a:LX/1na;

    .line 316829
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1na;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 316819
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 316820
    new-instance v0, LX/1nc;

    invoke-direct {v0}, LX/1nc;-><init>()V

    iput-object v0, p0, LX/1na;->c:LX/1nc;

    .line 316821
    return-void
.end method

.method public static a(LX/1De;II)LX/1ne;
    .locals 2

    .prologue
    .line 316830
    new-instance v0, LX/1nb;

    invoke-direct {v0}, LX/1nb;-><init>()V

    .line 316831
    sget-object v1, LX/1na;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ne;

    .line 316832
    if-nez v1, :cond_0

    .line 316833
    new-instance v1, LX/1ne;

    invoke-direct {v1}, LX/1ne;-><init>()V

    .line 316834
    :cond_0
    invoke-static {v1, p0, p1, p2, v0}, LX/1ne;->a$redex0(LX/1ne;LX/1De;IILX/1nb;)V

    .line 316835
    move-object v0, v1

    .line 316836
    return-object v0
.end method

.method public static c(LX/1De;)LX/1ne;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 316837
    invoke-static {p0, v0, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/1na;
    .locals 2

    .prologue
    .line 316838
    const-class v1, LX/1na;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1na;->a:LX/1na;

    if-nez v0, :cond_0

    .line 316839
    new-instance v0, LX/1na;

    invoke-direct {v0}, LX/1na;-><init>()V

    sput-object v0, LX/1na;->a:LX/1na;

    .line 316840
    :cond_0
    sget-object v0, LX/1na;->a:LX/1na;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 316841
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(IILX/1X1;)I
    .locals 5

    .prologue
    .line 316842
    check-cast p3, LX/1nb;

    .line 316843
    iget-object v1, p3, LX/1nb;->a:Ljava/lang/CharSequence;

    iget-object v2, p3, LX/1nb;->w:Landroid/text/Layout;

    iget-object v0, p3, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 316844
    check-cast v1, Landroid/text/Spanned;

    .line 316845
    const/4 v3, 0x0

    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_1

    .line 316846
    aget-object v4, v0, v3

    .line 316847
    invoke-interface {v1, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result p0

    .line 316848
    invoke-interface {v1, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    .line 316849
    sget-object p3, LX/1nc;->i:Landroid/graphics/Path;

    invoke-virtual {v2, p0, v4, p3}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 316850
    sget-object v4, LX/1nc;->i:Landroid/graphics/Path;

    sget-object p0, LX/1nc;->k:Landroid/graphics/RectF;

    const/4 p3, 0x1

    invoke-virtual {v4, p0, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 316851
    sget-object v4, LX/1nc;->k:Landroid/graphics/RectF;

    int-to-float p0, p1

    int-to-float p3, p2

    invoke-virtual {v4, p0, p3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 316852
    :goto_1
    move v0, v3

    .line 316853
    return v0

    .line 316854
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 316855
    :cond_1
    const/high16 v3, -0x80000000

    goto :goto_1
.end method

.method public final a(LX/1X1;)I
    .locals 2

    .prologue
    .line 316856
    check-cast p1, LX/1nb;

    .line 316857
    iget-boolean v1, p1, LX/1nb;->A:Z

    iget-object v0, p1, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 316858
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    array-length p0, v0

    :goto_0
    move v0, p0

    .line 316859
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 316860
    invoke-static {}, LX/1dS;->b()V

    .line 316861
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 27

    .prologue
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const v3, -0x273bb74c

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v26

    .line 316862
    check-cast p6, LX/1nb;

    .line 316863
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v25

    .line 316864
    move-object/from16 v0, p6

    iget-object v5, v0, LX/1nb;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iget-object v6, v0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p6

    iget-boolean v7, v0, LX/1nb;->c:Z

    move-object/from16 v0, p6

    iget v8, v0, LX/1nb;->d:I

    move-object/from16 v0, p6

    iget v9, v0, LX/1nb;->e:I

    move-object/from16 v0, p6

    iget v10, v0, LX/1nb;->f:F

    move-object/from16 v0, p6

    iget v11, v0, LX/1nb;->g:F

    move-object/from16 v0, p6

    iget v12, v0, LX/1nb;->h:F

    move-object/from16 v0, p6

    iget v13, v0, LX/1nb;->i:I

    move-object/from16 v0, p6

    iget-boolean v14, v0, LX/1nb;->j:Z

    move-object/from16 v0, p6

    iget v15, v0, LX/1nb;->k:I

    move-object/from16 v0, p6

    iget-object v0, v0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    move-object/from16 v16, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/1nb;->m:I

    move/from16 v17, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/1nb;->n:I

    move/from16 v18, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/1nb;->o:F

    move/from16 v19, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/1nb;->p:F

    move/from16 v20, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/1nb;->q:I

    move/from16 v21, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/1nb;->r:Landroid/graphics/Typeface;

    move-object/from16 v22, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    move-object/from16 v23, v0

    move-object/from16 v0, p6

    iget-boolean v0, v0, LX/1nb;->t:Z

    move/from16 v24, v0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move-object/from16 v4, p5

    invoke-static/range {v1 .. v25}, LX/1nc;->a(LX/1Dg;IILX/1no;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;ZIIFFFIZILandroid/content/res/ColorStateList;IIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLX/1np;)V

    .line 316865
    invoke-virtual/range {v25 .. v25}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    move-object/from16 v0, p6

    iput-object v1, v0, LX/1nb;->u:Landroid/text/Layout;

    .line 316866
    invoke-static/range {v25 .. v25}, LX/1S3;->a(LX/1np;)V

    .line 316867
    const/16 v1, 0x8

    const/16 v2, 0x1f

    const v3, 0x41525c80

    move/from16 v0, v26

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 26

    .prologue
    .line 316868
    check-cast p3, LX/1nb;

    .line 316869
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v23

    .line 316870
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v24

    .line 316871
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v25

    .line 316872
    move-object/from16 v0, p3

    iget-object v2, v0, LX/1nb;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v3, v0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p3

    iget-boolean v4, v0, LX/1nb;->c:Z

    move-object/from16 v0, p3

    iget v5, v0, LX/1nb;->e:I

    move-object/from16 v0, p3

    iget v6, v0, LX/1nb;->f:F

    move-object/from16 v0, p3

    iget v7, v0, LX/1nb;->g:F

    move-object/from16 v0, p3

    iget v8, v0, LX/1nb;->h:F

    move-object/from16 v0, p3

    iget v9, v0, LX/1nb;->i:I

    move-object/from16 v0, p3

    iget-boolean v10, v0, LX/1nb;->j:Z

    move-object/from16 v0, p3

    iget v11, v0, LX/1nb;->k:I

    move-object/from16 v0, p3

    iget-object v12, v0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    move-object/from16 v0, p3

    iget v13, v0, LX/1nb;->m:I

    move-object/from16 v0, p3

    iget v14, v0, LX/1nb;->n:I

    move-object/from16 v0, p3

    iget v15, v0, LX/1nb;->o:F

    move-object/from16 v0, p3

    iget v0, v0, LX/1nb;->p:F

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/1nb;->v:LX/1nd;

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/1nb;->q:I

    move/from16 v18, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/1nb;->r:Landroid/graphics/Typeface;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    move-object/from16 v20, v0

    move-object/from16 v0, p3

    iget-boolean v0, v0, LX/1nb;->t:Z

    move/from16 v21, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/1nb;->u:Landroid/text/Layout;

    move-object/from16 v22, v0

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v25}, LX/1nc;->a(LX/1Dg;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;ZIFFFIZILandroid/content/res/ColorStateList;IIFFLX/1nd;ILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLandroid/text/Layout;LX/1np;LX/1np;LX/1np;)V

    .line 316873
    invoke-virtual/range {v23 .. v23}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1nb;->w:Landroid/text/Layout;

    .line 316874
    invoke-static/range {v23 .. v23}, LX/1S3;->a(LX/1np;)V

    .line 316875
    invoke-virtual/range {v24 .. v24}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1nb;->x:Ljava/lang/Float;

    .line 316876
    invoke-static/range {v24 .. v24}, LX/1S3;->a(LX/1np;)V

    .line 316877
    invoke-virtual/range {v25 .. v25}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    .line 316878
    invoke-static/range {v25 .. v25}, LX/1S3;->a(LX/1np;)V

    .line 316879
    return-void
.end method

.method public final a(LX/3sp;IIILX/1X1;)V
    .locals 9

    .prologue
    .line 316880
    check-cast p5, LX/1nb;

    .line 316881
    iget-object v4, p5, LX/1nb;->a:Ljava/lang/CharSequence;

    iget-object v5, p5, LX/1nb;->w:Landroid/text/Layout;

    iget-object v6, p5, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    check-cast v6, [Landroid/text/style/ClickableSpan;

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    .line 316882
    check-cast v4, Landroid/text/Spanned;

    .line 316883
    aget-object v7, v6, v1

    .line 316884
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result p1

    .line 316885
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result p0

    .line 316886
    invoke-virtual {v5, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v8

    .line 316887
    invoke-virtual {v5, p0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result p2

    .line 316888
    if-ne v8, p2, :cond_0

    move v8, p0

    .line 316889
    :goto_0
    sget-object p2, LX/1nc;->i:Landroid/graphics/Path;

    invoke-virtual {v5, p1, v8, p2}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 316890
    sget-object v8, LX/1nc;->i:Landroid/graphics/Path;

    sget-object p2, LX/1nc;->k:Landroid/graphics/RectF;

    const/4 p3, 0x1

    invoke-virtual {v8, p2, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 316891
    sget-object v8, LX/1nc;->j:Landroid/graphics/Rect;

    sget-object p2, LX/1nc;->k:Landroid/graphics/RectF;

    iget p2, p2, Landroid/graphics/RectF;->left:F

    float-to-int p2, p2

    add-int/2addr p2, v2

    sget-object p3, LX/1nc;->k:Landroid/graphics/RectF;

    iget p3, p3, Landroid/graphics/RectF;->top:F

    float-to-int p3, p3

    add-int/2addr p3, v3

    sget-object p4, LX/1nc;->k:Landroid/graphics/RectF;

    iget p4, p4, Landroid/graphics/RectF;->right:F

    float-to-int p4, p4

    add-int/2addr p4, v2

    sget-object p5, LX/1nc;->k:Landroid/graphics/RectF;

    iget p5, p5, Landroid/graphics/RectF;->bottom:F

    float-to-int p5, p5

    add-int/2addr p5, v3

    invoke-virtual {v8, p2, p3, p4, p5}, Landroid/graphics/Rect;->set(IIII)V

    .line 316892
    sget-object v8, LX/1nc;->j:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 316893
    sget-object v7, LX/1nc;->j:Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 p0, 0x0

    const/4 p1, 0x1

    const/4 p2, 0x1

    invoke-virtual {v7, v8, p0, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 316894
    sget-object v7, LX/1nc;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 316895
    const-string v7, ""

    invoke-virtual {v0, v7}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 316896
    :goto_1
    return-void

    .line 316897
    :cond_0
    invoke-virtual {v5, v8}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v8

    goto :goto_0

    .line 316898
    :cond_1
    sget-object v8, LX/1nc;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v8}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 316899
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/3sp;->f(Z)V

    .line 316900
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/3sp;->a(Z)V

    .line 316901
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/3sp;->h(Z)V

    .line 316902
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/3sp;->c(Z)V

    .line 316903
    instance-of v8, v7, LX/4oc;

    if-eqz v8, :cond_2

    .line 316904
    check-cast v7, LX/4oc;

    .line 316905
    iget-object v8, v7, LX/4oc;->a:Ljava/lang/String;

    move-object v7, v8

    .line 316906
    invoke-virtual {v0, v7}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 316907
    :cond_2
    invoke-interface {v4, p1, p0}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(LX/3sp;LX/1X1;)V
    .locals 1

    .prologue
    .line 316822
    check-cast p2, LX/1nb;

    .line 316823
    iget-object v0, p2, LX/1nb;->a:Ljava/lang/CharSequence;

    .line 316824
    invoke-virtual {p1, v0}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    .line 316825
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 316826
    new-instance v0, LX/1oF;

    invoke-direct {v0}, LX/1oF;-><init>()V

    move-object v0, v0

    .line 316827
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 316723
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 316724
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 316725
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 316726
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 316727
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 316728
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 316729
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 316730
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 316731
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 316732
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v11

    .line 316733
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v12

    .line 316734
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v13

    .line 316735
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v14

    .line 316736
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v15

    .line 316737
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v16

    .line 316738
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v17

    .line 316739
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v18

    move-object/from16 v1, p1

    .line 316740
    invoke-static/range {v1 .. v18}, LX/1nc;->a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 316741
    check-cast p2, LX/1nb;

    .line 316742
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 316743
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    .line 316744
    :cond_0
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 316745
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 316746
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move-object/from16 v0, p2

    iput-boolean v1, v0, LX/1nb;->c:Z

    .line 316747
    :cond_1
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 316748
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 316749
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->p:F

    .line 316750
    :cond_2
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 316751
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 316752
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->d:I

    .line 316753
    :cond_3
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 316754
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 316755
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->e:I

    .line 316756
    :cond_4
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 316757
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 316758
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move-object/from16 v0, p2

    iput-boolean v1, v0, LX/1nb;->j:Z

    .line 316759
    :cond_5
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 316760
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 316761
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/1nb;->a:Ljava/lang/CharSequence;

    .line 316762
    :cond_6
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 316763
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 316764
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/ColorStateList;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    .line 316765
    :cond_7
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 316766
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 316767
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->m:I

    .line 316768
    :cond_8
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 316769
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 316770
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->z:I

    .line 316771
    :cond_9
    invoke-static {v11}, LX/1cy;->a(LX/1np;)V

    .line 316772
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 316773
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->n:I

    .line 316774
    :cond_a
    invoke-static {v12}, LX/1cy;->a(LX/1np;)V

    .line 316775
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 316776
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout$Alignment;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    .line 316777
    :cond_b
    invoke-static {v13}, LX/1cy;->a(LX/1np;)V

    .line 316778
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 316779
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->q:I

    .line 316780
    :cond_c
    invoke-static {v14}, LX/1cy;->a(LX/1np;)V

    .line 316781
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 316782
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->f:F

    .line 316783
    :cond_d
    invoke-static {v15}, LX/1cy;->a(LX/1np;)V

    .line 316784
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 316785
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->g:F

    .line 316786
    :cond_e
    invoke-static/range {v16 .. v16}, LX/1S3;->a(LX/1np;)V

    .line 316787
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 316788
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->h:F

    .line 316789
    :cond_f
    invoke-static/range {v17 .. v17}, LX/1S3;->a(LX/1np;)V

    .line 316790
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 316791
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/1nb;->i:I

    .line 316792
    :cond_10
    invoke-static/range {v18 .. v18}, LX/1S3;->a(LX/1np;)V

    .line 316793
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 316794
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 8

    .prologue
    .line 316795
    check-cast p3, LX/1nb;

    move-object v0, p2

    .line 316796
    check-cast v0, LX/1oF;

    iget-object v1, p3, LX/1nb;->a:Ljava/lang/CharSequence;

    iget v2, p3, LX/1nb;->k:I

    iget v3, p3, LX/1nb;->z:I

    iget-object v4, p3, LX/1nb;->l:Landroid/content/res/ColorStateList;

    iget-object v5, p3, LX/1nb;->w:Landroid/text/Layout;

    iget-object v6, p3, LX/1nb;->x:Ljava/lang/Float;

    iget-object v7, p3, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    check-cast v7, [Landroid/text/style/ClickableSpan;

    invoke-static/range {v0 .. v7}, LX/1nc;->a(LX/1oF;Ljava/lang/CharSequence;IILandroid/content/res/ColorStateList;Landroid/text/Layout;Ljava/lang/Float;[Landroid/text/style/ClickableSpan;)V

    .line 316797
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 316798
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 316799
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 316800
    check-cast p3, LX/1nb;

    .line 316801
    check-cast p2, LX/1oF;

    iget-object v0, p3, LX/1nb;->a:Ljava/lang/CharSequence;

    .line 316802
    const/4 p3, 0x0

    const/4 p1, 0x0

    .line 316803
    iput-object p1, p2, LX/1oF;->a:Landroid/text/Layout;

    .line 316804
    const/4 p0, 0x0

    iput p0, p2, LX/1oF;->c:F

    .line 316805
    iput-object p1, p2, LX/1oF;->e:Ljava/lang/CharSequence;

    .line 316806
    iput-object p1, p2, LX/1oF;->i:[Landroid/text/style/ClickableSpan;

    .line 316807
    iput-boolean p3, p2, LX/1oF;->d:Z

    .line 316808
    iput p3, p2, LX/1oF;->h:I

    .line 316809
    iput-object p1, p2, LX/1oF;->f:Landroid/content/res/ColorStateList;

    .line 316810
    iput p3, p2, LX/1oF;->g:I

    .line 316811
    instance-of p0, v0, LX/1oG;

    if-eqz p0, :cond_0

    .line 316812
    check-cast v0, LX/1oG;

    invoke-interface {v0, p2}, LX/1oG;->b(Landroid/graphics/drawable/Drawable;)V

    .line 316813
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 316814
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 316815
    const/4 v0, 0x1

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 316816
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 316817
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 316818
    const/16 v0, 0x1e

    return v0
.end method
