.class public LX/1du;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile i:LX/1du;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0TD;

.field private final d:LX/0tX;

.field private final e:LX/1dv;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Stack",
            "<",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 287004
    const-class v0, LX/1du;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1du;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0TD;LX/0tX;LX/1dv;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 286995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286996
    iput-object p1, p0, LX/1du;->b:LX/03V;

    .line 286997
    iput-object p2, p0, LX/1du;->c:LX/0TD;

    .line 286998
    iput-object p3, p0, LX/1du;->d:LX/0tX;

    .line 286999
    iput-object p4, p0, LX/1du;->e:LX/1dv;

    .line 287000
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1du;->f:Ljava/util/Map;

    .line 287001
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1du;->g:Ljava/util/Map;

    .line 287002
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1du;->h:Ljava/util/Map;

    .line 287003
    return-void
.end method

.method public static a(LX/0QB;)LX/1du;
    .locals 7

    .prologue
    .line 286982
    sget-object v0, LX/1du;->i:LX/1du;

    if-nez v0, :cond_1

    .line 286983
    const-class v1, LX/1du;

    monitor-enter v1

    .line 286984
    :try_start_0
    sget-object v0, LX/1du;->i:LX/1du;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 286985
    if-eqz v2, :cond_0

    .line 286986
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 286987
    new-instance p0, LX/1du;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1dv;->b(LX/0QB;)LX/1dv;

    move-result-object v6

    check-cast v6, LX/1dv;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1du;-><init>(LX/03V;LX/0TD;LX/0tX;LX/1dv;)V

    .line 286988
    move-object v0, p0

    .line 286989
    sput-object v0, LX/1du;->i:LX/1du;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286990
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 286991
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 286992
    :cond_1
    sget-object v0, LX/1du;->i:LX/1du;

    return-object v0

    .line 286993
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 286994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1du;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286975
    new-instance v0, LX/4XQ;

    invoke-direct {v0}, LX/4XQ;-><init>()V

    .line 286976
    iput-object p2, v0, LX/4XQ;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 286977
    move-object v0, v0

    .line 286978
    invoke-virtual {v0}, LX/4XQ;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    .line 286979
    iget-object v1, p0, LX/1du;->e:LX/1dv;

    sget-object v2, LX/0wD;->NEWSFEED:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    .line 286980
    const/4 v7, 0x1

    move-object v3, v1

    move-object v4, p1

    move-object v5, v0

    move-object v6, v2

    move-object v8, p3

    invoke-static/range {v3 .. v8}, LX/1dv;->a(LX/1dv;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 286981
    return-object v0
.end method

.method public static b(LX/1du;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)V
    .locals 1

    .prologue
    .line 286973
    iget-object v0, p0, LX/1du;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286974
    return-void
.end method

.method public static c(LX/1du;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 287005
    iget-object v0, p0, LX/1du;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 287006
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static d(LX/1du;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 286971
    iget-object v0, p0, LX/1du;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286972
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            "LX/0TF",
            "<",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286958
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286959
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 286960
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v6

    .line 286961
    invoke-static {p0, v6, p2}, LX/1du;->b(LX/1du;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)V

    .line 286962
    new-instance v0, LX/4XQ;

    invoke-direct {v0}, LX/4XQ;-><init>()V

    .line 286963
    iput-object p2, v0, LX/4XQ;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 286964
    move-object v0, v0

    .line 286965
    invoke-virtual {v0}, LX/4XQ;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    .line 286966
    invoke-static {p0, v6}, LX/1du;->c(LX/1du;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 286967
    iget-object v1, p0, LX/1du;->e:LX/1dv;

    sget-object v2, LX/0wD;->NEWSFEED:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v0, v2, v5}, LX/1dv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 286968
    new-instance v0, LX/AjA;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/AjA;-><init>(LX/1du;LX/0TF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;)V

    .line 286969
    new-instance v1, LX/AjB;

    move-object v2, p0

    move-object v3, v6

    move-object v4, p2

    move-object v5, v0

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, LX/AjB;-><init>(LX/1du;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;LX/0TF;)V

    iget-object v0, p0, LX/1du;->c:LX/0TD;

    invoke-static {v7, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 286970
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            "LX/0TF",
            "<",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286943
    new-instance v0, LX/80E;

    invoke-direct {v0}, LX/80E;-><init>()V

    move-object v0, v0

    .line 286944
    const-string v1, "node_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "action_path"

    invoke-static {p0, p1}, LX/1du;->c(LX/1du;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 286945
    iget-object v1, p0, LX/1du;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 286946
    new-instance v1, LX/AjC;

    invoke-direct {v1, p0, p1, p3, p2}, LX/AjC;-><init>(LX/1du;Ljava/lang/String;LX/0TF;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)V

    iget-object v2, p0, LX/1du;->c:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 286947
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 286956
    iget-object v0, p0, LX/1du;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286957
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 286954
    iget-object v1, p0, LX/1du;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 286955
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/1du;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 286948
    iget-object v0, p0, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 286949
    :goto_0
    return-object v0

    .line 286950
    :cond_0
    iget-object v0, p0, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 286951
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 286952
    goto :goto_0

    .line 286953
    :cond_1
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    goto :goto_0
.end method
