.class public LX/11b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 172674
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "interstitial/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 172675
    sput-object v0, LX/11b;->a:LX/0Tn;

    const-string v1, "data/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->b:LX/0Tn;

    .line 172676
    sget-object v0, LX/11b;->a:LX/0Tn;

    const-string v1, "triToId/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->c:LX/0Tn;

    .line 172677
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "last_impression/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->d:LX/0Tn;

    .line 172678
    sget-object v0, LX/11b;->a:LX/0Tn;

    const-string v1, "AllTriggers"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->e:LX/0Tn;

    .line 172679
    sget-object v0, LX/11b;->a:LX/0Tn;

    const-string v1, "data_restored"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->f:LX/0Tn;

    .line 172680
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "version/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->g:LX/0Tn;

    .line 172681
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "app_version/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/11b;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 172673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0Tn;
    .locals 2

    .prologue
    .line 172670
    sget-object v0, LX/11b;->c:LX/0Tn;

    invoke-virtual {p0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 172672
    sget-object v0, LX/11b;->b:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 172671
    sget-object v0, LX/11b;->d:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
