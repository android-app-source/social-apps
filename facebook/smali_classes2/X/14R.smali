.class public final enum LX/14R;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/14R;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/14R;

.field public static final enum AUTO:LX/14R;

.field public static final enum FILE_PART_ENTITY:LX/14R;

.field public static final enum MULTI_PART_ENTITY:LX/14R;

.field public static final enum SINGLE_STRING_ENTITY:LX/14R;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178852
    new-instance v0, LX/14R;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, LX/14R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14R;->AUTO:LX/14R;

    .line 178853
    new-instance v0, LX/14R;

    const-string v1, "MULTI_PART_ENTITY"

    invoke-direct {v0, v1, v3}, LX/14R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 178854
    new-instance v0, LX/14R;

    const-string v1, "SINGLE_STRING_ENTITY"

    invoke-direct {v0, v1, v4}, LX/14R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14R;->SINGLE_STRING_ENTITY:LX/14R;

    .line 178855
    new-instance v0, LX/14R;

    const-string v1, "FILE_PART_ENTITY"

    invoke-direct {v0, v1, v5}, LX/14R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14R;->FILE_PART_ENTITY:LX/14R;

    .line 178856
    const/4 v0, 0x4

    new-array v0, v0, [LX/14R;

    sget-object v1, LX/14R;->AUTO:LX/14R;

    aput-object v1, v0, v2

    sget-object v1, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    aput-object v1, v0, v3

    sget-object v1, LX/14R;->SINGLE_STRING_ENTITY:LX/14R;

    aput-object v1, v0, v4

    sget-object v1, LX/14R;->FILE_PART_ENTITY:LX/14R;

    aput-object v1, v0, v5

    sput-object v0, LX/14R;->$VALUES:[LX/14R;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 178857
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/14R;
    .locals 1

    .prologue
    .line 178858
    const-class v0, LX/14R;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/14R;

    return-object v0
.end method

.method public static values()[LX/14R;
    .locals 1

    .prologue
    .line 178859
    sget-object v0, LX/14R;->$VALUES:[LX/14R;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/14R;

    return-object v0
.end method
