.class public LX/1aq;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/1aj;
.implements LX/1ak;


# instance fields
.field private a:LX/1ak;

.field private final b:LX/1al;

.field private final c:[Landroid/graphics/drawable/Drawable;

.field public final d:[LX/1ai;

.field private final e:Landroid/graphics/Rect;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 278740
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 278741
    new-instance v1, LX/1al;

    invoke-direct {v1}, LX/1al;-><init>()V

    iput-object v1, p0, LX/1aq;->b:LX/1al;

    .line 278742
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/1aq;->e:Landroid/graphics/Rect;

    .line 278743
    iput-boolean v0, p0, LX/1aq;->f:Z

    .line 278744
    iput-boolean v0, p0, LX/1aq;->g:Z

    .line 278745
    iput-boolean v0, p0, LX/1aq;->h:Z

    .line 278746
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278747
    iput-object p1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    .line 278748
    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 278749
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    invoke-static {v1, p0, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278751
    :cond_0
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    new-array v0, v0, [LX/1ai;

    iput-object v0, p0, LX/1aq;->d:[LX/1ai;

    .line 278752
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 278739
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    return v0
.end method

.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 278734
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 278735
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 278736
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    return-object v0

    :cond_0
    move v0, v2

    .line 278737
    goto :goto_0

    :cond_1
    move v1, v2

    .line 278738
    goto :goto_1
.end method

.method public final a(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 278717
    if-ltz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 278718
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    if-ge p1, v0, :cond_3

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 278719
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    .line 278720
    if-eq p2, v0, :cond_1

    .line 278721
    if-eqz p2, :cond_0

    iget-boolean v1, p0, LX/1aq;->h:Z

    if-eqz v1, :cond_0

    .line 278722
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 278723
    :cond_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, p1

    invoke-static {v1, v3, v3}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278724
    invoke-static {p2, v3, v3}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278725
    iget-object v1, p0, LX/1aq;->b:LX/1al;

    invoke-static {p2, v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;LX/1al;)V

    .line 278726
    invoke-static {p2, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 278727
    invoke-static {p2, p0, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278728
    iput-boolean v2, p0, LX/1aq;->g:Z

    .line 278729
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aput-object p2, v1, p1

    .line 278730
    invoke-virtual {p0}, LX/1aq;->invalidateSelf()V

    .line 278731
    :cond_1
    return-object v0

    :cond_2
    move v0, v2

    .line 278732
    goto :goto_0

    :cond_3
    move v1, v2

    .line 278733
    goto :goto_1
.end method

.method public final a(LX/1ak;)V
    .locals 0

    .prologue
    .line 278715
    iput-object p1, p0, LX/1aq;->a:LX/1ak;

    .line 278716
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 278711
    iget-object v0, p0, LX/1aq;->a:LX/1ak;

    if-eqz v0, :cond_0

    .line 278712
    iget-object v0, p0, LX/1aq;->a:LX/1ak;

    invoke-interface {v0, p1}, LX/1ak;->a(Landroid/graphics/Matrix;)V

    .line 278713
    :goto_0
    return-void

    .line 278714
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 278654
    iget-object v0, p0, LX/1aq;->a:LX/1ak;

    if-eqz v0, :cond_0

    .line 278655
    iget-object v0, p0, LX/1aq;->a:LX/1ak;

    invoke-interface {v0, p1}, LX/1ak;->a(Landroid/graphics/RectF;)V

    .line 278656
    :goto_0
    return-void

    .line 278657
    :cond_0
    invoke-virtual {p0}, LX/1aq;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 278705
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278706
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278707
    if-eqz v1, :cond_0

    .line 278708
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 278709
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278710
    :cond_1
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 278699
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    iget-object v3, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 278700
    iget-object v3, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v3, v3, v0

    .line 278701
    if-eqz v3, :cond_0

    .line 278702
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 278703
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278704
    :cond_1
    if-lez v1, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final getIntrinsicWidth()I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 278693
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    iget-object v3, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 278694
    iget-object v3, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v3, v3, v0

    .line 278695
    if-eqz v3, :cond_0

    .line 278696
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 278697
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278698
    :cond_1
    if-lez v1, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 3

    .prologue
    .line 278684
    iget-object v0, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 278685
    const/4 v1, -0x2

    .line 278686
    :cond_0
    return v1

    .line 278687
    :cond_1
    const/4 v1, -0x1

    .line 278688
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 278689
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278690
    if-eqz v2, :cond_2

    .line 278691
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v2

    invoke-static {v1, v2}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    move-result v1

    .line 278692
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 278669
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 278670
    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 278671
    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 278672
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 278673
    iget-object v1, p0, LX/1aq;->e:Landroid/graphics/Rect;

    .line 278674
    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278675
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278676
    if-eqz v2, :cond_0

    .line 278677
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 278678
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 278679
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->top:I

    .line 278680
    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    .line 278681
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 278682
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278683
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 278667
    invoke-virtual {p0}, LX/1aq;->invalidateSelf()V

    .line 278668
    return-void
.end method

.method public final isStateful()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 278658
    iget-boolean v0, p0, LX/1aq;->g:Z

    if-nez v0, :cond_2

    .line 278659
    iput-boolean v1, p0, LX/1aq;->f:Z

    move v0, v1

    .line 278660
    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278661
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278662
    iget-boolean v4, p0, LX/1aq;->f:Z

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_1
    or-int/2addr v2, v4

    iput-boolean v2, p0, LX/1aq;->f:Z

    .line 278663
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 278664
    goto :goto_1

    .line 278665
    :cond_1
    iput-boolean v3, p0, LX/1aq;->g:Z

    .line 278666
    :cond_2
    iget-boolean v0, p0, LX/1aq;->f:Z

    return v0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 278581
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278582
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278583
    if-eqz v1, :cond_0

    .line 278584
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 278585
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278586
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1aq;->h:Z

    .line 278587
    return-object p0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 278588
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278589
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278590
    if-eqz v1, :cond_0

    .line 278591
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278592
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278593
    :cond_1
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 278594
    move v1, v0

    .line 278595
    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278596
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278597
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278598
    const/4 v1, 0x1

    .line 278599
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278600
    :cond_1
    return v1
.end method

.method public final onStateChange([I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 278601
    move v1, v0

    .line 278602
    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278603
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278604
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278605
    const/4 v1, 0x1

    .line 278606
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278607
    :cond_1
    return v1
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 278608
    invoke-virtual {p0, p2, p3, p4}, LX/1aq;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 278609
    return-void
.end method

.method public setAlpha(I)V
    .locals 2

    .prologue
    .line 278610
    iget-object v0, p0, LX/1aq;->b:LX/1al;

    .line 278611
    iput p1, v0, LX/1al;->a:I

    .line 278612
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278613
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278614
    if-eqz v1, :cond_0

    .line 278615
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 278616
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278617
    :cond_1
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 278618
    iget-object v0, p0, LX/1aq;->b:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->a(Landroid/graphics/ColorFilter;)V

    .line 278619
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278620
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278621
    if-eqz v1, :cond_0

    .line 278622
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 278623
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278624
    :cond_1
    return-void
.end method

.method public final setDither(Z)V
    .locals 2

    .prologue
    .line 278625
    iget-object v0, p0, LX/1aq;->b:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->a(Z)V

    .line 278626
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278627
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278628
    if-eqz v1, :cond_0

    .line 278629
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 278630
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278631
    :cond_1
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 2

    .prologue
    .line 278632
    iget-object v0, p0, LX/1aq;->b:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->b(Z)V

    .line 278633
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278634
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278635
    if-eqz v1, :cond_0

    .line 278636
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 278637
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278638
    :cond_1
    return-void
.end method

.method public final setHotspot(FF)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 278639
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 278640
    iget-object v1, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 278641
    if-eqz v1, :cond_0

    .line 278642
    invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 278643
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278644
    :cond_1
    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 3

    .prologue
    .line 278645
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v1

    .line 278646
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 278647
    iget-object v2, p0, LX/1aq;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 278648
    if-eqz v2, :cond_0

    .line 278649
    invoke-virtual {v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 278650
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278651
    :cond_1
    return v1
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 278652
    invoke-virtual {p0, p2}, LX/1aq;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 278653
    return-void
.end method
