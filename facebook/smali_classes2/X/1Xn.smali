.class public final LX/1Xn;
.super LX/1Xo;
.source ""


# static fields
.field private static final serialVersionUID:J = -0xb41c5b2afd5e69cL


# direct methods
.method private constructor <init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "LX/0lJ;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 272440
    invoke-direct/range {p0 .. p6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 272441
    return-void
.end method

.method public static b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "LX/0lJ;",
            ")",
            "LX/1Xn;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 272442
    new-instance v0, LX/1Xn;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private j(Ljava/lang/Object;)LX/1Xn;
    .locals 7

    .prologue
    .line 272443
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private k(Ljava/lang/Object;)LX/1Xn;
    .locals 7

    .prologue
    .line 272444
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private l(Ljava/lang/Object;)LX/1Xn;
    .locals 7

    .prologue
    .line 272445
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private m(Ljava/lang/Object;)LX/1Xn;
    .locals 7

    .prologue
    .line 272447
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private n(Ljava/lang/Object;)LX/1Xn;
    .locals 7

    .prologue
    .line 272446
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private y()LX/1Xn;
    .locals 7

    .prologue
    .line 272450
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    .line 272451
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2}, LX/0lJ;->b()LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3}, LX/0lJ;->b()LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272449
    invoke-direct {p0, p1}, LX/1Xn;->j(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 272448
    invoke-direct {p0}, LX/1Xn;->y()LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272438
    invoke-direct {p0, p1}, LX/1Xn;->k(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272414
    invoke-direct {p0, p1}, LX/1Xn;->l(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272439
    new-instance v0, LX/1Xn;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272413
    invoke-direct {p0, p1}, LX/1Xn;->m(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272415
    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    .line 272416
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272417
    if-ne p1, v0, :cond_0

    .line 272418
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic e(Ljava/lang/Object;)LX/1Xo;
    .locals 1

    .prologue
    .line 272419
    invoke-direct {p0, p1}, LX/1Xn;->j(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272420
    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    .line 272421
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272422
    if-ne p1, v0, :cond_0

    .line 272423
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/1Xo;
    .locals 1

    .prologue
    .line 272424
    invoke-direct {p0, p1}, LX/1Xn;->k(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic g(Ljava/lang/Object;)LX/1Xo;
    .locals 1

    .prologue
    .line 272425
    invoke-direct {p0, p1}, LX/1Xn;->l(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272426
    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272427
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272428
    if-ne p1, v0, :cond_0

    .line 272429
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic h(Ljava/lang/Object;)LX/1Xo;
    .locals 1

    .prologue
    .line 272430
    invoke-direct {p0, p1}, LX/1Xn;->m(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272431
    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272432
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272433
    if-ne p1, v0, :cond_0

    .line 272434
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xn;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xn;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic i(Ljava/lang/Object;)LX/1Xo;
    .locals 1

    .prologue
    .line 272435
    invoke-direct {p0, p1}, LX/1Xn;->n(Ljava/lang/Object;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272436
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[map type; class "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()LX/1Xo;
    .locals 1

    .prologue
    .line 272437
    invoke-direct {p0}, LX/1Xn;->y()LX/1Xn;

    move-result-object v0

    return-object v0
.end method
