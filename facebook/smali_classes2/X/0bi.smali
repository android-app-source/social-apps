.class public abstract LX/0bi;
.super LX/0aB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0aB;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:[I

.field private d:I

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87210
    const-class v0, LX/0bi;

    sput-object v0, LX/0bi;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 87211
    invoke-direct {p0}, LX/0aB;-><init>()V

    .line 87212
    iput v0, p0, LX/0bi;->d:I

    .line 87213
    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 87214
    iput-object p1, p0, LX/0bi;->b:LX/0Ot;

    .line 87215
    iput p2, p0, LX/0bi;->d:I

    .line 87216
    return-void

    .line 87217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs constructor <init>(LX/0Ot;[I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<TT;>;[I)V"
        }
    .end annotation

    .prologue
    .line 87218
    invoke-direct {p0}, LX/0aB;-><init>()V

    .line 87219
    const/4 v0, -0x1

    iput v0, p0, LX/0bi;->d:I

    .line 87220
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 87221
    iput-object p1, p0, LX/0bi;->b:LX/0Ot;

    .line 87222
    iput-object p2, p0, LX/0bi;->c:[I

    .line 87223
    return-void

    .line 87224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Uh;I)V
    .locals 1

    .prologue
    .line 87225
    iget-object v0, p0, LX/0bi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/0bi;->a(LX/0Uh;ILjava/lang/Object;)V

    .line 87226
    return-void
.end method

.method public abstract a(LX/0Uh;ILjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "ITT;)V"
        }
    .end annotation
.end method

.method public final declared-synchronized a(LX/0a8;)V
    .locals 2

    .prologue
    .line 87227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0bi;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 87228
    :goto_0
    monitor-exit p0

    return-void

    .line 87229
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/0bi;->e:Z

    .line 87230
    iget-object v0, p0, LX/0bi;->c:[I

    if-eqz v0, :cond_1

    .line 87231
    iget-object v0, p0, LX/0bi;->c:[I

    invoke-virtual {p1, p0, v0}, LX/0a8;->a(LX/0aB;[I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87233
    :cond_1
    iget v0, p0, LX/0bi;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 87234
    iget v0, p0, LX/0bi;->d:I

    invoke-virtual {p1, p0, v0}, LX/0a8;->a(LX/0aB;I)V

    goto :goto_0

    .line 87235
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
