.class public LX/0dm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final b:LX/0So;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90884
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0dm;->c:Landroid/util/SparseArray;

    .line 90885
    iput-object p1, p0, LX/0dm;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 90886
    iput-object p2, p0, LX/0dm;->b:LX/0So;

    .line 90887
    return-void
.end method

.method private static c(I)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 90866
    packed-switch p0, :pswitch_data_0

    .line 90867
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No annotation key to match id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90868
    :pswitch_0
    const-string v0, "create_js_context"

    .line 90869
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "evaluate_source_code"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d(I)I
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 90880
    packed-switch p0, :pswitch_data_0

    .line 90881
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No event to match id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90882
    :pswitch_0
    const v0, 0xb20005

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 90876
    packed-switch p1, :pswitch_data_0

    .line 90877
    iget-object v0, p0, LX/0dm;->c:Landroid/util/SparseArray;

    iget-object v1, p0, LX/0dm;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 90878
    :goto_0
    return-void

    .line 90879
    :pswitch_0
    iget-object v0, p0, LX/0dm;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/0dm;->d(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)V
    .locals 8

    .prologue
    .line 90870
    packed-switch p1, :pswitch_data_0

    .line 90871
    iget-object v1, p0, LX/0dm;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xb20005

    invoke-static {p1}, LX/0dm;->c(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/0dm;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iget-object v0, p0, LX/0dm;->c:Landroid/util/SparseArray;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, p1, v6}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 90872
    iget-object v0, p0, LX/0dm;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 90873
    :goto_0
    return-void

    .line 90874
    :pswitch_0
    iget-object v0, p0, LX/0dm;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/0dm;->d(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 90875
    iget-object v0, p0, LX/0dm;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
