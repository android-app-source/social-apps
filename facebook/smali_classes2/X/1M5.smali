.class public final LX/1M5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/common/collect/Multiset$Entry",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TE;",
            "LX/4xL;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:Ljava/util/Iterator;

.field public final synthetic c:LX/1Lz;


# direct methods
.method public constructor <init>(LX/1Lz;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 234778
    iput-object p1, p0, LX/1M5;->c:LX/1Lz;

    iput-object p2, p0, LX/1M5;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 234779
    iget-object v0, p0, LX/1M5;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 234780
    iget-object v0, p0, LX/1M5;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 234781
    iput-object v0, p0, LX/1M5;->a:Ljava/util/Map$Entry;

    .line 234782
    new-instance v1, LX/4wy;

    invoke-direct {v1, p0, v0}, LX/4wy;-><init>(LX/1M5;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public final remove()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 234783
    iget-object v0, p0, LX/1M5;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 234784
    iget-object v2, p0, LX/1M5;->c:LX/1Lz;

    iget-object v0, p0, LX/1M5;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    invoke-virtual {v0, v1}, LX/4xL;->d(I)I

    move-result v0

    int-to-long v0, v0

    .line 234785
    iget-wide v3, v2, LX/1Lz;->b:J

    sub-long/2addr v3, v0

    iput-wide v3, v2, LX/1Lz;->b:J

    .line 234786
    iget-object v0, p0, LX/1M5;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 234787
    const/4 v0, 0x0

    iput-object v0, p0, LX/1M5;->a:Ljava/util/Map$Entry;

    .line 234788
    return-void

    :cond_0
    move v0, v1

    .line 234789
    goto :goto_0
.end method
