.class public LX/17Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:I

.field private static volatile f:LX/17Z;


# instance fields
.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:S


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198925
    const/4 v0, 0x3

    sput v0, LX/17Z;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 198926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198927
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/17Z;->c:Ljava/util/HashSet;

    .line 198928
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    .line 198929
    iput-object p1, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 198930
    return-void
.end method

.method public static a(LX/0QB;)LX/17Z;
    .locals 4

    .prologue
    .line 198931
    sget-object v0, LX/17Z;->f:LX/17Z;

    if-nez v0, :cond_1

    .line 198932
    const-class v1, LX/17Z;

    monitor-enter v1

    .line 198933
    :try_start_0
    sget-object v0, LX/17Z;->f:LX/17Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 198934
    if-eqz v2, :cond_0

    .line 198935
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 198936
    new-instance p0, LX/17Z;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/17Z;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 198937
    move-object v0, p0

    .line 198938
    sput-object v0, LX/17Z;->f:LX/17Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198939
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 198940
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 198941
    :cond_1
    sget-object v0, LX/17Z;->f:LX/17Z;

    return-object v0

    .line 198942
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 198943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 198883
    if-eqz p0, :cond_4

    .line 198884
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 198885
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 198886
    :cond_0
    :goto_0
    return-object p0

    .line 198887
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 198888
    sget v2, LX/17Z;->a:I

    add-int/2addr v0, v2

    :goto_1
    if-ge v0, v1, :cond_0

    .line 198889
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 198890
    const/16 v3, 0x2f

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3f

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3d

    if-ne v2, v3, :cond_3

    .line 198891
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 198892
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198893
    :cond_4
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 198922
    invoke-static {p2}, LX/17Z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 198923
    iget-object v0, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x570001

    iget-object v1, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "tag_name"

    move v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIILjava/lang/String;Ljava/lang/String;)V

    .line 198924
    return-void
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 198919
    iget-object v1, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz p2, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-interface {v1, p1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 198920
    return-void

    .line 198921
    :cond_0
    iget-short v0, p0, LX/17Z;->e:S

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x570001

    .line 198903
    invoke-static {p2}, LX/17Z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198904
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 198905
    iget-object v2, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v3, "tag_name"

    invoke-interface {v2, v4, v1, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 198906
    iget-object v2, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 198907
    const/16 v0, 0x33

    iput-short v0, p0, LX/17Z;->e:S

    .line 198908
    :goto_0
    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198909
    if-eqz p1, :cond_0

    .line 198910
    iget-object v0, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 198911
    :cond_0
    return-void

    .line 198912
    :cond_1
    iget-object v2, p0, LX/17Z;->c:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 198913
    const/16 v2, 0xa

    iput-short v2, p0, LX/17Z;->e:S

    .line 198914
    iget-object v2, p0, LX/17Z;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198915
    :cond_2
    iget-object v2, p0, LX/17Z;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 198916
    const/16 v2, 0xb

    iput-short v2, p0, LX/17Z;->e:S

    .line 198917
    iget-object v2, p0, LX/17Z;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198918
    :cond_3
    const/4 v0, 0x2

    iput-short v0, p0, LX/17Z;->e:S

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const v3, 0x570001

    .line 198894
    if-nez p1, :cond_1

    .line 198895
    :cond_0
    :goto_0
    return-void

    .line 198896
    :cond_1
    if-eqz p2, :cond_2

    .line 198897
    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\d+"

    const-string v2, "#"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198898
    iget-object v2, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v3, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 198899
    :cond_2
    iget-object v1, p0, LX/17Z;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz p2, :cond_3

    const/4 v0, 0x3

    :goto_1
    invoke-interface {v1, v3, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 198900
    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198901
    iget-object v0, p0, LX/17Z;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 198902
    :cond_3
    iget-short v0, p0, LX/17Z;->e:S

    goto :goto_1
.end method
