.class public LX/16t;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 187700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187701
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;J)V
    .locals 5

    .prologue
    .line 187691
    invoke-virtual {p0, p1, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(J)V

    .line 187692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 187694
    :cond_0
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 187695
    if-eqz v0, :cond_1

    .line 187696
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187697
    invoke-static {v0, p1, p2}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 187698
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 187699
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V
    .locals 1

    .prologue
    .line 187687
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 187688
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, p1, p2}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 187689
    :goto_0
    return-void

    .line 187690
    :cond_0
    invoke-interface {p0, p1, p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->a(J)V

    goto :goto_0
.end method
