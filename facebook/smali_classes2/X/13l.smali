.class public LX/13l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static b:LX/13l;

.field public static c:Z

.field private static l:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13m;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2oz;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15m;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Z

.field private final i:Landroid/content/Context;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/Context;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/13m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2oz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15m;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177430
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/13l;->a:Ljava/util/List;

    .line 177431
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/13l;->d:Ljava/util/List;

    .line 177432
    iput-object p1, p0, LX/13l;->e:LX/0Ot;

    .line 177433
    iput-object p2, p0, LX/13l;->f:LX/0Ot;

    .line 177434
    iput-object p3, p0, LX/13l;->g:LX/0Ot;

    .line 177435
    const/16 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/13l;->h:Z

    .line 177436
    iput-object p5, p0, LX/13l;->i:Landroid/content/Context;

    .line 177437
    iput-object p6, p0, LX/13l;->j:LX/0Ot;

    .line 177438
    iput-object p7, p0, LX/13l;->k:LX/0Ot;

    .line 177439
    return-void
.end method

.method public static a(LX/0QB;)LX/13l;
    .locals 11

    .prologue
    .line 177418
    const-class v1, LX/13l;

    monitor-enter v1

    .line 177419
    :try_start_0
    sget-object v0, LX/13l;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 177420
    sput-object v2, LX/13l;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 177421
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177422
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 177423
    new-instance v3, LX/13l;

    const/16 v4, 0x1334

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x134e

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12f9

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const/16 v9, 0x455

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc49

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/13l;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/Context;LX/0Ot;LX/0Ot;)V

    .line 177424
    move-object v0, v3

    .line 177425
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 177426
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/13l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177427
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 177428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/13l;)V
    .locals 0

    .prologue
    .line 177416
    sput-object p0, LX/13l;->b:LX/13l;

    .line 177417
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177406
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/13l;->h:Z

    if-eqz v0, :cond_2

    .line 177407
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa4

    if-ne p1, v0, :cond_2

    .line 177408
    :cond_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177409
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177410
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177411
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177412
    if-eqz v0, :cond_1

    .line 177413
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177414
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177415
    :cond_2
    :try_start_1
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 177396
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177397
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177398
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177399
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177400
    if-eqz v0, :cond_0

    .line 177401
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    sget-object v3, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v1, v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    sget-object v3, LX/2qV;->PLAYING:LX/2qV;

    if-ne v1, v3, :cond_2

    .line 177402
    :cond_1
    iget-object v1, p0, LX/13l;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15m;

    invoke-interface {v1}, LX/15m;->c()V

    .line 177403
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177405
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/04g;)V
    .locals 5

    .prologue
    .line 177385
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177386
    invoke-virtual {p0}, LX/13l;->d()V

    .line 177387
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177388
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177389
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177390
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v2

    sget-object v4, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v2, v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v2

    sget-object v4, LX/2qV;->PLAYING:LX/2qV;

    if-ne v2, v4, :cond_0

    .line 177391
    :cond_1
    iget-object v2, p0, LX/13l;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15m;

    invoke-interface {v2}, LX/15m;->c()V

    .line 177392
    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 177393
    iget-object v1, p0, LX/13l;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177395
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 177309
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15m;

    invoke-interface {v0, p1}, LX/15m;->a(Landroid/content/Context;)V

    .line 177310
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177311
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177312
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177313
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177314
    if-eqz v0, :cond_0

    .line 177315
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177317
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 177375
    monitor-enter p0

    .line 177376
    :try_start_0
    iget-object v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v0

    .line 177377
    if-eqz v0, :cond_0

    .line 177378
    iget-object v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v0

    .line 177379
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 177380
    sget-object v1, LX/04G;->WATCH_AND_GO:LX/04G;

    if-ne v0, v1, :cond_0

    .line 177381
    sput-object p0, LX/13l;->b:LX/13l;

    .line 177382
    :cond_0
    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177383
    monitor-exit p0

    return-void

    .line 177384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2pb;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 177346
    monitor-enter p0

    .line 177347
    :try_start_0
    iget-boolean v0, p1, LX/2pb;->n:Z

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177348
    if-nez v0, :cond_0

    move v0, v1

    .line 177349
    :goto_0
    monitor-exit p0

    return v0

    .line 177350
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177351
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177352
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177353
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177354
    if-eqz v0, :cond_1

    .line 177355
    iget-object v4, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v4

    .line 177356
    if-eqz v0, :cond_1

    if-eq v0, p1, :cond_1

    .line 177357
    iget-boolean v4, v0, LX/2pb;->n:Z

    move v4, v4

    .line 177358
    if-eqz v4, :cond_1

    invoke-virtual {v0}, LX/2pb;->o()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 177359
    if-eqz p2, :cond_2

    .line 177360
    iget-object v4, v0, LX/2pb;->D:LX/04G;

    move-object v4, v4

    .line 177361
    sget-object v5, LX/04G;->WATCH_AND_GO:LX/04G;

    if-eq v4, v5, :cond_2

    .line 177362
    sget-object v4, LX/04g;->BY_MANAGER:LX/04g;

    invoke-virtual {v0, v4}, LX/2pb;->b(LX/04g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 177363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v2

    .line 177364
    goto :goto_0

    .line 177365
    :cond_3
    :try_start_2
    sget-object v0, LX/13l;->b:LX/13l;

    if-eqz v0, :cond_5

    sget-object v0, LX/13l;->b:LX/13l;

    if-eq v0, p0, :cond_5

    .line 177366
    if-eqz p2, :cond_4

    .line 177367
    iget-object v0, p0, LX/13l;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/13l;->i:Landroid/content/Context;

    sget-object v3, LX/0ax;->je:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 177368
    iget-object v0, p0, LX/13l;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/13l;->i:Landroid/content/Context;

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    move v0, v1

    .line 177369
    goto :goto_0

    .line 177370
    :cond_4
    sget-object v0, LX/13l;->b:LX/13l;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/13l;->a(LX/2pb;Z)Z

    move-result v0

    goto/16 :goto_0

    .line 177371
    :cond_5
    sget-boolean v0, LX/13l;->c:Z

    if-eqz v0, :cond_7

    .line 177372
    iget-object v0, p1, LX/2pb;->D:LX/04G;

    move-object v0, v0

    .line 177373
    sget-object v3, LX/04G;->WATCH_AND_GO:LX/04G;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v0, v3, :cond_6

    move v0, v1

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 177374
    goto/16 :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 177338
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177339
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177340
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177341
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177342
    if-eqz v0, :cond_0

    .line 177343
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177345
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(LX/04g;)V
    .locals 4

    .prologue
    .line 177329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->d:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177330
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177331
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177332
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177333
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v2

    sget-object v3, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v2

    sget-object v3, LX/2qV;->PLAYING:LX/2qV;

    if-eq v2, v3, :cond_0

    .line 177334
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177336
    :cond_1
    :try_start_1
    invoke-virtual {p0}, LX/13l;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177337
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 177321
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/13l;->a:Ljava/util/List;

    invoke-static {v0}, LX/13m;->a(Ljava/util/Collection;)V

    .line 177322
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/13l;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 177323
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177324
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 177325
    if-eqz v0, :cond_0

    .line 177326
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177328
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 177318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177319
    monitor-exit p0

    return-void

    .line 177320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
