.class public abstract LX/0P7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .locals 2

    .prologue
    .line 54948
    if-gez p1, :cond_0

    .line 54949
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "cannot store more than MAX_VALUE elements"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 54950
    :cond_0
    shr-int/lit8 v0, p0, 0x1

    add-int/2addr v0, p0

    add-int/lit8 v0, v0, 0x1

    .line 54951
    if-ge v0, p1, :cond_1

    .line 54952
    add-int/lit8 v0, p1, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 54953
    :cond_1
    if-gez v0, :cond_2

    .line 54954
    const v0, 0x7fffffff

    .line 54955
    :cond_2
    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)LX/0P7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 54956
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 54957
    invoke-virtual {p0, v1}, LX/0P7;->b(Ljava/lang/Object;)LX/0P7;

    goto :goto_0

    .line 54958
    :cond_0
    return-object p0
.end method

.method public a(Ljava/util/Iterator;)LX/0P7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 54959
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54960
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0P7;->b(Ljava/lang/Object;)LX/0P7;

    goto :goto_0

    .line 54961
    :cond_0
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)LX/0P7;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 54962
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 54963
    invoke-virtual {p0, v2}, LX/0P7;->b(Ljava/lang/Object;)LX/0P7;

    .line 54964
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54965
    :cond_0
    return-object p0
.end method

.method public abstract a()LX/0Py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;)LX/0P7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation
.end method
