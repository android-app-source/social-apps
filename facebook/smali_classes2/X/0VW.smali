.class public final LX/0VW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/00G;


# direct methods
.method public constructor <init>(LX/00G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67890
    iput-object p1, p0, LX/0VW;->a:LX/00G;

    .line 67891
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 67892
    iget-object v0, p0, LX/0VW;->a:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0VW;->a:LX/00G;

    invoke-virtual {v0}, LX/00G;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module installed in the wrong process: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0VW;->a:LX/00G;

    .line 67893
    iget-object p0, v2, LX/00G;->b:Ljava/lang/String;

    move-object v2, p0

    .line 67894
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 67895
    return-void

    .line 67896
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
