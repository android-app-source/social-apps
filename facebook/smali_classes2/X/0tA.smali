.class public LX/0tA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0tA;


# instance fields
.field public final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/3Bq;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mOptimisticOperations"
    .end annotation
.end field

.field public final b:LX/0Sh;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0Uh;

.field public final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0sk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Uh;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/Set;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Ljava/util/Set",
            "<",
            "LX/0sk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154053
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    .line 154054
    iput-object p1, p0, LX/0tA;->b:LX/0Sh;

    .line 154055
    iput-object p2, p0, LX/0tA;->c:Ljava/util/concurrent/ExecutorService;

    .line 154056
    iput-object p3, p0, LX/0tA;->d:LX/0Uh;

    .line 154057
    iput-object p4, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 154058
    iput-object p5, p0, LX/0tA;->f:Ljava/util/Set;

    .line 154059
    return-void
.end method

.method public static a(LX/0QB;)LX/0tA;
    .locals 11

    .prologue
    .line 154060
    sget-object v0, LX/0tA;->g:LX/0tA;

    if-nez v0, :cond_1

    .line 154061
    const-class v1, LX/0tA;

    monitor-enter v1

    .line 154062
    :try_start_0
    sget-object v0, LX/0tA;->g:LX/0tA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154063
    if-eqz v2, :cond_0

    .line 154064
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154065
    new-instance v3, LX/0tA;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 154066
    new-instance v8, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v9

    new-instance v10, LX/0tB;

    invoke-direct {v10, v0}, LX/0tB;-><init>(LX/0QB;)V

    invoke-direct {v8, v9, v10}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v8, v8

    .line 154067
    invoke-direct/range {v3 .. v8}, LX/0tA;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Uh;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/Set;)V

    .line 154068
    move-object v0, v3

    .line 154069
    sput-object v0, LX/0tA;->g:LX/0tA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154070
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154071
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154072
    :cond_1
    sget-object v0, LX/0tA;->g:LX/0tA;

    return-object v0

    .line 154073
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154083
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/0tA;IILjava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154075
    iget-object v1, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 154076
    :try_start_0
    iget-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 154077
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154078
    iget-object v1, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "queue_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, p2, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154079
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "optimistic_visitor_count"

    iget-object v2, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154080
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "tags_to_visit_count"

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154081
    return-void

    .line 154082
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 154039
    iget-object v0, p0, LX/0tA;->d:LX/0Uh;

    const/16 v1, 0x94

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 154040
    invoke-direct {p0}, LX/0tA;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154041
    :cond_0
    :goto_0
    return-void

    .line 154042
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154043
    const/4 v0, 0x0

    .line 154044
    iget-object v1, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 154045
    :try_start_0
    iget-object v2, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 154046
    iget-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Bq;

    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    .line 154047
    :cond_2
    iget-object v2, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154048
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154049
    if-eqz v0, :cond_0

    .line 154050
    iget-object v1, p0, LX/0tA;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$CancelRunnable;-><init>(LX/0tA;Ljava/lang/String;Ljava/util/Collection;)V

    const v0, -0x42102baf

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 154051
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/3Bq;)V
    .locals 3

    .prologue
    .line 154028
    invoke-direct {p0}, LX/0tA;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154029
    :cond_0
    :goto_0
    return-void

    .line 154030
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154031
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154032
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950001

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154033
    iget-object v1, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 154034
    :try_start_0
    iget-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154035
    iget-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154036
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154037
    iget-object v0, p0, LX/0tA;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$OptimisticRunnable;-><init>(LX/0tA;Ljava/lang/String;)V

    const v2, 0x74aeb4f5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 154038
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/3Bq;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 154022
    invoke-direct {p0}, LX/0tA;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154023
    :cond_0
    :goto_0
    return-void

    .line 154024
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154025
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154026
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154027
    iget-object v6, p0, LX/0tA;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;-><init>(LX/0tA;Ljava/lang/String;Ljava/lang/String;LX/3Bq;)V

    const v1, -0xd3e3724

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154016
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 154017
    iget-object v1, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 154018
    :try_start_0
    iget-object v2, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 154019
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154020
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 154021
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;LX/3Bq;)V
    .locals 1

    .prologue
    .line 154014
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/0tA;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bq;)V

    .line 154015
    return-void
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 154007
    iget-object v1, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 154008
    :try_start_0
    iget-object v0, p0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 154009
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154010
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 154011
    iget-object v0, p0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x950002

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 154012
    return-void

    .line 154013
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
