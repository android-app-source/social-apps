.class public LX/1Vy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CCv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Vy",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CCv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267036
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 267037
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Vy;->b:LX/0Zi;

    .line 267038
    iput-object p1, p0, LX/1Vy;->a:LX/0Ot;

    .line 267039
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vy;
    .locals 4

    .prologue
    .line 267040
    const-class v1, LX/1Vy;

    monitor-enter v1

    .line 267041
    :try_start_0
    sget-object v0, LX/1Vy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267042
    sput-object v2, LX/1Vy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267043
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267044
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267045
    new-instance v3, LX/1Vy;

    const/16 p0, 0x21e6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Vy;-><init>(LX/0Ot;)V

    .line 267046
    move-object v0, v3

    .line 267047
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267048
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267049
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 267051
    check-cast p2, LX/CCu;

    .line 267052
    iget-object v0, p0, LX/1Vy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CCv;

    iget-object v1, p2, LX/CCu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CCu;->b:LX/1Pk;

    invoke-virtual {v0, p1, v1, v2}, LX/CCv;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pk;)LX/1Dg;

    move-result-object v0

    .line 267053
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 267054
    invoke-static {}, LX/1dS;->b()V

    .line 267055
    iget v0, p1, LX/1dQ;->b:I

    .line 267056
    packed-switch v0, :pswitch_data_0

    .line 267057
    :goto_0
    return-object v2

    .line 267058
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 267059
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 267060
    check-cast v1, LX/CCu;

    .line 267061
    iget-object v3, p0, LX/1Vy;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CCv;

    iget-object v4, v1, LX/CCu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267062
    iget-object p1, v3, LX/CCv;->d:LX/0tT;

    invoke-virtual {p1}, LX/0tT;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 267063
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 267064
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;->a()Lcom/facebook/graphql/model/GraphQLFollowableTopic;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFollowableTopic;->j()Ljava/lang/String;

    move-result-object p2

    .line 267065
    iget-object p1, v3, LX/CCv;->f:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0Zb;

    const-string p0, "topic_story_header_clicked"

    const/4 v1, 0x0

    invoke-interface {p1, p0, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p1

    .line 267066
    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 267067
    const-string p0, "topic_id"

    invoke-virtual {p1, p0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 267068
    invoke-virtual {p1}, LX/0oG;->d()V

    .line 267069
    :cond_0
    sget-object p1, LX/0ax;->hB:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 267070
    iget-object p2, v3, LX/CCv;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p2, p0, p1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 267071
    :cond_1
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3d326fd2
        :pswitch_0
    .end packed-switch
.end method
