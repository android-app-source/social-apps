.class public LX/1LB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1L1;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/tooltip/PhotoReturnDetector$Listener;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1LC;

.field public final d:LX/0So;

.field public e:LX/1Zd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field private g:LX/0Uh;


# direct methods
.method public constructor <init>(LX/1L1;LX/0Uh;LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233652
    iput-object p3, p0, LX/1LB;->d:LX/0So;

    .line 233653
    iput-object p1, p0, LX/1LB;->a:LX/1L1;

    .line 233654
    iput-object p2, p0, LX/1LB;->g:LX/0Uh;

    .line 233655
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1LB;->b:Ljava/util/List;

    .line 233656
    new-instance v0, LX/1LC;

    invoke-direct {v0, p0}, LX/1LC;-><init>(LX/1LB;)V

    iput-object v0, p0, LX/1LB;->c:LX/1LC;

    .line 233657
    return-void
.end method

.method public static a(LX/0QB;)LX/1LB;
    .locals 6

    .prologue
    .line 233639
    const-class v1, LX/1LB;

    monitor-enter v1

    .line 233640
    :try_start_0
    sget-object v0, LX/1LB;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 233641
    sput-object v2, LX/1LB;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 233642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 233644
    new-instance p0, LX/1LB;

    invoke-static {v0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v3

    check-cast v3, LX/1L1;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/1LB;-><init>(LX/1L1;LX/0Uh;LX/0So;)V

    .line 233645
    move-object v0, p0

    .line 233646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 233647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1LB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 233649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1LB;)Z
    .locals 3

    .prologue
    .line 233650
    iget-object v0, p0, LX/1LB;->g:LX/0Uh;

    const/16 v1, 0x488

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
