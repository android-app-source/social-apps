.class public LX/1A4;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tz;

.field private static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static volatile d:LX/1A4;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 210064
    new-instance v0, LX/1A5;

    invoke-direct {v0}, LX/1A5;-><init>()V

    sput-object v0, LX/1A4;->a:LX/0Tz;

    .line 210065
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/1A6;->a:LX/0U1;

    .line 210066
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 210067
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1A4;->b:Ljava/lang/String;

    .line 210068
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/1A6;->a:LX/0U1;

    .line 210069
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 210070
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1A4;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 209984
    const-string v0, "saved_videos_analytics"

    const/4 v1, 0x1

    sget-object v2, LX/1A4;->a:LX/0Tz;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 209985
    return-void
.end method

.method public static a(LX/0QB;)LX/1A4;
    .locals 3

    .prologue
    .line 209986
    sget-object v0, LX/1A4;->d:LX/1A4;

    if-nez v0, :cond_1

    .line 209987
    const-class v1, LX/1A4;

    monitor-enter v1

    .line 209988
    :try_start_0
    sget-object v0, LX/1A4;->d:LX/1A4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 209989
    if-eqz v2, :cond_0

    .line 209990
    :try_start_1
    new-instance v0, LX/1A4;

    invoke-direct {v0}, LX/1A4;-><init>()V

    .line 209991
    move-object v0, v0

    .line 209992
    sput-object v0, LX/1A4;->d:LX/1A4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209993
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 209994
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209995
    :cond_1
    sget-object v0, LX/1A4;->d:LX/1A4;

    return-object v0

    .line 209996
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 209997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jf;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 209998
    :try_start_0
    invoke-static {p0, p1}, LX/1A4;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 209999
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v3, :cond_2

    move v0, v3

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 210000
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 210001
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 210002
    new-instance v2, LX/7Jf;

    invoke-direct {v2}, LX/7Jf;-><init>()V

    .line 210003
    sget-object v0, LX/1A6;->a:LX/0U1;

    .line 210004
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210005
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7Jf;->a:Ljava/lang/String;

    .line 210006
    sget-object v0, LX/1A6;->b:LX/0U1;

    .line 210007
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210008
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, LX/7Jf;->b:I

    .line 210009
    sget-object v0, LX/1A6;->c:LX/0U1;

    .line 210010
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210011
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, LX/7Jf;->c:J

    .line 210012
    sget-object v0, LX/1A6;->d:LX/0U1;

    .line 210013
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210014
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, LX/7Jf;->d:J

    .line 210015
    sget-object v0, LX/1A6;->e:LX/0U1;

    .line 210016
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210017
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, LX/7Jf;->e:I

    .line 210018
    sget-object v0, LX/1A6;->f:LX/0U1;

    .line 210019
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 210020
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7Jf;->f:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 210021
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 210022
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 210023
    :cond_1
    return-object v2

    .line 210024
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 210025
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 210026
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 210027
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;JLjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 210028
    invoke-static {p0, p1}, LX/1A4;->e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210029
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Analytics record already exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210030
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 210031
    sget-object v2, LX/1A6;->a:LX/0U1;

    .line 210032
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210033
    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210034
    sget-object v2, LX/1A6;->b:LX/0U1;

    .line 210035
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210036
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210037
    sget-object v2, LX/1A6;->c:LX/0U1;

    .line 210038
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210039
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 210040
    sget-object v2, LX/1A6;->d:LX/0U1;

    .line 210041
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210042
    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210043
    sget-object v2, LX/1A6;->e:LX/0U1;

    .line 210044
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210045
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210046
    sget-object v2, LX/1A6;->f:LX/0U1;

    .line 210047
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 210048
    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210049
    const-string v2, "saved_videos_analytics"

    const/4 v3, 0x0

    const v4, -0x3ae08109

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const v1, 0x189f15c7

    invoke-static {v1}, LX/03h;->a(I)V

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 210050
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .line 210051
    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 210052
    const-string v1, "saved_videos_analytics"

    sget-object v3, LX/1A4;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 210053
    const/4 v2, 0x0

    .line 210054
    :try_start_0
    invoke-static {p0, p1}, LX/1A4;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 210055
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gt v3, v0, :cond_1

    move v3, v0

    :goto_0
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 210056
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-ne v3, v0, :cond_2

    .line 210057
    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 210058
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    move v3, v1

    .line 210059
    goto :goto_0

    :cond_2
    move v0, v1

    .line 210060
    goto :goto_1

    .line 210061
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 210062
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 210063
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
