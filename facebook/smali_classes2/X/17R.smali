.class public LX/17R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/17R;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 197830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197831
    return-void
.end method

.method public static a(LX/0QB;)LX/17R;
    .locals 3

    .prologue
    .line 197832
    sget-object v0, LX/17R;->a:LX/17R;

    if-nez v0, :cond_1

    .line 197833
    const-class v1, LX/17R;

    monitor-enter v1

    .line 197834
    :try_start_0
    sget-object v0, LX/17R;->a:LX/17R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 197835
    if-eqz v2, :cond_0

    .line 197836
    :try_start_1
    new-instance v0, LX/17R;

    invoke-direct {v0}, LX/17R;-><init>()V

    .line 197837
    move-object v0, v0

    .line 197838
    sput-object v0, LX/17R;->a:LX/17R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197839
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 197840
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 197841
    :cond_1
    sget-object v0, LX/17R;->a:LX/17R;

    return-object v0

    .line 197842
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 197843
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 197844
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 197845
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197846
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 197847
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
