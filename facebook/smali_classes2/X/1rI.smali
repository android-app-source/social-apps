.class public final LX/1rI;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/0yW;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1rI;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331948
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 331949
    return-void
.end method

.method public static a(LX/0QB;)LX/1rI;
    .locals 4

    .prologue
    .line 331950
    sget-object v0, LX/1rI;->a:LX/1rI;

    if-nez v0, :cond_1

    .line 331951
    const-class v1, LX/1rI;

    monitor-enter v1

    .line 331952
    :try_start_0
    sget-object v0, LX/1rI;->a:LX/1rI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331953
    if-eqz v2, :cond_0

    .line 331954
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331955
    new-instance v3, LX/1rI;

    const/16 p0, 0x13c7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1rI;-><init>(LX/0Ot;)V

    .line 331956
    move-object v0, v3

    .line 331957
    sput-object v0, LX/1rI;->a:LX/1rI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331960
    :cond_1
    sget-object v0, LX/1rI;->a:LX/1rI;

    return-object v0

    .line 331961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 331963
    check-cast p3, LX/0yW;

    .line 331964
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 331965
    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331966
    sget-object v0, LX/1lF;->APP_FOREGROUND_TRIGGER:LX/1lF;

    invoke-virtual {p3, v0}, LX/0yW;->a(LX/1lF;)V

    .line 331967
    :cond_0
    return-void
.end method
