.class public final enum LX/1Wu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Wu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Wu;

.field public static final enum NOT_SCHEDULED:LX/1Wu;

.field public static final enum PLAYED:LX/1Wu;

.field public static final enum SCHEDULED:LX/1Wu;

.field public static final enum WAITING_FOR_INTERACTION_STOP:LX/1Wu;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270127
    new-instance v0, LX/1Wu;

    const-string v1, "NOT_SCHEDULED"

    invoke-direct {v0, v1, v2}, LX/1Wu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wu;->NOT_SCHEDULED:LX/1Wu;

    .line 270128
    new-instance v0, LX/1Wu;

    const-string v1, "SCHEDULED"

    invoke-direct {v0, v1, v3}, LX/1Wu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wu;->SCHEDULED:LX/1Wu;

    .line 270129
    new-instance v0, LX/1Wu;

    const-string v1, "WAITING_FOR_INTERACTION_STOP"

    invoke-direct {v0, v1, v4}, LX/1Wu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wu;->WAITING_FOR_INTERACTION_STOP:LX/1Wu;

    .line 270130
    new-instance v0, LX/1Wu;

    const-string v1, "PLAYED"

    invoke-direct {v0, v1, v5}, LX/1Wu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wu;->PLAYED:LX/1Wu;

    .line 270131
    const/4 v0, 0x4

    new-array v0, v0, [LX/1Wu;

    sget-object v1, LX/1Wu;->NOT_SCHEDULED:LX/1Wu;

    aput-object v1, v0, v2

    sget-object v1, LX/1Wu;->SCHEDULED:LX/1Wu;

    aput-object v1, v0, v3

    sget-object v1, LX/1Wu;->WAITING_FOR_INTERACTION_STOP:LX/1Wu;

    aput-object v1, v0, v4

    sget-object v1, LX/1Wu;->PLAYED:LX/1Wu;

    aput-object v1, v0, v5

    sput-object v0, LX/1Wu;->$VALUES:[LX/1Wu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 270126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Wu;
    .locals 1

    .prologue
    .line 270125
    const-class v0, LX/1Wu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Wu;

    return-object v0
.end method

.method public static values()[LX/1Wu;
    .locals 1

    .prologue
    .line 270124
    sget-object v0, LX/1Wu;->$VALUES:[LX/1Wu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Wu;

    return-object v0
.end method
