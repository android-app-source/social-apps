.class public LX/11Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/11Q;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172166
    iput-object p1, p0, LX/11Q;->a:LX/0Uh;

    .line 172167
    return-void
.end method

.method public static a(LX/0QB;)LX/11Q;
    .locals 4

    .prologue
    .line 172168
    sget-object v0, LX/11Q;->b:LX/11Q;

    if-nez v0, :cond_1

    .line 172169
    const-class v1, LX/11Q;

    monitor-enter v1

    .line 172170
    :try_start_0
    sget-object v0, LX/11Q;->b:LX/11Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172171
    if-eqz v2, :cond_0

    .line 172172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 172173
    new-instance p0, LX/11Q;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/11Q;-><init>(LX/0Uh;)V

    .line 172174
    move-object v0, p0

    .line 172175
    sput-object v0, LX/11Q;->b:LX/11Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172176
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172177
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172178
    :cond_1
    sget-object v0, LX/11Q;->b:LX/11Q;

    return-object v0

    .line 172179
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172180
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 172181
    iget-object v0, p0, LX/11Q;->a:LX/0Uh;

    const/16 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/14U;
    .locals 2

    .prologue
    .line 172182
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 172183
    invoke-direct {p0}, LX/11Q;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 172184
    sget-object v1, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v0, v1}, LX/14U;->a(LX/14V;)V

    .line 172185
    :cond_0
    return-object v0
.end method

.method public final a(LX/14U;)V
    .locals 1

    .prologue
    .line 172186
    invoke-direct {p0}, LX/11Q;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172187
    sget-object v0, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {p1, v0}, LX/14U;->a(LX/14V;)V

    .line 172188
    :cond_0
    return-void
.end method
