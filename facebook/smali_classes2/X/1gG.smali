.class public final enum LX/1gG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1gG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1gG;

.field public static final enum CONTENT:LX/1gG;

.field public static final enum TEMP:LX/1gG;


# instance fields
.field public final extension:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293923
    new-instance v0, LX/1gG;

    const-string v1, "CONTENT"

    const-string v2, ".cnt"

    invoke-direct {v0, v1, v3, v2}, LX/1gG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1gG;->CONTENT:LX/1gG;

    .line 293924
    new-instance v0, LX/1gG;

    const-string v1, "TEMP"

    const-string v2, ".tmp"

    invoke-direct {v0, v1, v4, v2}, LX/1gG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1gG;->TEMP:LX/1gG;

    .line 293925
    const/4 v0, 0x2

    new-array v0, v0, [LX/1gG;

    sget-object v1, LX/1gG;->CONTENT:LX/1gG;

    aput-object v1, v0, v3

    sget-object v1, LX/1gG;->TEMP:LX/1gG;

    aput-object v1, v0, v4

    sput-object v0, LX/1gG;->$VALUES:[LX/1gG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 293920
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 293921
    iput-object p3, p0, LX/1gG;->extension:Ljava/lang/String;

    .line 293922
    return-void
.end method

.method public static fromExtension(Ljava/lang/String;)LX/1gG;
    .locals 1

    .prologue
    .line 293926
    const-string v0, ".cnt"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293927
    sget-object v0, LX/1gG;->CONTENT:LX/1gG;

    .line 293928
    :goto_0
    return-object v0

    .line 293929
    :cond_0
    const-string v0, ".tmp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293930
    sget-object v0, LX/1gG;->TEMP:LX/1gG;

    goto :goto_0

    .line 293931
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1gG;
    .locals 1

    .prologue
    .line 293919
    const-class v0, LX/1gG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1gG;

    return-object v0
.end method

.method public static values()[LX/1gG;
    .locals 1

    .prologue
    .line 293918
    sget-object v0, LX/1gG;->$VALUES:[LX/1gG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1gG;

    return-object v0
.end method
