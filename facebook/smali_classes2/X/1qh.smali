.class public final LX/1qh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1qs;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1qs;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 331304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331305
    iput-object p1, p0, LX/1qh;->a:LX/0QB;

    .line 331306
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 331307
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1qh;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 331308
    packed-switch p2, :pswitch_data_0

    .line 331309
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331310
    :pswitch_0
    new-instance v1, LX/1qu;

    const/16 v0, 0xc

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, p0, v0}, LX/1qu;-><init>(LX/0Or;LX/0Uh;)V

    .line 331311
    move-object v0, v1

    .line 331312
    :goto_0
    return-object v0

    .line 331313
    :pswitch_1
    new-instance v0, LX/1qq;

    const/16 v1, 0xc

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1qq;-><init>(LX/0Or;)V

    .line 331314
    move-object v0, v0

    .line 331315
    goto :goto_0

    .line 331316
    :pswitch_2
    new-instance v0, LX/1qp;

    invoke-direct {v0}, LX/1qp;-><init>()V

    .line 331317
    move-object v0, v0

    .line 331318
    move-object v0, v0

    .line 331319
    goto :goto_0

    .line 331320
    :pswitch_3
    new-instance v0, LX/1qo;

    invoke-direct {v0}, LX/1qo;-><init>()V

    .line 331321
    move-object v0, v0

    .line 331322
    move-object v0, v0

    .line 331323
    goto :goto_0

    .line 331324
    :pswitch_4
    new-instance v0, LX/1v7;

    invoke-direct {v0}, LX/1v7;-><init>()V

    .line 331325
    move-object v0, v0

    .line 331326
    move-object v0, v0

    .line 331327
    goto :goto_0

    .line 331328
    :pswitch_5
    new-instance v0, LX/1qn;

    invoke-direct {v0}, LX/1qn;-><init>()V

    .line 331329
    move-object v0, v0

    .line 331330
    move-object v0, v0

    .line 331331
    goto :goto_0

    .line 331332
    :pswitch_6
    new-instance v0, LX/1v8;

    invoke-direct {v0}, LX/1v8;-><init>()V

    .line 331333
    move-object v0, v0

    .line 331334
    move-object v0, v0

    .line 331335
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 331336
    const/4 v0, 0x7

    return v0
.end method
