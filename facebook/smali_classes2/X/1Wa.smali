.class public LX/1Wa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Wc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5p;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Wb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269033
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 269034
    iput-object v0, p0, LX/1Wa;->a:LX/0Ot;

    .line 269035
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 269036
    iput-object v0, p0, LX/1Wa;->b:LX/0Ot;

    .line 269037
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 269038
    iput-object v0, p0, LX/1Wa;->c:LX/0Ot;

    .line 269039
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 269040
    iput-object v0, p0, LX/1Wa;->d:LX/0Ot;

    .line 269041
    return-void
.end method

.method public static a(LX/0QB;)LX/1Wa;
    .locals 8

    .prologue
    .line 269042
    const-class v1, LX/1Wa;

    monitor-enter v1

    .line 269043
    :try_start_0
    sget-object v0, LX/1Wa;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269044
    sput-object v2, LX/1Wa;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269045
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269046
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269047
    new-instance v3, LX/1Wa;

    invoke-direct {v3}, LX/1Wa;-><init>()V

    .line 269048
    const/16 v4, 0x8f8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1f71

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x78c

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ee

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class p0, LX/1Wb;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/1Wb;

    .line 269049
    iput-object v4, v3, LX/1Wa;->a:LX/0Ot;

    iput-object v5, v3, LX/1Wa;->b:LX/0Ot;

    iput-object v6, v3, LX/1Wa;->c:LX/0Ot;

    iput-object v7, v3, LX/1Wa;->d:LX/0Ot;

    iput-object p0, v3, LX/1Wa;->e:LX/1Wb;

    .line 269050
    move-object v0, v3

    .line 269051
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269052
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Wa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269053
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269054
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
