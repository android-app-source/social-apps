.class public LX/15R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/15R;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile i:LX/15R;


# instance fields
.field public final c:LX/0W3;

.field public final d:LX/0Xl;

.field public e:LX/0Yb;

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/15T;

.field public h:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 180569
    const-class v0, LX/15R;

    sput-object v0, LX/15R;->b:Ljava/lang/Class;

    .line 180570
    new-instance v0, LX/15S;

    invoke-direct {v0}, LX/15S;-><init>()V

    .line 180571
    iget-object v1, v0, LX/0gW;->f:Ljava/lang/String;

    move-object v0, v1

    .line 180572
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/15R;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/0Xl;LX/15T;)V
    .locals 2
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180605
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/15R;->f:Ljava/lang/ref/WeakReference;

    .line 180606
    iput-object p2, p0, LX/15R;->d:LX/0Xl;

    .line 180607
    iput-object p1, p0, LX/15R;->c:LX/0W3;

    .line 180608
    iput-object p3, p0, LX/15R;->g:LX/15T;

    .line 180609
    return-void
.end method

.method public static a(LX/0QB;)LX/15R;
    .locals 7

    .prologue
    .line 180610
    sget-object v0, LX/15R;->i:LX/15R;

    if-nez v0, :cond_1

    .line 180611
    const-class v1, LX/15R;

    monitor-enter v1

    .line 180612
    :try_start_0
    sget-object v0, LX/15R;->i:LX/15R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180613
    if-eqz v2, :cond_0

    .line 180614
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180615
    new-instance v6, LX/15R;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    .line 180616
    new-instance p0, LX/15T;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v5}, LX/15T;-><init>(LX/0SG;)V

    .line 180617
    move-object v5, p0

    .line 180618
    check-cast v5, LX/15T;

    invoke-direct {v6, v3, v4, v5}, LX/15R;-><init>(LX/0W3;LX/0Xl;LX/15T;)V

    .line 180619
    move-object v0, v6

    .line 180620
    sput-object v0, LX/15R;->i:LX/15R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180623
    :cond_1
    sget-object v0, LX/15R;->i:LX/15R;

    return-object v0

    .line 180624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/15R;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 180589
    const-string v0, "BlockAccessRestrictionSummary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180590
    const-string v0, "BlockAccessRestrictionDescription"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180591
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180592
    :cond_0
    const-string v0, "GraphqlErrorCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 180593
    sget-object v1, LX/15R;->b:Ljava/lang/Class;

    const-string v2, "Missing title or description for error "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180594
    :goto_0
    return-void

    .line 180595
    :cond_1
    iget-object v0, p0, LX/15R;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 180596
    sget-object v0, LX/15R;->b:Ljava/lang/Class;

    const-string v1, "Should not receive broadcasts when there\'s no foreground activity"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 180597
    :cond_2
    iget-object v3, p0, LX/15R;->g:LX/15T;

    iget-object v0, p0, LX/15R;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 180598
    iget-object v4, v3, LX/15T;->a:LX/0qK;

    invoke-virtual {v4}, LX/0qK;->a()Z

    move-result v4

    if-nez v4, :cond_3

    .line 180599
    :goto_1
    goto :goto_0

    .line 180600
    :cond_3
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 180601
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p0

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 180602
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p0

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180603
    new-instance p0, LX/0ju;

    invoke-direct {p0, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v4

    const p0, 0x7f080016

    new-instance p1, LX/8iI;

    invoke-direct {p1, v3}, LX/8iI;-><init>(LX/15T;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v4

    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    goto :goto_1
.end method

.method public static a$redex0(LX/15R;Landroid/content/Intent;)V
    .locals 11

    .prologue
    .line 180573
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 180574
    const-string v1, "GraphQLOperationName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180575
    iget-object v2, p0, LX/15R;->c:LX/0W3;

    sget-wide v4, LX/0X5;->kB:J

    const/4 v3, 0x0

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v2

    .line 180576
    if-eqz v2, :cond_1

    .line 180577
    iget-object v6, p0, LX/15R;->h:LX/0Rf;

    if-nez v6, :cond_0

    .line 180578
    iget-object v7, p0, LX/15R;->c:LX/0W3;

    sget-wide v9, LX/0X5;->kA:J

    const-string v8, "[]"

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 180579
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v8

    new-instance v9, LX/8iH;

    invoke-direct {v9}, LX/8iH;-><init>()V

    invoke-virtual {v8, v7, v9}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180580
    :goto_0
    invoke-static {v8}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v8

    move-object v7, v8

    .line 180581
    iput-object v7, p0, LX/15R;->h:LX/0Rf;

    .line 180582
    :cond_0
    iget-object v6, p0, LX/15R;->h:LX/0Rf;

    invoke-virtual {v6, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 180583
    :goto_1
    return-void

    .line 180584
    :cond_1
    sget-object v2, LX/15R;->a:LX/0Rf;

    invoke-virtual {v2, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 180585
    :goto_2
    goto :goto_1

    .line 180586
    :cond_2
    invoke-static {p0, v0}, LX/15R;->a(LX/15R;Landroid/os/Bundle;)V

    goto :goto_1

    .line 180587
    :catch_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 180588
    :cond_3
    invoke-static {p0, v0}, LX/15R;->a(LX/15R;Landroid/os/Bundle;)V

    goto :goto_2
.end method
