.class public LX/0sh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0si;
.implements LX/0sk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile w:LX/0sh;


# instance fields
.field public a:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final c:LX/0sl;

.field private final d:LX/0SG;

.field private final e:LX/0t3;

.field private final f:LX/0SI;

.field private final g:LX/0t4;

.field private final h:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final i:LX/0ox;

.field private final j:LX/0t5;

.field private final k:LX/0t8;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0sT;

.field private final n:LX/0t9;

.field private final o:LX/0t5;

.field private final p:LX/0tC;

.field private final q:LX/0Zb;

.field private final r:LX/0tD;

.field public s:LX/0t2;

.field private t:LX/15j;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final u:Ljava/io/File;

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152835
    const-class v0, LX/0si;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0sh;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0sl;LX/0pq;LX/0SG;LX/0t2;LX/0SI;LX/0t3;LX/0t4;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ox;LX/0t5;LX/0sT;LX/0t8;LX/0Ot;LX/0t9;LX/0t5;LX/0Zb;LX/0tC;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0sl;",
            "LX/0pq;",
            "LX/0SG;",
            "LX/0t2;",
            "LX/0SI;",
            "LX/0t3;",
            "LX/0t4;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0ox;",
            "Lcom/facebook/graphql/executor/iface/ObserverRegistry;",
            "LX/0sT;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;",
            "LX/0t9;",
            "LX/0t5;",
            "LX/0Zb;",
            "LX/0tC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152837
    const-wide/32 v2, 0x19000

    iput-wide v2, p0, LX/0sh;->a:J

    .line 152838
    const/4 v2, 0x0

    iput-object v2, p0, LX/0sh;->t:LX/15j;

    .line 152839
    iput-object p2, p0, LX/0sh;->c:LX/0sl;

    .line 152840
    iput-object p4, p0, LX/0sh;->d:LX/0SG;

    .line 152841
    iput-object p5, p0, LX/0sh;->s:LX/0t2;

    .line 152842
    iput-object p7, p0, LX/0sh;->e:LX/0t3;

    .line 152843
    iput-object p6, p0, LX/0sh;->f:LX/0SI;

    .line 152844
    iput-object p8, p0, LX/0sh;->g:LX/0t4;

    .line 152845
    iput-object p9, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 152846
    iput-object p10, p0, LX/0sh;->i:LX/0ox;

    .line 152847
    iput-object p11, p0, LX/0sh;->j:LX/0t5;

    .line 152848
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0sh;->k:LX/0t8;

    .line 152849
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0sh;->l:LX/0Ot;

    .line 152850
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0sh;->m:LX/0sT;

    .line 152851
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0sh;->n:LX/0t9;

    .line 152852
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0sh;->o:LX/0t5;

    .line 152853
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0sh;->q:LX/0Zb;

    .line 152854
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0sh;->p:LX/0tC;

    .line 152855
    invoke-virtual {p3, p0}, LX/0pq;->a(LX/0po;)V

    .line 152856
    invoke-static {p1}, LX/0sh;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    iput-object v2, p0, LX/0sh;->u:Ljava/io/File;

    .line 152857
    new-instance v2, LX/0tD;

    iget-object v3, p0, LX/0sh;->u:Ljava/io/File;

    invoke-direct {v2, v3}, LX/0tD;-><init>(Ljava/io/File;)V

    iput-object v2, p0, LX/0sh;->r:LX/0tD;

    .line 152858
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 152859
    const-string v0, "queries"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/0sr;->b:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=? AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/0sr;->d:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 152860
    int-to-long v0, v0

    return-wide v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 152861
    const-string v0, "queries"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/0sr;->a:LX/0U1;

    .line 152862
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 152863
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IN ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 152864
    int-to-long v0, v0

    return-wide v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;[J)J
    .locals 4

    .prologue
    .line 152865
    array-length v0, p1

    if-nez v0, :cond_0

    .line 152866
    const-wide/16 v0, 0x0

    .line 152867
    :goto_0
    return-wide v0

    .line 152868
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v0, LX/0sr;->a:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 152869
    const-string v0, " IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152870
    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 152871
    if-eqz v0, :cond_1

    .line 152872
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152873
    :cond_1
    aget-wide v2, p1, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 152874
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 152875
    :cond_2
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152876
    const-string v0, "queries"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 152877
    int-to-long v0, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 152878
    iget-object v0, p0, LX/0sh;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 152879
    iget-object v2, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 152880
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/0sr;->a:LX/0U1;

    .line 152881
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 152882
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FROM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "queries "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "WHERE (? - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->e:LX/0U1;

    .line 152883
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 152884
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->j:LX/0U1;

    .line 152885
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 152886
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->b:LX/0U1;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " NOT IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 152887
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v4, v5

    move-object v0, v4

    .line 152888
    invoke-static {v2, v3, v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 152889
    invoke-static {v2}, LX/0sh;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152890
    return-wide v0
.end method

.method public static a(LX/0QB;)LX/0sh;
    .locals 3

    .prologue
    .line 152891
    sget-object v0, LX/0sh;->w:LX/0sh;

    if-nez v0, :cond_1

    .line 152892
    const-class v1, LX/0sh;

    monitor-enter v1

    .line 152893
    :try_start_0
    sget-object v0, LX/0sh;->w:LX/0sh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152894
    if-eqz v2, :cond_0

    .line 152895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0sh;->b(LX/0QB;)LX/0sh;

    move-result-object v0

    sput-object v0, LX/0sh;->w:LX/0sh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152896
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152897
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152898
    :cond_1
    sget-object v0, LX/0sh;->w:LX/0sh;

    return-object v0

    .line 152899
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/Cursor;II)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "II)",
            "LX/0w5",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152828
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 152829
    if-eqz v0, :cond_0

    .line 152830
    invoke-static {v0}, LX/0w5;->a([B)LX/0w5;

    move-result-object v0

    .line 152831
    :goto_0
    return-object v0

    .line 152832
    :cond_0
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152833
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 152834
    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/0sh;Ljava/lang/String;[Ljava/lang/String;LX/0U1;JZZ)LX/1lM;
    .locals 24
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152901
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p0}, LX/0sh;->h()V

    .line 152902
    if-eqz p7, :cond_0

    .line 152903
    invoke-static/range {p6 .. p6}, LX/0PB;->checkArgument(Z)V

    .line 152904
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v22

    .line 152905
    const-wide/16 v6, -0x1

    .line 152906
    const v4, -0x3a07d491

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152907
    :try_start_1
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v23

    .line 152908
    :try_start_2
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 152909
    sget-object v4, LX/0sr;->a:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 152910
    sget-object v4, LX/0sr;->e:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 152911
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0sh;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v4

    sub-long/2addr v4, v10

    .line 152912
    cmp-long v4, v4, p4

    if-lez v4, :cond_1

    .line 152913
    :try_start_3
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 152914
    :try_start_4
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152915
    const v4, 0x20a1c01e

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v5, 0x0

    :goto_0
    monitor-exit p0

    return-object v5

    .line 152916
    :cond_1
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, LX/0sh;->b(Landroid/database/Cursor;)LX/1lL;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v4

    .line 152917
    if-nez v4, :cond_2

    .line 152918
    :try_start_6
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 152919
    :try_start_7
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152920
    const v4, -0x4c9b68aa

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v5, 0x0

    goto :goto_0

    .line 152921
    :cond_2
    :try_start_8
    sget-object v5, LX/0sr;->g:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    sget-object v8, LX/0sr;->f:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v8

    move-object/from16 v0, v23

    invoke-static {v0, v5, v8}, LX/0sh;->a(Landroid/database/Cursor;II)LX/0w5;

    move-result-object v16

    .line 152922
    sget-object v5, LX/0sr;->h:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 152923
    sget-object v5, LX/0sr;->i:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v18

    .line 152924
    sget-object v5, LX/0sr;->c:LX/0U1;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 152925
    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 152926
    if-eqz p6, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, LX/0sh;->a(J)Ljava/util/Set;

    move-result-object v15

    .line 152927
    :goto_1
    if-eqz p7, :cond_4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LX/0sh;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v21

    .line 152928
    :goto_2
    new-instance v5, LX/1lM;

    iget-object v12, v4, LX/1lL;->a:Ljava/nio/ByteBuffer;

    iget-object v13, v4, LX/1lL;->b:[B

    iget-object v14, v4, LX/1lL;->c:[B

    sget-object v19, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    invoke-direct/range {v5 .. v21}, LX/1lM;-><init>(JJJLjava/nio/ByteBuffer;[B[BLjava/util/Set;LX/0w5;I[BLX/0ta;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 152929
    :try_start_9
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 152930
    :try_start_a
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152931
    const v4, 0x6fc342e9

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 152932
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 152933
    :cond_3
    :try_start_b
    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v15

    goto :goto_1

    .line 152934
    :cond_4
    sget-object v21, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_2

    .line 152935
    :cond_5
    :try_start_c
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 152936
    :try_start_d
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152937
    const v4, 0x3309ef20

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const/4 v5, 0x0

    goto/16 :goto_0

    .line 152938
    :catchall_1
    move-exception v4

    :try_start_e
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 152939
    :catch_0
    move-exception v4

    .line 152940
    :try_start_f
    sget-object v5, LX/0sh;->b:Ljava/lang/String;

    const-string v8, "Failed to deserialize cache item"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5, v4, v8, v9}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152941
    const-wide/16 v4, -0x1

    cmp-long v4, v6, v4

    if-eqz v4, :cond_6

    .line 152942
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, LX/0sh;->b(J)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 152943
    :cond_6
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152944
    const v4, -0x4a3cd254

    move-object/from16 v0, v22

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    const/4 v5, 0x0

    goto/16 :goto_0

    .line 152945
    :catchall_2
    move-exception v4

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152946
    const v5, 0x46aad772

    move-object/from16 v0, v22

    invoke-static {v0, v5}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v4
.end method

.method private static a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 152947
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152948
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152949
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "graphql_cache_models"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(LX/1lM;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 152950
    iget-object v1, p1, LX/1lM;->d:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_0

    .line 152951
    :goto_0
    return-object v0

    .line 152952
    :cond_0
    iget-object v1, p1, LX/1lM;->i:[B

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/1lM;->i:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 152953
    :goto_1
    iget-object v1, p1, LX/1lM;->j:[B

    if-eqz v1, :cond_2

    iget-object v0, p1, LX/1lM;->j:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 152954
    :goto_2
    invoke-static {p0}, LX/0sh;->g(LX/0sh;)LX/15j;

    move-result-object v5

    .line 152955
    iget v0, p1, LX/1lM;->g:I

    iget-object v1, p1, LX/1lM;->d:Ljava/nio/ByteBuffer;

    iget-object v2, p1, LX/1lM;->f:LX/0w5;

    invoke-static/range {v0 .. v5}, LX/1lN;->a(ILjava/nio/ByteBuffer;LX/0w5;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v3, v0

    .line 152956
    goto :goto_1

    :cond_2
    move-object v4, v0

    .line 152957
    goto :goto_2
.end method

.method private a(J)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152958
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 152959
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/0st;->b:LX/0U1;

    .line 152960
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 152961
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM tags"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/0st;->a:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152962
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 152963
    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 152964
    sget-object v0, LX/0st;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 152965
    const/4 v0, 0x0

    .line 152966
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 152967
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, LX/0RA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    .line 152968
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152969
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 152970
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 152971
    return-object v0
.end method

.method private static a(LX/0sh;I)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 152972
    iget-object v0, p0, LX/0sh;->j:LX/0t5;

    invoke-virtual {v0}, LX/0t5;->a()Ljava/util/Collection;

    move-result-object v0

    .line 152973
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152974
    :cond_0
    const-string v1, ""

    .line 152975
    :goto_0
    move-object v0, v1

    .line 152976
    invoke-direct {p0, v0}, LX/0sh;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 152977
    int-to-long v4, p1

    sub-long v2, v4, v2

    long-to-int v1, v2

    .line 152978
    if-gtz v1, :cond_1

    .line 152979
    :goto_1
    return-void

    .line 152980
    :cond_1
    iget-object v2, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 152981
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/0sr;->a:LX/0U1;

    .line 152982
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 152983
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FROM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "queries "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->b:LX/0U1;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " NOT IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ORDER BY "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->e:LX/0U1;

    .line 152984
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 152985
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ASC LIMIT ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 152986
    int-to-long v4, v1

    .line 152987
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v3

    move-object v1, v1

    .line 152988
    invoke-static {v2, v0, v1}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    .line 152989
    invoke-static {v2}, LX/0sh;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 152990
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 152991
    const/4 v1, 0x1

    .line 152992
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 152993
    if-nez v2, :cond_3

    .line 152994
    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152995
    :cond_3
    const-string v2, "\'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152996
    const/4 v1, 0x0

    move v2, v1

    .line 152997
    goto :goto_2

    .line 152998
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private static a(LX/0sh;ILjava/lang/String;)V
    .locals 7
    .param p1    # I
        .annotation build Lcom/facebook/graphql/executor/cache/GraphQLDiskCacheImpl$RecordSizePrefix;
        .end annotation
    .end param

    .prologue
    .line 152999
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153000
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->e()J

    move-result-wide v0

    .line 153001
    iget-object v2, p0, LX/0sh;->u:Ljava/io/File;

    invoke-static {v2}, LX/39R;->b(Ljava/io/File;)J

    move-result-wide v2

    .line 153002
    iget-object v4, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_db_size"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, p1, v5, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 153003
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_file_size"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 153004
    :cond_0
    return-void
.end method

.method private static declared-synchronized a(LX/0sh;[J)V
    .locals 3

    .prologue
    .line 153005
    monitor-enter p0

    array-length v0, p1

    if-nez v0, :cond_0

    .line 153006
    :goto_0
    monitor-exit p0

    return-void

    .line 153007
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 153008
    const v0, -0x7010d0d3

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153009
    :try_start_1
    invoke-static {v1, p1}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;[J)J

    .line 153010
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 153011
    const v0, -0x1a202658

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 153012
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 153013
    :catchall_1
    move-exception v0

    const v2, -0x79f152fc

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 153126
    if-eqz p0, :cond_0

    .line 153127
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153128
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 153014
    const-string v1, "models"

    sget-object v2, LX/0sx;->a:LX/0U1;

    const-string v3, "queries"

    sget-object v4, LX/0sr;->k:LX/0U1;

    sget-object v5, LX/0sr;->l:LX/0U1;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0U1;Ljava/lang/String;LX/0U1;LX/0U1;)V

    .line 153015
    return-void
.end method

.method private declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)V
    .locals 2
    .param p4    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153129
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3, p4}, LX/2vK;->a(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153130
    monitor-exit p0

    return-void

    .line 153131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteStatement;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x6

    .line 153132
    invoke-static {p4}, LX/0t3;->b(Ljava/lang/Object;)I

    move-result v3

    .line 153133
    if-ne v3, v7, :cond_0

    .line 153134
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported type for consistency id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153135
    :cond_0
    instance-of v0, p4, Ljava/util/List;

    move v0, v0

    .line 153136
    if-eqz v0, :cond_2

    move v1, v2

    .line 153137
    :goto_0
    const/4 v0, 0x0

    .line 153138
    if-ne v3, v6, :cond_1

    .line 153139
    invoke-static {p4}, LX/0t3;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 153140
    :cond_1
    iget-object v4, p0, LX/0sh;->e:LX/0t3;

    invoke-virtual {v4, p4}, LX/0t3;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 153141
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 153142
    invoke-virtual {p1, v2, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 153143
    if-nez p3, :cond_3

    .line 153144
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 153145
    :goto_1
    const/4 v2, 0x3

    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v2, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 153146
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 153147
    const/4 v2, 0x5

    int-to-long v4, v3

    invoke-virtual {p1, v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 153148
    if-nez v0, :cond_4

    .line 153149
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 153150
    :goto_2
    int-to-long v0, v1

    invoke-virtual {p1, v7, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 153151
    const v0, 0xe064317

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v0, 0x52527495

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153152
    return-void

    .line 153153
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 153154
    :cond_3
    invoke-virtual {p1, v5, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_1

    .line 153155
    :cond_4
    invoke-virtual {p1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_2
.end method

.method private a(Ljava/io/File;LX/0w5;JLjava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    .line 153202
    invoke-virtual {p5}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-static {v0, p2, p3, p4}, LX/1lJ;->a(ILX/0w5;J)[B

    move-result-object v0

    .line 153203
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 153204
    :try_start_0
    invoke-static {v2, p5}, LX/31S;->a(Ljava/io/FileOutputStream;Ljava/nio/ByteBuffer;)I

    .line 153205
    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 153206
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 153207
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153208
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 153209
    iget-object v1, p0, LX/0sh;->p:LX/0tC;

    invoke-virtual {v1}, LX/0tC;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153210
    iget-object v1, p0, LX/0sh;->r:LX/0tD;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p5, v0}, LX/0tD;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;[B)V

    .line 153211
    :cond_0
    return-void

    .line 153212
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153213
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/nio/ByteBuffer;Ljava/io/File;LX/0w5;J)Z
    .locals 20
    .param p7    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/ContentValues;",
            "Landroid/content/ContentValues;",
            "Ljava/nio/ByteBuffer;",
            "Ljava/io/File;",
            "LX/0w5;",
            "J)Z"
        }
    .end annotation

    .prologue
    .line 153156
    monitor-enter p0

    if-eqz p8, :cond_0

    if-eqz p7, :cond_0

    move-object/from16 v7, p0

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    move-object/from16 v12, p7

    .line 153157
    :try_start_0
    invoke-direct/range {v7 .. v12}, LX/0sh;->a(Ljava/io/File;LX/0w5;JLjava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153158
    :cond_0
    const v6, 0x14475681

    :try_start_1
    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 153159
    :try_start_2
    invoke-static/range {p1 .. p3}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    .line 153160
    const-string v6, "models"

    sget-object v7, LX/0sx;->b:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    const v8, 0x58399ca1

    invoke-static {v8}, LX/03h;->a(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    invoke-virtual {v0, v6, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    const v6, 0x1487aac4

    invoke-static {v6}, LX/03h;->a(I)V

    .line 153161
    const-wide/16 v6, -0x1

    cmp-long v6, v8, v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 153162
    sget-object v6, LX/0sr;->k:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p5

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153163
    sget-object v6, LX/0sr;->l:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p5

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153164
    const-string v6, "queries"

    const/4 v7, 0x0

    const v8, 0x7224494b

    invoke-static {v8}, LX/03h;->a(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v6, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    const v6, -0x1316b668

    invoke-static {v6}, LX/03h;->a(I)V

    .line 153165
    const-wide/16 v6, -0x1

    cmp-long v6, v18, v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 153166
    new-instance v6, LX/2vK;

    invoke-static/range {p0 .. p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0sh;->f:LX/0SI;

    invoke-interface {v8}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0sh;->u:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0sh;->p:LX/0tC;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0sh;->d:LX/0SG;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0sh;->m:LX/0sT;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/0sh;->j:LX/0t5;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0sh;->k:LX/0t8;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/0sh;->l:LX/0Ot;

    const/16 v16, 0x1

    const/16 v17, 0x0

    invoke-direct/range {v6 .. v17}, LX/2vK;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/io/File;LX/0tC;LX/0SG;LX/0sT;LX/0t5;LX/0t8;LX/0Ot;ILjava/lang/String;)V

    .line 153167
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v18

    move-object/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)V

    .line 153168
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0sh;->m:LX/0sT;

    invoke-virtual {v7}, LX/0sT;->f()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 153169
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0sh;->n:LX/0t9;

    const/4 v8, 0x1

    new-array v8, v8, [J

    const/4 v9, 0x0

    aput-wide v18, v8, v9

    invoke-virtual {v7, v6, v8}, LX/0t9;->a(LX/2vL;[J)V

    .line 153170
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 153171
    const v6, -0x7208e514

    :try_start_3
    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 153172
    :goto_2
    const/4 v6, 0x1

    :goto_3
    monitor-exit p0

    return v6

    .line 153173
    :catch_0
    const/4 v6, 0x0

    goto :goto_3

    .line 153174
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 153175
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 153176
    :catch_1
    const/4 v6, 0x2

    :try_start_4
    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    aput-object p3, v6, v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 153177
    const v6, -0x146b3abd

    :try_start_5
    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 153178
    :goto_4
    const/4 v6, 0x0

    goto :goto_3

    .line 153179
    :catch_2
    move-exception v6

    .line 153180
    const/4 v7, 0x2

    :try_start_6
    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    const/4 v8, 0x1

    aput-object p3, v7, v8

    .line 153181
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 153182
    :catchall_0
    move-exception v6

    .line 153183
    const v7, 0x9175858

    :try_start_7
    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 153184
    :goto_5
    :try_start_8
    throw v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 153185
    :catchall_1
    move-exception v6

    monitor-exit p0

    throw v6

    .line 153186
    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_5
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;Ljava/lang/String;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")[J"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 153187
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\",\""

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153188
    const-string v1, "-1"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153189
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SELECT DISTINCT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0st;->a:LX/0U1;

    .line 153190
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 153191
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM tags"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", queries"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, LX/0st;->b:LX/0U1;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->d:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " =?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 153192
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p2, v0, v2

    .line 153193
    :goto_0
    invoke-virtual {p0, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 153194
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v4, v0, [J

    move v0, v2

    .line 153195
    :goto_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 153196
    add-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    aput-wide v6, v4, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    goto :goto_1

    .line 153197
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SELECT DISTINCT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0st;->a:LX/0U1;

    .line 153198
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 153199
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM tags"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, LX/0st;->b:LX/0U1;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 153200
    const/4 v0, 0x0

    goto :goto_0

    .line 153201
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    return-object v4

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static b(LX/0QB;)LX/0sh;
    .locals 20

    .prologue
    .line 153124
    new-instance v1, LX/0sh;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0sl;->a(LX/0QB;)LX/0sl;

    move-result-object v3

    check-cast v3, LX/0sl;

    invoke-static/range {p0 .. p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v4

    check-cast v4, LX/0pq;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0t2;->a(LX/0QB;)LX/0t2;

    move-result-object v6

    check-cast v6, LX/0t2;

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v7

    check-cast v7, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/0t3;->a(LX/0QB;)LX/0t3;

    move-result-object v8

    check-cast v8, LX/0t3;

    invoke-static/range {p0 .. p0}, LX/0t4;->a(LX/0QB;)LX/0t4;

    move-result-object v9

    check-cast v9, LX/0t4;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v11, LX/0ox;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/0ox;

    invoke-static/range {p0 .. p0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v12

    check-cast v12, LX/0t5;

    invoke-static/range {p0 .. p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v13

    check-cast v13, LX/0sT;

    invoke-static/range {p0 .. p0}, LX/0t6;->a(LX/0QB;)LX/0t8;

    move-result-object v14

    check-cast v14, LX/0t8;

    const/16 v15, 0x235e

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0t9;->a(LX/0QB;)LX/0t9;

    move-result-object v16

    check-cast v16, LX/0t9;

    invoke-static/range {p0 .. p0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v17

    check-cast v17, LX/0t5;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v18

    check-cast v18, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0tC;->a(LX/0QB;)LX/0tC;

    move-result-object v19

    check-cast v19, LX/0tC;

    invoke-direct/range {v1 .. v19}, LX/0sh;-><init>(Landroid/content/Context;LX/0sl;LX/0pq;LX/0SG;LX/0t2;LX/0SI;LX/0t3;LX/0t4;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ox;LX/0t5;LX/0sT;LX/0t8;LX/0Ot;LX/0t9;LX/0t5;LX/0Zb;LX/0tC;)V

    .line 153125
    return-object v1
.end method

.method private b(Landroid/database/Cursor;)LX/1lL;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v8, 0x0

    .line 153089
    sget-object v2, LX/0sx;->b:LX/0U1;

    .line 153090
    iget-object v0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v0

    .line 153091
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 153092
    if-nez v9, :cond_0

    .line 153093
    invoke-static {}, LX/1lL;->a()LX/1lL;

    move-result-object v2

    .line 153094
    :goto_0
    return-object v2

    .line 153095
    :cond_0
    sget-object v2, LX/0sx;->c:LX/0U1;

    .line 153096
    iget-object v0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v0

    .line 153097
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    .line 153098
    sget-object v2, LX/0sx;->d:LX/0U1;

    .line 153099
    iget-object v0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v0

    .line 153100
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 153101
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/0sh;->u:Ljava/io/File;

    invoke-direct {v2, v3, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 153102
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153103
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->length()J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    long-to-int v6, v2

    .line 153104
    if-nez v6, :cond_3

    move-object v3, v8

    .line 153105
    :goto_1
    if-eqz v3, :cond_2

    .line 153106
    :try_start_2
    iget-object v2, p0, LX/0sh;->p:LX/0tC;

    invoke-virtual {v2}, LX/0tC;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153107
    iget-object v2, p0, LX/0sh;->r:LX/0tD;

    invoke-virtual {v2, v9, v3}, LX/0tD;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    .line 153108
    :cond_1
    sget-object v2, LX/0sr;->g:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    sget-object v4, LX/0sr;->f:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    invoke-static {p1, v2, v4}, LX/0sh;->a(Landroid/database/Cursor;II)LX/0w5;

    move-result-object v2

    .line 153109
    invoke-static {v3, v2}, LX/1lJ;->a(Ljava/nio/ByteBuffer;LX/0w5;)V
    :try_end_2
    .catch LX/4VR; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153110
    :cond_2
    :try_start_3
    new-instance v2, LX/1lL;

    invoke-direct {v2, v3, v10, v11}, LX/1lL;-><init>(Ljava/nio/ByteBuffer;[B[B)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 153111
    :try_start_4
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 153112
    :catch_0
    move-exception v2

    .line 153113
    const-class v3, LX/0sh;

    const-string v4, "Error getting base buffer from file"

    new-array v5, v13, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153114
    invoke-static {}, LX/1lL;->a()LX/1lL;

    move-result-object v2

    goto :goto_0

    .line 153115
    :cond_3
    :try_start_5
    invoke-virtual {v12}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v3

    .line 153116
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 153117
    move-object v0, v3

    check-cast v0, Ljava/nio/MappedByteBuffer;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/nio/MappedByteBuffer;->load()Ljava/nio/MappedByteBuffer;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 153118
    :catch_1
    move-exception v2

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 153119
    :catchall_0
    move-exception v3

    move-object v8, v2

    move-object v2, v3

    :goto_2
    if-eqz v8, :cond_4

    :try_start_7
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    :goto_3
    :try_start_8
    throw v2

    :catch_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    move-object v2, v8

    goto/16 :goto_0

    .line 153120
    :catch_3
    move-exception v2

    .line 153121
    :goto_4
    :try_start_9
    sget-object v3, LX/0sh;->b:Ljava/lang/String;

    const-string v4, "Model file is not valid"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 153122
    :try_start_a
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    move-object v2, v8

    goto/16 :goto_0

    :catch_4
    move-exception v3

    invoke-static {v8, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v2

    goto :goto_2

    .line 153123
    :catch_5
    move-exception v2

    goto :goto_4
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 153087
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    invoke-static {p0, v0}, LX/0sh;->a(LX/0sh;[J)V

    .line 153088
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 153082
    const-string v0, "SELECT DISTINCT consistency.id FROM consistency LEFT JOIN tags ON tags.tag = consistency.id WHERE tags.tag IS NULL"

    .line 153083
    const-string v1, "consistency"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0sz;->a:LX/0U1;

    .line 153084
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 153085
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 153086
    return-void
.end method

.method private declared-synchronized b(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 153051
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 153052
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT OR REPLACE INTO consistency ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0sz;->a:LX/0U1;

    .line 153053
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153054
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->c:LX/0U1;

    .line 153055
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153056
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->b:LX/0U1;

    .line 153057
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153058
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->d:LX/0U1;

    .line 153059
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153060
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->e:LX/0U1;

    .line 153061
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153062
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->f:LX/0U1;

    .line 153063
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153064
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sz;->g:LX/0U1;

    .line 153065
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153066
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?, ?, ?, ?, ?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153067
    iget-object v1, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 153068
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v4, v0

    .line 153069
    const v0, -0x41d4d7a0

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153070
    :try_start_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153071
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 153072
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 153073
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-direct {p0, v4, v0, v2, v7}, LX/0sh;->a(Landroid/database/sqlite/SQLiteStatement;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 153074
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153075
    const v1, 0x2797fa8e

    :try_start_3
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 153076
    :goto_1
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 153077
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 153078
    :cond_1
    :try_start_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 153079
    :try_start_6
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 153080
    const v0, 0x69eeaf82

    :try_start_7
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 153081
    :goto_2
    monitor-exit p0

    return-void

    :catch_0
    goto :goto_2

    :catch_1
    goto :goto_1
.end method

.method private c(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153048
    iget-object v0, p0, LX/0sh;->m:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153049
    :goto_0
    return-void

    .line 153050
    :cond_0
    iget-object v0, p0, LX/0sh;->o:LX/0t5;

    const/4 v1, -0x1

    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, LX/0t5;->a(ILjava/util/Set;I)V

    goto :goto_0
.end method

.method private e(LX/0zO;)LX/1lM;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/1lM;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 153047
    invoke-static {}, LX/0sh;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    invoke-static {p0, p1}, LX/0sh;->f(LX/0sh;LX/0zO;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    sget-object v3, LX/0sr;->l:LX/0U1;

    iget-wide v4, p1, LX/0zO;->c:J

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/0sh;->a(LX/0sh;Ljava/lang/String;[Ljava/lang/String;LX/0U1;JZZ)LX/1lM;

    move-result-object v0

    return-object v0
.end method

.method private static e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 153016
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0sr;->a:LX/0U1;

    .line 153017
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153018
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->e:LX/0U1;

    .line 153019
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153020
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->c:LX/0U1;

    .line 153021
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153022
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->f:LX/0U1;

    .line 153023
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153024
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->g:LX/0U1;

    .line 153025
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153026
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->h:LX/0U1;

    .line 153027
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153028
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->i:LX/0U1;

    .line 153029
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153030
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->k:LX/0U1;

    .line 153031
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153032
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->l:LX/0U1;

    .line 153033
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153034
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->b:LX/0U1;

    .line 153035
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153036
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->c:LX/0U1;

    .line 153037
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153038
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->d:LX/0U1;

    .line 153039
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153040
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM queries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN models"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON queries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->l:LX/0U1;

    .line 153041
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153042
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "   = models"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sx;->a:LX/0U1;

    .line 153043
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153044
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->b:LX/0U1;

    .line 153045
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 153046
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/0sr;->d:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 152605
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private static f(LX/0sh;LX/0zO;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 152606
    iget-object v0, p0, LX/0sh;->s:LX/0t2;

    invoke-virtual {p1, v0}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized g(LX/0sh;)LX/15j;
    .locals 2

    .prologue
    .line 152601
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->t:LX/15j;

    if-nez v0, :cond_0

    .line 152602
    iget-object v0, p0, LX/0sh;->i:LX/0ox;

    sget-object v1, LX/1l9;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v0

    iput-object v0, p0, LX/0sh;->t:LX/15j;

    .line 152603
    :cond_0
    iget-object v0, p0, LX/0sh;->t:LX/15j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 152604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152587
    monitor-enter p0

    .line 152588
    :try_start_0
    iget-boolean v2, p0, LX/0sh;->v:Z

    if-nez v2, :cond_1

    .line 152589
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/0sh;->v:Z

    .line 152590
    invoke-static {p0}, LX/0sh;->g(LX/0sh;)LX/15j;

    move-result-object v2

    .line 152591
    invoke-virtual {v2}, LX/15j;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 152592
    invoke-virtual {v2}, LX/15j;->c()V

    .line 152593
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152594
    if-eqz v0, :cond_0

    .line 152595
    iget-object v0, p0, LX/0sh;->q:LX/0Zb;

    const-string v2, "graphql_response_cache_corrupt"

    invoke-interface {v0, v2, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 152596
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152597
    const-string v1, "cache_name"

    const-string v2, "GraphQLDiskCacheImpl"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 152598
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 152599
    :cond_0
    return-void

    .line 152600
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private declared-synchronized i()J
    .locals 4

    .prologue
    .line 152583
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->e()J

    move-result-wide v0

    .line 152584
    iget-object v2, p0, LX/0sh;->u:Ljava/io/File;

    invoke-static {v2}, LX/39R;->b(Ljava/io/File;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 152585
    add-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    .line 152586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/0sh;)V
    .locals 5

    .prologue
    .line 152580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, LX/0sh;->u:Ljava/io/File;

    sget-object v2, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    const-string v3, "models"

    sget-object v4, LX/0sx;->b:LX/0U1;

    invoke-static {v0, v1, v2, v3, v4}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;Ljava/util/Collection;Ljava/lang/String;LX/0U1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152581
    monitor-exit p0

    return-void

    .line 152582
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 8

    .prologue
    .line 152570
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 152571
    const v0, -0x7393f325

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 152572
    :try_start_1
    invoke-static {p0}, LX/0sh;->q(LX/0sh;)I

    move-result v0

    .line 152573
    const/4 v2, 0x1

    int-to-double v4, v0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-int v0, v4

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 152574
    invoke-static {p0, v0}, LX/0sh;->a(LX/0sh;I)V

    .line 152575
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152576
    const v0, 0x1bd7bb0d

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152577
    monitor-exit p0

    return-void

    .line 152578
    :catchall_0
    move-exception v0

    const v2, 0x16455e49

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 152579
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static l(LX/0sh;)V
    .locals 7

    .prologue
    .line 152553
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 152554
    const/4 v1, 0x0

    .line 152555
    :try_start_0
    const-string v2, "models"

    sget-object v3, LX/0sx;->b:LX/0U1;

    invoke-static {v0, v2, v3}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0U1;)Landroid/database/Cursor;

    move-result-object v1

    .line 152556
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152557
    sget-object v0, LX/0sx;->b:LX/0U1;

    .line 152558
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 152559
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 152560
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 152561
    :try_start_1
    iget-object v0, p0, LX/0sh;->r:LX/0tD;

    invoke-virtual {v0, v3}, LX/0tD;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152562
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 152563
    :cond_1
    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    .line 152564
    return-void

    .line 152565
    :catch_0
    move-exception v0

    .line 152566
    :try_start_3
    sget-object v4, LX/0sh;->b:Ljava/lang/String;

    const-string v5, "Detected corrupted file"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v0, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152567
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, LX/0sh;->u:Ljava/io/File;

    invoke-direct {v0, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 152568
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 152569
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 4

    .prologue
    .line 152541
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 152542
    const v0, -0x6a6f645a

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 152543
    :try_start_1
    const-string v0, "tags"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152544
    const-string v0, "queries"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152545
    const-string v0, "models"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152546
    const-string v0, "consistency"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152547
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152548
    const v0, -0x3c26d045

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 152549
    iget-object v0, p0, LX/0sh;->u:Ljava/io/File;

    invoke-static {v0}, LX/39R;->c(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152550
    monitor-exit p0

    return-void

    .line 152551
    :catchall_0
    move-exception v0

    const v2, -0x12125063

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 152552
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 152537
    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "VACUUM"

    const v2, 0x34c391f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x142fa422

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152538
    :goto_0
    return-void

    .line 152539
    :catch_0
    move-exception v0

    .line 152540
    sget-object v1, LX/0sh;->b:Ljava/lang/String;

    const-string v2, "SQLite disk too full to vacuum"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static p(LX/0sh;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 152534
    iget-object v0, p0, LX/0sh;->f:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 152535
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 152536
    return-object v0
.end method

.method private static q(LX/0sh;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 152528
    :try_start_0
    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 152529
    const-string v2, "SELECT COUNT(*) FROM queries"

    move-object v2, v2

    .line 152530
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 152531
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 152532
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 152533
    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0sh;->a(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 4

    .prologue
    .line 152508
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310001    # 4.499941E-39f

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152509
    const v0, 0x310001    # 4.499941E-39f

    :try_start_1
    const-string v1, "initial"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152510
    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152511
    invoke-static {p0}, LX/0sh;->j(LX/0sh;)V

    .line 152512
    invoke-direct {p0}, LX/0sh;->i()J

    move-result-wide v0

    .line 152513
    iget-wide v2, p0, LX/0sh;->a:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 152514
    :goto_0
    monitor-exit p0

    return-void

    .line 152515
    :cond_0
    :try_start_2
    invoke-direct {p0}, LX/0sh;->k()V

    .line 152516
    iget-object v0, p0, LX/0sh;->p:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152517
    invoke-static {p0}, LX/0sh;->l(LX/0sh;)V

    .line 152518
    :cond_1
    invoke-direct {p0}, LX/0sh;->n()V

    .line 152519
    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152520
    invoke-static {p0}, LX/0sh;->j(LX/0sh;)V

    .line 152521
    const v0, 0x310001    # 4.499941E-39f

    const-string v1, "final"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152522
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310001    # 4.499941E-39f

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 152523
    :catch_0
    move-exception v0

    .line 152524
    :try_start_3
    iget-object v1, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310001    # 4.499941E-39f

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 152525
    sget-object v1, LX/0sh;->b:Ljava/lang/String;

    const-string v2, "Failed to trim to minimum, truncating"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 152526
    invoke-virtual {p0}, LX/0sh;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 152527
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 152458
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152459
    :cond_0
    new-instance v1, LX/026;

    invoke-direct {v1}, LX/026;-><init>()V

    .line 152460
    :goto_0
    return-object v1

    .line 152461
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->hashCode()I

    move-result v4

    .line 152462
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x31002e    # 4.500004E-39f

    invoke-interface {v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 152463
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 152464
    new-instance v2, LX/026;

    invoke-direct {v2}, LX/026;-><init>()V

    .line 152465
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v1, "SELECT id, field_path, value, type, class_name, is_list FROM consistency WHERE id IN ("

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 152466
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v1

    .line 152467
    add-int/lit8 v1, v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    .line 152468
    const/4 v1, 0x0

    .line 152469
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v1

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 152470
    if-eqz v3, :cond_2

    .line 152471
    const-string v9, ","

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152472
    :cond_2
    const-string v9, "?"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152473
    aput-object v1, v7, v3

    .line 152474
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    .line 152475
    goto :goto_1

    .line 152476
    :cond_3
    const-string v1, ") AND user_id = ?"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152477
    invoke-static/range {p0 .. p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v1

    .line 152478
    array-length v3, v7

    add-int/lit8 v3, v3, -0x1

    aput-object v1, v7, v3

    .line 152479
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 152480
    :try_start_1
    sget-object v1, LX/0sz;->a:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 152481
    sget-object v1, LX/0sz;->c:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 152482
    sget-object v1, LX/0sz;->d:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 152483
    sget-object v1, LX/0sz;->e:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 152484
    sget-object v1, LX/0sz;->f:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 152485
    sget-object v1, LX/0sz;->g:LX/0U1;

    invoke-virtual {v1, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 152486
    :goto_2
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 152487
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 152488
    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 152489
    invoke-interface {v5, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 152490
    invoke-interface {v5, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 152491
    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 152492
    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v1, v0, :cond_6

    const/4 v1, 0x1

    .line 152493
    :goto_3
    if-eqz v1, :cond_7

    .line 152494
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0sh;->e:LX/0t3;

    invoke-virtual {v1, v14, v3, v15}, LX/0t3;->b(ILjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    move-object v3, v1

    .line 152495
    :goto_4
    invoke-interface {v2, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 152496
    if-nez v1, :cond_4

    .line 152497
    new-instance v1, LX/026;

    invoke-direct {v1}, LX/026;-><init>()V

    .line 152498
    invoke-interface {v2, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152499
    :cond_4
    invoke-interface {v1, v13, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 152500
    :catchall_0
    move-exception v1

    if-eqz v5, :cond_5

    .line 152501
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152502
    :catchall_1
    move-exception v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x31002e    # 4.500004E-39f

    const/4 v5, 0x2

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1

    .line 152503
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 152504
    :cond_7
    :try_start_3
    invoke-static {v14, v3, v15}, LX/0t3;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    move-object v3, v1

    goto :goto_4

    .line 152505
    :cond_8
    if-eqz v5, :cond_9

    .line 152506
    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 152507
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x31002e    # 4.500004E-39f

    const/4 v5, 0x2

    invoke-interface {v1, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(LX/0zO;)V
    .locals 3

    .prologue
    .line 152450
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 152451
    const v0, -0x617ed966

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 152452
    :try_start_1
    invoke-static {p0, p1}, LX/0sh;->f(LX/0sh;LX/0zO;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    .line 152453
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152454
    const v0, -0x1797e419

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152455
    monitor-exit p0

    return-void

    .line 152456
    :catchall_0
    move-exception v0

    const v2, -0x5fe7881a

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 152457
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 152607
    invoke-static {p1, p2}, LX/1lN;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)I

    move-result v0

    .line 152608
    iget-object v1, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 152609
    invoke-static {v0, v1}, LX/1lN;->a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 152610
    invoke-virtual {p0, p1, p2, v0}, LX/0sh;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/nio/ByteBuffer;)V

    .line 152611
    return-void
.end method

.method public final declared-synchronized a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/nio/ByteBuffer;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "Ljava/nio/ByteBuffer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152612
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, LX/1lN;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)I

    move-result v6

    .line 152613
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 152614
    iget-object v0, p0, LX/0sh;->s:LX/0t2;

    invoke-virtual {p1, v0, p2}, LX/0zO;->a(LX/0t2;Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 152615
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 152616
    iget-object v3, v0, LX/0gW;->h:Ljava/lang/String;

    move-object v0, v3

    .line 152617
    const-string v3, "Null persisted ID"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152618
    invoke-virtual {p2}, Lcom/facebook/graphql/executor/GraphQLResult;->a()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    move-object v4, v3

    .line 152619
    :goto_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 152620
    sget-object v3, LX/0sr;->b:LX/0U1;

    .line 152621
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 152622
    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152623
    sget-object v3, LX/0sr;->c:LX/0U1;

    .line 152624
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 152625
    invoke-virtual {v5, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152626
    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v3

    .line 152627
    sget-object v0, LX/0sr;->d:LX/0U1;

    .line 152628
    iget-object v7, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v7

    .line 152629
    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152630
    iget-object v0, p0, LX/0sh;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v10

    .line 152631
    sget-object v0, LX/0sr;->e:LX/0U1;

    .line 152632
    iget-object v7, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v7

    .line 152633
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 152634
    sget-object v0, LX/0sr;->f:LX/0U1;

    .line 152635
    iget-object v7, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v7

    .line 152636
    iget-object v7, p1, LX/0zO;->n:LX/0w5;

    move-object v7, v7

    .line 152637
    invoke-virtual {v7}, LX/0w5;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152638
    sget-object v0, LX/0sr;->h:LX/0U1;

    .line 152639
    iget-object v7, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v7

    .line 152640
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152641
    sget-object v0, LX/0sr;->i:LX/0U1;

    .line 152642
    iget-object v6, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v6

    .line 152643
    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 152644
    sget-object v0, LX/0sr;->j:LX/0U1;

    .line 152645
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 152646
    iget-wide v6, p1, LX/0zO;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 152647
    sget-object v0, LX/0sr;->g:LX/0U1;

    .line 152648
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 152649
    iget-object v4, p1, LX/0zO;->n:LX/0w5;

    move-object v4, v4

    .line 152650
    invoke-virtual {v4}, LX/0w5;->e()[B

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 152651
    iget-object v0, p0, LX/0sh;->m:LX/0sT;

    const/4 v4, 0x0

    .line 152652
    invoke-virtual {v0}, LX/0sT;->f()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, v0, LX/0sT;->a:LX/0ad;

    sget-short v7, LX/1NG;->k:S

    invoke-interface {v6, v7, v4}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v4, 0x1

    :cond_0
    move v0, v4

    .line 152653
    if-nez v0, :cond_1

    .line 152654
    iget-boolean v0, p1, LX/0zO;->p:Z

    move v0, v0

    .line 152655
    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    .line 152656
    :goto_1
    sget-object v4, LX/0sr;->m:LX/0U1;

    .line 152657
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 152658
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152659
    invoke-virtual {p2}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v4

    .line 152660
    new-instance v6, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v6, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 152661
    if-nez p3, :cond_7

    .line 152662
    const/4 v8, 0x0

    .line 152663
    :goto_3
    iget-object v0, p1, LX/0zO;->n:LX/0w5;

    move-object v9, v0

    .line 152664
    move-object v0, p0

    move-object v7, p3

    invoke-direct/range {v0 .. v11}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/nio/ByteBuffer;Ljava/io/File;LX/0w5;J)Z

    move-result v0

    .line 152665
    if-nez v0, :cond_2

    .line 152666
    invoke-virtual {p0}, LX/0sh;->U_()V

    .line 152667
    iget-object v0, p1, LX/0zO;->n:LX/0w5;

    move-object v9, v0

    .line 152668
    move-object v0, p0

    move-object v7, p3

    invoke-direct/range {v0 .. v11}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/nio/ByteBuffer;Ljava/io/File;LX/0w5;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152669
    :cond_2
    monitor-exit p0

    return-void

    .line 152670
    :cond_3
    :try_start_1
    invoke-virtual {p2}, Lcom/facebook/graphql/executor/GraphQLResult;->a()Ljava/util/Map;

    move-result-object v3

    const/4 v8, 0x0

    .line 152671
    new-instance v4, LX/186;

    const/16 v5, 0x800

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 152672
    sget-object v5, LX/4VD;->a:LX/4VD;

    .line 152673
    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_9

    if-eqz v8, :cond_9

    :cond_4
    const/4 v7, 0x0

    .line 152674
    :goto_4
    move v5, v7

    .line 152675
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 152676
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 152677
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 152678
    if-gez v5, :cond_8

    .line 152679
    const/4 v4, 0x0

    .line 152680
    :goto_5
    move-object v3, v4

    .line 152681
    move-object v4, v3

    goto/16 :goto_0

    .line 152682
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 152683
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 152684
    :cond_7
    iget-object v0, p0, LX/0sh;->u:Ljava/io/File;

    invoke-static {v0}, LX/39R;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v8

    .line 152685
    sget-object v0, LX/0sx;->b:LX/0U1;

    .line 152686
    iget-object v7, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v7

    .line 152687
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 152688
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 152689
    :cond_8
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 152690
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    goto :goto_5

    .line 152691
    :cond_9
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    .line 152692
    new-instance v9, LX/4C1;

    invoke-direct {v9, v7}, LX/4C1;-><init>(Ljava/util/Set;)V

    invoke-virtual {v4, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 152693
    new-instance v10, LX/4C3;

    invoke-direct {v10, v7}, LX/4C3;-><init>(Ljava/util/Set;)V

    .line 152694
    const/4 v7, 0x1

    invoke-virtual {v4, v10, v5, v7}, LX/186;->a(Ljava/util/List;LX/4Bv;Z)I

    move-result v7

    move v7, v7

    .line 152695
    invoke-static {v4, v9, v7}, LX/186;->c(LX/186;II)I

    move-result v7

    goto :goto_4
.end method

.method public final a(LX/3Bq;)V
    .locals 13

    .prologue
    .line 152696
    invoke-interface {p1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v12

    .line 152697
    invoke-interface {v12}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152698
    :cond_0
    :goto_0
    return-void

    .line 152699
    :cond_1
    new-instance v0, LX/2vK;

    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v2, p0, LX/0sh;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    iget-object v3, p0, LX/0sh;->u:Ljava/io/File;

    iget-object v4, p0, LX/0sh;->p:LX/0tC;

    iget-object v5, p0, LX/0sh;->d:LX/0SG;

    iget-object v6, p0, LX/0sh;->m:LX/0sT;

    iget-object v7, p0, LX/0sh;->j:LX/0t5;

    iget-object v8, p0, LX/0sh;->k:LX/0t8;

    iget-object v9, p0, LX/0sh;->l:LX/0Ot;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LX/2vK;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/io/File;LX/0tC;LX/0SG;LX/0sT;LX/0t5;LX/0t8;LX/0Ot;ILjava/lang/String;)V

    .line 152700
    monitor-enter p0

    .line 152701
    :try_start_0
    iget-object v1, p0, LX/0sh;->n:LX/0t9;

    invoke-virtual {v1, v0, v12, p1}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;)V

    .line 152702
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152703
    iget-boolean v1, v0, LX/2vK;->m:Z

    move v0, v1

    .line 152704
    if-eqz v0, :cond_0

    .line 152705
    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152706
    invoke-static {p0}, LX/0sh;->j(LX/0sh;)V

    goto :goto_0

    .line 152707
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 12
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152708
    iget-object v0, p0, LX/0sh;->m:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152709
    :cond_0
    :goto_0
    return-void

    .line 152710
    :cond_1
    new-instance v0, LX/2vK;

    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v2, p0, LX/0sh;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    iget-object v3, p0, LX/0sh;->u:Ljava/io/File;

    iget-object v4, p0, LX/0sh;->p:LX/0tC;

    iget-object v5, p0, LX/0sh;->d:LX/0SG;

    iget-object v6, p0, LX/0sh;->m:LX/0sT;

    iget-object v7, p0, LX/0sh;->j:LX/0t5;

    iget-object v8, p0, LX/0sh;->k:LX/0t8;

    iget-object v9, p0, LX/0sh;->l:LX/0Ot;

    const/4 v10, 0x1

    move-object/from16 v11, p4

    invoke-direct/range {v0 .. v11}, LX/2vK;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/io/File;LX/0tC;LX/0SG;LX/0sT;LX/0t5;LX/0t8;LX/0Ot;ILjava/lang/String;)V

    .line 152711
    monitor-enter p0

    .line 152712
    :try_start_0
    iget-object v1, p0, LX/0sh;->n:LX/0t9;

    invoke-virtual {v1, v0, p1, p2, p3}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;)V

    .line 152713
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152714
    invoke-virtual {v0}, LX/2vK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152715
    invoke-direct {p0, p1}, LX/0sh;->c(Ljava/util/Collection;)V

    goto :goto_0

    .line 152716
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152717
    iget-object v0, p0, LX/0sh;->m:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152718
    :cond_0
    :goto_0
    return-void

    .line 152719
    :cond_1
    new-instance v0, LX/2vK;

    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v2, p0, LX/0sh;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    iget-object v3, p0, LX/0sh;->u:Ljava/io/File;

    iget-object v4, p0, LX/0sh;->p:LX/0tC;

    iget-object v5, p0, LX/0sh;->d:LX/0SG;

    iget-object v6, p0, LX/0sh;->m:LX/0sT;

    iget-object v7, p0, LX/0sh;->j:LX/0t5;

    iget-object v8, p0, LX/0sh;->k:LX/0t8;

    iget-object v9, p0, LX/0sh;->l:LX/0Ot;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LX/2vK;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/io/File;LX/0tC;LX/0SG;LX/0sT;LX/0t5;LX/0t8;LX/0Ot;ILjava/lang/String;)V

    .line 152720
    monitor-enter p0

    .line 152721
    :try_start_0
    iget-object v1, p0, LX/0sh;->n:LX/0t9;

    invoke-virtual {v1, v0, p1, p2}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 152722
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152723
    iget-boolean v1, v0, LX/2vK;->m:Z

    move v0, v1

    .line 152724
    if-eqz v0, :cond_0

    .line 152725
    invoke-direct {p0, p1}, LX/0sh;->c(Ljava/util/Collection;)V

    goto :goto_0

    .line 152726
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 152727
    :try_start_0
    invoke-direct {p0, p1}, LX/0sh;->b(Ljava/util/Map;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152728
    :goto_0
    return-void

    .line 152729
    :catch_0
    invoke-virtual {p0}, LX/0sh;->U_()V

    .line 152730
    invoke-direct {p0, p1}, LX/0sh;->b(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152731
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 152732
    const v0, 0x52a35917

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 152733
    :try_start_1
    const-string v0, "-1"

    invoke-static {v1, p1, v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v1, v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;[J)J

    .line 152734
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152735
    const v0, 0x385c428

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152736
    monitor-exit p0

    return-void

    .line 152737
    :catchall_0
    move-exception v0

    const v2, -0x31c399b7

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 152738
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 152739
    invoke-direct {p0, p1}, LX/0sh;->e(LX/0zO;)LX/1lM;

    move-result-object v0

    .line 152740
    if-nez v0, :cond_0

    .line 152741
    :goto_0
    return-object v7

    .line 152742
    :cond_0
    invoke-direct {p0, v0}, LX/0sh;->a(LX/1lM;)Ljava/lang/Object;

    move-result-object v2

    .line 152743
    iget-object v1, v0, LX/1lM;->h:[B

    if-eqz v1, :cond_1

    .line 152744
    iget-object v1, v0, LX/1lM;->h:[B

    invoke-static {v1}, LX/1lN;->a([B)Ljava/util/Map;

    move-result-object v9

    .line 152745
    :goto_1
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v3, v0, LX/1lM;->k:LX/0ta;

    iget-wide v4, v0, LX/1lM;->c:J

    iget-object v6, v0, LX/1lM;->e:Ljava/util/Set;

    invoke-virtual {p1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v10

    move-object v8, v7

    move-object v11, v7

    invoke-direct/range {v1 .. v11}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v1

    goto :goto_0

    :cond_1
    move-object v9, v7

    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 152746
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310002    # 4.499942E-39f

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152747
    const v0, 0x310002    # 4.499942E-39f

    :try_start_1
    const-string v1, "initial"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152748
    invoke-direct {p0}, LX/0sh;->m()V

    .line 152749
    invoke-direct {p0}, LX/0sh;->n()V

    .line 152750
    const v0, 0x310002    # 4.499942E-39f

    const-string v1, "final"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152751
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310002    # 4.499942E-39f

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152752
    :goto_0
    monitor-exit p0

    return-void

    .line 152753
    :catch_0
    move-exception v0

    .line 152754
    :try_start_2
    iget-object v1, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310002    # 4.499942E-39f

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 152755
    sget-object v1, LX/0sh;->b:Ljava/lang/String;

    const-string v2, "Failed to trim to nothing"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 152756
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152757
    invoke-static {}, LX/0sh;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, p1}, LX/0sh;->f(LX/0sh;LX/0zO;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    sget-object v3, LX/0sr;->l:LX/0U1;

    iget-wide v4, p1, LX/0zO;->c:J

    const/4 v6, 0x1

    .line 152758
    iget-boolean v0, p1, LX/0zO;->p:Z

    move v7, v0

    .line 152759
    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/0sh;->a(LX/0sh;Ljava/lang/String;[Ljava/lang/String;LX/0U1;JZZ)LX/1lM;

    move-result-object v9

    .line 152760
    if-nez v9, :cond_0

    .line 152761
    const/4 v0, 0x0

    .line 152762
    :goto_0
    return-object v0

    .line 152763
    :cond_0
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 152764
    invoke-virtual {v0}, LX/0gW;->s()I

    move-result v0

    .line 152765
    iget-object v1, v9, LX/1lM;->i:[B

    if-eqz v1, :cond_2

    iget-object v1, v9, LX/1lM;->i:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 152766
    :goto_1
    iget-object v1, v9, LX/1lM;->j:[B

    if-eqz v1, :cond_3

    iget-object v1, v9, LX/1lM;->j:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 152767
    :goto_2
    const/4 v1, 0x0

    .line 152768
    iget-object v2, v9, LX/1lM;->d:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_4

    .line 152769
    iget v1, v9, LX/1lM;->g:I

    iget-object v2, v9, LX/1lM;->d:Ljava/nio/ByteBuffer;

    iget-object v3, v9, LX/1lM;->f:LX/0w5;

    iget-object v4, p0, LX/0sh;->k:LX/0t8;

    iget-object v5, v9, LX/1lM;->m:Ljava/util/Map;

    invoke-static {p0}, LX/0sh;->g(LX/0sh;)LX/15j;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1lN;->a(IILjava/nio/ByteBuffer;LX/0w5;LX/0t8;Ljava/util/Map;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;

    move-result-object v0

    .line 152770
    :goto_3
    const/4 v1, 0x0

    .line 152771
    iget-object v2, v9, LX/1lM;->h:[B

    if-eqz v2, :cond_1

    .line 152772
    iget-object v1, v9, LX/1lM;->h:[B

    invoke-static {v1}, LX/1lN;->a([B)Ljava/util/Map;

    move-result-object v1

    .line 152773
    :cond_1
    new-instance v2, LX/1lO;

    invoke-direct {v2}, LX/1lO;-><init>()V

    .line 152774
    iput-object v1, v2, LX/1lO;->f:Ljava/util/Map;

    .line 152775
    iput-object v0, v2, LX/1lO;->k:Ljava/lang/Object;

    .line 152776
    move-object v0, v2

    .line 152777
    iget-object v1, v9, LX/1lM;->k:LX/0ta;

    .line 152778
    iput-object v1, v0, LX/1lO;->b:LX/0ta;

    .line 152779
    move-object v0, v0

    .line 152780
    iget-wide v4, v9, LX/1lM;->c:J

    .line 152781
    iput-wide v4, v0, LX/1lO;->c:J

    .line 152782
    move-object v0, v0

    .line 152783
    iget-object v1, v9, LX/1lM;->e:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/1lO;->b(Ljava/util/Collection;)LX/1lO;

    move-result-object v0

    const/4 v1, 0x1

    .line 152784
    iput-boolean v1, v0, LX/1lO;->o:Z

    .line 152785
    invoke-virtual {v2}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    goto :goto_0

    .line 152786
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 152787
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 152788
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 152789
    const-string v1, "tags"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152790
    const-string v1, "queries"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152791
    const-string v1, "models"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 152792
    const-string v1, "consistency"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152793
    monitor-exit p0

    return-void

    .line 152794
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 152795
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 152796
    iget-object v0, p0, LX/0sh;->u:Ljava/io/File;

    invoke-static {v0}, LX/39R;->c(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152797
    monitor-exit p0

    return-void

    .line 152798
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 152799
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31002b    # 4.5E-39f

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152800
    const v0, 0x31002b    # 4.5E-39f

    :try_start_1
    const-string v1, "initial"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152801
    iget-object v0, p0, LX/0sh;->p:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152802
    invoke-static {p0}, LX/0sh;->l(LX/0sh;)V

    .line 152803
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0sh;->a(LX/0sh;I)V

    .line 152804
    invoke-static {p0}, LX/0sh;->f(LX/0sh;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0}, LX/0sh;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152805
    invoke-static {p0}, LX/0sh;->j(LX/0sh;)V

    .line 152806
    const v0, 0x31002b    # 4.5E-39f

    const-string v1, "final"

    invoke-static {p0, v0, v1}, LX/0sh;->a(LX/0sh;ILjava/lang/String;)V

    .line 152807
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31002b    # 4.5E-39f

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152808
    :goto_0
    monitor-exit p0

    return-void

    .line 152809
    :catch_0
    :try_start_2
    iget-object v0, p0, LX/0sh;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31002b    # 4.5E-39f

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 152810
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(LX/0zO;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152811
    iget-object v2, p0, LX/0sh;->c:LX/0sl;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 152812
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/0sr;->e:LX/0U1;

    .line 152813
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 152814
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FROM queries"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->b:LX/0U1;

    .line 152815
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 152816
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " =? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/0sr;->d:LX/0U1;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " =?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 152817
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p0, p1}, LX/0sh;->f(LX/0sh;LX/0zO;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p0}, LX/0sh;->p(LX/0sh;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 152818
    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 152819
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152820
    sget-object v3, LX/0sr;->e:LX/0U1;

    invoke-virtual {v3, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 152821
    iget-object v3, p0, LX/0sh;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 152822
    iget-wide v6, p1, LX/0zO;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 152823
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 152824
    :goto_0
    return v0

    .line 152825
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v0, v1

    .line 152826
    goto :goto_0

    .line 152827
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method
