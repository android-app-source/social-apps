.class public LX/0c3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/036;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Lg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87595
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 87596
    iput-object v0, p0, LX/0c3;->a:LX/0Ot;

    .line 87597
    return-void
.end method


# virtual methods
.method public final a()LX/02u;
    .locals 1

    .prologue
    .line 87610
    iget-object v0, p0, LX/0c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lg;

    .line 87611
    iget-object p0, v0, LX/2Lg;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/02u;

    move-object v0, p0

    .line 87612
    return-object v0
.end method

.method public final b()LX/0Pb;
    .locals 3

    .prologue
    .line 87607
    iget-object v0, p0, LX/0c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lg;

    .line 87608
    new-instance v2, LX/2Lh;

    iget-object v1, v0, LX/2Lg;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pb;

    new-instance p0, LX/0Pf;

    invoke-direct {p0}, LX/0Pf;-><init>()V

    invoke-direct {v2, v1, p0}, LX/2Lh;-><init>(LX/0Pb;LX/0Pb;)V

    move-object v0, v2

    .line 87609
    return-object v0
.end method

.method public final c()LX/02t;
    .locals 1

    .prologue
    .line 87604
    iget-object v0, p0, LX/0c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lg;

    .line 87605
    iget-object p0, v0, LX/2Lg;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/02t;

    move-object v0, p0

    .line 87606
    return-object v0
.end method

.method public final d()LX/03A;
    .locals 3

    .prologue
    .line 87601
    iget-object v0, p0, LX/0c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lg;

    .line 87602
    new-instance v2, LX/03A;

    iget-object v1, v0, LX/2Lg;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    iget-object v1, v0, LX/2Lg;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, p0, v1}, LX/03A;-><init>(Ljava/lang/Object;Ljava/util/concurrent/ExecutorService;)V

    move-object v0, v2

    .line 87603
    return-object v0
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 87598
    invoke-static {}, LX/02o;->b()LX/02o;

    move-result-object v0

    .line 87599
    invoke-virtual {v0, p0}, LX/02o;->a(LX/036;)V

    .line 87600
    return-void
.end method
