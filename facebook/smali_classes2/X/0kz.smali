.class public LX/0kz;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/content/res/Resources;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128048
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 128050
    sget-object v0, LX/0kz;->a:Landroid/content/res/Resources;

    if-nez v0, :cond_1

    .line 128051
    const-class v1, LX/0kz;

    monitor-enter v1

    .line 128052
    :try_start_0
    sget-object v0, LX/0kz;->a:Landroid/content/res/Resources;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 128053
    if-eqz v2, :cond_0

    .line 128054
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 128055
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, LX/0VV;->y(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object p0

    move-object v0, p0

    .line 128056
    sput-object v0, LX/0kz;->a:Landroid/content/res/Resources;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 128058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 128059
    :cond_1
    sget-object v0, LX/0kz;->a:Landroid/content/res/Resources;

    return-object v0

    .line 128060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 128061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128049
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/0VV;->y(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method
