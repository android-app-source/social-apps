.class public final LX/15x;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[C


# instance fields
.field private final b:LX/12B;

.field private c:[C

.field private d:I

.field private e:I

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[C>;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:I

.field private i:[C

.field public j:I

.field private k:Ljava/lang/String;

.field private l:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 183032
    const/4 v0, 0x0

    new-array v0, v0, [C

    sput-object v0, LX/15x;->a:[C

    return-void
.end method

.method public constructor <init>(LX/12B;)V
    .locals 1

    .prologue
    .line 183033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183034
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15x;->g:Z

    .line 183035
    iput-object p1, p0, LX/15x;->b:LX/12B;

    .line 183036
    return-void
.end method

.method private b(I)[C
    .locals 2

    .prologue
    .line 183037
    iget-object v0, p0, LX/15x;->b:LX/12B;

    if-eqz v0, :cond_0

    .line 183038
    iget-object v0, p0, LX/15x;->b:LX/12B;

    sget-object v1, LX/12D;->TEXT_BUFFER:LX/12D;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12D;I)[C

    move-result-object v0

    .line 183039
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x3e8

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [C

    goto :goto_0
.end method

.method private c(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 183040
    iget v0, p0, LX/15x;->e:I

    .line 183041
    iput v5, p0, LX/15x;->e:I

    .line 183042
    iget-object v1, p0, LX/15x;->c:[C

    .line 183043
    const/4 v2, 0x0

    iput-object v2, p0, LX/15x;->c:[C

    .line 183044
    iget v2, p0, LX/15x;->d:I

    .line 183045
    const/4 v3, -0x1

    iput v3, p0, LX/15x;->d:I

    .line 183046
    add-int v3, v0, p1

    .line 183047
    iget-object v4, p0, LX/15x;->i:[C

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/15x;->i:[C

    array-length v4, v4

    if-le v3, v4, :cond_1

    .line 183048
    :cond_0
    invoke-direct {p0, v3}, LX/15x;->b(I)[C

    move-result-object v3

    iput-object v3, p0, LX/15x;->i:[C

    .line 183049
    :cond_1
    if-lez v0, :cond_2

    .line 183050
    iget-object v3, p0, LX/15x;->i:[C

    invoke-static {v1, v2, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183051
    :cond_2
    iput v5, p0, LX/15x;->h:I

    .line 183052
    iput v0, p0, LX/15x;->j:I

    .line 183053
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 183054
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 183055
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    .line 183056
    :cond_0
    iget-object v0, p0, LX/15x;->i:[C

    .line 183057
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/15x;->g:Z

    .line 183058
    iget-object v1, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183059
    iget v1, p0, LX/15x;->h:I

    array-length v2, v0

    add-int/2addr v1, v2

    iput v1, p0, LX/15x;->h:I

    .line 183060
    array-length v1, v0

    .line 183061
    shr-int/lit8 v0, v1, 0x1

    .line 183062
    if-ge v0, p1, :cond_1

    .line 183063
    :goto_0
    const/high16 v0, 0x40000

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 183064
    new-array v1, v0, [C

    move-object v0, v1

    .line 183065
    const/4 v1, 0x0

    iput v1, p0, LX/15x;->j:I

    .line 183066
    iput-object v0, p0, LX/15x;->i:[C

    .line 183067
    return-void

    :cond_1
    move p1, v0

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183068
    iput-boolean v1, p0, LX/15x;->g:Z

    .line 183069
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 183070
    iput v1, p0, LX/15x;->h:I

    iput v1, p0, LX/15x;->j:I

    .line 183071
    return-void
.end method

.method private q()[C
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 183072
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 183073
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 183074
    :goto_0
    return-object v0

    .line 183075
    :cond_0
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_2

    .line 183076
    iget v0, p0, LX/15x;->e:I

    if-gtz v0, :cond_1

    .line 183077
    sget-object v0, LX/15x;->a:[C

    goto :goto_0

    .line 183078
    :cond_1
    iget v0, p0, LX/15x;->e:I

    .line 183079
    new-array v1, v0, [C

    move-object v0, v1

    .line 183080
    iget-object v1, p0, LX/15x;->c:[C

    iget v3, p0, LX/15x;->d:I

    iget v4, p0, LX/15x;->e:I

    invoke-static {v1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 183081
    :cond_2
    invoke-virtual {p0}, LX/15x;->c()I

    move-result v0

    .line 183082
    if-gtz v0, :cond_3

    .line 183083
    sget-object v0, LX/15x;->a:[C

    goto :goto_0

    .line 183084
    :cond_3
    new-array v1, v0, [C

    move-object v3, v1

    .line 183085
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 183086
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    move v1, v2

    :goto_1
    if-ge v4, v5, :cond_4

    .line 183087
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    check-cast v0, [C

    .line 183088
    array-length v6, v0

    .line 183089
    invoke-static {v0, v2, v3, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183090
    add-int/2addr v1, v6

    .line 183091
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 183092
    :goto_2
    iget-object v1, p0, LX/15x;->i:[C

    iget v4, p0, LX/15x;->j:I

    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v3

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 183093
    iget-object v0, p0, LX/15x;->b:LX/12B;

    if-nez v0, :cond_1

    .line 183094
    invoke-virtual {p0}, LX/15x;->b()V

    .line 183095
    :cond_0
    :goto_0
    return-void

    .line 183096
    :cond_1
    iget-object v0, p0, LX/15x;->i:[C

    if-eqz v0, :cond_0

    .line 183097
    invoke-virtual {p0}, LX/15x;->b()V

    .line 183098
    iget-object v0, p0, LX/15x;->i:[C

    .line 183099
    const/4 v1, 0x0

    iput-object v1, p0, LX/15x;->i:[C

    .line 183100
    iget-object v1, p0, LX/15x;->b:LX/12B;

    sget-object v2, LX/12D;->TEXT_BUFFER:LX/12D;

    invoke-virtual {v1, v2, v0}, LX/12B;->a(LX/12D;[C)V

    goto :goto_0
.end method

.method public final a(C)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 183101
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    .line 183102
    const/16 v0, 0x10

    invoke-direct {p0, v0}, LX/15x;->c(I)V

    .line 183103
    :cond_0
    iput-object v1, p0, LX/15x;->k:Ljava/lang/String;

    .line 183104
    iput-object v1, p0, LX/15x;->l:[C

    .line 183105
    iget-object v0, p0, LX/15x;->i:[C

    .line 183106
    iget v1, p0, LX/15x;->j:I

    array-length v2, v0

    if-lt v1, v2, :cond_1

    .line 183107
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/15x;->d(I)V

    .line 183108
    iget-object v0, p0, LX/15x;->i:[C

    .line 183109
    :cond_1
    iget v1, p0, LX/15x;->j:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/15x;->j:I

    aput-char p1, v0, v1

    .line 183110
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 183111
    iput-object v2, p0, LX/15x;->c:[C

    .line 183112
    const/4 v0, -0x1

    iput v0, p0, LX/15x;->d:I

    .line 183113
    iput v1, p0, LX/15x;->e:I

    .line 183114
    iput-object p1, p0, LX/15x;->k:Ljava/lang/String;

    .line 183115
    iput-object v2, p0, LX/15x;->l:[C

    .line 183116
    iget-boolean v0, p0, LX/15x;->g:Z

    if-eqz v0, :cond_0

    .line 183117
    invoke-direct {p0}, LX/15x;->p()V

    .line 183118
    :cond_0
    iput v1, p0, LX/15x;->j:I

    .line 183119
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 183120
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    .line 183121
    invoke-direct {p0, p3}, LX/15x;->c(I)V

    .line 183122
    :cond_0
    iput-object v1, p0, LX/15x;->k:Ljava/lang/String;

    .line 183123
    iput-object v1, p0, LX/15x;->l:[C

    .line 183124
    iget-object v0, p0, LX/15x;->i:[C

    .line 183125
    array-length v1, v0

    iget v2, p0, LX/15x;->j:I

    sub-int/2addr v1, v2

    .line 183126
    if-lt v1, p3, :cond_1

    .line 183127
    add-int v1, p2, p3

    iget v2, p0, LX/15x;->j:I

    invoke-virtual {p1, p2, v1, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 183128
    iget v0, p0, LX/15x;->j:I

    add-int/2addr v0, p3

    iput v0, p0, LX/15x;->j:I

    .line 183129
    :goto_0
    return-void

    .line 183130
    :cond_1
    if-lez v1, :cond_2

    .line 183131
    add-int v2, p2, v1

    iget v3, p0, LX/15x;->j:I

    invoke-virtual {p1, p2, v2, v0, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 183132
    sub-int/2addr p3, v1

    .line 183133
    add-int/2addr p2, v1

    .line 183134
    :cond_2
    invoke-direct {p0, p3}, LX/15x;->d(I)V

    .line 183135
    iget-object v0, p0, LX/15x;->i:[C

    array-length v0, v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 183136
    add-int v1, p2, v0

    iget-object v2, p0, LX/15x;->i:[C

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 183137
    iget v1, p0, LX/15x;->j:I

    add-int/2addr v1, v0

    iput v1, p0, LX/15x;->j:I

    .line 183138
    add-int/2addr p2, v0

    .line 183139
    sub-int/2addr p3, v0

    .line 183140
    if-gtz p3, :cond_2

    goto :goto_0
.end method

.method public final a([CII)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 183141
    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    .line 183142
    iput-object v0, p0, LX/15x;->l:[C

    .line 183143
    iput-object p1, p0, LX/15x;->c:[C

    .line 183144
    iput p2, p0, LX/15x;->d:I

    .line 183145
    iput p3, p0, LX/15x;->e:I

    .line 183146
    iget-boolean v0, p0, LX/15x;->g:Z

    if-eqz v0, :cond_0

    .line 183147
    invoke-direct {p0}, LX/15x;->p()V

    .line 183148
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 183149
    const/4 v0, -0x1

    iput v0, p0, LX/15x;->d:I

    .line 183150
    iput v2, p0, LX/15x;->j:I

    .line 183151
    iput v2, p0, LX/15x;->e:I

    .line 183152
    iput-object v1, p0, LX/15x;->c:[C

    .line 183153
    iput-object v1, p0, LX/15x;->k:Ljava/lang/String;

    .line 183154
    iput-object v1, p0, LX/15x;->l:[C

    .line 183155
    iget-boolean v0, p0, LX/15x;->g:Z

    if-eqz v0, :cond_0

    .line 183156
    invoke-direct {p0}, LX/15x;->p()V

    .line 183157
    :cond_0
    return-void
.end method

.method public final b([CII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 183020
    iput-object v1, p0, LX/15x;->c:[C

    .line 183021
    const/4 v0, -0x1

    iput v0, p0, LX/15x;->d:I

    .line 183022
    iput v2, p0, LX/15x;->e:I

    .line 183023
    iput-object v1, p0, LX/15x;->k:Ljava/lang/String;

    .line 183024
    iput-object v1, p0, LX/15x;->l:[C

    .line 183025
    iget-boolean v0, p0, LX/15x;->g:Z

    if-eqz v0, :cond_1

    .line 183026
    invoke-direct {p0}, LX/15x;->p()V

    .line 183027
    :cond_0
    :goto_0
    iput v2, p0, LX/15x;->h:I

    iput v2, p0, LX/15x;->j:I

    .line 183028
    invoke-virtual {p0, p1, p2, p3}, LX/15x;->c([CII)V

    .line 183029
    return-void

    .line 183030
    :cond_1
    iget-object v0, p0, LX/15x;->i:[C

    if-nez v0, :cond_0

    .line 183031
    invoke-direct {p0, p3}, LX/15x;->b(I)[C

    move-result-object v0

    iput-object v0, p0, LX/15x;->i:[C

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 183012
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    .line 183013
    iget v0, p0, LX/15x;->e:I

    .line 183014
    :goto_0
    return v0

    .line 183015
    :cond_0
    iget-object v0, p0, LX/15x;->l:[C

    if-eqz v0, :cond_1

    .line 183016
    iget-object v0, p0, LX/15x;->l:[C

    array-length v0, v0

    goto :goto_0

    .line 183017
    :cond_1
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 183018
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 183019
    :cond_2
    iget v0, p0, LX/15x;->h:I

    iget v1, p0, LX/15x;->j:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final c([CII)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 182903
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    .line 182904
    invoke-direct {p0, p3}, LX/15x;->c(I)V

    .line 182905
    :cond_0
    iput-object v1, p0, LX/15x;->k:Ljava/lang/String;

    .line 182906
    iput-object v1, p0, LX/15x;->l:[C

    .line 182907
    iget-object v0, p0, LX/15x;->i:[C

    .line 182908
    array-length v1, v0

    iget v2, p0, LX/15x;->j:I

    sub-int/2addr v1, v2

    .line 182909
    if-lt v1, p3, :cond_1

    .line 182910
    iget v1, p0, LX/15x;->j:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182911
    iget v0, p0, LX/15x;->j:I

    add-int/2addr v0, p3

    iput v0, p0, LX/15x;->j:I

    .line 182912
    :goto_0
    return-void

    .line 182913
    :cond_1
    if-lez v1, :cond_2

    .line 182914
    iget v2, p0, LX/15x;->j:I

    invoke-static {p1, p2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182915
    add-int/2addr p2, v1

    .line 182916
    sub-int/2addr p3, v1

    .line 182917
    :cond_2
    invoke-direct {p0, p3}, LX/15x;->d(I)V

    .line 182918
    iget-object v0, p0, LX/15x;->i:[C

    array-length v0, v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 182919
    iget-object v1, p0, LX/15x;->i:[C

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182920
    iget v1, p0, LX/15x;->j:I

    add-int/2addr v1, v0

    iput v1, p0, LX/15x;->j:I

    .line 182921
    add-int/2addr p2, v0

    .line 182922
    sub-int/2addr p3, v0

    .line 182923
    if-gtz p3, :cond_2

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 182924
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/15x;->d:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 182925
    iget v1, p0, LX/15x;->d:I

    if-gez v1, :cond_0

    iget-object v1, p0, LX/15x;->l:[C

    if-eqz v1, :cond_1

    .line 182926
    :cond_0
    :goto_0
    return v0

    .line 182927
    :cond_1
    iget-object v1, p0, LX/15x;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 182928
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()[C
    .locals 1

    .prologue
    .line 182929
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_0

    .line 182930
    iget-object v0, p0, LX/15x;->c:[C

    .line 182931
    :goto_0
    return-object v0

    .line 182932
    :cond_0
    iget-object v0, p0, LX/15x;->l:[C

    if-eqz v0, :cond_1

    .line 182933
    iget-object v0, p0, LX/15x;->l:[C

    goto :goto_0

    .line 182934
    :cond_1
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 182935
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, LX/15x;->l:[C

    goto :goto_0

    .line 182936
    :cond_2
    iget-boolean v0, p0, LX/15x;->g:Z

    if-nez v0, :cond_3

    .line 182937
    iget-object v0, p0, LX/15x;->i:[C

    goto :goto_0

    .line 182938
    :cond_3
    invoke-virtual {p0}, LX/15x;->h()[C

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 182939
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 182940
    iget-object v0, p0, LX/15x;->l:[C

    if-eqz v0, :cond_1

    .line 182941
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/15x;->l:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    .line 182942
    :cond_0
    :goto_0
    iget-object v0, p0, LX/15x;->k:Ljava/lang/String;

    :goto_1
    return-object v0

    .line 182943
    :cond_1
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_3

    .line 182944
    iget v0, p0, LX/15x;->e:I

    if-gtz v0, :cond_2

    .line 182945
    const-string v0, ""

    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    goto :goto_1

    .line 182946
    :cond_2
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/15x;->c:[C

    iget v2, p0, LX/15x;->d:I

    iget v3, p0, LX/15x;->e:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    goto :goto_0

    .line 182947
    :cond_3
    iget v0, p0, LX/15x;->h:I

    .line 182948
    iget v1, p0, LX/15x;->j:I

    .line 182949
    if-nez v0, :cond_5

    .line 182950
    if-nez v1, :cond_4

    const-string v0, ""

    :goto_2
    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, LX/15x;->i:[C

    invoke-direct {v0, v3, v2, v1}, Ljava/lang/String;-><init>([CII)V

    goto :goto_2

    .line 182951
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    add-int/2addr v0, v1

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 182952
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 182953
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_6

    .line 182954
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    .line 182955
    array-length v5, v0

    invoke-virtual {v3, v0, v2, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 182956
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 182957
    :cond_6
    iget-object v0, p0, LX/15x;->i:[C

    iget v1, p0, LX/15x;->j:I

    invoke-virtual {v3, v0, v2, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 182958
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/15x;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h()[C
    .locals 1

    .prologue
    .line 182959
    iget-object v0, p0, LX/15x;->l:[C

    .line 182960
    if-nez v0, :cond_0

    .line 182961
    invoke-direct {p0}, LX/15x;->q()[C

    move-result-object v0

    iput-object v0, p0, LX/15x;->l:[C

    .line 182962
    :cond_0
    return-object v0
.end method

.method public final i()Ljava/math/BigDecimal;
    .locals 4

    .prologue
    .line 182963
    iget-object v0, p0, LX/15x;->l:[C

    if-eqz v0, :cond_0

    .line 182964
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15x;->l:[C

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>([C)V

    .line 182965
    :goto_0
    return-object v0

    .line 182966
    :cond_0
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_1

    .line 182967
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15x;->c:[C

    iget v2, p0, LX/15x;->d:I

    iget v3, p0, LX/15x;->e:I

    invoke-direct {v0, v1, v2, v3}, Ljava/math/BigDecimal;-><init>([CII)V

    goto :goto_0

    .line 182968
    :cond_1
    iget v0, p0, LX/15x;->h:I

    if-nez v0, :cond_2

    .line 182969
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15x;->i:[C

    const/4 v2, 0x0

    iget v3, p0, LX/15x;->j:I

    invoke-direct {v0, v1, v2, v3}, Ljava/math/BigDecimal;-><init>([CII)V

    goto :goto_0

    .line 182970
    :cond_2
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, LX/15x;->h()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>([C)V

    goto :goto_0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 182971
    invoke-virtual {p0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/16K;->c(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public final k()[C
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 182972
    iget v0, p0, LX/15x;->d:I

    if-ltz v0, :cond_1

    .line 182973
    invoke-direct {p0, v2}, LX/15x;->c(I)V

    .line 182974
    :cond_0
    :goto_0
    iget-object v0, p0, LX/15x;->i:[C

    return-object v0

    .line 182975
    :cond_1
    iget-object v0, p0, LX/15x;->i:[C

    .line 182976
    if-nez v0, :cond_2

    .line 182977
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/15x;->b(I)[C

    move-result-object v0

    iput-object v0, p0, LX/15x;->i:[C

    goto :goto_0

    .line 182978
    :cond_2
    iget v1, p0, LX/15x;->j:I

    array-length v0, v0

    if-lt v1, v0, :cond_0

    .line 182979
    invoke-direct {p0, v2}, LX/15x;->d(I)V

    goto :goto_0
.end method

.method public final l()[C
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 182980
    const/4 v0, -0x1

    iput v0, p0, LX/15x;->d:I

    .line 182981
    iput v1, p0, LX/15x;->j:I

    .line 182982
    iput v1, p0, LX/15x;->e:I

    .line 182983
    iput-object v2, p0, LX/15x;->c:[C

    .line 182984
    iput-object v2, p0, LX/15x;->k:Ljava/lang/String;

    .line 182985
    iput-object v2, p0, LX/15x;->l:[C

    .line 182986
    iget-boolean v0, p0, LX/15x;->g:Z

    if-eqz v0, :cond_0

    .line 182987
    invoke-direct {p0}, LX/15x;->p()V

    .line 182988
    :cond_0
    iget-object v0, p0, LX/15x;->i:[C

    .line 182989
    if-nez v0, :cond_1

    .line 182990
    invoke-direct {p0, v1}, LX/15x;->b(I)[C

    move-result-object v0

    iput-object v0, p0, LX/15x;->i:[C

    .line 182991
    :cond_1
    return-object v0
.end method

.method public final n()[C
    .locals 2

    .prologue
    .line 182992
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 182993
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    .line 182994
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/15x;->g:Z

    .line 182995
    iget-object v0, p0, LX/15x;->f:Ljava/util/ArrayList;

    iget-object v1, p0, LX/15x;->i:[C

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182996
    iget-object v0, p0, LX/15x;->i:[C

    array-length v0, v0

    .line 182997
    iget v1, p0, LX/15x;->h:I

    add-int/2addr v1, v0

    iput v1, p0, LX/15x;->h:I

    .line 182998
    shr-int/lit8 v1, v0, 0x1

    add-int/2addr v0, v1

    const/high16 v1, 0x40000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 182999
    new-array v1, v0, [C

    move-object v0, v1

    .line 183000
    const/4 v1, 0x0

    iput v1, p0, LX/15x;->j:I

    .line 183001
    iput-object v0, p0, LX/15x;->i:[C

    .line 183002
    return-object v0
.end method

.method public final o()[C
    .locals 5

    .prologue
    const/high16 v4, 0x40000

    const/4 v3, 0x0

    .line 183003
    iget-object v1, p0, LX/15x;->i:[C

    .line 183004
    array-length v2, v1

    .line 183005
    if-ne v2, v4, :cond_0

    const v0, 0x40001

    .line 183006
    :goto_0
    new-array v4, v0, [C

    move-object v0, v4

    .line 183007
    iput-object v0, p0, LX/15x;->i:[C

    .line 183008
    iget-object v0, p0, LX/15x;->i:[C

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183009
    iget-object v0, p0, LX/15x;->i:[C

    return-object v0

    .line 183010
    :cond_0
    shr-int/lit8 v0, v2, 0x1

    add-int/2addr v0, v2

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183011
    invoke-virtual {p0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
