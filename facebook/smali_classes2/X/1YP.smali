.class public final LX/1YP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1YR;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1YR;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 273514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273515
    iput-object p1, p0, LX/1YP;->a:LX/0QB;

    .line 273516
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 273503
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1YP;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 273505
    packed-switch p2, :pswitch_data_0

    .line 273506
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273507
    :pswitch_0
    invoke-static {p1}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v0

    .line 273508
    :goto_0
    return-object v0

    .line 273509
    :pswitch_1
    invoke-static {p1}, LX/1YT;->a(LX/0QB;)LX/1YT;

    move-result-object v0

    goto :goto_0

    .line 273510
    :pswitch_2
    invoke-static {p1}, LX/1YW;->a(LX/0QB;)LX/1YW;

    move-result-object v0

    goto :goto_0

    .line 273511
    :pswitch_3
    invoke-static {p1}, LX/1YZ;->a(LX/0QB;)LX/1YZ;

    move-result-object v0

    goto :goto_0

    .line 273512
    :pswitch_4
    invoke-static {p1}, LX/1Yb;->a(LX/0QB;)LX/1Yb;

    move-result-object v0

    goto :goto_0

    .line 273513
    :pswitch_5
    invoke-static {p1}, LX/1Yc;->a(LX/0QB;)LX/1Yc;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 273504
    const/4 v0, 0x6

    return v0
.end method
