.class public LX/1MZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/1MZ;


# instance fields
.field public final a:LX/1Mc;

.field private final b:LX/1Ma;

.field public final c:LX/0ZR;

.field public final d:LX/0So;

.field private e:LX/1Mb;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ma;LX/0ZR;LX/0So;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 235767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235768
    sget-object v0, LX/1Mb;->DISCONNECTED:LX/1Mb;

    iput-object v0, p0, LX/1MZ;->e:LX/1Mb;

    .line 235769
    iput-wide v2, p0, LX/1MZ;->g:J

    .line 235770
    iput-wide v2, p0, LX/1MZ;->h:J

    .line 235771
    iput-boolean v1, p0, LX/1MZ;->i:Z

    .line 235772
    new-instance v0, LX/1Mc;

    invoke-direct {v0, p0}, LX/1Mc;-><init>(LX/1MZ;)V

    iput-object v0, p0, LX/1MZ;->a:LX/1Mc;

    .line 235773
    iput-object p1, p0, LX/1MZ;->b:LX/1Ma;

    .line 235774
    iput-object p2, p0, LX/1MZ;->c:LX/0ZR;

    .line 235775
    iput-object p3, p0, LX/1MZ;->d:LX/0So;

    .line 235776
    iput-boolean v1, p0, LX/1MZ;->j:Z

    .line 235777
    return-void
.end method

.method public static a(LX/0QB;)LX/1MZ;
    .locals 6

    .prologue
    .line 235754
    sget-object v0, LX/1MZ;->k:LX/1MZ;

    if-nez v0, :cond_1

    .line 235755
    const-class v1, LX/1MZ;

    monitor-enter v1

    .line 235756
    :try_start_0
    sget-object v0, LX/1MZ;->k:LX/1MZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235757
    if-eqz v2, :cond_0

    .line 235758
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235759
    new-instance p0, LX/1MZ;

    invoke-static {v0}, LX/1Ma;->a(LX/0QB;)LX/1Ma;

    move-result-object v3

    check-cast v3, LX/1Ma;

    invoke-static {v0}, LX/0ZR;->a(LX/0QB;)LX/0ZR;

    move-result-object v4

    check-cast v4, LX/0ZR;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/1MZ;-><init>(LX/1Ma;LX/0ZR;LX/0So;)V

    .line 235760
    move-object v0, p0

    .line 235761
    sput-object v0, LX/1MZ;->k:LX/1MZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235762
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235763
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235764
    :cond_1
    sget-object v0, LX/1MZ;->k:LX/1MZ;

    return-object v0

    .line 235765
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1u0;)LX/1Mb;
    .locals 4

    .prologue
    .line 235747
    sget-object v0, LX/2Z3;->b:[I

    invoke-virtual {p0}, LX/1u0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235748
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received a state I did not expect %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235749
    :pswitch_0
    sget-object v0, LX/1Mb;->CONNECTED:LX/1Mb;

    .line 235750
    :goto_0
    return-object v0

    .line 235751
    :pswitch_1
    sget-object v0, LX/1Mb;->CONNECTING:LX/1Mb;

    goto :goto_0

    .line 235752
    :pswitch_2
    sget-object v0, LX/1Mb;->CONNECT_SENT:LX/1Mb;

    goto :goto_0

    .line 235753
    :pswitch_3
    sget-object v0, LX/1Mb;->DISCONNECTED:LX/1Mb;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static declared-synchronized a(LX/1MZ;LX/1Mb;JJZ)V
    .locals 2

    .prologue
    .line 235734
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/1Mb;->CONNECT_SENT:LX/1Mb;

    if-ne p1, v0, :cond_1

    .line 235735
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1MZ;->j:Z

    .line 235736
    sget-object p1, LX/1Mb;->CONNECTED:LX/1Mb;

    .line 235737
    :goto_0
    iget-object v0, p0, LX/1MZ;->e:LX/1Mb;

    .line 235738
    iput-object p1, p0, LX/1MZ;->e:LX/1Mb;

    .line 235739
    iput-wide p2, p0, LX/1MZ;->g:J

    .line 235740
    iput-wide p4, p0, LX/1MZ;->h:J

    .line 235741
    iput-boolean p6, p0, LX/1MZ;->i:Z

    .line 235742
    iget-object v1, p0, LX/1MZ;->e:LX/1Mb;

    if-eq v1, v0, :cond_0

    .line 235743
    invoke-direct {p0}, LX/1MZ;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235744
    :cond_0
    monitor-exit p0

    return-void

    .line 235745
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/1MZ;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 235746
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 5

    .prologue
    .line 235721
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/2Z3;->a:[I

    iget-object v1, p0, LX/1MZ;->e:LX/1Mb;

    invoke-virtual {v1}, LX/1Mb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235722
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received a state I did not expect %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/1MZ;->e:LX/1Mb;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 235724
    :pswitch_0
    :try_start_1
    sget-object v0, LX/2EU;->CHANNEL_CONNECTING:LX/2EU;

    .line 235725
    :cond_0
    :goto_0
    iget-object v1, p0, LX/1MZ;->b:LX/1Ma;

    .line 235726
    iget-object v2, v1, LX/1Ma;->b:LX/0kd;

    invoke-virtual {v2}, LX/0kd;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, LX/1Ma;->d:LX/0Uh;

    const/16 v3, 0x12e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 235727
    iget-object v2, v1, LX/1Ma;->e:LX/0Wd;

    new-instance v3, Lcom/facebook/push/mqtt/external/PushStateBroadcaster$1;

    invoke-direct {v3, v1, v0}, Lcom/facebook/push/mqtt/external/PushStateBroadcaster$1;-><init>(LX/1Ma;LX/2EU;)V

    const v4, 0x6167d67f

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235728
    :goto_1
    monitor-exit p0

    return-void

    .line 235729
    :pswitch_1
    :try_start_2
    sget-object v0, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    goto :goto_0

    .line 235730
    :pswitch_2
    sget-object v0, LX/2EU;->CHANNEL_DISCONNECTED:LX/2EU;

    .line 235731
    iget-boolean v1, p0, LX/1MZ;->i:Z

    if-eqz v1, :cond_0

    .line 235732
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2EU;->setClockSkewDetected(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 235733
    :cond_1
    invoke-static {v1, v0}, LX/1Ma;->b(LX/1Ma;LX/2EU;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 235708
    monitor-enter p0

    .line 235709
    :try_start_0
    iget-wide v7, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->b:J

    move-wide v0, v7

    .line 235710
    iget-wide v2, p0, LX/1MZ;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 235711
    iget-wide v7, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->b:J

    move-wide v0, v7

    .line 235712
    iput-wide v0, p0, LX/1MZ;->f:J

    .line 235713
    iget-object v0, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    move-object v0, v0

    .line 235714
    invoke-static {v0}, LX/1MZ;->a(LX/1u0;)LX/1Mb;

    move-result-object v1

    .line 235715
    iget-wide v7, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    move-wide v2, v7

    .line 235716
    iget-wide v7, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    move-wide v4, v7

    .line 235717
    iget-boolean v0, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    move v6, v0

    .line 235718
    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/1MZ;->a(LX/1MZ;LX/1Mb;JJZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235719
    :cond_0
    monitor-exit p0

    return-void

    .line 235720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/1Mb;
    .locals 1

    .prologue
    .line 235778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1MZ;->e:LX/1Mb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()J
    .locals 2

    .prologue
    .line 235707
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1MZ;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 235706
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1MZ;->e:LX/1Mb;

    sget-object v1, LX/1Mb;->CONNECTED:LX/1Mb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 2

    .prologue
    .line 235705
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1MZ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1MZ;->e:LX/1Mb;

    sget-object v1, LX/1Mb;->CONNECTING:LX/1Mb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 235704
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1MZ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 7

    .prologue
    .line 235701
    monitor-enter p0

    :try_start_0
    sget-object v1, LX/1Mb;->DISCONNECTED:LX/1Mb;

    iget-wide v2, p0, LX/1MZ;->g:J

    iget-object v0, p0, LX/1MZ;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/1MZ;->a(LX/1MZ;LX/1Mb;JJZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235702
    monitor-exit p0

    return-void

    .line 235703
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
