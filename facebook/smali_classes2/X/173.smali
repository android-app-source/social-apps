.class public final LX/173;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImageAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 188864
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 188865
    instance-of v0, p0, LX/173;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 188866
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/173;
    .locals 2

    .prologue
    .line 188867
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 188868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/173;->b:LX/0Px;

    .line 188870
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/173;->c:LX/0Px;

    .line 188871
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->k()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/173;->d:LX/0Px;

    .line 188872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/173;->e:LX/0Px;

    .line 188873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 188874
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 188875
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/173;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 188876
    iput-object p1, p0, LX/173;->f:Ljava/lang/String;

    .line 188877
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 188878
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;-><init>(LX/173;)V

    .line 188879
    return-object v0
.end method
