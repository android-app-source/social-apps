.class public LX/1Yd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0So;

.field private final c:LX/03V;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoElapsedMonitor$ElapsedTimeListener;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/395;

.field private f:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273787
    const-class v0, LX/1Yd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Yd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273789
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1Yd;->d:Ljava/util/Set;

    .line 273790
    iput-object p2, p0, LX/1Yd;->c:LX/03V;

    .line 273791
    iput-object p1, p0, LX/1Yd;->b:LX/0So;

    .line 273792
    return-void
.end method

.method public static a(LX/0QB;)LX/1Yd;
    .locals 5

    .prologue
    .line 273793
    const-class v1, LX/1Yd;

    monitor-enter v1

    .line 273794
    :try_start_0
    sget-object v0, LX/1Yd;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273795
    sput-object v2, LX/1Yd;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273796
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273797
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273798
    new-instance p0, LX/1Yd;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/1Yd;-><init>(LX/0So;LX/03V;)V

    .line 273799
    move-object v0, p0

    .line 273800
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273801
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Yd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273802
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 273746
    goto :goto_1

    .line 273747
    :goto_0
    return-void

    .line 273748
    :goto_1
    iget-object v0, p0, LX/1Yd;->e:LX/395;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Yd;->f:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 273749
    :cond_0
    iget-object v0, p0, LX/1Yd;->c:LX/03V;

    sget-object v1, LX/1Yd;->a:Ljava/lang/String;

    const-string v2, "onExitFullScreen() is called before entering full screen"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273750
    :cond_1
    iget-object v0, p0, LX/1Yd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yc;

    .line 273751
    iget-object v2, p0, LX/1Yd;->e:LX/395;

    iget-object v3, p0, LX/1Yd;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iget-object v3, p0, LX/1Yd;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 273752
    if-eqz v2, :cond_2

    .line 273753
    iget-object v3, v2, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 273754
    if-nez v3, :cond_5

    .line 273755
    :cond_2
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 273756
    :goto_3
    move-object v3, v3

    .line 273757
    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 273758
    :goto_4
    goto :goto_2

    .line 273759
    :cond_3
    iput-object v8, p0, LX/1Yd;->e:LX/395;

    .line 273760
    iput-object v8, p0, LX/1Yd;->f:Ljava/lang/Long;

    goto :goto_0

    .line 273761
    :cond_4
    iget-object v6, v0, LX/1Yc;->g:LX/1Pi;

    .line 273762
    iget-object v7, v0, LX/1Yc;->f:LX/0ad;

    sget-char v9, LX/34q;->i:C

    const v10, 0x7f080d31

    iget-object v11, v0, LX/1Yc;->b:Landroid/content/res/Resources;

    invoke-interface {v7, v9, v10, v11}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    .line 273763
    iget-object v9, v0, LX/1Yc;->e:LX/1YX;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->VIDEO_WELCOME:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v9, v7, v10, v3, v11}, LX/1YX;->a(Ljava/lang/String;Ljava/lang/Long;LX/0Rf;Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;)LX/34n;

    move-result-object v7

    move-object v3, v7

    .line 273764
    invoke-interface {v6, v3}, LX/1Pi;->a(LX/34p;)V

    goto :goto_4

    .line 273765
    :cond_5
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v7

    .line 273766
    iget-object v3, v2, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 273767
    invoke-static {v3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 273768
    iget-object v3, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 273769
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 273770
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 273771
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273772
    :cond_6
    invoke-virtual {v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v3, 0x0

    move v6, v3

    :goto_5
    if-ge v6, v10, :cond_8

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/flatbuffers/Flattenable;

    .line 273773
    instance-of v11, v3, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v11, :cond_8

    .line 273774
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 273775
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 273776
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273777
    :cond_7
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_5

    .line 273778
    :cond_8
    invoke-virtual {v7}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    goto :goto_3
.end method

.method public final a(LX/395;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 273779
    goto :goto_1

    .line 273780
    :goto_0
    return-void

    .line 273781
    :goto_1
    iget-object v0, p0, LX/1Yd;->e:LX/395;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Yd;->f:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 273782
    :cond_0
    iget-object v0, p0, LX/1Yd;->c:LX/03V;

    sget-object v1, LX/1Yd;->a:Ljava/lang/String;

    const-string v2, "onEnterFullScreen() is called twice before exit"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273783
    iput-object v3, p0, LX/1Yd;->e:LX/395;

    .line 273784
    iput-object v3, p0, LX/1Yd;->f:Ljava/lang/Long;

    goto :goto_0

    .line 273785
    :cond_1
    iput-object p1, p0, LX/1Yd;->e:LX/395;

    .line 273786
    iget-object v0, p0, LX/1Yd;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1Yd;->f:Ljava/lang/Long;

    goto :goto_0
.end method
