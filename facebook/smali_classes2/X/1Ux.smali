.class public final LX/1Ux;
.super LX/1Ur;
.source ""


# static fields
.field public static final i:LX/1Up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256895
    new-instance v0, LX/1Ux;

    invoke-direct {v0}, LX/1Ux;-><init>()V

    sput-object v0, LX/1Ux;->i:LX/1Up;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 256894
    invoke-direct {p0}, LX/1Ur;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFFF)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 256886
    cmpl-float v0, p8, p7

    if-lez v0, :cond_0

    .line 256887
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p3

    mul-float/2addr v2, p8

    sub-float/2addr v1, v2

    mul-float/2addr v1, v4

    add-float/2addr v1, v0

    .line 256888
    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    .line 256889
    :goto_0
    invoke-virtual {p1, p8, p8}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 256890
    add-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 256891
    return-void

    .line 256892
    :cond_0
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    .line 256893
    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, p4

    mul-float/2addr v3, p7

    sub-float/2addr v2, v3

    mul-float/2addr v2, v4

    add-float/2addr v0, v2

    move p8, p7

    goto :goto_0
.end method
