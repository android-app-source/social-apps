.class public LX/1Qo;
.super LX/1Qp;
.source ""

# interfaces
.implements LX/1Qq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Qp",
        "<",
        "LX/1a1;",
        ">;",
        "LX/1Qq;"
    }
.end annotation


# instance fields
.field private final a:LX/1Rq;


# direct methods
.method public constructor <init>(LX/1Rq;)V
    .locals 0

    .prologue
    .line 245180
    invoke-direct {p0, p1}, LX/1Qp;-><init>(LX/1OO;)V

    .line 245181
    iput-object p1, p0, LX/1Qo;->a:LX/1Rq;

    .line 245182
    return-void
.end method


# virtual methods
.method public final a(LX/5Mj;)V
    .locals 1

    .prologue
    .line 245185
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/0fn;->a(LX/5Mj;)V

    .line 245186
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 245183
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 245184
    return-void
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 245179
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 245178
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    return v0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 245187
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 245188
    return-void
.end method

.method public final e()LX/1R4;
    .locals 1

    .prologue
    .line 245177
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 245176
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->g(I)I

    move-result v0

    return v0
.end method

.method public final h_(I)I
    .locals 1

    .prologue
    .line 245175
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 245172
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public final m_(I)I
    .locals 1

    .prologue
    .line 245174
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->m_(I)I

    move-result v0

    return v0
.end method

.method public final n_(I)I
    .locals 1

    .prologue
    .line 245173
    iget-object v0, p0, LX/1Qo;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->n_(I)I

    move-result v0

    return v0
.end method
