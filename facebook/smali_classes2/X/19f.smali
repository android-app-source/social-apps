.class public LX/19f;
.super LX/19e;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/19f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208216
    invoke-direct {p0, p1}, LX/19e;-><init>(Landroid/content/Context;)V

    .line 208217
    return-void
.end method

.method public static a(LX/0QB;)LX/19f;
    .locals 4

    .prologue
    .line 208202
    sget-object v0, LX/19f;->b:LX/19f;

    if-nez v0, :cond_1

    .line 208203
    const-class v1, LX/19f;

    monitor-enter v1

    .line 208204
    :try_start_0
    sget-object v0, LX/19f;->b:LX/19f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208205
    if-eqz v2, :cond_0

    .line 208206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208207
    new-instance p0, LX/19f;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/19f;-><init>(Landroid/content/Context;)V

    .line 208208
    move-object v0, p0

    .line 208209
    sput-object v0, LX/19f;->b:LX/19f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208210
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208211
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208212
    :cond_1
    sget-object v0, LX/19f;->b:LX/19f;

    return-object v0

    .line 208213
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/TextureView;
    .locals 2

    .prologue
    .line 208215
    new-instance v0, LX/7EB;

    iget-object v1, p0, LX/19e;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/7EB;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
