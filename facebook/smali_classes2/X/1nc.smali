.class public LX/1nc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:Landroid/content/res/ColorStateList;

.field public static final b:I

.field public static final c:Landroid/graphics/Typeface;

.field public static final d:LX/1nd;

.field public static final e:Landroid/text/Layout$Alignment;

.field private static final f:[Landroid/text/Layout$Alignment;

.field private static final g:[Landroid/text/TextUtils$TruncateAt;

.field private static final h:Landroid/graphics/Typeface;

.field public static final i:Landroid/graphics/Path;

.field public static final j:Landroid/graphics/Rect;

.field public static final k:Landroid/graphics/RectF;

.field private static final l:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1nq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 317104
    invoke-static {}, Landroid/text/Layout$Alignment;->values()[Landroid/text/Layout$Alignment;

    move-result-object v0

    sput-object v0, LX/1nc;->f:[Landroid/text/Layout$Alignment;

    .line 317105
    invoke-static {}, Landroid/text/TextUtils$TruncateAt;->values()[Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    sput-object v0, LX/1nc;->g:[Landroid/text/TextUtils$TruncateAt;

    .line 317106
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    sput-object v0, LX/1nc;->h:Landroid/graphics/Typeface;

    .line 317107
    const/4 v0, 0x0

    sput-object v0, LX/1nc;->a:Landroid/content/res/ColorStateList;

    .line 317108
    sget-object v0, LX/1nc;->h:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    sput v0, LX/1nc;->b:I

    .line 317109
    sget-object v0, LX/1nc;->h:Landroid/graphics/Typeface;

    sput-object v0, LX/1nc;->c:Landroid/graphics/Typeface;

    .line 317110
    sget-object v0, LX/1nd;->TOP:LX/1nd;

    sput-object v0, LX/1nc;->d:LX/1nd;

    .line 317111
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/1nc;->e:Landroid/text/Layout$Alignment;

    .line 317112
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, LX/1nc;->i:Landroid/graphics/Path;

    .line 317113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1nc;->j:Landroid/graphics/Rect;

    .line 317114
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/1nc;->k:Landroid/graphics/RectF;

    .line 317115
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1nc;->l:LX/0Zi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 317116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317117
    return-void
.end method

.method private static a(ILandroid/text/TextUtils$TruncateAt;ZIFFFIZLjava/lang/CharSequence;ILandroid/content/res/ColorStateList;IIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZI)Landroid/text/Layout;
    .locals 5

    .prologue
    .line 317031
    sget-object v1, LX/1nc;->l:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nq;

    .line 317032
    if-nez v1, :cond_0

    .line 317033
    new-instance v1, LX/1nq;

    invoke-direct {v1}, LX/1nq;-><init>()V

    .line 317034
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1nq;->c(Z)LX/1nq;

    .line 317035
    :cond_0
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 317036
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected size mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 317037
    :sswitch_0
    const/4 v2, 0x0

    .line 317038
    :goto_0
    invoke-virtual {v1, p1}, LX/1nq;->a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/1nq;->f(I)LX/1nq;

    move-result-object v3

    invoke-virtual {v3, p4, p5, p6, p7}, LX/1nq;->a(FFFI)LX/1nq;

    move-result-object v3

    invoke-virtual {v3, p8}, LX/1nq;->b(Z)LX/1nq;

    move-result-object v3

    invoke-virtual {v3, p9}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v3

    move/from16 v0, p13

    invoke-virtual {v3, v0}, LX/1nq;->b(I)LX/1nq;

    move-result-object v3

    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v4

    invoke-virtual {v3, v4, v2}, LX/1nq;->a(II)LX/1nq;

    .line 317039
    if-eqz p11, :cond_2

    .line 317040
    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/1nq;->a(Landroid/content/res/ColorStateList;)LX/1nq;

    .line 317041
    :goto_1
    sget-object v2, LX/1nc;->h:Landroid/graphics/Typeface;

    move-object/from16 v0, p17

    if-eq v0, v2, :cond_3

    .line 317042
    move-object/from16 v0, p17

    invoke-virtual {v1, v0}, LX/1nq;->a(Landroid/graphics/Typeface;)LX/1nq;

    .line 317043
    :goto_2
    const/4 v2, 0x2

    move/from16 v0, p20

    if-ne v0, v2, :cond_4

    sget-object v2, LX/0zo;->d:LX/0zr;

    :goto_3
    invoke-virtual {v1, v2}, LX/1nq;->a(LX/0zr;)LX/1nq;

    .line 317044
    invoke-virtual {v1, p2}, LX/1nq;->a(Z)LX/1nq;

    .line 317045
    move/from16 v0, p14

    invoke-virtual {v1, v0}, LX/1nq;->a(F)LX/1nq;

    .line 317046
    move/from16 v0, p15

    invoke-virtual {v1, v0}, LX/1nq;->b(F)LX/1nq;

    .line 317047
    move-object/from16 v0, p18

    invoke-virtual {v1, v0}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 317048
    move/from16 v0, p12

    invoke-virtual {v1, v0}, LX/1nq;->d(I)LX/1nq;

    .line 317049
    invoke-virtual {v1}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v2

    .line 317050
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 317051
    sget-object v3, LX/1nc;->l:LX/0Zi;

    invoke-virtual {v3, v1}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 317052
    if-eqz p19, :cond_1

    sget-boolean v1, LX/1V5;->e:Z

    if-nez v1, :cond_1

    .line 317053
    invoke-static {}, LX/1yv;->a()LX/1yv;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/1yv;->a(Landroid/text/Layout;)V

    .line 317054
    :cond_1
    return-object v2

    .line 317055
    :sswitch_1
    const/4 v2, 0x1

    .line 317056
    goto :goto_0

    .line 317057
    :sswitch_2
    const/4 v2, 0x2

    .line 317058
    goto :goto_0

    .line 317059
    :cond_2
    invoke-virtual {v1, p10}, LX/1nq;->c(I)LX/1nq;

    goto :goto_1

    .line 317060
    :cond_3
    move/from16 v0, p16

    invoke-virtual {v1, v0}, LX/1nq;->e(I)LX/1nq;

    goto :goto_2

    .line 317061
    :cond_4
    sget-object v2, LX/0zo;->c:LX/0zr;

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1np",
            "<",
            "Landroid/text/TextUtils$TruncateAt;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout$Alignment;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317062
    sget-object v1, LX/03r;->Text:[I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 317063
    const/4 v1, 0x0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_11

    .line 317064
    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 317065
    const/16 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 317066
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p7, v4}, LX/1np;->a(Ljava/lang/Object;)V

    .line 317067
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 317068
    :cond_1
    const/16 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 317069
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {p8, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317070
    :cond_2
    const/16 v5, 0x0

    if-ne v4, v5, :cond_3

    .line 317071
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p11

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317072
    :cond_3
    const/16 v5, 0x5

    if-ne v4, v5, :cond_4

    .line 317073
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 317074
    if-lez v4, :cond_0

    .line 317075
    sget-object v5, LX/1nc;->g:[Landroid/text/TextUtils$TruncateAt;

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v5, v4

    invoke-virtual {p1, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317076
    :cond_4
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_5

    const/16 v5, 0x11

    if-ne v4, v5, :cond_5

    .line 317077
    sget-object v5, LX/1nc;->f:[Landroid/text/Layout$Alignment;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    aget-object v4, v5, v4

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317078
    :cond_5
    const/16 v5, 0xb

    if-ne v4, v5, :cond_6

    .line 317079
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317080
    :cond_6
    const/16 v5, 0x9

    if-ne v4, v5, :cond_7

    .line 317081
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 317082
    :cond_7
    const/16 v5, 0x8

    if-ne v4, v5, :cond_8

    .line 317083
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p5, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317084
    :cond_8
    const/16 v5, 0xa

    if-ne v4, v5, :cond_9

    .line 317085
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p6, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317086
    :cond_9
    const/16 v5, 0x4

    if-ne v4, v5, :cond_a

    .line 317087
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317088
    :cond_a
    const/16 v5, 0x3

    if-ne v4, v5, :cond_b

    .line 317089
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317090
    :cond_b
    const/16 v5, 0x1

    if-ne v4, v5, :cond_c

    .line 317091
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317092
    :cond_c
    const/16 v5, 0x10

    if-ne v4, v5, :cond_d

    .line 317093
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317094
    :cond_d
    const/16 v5, 0xd

    if-ne v4, v5, :cond_e

    .line 317095
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p15

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317096
    :cond_e
    const/16 v5, 0xe

    if-ne v4, v5, :cond_f

    .line 317097
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p16

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317098
    :cond_f
    const/16 v5, 0xf

    if-ne v4, v5, :cond_10

    .line 317099
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p14

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317100
    :cond_10
    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 317101
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p17

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317102
    :cond_11
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 317103
    return-void
.end method

.method public static a(LX/1Dg;IILX/1no;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;ZIIFFFIZILandroid/content/res/ColorStateList;IIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLX/1np;)V
    .locals 22
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p15    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p18    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p19    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p20    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p21    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p22    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p23    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/ComponentLayout;",
            "II",
            "LX/1no;",
            "Ljava/lang/CharSequence;",
            "Landroid/text/TextUtils$TruncateAt;",
            "ZIIFFFIZI",
            "Landroid/content/res/ColorStateList;",
            "IIFFI",
            "Landroid/graphics/Typeface;",
            "Landroid/text/Layout$Alignment;",
            "Z",
            "LX/1np",
            "<",
            "Landroid/text/Layout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317016
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317017
    const/4 v1, 0x0

    move-object/from16 v0, p24

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 317018
    const/4 v1, 0x0

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->a:I

    .line 317019
    const/4 v1, 0x0

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->b:I

    .line 317020
    :goto_0
    return-void

    .line 317021
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->i()I

    move-result v21

    move/from16 v1, p1

    move-object/from16 v2, p5

    move/from16 v3, p6

    move/from16 v4, p8

    move/from16 v5, p9

    move/from16 v6, p10

    move/from16 v7, p11

    move/from16 v8, p12

    move/from16 v9, p13

    move-object/from16 v10, p4

    move/from16 v11, p14

    move-object/from16 v12, p15

    move/from16 v13, p16

    move/from16 v14, p17

    move/from16 v15, p18

    move/from16 v16, p19

    move/from16 v17, p20

    move-object/from16 v18, p21

    move-object/from16 v19, p22

    move/from16 v20, p23

    invoke-static/range {v1 .. v21}, LX/1nc;->a(ILandroid/text/TextUtils$TruncateAt;ZIFFFIZLjava/lang/CharSequence;ILandroid/content/res/ColorStateList;IIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZI)Landroid/text/Layout;

    move-result-object v2

    .line 317022
    move-object/from16 v0, p24

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 317023
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    move-result v1

    move/from16 v0, p1

    invoke-static {v0, v1}, LX/1mh;->b(II)I

    move-result v1

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->a:I

    .line 317024
    invoke-static {v2}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v1

    .line 317025
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    .line 317026
    move/from16 v0, p7

    if-ge v3, v0, :cond_1

    .line 317027
    invoke-virtual {v2}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 317028
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, p19

    add-float v2, v2, p18

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 317029
    sub-int v3, p7, v3

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 317030
    :cond_1
    move/from16 v0, p2

    invoke-static {v0, v1}, LX/1mh;->b(II)I

    move-result v1

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->b:I

    goto :goto_0
.end method

.method public static a(LX/1Dg;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;ZIFFFIZILandroid/content/res/ColorStateList;IIFFLX/1nd;ILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLandroid/text/Layout;LX/1np;LX/1np;LX/1np;)V
    .locals 23
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p2    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p5    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p6    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p7    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p11    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p14    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p15    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p16    # LX/1nd;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p18    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p19    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p20    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/ComponentLayout;",
            "Ljava/lang/CharSequence;",
            "Landroid/text/TextUtils$TruncateAt;",
            "ZIFFFIZI",
            "Landroid/content/res/ColorStateList;",
            "IIFF",
            "LX/1nd;",
            "I",
            "Landroid/graphics/Typeface;",
            "Landroid/text/Layout$Alignment;",
            "Z",
            "Landroid/text/Layout;",
            "LX/1np",
            "<",
            "Landroid/text/Layout;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<[",
            "Landroid/text/style/ClickableSpan;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317003
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317004
    :cond_0
    :goto_0
    return-void

    .line 317005
    :cond_1
    if-eqz p21, :cond_2

    .line 317006
    move-object/from16 v0, p22

    move-object/from16 v1, p21

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 317007
    :goto_1
    invoke-virtual/range {p22 .. p22}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/text/Layout;

    invoke-static {v2}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v2

    int-to-float v2, v2

    .line 317008
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->d()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->e()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->g()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 317009
    sget-object v4, LX/1oB;->a:[I

    invoke-virtual/range {p16 .. p16}, LX/1nd;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 317010
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    move-object/from16 v0, p23

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 317011
    :goto_2
    move-object/from16 v0, p1

    instance-of v2, v0, Landroid/text/Spanned;

    if-eqz v2, :cond_0

    move-object/from16 v2, p1

    .line 317012
    check-cast v2, Landroid/text/Spanned;

    const/4 v3, 0x0

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const-class v5, Landroid/text/style/ClickableSpan;

    invoke-interface {v2, v3, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p24

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 317013
    :cond_2
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->c()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, LX/1mh;->a(II)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->i()I

    move-result v22

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p1

    move/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p13

    move/from16 v16, p14

    move/from16 v17, p15

    move/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move/from16 v21, p20

    invoke-static/range {v2 .. v22}, LX/1nc;->a(ILandroid/text/TextUtils$TruncateAt;ZIFFFIZLjava/lang/CharSequence;ILandroid/content/res/ColorStateList;IIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZI)Landroid/text/Layout;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 317014
    :pswitch_0
    sub-float v2, v3, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    move-object/from16 v0, p23

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_2

    .line 317015
    :pswitch_1
    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    move-object/from16 v0, p23

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/1oF;Ljava/lang/CharSequence;IILandroid/content/res/ColorStateList;Landroid/text/Layout;Ljava/lang/Float;[Landroid/text/style/ClickableSpan;)V
    .locals 8
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p4    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 316998
    if-nez p6, :cond_1

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v4, p4

    move v5, p2

    move v6, p3

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, LX/1oF;->a(Ljava/lang/CharSequence;Landroid/text/Layout;FLandroid/content/res/ColorStateList;II[Landroid/text/style/ClickableSpan;)V

    .line 316999
    instance-of v0, p1, LX/1oG;

    if-eqz v0, :cond_0

    .line 317000
    check-cast p1, LX/1oG;

    invoke-interface {p1, p0}, LX/1oG;->a(Landroid/graphics/drawable/Drawable;)V

    .line 317001
    :cond_0
    return-void

    .line 317002
    :cond_1
    invoke-virtual {p6}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto :goto_0
.end method
