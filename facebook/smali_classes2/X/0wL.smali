.class public LX/0wL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0wL;


# instance fields
.field private final a:Landroid/view/accessibility/AccessibilityManager;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(Landroid/view/accessibility/AccessibilityManager;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 159668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159669
    iput-object p1, p0, LX/0wL;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 159670
    iput-object p2, p0, LX/0wL;->b:LX/03V;

    .line 159671
    return-void
.end method

.method public static a(LX/0QB;)LX/0wL;
    .locals 5

    .prologue
    .line 159672
    sget-object v0, LX/0wL;->c:LX/0wL;

    if-nez v0, :cond_1

    .line 159673
    const-class v1, LX/0wL;

    monitor-enter v1

    .line 159674
    :try_start_0
    sget-object v0, LX/0wL;->c:LX/0wL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 159675
    if-eqz v2, :cond_0

    .line 159676
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 159677
    new-instance p0, LX/0wL;

    invoke-static {v0}, LX/0sY;->b(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/0wL;-><init>(Landroid/view/accessibility/AccessibilityManager;LX/03V;)V

    .line 159678
    move-object v0, p0

    .line 159679
    sput-object v0, LX/0wL;->c:LX/0wL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159680
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 159681
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159682
    :cond_1
    sget-object v0, LX/0wL;->c:LX/0wL;

    return-object v0

    .line 159683
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 159684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;ZJ)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x1f4

    .line 159657
    if-nez p1, :cond_0

    .line 159658
    :goto_0
    return-void

    .line 159659
    :cond_0
    cmp-long v2, p3, v0

    if-gez v2, :cond_1

    move-wide p3, v0

    .line 159660
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    .line 159661
    new-instance v1, Lcom/facebook/accessibility/ViewAccessibilityHelper$2;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/facebook/accessibility/ViewAccessibilityHelper$2;-><init>(LX/0wL;Landroid/view/View;ZLjava/lang/NullPointerException;)V

    invoke-virtual {p1, v1, p3, p4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 159662
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159663
    :goto_0
    return-void

    .line 159664
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159665
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159666
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159667
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159646
    iget-object v0, p0, LX/0wL;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159647
    :cond_0
    :goto_0
    return-void

    .line 159648
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 159649
    if-eqz v0, :cond_0

    .line 159650
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 159651
    invoke-static {p1, v1}, LX/0vv;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 159652
    if-eqz p2, :cond_2

    .line 159653
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159654
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159655
    :cond_2
    invoke-interface {v0, p1, v1}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 159656
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 159630
    iget-object v0, p0, LX/0wL;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159631
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 159632
    invoke-virtual {p1, p2}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 159633
    :cond_0
    :goto_0
    return-void

    .line 159634
    :cond_1
    invoke-static {p1}, LX/0vv;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159635
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 159636
    const v0, 0x7f0d0094

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 159637
    const v0, 0x7f0d0094

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 159638
    const v0, 0x7f0d0095

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 159639
    const v0, 0x7f0d0096

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 159640
    const v0, 0x7f0d0097

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159641
    :cond_2
    if-ne p2, v2, :cond_3

    .line 159642
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 159643
    :cond_3
    const/4 v0, 0x2

    if-eq p2, v0, :cond_4

    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 159644
    :cond_4
    new-instance v0, LX/557;

    invoke-direct {v0, p0, p2, p1}, LX/557;-><init>(LX/0wL;ILandroid/view/View;)V

    .line 159645
    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ZLjava/lang/NullPointerException;)Z
    .locals 5
    .param p3    # Ljava/lang/NullPointerException;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 159620
    if-nez p1, :cond_0

    move v0, v1

    .line 159621
    :goto_0
    return v0

    .line 159622
    :cond_0
    if-eqz p2, :cond_1

    .line 159623
    const/high16 v0, 0x400000

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 159624
    :cond_1
    const/16 v0, 0x40

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1, v0, v2}, LX/0vv;->a(Landroid/view/View;ILandroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 159625
    :catch_0
    move-exception v0

    .line 159626
    iget-object v2, p0, LX/0wL;->b:LX/03V;

    if-eqz v2, :cond_2

    .line 159627
    iget-object v2, p0, LX/0wL;->b:LX/03V;

    const-class v3, LX/0wL;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NullPointerExceptioin when focusing on view"

    if-eqz p3, :cond_3

    :goto_1
    invoke-virtual {v2, v3, v4, p3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    .line 159628
    goto :goto_0

    :cond_3
    move-object p3, v0

    .line 159629
    goto :goto_1
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 159618
    const/4 v0, 0x1

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, p1, v0, v2, v3}, LX/0wL;->a(Landroid/view/View;ZJ)V

    .line 159619
    return-void
.end method
