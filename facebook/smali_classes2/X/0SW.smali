.class public final LX/0SW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# instance fields
.field public elapsedNanos:J

.field public isRunning:Z

.field private startTick:J

.field private final ticker:LX/0QV;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61365
    sget-object v0, LX/0QV;->SYSTEM_TICKER:LX/0QV;

    move-object v0, v0

    .line 61366
    iput-object v0, p0, LX/0SW;->ticker:LX/0QV;

    .line 61367
    return-void
.end method

.method private static abbreviate(Ljava/util/concurrent/TimeUnit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 61368
    sget-object v0, LX/4w6;->$SwitchMap$java$util$concurrent$TimeUnit:[I

    invoke-virtual {p0}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 61369
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61370
    :pswitch_0
    const-string v0, "ns"

    .line 61371
    :goto_0
    return-object v0

    .line 61372
    :pswitch_1
    const-string v0, "\u03bcs"

    goto :goto_0

    .line 61373
    :pswitch_2
    const-string v0, "ms"

    goto :goto_0

    .line 61374
    :pswitch_3
    const-string v0, "s"

    goto :goto_0

    .line 61375
    :pswitch_4
    const-string v0, "min"

    goto :goto_0

    .line 61376
    :pswitch_5
    const-string v0, "h"

    goto :goto_0

    .line 61377
    :pswitch_6
    const-string v0, "d"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static createStarted()LX/0SW;
    .locals 1
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 61378
    new-instance v0, LX/0SW;

    invoke-direct {v0}, LX/0SW;-><init>()V

    invoke-virtual {v0}, LX/0SW;->start()LX/0SW;

    move-result-object v0

    return-object v0
.end method

.method public static createUnstarted()LX/0SW;
    .locals 1
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 61379
    new-instance v0, LX/0SW;

    invoke-direct {v0}, LX/0SW;-><init>()V

    return-object v0
.end method

.method private elapsedNanos()J
    .locals 4

    .prologue
    .line 61380
    iget-boolean v0, p0, LX/0SW;->isRunning:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0SW;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    iget-wide v2, p0, LX/0SW;->startTick:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, LX/0SW;->elapsedNanos:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0SW;->elapsedNanos:J

    goto :goto_0
.end method


# virtual methods
.method public final elapsed(Ljava/util/concurrent/TimeUnit;)J
    .locals 3
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 61381
    invoke-direct {p0}, LX/0SW;->elapsedNanos()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final start()LX/0SW;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 61382
    iget-boolean v0, p0, LX/0SW;->isRunning:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "This stopwatch is already running."

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 61383
    iput-boolean v1, p0, LX/0SW;->isRunning:Z

    .line 61384
    iget-object v0, p0, LX/0SW;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    iput-wide v0, p0, LX/0SW;->startTick:J

    .line 61385
    return-object p0

    .line 61386
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final stop()LX/0SW;
    .locals 6

    .prologue
    .line 61387
    iget-object v0, p0, LX/0SW;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    .line 61388
    iget-boolean v2, p0, LX/0SW;->isRunning:Z

    const-string v3, "This stopwatch is already stopped."

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 61389
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/0SW;->isRunning:Z

    .line 61390
    iget-wide v2, p0, LX/0SW;->elapsedNanos:J

    iget-wide v4, p0, LX/0SW;->startTick:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0SW;->elapsedNanos:J

    .line 61391
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 11
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "String.format()"
    .end annotation

    .prologue
    .line 61392
    invoke-direct {p0}, LX/0SW;->elapsedNanos()J

    move-result-wide v0

    .line 61393
    const-wide/16 v9, 0x0

    .line 61394
    sget-object v7, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_0

    .line 61395
    sget-object v7, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 61396
    :goto_0
    move-object v2, v7

    .line 61397
    long-to-double v0, v0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v0, v4

    .line 61398
    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v4, "%.4g %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v2}, LX/0SW;->abbreviate(Ljava/util/concurrent/TimeUnit;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 61399
    :cond_0
    sget-object v7, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    .line 61400
    sget-object v7, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    .line 61401
    :cond_1
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    .line 61402
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    .line 61403
    :cond_2
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_3

    .line 61404
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    .line 61405
    :cond_3
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_4

    .line 61406
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    .line 61407
    :cond_4
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-lez v7, :cond_5

    .line 61408
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    .line 61409
    :cond_5
    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0
.end method
