.class public final LX/0PW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/0PW;",
            ">;"
        }
    .end annotation
.end field

.field public static b:I

.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0eu;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0eu;",
            ">;"
        }
    .end annotation
.end field

.field public f:[LX/0eu;

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56744
    new-instance v0, LX/0PX;

    invoke-direct {v0}, LX/0PX;-><init>()V

    sput-object v0, LX/0PW;->a:Ljava/lang/ThreadLocal;

    .line 56745
    const/4 v0, -0x1

    sput v0, LX/0PW;->b:I

    .line 56746
    new-instance v0, LX/0PY;

    invoke-direct {v0}, LX/0PY;-><init>()V

    sput-object v0, LX/0PW;->c:Ljava/util/Comparator;

    .line 56747
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/0PW;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56749
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0PW;->e:Landroid/util/SparseArray;

    .line 56750
    const/16 v0, 0x64

    new-array v0, v0, [LX/0eu;

    iput-object v0, p0, LX/0PW;->f:[LX/0eu;

    return-void
.end method

.method private static a(LX/0PW;J)I
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x0

    .line 56718
    const/16 v1, 0xa

    new-array v1, v1, [I

    move-object v2, v1

    move v3, v0

    move v1, v0

    .line 56719
    :goto_0
    iget v4, p0, LX/0PW;->g:I

    if-ge v0, v4, :cond_3

    .line 56720
    iget-object v4, p0, LX/0PW;->f:[LX/0eu;

    aget-object v4, v4, v0

    .line 56721
    if-eqz v4, :cond_1

    .line 56722
    iget-object v5, v4, LX/0eu;->a:LX/49P;

    move-object v5, v5

    .line 56723
    sget-object v6, LX/49P;->START:LX/49P;

    if-ne v5, v6, :cond_2

    .line 56724
    array-length v4, v2

    if-lt v1, v4, :cond_0

    .line 56725
    array-length v4, v2

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    .line 56726
    :cond_0
    aput v0, v2, v1

    .line 56727
    add-int/lit8 v1, v1, 0x1

    .line 56728
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56729
    :cond_2
    iget-object v5, v4, LX/0eu;->a:LX/49P;

    move-object v5, v5

    .line 56730
    sget-object v6, LX/49P;->STOP:LX/49P;

    if-ne v5, v6, :cond_1

    .line 56731
    add-int/lit8 v5, v1, -0x1

    aget v5, v2, v5

    .line 56732
    add-int/lit8 v1, v1, -0x1

    .line 56733
    iget-wide v11, v4, LX/0eu;->e:J

    move-wide v6, v11

    .line 56734
    iget-wide v11, v4, LX/0eu;->f:J

    move-wide v8, v11

    .line 56735
    sub-long/2addr v6, v8

    cmp-long v6, v6, p1

    if-gtz v6, :cond_1

    .line 56736
    iget-object v6, p0, LX/0PW;->f:[LX/0eu;

    aget-object v6, v6, v5

    .line 56737
    invoke-virtual {v4}, LX/0eu;->a()V

    .line 56738
    invoke-virtual {v6}, LX/0eu;->a()V

    .line 56739
    iget-object v4, p0, LX/0PW;->f:[LX/0eu;

    aput-object v10, v4, v0

    .line 56740
    iget-object v4, p0, LX/0PW;->f:[LX/0eu;

    aput-object v10, v4, v5

    .line 56741
    iget v4, p0, LX/0PW;->h:I

    add-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/0PW;->h:I

    .line 56742
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 56743
    :cond_3
    return v3
.end method

.method public static a(LX/0PW;LX/0eu;)V
    .locals 5

    .prologue
    .line 56701
    iget v0, p0, LX/0PW;->h:I

    mul-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/0PW;->g:I

    if-le v0, v1, :cond_2

    .line 56702
    const/4 v1, 0x0

    .line 56703
    move v0, v1

    move v2, v1

    .line 56704
    :goto_0
    iget v3, p0, LX/0PW;->g:I

    if-ge v2, v3, :cond_1

    .line 56705
    iget-object v3, p0, LX/0PW;->f:[LX/0eu;

    aget-object v3, v3, v2

    .line 56706
    if-eqz v3, :cond_0

    .line 56707
    iget-object v4, p0, LX/0PW;->f:[LX/0eu;

    aput-object v3, v4, v0

    .line 56708
    add-int/lit8 v0, v0, 0x1

    .line 56709
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 56710
    goto :goto_0

    .line 56711
    :cond_1
    iput v0, p0, LX/0PW;->g:I

    .line 56712
    iput v1, p0, LX/0PW;->h:I

    .line 56713
    :cond_2
    iget v0, p0, LX/0PW;->g:I

    iget-object v1, p0, LX/0PW;->f:[LX/0eu;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 56714
    iget-object v0, p0, LX/0PW;->f:[LX/0eu;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 56715
    iget-object v1, p0, LX/0PW;->f:[LX/0eu;

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0eu;

    iput-object v0, p0, LX/0PW;->f:[LX/0eu;

    .line 56716
    :cond_3
    iget-object v0, p0, LX/0PW;->f:[LX/0eu;

    iget v1, p0, LX/0PW;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0PW;->g:I

    aput-object p1, v0, v1

    .line 56717
    return-void
.end method

.method private static b(LX/0PW;I)I
    .locals 12

    .prologue
    .line 56674
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v4

    .line 56675
    if-gt v4, p1, :cond_2

    .line 56676
    const-wide/16 v4, -0x1

    .line 56677
    :cond_0
    :goto_0
    move-wide v0, v4

    .line 56678
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 56679
    const/4 v0, 0x0

    .line 56680
    :goto_1
    return v0

    :cond_1
    invoke-static {p0, v0, v1}, LX/0PW;->a(LX/0PW;J)I

    move-result v0

    goto :goto_1

    .line 56681
    :cond_2
    new-instance v5, Ljava/util/PriorityQueue;

    iget v4, p0, LX/0PW;->g:I

    div-int/lit8 v4, v4, 0x2

    sget-object v6, LX/0PW;->c:Ljava/util/Comparator;

    invoke-direct {v5, v4, v6}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 56682
    const/4 v4, 0x0

    :goto_2
    iget v6, p0, LX/0PW;->g:I

    if-ge v4, v6, :cond_4

    .line 56683
    iget-object v6, p0, LX/0PW;->f:[LX/0eu;

    aget-object v6, v6, v4

    .line 56684
    if-eqz v6, :cond_3

    .line 56685
    iget-object v7, v6, LX/0eu;->a:LX/49P;

    move-object v7, v7

    .line 56686
    sget-object v8, LX/49P;->STOP:LX/49P;

    if-ne v7, v8, :cond_3

    .line 56687
    invoke-virtual {v5, v6}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 56688
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 56689
    :cond_4
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v4

    sub-int v6, v4, p1

    .line 56690
    invoke-virtual {v5}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 56691
    const-wide/16 v4, 0x0

    .line 56692
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    if-lez v6, :cond_5

    .line 56693
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0eu;

    .line 56694
    iget-wide v10, v4, LX/0eu;->e:J

    move-wide v8, v10

    .line 56695
    iget-wide v10, v4, LX/0eu;->f:J

    move-wide v4, v10

    .line 56696
    sub-long v4, v8, v4

    .line 56697
    add-int/lit8 v6, v6, -0x2

    .line 56698
    goto :goto_3

    .line 56699
    :cond_5
    if-lez v6, :cond_0

    .line 56700
    const-wide v4, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private d()V
    .locals 14

    .prologue
    const/16 v5, 0x7d0

    const-wide/32 v6, 0xf4240

    const/16 v4, 0x5dc

    .line 56751
    const/4 v1, 0x0

    .line 56752
    invoke-static {}, LX/0PZ;->b()J

    move-result-wide v2

    .line 56753
    :try_start_0
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gt v0, v5, :cond_1

    .line 56754
    invoke-static {}, LX/0PZ;->b()J

    move-result-wide v0

    .line 56755
    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 56756
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    .line 56757
    :cond_0
    :goto_0
    return-void

    .line 56758
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/0PW;->e(LX/0PW;)I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    .line 56759
    const/16 v0, 0x5dc

    invoke-static {p0, v0}, LX/0PW;->b(LX/0PW;I)I

    move-result v0

    add-int/2addr v1, v0

    .line 56760
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v0

    if-le v0, v4, :cond_4

    .line 56761
    const/16 v0, 0x5dc

    const/4 v8, 0x0

    .line 56762
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v9

    sub-int/2addr v9, v0

    move v10, v8

    .line 56763
    :goto_1
    iget v11, p0, LX/0PW;->g:I

    if-ge v8, v11, :cond_3

    .line 56764
    if-lez v9, :cond_3

    .line 56765
    iget-object v11, p0, LX/0PW;->f:[LX/0eu;

    aget-object v11, v11, v8

    .line 56766
    if-eqz v11, :cond_2

    .line 56767
    iget-object v12, v11, LX/0eu;->a:LX/49P;

    move-object v12, v12

    .line 56768
    sget-object v13, LX/49P;->COMMENT:LX/49P;

    if-ne v12, v13, :cond_2

    .line 56769
    invoke-virtual {v11}, LX/0eu;->a()V

    .line 56770
    iget-object v11, p0, LX/0PW;->f:[LX/0eu;

    const/4 v12, 0x0

    aput-object v12, v11, v8

    .line 56771
    iget v11, p0, LX/0PW;->h:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, LX/0PW;->h:I

    .line 56772
    add-int/lit8 v10, v10, 0x1

    .line 56773
    add-int/lit8 v9, v9, -0x1

    .line 56774
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 56775
    :cond_3
    move v0, v10

    .line 56776
    add-int/2addr v1, v0

    .line 56777
    :cond_4
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v0

    if-le v0, v5, :cond_a

    .line 56778
    const-string v0, "ThreadTrace"

    const-string v4, "Resetting because hit couldn\'t get under hard limit after normal pruning"

    invoke-static {v0, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 56779
    const/4 v4, 0x0

    .line 56780
    move v0, v4

    :goto_2
    iget v5, p0, LX/0PW;->g:I

    if-ge v0, v5, :cond_7

    .line 56781
    iget-object v5, p0, LX/0PW;->f:[LX/0eu;

    aget-object v5, v5, v0

    .line 56782
    if-eqz v5, :cond_6

    .line 56783
    iget-object v8, v5, LX/0eu;->a:LX/49P;

    move-object v8, v8

    .line 56784
    sget-object v9, LX/49P;->START:LX/49P;

    if-ne v8, v9, :cond_5

    .line 56785
    iget-object v8, p0, LX/0PW;->e:Landroid/util/SparseArray;

    .line 56786
    iget v9, v5, LX/0eu;->b:I

    move v9, v9

    .line 56787
    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->remove(I)V

    .line 56788
    :cond_5
    invoke-virtual {v5}, LX/0eu;->a()V

    .line 56789
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 56790
    :cond_7
    iput v4, p0, LX/0PW;->g:I

    .line 56791
    iput v4, p0, LX/0PW;->h:I

    .line 56792
    iget-object v0, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56793
    :cond_8
    :goto_3
    invoke-static {}, LX/0PZ;->b()J

    move-result-wide v4

    .line 56794
    sub-long v2, v4, v2

    cmp-long v0, v2, v6

    if-gtz v0, :cond_9

    if-lez v1, :cond_0

    .line 56795
    :cond_9
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    goto/16 :goto_0

    .line 56796
    :cond_a
    :try_start_2
    invoke-static {p0}, LX/0PW;->g(LX/0PW;)I

    move-result v0

    if-le v0, v4, :cond_8

    .line 56797
    const-string v0, "ThreadTrace"

    const-string v4, "Couldn\'t get under soft limit after normal pruning"

    invoke-static {v0, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 56798
    :catchall_0
    move-exception v0

    invoke-static {}, LX/0PZ;->b()J

    move-result-wide v4

    .line 56799
    sub-long v2, v4, v2

    cmp-long v2, v2, v6

    if-gtz v2, :cond_b

    if-lez v1, :cond_c

    .line 56800
    :cond_b
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    .line 56801
    :cond_c
    throw v0
.end method

.method private static e(LX/0PW;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 56651
    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v4

    move v0, v1

    move v2, v1

    move v3, v1

    .line 56652
    :goto_0
    iget v6, p0, LX/0PW;->g:I

    if-ge v0, v6, :cond_3

    .line 56653
    iget-object v6, p0, LX/0PW;->f:[LX/0eu;

    aget-object v6, v6, v0

    .line 56654
    if-eqz v6, :cond_1

    .line 56655
    iget-object v7, v6, LX/0eu;->a:LX/49P;

    move-object v7, v7

    .line 56656
    sget-object v8, LX/49P;->START:LX/49P;

    if-ne v7, v8, :cond_2

    .line 56657
    add-int/lit8 v3, v3, 0x1

    .line 56658
    :cond_0
    :goto_1
    iget-wide v10, v6, LX/0eu;->e:J

    move-wide v6, v10

    .line 56659
    sub-long v6, v4, v6

    const-wide v8, 0x29e8d60800L

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    .line 56660
    if-nez v3, :cond_1

    move v2, v0

    .line 56661
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56662
    :cond_2
    sget-object v8, LX/49P;->STOP:LX/49P;

    if-ne v7, v8, :cond_0

    .line 56663
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 56664
    :cond_3
    if-lez v2, :cond_5

    move v0, v1

    .line 56665
    :goto_2
    add-int/lit8 v3, v2, 0x1

    if-ge v1, v3, :cond_6

    .line 56666
    iget-object v3, p0, LX/0PW;->f:[LX/0eu;

    aget-object v3, v3, v1

    .line 56667
    if-eqz v3, :cond_4

    .line 56668
    invoke-virtual {v3}, LX/0eu;->a()V

    .line 56669
    iget-object v3, p0, LX/0PW;->f:[LX/0eu;

    const/4 v4, 0x0

    aput-object v4, v3, v1

    .line 56670
    iget v3, p0, LX/0PW;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/0PW;->h:I

    .line 56671
    add-int/lit8 v0, v0, 0x1

    .line 56672
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 56673
    :cond_6
    return v0
.end method

.method public static g(LX/0PW;)I
    .locals 2

    .prologue
    .line 56650
    iget v0, p0, LX/0PW;->g:I

    iget v1, p0, LX/0PW;->h:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public static h()I
    .locals 2

    .prologue
    .line 56646
    sget-object v0, LX/0PW;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 56647
    :goto_0
    sget v1, LX/0PW;->b:I

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    .line 56648
    :cond_0
    sget-object v0, LX/0PW;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    goto :goto_0

    .line 56649
    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/Object;Z)I
    .locals 11

    .prologue
    .line 56612
    invoke-direct {p0}, LX/0PW;->d()V

    .line 56613
    invoke-static {}, LX/0PW;->h()I

    move-result v0

    .line 56614
    const-wide/16 v7, -0x1

    .line 56615
    if-eqz p3, :cond_0

    sget-object v3, LX/49P;->START_ASYNC:LX/49P;

    :goto_0
    move v4, v0

    move-object v5, p1

    move-object v6, p2

    move-wide v9, v7

    invoke-static/range {v3 .. v10}, LX/0eu;->a(LX/49P;ILjava/lang/String;[Ljava/lang/Object;JJ)LX/0eu;

    move-result-object v3

    move-object v1, v3

    .line 56616
    invoke-static {p0, v1}, LX/0PW;->a(LX/0PW;LX/0eu;)V

    .line 56617
    iget-object v2, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 56618
    iget v0, v1, LX/0eu;->b:I

    move v0, v0

    .line 56619
    return v0

    :cond_0
    sget-object v3, LX/49P;->START:LX/49P;

    goto :goto_0
.end method

.method public final a(IJZ)J
    .locals 10

    .prologue
    .line 56620
    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v4

    .line 56621
    if-nez p4, :cond_0

    .line 56622
    const-wide/32 v0, 0x2dc6c0

    move-wide v2, v0

    .line 56623
    :goto_0
    iget-object v0, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    .line 56624
    if-gez v1, :cond_1

    .line 56625
    const-wide/16 v0, -0x1

    .line 56626
    :goto_1
    return-wide v0

    .line 56627
    :cond_0
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p2

    move-wide v2, v0

    goto :goto_0

    .line 56628
    :cond_1
    iget-object v0, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eu;

    .line 56629
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_3

    .line 56630
    iget-object v6, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 56631
    :goto_2
    iget-wide v8, v0, LX/0eu;->e:J

    move-wide v6, v8

    .line 56632
    sub-long/2addr v4, v6

    .line 56633
    cmp-long v1, v4, v2

    if-gez v1, :cond_5

    .line 56634
    iget v1, p0, LX/0PW;->g:I

    .line 56635
    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-ltz v1, :cond_2

    .line 56636
    iget-object v2, p0, LX/0PW;->f:[LX/0eu;

    aget-object v2, v2, v1

    .line 56637
    if-ne v2, v0, :cond_4

    .line 56638
    iget-object v2, p0, LX/0PW;->f:[LX/0eu;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 56639
    iget v1, p0, LX/0PW;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0PW;->h:I

    .line 56640
    invoke-virtual {v0}, LX/0eu;->a()V

    :cond_2
    :goto_4
    move-wide v0, v4

    .line 56641
    goto :goto_1

    .line 56642
    :cond_3
    iget-object v1, p0, LX/0PW;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_2

    .line 56643
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 56644
    :cond_5
    invoke-static {v0}, LX/0eu;->a(LX/0eu;)LX/0eu;

    move-result-object v0

    .line 56645
    invoke-static {p0, v0}, LX/0PW;->a(LX/0PW;LX/0eu;)V

    goto :goto_4
.end method
