.class public final LX/0iN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iO;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/FbMainTabActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V
    .locals 0

    .prologue
    .line 120779
    iput-object p1, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 120804
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->h()V

    .line 120805
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 120799
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->f()LX/0fK;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120800
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->f()LX/0fK;

    move-result-object v0

    invoke-virtual {v0}, LX/0fK;->f()V

    .line 120801
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 120802
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-static {v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->U(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    .line 120803
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 120795
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-boolean v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aL:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 120796
    iget-boolean p0, v0, LX/0fM;->a:Z

    move p0, p0

    .line 120797
    if-nez p0, :cond_2

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 120798
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120780
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    if-eqz v0, :cond_0

    .line 120781
    sget-object v0, LX/ArJ;->TAP_PR_CTA_IN_FEED:LX/ArJ;

    .line 120782
    iget-object v2, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/FbMainTabActivity;->aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 120783
    iget-object v3, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    .line 120784
    iput-object v1, v3, Lcom/facebook/katana/activity/FbMainTabActivity;->aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 120785
    move-object v1, v2

    .line 120786
    :goto_0
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->newBuilder()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->setCTAConfig(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->setStartReason(LX/ArJ;)Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->a()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    move-result-object v0

    return-object v0

    .line 120787
    :cond_0
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 120788
    iget-boolean v2, v0, LX/0fd;->h:Z

    move v0, v2

    .line 120789
    if-eqz v0, :cond_1

    .line 120790
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    const/4 v2, 0x0

    .line 120791
    iput-boolean v2, v0, LX/0fd;->h:Z

    .line 120792
    sget-object v0, LX/ArJ;->TAP_CAMERA_BUTTON_IN_FEED:LX/ArJ;

    goto :goto_0

    .line 120793
    :cond_1
    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aN:LX/0gs;

    sget-object v2, LX/0gs;->ANIMATING:LX/0gs;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, LX/0iN;->a:Lcom/facebook/katana/activity/FbMainTabActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/FbMainTabActivity;->aN:LX/0gs;

    sget-object v2, LX/0gs;->OPENED:LX/0gs;

    if-ne v0, v2, :cond_3

    .line 120794
    :cond_2
    sget-object v0, LX/ArJ;->SWIPE_TO_CAMERA:LX/ArJ;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
