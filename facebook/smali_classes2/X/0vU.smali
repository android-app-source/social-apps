.class public final enum LX/0vU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0vU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0vU;

.field public static final enum BASIC:LX/0vU;

.field public static final enum PHASED:LX/0vU;

.field public static final enum PROFILING:LX/0vU;

.field public static final enum UNSPECIFIED:LX/0vU;


# instance fields
.field public schedulerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 157900
    new-instance v0, LX/0vU;

    const-string v1, "UNSPECIFIED"

    const-string v2, "unspecified"

    invoke-direct {v0, v1, v3, v2}, LX/0vU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0vU;->UNSPECIFIED:LX/0vU;

    .line 157901
    new-instance v0, LX/0vU;

    const-string v1, "BASIC"

    const-string v2, "basic"

    invoke-direct {v0, v1, v4, v2}, LX/0vU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0vU;->BASIC:LX/0vU;

    .line 157902
    new-instance v0, LX/0vU;

    const-string v1, "PHASED"

    const-string v2, "phased"

    invoke-direct {v0, v1, v5, v2}, LX/0vU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0vU;->PHASED:LX/0vU;

    .line 157903
    new-instance v0, LX/0vU;

    const-string v1, "PROFILING"

    const-string v2, "profiling"

    invoke-direct {v0, v1, v6, v2}, LX/0vU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0vU;->PROFILING:LX/0vU;

    .line 157904
    const/4 v0, 0x4

    new-array v0, v0, [LX/0vU;

    sget-object v1, LX/0vU;->UNSPECIFIED:LX/0vU;

    aput-object v1, v0, v3

    sget-object v1, LX/0vU;->BASIC:LX/0vU;

    aput-object v1, v0, v4

    sget-object v1, LX/0vU;->PHASED:LX/0vU;

    aput-object v1, v0, v5

    sget-object v1, LX/0vU;->PROFILING:LX/0vU;

    aput-object v1, v0, v6

    sput-object v0, LX/0vU;->$VALUES:[LX/0vU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157905
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 157906
    iput-object p3, p0, LX/0vU;->schedulerName:Ljava/lang/String;

    .line 157907
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0vU;
    .locals 1

    .prologue
    .line 157908
    const-class v0, LX/0vU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0vU;

    return-object v0
.end method

.method public static values()[LX/0vU;
    .locals 1

    .prologue
    .line 157909
    sget-object v0, LX/0vU;->$VALUES:[LX/0vU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0vU;

    return-object v0
.end method
