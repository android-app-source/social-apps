.class public final LX/0lP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lQ;


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129339
    iput-object p1, p0, LX/0lP;->a:Ljava/util/HashMap;

    .line 129340
    return-void
.end method

.method public static a(LX/0lP;LX/0lP;)LX/0lP;
    .locals 4

    .prologue
    .line 129329
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object p0, p1

    .line 129330
    :cond_1
    :goto_0
    return-object p0

    .line 129331
    :cond_2
    if-eqz p1, :cond_1

    iget-object v0, p1, LX/0lP;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 129332
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 129333
    iget-object v0, p1, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    .line 129334
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 129335
    :cond_3
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    .line 129336
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 129337
    :cond_4
    new-instance p0, LX/0lP;

    invoke-direct {p0, v1}, LX/0lP;-><init>(Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method public static c(LX/0lP;Ljava/lang/annotation/Annotation;)V
    .locals 2

    .prologue
    .line 129325
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 129326
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    .line 129327
    :cond_0
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129328
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 129324
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 129315
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 129316
    const/4 v0, 0x0

    .line 129317
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    goto :goto_0
.end method

.method public final a(Ljava/lang/annotation/Annotation;)V
    .locals 2

    .prologue
    .line 129321
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 129322
    :cond_0
    invoke-static {p0, p1}, LX/0lP;->c(LX/0lP;Ljava/lang/annotation/Annotation;)V

    .line 129323
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129318
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 129319
    const-string v0, "[null]"

    .line 129320
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lP;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
