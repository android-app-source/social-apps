.class public LX/0yj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

.field private final c:LX/0SG;

.field private final d:LX/0lp;

.field public e:[Ljava/lang/String;

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0yo;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[D>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166055
    const-class v0, LX/0yj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0yj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;LX/0SG;LX/0lp;)V
    .locals 0
    .param p1    # Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166051
    iput-object p1, p0, LX/0yj;->b:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    .line 166052
    iput-object p2, p0, LX/0yj;->c:LX/0SG;

    .line 166053
    iput-object p3, p0, LX/0yj;->d:LX/0lp;

    .line 166054
    return-void
.end method

.method public static a(LX/0yj;[DLX/14s;LX/0y8;)V
    .locals 12

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 166056
    iget-object v0, p0, LX/0yj;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166057
    invoke-static {v0}, LX/0yo;->getEnum(Ljava/lang/String;)LX/0yo;

    move-result-object v1

    .line 166058
    iget-object v0, p0, LX/0yj;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 166059
    sget-object v0, LX/2tW;->a:[I

    invoke-virtual {v1}, LX/0yo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 166060
    :pswitch_0
    iget v0, p2, LX/14s;->mSeenState:I

    move v0, v0

    .line 166061
    if-nez v0, :cond_0

    move-wide v0, v2

    :goto_1
    aput-wide v0, p1, v7

    goto :goto_0

    :cond_0
    move-wide v0, v4

    goto :goto_1

    .line 166062
    :pswitch_1
    iget-object v0, p0, LX/0yj;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 166063
    iget-wide v10, p2, LX/14s;->mFetchedAt:J

    move-wide v8, v10

    .line 166064
    sub-long/2addr v0, v8

    .line 166065
    long-to-double v0, v0

    aput-wide v0, p1, v7

    goto :goto_0

    .line 166066
    :pswitch_2
    iget v0, p2, LX/14s;->mImageCacheState:I

    move v0, v0

    .line 166067
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto :goto_0

    .line 166068
    :pswitch_3
    iget v0, p2, LX/14s;->mLiveVideoState:I

    move v0, v0

    .line 166069
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move-wide v0, v4

    :goto_2
    aput-wide v0, p1, v7

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_2

    .line 166070
    :pswitch_4
    invoke-virtual {p3}, LX/0y8;->a()LX/0p3;

    move-result-object v0

    invoke-virtual {v0}, LX/0p3;->ordinal()I

    move-result v0

    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto :goto_0

    .line 166071
    :pswitch_5
    invoke-virtual {p3}, LX/0y8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move-wide v0, v2

    :goto_3
    aput-wide v0, p1, v7

    goto :goto_0

    :cond_2
    move-wide v0, v4

    goto :goto_3

    .line 166072
    :pswitch_6
    invoke-virtual {p3}, LX/0y8;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    move-wide v0, v4

    :goto_4
    aput-wide v0, p1, v7

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_4

    .line 166073
    :pswitch_7
    iget v0, p2, LX/14s;->mVideoCacheState:I

    move v0, v0

    .line 166074
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto :goto_0

    .line 166075
    :pswitch_8
    iget-boolean v0, p2, LX/14s;->mStoryHasVideo:Z

    move v0, v0

    .line 166076
    if-eqz v0, :cond_4

    move-wide v0, v4

    :goto_5
    aput-wide v0, p1, v7

    goto/16 :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_5

    .line 166077
    :pswitch_9
    invoke-virtual {p3}, LX/0y8;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    move-wide v0, v4

    :goto_6
    aput-wide v0, p1, v7

    goto/16 :goto_0

    :cond_5
    move-wide v0, v2

    goto :goto_6

    .line 166078
    :pswitch_a
    iget-boolean v0, p2, LX/14s;->mStoryHasDownloadedVideo:Z

    move v0, v0

    .line 166079
    if-eqz v0, :cond_6

    move-wide v0, v4

    :goto_7
    aput-wide v0, p1, v7

    goto/16 :goto_0

    :cond_6
    move-wide v0, v2

    goto :goto_7

    .line 166080
    :pswitch_b
    iget v0, p2, LX/14s;->mImagesLoaded:I

    move v0, v0

    .line 166081
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166082
    :pswitch_c
    iget v0, p2, LX/14s;->mImagesExpected:I

    move v0, v0

    .line 166083
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166084
    :pswitch_d
    invoke-virtual {p3}, LX/0y8;->e()I

    move-result v0

    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166085
    :pswitch_e
    iget-boolean v0, p2, LX/14s;->mIsAttachmentTextLoaded:Z

    move v0, v0

    .line 166086
    if-eqz v0, :cond_7

    move-wide v0, v4

    :goto_8
    aput-wide v0, p1, v7

    goto/16 :goto_0

    :cond_7
    move-wide v0, v2

    goto :goto_8

    .line 166087
    :pswitch_f
    iget-boolean v0, p2, LX/14s;->mHasAttachmentText:Z

    move v0, v0

    .line 166088
    if-eqz v0, :cond_8

    move-wide v0, v4

    :goto_9
    aput-wide v0, p1, v7

    goto/16 :goto_0

    :cond_8
    move-wide v0, v2

    goto :goto_9

    .line 166089
    :pswitch_10
    iget v0, p2, LX/14s;->mAttachmentMediaCacheState:I

    move v0, v0

    .line 166090
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166091
    :pswitch_11
    iget v0, p2, LX/14s;->mAttachmentMediaExpected:I

    move v0, v0

    .line 166092
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166093
    :pswitch_12
    iget v0, p2, LX/14s;->mAttachmentMediaLoaded:I

    move v0, v0

    .line 166094
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166095
    :pswitch_13
    iget v0, p2, LX/14s;->mLinkCacheState:I

    move v0, v0

    .line 166096
    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166097
    :pswitch_14
    invoke-virtual {p3}, LX/0y8;->f()I

    move-result v0

    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166098
    :pswitch_15
    invoke-virtual {p3}, LX/0y8;->g()I

    move-result v0

    int-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166099
    :pswitch_16
    iget-wide v10, p2, LX/14s;->mLiveCommentAgeMs:J

    move-wide v0, v10

    .line 166100
    long-to-double v0, v0

    aput-wide v0, p1, v7

    goto/16 :goto_0

    .line 166101
    :cond_9
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public static a(LX/0yj;[DLjava/lang/String;)V
    .locals 12

    .prologue
    .line 166029
    iget-object v0, p0, LX/0yj;->d:LX/0lp;

    .line 166030
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 166031
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 166032
    :goto_0
    move-object v2, v6

    .line 166033
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166034
    iget-object v1, p0, LX/0yj;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166035
    iget-object v1, p0, LX/0yj;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    aput-wide v4, p1, v1

    goto :goto_1

    .line 166036
    :cond_1
    return-void

    .line 166037
    :cond_2
    :try_start_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 166038
    invoke-virtual {v0, p2}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v7

    .line 166039
    invoke-virtual {v7}, LX/15w;->c()LX/15z;

    .line 166040
    invoke-virtual {v7}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-ne v8, v9, :cond_3

    .line 166041
    :goto_2
    invoke-virtual {v7}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 166042
    invoke-virtual {v7}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 166043
    invoke-virtual {v7}, LX/15w;->c()LX/15z;

    .line 166044
    invoke-virtual {v7}, LX/15w;->G()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    .line 166045
    invoke-interface {v6, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 166046
    :catch_0
    move-exception v6

    .line 166047
    sget-object v7, LX/0yj;->a:Ljava/lang/String;

    const-string v8, "Failed to parse json"

    invoke-static {v7, v8, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 166048
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    goto :goto_0

    .line 166049
    :cond_3
    :try_start_1
    invoke-virtual {v7}, LX/15w;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
