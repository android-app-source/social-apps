.class public LX/1kY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1kG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1kH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1kD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1kZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/1kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 309948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309949
    return-void
.end method

.method private static a(LX/1kY;LX/1vV;)LX/1lR;
    .locals 3

    .prologue
    .line 310023
    iget-object v0, p0, LX/1kY;->d:LX/1kD;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/1vV;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1kD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310024
    sget-object v0, LX/1lR;->a:LX/1lR;

    return-object v0
.end method

.method private static a(LX/1kY;I)Z
    .locals 7

    .prologue
    .line 310011
    iget-object v0, p0, LX/1kY;->e:LX/1kZ;

    .line 310012
    iget-object v1, v0, LX/1kZ;->h:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 310013
    iget-object v1, v0, LX/1kZ;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/1vU;->f:I

    const/4 v5, 0x3

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/1kZ;->h:Ljava/lang/Integer;

    .line 310014
    :cond_0
    iget-object v1, v0, LX/1kZ;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v1, v1

    .line 310015
    if-lt p1, v1, :cond_1

    const/4 v0, 0x1

    .line 310016
    :goto_0
    iget-object v2, p0, LX/1kY;->g:LX/1kb;

    .line 310017
    if-eqz v0, :cond_2

    .line 310018
    :goto_1
    return v0

    .line 310019
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 310020
    :cond_2
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "num_new_photos"

    invoke-virtual {v3, v4, p1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v3

    const-string v4, "num_new_photos_required"

    invoke-virtual {v3, v4, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v3

    .line 310021
    iget-object v4, v2, LX/1kb;->a:LX/0if;

    sget-object v5, LX/0ig;->T:LX/0ih;

    const-string v6, "not_enough_new_photos"

    const/4 p0, 0x0

    invoke-virtual {v4, v5, v6, p0, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 310022
    invoke-static {v2}, LX/1kb;->d(LX/1kb;)V

    goto :goto_1
.end method

.method private static a(LX/1kY;Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 309998
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 309999
    :goto_0
    iget-object v2, p0, LX/1kY;->e:LX/1kZ;

    .line 310000
    iget-object v3, v2, LX/1kZ;->i:Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 310001
    iget-object v3, v2, LX/1kZ;->a:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v6, LX/1vU;->g:I

    const/4 p1, 0x3

    invoke-interface {v3, v4, v5, v6, p1}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, LX/1kZ;->i:Ljava/lang/Integer;

    .line 310002
    :cond_0
    iget-object v3, v2, LX/1kZ;->i:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v2, v3

    .line 310003
    if-lt v0, v2, :cond_1

    const/4 v1, 0x1

    .line 310004
    :cond_1
    iget-object v3, p0, LX/1kY;->g:LX/1kb;

    .line 310005
    if-eqz v1, :cond_3

    .line 310006
    :goto_1
    return v1

    :cond_2
    move v0, v1

    .line 310007
    goto :goto_0

    .line 310008
    :cond_3
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string v5, "num_photo_media_models"

    invoke-virtual {v4, v5, v0}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    const-string v5, "num_photo_media_models_required"

    invoke-virtual {v4, v5, v2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    .line 310009
    iget-object v5, v3, LX/1kb;->a:LX/0if;

    sget-object v6, LX/0ig;->T:LX/0ih;

    const-string p0, "not_enough_photo_media_models"

    const/4 p1, 0x0

    invoke-virtual {v5, v6, p0, p1, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 310010
    invoke-static {v3}, LX/1kb;->d(LX/1kb;)V

    goto :goto_1
.end method

.method public static d(LX/1kY;)J
    .locals 12

    .prologue
    .line 310025
    iget-object v0, p0, LX/1kY;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/1kY;->e:LX/1kZ;

    .line 310026
    iget-object v4, v2, LX/1kZ;->k:Ljava/lang/Long;

    if-nez v4, :cond_0

    .line 310027
    iget-object v5, v2, LX/1kZ;->a:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget-wide v8, LX/1vU;->h:J

    const-wide/32 v10, 0x5265c00

    invoke-interface/range {v5 .. v11}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v2, LX/1kZ;->k:Ljava/lang/Long;

    .line 310028
    :cond_0
    iget-object v4, v2, LX/1kZ;->k:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide v2, v4

    .line 310029
    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private static e(LX/1kY;)I
    .locals 14

    .prologue
    .line 309993
    iget-object v0, p0, LX/1kY;->b:LX/1kG;

    iget-object v1, p0, LX/1kY;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/1kY;->e:LX/1kZ;

    .line 309994
    iget-object v6, v1, LX/1kZ;->l:Ljava/lang/Long;

    if-nez v6, :cond_0

    .line 309995
    iget-object v7, v1, LX/1kZ;->a:LX/0ad;

    sget-object v8, LX/0c0;->Cached:LX/0c0;

    sget-object v9, LX/0c1;->Off:LX/0c1;

    sget-wide v10, LX/1vU;->e:J

    const-wide/32 v12, 0x5265c00

    invoke-interface/range {v7 .. v13}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v1, LX/1kZ;->l:Ljava/lang/Long;

    .line 309996
    :cond_0
    iget-object v6, v1, LX/1kZ;->l:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-wide v4, v6

    .line 309997
    sub-long/2addr v2, v4

    sget-object v1, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v0, v2, v3, v1}, LX/1kG;->a(JLX/1po;)I

    move-result v0

    return v0
.end method

.method private static f(LX/1kY;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309988
    iget-object v0, p0, LX/1kY;->c:LX/1kH;

    const/4 v1, 0x1

    iget-object v2, p0, LX/1kY;->e:LX/1kZ;

    .line 309989
    iget-object v3, v2, LX/1kZ;->j:Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 309990
    iget-object v3, v2, LX/1kZ;->a:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v6, LX/1vU;->d:I

    const/16 p0, 0x1e

    invoke-interface {v3, v4, v5, v6, p0}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, LX/1kZ;->j:Ljava/lang/Integer;

    .line 309991
    :cond_0
    iget-object v3, v2, LX/1kZ;->j:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v2, v3

    .line 309992
    sget-object v3, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v0, v1, v2, v3}, LX/1kH;->a(ZILX/1po;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1lR;
    .locals 14

    .prologue
    .line 309950
    iget-object v0, p0, LX/1kY;->g:LX/1kb;

    .line 309951
    iget-object v1, v0, LX/1kb;->a:LX/0if;

    sget-object v2, LX/0ig;->T:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 309952
    iget-object v10, p0, LX/1kY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/1kp;->j:LX/0Tn;

    const-wide/16 v12, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    move-wide v5, v10

    .line 309953
    invoke-static {p0}, LX/1kY;->d(LX/1kY;)J

    move-result-wide v7

    .line 309954
    cmp-long v3, v5, v7

    if-lez v3, :cond_6

    const/4 v9, 0x1

    .line 309955
    :goto_0
    iget-object v4, p0, LX/1kY;->g:LX/1kb;

    .line 309956
    if-nez v9, :cond_7

    .line 309957
    :goto_1
    move v0, v9

    .line 309958
    if-eqz v0, :cond_0

    .line 309959
    sget-object v0, LX/1vV;->DISMISSED_RECENTLY:LX/1vV;

    invoke-static {p0, v0}, LX/1kY;->a(LX/1kY;LX/1vV;)LX/1lR;

    move-result-object v0

    .line 309960
    :goto_2
    return-object v0

    .line 309961
    :cond_0
    invoke-static {p0}, LX/1kY;->e(LX/1kY;)I

    move-result v1

    .line 309962
    invoke-static {p0, v1}, LX/1kY;->a(LX/1kY;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 309963
    sget-object v0, LX/1vV;->NOT_ENOUGH_NEW_PHOTOS:LX/1vV;

    invoke-static {p0, v0}, LX/1kY;->a(LX/1kY;LX/1vV;)LX/1lR;

    move-result-object v0

    goto :goto_2

    .line 309964
    :cond_1
    invoke-static {p0}, LX/1kY;->f(LX/1kY;)Ljava/util/List;

    move-result-object v2

    .line 309965
    invoke-static {p0, v2}, LX/1kY;->a(LX/1kY;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 309966
    sget-object v0, LX/1vV;->NOT_ENOUGH_PHOTOS:LX/1vV;

    invoke-static {p0, v0}, LX/1kY;->a(LX/1kY;LX/1vV;)LX/1lR;

    move-result-object v0

    goto :goto_2

    .line 309967
    :cond_2
    iget-object v0, p0, LX/1kY;->e:LX/1kZ;

    const/4 v3, 0x0

    .line 309968
    iget-object v4, v0, LX/1kZ;->e:Ljava/lang/Boolean;

    if-nez v4, :cond_4

    .line 309969
    iget-object v4, v0, LX/1kZ;->b:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, LX/1kZ;->a:LX/0ad;

    sget-short v5, LX/1vU;->b:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v3, 0x1

    :cond_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, LX/1kZ;->e:Ljava/lang/Boolean;

    .line 309970
    :cond_4
    iget-object v3, v0, LX/1kZ;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v0, v3

    .line 309971
    if-nez v0, :cond_5

    .line 309972
    iget-object v0, p0, LX/1kY;->g:LX/1kb;

    .line 309973
    iget-object v1, v0, LX/1kb;->a:LX/0if;

    sget-object v2, LX/0ig;->T:LX/0ih;

    const-string v3, "not_eligible_for_prompt_qe_param"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 309974
    invoke-static {v0}, LX/1kb;->d(LX/1kb;)V

    .line 309975
    sget-object v0, LX/1vV;->NOT_ELIGIBLE_FOR_PROMPT:LX/1vV;

    invoke-static {p0, v0}, LX/1kY;->a(LX/1kY;LX/1vV;)LX/1lR;

    move-result-object v0

    goto :goto_2

    .line 309976
    :cond_5
    new-instance v0, LX/1lR;

    invoke-direct {v0, v2}, LX/1lR;-><init>(Ljava/util/List;)V

    .line 309977
    iput v1, v0, LX/1lR;->f:I

    .line 309978
    const/4 v1, 0x1

    .line 309979
    iput-boolean v1, v0, LX/1lR;->l:Z

    .line 309980
    iget-object v1, p0, LX/1kY;->g:LX/1kb;

    .line 309981
    iget-object v2, v1, LX/1kb;->a:LX/0if;

    sget-object v3, LX/0ig;->T:LX/0ih;

    const-string v4, "eligible_for_ranking"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 309982
    invoke-static {v1}, LX/1kb;->d(LX/1kb;)V

    .line 309983
    goto :goto_2

    .line 309984
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 309985
    :cond_7
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v10, "last_dismissed_time_ms"

    invoke-virtual {v3, v10, v5, v6}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v3

    const-string v10, "last_dismissed_time_allowed_ms"

    invoke-virtual {v3, v10, v7, v8}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v3

    .line 309986
    iget-object v10, v4, LX/1kb;->a:LX/0if;

    sget-object v11, LX/0ig;->T:LX/0ih;

    const-string v12, "recently_dismissed_prompt"

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 309987
    invoke-static {v4}, LX/1kb;->d(LX/1kb;)V

    goto/16 :goto_1
.end method
