.class public LX/11M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/11M;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4dA;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0lC;

.field public c:LX/0Xl;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lC;LX/0Xl;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172001
    iput-object p1, p0, LX/11M;->b:LX/0lC;

    .line 172002
    iput-object p2, p0, LX/11M;->c:LX/0Xl;

    .line 172003
    iput-object p3, p0, LX/11M;->d:LX/0Or;

    .line 172004
    return-void
.end method

.method public static a(LX/0QB;)LX/11M;
    .locals 6

    .prologue
    .line 171985
    sget-object v0, LX/11M;->e:LX/11M;

    if-nez v0, :cond_1

    .line 171986
    const-class v1, LX/11M;

    monitor-enter v1

    .line 171987
    :try_start_0
    sget-object v0, LX/11M;->e:LX/11M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 171988
    if-eqz v2, :cond_0

    .line 171989
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 171990
    new-instance v5, LX/11M;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/11M;-><init>(LX/0lC;LX/0Xl;LX/0Or;)V

    .line 171991
    const/16 v3, 0x2518

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 171992
    iput-object v3, v5, LX/11M;->a:LX/0Or;

    .line 171993
    move-object v0, v5

    .line 171994
    sput-object v0, LX/11M;->e:LX/11M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171995
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 171996
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 171997
    :cond_1
    sget-object v0, LX/11M;->e:LX/11M;

    return-object v0

    .line 171998
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 171999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/11M;)V
    .locals 3

    .prologue
    .line 171983
    iget-object v0, p0, LX/11M;->c:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.facebook.http.protocol.CHECKPOINT_API_EXCEPTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 171984
    return-void
.end method

.method public static a(LX/11M;LX/0lF;LX/0lC;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 172005
    if-nez p1, :cond_1

    .line 172006
    :cond_0
    return-void

    .line 172007
    :cond_1
    invoke-virtual {p1}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 172008
    const-string v0, "error_code"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172009
    const-string v0, "error_msg"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 172010
    const-string v1, "error_data"

    invoke-virtual {p1, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 172011
    if-eqz v1, :cond_2

    .line 172012
    :try_start_0
    invoke-virtual {p2, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 172013
    const-string v3, "error_message"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 172014
    const-string v3, "error_message"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 172015
    :cond_2
    :goto_0
    new-instance v4, LX/2Oo;

    const-string v2, "error_code"

    invoke-virtual {p1, v2}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    invoke-static {v2, v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v0

    .line 172016
    iput-object v1, v0, LX/2AV;->d:Ljava/lang/String;

    .line 172017
    move-object v0, v0

    .line 172018
    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172019
    iput-object v1, v0, LX/2AV;->g:Ljava/lang/String;

    .line 172020
    move-object v0, v0

    .line 172021
    sget-object v1, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    invoke-virtual {v0, v1}, LX/2AV;->a(LX/2Aa;)LX/2AV;

    move-result-object v0

    const-string v1, "is_transient"

    invoke-virtual {p1, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0lF;->a(Z)Z

    move-result v1

    .line 172022
    iput-boolean v1, v0, LX/2AV;->i:Z

    .line 172023
    move-object v0, v0

    .line 172024
    invoke-virtual {v0}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-direct {v4, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    .line 172025
    :cond_3
    :goto_1
    if-eqz v4, :cond_0

    .line 172026
    throw v4

    .line 172027
    :cond_4
    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 172028
    new-instance v4, LX/2Oo;

    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    const-string v1, "error_description"

    invoke-virtual {p1, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v0

    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172029
    iput-object v1, v0, LX/2AV;->g:Ljava/lang/String;

    .line 172030
    move-object v0, v0

    .line 172031
    sget-object v1, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    invoke-virtual {v0, v1}, LX/2AV;->a(LX/2Aa;)LX/2AV;

    move-result-object v0

    const-string v1, "is_transient"

    invoke-virtual {p1, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0lF;->a(Z)Z

    move-result v1

    .line 172032
    iput-boolean v1, v0, LX/2AV;->i:Z

    .line 172033
    move-object v0, v0

    .line 172034
    invoke-virtual {v0}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-direct {v4, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto :goto_1

    .line 172035
    :cond_5
    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 172036
    const-string v0, "error"

    invoke-virtual {p1, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 172037
    const-string v0, "code"

    invoke-virtual {v7, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "description"

    invoke-virtual {v7, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 172038
    const-string v0, "code"

    invoke-virtual {v7, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 172039
    const-string v1, "description"

    invoke-virtual {v7, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v1

    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172040
    iput-object v2, v1, LX/2AV;->g:Ljava/lang/String;

    .line 172041
    move-object v1, v1

    .line 172042
    sget-object v2, LX/2Aa;->GRAPHQL_KERROR_DOMAIN:LX/2Aa;

    invoke-virtual {v1, v2}, LX/2AV;->a(LX/2Aa;)LX/2AV;

    move-result-object v1

    const-string v2, "is_transient"

    invoke-virtual {v7, v2}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0lF;->a(Z)Z

    move-result v2

    .line 172043
    iput-boolean v2, v1, LX/2AV;->i:Z

    .line 172044
    move-object v1, v1

    .line 172045
    invoke-virtual {v1}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 172046
    sparse-switch v0, :sswitch_data_0

    .line 172047
    new-instance v0, LX/2Oo;

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    :goto_2
    move-object v4, v0

    .line 172048
    goto/16 :goto_1

    .line 172049
    :cond_6
    const-string v0, "message"

    invoke-virtual {v7, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 172050
    const-string v0, "code"

    invoke-virtual {v7, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "code"

    invoke-virtual {v7, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 172051
    :goto_3
    const-string v2, "error_subcode"

    invoke-virtual {v7, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "error_subcode"

    invoke-virtual {v7, v2}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    .line 172052
    :goto_4
    const-string v3, "error_user_title"

    invoke-virtual {v7, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "error_user_title"

    invoke-virtual {v7, v3}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 172053
    :goto_5
    const-string v6, "error_user_msg"

    invoke-virtual {v7, v6}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "error_user_msg"

    invoke-virtual {v7, v6}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 172054
    :goto_6
    const/16 v8, 0xbe

    if-ne v0, v8, :cond_7

    .line 172055
    packed-switch v2, :pswitch_data_0

    .line 172056
    :goto_7
    if-nez v1, :cond_7

    .line 172057
    iget-object v8, p0, LX/11M;->d:LX/0Or;

    if-eqz v8, :cond_7

    iget-object v8, p0, LX/11M;->d:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_10

    .line 172058
    :cond_7
    :goto_8
    if-nez v1, :cond_3

    .line 172059
    new-instance v4, LX/2Oo;

    const-string v1, "message"

    invoke-virtual {v7, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v0

    .line 172060
    iput v2, v0, LX/2AV;->b:I

    .line 172061
    move-object v0, v0

    .line 172062
    const-string v1, "error_data"

    invoke-virtual {v7, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172063
    iput-object v1, v0, LX/2AV;->d:Ljava/lang/String;

    .line 172064
    move-object v0, v0

    .line 172065
    iput-object v3, v0, LX/2AV;->e:Ljava/lang/String;

    .line 172066
    move-object v0, v0

    .line 172067
    iput-object v6, v0, LX/2AV;->f:Ljava/lang/String;

    .line 172068
    move-object v0, v0

    .line 172069
    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172070
    iput-object v1, v0, LX/2AV;->g:Ljava/lang/String;

    .line 172071
    move-object v0, v0

    .line 172072
    sget-object v1, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    invoke-virtual {v0, v1}, LX/2AV;->a(LX/2Aa;)LX/2AV;

    move-result-object v0

    const-string v1, "is_transient"

    invoke-virtual {v7, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0lF;->a(Z)Z

    move-result v1

    .line 172073
    iput-boolean v1, v0, LX/2AV;->i:Z

    .line 172074
    move-object v0, v0

    .line 172075
    invoke-virtual {v0}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-direct {v4, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 172076
    goto/16 :goto_3

    :cond_9
    move v2, v1

    .line 172077
    goto/16 :goto_4

    :cond_a
    move-object v3, v4

    .line 172078
    goto :goto_5

    :cond_b
    move-object v6, v4

    .line 172079
    goto :goto_6

    .line 172080
    :pswitch_0
    invoke-static {p0}, LX/11M;->a(LX/11M;)V

    move v1, v5

    .line 172081
    goto :goto_7

    .line 172082
    :pswitch_1
    invoke-static {p0}, LX/11M;->c(LX/11M;)V

    goto :goto_7

    .line 172083
    :cond_c
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172084
    new-instance v0, LX/28E;

    const-string v1, "Response was neither an array or a dictionary"

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    goto/16 :goto_0

    .line 172085
    :sswitch_0
    const-string v0, "error_subcode"

    invoke-virtual {v7, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "error_subcode"

    invoke-virtual {v7, v0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 172086
    :goto_9
    const/16 v2, 0x1ea

    if-ne v0, v2, :cond_e

    .line 172087
    invoke-static {p0}, LX/11M;->a(LX/11M;)V

    .line 172088
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 172089
    :cond_d
    const/4 v0, 0x0

    goto :goto_9

    .line 172090
    :cond_e
    const/16 v2, 0x1eb

    if-ne v0, v2, :cond_f

    .line 172091
    invoke-static {p0}, LX/11M;->c(LX/11M;)V

    .line 172092
    new-instance v0, LX/2Oo;

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_2

    .line 172093
    :cond_f
    new-instance v0, LX/4cy;

    invoke-direct {v0, v1}, LX/4cy;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_2

    .line 172094
    :sswitch_1
    new-instance v0, LX/4cy;

    invoke-direct {v0, v1}, LX/4cy;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_2

    .line 172095
    :sswitch_2
    new-instance v0, LX/4cz;

    invoke-direct {v0, v1}, LX/4cz;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_2

    .line 172096
    :sswitch_3
    new-instance v0, LX/4d0;

    invoke-direct {v0, v1}, LX/4d0;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    goto/16 :goto_2

    .line 172097
    :cond_10
    iget-object v9, p0, LX/11M;->c:LX/0Xl;

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    const-string p2, "arg_uid"

    iget-object v8, p0, LX/11M;->d:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/user/model/User;

    .line 172098
    iget-object p0, v8, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, p0

    .line 172099
    invoke-virtual {v10, p2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v10, "com.facebook.http.protocol.AUTH_TOKEN_EXCEPTION"

    invoke-virtual {v8, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    invoke-interface {v9, v8}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto/16 :goto_8

    nop

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_1
        0xbe -> :sswitch_0
        0x198eff -> :sswitch_2
        0x198f05 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1ea
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/11M;LX/1pF;)V
    .locals 4

    .prologue
    .line 171954
    iget v0, p1, LX/1pF;->a:I

    move v0, v0

    .line 171955
    const/16 v1, 0x12c

    if-lt v0, v1, :cond_3

    .line 171956
    iget-object v1, p1, LX/1pF;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 171957
    invoke-virtual {p1}, LX/1pF;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/1pF;->c:Ljava/lang/String;

    .line 171958
    :cond_0
    iget-object v1, p1, LX/1pF;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171959
    const/16 v2, 0x190

    if-lt v0, v2, :cond_1

    .line 171960
    invoke-virtual {p0, v1}, LX/11M;->a(Ljava/lang/String;)V

    .line 171961
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 171962
    iget-object v3, p1, LX/1pF;->b:Ljava/lang/String;

    move-object v3, v3

    .line 171963
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171964
    if-eqz v1, :cond_2

    .line 171965
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171966
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171967
    :cond_2
    new-instance v1, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v1

    .line 171968
    :cond_3
    return-void
.end method

.method public static c(LX/11M;)V
    .locals 1

    .prologue
    .line 171981
    iget-object v0, p0, LX/11M;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 171982
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171969
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171970
    :goto_0
    return-void

    .line 171971
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/11M;->b:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    iget-object v1, p0, LX/11M;->b:LX/0lC;

    invoke-static {p0, v0, v1}, LX/11M;->a(LX/11M;LX/0lF;LX/0lC;)V
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 171972
    :catch_0
    move-exception v0

    .line 171973
    throw v0

    .line 171974
    :catch_1
    goto :goto_0

    .line 171975
    :catch_2
    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpResponse;)V
    .locals 4

    .prologue
    .line 171976
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171977
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 171978
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 171979
    new-instance v2, LX/1pE;

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0, v1}, LX/1pE;-><init>(LX/11M;ILjava/lang/String;Lorg/apache/http/HttpEntity;)V

    invoke-static {p0, v2}, LX/11M;->a(LX/11M;LX/1pF;)V

    .line 171980
    return-void
.end method
