.class public final LX/0uo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/0uk;",
            "Ljava/util/ArrayList",
            "<",
            "LX/3Tq;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 157093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157094
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0uo;->a:Ljava/util/HashMap;

    .line 157095
    return-void
.end method

.method private static a(LX/0uo;LX/0uk;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0uk;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/3Tq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157074
    iget-object v0, p0, LX/0uo;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 157075
    if-nez v0, :cond_0

    .line 157076
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 157077
    iget-object v1, p0, LX/0uo;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157078
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized a$redex0(LX/0uo;LX/0uk;LX/3Tq;)V
    .locals 1

    .prologue
    .line 157090
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0uo;->a(LX/0uo;LX/0uk;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157091
    monitor-exit p0

    return-void

    .line 157092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/0uo;Ljava/util/Collection;LX/0uk;Lcom/google/common/util/concurrent/ListenableFuture;)LX/3Tq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0uk;",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ")",
            "LX/3Tq;"
        }
    .end annotation

    .prologue
    .line 157096
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3Tq;

    invoke-direct {v0, p1, p3}, LX/3Tq;-><init>(Ljava/util/Collection;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 157097
    invoke-static {p0, p2}, LX/0uo;->a(LX/0uo;LX/0uk;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157098
    monitor-exit p0

    return-object v0

    .line 157099
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;LX/0uk;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/0uk",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157083
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/0uo;->a(LX/0uo;LX/0uk;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157084
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tq;

    .line 157085
    iget-object v1, v0, LX/3Tq;->a:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 157086
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157087
    iget-object v0, v0, LX/3Tq;->b:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157088
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 157089
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/0uk;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 157079
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 157080
    invoke-static {p0, v0, p2, p3}, LX/0uo;->b(LX/0uo;Ljava/util/Collection;LX/0uk;Lcom/google/common/util/concurrent/ListenableFuture;)LX/3Tq;

    move-result-object v1

    .line 157081
    new-instance p1, LX/3Tr;

    invoke-direct {p1, p0, p2, v1}, LX/3Tr;-><init>(LX/0uo;LX/0uk;LX/3Tq;)V

    invoke-static {p3, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 157082
    return-void
.end method
