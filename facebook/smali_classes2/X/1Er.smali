.class public LX/1Er;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/1Er;


# instance fields
.field private final b:LX/1ES;

.field private final c:LX/1Es;

.field private final d:LX/1Es;

.field private final e:Ljava/lang/String;

.field private f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/1Es;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/1Es;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221358
    const-class v0, LX/1Er;

    sput-object v0, LX/1Er;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1ES;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 221301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221302
    new-instance v0, LX/1Es;

    .line 221303
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "fb_temp"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v1

    .line 221304
    invoke-direct {v0, v1}, LX/1Es;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/1Er;->c:LX/1Es;

    .line 221305
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Er;->f:LX/0am;

    .line 221306
    new-instance v0, LX/1Es;

    .line 221307
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "orcatemp"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v1

    .line 221308
    invoke-direct {v0, v1}, LX/1Es;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/1Er;->d:LX/1Es;

    .line 221309
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Er;->g:LX/0am;

    .line 221310
    iput-object p2, p0, LX/1Er;->b:LX/1ES;

    .line 221311
    iput-object p3, p0, LX/1Er;->e:Ljava/lang/String;

    .line 221312
    return-void
.end method

.method public static a(LX/0QB;)LX/1Er;
    .locals 6

    .prologue
    .line 221345
    sget-object v0, LX/1Er;->h:LX/1Er;

    if-nez v0, :cond_1

    .line 221346
    const-class v1, LX/1Er;

    monitor-enter v1

    .line 221347
    :try_start_0
    sget-object v0, LX/1Er;->h:LX/1Er;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221348
    if-eqz v2, :cond_0

    .line 221349
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221350
    new-instance p0, LX/1Er;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1ES;->a(LX/0QB;)LX/1ES;

    move-result-object v4

    check-cast v4, LX/1ES;

    invoke-static {v0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v3, v4, v5}, LX/1Er;-><init>(Landroid/content/Context;LX/1ES;Ljava/lang/String;)V

    .line 221351
    move-object v0, p0

    .line 221352
    sput-object v0, LX/1Er;->h:LX/1Er;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221353
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221354
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221355
    :cond_1
    sget-object v0, LX/1Er;->h:LX/1Er;

    return-object v0

    .line 221356
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221357
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Er;LX/46h;)LX/1Es;
    .locals 1

    .prologue
    .line 221335
    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    if-ne p1, v0, :cond_0

    .line 221336
    iget-object v0, p0, LX/1Er;->c:LX/1Es;

    .line 221337
    :goto_0
    return-object v0

    .line 221338
    :cond_0
    sget-object v0, LX/46h;->REQUIRE_SDCARD:LX/46h;

    if-ne p1, v0, :cond_1

    .line 221339
    invoke-direct {p0}, LX/1Er;->b()LX/1Es;

    move-result-object v0

    goto :goto_0

    .line 221340
    :cond_1
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    if-ne p1, v0, :cond_3

    .line 221341
    invoke-static {}, LX/1Er;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221342
    invoke-direct {p0}, LX/1Er;->b()LX/1Es;

    move-result-object v0

    goto :goto_0

    .line 221343
    :cond_2
    iget-object v0, p0, LX/1Er;->c:LX/1Es;

    goto :goto_0

    .line 221344
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method private b()LX/1Es;
    .locals 5

    .prologue
    .line 221332
    iget-object v0, p0, LX/1Er;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221333
    new-instance v0, LX/1Es;

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, LX/1Er;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "fb_temp"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/1Es;-><init>(Ljava/io/File;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Er;->f:LX/0am;

    .line 221334
    :cond_0
    iget-object v0, p0, LX/1Er;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Es;

    return-object v0
.end method

.method private static d()Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 221331
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 221329
    iget-object v0, p0, LX/1Er;->b:LX/1ES;

    const-class v1, Lcom/facebook/common/tempfile/TempFileDelayedWorker;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v4, 0x5265c00

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/1ES;->a(Ljava/lang/Class;J)V

    .line 221330
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;
    .locals 1

    .prologue
    .line 221326
    invoke-direct {p0}, LX/1Er;->e()V

    .line 221327
    invoke-static {p0, p3}, LX/1Er;->a(LX/1Er;LX/46h;)LX/1Es;

    move-result-object v0

    .line 221328
    invoke-virtual {v0, p1, p2}, LX/1Es;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/io/File;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 221323
    if-eqz p3, :cond_0

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    .line 221324
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    return-object v0

    .line 221325
    :cond_0
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0
.end method

.method public final a()V
    .locals 7

    .prologue
    const-wide/32 v2, 0x5265c00

    .line 221313
    invoke-direct {p0}, LX/1Er;->b()LX/1Es;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/1Es;->a(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 221314
    iget-object v1, p0, LX/1Er;->c:LX/1Es;

    invoke-virtual {v1, v2, v3}, LX/1Es;->a(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 221315
    iget-object v1, p0, LX/1Er;->d:LX/1Es;

    invoke-virtual {v1, v2, v3}, LX/1Es;->a(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 221316
    iget-object v1, p0, LX/1Er;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 221317
    new-instance v1, LX/1Es;

    new-instance v4, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    const-string v6, "orcatemp"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v4}, LX/1Es;-><init>(Ljava/io/File;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/1Er;->g:LX/0am;

    .line 221318
    :cond_0
    iget-object v1, p0, LX/1Er;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Es;

    move-object v1, v1

    .line 221319
    invoke-virtual {v1, v2, v3}, LX/1Es;->a(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 221320
    if-eqz v0, :cond_1

    .line 221321
    invoke-direct {p0}, LX/1Er;->e()V

    .line 221322
    :cond_1
    return-void
.end method
