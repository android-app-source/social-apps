.class public final LX/1VP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1VQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Enum;",
        ">",
        "Ljava/lang/Object;",
        "LX/1VQ",
        "<",
        "Ljava/lang/Class",
        "<TT;>;TT;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1VP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 260270
    new-instance v0, LX/1VP;

    invoke-direct {v0}, LX/1VP;-><init>()V

    sput-object v0, LX/1VP;->a:LX/1VP;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 260271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/nio/ByteBuffer;II)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260272
    check-cast p1, Ljava/lang/Class;

    .line 260273
    mul-int/lit8 v0, p4, 0x2

    add-int/2addr v0, p3

    .line 260274
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v1

    .line 260275
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 260276
    const-string v0, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-static {p1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    .line 260277
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    invoke-static {v0, p1}, LX/0ah;->a(SLjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0
.end method
