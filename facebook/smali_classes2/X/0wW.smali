.class public LX/0wW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0wd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0wd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0wY;

.field private final d:LX/0wZ;

.field private final e:LX/0SG;

.field private f:J

.field private g:LX/0wb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0wb",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>(LX/0SG;LX/0wY;LX/0wZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 160164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0wW;->a:Ljava/util/Map;

    .line 160166
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, LX/0P9;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/0wW;->b:Ljava/util/Set;

    .line 160167
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0wW;->f:J

    .line 160168
    new-instance v0, LX/0wb;

    invoke-direct {v0}, LX/0wb;-><init>()V

    iput-object v0, p0, LX/0wW;->g:LX/0wb;

    .line 160169
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0wW;->h:Z

    .line 160170
    iput-object p1, p0, LX/0wW;->e:LX/0SG;

    .line 160171
    iput-object p2, p0, LX/0wW;->c:LX/0wY;

    .line 160172
    iput-object p3, p0, LX/0wW;->d:LX/0wZ;

    .line 160173
    iget-object v0, p0, LX/0wW;->d:LX/0wZ;

    .line 160174
    iput-object p0, v0, LX/0wZ;->a:LX/0wW;

    .line 160175
    return-void
.end method

.method public static a(LX/0QB;)LX/0wW;
    .locals 1

    .prologue
    .line 160163
    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    return-object v0
.end method

.method private a(JJ)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 160107
    iget-object v0, p0, LX/0wW;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    .line 160108
    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160109
    iget-boolean v2, v0, LX/0wd;->j:Z

    move v2, v2

    .line 160110
    if-nez v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 160111
    if-eqz v2, :cond_1

    .line 160112
    long-to-double v2, p3

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0wd;->f(D)V

    goto :goto_0

    .line 160113
    :cond_1
    iget-object v2, p0, LX/0wW;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160114
    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/0wW;
    .locals 4

    .prologue
    .line 160161
    new-instance v3, LX/0wW;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v1

    check-cast v1, LX/0wY;

    invoke-static {p0}, LX/0wZ;->a(LX/0QB;)LX/0wZ;

    move-result-object v2

    check-cast v2, LX/0wZ;

    invoke-direct {v3, v0, v1, v2}, LX/0wW;-><init>(LX/0SG;LX/0wY;LX/0wZ;)V

    .line 160162
    return-object v3
.end method

.method private b(LX/0wd;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 160152
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160153
    iget-object v0, p0, LX/0wW;->a:Ljava/util/Map;

    .line 160154
    iget-object v1, p1, LX/0wd;->d:Ljava/lang/String;

    move-object v1, v1

    .line 160155
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 160156
    iget-object v0, p0, LX/0wW;->a:Ljava/util/Map;

    .line 160157
    iget-object v1, p1, LX/0wd;->d:Ljava/lang/String;

    move-object v1, v1

    .line 160158
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160159
    return-void

    .line 160160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0wd;
    .locals 1

    .prologue
    .line 160149
    new-instance v0, LX/0wd;

    invoke-direct {v0, p0}, LX/0wd;-><init>(LX/0wW;)V

    .line 160150
    invoke-direct {p0, v0}, LX/0wW;->b(LX/0wd;)V

    .line 160151
    return-object v0
.end method

.method public final a(LX/0wd;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 160143
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160144
    iget-object v0, p0, LX/0wW;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 160145
    iget-object v0, p0, LX/0wW;->a:Ljava/util/Map;

    .line 160146
    iget-object v1, p1, LX/0wd;->d:Ljava/lang/String;

    move-object v1, v1

    .line 160147
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160148
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 160133
    iget-object v0, p0, LX/0wW;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    .line 160134
    if-nez v0, :cond_0

    .line 160135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "springId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not reference a registered spring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160136
    :cond_0
    monitor-enter p0

    .line 160137
    :try_start_0
    iget-object v1, p0, LX/0wW;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160138
    iget-boolean v0, p0, LX/0wW;->h:Z

    move v0, v0

    .line 160139
    if-eqz v0, :cond_1

    .line 160140
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0wW;->h:Z

    .line 160141
    iget-object v0, p0, LX/0wW;->c:LX/0wY;

    iget-object v1, p0, LX/0wW;->d:LX/0wZ;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 160142
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 160115
    iget-object v0, p0, LX/0wW;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 160116
    iget-wide v2, p0, LX/0wW;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 160117
    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, LX/0wW;->f:J

    .line 160118
    :cond_0
    iget-wide v2, p0, LX/0wW;->f:J

    sub-long v2, v0, v2

    .line 160119
    iput-wide v0, p0, LX/0wW;->f:J

    .line 160120
    iget-object v4, p0, LX/0wW;->g:LX/0wb;

    invoke-virtual {v4}, LX/0wb;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 160121
    :cond_1
    invoke-direct {p0, v0, v1, v2, v3}, LX/0wW;->a(JJ)V

    .line 160122
    monitor-enter p0

    .line 160123
    :try_start_0
    iget-object v0, p0, LX/0wW;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 160124
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0wW;->h:Z

    .line 160125
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0wW;->f:J

    .line 160126
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160127
    iget-object v0, p0, LX/0wW;->g:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 160128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 160129
    :cond_3
    iget-object v0, p0, LX/0wW;->c:LX/0wY;

    iget-object v1, p0, LX/0wW;->d:LX/0wZ;

    invoke-interface {v0, v1}, LX/0wY;->b(LX/0wa;)V

    .line 160130
    iget-boolean v0, p0, LX/0wW;->h:Z

    if-nez v0, :cond_4

    .line 160131
    iget-object v0, p0, LX/0wW;->c:LX/0wY;

    iget-object v1, p0, LX/0wW;->d:LX/0wZ;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 160132
    :cond_4
    return-void
.end method
