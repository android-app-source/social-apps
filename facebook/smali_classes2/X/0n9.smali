.class public final LX/0n9;
.super LX/0nA;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 134763
    invoke-direct {p0}, LX/0nA;-><init>()V

    .line 134764
    new-instance v0, Ljava/util/ArrayList;

    mul-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    .line 134765
    return-void
.end method

.method public static a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134758
    invoke-direct {p0, p1}, LX/0n9;->d(Ljava/lang/String;)V

    .line 134759
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134760
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134761
    iget v0, p0, LX/0n9;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0n9;->c:I

    .line 134762
    return-void
.end method

.method private static c(LX/0n9;Ljava/lang/String;LX/0nA;)V
    .locals 1

    .prologue
    .line 134753
    const-string v0, "subParams cannot be null!"

    invoke-static {p2, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134754
    invoke-direct {p0, p1}, LX/0n9;->d(Ljava/lang/String;)V

    .line 134755
    invoke-virtual {p2}, LX/0nA;->c()V

    .line 134756
    invoke-static {p0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134757
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 134750
    if-ltz p1, :cond_0

    iget v0, p0, LX/0n9;->c:I

    if-lt p1, v0, :cond_1

    .line 134751
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 134752
    :cond_1
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 134746
    invoke-virtual {p0}, LX/0nA;->g()V

    .line 134747
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134748
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "key="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134749
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 134708
    iget v0, p0, LX/0n9;->c:I

    sub-int/2addr v0, p1

    .line 134709
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 134710
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    iget-object v2, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 134711
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    iget-object v2, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 134712
    :cond_0
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 134713
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0nA;)V
    .locals 0

    .prologue
    .line 134740
    invoke-static {p0, p1, p2}, LX/0n9;->c(LX/0n9;Ljava/lang/String;LX/0nA;)V

    .line 134741
    invoke-virtual {p2, p0}, LX/0nA;->a(LX/0nA;)V

    .line 134742
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134738
    invoke-static {p0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134739
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/0n9;
    .locals 1

    .prologue
    .line 134743
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    .line 134744
    invoke-virtual {p0, p1, v0}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 134745
    return-object v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 134736
    invoke-direct {p0, p1}, LX/0n9;->d(I)V

    .line 134737
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    mul-int/lit8 v1, p1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;LX/0nA;)V
    .locals 0

    .prologue
    .line 134731
    invoke-static {p0, p1, p2}, LX/0n9;->c(LX/0n9;Ljava/lang/String;LX/0nA;)V

    .line 134732
    invoke-virtual {p2}, LX/0nA;->c()V

    .line 134733
    iput-object p0, p2, LX/0nA;->h:LX/0nA;

    .line 134734
    iget-object p0, p2, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 134735
    return-void
.end method

.method public final c(Ljava/lang/String;)LX/0zL;
    .locals 1

    .prologue
    .line 134728
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->c()LX/0zL;

    move-result-object v0

    .line 134729
    invoke-virtual {p0, p1, v0}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 134730
    return-object v0
.end method

.method public final c(I)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 134726
    invoke-direct {p0, p1}, LX/0n9;->d(I)V

    .line 134727
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 134720
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/0n9;->c:I

    if-ge v1, v0, :cond_1

    .line 134721
    invoke-virtual {p0, v1}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 134722
    instance-of v2, v0, LX/0nA;

    if-eqz v2, :cond_0

    .line 134723
    check-cast v0, LX/0nA;

    invoke-virtual {v0}, LX/0nA;->b()V

    .line 134724
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 134725
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 134717
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    .line 134718
    iget-object v1, v0, LX/0Zh;->a:LX/0Zi;

    invoke-virtual {v1, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 134719
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 134714
    iget-object v0, p0, LX/0n9;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134715
    const/4 v0, 0x0

    iput v0, p0, LX/0n9;->c:I

    .line 134716
    return-void
.end method
