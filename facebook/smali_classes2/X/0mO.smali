.class public LX/0mO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0mO;


# instance fields
.field public a:LX/00G;


# direct methods
.method public constructor <init>(LX/00G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 132453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132454
    iput-object p1, p0, LX/0mO;->a:LX/00G;

    .line 132455
    return-void
.end method

.method public static a(LX/0QB;)LX/0mO;
    .locals 4

    .prologue
    .line 132456
    sget-object v0, LX/0mO;->b:LX/0mO;

    if-nez v0, :cond_1

    .line 132457
    const-class v1, LX/0mO;

    monitor-enter v1

    .line 132458
    :try_start_0
    sget-object v0, LX/0mO;->b:LX/0mO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132459
    if-eqz v2, :cond_0

    .line 132460
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132461
    new-instance p0, LX/0mO;

    invoke-static {v0}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v3

    check-cast v3, LX/00G;

    invoke-direct {p0, v3}, LX/0mO;-><init>(LX/00G;)V

    .line 132462
    move-object v0, p0

    .line 132463
    sput-object v0, LX/0mO;->b:LX/0mO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132464
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132465
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132466
    :cond_1
    sget-object v0, LX/0mO;->b:LX/0mO;

    return-object v0

    .line 132467
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132468
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
