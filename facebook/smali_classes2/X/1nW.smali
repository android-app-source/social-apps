.class public final LX/1nW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1nA;


# direct methods
.method public constructor <init>(LX/1nA;)V
    .locals 0

    .prologue
    .line 316515
    iput-object p1, p0, LX/1nW;->a:LX/1nA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x155866bb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 316507
    :try_start_0
    iget-object v0, p0, LX/1nW;->a:LX/1nA;

    iget-object v0, v0, LX/1nA;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82I;

    const v1, 0x7f0d0073

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v3}, LX/82I;->a(Ljava/lang/String;Landroid/view/View;LX/0lF;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316508
    :goto_0
    const v0, -0x1307252c

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 316509
    :catch_0
    move-exception v0

    .line 316510
    iget-object v1, p0, LX/1nW;->a:LX/1nA;

    iget-object v1, v1, LX/1nA;->l:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/1nA;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_DefaultFeedUnitRenderer.viewToUrlListener.onClick"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Couldn\'t launch external Activity for URI"

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    .line 316511
    iput-object v0, v3, LX/0VK;->c:Ljava/lang/Throwable;

    .line 316512
    move-object v0, v3

    .line 316513
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 316514
    iget-object v0, p0, LX/1nW;->a:LX/1nA;

    iget-object v0, v0, LX/1nA;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081122

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method
