.class public LX/1oH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ag;


# instance fields
.field public final a:LX/1af;

.field public b:LX/1De;

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:Landroid/graphics/drawable/Drawable;

.field public l:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(LX/1af;)V
    .locals 0

    .prologue
    .line 318912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318913
    iput-object p1, p0, LX/1oH;->a:LX/1af;

    .line 318914
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 318911
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(FZ)V
    .locals 1

    .prologue
    .line 318909
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1, p2}, LX/1af;->a(FZ)V

    .line 318910
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 318907
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(I)V

    .line 318908
    return-void
.end method

.method public final a(LX/1Up;)V
    .locals 1

    .prologue
    .line 318905
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(LX/1Up;)V

    .line 318906
    return-void
.end method

.method public final a(LX/1dc;)V
    .locals 4
    .param p1    # LX/1dc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 318894
    iget-object v0, p0, LX/1oH;->g:LX/1dc;

    if-eqz v0, :cond_1

    .line 318895
    iget-object v0, p0, LX/1oH;->g:LX/1dc;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318896
    :goto_0
    return-void

    .line 318897
    :cond_0
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    iget-object v1, p0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/1oH;->g:LX/1dc;

    invoke-static {v0, v1, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318898
    iput-object v3, p0, LX/1oH;->g:LX/1dc;

    .line 318899
    iput-object v3, p0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    .line 318900
    :cond_1
    if-nez p1, :cond_2

    .line 318901
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, v3}, LX/1af;->f(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 318902
    :cond_2
    iput-object p1, p0, LX/1oH;->g:LX/1dc;

    .line 318903
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    .line 318904
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    iget-object v1, p0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->f(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(LX/1dc;LX/1Up;)V
    .locals 4
    .param p1    # LX/1dc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Up;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 318883
    iget-object v0, p0, LX/1oH;->c:LX/1dc;

    if-eqz v0, :cond_1

    .line 318884
    iget-object v0, p0, LX/1oH;->c:LX/1dc;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318885
    :goto_0
    return-void

    .line 318886
    :cond_0
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    iget-object v1, p0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/1oH;->c:LX/1dc;

    invoke-static {v0, v1, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318887
    iput-object v3, p0, LX/1oH;->c:LX/1dc;

    .line 318888
    iput-object v3, p0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    .line 318889
    :cond_1
    if-nez p1, :cond_2

    .line 318890
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, v3}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 318891
    :cond_2
    iput-object p1, p0, LX/1oH;->c:LX/1dc;

    .line 318892
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    .line 318893
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    iget-object v1, p0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    goto :goto_0

    :cond_3
    sget-object p2, LX/1Uo;->a:LX/1Up;

    goto :goto_1
.end method

.method public final a(LX/1dc;LX/1Up;I)V
    .locals 4
    .param p1    # LX/1dc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Up;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 318866
    iget-object v0, p0, LX/1oH;->f:LX/1dc;

    if-eqz v0, :cond_1

    .line 318867
    iget-object v0, p0, LX/1oH;->f:LX/1dc;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318868
    :goto_0
    return-void

    .line 318869
    :cond_0
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    iget-object v1, p0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/1oH;->f:LX/1dc;

    invoke-static {v0, v1, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318870
    iput-object v3, p0, LX/1oH;->f:LX/1dc;

    .line 318871
    iput-object v3, p0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    .line 318872
    :cond_1
    if-nez p1, :cond_2

    .line 318873
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, v3}, LX/1af;->e(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 318874
    :cond_2
    iput-object p1, p0, LX/1oH;->f:LX/1dc;

    .line 318875
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    .line 318876
    iget-object v0, p0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 318877
    if-lez p3, :cond_4

    .line 318878
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-direct {v0, v1, p3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 318879
    :goto_1
    iget-object v1, p0, LX/1oH;->a:LX/1af;

    if-eqz p2, :cond_3

    :goto_2
    const/4 v2, 0x3

    .line 318880
    invoke-static {v1, v2, v0}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 318881
    invoke-static {v1, v2}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ao;->a(LX/1Up;)V

    .line 318882
    goto :goto_0

    :cond_3
    sget-object p2, LX/1Uo;->a:LX/1Up;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(LX/4Ab;)V
    .locals 1

    .prologue
    .line 318864
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(LX/4Ab;)V

    .line 318865
    return-void
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 318915
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/ColorFilter;)V

    .line 318916
    return-void
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 318825
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/PointF;)V

    .line 318826
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 318829
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/drawable/Drawable;)V

    .line 318830
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;FZ)V
    .locals 1

    .prologue
    .line 318831
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1, p2, p3}, LX/1af;->a(Landroid/graphics/drawable/Drawable;FZ)V

    .line 318832
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 318833
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Ljava/lang/Throwable;)V

    .line 318834
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 318835
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0}, LX/1af;->b()V

    .line 318836
    return-void
.end method

.method public final b(LX/1dc;LX/1Up;)V
    .locals 4
    .param p1    # LX/1dc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Up;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 318837
    iget-object v0, p0, LX/1oH;->d:LX/1dc;

    if-eqz v0, :cond_1

    .line 318838
    iget-object v0, p0, LX/1oH;->d:LX/1dc;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318839
    :goto_0
    return-void

    .line 318840
    :cond_0
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    iget-object v1, p0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/1oH;->d:LX/1dc;

    invoke-static {v0, v1, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318841
    iput-object v3, p0, LX/1oH;->d:LX/1dc;

    .line 318842
    iput-object v3, p0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    .line 318843
    :cond_1
    if-nez p1, :cond_2

    .line 318844
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, v3}, LX/1af;->d(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 318845
    :cond_2
    iput-object p1, p0, LX/1oH;->d:LX/1dc;

    .line 318846
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    .line 318847
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    iget-object v1, p0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p2, :cond_3

    :goto_1
    const/4 v2, 0x4

    .line 318848
    invoke-static {v0, v2, v1}, LX/1af;->a(LX/1af;ILandroid/graphics/drawable/Drawable;)V

    .line 318849
    invoke-static {v0, v2}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ao;->a(LX/1Up;)V

    .line 318850
    goto :goto_0

    :cond_3
    sget-object p2, LX/1Uo;->a:LX/1Up;

    goto :goto_1
.end method

.method public final b(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 318851
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 318852
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 318827
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Ljava/lang/Throwable;)V

    .line 318828
    return-void
.end method

.method public final c(LX/1dc;LX/1Up;)V
    .locals 4
    .param p1    # LX/1dc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Up;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 318853
    iget-object v0, p0, LX/1oH;->e:LX/1dc;

    if-eqz v0, :cond_1

    .line 318854
    iget-object v0, p0, LX/1oH;->e:LX/1dc;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318855
    :goto_0
    return-void

    .line 318856
    :cond_0
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    iget-object v1, p0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/1oH;->e:LX/1dc;

    invoke-static {v0, v1, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318857
    iput-object v3, p0, LX/1oH;->e:LX/1dc;

    .line 318858
    iput-object v3, p0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    .line 318859
    :cond_1
    if-nez p1, :cond_2

    .line 318860
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    invoke-virtual {v0, v3}, LX/1af;->c(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 318861
    :cond_2
    iput-object p1, p0, LX/1oH;->e:LX/1dc;

    .line 318862
    iget-object v0, p0, LX/1oH;->b:LX/1De;

    invoke-static {v0, p1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    .line 318863
    iget-object v0, p0, LX/1oH;->a:LX/1af;

    iget-object v1, p0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/1af;->b(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    goto :goto_0

    :cond_3
    sget-object p2, LX/1Uo;->a:LX/1Up;

    goto :goto_1
.end method
