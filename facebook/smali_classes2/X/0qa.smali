.class public LX/0qa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private static volatile d:LX/0qa;


# instance fields
.field private final b:LX/0Zb;

.field public final c:LX/0pV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 147938
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LX/0qa;->a:I

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0pV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147935
    iput-object p1, p0, LX/0qa;->b:LX/0Zb;

    .line 147936
    iput-object p2, p0, LX/0qa;->c:LX/0pV;

    .line 147937
    return-void
.end method

.method public static a(LX/0QB;)LX/0qa;
    .locals 5

    .prologue
    .line 147921
    sget-object v0, LX/0qa;->d:LX/0qa;

    if-nez v0, :cond_1

    .line 147922
    const-class v1, LX/0qa;

    monitor-enter v1

    .line 147923
    :try_start_0
    sget-object v0, LX/0qa;->d:LX/0qa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147924
    if-eqz v2, :cond_0

    .line 147925
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147926
    new-instance p0, LX/0qa;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v4

    check-cast v4, LX/0pV;

    invoke-direct {p0, v3, v4}, LX/0qa;-><init>(LX/0Zb;LX/0pV;)V

    .line 147927
    move-object v0, p0

    .line 147928
    sput-object v0, LX/0qa;->d:LX/0qa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147929
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147930
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147931
    :cond_1
    sget-object v0, LX/0qa;->d:LX/0qa;

    return-object v0

    .line 147932
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0qa;Z)V
    .locals 4

    .prologue
    .line 147895
    iget-object v0, p0, LX/0qa;->c:LX/0pV;

    const-string v1, "NewsFeedFragment"

    sget-object v2, LX/0rj;->LOADING_INDICATOR_HIDDEN:LX/0rj;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 147896
    return-void
.end method


# virtual methods
.method public final a(LX/AkT;)V
    .locals 8

    .prologue
    .line 147899
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "head_fetch_stats"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "duration_ms"

    .line 147900
    iget-wide v4, p1, LX/AkT;->b:J

    move-wide v2, v4

    .line 147901
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "duration_rerank_ms"

    .line 147902
    iget-wide v4, p1, LX/AkT;->c:J

    move-wide v2, v4

    .line 147903
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "duration_first_ms"

    .line 147904
    iget-wide v4, p1, LX/AkT;->d:J

    move-wide v2, v4

    .line 147905
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "fetch_feed_cause"

    .line 147906
    iget-object v2, p1, LX/AkT;->a:LX/0gf;

    move-object v2, v2

    .line 147907
    if-nez v2, :cond_0

    sget-object v3, LX/0gf;->UNKNOWN:LX/0gf;

    invoke-virtual {v3}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v2, v3

    .line 147908
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_success"

    .line 147909
    iget-boolean v2, p1, LX/AkT;->e:Z

    move v2, v2

    .line 147910
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_asyncfeed"

    .line 147911
    iget-boolean v2, p1, LX/AkT;->f:Z

    move v2, v2

    .line 147912
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "min_duration"

    .line 147913
    iget-wide v4, p1, LX/AkT;->c:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 147914
    iget-wide v4, p1, LX/AkT;->c:J

    iget-wide v6, p1, LX/AkT;->d:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 147915
    :goto_1
    move-wide v2, v4

    .line 147916
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "news_feed_rerank"

    .line 147917
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 147918
    move-object v0, v0

    .line 147919
    iget-object v1, p0, LX/0qa;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 147920
    return-void

    :cond_0
    invoke-virtual {v2}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    iget-wide v4, p1, LX/AkT;->d:J

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 147897
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0qa;->a(LX/0qa;Z)V

    .line 147898
    return-void
.end method
