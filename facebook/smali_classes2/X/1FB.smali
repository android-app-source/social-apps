.class public LX/1FB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/1F8;

.field private b:LX/4eM;

.field private c:LX/1FO;

.field private d:LX/1Fk;

.field private e:LX/1Fj;

.field private f:LX/1Fl;

.field public g:LX/4eS;

.field private h:LX/1FQ;


# direct methods
.method public constructor <init>(LX/1F8;)V
    .locals 1

    .prologue
    .line 221669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221670
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1F8;

    iput-object v0, p0, LX/1FB;->a:LX/1F8;

    .line 221671
    return-void
.end method


# virtual methods
.method public final a()LX/4eM;
    .locals 5

    .prologue
    .line 221699
    iget-object v0, p0, LX/1FB;->b:LX/4eM;

    if-nez v0, :cond_0

    .line 221700
    new-instance v0, LX/4eM;

    iget-object v1, p0, LX/1FB;->a:LX/1F8;

    .line 221701
    iget-object v2, v1, LX/1F8;->d:LX/0rb;

    move-object v1, v2

    .line 221702
    iget-object v2, p0, LX/1FB;->a:LX/1F8;

    .line 221703
    iget-object v3, v2, LX/1F8;->a:LX/1F7;

    move-object v2, v3

    .line 221704
    iget-object v3, p0, LX/1FB;->a:LX/1F8;

    .line 221705
    iget-object v4, v3, LX/1F8;->b:LX/1F0;

    move-object v3, v4

    .line 221706
    invoke-direct {v0, v1, v2, v3}, LX/4eM;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v0, p0, LX/1FB;->b:LX/4eM;

    .line 221707
    :cond_0
    iget-object v0, p0, LX/1FB;->b:LX/4eM;

    return-object v0
.end method

.method public final b()LX/1FO;
    .locals 4

    .prologue
    .line 221708
    iget-object v0, p0, LX/1FB;->c:LX/1FO;

    if-nez v0, :cond_0

    .line 221709
    new-instance v0, LX/1FO;

    iget-object v1, p0, LX/1FB;->a:LX/1F8;

    .line 221710
    iget-object v2, v1, LX/1F8;->d:LX/0rb;

    move-object v1, v2

    .line 221711
    iget-object v2, p0, LX/1FB;->a:LX/1F8;

    .line 221712
    iget-object v3, v2, LX/1F8;->c:LX/1F7;

    move-object v2, v3

    .line 221713
    invoke-direct {v0, v1, v2}, LX/1FO;-><init>(LX/0rb;LX/1F7;)V

    iput-object v0, p0, LX/1FB;->c:LX/1FO;

    .line 221714
    :cond_0
    iget-object v0, p0, LX/1FB;->c:LX/1FO;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 221687
    iget-object v0, p0, LX/1FB;->a:LX/1F8;

    .line 221688
    iget-object p0, v0, LX/1F8;->c:LX/1F7;

    move-object v0, p0

    .line 221689
    iget v0, v0, LX/1F7;->f:I

    return v0
.end method

.method public final d()LX/1Fk;
    .locals 5

    .prologue
    .line 221690
    iget-object v0, p0, LX/1FB;->d:LX/1Fk;

    if-nez v0, :cond_0

    .line 221691
    new-instance v0, LX/1Fk;

    iget-object v1, p0, LX/1FB;->a:LX/1F8;

    .line 221692
    iget-object v2, v1, LX/1F8;->d:LX/0rb;

    move-object v1, v2

    .line 221693
    iget-object v2, p0, LX/1FB;->a:LX/1F8;

    .line 221694
    iget-object v3, v2, LX/1F8;->e:LX/1F7;

    move-object v2, v3

    .line 221695
    iget-object v3, p0, LX/1FB;->a:LX/1F8;

    .line 221696
    iget-object v4, v3, LX/1F8;->f:LX/1F0;

    move-object v3, v4

    .line 221697
    invoke-direct {v0, v1, v2, v3}, LX/1Fk;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v0, p0, LX/1FB;->d:LX/1Fk;

    .line 221698
    :cond_0
    iget-object v0, p0, LX/1FB;->d:LX/1Fk;

    return-object v0
.end method

.method public final e()LX/1Fj;
    .locals 3

    .prologue
    .line 221684
    iget-object v0, p0, LX/1FB;->e:LX/1Fj;

    if-nez v0, :cond_0

    .line 221685
    new-instance v0, LX/1Fj;

    invoke-virtual {p0}, LX/1FB;->d()LX/1Fk;

    move-result-object v1

    invoke-virtual {p0}, LX/1FB;->f()LX/1Fl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1Fj;-><init>(LX/1Fk;LX/1Fl;)V

    iput-object v0, p0, LX/1FB;->e:LX/1Fj;

    .line 221686
    :cond_0
    iget-object v0, p0, LX/1FB;->e:LX/1Fj;

    return-object v0
.end method

.method public final f()LX/1Fl;
    .locals 2

    .prologue
    .line 221681
    iget-object v0, p0, LX/1FB;->f:LX/1Fl;

    if-nez v0, :cond_0

    .line 221682
    new-instance v0, LX/1Fl;

    invoke-virtual {p0}, LX/1FB;->h()LX/1FQ;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Fl;-><init>(LX/1FQ;)V

    iput-object v0, p0, LX/1FB;->f:LX/1Fl;

    .line 221683
    :cond_0
    iget-object v0, p0, LX/1FB;->f:LX/1Fl;

    return-object v0
.end method

.method public final h()LX/1FQ;
    .locals 5

    .prologue
    .line 221672
    iget-object v0, p0, LX/1FB;->h:LX/1FQ;

    if-nez v0, :cond_0

    .line 221673
    new-instance v0, LX/1FQ;

    iget-object v1, p0, LX/1FB;->a:LX/1F8;

    .line 221674
    iget-object v2, v1, LX/1F8;->d:LX/0rb;

    move-object v1, v2

    .line 221675
    iget-object v2, p0, LX/1FB;->a:LX/1F8;

    .line 221676
    iget-object v3, v2, LX/1F8;->g:LX/1F7;

    move-object v2, v3

    .line 221677
    iget-object v3, p0, LX/1FB;->a:LX/1F8;

    .line 221678
    iget-object v4, v3, LX/1F8;->h:LX/1F0;

    move-object v3, v4

    .line 221679
    invoke-direct {v0, v1, v2, v3}, LX/1FQ;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v0, p0, LX/1FB;->h:LX/1FQ;

    .line 221680
    :cond_0
    iget-object v0, p0, LX/1FB;->h:LX/1FQ;

    return-object v0
.end method
