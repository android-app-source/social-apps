.class public LX/0aN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0aN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/io/File;

.field private c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84613
    const-class v0, LX/0aN;

    sput-object v0, LX/0aN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Z)V
    .locals 1

    .prologue
    .line 84653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84654
    invoke-virtual {p1}, Ljava/io/File;->isAbsolute()Z

    move-result v0

    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 84655
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 84656
    iput-object p1, p0, LX/0aN;->b:Ljava/io/File;

    .line 84657
    iput-boolean p2, p0, LX/0aN;->c:Z

    .line 84658
    return-void

    .line 84659
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 84643
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84644
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84645
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 84646
    :try_start_0
    invoke-virtual {v2, p3}, Ljava/io/FileOutputStream;->write([B)V

    .line 84647
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84648
    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 84649
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84650
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 84651
    return-void

    .line 84652
    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    invoke-static {v2, v1}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method private b(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 84642
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/0aN;->b:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/io/File;)Ljava/nio/ByteBuffer;
    .locals 9

    .prologue
    .line 84623
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v2, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 84624
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    .line 84625
    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    .line 84626
    new-instance v0, LX/5ol;

    const-string v3, "impossibly long QE file"

    invoke-direct {v0, v3}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 84627
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 84628
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_0
    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_1
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    .line 84629
    new-instance v1, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error reading QE store "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/5ol;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 84630
    :cond_0
    long-to-int v3, v4

    .line 84631
    :try_start_5
    new-array v4, v3, [B

    .line 84632
    const/4 v0, 0x0

    .line 84633
    :goto_2
    if-ge v0, v3, :cond_2

    .line 84634
    sub-int v5, v3, v0

    invoke-virtual {v2, v4, v0, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v5

    .line 84635
    if-gez v5, :cond_1

    .line 84636
    new-instance v0, LX/5ol;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected short read of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84637
    :catchall_1
    move-exception v0

    goto :goto_0

    .line 84638
    :cond_1
    add-int/2addr v0, v5

    .line 84639
    goto :goto_2

    .line 84640
    :cond_2
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    .line 84641
    :try_start_6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    return-object v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 84622
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p1}, LX/0aN;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "current.bin"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 84615
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84616
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84617
    new-instance v0, LX/5ol;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exists but is not a directory!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84618
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84619
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84620
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not create dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84621
    :cond_1
    return-void
.end method

.method private d(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 84614
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p1}, LX/0aN;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "current.bin.tmp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static d(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 84609
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84610
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84611
    sget-object v0, LX/0aN;->a:Ljava/lang/Class;

    const-string v1, "Failed to unlink %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84612
    :cond_0
    return-void
.end method

.method public static e(Ljava/io/File;)Z
    .locals 4

    .prologue
    .line 84608
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 84660
    invoke-static {p0, p1, p2}, LX/0aN;->j(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84661
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84662
    new-instance v1, Ljava/io/File;

    const-string v2, "index.bin.tmp"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v1

    .line 84663
    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84664
    invoke-direct {p0, p1, p2}, LX/0aN;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84665
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84666
    new-instance v1, Ljava/io/File;

    const-string v2, "data.bin.tmp"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v1

    .line 84667
    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84668
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84669
    return-void
.end method

.method public static g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 84527
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p1}, LX/0aN;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private h(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 84528
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84529
    new-instance v1, Ljava/io/File;

    const-string v2, "data.bin"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static j(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 84530
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84531
    new-instance v1, Ljava/io/File;

    const-string v2, "index.bin"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 84590
    invoke-direct {p0, p1}, LX/0aN;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 84591
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 84592
    :cond_0
    :goto_0
    return-object v0

    .line 84593
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84594
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 84595
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v2, v3}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 84596
    if-eqz v1, :cond_0

    .line 84597
    invoke-static {p0, p1, v1}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 84598
    const/4 v3, 0x0

    .line 84599
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    .line 84600
    :cond_2
    :goto_1
    move v2, v3

    .line 84601
    if-eqz v2, :cond_0

    move-object v0, v1

    .line 84602
    goto :goto_0

    .line 84603
    :catchall_0
    move-exception v1

    const/4 v3, 0x1

    invoke-static {v2, v3}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 84604
    :catch_0
    goto :goto_0

    .line 84605
    :cond_3
    new-instance v4, Ljava/io/File;

    const-string p0, "index.bin"

    invoke-direct {v4, v2, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84606
    new-instance p0, Ljava/io/File;

    const-string p1, "data.bin"

    invoke-direct {p0, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84607
    invoke-static {v4}, LX/0aN;->e(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0}, LX/0aN;->e(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84532
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84533
    invoke-static {p0, p1, p2}, LX/0aN;->g(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->c(Ljava/io/File;)V

    .line 84534
    return-void

    .line 84535
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .prologue
    .line 84536
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84537
    const-string v4, "data.bin.tmp"

    const-string v5, "data.bin"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V

    .line 84538
    return-void

    .line 84539
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 84540
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84541
    iget-object v0, p0, LX/0aN;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 84542
    if-nez v3, :cond_2

    .line 84543
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 84544
    goto :goto_0

    .line 84545
    :cond_2
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 84546
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 84547
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 84548
    invoke-interface {p1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 84549
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 84550
    if-eqz v7, :cond_4

    .line 84551
    array-length v8, v7

    move v0, v1

    :goto_2
    if-ge v0, v8, :cond_4

    aget-object v9, v7, v0

    .line 84552
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 84553
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 84554
    invoke-direct {p0, v6, v9}, LX/0aN;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 84555
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 84556
    :cond_4
    invoke-direct {p0, v6}, LX/0aN;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84557
    invoke-direct {p0, v6}, LX/0aN;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0aN;->d(Ljava/io/File;)V

    .line 84558
    invoke-static {v5}, LX/0aN;->d(Ljava/io/File;)V

    .line 84559
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 84560
    invoke-direct {p0, p1, p2}, LX/0aN;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84561
    invoke-static {v0}, LX/0aN;->b(Ljava/io/File;)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .prologue
    .line 84562
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84563
    const-string v4, "index.bin.tmp"

    const-string v5, "index.bin"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V

    .line 84564
    return-void

    .line 84565
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84566
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84567
    invoke-direct {p0, p1}, LX/0aN;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84568
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 84569
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 84570
    :try_start_0
    invoke-virtual {v4, p2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 84571
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84572
    invoke-static {v4, v2}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 84573
    invoke-direct {p0, p1}, LX/0aN;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 84574
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 84575
    return-void

    :cond_0
    move v0, v2

    .line 84576
    goto :goto_0

    .line 84577
    :catchall_0
    move-exception v0

    invoke-static {v4, v1}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 84578
    iget-boolean v0, p0, LX/0aN;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 84579
    invoke-direct {p0, p1}, LX/0aN;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 84580
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 84581
    if-nez v0, :cond_2

    .line 84582
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 84583
    goto :goto_0

    .line 84584
    :cond_2
    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 84585
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 84586
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 84587
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 84588
    invoke-direct {p0, p1, v3}, LX/0aN;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 84589
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
