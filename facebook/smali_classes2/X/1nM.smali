.class public LX/1nM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/1Uf;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zK;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifySpanFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field private final e:LX/03V;

.field public final f:LX/0Uh;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Cn;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Z

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8rQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Uf;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/03V;LX/0Uh;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;)V
    .locals 1
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsWorkMultiCompanyGroupsEnabledGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Uf;",
            "LX/0Ot",
            "<",
            "LX/1zK;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifySpanFactory;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/3Cn;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "LX/8rQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 316089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316090
    iput-object p1, p0, LX/1nM;->a:LX/1Uf;

    .line 316091
    iput-object p2, p0, LX/1nM;->b:LX/0Ot;

    .line 316092
    iput-object p3, p0, LX/1nM;->c:LX/0Ot;

    .line 316093
    const v0, 0x7f081034

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1nM;->d:Ljava/lang/String;

    .line 316094
    iput-object p5, p0, LX/1nM;->e:LX/03V;

    .line 316095
    iput-object p6, p0, LX/1nM;->f:LX/0Uh;

    .line 316096
    iput-object p7, p0, LX/1nM;->g:LX/0Ot;

    .line 316097
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1nM;->h:Z

    .line 316098
    iput-object p9, p0, LX/1nM;->i:LX/0Ot;

    .line 316099
    return-void
.end method

.method public static a(LX/0QB;)LX/1nM;
    .locals 13

    .prologue
    .line 316100
    const-class v1, LX/1nM;

    monitor-enter v1

    .line 316101
    :try_start_0
    sget-object v0, LX/1nM;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 316102
    sput-object v2, LX/1nM;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 316103
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316104
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 316105
    new-instance v3, LX/1nM;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    const/16 v5, 0x129e

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x129a

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v10, 0x129f

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/1nN;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    const/16 v12, 0x3751

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/1nM;-><init>(LX/1Uf;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/03V;LX/0Uh;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;)V

    .line 316106
    move-object v0, v3

    .line 316107
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 316108
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1nM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316109
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 316110
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;II)Landroid/text/Spannable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;II)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 316111
    invoke-static {p0, p1, p2}, LX/1nM;->c(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;

    move-result-object v1

    .line 316112
    if-nez v1, :cond_0

    .line 316113
    const/4 v0, 0x0

    .line 316114
    :goto_0
    return-object v0

    .line 316115
    :cond_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 316116
    iget-object v0, p0, LX/1nM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zK;

    .line 316117
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 316118
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 316119
    iget-object p2, v0, LX/1zK;->c:LX/1WC;

    invoke-virtual {p2, v2}, LX/1WC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, v0, LX/1zK;->b:LX/1WB;

    invoke-virtual {p2, v2}, LX/1WB;->b(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result p2

    const/4 p3, -0x1

    if-eq p2, p3, :cond_2

    .line 316120
    iget-object p2, v0, LX/1zK;->b:LX/1WB;

    invoke-virtual {p2, v2}, LX/1WB;->b(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v2

    .line 316121
    :goto_1
    move p3, v2

    .line 316122
    :cond_1
    iget-object v2, p0, LX/1nM;->a:LX/1Uf;

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v4

    iget-object v3, p0, LX/1nM;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1zL;

    .line 316123
    iget-object v5, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 316124
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v5

    .line 316125
    if-eqz v5, :cond_6

    .line 316126
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 316127
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 316128
    iget-object v5, v3, LX/1zL;->b:LX/1zN;

    invoke-virtual {v5, p1}, LX/1zN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1zP;

    move-result-object v5

    const v6, 0x7f0a0429

    .line 316129
    iput v6, v5, LX/1zQ;->b:I

    .line 316130
    move-object v5, v5

    .line 316131
    :goto_2
    move-object v5, v5

    .line 316132
    iget-object v7, p0, LX/1nM;->d:Ljava/lang/String;

    move-object v3, v1

    move v6, p3

    invoke-virtual/range {v2 .. v7}, LX/1Uf;->a(Ljava/lang/CharSequence;ILandroid/text/style/CharacterStyle;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v2

    .line 316133
    if-nez v2, :cond_5

    new-instance v2, LX/1zR;

    invoke-static {p0, p1, v1}, LX/1nM;->a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1zR;-><init>(Ljava/lang/CharSequence;)V

    :goto_3
    move-object v0, v2

    .line 316134
    goto :goto_0

    .line 316135
    :cond_2
    invoke-static {v2}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-nez p2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v2, 0x1

    .line 316136
    :goto_4
    invoke-virtual {v0, v1, v2}, LX/1zK;->a(Ljava/lang/CharSequence;Z)I

    move-result v2

    goto :goto_1

    .line 316137
    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    invoke-static {p0, p1, v2}, LX/1nM;->a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v2

    goto :goto_3

    :cond_6
    iget-object v5, v3, LX/1zL;->b:LX/1zN;

    invoke-virtual {v5, p1}, LX/1zN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1zP;

    move-result-object v5

    goto :goto_2
.end method

.method public static a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/Spannable;)Landroid/text/Spannable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/Spannable;",
            ")",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 316138
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 316139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 316140
    const/4 v2, 0x0

    .line 316141
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 316142
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_1

    .line 316143
    :cond_0
    :goto_1
    return-object p2

    .line 316144
    :cond_1
    iget-object v1, p0, LX/1nM;->a:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v0

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    .line 316145
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object p2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method private static c(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 316146
    if-eqz p1, :cond_0

    .line 316147
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 316148
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_2

    .line 316149
    :cond_0
    const/4 v0, 0x0

    .line 316150
    :cond_1
    :goto_0
    return-object v0

    .line 316151
    :cond_2
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v0

    .line 316152
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 316153
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 316154
    :try_start_0
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v1

    .line 316155
    iget-object v0, p0, LX/1nM;->a:LX/1Uf;

    sget-object v3, LX/1eK;->MESSAGE:LX/1eK;

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 316156
    iget-boolean v0, p0, LX/1nM;->h:Z

    if-eqz v0, :cond_3

    .line 316157
    iget-object v0, p0, LX/1nM;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cn;

    invoke-virtual {v0, v1, v3}, LX/3Cn;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Landroid/text/SpannableStringBuilder;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    move-object v0, v3

    .line 316158
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_18

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 316159
    if-eqz v1, :cond_17

    .line 316160
    iget-object v0, p0, LX/1nM;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8rQ;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v1

    .line 316161
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 316162
    iget-object v2, v0, LX/8rQ;->d:LX/8rO;

    invoke-virtual {v2}, LX/8rO;->c()V

    .line 316163
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 316164
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v6

    .line 316165
    iget-object v3, v0, LX/8rQ;->d:LX/8rO;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->j()I

    move-result v7

    const p0, 0x7fffffff

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 316166
    iget-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    if-nez v8, :cond_19

    .line 316167
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    .line 316168
    :cond_4
    :goto_4
    iget v8, v3, LX/8rO;->g:I

    if-gt v7, v8, :cond_5

    .line 316169
    iput p0, v3, LX/8rO;->g:I

    .line 316170
    :cond_5
    iget v8, v3, LX/8rO;->e:I

    if-lt v7, v8, :cond_6

    iget v8, v3, LX/8rO;->e:I

    if-ne v7, v8, :cond_7

    iget-object v8, v3, LX/8rO;->f:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-eq v6, v8, :cond_7

    .line 316171
    :cond_6
    iput p0, v3, LX/8rO;->e:I

    .line 316172
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    iput-object v8, v3, LX/8rO;->f:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 316173
    :cond_7
    invoke-static {v6}, LX/8rO;->a(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, v3, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-eq v8, v6, :cond_9

    .line 316174
    :cond_8
    iget-object v8, v3, LX/8rO;->h:LX/8rM;

    .line 316175
    add-int/lit8 p0, v7, -0x1

    iput p0, v8, LX/8rM;->d:I

    .line 316176
    iget-object p0, v8, LX/8rM;->c:[I

    const/4 p1, 0x0

    aput p1, p0, v7

    .line 316177
    :cond_9
    invoke-static {v6}, LX/8rO;->a(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 316178
    iget-boolean v8, v3, LX/8rO;->d:Z

    if-nez v8, :cond_1a

    invoke-static {v3, v7}, LX/8rO;->a(LX/8rO;I)Z

    move-result v8

    if-nez v8, :cond_1a

    .line 316179
    iput-boolean v10, v3, LX/8rO;->d:Z

    .line 316180
    :cond_a
    :goto_5
    iget v8, v3, LX/8rO;->e:I

    if-ge v7, v8, :cond_b

    .line 316181
    iput v7, v3, LX/8rO;->e:I

    .line 316182
    iput-object v6, v3, LX/8rO;->f:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 316183
    :cond_b
    iget-object v8, v3, LX/8rO;->h:LX/8rM;

    invoke-virtual {v8, v7, v6}, LX/8rM;->a(ILcom/facebook/graphql/enums/GraphQLComposedBlockType;)V

    .line 316184
    :cond_c
    :goto_6
    iput v7, v3, LX/8rO;->b:I

    .line 316185
    iput-object v6, v3, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 316186
    iget-object v3, v0, LX/8rQ;->d:LX/8rO;

    const/4 v7, 0x0

    .line 316187
    iget-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    if-eqz v8, :cond_d

    iget-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 316188
    :cond_d
    :goto_7
    move v3, v7

    .line 316189
    if-eqz v3, :cond_e

    .line 316190
    sget-object v3, LX/8rP;->a:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v7

    aget v3, v3, v7

    packed-switch v3, :pswitch_data_0

    .line 316191
    const v3, 0x7f0b0050

    :goto_8
    move v3, v3

    .line 316192
    invoke-static {v0, v4, v3}, LX/8rQ;->a(LX/8rQ;Landroid/text/SpannableStringBuilder;I)V

    .line 316193
    :cond_e
    iget-object v3, v0, LX/8rQ;->a:LX/8rI;

    iget-object v7, v0, LX/8rQ;->d:LX/8rO;

    const/4 p2, 0x3

    const/4 p1, 0x2

    const/4 v1, 0x0

    const/4 p0, 0x1

    .line 316194
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->j()I

    move-result v9

    .line 316195
    sget-object v8, LX/8rH;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v10

    aget v8, v8, v10

    packed-switch v8, :pswitch_data_1

    .line 316196
    sget-object v8, LX/8rI;->a:[Ljava/lang/Object;

    :goto_9
    move-object v7, v8

    .line 316197
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 316198
    iget-object v3, v0, LX/8rQ;->a:LX/8rI;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v10

    .line 316199
    sget-object p0, LX/8rH;->a:[I

    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_2

    .line 316200
    :goto_a
    move-object v3, v9

    .line 316201
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 316202
    array-length v9, v7

    const/4 v3, 0x0

    :goto_b
    if-ge v3, v9, :cond_f

    aget-object v10, v7, v3

    .line 316203
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    const/16 p1, 0x21

    invoke-virtual {v4, v10, v8, p0, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 316204
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    .line 316205
    :cond_f
    iget-object v3, v0, LX/8rQ;->d:LX/8rO;

    .line 316206
    sget-object v7, LX/8rN;->a:[I

    iget-object v9, v3, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_3

    .line 316207
    const/4 v7, 0x0

    :goto_c
    move v3, v7

    .line 316208
    if-eqz v3, :cond_10

    .line 316209
    sget-object v3, LX/8rP;->a:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v7

    aget v3, v3, v7

    packed-switch v3, :pswitch_data_4

    .line 316210
    const/4 v3, -0x1

    :goto_d
    move v3, v3

    .line 316211
    invoke-static {v0, v4, v3}, LX/8rQ;->a(LX/8rQ;Landroid/text/SpannableStringBuilder;I)V

    .line 316212
    :cond_10
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->l()LX/0Px;

    move-result-object v3

    const/16 p2, 0x21

    .line 316213
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_13

    .line 316214
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_11
    :goto_e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;

    .line 316215
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->k()I

    move-result v9

    add-int/2addr v9, v8

    .line 316216
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v10

    invoke-static {v10}, LX/8bQ;->a(Lcom/facebook/graphql/enums/GraphQLInlineStyle;)Ljava/lang/Object;

    move-result-object v10

    .line 316217
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->j()I

    move-result p0

    add-int/2addr p0, v9

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 316218
    if-eqz v10, :cond_12

    .line 316219
    invoke-virtual {v4, v10, v9, p0, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 316220
    :cond_12
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v6

    .line 316221
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->CODE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    if-ne v6, v10, :cond_20

    .line 316222
    new-instance v10, Landroid/text/style/BackgroundColorSpan;

    iget p1, v0, LX/8rQ;->e:I

    invoke-direct {v10, p1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 316223
    :goto_f
    move-object v6, v10

    .line 316224
    if-eqz v6, :cond_11

    .line 316225
    invoke-virtual {v4, v6, v9, p0, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_e

    .line 316226
    :cond_13
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->k()LX/0Px;

    move-result-object v2

    const/16 p2, 0x21

    .line 316227
    if-nez v2, :cond_21

    .line 316228
    :cond_14
    invoke-static {v4}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    goto/16 :goto_3

    .line 316229
    :cond_15
    move-object v0, v4

    .line 316230
    goto/16 :goto_0

    .line 316231
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 316232
    iget-object v3, p0, LX/1nM;->e:LX/03V;

    const-string v4, "LinkifyUtil.IndexOutOfBoundsException"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    :goto_10
    invoke-virtual {v3, v4, v0, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 316233
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_1

    .line 316234
    :cond_16
    const-string v0, "No debug info"

    goto :goto_10

    .line 316235
    :cond_17
    const/4 v3, 0x0

    .line 316236
    iget-object v1, p0, LX/1nM;->f:LX/0Uh;

    const/16 v4, 0xf6

    invoke-virtual {v1, v4, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 316237
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 316238
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_24

    const/4 v1, 0x1

    :goto_11
    move v1, v1

    .line 316239
    if-eqz v1, :cond_1

    .line 316240
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/8u7;->a(Landroid/text/Spanned;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_0

    :cond_18
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 316241
    :cond_19
    iget-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 316242
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v3, LX/8rO;->c:Ljava/lang/Boolean;

    goto/16 :goto_4

    .line 316243
    :cond_1a
    iget-object v8, v3, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v6, v8, :cond_a

    .line 316244
    iput-boolean v9, v3, LX/8rO;->d:Z

    goto/16 :goto_5

    .line 316245
    :cond_1b
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v6, v8, :cond_c

    .line 316246
    iget v8, v3, LX/8rO;->g:I

    if-ge v7, v8, :cond_c

    .line 316247
    iput v7, v3, LX/8rO;->g:I

    goto/16 :goto_6

    .line 316248
    :cond_1c
    iget v8, v3, LX/8rO;->b:I

    invoke-static {v3, v8}, LX/8rO;->a(LX/8rO;I)Z

    move-result v8

    move v8, v8

    .line 316249
    if-eqz v8, :cond_1d

    iget-boolean v8, v3, LX/8rO;->d:Z

    if-nez v8, :cond_1d

    iget-object v8, v3, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-static {v8}, LX/8rO;->a(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 316250
    :cond_1d
    const/4 v7, 0x1

    goto/16 :goto_7

    .line 316251
    :pswitch_0
    const v3, 0x7f0b0052

    goto/16 :goto_8

    .line 316252
    :pswitch_1
    new-array v8, p1, [Ljava/lang/Object;

    new-instance v10, LX/8uC;

    invoke-direct {v10, v9}, LX/8uC;-><init>(I)V

    aput-object v10, v8, v1

    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    const/high16 v10, 0x3fc00000    # 1.5f

    invoke-direct {v9, v10}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    aput-object v9, v8, p0

    goto/16 :goto_9

    .line 316253
    :pswitch_2
    new-array v8, p2, [Ljava/lang/Object;

    new-instance v10, LX/8uC;

    invoke-direct {v10, v9}, LX/8uC;-><init>(I)V

    aput-object v10, v8, v1

    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    iget v10, v3, LX/8rI;->c:I

    invoke-direct {v9, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v9, v8, p0

    new-instance v9, Landroid/text/style/StyleSpan;

    invoke-direct {v9, p0}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v9, v8, p1

    goto/16 :goto_9

    .line 316254
    :pswitch_3
    new-array v8, p0, [Ljava/lang/Object;

    new-instance v10, LX/8uE;

    .line 316255
    iget-object p0, v7, LX/8rO;->h:LX/8rM;

    invoke-virtual {p0}, LX/8rM;->c()I

    move-result p0

    and-int/lit8 p0, p0, 0x1

    const/4 p1, 0x1

    if-ne p0, p1, :cond_1e

    const/4 p0, 0x4

    :goto_12
    move p0, p0

    .line 316256
    iget-object p1, v7, LX/8rO;->h:LX/8rM;

    const/4 p2, -0x1

    .line 316257
    iget v7, p1, LX/8rM;->d:I

    if-ne v7, p2, :cond_1f

    .line 316258
    :goto_13
    move p1, p2

    .line 316259
    move p1, p1

    .line 316260
    iget p2, v3, LX/8rI;->e:I

    invoke-direct {v10, v9, p0, p1, p2}, LX/8uE;-><init>(IIII)V

    aput-object v10, v8, v1

    goto/16 :goto_9

    .line 316261
    :pswitch_4
    new-array v8, p0, [Ljava/lang/Object;

    new-instance v10, LX/8uF;

    invoke-virtual {v7}, LX/8rO;->f()I

    move-result p0

    iget p1, v3, LX/8rI;->e:I

    invoke-direct {v10, v9, p0, p1}, LX/8uF;-><init>(III)V

    aput-object v10, v8, v1

    goto/16 :goto_9

    .line 316262
    :pswitch_5
    new-array v8, p2, [Ljava/lang/Object;

    new-instance v10, LX/8uC;

    invoke-direct {v10, v9}, LX/8uC;-><init>(I)V

    aput-object v10, v8, v1

    new-instance v9, LX/8uA;

    invoke-direct {v9}, LX/8uA;-><init>()V

    iget v10, v3, LX/8rI;->d:I

    invoke-virtual {v9, v10}, LX/8u8;->a(I)LX/8u8;

    move-result-object v9

    aput-object v9, v8, p0

    new-instance v9, Landroid/text/style/TypefaceSpan;

    const-string v10, "monospace"

    invoke-direct {v9, v10}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    aput-object v9, v8, p1

    goto/16 :goto_9

    .line 316263
    :pswitch_6
    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    new-instance v10, LX/8uC;

    invoke-direct {v10, v9}, LX/8uC;-><init>(I)V

    aput-object v10, v8, v1

    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    iget v10, v3, LX/8rI;->c:I

    invoke-direct {v9, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v9, v8, p0

    new-instance v9, Landroid/text/style/StyleSpan;

    invoke-direct {v9, p1}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v9, v8, p1

    new-instance v9, LX/8u9;

    invoke-direct {v9}, LX/8u9;-><init>()V

    iget v10, v3, LX/8rI;->c:I

    invoke-virtual {v9, v10}, LX/8u8;->a(I)LX/8u8;

    move-result-object v9

    aput-object v9, v8, p2

    goto/16 :goto_9

    .line 316264
    :pswitch_7
    new-array v8, p0, [Ljava/lang/Object;

    new-instance v10, LX/8uC;

    invoke-direct {v10, v9}, LX/8uC;-><init>(I)V

    aput-object v10, v8, v1

    goto/16 :goto_9

    :cond_1e
    const/4 p0, 0x5

    goto :goto_12

    :cond_1f
    iget-object p2, p1, LX/8rM;->b:[I

    iget v7, p1, LX/8rM;->d:I

    aget p2, p2, v7

    goto :goto_13

    .line 316265
    :pswitch_8
    iget-object p0, v3, LX/8rI;->b:LX/23P;

    const/4 p1, 0x0

    invoke-virtual {p0, v9, p1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v9

    goto/16 :goto_a

    .line 316266
    :pswitch_9
    const/4 v7, 0x1

    goto/16 :goto_c

    .line 316267
    :pswitch_a
    const v3, 0x7f0b188c

    goto/16 :goto_d

    .line 316268
    :pswitch_b
    const v3, 0x7f0b188d

    goto/16 :goto_d

    :cond_20
    const/4 v10, 0x0

    goto/16 :goto_f

    .line 316269
    :cond_21
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_22
    :goto_14
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    .line 316270
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->k()I

    move-result v7

    add-int/2addr v7, v8

    .line 316271
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->j()I

    move-result v9

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    sub-int/2addr v10, v7

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 316272
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v10

    .line 316273
    if-eqz v10, :cond_23

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    if-eqz p0, :cond_23

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const p1, 0x1eaef984

    if-ne p0, p1, :cond_23

    const/4 p0, 0x1

    :goto_15
    move v10, p0

    .line 316274
    iget-object p0, v0, LX/8rQ;->c:LX/8rL;

    iget-object p1, v0, LX/8rQ;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v3, v10}, LX/8rL;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;Z)LX/8rJ;

    move-result-object v3

    .line 316275
    if-eqz v3, :cond_22

    .line 316276
    add-int p0, v7, v9

    invoke-virtual {v4, v3, v7, p0, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 316277
    if-nez v10, :cond_22

    .line 316278
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v10, 0x1

    invoke-direct {v3, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/2addr v9, v7

    invoke-virtual {v4, v3, v7, v9, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_14

    :cond_23
    const/4 p0, 0x0

    goto :goto_15

    :cond_24
    move v1, v3

    goto/16 :goto_11

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 316279
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1nM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 316280
    invoke-static {p0, p1, p2}, LX/1nM;->c(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/1nM;->a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v1

    .line 316281
    if-nez v1, :cond_0

    .line 316282
    const/4 v0, 0x0

    .line 316283
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1zR;

    invoke-direct {v0, v1}, LX/1zR;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
