.class public LX/1Q2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pe;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3JJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244279
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Q2;->a:Ljava/util/Map;

    .line 244280
    return-void
.end method

.method public static a(LX/0QB;)LX/1Q2;
    .locals 1

    .prologue
    .line 244275
    new-instance v0, LX/1Q2;

    invoke-direct {v0}, LX/1Q2;-><init>()V

    .line 244276
    move-object v0, v0

    .line 244277
    return-object v0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 244246
    if-nez p0, :cond_1

    .line 244247
    :cond_0
    :goto_0
    return-object v0

    .line 244248
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 244249
    if-eqz v1, :cond_0

    .line 244250
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2

    .prologue
    .line 244271
    invoke-static {p1}, LX/1Q2;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 244272
    if-nez v0, :cond_0

    .line 244273
    :goto_0
    return-void

    .line 244274
    :cond_0
    iget-object v1, p0, LX/1Q2;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 244267
    invoke-static {p1}, LX/1Q2;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 244268
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 244269
    :cond_0
    :goto_0
    return-void

    .line 244270
    :cond_1
    iget-object v1, p0, LX/1Q2;->a:Ljava/util/Map;

    new-instance v2, LX/3JJ;

    invoke-direct {v2, p1, p2}, LX/3JJ;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 3

    .prologue
    .line 244254
    invoke-static {p1}, LX/1Q2;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 244255
    if-nez v0, :cond_1

    .line 244256
    :cond_0
    :goto_0
    return-void

    .line 244257
    :cond_1
    iget-object v1, p0, LX/1Q2;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3JJ;

    .line 244258
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/3JJ;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 244259
    iget-object v1, v0, LX/3JJ;->b:Landroid/view/View;

    instance-of v1, v1, LX/3FP;

    if-eqz v1, :cond_2

    .line 244260
    iget-object v1, v0, LX/3JJ;->b:Landroid/view/View;

    check-cast v1, LX/3FP;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/3FP;->setShowLiveCommentDialogFragment(Z)V

    .line 244261
    :cond_2
    iget-object v1, v0, LX/3JJ;->b:Landroid/view/View;

    instance-of v1, v1, LX/3JH;

    if-eqz v1, :cond_3

    .line 244262
    iget-object v0, v0, LX/3JJ;->b:Landroid/view/View;

    check-cast v0, LX/3JH;

    .line 244263
    iget-object v1, v0, LX/3JH;->b:LX/7gP;

    move-object v0, v1

    .line 244264
    iget-object v1, v0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v1}, LX/7Kf;->callOnClick()Z

    .line 244265
    goto :goto_0

    .line 244266
    :cond_3
    iget-object v0, v0, LX/3JJ;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 244251
    if-eqz p1, :cond_0

    .line 244252
    invoke-static {p1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 244253
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Q2;->a:Ljava/util/Map;

    invoke-static {p1}, LX/1Q2;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
