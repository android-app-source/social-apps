.class public final LX/0Rm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final predicate:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 60453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60454
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    iput-object v0, p0, LX/0Rm;->predicate:LX/0Rl;

    .line 60455
    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 60446
    iget-object v0, p0, LX/0Rm;->predicate:LX/0Rl;

    invoke-interface {v0, p1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 60449
    instance-of v0, p1, LX/0Rm;

    if-eqz v0, :cond_0

    .line 60450
    check-cast p1, LX/0Rm;

    .line 60451
    iget-object v0, p0, LX/0Rm;->predicate:LX/0Rl;

    iget-object v1, p1, LX/0Rm;->predicate:LX/0Rl;

    invoke-interface {v0, v1}, LX/0Rl;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 60452
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 60448
    iget-object v0, p0, LX/0Rm;->predicate:LX/0Rl;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60447
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Predicates.not("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0Rm;->predicate:LX/0Rl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
