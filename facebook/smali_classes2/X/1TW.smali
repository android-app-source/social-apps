.class public LX/1TW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 251884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251885
    iput-object p1, p0, LX/1TW;->a:LX/0Ot;

    .line 251886
    iput-object p2, p0, LX/1TW;->b:LX/0Ot;

    .line 251887
    return-void
.end method

.method public static a(LX/0QB;)LX/1TW;
    .locals 5

    .prologue
    .line 251870
    const-class v1, LX/1TW;

    monitor-enter v1

    .line 251871
    :try_start_0
    sget-object v0, LX/1TW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 251872
    sput-object v2, LX/1TW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251873
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251874
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 251875
    new-instance v3, LX/1TW;

    const/16 v4, 0x2022

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x201a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1TW;-><init>(LX/0Ot;LX/0Ot;)V

    .line 251876
    move-object v0, v3

    .line 251877
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 251878
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251879
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 251880
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 251881
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    iget-object v1, p0, LX/1TW;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251882
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    iget-object v1, p0, LX/1TW;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251883
    return-void
.end method
