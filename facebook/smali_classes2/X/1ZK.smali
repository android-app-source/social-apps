.class public LX/1ZK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ZL;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1SP;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0bW;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/0Or;LX/0bW;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1SP;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "LX/0bW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274504
    iput-object p1, p0, LX/1ZK;->a:LX/0Or;

    .line 274505
    iput-object p2, p0, LX/1ZK;->b:Landroid/content/Context;

    .line 274506
    iput-object p3, p0, LX/1ZK;->c:Lcom/facebook/content/SecureContextHelper;

    .line 274507
    iput-object p4, p0, LX/1ZK;->d:Ljava/util/concurrent/Executor;

    .line 274508
    iput-object p5, p0, LX/1ZK;->e:LX/0Or;

    .line 274509
    iput-object p6, p0, LX/1ZK;->f:LX/0bW;

    .line 274510
    return-void
.end method

.method public static b(LX/0QB;)LX/1ZK;
    .locals 7

    .prologue
    .line 274513
    new-instance v0, LX/1ZK;

    const/16 v1, 0xf5d

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0xa

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v6

    check-cast v6, LX/0bW;

    invoke-direct/range {v0 .. v6}, LX/1ZK;-><init>(LX/0Or;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/0Or;LX/0bW;)V

    .line 274514
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 274511
    iget-object v0, p0, LX/1ZK;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/1ZM;

    invoke-direct {v1, p0}, LX/1ZM;-><init>(LX/1ZK;)V

    iget-object v2, p0, LX/1ZK;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 274512
    return-void
.end method
