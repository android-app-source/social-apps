.class public LX/18Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/18R;

.field private static volatile i:LX/18Q;


# instance fields
.field private b:Landroid/os/Handler;

.field public final c:LX/0SG;

.field public final d:LX/18S;

.field public final e:LX/03V;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;",
            "LX/18R;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;",
            "Lcom/facebook/megaphone/data/MegaphoneStore$MegaphoneUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 206516
    new-instance v0, LX/18R;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/18R;-><init>(LX/3TD;J)V

    sput-object v0, LX/18Q;->a:LX/18R;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/18S;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206504
    iput-object p1, p0, LX/18Q;->c:LX/0SG;

    .line 206505
    iput-object p2, p0, LX/18Q;->d:LX/18S;

    .line 206506
    iput-object p3, p0, LX/18Q;->e:LX/03V;

    .line 206507
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/18Q;->f:Ljava/util/Map;

    .line 206508
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    .line 206509
    const/4 p1, 0x0

    .line 206510
    instance-of p2, v0, LX/18d;

    if-nez p2, :cond_0

    instance-of p2, v0, LX/18f;

    if-eqz p2, :cond_1

    .line 206511
    :cond_0
    :goto_0
    move-object p1, v0

    .line 206512
    move-object v0, p1

    .line 206513
    iput-object v0, p0, LX/18Q;->g:LX/0Xu;

    .line 206514
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-static {v0}, LX/0P9;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/18Q;->h:Ljava/util/Set;

    .line 206515
    return-void

    :cond_1
    new-instance p2, LX/18d;

    invoke-direct {p2, v0, p1}, LX/18d;-><init>(LX/0Xu;Ljava/lang/Object;)V

    move-object v0, p2

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/18Q;
    .locals 13

    .prologue
    .line 206481
    sget-object v0, LX/18Q;->i:LX/18Q;

    if-nez v0, :cond_1

    .line 206482
    const-class v1, LX/18Q;

    monitor-enter v1

    .line 206483
    :try_start_0
    sget-object v0, LX/18Q;->i:LX/18Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206484
    if-eqz v2, :cond_0

    .line 206485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 206486
    new-instance v6, LX/18Q;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    .line 206487
    new-instance v7, LX/18S;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v9

    check-cast v9, LX/0Wd;

    .line 206488
    new-instance v11, LX/18T;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v10

    check-cast v10, LX/0sO;

    invoke-direct {v11, v10}, LX/18T;-><init>(LX/0sO;)V

    .line 206489
    move-object v10, v11

    .line 206490
    check-cast v10, LX/18T;

    .line 206491
    new-instance v12, LX/18U;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-direct {v12, v11}, LX/18U;-><init>(LX/0SG;)V

    .line 206492
    move-object v11, v12

    .line 206493
    check-cast v11, LX/18U;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v12

    check-cast v12, LX/18V;

    invoke-direct/range {v7 .. v12}, LX/18S;-><init>(Landroid/content/res/Resources;LX/0Wd;LX/18T;LX/18U;LX/18V;)V

    .line 206494
    move-object v4, v7

    .line 206495
    check-cast v4, LX/18S;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v3, v4, v5}, LX/18Q;-><init>(LX/0SG;LX/18S;LX/03V;)V

    .line 206496
    move-object v0, v6

    .line 206497
    sput-object v0, LX/18Q;->i:LX/18Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206498
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206499
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206500
    :cond_1
    sget-object v0, LX/18Q;->i:LX/18Q;

    return-object v0

    .line 206501
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206502
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/18Q;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 206517
    iget-object v0, p0, LX/18Q;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 206518
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/18Q;->b:Landroid/os/Handler;

    .line 206519
    :cond_0
    iget-object v0, p0, LX/18Q;->b:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;LX/3TD;J)V
    .locals 3
    .param p2    # LX/3TD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 206475
    if-eqz p2, :cond_0

    .line 206476
    iget-object v0, p2, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v0, v0

    .line 206477
    if-nez v0, :cond_1

    .line 206478
    :cond_0
    :goto_0
    return-void

    .line 206479
    :cond_1
    iget-object v0, p0, LX/18Q;->f:Ljava/util/Map;

    new-instance v1, LX/18R;

    invoke-direct {v1, p2, p3, p4}, LX/18R;-><init>(LX/3TD;J)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206480
    invoke-virtual {p0, p1}, LX/18Q;->d(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V
    .locals 2

    .prologue
    .line 206471
    iget-object v0, p0, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 206472
    iget-object v0, p0, LX/18Q;->f:Ljava/util/Map;

    sget-object v1, LX/18Q;->a:LX/18R;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206473
    invoke-virtual {p0, p1}, LX/18Q;->d(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V

    .line 206474
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 206468
    iget-object v0, p0, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 206469
    iget-object v0, p0, LX/18Q;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 206470
    return-void
.end method

.method public final d(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V
    .locals 4

    .prologue
    .line 206465
    iget-object v0, p0, LX/18Q;->g:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ct;

    .line 206466
    invoke-static {p0}, LX/18Q;->a(LX/18Q;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/facebook/megaphone/data/MegaphoneStore$2;

    invoke-direct {v3, p0, v0}, Lcom/facebook/megaphone/data/MegaphoneStore$2;-><init>(LX/18Q;LX/1Ct;)V

    const v0, -0x7f4a84f1

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 206467
    :cond_0
    return-void
.end method
