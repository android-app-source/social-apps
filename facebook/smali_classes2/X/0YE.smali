.class public LX/0YE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0YE;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80770
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0YE;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 80771
    return-void
.end method

.method public static a(LX/0QB;)LX/0YE;
    .locals 3

    .prologue
    .line 80772
    sget-object v0, LX/0YE;->b:LX/0YE;

    if-nez v0, :cond_1

    .line 80773
    const-class v1, LX/0YE;

    monitor-enter v1

    .line 80774
    :try_start_0
    sget-object v0, LX/0YE;->b:LX/0YE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80775
    if-eqz v2, :cond_0

    .line 80776
    :try_start_1
    new-instance v0, LX/0YE;

    invoke-direct {v0}, LX/0YE;-><init>()V

    .line 80777
    move-object v0, v0

    .line 80778
    sput-object v0, LX/0YE;->b:LX/0YE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80779
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80780
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80781
    :cond_1
    sget-object v0, LX/0YE;->b:LX/0YE;

    return-object v0

    .line 80782
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 80784
    iget-object v0, p0, LX/0YE;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 80785
    iget-object v0, p0, LX/0YE;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 80786
    return-void
.end method
