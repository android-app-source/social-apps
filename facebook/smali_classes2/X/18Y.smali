.class public LX/18Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/18Z;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 206656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206657
    iput-boolean p1, p0, LX/18Y;->a:Z

    .line 206658
    return-void
.end method


# virtual methods
.method public final a(LX/2Vj;LX/15w;LX/11M;)LX/2WM;
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 206660
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 206661
    sget-object v0, LX/2WM;->a:LX/2WM;

    .line 206662
    :goto_0
    return-object v0

    .line 206663
    :cond_0
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 206664
    const-string v0, "code"

    invoke-virtual {p2}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 206665
    new-instance v0, LX/28E;

    const-string v1, "Invalid format. \'code\' node not found."

    invoke-virtual {p2}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 206666
    :cond_1
    invoke-virtual {p2, v2}, LX/15w;->a(I)I

    move-result v1

    .line 206667
    if-ne v1, v2, :cond_2

    .line 206668
    new-instance v0, LX/28E;

    const-string v1, "Invalid format. \'code\' value not found."

    invoke-virtual {p2}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 206669
    :cond_2
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 206670
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 206671
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 206672
    const-string v0, "body"

    invoke-virtual {p2}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 206673
    new-instance v0, LX/28E;

    const-string v1, "Invalid format. \'body\' node not found."

    invoke-virtual {p2}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 206674
    :cond_3
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 206675
    new-instance v2, LX/2WK;

    iget-boolean v0, p0, LX/18Y;->a:Z

    invoke-direct {v2, p2, v0}, LX/2WK;-><init>(LX/15w;Z)V

    .line 206676
    new-instance v0, LX/2WL;

    invoke-direct {v0, p3, v1, v3, v2}, LX/2WL;-><init>(LX/11M;ILjava/lang/String;LX/15w;)V

    invoke-static {p3, v0}, LX/11M;->a(LX/11M;LX/1pF;)V

    .line 206677
    new-instance v0, LX/2WM;

    invoke-direct {v0, v1, v3, v3, v2}, LX/2WM;-><init>(ILjava/util/List;Ljava/lang/String;LX/15w;)V

    goto :goto_0
.end method

.method public final a(LX/0n9;)V
    .locals 0

    .prologue
    .line 206659
    return-void
.end method
