.class public LX/16S;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/16S;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/16U;

.field public final c:LX/16W;

.field public final d:LX/0tQ;

.field public final e:Landroid/os/Handler;

.field public f:LX/2fs;

.field public g:Ljava/lang/String;

.field public h:LX/0hs;


# direct methods
.method public constructor <init>(LX/0tQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/16U;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 185315
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 185316
    new-instance v0, LX/16W;

    invoke-direct {v0, p0}, LX/16W;-><init>(LX/16S;)V

    iput-object v0, p0, LX/16S;->c:LX/16W;

    .line 185317
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/16S;->e:Landroid/os/Handler;

    .line 185318
    iput-object p2, p0, LX/16S;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 185319
    iput-object p1, p0, LX/16S;->d:LX/0tQ;

    .line 185320
    iput-object p3, p0, LX/16S;->b:LX/16U;

    .line 185321
    return-void
.end method

.method public static a(LX/0QB;)LX/16S;
    .locals 6

    .prologue
    .line 185250
    sget-object v0, LX/16S;->i:LX/16S;

    if-nez v0, :cond_1

    .line 185251
    const-class v1, LX/16S;

    monitor-enter v1

    .line 185252
    :try_start_0
    sget-object v0, LX/16S;->i:LX/16S;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 185253
    if-eqz v2, :cond_0

    .line 185254
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 185255
    new-instance p0, LX/16S;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v5

    check-cast v5, LX/16U;

    invoke-direct {p0, v3, v4, v5}, LX/16S;-><init>(LX/0tQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/16U;)V

    .line 185256
    move-object v0, p0

    .line 185257
    sput-object v0, LX/16S;->i:LX/16S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185258
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 185259
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 185260
    :cond_1
    sget-object v0, LX/16S;->i:LX/16S;

    return-object v0

    .line 185261
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 185262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 185311
    iput-object v0, p0, LX/16S;->g:Ljava/lang/String;

    .line 185312
    iput-object v0, p0, LX/16S;->f:LX/2fs;

    .line 185313
    iget-object v0, p0, LX/16S;->b:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/16S;->c:LX/16W;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 185314
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185302
    iget-object v0, p0, LX/16S;->d:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/16S;->d:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185303
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 185304
    :goto_0
    return-object v0

    .line 185305
    :cond_1
    iget-object v0, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-ne v0, v1, :cond_3

    .line 185306
    iget-object v0, p0, LX/16S;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1vE;->f:LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185307
    iget-object v0, p0, LX/16S;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->f:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 185308
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    .line 185309
    :cond_2
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0

    .line 185310
    :cond_3
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 185265
    const-class v0, LX/0f0;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f0;

    .line 185266
    iget-object v1, p0, LX/16S;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 185267
    check-cast p2, Ljava/lang/String;

    .line 185268
    iget-object v1, p0, LX/16S;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 185269
    iput-object p2, p0, LX/16S;->g:Ljava/lang/String;

    .line 185270
    iget-object v1, p0, LX/16S;->b:LX/16U;

    const-class v2, LX/1ub;

    iget-object v5, p0, LX/16S;->c:LX/16W;

    invoke-virtual {v1, v2, v5}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 185271
    :cond_0
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/16S;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/16S;->h:LX/0hs;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/16S;->h:LX/0hs;

    .line 185272
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 185273
    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p0, LX/16S;->f:LX/2fs;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/16S;->f:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    .line 185274
    sget-object v2, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v1, v2, :cond_7

    :cond_2
    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 185275
    if-nez v1, :cond_4

    .line 185276
    :cond_3
    invoke-direct {p0}, LX/16S;->d()V

    .line 185277
    iget-object v0, p0, LX/16S;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 185278
    :goto_1
    return-void

    .line 185279
    :cond_4
    const-class v1, LX/0f3;

    invoke-interface {v0, v1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f3;

    .line 185280
    if-nez v0, :cond_5

    .line 185281
    iget-object v0, p0, LX/16S;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->f:LX/0Tn;

    invoke-interface {v0, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 185282
    invoke-direct {p0}, LX/16S;->d()V

    goto :goto_1

    .line 185283
    :cond_5
    invoke-interface {v0}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    if-ne v1, v2, :cond_6

    .line 185284
    invoke-direct {p0}, LX/16S;->d()V

    goto :goto_1

    .line 185285
    :cond_6
    new-instance v1, LX/0hs;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/16S;->h:LX/0hs;

    .line 185286
    iget-object v1, p0, LX/16S;->h:LX/0hs;

    const/4 v2, -0x1

    .line 185287
    iput v2, v1, LX/0hs;->t:I

    .line 185288
    iget-object v1, p0, LX/16S;->h:LX/0hs;

    .line 185289
    sget-object v2, LX/Cv2;->a:[I

    iget-object v5, p0, LX/16S;->d:LX/0tQ;

    invoke-virtual {v5}, LX/0tQ;->m()LX/2qY;

    move-result-object v5

    invoke-virtual {v5}, LX/2qY;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 185290
    const v2, 0x7f080d39

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_2
    move-object v2, v2

    .line 185291
    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 185292
    iget-object v1, p0, LX/16S;->h:LX/0hs;

    const v2, 0x7f080d3d

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 185293
    sget-object v1, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget-object v2, p0, LX/16S;->h:LX/0hs;

    invoke-interface {v0, v1, v2}, LX/0f3;->a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V

    .line 185294
    sput-boolean v3, LX/Cv5;->a:Z

    .line 185295
    iget-object v0, p0, LX/16S;->h:LX/0hs;

    new-instance v1, LX/Cv1;

    invoke-direct {v1, p0}, LX/Cv1;-><init>(LX/16S;)V

    .line 185296
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 185297
    iput-object v4, p0, LX/16S;->g:Ljava/lang/String;

    .line 185298
    iput-object v4, p0, LX/16S;->f:LX/2fs;

    .line 185299
    iget-object v0, p0, LX/16S;->b:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/16S;->c:LX/16W;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 185300
    :pswitch_0
    const v2, 0x7f080d38

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    .line 185301
    :pswitch_1
    const v2, 0x7f080d3a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185264
    const-string v0, "4163"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185263
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
