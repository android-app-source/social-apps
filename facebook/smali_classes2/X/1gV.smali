.class public final LX/1gV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1gW;


# instance fields
.field public final synthetic a:LX/0WS;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>(LX/0WS;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 294399
    iput-object p1, p0, LX/1gV;->a:LX/0WS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294400
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    .line 294401
    iput-boolean v2, p0, LX/1gV;->c:Z

    .line 294402
    iput-boolean v2, p0, LX/1gV;->d:Z

    return-void
.end method

.method private static a(LX/1gV;Ljava/util/Map;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 294379
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 294380
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v4, v0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 294381
    :try_start_0
    iget-boolean v0, p0, LX/1gV;->c:Z

    if-eqz v0, :cond_0

    .line 294382
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 294383
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 294384
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 294385
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 294386
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 294387
    sget-object v6, LX/0WS;->a:Ljava/lang/Object;

    if-ne v0, v6, :cond_2

    .line 294388
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 294389
    :goto_1
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 294390
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 294391
    :cond_2
    :try_start_1
    invoke-static {v0}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LX/1gV;->a:LX/0WS;

    iget-object v7, v7, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 294392
    iget-object v6, p0, LX/1gV;->a:LX/0WS;

    iget-object v6, v6, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 294393
    :cond_3
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v1, v0, LX/0WS;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v5, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 294394
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294395
    iput-boolean v2, p0, LX/1gV;->c:Z

    .line 294396
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 294397
    return-object v3

    :cond_4
    move v0, v2

    .line 294398
    goto :goto_2
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 294366
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 294367
    const-string v1, "commit is called on the main thread."

    .line 294368
    if-ne p1, v0, :cond_1

    .line 294369
    const-string v2, "LightSharedPreferencesImpl"

    invoke-static {v2, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 294370
    :cond_0
    invoke-static {p0}, LX/1gV;->e(LX/1gV;)Ljava/util/Map;

    move-result-object v1

    .line 294371
    :try_start_0
    invoke-static {p0, v1}, LX/1gV;->a(LX/1gV;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    .line 294372
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 294373
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    invoke-static {v0, v1}, LX/0WS;->a$redex0(LX/0WS;Ljava/util/Set;)V

    .line 294374
    invoke-static {p0}, LX/1gV;->d(LX/1gV;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 294375
    invoke-static {p0}, LX/1gV;->g(LX/1gV;)V

    .line 294376
    :goto_0
    return v0

    .line 294377
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294378
    :cond_2
    invoke-static {p0}, LX/1gV;->g(LX/1gV;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/1gV;->g(LX/1gV;)V

    throw v0
.end method

.method public static d(LX/1gV;)Z
    .locals 4

    .prologue
    .line 294351
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294352
    const/4 v0, 0x1

    .line 294353
    :goto_0
    return v0

    .line 294354
    :cond_0
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v1, v0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 294355
    :try_start_0
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 294356
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, LX/1gV;->a:LX/0WS;

    iget-object v2, v2, LX/0WS;->i:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 294357
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294358
    :try_start_1
    iget-object v1, p0, LX/1gV;->a:LX/0WS;

    iget-object v2, v1, LX/0WS;->f:LX/0WR;

    invoke-static {v0}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-virtual {v2, v1}, LX/0WR;->b(Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 294359
    const/4 v1, 0x1

    .line 294360
    :goto_1
    move v0, v1

    .line 294361
    goto :goto_0

    .line 294362
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 294363
    :catch_0
    move-exception v1

    .line 294364
    const-string v2, "LightSharedPreferencesImpl"

    const-string v3, "Commit to disk failed."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 294365
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static declared-synchronized e(LX/1gV;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294346
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1gV;->d:Z

    if-eqz v0, :cond_0

    .line 294347
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to freeze an editor that is already frozen!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 294349
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1gV;->d:Z

    .line 294350
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 294343
    iget-boolean v0, p0, LX/1gV;->d:Z

    if-eqz v0, :cond_0

    .line 294344
    new-instance v0, Ljava/util/ConcurrentModificationException;

    const-string v1, "Editors shouldn\'t be modified during commit!"

    invoke-direct {v0, v1}, Ljava/util/ConcurrentModificationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294345
    :cond_0
    return-void
.end method

.method private static declared-synchronized g(LX/1gV;)V
    .locals 1

    .prologue
    .line 294340
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1gV;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294341
    monitor-exit p0

    return-void

    .line 294342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/1gW;
    .locals 1

    .prologue
    .line 294337
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294338
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1gV;->c:Z

    .line 294339
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/1gW;
    .locals 3

    .prologue
    .line 294403
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294404
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/0WS;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294405
    return-object p0
.end method

.method public final a(Ljava/lang/String;F)LX/1gW;
    .locals 3

    .prologue
    .line 294334
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294335
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294336
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)LX/1gW;
    .locals 3

    .prologue
    .line 294331
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294332
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294333
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)LX/1gW;
    .locals 4

    .prologue
    .line 294328
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294329
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294330
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;
    .locals 3

    .prologue
    .line 294323
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294324
    if-nez p2, :cond_0

    .line 294325
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/0WS;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294326
    :goto_0
    return-object p0

    .line 294327
    :cond_0
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)LX/1gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1gW;"
        }
    .end annotation

    .prologue
    .line 294318
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294319
    if-nez p2, :cond_0

    .line 294320
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/0WS;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294321
    :goto_0
    return-object p0

    .line 294322
    :cond_0
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)LX/1gW;
    .locals 3

    .prologue
    .line 294315
    invoke-direct {p0}, LX/1gV;->f()V

    .line 294316
    iget-object v0, p0, LX/1gV;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294317
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 294306
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget v0, v0, LX/0WS;->g:I

    invoke-direct {p0, v0}, LX/1gV;->a(I)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 294307
    invoke-static {p0}, LX/1gV;->e(LX/1gV;)Ljava/util/Map;

    move-result-object v0

    .line 294308
    :try_start_0
    invoke-static {p0, v0}, LX/1gV;->a(LX/1gV;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 294309
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294310
    iget-object v1, p0, LX/1gV;->a:LX/0WS;

    invoke-static {v1, v0}, LX/0WS;->a$redex0(LX/0WS;Ljava/util/Set;)V

    .line 294311
    iget-object v0, p0, LX/1gV;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$EditorImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$EditorImpl$1;-><init>(LX/1gV;)V

    const v2, 0x563a1682

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294312
    :cond_0
    invoke-static {p0}, LX/1gV;->g(LX/1gV;)V

    .line 294313
    return-void

    .line 294314
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/1gV;->g(LX/1gV;)V

    throw v0
.end method
