.class public LX/1Ve;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/14w;

.field public final b:LX/1VK;

.field private final c:LX/17Q;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(LX/14w;LX/1VK;LX/17Q;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266022
    iput-object p1, p0, LX/1Ve;->a:LX/14w;

    .line 266023
    iput-object p2, p0, LX/1Ve;->b:LX/1VK;

    .line 266024
    iput-object p3, p0, LX/1Ve;->c:LX/17Q;

    .line 266025
    iput-object p4, p0, LX/1Ve;->d:LX/0Zb;

    .line 266026
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ve;
    .locals 7

    .prologue
    .line 266000
    const-class v1, LX/1Ve;

    monitor-enter v1

    .line 266001
    :try_start_0
    sget-object v0, LX/1Ve;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266002
    sput-object v2, LX/1Ve;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266003
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266004
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266005
    new-instance p0, LX/1Ve;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    const-class v4, LX/1VK;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1VK;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Ve;-><init>(LX/14w;LX/1VK;LX/17Q;LX/0Zb;)V

    .line 266006
    move-object v0, p0

    .line 266007
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266008
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Ve;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266009
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266011
    if-nez p1, :cond_0

    .line 266012
    :goto_0
    return-void

    .line 266013
    :cond_0
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 266014
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 266015
    const/4 v1, 0x0

    .line 266016
    :goto_1
    move-object v0, v1

    .line 266017
    iget-object v1, p0, LX/1Ve;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "inline_composer_click"

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v1, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string p1, "native_newsfeed"

    .line 266018
    iput-object p1, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 266019
    move-object v1, v1

    .line 266020
    goto :goto_1
.end method
