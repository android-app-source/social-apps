.class public LX/1sJ;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 334550
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 334551
    return-void
.end method

.method public static a(LX/0Or;LX/0Or;)LX/1sz;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ExplicitComplexProvider"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1sK;",
            ">;",
            "LX/0Or",
            "<",
            "LX/49T;",
            ">;)",
            "LX/1sz;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 334552
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 334553
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sz;

    .line 334554
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sz;

    goto :goto_0
.end method

.method public static a()LX/3xs;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 334555
    new-instance v0, LX/3xs;

    invoke-direct {v0}, LX/3xs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 334556
    return-void
.end method
