.class public LX/1Kr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Kq;


# instance fields
.field private final a:LX/1AQ;

.field private final b:J

.field private c:J


# direct methods
.method public constructor <init>(ILX/1AQ;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233185
    const-wide/16 v4, -0x1

    iput-wide v4, p0, LX/1Kr;->c:J

    .line 233186
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Interval %s is invalid."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 233187
    int-to-long v0, p1

    iput-wide v0, p0, LX/1Kr;->b:J

    .line 233188
    iput-object p2, p0, LX/1Kr;->a:LX/1AQ;

    .line 233189
    return-void

    :cond_0
    move v0, v2

    .line 233190
    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    .line 233191
    iget-object v0, p0, LX/1Kr;->a:LX/1AQ;

    .line 233192
    iget-wide v6, v0, LX/1AQ;->c:J

    move-wide v0, v6

    .line 233193
    iget-wide v2, p0, LX/1Kr;->c:J

    iget-wide v4, p0, LX/1Kr;->b:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/1Kr;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 233194
    :cond_0
    iget-object v0, p0, LX/1Kr;->a:LX/1AQ;

    .line 233195
    iget-wide v6, v0, LX/1AQ;->c:J

    move-wide v0, v6

    .line 233196
    iput-wide v0, p0, LX/1Kr;->c:J

    .line 233197
    const/4 v0, 0x0

    .line 233198
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
