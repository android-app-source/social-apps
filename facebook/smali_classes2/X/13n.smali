.class public LX/13n;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/13n;


# instance fields
.field public a:Landroid/app/Activity;

.field private b:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177467
    const/4 v0, 0x0

    iput v0, p0, LX/13n;->b:I

    .line 177468
    return-void
.end method

.method public static a(LX/0QB;)LX/13n;
    .locals 3

    .prologue
    .line 177472
    sget-object v0, LX/13n;->c:LX/13n;

    if-nez v0, :cond_1

    .line 177473
    const-class v1, LX/13n;

    monitor-enter v1

    .line 177474
    :try_start_0
    sget-object v0, LX/13n;->c:LX/13n;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177475
    if-eqz v2, :cond_0

    .line 177476
    :try_start_1
    new-instance v0, LX/13n;

    invoke-direct {v0}, LX/13n;-><init>()V

    .line 177477
    move-object v0, v0

    .line 177478
    sput-object v0, LX/13n;->c:LX/13n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177479
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177480
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177481
    :cond_1
    sget-object v0, LX/13n;->c:LX/13n;

    return-object v0

    .line 177482
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 177484
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/13n;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/13n;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177485
    monitor-exit p0

    return-void

    .line 177486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 177469
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/13n;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/13n;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177470
    monitor-exit p0

    return-void

    .line 177471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
