.class public LX/1lu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1CJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 312503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312504
    iput-object p1, p0, LX/1lu;->a:LX/0Ot;

    .line 312505
    iput-object p2, p0, LX/1lu;->b:LX/0Ot;

    .line 312506
    iput-object p3, p0, LX/1lu;->c:LX/0Ot;

    .line 312507
    return-void
.end method

.method public static a(LX/0QB;)LX/1lu;
    .locals 1

    .prologue
    .line 312502
    invoke-static {p0}, LX/1lu;->b(LX/0QB;)LX/1lu;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1lu;
    .locals 4

    .prologue
    .line 312483
    new-instance v0, LX/1lu;

    const/16 v1, 0x3cf

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xf39

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3c3

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1lu;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 312484
    return-object v0
.end method


# virtual methods
.method public final a(LX/1gl;LX/0qm;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 312485
    iget-object v0, p0, LX/1lu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v4

    .line 312486
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312487
    :cond_0
    :goto_0
    return-void

    .line 312488
    :cond_1
    invoke-interface {p1}, LX/1gl;->a()V

    .line 312489
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 312490
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v6

    .line 312491
    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/1lu;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/1EZ;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 312492
    iget-object v0, p0, LX/1lu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    .line 312493
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 312494
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-interface {p1, v8, v9, v1}, LX/1gl;->a(JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 312495
    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v7

    .line 312496
    invoke-virtual {p2, v6}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {p2, v7}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 312497
    :goto_3
    iget-object v1, p0, LX/1lu;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1CJ;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v0

    .line 312498
    iput-boolean v3, v0, LX/4Zb;->a:Z

    .line 312499
    goto :goto_2

    .line 312500
    :cond_4
    invoke-interface {p1}, LX/1gl;->b()V

    goto/16 :goto_0

    .line 312501
    :cond_5
    invoke-virtual {p2, v1, v7}, LX/0qm;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_3
.end method
