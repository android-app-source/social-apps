.class public LX/0hx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/0hx;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/0gh;

.field private final d:LX/0Vw;

.field private e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119367
    const-class v0, LX/0hx;

    sput-object v0, LX/0hx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0gh;LX/0Vw;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 119361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119362
    iput-object p1, p0, LX/0hx;->b:LX/0Zb;

    .line 119363
    iput-object p2, p0, LX/0hx;->c:LX/0gh;

    .line 119364
    iput-object p3, p0, LX/0hx;->d:LX/0Vw;

    .line 119365
    iput-object p4, p0, LX/0hx;->e:Landroid/content/res/Resources;

    .line 119366
    return-void
.end method

.method public static a(LX/0QB;)LX/0hx;
    .locals 7

    .prologue
    .line 119348
    sget-object v0, LX/0hx;->f:LX/0hx;

    if-nez v0, :cond_1

    .line 119349
    const-class v1, LX/0hx;

    monitor-enter v1

    .line 119350
    :try_start_0
    sget-object v0, LX/0hx;->f:LX/0hx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 119351
    if-eqz v2, :cond_0

    .line 119352
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 119353
    new-instance p0, LX/0hx;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v4

    check-cast v4, LX/0gh;

    invoke-static {v0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v5

    check-cast v5, LX/0Vw;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0hx;-><init>(LX/0Zb;LX/0gh;LX/0Vw;Landroid/content/res/Resources;)V

    .line 119354
    move-object v0, p0

    .line 119355
    sput-object v0, LX/0hx;->f:LX/0hx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119356
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 119357
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119358
    :cond_1
    sget-object v0, LX/0hx;->f:LX/0hx;

    return-object v0

    .line 119359
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 119360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3zI;Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 119341
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "content"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "flags"

    invoke-virtual {p0}, LX/3zI;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 119342
    iput-wide p3, v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 119343
    if-eqz p1, :cond_0

    .line 119344
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 119345
    :cond_0
    if-eqz p2, :cond_1

    .line 119346
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 119347
    :cond_1
    return-object v0
.end method

.method public static a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 119340
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 119301
    iget-object v0, p0, LX/0hx;->c:LX/0gh;

    invoke-virtual {v0}, LX/0gh;->g()V

    .line 119302
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 119336
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 119337
    const-string v0, "progress_spinner_time"

    .line 119338
    iget-object v1, p0, LX/0hx;->d:LX/0Vw;

    invoke-virtual {v1, v0, p1, p2}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 119339
    :cond_0
    return-void
.end method

.method public final a(LX/3zI;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 119333
    const-wide/16 v0, -0x1

    invoke-static {p1, p2, p3, v0, v1}, LX/0hx;->a(LX/3zI;Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 119334
    iget-object v1, p0, LX/0hx;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 119335
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 119331
    const-string v0, "button"

    invoke-virtual {p0, p1, v0, p2}, LX/0hx;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119332
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 119321
    const-string v0, "click"

    .line 119322
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 119323
    iput-object p1, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 119324
    move-object v1, v1

    .line 119325
    iput-object p2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 119326
    move-object v1, v1

    .line 119327
    if-eqz p3, :cond_0

    .line 119328
    iput-object p3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 119329
    :cond_0
    iget-object v2, p0, LX/0hx;->b:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 119330
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 119317
    if-eqz p1, :cond_0

    const-string v0, "progress_spinner_modal"

    .line 119318
    :goto_0
    iget-object v1, p0, LX/0hx;->d:LX/0Vw;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 119319
    return-void

    .line 119320
    :cond_0
    const-string v0, "progress_spinner_non_modal"

    goto :goto_0
.end method

.method public final a(JLandroid/view/View;)Z
    .locals 1

    .prologue
    .line 119313
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 119314
    invoke-virtual {p0, p1, p2}, LX/0hx;->a(J)V

    .line 119315
    const/4 v0, 0x1

    .line 119316
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 119303
    const-string v0, "click"

    .line 119304
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 119305
    iput-object p1, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 119306
    move-object v1, v1

    .line 119307
    iput-object p2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 119308
    move-object v1, v1

    .line 119309
    if-eqz p3, :cond_0

    .line 119310
    iput-object p3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 119311
    :cond_0
    iget-object v2, p0, LX/0hx;->b:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 119312
    return-void
.end method
