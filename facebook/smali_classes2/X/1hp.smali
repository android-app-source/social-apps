.class public final LX/1hp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1hr;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1hr;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 297126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297127
    iput-object p1, p0, LX/1hp;->a:LX/0QB;

    .line 297128
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 297129
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1hp;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 297130
    packed-switch p2, :pswitch_data_0

    .line 297131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297132
    :pswitch_0
    invoke-static {p1}, LX/1hq;->a(LX/0QB;)LX/1hq;

    move-result-object v0

    .line 297133
    :goto_0
    return-object v0

    .line 297134
    :pswitch_1
    new-instance v0, LX/1hu;

    invoke-direct {v0}, LX/1hu;-><init>()V

    .line 297135
    move-object v0, v0

    .line 297136
    move-object v0, v0

    .line 297137
    goto :goto_0

    .line 297138
    :pswitch_2
    invoke-static {p1}, LX/1hv;->a(LX/0QB;)LX/1hv;

    move-result-object v0

    goto :goto_0

    .line 297139
    :pswitch_3
    new-instance v0, LX/1hw;

    const/16 v1, 0x387

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 p0, 0x15b4

    invoke-static {p1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const/16 p2, 0x13db

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/1hw;-><init>(LX/0Or;LX/0Or;LX/0Ot;)V

    .line 297140
    move-object v0, v0

    .line 297141
    goto :goto_0

    .line 297142
    :pswitch_4
    invoke-static {p1}, LX/1hx;->a(LX/0QB;)LX/1hx;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 297143
    const/4 v0, 0x5

    return v0
.end method
