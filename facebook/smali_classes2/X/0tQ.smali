.class public LX/0tQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0tQ;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Uh;

.field public final c:LX/0W3;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154588
    iput-object p1, p0, LX/0tQ;->a:LX/0ad;

    .line 154589
    iput-object p2, p0, LX/0tQ;->b:LX/0Uh;

    .line 154590
    iput-object p3, p0, LX/0tQ;->c:LX/0W3;

    .line 154591
    return-void
.end method

.method public static Q(LX/0tQ;)Z
    .locals 3

    .prologue
    .line 154592
    iget-object v0, p0, LX/0tQ;->b:LX/0Uh;

    const/16 v1, 0x226

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/0tQ;
    .locals 6

    .prologue
    .line 154593
    sget-object v0, LX/0tQ;->d:LX/0tQ;

    if-nez v0, :cond_1

    .line 154594
    const-class v1, LX/0tQ;

    monitor-enter v1

    .line 154595
    :try_start_0
    sget-object v0, LX/0tQ;->d:LX/0tQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154596
    if-eqz v2, :cond_0

    .line 154597
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154598
    new-instance p0, LX/0tQ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/0tQ;-><init>(LX/0ad;LX/0Uh;LX/0W3;)V

    .line 154599
    move-object v0, p0

    .line 154600
    sput-object v0, LX/0tQ;->d:LX/0tQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154601
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154602
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154603
    :cond_1
    sget-object v0, LX/0tQ;->d:LX/0tQ;

    return-object v0

    .line 154604
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154605
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final B()Z
    .locals 3

    .prologue
    .line 154606
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->d:S

    invoke-static {p0}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154607
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0tQ;->J()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final E()Z
    .locals 3

    .prologue
    .line 154608
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->P:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final G()Z
    .locals 3

    .prologue
    .line 154609
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->O:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final H()Z
    .locals 3

    .prologue
    .line 154610
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->e:S

    invoke-static {p0}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 154583
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154584
    iget-object v3, p0, LX/0tQ;->c:LX/0W3;

    sget-wide v5, LX/0X5;->cb:J

    invoke-interface {v3, v5, v6}, LX/0W4;->a(J)Z

    move-result v3

    move v1, v3

    .line 154585
    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final L()Z
    .locals 3

    .prologue
    .line 154611
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->V:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 154612
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->v:S

    invoke-static {p0}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 154613
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->K:S

    invoke-static {p0}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154586
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0tQ;->J()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->z:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154614
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0tQ;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/0tQ;->J()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->r:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final g()J
    .locals 6

    .prologue
    .line 154571
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-wide v2, LX/0wh;->o:J

    const-wide v4, 0x7fffffffffffffffL

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()J
    .locals 6

    .prologue
    .line 154572
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-wide v2, LX/0wh;->J:J

    const-wide/16 v4, 0x1f4

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final j()J
    .locals 6

    .prologue
    .line 154573
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-wide v2, LX/0wh;->L:J

    const-wide/32 v4, 0x1499700

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final l()J
    .locals 6

    .prologue
    .line 154574
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-wide v2, LX/0wh;->W:J

    const-wide/32 v4, 0xa4cb800

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final m()LX/2qY;
    .locals 3

    .prologue
    .line 154575
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-char v1, LX/0wh;->I:C

    sget-object v2, LX/2qY;->DOWNLOAD:LX/2qY;

    invoke-virtual {v2}, LX/2qY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2qY;->of(Ljava/lang/String;)LX/2qY;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 3

    .prologue
    .line 154576
    iget-object v0, p0, LX/0tQ;->a:LX/0ad;

    sget-short v1, LX/0wh;->w:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154577
    invoke-virtual {p0}, LX/0tQ;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0tQ;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->N:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final s()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154578
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final t()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154579
    invoke-virtual {p0}, LX/0tQ;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->T:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final u()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154580
    invoke-virtual {p0}, LX/0tQ;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->Q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final w()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154581
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->k:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final y()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154582
    invoke-virtual {p0}, LX/0tQ;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->j:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
