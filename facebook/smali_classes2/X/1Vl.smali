.class public LX/1Vl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/1Vr;

.field public final c:LX/17W;

.field public final d:LX/17U;

.field public final e:LX/1Vn;

.field public final f:LX/0ad;

.field public final g:LX/1Vo;


# direct methods
.method public constructor <init>(LX/1Vm;LX/1Vg;LX/17W;LX/17U;LX/1Vn;LX/0ad;LX/1Vo;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266390
    iput-object p1, p0, LX/1Vl;->a:LX/1Vm;

    .line 266391
    new-instance v0, LX/1Vp;

    invoke-direct {v0}, LX/1Vp;-><init>()V

    invoke-virtual {p2, v0}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    iput-object v0, p0, LX/1Vl;->b:LX/1Vr;

    .line 266392
    iput-object p3, p0, LX/1Vl;->c:LX/17W;

    .line 266393
    iput-object p4, p0, LX/1Vl;->d:LX/17U;

    .line 266394
    iput-object p5, p0, LX/1Vl;->e:LX/1Vn;

    .line 266395
    iput-object p6, p0, LX/1Vl;->f:LX/0ad;

    .line 266396
    iput-object p7, p0, LX/1Vl;->g:LX/1Vo;

    .line 266397
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vl;
    .locals 12

    .prologue
    .line 266398
    const-class v1, LX/1Vl;

    monitor-enter v1

    .line 266399
    :try_start_0
    sget-object v0, LX/1Vl;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266400
    sput-object v2, LX/1Vl;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266401
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266402
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266403
    new-instance v3, LX/1Vl;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v4

    check-cast v4, LX/1Vm;

    const-class v5, LX/1Vg;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1Vg;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/17U;->a(LX/0QB;)LX/17U;

    move-result-object v7

    check-cast v7, LX/17U;

    .line 266404
    new-instance v9, LX/1Vn;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-direct {v9, v8}, LX/1Vn;-><init>(LX/0Zb;)V

    .line 266405
    move-object v8, v9

    .line 266406
    check-cast v8, LX/1Vn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    .line 266407
    new-instance v11, LX/1Vo;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct {v11, v10}, LX/1Vo;-><init>(LX/0Uh;)V

    .line 266408
    move-object v10, v11

    .line 266409
    check-cast v10, LX/1Vo;

    invoke-direct/range {v3 .. v10}, LX/1Vl;-><init>(LX/1Vm;LX/1Vg;LX/17W;LX/17U;LX/1Vn;LX/0ad;LX/1Vo;)V

    .line 266410
    move-object v0, v3

    .line 266411
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266412
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266413
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1Vl;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 266415
    iget-object v0, p0, LX/1Vl;->b:LX/1Vr;

    invoke-virtual {v0}, LX/1Vr;->a()Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 266416
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266417
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266418
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 266419
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 266420
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 266421
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x1881156a

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 266422
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 266423
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
