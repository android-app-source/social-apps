.class public LX/1kH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:[Ljava/lang/String;

.field private static final c:Landroid/net/Uri;

.field private static final d:[Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 309383
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/1kH;->a:Landroid/net/Uri;

    .line 309384
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, LX/1kH;->c:Landroid/net/Uri;

    .line 309385
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "duration"

    aput-object v1, v0, v5

    sput-object v0, LX/1kH;->d:[Ljava/lang/String;

    .line 309386
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "date_added"

    aput-object v1, v0, v5

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "media_type"

    aput-object v1, v0, v6

    const-string v1, "width"

    aput-object v1, v0, v4

    const-string v1, "height"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, LX/1kH;->b:[Ljava/lang/String;

    .line 309387
    const-string v0, "%s = %d AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "_data"

    aput-object v2, v1, v6

    const-string v2, "_data"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kH;->e:Ljava/lang/String;

    .line 309388
    const-string v0, "%s = %d AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "_data"

    aput-object v2, v1, v6

    const-string v2, "_data"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kH;->f:Ljava/lang/String;

    .line 309389
    const-string v0, "(%s = %d or %s = %d) AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "media_type"

    aput-object v2, v1, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "_data"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "_data"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kH;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309380
    iput-object p1, p0, LX/1kH;->h:Landroid/content/Context;

    .line 309381
    iput-object p2, p0, LX/1kH;->i:LX/0Sh;

    .line 309382
    return-void
.end method

.method public static a(Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 309378
    const/16 v0, 0x9

    invoke-static {p0, v0}, LX/1kH;->a(Landroid/net/Uri;I)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/net/Uri;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 309339
    if-nez p0, :cond_0

    .line 309340
    :goto_0
    return v0

    .line 309341
    :cond_0
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 309342
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 309343
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 309344
    if-nez v2, :cond_1

    .line 309345
    :goto_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 309346
    :cond_1
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 309347
    :catch_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method

.method private a(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 309369
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    .line 309370
    :goto_0
    return v0

    .line 309371
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1kH;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/1kH;->c:Landroid/net/Uri;

    sget-object v2, LX/1kH;->d:[Ljava/lang/String;

    const-string v3, "%s = \'%s\'"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v7, "_data"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 309372
    if-nez v1, :cond_1

    move v0, v6

    .line 309373
    goto :goto_0

    .line 309374
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 309375
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 309376
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 309377
    :catch_0
    move v0, v6

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1kH;
    .locals 5

    .prologue
    .line 309358
    const-class v1, LX/1kH;

    monitor-enter v1

    .line 309359
    :try_start_0
    sget-object v0, LX/1kH;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 309360
    sput-object v2, LX/1kH;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 309361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 309363
    new-instance p0, LX/1kH;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/1kH;-><init>(Landroid/content/Context;LX/0Sh;)V

    .line 309364
    move-object v0, p0

    .line 309365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 309366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1kH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 309368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(I)LX/1po;
    .locals 1

    .prologue
    .line 309390
    packed-switch p0, :pswitch_data_0

    .line 309391
    :pswitch_0
    sget-object v0, LX/1po;->UNKNOWN:LX/1po;

    :goto_0
    return-object v0

    .line 309392
    :pswitch_1
    sget-object v0, LX/1po;->PHOTO:LX/1po;

    goto :goto_0

    .line 309393
    :pswitch_2
    sget-object v0, LX/1po;->VIDEO:LX/1po;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/1po;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 309350
    if-nez p0, :cond_0

    .line 309351
    sget-object v0, LX/1kH;->g:Ljava/lang/String;

    .line 309352
    :goto_0
    return-object v0

    .line 309353
    :cond_0
    sget-object v0, LX/1po;->PHOTO:LX/1po;

    if-ne p0, v0, :cond_1

    .line 309354
    sget-object v0, LX/1kH;->e:Ljava/lang/String;

    goto :goto_0

    .line 309355
    :cond_1
    sget-object v0, LX/1po;->VIDEO:LX/1po;

    if-ne p0, v0, :cond_2

    .line 309356
    sget-object v0, LX/1kH;->f:Ljava/lang/String;

    goto :goto_0

    .line 309357
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid media type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/1po;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 309349
    const/16 v0, 0x13

    invoke-static {p0, v0}, LX/1kH;->a(Landroid/net/Uri;I)I

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 309348
    invoke-static {p0}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    sget-object v1, LX/1ld;->b:LX/1lW;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 309338
    const/16 v0, 0x12

    invoke-static {p0, v0}, LX/1kH;->a(Landroid/net/Uri;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(ZI)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309337
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1kH;->a(ZILX/1po;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZILX/1po;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "LX/1po;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309310
    if-lez p2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 309311
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1kH;->i:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 309312
    const/4 v8, 0x0

    .line 309313
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1kH;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/1kH;->a:Landroid/net/Uri;

    sget-object v4, LX/1kH;->b:[Ljava/lang/String;

    invoke-static/range {p3 .. p3}, LX/1kH;->a(LX/1po;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "date_added DESC LIMIT "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 309314
    :try_start_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 309315
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 309316
    :cond_0
    if-eqz v10, :cond_1

    .line 309317
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v2, v9

    :goto_1
    return-object v2

    .line 309318
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 309319
    :cond_3
    :goto_2
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 309320
    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, LX/1kH;->a(I)LX/1po;

    move-result-object v4

    .line 309321
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 309322
    if-eqz p1, :cond_4

    invoke-static {v3}, LX/1kH;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 309323
    :cond_4
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 309324
    const/4 v2, 0x3

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 309325
    const/4 v2, 0x4

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 309326
    const/4 v2, 0x5

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 309327
    sget-object v2, LX/1po;->VIDEO:LX/1po;

    if-ne v4, v2, :cond_6

    .line 309328
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LX/1kH;->a(Ljava/lang/String;)I

    move-result v5

    .line 309329
    int-to-long v12, v5

    const-wide/16 v14, 0x3e8

    cmp-long v2, v12, v14

    if-ltz v2, :cond_3

    .line 309330
    new-instance v2, Lcom/facebook/media/util/model/MediaModel;

    sget-object v4, LX/1po;->VIDEO:LX/1po;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;IIII)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 309331
    :catchall_0
    move-exception v2

    move-object v3, v10

    :goto_3
    if-eqz v3, :cond_5

    .line 309332
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .line 309333
    :cond_6
    :try_start_3
    new-instance v2, Lcom/facebook/media/util/model/MediaModel;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;IIII)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 309334
    :cond_7
    if-eqz v10, :cond_8

    .line 309335
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object v2, v9

    goto :goto_1

    .line 309336
    :catchall_1
    move-exception v2

    move-object v3, v8

    goto :goto_3
.end method
