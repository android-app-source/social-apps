.class public LX/0qX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0qX;


# instance fields
.field private final a:LX/0W3;

.field public final b:LX/0oz;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0W3;LX/0oz;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 147865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147866
    iput-object p1, p0, LX/0qX;->a:LX/0W3;

    .line 147867
    iput-object p2, p0, LX/0qX;->b:LX/0oz;

    .line 147868
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0qX;->c:Ljava/util/Map;

    .line 147869
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0qX;->d:Ljava/util/Map;

    .line 147870
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0qX;->e:Ljava/util/Map;

    .line 147871
    return-void
.end method

.method public static a(LX/0qX;JI)I
    .locals 3

    .prologue
    .line 147830
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 147831
    iget-object v1, p0, LX/0qX;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147832
    iget-object v1, p0, LX/0qX;->d:Ljava/util/Map;

    iget-object v2, p0, LX/0qX;->a:LX/0W3;

    invoke-interface {v2, p1, p2, p3}, LX/0W4;->a(JI)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147833
    :cond_0
    iget-object v1, p0, LX/0qX;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/0qX;
    .locals 5

    .prologue
    .line 147852
    sget-object v0, LX/0qX;->f:LX/0qX;

    if-nez v0, :cond_1

    .line 147853
    const-class v1, LX/0qX;

    monitor-enter v1

    .line 147854
    :try_start_0
    sget-object v0, LX/0qX;->f:LX/0qX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147855
    if-eqz v2, :cond_0

    .line 147856
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147857
    new-instance p0, LX/0qX;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-direct {p0, v3, v4}, LX/0qX;-><init>(LX/0W3;LX/0oz;)V

    .line 147858
    move-object v0, p0

    .line 147859
    sput-object v0, LX/0qX;->f:LX/0qX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147860
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147861
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147862
    :cond_1
    sget-object v0, LX/0qX;->f:LX/0qX;

    return-object v0

    .line 147863
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0qX;JLorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147846
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 147847
    iget-object v1, p0, LX/0qX;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147848
    :try_start_0
    iget-object v1, p0, LX/0qX;->e:Ljava/util/Map;

    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, LX/0qX;->a:LX/0W3;

    const/4 v4, 0x0

    invoke-interface {v3, p1, p2, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147849
    iget-object v1, p0, LX/0qX;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    .line 147850
    :goto_0
    return-object p3

    :cond_0
    iget-object v1, p0, LX/0qX;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    move-object p3, v0

    goto :goto_0

    .line 147851
    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method public static a(LX/0qX;JZ)Z
    .locals 3

    .prologue
    .line 147872
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 147873
    iget-object v1, p0, LX/0qX;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147874
    iget-object v1, p0, LX/0qX;->c:Ljava/util/Map;

    iget-object v2, p0, LX/0qX;->a:LX/0W3;

    invoke-interface {v2, p1, p2, p3}, LX/0W4;->a(JZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147875
    :cond_0
    iget-object v1, p0, LX/0qX;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147845
    sget-wide v0, LX/0X5;->cn:J

    invoke-static {p0, v0, v1, p1}, LX/0qX;->a(LX/0qX;JLorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 147844
    sget-wide v0, LX/0X5;->co:J

    invoke-static {p0, v0, v1, p1}, LX/0qX;->a(LX/0qX;JI)I

    move-result v0

    return v0
.end method

.method public final c(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 147835
    sget-wide v2, LX/0X5;->cp:J

    const/4 v1, 0x0

    invoke-static {p0, v2, v3, v1}, LX/0qX;->a(LX/0qX;JLorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    .line 147836
    if-eqz v1, :cond_0

    .line 147837
    :try_start_0
    iget-object v2, p0, LX/0qX;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    .line 147838
    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 147839
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    move v1, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147840
    if-ne v1, v0, :cond_1

    move p1, v0

    .line 147841
    :cond_0
    :goto_0
    return p1

    .line 147842
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 147843
    :catch_0
    goto :goto_0
.end method

.method public final d(Z)Z
    .locals 2

    .prologue
    .line 147834
    sget-wide v0, LX/0X5;->cs:J

    invoke-static {p0, v0, v1, p1}, LX/0qX;->a(LX/0qX;JZ)Z

    move-result v0

    return v0
.end method
