.class public LX/0jN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0jN;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0SI;

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/3AK;

.field public e:LX/98h;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SI;LX/0Or;LX/3AK;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/earlyfetch/gating/IsEarlyFetchEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SI;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3AK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 122959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122960
    iput-object p1, p0, LX/0jN;->a:Landroid/content/Context;

    .line 122961
    iput-object p2, p0, LX/0jN;->b:LX/0SI;

    .line 122962
    iput-object p3, p0, LX/0jN;->c:LX/0Or;

    .line 122963
    iput-object p4, p0, LX/0jN;->d:LX/3AK;

    .line 122964
    return-void
.end method

.method public static a(LX/0QB;)LX/0jN;
    .locals 7

    .prologue
    .line 122965
    sget-object v0, LX/0jN;->f:LX/0jN;

    if-nez v0, :cond_1

    .line 122966
    const-class v1, LX/0jN;

    monitor-enter v1

    .line 122967
    :try_start_0
    sget-object v0, LX/0jN;->f:LX/0jN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 122968
    if-eqz v2, :cond_0

    .line 122969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 122970
    new-instance v6, LX/0jN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    const/16 v5, 0x148d

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/3AJ;->a(LX/0QB;)LX/3AJ;

    move-result-object v5

    check-cast v5, LX/3AK;

    invoke-direct {v6, v3, v4, p0, v5}, LX/0jN;-><init>(Landroid/content/Context;LX/0SI;LX/0Or;LX/3AK;)V

    .line 122971
    move-object v0, v6

    .line 122972
    sput-object v0, LX/0jN;->f:LX/0jN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122973
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 122974
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122975
    :cond_1
    sget-object v0, LX/0jN;->f:LX/0jN;

    return-object v0

    .line 122976
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 122977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 122978
    iget-object v0, p0, LX/0jN;->e:LX/98h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0jN;->e:LX/98h;

    invoke-virtual {v0}, LX/98h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122979
    iget-object v0, p0, LX/0jN;->e:LX/98h;

    const/4 v2, 0x0

    .line 122980
    iget-object v1, v0, LX/98h;->e:LX/98g;

    if-eqz v1, :cond_0

    .line 122981
    iget-object v1, v0, LX/98h;->e:LX/98g;

    .line 122982
    iget-object v3, v1, LX/98g;->b:Ljava/lang/Object;

    move-object v1, v3

    .line 122983
    invoke-virtual {v0, v1}, LX/98h;->a(Ljava/lang/Object;)V

    .line 122984
    iput-object v2, v0, LX/98h;->e:LX/98g;

    .line 122985
    iput-object v2, v0, LX/98h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 122986
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0jN;->e:LX/98h;

    .line 122987
    iget-object v0, p0, LX/0jN;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 122988
    :cond_1
    :goto_0
    return-void

    .line 122989
    :cond_2
    const-string v0, "target_fragment"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122990
    const-string v0, "target_fragment"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 122991
    invoke-static {v0}, LX/0jj;->a(I)LX/0cQ;

    move-result-object v0

    .line 122992
    if-eqz v0, :cond_1

    .line 122993
    iget-object v2, p0, LX/0jN;->d:LX/3AK;

    invoke-interface {v2, v0}, LX/3AK;->a(LX/0cQ;)LX/98h;

    move-result-object v2

    .line 122994
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/98h;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 122995
    iget-object v3, p0, LX/0jN;->b:LX/0SI;

    iget-object v4, p0, LX/0jN;->a:Landroid/content/Context;

    .line 122996
    iput-object v3, v2, LX/98h;->c:LX/0SI;

    .line 122997
    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    iput-object v5, v2, LX/98h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 122998
    invoke-virtual {v2, v4, p1}, LX/98h;->a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;

    move-result-object v5

    iput-object v5, v2, LX/98h;->e:LX/98g;

    .line 122999
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iput-wide v5, v2, LX/98h;->b:J

    iput-wide v5, v2, LX/98h;->a:J

    .line 123000
    :cond_3
    iput-object v2, p0, LX/0jN;->e:LX/98h;

    .line 123001
    goto :goto_0
.end method
