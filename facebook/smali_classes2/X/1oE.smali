.class public LX/1oE;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/1cu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/graphics/drawable/Drawable;",
        ">",
        "Landroid/graphics/drawable/Drawable;",
        "Landroid/graphics/drawable/Drawable$Callback;",
        "LX/1cu;"
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private b:LX/1oA;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318658
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 318659
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 318660
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 318661
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 318662
    :cond_0
    iput-object v1, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    .line 318663
    iput-object v1, p0, LX/1oE;->b:LX/1oA;

    .line 318664
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1oE;->c:Z

    .line 318665
    return-void
.end method

.method public final a(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 318666
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318667
    :goto_0
    return-void

    .line 318668
    :cond_0
    invoke-virtual {p0}, LX/1oE;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 318669
    iget-object v0, p0, LX/1oE;->b:LX/1oA;

    if-eqz v0, :cond_1

    move v0, v1

    .line 318670
    :goto_1
    iget-object v3, p0, LX/1oE;->b:LX/1oA;

    if-eqz v3, :cond_2

    .line 318671
    :goto_2
    iget-object v2, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    add-int v3, v0, p1

    add-int v4, v1, p2

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 318672
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 318673
    :cond_2
    iget v1, v2, Landroid/graphics/Rect;->top:I

    goto :goto_2
.end method

.method public final a(Landroid/graphics/drawable/Drawable;LX/1oA;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/1oA;",
            ")V"
        }
    .end annotation

    .prologue
    .line 318674
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 318675
    :goto_0
    return-void

    .line 318676
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 318677
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 318678
    :cond_1
    iput-object p1, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    .line 318679
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 318680
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 318681
    :cond_2
    iput-object p2, p0, LX/1oE;->b:LX/1oA;

    .line 318682
    iget-object v0, p0, LX/1oE;->b:LX/1oA;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1oE;->b:LX/1oA;

    .line 318683
    iget-boolean v1, v0, LX/1oA;->a:Z

    move v0, v1

    .line 318684
    if-nez v0, :cond_5

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_4

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/InsetDrawable;

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/1oE;->c:Z

    .line 318685
    invoke-virtual {p0}, LX/1oE;->invalidateSelf()V

    goto :goto_0

    .line 318686
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 318687
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/RippleDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1oE;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 318688
    invoke-virtual {p0}, LX/1oE;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 318689
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 318690
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    .line 318691
    iget-object v2, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 318692
    const/4 v0, 0x0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 318693
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 318694
    :cond_0
    :goto_0
    return-void

    .line 318695
    :cond_1
    invoke-virtual {p0}, LX/1oE;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 318696
    iget-object v1, p0, LX/1oE;->b:LX/1oA;

    if-eqz v1, :cond_3

    .line 318697
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 318698
    iget-boolean v2, p0, LX/1oE;->c:Z

    if-eqz v2, :cond_2

    .line 318699
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 318700
    :cond_2
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 318701
    iget-object v0, p0, LX/1oE;->b:LX/1oA;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 318702
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 318703
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 318704
    :cond_3
    iget-boolean v1, p0, LX/1oE;->c:Z

    if-eqz v1, :cond_4

    .line 318705
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 318706
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 318707
    :cond_4
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 318708
    iget-boolean v0, p0, LX/1oE;->c:Z

    if-eqz v0, :cond_0

    .line 318709
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 318710
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    goto :goto_0
.end method

.method public final getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 318711
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 318712
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 318713
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final getMinimumHeight()I
    .locals 1

    .prologue
    .line 318714
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final getMinimumWidth()I
    .locals 1

    .prologue
    .line 318715
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 318657
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    goto :goto_0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 318716
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getState()[I
    .locals 1

    .prologue
    .line 318630
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    goto :goto_0
.end method

.method public final getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    .prologue
    .line 318631
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    goto :goto_0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 318632
    invoke-virtual {p0}, LX/1oE;->invalidateSelf()V

    .line 318633
    return-void
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 318634
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 318635
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 318636
    invoke-virtual {p0, p2, p3, p4}, LX/1oE;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 318637
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 318638
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318639
    :goto_0
    return-void

    .line 318640
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public final setChangingConfigurations(I)V
    .locals 1

    .prologue
    .line 318641
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318642
    :goto_0
    return-void

    .line 318643
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    goto :goto_0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 318644
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318645
    :goto_0
    return-void

    .line 318646
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 318647
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318648
    :goto_0
    return-void

    .line 318649
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    goto :goto_0
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 318650
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 318651
    :goto_0
    return-void

    .line 318652
    :cond_0
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    goto :goto_0
.end method

.method public final setState([I)Z
    .locals 1

    .prologue
    .line 318653
    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 318654
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 318655
    invoke-virtual {p0, p2}, LX/1oE;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 318656
    return-void
.end method
