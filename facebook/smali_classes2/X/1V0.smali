.class public LX/1V0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1V1;

.field private final b:LX/1V2;

.field private final c:LX/14w;


# direct methods
.method public constructor <init>(LX/1V1;LX/1V2;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256978
    iput-object p1, p0, LX/1V0;->a:LX/1V1;

    .line 256979
    iput-object p2, p0, LX/1V0;->b:LX/1V2;

    .line 256980
    iput-object p3, p0, LX/1V0;->c:LX/14w;

    .line 256981
    return-void
.end method

.method public static a(LX/0QB;)LX/1V0;
    .locals 6

    .prologue
    .line 256958
    const-class v1, LX/1V0;

    monitor-enter v1

    .line 256959
    :try_start_0
    sget-object v0, LX/1V0;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 256960
    sput-object v2, LX/1V0;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256961
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256962
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 256963
    new-instance p0, LX/1V0;

    invoke-static {v0}, LX/1V1;->a(LX/0QB;)LX/1V1;

    move-result-object v3

    check-cast v3, LX/1V1;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v4

    check-cast v4, LX/1V2;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-direct {p0, v3, v4, v5}, LX/1V0;-><init>(LX/1V1;LX/1V2;LX/14w;)V

    .line 256964
    move-object v0, p0

    .line 256965
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 256966
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256967
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 256968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;Z)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ps;",
            "LX/1X6;",
            "LX/1X1",
            "<*>;Z)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 256971
    iget-object v0, p3, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, LX/1V0;->c:LX/14w;

    invoke-static {v0, v1}, LX/1X7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)I

    move-result v0

    .line 256972
    iget-object v1, p3, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p3, LX/1X6;->e:LX/1X9;

    iget-object v3, p0, LX/1V0;->b:LX/1V2;

    invoke-interface {p2}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    invoke-interface {p2}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    invoke-interface {p2}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    invoke-interface {p2}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p2}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v1

    .line 256973
    iget-object v2, p0, LX/1V0;->a:LX/1V1;

    invoke-virtual {v2, p1}, LX/1V1;->c(LX/1De;)LX/1XB;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/1XB;->a(LX/1X1;)LX/1XB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1XB;->h(I)LX/1XB;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1XB;->a(LX/1X9;)LX/1XB;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XB;->a(LX/1X6;)LX/1XB;

    move-result-object v0

    .line 256974
    iget-object v1, v0, LX/1XB;->a:LX/1XA;

    iput-boolean p5, v1, LX/1XA;->e:Z

    .line 256975
    move-object v0, v0

    .line 256976
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ps;",
            "LX/1X6;",
            "LX/1X1",
            "<*>;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 256970
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;Z)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ps;",
            "LX/1X6;",
            "LX/1X1",
            "<*>;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 256969
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;Z)LX/1X1;

    move-result-object v0

    return-object v0
.end method
