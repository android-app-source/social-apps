.class public final LX/0da;
.super LX/0dM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0dM",
        "<",
        "LX/FK3;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0da;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FK3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90494
    sget-object v0, LX/0db;->H:LX/0Tn;

    sget-object v1, LX/0dR;->PREFIX:LX/0dR;

    invoke-direct {p0, p1, v0, v1}, LX/0dM;-><init>(LX/0Ot;LX/0Tn;LX/0dR;)V

    .line 90495
    return-void
.end method

.method public static a(LX/0QB;)LX/0da;
    .locals 4

    .prologue
    .line 90496
    sget-object v0, LX/0da;->b:LX/0da;

    if-nez v0, :cond_1

    .line 90497
    const-class v1, LX/0da;

    monitor-enter v1

    .line 90498
    :try_start_0
    sget-object v0, LX/0da;->b:LX/0da;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90499
    if-eqz v2, :cond_0

    .line 90500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90501
    new-instance v3, LX/0da;

    const/16 p0, 0x28e9

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0da;-><init>(LX/0Ot;)V

    .line 90502
    move-object v0, v3

    .line 90503
    sput-object v0, LX/0da;->b:LX/0da;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90504
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90505
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90506
    :cond_1
    sget-object v0, LX/0da;->b:LX/0da;

    return-object v0

    .line 90507
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 90509
    check-cast p3, LX/FK3;

    .line 90510
    invoke-static {p3, p2}, LX/FK3;->a$redex0(LX/FK3;LX/0Tn;)V

    .line 90511
    return-void
.end method
