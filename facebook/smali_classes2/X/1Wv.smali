.class public LX/1Wv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field private static d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public e:LX/0SG;

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 270178
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "moments_upsell_prefs/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 270179
    sput-object v0, LX/1Wv;->a:LX/0Tn;

    const-string v1, "impression_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1Wv;->b:LX/0Tn;

    .line 270180
    sget-object v0, LX/1Wv;->a:LX/0Tn;

    const-string v1, "last_impression_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1Wv;->c:LX/0Tn;

    .line 270181
    sget-object v0, LX/1Wv;->b:LX/0Tn;

    sget-object v1, LX/1Wv;->c:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1Wv;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270183
    iput-object p1, p0, LX/1Wv;->e:LX/0SG;

    .line 270184
    iput-object p2, p0, LX/1Wv;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 270185
    return-void
.end method

.method public static b(LX/0QB;)LX/1Wv;
    .locals 3

    .prologue
    .line 270186
    new-instance v2, LX/1Wv;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/1Wv;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 270187
    return-object v2
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270188
    sget-object v0, LX/1Wv;->d:LX/0Rf;

    return-object v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 270189
    iget-object v0, p0, LX/1Wv;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1Wv;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method
