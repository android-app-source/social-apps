.class public final LX/1Sz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R4;


# instance fields
.field public final synthetic a:LX/1Ru;


# direct methods
.method public constructor <init>(LX/1Ru;)V
    .locals 0

    .prologue
    .line 249468
    iput-object p1, p0, LX/1Sz;->a:LX/1Ru;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 249448
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249449
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    invoke-virtual {v0}, LX/1Ru;->b()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1Rb;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249462
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249463
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->j:LX/1Sw;

    invoke-virtual {v0, p1}, LX/1Sw;->a(I)I

    move-result v0

    .line 249464
    iget-object v1, p0, LX/1Sz;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->j:LX/1Sw;

    .line 249465
    invoke-virtual {v1, p1}, LX/1Sw;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1Sw;->d(I)I

    move-result v2

    .line 249466
    sub-int v2, p1, v2

    move v1, v2

    .line 249467
    iget-object v2, p0, LX/1Sz;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    invoke-virtual {v0, v1}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Rb;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 249450
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249451
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v1

    .line 249452
    :goto_0
    if-ge v3, v4, :cond_2

    .line 249453
    iget-object v0, p0, LX/1Sz;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 249454
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v5

    move v2, v1

    .line 249455
    :goto_1
    if-ge v2, v5, :cond_1

    .line 249456
    invoke-virtual {v0, v2}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v6

    if-ne p1, v6, :cond_0

    .line 249457
    const/4 v0, 0x1

    .line 249458
    :goto_2
    return v0

    .line 249459
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 249460
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 249461
    goto :goto_2
.end method
