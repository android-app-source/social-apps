.class public final LX/1co;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/1cn;

.field public b:Landroid/graphics/Canvas;

.field public c:I


# direct methods
.method public constructor <init>(LX/1cn;)V
    .locals 0

    .prologue
    .line 283171
    iput-object p1, p0, LX/1co;->a:LX/1cn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283172
    return-void
.end method

.method public static a$redex0(LX/1co;)Z
    .locals 2

    .prologue
    .line 283173
    iget-object v0, p0, LX/1co;->b:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    iget v0, p0, LX/1co;->c:I

    iget-object v1, p0, LX/1co;->a:LX/1cn;

    iget-object v1, v1, LX/1cn;->a:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/1co;)V
    .locals 5

    .prologue
    .line 283174
    iget-object v0, p0, LX/1co;->b:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 283175
    :goto_0
    return-void

    .line 283176
    :cond_0
    iget v0, p0, LX/1co;->c:I

    iget-object v1, p0, LX/1co;->a:LX/1cn;

    iget-object v1, v1, LX/1cn;->a:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v3

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    .line 283177
    iget-object v0, p0, LX/1co;->a:LX/1cn;

    iget-object v0, v0, LX/1cn;->a:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 283178
    iget-object v1, v0, LX/1cv;->t:LX/1cz;

    move-object v1, v1

    .line 283179
    if-eqz v1, :cond_1

    .line 283180
    iget-object v1, v0, LX/1cv;->t:LX/1cz;

    move-object v1, v1

    .line 283181
    :goto_2
    instance-of v4, v1, Landroid/view/View;

    if-eqz v4, :cond_2

    .line 283182
    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/1co;->c:I

    goto :goto_0

    .line 283183
    :cond_1
    iget-object v1, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 283184
    goto :goto_2

    .line 283185
    :cond_2
    iget-object v4, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v4

    .line 283186
    invoke-virtual {v0}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1mm;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 283187
    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/1co;->b:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 283188
    invoke-static {}, LX/1mm;->a()V

    .line 283189
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 283190
    :cond_3
    iget-object v0, p0, LX/1co;->a:LX/1cn;

    iget-object v0, v0, LX/1cn;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    iput v0, p0, LX/1co;->c:I

    goto :goto_0
.end method
