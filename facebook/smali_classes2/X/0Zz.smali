.class public final enum LX/0Zz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Zz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Zz;

.field public static final enum CROSS_APP:LX/0Zz;

.field public static final enum CROSS_PROCESS:LX/0Zz;

.field public static final enum GLOBAL:LX/0Zz;

.field public static final enum LOCAL:LX/0Zz;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84254
    new-instance v0, LX/0Zz;

    const-string v1, "CROSS_APP"

    invoke-direct {v0, v1, v2}, LX/0Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Zz;->CROSS_APP:LX/0Zz;

    .line 84255
    new-instance v0, LX/0Zz;

    const-string v1, "CROSS_PROCESS"

    invoke-direct {v0, v1, v3}, LX/0Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Zz;->CROSS_PROCESS:LX/0Zz;

    .line 84256
    new-instance v0, LX/0Zz;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v4}, LX/0Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Zz;->LOCAL:LX/0Zz;

    .line 84257
    new-instance v0, LX/0Zz;

    const-string v1, "GLOBAL"

    invoke-direct {v0, v1, v5}, LX/0Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Zz;->GLOBAL:LX/0Zz;

    .line 84258
    const/4 v0, 0x4

    new-array v0, v0, [LX/0Zz;

    sget-object v1, LX/0Zz;->CROSS_APP:LX/0Zz;

    aput-object v1, v0, v2

    sget-object v1, LX/0Zz;->CROSS_PROCESS:LX/0Zz;

    aput-object v1, v0, v3

    sget-object v1, LX/0Zz;->LOCAL:LX/0Zz;

    aput-object v1, v0, v4

    sget-object v1, LX/0Zz;->GLOBAL:LX/0Zz;

    aput-object v1, v0, v5

    sput-object v0, LX/0Zz;->$VALUES:[LX/0Zz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 84253
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Zz;
    .locals 1

    .prologue
    .line 84251
    const-class v0, LX/0Zz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Zz;

    return-object v0
.end method

.method public static values()[LX/0Zz;
    .locals 1

    .prologue
    .line 84252
    sget-object v0, LX/0Zz;->$VALUES:[LX/0Zz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Zz;

    return-object v0
.end method
