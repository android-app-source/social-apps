.class public LX/1U1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1U1;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/survey/SurveyGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/survey/SurveyGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255041
    iput-object p1, p0, LX/1U1;->a:LX/0Ot;

    .line 255042
    return-void
.end method

.method public static a(LX/0QB;)LX/1U1;
    .locals 4

    .prologue
    .line 255043
    sget-object v0, LX/1U1;->b:LX/1U1;

    if-nez v0, :cond_1

    .line 255044
    const-class v1, LX/1U1;

    monitor-enter v1

    .line 255045
    :try_start_0
    sget-object v0, LX/1U1;->b:LX/1U1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 255046
    if-eqz v2, :cond_0

    .line 255047
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 255048
    new-instance v3, LX/1U1;

    const/16 p0, 0x21d6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1U1;-><init>(LX/0Ot;)V

    .line 255049
    move-object v0, v3

    .line 255050
    sput-object v0, LX/1U1;->b:LX/1U1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255051
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 255052
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 255053
    :cond_1
    sget-object v0, LX/1U1;->b:LX/1U1;

    return-object v0

    .line 255054
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 255055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255056
    sget-object v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255057
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255058
    const-class v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    iget-object v1, p0, LX/1U1;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255059
    return-void
.end method
