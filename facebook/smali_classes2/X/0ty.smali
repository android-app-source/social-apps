.class public LX/0ty;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/contextual/ContextsProviderRegistry;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0u0;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155832
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0u0;
    .locals 3

    .prologue
    .line 155833
    sget-object v0, LX/0ty;->a:LX/0u0;

    if-nez v0, :cond_1

    .line 155834
    const-class v1, LX/0ty;

    monitor-enter v1

    .line 155835
    :try_start_0
    sget-object v0, LX/0ty;->a:LX/0u0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155836
    if-eqz v2, :cond_0

    .line 155837
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155838
    invoke-static {v0}, LX/0tz;->a(LX/0QB;)Ljava/util/Set;

    move-result-object p0

    invoke-static {p0}, LX/0tv;->a(Ljava/util/Set;)LX/0u0;

    move-result-object p0

    move-object v0, p0

    .line 155839
    sput-object v0, LX/0ty;->a:LX/0u0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155840
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155841
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155842
    :cond_1
    sget-object v0, LX/0ty;->a:LX/0u0;

    return-object v0

    .line 155843
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155845
    invoke-static {p0}, LX/0tz;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0tv;->a(Ljava/util/Set;)LX/0u0;

    move-result-object v0

    return-object v0
.end method
