.class public LX/0iX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile p:LX/0iX;


# instance fields
.field public final a:LX/0iY;

.field public final b:LX/0So;

.field public final c:LX/0ad;

.field public final d:LX/0iZ;

.field public final e:Z

.field public final f:Z

.field private final g:Z

.field public h:Z

.field public i:Z

.field public j:J

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0ib;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1rN;

.field public m:Z

.field public n:I

.field public o:I


# direct methods
.method public constructor <init>(LX/0iY;LX/0So;LX/0ad;LX/0iZ;Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 121041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121042
    iput-boolean v3, p0, LX/0iX;->i:Z

    .line 121043
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0iX;->j:J

    .line 121044
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/0iX;->k:Ljava/util/Set;

    .line 121045
    sget-object v0, LX/1rN;->UNKNOWN:LX/1rN;

    iput-object v0, p0, LX/0iX;->l:LX/1rN;

    .line 121046
    iput v3, p0, LX/0iX;->n:I

    .line 121047
    iput-object p1, p0, LX/0iX;->a:LX/0iY;

    .line 121048
    iput-object p2, p0, LX/0iX;->b:LX/0So;

    .line 121049
    iput-object p3, p0, LX/0iX;->c:LX/0ad;

    .line 121050
    iput-object p4, p0, LX/0iX;->d:LX/0iZ;

    .line 121051
    invoke-virtual {p0}, LX/0iX;->d()V

    .line 121052
    invoke-static {p0}, LX/0iX;->i(LX/0iX;)V

    .line 121053
    new-instance v0, LX/1rP;

    invoke-direct {v0, p0}, LX/1rP;-><init>(LX/0iX;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121054
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    .line 121055
    iput-boolean v3, v0, LX/0iY;->t:Z

    .line 121056
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->m()I

    move-result v0

    iput v0, p0, LX/0iX;->o:I

    .line 121057
    iget-object v0, p0, LX/0iX;->c:LX/0ad;

    sget-short v1, LX/1rJ;->w:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->e:Z

    .line 121058
    iget-object v0, p0, LX/0iX;->c:LX/0ad;

    sget-short v1, LX/1rJ;->x:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->f:Z

    .line 121059
    iget-object v0, p0, LX/0iX;->c:LX/0ad;

    sget-short v1, LX/1rJ;->k:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->g:Z

    .line 121060
    return-void
.end method

.method public static a(LX/0QB;)LX/0iX;
    .locals 9

    .prologue
    .line 121061
    sget-object v0, LX/0iX;->p:LX/0iX;

    if-nez v0, :cond_1

    .line 121062
    const-class v1, LX/0iX;

    monitor-enter v1

    .line 121063
    :try_start_0
    sget-object v0, LX/0iX;->p:LX/0iX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121064
    if-eqz v2, :cond_0

    .line 121065
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121066
    new-instance v3, LX/0iX;

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v4

    check-cast v4, LX/0iY;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/0iZ;->a(LX/0QB;)LX/0iZ;

    move-result-object v7

    check-cast v7, LX/0iZ;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct/range {v3 .. v8}, LX/0iX;-><init>(LX/0iY;LX/0So;LX/0ad;LX/0iZ;Landroid/content/Context;)V

    .line 121067
    move-object v0, v3

    .line 121068
    sput-object v0, LX/0iX;->p:LX/0iX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121069
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121070
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121071
    :cond_1
    sget-object v0, LX/0iX;->p:LX/0iX;

    return-object v0

    .line 121072
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/04g;)V
    .locals 2

    .prologue
    .line 121088
    iget-object v0, p0, LX/0iX;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ib;

    .line 121089
    invoke-interface {v0, p1}, LX/0ib;->a(LX/04g;)V

    goto :goto_0

    .line 121090
    :cond_0
    return-void
.end method

.method public static i(LX/0iX;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 121074
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->g()Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->m:Z

    .line 121075
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->h:Z

    .line 121076
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    .line 121077
    iget-object v1, v0, LX/0iY;->b:LX/0ad;

    sget-short v2, LX/1rJ;->j:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 121078
    if-eqz v0, :cond_0

    .line 121079
    iget-object v0, p0, LX/0iX;->d:LX/0iZ;

    .line 121080
    iget-object v1, v0, LX/0iZ;->a:LX/0if;

    sget-object v2, LX/0ig;->aM:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 121081
    sget-object v1, LX/0ia;->START_SESSION:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 121082
    iget-object v1, p0, LX/0iX;->d:LX/0iZ;

    iget-boolean v0, p0, LX/0iX;->m:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/0ia;->MUTE_SWITCH_OFF:LX/0ia;

    :goto_0
    invoke-virtual {v1, v0}, LX/0iZ;->a(LX/0ia;)V

    .line 121083
    :cond_0
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    .line 121084
    iget-object v1, v0, LX/0iY;->b:LX/0ad;

    sget v2, LX/1rJ;->r:I

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    iput v1, v0, LX/0iY;->s:I

    .line 121085
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->b()Z

    move-result v0

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0, v1}, LX/0iX;->a(ZLX/04g;)V

    .line 121086
    return-void

    .line 121087
    :cond_1
    sget-object v0, LX/0ia;->MUTE_SWITCH_ON:LX/0ia;

    goto :goto_0
.end method

.method public static j(LX/0iX;)V
    .locals 3

    .prologue
    .line 121028
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->g()Z

    move-result v0

    iput-boolean v0, p0, LX/0iX;->m:Z

    .line 121029
    const/4 v0, 0x0

    .line 121030
    iget-object v1, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0iX;->c:LX/0ad;

    sget-short v2, LX/1rJ;->v:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, LX/0iX;->m:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, LX/0iX;->h:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/0iX;->i:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 121031
    if-eqz v0, :cond_3

    .line 121032
    iget-object v0, p0, LX/0iX;->d:LX/0iZ;

    sget-object v1, LX/0ia;->RESET_BY_MUTE_SWITCH:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 121033
    const/4 v0, 0x0

    sget-object v1, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {p0, v0, v1}, LX/0iX;->b(ZLX/04g;)V

    .line 121034
    :cond_2
    :goto_0
    return-void

    .line 121035
    :cond_3
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/0iX;->m:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/0iX;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/0iX;->i:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 121036
    if-eqz v0, :cond_2

    .line 121037
    iget-object v0, p0, LX/0iX;->d:LX/0iZ;

    sget-object v1, LX/0ia;->RESET_BY_MUTE_SWITCH:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 121038
    const/4 v0, 0x1

    sget-object v1, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {p0, v0, v1}, LX/0iX;->b(ZLX/04g;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0ib;)V
    .locals 1

    .prologue
    .line 121039
    iget-object v0, p0, LX/0iX;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121040
    return-void
.end method

.method public final a(ZLX/04g;)V
    .locals 2

    .prologue
    .line 121024
    iget-object v0, p0, LX/0iX;->l:LX/1rN;

    sget-object v1, LX/1rN;->OFF:LX/1rN;

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/0iX;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0iX;->i:Z

    .line 121025
    invoke-direct {p0, p2}, LX/0iX;->a(LX/04g;)V

    .line 121026
    return-void

    .line 121027
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/04D;LX/2pa;)Z
    .locals 1
    .param p2    # LX/2pa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121023
    invoke-virtual {p0, p1, p2}, LX/0iX;->b(LX/04D;LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0iX;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0ib;)V
    .locals 1

    .prologue
    .line 121021
    iget-object v0, p0, LX/0iX;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 121022
    return-void
.end method

.method public final b(ZLX/04g;)V
    .locals 2

    .prologue
    .line 121015
    iget-boolean v0, p0, LX/0iX;->i:Z

    if-eq v0, p1, :cond_1

    .line 121016
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-ne p2, v0, :cond_0

    .line 121017
    iget-object v0, p0, LX/0iX;->d:LX/0iZ;

    sget-object v1, LX/0ia;->RESET_BY_BACKGROUND_AUDIO:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 121018
    :cond_0
    iput-boolean p1, p0, LX/0iX;->i:Z

    .line 121019
    invoke-direct {p0, p2}, LX/0iX;->a(LX/04g;)V

    .line 121020
    :cond_1
    return-void
.end method

.method public final b(LX/04D;LX/2pa;)Z
    .locals 3
    .param p2    # LX/2pa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 121010
    iget-boolean v1, p0, LX/0iX;->g:Z

    if-eqz v1, :cond_1

    invoke-static {p2}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121011
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_0

    iget-object v1, p2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_0

    iget-object v1, p2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    if-nez v1, :cond_0

    invoke-virtual {p2}, LX/2pa;->f()LX/3HY;

    move-result-object v1

    sget-object v2, LX/3HY;->REGULAR:LX/3HY;

    if-ne v1, v2, :cond_0

    .line 121012
    sget-object v1, LX/04D;->FEED:LX/04D;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/04D;->VIDEO_SETS:LX/04D;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/04D;->SINGLE_CREATOR_SET_INLINE:LX/04D;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/04D;->CAROUSEL_VIDEO:LX/04D;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    if-ne p1, v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 121013
    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/0iX;->h:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 121014
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 121007
    iget-object v0, p0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v0}, LX/0iY;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/1rN;->ON:LX/1rN;

    :goto_0
    iput-object v0, p0, LX/0iX;->l:LX/1rN;

    .line 121008
    return-void

    .line 121009
    :cond_0
    sget-object v0, LX/1rN;->OFF:LX/1rN;

    goto :goto_0
.end method
