.class public LX/1Oe;
.super LX/1Of;
.source ""


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3wq;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3wp;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "LX/3wq;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "LX/3wp;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 241743
    invoke-direct {p0}, LX/1Of;-><init>()V

    .line 241744
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    .line 241745
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    .line 241746
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    .line 241747
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    .line 241748
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    .line 241749
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    .line 241750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    .line 241751
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->h:Ljava/util/ArrayList;

    .line 241752
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->i:Ljava/util/ArrayList;

    .line 241753
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->j:Ljava/util/ArrayList;

    .line 241754
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Oe;->k:Ljava/util/ArrayList;

    .line 241755
    return-void
.end method

.method public static synthetic a(LX/1Oe;LX/1a1;IIII)V
    .locals 9

    .prologue
    .line 241760
    const/4 v2, 0x0

    .line 241761
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 241762
    sub-int v3, p4, p2

    .line 241763
    sub-int v4, p5, p3

    .line 241764
    if-eqz v3, :cond_0

    .line 241765
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/3sU;->b(F)LX/3sU;

    .line 241766
    :cond_0
    if-eqz v4, :cond_1

    .line 241767
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/3sU;->c(F)LX/3sU;

    .line 241768
    :cond_1
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v5

    .line 241769
    iget-object v0, p0, LX/1Oe;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241770
    iget-wide v7, p0, LX/1Of;->e:J

    move-wide v0, v7

    .line 241771
    invoke-virtual {v5, v0, v1}, LX/3sU;->a(J)LX/3sU;

    move-result-object v6

    new-instance v0, LX/3wm;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/3wm;-><init>(LX/1Oe;LX/1a1;IILX/3sU;)V

    invoke-virtual {v6, v0}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 241772
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241756
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 241757
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->a()V

    .line 241758
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241759
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;LX/1a1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3wp;",
            ">;",
            "LX/1a1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 241712
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 241713
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wp;

    .line 241714
    invoke-direct {p0, v0, p2}, LX/1Oe;->a(LX/3wp;LX/1a1;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241715
    iget-object v2, v0, LX/3wp;->a:LX/1a1;

    if-nez v2, :cond_0

    iget-object v2, v0, LX/3wp;->b:LX/1a1;

    if-nez v2, :cond_0

    .line 241716
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 241717
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241718
    :cond_1
    return-void
.end method

.method private a(LX/3wp;LX/1a1;)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 241733
    iget-object v2, p1, LX/3wp;->b:LX/1a1;

    if-ne v2, p2, :cond_1

    .line 241734
    iput-object v3, p1, LX/3wp;->b:LX/1a1;

    .line 241735
    :goto_0
    iget-object v2, p2, LX/1a1;->a:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241736
    iget-object v2, p2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2, v4}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241737
    iget-object v2, p2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2, v4}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241738
    invoke-virtual {p0, p2, v0}, LX/1Of;->a(LX/1a1;Z)V

    move v0, v1

    .line 241739
    :cond_0
    return v0

    .line 241740
    :cond_1
    iget-object v2, p1, LX/3wp;->a:LX/1a1;

    if-ne v2, p2, :cond_0

    .line 241741
    iput-object v3, p1, LX/3wp;->a:LX/1a1;

    move v0, v1

    .line 241742
    goto :goto_0
.end method

.method private b(LX/3wp;)V
    .locals 1

    .prologue
    .line 241728
    iget-object v0, p1, LX/3wp;->a:LX/1a1;

    if-eqz v0, :cond_0

    .line 241729
    iget-object v0, p1, LX/3wp;->a:LX/1a1;

    invoke-direct {p0, p1, v0}, LX/1Oe;->a(LX/3wp;LX/1a1;)Z

    .line 241730
    :cond_0
    iget-object v0, p1, LX/3wp;->b:LX/1a1;

    if-eqz v0, :cond_1

    .line 241731
    iget-object v0, p1, LX/3wp;->b:LX/1a1;

    invoke-direct {p0, p1, v0}, LX/1Oe;->a(LX/3wp;LX/1a1;)Z

    .line 241732
    :cond_1
    return-void
.end method

.method private g(LX/1a1;)V
    .locals 6

    .prologue
    .line 241722
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 241723
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    .line 241724
    iget-object v1, p0, LX/1Oe;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241725
    iget-wide v4, p0, LX/1Of;->d:J

    move-wide v2, v4

    .line 241726
    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/3sU;->a(F)LX/3sU;

    move-result-object v1

    new-instance v2, LX/3wk;

    invoke-direct {v2, p0, p1, v0}, LX/3wk;-><init>(LX/1Oe;LX/1a1;LX/3sU;)V

    invoke-virtual {v1, v2}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 241727
    return-void
.end method

.method private i(LX/1a1;)V
    .locals 1

    .prologue
    .line 241719
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/3os;->a(Landroid/view/View;)V

    .line 241720
    invoke-virtual {p0, p1}, LX/1Of;->c(LX/1a1;)V

    .line 241721
    return-void
.end method

.method public static j(LX/1Oe;)V
    .locals 1

    .prologue
    .line 241773
    invoke-virtual {p0}, LX/1Of;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241774
    invoke-virtual {p0}, LX/1Of;->i()V

    .line 241775
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 241524
    iget-object v1, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v0

    .line 241525
    :goto_0
    iget-object v3, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v0

    .line 241526
    :goto_1
    iget-object v4, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v0

    .line 241527
    :goto_2
    iget-object v5, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v0

    .line 241528
    :goto_3
    if-nez v1, :cond_5

    if-nez v3, :cond_5

    if-nez v5, :cond_5

    if-nez v4, :cond_5

    .line 241529
    :cond_0
    :goto_4
    return-void

    :cond_1
    move v1, v2

    .line 241530
    goto :goto_0

    :cond_2
    move v3, v2

    .line 241531
    goto :goto_1

    :cond_3
    move v4, v2

    .line 241532
    goto :goto_2

    :cond_4
    move v5, v2

    .line 241533
    goto :goto_3

    .line 241534
    :cond_5
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241535
    invoke-direct {p0, v0}, LX/1Oe;->g(LX/1a1;)V

    goto :goto_5

    .line 241536
    :cond_6
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241537
    if-eqz v3, :cond_7

    .line 241538
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 241539
    iget-object v8, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241540
    iget-object v8, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241541
    iget-object v8, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 241542
    new-instance v8, Landroid/support/v7/widget/DefaultItemAnimator$1;

    invoke-direct {v8, p0, v0}, Landroid/support/v7/widget/DefaultItemAnimator$1;-><init>(LX/1Oe;Ljava/util/ArrayList;)V

    .line 241543
    if-eqz v1, :cond_a

    .line 241544
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wq;

    iget-object v0, v0, LX/3wq;->a:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 241545
    iget-wide v13, p0, LX/1Of;->d:J

    move-wide v10, v13

    .line 241546
    invoke-static {v0, v8, v10, v11}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 241547
    :cond_7
    :goto_6
    if-eqz v4, :cond_8

    .line 241548
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 241549
    iget-object v8, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241550
    iget-object v8, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241551
    iget-object v8, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 241552
    new-instance v8, Landroid/support/v7/widget/DefaultItemAnimator$2;

    invoke-direct {v8, p0, v0}, Landroid/support/v7/widget/DefaultItemAnimator$2;-><init>(LX/1Oe;Ljava/util/ArrayList;)V

    .line 241553
    if-eqz v1, :cond_b

    .line 241554
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wp;

    iget-object v0, v0, LX/3wp;->a:LX/1a1;

    .line 241555
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 241556
    iget-wide v13, p0, LX/1Of;->d:J

    move-wide v10, v13

    .line 241557
    invoke-static {v0, v8, v10, v11}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 241558
    :cond_8
    :goto_7
    if-eqz v5, :cond_0

    .line 241559
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 241560
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241561
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241562
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241563
    new-instance v12, Landroid/support/v7/widget/DefaultItemAnimator$3;

    invoke-direct {v12, p0, v5}, Landroid/support/v7/widget/DefaultItemAnimator$3;-><init>(LX/1Oe;Ljava/util/ArrayList;)V

    .line 241564
    if-nez v1, :cond_9

    if-nez v3, :cond_9

    if-eqz v4, :cond_f

    .line 241565
    :cond_9
    if-eqz v1, :cond_c

    .line 241566
    iget-wide v13, p0, LX/1Of;->d:J

    move-wide v0, v13

    .line 241567
    move-wide v10, v0

    .line 241568
    :goto_8
    if-eqz v3, :cond_d

    .line 241569
    iget-wide v13, p0, LX/1Of;->e:J

    move-wide v0, v13

    .line 241570
    move-wide v8, v0

    .line 241571
    :goto_9
    if-eqz v4, :cond_e

    .line 241572
    iget-wide v13, p0, LX/1Of;->f:J

    move-wide v0, v13

    .line 241573
    :goto_a
    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long v6, v10, v0

    .line 241574
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 241575
    invoke-static {v0, v12, v6, v7}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto/16 :goto_4

    .line 241576
    :cond_a
    invoke-interface {v8}, Ljava/lang/Runnable;->run()V

    goto :goto_6

    .line 241577
    :cond_b
    invoke-interface {v8}, Ljava/lang/Runnable;->run()V

    goto :goto_7

    :cond_c
    move-wide v10, v6

    .line 241578
    goto :goto_8

    :cond_d
    move-wide v8, v6

    .line 241579
    goto :goto_9

    :cond_e
    move-wide v0, v6

    .line 241580
    goto :goto_a

    .line 241581
    :cond_f
    invoke-interface {v12}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_4
.end method

.method public final a(LX/1a1;)Z
    .locals 1

    .prologue
    .line 241521
    invoke-direct {p0, p1}, LX/1Oe;->i(LX/1a1;)V

    .line 241522
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241523
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/1a1;IIII)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 241506
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 241507
    int-to-float v1, p2

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2}, LX/0vv;->r(Landroid/view/View;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v2, v1

    .line 241508
    int-to-float v1, p3

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v3}, LX/0vv;->s(Landroid/view/View;)F

    move-result v3

    add-float/2addr v1, v3

    float-to-int v3, v1

    .line 241509
    invoke-direct {p0, p1}, LX/1Oe;->i(LX/1a1;)V

    .line 241510
    sub-int v1, p4, v2

    .line 241511
    sub-int v4, p5, v3

    .line 241512
    if-nez v1, :cond_0

    if-nez v4, :cond_0

    .line 241513
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 241514
    :goto_0
    return v6

    .line 241515
    :cond_0
    if-eqz v1, :cond_1

    .line 241516
    neg-int v1, v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241517
    :cond_1
    if-eqz v4, :cond_2

    .line 241518
    neg-int v1, v4

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241519
    :cond_2
    iget-object v7, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    new-instance v0, LX/3wq;

    move-object v1, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/3wq;-><init>(LX/1a1;IIIIB)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241520
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public final a(LX/1a1;LX/1a1;IIII)Z
    .locals 9

    .prologue
    .line 241490
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->r(Landroid/view/View;)F

    move-result v0

    .line 241491
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v1}, LX/0vv;->s(Landroid/view/View;)F

    move-result v1

    .line 241492
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2}, LX/0vv;->f(Landroid/view/View;)F

    move-result v2

    .line 241493
    invoke-direct {p0, p1}, LX/1Oe;->i(LX/1a1;)V

    .line 241494
    sub-int v3, p5, p3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    float-to-int v3, v3

    .line 241495
    sub-int v4, p6, p4

    int-to-float v4, v4

    sub-float/2addr v4, v1

    float-to-int v4, v4

    .line 241496
    iget-object v5, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v5, v0}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241497
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241498
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v2}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241499
    if-eqz p2, :cond_0

    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 241500
    invoke-direct {p0, p2}, LX/1Oe;->i(LX/1a1;)V

    .line 241501
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    neg-int v1, v3

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241502
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    neg-int v1, v4

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241503
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241504
    :cond_0
    iget-object v8, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    new-instance v0, LX/3wp;

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, LX/3wp;-><init>(LX/1a1;LX/1a1;IIIIB)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241505
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 241582
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1a1;)Z
    .locals 2

    .prologue
    .line 241583
    invoke-direct {p0, p1}, LX/1Oe;->i(LX/1a1;)V

    .line 241584
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241585
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241586
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 241587
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241588
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 241589
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wq;

    .line 241590
    iget-object v2, v0, LX/3wq;->a:LX/1a1;

    iget-object v2, v2, LX/1a1;->a:Landroid/view/View;

    .line 241591
    invoke-static {v2, v5}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241592
    invoke-static {v2, v5}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241593
    iget-object v0, v0, LX/3wq;->a:LX/1a1;

    invoke-virtual {p0, v0}, LX/1Of;->e(LX/1a1;)V

    .line 241594
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241595
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241596
    :cond_0
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241597
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 241598
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241599
    invoke-virtual {p0, v0}, LX/1Of;->d(LX/1a1;)V

    .line 241600
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241601
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 241602
    :cond_1
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241603
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    .line 241604
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241605
    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    .line 241606
    invoke-static {v2, v6}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241607
    invoke-virtual {p0, v0}, LX/1Of;->f(LX/1a1;)V

    .line 241608
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241609
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 241610
    :cond_2
    iget-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241611
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_3

    .line 241612
    iget-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wp;

    invoke-direct {p0, v0}, LX/1Oe;->b(LX/3wp;)V

    .line 241613
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 241614
    :cond_3
    iget-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241615
    invoke-virtual {p0}, LX/1Of;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 241616
    :goto_4
    return-void

    .line 241617
    :cond_4
    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241618
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_5
    if-ltz v3, :cond_7

    .line 241619
    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241620
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 241621
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_6
    if-ltz v2, :cond_6

    .line 241622
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3wq;

    .line 241623
    iget-object v4, v1, LX/3wq;->a:LX/1a1;

    .line 241624
    iget-object v4, v4, LX/1a1;->a:Landroid/view/View;

    .line 241625
    invoke-static {v4, v5}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241626
    invoke-static {v4, v5}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241627
    iget-object v1, v1, LX/3wq;->a:LX/1a1;

    invoke-virtual {p0, v1}, LX/1Of;->e(LX/1a1;)V

    .line 241628
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241629
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 241630
    iget-object v1, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241631
    :cond_5
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_6

    .line 241632
    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_5

    .line 241633
    :cond_7
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241634
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_7
    if-ltz v3, :cond_a

    .line 241635
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241636
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 241637
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_8
    if-ltz v2, :cond_9

    .line 241638
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 241639
    iget-object v4, v1, LX/1a1;->a:Landroid/view/View;

    .line 241640
    invoke-static {v4, v6}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241641
    invoke-virtual {p0, v1}, LX/1Of;->f(LX/1a1;)V

    .line 241642
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241643
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 241644
    iget-object v1, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241645
    :cond_8
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_8

    .line 241646
    :cond_9
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_7

    .line 241647
    :cond_a
    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241648
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_9
    if-ltz v3, :cond_d

    .line 241649
    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241650
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 241651
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_a
    if-ltz v2, :cond_c

    .line 241652
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3wp;

    invoke-direct {p0, v1}, LX/1Oe;->b(LX/3wp;)V

    .line 241653
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 241654
    iget-object v1, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241655
    :cond_b
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_a

    .line 241656
    :cond_c
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_9

    .line 241657
    :cond_d
    iget-object v0, p0, LX/1Oe;->j:Ljava/util/ArrayList;

    invoke-static {v0}, LX/1Oe;->a(Ljava/util/List;)V

    .line 241658
    iget-object v0, p0, LX/1Oe;->i:Ljava/util/ArrayList;

    invoke-static {v0}, LX/1Oe;->a(Ljava/util/List;)V

    .line 241659
    iget-object v0, p0, LX/1Oe;->h:Ljava/util/ArrayList;

    invoke-static {v0}, LX/1Oe;->a(Ljava/util/List;)V

    .line 241660
    iget-object v0, p0, LX/1Oe;->k:Ljava/util/ArrayList;

    invoke-static {v0}, LX/1Oe;->a(Ljava/util/List;)V

    .line 241661
    invoke-virtual {p0}, LX/1Of;->i()V

    goto/16 :goto_4
.end method

.method public final c(LX/1a1;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 241662
    iget-object v4, p1, LX/1a1;->a:Landroid/view/View;

    .line 241663
    invoke-static {v4}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->a()V

    .line 241664
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 241665
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wq;

    .line 241666
    iget-object v0, v0, LX/3wq;->a:LX/1a1;

    if-ne v0, p1, :cond_0

    .line 241667
    invoke-static {v4, v5}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241668
    invoke-static {v4, v5}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241669
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 241670
    iget-object v0, p0, LX/1Oe;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241671
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241672
    :cond_1
    iget-object v0, p0, LX/1Oe;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, LX/1Oe;->a(Ljava/util/List;LX/1a1;)V

    .line 241673
    iget-object v0, p0, LX/1Oe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241674
    invoke-static {v4, v6}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241675
    invoke-virtual {p0, p1}, LX/1Of;->d(LX/1a1;)V

    .line 241676
    :cond_2
    iget-object v0, p0, LX/1Oe;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 241677
    invoke-static {v4, v6}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241678
    invoke-virtual {p0, p1}, LX/1Of;->f(LX/1a1;)V

    .line 241679
    :cond_3
    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    .line 241680
    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241681
    invoke-direct {p0, v0, p1}, LX/1Oe;->a(Ljava/util/List;LX/1a1;)V

    .line 241682
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 241683
    iget-object v0, p0, LX/1Oe;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241684
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 241685
    :cond_5
    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_8

    .line 241686
    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241687
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_3
    if-ltz v2, :cond_6

    .line 241688
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3wq;

    .line 241689
    iget-object v1, v1, LX/3wq;->a:LX/1a1;

    if-ne v1, p1, :cond_7

    .line 241690
    invoke-static {v4, v5}, LX/0vv;->b(Landroid/view/View;F)V

    .line 241691
    invoke-static {v4, v5}, LX/0vv;->a(Landroid/view/View;F)V

    .line 241692
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 241693
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241694
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 241695
    iget-object v0, p0, LX/1Oe;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241696
    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    .line 241697
    :cond_7
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_3

    .line 241698
    :cond_8
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_a

    .line 241699
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241700
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 241701
    invoke-static {v4, v6}, LX/0vv;->c(Landroid/view/View;F)V

    .line 241702
    invoke-virtual {p0, p1}, LX/1Of;->f(LX/1a1;)V

    .line 241703
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 241704
    iget-object v0, p0, LX/1Oe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241705
    :cond_9
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 241706
    :cond_a
    iget-object v0, p0, LX/1Oe;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241707
    iget-object v0, p0, LX/1Oe;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241708
    iget-object v0, p0, LX/1Oe;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241709
    iget-object v0, p0, LX/1Oe;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241710
    invoke-static {p0}, LX/1Oe;->j(LX/1Oe;)V

    .line 241711
    return-void
.end method
