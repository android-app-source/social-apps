.class public LX/0RF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0RC;
.implements LX/0RD;


# instance fields
.field private a:LX/0QA;

.field public final b:LX/0RB;

.field private c:LX/0SA;


# direct methods
.method public constructor <init>(LX/0RB;)V
    .locals 0

    .prologue
    .line 59744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59745
    iput-object p1, p0, LX/0RF;->b:LX/0RB;

    .line 59746
    return-void
.end method

.method public static a(LX/0S7;)V
    .locals 0

    .prologue
    .line 59741
    invoke-virtual {p0}, LX/0S7;->b()V

    .line 59742
    invoke-virtual {p0}, LX/0S7;->c()V

    .line 59743
    return-void
.end method


# virtual methods
.method public final a(LX/0Or;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Or",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59747
    new-instance v0, LX/4ft;

    invoke-direct {v0, p0, p1}, LX/4ft;-><init>(LX/0RF;LX/0Or;)V

    return-object v0
.end method

.method public final a(LX/0QA;)V
    .locals 2

    .prologue
    .line 59738
    iput-object p1, p0, LX/0RF;->a:LX/0QA;

    .line 59739
    new-instance v0, LX/0SA;

    iget-object v1, p0, LX/0RF;->a:LX/0QA;

    invoke-direct {v0, v1, p0}, LX/0SA;-><init>(LX/0QA;LX/0RF;)V

    iput-object v0, p0, LX/0RF;->c:LX/0SA;

    .line 59740
    return-void
.end method

.method public annotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59737
    const-class v0, Ljavax/inject/Singleton;

    return-object v0
.end method

.method public enterScope()LX/0S7;
    .locals 2

    .prologue
    .line 59733
    iget-object v0, p0, LX/0RF;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    .line 59734
    invoke-virtual {v0}, LX/0S7;->a()V

    .line 59735
    iget-object v1, p0, LX/0RF;->c:LX/0SA;

    invoke-virtual {v0, v1}, LX/0S7;->a(LX/0R6;)V

    .line 59736
    return-object v0
.end method
