.class public LX/0nQ;
.super LX/0nR;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final c:LX/0nQ;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 135435
    new-instance v0, LX/0nQ;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/0nQ;-><init>(LX/0nY;)V

    sput-object v0, LX/0nQ;->c:LX/0nQ;

    return-void
.end method

.method private constructor <init>(LX/0nY;)V
    .locals 0

    .prologue
    .line 135436
    invoke-direct {p0, p1}, LX/0nR;-><init>(LX/0nY;)V

    .line 135437
    return-void
.end method

.method private static a(LX/0lS;)LX/2Aw;
    .locals 1

    .prologue
    .line 135438
    new-instance v0, LX/2Aw;

    invoke-direct {v0, p0}, LX/2Aw;-><init>(LX/0lS;)V

    return-object v0
.end method

.method private a(LX/0my;LX/2Aq;LX/1Y3;LX/2Az;ZLX/2An;)LX/2Ax;
    .locals 8

    .prologue
    .line 135439
    invoke-virtual {p2}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v1

    .line 135440
    invoke-virtual {p1}, LX/0mz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135441
    invoke-virtual {p6}, LX/2An;->k()V

    .line 135442
    :cond_0
    invoke-virtual {p6, p3}, LX/0lO;->a(LX/1Y3;)LX/0lJ;

    move-result-object v2

    .line 135443
    new-instance v0, LX/2B3;

    invoke-virtual {p2}, LX/2Aq;->b()LX/2Vb;

    move-result-object v3

    invoke-virtual {p4}, LX/2Az;->a()LX/0lQ;

    move-result-object v4

    invoke-virtual {p2}, LX/2Aq;->s()Z

    move-result v6

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, LX/2B3;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/0lQ;LX/2An;Z)V

    .line 135444
    invoke-virtual {p0, p1, p6}, LX/0nR;->a(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v3

    .line 135445
    instance-of v1, v3, LX/2B9;

    if-eqz v1, :cond_1

    move-object v1, v3

    .line 135446
    check-cast v1, LX/2B9;

    invoke-interface {v1, p1}, LX/2B9;->a(LX/0my;)V

    .line 135447
    :cond_1
    instance-of v1, v3, LX/0nU;

    if-eqz v1, :cond_2

    .line 135448
    check-cast v3, LX/0nU;

    invoke-interface {v3, p1, v0}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v3

    .line 135449
    :cond_2
    const/4 v5, 0x0

    .line 135450
    iget-object v0, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135451
    invoke-static {v0}, LX/1Xw;->e(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135452
    iget-object v0, p1, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 135453
    invoke-direct {p0, v2, v0, p6}, LX/0nQ;->b(LX/0lJ;LX/0m2;LX/2An;)LX/4qz;

    move-result-object v5

    .line 135454
    :cond_3
    iget-object v0, p1, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 135455
    invoke-direct {p0, v2, v0, p6}, LX/0nQ;->a(LX/0lJ;LX/0m2;LX/2An;)LX/4qz;

    move-result-object v4

    move-object v0, p4

    move-object v1, p2

    move-object v6, p6

    move v7, p5

    .line 135456
    invoke-virtual/range {v0 .. v7}, LX/2Az;->a(LX/2Aq;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;LX/4qz;LX/2An;Z)LX/2Ax;

    move-result-object v0

    .line 135457
    return-object v0
.end method

.method private static a(LX/0m2;LX/0lS;)LX/2Az;
    .locals 1

    .prologue
    .line 135458
    new-instance v0, LX/2Az;

    invoke-direct {v0, p0, p1}, LX/2Az;-><init>(LX/0m2;LX/0lS;)V

    return-object v0
.end method

.method private a(LX/0lJ;LX/0m2;LX/2An;)LX/4qz;
    .locals 3

    .prologue
    .line 135459
    invoke-virtual {p2}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    .line 135460
    invoke-virtual {v0, p2, p3, p1}, LX/0lU;->a(LX/0m4;LX/2An;LX/0lJ;)LX/4qy;

    move-result-object v1

    .line 135461
    if-nez v1, :cond_0

    .line 135462
    invoke-virtual {p0, p2, p1}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v0

    .line 135463
    :goto_0
    return-object v0

    .line 135464
    :cond_0
    iget-object v2, p2, LX/0m3;->_subtypeResolver:LX/0m0;

    move-object v2, v2

    .line 135465
    invoke-virtual {v2, p3, p2, v0, p1}, LX/0m0;->a(LX/2An;LX/0m4;LX/0lU;LX/0lJ;)Ljava/util/Collection;

    move-result-object v0

    .line 135466
    invoke-interface {v1, p2, p1, v0}, LX/4qy;->a(LX/0m2;LX/0lJ;Ljava/util/Collection;)LX/4qz;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0my;LX/0lS;Ljava/util/List;)LX/4rR;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lS;",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;)",
            "LX/4rR;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 135467
    invoke-virtual {p1}, LX/0lS;->d()LX/4qt;

    move-result-object v4

    .line 135468
    if-nez v4, :cond_0

    move-object v0, v1

    .line 135469
    :goto_0
    return-object v0

    .line 135470
    :cond_0
    iget-object v0, v4, LX/4qt;->b:Ljava/lang/Class;

    move-object v0, v0

    .line 135471
    const-class v2, LX/4pV;

    if-ne v0, v2, :cond_4

    .line 135472
    iget-object v0, v4, LX/4qt;->a:Ljava/lang/String;

    move-object v5, v0

    .line 135473
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    move v2, v3

    .line 135474
    :goto_1
    if-ne v2, v6, :cond_1

    .line 135475
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Object Id definition for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0lS;->b()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": can not find property with name \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135476
    :cond_1
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ax;

    .line 135477
    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 135478
    if-lez v2, :cond_2

    .line 135479
    invoke-interface {p2, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 135480
    invoke-interface {p2, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 135481
    :cond_2
    invoke-virtual {v0}, LX/2Ax;->a()LX/0lJ;

    move-result-object v2

    .line 135482
    new-instance v3, LX/4rS;

    invoke-direct {v3, v4, v0}, LX/4rS;-><init>(LX/4qt;LX/2Ax;)V

    .line 135483
    iget-boolean v0, v4, LX/4qt;->d:Z

    move v0, v0

    .line 135484
    invoke-static {v2, v1, v3, v0}, LX/4rR;->a(LX/0lJ;Ljava/lang/String;LX/4pS;Z)LX/4rR;

    move-result-object v0

    goto :goto_0

    .line 135485
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 135486
    :cond_4
    invoke-virtual {p0, v0}, LX/0mz;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    .line 135487
    invoke-virtual {p0}, LX/0mz;->c()LX/0li;

    move-result-object v1

    const-class v2, LX/4pS;

    invoke-virtual {v1, v0, v2}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v0

    aget-object v0, v0, v3

    .line 135488
    invoke-virtual {p1}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, LX/0mz;->a(LX/0lO;LX/4qt;)LX/4pS;

    move-result-object v1

    .line 135489
    iget-object v2, v4, LX/4qt;->a:Ljava/lang/String;

    move-object v2, v2

    .line 135490
    iget-boolean v3, v4, LX/4qt;->d:Z

    move v3, v3

    .line 135491
    invoke-static {v0, v2, v1, v3}, LX/4rR;->a(LX/0lJ;Ljava/lang/String;LX/4pS;Z)LX/4rR;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(LX/0my;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135492
    invoke-virtual {p2}, LX/0lS;->b()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/lang/Object;

    if-ne v1, v2, :cond_0

    .line 135493
    iget-object v0, p1, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 135494
    :goto_0
    return-object v0

    .line 135495
    :cond_0
    iget-object v1, p1, LX/0my;->_config:LX/0m2;

    move-object v9, v1

    .line 135496
    invoke-static {p2}, LX/0nQ;->a(LX/0lS;)LX/2Aw;

    move-result-object v8

    .line 135497
    iput-object v9, v8, LX/2Aw;->b:LX/0m2;

    .line 135498
    invoke-direct {p0, p1, p2, v8}, LX/0nQ;->a(LX/0my;LX/0lS;LX/2Aw;)Ljava/util/List;

    move-result-object v1

    .line 135499
    if-nez v1, :cond_1

    .line 135500
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 135501
    :cond_1
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135502
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0na;

    .line 135503
    invoke-virtual {v1, v2}, LX/0na;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object v2, v1

    .line 135504
    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 135505
    :cond_3
    invoke-static {v9, p2, v2}, LX/0nQ;->a(LX/0m2;LX/0lS;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 135506
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135507
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135508
    move-object v1, v1

    .line 135509
    goto :goto_2

    .line 135510
    :cond_4
    invoke-static {p1, p2, v1}, LX/0nQ;->a(LX/0my;LX/0lS;Ljava/util/List;)LX/4rR;

    move-result-object v2

    .line 135511
    iput-object v2, v8, LX/2Aw;->h:LX/4rR;

    .line 135512
    iput-object v1, v8, LX/2Aw;->c:Ljava/util/List;

    .line 135513
    invoke-static {v9, p2}, LX/0nQ;->b(LX/0m2;LX/0lS;)Ljava/lang/Object;

    move-result-object v1

    .line 135514
    iput-object v1, v8, LX/2Aw;->f:Ljava/lang/Object;

    .line 135515
    invoke-virtual {p2}, LX/0lS;->n()LX/2An;

    move-result-object v6

    .line 135516
    if-eqz v6, :cond_6

    .line 135517
    invoke-virtual {v9}, LX/0m4;->h()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 135518
    invoke-virtual {v6}, LX/2An;->k()V

    .line 135519
    :cond_5
    invoke-virtual {p2}, LX/0lS;->f()LX/1Y3;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/0lO;->a(LX/1Y3;)LX/0lJ;

    move-result-object v1

    .line 135520
    sget-object v2, LX/0m6;->USE_STATIC_TYPING:LX/0m6;

    invoke-virtual {v9, v2}, LX/0m4;->a(LX/0m6;)Z

    move-result v2

    .line 135521
    invoke-virtual {v1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v10

    .line 135522
    invoke-virtual {p0, v9, v10}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v3

    move-object v4, v0

    move-object v5, v0

    .line 135523
    invoke-static/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a([Ljava/lang/String;LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-result-object v11

    .line 135524
    new-instance v1, LX/2B3;

    invoke-virtual {v6}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, LX/0lS;->g()LX/0lQ;

    move-result-object v5

    const/4 v7, 0x0

    move-object v3, v10

    move-object v4, v0

    invoke-direct/range {v1 .. v7}, LX/2B3;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/0lQ;LX/2An;Z)V

    .line 135525
    new-instance v0, LX/4rL;

    invoke-direct {v0, v1, v6, v11}, LX/4rL;-><init>(LX/2Ay;LX/2An;Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;)V

    .line 135526
    iput-object v0, v8, LX/2Aw;->e:LX/4rL;

    .line 135527
    :cond_6
    invoke-static {v9, v8}, LX/0nQ;->a(LX/0m2;LX/2Aw;)V

    .line 135528
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0}, LX/0nY;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 135529
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v0, v8

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135530
    move-object v0, v0

    .line 135531
    goto :goto_3

    :cond_7
    move-object v0, v8

    .line 135532
    :cond_8
    invoke-virtual {v0}, LX/2Aw;->g()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 135533
    if-nez v1, :cond_9

    .line 135534
    invoke-virtual {p2}, LX/0lS;->e()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 135535
    invoke-virtual {v0}, LX/2Aw;->h()Lcom/fasterxml/jackson/databind/ser/BeanSerializer;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    .line 135536
    goto/16 :goto_0
.end method

.method private static a(LX/0m2;LX/0lS;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lS;",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135537
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    .line 135538
    invoke-virtual {p1}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    .line 135539
    invoke-virtual {v0, v1}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v0

    .line 135540
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    .line 135541
    invoke-static {v0}, LX/0nj;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    .line 135542
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 135543
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135544
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ax;

    invoke-virtual {v0}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135545
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 135546
    :cond_1
    return-object p2
.end method

.method private a(LX/0my;LX/0lS;LX/2Aw;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lS;",
            "LX/2Aw;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135394
    invoke-virtual {p2}, LX/0lS;->h()Ljava/util/List;

    move-result-object v1

    .line 135395
    iget-object v2, p1, LX/0my;->_config:LX/0m2;

    move-object v8, v2

    .line 135396
    invoke-static {v8, v1}, LX/0nQ;->a(LX/0m2;Ljava/util/List;)V

    .line 135397
    sget-object v2, LX/0m6;->REQUIRE_SETTERS_FOR_GETTERS:LX/0m6;

    invoke-virtual {v8, v2}, LX/0m4;->a(LX/0m6;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135398
    invoke-static {v1}, LX/0nQ;->a(Ljava/util/List;)V

    .line 135399
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135400
    :goto_0
    return-object v0

    .line 135401
    :cond_1
    invoke-static {v8, p2, v0}, LX/0nR;->a(LX/0m2;LX/0lS;LX/4qz;)Z

    move-result v5

    .line 135402
    invoke-static {v8, p2}, LX/0nQ;->a(LX/0m2;LX/0lS;)LX/2Az;

    move-result-object v4

    .line 135403
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 135404
    invoke-virtual {p2}, LX/0lS;->f()LX/1Y3;

    move-result-object v3

    .line 135405
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Aq;

    .line 135406
    invoke-virtual {v2}, LX/2Aq;->m()LX/2An;

    move-result-object v6

    .line 135407
    invoke-virtual {v2}, LX/2Aq;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135408
    if-eqz v6, :cond_2

    .line 135409
    invoke-virtual {v8}, LX/0m4;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135410
    invoke-virtual {v6}, LX/2An;->k()V

    .line 135411
    :cond_3
    invoke-virtual {p3, v6}, LX/2Aw;->a(LX/2An;)V

    goto :goto_1

    .line 135412
    :cond_4
    invoke-virtual {v2}, LX/2Aq;->q()LX/4pn;

    move-result-object v0

    .line 135413
    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/4pn;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 135414
    :cond_5
    instance-of v0, v6, LX/2At;

    if-eqz v0, :cond_6

    .line 135415
    check-cast v6, LX/2At;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/0nQ;->a(LX/0my;LX/2Aq;LX/1Y3;LX/2Az;ZLX/2An;)LX/2Ax;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 135416
    :cond_6
    check-cast v6, LX/2Am;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/0nQ;->a(LX/0my;LX/2Aq;LX/1Y3;LX/2Az;ZLX/2An;)LX/2Ax;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    move-object v0, v7

    .line 135417
    goto :goto_0
.end method

.method private static a(LX/0m2;LX/2Aw;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 135418
    iget-object v1, p1, LX/2Aw;->c:Ljava/util/List;

    move-object v3, v1

    .line 135419
    sget-object v1, LX/0m6;->DEFAULT_VIEW_INCLUSION:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v4

    .line 135420
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    .line 135421
    new-array v6, v5, [LX/2Ax;

    move v2, v0

    move v1, v0

    .line 135422
    :goto_0
    if-ge v2, v5, :cond_2

    .line 135423
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ax;

    .line 135424
    iget-object v7, v0, LX/2Ax;->p:[Ljava/lang/Class;

    move-object v7, v7

    .line 135425
    if-nez v7, :cond_0

    .line 135426
    if-eqz v4, :cond_1

    .line 135427
    aput-object v0, v6, v2

    move v0, v1

    .line 135428
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 135429
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 135430
    invoke-static {v0, v7}, LX/4rQ;->a(LX/2Ax;[Ljava/lang/Class;)LX/2Ax;

    move-result-object v0

    aput-object v0, v6, v2

    :cond_1
    move v0, v1

    goto :goto_1

    .line 135431
    :cond_2
    if-eqz v4, :cond_3

    if-nez v1, :cond_3

    .line 135432
    :goto_2
    return-void

    .line 135433
    :cond_3
    iput-object v6, p1, LX/2Aw;->d:[LX/2Ax;

    .line 135434
    goto :goto_2
.end method

.method private static a(LX/0m2;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135302
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v1

    .line 135303
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 135304
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 135305
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135306
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Aq;

    .line 135307
    invoke-virtual {v0}, LX/2Aq;->m()LX/2An;

    move-result-object v0

    .line 135308
    if-nez v0, :cond_1

    .line 135309
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 135310
    :cond_1
    invoke-virtual {v0}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v4

    .line 135311
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 135312
    if-nez v0, :cond_3

    .line 135313
    invoke-virtual {p0, v4}, LX/0m4;->c(Ljava/lang/Class;)LX/0lS;

    move-result-object v0

    .line 135314
    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    .line 135315
    invoke-virtual {v1, v0}, LX/0lU;->c(LX/0lN;)Ljava/lang/Boolean;

    move-result-object v0

    .line 135316
    if-nez v0, :cond_2

    .line 135317
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 135318
    :cond_2
    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135319
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135320
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 135321
    :cond_4
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135388
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 135389
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135390
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Aq;

    .line 135391
    invoke-virtual {v0}, LX/2Aq;->d()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, LX/2Aq;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135392
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 135393
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 135322
    invoke-static {p0}, LX/1Xw;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1Xw;->c(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0lJ;LX/0m2;LX/2An;)LX/4qz;
    .locals 4

    .prologue
    .line 135323
    invoke-virtual {p1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    .line 135324
    invoke-virtual {p2}, LX/0m4;->a()LX/0lU;

    move-result-object v1

    .line 135325
    invoke-virtual {v1, p2, p3, p1}, LX/0lU;->b(LX/0m4;LX/2An;LX/0lJ;)LX/4qy;

    move-result-object v2

    .line 135326
    if-nez v2, :cond_0

    .line 135327
    invoke-virtual {p0, p2, v0}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v0

    .line 135328
    :goto_0
    return-object v0

    .line 135329
    :cond_0
    iget-object v3, p2, LX/0m3;->_subtypeResolver:LX/0m0;

    move-object v3, v3

    .line 135330
    invoke-virtual {v3, p3, p2, v1, v0}, LX/0m0;->a(LX/2An;LX/0m4;LX/0lU;LX/0lJ;)Ljava/util/Collection;

    move-result-object v1

    .line 135331
    invoke-interface {v2, p2, v0, v1}, LX/4qy;->a(LX/0m2;LX/0lJ;Ljava/util/Collection;)LX/4qz;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135332
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135333
    invoke-static {v0}, LX/0nQ;->a(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135334
    invoke-virtual {p2}, LX/0lJ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135335
    const/4 v0, 0x0

    .line 135336
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p3}, LX/0nQ;->a(LX/0my;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/0m2;LX/0lS;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 135337
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    invoke-virtual {p1}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->d(LX/0lN;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private c(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            "Z)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135338
    invoke-virtual {p0, p1, p2, p3}, LX/0nR;->a(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135339
    if-eqz v0, :cond_1

    .line 135340
    :cond_0
    return-object v0

    .line 135341
    :cond_1
    iget-object v1, p1, LX/0my;->_config:LX/0m2;

    move-object v1, v1

    .line 135342
    invoke-virtual {p2}, LX/0lJ;->l()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 135343
    if-nez p4, :cond_2

    .line 135344
    const/4 v0, 0x0

    invoke-static {v1, p3, v0}, LX/0nR;->a(LX/0m2;LX/0lS;LX/4qz;)Z

    move-result p4

    .line 135345
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0nR;->b(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135346
    if-nez v0, :cond_0

    .line 135347
    :cond_3
    if-nez v0, :cond_4

    .line 135348
    invoke-static {p2}, LX/0nR;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135349
    if-nez v0, :cond_4

    .line 135350
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0nR;->a(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135351
    if-nez v0, :cond_4

    .line 135352
    invoke-direct {p0, p1, p2, p3}, LX/0nQ;->b(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135353
    if-nez v0, :cond_4

    .line 135354
    invoke-virtual {p0, v1, p2, p3, p4}, LX/0nR;->a(LX/0m2;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135355
    :cond_4
    if-eqz v0, :cond_0

    .line 135356
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135357
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135358
    move-object v0, v0

    .line 135359
    goto :goto_0

    .line 135360
    :cond_5
    invoke-virtual {p0}, LX/0nQ;->a()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nZ;

    .line 135361
    invoke-interface {v0, v1, p2, p3}, LX/0nZ;->a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135362
    if-nez v0, :cond_3

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0nY;)LX/0nS;
    .locals 3

    .prologue
    .line 135363
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    if-ne v0, p1, :cond_0

    .line 135364
    :goto_0
    return-object p0

    .line 135365
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, LX/0nQ;

    if-eq v0, v1, :cond_1

    .line 135366
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Subtype of BeanSerializerFactory ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has not properly overridden method \'withAdditionalSerializers\': can not instantiate subtype with additional serializer definitions"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135367
    :cond_1
    new-instance p0, LX/0nQ;

    invoke-direct {p0, p1}, LX/0nQ;-><init>(LX/0nY;)V

    goto :goto_0
.end method

.method public final a(LX/0my;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 135368
    iget-object v0, p1, LX/0my;->_config:LX/0m2;

    move-object v3, v0

    .line 135369
    invoke-virtual {v3, p2}, LX/0m2;->b(LX/0lJ;)LX/0lS;

    move-result-object v2

    .line 135370
    invoke-virtual {v2}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0nR;->a(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135371
    if-eqz v0, :cond_0

    .line 135372
    :goto_0
    return-object v0

    .line 135373
    :cond_0
    invoke-virtual {v2}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    invoke-static {v3, v0, p2}, LX/0nR;->a(LX/0m2;LX/0lO;LX/0lJ;)LX/0lJ;

    move-result-object v4

    .line 135374
    if-ne v4, p2, :cond_1

    .line 135375
    const/4 v0, 0x0

    .line 135376
    :goto_1
    invoke-virtual {v2}, LX/0lS;->q()LX/1Xr;

    move-result-object v5

    .line 135377
    if-nez v5, :cond_2

    .line 135378
    invoke-direct {p0, p1, v4, v2, v0}, LX/0nQ;->c(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0

    .line 135379
    :cond_1
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135380
    invoke-virtual {v4, v0}, LX/0lJ;->g(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 135381
    invoke-virtual {v3, v4}, LX/0m2;->b(LX/0lJ;)LX/0lS;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    goto :goto_1

    .line 135382
    :cond_2
    invoke-virtual {p1}, LX/0mz;->c()LX/0li;

    invoke-interface {v5}, LX/1Xr;->c()LX/0lJ;

    move-result-object v6

    .line 135383
    iget-object v0, v4, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135384
    invoke-virtual {v6, v0}, LX/0lJ;->g(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 135385
    invoke-virtual {v3, v6}, LX/0m2;->b(LX/0lJ;)LX/0lS;

    move-result-object v2

    .line 135386
    :cond_3
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    invoke-direct {p0, p1, v6, v2, v1}, LX/0nQ;->c(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    invoke-direct {v0, v5, v6, v1}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135387
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0}, LX/0nY;->c()Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
