.class public LX/0Rg;
.super LX/0Rh;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rh",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0Rg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rg",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final transient b:[LX/0P3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0P3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final transient c:[LX/0P3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0P3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final transient d:[Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final transient e:I

.field public final transient f:I

.field private transient g:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 60388
    new-instance v0, LX/0Rg;

    sget-object v3, LX/0P1;->EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;

    check-cast v3, [Ljava/util/Map$Entry;

    move-object v2, v1

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/0Rg;-><init>([LX/0P3;[LX/0P3;[Ljava/util/Map$Entry;II)V

    sput-object v0, LX/0Rg;->a:LX/0Rg;

    return-void
.end method

.method private constructor <init>([LX/0P3;[LX/0P3;[Ljava/util/Map$Entry;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/0P3",
            "<TK;TV;>;[",
            "LX/0P3",
            "<TK;TV;>;[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;II)V"
        }
    .end annotation

    .prologue
    .line 60389
    invoke-direct {p0}, LX/0Rh;-><init>()V

    .line 60390
    iput-object p1, p0, LX/0Rg;->b:[LX/0P3;

    .line 60391
    iput-object p2, p0, LX/0Rg;->c:[LX/0P3;

    .line 60392
    iput-object p3, p0, LX/0Rg;->d:[Ljava/util/Map$Entry;

    .line 60393
    iput p4, p0, LX/0Rg;->e:I

    .line 60394
    iput p5, p0, LX/0Rg;->f:I

    .line 60395
    return-void
.end method

.method public static a(I[Ljava/util/Map$Entry;)LX/0Rg;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)",
            "LX/0Rg",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60355
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, p0

    invoke-static {v0, v2}, LX/0PB;->checkPositionIndex(II)I

    .line 60356
    const-wide v2, 0x3ff3333333333333L    # 1.2

    move/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/0PC;->a(ID)I

    move-result v2

    .line 60357
    add-int/lit8 v6, v2, -0x1

    .line 60358
    invoke-static {v2}, LX/0P3;->a(I)[LX/0P3;

    move-result-object v3

    .line 60359
    invoke-static {v2}, LX/0P3;->a(I)[LX/0P3;

    move-result-object v4

    .line 60360
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, p0

    if-ne v0, v2, :cond_0

    move-object/from16 v5, p1

    .line 60361
    :goto_0
    const/4 v7, 0x0

    .line 60362
    const/4 v2, 0x0

    move v9, v2

    :goto_1
    move/from16 v0, p0

    if-ge v9, v0, :cond_4

    .line 60363
    aget-object v8, p1, v9

    .line 60364
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    .line 60365
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    .line 60366
    invoke-static {v10, v11}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 60367
    invoke-virtual {v10}, Ljava/lang/Object;->hashCode()I

    move-result v12

    .line 60368
    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v13

    .line 60369
    invoke-static {v12}, LX/0PC;->a(I)I

    move-result v2

    and-int v14, v2, v6

    .line 60370
    invoke-static {v13}, LX/0PC;->a(I)I

    move-result v2

    and-int v15, v2, v6

    .line 60371
    aget-object v2, v3, v14

    .line 60372
    invoke-static {v10, v8, v2}, LX/0PA;->a(Ljava/lang/Object;Ljava/util/Map$Entry;LX/0P3;)V

    .line 60373
    aget-object v16, v4, v15

    .line 60374
    move-object/from16 v0, v16

    invoke-static {v11, v8, v0}, LX/0Rg;->a(Ljava/lang/Object;Ljava/util/Map$Entry;LX/0P3;)V

    .line 60375
    if-nez v16, :cond_3

    if-nez v2, :cond_3

    .line 60376
    instance-of v2, v8, LX/0P3;

    if-eqz v2, :cond_1

    move-object v2, v8

    check-cast v2, LX/0P3;

    invoke-virtual {v2}, LX/0P3;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    .line 60377
    :goto_2
    if-eqz v2, :cond_2

    check-cast v8, LX/0P3;

    .line 60378
    :goto_3
    aput-object v8, v3, v14

    .line 60379
    aput-object v8, v4, v15

    .line 60380
    aput-object v8, v5, v9

    .line 60381
    xor-int v2, v12, v13

    add-int/2addr v7, v2

    .line 60382
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_1

    .line 60383
    :cond_0
    invoke-static/range {p0 .. p0}, LX/0P3;->a(I)[LX/0P3;

    move-result-object v5

    goto :goto_0

    .line 60384
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 60385
    :cond_2
    new-instance v8, LX/0P3;

    invoke-direct {v8, v10, v11}, LX/0P3;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 60386
    :cond_3
    new-instance v8, LX/4yF;

    move-object/from16 v0, v16

    invoke-direct {v8, v10, v11, v2, v0}, LX/4yF;-><init>(Ljava/lang/Object;Ljava/lang/Object;LX/0P3;LX/0P3;)V

    goto :goto_3

    .line 60387
    :cond_4
    new-instance v2, LX/0Rg;

    invoke-direct/range {v2 .. v7}, LX/0Rg;-><init>([LX/0P3;[LX/0P3;[Ljava/util/Map$Entry;II)V

    return-object v2
.end method

.method public static varargs a([Ljava/util/Map$Entry;)LX/0Rg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)",
            "LX/0Rg",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60354
    array-length v0, p0

    invoke-static {v0, p0}, LX/0Rg;->a(I[Ljava/util/Map$Entry;)LX/0Rg;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/util/Map$Entry;LX/0P3;)V
    .locals 2
    .param p2    # LX/0P3;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Map$Entry",
            "<**>;",
            "LX/0P3",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 60349
    :goto_0
    if-eqz p2, :cond_1

    .line 60350
    invoke-virtual {p2}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v1, "value"

    invoke-static {v0, v1, p1, p2}, LX/0P1;->checkNoConflict(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 60351
    invoke-virtual {p2}, LX/0P3;->b()LX/0P3;

    move-result-object p2

    goto :goto_0

    .line 60352
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 60353
    :cond_1
    return-void
.end method


# virtual methods
.method public final synthetic a_()LX/0Ri;
    .locals 1

    .prologue
    .line 60348
    invoke-virtual {p0}, LX/0Rg;->e()LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final createEntrySet()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 60396
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60397
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 60398
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/12n;

    iget-object v1, p0, LX/0Rg;->d:[Ljava/util/Map$Entry;

    invoke-direct {v0, p0, v1}, LX/12n;-><init>(LX/0P1;[Ljava/util/Map$Entry;)V

    goto :goto_0
.end method

.method public final e()LX/0Rh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 60343
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60344
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 60345
    :cond_0
    :goto_0
    return-object v0

    .line 60346
    :cond_1
    iget-object v0, p0, LX/0Rg;->g:LX/0Rh;

    .line 60347
    if-nez v0, :cond_0

    new-instance v0, LX/50P;

    invoke-direct {v0, p0}, LX/50P;-><init>(LX/0Rg;)V

    iput-object v0, p0, LX/0Rg;->g:LX/0Rh;

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 60342
    iget-object v0, p0, LX/0Rg;->b:[LX/0P3;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0Rg;->b:[LX/0P3;

    iget v1, p0, LX/0Rg;->e:I

    invoke-static {p1, v0, v1}, LX/0PA;->a(Ljava/lang/Object;[LX/0P3;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 60341
    iget v0, p0, LX/0Rg;->f:I

    return v0
.end method

.method public final isHashCodeFast()Z
    .locals 1

    .prologue
    .line 60340
    const/4 v0, 0x1

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 60338
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 60339
    iget-object v0, p0, LX/0Rg;->d:[Ljava/util/Map$Entry;

    array-length v0, v0

    return v0
.end method
