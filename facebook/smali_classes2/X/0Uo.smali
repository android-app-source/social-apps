.class public LX/0Uo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile ab:LX/0Uo;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;


# instance fields
.field public final A:Landroid/os/MessageQueue$IdleHandler;

.field public final B:Landroid/os/MessageQueue$IdleHandler;

.field public final C:Landroid/os/MessageQueue$IdleHandler;

.field private final D:Ljava/lang/Runnable;

.field private final E:Ljava/lang/Runnable;

.field private final F:Ljava/lang/Runnable;

.field private G:LX/0cJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/util/concurrent/ScheduledFuture;

.field public I:Ljava/util/concurrent/ScheduledFuture;

.field public volatile J:J

.field public volatile K:J

.field public volatile L:J

.field public volatile M:J

.field public volatile N:J

.field public volatile O:J

.field public volatile P:J

.field public volatile Q:J

.field public volatile R:J

.field public volatile S:Z

.field public volatile T:Z

.field public volatile U:Z

.field public volatile V:Z

.field public volatile W:J

.field public final X:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/0cK;",
            "Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private Y:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public Z:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile aa:Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

.field private final d:LX/0Uq;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:Landroid/content/Context;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0c8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/app/KeyguardManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ks;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final v:LX/0Ux;

.field public final w:LX/0Uy;

.field public final x:Ljava/lang/Runnable;

.field public final y:Ljava/lang/Runnable;

.field public final z:Landroid/os/MessageQueue$IdleHandler;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/0Uo;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".USER_MAYBE_BECAME_ACTIVE_OR_INACTIVE_IN_APP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Uo;->a:Ljava/lang/String;

    .line 66893
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "app_state/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 66894
    sput-object v0, LX/0Uo;->b:LX/0Tn;

    const-string v1, "last_first_run_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0Uo;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Uq;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;LX/0Or;LX/0Or;)V
    .locals 3
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/init/NeedsPostUpgradeInitOnBackgroundThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uq;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 66895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66896
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66897
    iput-object v0, p0, LX/0Uo;->j:LX/0Ot;

    .line 66898
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66899
    iput-object v0, p0, LX/0Uo;->k:LX/0Ot;

    .line 66900
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66901
    iput-object v0, p0, LX/0Uo;->l:LX/0Ot;

    .line 66902
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66903
    iput-object v0, p0, LX/0Uo;->m:LX/0Ot;

    .line 66904
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66905
    iput-object v0, p0, LX/0Uo;->n:LX/0Ot;

    .line 66906
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66907
    iput-object v0, p0, LX/0Uo;->o:LX/0Ot;

    .line 66908
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66909
    iput-object v0, p0, LX/0Uo;->p:LX/0Ot;

    .line 66910
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66911
    iput-object v0, p0, LX/0Uo;->q:LX/0Ot;

    .line 66912
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66913
    iput-object v0, p0, LX/0Uo;->r:LX/0Ot;

    .line 66914
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66915
    iput-object v0, p0, LX/0Uo;->s:LX/0Ot;

    .line 66916
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66917
    iput-object v0, p0, LX/0Uo;->t:LX/0Ot;

    .line 66918
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 66919
    iput-object v0, p0, LX/0Uo;->u:LX/0Ot;

    .line 66920
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/appstate/AppStateManager$1;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->x:Ljava/lang/Runnable;

    .line 66921
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/common/appstate/AppStateManager$2;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->y:Ljava/lang/Runnable;

    .line 66922
    new-instance v0, LX/0Us;

    invoke-direct {v0, p0}, LX/0Us;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->z:Landroid/os/MessageQueue$IdleHandler;

    .line 66923
    new-instance v0, LX/0Ut;

    invoke-direct {v0, p0}, LX/0Ut;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->A:Landroid/os/MessageQueue$IdleHandler;

    .line 66924
    new-instance v0, LX/0Uu;

    invoke-direct {v0, p0}, LX/0Uu;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->B:Landroid/os/MessageQueue$IdleHandler;

    .line 66925
    new-instance v0, LX/0Uv;

    invoke-direct {v0, p0}, LX/0Uv;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->C:Landroid/os/MessageQueue$IdleHandler;

    .line 66926
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$7;

    invoke-direct {v0, p0}, Lcom/facebook/common/appstate/AppStateManager$7;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->D:Ljava/lang/Runnable;

    .line 66927
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$8;

    invoke-direct {v0, p0}, Lcom/facebook/common/appstate/AppStateManager$8;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->E:Ljava/lang/Runnable;

    .line 66928
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$9;

    invoke-direct {v0, p0}, Lcom/facebook/common/appstate/AppStateManager$9;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->F:Ljava/lang/Runnable;

    .line 66929
    iput-boolean v2, p0, LX/0Uo;->U:Z

    .line 66930
    iput-boolean v2, p0, LX/0Uo;->V:Z

    .line 66931
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0Uo;->W:J

    .line 66932
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/0Uo;->X:LX/01J;

    .line 66933
    iput v2, p0, LX/0Uo;->Y:I

    .line 66934
    iput v2, p0, LX/0Uo;->Z:I

    .line 66935
    new-instance v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    invoke-direct {v0}, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;-><init>()V

    iput-object v0, p0, LX/0Uo;->aa:Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    .line 66936
    new-instance v0, LX/0Ux;

    invoke-direct {v0, p0}, LX/0Ux;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->v:LX/0Ux;

    .line 66937
    new-instance v0, LX/0Uy;

    invoke-direct {v0, p0}, LX/0Uy;-><init>(LX/0Uo;)V

    iput-object v0, p0, LX/0Uo;->w:LX/0Uy;

    .line 66938
    iput-object p1, p0, LX/0Uo;->d:LX/0Uq;

    .line 66939
    iput-object p2, p0, LX/0Uo;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 66940
    iput-object p3, p0, LX/0Uo;->f:Landroid/content/Context;

    .line 66941
    iput-object p4, p0, LX/0Uo;->h:LX/0Or;

    .line 66942
    iput-object p5, p0, LX/0Uo;->g:LX/0Or;

    .line 66943
    return-void
.end method

.method public static A(LX/0Uo;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66859
    iget-object v0, p0, LX/0Uo;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0Uo;->c:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 66860
    iget-object v0, p0, LX/0Uo;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 66861
    :try_start_0
    iget-object v0, p0, LX/0Uo;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 66862
    iget-wide v8, v6, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-wide v10, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 66863
    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    iput-wide v10, p0, LX/0Uo;->W:J

    .line 66864
    cmp-long v0, v8, v4

    if-lez v0, :cond_0

    .line 66865
    iget-wide v4, v6, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v0, v8, v4

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/0Uo;->U:Z

    .line 66866
    iget-wide v4, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v0, v8, v4

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/0Uo;->V:Z

    .line 66867
    iget-object v0, p0, LX/0Uo;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v4, LX/0Uo;->c:LX/0Tn;

    invoke-interface {v0, v4, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 66868
    iget-wide v4, v6, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-wide v6, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    .line 66869
    iget-object v0, p0, LX/0Uo;->h:LX/0Or;

    if-nez v0, :cond_4
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 66870
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 66871
    goto :goto_0

    :cond_2
    move v0, v2

    .line 66872
    goto :goto_1

    .line 66873
    :catch_0
    move-exception v0

    .line 66874
    const-string v4, "AppStateManager"

    const-string v5, "Can\'t find our own package name : %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v4, v0, v5, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 66875
    :catch_1
    move-exception v0

    .line 66876
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Landroid/os/DeadObjectException;

    if-eqz v1, :cond_3

    .line 66877
    const-string v1, "AppStateManager"

    const-string v3, "PackageManager connection lost"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 66878
    :cond_3
    throw v0

    .line 66879
    :cond_4
    iget-object v0, p0, LX/0Uo;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 66880
    if-eqz v0, :cond_0

    .line 66881
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    .line 66882
    iget-object v4, p0, LX/0Uo;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/common/appstate/AppStateManager$12;

    invoke-direct {v6, p0, v0}, Lcom/facebook/common/appstate/AppStateManager$12;-><init>(LX/0Uo;LX/0Up;)V

    const v0, -0x45827ddb

    invoke-static {v4, v6, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3
.end method

.method public static I(LX/0Uo;)V
    .locals 2

    .prologue
    .line 66944
    iget-object v0, p0, LX/0Uo;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, LX/0Uo;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 66945
    return-void
.end method

.method public static J(LX/0Uo;)V
    .locals 2

    .prologue
    .line 66946
    iget-object v0, p0, LX/0Uo;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 66947
    iget-object v0, p0, LX/0Uo;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, LX/0Uo;->E:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 66948
    return-void
.end method

.method public static K(LX/0Uo;)V
    .locals 2

    .prologue
    .line 66949
    iget-object v0, p0, LX/0Uo;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    sget-object v1, LX/0Uo;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 66950
    iget-object v0, p0, LX/0Uo;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, LX/0Uo;->F:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 66951
    return-void
.end method

.method public static declared-synchronized M(LX/0Uo;)V
    .locals 11

    .prologue
    .line 66952
    monitor-enter p0

    .line 66953
    :try_start_0
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    if-nez v1, :cond_3

    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v1

    if-nez v1, :cond_3

    .line 66954
    invoke-static {p0}, LX/0Uo;->I(LX/0Uo;)V

    .line 66955
    iget-object v1, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, p0, LX/0Uo;->R:J

    .line 66956
    :cond_0
    :goto_0
    const/4 v4, 0x0

    .line 66957
    iget-object v3, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v3, :cond_1

    .line 66958
    iget-object v3, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v3, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 66959
    const/4 v3, 0x0

    iput-object v3, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    .line 66960
    :cond_1
    iget-object v3, p0, LX/0Uo;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v5, LX/0X5;->fI:J

    invoke-interface {v3, v5, v6, v4}, LX/0W4;->a(JZ)Z

    move-result v3

    if-nez v3, :cond_2

    .line 66961
    iget-object v3, p0, LX/0Uo;->m:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v5, p0, LX/0Uo;->y:Ljava/lang/Runnable;

    iget-object v4, p0, LX/0Uo;->g:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0W3;

    sget-wide v7, LX/0X5;->fH:J

    const-wide/16 v9, 0x12c

    invoke-interface {v4, v7, v8, v9, v10}, LX/0W4;->a(JJ)J

    move-result-wide v7

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v5, v7, v8, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    .line 66962
    :cond_2
    invoke-static {p0}, LX/0Uo;->K(LX/0Uo;)V

    .line 66963
    iget v0, p0, LX/0Uo;->Z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Uo;->Z:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66964
    monitor-exit p0

    return-void

    .line 66965
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66966
    :cond_3
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 66967
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 66968
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static declared-synchronized N(LX/0Uo;)V
    .locals 6

    .prologue
    .line 66969
    monitor-enter p0

    .line 66970
    :try_start_0
    iget-object v1, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 66971
    iget-object v1, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 66972
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Uo;->I:Ljava/util/concurrent/ScheduledFuture;

    .line 66973
    :cond_0
    iget-boolean v1, p0, LX/0Uo;->S:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, LX/0Uo;->T:Z

    if-eqz v1, :cond_1

    .line 66974
    iget-object v1, p0, LX/0Uo;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/0Uo;->x:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    .line 66975
    :cond_1
    invoke-static {p0}, LX/0Uo;->K(LX/0Uo;)V

    .line 66976
    iget v0, p0, LX/0Uo;->Z:I

    if-lez v0, :cond_2

    .line 66977
    iget v0, p0, LX/0Uo;->Z:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0Uo;->Z:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66978
    :cond_2
    monitor-exit p0

    return-void

    .line 66979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized O(LX/0Uo;)V
    .locals 3

    .prologue
    .line 66980
    monitor-enter p0

    .line 66981
    :try_start_0
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    if-nez v1, :cond_1

    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 66982
    invoke-static {p0}, LX/0Uo;->I(LX/0Uo;)V

    .line 66983
    iget-object v1, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, p0, LX/0Uo;->R:J

    .line 66984
    :cond_0
    :goto_0
    invoke-static {p0}, LX/0Uo;->K(LX/0Uo;)V

    .line 66985
    iget v0, p0, LX/0Uo;->Y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Uo;->Y:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66986
    monitor-exit p0

    return-void

    .line 66987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66988
    :cond_1
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 66989
    iget-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 66990
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static declared-synchronized P(LX/0Uo;)V
    .locals 6

    .prologue
    .line 66991
    monitor-enter p0

    .line 66992
    :try_start_0
    iget-object v1, p0, LX/0Uo;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/0Uo;->x:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, LX/0Uo;->H:Ljava/util/concurrent/ScheduledFuture;

    .line 66993
    invoke-static {p0}, LX/0Uo;->K(LX/0Uo;)V

    .line 66994
    iget v0, p0, LX/0Uo;->Y:I

    if-lez v0, :cond_0

    .line 66995
    iget v0, p0, LX/0Uo;->Y:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0Uo;->Y:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66996
    :cond_0
    monitor-exit p0

    return-void

    .line 66997
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/0Uo;
    .locals 3

    .prologue
    .line 66998
    sget-object v0, LX/0Uo;->ab:LX/0Uo;

    if-nez v0, :cond_1

    .line 66999
    const-class v1, LX/0Uo;

    monitor-enter v1

    .line 67000
    :try_start_0
    sget-object v0, LX/0Uo;->ab:LX/0Uo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67001
    if-eqz v2, :cond_0

    .line 67002
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0Uo;->b(LX/0QB;)LX/0Uo;

    move-result-object v0

    sput-object v0, LX/0Uo;->ab:LX/0Uo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67003
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67004
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67005
    :cond_1
    sget-object v0, LX/0Uo;->ab:LX/0Uo;

    return-object v0

    .line 67006
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0Uo;Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;)V
    .locals 2

    .prologue
    .line 67008
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    iget-boolean v1, p2, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    if-eq v0, v1, :cond_0

    .line 67009
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    if-eqz v0, :cond_2

    .line 67010
    invoke-static {p0}, LX/0Uo;->O(LX/0Uo;)V

    .line 67011
    :cond_0
    :goto_0
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    iget-boolean v1, p2, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    if-eq v0, v1, :cond_1

    .line 67012
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    if-eqz v0, :cond_3

    .line 67013
    invoke-static {p0}, LX/0Uo;->M(LX/0Uo;)V

    .line 67014
    :cond_1
    :goto_1
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    iput-boolean v0, p2, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    .line 67015
    iget-boolean v0, p1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    iput-boolean v0, p2, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67016
    monitor-exit p0

    return-void

    .line 67017
    :cond_2
    :try_start_1
    invoke-static {p0}, LX/0Uo;->P(LX/0Uo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 67018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67019
    :cond_3
    :try_start_2
    invoke-static {p0}, LX/0Uo;->N(LX/0Uo;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/0Uo;
    .locals 14

    .prologue
    .line 67020
    new-instance v0, LX/0Uo;

    invoke-static {p0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v1

    check-cast v1, LX/0Uq;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 67021
    new-instance v4, LX/0Ur;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-direct {v4, v5}, LX/0Ur;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 67022
    const/16 v5, 0xdf4

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/0Uo;-><init>(LX/0Uq;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;LX/0Or;LX/0Or;)V

    .line 67023
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x246

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1ce

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x271

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1430

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe0c

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1c6

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2db

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x19

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x140f

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1032

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x240

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    .line 67024
    iput-object v1, v0, LX/0Uo;->i:LX/0Uh;

    iput-object v2, v0, LX/0Uo;->j:LX/0Ot;

    iput-object v3, v0, LX/0Uo;->k:LX/0Ot;

    iput-object v4, v0, LX/0Uo;->l:LX/0Ot;

    iput-object v5, v0, LX/0Uo;->m:LX/0Ot;

    iput-object v6, v0, LX/0Uo;->n:LX/0Ot;

    iput-object v7, v0, LX/0Uo;->o:LX/0Ot;

    iput-object v8, v0, LX/0Uo;->p:LX/0Ot;

    iput-object v9, v0, LX/0Uo;->q:LX/0Ot;

    iput-object v10, v0, LX/0Uo;->r:LX/0Ot;

    iput-object v11, v0, LX/0Uo;->s:LX/0Ot;

    iput-object v12, v0, LX/0Uo;->t:LX/0Ot;

    iput-object v13, v0, LX/0Uo;->u:LX/0Ot;

    .line 67025
    return-object v0
.end method

.method public static v(LX/0Uo;)LX/0cJ;
    .locals 4

    .prologue
    .line 67026
    iget-object v0, p0, LX/0Uo;->G:LX/0cJ;

    if-nez v0, :cond_1

    .line 67027
    monitor-enter p0

    .line 67028
    :try_start_0
    iget-object v0, p0, LX/0Uo;->G:LX/0cJ;

    if-nez v0, :cond_0

    .line 67029
    iget-object v0, p0, LX/0Uo;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0c8;

    const-string v2, "com.facebook.common.appstate.peers"

    iget-object v1, p0, LX/0Uo;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0c8;->a(Ljava/lang/String;LX/0Xl;Z)LX/0cJ;

    move-result-object v0

    iput-object v0, p0, LX/0Uo;->G:LX/0cJ;

    .line 67030
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67031
    :cond_1
    iget-object v0, p0, LX/0Uo;->G:LX/0cJ;

    return-object v0

    .line 67032
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static w$redex0(LX/0Uo;)V
    .locals 2

    .prologue
    .line 66883
    iget-object v0, p0, LX/0Uo;->aa:Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    iget-boolean v1, p0, LX/0Uo;->S:Z

    iput-boolean v1, v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    .line 66884
    iget-object v0, p0, LX/0Uo;->aa:Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    iget-boolean v1, p0, LX/0Uo;->T:Z

    iput-boolean v1, v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    .line 66885
    invoke-static {p0}, LX/0Uo;->v(LX/0Uo;)LX/0cJ;

    move-result-object v0

    invoke-static {p0}, LX/0Uo;->x(LX/0Uo;)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0cJ;->a(Landroid/os/Message;)V

    .line 66886
    return-void
.end method

.method public static x(LX/0Uo;)Landroid/os/Message;
    .locals 5

    .prologue
    .line 66887
    const/4 v0, 0x0

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 66888
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 66889
    const-string v2, "app_state_info"

    new-instance v3, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    iget-object v4, p0, LX/0Uo;->aa:Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    invoke-direct {v3, v4}, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;-><init>(Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66890
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 66891
    return-object v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 66818
    const-string v0, "AppStateManager.notifyApplicationOnCreateComplete"

    const v1, -0x5029afbf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 66819
    :try_start_0
    iput-wide p1, p0, LX/0Uo;->J:J

    .line 66820
    iget-object v0, p0, LX/0Uo;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/common/appstate/AppStateManager$10;

    invoke-direct {v1, p0}, Lcom/facebook/common/appstate/AppStateManager$10;-><init>(LX/0Uo;)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 66821
    iget-object v0, p0, LX/0Uo;->d:LX/0Uq;

    new-instance v1, LX/0Uz;

    invoke-direct {v1, p0}, LX/0Uz;-><init>(LX/0Uo;)V

    invoke-virtual {v0, v1}, LX/0Uq;->a(LX/0V0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66822
    const v0, 0x1ea72377

    invoke-static {v0}, LX/02m;->a(I)V

    .line 66823
    return-void

    .line 66824
    :catchall_0
    move-exception v0

    const v1, -0x56f82b53

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(J)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    .line 66813
    invoke-virtual {p0}, LX/0Uo;->c()J

    move-result-wide v2

    const-wide/16 v4, 0xfa0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 66814
    :cond_0
    :goto_0
    return v0

    .line 66815
    :cond_1
    iget-object v7, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    iget-wide v9, p0, LX/0Uo;->Q:J

    sub-long/2addr v7, v9

    move-wide v2, v7

    .line 66816
    cmp-long v1, v2, p1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 66817
    iget-object v0, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/0Uo;->J:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 66825
    iget-object v0, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/0Uo;->K:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 66826
    iget-wide v0, p0, LX/0Uo;->K:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 66827
    iget-object v0, p0, LX/0Uo;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/14i;->b:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66828
    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Uo;->q()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 66829
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 66830
    goto :goto_0

    .line 66831
    :cond_1
    invoke-virtual {p0}, LX/0Uo;->j()Z

    move-result v0

    goto :goto_0
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 66832
    invoke-static {p0}, LX/0Uo;->v(LX/0Uo;)LX/0cJ;

    move-result-object v0

    .line 66833
    new-instance v1, LX/28X;

    invoke-direct {v1, p0}, LX/28X;-><init>(LX/0Uo;)V

    invoke-interface {v0, v1}, LX/0cJ;->a(LX/0cN;)V

    .line 66834
    const/16 v1, 0x2710

    new-instance v2, LX/28Y;

    invoke-direct {v2, p0}, LX/28Y;-><init>(LX/0Uo;)V

    invoke-interface {v0, v1, v2}, LX/0cJ;->a(ILX/0cM;)V

    .line 66835
    invoke-interface {v0}, LX/0Up;->init()V

    .line 66836
    invoke-static {p0}, LX/0Uo;->w$redex0(LX/0Uo;)V

    .line 66837
    return-void
.end method

.method public final j()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x1388

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66838
    iget-object v0, p0, LX/0Uo;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/14i;->a:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66839
    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Uo;->q()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    move v0, v1

    .line 66840
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 66841
    goto :goto_0

    .line 66842
    :cond_1
    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/0Uo;->c()J

    move-result-wide v4

    const-wide/16 v6, 0xfa0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    invoke-virtual {p0}, LX/0Uo;->q()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final k()LX/03R;
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa0

    const-wide/16 v2, 0x0

    .line 66843
    iget-wide v0, p0, LX/0Uo;->K:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 66844
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 66845
    :goto_0
    return-object v0

    .line 66846
    :cond_0
    iget-wide v0, p0, LX/0Uo;->L:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 66847
    invoke-virtual {p0}, LX/0Uo;->d()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 66848
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 66849
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 66850
    :cond_2
    iget-wide v0, p0, LX/0Uo;->L:J

    iget-wide v2, p0, LX/0Uo;->K:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    :cond_3
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public final l()Z
    .locals 4

    .prologue
    .line 66851
    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66852
    iget-object v0, p0, LX/0Uo;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    move v0, v0

    .line 66853
    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, LX/0Uo;->o()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/0Uo;->c()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 66854
    iget-wide v0, p0, LX/0Uo;->N:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, LX/0Uo;->O:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized o()Z
    .locals 1

    .prologue
    .line 66855
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0Uo;->Z:I

    if-gtz v0, :cond_0

    iget v0, p0, LX/0Uo;->Y:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final q()J
    .locals 6

    .prologue
    .line 66856
    iget-object v0, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/0Uo;->M:J

    sub-long v2, v0, v2

    iget-object v0, p0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, LX/0Uo;->P:J

    sub-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final declared-synchronized t()I
    .locals 1

    .prologue
    .line 66857
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0Uo;->Z:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized u()I
    .locals 1

    .prologue
    .line 66858
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0Uo;->Y:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
