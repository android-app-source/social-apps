.class public LX/10M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/10N;

.field public final c:LX/0lC;

.field public final d:LX/03V;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0WJ;

.field private final g:LX/0So;

.field private final h:LX/10O;

.field public final i:LX/0SG;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;LX/0Or;LX/0WJ;LX/10N;LX/0So;LX/10O;LX/0SG;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0WJ;",
            "LX/10N;",
            "LX/0So;",
            "LX/10O;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 168829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168830
    iput-object p1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 168831
    iput-object p2, p0, LX/10M;->c:LX/0lC;

    .line 168832
    iput-object p3, p0, LX/10M;->d:LX/03V;

    .line 168833
    iput-object p4, p0, LX/10M;->e:LX/0Or;

    .line 168834
    iput-object p5, p0, LX/10M;->f:LX/0WJ;

    .line 168835
    iput-object p6, p0, LX/10M;->b:LX/10N;

    .line 168836
    iput-object p7, p0, LX/10M;->g:LX/0So;

    .line 168837
    iput-object p8, p0, LX/10M;->h:LX/10O;

    .line 168838
    iput-object p9, p0, LX/10M;->i:LX/0SG;

    .line 168839
    return-void
.end method

.method public static a(LX/0QB;)LX/10M;
    .locals 1

    .prologue
    .line 168840
    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/10M;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168841
    new-instance v0, LX/3T3;

    invoke-direct {v0, p0}, LX/3T3;-><init>(LX/10M;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 168842
    return-object p1
.end method

.method public static b(LX/0QB;)LX/10M;
    .locals 10

    .prologue
    .line 168843
    new-instance v0, LX/10M;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    invoke-static {p0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v6

    check-cast v6, LX/10N;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {p0}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v8

    check-cast v8, LX/10O;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-direct/range {v0 .. v9}, LX/10M;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;LX/0Or;LX/0WJ;LX/10N;LX/0So;LX/10O;LX/0SG;)V

    .line 168844
    return-object v0
.end method

.method public static i(LX/10M;Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 168845
    iget-object v0, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/2ss;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168846
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 168847
    :goto_0
    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168848
    invoke-virtual {p0}, LX/10M;->b()Ljava/util/List;

    move-result-object v0

    .line 168849
    iget-object v1, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v1}, LX/10N;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v1}, LX/10N;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 168850
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 168851
    iget-object v2, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/26p;->z:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 168852
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 168853
    const/4 v5, 0x0

    .line 168854
    :try_start_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    move-object v1, v3

    check-cast v1, Ljava/lang/String;

    move-object v2, v1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    .line 168855
    :goto_1
    if-eqz v2, :cond_1

    .line 168856
    :try_start_1
    iget-object v3, p0, LX/10M;->c:LX/0lC;

    const-class v5, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v3, v2, v5}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 168857
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 168858
    :catch_0
    move-exception v2

    .line 168859
    iget-object v3, p0, LX/10M;->d:LX/03V;

    const-string v5, "FB4ADBLStoreManager"

    const-string v7, "Error encountered in reading the DBLPasswordAccountcredentials from FbSharedPreferences"

    invoke-virtual {v3, v5, v7, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168860
    :catch_1
    move-exception v3

    .line 168861
    iget-object v7, p0, LX/10M;->d:LX/03V;

    const-string v8, "FB4ADBLStoreManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "retrievePasswordAccounts: Error encountered in casting password account credential value from Object of type "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " to String"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v8, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v5

    goto :goto_1

    .line 168862
    :cond_2
    iget-object v2, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v2}, LX/10N;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 168863
    invoke-static {p0, v4}, LX/10M;->a(LX/10M;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 168864
    :goto_2
    move-object v1, v2

    .line 168865
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 168866
    :cond_3
    iget-object v1, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v1}, LX/10N;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 168867
    invoke-static {p0, v0}, LX/10M;->a(LX/10M;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 168868
    :cond_4
    return-object v0

    :cond_5
    move-object v2, v4

    goto :goto_2
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 4

    .prologue
    .line 168869
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 168870
    :goto_0
    return-void

    .line 168871
    :cond_0
    sget-object v0, LX/26p;->k:LX/0Tn;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168872
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/10M;->d(Ljava/lang/String;)V

    .line 168873
    :try_start_0
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 168874
    iget-object v2, p0, LX/10M;->c:LX/0lC;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 168875
    invoke-interface {v1}, LX/0hN;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168876
    :catch_0
    move-exception v0

    .line 168877
    iget-object v1, p0, LX/10M;->d:LX/03V;

    const-string v2, "FB4ADBLStoreManager"

    const-string v3, "Error encountered in saving the DBLcredentials in FbSharedPreferences"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 168878
    if-nez p1, :cond_0

    .line 168879
    :goto_0
    return-void

    .line 168880
    :cond_0
    sget-object v0, LX/26p;->k:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168881
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 168882
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 168883
    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 168884
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 168885
    if-nez p1, :cond_0

    .line 168886
    :goto_0
    return-object v1

    .line 168887
    :cond_0
    invoke-virtual {p0, p1}, LX/10M;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168888
    sget-object v0, LX/26p;->z:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168889
    :goto_1
    iget-object v2, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168890
    if-eqz v0, :cond_2

    .line 168891
    :try_start_0
    iget-object v2, p0, LX/10M;->c:LX/0lC;

    const-class v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v1, v0

    .line 168892
    goto :goto_0

    .line 168893
    :cond_1
    sget-object v0, LX/26p;->k:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    goto :goto_1

    .line 168894
    :catch_0
    move-exception v0

    .line 168895
    iget-object v2, p0, LX/10M;->d:LX/03V;

    const-string v3, "FB4ADBLStoreManager"

    const-string v4, "Error encountered in reading the DBLcredentials from FbSharedPreferences"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final b()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168808
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 168809
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->k:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 168810
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 168811
    const/4 v4, 0x0

    .line 168812
    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    .line 168813
    :goto_1
    if-eqz v1, :cond_0

    .line 168814
    :try_start_1
    iget-object v2, p0, LX/10M;->c:LX/0lC;

    const-class v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v2, v1, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 168815
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 168816
    :catch_0
    move-exception v1

    .line 168817
    iget-object v2, p0, LX/10M;->d:LX/03V;

    const-string v4, "FB4ADBLStoreManager"

    const-string v6, "Error encountered in reading the DBLcredentials from FbSharedPreferences"

    invoke-virtual {v2, v4, v6, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168818
    :catch_1
    move-exception v2

    .line 168819
    iget-object v6, p0, LX/10M;->d:LX/03V;

    const-string v7, "FB4ADBLStoreManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "retrieveOneTapLoginAccounts: Error encountered in casting one tap login credential value from Object of type "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " to String"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v7, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v4

    goto :goto_1

    .line 168820
    :cond_1
    iget-object v1, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v1}, LX/10N;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168821
    invoke-static {p0, v3}, LX/10M;->a(LX/10M;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 168822
    :goto_2
    return-object v1

    :cond_2
    move-object v1, v3

    goto :goto_2
.end method

.method public final b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 4

    .prologue
    .line 168823
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 168824
    :goto_0
    return-void

    .line 168825
    :cond_0
    sget-object v0, LX/26p;->z:LX/0Tn;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168826
    :try_start_0
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/10M;->c:LX/0lC;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/26p;->y:LX/0Tn;

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, 0x3

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168827
    :catch_0
    move-exception v0

    .line 168828
    iget-object v1, p0, LX/10M;->d:LX/03V;

    const-string v2, "FB4ADBLStoreManager"

    const-string v3, "Error encountered in saving the Password account credentials in FbSharedPreferences"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 168731
    invoke-virtual {p0}, LX/10M;->d()I

    move-result v1

    .line 168732
    const/4 v0, 0x0

    .line 168733
    iget-object v2, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v2}, LX/10N;->b()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v2}, LX/10N;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168734
    :cond_0
    invoke-virtual {p0}, LX/10M;->i()I

    move-result v0

    .line 168735
    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 168736
    if-nez p1, :cond_0

    .line 168737
    const/4 v0, 0x0

    .line 168738
    :goto_0
    return v0

    .line 168739
    :cond_0
    sget-object v0, LX/26p;->k:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168740
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 168741
    iget-object v0, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->k:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 168742
    invoke-virtual {p0, p1}, LX/10M;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168743
    sget-object v0, LX/26p;->z:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168744
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-static {p1}, LX/2ss;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 168745
    :cond_0
    return-void
.end method

.method public final e()Lcom/facebook/auth/credentials/DBLFacebookCredentials;
    .locals 2

    .prologue
    .line 168746
    invoke-virtual {p0}, LX/10M;->a()Ljava/util/List;

    move-result-object v0

    .line 168747
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168748
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 168749
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 168750
    if-nez p1, :cond_0

    .line 168751
    const/4 v0, 0x0

    .line 168752
    :goto_0
    return v0

    .line 168753
    :cond_0
    sget-object v0, LX/26p;->z:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168754
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 168755
    invoke-virtual {p0}, LX/10M;->c()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 168756
    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 168757
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    .line 168758
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 168759
    iget-object v0, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->z:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 168760
    iget-object v0, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->r:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final m()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 168761
    iget-object v0, p0, LX/10M;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/user/model/User;

    .line 168762
    if-eqz v6, :cond_0

    .line 168763
    iget-object v0, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v0

    .line 168764
    if-nez v1, :cond_1

    .line 168765
    :cond_0
    :goto_0
    return-void

    .line 168766
    :cond_1
    const/4 v7, -0x1

    const/4 v4, 0x0

    .line 168767
    if-nez v1, :cond_5

    move v0, v4

    .line 168768
    :goto_1
    move v0, v0

    .line 168769
    if-eqz v0, :cond_0

    .line 168770
    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168771
    iget-object v3, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3, v0, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 168772
    add-int/lit8 v3, v3, 0x1

    .line 168773
    iget-object v4, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v0, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 168774
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 168775
    const-string v0, "counter_value"

    invoke-virtual {v11, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168776
    iget-object v0, p0, LX/10M;->h:LX/10O;

    const-string v4, "password_account_counter_incremented"

    invoke-virtual {v0, v4, v11}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 168777
    invoke-virtual {p0}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->j()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 168778
    :goto_2
    if-eqz v0, :cond_4

    const/4 v0, 0x3

    .line 168779
    :goto_3
    if-ge v3, v0, :cond_2

    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168780
    :cond_2
    if-eqz v6, :cond_0

    .line 168781
    iget-object v0, p0, LX/10M;->f:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 168782
    if-eqz v5, :cond_0

    .line 168783
    sget-object v0, LX/26p;->z:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    move-object v10, v0

    check-cast v10, LX/0Tn;

    .line 168784
    new-instance v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, p0, LX/10M;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v6}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    .line 168785
    iget-object v4, v6, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v4, v4

    .line 168786
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v4

    .line 168787
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v5, v7

    .line 168788
    invoke-virtual {v6}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    const-string v7, "password_account"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 168789
    :try_start_0
    iget-object v1, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/10M;->c:LX/0lC;

    invoke-virtual {v2, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v10, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 168790
    const-string v0, "account_type"

    const-string v1, "password_account"

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168791
    iget-object v0, p0, LX/10M;->h:LX/10O;

    const-string v1, "dbl_nux_save_account"

    invoke-virtual {v0, v1, v11}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 168792
    :catch_0
    move-exception v0

    .line 168793
    iget-object v1, p0, LX/10M;->d:LX/03V;

    const-string v2, "FB4ADBLStoreManager"

    const-string v3, "Error encountered in saving the Password account credentials in FbSharedPreferences"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    move v0, v8

    .line 168794
    goto :goto_2

    :cond_4
    move v0, v2

    .line 168795
    goto :goto_3

    .line 168796
    :cond_5
    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->c()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->j()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v4

    .line 168797
    goto/16 :goto_1

    .line 168798
    :cond_6
    iget-object v0, p0, LX/10M;->b:LX/10N;

    invoke-virtual {v0}, LX/10N;->l()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v3, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v3, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    if-ne v0, v7, :cond_7

    move v0, v4

    .line 168799
    goto/16 :goto_1

    .line 168800
    :cond_7
    sget-object v0, LX/26p;->k:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 168801
    sget-object v3, LX/26p;->z:LX/0Tn;

    invoke-virtual {v3, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    .line 168802
    iget-object v5, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v0, v4

    .line 168803
    goto/16 :goto_1

    .line 168804
    :cond_8
    iget-object v3, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 168805
    iget-object v0, p0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v0, LX/26p;->y:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v3, v0, v7}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move v0, v4

    .line 168806
    goto/16 :goto_1

    .line 168807
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_1
.end method
