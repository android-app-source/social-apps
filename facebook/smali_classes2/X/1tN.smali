.class public LX/1tN;
.super LX/05I;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/1tN;


# instance fields
.field private final h:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336432
    new-instance v2, LX/04v;

    invoke-direct {v2, p1}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 336433
    sget-object v0, LX/01o;->a:LX/01o;

    move-object v3, v0

    .line 336434
    const-string v4, "MqttLite"

    sget-object v5, LX/05J;->FBNS:LX/05J;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/05I;-><init>(Landroid/content/Context;LX/04v;LX/01o;Ljava/lang/String;LX/05J;)V

    .line 336435
    iput-object p2, p0, LX/1tN;->h:LX/03V;

    .line 336436
    return-void
.end method

.method public static a(LX/0QB;)LX/1tN;
    .locals 5

    .prologue
    .line 336437
    sget-object v0, LX/1tN;->i:LX/1tN;

    if-nez v0, :cond_1

    .line 336438
    const-class v1, LX/1tN;

    monitor-enter v1

    .line 336439
    :try_start_0
    sget-object v0, LX/1tN;->i:LX/1tN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336440
    if-eqz v2, :cond_0

    .line 336441
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 336442
    new-instance p0, LX/1tN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/1tN;-><init>(Landroid/content/Context;LX/03V;)V

    .line 336443
    move-object v0, p0

    .line 336444
    sput-object v0, LX/1tN;->i:LX/1tN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336445
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336446
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336447
    :cond_1
    sget-object v0, LX/1tN;->i:LX/1tN;

    return-object v0

    .line 336448
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1tN;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 336450
    :try_start_0
    iget-object v0, p0, LX/05I;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LX/37V;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 336451
    :goto_0
    return-void

    .line 336452
    :catch_0
    move-exception v0

    .line 336453
    iget-object v1, p0, LX/1tN;->h:LX/03V;

    const-string v2, "FbnsNotificationDeliveryHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to start notification handler service. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 336454
    invoke-static {p0, p1}, LX/1tN;->b(LX/1tN;Landroid/content/Intent;)V

    .line 336455
    const/4 v0, 0x1

    return v0
.end method
