.class public LX/1ix;
.super LX/1iY;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/logging/annotations/IsZeroHttpEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299414
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299415
    iput-object p1, p0, LX/1ix;->a:LX/0Zb;

    .line 299416
    iput-object p2, p0, LX/1ix;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 299417
    iput-object p3, p0, LX/1ix;->c:LX/0Or;

    .line 299418
    iput-object p4, p0, LX/1ix;->d:LX/0Or;

    .line 299419
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 299420
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/1ix;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299421
    iget-object v0, p0, LX/1ix;->a:LX/0Zb;

    invoke-direct {p0}, LX/1ix;->f()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 299422
    :cond_0
    return-void
.end method

.method private f()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 14

    .prologue
    .line 299423
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "zero_http"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 299424
    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    .line 299425
    iget-object v2, p0, LX/1iY;->d:LX/1iW;

    move-object v2, v2

    .line 299426
    iget-object v3, p0, LX/1iY;->d:LX/1iW;

    move-object v3, v3

    .line 299427
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/1iW;->j:Z

    .line 299428
    iget-object v3, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v3, v3

    .line 299429
    const-string v4, "http_stack"

    .line 299430
    iget-object v5, v2, LX/1iW;->c:Ljava/lang/String;

    move-object v5, v5

    .line 299431
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299432
    const-string v4, "network_type"

    .line 299433
    iget-object v5, v2, LX/1iW;->g:Ljava/lang/String;

    move-object v5, v5

    .line 299434
    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299435
    const-string v4, "network_subtype"

    .line 299436
    iget-object v5, v2, LX/1iW;->h:Ljava/lang/String;

    move-object v5, v5

    .line 299437
    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299438
    invoke-interface {v0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v4

    .line 299439
    const-string v0, "uri"

    invoke-virtual {v1, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299440
    iget-object v0, v2, LX/1iW;->e:Ljava/lang/String;

    move-object v0, v0

    .line 299441
    if-eqz v0, :cond_0

    .line 299442
    const-string v0, "ip_address"

    .line 299443
    iget-object v5, v2, LX/1iW;->e:Ljava/lang/String;

    move-object v5, v5

    .line 299444
    invoke-virtual {v1, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299445
    :cond_0
    iget-object v0, p0, LX/1ix;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yh;

    .line 299446
    const-string v5, "hostname"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299447
    const-string v4, "body_bytes_written"

    invoke-virtual {v2}, LX/1iW;->i()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299448
    invoke-virtual {v0}, LX/0yh;->getRegistrationStatusKey()LX/0Tn;

    move-result-object v4

    invoke-virtual {v4}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/1ix;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0}, LX/0yh;->getRegistrationStatusKey()LX/0Tn;

    move-result-object v6

    const-string v7, "unknown"

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299449
    invoke-virtual {v0}, LX/0yh;->getCampaignIdKey()LX/0Tn;

    move-result-object v4

    invoke-virtual {v4}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/1ix;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0}, LX/0yh;->getCampaignIdKey()LX/0Tn;

    move-result-object v0

    const-string v6, ""

    invoke-interface {v5, v0, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299450
    if-eqz v3, :cond_1

    .line 299451
    const-string v0, "content_type"

    invoke-static {v3}, LX/1Gl;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299452
    const-string v0, "body_bytes_read"

    .line 299453
    iget-object v8, v2, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 299454
    iget-wide v12, v8, LX/1iX;->a:J

    move-wide v8, v12

    .line 299455
    iget-object v10, v2, LX/1iW;->responseBodyBytes:LX/1iX;

    .line 299456
    iget-wide v12, v10, LX/1iX;->a:J

    move-wide v10, v12

    .line 299457
    add-long/2addr v8, v10

    move-wide v4, v8

    .line 299458
    invoke-virtual {v1, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299459
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 299460
    if-eqz v0, :cond_1

    .line 299461
    const-string v2, "status_code"

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 299462
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 0
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299463
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 299464
    invoke-direct {p0}, LX/1ix;->e()V

    .line 299465
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 0

    .prologue
    .line 299466
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 299467
    invoke-direct {p0}, LX/1ix;->e()V

    .line 299468
    return-void
.end method
