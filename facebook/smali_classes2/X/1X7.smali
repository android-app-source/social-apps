.class public LX/1X7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 270533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270534
    return-void
.end method

.method private static a(Landroid/content/Context;F)I
    .locals 2

    .prologue
    .line 270532
    const/4 v0, 0x0

    invoke-static {p0, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)I
    .locals 1
    .param p0    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/14w;",
            ")I"
        }
    .end annotation

    .prologue
    .line 270531
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p0}, LX/14w;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;
    .locals 8
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1X9;",
            "LX/1V2;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "LX/1X9;"
        }
    .end annotation

    .prologue
    .line 270529
    if-eqz p2, :cond_0

    .line 270530
    :goto_0
    return-object p2

    :cond_0
    move-object v0, p3

    move-object v1, p1

    move v2, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, LX/1V2;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object p2

    goto :goto_0
.end method

.method private static a(ILandroid/graphics/drawable/Drawable;LX/1X9;ILandroid/content/Context;LX/1Ua;LX/1V8;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 270515
    if-gez p0, :cond_0

    .line 270516
    :goto_0
    return-object p1

    .line 270517
    :cond_0
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 270518
    invoke-virtual {p6, p5, p2, p3}, LX/1V8;->b(LX/1Ua;LX/1X9;I)LX/1Ub;

    move-result-object v0

    .line 270519
    iget-object v2, v0, LX/1Ub;->d:LX/1Uc;

    move-object v2, v2

    .line 270520
    invoke-interface {v2, p3}, LX/1Uc;->a(I)F

    move-result v3

    .line 270521
    invoke-static {p4, v3}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v2

    .line 270522
    iget v4, v0, LX/1Ub;->c:F

    move v4, v4

    .line 270523
    add-float/2addr v3, v4

    invoke-static {p4, v3}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v4

    .line 270524
    iget v3, v0, LX/1Ub;->a:F

    move v3, v3

    .line 270525
    invoke-static {p4, v3}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v3

    .line 270526
    iget v5, v0, LX/1Ub;->b:F

    move v0, v5

    .line 270527
    invoke-static {p4, v0}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v5

    move-object v0, p1

    .line 270528
    invoke-static/range {v0 .. v5}, LX/1X7;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;IIII)Landroid/graphics/drawable/LayerDrawable;

    move-result-object p1

    goto :goto_0
.end method

.method private static a(ILandroid/graphics/drawable/Drawable;Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 270511
    if-gez p0, :cond_0

    .line 270512
    :goto_0
    return-object p1

    .line 270513
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 270514
    iget v2, p3, Landroid/graphics/Rect;->left:I

    iget v3, p3, Landroid/graphics/Rect;->top:I

    iget v4, p3, Landroid/graphics/Rect;->right:I

    iget v5, p3, Landroid/graphics/Rect;->bottom:I

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/1X7;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;IIII)Landroid/graphics/drawable/LayerDrawable;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    .line 270490
    move-object/from16 v0, p9

    invoke-static {p0, p1, p4, p5, v0}, LX/1X7;->a(LX/1X9;ILX/1dp;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 270491
    invoke-static {p2, v1, p5, p6}, LX/1X7;->a(ILandroid/graphics/drawable/Drawable;Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move v1, p3

    move-object v3, p0

    move v4, p1

    move-object v5, p5

    move-object v6, p7

    move-object/from16 v7, p8

    .line 270492
    invoke-static/range {v1 .. v7}, LX/1X7;->a(ILandroid/graphics/drawable/Drawable;LX/1X9;ILandroid/content/Context;LX/1Ua;LX/1V8;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method private static a(LX/1X9;ILX/1dp;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 270510
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p2, v0, p0, p1, p4}, LX/1dp;->a(Landroid/content/res/Resources;LX/1X9;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;IIII)Landroid/graphics/drawable/LayerDrawable;
    .locals 6

    .prologue
    .line 270506
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 270507
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 270508
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 270509
    return-object v0
.end method

.method public static a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 270493
    invoke-virtual {p2, p3, p0, p1}, LX/1V8;->a(LX/1Ua;LX/1X9;I)LX/1Ub;

    move-result-object v0

    .line 270494
    iget-object v1, v0, LX/1Ub;->d:LX/1Uc;

    move-object v1, v1

    .line 270495
    invoke-interface {v1, p1}, LX/1Uc;->a(I)F

    move-result v1

    .line 270496
    invoke-static {p4, v1}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v2

    .line 270497
    iget v3, v0, LX/1Ub;->a:F

    move v3, v3

    .line 270498
    invoke-static {p4, v3}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v3

    .line 270499
    iget v4, v0, LX/1Ub;->c:F

    move v4, v4

    .line 270500
    add-float/2addr v1, v4

    invoke-static {p4, v1}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v1

    .line 270501
    iget v4, v0, LX/1Ub;->b:F

    move v0, v4

    .line 270502
    invoke-static {p4, v0}, LX/1X7;->a(Landroid/content/Context;F)I

    move-result v0

    invoke-virtual {p5, v2, v3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 270503
    iget v0, p5, Landroid/graphics/Rect;->top:I

    if-ltz v0, :cond_0

    iget v0, p5, Landroid/graphics/Rect;->bottom:I

    if-ltz v0, :cond_0

    iget v0, p5, Landroid/graphics/Rect;->left:I

    if-ltz v0, :cond_0

    iget v0, p5, Landroid/graphics/Rect;->right:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 270504
    return-void

    .line 270505
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
