.class public final enum LX/0hf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0hf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0hf;

.field public static final enum FEED:LX/0hf;

.field public static final enum NOTIFICATION:LX/0hf;

.field public static final enum ROW:LX/0hf;

.field public static final enum SHARESHEET:LX/0hf;

.field public static final enum THREAD:LX/0hf;

.field public static final enum UNKNOWN:LX/0hf;


# instance fields
.field public source:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118036
    new-instance v0, LX/0hf;

    const-string v1, "SHARESHEET"

    const-string v2, "sharesheet"

    invoke-direct {v0, v1, v4, v2}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->SHARESHEET:LX/0hf;

    .line 118037
    new-instance v0, LX/0hf;

    const-string v1, "FEED"

    const-string v2, ""

    invoke-direct {v0, v1, v5, v2}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->FEED:LX/0hf;

    .line 118038
    new-instance v0, LX/0hf;

    const-string v1, "THREAD"

    const-string v2, "thread"

    invoke-direct {v0, v1, v6, v2}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->THREAD:LX/0hf;

    .line 118039
    new-instance v0, LX/0hf;

    const-string v1, "ROW"

    const-string v2, "row"

    invoke-direct {v0, v1, v7, v2}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->ROW:LX/0hf;

    .line 118040
    new-instance v0, LX/0hf;

    const-string v1, "NOTIFICATION"

    const-string v2, "notification"

    invoke-direct {v0, v1, v8, v2}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->NOTIFICATION:LX/0hf;

    .line 118041
    new-instance v0, LX/0hf;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/0hf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0hf;->UNKNOWN:LX/0hf;

    .line 118042
    const/4 v0, 0x6

    new-array v0, v0, [LX/0hf;

    sget-object v1, LX/0hf;->SHARESHEET:LX/0hf;

    aput-object v1, v0, v4

    sget-object v1, LX/0hf;->FEED:LX/0hf;

    aput-object v1, v0, v5

    sget-object v1, LX/0hf;->THREAD:LX/0hf;

    aput-object v1, v0, v6

    sget-object v1, LX/0hf;->ROW:LX/0hf;

    aput-object v1, v0, v7

    sget-object v1, LX/0hf;->NOTIFICATION:LX/0hf;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0hf;->UNKNOWN:LX/0hf;

    aput-object v2, v0, v1

    sput-object v0, LX/0hf;->$VALUES:[LX/0hf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118043
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118044
    iput-object p3, p0, LX/0hf;->source:Ljava/lang/String;

    .line 118045
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0hf;
    .locals 1

    .prologue
    .line 118046
    const-class v0, LX/0hf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0hf;

    return-object v0
.end method

.method public static values()[LX/0hf;
    .locals 1

    .prologue
    .line 118047
    sget-object v0, LX/0hf;->$VALUES:[LX/0hf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0hf;

    return-object v0
.end method


# virtual methods
.method public final getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118048
    iget-object v0, p0, LX/0hf;->source:Ljava/lang/String;

    return-object v0
.end method
