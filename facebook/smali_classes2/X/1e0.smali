.class public LX/1e0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1e1;

.field public final b:LX/17W;

.field public final c:LX/0kL;

.field private final d:LX/1e2;

.field public final e:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1e1;LX/17W;LX/0kL;LX/1e2;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287259
    iput-object p1, p0, LX/1e0;->a:LX/1e1;

    .line 287260
    iput-object p2, p0, LX/1e0;->b:LX/17W;

    .line 287261
    iput-object p3, p0, LX/1e0;->c:LX/0kL;

    .line 287262
    iput-object p4, p0, LX/1e0;->d:LX/1e2;

    .line 287263
    new-instance v0, LX/1e3;

    invoke-direct {v0, p0}, LX/1e3;-><init>(LX/1e0;)V

    move-object v0, v0

    .line 287264
    iput-object v0, p0, LX/1e0;->e:LX/0TF;

    .line 287265
    return-void
.end method

.method public static a(LX/0QB;)LX/1e0;
    .locals 1

    .prologue
    .line 287257
    invoke-static {p0}, LX/1e0;->b(LX/0QB;)LX/1e0;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 287256
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1e0;
    .locals 5

    .prologue
    .line 287251
    new-instance v4, LX/1e0;

    .line 287252
    new-instance v2, LX/1e1;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v1}, LX/1e1;-><init>(LX/0tX;LX/1Ck;)V

    .line 287253
    move-object v0, v2

    .line 287254
    check-cast v0, LX/1e1;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0}, LX/1e2;->b(LX/0QB;)LX/1e2;

    move-result-object v3

    check-cast v3, LX/1e2;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1e0;-><init>(LX/1e1;LX/17W;LX/0kL;LX/1e2;)V

    .line 287255
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 287266
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0823e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 287267
    new-instance v1, LX/8yH;

    invoke-direct {v1, p0, p2}, LX/8yH;-><init>(LX/1e0;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 287268
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 287242
    iget-object v1, p0, LX/1e0;->d:LX/1e2;

    .line 287243
    iget-object v2, v1, LX/1e2;->a:LX/0Uh;

    const/16 v3, 0x5e4

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 287244
    if-nez v1, :cond_1

    .line 287245
    :cond_0
    :goto_0
    return v0

    .line 287246
    :cond_1
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 287247
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 287248
    invoke-static {p1}, LX/1e0;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287249
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 287250
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 287239
    const-string v0, "[FB] Add Social Recs List"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 287240
    new-instance v1, LX/8yI;

    invoke-direct {v1, p0, p2, p3}, LX/8yI;-><init>(LX/1e0;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 287241
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 287232
    iget-object v1, p0, LX/1e0;->d:LX/1e2;

    .line 287233
    iget-object v2, v1, LX/1e2;->a:LX/0Uh;

    const/16 v3, 0x28a

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 287234
    if-nez v1, :cond_1

    .line 287235
    :cond_0
    :goto_0
    return v0

    .line 287236
    :cond_1
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 287237
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 287238
    invoke-static {p1}, LX/1e0;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
