.class public final LX/0uq;
.super LX/0ur;
.source ""


# instance fields
.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 157170
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 157171
    instance-of v0, p0, LX/0uq;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 157172
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0uq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)",
            "LX/0uq;"
        }
    .end annotation

    .prologue
    .line 157168
    iput-object p1, p0, LX/0uq;->d:LX/0Px;

    .line 157169
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/0uq;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157166
    iput-object p1, p0, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 157167
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;)LX/0uq;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157164
    iput-object p1, p0, LX/0uq;->h:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 157165
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/0uq;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157158
    iput-object p1, p0, LX/0uq;->c:Ljava/lang/String;

    .line 157159
    return-object p0
.end method

.method public final a(Z)LX/0uq;
    .locals 0

    .prologue
    .line 157162
    iput-boolean p1, p0, LX/0uq;->f:Z

    .line 157163
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 2

    .prologue
    .line 157160
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;-><init>(LX/0uq;)V

    .line 157161
    return-object v0
.end method
