.class public final enum LX/0yY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yY;

.field public static final enum ATTACHMENT_DOWNLOAD_INTERSTITIAL:LX/0yY;

.field public static final enum ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL:LX/0yY;

.field public static final enum ATTACHMENT_UPLOAD_INTERSTITIAL:LX/0yY;

.field public static final enum AUDIO_PLAY_INTERSTITIAL:LX/0yY;

.field public static final enum AUDIO_UPLOAD_INTERSTITIAL:LX/0yY;

.field public static final enum BUY_CONFIRM_INTERSTITIAL:LX/0yY;

.field public static final enum CARRIER_MANAGER:LX/0yY;

.field public static final enum CHECKIN_INTERSTITIAL:LX/0yY;

.field public static final enum DATA_SAVING_MODE:LX/0yY;

.field public static final enum DIALTONE_AUTOMATIC_MODE:LX/0yY;

.field public static final enum DIALTONE_AUTOMATIC_MODE_WITH_CONFIRMATION:LX/0yY;

.field public static final enum DIALTONE_FACEWEB:LX/0yY;

.field public static final enum DIALTONE_FEED_CAPPING:LX/0yY;

.field public static final enum DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

.field public static final enum DIALTONE_OPTIN:LX/0yY;

.field public static final enum DIALTONE_OPTOUT_REMINDER:LX/0yY;

.field public static final enum DIALTONE_PHOTO:LX/0yY;

.field public static final enum DIALTONE_PHOTOCAP_ERROR:LX/0yY;

.field public static final enum DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

.field public static final enum DIALTONE_PHOTO_CAPPING:LX/0yY;

.field public static final enum DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

.field public static final enum DIALTONE_SHOW_INTERSTITIAL_WITH_CAP:LX/0yY;

.field public static final enum DIALTONE_STICKY_MODE:LX/0yY;

.field public static final enum DIALTONE_SWITCHER_NUX:LX/0yY;

.field public static final enum DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

.field public static final enum DIALTONE_TOGGLE_BOOKMARK:LX/0yY;

.field public static final enum DIALTONE_TOGGLE_FB4A_SERVER_STICKY:LX/0yY;

.field public static final enum DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

.field public static final enum DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

.field public static final enum EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

.field public static final enum FB4A_INDICATOR:LX/0yY;

.field public static final enum FBS_CONTENT_NOTIFICATIONS:LX/0yY;

.field public static final enum FBS_CONTENT_SEARCH:LX/0yY;

.field public static final enum FBS_MANAGE_DATA_TAB:LX/0yY;

.field public static final enum FBS_OPEN_PLATFORM:LX/0yY;

.field public static final enum FBS_SYSTEM_NOTIFICATIONS:LX/0yY;

.field public static final enum FLEX_PLUS:LX/0yY;

.field public static final enum FREE_DATA_CAPPING:LX/0yY;

.field public static final enum FREE_DATA_NOTIFICATION:LX/0yY;

.field public static final enum FREE_MESSENGER_SETTING:LX/0yY;

.field public static final enum IMAGE_SEARCH_INTERSTITIAL:LX/0yY;

.field public static final enum INSTANT_ARTICLE_SETTING:LX/0yY;

.field public static final enum IORG_BACKGROUND_EXTERNAL_URL_PROMPT:LX/0yY;

.field public static final enum IORG_BACKGROUND_EXTERNAL_URL_STATUS:LX/0yY;

.field public static final enum IORG_EXTERNAL_URL:LX/0yY;

.field public static final enum IORG_EXTERNAL_URL_SAFE_MODE_PROMPT:LX/0yY;

.field public static final enum IORG_FB4A_URL:LX/0yY;

.field public static final enum LEAVING_APP_INTERSTITIAL:LX/0yY;

.field public static final enum LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

.field public static final enum MESSAGE_CAPPING:LX/0yY;

.field public static final enum MESSAGE_CAPPING_SETTING:LX/0yY;

.field public static final enum MESSENGER_ZERO_BALANCE_DETECTION:LX/0yY;

.field public static final enum NATIVE_OPTIN_INTERSTITIAL:LX/0yY;

.field public static final enum NATIVE_UPSELL_INTERSTITIAL:LX/0yY;

.field public static final enum OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

.field public static final enum OPTIN_GROUP_INTERSTITIAL:LX/0yY;

.field public static final enum SEND_MEDIA_FILE_INTERSTITIAL:LX/0yY;

.field public static final enum SEND_MESSAGE_INTERSTITIAL:LX/0yY;

.field public static final enum SEND_STICKER_INTERSTITIAL:LX/0yY;

.field public static final enum SMS_THREAD_INTERSTITIAL:LX/0yY;

.field public static final enum SWITCH_TO_DIALTONE:LX/0yY;

.field public static final enum TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

.field public static final enum UNKNOWN:LX/0yY;

.field public static final enum UPSELL_DONT_WARN_AGAIN:LX/0yY;

.field public static final enum UPSELL_DONT_WARN_AGAIN_CHECKBOX_CHECKED:LX/0yY;

.field public static final enum UPSELL_USE_DATA_OR_STAY_FREE_SCREEN:LX/0yY;

.field public static final enum VIDEOHOME_FREE_VIDEOS:LX/0yY;

.field public static final enum VIDEO_PLAY_INTERSTITIAL:LX/0yY;

.field public static final enum VIDEO_SAVE_INTERSTITIAL:LX/0yY;

.field public static final enum VIDEO_SCREENCAP:LX/0yY;

.field public static final enum VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

.field public static final enum VIEW_MAP_INTERSTITIAL:LX/0yY;

.field public static final enum VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

.field public static final enum VOIP_CALL_INTERSTITIAL:LX/0yY;

.field public static final enum VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

.field public static final enum VPN_DATA_CONTROL:LX/0yY;

.field public static final enum ZERO_BALANCE_DETECTION:LX/0yY;

.field public static final enum ZERO_BALANCE_WEBVIEW:LX/0yY;

.field public static final enum ZERO_INDICATOR:LX/0yY;

.field public static final enum ZERO_RATED_INTERSTITIAL:LX/0yY;

.field private static mIorgZeroFeatureKeys:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public static final sDialtoneFeatureKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final prefString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 165054
    new-instance v0, LX/0yY;

    const-string v1, "UNKNOWN"

    const-string v2, ""

    invoke-direct {v0, v1, v4, v2}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->UNKNOWN:LX/0yY;

    .line 165055
    new-instance v0, LX/0yY;

    const-string v1, "ZERO_INDICATOR"

    const-string v2, "promo_banner"

    invoke-direct {v0, v1, v5, v2}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    .line 165056
    new-instance v0, LX/0yY;

    const-string v1, "FB4A_INDICATOR"

    const-string v2, "zero_state"

    invoke-direct {v0, v1, v6, v2}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    .line 165057
    new-instance v0, LX/0yY;

    const-string v1, "EXTERNAL_URLS_INTERSTITIAL"

    const-string v2, "url_interstitial"

    invoke-direct {v0, v1, v7, v2}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    .line 165058
    new-instance v0, LX/0yY;

    const-string v1, "IMAGE_SEARCH_INTERSTITIAL"

    const-string v2, "image_search_interstitial"

    invoke-direct {v0, v1, v8, v2}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IMAGE_SEARCH_INTERSTITIAL:LX/0yY;

    .line 165059
    new-instance v0, LX/0yY;

    const-string v1, "VIEW_TIMELINE_INTERSTITIAL"

    const/4 v2, 0x5

    const-string v3, "timeline_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

    .line 165060
    new-instance v0, LX/0yY;

    const-string v1, "VIEW_MAP_INTERSTITIAL"

    const/4 v2, 0x6

    const-string v3, "map_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    .line 165061
    new-instance v0, LX/0yY;

    const-string v1, "VOIP_CALL_INTERSTITIAL"

    const/4 v2, 0x7

    const-string v3, "voip_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    .line 165062
    new-instance v0, LX/0yY;

    const-string v1, "VOIP_INCOMING_CALL_INTERSTITIAL"

    const/16 v2, 0x8

    const-string v3, "voip_incoming_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

    .line 165063
    new-instance v0, LX/0yY;

    const-string v1, "LOCATION_SERVICES_INTERSTITIAL"

    const/16 v2, 0x9

    const-string v3, "location_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    .line 165064
    new-instance v0, LX/0yY;

    const-string v1, "NATIVE_OPTIN_INTERSTITIAL"

    const/16 v2, 0xa

    const-string v3, "native_optin_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->NATIVE_OPTIN_INTERSTITIAL:LX/0yY;

    .line 165065
    new-instance v0, LX/0yY;

    const-string v1, "NATIVE_UPSELL_INTERSTITIAL"

    const/16 v2, 0xb

    const-string v3, "native_url_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->NATIVE_UPSELL_INTERSTITIAL:LX/0yY;

    .line 165066
    new-instance v0, LX/0yY;

    const-string v1, "LEAVING_APP_INTERSTITIAL"

    const/16 v2, 0xc

    const-string v3, "dialog_when_leaving_app"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->LEAVING_APP_INTERSTITIAL:LX/0yY;

    .line 165067
    new-instance v0, LX/0yY;

    const-string v1, "CHECKIN_INTERSTITIAL"

    const/16 v2, 0xd

    const-string v3, "checkin_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->CHECKIN_INTERSTITIAL:LX/0yY;

    .line 165068
    new-instance v0, LX/0yY;

    const-string v1, "BUY_CONFIRM_INTERSTITIAL"

    const/16 v2, 0xe

    const-string v3, "buy_confirm_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->BUY_CONFIRM_INTERSTITIAL:LX/0yY;

    .line 165069
    new-instance v0, LX/0yY;

    const-string v1, "OPTIN_GROUP_INTERSTITIAL"

    const/16 v2, 0xf

    const-string v3, "optin_group_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->OPTIN_GROUP_INTERSTITIAL:LX/0yY;

    .line 165070
    new-instance v0, LX/0yY;

    const-string v1, "UPSELL_USE_DATA_OR_STAY_FREE_SCREEN"

    const/16 v2, 0x10

    const-string v3, "upsell_use_data_or_stay_free_screen"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->UPSELL_USE_DATA_OR_STAY_FREE_SCREEN:LX/0yY;

    .line 165071
    new-instance v0, LX/0yY;

    const-string v1, "VIDEO_UPLOAD_INTERSTITIAL"

    const/16 v2, 0x11

    const-string v3, "upload_video_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    .line 165072
    new-instance v0, LX/0yY;

    const-string v1, "VIDEO_PLAY_INTERSTITIAL"

    const/16 v2, 0x12

    const-string v3, "play_video_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    .line 165073
    new-instance v0, LX/0yY;

    const-string v1, "VIDEO_SAVE_INTERSTITIAL"

    const/16 v2, 0x13

    const-string v3, "save_video_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIDEO_SAVE_INTERSTITIAL:LX/0yY;

    .line 165074
    new-instance v0, LX/0yY;

    const-string v1, "AUDIO_UPLOAD_INTERSTITIAL"

    const/16 v2, 0x14

    const-string v3, "upload_audio_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->AUDIO_UPLOAD_INTERSTITIAL:LX/0yY;

    .line 165075
    new-instance v0, LX/0yY;

    const-string v1, "AUDIO_PLAY_INTERSTITIAL"

    const/16 v2, 0x15

    const-string v3, "play_audio_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->AUDIO_PLAY_INTERSTITIAL:LX/0yY;

    .line 165076
    new-instance v0, LX/0yY;

    const-string v1, "ATTACHMENT_UPLOAD_INTERSTITIAL"

    const/16 v2, 0x16

    const-string v3, "upload_attachment_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ATTACHMENT_UPLOAD_INTERSTITIAL:LX/0yY;

    .line 165077
    new-instance v0, LX/0yY;

    const-string v1, "ATTACHMENT_DOWNLOAD_INTERSTITIAL"

    const/16 v2, 0x17

    const-string v3, "download_attachment_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ATTACHMENT_DOWNLOAD_INTERSTITIAL:LX/0yY;

    .line 165078
    new-instance v0, LX/0yY;

    const-string v1, "ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL"

    const/16 v2, 0x18

    const-string v3, "download_mms_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL:LX/0yY;

    .line 165079
    new-instance v0, LX/0yY;

    const-string v1, "SEND_STICKER_INTERSTITIAL"

    const/16 v2, 0x19

    const-string v3, "send_sticker_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->SEND_STICKER_INTERSTITIAL:LX/0yY;

    .line 165080
    new-instance v0, LX/0yY;

    const-string v1, "SEND_MEDIA_FILE_INTERSTITIAL"

    const/16 v2, 0x1a

    const-string v3, "send_media_file_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->SEND_MEDIA_FILE_INTERSTITIAL:LX/0yY;

    .line 165081
    new-instance v0, LX/0yY;

    const-string v1, "SEND_MESSAGE_INTERSTITIAL"

    const/16 v2, 0x1b

    const-string v3, "send_message_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->SEND_MESSAGE_INTERSTITIAL:LX/0yY;

    .line 165082
    new-instance v0, LX/0yY;

    const-string v1, "CARRIER_MANAGER"

    const/16 v2, 0x1c

    const-string v3, "carrier_manager"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->CARRIER_MANAGER:LX/0yY;

    .line 165083
    new-instance v0, LX/0yY;

    const-string v1, "ZERO_RATED_INTERSTITIAL"

    const/16 v2, 0x1d

    const-string v3, "zero_rated_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ZERO_RATED_INTERSTITIAL:LX/0yY;

    .line 165084
    new-instance v0, LX/0yY;

    const-string v1, "SMS_THREAD_INTERSTITIAL"

    const/16 v2, 0x1e

    const-string v3, "sms_thread_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    .line 165085
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_OPTIN"

    const/16 v2, 0x1f

    const-string v3, "dialtone_optin"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_OPTIN:LX/0yY;

    .line 165086
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_PHOTO"

    const/16 v2, 0x20

    const-string v3, "photo_dialtone"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    .line 165087
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_FACEWEB"

    const/16 v2, 0x21

    const-string v3, "dialtone_faceweb_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    .line 165088
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_PHOTO_CAPPING"

    const/16 v2, 0x22

    const-string v3, "dialtone_photo_capping"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    .line 165089
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_FEED_CAPPING"

    const/16 v2, 0x23

    const-string v3, "dialtone_feed_capping"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_FEED_CAPPING:LX/0yY;

    .line 165090
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_AUTOMATIC_MODE"

    const/16 v2, 0x24

    const-string v3, "automatic_mode"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_AUTOMATIC_MODE:LX/0yY;

    .line 165091
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_AUTOMATIC_MODE_WITH_CONFIRMATION"

    const/16 v2, 0x25

    const-string v3, "automatic_mode_with_confirmation"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_AUTOMATIC_MODE_WITH_CONFIRMATION:LX/0yY;

    .line 165092
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_MANUAL_SWITCHER_MODE"

    const/16 v2, 0x26

    const-string v3, "manual_switcher_mode"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    .line 165093
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_STICKY_MODE"

    const/16 v2, 0x27

    const-string v3, "dialtone_sticky_mode"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_STICKY_MODE:LX/0yY;

    .line 165094
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_TOGGLE_FB4A_SERVER_STICKY"

    const/16 v2, 0x28

    const-string v3, "dialtone_toggle_fb4a_server_sticky"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_TOGGLE_FB4A_SERVER_STICKY:LX/0yY;

    .line 165095
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_SWITCHER_NUX"

    const/16 v2, 0x29

    const-string v3, "dialtone_toggle_nux"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_SWITCHER_NUX:LX/0yY;

    .line 165096
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER"

    const/16 v2, 0x2a

    const-string v3, "dialtone_switcher_zero_balance_reminder"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

    .line 165097
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_OPTOUT_REMINDER"

    const/16 v2, 0x2b

    const-string v3, "dialtone_optout_reminder"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_OPTOUT_REMINDER:LX/0yY;

    .line 165098
    new-instance v0, LX/0yY;

    const-string v1, "SWITCH_TO_DIALTONE"

    const/16 v2, 0x2c

    const-string v3, "switch_to_dialtone_mode"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->SWITCH_TO_DIALTONE:LX/0yY;

    .line 165099
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_TOGGLE_BOOKMARK"

    const/16 v2, 0x2d

    const-string v3, "dialtone_toggle_bookmark"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_TOGGLE_BOOKMARK:LX/0yY;

    .line 165100
    new-instance v0, LX/0yY;

    const-string v1, "FLEX_PLUS"

    const/16 v2, 0x2e

    const-string v3, "flex_plus"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FLEX_PLUS:LX/0yY;

    .line 165101
    new-instance v0, LX/0yY;

    const-string v1, "VIDEO_SCREENCAP"

    const/16 v2, 0x2f

    const-string v3, "video_screencap"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIDEO_SCREENCAP:LX/0yY;

    .line 165102
    new-instance v0, LX/0yY;

    const-string v1, "DATA_SAVING_MODE"

    const/16 v2, 0x30

    const-string v3, "data_saving_mode"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DATA_SAVING_MODE:LX/0yY;

    .line 165103
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_PHOTO_INTERSTITIAL"

    const/16 v2, 0x31

    const-string v3, "dialtone_photo_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    .line 165104
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_VIDEO_INTERSTITIAL"

    const/16 v2, 0x32

    const-string v3, "dialtone_video_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    .line 165105
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_TOGGLE_INTERSTITIAL"

    const/16 v2, 0x33

    const-string v3, "dialtone_toggle_interstitial"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

    .line 165106
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_SHOW_INTERSTITIAL_WITH_CAP"

    const/16 v2, 0x34

    const-string v3, "dialtone_show_interstitial_with_cap"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_SHOW_INTERSTITIAL_WITH_CAP:LX/0yY;

    .line 165107
    new-instance v0, LX/0yY;

    const-string v1, "INSTANT_ARTICLE_SETTING"

    const/16 v2, 0x35

    const-string v3, "instant_article_setting"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->INSTANT_ARTICLE_SETTING:LX/0yY;

    .line 165108
    new-instance v0, LX/0yY;

    const-string v1, "ZERO_BALANCE_WEBVIEW"

    const/16 v2, 0x36

    const-string v3, "zero_balance_webview"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ZERO_BALANCE_WEBVIEW:LX/0yY;

    .line 165109
    new-instance v0, LX/0yY;

    const-string v1, "MESSAGE_CAPPING"

    const/16 v2, 0x37

    const-string v3, "message_capping"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->MESSAGE_CAPPING:LX/0yY;

    .line 165110
    new-instance v0, LX/0yY;

    const-string v1, "MESSAGE_CAPPING_SETTING"

    const/16 v2, 0x38

    const-string v3, "message_capping_setting"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->MESSAGE_CAPPING_SETTING:LX/0yY;

    .line 165111
    new-instance v0, LX/0yY;

    const-string v1, "FREE_DATA_CAPPING"

    const/16 v2, 0x39

    const-string v3, "free_data_capping"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FREE_DATA_CAPPING:LX/0yY;

    .line 165112
    new-instance v0, LX/0yY;

    const-string v1, "FREE_MESSENGER_SETTING"

    const/16 v2, 0x3a

    const-string v3, "free_messenger_setting"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FREE_MESSENGER_SETTING:LX/0yY;

    .line 165113
    new-instance v0, LX/0yY;

    const-string v1, "MESSENGER_ZERO_BALANCE_DETECTION"

    const/16 v2, 0x3b

    const-string v3, "messenger_zero_balance_detection"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->MESSENGER_ZERO_BALANCE_DETECTION:LX/0yY;

    .line 165114
    new-instance v0, LX/0yY;

    const-string v1, "UPSELL_DONT_WARN_AGAIN"

    const/16 v2, 0x3c

    const-string v3, "upsell_dont_warn_again"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->UPSELL_DONT_WARN_AGAIN:LX/0yY;

    .line 165115
    new-instance v0, LX/0yY;

    const-string v1, "UPSELL_DONT_WARN_AGAIN_CHECKBOX_CHECKED"

    const/16 v2, 0x3d

    const-string v3, "upsell_dont_warn_again_checkbox_checked"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->UPSELL_DONT_WARN_AGAIN_CHECKBOX_CHECKED:LX/0yY;

    .line 165116
    new-instance v0, LX/0yY;

    const-string v1, "FREE_DATA_NOTIFICATION"

    const/16 v2, 0x3e

    const-string v3, "free_data_persistent_notification"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FREE_DATA_NOTIFICATION:LX/0yY;

    .line 165117
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_PHOTOCAP_SPINNER"

    const/16 v2, 0x3f

    const-string v3, "dialtone_photocap_spinner"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    .line 165118
    new-instance v0, LX/0yY;

    const-string v1, "DIALTONE_PHOTOCAP_ERROR"

    const/16 v2, 0x40

    const-string v3, "dialtone_photocap_error"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->DIALTONE_PHOTOCAP_ERROR:LX/0yY;

    .line 165119
    new-instance v0, LX/0yY;

    const-string v1, "TIMEBASED_OFFLINE_VIDEO_DOWNLOAD"

    const/16 v2, 0x41

    const-string v3, "timebased_offline_video_download"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    .line 165120
    new-instance v0, LX/0yY;

    const-string v1, "OFF_PEAK_VIDEO_DOWNLOAD"

    const/16 v2, 0x42

    const-string v3, "off_peak_video_download"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    .line 165121
    new-instance v0, LX/0yY;

    const-string v1, "VPN_DATA_CONTROL"

    const/16 v2, 0x43

    const-string v3, "vpn_data_control"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VPN_DATA_CONTROL:LX/0yY;

    .line 165122
    new-instance v0, LX/0yY;

    const-string v1, "IORG_EXTERNAL_URL"

    const/16 v2, 0x44

    const-string v3, "iorg_external_url"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IORG_EXTERNAL_URL:LX/0yY;

    .line 165123
    new-instance v0, LX/0yY;

    const-string v1, "IORG_EXTERNAL_URL_SAFE_MODE_PROMPT"

    const/16 v2, 0x45

    const-string v3, "iorg_external_url_safe_mode_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IORG_EXTERNAL_URL_SAFE_MODE_PROMPT:LX/0yY;

    .line 165124
    new-instance v0, LX/0yY;

    const-string v1, "IORG_FB4A_URL"

    const/16 v2, 0x46

    const-string v3, "iorg_fb4a_url"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IORG_FB4A_URL:LX/0yY;

    .line 165125
    new-instance v0, LX/0yY;

    const-string v1, "IORG_BACKGROUND_EXTERNAL_URL_PROMPT"

    const/16 v2, 0x47

    const-string v3, "iorg_background_external_url_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_PROMPT:LX/0yY;

    .line 165126
    new-instance v0, LX/0yY;

    const-string v1, "IORG_BACKGROUND_EXTERNAL_URL_STATUS"

    const/16 v2, 0x48

    const-string v3, "iorg_background_external_url_status"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_STATUS:LX/0yY;

    .line 165127
    new-instance v0, LX/0yY;

    const-string v1, "FBS_MANAGE_DATA_TAB"

    const/16 v2, 0x49

    const-string v3, "fbs_app_manage_data_tab"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FBS_MANAGE_DATA_TAB:LX/0yY;

    .line 165128
    new-instance v0, LX/0yY;

    const-string v1, "FBS_OPEN_PLATFORM"

    const/16 v2, 0x4a

    const-string v3, "fbs_open_platform"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FBS_OPEN_PLATFORM:LX/0yY;

    .line 165129
    new-instance v0, LX/0yY;

    const-string v1, "FBS_CONTENT_SEARCH"

    const/16 v2, 0x4b

    const-string v3, "fbs_content_search"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FBS_CONTENT_SEARCH:LX/0yY;

    .line 165130
    new-instance v0, LX/0yY;

    const-string v1, "FBS_SYSTEM_NOTIFICATIONS"

    const/16 v2, 0x4c

    const-string v3, "fbs_system_notifications"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FBS_SYSTEM_NOTIFICATIONS:LX/0yY;

    .line 165131
    new-instance v0, LX/0yY;

    const-string v1, "FBS_CONTENT_NOTIFICATIONS"

    const/16 v2, 0x4d

    const-string v3, "fbs_content-notifications"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->FBS_CONTENT_NOTIFICATIONS:LX/0yY;

    .line 165132
    new-instance v0, LX/0yY;

    const-string v1, "ZERO_BALANCE_DETECTION"

    const/16 v2, 0x4e

    const-string v3, "zero_balance_detection"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->ZERO_BALANCE_DETECTION:LX/0yY;

    .line 165133
    new-instance v0, LX/0yY;

    const-string v1, "VIDEOHOME_FREE_VIDEOS"

    const/16 v2, 0x4f

    const-string v3, "videohome_free_videos"

    invoke-direct {v0, v1, v2, v3}, LX/0yY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0yY;->VIDEOHOME_FREE_VIDEOS:LX/0yY;

    .line 165134
    const/16 v0, 0x50

    new-array v0, v0, [LX/0yY;

    sget-object v1, LX/0yY;->UNKNOWN:LX/0yY;

    aput-object v1, v0, v4

    sget-object v1, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    aput-object v1, v0, v5

    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    aput-object v1, v0, v6

    sget-object v1, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    aput-object v1, v0, v7

    sget-object v1, LX/0yY;->IMAGE_SEARCH_INTERSTITIAL:LX/0yY;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0yY;->VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0yY;->VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0yY;->NATIVE_OPTIN_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0yY;->NATIVE_UPSELL_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0yY;->LEAVING_APP_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0yY;->CHECKIN_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0yY;->BUY_CONFIRM_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0yY;->OPTIN_GROUP_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0yY;->UPSELL_USE_DATA_OR_STAY_FREE_SCREEN:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0yY;->VIDEO_SAVE_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0yY;->AUDIO_UPLOAD_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0yY;->AUDIO_PLAY_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0yY;->ATTACHMENT_UPLOAD_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0yY;->ATTACHMENT_DOWNLOAD_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0yY;->ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0yY;->SEND_STICKER_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/0yY;->SEND_MEDIA_FILE_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/0yY;->SEND_MESSAGE_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/0yY;->CARRIER_MANAGER:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/0yY;->ZERO_RATED_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/0yY;->DIALTONE_OPTIN:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/0yY;->DIALTONE_FEED_CAPPING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/0yY;->DIALTONE_AUTOMATIC_MODE:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/0yY;->DIALTONE_AUTOMATIC_MODE_WITH_CONFIRMATION:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/0yY;->DIALTONE_STICKY_MODE:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/0yY;->DIALTONE_TOGGLE_FB4A_SERVER_STICKY:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/0yY;->DIALTONE_SWITCHER_NUX:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/0yY;->DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/0yY;->DIALTONE_OPTOUT_REMINDER:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/0yY;->SWITCH_TO_DIALTONE:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/0yY;->DIALTONE_TOGGLE_BOOKMARK:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/0yY;->FLEX_PLUS:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/0yY;->VIDEO_SCREENCAP:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/0yY;->DATA_SAVING_MODE:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/0yY;->DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/0yY;->DIALTONE_SHOW_INTERSTITIAL_WITH_CAP:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/0yY;->INSTANT_ARTICLE_SETTING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/0yY;->ZERO_BALANCE_WEBVIEW:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/0yY;->MESSAGE_CAPPING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/0yY;->MESSAGE_CAPPING_SETTING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/0yY;->FREE_DATA_CAPPING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/0yY;->FREE_MESSENGER_SETTING:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/0yY;->MESSENGER_ZERO_BALANCE_DETECTION:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/0yY;->UPSELL_DONT_WARN_AGAIN:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/0yY;->UPSELL_DONT_WARN_AGAIN_CHECKBOX_CHECKED:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/0yY;->FREE_DATA_NOTIFICATION:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/0yY;->DIALTONE_PHOTOCAP_ERROR:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/0yY;->VPN_DATA_CONTROL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/0yY;->IORG_EXTERNAL_URL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/0yY;->IORG_EXTERNAL_URL_SAFE_MODE_PROMPT:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/0yY;->IORG_FB4A_URL:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_PROMPT:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_STATUS:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/0yY;->FBS_MANAGE_DATA_TAB:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/0yY;->FBS_OPEN_PLATFORM:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/0yY;->FBS_CONTENT_SEARCH:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/0yY;->FBS_SYSTEM_NOTIFICATIONS:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/0yY;->FBS_CONTENT_NOTIFICATIONS:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/0yY;->ZERO_BALANCE_DETECTION:LX/0yY;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/0yY;->VIDEOHOME_FREE_VIDEOS:LX/0yY;

    aput-object v2, v0, v1

    sput-object v0, LX/0yY;->$VALUES:[LX/0yY;

    .line 165135
    sget-object v0, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    sget-object v1, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    sget-object v2, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    sget-object v3, LX/0yY;->DIALTONE_FEED_CAPPING:LX/0yY;

    sget-object v4, LX/0yY;->DIALTONE_AUTOMATIC_MODE:LX/0yY;

    sget-object v5, LX/0yY;->DIALTONE_AUTOMATIC_MODE_WITH_CONFIRMATION:LX/0yY;

    sget-object v6, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0yY;->sDialtoneFeatureKeys:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165051
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 165052
    iput-object p3, p0, LX/0yY;->prefString:Ljava/lang/String;

    .line 165053
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/0yY;
    .locals 5

    .prologue
    .line 165045
    if-eqz p0, :cond_1

    .line 165046
    invoke-static {}, LX/0yY;->values()[LX/0yY;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 165047
    iget-object v4, v0, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 165048
    :goto_1
    return-object v0

    .line 165049
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 165050
    :cond_1
    sget-object v0, LX/0yY;->UNKNOWN:LX/0yY;

    goto :goto_1
.end method

.method public static fromStrings(Ljava/lang/Iterable;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165041
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165042
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 165043
    invoke-static {v0}, LX/0yY;->fromString(Ljava/lang/String;)LX/0yY;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165044
    :cond_0
    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getFbsFixedFeatureKeys()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165136
    const-class v1, LX/0yY;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0yY;->mIorgZeroFeatureKeys:LX/0Px;

    if-nez v0, :cond_0

    .line 165137
    sget-object v0, LX/0yY;->IORG_EXTERNAL_URL:LX/0yY;

    sget-object v2, LX/0yY;->IORG_EXTERNAL_URL_SAFE_MODE_PROMPT:LX/0yY;

    sget-object v3, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_PROMPT:LX/0yY;

    sget-object v4, LX/0yY;->IORG_BACKGROUND_EXTERNAL_URL_STATUS:LX/0yY;

    sget-object v5, LX/0yY;->IORG_FB4A_URL:LX/0yY;

    invoke-static {v0, v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0yY;->mIorgZeroFeatureKeys:LX/0Px;

    .line 165138
    :cond_0
    sget-object v0, LX/0yY;->mIorgZeroFeatureKeys:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 165139
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static toStrings(Ljava/lang/Iterable;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/0yY;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165037
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165038
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yY;

    .line 165039
    iget-object v0, v0, LX/0yY;->prefString:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165040
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yY;
    .locals 1

    .prologue
    .line 165036
    const-class v0, LX/0yY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yY;

    return-object v0
.end method

.method public static values()[LX/0yY;
    .locals 1

    .prologue
    .line 165035
    sget-object v0, LX/0yY;->$VALUES:[LX/0yY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yY;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165034
    iget-object v0, p0, LX/0yY;->prefString:Ljava/lang/String;

    return-object v0
.end method
