.class public LX/0Ua;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0UW;

.field public final c:LX/0Ub;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ub",
            "<",
            "LX/0VM;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ub;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ub",
            "<",
            "LX/2LE;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/0Uf;

.field private final h:Ljava/io/File;

.field public i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66462
    const-class v0, LX/0Ua;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Ua;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0UW;LX/0Ub;LX/0Ub;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0UW;",
            "LX/0Ub",
            "<",
            "LX/0VM;",
            ">;",
            "LX/0Ub",
            "<",
            "LX/2LE;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66456
    iput-object p1, p0, LX/0Ua;->b:LX/0UW;

    .line 66457
    iput-object p2, p0, LX/0Ua;->c:LX/0Ub;

    .line 66458
    iput-object p3, p0, LX/0Ua;->d:LX/0Ub;

    .line 66459
    iput-object p4, p0, LX/0Ua;->h:Ljava/io/File;

    .line 66460
    new-instance v0, LX/0Uf;

    new-instance v1, Ljava/io/File;

    const-string v2, "file_lock"

    invoke-direct {v1, p4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/0Uf;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/0Ua;->g:LX/0Uf;

    .line 66461
    return-void
.end method

.method public constructor <init>(LX/0UW;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 66427
    new-instance v0, LX/0Ub;

    new-instance v1, LX/0Uc;

    invoke-direct {v1}, LX/0Uc;-><init>()V

    const-string v2, "gk_state"

    invoke-direct {v0, v1, p2, v2}, LX/0Ub;-><init>(LX/0Ud;Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, LX/0Ub;

    new-instance v2, LX/0Ue;

    invoke-direct {v2}, LX/0Ue;-><init>()V

    const-string v3, "gk_names"

    invoke-direct {v1, v2, p2, v3}, LX/0Ub;-><init>(LX/0Ud;Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1, p2}, LX/0Ua;-><init>(LX/0UW;LX/0Ub;LX/0Ub;Ljava/io/File;)V

    .line 66428
    return-void
.end method

.method public static a(LX/0Ui;IB)V
    .locals 1

    .prologue
    .line 66444
    and-int/lit8 v0, p2, 0x3

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 66445
    if-eqz v0, :cond_0

    .line 66446
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 66447
    invoke-virtual {p0, p1, v0}, LX/0Ui;->a(IZ)V

    .line 66448
    :goto_2
    and-int/lit8 v0, p2, 0xc

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 66449
    if-eqz v0, :cond_1

    .line 66450
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 66451
    invoke-virtual {p0, p1, v0}, LX/0Ui;->b(IZ)V

    .line 66452
    :goto_5
    return-void

    .line 66453
    :cond_0
    invoke-virtual {p0, p1}, LX/0Ui;->e(I)V

    goto :goto_2

    .line 66454
    :cond_1
    invoke-virtual {p0, p1}, LX/0Ui;->f(I)V

    goto :goto_5

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 66441
    if-eq p0, p1, :cond_0

    .line 66442
    sget-object v2, LX/0Ua;->a:Ljava/lang/String;

    const-string v3, "The number of gatekeepers in files doesn\'t match: %s and %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66443
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized c(LX/0Ua;)Z
    .locals 2

    .prologue
    .line 66463
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Ua;->e(LX/0Ua;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ua;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 66437
    iget-object v2, p0, LX/0Ua;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66438
    iget-object v2, p0, LX/0Ua;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66439
    sget-object v2, LX/0Ua;->a:Ljava/lang/String;

    const-string v3, "Cannot create working directory: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/0Ua;->h:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66440
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/0Ua;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ua;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 66434
    iget-object v0, p0, LX/0Ua;->b:LX/0UW;

    invoke-interface {v0}, LX/0UW;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0Ua;->f:Ljava/lang/String;

    .line 66435
    :cond_0
    iget-object v0, p0, LX/0Ua;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 66436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/0Ua;LX/0Ui;)V
    .locals 1

    .prologue
    .line 66429
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Ua;->c(LX/0Ua;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66430
    invoke-virtual {p0, p1}, LX/0Ua;->b(LX/0Ui;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66431
    :cond_0
    monitor-exit p0

    return-void

    .line 66432
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0Ui;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 66382
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/0Ua;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 66383
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 66384
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, LX/0Ua;->e:Z

    .line 66385
    invoke-direct {p0}, LX/0Ua;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66386
    iget-object v1, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v1}, LX/0Uf;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 66387
    :try_start_2
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66388
    iget-object v0, p0, LX/0Ua;->c:LX/0Ub;

    invoke-virtual {v0}, LX/0Ub;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0VM;

    .line 66389
    if-nez v0, :cond_2

    move v0, v2

    .line 66390
    :goto_1
    move v0, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66391
    :try_start_3
    iget-object v1, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v1}, LX/0Uf;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 66392
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66393
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v1, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v1}, LX/0Uf;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 66394
    :cond_2
    invoke-static {p0}, LX/0Ua;->e(LX/0Ua;)Ljava/lang/String;

    move-result-object v1

    .line 66395
    iget-object v4, v0, LX/0VM;->a:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    move v1, v4

    .line 66396
    if-nez v1, :cond_3

    move v1, v3

    .line 66397
    :goto_2
    if-eqz v1, :cond_9

    .line 66398
    iget-object v1, p0, LX/0Ua;->d:LX/0Ub;

    invoke-virtual {v1}, LX/0Ub;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2LE;

    .line 66399
    if-nez v1, :cond_4

    move v0, v2

    .line 66400
    goto :goto_1

    :cond_3
    move v1, v2

    .line 66401
    goto :goto_2

    .line 66402
    :cond_4
    iget-object v4, v0, LX/0VM;->a:Ljava/lang/String;

    iget-object v5, v1, LX/2LE;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 66403
    sget-object v4, LX/0Ua;->a:Ljava/lang/String;

    const-string v5, "The hash of gatekeeper names in files doesn\'t match: %s and %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v0, v0, LX/0VM;->a:Ljava/lang/String;

    aput-object v0, v6, v2

    iget-object v0, v1, LX/2LE;->a:Ljava/lang/String;

    aput-object v0, v6, v3

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 66404
    goto :goto_1

    .line 66405
    :cond_5
    iget-object v4, v0, LX/0VM;->b:[B

    array-length v4, v4

    iget-object v5, v1, LX/2LE;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v4, v5}, LX/0Ua;->a(II)Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v2

    .line 66406
    goto :goto_1

    .line 66407
    :cond_6
    iget-object v1, v1, LX/2LE;->b:Ljava/util/ArrayList;

    iget-object v2, v0, LX/0VM;->b:[B

    .line 66408
    new-instance v6, LX/4CS;

    iget-object v4, p0, LX/0Ua;->b:LX/0UW;

    invoke-direct {v6, v4}, LX/4CS;-><init>(LX/0UW;)V

    .line 66409
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 66410
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v7, :cond_8

    .line 66411
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 66412
    invoke-virtual {v6, v4}, LX/4CS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 66413
    if-eqz v4, :cond_7

    .line 66414
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-byte v8, v2, v5

    invoke-static {p1, v4, v8}, LX/0Ua;->a(LX/0Ui;IB)V

    .line 66415
    :cond_7
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 66416
    :cond_8
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    move-object v4, v4

    .line 66417
    new-instance v5, Lcom/facebook/gk/store/GatekeeperRepository$1;

    invoke-direct {v5, p0, p1}, Lcom/facebook/gk/store/GatekeeperRepository$1;-><init>(LX/0Ua;LX/0Ui;)V

    const v6, -0x46d18ff4

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 66418
    :goto_4
    iget-object v0, v0, LX/0VM;->a:Ljava/lang/String;

    iput-object v0, p0, LX/0Ua;->i:Ljava/lang/String;

    move v0, v3

    .line 66419
    goto/16 :goto_1

    .line 66420
    :cond_9
    iget-object v1, v0, LX/0VM;->b:[B

    array-length v1, v1

    iget-object v4, p0, LX/0Ua;->b:LX/0UW;

    invoke-interface {v4}, LX/0UW;->a()I

    move-result v4

    invoke-static {v1, v4}, LX/0Ua;->a(II)Z

    move-result v1

    if-nez v1, :cond_a

    move v0, v2

    .line 66421
    goto/16 :goto_1

    .line 66422
    :cond_a
    iget-object v1, v0, LX/0VM;->b:[B

    .line 66423
    const/4 v2, 0x0

    :goto_5
    iget-object v4, p0, LX/0Ua;->b:LX/0UW;

    invoke-interface {v4}, LX/0UW;->a()I

    move-result v4

    if-ge v2, v4, :cond_b

    .line 66424
    aget-byte v4, v1, v2

    invoke-static {p1, v2, v4}, LX/0Ua;->a(LX/0Ui;IB)V

    .line 66425
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 66426
    :cond_b
    goto :goto_4
.end method

.method public final declared-synchronized b(LX/0Ui;)V
    .locals 9

    .prologue
    .line 66355
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0Ua;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 66356
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 66357
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/0Ua;->e(LX/0Ua;)Ljava/lang/String;

    .line 66358
    iget-object v0, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v0}, LX/0Uf;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 66359
    :try_start_2
    iget-boolean v0, p0, LX/0Ua;->e:Z

    if-nez v0, :cond_2

    .line 66360
    iget-object v0, p0, LX/0Ua;->c:LX/0Ub;

    invoke-virtual {v0}, LX/0Ub;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0VM;

    .line 66361
    if-eqz v0, :cond_2

    .line 66362
    iget-object v0, v0, LX/0VM;->a:Ljava/lang/String;

    iput-object v0, p0, LX/0Ua;->i:Ljava/lang/String;

    .line 66363
    :cond_2
    invoke-static {p0}, LX/0Ua;->c(LX/0Ua;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 66364
    new-instance v0, LX/2LE;

    iget-object v1, p0, LX/0Ua;->b:LX/0UW;

    invoke-interface {v1}, LX/0UW;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Ua;->b:LX/0UW;

    invoke-interface {v2}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2LE;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 66365
    iget-object v1, p0, LX/0Ua;->d:LX/0Ub;

    invoke-virtual {v1, v0}, LX/0Ub;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66366
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v0}, LX/0Uf;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 66367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66368
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v1, p0, LX/0Ua;->g:LX/0Uf;

    invoke-virtual {v1}, LX/0Uf;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 66369
    :cond_3
    invoke-static {p0}, LX/0Ua;->e(LX/0Ua;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0Ua;->i:Ljava/lang/String;

    .line 66370
    :cond_4
    new-instance v0, LX/0VM;

    invoke-static {p0}, LX/0Ua;->e(LX/0Ua;)Ljava/lang/String;

    move-result-object v1

    .line 66371
    iget-object v2, p1, LX/0Ui;->a:[LX/03R;

    array-length v2, v2

    move v3, v2

    .line 66372
    new-array v4, v3, [B

    .line 66373
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_5

    .line 66374
    invoke-virtual {p1, v2}, LX/0Ui;->b(I)LX/03R;

    move-result-object v5

    invoke-virtual {p1, v2}, LX/0Ui;->c(I)LX/03R;

    move-result-object v6

    .line 66375
    invoke-static {v5}, LX/2LF;->a(LX/03R;)B

    move-result v7

    invoke-static {v6}, LX/2LF;->b(LX/03R;)B

    move-result v8

    or-int/2addr v7, v8

    int-to-byte v7, v7

    move v5, v7

    .line 66376
    aput-byte v5, v4, v2

    .line 66377
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 66378
    :cond_5
    move-object v2, v4

    .line 66379
    invoke-direct {v0, v1, v2}, LX/0VM;-><init>(Ljava/lang/String;[B)V

    .line 66380
    iget-object v1, p0, LX/0Ua;->c:LX/0Ub;

    invoke-virtual {v1, v0}, LX/0Ub;->a(Ljava/lang/Object;)Z

    .line 66381
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Ua;->e:Z

    goto :goto_1
.end method
