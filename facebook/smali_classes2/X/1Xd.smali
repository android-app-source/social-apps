.class public LX/1Xd;
.super LX/15v;
.source ""


# instance fields
.field public b:LX/0lD;

.field public c:LX/1Xf;

.field public d:LX/15z;

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(LX/0lF;)V
    .locals 1

    .prologue
    .line 271465
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1Xd;-><init>(LX/0lF;LX/0lD;)V

    return-void
.end method

.method public constructor <init>(LX/0lF;LX/0lD;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271446
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/15v;-><init>(I)V

    .line 271447
    iput-object p2, p0, LX/1Xd;->b:LX/0lD;

    .line 271448
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271449
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    iput-object v0, p0, LX/1Xd;->d:LX/15z;

    .line 271450
    new-instance v0, LX/1Xi;

    invoke-direct {v0, p1, v1}, LX/1Xi;-><init>(LX/0lF;LX/1Xf;)V

    iput-object v0, p0, LX/1Xd;->c:LX/1Xf;

    .line 271451
    :goto_0
    return-void

    .line 271452
    :cond_0
    invoke-virtual {p1}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271453
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    iput-object v0, p0, LX/1Xd;->d:LX/15z;

    .line 271454
    new-instance v0, LX/1Xe;

    invoke-direct {v0, p1, v1}, LX/1Xe;-><init>(LX/0lF;LX/1Xf;)V

    iput-object v0, p0, LX/1Xd;->c:LX/1Xf;

    goto :goto_0

    .line 271455
    :cond_1
    new-instance v0, LX/4rI;

    invoke-direct {v0, p1, v1}, LX/4rI;-><init>(LX/0lF;LX/1Xf;)V

    iput-object v0, p0, LX/1Xd;->c:LX/1Xf;

    goto :goto_0
.end method

.method private K()LX/0lF;
    .locals 1

    .prologue
    .line 271466
    iget-boolean v0, p0, LX/1Xd;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    if-nez v0, :cond_1

    .line 271467
    :cond_0
    const/4 v0, 0x0

    .line 271468
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->l()LX/0lF;

    move-result-object v0

    goto :goto_0
.end method

.method private L()LX/0lF;
    .locals 3

    .prologue
    .line 271469
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v0

    .line 271470
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->m()Z

    move-result v1

    if-nez v1, :cond_2

    .line 271471
    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 271472
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current token ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not numeric, can not use numeric value accessors"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 271473
    :cond_1
    invoke-virtual {v0}, LX/0lF;->a()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 271474
    :cond_2
    return-object v0
.end method


# virtual methods
.method public final A()F
    .locals 2

    .prologue
    .line 271475
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->y()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final B()D
    .locals 2

    .prologue
    .line 271476
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->y()D

    move-result-wide v0

    return-wide v0
.end method

.method public final C()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 271477
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->z()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public final D()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 271480
    iget-boolean v0, p0, LX/1Xd;->f:Z

    if-nez v0, :cond_1

    .line 271481
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v0

    .line 271482
    if-eqz v0, :cond_1

    .line 271483
    invoke-virtual {v0}, LX/0lF;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271484
    check-cast v0, LX/4rJ;

    .line 271485
    iget-object v1, v0, LX/4rJ;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 271486
    :goto_0
    return-object v0

    .line 271487
    :cond_0
    invoke-virtual {v0}, LX/0lF;->r()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 271488
    check-cast v0, LX/4rH;

    invoke-virtual {v0}, LX/0lF;->t()[B

    move-result-object v0

    goto :goto_0

    .line 271489
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final P()V
    .locals 0

    .prologue
    .line 271478
    invoke-static {}, LX/4pl;->b()V

    .line 271479
    return-void
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 271536
    iget-object v0, p0, LX/1Xd;->b:LX/0lD;

    return-object v0
.end method

.method public final a(LX/0lD;)V
    .locals 0

    .prologue
    .line 271534
    iput-object p1, p0, LX/1Xd;->b:LX/0lD;

    .line 271535
    return-void
.end method

.method public final a(LX/0ln;)[B
    .locals 2

    .prologue
    .line 271523
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v0

    .line 271524
    if-eqz v0, :cond_1

    .line 271525
    invoke-virtual {v0}, LX/0lF;->t()[B

    move-result-object v1

    .line 271526
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 271527
    :goto_0
    return-object v0

    .line 271528
    :cond_0
    invoke-virtual {v0}, LX/0lF;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 271529
    check-cast v0, LX/4rJ;

    .line 271530
    iget-object v1, v0, LX/4rJ;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 271531
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    .line 271532
    check-cast v0, [B

    check-cast v0, [B

    goto :goto_0

    .line 271533
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/15z;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 271495
    iget-object v1, p0, LX/1Xd;->d:LX/15z;

    if-eqz v1, :cond_0

    .line 271496
    iget-object v1, p0, LX/1Xd;->d:LX/15z;

    iput-object v1, p0, LX/1Xd;->K:LX/15z;

    .line 271497
    iput-object v0, p0, LX/1Xd;->d:LX/15z;

    .line 271498
    iget-object v0, p0, LX/15v;->K:LX/15z;

    .line 271499
    :goto_0
    return-object v0

    .line 271500
    :cond_0
    iget-boolean v1, p0, LX/1Xd;->e:Z

    if-eqz v1, :cond_5

    .line 271501
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Xd;->e:Z

    .line 271502
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->m()Z

    move-result v0

    if-nez v0, :cond_2

    .line 271503
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    :goto_1
    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    .line 271504
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto :goto_0

    .line 271505
    :cond_1
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    goto :goto_1

    .line 271506
    :cond_2
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->n()LX/1Xf;

    move-result-object v0

    iput-object v0, p0, LX/1Xd;->c:LX/1Xf;

    .line 271507
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->j()LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    .line 271508
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_4

    .line 271509
    :cond_3
    iput-boolean v2, p0, LX/1Xd;->e:Z

    .line 271510
    :cond_4
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto :goto_0

    .line 271511
    :cond_5
    iget-object v1, p0, LX/1Xd;->c:LX/1Xf;

    if-nez v1, :cond_6

    .line 271512
    iput-boolean v2, p0, LX/1Xd;->f:Z

    goto :goto_0

    .line 271513
    :cond_6
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->j()LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    .line 271514
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_9

    .line 271515
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_8

    .line 271516
    :cond_7
    iput-boolean v2, p0, LX/1Xd;->e:Z

    .line 271517
    :cond_8
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto :goto_0

    .line 271518
    :cond_9
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->k()LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    .line 271519
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    .line 271520
    iget-object v1, v0, LX/1Xf;->c:LX/1Xf;

    move-object v0, v1

    .line 271521
    iput-object v0, p0, LX/1Xd;->c:LX/1Xf;

    .line 271522
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271490
    iget-boolean v0, p0, LX/1Xd;->f:Z

    if-nez v0, :cond_0

    .line 271491
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Xd;->f:Z

    .line 271492
    iput-object v1, p0, LX/1Xd;->c:LX/1Xf;

    .line 271493
    iput-object v1, p0, LX/1Xd;->K:LX/15z;

    .line 271494
    :cond_0
    return-void
.end method

.method public final f()LX/15w;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 271457
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 271458
    iput-boolean v2, p0, LX/1Xd;->e:Z

    .line 271459
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    .line 271460
    :cond_0
    :goto_0
    return-object p0

    .line 271461
    :cond_1
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_0

    .line 271462
    iput-boolean v2, p0, LX/1Xd;->e:Z

    .line 271463
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    iput-object v0, p0, LX/1Xd;->K:LX/15z;

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271464
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()LX/12V;
    .locals 1

    .prologue
    .line 271423
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    return-object v0
.end method

.method public final k()LX/28G;
    .locals 1

    .prologue
    .line 271424
    sget-object v0, LX/28G;->a:LX/28G;

    return-object v0
.end method

.method public final l()LX/28G;
    .locals 1

    .prologue
    .line 271425
    sget-object v0, LX/28G;->a:LX/28G;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 271426
    iget-boolean v1, p0, LX/1Xd;->f:Z

    if-eqz v1, :cond_1

    .line 271427
    :cond_0
    :goto_0
    return-object v0

    .line 271428
    :cond_1
    sget-object v1, LX/1Xk;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 271429
    :cond_2
    iget-object v1, p0, LX/15v;->K:LX/15z;

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 271430
    :pswitch_0
    iget-object v0, p0, LX/1Xd;->c:LX/1Xf;

    invoke-virtual {v0}, LX/1Xf;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 271431
    :pswitch_1
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 271432
    :pswitch_2
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 271433
    :pswitch_3
    invoke-direct {p0}, LX/1Xd;->K()LX/0lF;

    move-result-object v1

    .line 271434
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0lF;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 271435
    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final p()[C
    .locals 1

    .prologue
    .line 271436
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    return-object v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 271437
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 271438
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 271439
    const/4 v0, 0x0

    return v0
.end method

.method public final t()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 271440
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/16L;
    .locals 1

    .prologue
    .line 271441
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    .line 271442
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/0lF;->b()LX/16L;

    move-result-object v0

    goto :goto_0
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 271443
    sget-object v0, LX/4pz;->VERSION:LX/0ne;

    return-object v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 271444
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->w()I

    move-result v0

    return v0
.end method

.method public final y()J
    .locals 2

    .prologue
    .line 271445
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->x()J

    move-result-wide v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 271456
    invoke-direct {p0}, LX/1Xd;->L()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->A()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
