.class public LX/1kx;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "LX/0zO;",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11F;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/0sT;


# direct methods
.method public constructor <init>(LX/0sO;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0sT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sO;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/11F;",
            ">;",
            "LX/0sT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 310781
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 310782
    iput-object p2, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 310783
    iput-object p3, p0, LX/1kx;->b:LX/0Ot;

    .line 310784
    iput-object p4, p0, LX/1kx;->d:LX/0sT;

    .line 310785
    return-void
.end method

.method public static a(LX/0QB;)LX/1kx;
    .locals 1

    .prologue
    .line 310843
    invoke-static {p0}, LX/1kx;->b(LX/0QB;)LX/1kx;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0zO;LX/1pN;LX/15w;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6

    .prologue
    .line 310831
    iget-object v0, p0, LX/1kx;->d:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 310832
    iget-object v0, p0, LX/1kx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11F;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, p3, p1, v2, v3}, LX/11F;->a(LX/15w;LX/0zO;LX/0ta;Z)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 310833
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 310834
    iget-object v1, p0, LX/0ro;->a:LX/0sO;

    .line 310835
    const/4 v3, 0x0

    .line 310836
    iget-object v2, p2, LX/1pN;->c:LX/0Px;

    move-object v5, v2

    .line 310837
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, p0, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/Header;

    .line 310838
    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p3, "x-fb-graphql-response-id"

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 310839
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 310840
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_0

    .line 310841
    :cond_0
    invoke-static {v1, v0, v3}, LX/11F;->a(LX/0sO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    move-object v0, v2

    .line 310842
    :cond_1
    return-object v0

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method

.method private static b(LX/0zO;LX/1pN;)LX/14S;
    .locals 2

    .prologue
    .line 310820
    iget-object v0, p1, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 310821
    instance-of v1, v0, LX/15w;

    if-eqz v1, :cond_0

    .line 310822
    sget-object v0, LX/14S;->JSONPARSER:LX/14S;

    .line 310823
    :goto_0
    return-object v0

    .line 310824
    :cond_0
    instance-of v0, v0, Ljava/io/InputStream;

    if-eqz v0, :cond_2

    .line 310825
    invoke-static {p0}, LX/1kx;->d(LX/0zO;)LX/0gW;

    move-result-object v0

    .line 310826
    iget-object v1, v0, LX/0gW;->i:Ljava/lang/String;

    move-object v0, v1

    .line 310827
    if-eqz v0, :cond_1

    .line 310828
    sget-object v0, LX/14S;->FLATBUFFER_IDL:LX/14S;

    goto :goto_0

    .line 310829
    :cond_1
    sget-object v0, LX/14S;->FLATBUFFER:LX/14S;

    goto :goto_0

    .line 310830
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "responseObject should either be JsonParser or InputStream"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(LX/0QB;)LX/1kx;
    .locals 5

    .prologue
    .line 310818
    new-instance v3, LX/1kx;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0xb01

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v2

    check-cast v2, LX/0sT;

    invoke-direct {v3, v0, v1, v4, v2}, LX/1kx;-><init>(LX/0sO;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0sT;)V

    .line 310819
    return-object v3
.end method

.method private static d(LX/0zO;)LX/0gW;
    .locals 1

    .prologue
    .line 310816
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 310817
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Object;)LX/0zW;
    .locals 1

    .prologue
    .line 310812
    check-cast p3, LX/0zO;

    .line 310813
    invoke-virtual {p3}, LX/0zO;->i()LX/0zW;

    move-result-object v0

    .line 310814
    if-eqz v0, :cond_0

    .line 310815
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0ro;->a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Object;)LX/0zW;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 310788
    check-cast p1, LX/0zO;

    const/16 v5, 0xc1

    .line 310789
    const-string v0, "GenericGraphQLMethod.getResponse"

    const v1, -0x819067

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 310790
    invoke-virtual {p1}, LX/0zO;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x310029    # 4.499997E-39f

    move v1, v0

    .line 310791
    :goto_0
    iget v0, p1, LX/0zO;->w:I

    move v2, v0

    .line 310792
    :try_start_0
    invoke-static {p1, p2}, LX/1kx;->b(LX/0zO;LX/1pN;)LX/14S;

    move-result-object v0

    .line 310793
    sget-object v3, LX/14S;->FLATBUFFER:LX/14S;

    if-ne v0, v3, :cond_0

    .line 310794
    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "flatbuffer_from_server"

    invoke-interface {v3, v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310795
    :cond_0
    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v4, 0x10

    invoke-interface {v3, v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310796
    sget-object v3, LX/14S;->JSONPARSER:LX/14S;

    if-ne v0, v3, :cond_2

    .line 310797
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/1kx;->a(LX/0zO;LX/1pN;LX/15w;)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 310798
    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v1, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310799
    const v1, 0x6e217c8c

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_1
    return-object v0

    .line 310800
    :cond_1
    const v0, 0x310026    # 4.499993E-39f

    move v1, v0

    goto :goto_0

    .line 310801
    :cond_2
    :try_start_1
    sget-object v3, LX/14S;->FLATBUFFER:LX/14S;

    if-ne v0, v3, :cond_3

    .line 310802
    iget-object v0, p0, LX/1kx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11F;

    invoke-virtual {p2}, LX/1pN;->f()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, LX/11F;->a(Ljava/io/InputStream;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 310803
    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v1, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310804
    const v1, -0x6e407c82

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 310805
    :cond_3
    :try_start_2
    sget-object v3, LX/14S;->FLATBUFFER_IDL:LX/14S;

    if-ne v0, v3, :cond_4

    .line 310806
    iget-object v0, p0, LX/1kx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11F;

    invoke-virtual {p2}, LX/1pN;->f()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, LX/11F;->b(Ljava/io/InputStream;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 310807
    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v1, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310808
    const v1, 0x5abcd113

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 310809
    :cond_4
    :try_start_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 310810
    :catchall_0
    move-exception v0

    iget-object v3, p0, LX/1kx;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v1, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310811
    const v1, -0x305eb649

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 310787
    check-cast p1, LX/0zO;

    invoke-direct {p0, p1, p2, p3}, LX/1kx;->a(LX/0zO;LX/1pN;LX/15w;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 310786
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 2

    .prologue
    .line 310844
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should never be called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()LX/14S;
    .locals 1

    .prologue
    .line 310762
    sget-object v0, LX/14S;->JSONPARSER:LX/14S;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 310763
    check-cast p1, LX/0zO;

    .line 310764
    invoke-virtual {p1}, LX/0zO;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "post"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "get"

    goto :goto_0
.end method

.method public final e(Ljava/lang/Object;)LX/0w7;
    .locals 1

    .prologue
    .line 310765
    check-cast p1, LX/0zO;

    .line 310766
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    if-nez v0, :cond_0

    .line 310767
    sget-object v0, LX/0w7;->a:LX/0w7;

    .line 310768
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 310769
    check-cast p1, LX/0zO;

    invoke-static {p1}, LX/1kx;->d(LX/0zO;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/Object;)LX/14P;
    .locals 1

    .prologue
    .line 310770
    check-cast p1, LX/0zO;

    .line 310771
    invoke-virtual {p1}, LX/0zO;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/14P;->CONSERVATIVE:LX/14P;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/14P;->RETRY_SAFE:LX/14P;

    goto :goto_0
.end method

.method public final h(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 310772
    check-cast p1, LX/0zO;

    .line 310773
    if-eqz p1, :cond_0

    iget-boolean v0, p1, LX/0zO;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 310774
    check-cast p1, LX/0zO;

    .line 310775
    invoke-virtual {p1}, LX/0zO;->i()LX/0zW;

    move-result-object v0

    invoke-virtual {v0}, LX/0zW;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 310776
    check-cast p1, LX/0zO;

    .line 310777
    iget-object v0, p1, LX/0zO;->f:LX/0Px;

    return-object v0
.end method

.method public final k(Ljava/lang/Object;)LX/0Px;
    .locals 1

    .prologue
    .line 310778
    check-cast p1, LX/0zO;

    .line 310779
    iget-object v0, p1, LX/0zO;->x:LX/0Px;

    move-object v0, v0

    .line 310780
    return-object v0
.end method
