.class public LX/0dC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/0dC;


# instance fields
.field private volatile b:LX/0dI;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90105
    const-class v0, LX/0dC;

    sput-object v0, LX/0dC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90107
    iput-object p1, p0, LX/0dC;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90108
    iput-object p2, p0, LX/0dC;->d:LX/0SG;

    .line 90109
    return-void
.end method

.method public static a(LX/0QB;)LX/0dC;
    .locals 5

    .prologue
    .line 90110
    sget-object v0, LX/0dC;->e:LX/0dC;

    if-nez v0, :cond_1

    .line 90111
    const-class v1, LX/0dC;

    monitor-enter v1

    .line 90112
    :try_start_0
    sget-object v0, LX/0dC;->e:LX/0dC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90113
    if-eqz v2, :cond_0

    .line 90114
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90115
    new-instance p0, LX/0dC;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/0dC;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 90116
    move-object v0, p0

    .line 90117
    sput-object v0, LX/0dC;->e:LX/0dC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90120
    :cond_1
    sget-object v0, LX/0dC;->e:LX/0dC;

    return-object v0

    .line 90121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0SG;)LX/0dI;
    .locals 4

    .prologue
    .line 90073
    new-instance v0, LX/0dI;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, LX/0dI;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public static b(LX/0dC;LX/0dI;)V
    .locals 6

    .prologue
    .line 90098
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "saving device id from shared prefs: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90099
    iget-object v0, p0, LX/0dC;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->b:LX/0Tn;

    .line 90100
    iget-wide v4, p1, LX/0dI;->b:J

    move-wide v2, v4

    .line 90101
    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->a:LX/0Tn;

    .line 90102
    iget-object v2, p1, LX/0dI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 90103
    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 90104
    return-void
.end method

.method private declared-synchronized d()LX/0dI;
    .locals 7

    .prologue
    .line 90085
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0dC;->b:LX/0dI;

    if-nez v0, :cond_1

    .line 90086
    const-wide v5, 0x7fffffffffffffffL

    .line 90087
    iget-object v1, p0, LX/0dC;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dH;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90088
    iget-object v1, p0, LX/0dC;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dH;->b:LX/0Tn;

    invoke-interface {v1, v3, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 90089
    if-eqz v2, :cond_0

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    .line 90090
    :cond_0
    iget-object v1, p0, LX/0dC;->d:LX/0SG;

    invoke-static {v1}, LX/0dC;->a(LX/0SG;)LX/0dI;

    move-result-object v1

    .line 90091
    invoke-static {p0, v1}, LX/0dC;->b(LX/0dC;LX/0dI;)V

    .line 90092
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loaded device id from shared prefs: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90093
    move-object v0, v1

    .line 90094
    iput-object v0, p0, LX/0dC;->b:LX/0dI;

    .line 90095
    :cond_1
    iget-object v0, p0, LX/0dC;->b:LX/0dI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 90096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90097
    :cond_2
    new-instance v1, LX/0dI;

    invoke-direct {v1, v2, v3, v4}, LX/0dI;-><init>(Ljava/lang/String;J)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90079
    invoke-direct {p0}, LX/0dC;->d()LX/0dI;

    move-result-object v0

    .line 90080
    if-nez v0, :cond_0

    .line 90081
    const/4 v0, 0x0

    .line 90082
    :goto_0
    return-object v0

    .line 90083
    :cond_0
    iget-object p0, v0, LX/0dI;->a:Ljava/lang/String;

    move-object v0, p0

    .line 90084
    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0dI;)V
    .locals 1

    .prologue
    .line 90075
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0dC;->b:LX/0dI;

    .line 90076
    iget-object v0, p0, LX/0dC;->b:LX/0dI;

    invoke-static {p0, v0}, LX/0dC;->b(LX/0dC;LX/0dI;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90077
    monitor-exit p0

    return-void

    .line 90078
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()LX/0dI;
    .locals 1

    .prologue
    .line 90074
    invoke-direct {p0}, LX/0dC;->d()LX/0dI;

    move-result-object v0

    return-object v0
.end method
