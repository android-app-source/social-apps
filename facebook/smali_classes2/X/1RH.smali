.class public LX/1RH;
.super LX/1RI;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/1E1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245986
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1RH;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;LX/1E1;)V
    .locals 0
    .param p1    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/ipc/productionprompts/annotations/TopLevel;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1RJ;",
            ">;",
            "LX/1E1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245983
    invoke-direct {p0, p1}, LX/1RI;-><init>(Ljava/util/Set;)V

    .line 245984
    iput-object p2, p0, LX/1RH;->a:LX/1E1;

    .line 245985
    return-void
.end method

.method public static a(LX/0QB;)LX/1RH;
    .locals 9

    .prologue
    .line 245952
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 245953
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 245954
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 245955
    if-nez v1, :cond_0

    .line 245956
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245957
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 245958
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 245959
    sget-object v1, LX/1RH;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 245960
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 245961
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 245962
    :cond_1
    if-nez v1, :cond_4

    .line 245963
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 245964
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 245965
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 245966
    new-instance v7, LX/1RH;

    .line 245967
    new-instance v1, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance p0, LX/1RL;

    invoke-direct {p0, v0}, LX/1RL;-><init>(LX/0QB;)V

    invoke-direct {v1, v8, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v8, v1

    .line 245968
    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v1

    check-cast v1, LX/1E1;

    invoke-direct {v7, v8, v1}, LX/1RH;-><init>(Ljava/util/Set;LX/1E1;)V

    .line 245969
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 245970
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 245971
    if-nez v1, :cond_2

    .line 245972
    sget-object v0, LX/1RH;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RH;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 245973
    :goto_1
    if-eqz v0, :cond_3

    .line 245974
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 245975
    :goto_3
    check-cast v0, LX/1RH;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 245976
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 245977
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 245978
    :catchall_1
    move-exception v0

    .line 245979
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 245980
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 245981
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 245982
    :cond_2
    :try_start_8
    sget-object v0, LX/1RH;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RH;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/1EE;Landroid/content/Context;)LX/AkL;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 245950
    invoke-virtual {p1}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/1RI;->a(LX/1RN;Landroid/content/Context;)LX/AlW;

    move-result-object v0

    .line 245951
    return-object v0
.end method

.method public final a(LX/1RN;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245987
    iget-object v0, p0, LX/1RH;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245988
    invoke-super {p0, p1}, LX/1RI;->a(LX/1RN;)V

    .line 245989
    :cond_0
    return-void
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 1
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245947
    iget-object v0, p0, LX/1RH;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245948
    invoke-super {p0, p1, p2}, LX/1RI;->a(LX/2xq;LX/1RN;)V

    .line 245949
    :cond_0
    return-void
.end method

.method public final a(ZLX/1RN;)V
    .locals 1
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245944
    iget-object v0, p0, LX/1RH;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245945
    invoke-super {p0, p1, p2}, LX/1RI;->a(ZLX/1RN;)V

    .line 245946
    :cond_0
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 1
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245941
    iget-object v0, p0, LX/1RH;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245942
    invoke-super {p0, p1, p2}, LX/1RI;->b(LX/2xq;LX/1RN;)V

    .line 245943
    :cond_0
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 245940
    const/4 v0, 0x1

    return v0
.end method
