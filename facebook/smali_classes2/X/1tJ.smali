.class public LX/1tJ;
.super LX/1qk;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1tJ;


# direct methods
.method public constructor <init>(LX/1r1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336360
    const-string v0, "MqttLite"

    invoke-direct {p0, p1, v0}, LX/1qk;-><init>(LX/1r1;Ljava/lang/String;)V

    .line 336361
    return-void
.end method

.method public static a(LX/0QB;)LX/1tJ;
    .locals 4

    .prologue
    .line 336362
    sget-object v0, LX/1tJ;->b:LX/1tJ;

    if-nez v0, :cond_1

    .line 336363
    const-class v1, LX/1tJ;

    monitor-enter v1

    .line 336364
    :try_start_0
    sget-object v0, LX/1tJ;->b:LX/1tJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336365
    if-eqz v2, :cond_0

    .line 336366
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 336367
    new-instance p0, LX/1tJ;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v3

    check-cast v3, LX/1r1;

    invoke-direct {p0, v3}, LX/1tJ;-><init>(LX/1r1;)V

    .line 336368
    move-object v0, p0

    .line 336369
    sput-object v0, LX/1tJ;->b:LX/1tJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336370
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336371
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336372
    :cond_1
    sget-object v0, LX/1tJ;->b:LX/1tJ;

    return-object v0

    .line 336373
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336374
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
