.class public LX/1qw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1qw;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1rV;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0eF;

.field private final c:LX/0W9;


# direct methods
.method public constructor <init>(LX/0eF;LX/0W9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331557
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1qw;->a:Ljava/util/Set;

    .line 331558
    iput-object p1, p0, LX/1qw;->b:LX/0eF;

    .line 331559
    iput-object p2, p0, LX/1qw;->c:LX/0W9;

    .line 331560
    return-void
.end method

.method public static a(LX/0QB;)LX/1qw;
    .locals 5

    .prologue
    .line 331561
    sget-object v0, LX/1qw;->d:LX/1qw;

    if-nez v0, :cond_1

    .line 331562
    const-class v1, LX/1qw;

    monitor-enter v1

    .line 331563
    :try_start_0
    sget-object v0, LX/1qw;->d:LX/1qw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331564
    if-eqz v2, :cond_0

    .line 331565
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331566
    new-instance p0, LX/1qw;

    invoke-static {v0}, LX/0eE;->a(LX/0QB;)LX/0eE;

    move-result-object v3

    check-cast v3, LX/0eF;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {p0, v3, v4}, LX/1qw;-><init>(LX/0eF;LX/0W9;)V

    .line 331567
    move-object v0, p0

    .line 331568
    sput-object v0, LX/1qw;->d:LX/1qw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331569
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331570
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331571
    :cond_1
    sget-object v0, LX/1qw;->d:LX/1qw;

    return-object v0

    .line 331572
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1qw;Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 331574
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/1qw;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 331575
    iget-object v0, p0, LX/1qw;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rV;

    .line 331576
    invoke-interface {v0, p1}, LX/1rV;->a(Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 331577
    if-eqz v0, :cond_0

    .line 331578
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331579
    :cond_1
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1rV;)V
    .locals 1

    .prologue
    .line 331580
    iget-object v0, p0, LX/1qw;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 331581
    return-void
.end method
