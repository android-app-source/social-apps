.class public LX/1pk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1pl;

.field public final e:LX/0Uo;

.field public final f:LX/1pm;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Ck;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Zb;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 329634
    const-class v0, LX/1pk;

    sput-object v0, LX/1pk;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/1pl;LX/0Uo;LX/1pm;LX/0Ot;LX/1Ck;LX/0Ot;LX/0Zb;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoCapFeatureEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1pl;",
            "LX/0Uo;",
            "LX/1pm;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329636
    iput-object p1, p0, LX/1pk;->b:Landroid/content/Context;

    .line 329637
    iput-object p2, p0, LX/1pk;->c:LX/0Or;

    .line 329638
    iput-object p3, p0, LX/1pk;->d:LX/1pl;

    .line 329639
    iput-object p4, p0, LX/1pk;->e:LX/0Uo;

    .line 329640
    iput-object p5, p0, LX/1pk;->f:LX/1pm;

    .line 329641
    iput-object p6, p0, LX/1pk;->g:LX/0Ot;

    .line 329642
    iput-object p7, p0, LX/1pk;->h:LX/1Ck;

    .line 329643
    iput-object p8, p0, LX/1pk;->i:LX/0Ot;

    .line 329644
    iput-object p9, p0, LX/1pk;->j:LX/0Zb;

    .line 329645
    iput-object p10, p0, LX/1pk;->k:LX/0Ot;

    .line 329646
    return-void
.end method
