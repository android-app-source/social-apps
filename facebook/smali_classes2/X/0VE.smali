.class public LX/0VE;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67518
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 67519
    return-void
.end method

.method public static a(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;Landroid/content/Context;LX/0Uh;)LX/03U;
    .locals 6
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/errorreporting/ErrorReportingSingleThreadExecutor;
        .end annotation
    .end param
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/Random;",
            "Landroid/content/Context;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")",
            "LX/03U;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 67520
    new-instance v0, LX/03U;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/03U;-><init>(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;Landroid/content/Context;)V

    .line 67521
    new-instance v1, LX/0VL;

    invoke-direct {v1, p5}, LX/0VL;-><init>(LX/0Uh;)V

    invoke-static {v0, v1}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a(LX/03V;LX/0VL;)V

    .line 67522
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 67523
    return-void
.end method
