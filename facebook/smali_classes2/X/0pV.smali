.class public LX/0pV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/0Ze;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pV;


# instance fields
.field private final a:LX/0Zb;

.field public b:LX/0Yw;

.field private c:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144593
    iput-object p1, p0, LX/0pV;->a:LX/0Zb;

    .line 144594
    new-instance v0, LX/0Yw;

    const v1, 0x7fffffff

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/0pV;->b:LX/0Yw;

    .line 144595
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/0pV;->c:Ljava/lang/StringBuilder;

    .line 144596
    return-void
.end method

.method public static a(LX/0QB;)LX/0pV;
    .locals 4

    .prologue
    .line 144579
    sget-object v0, LX/0pV;->d:LX/0pV;

    if-nez v0, :cond_1

    .line 144580
    const-class v1, LX/0pV;

    monitor-enter v1

    .line 144581
    :try_start_0
    sget-object v0, LX/0pV;->d:LX/0pV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144582
    if-eqz v2, :cond_0

    .line 144583
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144584
    new-instance p0, LX/0pV;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/0pV;-><init>(LX/0Zb;)V

    .line 144585
    move-object v0, p0

    .line 144586
    sput-object v0, LX/0pV;->d:LX/0pV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144587
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144588
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144589
    :cond_1
    sget-object v0, LX/0pV;->d:LX/0pV;

    return-object v0

    .line 144590
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144591
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 144569
    invoke-interface {p1}, LX/1gt;->getClientEventName()Ljava/lang/String;

    move-result-object v0

    .line 144570
    if-nez v0, :cond_1

    .line 144571
    :cond_0
    :goto_0
    return-void

    .line 144572
    :cond_1
    iget-object v1, p0, LX/0pV;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 144573
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144574
    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    .line 144575
    invoke-virtual {v0, p2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 144576
    :cond_2
    if-eqz p4, :cond_3

    if-eqz p5, :cond_3

    .line 144577
    invoke-virtual {v0, p4, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 144578
    :cond_3
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 144568
    iget-object v0, p0, LX/0pV;->b:LX/0Yw;

    invoke-virtual {v0}, LX/0Yw;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/0rj;)V
    .locals 3

    .prologue
    .line 144563
    const-string v0, ""

    .line 144564
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NewsFeedFragment ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 144565
    iget v2, p1, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v2, v2

    .line 144566
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LX/0rj;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144567
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0rj;)V
    .locals 2

    .prologue
    .line 144561
    invoke-virtual {p2}, LX/0rj;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, p1, v0, v1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144562
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144559
    invoke-virtual {p2}, LX/0rj;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144560
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1gt;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 144597
    move-object v0, p0

    move-object v1, p2

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/0pV;->a(LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144598
    invoke-interface {p2}, LX/1gt;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, p1, v0, v1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144599
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 144556
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/0pV;->a(LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144557
    invoke-interface {p2}, LX/1gt;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144558
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 144553
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/0pV;->a(LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144554
    invoke-interface {p2}, LX/1gt;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144555
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144551
    const-string v0, ""

    invoke-virtual {p0, p1, p2, v0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144552
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 144545
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pV;->c:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 144546
    iget-object v0, p0, LX/0pV;->c:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144547
    iget-object v0, p0, LX/0pV;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144548
    iget-object v1, p0, LX/0pV;->b:LX/0Yw;

    invoke-virtual {v1, v0}, LX/0Yw;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144549
    monitor-exit p0

    return-void

    .line 144550
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144544
    invoke-virtual {p0}, LX/0pV;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144543
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144542
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "news_feed_events"

    iget-object v2, p0, LX/0pV;->b:LX/0Yw;

    invoke-virtual {v2}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
