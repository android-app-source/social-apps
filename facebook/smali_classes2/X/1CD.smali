.class public LX/1CD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Landroid/content/IntentFilter;

.field private static volatile e:LX/1CD;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:Z

.field public d:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 215636
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1CD;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215633
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1CD;->c:Z

    .line 215634
    iput-object p1, p0, LX/1CD;->b:Landroid/content/Context;

    .line 215635
    return-void
.end method

.method public static a(LX/0QB;)LX/1CD;
    .locals 4

    .prologue
    .line 215604
    sget-object v0, LX/1CD;->e:LX/1CD;

    if-nez v0, :cond_1

    .line 215605
    const-class v1, LX/1CD;

    monitor-enter v1

    .line 215606
    :try_start_0
    sget-object v0, LX/1CD;->e:LX/1CD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 215607
    if-eqz v2, :cond_0

    .line 215608
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 215609
    new-instance p0, LX/1CD;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/1CD;-><init>(Landroid/content/Context;)V

    .line 215610
    move-object v0, p0

    .line 215611
    sput-object v0, LX/1CD;->e:LX/1CD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215612
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 215613
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215614
    :cond_1
    sget-object v0, LX/1CD;->e:LX/1CD;

    return-object v0

    .line 215615
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 215616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/45B;
    .locals 4

    .prologue
    .line 215617
    const/4 v3, 0x1

    .line 215618
    iget-boolean v0, p0, LX/1CD;->c:Z

    if-eqz v0, :cond_2

    .line 215619
    iget-object v0, p0, LX/1CD;->d:Landroid/content/Intent;

    .line 215620
    :goto_0
    move-object v0, v0

    .line 215621
    if-eqz v0, :cond_1

    .line 215622
    const-string v1, "state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 215623
    sget-object v0, LX/45B;->DISCONNECTED:LX/45B;

    .line 215624
    :goto_1
    return-object v0

    .line 215625
    :cond_0
    sget-object v0, LX/45B;->CONNECTED:LX/45B;

    goto :goto_1

    .line 215626
    :cond_1
    sget-object v0, LX/45B;->UNKNOWN:LX/45B;

    goto :goto_1

    .line 215627
    :cond_2
    invoke-static {v3}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 215628
    const-string v1, "android.intent.action.HEADSET_PLUG"

    new-instance v2, LX/45A;

    invoke-direct {v2, p0}, LX/45A;-><init>(LX/1CD;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215629
    iget-object v1, p0, LX/1CD;->b:Landroid/content/Context;

    new-instance v2, LX/0Yd;

    invoke-direct {v2, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    sget-object v0, LX/1CD;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, LX/1CD;->d:Landroid/content/Intent;

    .line 215630
    iput-boolean v3, p0, LX/1CD;->c:Z

    .line 215631
    iget-object v0, p0, LX/1CD;->d:Landroid/content/Intent;

    goto :goto_0
.end method
