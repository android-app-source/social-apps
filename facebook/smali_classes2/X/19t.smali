.class public final enum LX/19t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/19t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/19t;

.field public static final enum CELL:LX/19t;

.field public static final enum NONE:LX/19t;

.field public static final enum UNKNOWN:LX/19t;

.field public static final enum WIFI:LX/19t;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 208993
    new-instance v0, LX/19t;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/19t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/19t;->UNKNOWN:LX/19t;

    .line 208994
    new-instance v0, LX/19t;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/19t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/19t;->NONE:LX/19t;

    .line 208995
    new-instance v0, LX/19t;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v4}, LX/19t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/19t;->WIFI:LX/19t;

    .line 208996
    new-instance v0, LX/19t;

    const-string v1, "CELL"

    invoke-direct {v0, v1, v5}, LX/19t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/19t;->CELL:LX/19t;

    .line 208997
    const/4 v0, 0x4

    new-array v0, v0, [LX/19t;

    sget-object v1, LX/19t;->UNKNOWN:LX/19t;

    aput-object v1, v0, v2

    sget-object v1, LX/19t;->NONE:LX/19t;

    aput-object v1, v0, v3

    sget-object v1, LX/19t;->WIFI:LX/19t;

    aput-object v1, v0, v4

    sget-object v1, LX/19t;->CELL:LX/19t;

    aput-object v1, v0, v5

    sput-object v0, LX/19t;->$VALUES:[LX/19t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 208990
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/19t;
    .locals 1

    .prologue
    .line 208992
    const-class v0, LX/19t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/19t;

    return-object v0
.end method

.method public static values()[LX/19t;
    .locals 1

    .prologue
    .line 208991
    sget-object v0, LX/19t;->$VALUES:[LX/19t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/19t;

    return-object v0
.end method
