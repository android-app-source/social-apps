.class public LX/0bb;
.super LX/0bY;
.source ""

# interfaces
.implements LX/0bZ;


# instance fields
.field private final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87066
    invoke-direct {p0}, LX/0bY;-><init>()V

    .line 87067
    iput-object p1, p0, LX/0bb;->a:LX/0am;

    .line 87068
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87069
    const-string v1, "Pulsar presence stayed the same: %s"

    iget-object v0, p0, LX/0bb;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0bb;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public final d()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87070
    iget-object v0, p0, LX/0bb;->a:LX/0am;

    return-object v0
.end method
