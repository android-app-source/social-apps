.class public LX/0yX;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165011
    const-class v0, LX/0yX;

    sput-object v0, LX/0yX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 165012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165013
    const-string v0, "zero_data_state_change_event"

    iput-object v0, p0, LX/0yX;->b:Ljava/lang/String;

    .line 165014
    iput-object p1, p0, LX/0yX;->e:LX/0Zb;

    .line 165015
    iput-object p2, p0, LX/0yX;->c:LX/0Or;

    .line 165016
    iput-object p3, p0, LX/0yX;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 165017
    return-void
.end method


# virtual methods
.method public final a(LX/2XZ;LX/2XZ;ILjava/lang/String;JJJ)V
    .locals 5

    .prologue
    .line 165018
    iget-object v0, p0, LX/0yX;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yh;

    .line 165019
    iget-object v1, p0, LX/0yX;->e:LX/0Zb;

    const-string v2, "zero_data_state_change_event"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 165020
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165021
    const-string v2, "Old State"

    invoke-virtual {p1}, LX/2XZ;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 165022
    const-string v2, "New State"

    invoke-virtual {p2}, LX/2XZ;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 165023
    const-string v2, "Most recent status code"

    invoke-virtual {v1, v2, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 165024
    const-string v2, "uri"

    invoke-virtual {v1, v2, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 165025
    const-string v2, "Consecutive failed standard requests"

    invoke-virtual {v1, v2, p5, p6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 165026
    const-string v2, "Consecutive failed bootstrap requests"

    invoke-virtual {v1, v2, p7, p8}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 165027
    const-string v2, "Last state change time"

    invoke-virtual {v1, v2, p9, p10}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 165028
    invoke-virtual {v0}, LX/0yh;->getRegistrationStatusKey()LX/0Tn;

    move-result-object v2

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/0yX;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0}, LX/0yh;->getRegistrationStatusKey()LX/0Tn;

    move-result-object v0

    const-string v4, "unknown"

    invoke-interface {v3, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 165029
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 165030
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 165031
    iget-object v0, p0, LX/0yX;->e:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 165032
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 165033
    return-void
.end method
