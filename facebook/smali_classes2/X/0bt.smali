.class public final LX/0bt;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/3R2;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bt;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3R2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87392
    const/16 v0, 0x1eb

    move v0, v0

    .line 87393
    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;I)V

    .line 87394
    return-void
.end method

.method public static a(LX/0QB;)LX/0bt;
    .locals 4

    .prologue
    .line 87398
    sget-object v0, LX/0bt;->b:LX/0bt;

    if-nez v0, :cond_1

    .line 87399
    const-class v1, LX/0bt;

    monitor-enter v1

    .line 87400
    :try_start_0
    sget-object v0, LX/0bt;->b:LX/0bt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87401
    if-eqz v2, :cond_0

    .line 87402
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87403
    new-instance v3, LX/0bt;

    const/16 p0, 0xdd8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0bt;-><init>(LX/0Ot;)V

    .line 87404
    move-object v0, v3

    .line 87405
    sput-object v0, LX/0bt;->b:LX/0bt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87408
    :cond_1
    sget-object v0, LX/0bt;->b:LX/0bt;

    return-object v0

    .line 87409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 87395
    check-cast p3, LX/3R2;

    .line 87396
    invoke-virtual {p3}, LX/3R2;->a()V

    .line 87397
    return-void
.end method
