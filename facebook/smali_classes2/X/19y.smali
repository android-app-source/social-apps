.class public final LX/19y;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 209825
    new-instance v0, LX/0su;

    sget-object v1, LX/19z;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/19y;->a:LX/0sv;

    .line 209826
    sget-object v0, LX/19z;->a:LX/0U1;

    sget-object v1, LX/19z;->b:LX/0U1;

    sget-object v2, LX/19z;->c:LX/0U1;

    sget-object v3, LX/19z;->d:LX/0U1;

    sget-object v4, LX/19z;->e:LX/0U1;

    sget-object v5, LX/19z;->f:LX/0U1;

    sget-object v6, LX/19z;->g:LX/0U1;

    sget-object v7, LX/19z;->h:LX/0U1;

    sget-object v8, LX/19z;->i:LX/0U1;

    sget-object v9, LX/19z;->j:LX/0U1;

    sget-object v10, LX/19z;->k:LX/0U1;

    sget-object v11, LX/19z;->l:LX/0U1;

    const/4 v12, 0x5

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/19z;->m:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/19z;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/19z;->o:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, LX/19z;->p:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, LX/19z;->q:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/19y;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 209827
    const-string v0, "saved_videos"

    sget-object v1, LX/19y;->b:LX/0Px;

    sget-object v2, LX/19y;->a:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 209828
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 209829
    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    .line 209830
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN last_check_time LONG"

    const v1, 0x587acb83

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x266b888c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209831
    :cond_0
    const/4 v0, 0x3

    if-ge p2, v0, :cond_1

    .line 209832
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN scheduling_policy INTEGER"

    const v1, 0x3a288d4c

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6e1d4195

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209833
    :cond_1
    const/4 v0, 0x4

    if-ge p2, v0, :cond_2

    .line 209834
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN download_type INTEGER"

    const v1, -0x296515d7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3b336624

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209835
    :cond_2
    const/4 v0, 0x5

    if-ge p2, v0, :cond_3

    .line 209836
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN last_update_time LONG"

    const v1, 0x16c4a596    # 3.1770007E-25f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x451921ea

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209837
    :cond_3
    const/4 v0, 0x6

    if-ge p2, v0, :cond_4

    .line 209838
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN audio_url TEXT"

    const v1, -0x782f63b7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x473ece95

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209839
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN audio_size INTEGER"

    const v1, 0x389112fc

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2144e892

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209840
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN audio_downloaded_size INTEGER"

    const v1, 0x2d5bf24b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x4f04e3b6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209841
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN audio_file TEXT"

    const v1, 0x3c395716

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x1cfd4477

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209842
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN video_format_id TEXT"

    const v1, -0x24cff064

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x63db9e8b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209843
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN audio_format_id TEXT"

    const v1, -0x57df4151

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x29537077

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209844
    :cond_4
    const/4 v0, 0x7

    if-ge p2, v0, :cond_5

    .line 209845
    const-string v0, "ALTER TABLE saved_videos ADD COLUMN dash_manifest TEXT"

    const v1, 0x310b80c8

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x27604257

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209846
    :cond_5
    return-void
.end method
