.class public abstract LX/0wv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161372
    iput-object p0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    .line 161373
    return-void
.end method

.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 161368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161369
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    iput-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    .line 161370
    return-void
.end method

.method public static a(Ljava/lang/Iterable;)LX/0wv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "LX/0wv",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161367
    instance-of v0, p0, LX/0wv;

    if-eqz v0, :cond_0

    check-cast p0, LX/0wv;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4xm;

    invoke-direct {v0, p0, p0}, LX/4xm;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161365
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 161366
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0QK;)LX/0wv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QK",
            "<-TE;TT;>;)",
            "LX/0wv",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161364
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0, p1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Rl;)LX/0wv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-TE;>;)",
            "LX/0wv",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161357
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0, p1}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)LX/0wv;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Class.isInstance"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0wv",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161363
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0, p1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161362
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0Rl;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-TE;>;)Z"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161361
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0, p1}, LX/0Ph;->d(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final c()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161360
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/0Rl;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-TE;>;)Z"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161359
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0, p1}, LX/0Ph;->e(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161358
    iget-object v0, p0, LX/0wv;->a:Ljava/lang/Iterable;

    invoke-static {v0}, LX/0Ph;->c(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
