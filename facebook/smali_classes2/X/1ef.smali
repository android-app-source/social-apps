.class public final LX/1ef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:LX/1bh;

.field public final synthetic c:LX/1HY;


# direct methods
.method public constructor <init>(LX/1HY;Ljava/util/concurrent/atomic/AtomicBoolean;LX/1bh;)V
    .locals 0

    .prologue
    .line 288713
    iput-object p1, p0, LX/1ef;->c:LX/1HY;

    iput-object p2, p0, LX/1ef;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, LX/1ef;->b:LX/1bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 288714
    iget-object v0, p0, LX/1ef;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288715
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 288716
    :cond_0
    iget-object v0, p0, LX/1ef;->c:LX/1HY;

    iget-object v0, v0, LX/1HY;->g:LX/1IY;

    iget-object v1, p0, LX/1ef;->b:LX/1bh;

    invoke-virtual {v0, v1}, LX/1IY;->a(LX/1bh;)LX/1FL;

    move-result-object v0

    .line 288717
    if-eqz v0, :cond_2

    .line 288718
    iget-object v1, p0, LX/1ef;->b:LX/1bh;

    invoke-interface {v1}, LX/1bh;->a()Ljava/lang/String;

    .line 288719
    iget-object v1, p0, LX/1ef;->c:LX/1HY;

    iget-object v1, v1, LX/1HY;->h:LX/1GF;

    iget-object v2, p0, LX/1ef;->b:LX/1bh;

    invoke-interface {v1, v2}, LX/1GF;->c(LX/1bh;)V

    .line 288720
    iget-object v1, p0, LX/1ef;->b:LX/1bh;

    .line 288721
    iput-object v1, v0, LX/1FL;->i:LX/1bh;

    .line 288722
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 288723
    if-eqz v0, :cond_1

    .line 288724
    invoke-virtual {v0}, LX/1FL;->close()V

    .line 288725
    :cond_1
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 288726
    :cond_2
    iget-object v0, p0, LX/1ef;->b:LX/1bh;

    invoke-interface {v0}, LX/1bh;->a()Ljava/lang/String;

    .line 288727
    iget-object v0, p0, LX/1ef;->c:LX/1HY;

    iget-object v0, v0, LX/1HY;->h:LX/1GF;

    invoke-interface {v0}, LX/1GF;->f()V

    .line 288728
    :try_start_0
    iget-object v0, p0, LX/1ef;->c:LX/1HY;

    iget-object v1, p0, LX/1ef;->b:LX/1bh;

    invoke-static {v0, v1}, LX/1HY;->f(LX/1HY;LX/1bh;)LX/1FK;

    move-result-object v0

    .line 288729
    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 288730
    :try_start_1
    new-instance v0, LX/1FL;

    invoke-direct {v0, v1}, LX/1FL;-><init>(LX/1FJ;)V

    .line 288731
    iget-object v2, p0, LX/1ef;->b:LX/1bh;

    .line 288732
    iput-object v2, v0, LX/1FL;->i:LX/1bh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288733
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 288734
    :catch_0
    const/4 v0, 0x0

    .line 288735
    :cond_3
    return-object v0

    .line 288736
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
