.class public LX/1ku;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:LX/0sg;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kx;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tc;

.field private final d:LX/11H;

.field private final e:LX/0SI;

.field private final f:LX/0SI;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0t2;

.field private final i:LX/0sT;

.field private final j:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final k:LX/0tA;

.field private final l:LX/03V;

.field private final m:LX/0Uh;

.field private final n:LX/1kv;


# direct methods
.method public constructor <init>(LX/0sg;LX/0Ot;LX/0tc;LX/11H;LX/0SI;LX/0SI;Ljava/util/Set;LX/0t2;LX/0sT;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0tA;LX/0Uh;LX/03V;LX/1kv;)V
    .locals 0
    .param p6    # LX/0SI;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "LX/0Ot",
            "<",
            "LX/1kx;",
            ">;",
            "LX/0tc;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0SI;",
            "LX/0SI;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "LX/0t2;",
            "LX/0sT;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0tA;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1kv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 310467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310468
    iput-object p1, p0, LX/1ku;->a:LX/0sg;

    .line 310469
    iput-object p2, p0, LX/1ku;->b:LX/0Ot;

    .line 310470
    iput-object p3, p0, LX/1ku;->c:LX/0tc;

    .line 310471
    iput-object p4, p0, LX/1ku;->d:LX/11H;

    .line 310472
    iput-object p5, p0, LX/1ku;->e:LX/0SI;

    .line 310473
    iput-object p6, p0, LX/1ku;->f:LX/0SI;

    .line 310474
    iput-object p7, p0, LX/1ku;->g:Ljava/util/Set;

    .line 310475
    iput-object p8, p0, LX/1ku;->h:LX/0t2;

    .line 310476
    iput-object p9, p0, LX/1ku;->i:LX/0sT;

    .line 310477
    iput-object p10, p0, LX/1ku;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 310478
    iput-object p11, p0, LX/1ku;->k:LX/0tA;

    .line 310479
    iput-object p12, p0, LX/1ku;->m:LX/0Uh;

    .line 310480
    iput-object p13, p0, LX/1ku;->l:LX/03V;

    .line 310481
    iput-object p14, p0, LX/1ku;->n:LX/1kv;

    .line 310482
    return-void
.end method

.method public static a(LX/0QB;)LX/1ku;
    .locals 3

    .prologue
    .line 310459
    const-class v1, LX/1ku;

    monitor-enter v1

    .line 310460
    :try_start_0
    sget-object v0, LX/1ku;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 310461
    sput-object v2, LX/1ku;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 310462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1ku;->b(LX/0QB;)LX/1ku;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 310464
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ku;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310465
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 310466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/1ku;LX/0zO;)LX/1kx;
    .locals 1

    .prologue
    .line 310456
    iget-object v0, p1, LX/0zO;->u:LX/1kx;

    move-object v0, v0

    .line 310457
    if-eqz v0, :cond_0

    .line 310458
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1ku;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kx;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1ku;
    .locals 15

    .prologue
    .line 310453
    new-instance v0, LX/1ku;

    invoke-static {p0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v1

    check-cast v1, LX/0sg;

    const/16 v2, 0xafa

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v3

    check-cast v3, LX/0tc;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-interface {p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v6

    invoke-static {v6}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v6

    check-cast v6, LX/0SI;

    invoke-static {p0}, LX/0tj;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v7

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v8

    check-cast v8, LX/0t2;

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v9

    check-cast v9, LX/0sT;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v11

    check-cast v11, LX/0tA;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static {p0}, LX/1kv;->b(LX/0QB;)LX/1kv;

    move-result-object v14

    check-cast v14, LX/1kv;

    invoke-direct/range {v0 .. v14}, LX/1ku;-><init>(LX/0sg;LX/0Ot;LX/0tc;LX/11H;LX/0SI;LX/0SI;Ljava/util/Set;LX/0t2;LX/0sT;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0tA;LX/0Uh;LX/03V;LX/1kv;)V

    .line 310454
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/locks/ReadWriteLock;LX/0zO;LX/1kt;I)Lcom/facebook/graphql/executor/CacheReadRunner;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/locks/ReadWriteLock;",
            "LX/0zO",
            "<TT;>;",
            "LX/1kt",
            "<TT;>;I)",
            "Lcom/facebook/graphql/executor/CacheReadRunner",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 310455
    new-instance v2, Lcom/facebook/graphql/executor/CacheReadRunner;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1ku;->a:LX/0sg;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/1ku;->a(LX/1ku;LX/0zO;)LX/1kx;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1ku;->c:LX/0tc;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1ku;->d:LX/11H;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1ku;->f:LX/0SI;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1ku;->e:LX/0SI;

    invoke-interface {v4}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/1ku;->g:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/1ku;->h:LX/0t2;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/1ku;->i:LX/0sT;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/1ku;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1ku;->k:LX/0tA;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1ku;->l:LX/03V;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1ku;->m:LX/0Uh;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1ku;->n:LX/1kv;

    move-object/from16 v19, v0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v20, p4

    invoke-direct/range {v2 .. v20}, Lcom/facebook/graphql/executor/CacheReadRunner;-><init>(LX/0sg;Ljava/util/concurrent/locks/ReadWriteLock;LX/1kx;LX/0tc;LX/11H;LX/0zO;LX/1kt;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/Set;LX/0t2;LX/0sT;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0tA;LX/03V;LX/0Uh;LX/1kv;I)V

    return-object v2
.end method
