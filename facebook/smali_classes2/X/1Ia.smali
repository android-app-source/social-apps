.class public LX/1Ia;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ib;


# instance fields
.field private final a:LX/1HY;

.field private final b:LX/1HY;

.field private final c:LX/1Ao;


# direct methods
.method public constructor <init>(LX/1HY;LX/1HY;LX/1Ao;)V
    .locals 0

    .prologue
    .line 228895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228896
    iput-object p1, p0, LX/1Ia;->a:LX/1HY;

    .line 228897
    iput-object p2, p0, LX/1Ia;->b:LX/1HY;

    .line 228898
    iput-object p3, p0, LX/1Ia;->c:LX/1Ao;

    .line 228899
    return-void
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/1eg",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228900
    iget-object v0, p0, LX/1Ia;->c:LX/1Ao;

    invoke-virtual {v0, p1, p2}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 228901
    iget-object v1, p1, LX/1bf;->a:LX/1bb;

    move-object v1, v1

    .line 228902
    sget-object v2, LX/1bb;->SMALL:LX/1bb;

    if-ne v1, v2, :cond_0

    .line 228903
    iget-object v1, p0, LX/1Ia;->b:LX/1HY;

    invoke-virtual {v1, v0, p3}, LX/1HY;->a(LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v0

    .line 228904
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/1Ia;->a:LX/1HY;

    invoke-virtual {v1, v0, p3}, LX/1HY;->a(LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1FL;LX/1bf;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 228905
    iget-object v0, p0, LX/1Ia;->c:LX/1Ao;

    invoke-virtual {v0, p2, p3}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 228906
    iget-object v1, p2, LX/1bf;->a:LX/1bb;

    move-object v1, v1

    .line 228907
    sget-object v2, LX/1bb;->SMALL:LX/1bb;

    if-ne v1, v2, :cond_0

    .line 228908
    iget-object v1, p0, LX/1Ia;->b:LX/1HY;

    invoke-virtual {v1, v0, p1}, LX/1HY;->a(LX/1bh;LX/1FL;)V

    .line 228909
    :goto_0
    return-void

    .line 228910
    :cond_0
    iget-object v1, p0, LX/1Ia;->a:LX/1HY;

    invoke-virtual {v1, v0, p1}, LX/1HY;->a(LX/1bh;LX/1FL;)V

    goto :goto_0
.end method
