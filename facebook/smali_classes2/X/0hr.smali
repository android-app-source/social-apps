.class public LX/0hr;
.super LX/0hi;
.source ""


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/10V;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/10V;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 118633
    invoke-direct {p0}, LX/0hi;-><init>()V

    .line 118634
    iput-object p1, p0, LX/0hr;->a:LX/0Ot;

    .line 118635
    iput-object p2, p0, LX/0hr;->b:LX/0Ot;

    .line 118636
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 118629
    iput-object v0, p0, LX/0hr;->a:LX/0Ot;

    .line 118630
    iput-object v0, p0, LX/0hr;->b:LX/0Ot;

    .line 118631
    invoke-super {p0}, LX/0hi;->a()V

    .line 118632
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 118624
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118625
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118626
    iget-object p0, v0, LX/0hC;->a:LX/0hE;

    if-eqz p0, :cond_0

    .line 118627
    iget-object p0, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {p0, p1}, LX/0hE;->t_(I)V

    .line 118628
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118612
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118613
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    const/4 p1, 0x0

    .line 118614
    iput-object p1, v0, LX/0hC;->a:LX/0hE;

    .line 118615
    iput-object p1, v0, LX/0hC;->f:Landroid/app/Activity;

    .line 118616
    :cond_0
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 118617
    const/4 v0, 0x1

    move v0, v0

    .line 118618
    if-eqz v0, :cond_2

    .line 118619
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10V;

    .line 118620
    iget-object p0, v0, LX/10V;->c:LX/0wd;

    if-eqz p0, :cond_1

    .line 118621
    iget-object p0, v0, LX/10V;->c:LX/0wd;

    invoke-virtual {p0}, LX/0wd;->a()V

    .line 118622
    :cond_1
    const/4 p0, 0x0

    iput-object p0, v0, LX/10V;->a:Landroid/app/Activity;

    .line 118623
    :cond_2
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118594
    :try_start_0
    const-string v0, "FbMainTabActivityControllerCallbacksDispatcher.onCreateActivity"

    const v1, -0x2e6c55cf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118595
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 118596
    const/4 v0, 0x1

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 118597
    if-eqz v0, :cond_0

    .line 118598
    :try_start_1
    const-string v0, "FbMainTabActivityFabViewController"

    const v1, 0x185f1384

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118599
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10V;

    .line 118600
    iput-object p1, v0, LX/10V;->a:Landroid/app/Activity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118601
    const v0, 0x699b863a

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118602
    :cond_0
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 118603
    :try_start_3
    const-string v0, "FbMainTabActivityFullScreenVideoPlayerController"

    const v1, -0x3f2d8c13

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118604
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118605
    iput-object p1, v0, LX/0hC;->f:Landroid/app/Activity;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 118606
    const v0, 0x54cc114c

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 118607
    :cond_1
    const v0, 0x727db577

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118608
    return-void

    .line 118609
    :catchall_0
    move-exception v0

    const v1, 0xd78f141

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 118610
    :catchall_1
    move-exception v0

    const v1, 0x697bf29f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118611
    :catchall_2
    move-exception v0

    const v1, -0x789f8b57

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(ZILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 6
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118574
    :try_start_0
    const-string v0, "FbMainTabActivityControllerCallbacksDispatcher.updateFabView"

    const v1, -0x5e5a53a6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118575
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 118576
    const/4 v0, 0x1

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 118577
    if-eqz v0, :cond_0

    .line 118578
    :try_start_1
    const-string v0, "FbMainTabActivityFabViewController"

    const v1, 0x237389cd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118579
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10V;

    .line 118580
    invoke-static {v0}, LX/10V;->e(LX/10V;)V

    .line 118581
    if-nez p1, :cond_1

    .line 118582
    invoke-virtual {v0}, LX/10V;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118583
    :goto_0
    const v0, -0xcedea8f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118584
    :cond_0
    const v0, 0x18e35896

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118585
    return-void

    .line 118586
    :catchall_0
    move-exception v0

    const v1, 0x13ee71e7

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 118587
    :catchall_1
    move-exception v0

    const v1, 0x780eb1bf

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118588
    :cond_1
    iget-object v2, v0, LX/10V;->b:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/uicontrib/fab/FabView;

    .line 118589
    invoke-virtual {v2, p2}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableID(I)V

    .line 118590
    invoke-virtual {v2, p3}, Lcom/facebook/uicontrib/fab/FabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118591
    invoke-virtual {v2, p4}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118592
    iget-object v2, v0, LX/10V;->b:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/uicontrib/fab/FabView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 118593
    iget-object v2, v0, LX/10V;->c:LX/0wd;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final b()V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118567
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 118568
    const/4 v0, 0x1

    move v0, v0

    .line 118569
    if-eqz v0, :cond_0

    .line 118570
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10V;

    .line 118571
    iget-object v1, v0, LX/10V;->b:LX/0zw;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/10V;->b:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118572
    iget-object v1, v0, LX/10V;->b:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/fab/FabView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118573
    :cond_0
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118529
    :try_start_0
    const-string v0, "FbMainTabActivityControllerCallbacksDispatcher.onResume"

    const v1, -0x662bf000

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118530
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 118531
    :try_start_1
    const-string v0, "FbMainTabActivityFullScreenVideoPlayerController"

    const v1, 0x4d8b52f8    # 2.92183808E8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118532
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118533
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118534
    iget-object v1, v0, LX/0hC;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    const-string p0, "system_triggered"

    invoke-virtual {v1, p0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v1

    const-string p0, "video"

    const/4 p1, 0x1

    invoke-virtual {v1, p0, p1}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 118535
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {v1}, LX/0hE;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118536
    :cond_0
    const v0, 0x45fbb35d

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118537
    :cond_1
    const v0, -0x66c76b9b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118538
    return-void

    .line 118539
    :catchall_0
    move-exception v0

    const v1, 0xa86885e

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 118540
    :catchall_1
    move-exception v0

    const v1, 0x2d01057d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 118562
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 118563
    const/4 v0, 0x1

    move v0, v0

    .line 118564
    if-eqz v0, :cond_0

    .line 118565
    iget-object v0, p0, LX/0hr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10V;

    invoke-virtual {v0}, LX/10V;->d()V

    .line 118566
    :cond_0
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118557
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118558
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118559
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 118560
    iget-object p0, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {p0}, LX/0hE;->g()V

    .line 118561
    :cond_0
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118546
    :try_start_0
    const-string v0, "FbMainTabActivityControllerCallbacksDispatcher.onStartActivity"

    const v1, -0x6d58309b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118547
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 118548
    :try_start_1
    const-string v0, "FbMainTabActivityFullScreenVideoPlayerController"

    const v1, -0x3aad7c10

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118549
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118550
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118551
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {v1}, LX/0hE;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118552
    :cond_0
    const v0, 0x38fe138f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118553
    :cond_1
    const v0, 0x1493e50c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118554
    return-void

    .line 118555
    :catchall_0
    move-exception v0

    const v1, -0x3914d147

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 118556
    :catchall_1
    move-exception v0

    const v1, 0x6b0624fd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118541
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118542
    iget-object v0, p0, LX/0hr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 118543
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 118544
    iget-object p0, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {p0}, LX/0hE;->h()V

    .line 118545
    :cond_0
    return-void
.end method
