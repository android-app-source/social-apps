.class public final LX/1VT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1VQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1VQ",
        "<",
        "Ljava/lang/Class",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1VT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 260403
    new-instance v0, LX/1VT;

    invoke-direct {v0}, LX/1VT;-><init>()V

    sput-object v0, LX/1VT;->a:LX/1VT;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 260404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/nio/ByteBuffer;II)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260405
    mul-int/lit8 v0, p4, 0x4

    add-int/2addr v0, p3

    .line 260406
    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 260407
    if-nez v1, :cond_0

    .line 260408
    const/4 v0, 0x0

    .line 260409
    :goto_0
    return-object v0

    :cond_0
    add-int/2addr v0, v1

    invoke-static {p2, v0}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
