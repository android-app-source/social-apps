.class public final LX/1I9;
.super LX/1IA;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:[C

.field private final g:[B

.field private final h:[Z


# direct methods
.method public constructor <init>(Ljava/lang/String;[C)V
    .locals 10

    .prologue
    const/16 v4, 0x8

    const/4 v9, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 228435
    invoke-direct {p0}, LX/1IA;-><init>()V

    .line 228436
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1I9;->e:Ljava/lang/String;

    .line 228437
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    iput-object v0, p0, LX/1I9;->f:[C

    .line 228438
    :try_start_0
    array-length v0, p2

    sget-object v2, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {v0, v2}, LX/1IS;->a(ILjava/math/RoundingMode;)I

    move-result v0

    iput v0, p0, LX/1I9;->b:I
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228439
    iget v0, p0, LX/1I9;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->lowestOneBit(I)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 228440
    div-int v2, v4, v0

    iput v2, p0, LX/1I9;->c:I

    .line 228441
    iget v2, p0, LX/1I9;->b:I

    div-int v0, v2, v0

    iput v0, p0, LX/1I9;->d:I

    .line 228442
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1I9;->a:I

    .line 228443
    const/16 v0, 0x80

    new-array v4, v0, [B

    .line 228444
    invoke-static {v4, v9}, Ljava/util/Arrays;->fill([BB)V

    move v0, v1

    .line 228445
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 228446
    aget-char v5, p2, v0

    .line 228447
    sget-object v2, LX/1IA;->ASCII:LX/1IA;

    invoke-virtual {v2, v5}, LX/1IA;->matches(C)Z

    move-result v2

    const-string v6, "Non-ASCII character: %s"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v2, v6, v7}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 228448
    aget-byte v2, v4, v5

    if-ne v2, v9, :cond_0

    move v2, v3

    :goto_1
    const-string v6, "Duplicate character: %s"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v2, v6, v7}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 228449
    int-to-byte v2, v0

    aput-byte v2, v4, v5

    .line 228450
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228451
    :catch_0
    move-exception v0

    .line 228452
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal alphabet length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move v2, v1

    .line 228453
    goto :goto_1

    .line 228454
    :cond_1
    iput-object v4, p0, LX/1I9;->g:[B

    .line 228455
    iget v0, p0, LX/1I9;->c:I

    new-array v0, v0, [Z

    .line 228456
    :goto_2
    iget v2, p0, LX/1I9;->d:I

    if-ge v1, v2, :cond_2

    .line 228457
    mul-int/lit8 v2, v1, 0x8

    iget v4, p0, LX/1I9;->b:I

    sget-object v5, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v2, v4, v5}, LX/1IS;->a(IILjava/math/RoundingMode;)I

    move-result v2

    aput-boolean v3, v0, v2

    .line 228458
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 228459
    :cond_2
    iput-object v0, p0, LX/1I9;->h:[Z

    .line 228460
    return-void
.end method

.method public static b(LX/1I9;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 228429
    iget-object v2, p0, LX/1I9;->f:[C

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-char v4, v2, v1

    .line 228430
    const/16 p0, 0x61

    if-lt v4, p0, :cond_2

    const/16 p0, 0x7a

    if-gt v4, p0, :cond_2

    const/4 p0, 0x1

    :goto_1
    move v4, p0

    .line 228431
    if-eqz v4, :cond_1

    .line 228432
    const/4 v0, 0x1

    .line 228433
    :cond_0
    return v0

    .line 228434
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(I)C
    .locals 1

    .prologue
    .line 228428
    iget-object v0, p0, LX/1I9;->f:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public final a(C)I
    .locals 4

    .prologue
    .line 228425
    const/16 v0, 0x7f

    if-gt p1, v0, :cond_0

    iget-object v0, p0, LX/1I9;->g:[B

    aget-byte v0, v0, p1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 228426
    :cond_0
    new-instance v1, LX/51z;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Unrecognized character: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, LX/1IA;->INVISIBLE:LX/1IA;

    invoke-virtual {v0, p1}, LX/1IA;->matches(C)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "0x"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/51z;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    goto :goto_0

    .line 228427
    :cond_2
    iget-object v0, p0, LX/1I9;->g:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 228422
    iget-object v0, p0, LX/1I9;->h:[Z

    iget v1, p0, LX/1I9;->c:I

    rem-int v1, p1, v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public final matches(C)Z
    .locals 2

    .prologue
    .line 228424
    sget-object v0, LX/1IA;->ASCII:LX/1IA;

    invoke-virtual {v0, p1}, LX/1IA;->matches(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1I9;->g:[B

    aget-byte v0, v0, p1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228423
    iget-object v0, p0, LX/1I9;->e:Ljava/lang/String;

    return-object v0
.end method
