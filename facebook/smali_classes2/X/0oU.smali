.class public LX/0oU;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141600
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 141601
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/quicklog/QuickPerformanceLogger;)LX/00u;
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 141602
    invoke-static {p0}, LX/00u;->a(Landroid/content/Context;)LX/00u;

    move-result-object v0

    .line 141603
    invoke-virtual {v0, p1}, LX/00u;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 141604
    return-object v0
.end method

.method public static a(LX/00u;)LX/0oW;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 141605
    new-instance v0, LX/0oV;

    invoke-direct {v0, p0}, LX/0oV;-><init>(LX/00u;)V

    return-object v0
.end method

.method public static a()LX/G5q;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 141606
    new-instance v0, LX/G5q;

    invoke-direct {v0}, LX/G5q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 141607
    return-void
.end method
