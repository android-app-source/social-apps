.class public LX/1by;
.super LX/1bz;
.source ""


# static fields
.field private static a:LX/1by;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281452
    const/4 v0, 0x0

    sput-object v0, LX/1by;->a:LX/1by;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 281458
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, LX/1bz;-><init>(Landroid/os/Handler;)V

    .line 281459
    return-void
.end method

.method public static b()LX/1by;
    .locals 1

    .prologue
    .line 281460
    sget-object v0, LX/1by;->a:LX/1by;

    if-nez v0, :cond_0

    .line 281461
    new-instance v0, LX/1by;

    invoke-direct {v0}, LX/1by;-><init>()V

    sput-object v0, LX/1by;->a:LX/1by;

    .line 281462
    :cond_0
    sget-object v0, LX/1by;->a:LX/1by;

    return-object v0
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 281453
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LX/1bz;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 281454
    if-eqz v0, :cond_0

    .line 281455
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 281456
    :goto_1
    return-void

    .line 281457
    :cond_0
    invoke-super {p0, p1}, LX/1bz;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
