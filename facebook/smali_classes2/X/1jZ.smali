.class public LX/1jZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1jZ;


# instance fields
.field public final a:LX/0qR;

.field private final b:LX/0ad;

.field private final c:LX/1ja;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2xl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0qR;LX/0ad;LX/1ja;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 300626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300627
    iput-object p1, p0, LX/1jZ;->a:LX/0qR;

    .line 300628
    iput-object p2, p0, LX/1jZ;->b:LX/0ad;

    .line 300629
    iput-object p3, p0, LX/1jZ;->c:LX/1ja;

    .line 300630
    new-instance v0, LX/1jb;

    invoke-direct {v0, p0}, LX/1jb;-><init>(LX/1jZ;)V

    iput-object v0, p0, LX/1jZ;->d:Ljava/util/Map;

    .line 300631
    return-void
.end method

.method public static a(LX/0QB;)LX/1jZ;
    .locals 6

    .prologue
    .line 300642
    sget-object v0, LX/1jZ;->e:LX/1jZ;

    if-nez v0, :cond_1

    .line 300643
    const-class v1, LX/1jZ;

    monitor-enter v1

    .line 300644
    :try_start_0
    sget-object v0, LX/1jZ;->e:LX/1jZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 300645
    if-eqz v2, :cond_0

    .line 300646
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 300647
    new-instance p0, LX/1jZ;

    invoke-static {v0}, LX/0qR;->a(LX/0QB;)LX/0qR;

    move-result-object v3

    check-cast v3, LX/0qR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    .line 300648
    new-instance v5, LX/1ja;

    invoke-direct {v5}, LX/1ja;-><init>()V

    .line 300649
    move-object v5, v5

    .line 300650
    move-object v5, v5

    .line 300651
    check-cast v5, LX/1ja;

    invoke-direct {p0, v3, v4, v5}, LX/1jZ;-><init>(LX/0qR;LX/0ad;LX/1ja;)V

    .line 300652
    move-object v0, p0

    .line 300653
    sput-object v0, LX/1jZ;->e:LX/1jZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300654
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 300655
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 300656
    :cond_1
    sget-object v0, LX/1jZ;->e:LX/1jZ;

    return-object v0

    .line 300657
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 300658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;
    .locals 3
    .param p0    # LX/1jZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 300632
    if-nez p1, :cond_0

    .line 300633
    const/4 v0, 0x0

    .line 300634
    :goto_0
    return-object v0

    .line 300635
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    .line 300636
    iget-object v1, p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v1, v1

    .line 300637
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 300638
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1jZ;->c(Ljava/lang/String;)LX/2xl;

    move-result-object v0

    goto :goto_0

    .line 300639
    :cond_2
    iget-object v2, p0, LX/1jZ;->a:LX/0qR;

    invoke-virtual {v2, v1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 300640
    if-nez v2, :cond_1

    .line 300641
    iget-object v2, p0, LX/1jZ;->a:LX/0qR;

    invoke-virtual {v2, v1, v0}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(LX/1jZ;Ljava/lang/String;)LX/2xl;
    .locals 2
    .param p0    # LX/1jZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 300619
    if-nez p1, :cond_1

    .line 300620
    :cond_0
    :goto_0
    return-object v0

    .line 300621
    :cond_1
    iget-object v1, p0, LX/1jZ;->a:LX/0qR;

    invoke-virtual {v1, p1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 300622
    if-eqz v1, :cond_0

    .line 300623
    monitor-enter p0

    .line 300624
    :try_start_0
    iget-object v0, p0, LX/1jZ;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xl;

    monitor-exit p0

    goto :goto_0

    .line 300625
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c(Ljava/lang/String;)LX/2xl;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 300659
    if-nez p1, :cond_0

    .line 300660
    const/4 v0, 0x0

    .line 300661
    :goto_0
    return-object v0

    .line 300662
    :cond_0
    monitor-enter p0

    .line 300663
    :try_start_0
    iget-object v0, p0, LX/1jZ;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xl;

    .line 300664
    if-nez v0, :cond_1

    .line 300665
    new-instance v0, LX/2xl;

    invoke-direct {v0}, LX/2xl;-><init>()V

    .line 300666
    iget-object v1, p0, LX/1jZ;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300667
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 300668
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;IZZ)V
    .locals 6
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;IZZ)V"
        }
    .end annotation

    .prologue
    .line 300600
    invoke-virtual {p0}, LX/1jZ;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 300601
    :cond_0
    return-void

    .line 300602
    :cond_1
    if-eqz p1, :cond_0

    .line 300603
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300604
    invoke-static {p0, v0}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object v3

    .line 300605
    if-eqz v3, :cond_2

    .line 300606
    invoke-virtual {v3}, LX/2xl;->l()V

    .line 300607
    invoke-virtual {v3, p2}, LX/2xl;->e(I)V

    .line 300608
    invoke-virtual {v3, p3}, LX/2xl;->h(Z)V

    .line 300609
    invoke-virtual {v3, p4}, LX/2xl;->i(Z)V

    .line 300610
    const-string v4, "Ad"

    .line 300611
    iget-object v5, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v5, v5

    .line 300612
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/2xl;->j(Z)V

    .line 300613
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-static {v4}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/2xl;->k(Z)V

    .line 300614
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v3, v4}, LX/2xl;->l(Z)V

    .line 300615
    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F()Z

    move-result v4

    invoke-virtual {v3, v4}, LX/2xl;->m(Z)V

    .line 300616
    iget-boolean v4, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->B:Z

    move v0, v4

    .line 300617
    invoke-virtual {v3, v0}, LX/2xl;->n(Z)V

    .line 300618
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/model/ClientFeedUnitEdge;IIIIZ)V
    .locals 4
    .param p1    # Lcom/facebook/feed/model/ClientFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 300581
    invoke-virtual {p0}, LX/1jZ;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 300582
    :cond_0
    :goto_0
    return-void

    .line 300583
    :cond_1
    invoke-static {p0, p1}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object v3

    .line 300584
    if-eqz v3, :cond_0

    .line 300585
    invoke-virtual {v3}, LX/2xl;->j()V

    .line 300586
    invoke-virtual {v3, p2}, LX/2xl;->a(I)V

    .line 300587
    invoke-virtual {v3, p3}, LX/2xl;->b(I)V

    .line 300588
    invoke-virtual {v3, p6}, LX/2xl;->a(Z)V

    .line 300589
    if-ge p3, p4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, LX/2xl;->b(Z)V

    .line 300590
    add-int/lit8 v0, p5, -0x1

    if-lt p3, v0, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, LX/2xl;->c(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 300591
    goto :goto_1

    :cond_3
    move v1, v2

    .line 300592
    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 300598
    iget-object v1, p0, LX/1jZ;->b:LX/0ad;

    sget v2, LX/0fe;->N:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v1

    .line 300599
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 300593
    invoke-virtual {p0}, LX/1jZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300594
    :goto_0
    return-void

    .line 300595
    :cond_0
    monitor-enter p0

    .line 300596
    :try_start_0
    iget-object v0, p0, LX/1jZ;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 300597
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
