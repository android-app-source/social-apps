.class public final LX/1Ei;
.super LX/1Ej;
.source ""

# interfaces
.implements LX/0Ri;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/1Ej",
        "<TK;TV;>;",
        "LX/0Ri",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private transient a:[LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient b:[LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public transient c:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient d:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public transient e:I

.field private transient f:I

.field public transient g:I

.field private transient h:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 220907
    invoke-direct {p0}, LX/1Ej;-><init>()V

    .line 220908
    invoke-direct {p0, p1}, LX/1Ei;->b(I)V

    .line 220909
    return-void
.end method

.method public static a()LX/1Ei;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/1Ei",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 220938
    const/16 v0, 0x10

    invoke-static {v0}, LX/1Ei;->a(I)LX/1Ei;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)LX/1Ei;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I)",
            "LX/1Ei",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 220937
    new-instance v0, LX/1Ei;

    invoke-direct {v0, p0}, LX/1Ei;-><init>(I)V

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 220916
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    .line 220917
    invoke-static {p2}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v2

    .line 220918
    invoke-static {p0, p1, v1}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v3

    .line 220919
    if-eqz v3, :cond_0

    iget v4, v3, LX/1Ek;->valueHash:I

    if-ne v2, v4, :cond_0

    iget-object v4, v3, LX/0P4;->value:Ljava/lang/Object;

    invoke-static {p2, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220920
    :goto_0
    return-object p2

    .line 220921
    :cond_0
    invoke-static {p0, p2, v2}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v4

    .line 220922
    if-eqz v4, :cond_1

    .line 220923
    if-eqz p3, :cond_2

    .line 220924
    invoke-static {p0, v4}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 220925
    :cond_1
    new-instance v4, LX/1Ek;

    invoke-direct {v4, p1, v1, p2, v2}, LX/1Ek;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 220926
    if-eqz v3, :cond_3

    .line 220927
    invoke-static {p0, v3}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 220928
    invoke-static {p0, v4, v3}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 220929
    iput-object v0, v3, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 220930
    iput-object v0, v3, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 220931
    invoke-direct {p0}, LX/1Ei;->e()V

    .line 220932
    iget-object p2, v3, LX/0P4;->value:Ljava/lang/Object;

    goto :goto_0

    .line 220933
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "value already present: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220934
    :cond_3
    invoke-static {p0, v4, v0}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 220935
    invoke-direct {p0}, LX/1Ei;->e()V

    move-object p2, v0

    .line 220936
    goto :goto_0
.end method

.method public static a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;
    .locals 2
    .param p0    # LX/1Ei;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 220910
    iget-object v0, p0, LX/1Ei;->a:[LX/1Ek;

    iget v1, p0, LX/1Ei;->f:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    .line 220911
    :goto_0
    if-eqz v0, :cond_1

    .line 220912
    iget v1, v0, LX/1Ek;->keyHash:I

    if-ne p2, v1, :cond_0

    iget-object v1, v0, LX/0P4;->key:Ljava/lang/Object;

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220913
    :goto_1
    return-object v0

    .line 220914
    :cond_0
    iget-object v0, v0, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    goto :goto_0

    .line 220915
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/1Ei;LX/1Ek;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ek",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 220884
    iget v0, p1, LX/1Ek;->keyHash:I

    iget v2, p0, LX/1Ei;->f:I

    and-int v3, v0, v2

    .line 220885
    iget-object v0, p0, LX/1Ei;->a:[LX/1Ek;

    aget-object v0, v0, v3

    move-object v2, v1

    .line 220886
    :goto_0
    if-ne v0, p1, :cond_1

    .line 220887
    if-nez v2, :cond_0

    .line 220888
    iget-object v0, p0, LX/1Ei;->a:[LX/1Ek;

    iget-object v2, p1, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    aput-object v2, v0, v3

    .line 220889
    :goto_1
    iget v0, p1, LX/1Ek;->valueHash:I

    iget v2, p0, LX/1Ei;->f:I

    and-int/2addr v2, v0

    .line 220890
    iget-object v0, p0, LX/1Ei;->b:[LX/1Ek;

    aget-object v0, v0, v2

    .line 220891
    :goto_2
    if-ne v0, p1, :cond_3

    .line 220892
    if-nez v1, :cond_2

    .line 220893
    iget-object v0, p0, LX/1Ei;->b:[LX/1Ek;

    iget-object v1, p1, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    aput-object v1, v0, v2

    .line 220894
    :goto_3
    iget-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    if-nez v0, :cond_4

    .line 220895
    iget-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iput-object v0, p0, LX/1Ei;->c:LX/1Ek;

    .line 220896
    :goto_4
    iget-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    if-nez v0, :cond_5

    .line 220897
    iget-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    iput-object v0, p0, LX/1Ei;->d:LX/1Ek;

    .line 220898
    :goto_5
    iget v0, p0, LX/1Ei;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1Ei;->e:I

    .line 220899
    iget v0, p0, LX/1Ei;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Ei;->g:I

    .line 220900
    return-void

    .line 220901
    :cond_0
    iget-object v0, p1, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    iput-object v0, v2, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    goto :goto_1

    .line 220902
    :cond_1
    iget-object v2, v0, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_0

    .line 220903
    :cond_2
    iget-object v0, p1, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    iput-object v0, v1, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    goto :goto_3

    .line 220904
    :cond_3
    iget-object v1, v0, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    .line 220905
    :cond_4
    iget-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    iget-object v1, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iput-object v1, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    goto :goto_4

    .line 220906
    :cond_5
    iget-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iget-object v1, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    iput-object v1, v0, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    goto :goto_5
.end method

.method public static a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V
    .locals 2
    .param p1    # LX/1Ek;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ek",
            "<TK;TV;>;",
            "LX/1Ek",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 220860
    iget v0, p1, LX/1Ek;->keyHash:I

    iget v1, p0, LX/1Ei;->f:I

    and-int/2addr v0, v1

    .line 220861
    iget-object v1, p0, LX/1Ei;->a:[LX/1Ek;

    aget-object v1, v1, v0

    iput-object v1, p1, LX/1Ek;->nextInKToVBucket:LX/1Ek;

    .line 220862
    iget-object v1, p0, LX/1Ei;->a:[LX/1Ek;

    aput-object p1, v1, v0

    .line 220863
    iget v0, p1, LX/1Ek;->valueHash:I

    iget v1, p0, LX/1Ei;->f:I

    and-int/2addr v0, v1

    .line 220864
    iget-object v1, p0, LX/1Ei;->b:[LX/1Ek;

    aget-object v1, v1, v0

    iput-object v1, p1, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    .line 220865
    iget-object v1, p0, LX/1Ei;->b:[LX/1Ek;

    aput-object p1, v1, v0

    .line 220866
    if-nez p2, :cond_1

    .line 220867
    iget-object v0, p0, LX/1Ei;->d:LX/1Ek;

    iput-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 220868
    const/4 v0, 0x0

    iput-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 220869
    iget-object v0, p0, LX/1Ei;->d:LX/1Ek;

    if-nez v0, :cond_0

    .line 220870
    iput-object p1, p0, LX/1Ei;->c:LX/1Ek;

    .line 220871
    :goto_0
    iput-object p1, p0, LX/1Ei;->d:LX/1Ek;

    .line 220872
    :goto_1
    iget v0, p0, LX/1Ei;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Ei;->e:I

    .line 220873
    iget v0, p0, LX/1Ei;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Ei;->g:I

    .line 220874
    return-void

    .line 220875
    :cond_0
    iget-object v0, p0, LX/1Ei;->d:LX/1Ek;

    iput-object p1, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    goto :goto_0

    .line 220876
    :cond_1
    iget-object v0, p2, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    iput-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 220877
    iget-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    if-nez v0, :cond_2

    .line 220878
    iput-object p1, p0, LX/1Ei;->c:LX/1Ek;

    .line 220879
    :goto_2
    iget-object v0, p2, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iput-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 220880
    iget-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    if-nez v0, :cond_3

    .line 220881
    iput-object p1, p0, LX/1Ei;->d:LX/1Ek;

    goto :goto_1

    .line 220882
    :cond_2
    iget-object v0, p1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    iput-object p1, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    goto :goto_2

    .line 220883
    :cond_3
    iget-object v0, p1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iput-object p1, v0, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    goto :goto_1
.end method

.method public static b(LX/1Ei;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 6
    .param p0    # LX/1Ei;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TK;Z)TK;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 220841
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v0

    .line 220842
    invoke-static {p2}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    .line 220843
    invoke-static {p0, p1, v0}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v2

    .line 220844
    if-eqz v2, :cond_0

    iget v3, v2, LX/1Ek;->keyHash:I

    if-ne v1, v3, :cond_0

    iget-object v3, v2, LX/0P4;->key:Ljava/lang/Object;

    invoke-static {p2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220845
    :goto_0
    return-object p2

    .line 220846
    :cond_0
    invoke-static {p0, p2, v1}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v3

    .line 220847
    if-eqz v3, :cond_1

    .line 220848
    if-eqz p3, :cond_4

    .line 220849
    invoke-static {p0, v3}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 220850
    :cond_1
    if-eqz v2, :cond_2

    .line 220851
    invoke-static {p0, v2}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 220852
    :cond_2
    new-instance v4, LX/1Ek;

    invoke-direct {v4, p2, v1, p1, v0}, LX/1Ek;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 220853
    invoke-static {p0, v4, v3}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 220854
    if-eqz v3, :cond_3

    .line 220855
    iput-object v5, v3, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 220856
    iput-object v5, v3, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 220857
    :cond_3
    invoke-direct {p0}, LX/1Ei;->e()V

    .line 220858
    invoke-static {v2}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_0

    .line 220859
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "value already present: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 220829
    const-string v0, "expectedSize"

    invoke-static {p1, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 220830
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p1, v0, v1}, LX/0PC;->a(ID)I

    move-result v0

    .line 220831
    new-array v1, v0, [LX/1Ek;

    move-object v1, v1

    .line 220832
    iput-object v1, p0, LX/1Ei;->a:[LX/1Ek;

    .line 220833
    new-array v1, v0, [LX/1Ek;

    move-object v1, v1

    .line 220834
    iput-object v1, p0, LX/1Ei;->b:[LX/1Ek;

    .line 220835
    iput-object v3, p0, LX/1Ei;->c:LX/1Ek;

    .line 220836
    iput-object v3, p0, LX/1Ei;->d:LX/1Ek;

    .line 220837
    iput v2, p0, LX/1Ei;->e:I

    .line 220838
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1Ei;->f:I

    .line 220839
    iput v2, p0, LX/1Ei;->g:I

    .line 220840
    return-void
.end method

.method public static b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;
    .locals 2
    .param p0    # LX/1Ei;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 220823
    iget-object v0, p0, LX/1Ei;->b:[LX/1Ek;

    iget v1, p0, LX/1Ei;->f:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    .line 220824
    :goto_0
    if-eqz v0, :cond_1

    .line 220825
    iget v1, v0, LX/1Ek;->valueHash:I

    if-ne p2, v1, :cond_0

    iget-object v1, v0, LX/0P4;->value:Ljava/lang/Object;

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220826
    :goto_1
    return-object v0

    .line 220827
    :cond_0
    iget-object v0, v0, LX/1Ek;->nextInVToKBucket:LX/1Ek;

    goto :goto_0

    .line 220828
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    .line 220808
    iget-object v0, p0, LX/1Ei;->a:[LX/1Ek;

    .line 220809
    iget v1, p0, LX/1Ei;->e:I

    array-length v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v2, v4, v5}, LX/0PC;->a(IID)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220810
    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    .line 220811
    new-array v1, v0, [LX/1Ek;

    move-object v1, v1

    .line 220812
    iput-object v1, p0, LX/1Ei;->a:[LX/1Ek;

    .line 220813
    new-array v1, v0, [LX/1Ek;

    move-object v1, v1

    .line 220814
    iput-object v1, p0, LX/1Ei;->b:[LX/1Ek;

    .line 220815
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1Ei;->f:I

    .line 220816
    const/4 v0, 0x0

    iput v0, p0, LX/1Ei;->e:I

    .line 220817
    iget-object v0, p0, LX/1Ei;->c:LX/1Ek;

    .line 220818
    :goto_0
    if-eqz v0, :cond_0

    .line 220819
    invoke-static {p0, v0, v0}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 220820
    iget-object v0, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    goto :goto_0

    .line 220821
    :cond_0
    iget v0, p0, LX/1Ei;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Ei;->g:I

    .line 220822
    :cond_1
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 220803
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 220804
    const/16 v0, 0x10

    invoke-direct {p0, v0}, LX/1Ei;->b(I)V

    .line 220805
    invoke-static {p1}, LX/50X;->a(Ljava/io/ObjectInputStream;)I

    move-result v0

    .line 220806
    invoke-static {p0, p1, v0}, LX/50X;->a(Ljava/util/Map;Ljava/io/ObjectInputStream;I)V

    .line 220807
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 220939
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 220940
    invoke-static {p0, p1}, LX/50X;->a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V

    .line 220941
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 220776
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/1Ei;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a_()LX/0Ri;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ri",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 220777
    iget-object v0, p0, LX/1Ei;->h:LX/0Ri;

    if-nez v0, :cond_0

    new-instance v0, LX/2cM;

    invoke-direct {v0, p0}, LX/2cM;-><init>(LX/1Ei;)V

    iput-object v0, p0, LX/1Ei;->h:LX/0Ri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Ei;->h:LX/0Ri;

    goto :goto_0
.end method

.method public final b_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 220778
    invoke-virtual {p0}, LX/1Ei;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0}, LX/0Ri;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220779
    const/4 v0, 0x0

    iput v0, p0, LX/1Ei;->e:I

    .line 220780
    iget-object v0, p0, LX/1Ei;->a:[LX/1Ek;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220781
    iget-object v0, p0, LX/1Ei;->b:[LX/1Ek;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220782
    iput-object v1, p0, LX/1Ei;->c:LX/1Ek;

    .line 220783
    iput-object v1, p0, LX/1Ei;->d:LX/1Ek;

    .line 220784
    iget v0, p0, LX/1Ei;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Ei;->g:I

    .line 220785
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220786
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, p1, v0}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220787
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, p1, v0}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 220788
    new-instance v0, LX/4xr;

    invoke-direct {v0, p0}, LX/4xr;-><init>(LX/1Ei;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 220789
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, p1, v0}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    .line 220790
    if-nez v0, :cond_0

    const/4 p0, 0x0

    :goto_0
    move-object v0, p0

    .line 220791
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 220802
    new-instance v0, LX/4xz;

    invoke-direct {v0, p0}, LX/4xz;-><init>(LX/1Ei;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 220792
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1Ei;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 220793
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-static {p0, p1, v1}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v1

    .line 220794
    if-nez v1, :cond_0

    .line 220795
    :goto_0
    return-object v0

    .line 220796
    :cond_0
    invoke-static {p0, v1}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 220797
    iput-object v0, v1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 220798
    iput-object v0, v1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 220799
    iget-object v0, v1, LX/0P4;->value:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 220800
    iget v0, p0, LX/1Ei;->e:I

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 220801
    invoke-virtual {p0}, LX/1Ei;->b_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
