.class public LX/1GD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GE;
.implements LX/1GF;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Runtime;

.field private static volatile j:LX/1GD;


# instance fields
.field private final b:LX/1GH;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final d:LX/0SG;

.field private e:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<**>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<**>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225116
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, LX/1GD;->a:Ljava/lang/Runtime;

    return-void
.end method

.method public constructor <init>(LX/1GG;LX/0SG;LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 225070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225071
    const-string v0, "image_cache_stats_counter"

    .line 225072
    new-instance v3, LX/1GH;

    invoke-static {p1}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v2

    check-cast v2, LX/2WA;

    invoke-direct {v3, v2, v0}, LX/1GH;-><init>(LX/2WA;Ljava/lang/String;)V

    .line 225073
    move-object v0, v3

    .line 225074
    iput-object v0, p0, LX/1GD;->b:LX/1GH;

    .line 225075
    iput-object p2, p0, LX/1GD;->d:LX/0SG;

    .line 225076
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/1GD;->c:Ljava/util/List;

    .line 225077
    sget-short v0, LX/1FD;->n:S

    invoke-interface {p3, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225078
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    .line 225079
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1GD;->i:Z

    .line 225080
    :goto_0
    return-void

    .line 225081
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    .line 225082
    iput-boolean v1, p0, LX/1GD;->i:Z

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1GD;
    .locals 6

    .prologue
    .line 225083
    sget-object v0, LX/1GD;->j:LX/1GD;

    if-nez v0, :cond_1

    .line 225084
    const-class v1, LX/1GD;

    monitor-enter v1

    .line 225085
    :try_start_0
    sget-object v0, LX/1GD;->j:LX/1GD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225086
    if-eqz v2, :cond_0

    .line 225087
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225088
    new-instance p0, LX/1GD;

    const-class v3, LX/1GG;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1GG;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/1GD;-><init>(LX/1GG;LX/0SG;LX/0ad;)V

    .line 225089
    move-object v0, p0

    .line 225090
    sput-object v0, LX/1GD;->j:LX/1GD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225093
    :cond_1
    sget-object v0, LX/1GD;->j:LX/1GD;

    return-object v0

    .line 225094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized d(LX/1bh;)V
    .locals 10

    .prologue
    .line 225096
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1GD;->i:Z

    if-eqz v0, :cond_1

    .line 225097
    iget-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 225098
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 225099
    :cond_0
    invoke-static {p1}, LX/1gD;->b(LX/1bh;)Ljava/lang/String;

    move-result-object v0

    .line 225100
    iget-object v4, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 225101
    iget-object v4, p0, LX/1GD;->h:Ljava/util/Map;

    new-instance v5, LX/3rL;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225102
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 225103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 225104
    :cond_2
    iget-object v5, p0, LX/1GD;->h:Ljava/util/Map;

    new-instance v6, LX/3rL;

    iget-object v4, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3rL;

    iget-object v4, v4, LX/3rL;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v7, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v6, v4, v7}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 225105
    monitor-enter p0

    .line 225106
    :try_start_0
    iget-object v0, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1GD;->e:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 225107
    monitor-exit p0

    .line 225108
    :goto_0
    return-void

    .line 225109
    :cond_0
    iget-object v0, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1GD;->e:J

    .line 225110
    invoke-direct {p0}, LX/1GD;->l()V

    .line 225111
    invoke-direct {p0}, LX/1GD;->m()V

    .line 225112
    invoke-direct {p0}, LX/1GD;->n()V

    .line 225113
    iget-object v0, p0, LX/1GD;->c:Ljava/util/List;

    iget-object v1, p0, LX/1GD;->b:LX/1GH;

    invoke-virtual {v1}, LX/0Vx;->c()LX/0lF;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225114
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    invoke-virtual {v0}, LX/0Vx;->W_()V

    .line 225115
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized l()V
    .locals 4

    .prologue
    .line 225117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1GD;->f:LX/1Fg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 225118
    :goto_0
    monitor-exit p0

    return-void

    .line 225119
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_memory_cache_entries"

    iget-object v2, p0, LX/1GD;->f:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225120
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_memory_cache_size"

    iget-object v2, p0, LX/1GD;->f:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->b()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225121
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_memory_cache_lru_entries"

    iget-object v2, p0, LX/1GD;->f:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->c()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225122
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_memory_cache_lru_size"

    iget-object v2, p0, LX/1GD;->f:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->d()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 225123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 4

    .prologue
    .line 225124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1GD;->g:LX/1Fg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 225125
    :goto_0
    monitor-exit p0

    return-void

    .line 225126
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_entries"

    iget-object v2, p0, LX/1GD;->g:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225127
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_size"

    iget-object v2, p0, LX/1GD;->g:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->b()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225128
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_lru_entries"

    iget-object v2, p0, LX/1GD;->g:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->c()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 225129
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_lru_size"

    iget-object v2, p0, LX/1GD;->g:LX/1Fg;

    invoke-virtual {v2}, LX/1Fg;->d()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 225130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized n()V
    .locals 6

    .prologue
    .line 225135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_usage"

    sget-object v2, LX/1GD;->a:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    sget-object v4, LX/1GD;->a:Ljava/lang/Runtime;

    invoke-virtual {v4}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->b(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225136
    monitor-exit p0

    return-void

    .line 225137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 225131
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1GD;->i:Z

    if-eqz v0, :cond_0

    .line 225132
    iget-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225133
    :cond_0
    monitor-exit p0

    return-void

    .line 225134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1Fg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fg",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 225152
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1GD;->f:LX/1Fg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225153
    monitor-exit p0

    return-void

    .line 225154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/1bh;)V
    .locals 4

    .prologue
    .line 225147
    invoke-direct {p0, p1}, LX/1GD;->d(LX/1bh;)V

    .line 225148
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225149
    monitor-enter p0

    .line 225150
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_cache_hit"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225151
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/1gC;)V
    .locals 0

    .prologue
    .line 225155
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 225142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1GD;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225143
    const-string v0, "image_cache_stats_counter"

    iget-object v1, p0, LX/1GD;->c:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 225144
    iget-object v0, p0, LX/1GD;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225145
    :cond_0
    monitor-exit p0

    return-void

    .line 225146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 225138
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225139
    monitor-enter p0

    .line 225140
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_cache_put"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225141
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b(LX/1Fg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fg",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 225062
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1GD;->g:LX/1Fg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225063
    monitor-exit p0

    return-void

    .line 225064
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/1bh;)V
    .locals 4

    .prologue
    .line 225065
    invoke-direct {p0, p1}, LX/1GD;->d(LX/1bh;)V

    .line 225066
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225067
    monitor-enter p0

    .line 225068
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_hit"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225069
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/1gC;)V
    .locals 0

    .prologue
    .line 225038
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 225034
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225035
    monitor-enter p0

    .line 225036
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "bitmap_cache_miss"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225037
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(LX/1bh;)V
    .locals 4

    .prologue
    .line 225029
    invoke-direct {p0, p1}, LX/1GD;->d(LX/1bh;)V

    .line 225030
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225031
    monitor-enter p0

    .line 225032
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "staging_area_hit"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225033
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(LX/1gC;)V
    .locals 0

    .prologue
    .line 225028
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 225016
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225017
    monitor-enter p0

    .line 225018
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_put"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225019
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d(LX/1gC;)V
    .locals 6

    .prologue
    .line 225020
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1GD;->i:Z

    if-eqz v0, :cond_0

    .line 225021
    iget-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/3rL;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/1GD;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225022
    :cond_0
    monitor-exit p0

    return-void

    .line 225023
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 225024
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225025
    monitor-enter p0

    .line 225026
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "memory_cache_miss"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225027
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e(LX/1gC;)V
    .locals 0

    .prologue
    .line 225039
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 225040
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225041
    monitor-enter p0

    .line 225042
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "staging_area_miss"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225043
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f(LX/1gC;)V
    .locals 0

    .prologue
    .line 225044
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 225058
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225059
    monitor-enter p0

    .line 225060
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "disk_cache_hit"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225061
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized g(LX/1gC;)V
    .locals 2

    .prologue
    .line 225045
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1GD;->i:Z

    if-eqz v0, :cond_0

    .line 225046
    iget-object v0, p0, LX/1GD;->h:Ljava/util/Map;

    invoke-interface {p1}, LX/1gC;->a()LX/1bh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225047
    :cond_0
    monitor-exit p0

    return-void

    .line 225048
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 225049
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225050
    monitor-enter p0

    .line 225051
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "disk_cache_miss"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225052
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 225053
    invoke-direct {p0}, LX/1GD;->k()V

    .line 225054
    monitor-enter p0

    .line 225055
    :try_start_0
    iget-object v0, p0, LX/1GD;->b:LX/1GH;

    const-string v1, "disk_cache_get_fail"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 225056
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized j()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 225057
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1GD;->h:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
