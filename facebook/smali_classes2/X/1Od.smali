.class public final LX/1Od;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:Landroid/support/v7/widget/RecyclerView;

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:LX/1YF;

.field public h:LX/1PB;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 241220
    iput-object p1, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    .line 241222
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    .line 241223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    .line 241224
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1Od;->e:Ljava/util/List;

    .line 241225
    const/4 v0, 0x2

    iput v0, p0, LX/1Od;->f:I

    return-void
.end method

.method private a(JIZ)LX/1a1;
    .locals 7

    .prologue
    .line 241226
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241227
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 241228
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241229
    iget-wide v5, v0, LX/1a1;->d:J

    move-wide v2, v5

    .line 241230
    cmp-long v2, v2, p1

    if-nez v2, :cond_2

    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v2

    if-nez v2, :cond_2

    .line 241231
    iget v2, v0, LX/1a1;->e:I

    move v2, v2

    .line 241232
    if-ne p3, v2, :cond_1

    .line 241233
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, LX/1a1;->b(I)V

    .line 241234
    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241235
    iget-object v1, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241236
    iget-boolean v2, v1, LX/1Ok;->k:Z

    move v1, v2

    .line 241237
    if-nez v1, :cond_0

    .line 241238
    const/4 v1, 0x2

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, LX/1a1;->a(II)V

    .line 241239
    :cond_0
    :goto_1
    return-object v0

    .line 241240
    :cond_1
    if-nez p4, :cond_2

    .line 241241
    iget-object v2, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241242
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v0, LX/1a1;->a:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 241243
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/1Od;->b(Landroid/view/View;)V

    .line 241244
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241245
    :cond_3
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241246
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_6

    .line 241247
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241248
    iget-wide v5, v0, LX/1a1;->d:J

    move-wide v2, v5

    .line 241249
    cmp-long v2, v2, p1

    if-nez v2, :cond_5

    .line 241250
    iget v2, v0, LX/1a1;->e:I

    move v2, v2

    .line 241251
    if-ne p3, v2, :cond_4

    .line 241252
    if-nez p4, :cond_0

    .line 241253
    iget-object v2, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 241254
    :cond_4
    if-nez p4, :cond_5

    .line 241255
    invoke-direct {p0, v1}, LX/1Od;->e(I)V

    .line 241256
    :cond_5
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 241257
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IZ)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 241258
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0}, LX/1Ok;->e()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 241259
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Item count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v2}, LX/1Ok;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241260
    :cond_1
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241261
    iget-boolean v4, v0, LX/1Ok;->k:Z

    move v0, v4

    .line 241262
    if-eqz v0, :cond_16

    .line 241263
    invoke-direct {p0, p1}, LX/1Od;->f(I)LX/1a1;

    move-result-object v4

    .line 241264
    if-eqz v4, :cond_5

    move v0, v1

    :goto_0
    move-object v8, v4

    move v4, v0

    move-object v0, v8

    .line 241265
    :goto_1
    if-nez v0, :cond_15

    .line 241266
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, LX/1Od;->b(IIZ)LX/1a1;

    move-result-object v0

    .line 241267
    if-eqz v0, :cond_15

    .line 241268
    invoke-direct {p0, v0}, LX/1Od;->c(LX/1a1;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 241269
    if-nez p2, :cond_3

    .line 241270
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, LX/1a1;->b(I)V

    .line 241271
    invoke-virtual {v0}, LX/1a1;->h()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 241272
    iget-object v5, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v5, v6, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 241273
    invoke-virtual {v0}, LX/1a1;->i()V

    .line 241274
    :cond_2
    :goto_2
    invoke-virtual {p0, v0}, LX/1Od;->a(LX/1a1;)V

    :cond_3
    move-object v0, v3

    move v3, v4

    .line 241275
    :goto_3
    if-nez v0, :cond_14

    .line 241276
    iget-object v4, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v4, p1}, LX/1On;->a(I)I

    move-result v4

    .line 241277
    if-ltz v4, :cond_4

    iget-object v5, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v5}, LX/1OM;->ij_()I

    move-result v5

    if-lt v4, v5, :cond_8

    .line 241278
    :cond_4
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistency detected. Invalid item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(offset:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v2}, LX/1Ok;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    .line 241279
    goto :goto_0

    .line 241280
    :cond_6
    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 241281
    invoke-virtual {v0}, LX/1a1;->k()V

    goto :goto_2

    :cond_7
    move v3, v1

    .line 241282
    goto :goto_3

    .line 241283
    :cond_8
    iget-object v5, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v5, v4}, LX/1OM;->getItemViewType(I)I

    move-result v5

    .line 241284
    iget-object v6, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v6}, LX/1OM;->eC_()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 241285
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0, v4}, LX/1OM;->C_(I)J

    move-result-wide v6

    invoke-direct {p0, v6, v7, v5, p2}, LX/1Od;->a(JIZ)LX/1a1;

    move-result-object v0

    .line 241286
    if-eqz v0, :cond_9

    .line 241287
    iput v4, v0, LX/1a1;->b:I

    move v3, v1

    .line 241288
    :cond_9
    if-nez v0, :cond_b

    iget-object v4, p0, LX/1Od;->h:LX/1PB;

    if-eqz v4, :cond_b

    .line 241289
    iget-object v4, p0, LX/1Od;->h:LX/1PB;

    invoke-virtual {v4, p1, v5}, LX/1PB;->a(II)Landroid/view/View;

    move-result-object v4

    .line 241290
    if-eqz v4, :cond_b

    .line 241291
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 241292
    if-nez v0, :cond_a

    .line 241293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getViewForPositionAndType returned a view which does not have a ViewHolder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241294
    :cond_a
    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 241295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241296
    :cond_b
    if-nez v0, :cond_c

    .line 241297
    invoke-virtual {p0}, LX/1Od;->e()LX/1YF;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1YF;->a(I)LX/1a1;

    move-result-object v0

    .line 241298
    if-eqz v0, :cond_c

    .line 241299
    invoke-virtual {v0}, LX/1a1;->u()V

    .line 241300
    sget-boolean v4, Landroid/support/v7/widget/RecyclerView;->h:Z

    if-eqz v4, :cond_c

    .line 241301
    invoke-direct {p0, v0}, LX/1Od;->d(LX/1a1;)V

    .line 241302
    :cond_c
    if-nez v0, :cond_14

    .line 241303
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget-object v4, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4, v5}, LX/1OM;->b(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    move v4, v3

    move-object v3, v0

    .line 241304
    :goto_4
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241305
    iget-boolean v5, v0, LX/1Ok;->k:Z

    move v0, v5

    .line 241306
    if-eqz v0, :cond_d

    invoke-virtual {v3}, LX/1a1;->p()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 241307
    iput p1, v3, LX/1a1;->f:I

    move v5, v2

    .line 241308
    :goto_5
    iget-object v0, v3, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 241309
    if-nez v0, :cond_10

    .line 241310
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 241311
    iget-object v6, v3, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241312
    :goto_6
    iput-object v3, v0, LX/1a3;->a:LX/1a1;

    .line 241313
    if-eqz v4, :cond_12

    if-eqz v5, :cond_12

    :goto_7
    iput-boolean v1, v0, LX/1a3;->d:Z

    .line 241314
    iget-object v0, v3, LX/1a1;->a:Landroid/view/View;

    return-object v0

    .line 241315
    :cond_d
    invoke-virtual {v3}, LX/1a1;->p()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, LX/1a1;->n()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {v3}, LX/1a1;->m()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 241316
    :cond_e
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0, p1}, LX/1On;->a(I)I

    move-result v0

    .line 241317
    iget-object v5, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iput-object v5, v3, LX/1a1;->k:Landroid/support/v7/widget/RecyclerView;

    .line 241318
    iget-object v5, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v5, v3, v0}, LX/1OM;->b(LX/1a1;I)V

    .line 241319
    iget-object v0, v3, LX/1a1;->a:Landroid/view/View;

    invoke-direct {p0, v0}, LX/1Od;->d(Landroid/view/View;)V

    .line 241320
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241321
    iget-boolean v5, v0, LX/1Ok;->k:Z

    move v0, v5

    .line 241322
    if-eqz v0, :cond_f

    .line 241323
    iput p1, v3, LX/1a1;->f:I

    :cond_f
    move v5, v1

    goto :goto_5

    .line 241324
    :cond_10
    iget-object v6, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v6

    if-nez v6, :cond_11

    .line 241325
    iget-object v6, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 241326
    iget-object v6, v3, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 241327
    :cond_11
    check-cast v0, LX/1a3;

    goto :goto_6

    :cond_12
    move v1, v2

    .line 241328
    goto :goto_7

    :cond_13
    move v5, v2

    goto :goto_5

    :cond_14
    move v4, v3

    move-object v3, v0

    goto/16 :goto_4

    :cond_15
    move v3, v4

    goto/16 :goto_3

    :cond_16
    move-object v0, v3

    move v4, v2

    goto/16 :goto_1
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 241329
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 241330
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 241331
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 241332
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, LX/1Od;->a(Landroid/view/ViewGroup;Z)V

    .line 241333
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241334
    :cond_1
    if-nez p2, :cond_2

    .line 241335
    :goto_1
    return-void

    .line 241336
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 241337
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 241338
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 241339
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    .line 241340
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 241341
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private b(IIZ)LX/1a1;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 241342
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 241343
    :goto_0
    if-ge v2, v3, :cond_1

    .line 241344
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241345
    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v4

    if-ne v4, p1, :cond_6

    invoke-virtual {v0}, LX/1a1;->m()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v4, v4, LX/1Ok;->k:Z

    if-nez v4, :cond_0

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v4

    if-nez v4, :cond_6

    .line 241346
    :cond_0
    const/4 v2, -0x1

    if-eq p2, v2, :cond_5

    .line 241347
    iget v2, v0, LX/1a1;->e:I

    move v2, v2

    .line 241348
    if-eq v2, p2, :cond_5

    .line 241349
    const-string v2, "RecyclerView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Scrap view for position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isn\'t dirty but has wrong view type! (found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 241350
    iget v4, v0, LX/1a1;->e:I

    move v0, v4

    .line 241351
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " but expected "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241352
    :cond_1
    if-nez p3, :cond_3

    .line 241353
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    .line 241354
    iget-object v2, v0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 241355
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_a

    .line 241356
    iget-object v2, v0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 241357
    iget-object v5, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v5, v2}, LX/1Ou;->b(Landroid/view/View;)LX/1a1;

    move-result-object v5

    .line 241358
    invoke-virtual {v5}, LX/1a1;->d()I

    move-result v6

    if-ne v6, p1, :cond_9

    invoke-virtual {v5}, LX/1a1;->m()Z

    move-result v6

    if-nez v6, :cond_9

    const/4 v6, -0x1

    if-eq p2, v6, :cond_2

    .line 241359
    iget v6, v5, LX/1a1;->e:I

    move v5, v6

    .line 241360
    if-ne v5, p2, :cond_9

    .line 241361
    :cond_2
    :goto_2
    move-object v0, v2

    .line 241362
    if-eqz v0, :cond_3

    .line 241363
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget-object v3, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1Of;->c(LX/1a1;)V

    .line 241364
    :cond_3
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241365
    :goto_3
    if-ge v1, v2, :cond_8

    .line 241366
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241367
    invoke-virtual {v0}, LX/1a1;->m()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    if-ne v3, p1, :cond_7

    .line 241368
    if-nez p3, :cond_4

    .line 241369
    iget-object v2, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241370
    :cond_4
    :goto_4
    return-object v0

    .line 241371
    :cond_5
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, LX/1a1;->b(I)V

    goto :goto_4

    .line 241372
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 241373
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 241374
    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 241375
    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 241376
    :cond_a
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private c(LX/1a1;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 241377
    invoke-virtual {p1}, LX/1a1;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241378
    :cond_0
    :goto_0
    return v0

    .line 241379
    :cond_1
    iget v2, p1, LX/1a1;->b:I

    if-ltz v2, :cond_2

    iget v2, p1, LX/1a1;->b:I

    iget-object v3, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 241380
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistency detected. Invalid view holder adapter position"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241381
    :cond_3
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241382
    iget-boolean v3, v2, LX/1Ok;->k:Z

    move v2, v3

    .line 241383
    if-nez v2, :cond_4

    .line 241384
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget v3, p1, LX/1a1;->b:I

    invoke-virtual {v2, v3}, LX/1OM;->getItemViewType(I)I

    move-result v2

    .line 241385
    iget v3, p1, LX/1a1;->e:I

    move v3, v3

    .line 241386
    if-eq v2, v3, :cond_4

    move v0, v1

    .line 241387
    goto :goto_0

    .line 241388
    :cond_4
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v2}, LX/1OM;->eC_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241389
    iget-wide v6, p1, LX/1a1;->d:J

    move-wide v2, v6

    .line 241390
    iget-object v4, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget v5, p1, LX/1a1;->b:I

    invoke-virtual {v4, v5}, LX/1OM;->C_(I)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private d(LX/1a1;)V
    .locals 2

    .prologue
    .line 241391
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 241392
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/1Od;->a(Landroid/view/ViewGroup;Z)V

    .line 241393
    :cond_0
    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 241394
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241395
    invoke-static {p1}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 241396
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;I)V

    .line 241397
    :cond_0
    invoke-static {p1}, LX/0vv;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 241398
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->af:LX/1Ow;

    .line 241399
    iget-object p0, v0, LX/1Ow;->c:LX/0vn;

    move-object v0, p0

    .line 241400
    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 241401
    :cond_1
    return-void
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 241402
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241403
    invoke-direct {p0, v0}, LX/1Od;->e(LX/1a1;)V

    .line 241404
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241405
    return-void
.end method

.method private e(LX/1a1;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 241406
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 241407
    invoke-direct {p0, p1}, LX/1Od;->f(LX/1a1;)V

    .line 241408
    iput-object v1, p1, LX/1a1;->k:Landroid/support/v7/widget/RecyclerView;

    .line 241409
    invoke-virtual {p0}, LX/1Od;->e()LX/1YF;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1YF;->a(LX/1a1;)V

    .line 241410
    return-void
.end method

.method private f(I)LX/1a1;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x20

    const/4 v2, 0x0

    .line 241411
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v0, v1

    .line 241412
    :goto_0
    return-object v0

    :cond_1
    move v3, v2

    .line 241413
    :goto_1
    if-ge v3, v4, :cond_3

    .line 241414
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241415
    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 241416
    invoke-virtual {v0, v10}, LX/1a1;->b(I)V

    goto :goto_0

    .line 241417
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 241418
    :cond_3
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 241419
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0, p1}, LX/1On;->a(I)I

    move-result v0

    .line 241420
    if-lez v0, :cond_5

    iget-object v3, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 241421
    iget-object v3, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v3, v0}, LX/1OM;->C_(I)J

    move-result-wide v6

    .line 241422
    :goto_2
    if-ge v2, v4, :cond_5

    .line 241423
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241424
    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v3

    if-nez v3, :cond_4

    .line 241425
    iget-wide v11, v0, LX/1a1;->d:J

    move-wide v8, v11

    .line 241426
    cmp-long v3, v8, v6

    if-nez v3, :cond_4

    .line 241427
    invoke-virtual {v0, v10}, LX/1a1;->b(I)V

    goto :goto_0

    .line 241428
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 241429
    goto :goto_0
.end method

.method private f(LX/1a1;)V
    .locals 1

    .prologue
    .line 241430
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->q:LX/1OU;

    if-eqz v0, :cond_0

    .line 241431
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->q:LX/1OU;

    invoke-interface {v0, p1}, LX/1OU;->a(LX/1a1;)V

    .line 241432
    :cond_0
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_1

    .line 241433
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 241434
    :cond_1
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    if-eqz v0, :cond_2

    .line 241435
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p1}, LX/1Ok;->a(LX/1a1;)V

    .line 241436
    :cond_2
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 241437
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241438
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 241439
    invoke-direct {p0, v0}, LX/1Od;->e(I)V

    .line 241440
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 241441
    :cond_0
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241442
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 241443
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241444
    invoke-direct {p0}, LX/1Od;->j()V

    .line 241445
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 241446
    iput p1, p0, LX/1Od;->f:I

    .line 241447
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 241448
    invoke-direct {p0, v0}, LX/1Od;->e(I)V

    .line 241449
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 241450
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 241451
    if-ge p1, p2, :cond_1

    .line 241452
    const/4 v0, -0x1

    move v1, v0

    move v2, p2

    move v3, p1

    .line 241453
    :goto_0
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v5

    .line 241454
    :goto_1
    if-ge v4, v6, :cond_3

    .line 241455
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241456
    if-eqz v0, :cond_0

    iget v7, v0, LX/1a1;->b:I

    if-lt v7, v3, :cond_0

    iget v7, v0, LX/1a1;->b:I

    if-gt v7, v2, :cond_0

    .line 241457
    iget v7, v0, LX/1a1;->b:I

    if-ne v7, p1, :cond_2

    .line 241458
    sub-int v7, p2, p1

    invoke-virtual {v0, v7, v5}, LX/1a1;->a(IZ)V

    .line 241459
    :cond_0
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 241460
    :cond_1
    const/4 v0, 0x1

    move v1, v0

    move v2, p1

    move v3, p2

    goto :goto_0

    .line 241461
    :cond_2
    invoke-virtual {v0, v1, v5}, LX/1a1;->a(IZ)V

    goto :goto_2

    .line 241462
    :cond_3
    return-void
.end method

.method public final a(IIZ)V
    .locals 4

    .prologue
    .line 241463
    add-int v2, p1, p2

    .line 241464
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241465
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 241466
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241467
    if-eqz v0, :cond_0

    .line 241468
    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    if-lt v3, v2, :cond_1

    .line 241469
    neg-int v3, p2

    invoke-virtual {v0, v3, p3}, LX/1a1;->a(IZ)V

    .line 241470
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241471
    :cond_1
    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    if-lt v3, p1, :cond_0

    .line 241472
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, LX/1a1;->b(I)V

    .line 241473
    invoke-direct {p0, v1}, LX/1Od;->e(I)V

    goto :goto_1

    .line 241474
    :cond_2
    return-void
.end method

.method public final a(LX/1OM;LX/1OM;Z)V
    .locals 1

    .prologue
    .line 241217
    invoke-virtual {p0}, LX/1Od;->a()V

    .line 241218
    invoke-virtual {p0}, LX/1Od;->e()LX/1YF;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/1YF;->a(LX/1OM;LX/1OM;Z)V

    .line 241219
    return-void
.end method

.method public final a(LX/1YF;)V
    .locals 2

    .prologue
    .line 241084
    iget-object v0, p0, LX/1Od;->g:LX/1YF;

    if-eqz v0, :cond_0

    .line 241085
    iget-object v0, p0, LX/1Od;->g:LX/1YF;

    invoke-virtual {v0}, LX/1YF;->b()V

    .line 241086
    :cond_0
    iput-object p1, p0, LX/1Od;->g:LX/1YF;

    .line 241087
    if-eqz p1, :cond_1

    .line 241088
    iget-object v0, p0, LX/1Od;->g:LX/1YF;

    .line 241089
    invoke-virtual {v0}, LX/1YF;->a()V

    .line 241090
    :cond_1
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241091
    invoke-virtual {p1}, LX/1a1;->h()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 241092
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Scrapped or attached views may not be recycled. isScrap:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1a1;->h()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isAttached:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move v0, v1

    goto :goto_0

    .line 241093
    :cond_2
    invoke-virtual {p1}, LX/1a1;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 241094
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tmp detached view should be removed from RecyclerView before it can be recycled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241095
    :cond_3
    invoke-virtual {p1}, LX/1a1;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 241096
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241097
    :cond_4
    invoke-static {p1}, LX/1a1;->B(LX/1a1;)Z

    move-result v3

    .line 241098
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v2, :cond_8

    if-eqz v3, :cond_8

    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v2, p1}, LX/1OM;->b(LX/1a1;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v0

    .line 241099
    :goto_1
    if-nez v2, :cond_5

    invoke-virtual {p1}, LX/1a1;->v()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 241100
    :cond_5
    const/16 v2, 0x4e

    invoke-virtual {p1, v2}, LX/1a1;->a(I)Z

    move-result v2

    if-nez v2, :cond_a

    .line 241101
    iget-object v2, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241102
    iget v4, p0, LX/1Od;->f:I

    if-ne v2, v4, :cond_6

    if-lez v2, :cond_6

    .line 241103
    invoke-direct {p0, v1}, LX/1Od;->e(I)V

    .line 241104
    :cond_6
    iget v4, p0, LX/1Od;->f:I

    if-ge v2, v4, :cond_a

    .line 241105
    iget-object v2, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v0

    .line 241106
    :goto_2
    if-nez v2, :cond_9

    .line 241107
    invoke-direct {p0, p1}, LX/1Od;->e(LX/1a1;)V

    move v1, v0

    move v0, v2

    .line 241108
    :goto_3
    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v2, p1}, LX/1Ok;->a(LX/1a1;)V

    .line 241109
    if-nez v0, :cond_7

    if-nez v1, :cond_7

    if-eqz v3, :cond_7

    .line 241110
    const/4 v0, 0x0

    iput-object v0, p1, LX/1a1;->k:Landroid/support/v7/widget/RecyclerView;

    .line 241111
    :cond_7
    return-void

    :cond_8
    move v2, v1

    .line 241112
    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_3

    :cond_a
    move v2, v1

    goto :goto_2

    :cond_b
    move v0, v1

    goto :goto_3
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 241113
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 241114
    invoke-virtual {v0}, LX/1a1;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241115
    iget-object v1, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 241116
    :cond_0
    invoke-virtual {v0}, LX/1a1;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 241117
    invoke-virtual {v0}, LX/1a1;->i()V

    .line 241118
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, LX/1Od;->a(LX/1a1;)V

    .line 241119
    return-void

    .line 241120
    :cond_2
    invoke-virtual {v0}, LX/1a1;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241121
    invoke-virtual {v0}, LX/1a1;->k()V

    goto :goto_0
.end method

.method public final b(I)I
    .locals 3

    .prologue
    .line 241122
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0}, LX/1Ok;->e()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 241123
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State item count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v2}, LX/1Ok;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241124
    :cond_1
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 241125
    iget-boolean v1, v0, LX/1Ok;->k:Z

    move v0, v1

    .line 241126
    if-nez v0, :cond_2

    .line 241127
    :goto_0
    return p1

    :cond_2
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0, p1}, LX/1On;->a(I)I

    move-result p1

    goto :goto_0
.end method

.method public final b(II)V
    .locals 4

    .prologue
    .line 241128
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241129
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 241130
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241131
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    if-lt v3, p1, :cond_0

    .line 241132
    const/4 v3, 0x1

    invoke-virtual {v0, p2, v3}, LX/1a1;->a(IZ)V

    .line 241133
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241134
    :cond_1
    return-void
.end method

.method public final b(LX/1a1;)V
    .locals 1

    .prologue
    .line 241135
    invoke-virtual {p1}, LX/1a1;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 241136
    :cond_0
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241137
    :goto_0
    const/4 v0, 0x0

    .line 241138
    iput-object v0, p1, LX/1a1;->o:LX/1Od;

    .line 241139
    invoke-virtual {p1}, LX/1a1;->k()V

    .line 241140
    return-void

    .line 241141
    :cond_1
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 241142
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 241143
    const/4 v1, 0x0

    .line 241144
    iput-object v1, v0, LX/1a1;->o:LX/1Od;

    .line 241145
    invoke-virtual {v0}, LX/1a1;->k()V

    .line 241146
    invoke-virtual {p0, v0}, LX/1Od;->a(LX/1a1;)V

    .line 241147
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 241148
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 241149
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1Od;->a(IZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c(II)V
    .locals 4

    .prologue
    .line 241150
    add-int v2, p1, p2

    .line 241151
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 241152
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 241153
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241154
    if-eqz v0, :cond_0

    .line 241155
    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    .line 241156
    if-lt v3, p1, :cond_0

    if-ge v3, v2, :cond_0

    .line 241157
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, LX/1a1;->b(I)V

    .line 241158
    invoke-direct {p0, v1}, LX/1Od;->e(I)V

    .line 241159
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 241160
    :cond_1
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 241161
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 241162
    iput-object p0, v0, LX/1a1;->o:LX/1Od;

    .line 241163
    invoke-virtual {v0}, LX/1a1;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 241164
    :cond_0
    invoke-virtual {v0}, LX/1a1;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->eC_()Z

    move-result v1

    if-nez v1, :cond_1

    .line 241165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241166
    :cond_1
    iget-object v1, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241167
    :goto_0
    return-void

    .line 241168
    :cond_2
    iget-object v1, p0, LX/1Od;->d:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 241169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/1Od;->d:Ljava/util/ArrayList;

    .line 241170
    :cond_3
    iget-object v1, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 241171
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 241172
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241173
    return-void
.end method

.method public final e()LX/1YF;
    .locals 1

    .prologue
    .line 241174
    iget-object v0, p0, LX/1Od;->g:LX/1YF;

    if-nez v0, :cond_0

    .line 241175
    new-instance v0, LX/1YF;

    invoke-direct {v0}, LX/1YF;-><init>()V

    iput-object v0, p0, LX/1Od;->g:LX/1YF;

    .line 241176
    :cond_0
    iget-object v0, p0, LX/1Od;->g:LX/1YF;

    return-object v0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 241177
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241178
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 241179
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241180
    if-eqz v0, :cond_0

    .line 241181
    const/16 v3, 0x200

    invoke-virtual {v0, v3}, LX/1a1;->b(I)V

    .line 241182
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241183
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 241184
    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Od;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241185
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241186
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 241187
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241188
    if-eqz v0, :cond_0

    .line 241189
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, LX/1a1;->b(I)V

    .line 241190
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/1a1;->a(Ljava/lang/Object;)V

    .line 241191
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241192
    :cond_1
    invoke-direct {p0}, LX/1Od;->j()V

    .line 241193
    :cond_2
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 241194
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 241195
    :goto_0
    if-ge v2, v3, :cond_0

    .line 241196
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241197
    invoke-virtual {v0}, LX/1a1;->a()V

    .line 241198
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 241199
    :cond_0
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 241200
    :goto_1
    if-ge v2, v3, :cond_1

    .line 241201
    iget-object v0, p0, LX/1Od;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    invoke-virtual {v0}, LX/1a1;->a()V

    .line 241202
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 241203
    :cond_1
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 241204
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241205
    :goto_2
    if-ge v1, v2, :cond_2

    .line 241206
    iget-object v0, p0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    invoke-virtual {v0}, LX/1a1;->a()V

    .line 241207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 241208
    :cond_2
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 241209
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 241210
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 241211
    iget-object v0, p0, LX/1Od;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 241212
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 241213
    if-eqz v0, :cond_0

    .line 241214
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/1a3;->c:Z

    .line 241215
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241216
    :cond_1
    return-void
.end method
