.class public LX/0qO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0qO;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147412
    iput-object p1, p0, LX/0qO;->a:LX/0Zb;

    .line 147413
    return-void
.end method

.method public static a(LX/0QB;)LX/0qO;
    .locals 4

    .prologue
    .line 147431
    sget-object v0, LX/0qO;->b:LX/0qO;

    if-nez v0, :cond_1

    .line 147432
    const-class v1, LX/0qO;

    monitor-enter v1

    .line 147433
    :try_start_0
    sget-object v0, LX/0qO;->b:LX/0qO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147434
    if-eqz v2, :cond_0

    .line 147435
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147436
    new-instance p0, LX/0qO;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/0qO;-><init>(LX/0Zb;)V

    .line 147437
    move-object v0, p0

    .line 147438
    sput-object v0, LX/0qO;->b:LX/0qO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147439
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147440
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147441
    :cond_1
    sget-object v0, LX/0qO;->b:LX/0qO;

    return-object v0

    .line 147442
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147443
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;ILjava/lang/String;LX/0qT;)V
    .locals 4

    .prologue
    .line 147414
    iget-object v0, p0, LX/0qO;->a:LX/0Zb;

    const-string v1, "fb4a_feed_story_refetch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 147415
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 147416
    :goto_0
    return-void

    .line 147417
    :cond_0
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v1, v1

    .line 147418
    invoke-virtual {p4, p3, v1}, LX/0qT;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v1

    .line 147419
    const-string v2, "feed_type"

    .line 147420
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v3, v3

    .line 147421
    invoke-virtual {v3}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 147422
    const-string v2, "story_index"

    invoke-virtual {v0, v2, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 147423
    const-string v2, "fetch_type"

    .line 147424
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v3, v3

    .line 147425
    invoke-virtual {v3}, LX/0gf;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 147426
    const-string v2, "refetched"

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 147427
    const-string v1, "caller_context"

    .line 147428
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 147429
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 147430
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
