.class public LX/0re;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0rf;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:I

.field private final j:I

.field public final k:LX/0pk;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0rg;


# direct methods
.method public constructor <init>(IIILX/0pk;LX/0Ot;LX/0rg;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "LX/0pk;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0rg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150022
    if-gtz p2, :cond_0

    .line 150023
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150024
    :cond_0
    if-gtz p1, :cond_1

    .line 150025
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxEntries <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150026
    :cond_1
    iput p3, p0, LX/0re;->i:I

    .line 150027
    iput p2, p0, LX/0re;->c:I

    .line 150028
    iput p1, p0, LX/0re;->j:I

    .line 150029
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x0

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    .line 150030
    iput-object p4, p0, LX/0re;->k:LX/0pk;

    .line 150031
    iput-object p5, p0, LX/0re;->l:LX/0Ot;

    .line 150032
    iput-object p6, p0, LX/0re;->m:LX/0rg;

    .line 150033
    return-void
.end method

.method private constructor <init>(IILX/0pk;LX/0Ot;LX/0rg;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LX/0pk;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0rg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150019
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/0re;-><init>(IIILX/0pk;LX/0Ot;LX/0rg;)V

    .line 150020
    return-void
.end method

.method public constructor <init>(ILX/0pk;LX/0Ot;LX/0rg;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0pk;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0rg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150017
    const v2, 0x7fffffff

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/0re;-><init>(IILX/0pk;LX/0Ot;LX/0rg;)V

    .line 150018
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 149994
    monitor-enter p0

    .line 149995
    :try_start_0
    iget v0, p0, LX/0re;->b:I

    if-gt v0, p1, :cond_0

    .line 149996
    monitor-exit p0

    .line 149997
    :goto_0
    return-void

    .line 149998
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 149999
    const/4 v0, 0x0

    move v1, v0

    .line 150000
    :goto_1
    monitor-enter p0

    .line 150001
    :try_start_1
    iget v0, p0, LX/0re;->b:I

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/0re;->b:I

    if-eqz v0, :cond_2

    .line 150002
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".sizeOf() is reporting inconsistent results!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150003
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 150004
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 150005
    :cond_2
    :try_start_3
    iget v0, p0, LX/0re;->i:I

    if-lt v1, v0, :cond_3

    iget v0, p0, LX/0re;->b:I

    if-le v0, p1, :cond_4

    :cond_3
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 150006
    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150007
    invoke-static {p0, v1}, LX/0re;->e(LX/0re;I)V

    goto :goto_0

    .line 150008
    :cond_5
    :try_start_4
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 150009
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 150010
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 150011
    iget-object v3, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150012
    iget v3, p0, LX/0re;->b:I

    invoke-direct {p0, v2, v0}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    sub-int v0, v3, v0

    iput v0, p0, LX/0re;->b:I

    .line 150013
    iget v0, p0, LX/0re;->b:I

    invoke-direct {p0, v0}, LX/0re;->f(I)V

    .line 150014
    iget v0, p0, LX/0re;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0re;->f:I

    .line 150015
    add-int/lit8 v0, v1, 0x1

    .line 150016
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v1, v0

    goto/16 :goto_1
.end method

.method private a(II)V
    .locals 0

    .prologue
    .line 149991
    invoke-direct {p0, p1}, LX/0re;->a(I)V

    .line 149992
    invoke-direct {p0, p2}, LX/0re;->b(I)V

    .line 149993
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 149971
    monitor-enter p0

    .line 149972
    :try_start_0
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 149973
    monitor-exit p0

    .line 149974
    :goto_0
    return-void

    .line 149975
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149976
    const/4 v0, 0x0

    move v1, v0

    .line 149977
    :goto_1
    monitor-enter p0

    .line 149978
    :try_start_1
    iget v0, p0, LX/0re;->i:I

    if-lt v1, v0, :cond_1

    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-le v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149979
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 149980
    invoke-static {p0, v1}, LX/0re;->e(LX/0re;I)V

    goto :goto_0

    .line 149981
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 149982
    :cond_3
    :try_start_3
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 149983
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 149984
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 149985
    iget-object v3, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149986
    iget v3, p0, LX/0re;->b:I

    invoke-direct {p0, v2, v0}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    sub-int v0, v3, v0

    iput v0, p0, LX/0re;->b:I

    .line 149987
    iget v0, p0, LX/0re;->b:I

    invoke-direct {p0, v0}, LX/0re;->f(I)V

    .line 149988
    iget v0, p0, LX/0re;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0re;->f:I

    .line 149989
    add-int/lit8 v0, v1, 0x1

    .line 149990
    monitor-exit p0

    move v1, v0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 149967
    invoke-virtual {p0, p1, p2}, LX/0re;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 149968
    if-gez v0, :cond_0

    .line 149969
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Negative size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149970
    :cond_0
    return v0
.end method

.method private static e(LX/0re;I)V
    .locals 4

    .prologue
    .line 149965
    iget-object v0, p0, LX/0re;->k:LX/0pk;

    sget-object v1, LX/37E;->CACHE_FULL:LX/37E;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, LX/0pk;->a(LX/37E;IJ)V

    .line 149966
    return-void
.end method

.method private f(I)V
    .locals 4

    .prologue
    .line 150034
    iget-object v0, p0, LX/0re;->k:LX/0pk;

    int-to-long v2, p1

    .line 150035
    sget-object v1, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-static {v0, v1, v2, v3}, LX/0pk;->b(LX/0pk;LX/17N;J)V

    .line 150036
    iget-object v0, p0, LX/0re;->k:LX/0pk;

    invoke-virtual {p0}, LX/0re;->b()I

    move-result v1

    int-to-long v2, v1

    .line 150037
    sget-object v1, LX/17N;->ENTRIES_COUNT:LX/17N;

    invoke-static {v0, v1, v2, v3}, LX/0pk;->b(LX/0pk;LX/17N;J)V

    .line 150038
    invoke-virtual {p0}, LX/0re;->b()I

    move-result v0

    if-lez v0, :cond_4

    .line 150039
    iget-object v0, p0, LX/0re;->m:LX/0rg;

    sget-object v1, LX/0rg;->SIZE:LX/0rg;

    invoke-virtual {v0, v1}, LX/0rg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0re;->m:LX/0rg;

    sget-object v1, LX/0rg;->COUNT_AND_SIZE:LX/0rg;

    invoke-virtual {v0, v1}, LX/0rg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 150040
    if-eqz v0, :cond_1

    .line 150041
    iget-object v0, p0, LX/0re;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/0re;->k:LX/0pk;

    sget-object v2, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150042
    :cond_1
    iget-object v0, p0, LX/0re;->m:LX/0rg;

    sget-object v1, LX/0rg;->COUNT:LX/0rg;

    invoke-virtual {v0, v1}, LX/0rg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/0re;->m:LX/0rg;

    sget-object v1, LX/0rg;->COUNT_AND_SIZE:LX/0rg;

    invoke-virtual {v0, v1}, LX/0rg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 150043
    if-eqz v0, :cond_3

    .line 150044
    iget-object v0, p0, LX/0re;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/0re;->k:LX/0pk;

    sget-object v2, LX/17N;->ENTRIES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/0re;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150045
    :cond_3
    :goto_2
    return-void

    .line 150046
    :cond_4
    iget-object v0, p0, LX/0re;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/0re;->k:LX/0pk;

    sget-object v2, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    .line 150047
    iget-object v0, p0, LX/0re;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/0re;->k:LX/0pk;

    sget-object v2, LX/17N;->ENTRIES_COUNT:LX/17N;

    invoke-virtual {v1, v2}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 149935
    if-nez p1, :cond_0

    .line 149936
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149937
    :cond_0
    monitor-enter p0

    .line 149938
    :try_start_0
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149939
    if-eqz v0, :cond_2

    .line 149940
    iget v1, p0, LX/0re;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0re;->g:I

    .line 149941
    const/4 v1, 0x1

    .line 149942
    iget-object v4, p0, LX/0re;->k:LX/0pk;

    int-to-long v6, v1

    invoke-virtual {v4, v6, v7}, LX/0pk;->a(J)V

    .line 149943
    monitor-exit p0

    .line 149944
    :cond_1
    :goto_0
    return-object v0

    .line 149945
    :cond_2
    iget v0, p0, LX/0re;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0re;->h:I

    .line 149946
    const/4 v0, 0x1

    .line 149947
    iget-object v4, p0, LX/0re;->k:LX/0pk;

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, LX/0pk;->b(J)V

    .line 149948
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149949
    const/4 v0, 0x0

    move-object v1, v0

    .line 149950
    if-nez v1, :cond_3

    .line 149951
    const/4 v0, 0x0

    goto :goto_0

    .line 149952
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 149953
    :cond_3
    monitor-enter p0

    .line 149954
    :try_start_2
    iget v0, p0, LX/0re;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0re;->e:I

    .line 149955
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149956
    if-eqz v0, :cond_4

    .line 149957
    iget-object v2, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149958
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149959
    if-nez v0, :cond_1

    .line 149960
    iget v0, p0, LX/0re;->c:I

    invoke-direct {p0, v0}, LX/0re;->a(I)V

    move-object v0, v1

    .line 149961
    goto :goto_0

    .line 149962
    :cond_4
    :try_start_3
    iget v2, p0, LX/0re;->b:I

    invoke-direct {p0, p1, v1}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, LX/0re;->b:I

    .line 149963
    iget v2, p0, LX/0re;->b:I

    invoke-direct {p0, v2}, LX/0re;->f(I)V

    goto :goto_1

    .line 149964
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 149898
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 149899
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null || value == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149900
    :cond_1
    monitor-enter p0

    .line 149901
    :try_start_0
    iget v0, p0, LX/0re;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0re;->d:I

    .line 149902
    iget v0, p0, LX/0re;->b:I

    invoke-direct {p0, p1, p2}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/0re;->b:I

    .line 149903
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149904
    if-eqz v0, :cond_2

    .line 149905
    iget v1, p0, LX/0re;->b:I

    invoke-direct {p0, p1, v0}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/0re;->b:I

    .line 149906
    :cond_2
    iget v1, p0, LX/0re;->b:I

    invoke-direct {p0, v1}, LX/0re;->f(I)V

    .line 149907
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149908
    iget v1, p0, LX/0re;->c:I

    iget v2, p0, LX/0re;->j:I

    invoke-direct {p0, v1, v2}, LX/0re;->a(II)V

    .line 149909
    return-object v0

    .line 149910
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 149933
    const/4 v0, -0x1

    invoke-direct {p0, v0}, LX/0re;->a(I)V

    .line 149934
    return-void
.end method

.method public final a(LX/32G;)V
    .locals 4

    .prologue
    .line 149928
    sget-object v0, LX/32G;->OnAppBackgrounded:LX/32G;

    if-ne p1, v0, :cond_0

    .line 149929
    :goto_0
    return-void

    .line 149930
    :cond_0
    invoke-virtual {p1}, LX/32G;->getSuggestedTrimRatio()D

    move-result-wide v0

    .line 149931
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v0, v2, v0

    invoke-virtual {p0}, LX/0re;->b()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 149932
    const v1, 0x7fffffff

    invoke-direct {p0, v1, v0}, LX/0re;->a(II)V

    goto :goto_0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 149927
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 149926
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 149916
    if-nez p1, :cond_0

    .line 149917
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149918
    :cond_0
    monitor-enter p0

    .line 149919
    :try_start_0
    iget-object v0, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149920
    if-eqz v0, :cond_1

    .line 149921
    iget v1, p0, LX/0re;->b:I

    invoke-direct {p0, p1, v0}, LX/0re;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/0re;->b:I

    .line 149922
    iget v1, p0, LX/0re;->b:I

    invoke-direct {p0, v1}, LX/0re;->f(I)V

    .line 149923
    :cond_1
    monitor-exit p0

    .line 149924
    return-object v0

    .line 149925
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 149915
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, LX/0re;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 149911
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/0re;->g:I

    iget v2, p0, LX/0re;->h:I

    add-int/2addr v1, v2

    .line 149912
    if-eqz v1, :cond_0

    iget v0, p0, LX/0re;->g:I

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    .line 149913
    :cond_0
    const-string v1, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/0re;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LX/0re;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, LX/0re;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 149914
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
