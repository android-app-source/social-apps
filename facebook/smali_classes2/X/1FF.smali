.class public LX/1FF;
.super LX/04L;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final b:LX/1FO;


# direct methods
.method public constructor <init>(LX/1FO;)V
    .locals 0

    .prologue
    .line 221967
    invoke-direct {p0}, LX/04L;-><init>()V

    .line 221968
    iput-object p1, p0, LX/1FF;->b:LX/1FO;

    .line 221969
    return-void
.end method


# virtual methods
.method public final a(LX/1FJ;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 221970
    invoke-static {p1, p2}, LX/04L;->a(LX/1FJ;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    move-object v2, v0

    .line 221971
    :goto_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 221972
    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v3

    if-gt p2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 221973
    iget-object v1, p0, LX/1FF;->b:LX/1FO;

    add-int/lit8 v3, p2, 0x2

    invoke-virtual {v1, v3}, LX/1FO;->a(I)LX/1FJ;

    move-result-object v3

    .line 221974
    :try_start_0
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 221975
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v1, v5, p2}, LX/1FK;->a(I[BII)V

    .line 221976
    if-eqz v2, :cond_1

    .line 221977
    const/4 v0, -0x1

    aput-byte v0, v1, p2

    .line 221978
    add-int/lit8 v0, p2, 0x1

    const/16 v2, -0x27

    aput-byte v2, v1, v0

    .line 221979
    add-int/lit8 p2, p2, 0x2

    .line 221980
    :cond_1
    const/4 v0, 0x0

    invoke-static {v1, v0, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221981
    const-string v1, "BitmapFactory returned null"

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221982
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    .line 221983
    :cond_2
    sget-object v0, LX/04L;->a:[B

    move-object v2, v0

    goto :goto_0

    .line 221984
    :catchall_0
    move-exception v0

    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final a(LX/1FJ;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 221985
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 221986
    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v2

    .line 221987
    iget-object v1, p0, LX/1FF;->b:LX/1FO;

    invoke-virtual {v1, v2}, LX/1FO;->a(I)LX/1FJ;

    move-result-object v3

    .line 221988
    :try_start_0
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 221989
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v1, v5, v2}, LX/1FK;->a(I[BII)V

    .line 221990
    const/4 v0, 0x0

    invoke-static {v1, v0, v2, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221991
    const-string v1, "BitmapFactory returned null"

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221992
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
