.class public LX/0bD;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/26n;",
        "LX/0bF;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0bD;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86765
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 86766
    return-void
.end method

.method public static a(LX/0QB;)LX/0bD;
    .locals 3

    .prologue
    .line 86767
    sget-object v0, LX/0bD;->a:LX/0bD;

    if-nez v0, :cond_1

    .line 86768
    const-class v1, LX/0bD;

    monitor-enter v1

    .line 86769
    :try_start_0
    sget-object v0, LX/0bD;->a:LX/0bD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 86770
    if-eqz v2, :cond_0

    .line 86771
    :try_start_1
    new-instance v0, LX/0bD;

    invoke-direct {v0}, LX/0bD;-><init>()V

    .line 86772
    move-object v0, v0

    .line 86773
    sput-object v0, LX/0bD;->a:LX/0bD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86774
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 86775
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86776
    :cond_1
    sget-object v0, LX/0bD;->a:LX/0bD;

    return-object v0

    .line 86777
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 86778
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
