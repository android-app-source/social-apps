.class public LX/1iM;
.super Ljava/io/InputStream;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1iM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1AI;

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private f:J

.field private g:I

.field public volatile h:Z

.field private i:Ljava/lang/Throwable;

.field private j:I

.field public volatile k:Z

.field public volatile l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298168
    const-class v0, LX/1iM;

    sput-object v0, LX/1iM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1AI;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298169
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 298170
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 298171
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, LX/1iM;->d:Ljava/util/concurrent/locks/Condition;

    .line 298172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    .line 298173
    iput-boolean v1, p0, LX/1iM;->h:Z

    .line 298174
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iM;->i:Ljava/lang/Throwable;

    .line 298175
    iput-boolean v1, p0, LX/1iM;->k:Z

    .line 298176
    iput-boolean v1, p0, LX/1iM;->l:Z

    .line 298177
    iput-object p1, p0, LX/1iM;->b:LX/1AI;

    .line 298178
    iput p2, p0, LX/1iM;->j:I

    .line 298179
    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 298180
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1iM;->h:Z

    .line 298181
    iget-object v0, p0, LX/1iM;->i:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    iget-object p1, p0, LX/1iM;->i:Ljava/lang/Throwable;

    :cond_0
    iput-object p1, p0, LX/1iM;->i:Ljava/lang/Throwable;

    .line 298182
    iget-object v0, p0, LX/1iM;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 298183
    return-void
.end method

.method private b(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 298120
    iget-object v0, p0, LX/1iM;->b:LX/1AI;

    invoke-interface {v0, p1}, LX/1AI;->releaseBodyBuffer(Ljava/nio/ByteBuffer;)V

    .line 298121
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 298184
    iget-object v0, p0, LX/1iM;->i:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 298185
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, LX/1iM;->i:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 298186
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 298199
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 298200
    invoke-direct {p0, v0}, LX/1iM;->b(Ljava/nio/ByteBuffer;)V

    .line 298201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 298202
    :cond_0
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 298203
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298187
    :goto_0
    iget-boolean v0, p0, LX/1iM;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298188
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1iM;->k:Z

    .line 298189
    :try_start_0
    iget-object v0, p0, LX/1iM;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->awaitUninterruptibly()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298190
    iput-boolean v1, p0, LX/1iM;->k:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, LX/1iM;->k:Z

    throw v0

    .line 298191
    :cond_0
    return-void
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 298192
    iget-boolean v0, p0, LX/1iM;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/1iM;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298193
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298194
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 298195
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298196
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, LX/1iM;->b(Ljava/nio/ByteBuffer;)V

    .line 298197
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 298198
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 298158
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298159
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1iM;->b(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298160
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 298161
    return-void

    .line 298162
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 298163
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298164
    invoke-direct {p0, p1}, LX/1iM;->b(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298165
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 298166
    return-void

    .line 298167
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 298065
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298066
    iget-boolean v0, p0, LX/1iM;->h:Z

    if-eqz v0, :cond_0

    .line 298067
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Writing to closed buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298068
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 298069
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/1iM;->l:Z

    if-eqz v0, :cond_1

    .line 298070
    invoke-direct {p0, p1}, LX/1iM;->b(Ljava/nio/ByteBuffer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298071
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 298072
    :goto_0
    return-void

    .line 298073
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 298074
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 298075
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298076
    iget-wide v0, p0, LX/1iM;->f:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1iM;->f:J

    .line 298077
    iget v0, p0, LX/1iM;->g:I

    invoke-virtual {p0}, LX/1iM;->available()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/1iM;->g:I

    .line 298078
    iget-object v0, p0, LX/1iM;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 298079
    :cond_2
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 298080
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final available()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 298081
    :try_start_0
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298082
    iget-object v1, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 298083
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/2addr v2, v0

    .line 298084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 298085
    :cond_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b()J
    .locals 3

    .prologue
    .line 298086
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298087
    iget-wide v0, p0, LX/1iM;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298088
    iget-object v2, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 298089
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298090
    iget v0, p0, LX/1iM;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298091
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final close()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 298092
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298093
    invoke-direct {p0}, LX/1iM;->f()V

    .line 298094
    invoke-direct {p0}, LX/1iM;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298095
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 298096
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 298097
    iput-boolean v2, p0, LX/1iM;->l:Z

    .line 298098
    return-void

    .line 298099
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 298100
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 298101
    iput-boolean v2, p0, LX/1iM;->l:Z

    throw v0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 298102
    invoke-virtual {p0}, LX/1iM;->a()V

    .line 298103
    invoke-direct {p0}, LX/1iM;->f()V

    .line 298104
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 298105
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 298106
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298107
    invoke-direct {p0}, LX/1iM;->g()V

    .line 298108
    invoke-direct {p0}, LX/1iM;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298109
    invoke-direct {p0}, LX/1iM;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298110
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v0, -0x1

    :goto_0
    return v0

    .line 298111
    :cond_0
    :try_start_1
    const/4 v1, 0x0

    .line 298112
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298113
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 298114
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 298115
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 298116
    invoke-static {p0}, LX/1iM;->j(LX/1iM;)V

    .line 298117
    move v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298118
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_1
    move v0, v1

    .line 298119
    goto :goto_1
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 298122
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/1iM;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 298123
    :try_start_0
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 298124
    invoke-direct {p0}, LX/1iM;->g()V

    .line 298125
    invoke-direct {p0}, LX/1iM;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298126
    invoke-direct {p0}, LX/1iM;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298127
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v0, -0x1

    :goto_0
    return v0

    .line 298128
    :cond_0
    if-gtz p3, :cond_1

    .line 298129
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    move v1, p3

    .line 298130
    :goto_1
    if-lez v1, :cond_3

    .line 298131
    :try_start_1
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 298132
    :goto_2
    if-eqz v0, :cond_3

    .line 298133
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 298134
    invoke-virtual {v0, p1, p2, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 298135
    sub-int v0, v1, v2

    .line 298136
    add-int/2addr p2, v2

    .line 298137
    invoke-static {p0}, LX/1iM;->j(LX/1iM;)V

    move v1, v0

    .line 298138
    goto :goto_1

    .line 298139
    :cond_2
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 298140
    :cond_3
    sub-int v0, p3, v1

    .line 298141
    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final skip(J)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 298142
    :try_start_0
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298143
    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    .line 298144
    iget-object v0, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move-wide v0, v2

    :goto_0
    return-wide v0

    :cond_0
    move-wide v4, p1

    .line 298145
    :goto_1
    cmp-long v0, v4, v2

    if-lez v0, :cond_2

    .line 298146
    :try_start_1
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 298147
    :goto_2
    if-eqz v0, :cond_2

    .line 298148
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    int-to-long v6, v1

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v1, v6

    .line 298149
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 298150
    int-to-long v0, v1

    sub-long v0, v4, v0

    .line 298151
    invoke-static {p0}, LX/1iM;->j(LX/1iM;)V

    move-wide v4, v0

    .line 298152
    goto :goto_1

    .line 298153
    :cond_1
    iget-object v0, p0, LX/1iM;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 298154
    :cond_2
    cmp-long v0, p1, v4

    if-nez v0, :cond_3

    .line 298155
    invoke-direct {p0}, LX/1iM;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298156
    :cond_3
    sub-long v0, p1, v4

    .line 298157
    iget-object v2, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1iM;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
