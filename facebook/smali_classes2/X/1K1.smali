.class public LX/1K1;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fs;
.implements LX/0fy;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field private final a:LX/1Jg;

.field private final b:LX/0fx;

.field private c:LX/0g8;

.field private d:Z


# direct methods
.method public constructor <init>(LX/1Jg;)V
    .locals 1

    .prologue
    .line 230799
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 230800
    iput-object p1, p0, LX/1K1;->a:LX/1Jg;

    .line 230801
    invoke-interface {p1}, LX/1Jg;->a()LX/0fx;

    move-result-object v0

    iput-object v0, p0, LX/1K1;->b:LX/0fx;

    .line 230802
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 230803
    iget-boolean v0, p0, LX/1K1;->d:Z

    if-nez v0, :cond_0

    .line 230804
    :goto_0
    return-void

    .line 230805
    :cond_0
    iget-object v0, p0, LX/1K1;->b:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 230806
    iget-boolean v0, p0, LX/1K1;->d:Z

    if-eqz v0, :cond_0

    .line 230807
    iget-object v0, p0, LX/1K1;->b:LX/0fx;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 230808
    :cond_0
    return-void
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 230809
    iput-object p2, p0, LX/1K1;->c:LX/0g8;

    .line 230810
    iget-object v0, p0, LX/1K1;->a:LX/1Jg;

    invoke-interface {p1}, LX/1Qr;->e()LX/1R4;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/1R4;)V

    .line 230811
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 230812
    iget-object v0, p0, LX/1K1;->a:LX/1Jg;

    iget-object v1, p0, LX/1K1;->c:LX/0g8;

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/0g8;)V

    .line 230813
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1K1;->d:Z

    .line 230814
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 230815
    iget-object v0, p0, LX/1K1;->a:LX/1Jg;

    invoke-interface {v0}, LX/1Jg;->b()V

    .line 230816
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1K1;->d:Z

    .line 230817
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 230818
    const/4 v0, 0x0

    iput-object v0, p0, LX/1K1;->c:LX/0g8;

    .line 230819
    return-void
.end method
