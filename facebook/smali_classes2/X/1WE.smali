.class public LX/1WE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1XF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1WE",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1XF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267833
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 267834
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1WE;->b:LX/0Zi;

    .line 267835
    iput-object p1, p0, LX/1WE;->a:LX/0Ot;

    .line 267836
    return-void
.end method

.method public static a(LX/0QB;)LX/1WE;
    .locals 4

    .prologue
    .line 267822
    const-class v1, LX/1WE;

    monitor-enter v1

    .line 267823
    :try_start_0
    sget-object v0, LX/1WE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267824
    sput-object v2, LX/1WE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267825
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267826
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267827
    new-instance v3, LX/1WE;

    const/16 p0, 0x747

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1WE;-><init>(LX/0Ot;)V

    .line 267828
    move-object v0, v3

    .line 267829
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267830
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267831
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267832
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/48C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267837
    const v0, 0x3857c395

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 16

    .prologue
    .line 267819
    check-cast p2, LX/1XE;

    .line 267820
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WE;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1XF;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/1XE;->b:LX/1Pn;

    move-object/from16 v0, p2

    iget v5, v0, LX/1XE;->c:I

    move-object/from16 v0, p2

    iget-object v6, v0, LX/1XE;->d:Ljava/lang/Integer;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/1XE;->e:Ljava/lang/Integer;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/1XE;->f:Landroid/graphics/Typeface;

    move-object/from16 v0, p2

    iget-object v9, v0, LX/1XE;->g:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p2

    iget-boolean v10, v0, LX/1XE;->h:Z

    move-object/from16 v0, p2

    iget-object v11, v0, LX/1XE;->i:LX/1xz;

    move-object/from16 v0, p2

    iget v12, v0, LX/1XE;->j:I

    move-object/from16 v0, p2

    iget v13, v0, LX/1XE;->k:I

    move-object/from16 v0, p2

    iget v14, v0, LX/1XE;->l:I

    move-object/from16 v0, p2

    iget v15, v0, LX/1XE;->m:F

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v15}, LX/1XF;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;ILjava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLX/1xz;IIIF)LX/1Dg;

    move-result-object v1

    .line 267821
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 267790
    invoke-static {}, LX/1dS;->b()V

    .line 267791
    iget v0, p1, LX/1dQ;->b:I

    .line 267792
    packed-switch v0, :pswitch_data_0

    .line 267793
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 267794
    :pswitch_0
    check-cast p2, LX/48C;

    .line 267795
    iget-object v0, p2, LX/48C;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 267796
    check-cast v1, LX/1XE;

    .line 267797
    iget-object v2, p0, LX/1WE;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1XF;

    iget-object v3, v1, LX/1XE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267798
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 267799
    iget-object p1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 267800
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267801
    new-instance p0, LX/6WS;

    const/4 v1, 0x1

    invoke-direct {p0, p2, v1}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 267802
    new-instance v1, LX/5OG;

    invoke-direct {v1, p2}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 267803
    invoke-virtual {p0, v1}, LX/5OM;->a(LX/5OG;)V

    .line 267804
    const v3, 0x7f081081

    invoke-virtual {v1, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    .line 267805
    new-instance v3, LX/Bt2;

    invoke-direct {v3, v2, p2, p1}, LX/Bt2;-><init>(LX/1XF;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v1, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 267806
    move-object p1, p0

    .line 267807
    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 267808
    const/4 p1, 0x1

    move v2, p1

    .line 267809
    move v0, v2

    .line 267810
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3857c395
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1XG;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1WE",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 267811
    new-instance v1, LX/1XE;

    invoke-direct {v1, p0}, LX/1XE;-><init>(LX/1WE;)V

    .line 267812
    iget-object v2, p0, LX/1WE;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1XG;

    .line 267813
    if-nez v2, :cond_0

    .line 267814
    new-instance v2, LX/1XG;

    invoke-direct {v2, p0}, LX/1XG;-><init>(LX/1WE;)V

    .line 267815
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1XG;->a$redex0(LX/1XG;LX/1De;IILX/1XE;)V

    .line 267816
    move-object v1, v2

    .line 267817
    move-object v0, v1

    .line 267818
    return-object v0
.end method
