.class public LX/0oq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:I

.field public c:I

.field private d:I

.field private e:J

.field public f:LX/0ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ot",
            "<TT;>;"
        }
    .end annotation
.end field

.field private g:LX/0So;

.field private final h:LX/46Q;


# direct methods
.method public constructor <init>(LX/46Q;Ljava/lang/Class;LX/0So;)V
    .locals 2
    .param p1    # LX/46Q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/46Q;",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 143208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143209
    iput v1, p0, LX/0oq;->b:I

    .line 143210
    const/16 v0, 0x400

    iput v0, p0, LX/0oq;->c:I

    .line 143211
    iput v1, p0, LX/0oq;->d:I

    .line 143212
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, LX/0oq;->e:J

    .line 143213
    iput-object p1, p0, LX/0oq;->h:LX/46Q;

    .line 143214
    iput-object p2, p0, LX/0oq;->a:Ljava/lang/Class;

    .line 143215
    iput-object p3, p0, LX/0oq;->g:LX/0So;

    .line 143216
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;LX/0So;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143227
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, LX/0oq;-><init>(LX/46Q;Ljava/lang/Class;LX/0So;)V

    .line 143228
    return-void
.end method


# virtual methods
.method public final a()LX/0ou;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0ou",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 143217
    iget-object v0, p0, LX/0oq;->g:LX/0So;

    if-nez v0, :cond_0

    .line 143218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must add a clock to the object pool builder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143219
    :cond_0
    iget-object v8, p0, LX/0oq;->f:LX/0ot;

    .line 143220
    if-nez v8, :cond_1

    .line 143221
    new-instance v8, LX/0os;

    iget-object v0, p0, LX/0oq;->a:Ljava/lang/Class;

    invoke-direct {v8, v0}, LX/0os;-><init>(Ljava/lang/Class;)V

    .line 143222
    :cond_1
    new-instance v1, LX/0ou;

    iget-object v2, p0, LX/0oq;->a:Ljava/lang/Class;

    iget v3, p0, LX/0oq;->b:I

    iget v4, p0, LX/0oq;->c:I

    iget v5, p0, LX/0oq;->d:I

    iget-wide v6, p0, LX/0oq;->e:J

    iget-object v9, p0, LX/0oq;->g:LX/0So;

    invoke-direct/range {v1 .. v9}, LX/0ou;-><init>(Ljava/lang/Class;IIIJLX/0ot;LX/0So;)V

    .line 143223
    iget-object v0, p0, LX/0oq;->h:LX/46Q;

    if-eqz v0, :cond_2

    .line 143224
    iget-object v0, p0, LX/0oq;->h:LX/46Q;

    iget-object v2, p0, LX/0oq;->a:Ljava/lang/Class;

    .line 143225
    iget-object v3, v0, LX/46Q;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143226
    :cond_2
    return-object v1
.end method
