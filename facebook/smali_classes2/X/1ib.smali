.class public LX/1ib;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/webrequest/NetworkDataLoggerConfigProvider;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298711
    const-class v0, LX/1ib;

    sput-object v0, LX/1ib;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/webrequest/NetworkDataLoggerConfigProvider;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298707
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1ib;->f:J

    .line 298708
    iput-object p1, p0, LX/1ib;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 298709
    iput-object p2, p0, LX/1ib;->c:Ljava/util/Set;

    .line 298710
    return-void
.end method

.method public static b(LX/0QB;)LX/1ib;
    .locals 5

    .prologue
    .line 298702
    new-instance v1, LX/1ib;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 298703
    new-instance v2, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/1ic;

    invoke-direct {v4, p0}, LX/1ic;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v2

    .line 298704
    invoke-direct {v1, v0, v2}, LX/1ib;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;)V

    .line 298705
    return-object v1
.end method

.method private declared-synchronized b()V
    .locals 7

    .prologue
    .line 298686
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ib;->d:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1ib;->e:LX/0P1;

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/1ib;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x2932e00

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 298687
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/1ib;->f:J

    .line 298688
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 298689
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 298690
    iget-object v0, p0, LX/1ib;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pc;

    .line 298691
    invoke-virtual {v0, v1}, LX/1pc;->a(LX/0P2;)V

    .line 298692
    iget-object v4, v0, LX/1pc;->a:Landroid/content/Context;

    const-string v5, "photos"

    invoke-static {v4, v5}, LX/1pc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    const-string v5, "photo_upload"

    invoke-virtual {v2, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    iget-object v5, v0, LX/1pc;->a:Landroid/content/Context;

    const-string v6, "message_images"

    invoke-static {v5, v6}, LX/1pc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    const-string v6, "photo_upload"

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    iget-object v5, v0, LX/1pc;->a:Landroid/content/Context;

    .line 298693
    invoke-static {v5}, LX/38I;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 298694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v0, "(me|\\d+)"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v0, "/videos"

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 298695
    move-object v5, v6

    .line 298696
    const-string v6, "video_upload"

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298697
    goto :goto_0

    .line 298698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 298699
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/1ib;->d:LX/0P1;

    .line 298700
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/1ib;->e:LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298701
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/net/URI;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 298664
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1ib;->b()V

    .line 298665
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    .line 298666
    iget-object v0, p0, LX/1ib;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 298667
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/regex/Pattern;

    .line 298668
    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 298669
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298670
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298671
    :goto_0
    monitor-exit p0

    return-object v0

    .line 298672
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1ib;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 298673
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 298674
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 298675
    :cond_3
    invoke-virtual {p1}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 298676
    if-eqz v0, :cond_5

    const-string v1, "fbcdn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "fbstatic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 298677
    :cond_4
    const-string v0, "cdn"

    goto :goto_0

    .line 298678
    :cond_5
    if-eqz v0, :cond_6

    const-string v1, "fbexternal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 298679
    const-string v0, "cdn_external"

    goto :goto_0

    .line 298680
    :cond_6
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 298681
    if-eqz v0, :cond_8

    const-string v1, ".facebook.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 298682
    if-eqz v0, :cond_7

    .line 298683
    const-string v0, "other_fb"

    goto :goto_0

    .line 298684
    :cond_7
    const-string v0, "uncategorized"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298685
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 298660
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/1ib;->d:LX/0P1;

    .line 298661
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ib;->e:LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298662
    monitor-exit p0

    return-void

    .line 298663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 298658
    iget-object v0, p0, LX/1ib;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->p:LX/0Tn;

    new-instance v2, LX/2Ed;

    invoke-direct {v2, p0}, LX/2Ed;-><init>(LX/1ib;)V

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c(LX/0Tn;LX/0dN;)V

    .line 298659
    return-void
.end method
