.class public LX/0dD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field private final a:LX/0dE;


# direct methods
.method public constructor <init>(LX/0dE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90124
    iput-object p1, p0, LX/0dD;->a:LX/0dE;

    .line 90125
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90126
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 90127
    instance-of v0, p1, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_0

    .line 90128
    const-string v0, "n/a"

    .line 90129
    :goto_0
    return-object v0

    .line 90130
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0dD;->a:LX/0dE;

    .line 90131
    sget-object v1, LX/2fu;->a:Ljava/util/Set;

    iget-object v2, v0, LX/0dE;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0dE;->a(LX/0dE;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1
    :try_end_0
    .catch LX/44K; {:try_start_0 .. :try_end_0} :catch_0

    .line 90132
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90133
    :cond_1
    const-string v0, "none"

    goto :goto_0

    .line 90134
    :catch_0
    const-string v0, "exception"

    goto :goto_0

    .line 90135
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 90136
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 90137
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "={"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "}\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 90138
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
