.class public LX/1cp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1cp;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final b:LX/0YT;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 283227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283228
    new-instance v0, LX/0YT;

    invoke-direct {v0}, LX/0YT;-><init>()V

    iput-object v0, p0, LX/1cp;->b:LX/0YT;

    .line 283229
    iput-object p1, p0, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 283230
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 283215
    packed-switch p0, :pswitch_data_0

    .line 283216
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No QPL event to match id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283217
    :pswitch_1
    const v0, 0x8a0001

    .line 283218
    :goto_0
    return v0

    .line 283219
    :pswitch_2
    const v0, 0x8a0002

    goto :goto_0

    .line 283220
    :pswitch_3
    const v0, 0x8a0003

    goto :goto_0

    .line 283221
    :pswitch_4
    const v0, 0x8a0004

    goto :goto_0

    .line 283222
    :pswitch_5
    const v0, 0x8a0005

    goto :goto_0

    .line 283223
    :pswitch_6
    const v0, 0x8a0006

    goto :goto_0

    .line 283224
    :pswitch_7
    const v0, 0x8a0007

    goto :goto_0

    .line 283225
    :pswitch_8
    const v0, 0x8a000a

    goto :goto_0

    .line 283226
    :pswitch_9
    const v0, 0x8a000b

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/1cp;
    .locals 4

    .prologue
    .line 283202
    sget-object v0, LX/1cp;->c:LX/1cp;

    if-nez v0, :cond_1

    .line 283203
    const-class v1, LX/1cp;

    monitor-enter v1

    .line 283204
    :try_start_0
    sget-object v0, LX/1cp;->c:LX/1cp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 283205
    if-eqz v2, :cond_0

    .line 283206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 283207
    new-instance p0, LX/1cp;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/1cp;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 283208
    move-object v0, p0

    .line 283209
    sput-object v0, LX/1cp;->c:LX/1cp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283210
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 283211
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 283212
    :cond_1
    sget-object v0, LX/1cp;->c:LX/1cp;

    return-object v0

    .line 283213
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 283214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 283200
    iget-object v0, p0, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/1cp;->a(I)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 283201
    return-void
.end method

.method public final a(ILjava/lang/Object;I)V
    .locals 4

    .prologue
    .line 283191
    iget-object v0, p0, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/1cp;->a(I)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    const/16 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 283192
    iget-object v0, p0, LX/1cp;->b:LX/0YT;

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v1, p1

    .line 283193
    iget-object v2, v0, LX/0YT;->a:LX/0YU;

    .line 283194
    invoke-virtual {v2, v1}, LX/0YU;->b(I)V

    .line 283195
    return-void
.end method

.method public final a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 283198
    iget-object v0, p0, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/1cp;->a(I)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 283199
    return-void
.end method

.method public final b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 283196
    iget-object v0, p0, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p1}, LX/1cp;->a(I)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 283197
    return-void
.end method
