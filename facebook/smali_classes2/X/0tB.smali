.class public final LX/0tB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0sk;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0sk;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 154093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154094
    iput-object p1, p0, LX/0tB;->a:LX/0QB;

    .line 154095
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 154084
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0tB;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 154085
    packed-switch p2, :pswitch_data_0

    .line 154086
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154087
    :pswitch_0
    invoke-static {p1}, LX/1N8;->a(LX/0QB;)LX/1N8;

    move-result-object v0

    .line 154088
    :goto_0
    return-object v0

    .line 154089
    :pswitch_1
    invoke-static {p1}, LX/2k0;->a(LX/0QB;)LX/2k0;

    move-result-object v0

    goto :goto_0

    .line 154090
    :pswitch_2
    invoke-static {p1}, LX/2kG;->a(LX/0QB;)LX/2kG;

    move-result-object v0

    goto :goto_0

    .line 154091
    :pswitch_3
    invoke-static {p1}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 154092
    const/4 v0, 0x4

    return v0
.end method
