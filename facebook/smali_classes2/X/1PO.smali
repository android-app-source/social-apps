.class public LX/1PO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Py;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0bH;

.field public final b:LX/0So;

.field private final c:LX/0ad;

.field public final d:LX/1PP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1PO",
            "<TE;>.RefreshTriggerEventSubscriber;"
        }
    .end annotation
.end field

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>(LX/0bH;LX/0So;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244013
    iput-object p1, p0, LX/1PO;->a:LX/0bH;

    .line 244014
    iput-object p2, p0, LX/1PO;->b:LX/0So;

    .line 244015
    iput-object p3, p0, LX/1PO;->c:LX/0ad;

    .line 244016
    new-instance v0, LX/1PP;

    invoke-direct {v0, p0}, LX/1PP;-><init>(LX/1PO;)V

    iput-object v0, p0, LX/1PO;->d:LX/1PP;

    .line 244017
    return-void
.end method

.method public static a(LX/0QB;)LX/1PO;
    .locals 6

    .prologue
    .line 244000
    const-class v1, LX/1PO;

    monitor-enter v1

    .line 244001
    :try_start_0
    sget-object v0, LX/1PO;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 244002
    sput-object v2, LX/1PO;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244003
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244004
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 244005
    new-instance p0, LX/1PO;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/1PO;-><init>(LX/0bH;LX/0So;LX/0ad;)V

    .line 244006
    move-object v0, p0

    .line 244007
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 244008
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1PO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244009
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 244010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1PO;)Z
    .locals 5

    .prologue
    .line 244011
    iget-object v0, p0, LX/1PO;->c:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/0fe;->bx:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method
