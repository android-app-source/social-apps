.class public abstract LX/12q;
.super LX/0Px;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Px",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175773
    invoke-direct {p0}, LX/0Px;-><init>()V

    .line 175774
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "serialization"
    .end annotation

    .prologue
    .line 175775
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract a()LX/0Py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TE;>;"
        }
    .end annotation
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 175776
    invoke-virtual {p0}, LX/12q;->a()LX/0Py;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Py;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 175777
    invoke-virtual {p0}, LX/12q;->a()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 175778
    invoke-virtual {p0}, LX/12q;->a()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 175779
    invoke-virtual {p0}, LX/12q;->a()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->size()I

    move-result v0

    return v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "serialization"
    .end annotation

    .prologue
    .line 175780
    new-instance v0, LX/4y0;

    invoke-virtual {p0}, LX/12q;->a()LX/0Py;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4y0;-><init>(LX/0Py;)V

    return-object v0
.end method
