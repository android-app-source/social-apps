.class public LX/1I8;
.super LX/1I6;
.source ""


# instance fields
.field private transient a:LX/1I6;

.field public final b:LX/1I9;

.field public final c:Ljava/lang/Character;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1I9;Ljava/lang/Character;)V
    .locals 4
    .param p2    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 228410
    invoke-direct {p0}, LX/1I6;-><init>()V

    .line 228411
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1I9;

    iput-object v0, p0, LX/1I8;->b:LX/1I9;

    .line 228412
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p1, v0}, LX/1I9;->matches(C)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Padding character %s was already in alphabet"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-static {v0, v3, v2}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 228413
    iput-object p2, p0, LX/1I8;->c:Ljava/lang/Character;

    .line 228414
    return-void

    :cond_1
    move v0, v1

    .line 228415
    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
    .locals 2
    .param p3    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228386
    new-instance v0, LX/1I9;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/1I9;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0, p3}, LX/1I8;-><init>(LX/1I9;Ljava/lang/Character;)V

    .line 228387
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 228409
    iget-object v0, p0, LX/1I8;->b:LX/1I9;

    iget v0, v0, LX/1I9;->c:I

    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    iget v1, v1, LX/1I9;->d:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, v1, v2}, LX/1IS;->a(IILjava/math/RoundingMode;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public a([BLjava/lang/CharSequence;)I
    .locals 16

    .prologue
    .line 228390
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228391
    invoke-virtual/range {p0 .. p0}, LX/1I8;->a()LX/1IA;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/1IA;->trimTrailingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 228392
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1I8;->b:LX/1I9;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v2, v3}, LX/1I9;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 228393
    new-instance v2, LX/51z;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid input length "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/51z;-><init>(Ljava/lang/String;)V

    throw v2

    .line 228394
    :cond_0
    const/4 v3, 0x0

    .line 228395
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 228396
    const-wide/16 v6, 0x0

    .line 228397
    const/4 v5, 0x0

    .line 228398
    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, LX/1I8;->b:LX/1I9;

    iget v8, v8, LX/1I9;->c:I

    if-ge v4, v8, :cond_1

    .line 228399
    move-object/from16 v0, p0

    iget-object v8, v0, LX/1I8;->b:LX/1I9;

    iget v8, v8, LX/1I9;->b:I

    shl-long v8, v6, v8

    .line 228400
    add-int v6, v2, v4

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-ge v6, v7, :cond_4

    .line 228401
    move-object/from16 v0, p0

    iget-object v7, v0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v6, v5, 0x1

    add-int/2addr v5, v2

    invoke-interface {v10, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v7, v5}, LX/1I9;->a(C)I

    move-result v5

    int-to-long v12, v5

    or-long/2addr v8, v12

    move v5, v6

    move-wide v6, v8

    .line 228402
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 228403
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1I8;->b:LX/1I9;

    iget v4, v4, LX/1I9;->d:I

    mul-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1I8;->b:LX/1I9;

    iget v8, v8, LX/1I9;->b:I

    mul-int/2addr v5, v8

    sub-int v8, v4, v5

    .line 228404
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1I8;->b:LX/1I9;

    iget v4, v4, LX/1I9;->d:I

    add-int/lit8 v4, v4, -0x1

    mul-int/lit8 v4, v4, 0x8

    :goto_3
    if-lt v4, v8, :cond_2

    .line 228405
    add-int/lit8 v5, v3, 0x1

    ushr-long v12, v6, v4

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v9, v12

    int-to-byte v9, v9

    aput-byte v9, p1, v3

    .line 228406
    add-int/lit8 v3, v4, -0x8

    move v4, v3

    move v3, v5

    goto :goto_3

    .line 228407
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1I8;->b:LX/1I9;

    iget v4, v4, LX/1I9;->c:I

    add-int/2addr v2, v4

    goto :goto_0

    .line 228408
    :cond_3
    return v3

    :cond_4
    move-wide v6, v8

    goto :goto_2
.end method

.method public a(LX/1I9;Ljava/lang/Character;)LX/1I6;
    .locals 1
    .param p2    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228389
    new-instance v0, LX/1I8;

    invoke-direct {v0, p1, p2}, LX/1I8;-><init>(LX/1I9;Ljava/lang/Character;)V

    return-object v0
.end method

.method public final a()LX/1IA;
    .locals 1

    .prologue
    .line 228388
    iget-object v0, p0, LX/1I8;->c:Ljava/lang/Character;

    if-nez v0, :cond_0

    sget-object v0, LX/1IA;->NONE:LX/1IA;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1I8;->c:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, LX/1IA;->is(C)LX/1IA;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Appendable;[BII)V
    .locals 4

    .prologue
    .line 228416
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228417
    add-int v0, p3, p4

    array-length v1, p2

    invoke-static {p3, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    .line 228418
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p4, :cond_0

    .line 228419
    add-int v1, p3, v0

    iget-object v2, p0, LX/1I8;->b:LX/1I9;

    iget v2, v2, LX/1I9;->d:I

    sub-int v3, p4, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, p1, p2, v1, v2}, LX/1I8;->b(Ljava/lang/Appendable;[BII)V

    .line 228420
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    iget v1, v1, LX/1I9;->d:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 228421
    :cond_0
    return-void
.end method

.method public final b(I)I
    .locals 4

    .prologue
    .line 228334
    iget-object v0, p0, LX/1I8;->b:LX/1I9;

    iget v0, v0, LX/1I9;->b:I

    int-to-long v0, v0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x7

    add-long/2addr v0, v2

    const-wide/16 v2, 0x8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final b()LX/1I6;
    .locals 2

    .prologue
    .line 228335
    iget-object v0, p0, LX/1I8;->c:Ljava/lang/Character;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, LX/1I8;->b:LX/1I9;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/1I8;->a(LX/1I9;Ljava/lang/Character;)LX/1I6;

    move-result-object p0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Appendable;[BII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 228336
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228337
    add-int v0, p3, p4

    array-length v2, p2

    invoke-static {p3, v0, v2}, LX/0PB;->checkPositionIndexes(III)V

    .line 228338
    iget-object v0, p0, LX/1I8;->b:LX/1I9;

    iget v0, v0, LX/1I9;->d:I

    if-gt p4, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 228339
    const-wide/16 v2, 0x0

    move v0, v1

    .line 228340
    :goto_1
    if-ge v0, p4, :cond_1

    .line 228341
    add-int v4, p3, v0

    aget-byte v4, p2, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 228342
    const/16 v4, 0x8

    shl-long/2addr v2, v4

    .line 228343
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 228344
    goto :goto_0

    .line 228345
    :cond_1
    add-int/lit8 v0, p4, 0x1

    mul-int/lit8 v0, v0, 0x8

    iget-object v4, p0, LX/1I8;->b:LX/1I9;

    iget v4, v4, LX/1I9;->b:I

    sub-int v4, v0, v4

    move v0, v1

    .line 228346
    :goto_2
    mul-int/lit8 v1, p4, 0x8

    if-ge v0, v1, :cond_2

    .line 228347
    sub-int v1, v4, v0

    ushr-long v6, v2, v1

    long-to-int v1, v6

    iget-object v5, p0, LX/1I8;->b:LX/1I9;

    iget v5, v5, LX/1I9;->a:I

    and-int/2addr v1, v5

    .line 228348
    iget-object v5, p0, LX/1I8;->b:LX/1I9;

    invoke-virtual {v5, v1}, LX/1I9;->a(I)C

    move-result v1

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228349
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    iget v1, v1, LX/1I9;->b:I

    add-int/2addr v0, v1

    .line 228350
    goto :goto_2

    .line 228351
    :cond_2
    iget-object v1, p0, LX/1I8;->c:Ljava/lang/Character;

    if-eqz v1, :cond_3

    .line 228352
    :goto_3
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    iget v1, v1, LX/1I9;->d:I

    mul-int/lit8 v1, v1, 0x8

    if-ge v0, v1, :cond_3

    .line 228353
    iget-object v1, p0, LX/1I8;->c:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228354
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    iget v1, v1, LX/1I9;->b:I

    add-int/2addr v0, v1

    goto :goto_3

    .line 228355
    :cond_3
    return-void
.end method

.method public final c()LX/1I6;
    .locals 7

    .prologue
    .line 228356
    iget-object v0, p0, LX/1I8;->a:LX/1I6;

    .line 228357
    if-nez v0, :cond_1

    .line 228358
    iget-object v0, p0, LX/1I8;->b:LX/1I9;

    const/4 v2, 0x0

    .line 228359
    const/4 v1, 0x0

    .line 228360
    iget-object v4, v0, LX/1I9;->f:[C

    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_0

    aget-char v6, v4, v3

    .line 228361
    invoke-static {v6}, LX/1IV;->isUpperCase(C)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 228362
    const/4 v1, 0x1

    .line 228363
    :cond_0
    move v1, v1

    .line 228364
    if-nez v1, :cond_3

    .line 228365
    :goto_1
    move-object v0, v0

    .line 228366
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    if-ne v0, v1, :cond_2

    move-object v0, p0

    :goto_2
    iput-object v0, p0, LX/1I8;->a:LX/1I6;

    .line 228367
    :cond_1
    return-object v0

    .line 228368
    :cond_2
    iget-object v1, p0, LX/1I8;->c:Ljava/lang/Character;

    invoke-virtual {p0, v0, v1}, LX/1I8;->a(LX/1I9;Ljava/lang/Character;)LX/1I6;

    move-result-object v0

    goto :goto_2

    .line 228369
    :cond_3
    invoke-static {v0}, LX/1I9;->b(LX/1I9;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    const-string v3, "Cannot call lowerCase() on a mixed-case alphabet"

    invoke-static {v1, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 228370
    iget-object v1, v0, LX/1I9;->f:[C

    array-length v1, v1

    new-array v3, v1, [C

    .line 228371
    :goto_4
    iget-object v1, v0, LX/1I9;->f:[C

    array-length v1, v1

    if-ge v2, v1, :cond_6

    .line 228372
    iget-object v1, v0, LX/1I9;->f:[C

    aget-char v1, v1, v2

    .line 228373
    invoke-static {v1}, LX/1IV;->isUpperCase(C)Z

    move-result v4

    if-eqz v4, :cond_4

    xor-int/lit8 v4, v1, 0x20

    int-to-char v1, v4

    :cond_4
    move v1, v1

    .line 228374
    aput-char v1, v3, v2

    .line 228375
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    move v1, v2

    .line 228376
    goto :goto_3

    .line 228377
    :cond_6
    new-instance v1, LX/1I9;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, LX/1I9;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".lowerCase()"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, LX/1I9;-><init>(Ljava/lang/String;[C)V

    move-object v0, v1

    goto :goto_1

    .line 228378
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 228379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BaseEncoding."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 228380
    iget-object v1, p0, LX/1I8;->b:LX/1I9;

    invoke-virtual {v1}, LX/1I9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228381
    const/16 v1, 0x8

    iget-object v2, p0, LX/1I8;->b:LX/1I9;

    iget v2, v2, LX/1I9;->b:I

    rem-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 228382
    iget-object v1, p0, LX/1I8;->c:Ljava/lang/Character;

    if-nez v1, :cond_1

    .line 228383
    const-string v1, ".omitPadding()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228384
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 228385
    :cond_1
    const-string v1, ".withPadChar("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1I8;->c:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
