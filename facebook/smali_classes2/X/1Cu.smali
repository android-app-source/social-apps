.class public LX/1Cu;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:LX/1Cz;

.field public final b:LX/1Cz;

.field public final c:LX/1Cz;

.field public final d:LX/1Cz;

.field public final e:LX/1Cz;

.field public final f:LX/1Cz;

.field public final g:LX/1Cz;

.field public final h:LX/1Cz;

.field public final i:LX/1Cz;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/76X;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18Q;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/1Cx;

.field public final m:LX/0qb;

.field private final n:LX/14z;

.field private final o:[LX/1Cz;

.field public final p:LX/0Wd;

.field public final q:LX/0Sh;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/76T;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/3TD;

.field public t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public u:Z

.field private v:Z

.field private final w:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1Cx;LX/0qb;LX/14z;LX/0Wd;LX/0Sh;LX/0Ot;)V
    .locals 2
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/76X;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/18Q;",
            ">;",
            "LX/1Cx;",
            "LX/0qb;",
            "LX/14z;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/76T;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 217033
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 217034
    new-instance v0, LX/1Cy;

    invoke-direct {v0, p0}, LX/1Cy;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->a:LX/1Cz;

    .line 217035
    new-instance v0, LX/1D0;

    invoke-direct {v0, p0}, LX/1D0;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->b:LX/1Cz;

    .line 217036
    new-instance v0, LX/1D1;

    invoke-direct {v0, p0}, LX/1D1;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->c:LX/1Cz;

    .line 217037
    new-instance v0, LX/1D2;

    invoke-direct {v0, p0}, LX/1D2;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->d:LX/1Cz;

    .line 217038
    new-instance v0, LX/1D3;

    invoke-direct {v0, p0}, LX/1D3;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->e:LX/1Cz;

    .line 217039
    new-instance v0, LX/1D4;

    invoke-direct {v0, p0}, LX/1D4;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->f:LX/1Cz;

    .line 217040
    new-instance v0, LX/1D5;

    invoke-direct {v0, p0}, LX/1D5;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->g:LX/1Cz;

    .line 217041
    new-instance v0, LX/1D6;

    invoke-direct {v0, p0}, LX/1D6;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->h:LX/1Cz;

    .line 217042
    new-instance v0, LX/1D7;

    invoke-direct {v0, p0}, LX/1D7;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->i:LX/1Cz;

    .line 217043
    iput-boolean v1, p0, LX/1Cu;->u:Z

    .line 217044
    iput-boolean v1, p0, LX/1Cu;->v:Z

    .line 217045
    new-instance v0, Lcom/facebook/feed/megaphone/FeedMegaphoneAdapter$10;

    invoke-direct {v0, p0}, Lcom/facebook/feed/megaphone/FeedMegaphoneAdapter$10;-><init>(LX/1Cu;)V

    iput-object v0, p0, LX/1Cu;->w:Ljava/lang/Runnable;

    .line 217046
    iput-object p1, p0, LX/1Cu;->j:LX/0Ot;

    .line 217047
    iput-object p2, p0, LX/1Cu;->k:LX/0Ot;

    .line 217048
    iput-object p3, p0, LX/1Cu;->l:LX/1Cx;

    .line 217049
    iput-object p4, p0, LX/1Cu;->m:LX/0qb;

    .line 217050
    iput-object p5, p0, LX/1Cu;->n:LX/14z;

    .line 217051
    iput-object p6, p0, LX/1Cu;->p:LX/0Wd;

    .line 217052
    iput-object p7, p0, LX/1Cu;->q:LX/0Sh;

    .line 217053
    iput-object p8, p0, LX/1Cu;->r:LX/0Ot;

    .line 217054
    const/16 v0, 0x9

    new-array v0, v0, [LX/1Cz;

    const/4 v1, 0x0

    iget-object p1, p0, LX/1Cu;->a:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x1

    iget-object p1, p0, LX/1Cu;->b:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x2

    iget-object p1, p0, LX/1Cu;->c:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x3

    iget-object p1, p0, LX/1Cu;->g:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x4

    iget-object p1, p0, LX/1Cu;->h:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x5

    iget-object p1, p0, LX/1Cu;->d:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x6

    iget-object p1, p0, LX/1Cu;->e:LX/1Cz;

    aput-object p1, v0, v1

    const/4 v1, 0x7

    iget-object p1, p0, LX/1Cu;->f:LX/1Cz;

    aput-object p1, v0, v1

    const/16 v1, 0x8

    iget-object p1, p0, LX/1Cu;->i:LX/1Cz;

    aput-object p1, v0, v1

    move-object v0, v0

    .line 217055
    iput-object v0, p0, LX/1Cu;->o:[LX/1Cz;

    .line 217056
    return-void
.end method

.method private a(LX/1Cz;Landroid/view/View;)V
    .locals 12

    .prologue
    .line 217162
    iget-object v0, p0, LX/1Cu;->b:LX/1Cz;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/1Cu;->g:LX/1Cz;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/1Cu;->h:LX/1Cz;

    if-ne p1, v0, :cond_2

    .line 217163
    :cond_0
    check-cast p2, LX/B9N;

    iget-object v0, p0, LX/1Cu;->s:LX/3TD;

    invoke-interface {p2, v0}, LX/B9N;->setMegaphoneStory(LX/3TD;)V

    .line 217164
    :cond_1
    :goto_0
    return-void

    .line 217165
    :cond_2
    iget-object v0, p0, LX/1Cu;->c:LX/1Cz;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, LX/1Cu;->d:LX/1Cz;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, LX/1Cu;->e:LX/1Cz;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, LX/1Cu;->f:LX/1Cz;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, LX/1Cu;->i:LX/1Cz;

    if-ne p1, v0, :cond_1

    instance-of v0, p2, LX/76V;

    if-eqz v0, :cond_1

    .line 217166
    :cond_3
    sget-object v0, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-static {v0}, LX/1Cx;->c(LX/0rH;)Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v1

    move-object v0, p2

    .line 217167
    check-cast v0, LX/76V;

    iget-object v2, p0, LX/1Cu;->w:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, LX/76V;->setOnDismiss(Ljava/lang/Runnable;)V

    move-object v0, p2

    .line 217168
    check-cast v0, LX/76V;

    iget-object v2, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    sget-object v3, LX/1XZ;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, LX/76V;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 217169
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/1Cu;->v:Z

    if-eqz v0, :cond_1

    .line 217170
    iget-object v0, p0, LX/1Cu;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76T;

    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 217171
    iget-object v4, v0, LX/76T;->a:LX/0W3;

    sget-wide v6, LX/0X5;->lJ:J

    invoke-interface {v4, v6, v7, v10}, LX/0W4;->a(JZ)Z

    move-result v5

    .line 217172
    iget-object v4, v0, LX/76T;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    invoke-virtual {v4}, LX/3RX;->a()Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v5, :cond_4

    .line 217173
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Cu;->v:Z

    goto :goto_0

    .line 217174
    :cond_4
    iget-object v4, v0, LX/76T;->a:LX/0W3;

    sget-wide v6, LX/0X5;->lI:J

    invoke-interface {v4, v6, v7, v10}, LX/0W4;->a(JI)I

    move-result v4

    .line 217175
    iget-object v5, v0, LX/76T;->a:LX/0W3;

    sget-wide v6, LX/0X5;->lH:J

    const-wide/16 v8, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, LX/0W4;->a(JD)D

    move-result-wide v6

    double-to-float v6, v6

    .line 217176
    iget-object v5, v0, LX/76T;->a:LX/0W3;

    sget-wide v8, LX/0X5;->lK:J

    invoke-interface {v5, v8, v9, v10}, LX/0W4;->a(JZ)Z

    move-result v5

    .line 217177
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 217178
    :pswitch_1
    if-eqz v5, :cond_5

    .line 217179
    iget-object v4, v0, LX/76T;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    const-string v5, "qp_alert_notify_1"

    invoke-virtual {v4, v5}, LX/3RX;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 217180
    :cond_5
    iget-object v4, v0, LX/76T;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    iget-object v5, v0, LX/76T;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7Cc;

    const-string v7, "qp_alert_notify_1"

    invoke-virtual {v5, v7}, LX/7Cc;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v11, v6}, LX/3RX;->a(IIF)LX/7Cb;

    goto :goto_1

    .line 217181
    :pswitch_2
    if-eqz v5, :cond_6

    .line 217182
    iget-object v4, v0, LX/76T;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    const-string v5, "qp_alert_notify_4"

    invoke-virtual {v4, v5}, LX/3RX;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 217183
    :cond_6
    iget-object v4, v0, LX/76T;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3RX;

    iget-object v5, v0, LX/76T;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7Cc;

    const-string v7, "qp_alert_notify_4"

    invoke-virtual {v5, v7}, LX/7Cc;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v11, v6}, LX/3RX;->a(IIF)LX/7Cb;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static g(LX/1Cu;)Z
    .locals 1

    .prologue
    .line 217161
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1Cu;->j(LX/1Cu;)Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/1Cu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 217147
    iget-object v0, p0, LX/1Cu;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18Q;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 217148
    invoke-static {v0}, LX/18Q;->a(LX/18Q;)Landroid/os/Handler;

    .line 217149
    iget-object v3, v0, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/18R;

    .line 217150
    if-eqz v3, :cond_3

    .line 217151
    iget-object v3, v3, LX/18R;->a:LX/3TD;

    .line 217152
    :goto_0
    move-object v2, v3

    .line 217153
    invoke-direct {p0}, LX/1Cu;->i()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v3

    .line 217154
    iget-object v0, p0, LX/1Cu;->s:LX/3TD;

    if-ne v2, v0, :cond_0

    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eq v3, v0, :cond_2

    :cond_0
    move v0, v1

    .line 217155
    :goto_1
    iput-object v2, p0, LX/1Cu;->s:LX/3TD;

    .line 217156
    iput-object v3, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 217157
    invoke-static {p0}, LX/1Cu;->g(LX/1Cu;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 217158
    iput-boolean v1, p0, LX/1Cu;->v:Z

    .line 217159
    :cond_1
    return v0

    .line 217160
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private i()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 217126
    sget-object v0, LX/0rH;->NEWS_FEED:LX/0rH;

    .line 217127
    iget-object v1, p0, LX/1Cu;->m:LX/0qb;

    iget-object v2, p0, LX/1Cu;->l:LX/1Cx;

    invoke-virtual {v1, v2, v0}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217128
    iget-object v1, p0, LX/1Cu;->m:LX/0qb;

    invoke-virtual {v1}, LX/0qb;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 217129
    iget-object v1, p0, LX/1Cu;->l:LX/1Cx;

    invoke-virtual {v1, v0}, LX/1Cx;->a(LX/0rH;)LX/0qi;

    .line 217130
    :cond_0
    iget-object v1, p0, LX/1Cu;->l:LX/1Cx;

    const/4 v3, 0x0

    .line 217131
    invoke-static {v0}, LX/1Cx;->c(LX/0rH;)Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v2

    .line 217132
    if-nez v2, :cond_2

    move-object v2, v3

    .line 217133
    :goto_0
    move-object v0, v2

    .line 217134
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 217135
    :cond_2
    iget-object v4, v1, LX/1Cx;->b:LX/0iA;

    sget-object v5, LX/1XZ;->a:Ljava/lang/String;

    const-class p0, LX/13D;

    invoke-virtual {v4, v5, p0, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/13D;

    .line 217136
    if-nez v2, :cond_3

    move-object v2, v3

    .line 217137
    goto :goto_0

    .line 217138
    :cond_3
    iget-object v3, v2, LX/13D;->a:LX/13K;

    .line 217139
    iget-object v4, v3, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-nez v4, :cond_4

    .line 217140
    const/4 v4, 0x0

    .line 217141
    :goto_2
    move-object v3, v4

    .line 217142
    move-object v2, v3

    .line 217143
    goto :goto_0

    .line 217144
    :cond_4
    iget-object v4, v3, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v2, v3, LX/13K;->r:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v3, v4, v2}, LX/13K;->a(LX/13K;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 217145
    invoke-static {v3}, LX/13K;->h(LX/13K;)V

    .line 217146
    :cond_5
    iget-object v4, v3, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    goto :goto_2
.end method

.method private static j(LX/1Cu;)Lcom/facebook/graphql/model/GraphQLMegaphone;
    .locals 1

    .prologue
    .line 217123
    iget-object v0, p0, LX/1Cu;->s:LX/3TD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Cu;->s:LX/3TD;

    .line 217124
    iget-object p0, v0, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v0, p0

    .line 217125
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()LX/1Cz;
    .locals 3

    .prologue
    .line 217102
    invoke-static {p0}, LX/1Cu;->j(LX/1Cu;)Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v0

    .line 217103
    if-eqz v0, :cond_2

    .line 217104
    iget-object v1, p0, LX/1Cu;->s:LX/3TD;

    .line 217105
    iget-object v2, v1, LX/3TD;->a:Ljava/lang/String;

    move-object v1, v2

    .line 217106
    if-eqz v1, :cond_0

    .line 217107
    iget-object v0, p0, LX/1Cu;->h:LX/1Cz;

    .line 217108
    :goto_0
    return-object v0

    .line 217109
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->v()Ljava/lang/String;

    move-result-object v0

    const-string v2, "2.0"

    invoke-static {v0, v2}, LX/14z;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    .line 217110
    iget-object v0, p0, LX/1Cu;->g:LX/1Cz;

    goto :goto_0

    .line 217111
    :cond_1
    iget-object v0, p0, LX/1Cu;->b:LX/1Cz;

    goto :goto_0

    .line 217112
    :cond_2
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v0, :cond_7

    .line 217113
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->BRANDED_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_3

    .line 217114
    iget-object v0, p0, LX/1Cu;->d:LX/1Cz;

    goto :goto_0

    .line 217115
    :cond_3
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SURVEY_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_4

    .line 217116
    iget-object v0, p0, LX/1Cu;->e:LX/1Cz;

    goto :goto_0

    .line 217117
    :cond_4
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->BLAST_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_5

    .line 217118
    iget-object v0, p0, LX/1Cu;->f:LX/1Cz;

    goto :goto_0

    .line 217119
    :cond_5
    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CUSTOM_RENDERED:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_6

    .line 217120
    iget-object v0, p0, LX/1Cu;->i:LX/1Cz;

    goto :goto_0

    .line 217121
    :cond_6
    iget-object v0, p0, LX/1Cu;->c:LX/1Cz;

    goto :goto_0

    .line 217122
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MegaphoneController#hasMegaphone returned true for a megaphone with no view type mapped."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 217101
    iget-object v0, p0, LX/1Cu;->o:[LX/1Cz;

    aget-object v0, v0, p1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 217099
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 217100
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 217086
    const-string v0, "FeedMegaphoneAdapter.bindView"

    const v1, 0x7762e99a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 217087
    :try_start_0
    iget-object v0, p0, LX/1Cu;->o:[LX/1Cz;

    aget-object v0, v0, p4

    .line 217088
    iget-object v1, p0, LX/1Cu;->a:LX/1Cz;

    if-ne p2, v1, :cond_0

    .line 217089
    invoke-static {p0}, LX/1Cu;->g(LX/1Cu;)Z

    move-result v0

    .line 217090
    const v1, 0x7f0d117c

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz v0, :cond_3

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217091
    :goto_1
    const v0, -0x203b4c5f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 217092
    return-void

    .line 217093
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/1Cu;->b:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->c:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->h:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->g:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->d:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->e:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->f:LX/1Cz;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, LX/1Cu;->i:LX/1Cz;

    if-ne v0, v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 217094
    if-eqz v1, :cond_2

    .line 217095
    invoke-direct {p0, v0, p3}, LX/1Cu;->a(LX/1Cz;Landroid/view/View;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 217096
    :catchall_0
    move-exception v0

    const v1, -0x77514111

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 217097
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 217098
    :cond_3
    :try_start_3
    const/4 v1, 0x0

    goto :goto_0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 217082
    invoke-static {p0}, LX/1Cu;->h(LX/1Cu;)Z

    move-result v0

    .line 217083
    if-eqz v0, :cond_0

    .line 217084
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 217085
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 217081
    invoke-static {p0}, LX/1Cu;->g(LX/1Cu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 217075
    invoke-virtual {p0}, LX/1Cu;->getCount()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 217076
    if-nez p1, :cond_0

    .line 217077
    iget-object v0, p0, LX/1Cu;->a:LX/1Cz;

    .line 217078
    :goto_0
    return-object v0

    .line 217079
    :cond_0
    invoke-static {p0}, LX/1Cu;->g(LX/1Cu;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 217080
    invoke-direct {p0}, LX/1Cu;->k()LX/1Cz;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 217074
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 217068
    invoke-virtual {p0, p1}, LX/1Cu;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 217069
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, LX/1Cu;->o:[LX/1Cz;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 217070
    iget-object v2, p0, LX/1Cu;->o:[LX/1Cz;

    aget-object v2, v2, v1

    if-ne v0, v2, :cond_0

    .line 217071
    return v1

    .line 217072
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217073
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217061
    iget-object v0, p0, LX/1Cu;->o:[LX/1Cz;

    invoke-virtual {p0, p1}, LX/1Cu;->getItemViewType(I)I

    move-result v1

    aget-object v0, v0, v1

    .line 217062
    if-eqz p2, :cond_0

    iget-object v1, p0, LX/1Cu;->i:LX/1Cz;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v0, p0, LX/1Cu;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76X;

    iget-object v2, p0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    .line 217063
    iget-object v3, v0, LX/76X;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/76W;

    .line 217064
    if-nez v3, :cond_1

    const/4 v3, 0x0

    :goto_0
    move-object v0, v3

    .line 217065
    if-eq v1, v0, :cond_0

    .line 217066
    const/4 p2, 0x0

    .line 217067
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-interface {v3}, LX/76W;->b()Ljava/lang/Class;

    move-result-object v3

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 217060
    iget-object v0, p0, LX/1Cu;->o:[LX/1Cz;

    array-length v0, v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 217057
    invoke-static {p0}, LX/1Cu;->h(LX/1Cu;)Z

    .line 217058
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 217059
    return-void
.end method
