.class public LX/1E7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field public final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(LX/0ad;Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 219206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219207
    iput-object p1, p0, LX/1E7;->a:LX/0ad;

    .line 219208
    iput-object p2, p0, LX/1E7;->b:Landroid/content/Context;

    .line 219209
    return-void
.end method

.method public static b(LX/0QB;)LX/1E7;
    .locals 3

    .prologue
    .line 219210
    new-instance v2, LX/1E7;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/1E7;-><init>(LX/0ad;Landroid/content/Context;)V

    .line 219211
    return-object v2
.end method

.method public static c(LX/1E7;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 219212
    iget-object v0, p0, LX/1E7;->a:LX/0ad;

    sget-char v1, LX/1EC;->h:C

    const-string v2, "CONTROL"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
