.class public LX/1MX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MY;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1MX;


# instance fields
.field private final a:LX/1MZ;


# direct methods
.method public constructor <init>(LX/1MZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235680
    iput-object p1, p0, LX/1MX;->a:LX/1MZ;

    .line 235681
    return-void
.end method

.method public static a(LX/0QB;)LX/1MX;
    .locals 4

    .prologue
    .line 235688
    sget-object v0, LX/1MX;->b:LX/1MX;

    if-nez v0, :cond_1

    .line 235689
    const-class v1, LX/1MX;

    monitor-enter v1

    .line 235690
    :try_start_0
    sget-object v0, LX/1MX;->b:LX/1MX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235691
    if-eqz v2, :cond_0

    .line 235692
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235693
    new-instance p0, LX/1MX;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v3

    check-cast v3, LX/1MZ;

    invoke-direct {p0, v3}, LX/1MX;-><init>(LX/1MZ;)V

    .line 235694
    move-object v0, p0

    .line 235695
    sput-object v0, LX/1MX;->b:LX/1MX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235696
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235697
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235698
    :cond_1
    sget-object v0, LX/1MX;->b:LX/1MX;

    return-object v0

    .line 235699
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235700
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/763;
    .locals 2

    .prologue
    .line 235682
    sget-object v0, LX/76F;->a:[I

    iget-object v1, p0, LX/1MX;->a:LX/1MZ;

    invoke-virtual {v1}, LX/1MZ;->b()LX/1Mb;

    move-result-object v1

    invoke-virtual {v1}, LX/1Mb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235683
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 235684
    :pswitch_0
    sget-object v0, LX/763;->CONNECTED:LX/763;

    .line 235685
    :goto_0
    return-object v0

    .line 235686
    :pswitch_1
    sget-object v0, LX/763;->CONNECTING:LX/763;

    goto :goto_0

    .line 235687
    :pswitch_2
    sget-object v0, LX/763;->DISCONNECTED:LX/763;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
