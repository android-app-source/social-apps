.class public LX/13P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/13N;

.field private final c:LX/0iA;

.field private final d:LX/0ka;

.field private final e:LX/0u7;

.field private final f:LX/0kb;

.field private final g:LX/0lC;

.field public final h:LX/13Q;


# direct methods
.method public constructor <init>(LX/0Zb;LX/13N;LX/0iA;LX/0ka;LX/0u7;LX/0kb;LX/0lC;LX/13Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176747
    iput-object p1, p0, LX/13P;->a:LX/0Zb;

    .line 176748
    iput-object p2, p0, LX/13P;->b:LX/13N;

    .line 176749
    iput-object p3, p0, LX/13P;->c:LX/0iA;

    .line 176750
    iput-object p4, p0, LX/13P;->d:LX/0ka;

    .line 176751
    iput-object p5, p0, LX/13P;->e:LX/0u7;

    .line 176752
    iput-object p6, p0, LX/13P;->f:LX/0kb;

    .line 176753
    iput-object p7, p0, LX/13P;->g:LX/0lC;

    .line 176754
    iput-object p8, p0, LX/13P;->h:LX/13Q;

    .line 176755
    return-void
.end method

.method public static a(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 4

    .prologue
    .line 176682
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {p1, v0}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 176683
    if-eqz p3, :cond_0

    .line 176684
    const-string v0, "trigger"

    iget-object v1, p3, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176685
    :cond_0
    const-string v0, "impression_count"

    iget-object v1, p0, LX/13P;->b:LX/13N;

    sget-object v2, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-virtual {v1, p2, v2}, LX/13N;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176686
    const-string v0, "last_impression_timestamp"

    iget-object v1, p0, LX/13P;->b:LX/13N;

    sget-object v2, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-virtual {v1, p2, v2}, LX/13N;->d(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176687
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 176688
    iget-object v0, p0, LX/13P;->g:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v2

    .line 176689
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176690
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 176691
    :cond_1
    const-string v0, "instance_log_data"

    invoke-virtual {p1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176692
    :cond_2
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 176693
    const-string v0, "quick_promotion"

    .line 176694
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 176695
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176696
    const-string v0, "promotion_id"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176697
    return-void
.end method

.method public static b(LX/0QB;)LX/13P;
    .locals 9

    .prologue
    .line 176698
    new-instance v0, LX/13P;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/13N;->a(LX/0QB;)LX/13N;

    move-result-object v2

    check-cast v2, LX/13N;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v4

    check-cast v4, LX/0ka;

    invoke-static {p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v5

    check-cast v5, LX/0u7;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v8

    check-cast v8, LX/13Q;

    invoke-direct/range {v0 .. v8}, LX/13P;-><init>(LX/0Zb;LX/13N;LX/0iA;LX/0ka;LX/0u7;LX/0kb;LX/0lC;LX/13Q;)V

    .line 176699
    return-object v0
.end method

.method public static b(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 176700
    const-string v0, "battery_percentage"

    iget-object v1, p0, LX/13P;->e:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->a()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176701
    const-string v0, "charging_state"

    iget-object v1, p0, LX/13P;->e:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->b()LX/0y1;

    move-result-object v1

    invoke-virtual {v1}, LX/0y1;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176702
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 176703
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 176704
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 176705
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 176706
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 176707
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 176708
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 176709
    const-string v2, "seconds_from_midnight"

    invoke-virtual {p1, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176710
    iget-object v0, p0, LX/13P;->d:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176711
    sget-object v0, LX/2zP;->WIFI:LX/2zP;

    .line 176712
    :goto_0
    const-string v1, "connection"

    invoke-virtual {v0}, LX/2zP;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176713
    return-void

    .line 176714
    :cond_0
    iget-object v0, p0, LX/13P;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176715
    sget-object v0, LX/2zP;->CELLULAR:LX/2zP;

    goto :goto_0

    .line 176716
    :cond_1
    sget-object v0, LX/2zP;->NOT_CONNECTED:LX/2zP;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/77m;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 176717
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176718
    invoke-static {v0}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176719
    invoke-static {v0, p2}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 176720
    const-string v1, "object_id"

    invoke-virtual {p1}, LX/77m;->toAnalyticEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176721
    iget-object v1, p0, LX/13P;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176722
    return-void
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 3

    .prologue
    .line 176723
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176724
    sget-object v0, LX/77l;->a:[I

    invoke-virtual {p2}, LX/77m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 176725
    :goto_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176726
    invoke-static {v0}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176727
    const-string v1, "object_id"

    invoke-virtual {p2}, LX/77m;->toAnalyticEventName()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176728
    invoke-static {p0, v0, p3, p5}, LX/13P;->a(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 176729
    if-eqz p1, :cond_0

    .line 176730
    const-string v1, "action_url"

    iget-object p4, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176731
    :cond_0
    invoke-static {p0, v0}, LX/13P;->b(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176732
    iget-object v1, p0, LX/13P;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176733
    return-void

    .line 176734
    :pswitch_0
    iget-object v0, p0, LX/13P;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/interstitial/manager/InterstitialLogger;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 176735
    :pswitch_1
    iget-object v0, p0, LX/13P;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    .line 176736
    sget-object v1, LX/6Xs;->ACTION:LX/6Xs;

    sget-object v2, LX/6Xr;->SECONDARY:LX/6Xr;

    invoke-static {v0, p4, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;LX/6Xr;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 176737
    goto :goto_0

    .line 176738
    :pswitch_2
    iget-object v0, p0, LX/13P;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/interstitial/manager/InterstitialLogger;->d(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 176739
    iget-boolean v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->logEligibilityWaterfall:Z

    if-nez v0, :cond_0

    .line 176740
    :goto_0
    return-void

    .line 176741
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "qp_eligibility_waterfall"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176742
    invoke-static {v0}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176743
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v0, v1}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 176744
    const-string v1, "step"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 176745
    iget-object v1, p0, LX/13P;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
