.class public final LX/0vV;
.super LX/0vW;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0vW",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public transient a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 157923
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, LX/0vW;-><init>(Ljava/util/Map;)V

    .line 157924
    const/4 v0, 0x2

    iput v0, p0, LX/0vV;->a:I

    .line 157925
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 157926
    invoke-static {p1}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0vW;-><init>(Ljava/util/Map;)V

    .line 157927
    const/4 v0, 0x2

    iput v0, p0, LX/0vV;->a:I

    .line 157928
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 157929
    iput p2, p0, LX/0vV;->a:I

    .line 157930
    return-void

    .line 157931
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/0Xu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 157932
    invoke-interface {p1}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0vW;-><init>(Ljava/util/Map;)V

    .line 157933
    const/4 v0, 0x2

    iput v0, p0, LX/0vV;->a:I

    .line 157934
    invoke-virtual {p0, p1}, LX/0vV;->a(LX/0Xu;)Z

    .line 157935
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 157936
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 157937
    const/4 v0, 0x2

    iput v0, p0, LX/0vV;->a:I

    .line 157938
    invoke-static {p1}, LX/50X;->a(Ljava/io/ObjectInputStream;)I

    move-result v0

    .line 157939
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 157940
    invoke-virtual {p0, v1}, LX/0Xs;->a(Ljava/util/Map;)V

    .line 157941
    invoke-static {p0, p1, v0}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectInputStream;I)V

    .line 157942
    return-void
.end method

.method public static u()LX/0vV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0vV",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 157949
    new-instance v0, LX/0vV;

    invoke-direct {v0}, LX/0vV;-><init>()V

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 157943
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 157944
    invoke-static {p0, p1}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectOutputStream;)V

    .line 157945
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 157946
    iget v0, p0, LX/0vV;->a:I

    invoke-static {v0}, LX/0RA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/0Xu;)Z
    .locals 1

    .prologue
    .line 157947
    invoke-super {p0, p1}, LX/0vW;->a(LX/0Xu;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157948
    invoke-super {p0, p1, p2}, LX/0vW;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157921
    invoke-super {p0, p1, p2}, LX/0vW;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 157922
    invoke-virtual {p0}, LX/0vV;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157910
    invoke-super {p0, p1, p2}, LX/0vW;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .locals 1

    .prologue
    .line 157911
    invoke-super {p0}, LX/0vW;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157912
    invoke-super {p0, p1}, LX/0vW;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic g()V
    .locals 0

    .prologue
    .line 157913
    invoke-super {p0}, LX/0vW;->g()V

    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 157914
    invoke-super {p0, p1}, LX/0vW;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 157915
    invoke-super {p0}, LX/0vW;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 157916
    invoke-super {p0}, LX/0vW;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .locals 1

    .prologue
    .line 157917
    invoke-super {p0}, LX/0vW;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .locals 1

    .prologue
    .line 157918
    invoke-super {p0}, LX/0vW;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()LX/1M1;
    .locals 1

    .prologue
    .line 157919
    invoke-super {p0}, LX/0vW;->q()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157920
    invoke-super {p0}, LX/0vW;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
