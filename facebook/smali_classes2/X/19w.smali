.class public LX/19w;
.super LX/0Tr;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ConstructorMayLeakThis"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final c:LX/19x;

.field private static volatile l:LX/19w;


# instance fields
.field public final b:Ljava/lang/String;

.field public final d:LX/0tQ;

.field private final e:Landroid/content/Context;

.field public f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/7Jg;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0TD;

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;

.field private i:J

.field public j:LX/0SG;

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209326
    const-class v0, LX/19w;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19w;->a:Ljava/lang/String;

    .line 209327
    new-instance v0, LX/19x;

    invoke-direct {v0}, LX/19x;-><init>()V

    sput-object v0, LX/19w;->c:LX/19x;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/19x;LX/1A1;LX/1A4;LX/0TD;LX/0tQ;LX/0SG;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 209328
    invoke-static {p3, p4, p5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "savedvideos.db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 209329
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/19w;->i:J

    .line 209330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/SavedVideos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19w;->b:Ljava/lang/String;

    .line 209331
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    .line 209332
    iput-object p6, p0, LX/19w;->g:LX/0TD;

    .line 209333
    iput-object p7, p0, LX/19w;->d:LX/0tQ;

    .line 209334
    iput-object p8, p0, LX/19w;->j:LX/0SG;

    .line 209335
    iput-object p1, p0, LX/19w;->e:Landroid/content/Context;

    .line 209336
    invoke-direct {p0}, LX/19w;->w()V

    .line 209337
    iget-object v0, p0, LX/19w;->g:LX/0TD;

    new-instance v1, LX/1A7;

    invoke-direct {v1, p0}, LX/1A7;-><init>(LX/19w;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/19w;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 209338
    return-void
.end method

.method public static a(LX/0QB;)LX/19w;
    .locals 12

    .prologue
    .line 209339
    sget-object v0, LX/19w;->l:LX/19w;

    if-nez v0, :cond_1

    .line 209340
    const-class v1, LX/19w;

    monitor-enter v1

    .line 209341
    :try_start_0
    sget-object v0, LX/19w;->l:LX/19w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 209342
    if-eqz v2, :cond_0

    .line 209343
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 209344
    new-instance v3, LX/19w;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/19x;->a(LX/0QB;)LX/19x;

    move-result-object v6

    check-cast v6, LX/19x;

    invoke-static {v0}, LX/1A1;->a(LX/0QB;)LX/1A1;

    move-result-object v7

    check-cast v7, LX/1A1;

    invoke-static {v0}, LX/1A4;->a(LX/0QB;)LX/1A4;

    move-result-object v8

    check-cast v8, LX/1A4;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v10

    check-cast v10, LX/0tQ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-direct/range {v3 .. v11}, LX/19w;-><init>(Landroid/content/Context;LX/0Tt;LX/19x;LX/1A1;LX/1A4;LX/0TD;LX/0tQ;LX/0SG;)V

    .line 209345
    move-object v0, v3

    .line 209346
    sput-object v0, LX/19w;->l:LX/19w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209347
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 209348
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209349
    :cond_1
    sget-object v0, LX/19w;->l:LX/19w;

    return-object v0

    .line 209350
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 209351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(JJ)Z
    .locals 5

    .prologue
    .line 209352
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p1

    div-long/2addr v0, p3

    .line 209353
    iget-object v2, p0, LX/19w;->d:LX/0tQ;

    .line 209354
    iget-object v3, v2, LX/0tQ;->a:LX/0ad;

    sget v4, LX/0wh;->G:I

    const/16 p0, 0x64

    invoke-interface {v3, v4, p0}, LX/0ad;->a(II)I

    move-result v3

    move v2, v3

    .line 209355
    int-to-long v2, v2

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    .line 209356
    const/4 v0, 0x1

    .line 209357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/7Jg;)Z
    .locals 7

    .prologue
    .line 209358
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209359
    const v0, -0x711a0328

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209360
    :try_start_0
    iget-object v0, p1, LX/7Jg;->a:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 209361
    const-string v4, "saved_videos_analytics"

    sget-object v5, LX/1A4;->c:Ljava/lang/String;

    new-array v6, v2, [Ljava/lang/String;

    aput-object v0, v6, v3

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_1

    .line 209362
    :goto_0
    iget-object v0, p1, LX/7Jg;->a:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 209363
    const-string v4, "saved_videos"

    sget-object v5, LX/19x;->g:Ljava/lang/String;

    new-array v6, v2, [Ljava/lang/String;

    aput-object v0, v6, v3

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_2

    .line 209364
    :goto_1
    iget-object v0, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-static {v1, v0}, LX/1A1;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 209365
    iget-boolean v0, p1, LX/7Jg;->o:Z

    if-nez v0, :cond_0

    .line 209366
    iget-wide v2, p0, LX/19w;->i:J

    iget-wide v4, p1, LX/7Jg;->d:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/19w;->i:J

    .line 209367
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209368
    const v0, 0x4d84ca2f    # 2.78480352E8f

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209369
    const/4 v0, 0x1

    return v0

    .line 209370
    :catch_0
    move-exception v0

    .line 209371
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception in deleting video"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209372
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209373
    :catchall_0
    move-exception v0

    const v2, -0xc8386e

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_1
    :try_start_2
    goto :goto_0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    goto :goto_1
.end method

.method public static s(LX/19w;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 209374
    iget-boolean v0, p0, LX/19w;->k:Z

    if-eqz v0, :cond_0

    .line 209375
    :goto_0
    return-void

    .line 209376
    :cond_0
    iget-object v0, p0, LX/19w;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 209377
    :try_start_0
    invoke-static {p0}, LX/19w;->u(LX/19w;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209378
    :catch_0
    move-exception v0

    .line 209379
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception initializing db"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209380
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move v0, v1

    .line 209381
    goto :goto_1
.end method

.method public static declared-synchronized u(LX/19w;)V
    .locals 8

    .prologue
    .line 209382
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/19w;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    .line 209383
    :goto_0
    monitor-exit p0

    return-void

    .line 209384
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209385
    const v0, 0x34e71ced

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 209386
    :try_start_2
    invoke-static {v1}, LX/19x;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v0

    .line 209387
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209388
    iget-boolean v3, v0, LX/7Jg;->o:Z

    if-nez v3, :cond_1

    .line 209389
    iget-wide v4, p0, LX/19w;->i:J

    iget-wide v6, v0, LX/7Jg;->d:J

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/19w;->i:J

    .line 209390
    :cond_1
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v3, v4, :cond_2

    iget-wide v4, v0, LX/7Jg;->d:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    new-instance v3, Ljava/io/File;

    iget-object v4, v0, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/7Jg;->k:Ljava/lang/String;

    if-eqz v3, :cond_3

    new-instance v3, Ljava/io/File;

    iget-object v4, v0, LX/7Jg;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 209391
    :cond_2
    invoke-direct {p0, v0}, LX/19w;->a(LX/7Jg;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 209392
    :catchall_0
    move-exception v0

    const v2, -0x4ce282d4

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 209393
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 209394
    :cond_3
    :try_start_4
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v3, v4, :cond_4

    .line 209395
    iget-object v0, v0, LX/7Jg;->a:Ljava/lang/String;

    sget-object v3, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    invoke-static {v1, v0, v3}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/1A0;)LX/7Jg;

    move-result-object v0

    .line 209396
    iget-object v0, v0, LX/7Jg;->a:Ljava/lang/String;

    sget-object v3, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-static {v1, v0, v3}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/1A0;)LX/7Jg;

    move-result-object v0

    .line 209397
    :cond_4
    iget-object v3, p0, LX/19w;->f:Ljava/util/HashMap;

    iget-object v4, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 209398
    :cond_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 209399
    const v0, -0x52b085b0

    :try_start_5
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209400
    invoke-direct {p0}, LX/19w;->v()V

    .line 209401
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/19w;->k:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0
.end method

.method private declared-synchronized v()V
    .locals 4

    .prologue
    .line 209402
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209403
    const v0, -0x4cbc6654

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 209404
    :try_start_1
    invoke-static {v1}, LX/1A1;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v0

    .line 209405
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 209406
    iget-object v3, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 209407
    invoke-static {v1, v0}, LX/1A1;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209408
    :catchall_0
    move-exception v0

    const v2, -0x27a5c786

    :try_start_2
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209409
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 209410
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209411
    const v0, 0x316175d1

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209412
    monitor-exit p0

    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 209413
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/19w;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209414
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 209415
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 209416
    :cond_0
    :goto_0
    return-void

    .line 209417
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209418
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 209419
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(ZZ)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209420
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 209421
    :try_start_0
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209422
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209423
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209424
    iget-boolean v3, v0, LX/7Jg;->o:Z

    if-eqz v3, :cond_2

    if-eqz p1, :cond_1

    .line 209425
    :cond_2
    iget-object v0, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 209426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 209427
    :cond_3
    :try_start_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/1A0;)LX/7Jg;
    .locals 9

    .prologue
    .line 209428
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209429
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 209430
    const v0, -0x690461e0

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209431
    :try_start_0
    invoke-static {v2, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209432
    if-nez v0, :cond_0

    .line 209433
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown video id "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209434
    :catch_0
    move-exception v0

    .line 209435
    :try_start_1
    sget-object v1, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209436
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209437
    :catchall_0
    move-exception v0

    const v1, 0x3cc25cbc

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 209438
    :cond_0
    :try_start_2
    iget-object v1, v0, LX/7Jg;->l:LX/1A0;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v1, p2, :cond_1

    .line 209439
    const v1, -0x120ec6eb

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    :goto_0
    return-object v0

    .line 209440
    :cond_1
    :try_start_3
    sget-object v0, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-eq p2, v0, :cond_2

    sget-object v0, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne p2, v0, :cond_3

    .line 209441
    :cond_2
    invoke-static {v2, p1}, LX/1A4;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jf;

    move-result-object v0

    .line 209442
    if-nez v0, :cond_6

    .line 209443
    :cond_3
    :goto_1
    invoke-static {v2, p1, p2}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/1A0;)LX/7Jg;

    move-result-object v1

    .line 209444
    iget-object v0, v1, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v0, v3, :cond_4

    iget-object v0, v1, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-ne v0, v3, :cond_5

    .line 209445
    :cond_4
    iget-object v0, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 209446
    invoke-static {v2, p1}, LX/1A4;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jf;

    move-result-object v0

    .line 209447
    if-nez v0, :cond_7

    .line 209448
    :cond_5
    :goto_2
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209449
    :try_start_4
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 209450
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209451
    iget-object v3, v1, LX/7Jg;->l:LX/1A0;

    iput-object v3, v0, LX/7Jg;->l:LX/1A0;

    .line 209452
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209453
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 209454
    const v0, 0x2cfb55d5

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    move-object v0, v1

    goto :goto_0

    .line 209455
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 209456
    :cond_6
    :try_start_8
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 209457
    sget-object v3, LX/1A6;->b:LX/0U1;

    .line 209458
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 209459
    iget v4, v0, LX/7Jf;->b:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209460
    const-string v3, "saved_videos_analytics"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/1A6;->a:LX/0U1;

    .line 209461
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209462
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "= ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v0, v0, LX/7Jf;->a:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 209463
    :cond_7
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 209464
    sget-object v6, LX/1A6;->d:LX/0U1;

    .line 209465
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 209466
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209467
    const-string v6, "saved_videos_analytics"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/1A6;->a:LX/0U1;

    .line 209468
    iget-object p2, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, p2

    .line 209469
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "= ?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 p2, 0x0

    iget-object v0, v0, LX/7Jf;->a:Ljava/lang/String;

    aput-object v0, v8, p2

    invoke-virtual {v2, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public final a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/7Jh;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 209470
    iget-object v0, p0, LX/19w;->g:LX/0TD;

    new-instance v1, LX/7JY;

    invoke-direct {v1, p0, p1}, LX/7JY;-><init>(LX/19w;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1A0;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1A0;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209471
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209472
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209473
    const v0, -0x6c0d176

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209474
    :try_start_0
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 209475
    sget-object v2, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-ne p1, v2, :cond_1

    .line 209476
    sget-object v2, LX/19x;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 209477
    :cond_0
    :goto_0
    move-object v0, v0

    .line 209478
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209479
    const v2, 0x5f7c7767

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209480
    return-object v0

    .line 209481
    :catch_0
    move-exception v0

    .line 209482
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209483
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209484
    :catchall_0
    move-exception v0

    const v2, -0x3b1f8910

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 209485
    :cond_1
    sget-object v2, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-ne p1, v2, :cond_0

    .line 209486
    sget-object v2, LX/19x;->e:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/2ft;LX/1A0;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2ft;",
            "LX/1A0;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209487
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209488
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209489
    const v0, 0xc1d3bd8

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209490
    :try_start_0
    iget v0, p1, LX/2ft;->mValue:I

    iget v2, p2, LX/1A0;->mValue:I

    .line 209491
    sget-object v3, LX/19x;->f:Ljava/lang/String;

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/String;

    const/4 p1, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p0, p1

    const/4 p1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p0, p1

    const/4 p1, -0x1

    invoke-static {v1, v3, p0, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    move-object v0, v3

    .line 209492
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209493
    const v2, 0x5397e0fe

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209494
    return-object v0

    .line 209495
    :catch_0
    move-exception v0

    .line 209496
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209497
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209498
    :catchall_0
    move-exception v0

    const v2, -0x4e207f31

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/7Jg;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 209499
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209500
    iget-wide v0, p1, LX/7Jg;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 209501
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209502
    const v0, -0x6f0be42

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209503
    :try_start_0
    iget-object v0, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p1, LX/7Jg;->p:J

    .line 209504
    iget-object v0, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p1, LX/7Jg;->m:J

    .line 209505
    invoke-static {v1, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)Z

    .line 209506
    iget-object v0, p1, LX/7Jg;->a:Ljava/lang/String;

    iget-object v2, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v1, v0, v2, v3, p2}, LX/1A4;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;JLjava/lang/String;)Z

    .line 209507
    iget-boolean v0, p1, LX/7Jg;->o:Z

    if-nez v0, :cond_0

    .line 209508
    iget-wide v2, p0, LX/19w;->i:J

    iget-wide v4, p1, LX/7Jg;->d:J

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/19w;->i:J

    .line 209509
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 209510
    :try_start_1
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    iget-object v2, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209511
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209512
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209513
    const v0, -0xabdcb19

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209514
    return-void

    .line 209515
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 209516
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209517
    :catch_0
    move-exception v0

    .line 209518
    :try_start_5
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209519
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 209520
    :catchall_1
    move-exception v0

    const v2, -0x422c8a8a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/7Jh;)V
    .locals 8

    .prologue
    .line 209521
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209522
    const v0, -0x5134079c

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209523
    :try_start_0
    iget-object v0, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 209524
    invoke-static {p1}, LX/1A1;->a(LX/7Jh;)V

    .line 209525
    const/4 v0, 0x1

    .line 209526
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 209527
    if-eqz v0, :cond_0

    .line 209528
    sget-object v5, LX/1A3;->a:LX/0U1;

    .line 209529
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209530
    iget-object v6, p1, LX/7Jh;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209531
    sget-object v5, LX/1A3;->d:LX/0U1;

    .line 209532
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209533
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209534
    :cond_0
    sget-object v5, LX/1A3;->b:LX/0U1;

    .line 209535
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209536
    iget-object v6, p1, LX/7Jh;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209537
    sget-object v5, LX/1A3;->c:LX/0U1;

    .line 209538
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209539
    iget-object v6, p1, LX/7Jh;->c:[B

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 209540
    sget-object v5, LX/1A3;->e:LX/0U1;

    .line 209541
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209542
    iget-object v6, p1, LX/7Jh;->e:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209543
    move-object v0, v4

    .line 209544
    const-string v4, "saved_video_stories"

    const/4 v5, 0x0

    const/4 v6, 0x5

    const v7, 0x7c37c792

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v1, v4, v5, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, -0x6b75af7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 209545
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209546
    const v0, 0x31ee59d6

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209547
    return-void

    .line 209548
    :catch_0
    move-exception v0

    .line 209549
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception in adding video story record"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209550
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209551
    :catchall_0
    move-exception v0

    const v2, 0x1ca84120

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;J)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 209552
    invoke-virtual {p0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v1

    .line 209553
    if-nez v1, :cond_0

    .line 209554
    :goto_0
    return-void

    .line 209555
    :cond_0
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209556
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 209557
    const v0, 0x2a34e331

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209558
    :try_start_0
    iget-object v0, v1, LX/7Jg;->b:Landroid/net/Uri;

    invoke-static {v0}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 209559
    invoke-static {p2}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 209560
    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v4, v1, LX/7Jg;->d:J

    cmp-long v0, v4, p3

    if-eqz v0, :cond_2

    .line 209561
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v3, v1, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209562
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 209563
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v3, v1, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 209564
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 209565
    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 209566
    sget-object v3, LX/19z;->b:LX/0U1;

    .line 209567
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 209568
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209569
    sget-object v3, LX/19z;->c:LX/0U1;

    .line 209570
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 209571
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209572
    const-string v3, "saved_videos"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/19z;->a:LX/0U1;

    .line 209573
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 209574
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "= ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 209575
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 209576
    :try_start_1
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 209577
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209578
    iput-object p2, v0, LX/7Jg;->b:Landroid/net/Uri;

    .line 209579
    iput-wide p3, v0, LX/7Jg;->d:J

    .line 209580
    const-wide/16 v4, 0x0

    iput-wide v4, v1, LX/7Jg;->f:J

    .line 209581
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209582
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209583
    const v0, 0x1ad3be39

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto/16 :goto_0

    .line 209584
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209585
    :catch_0
    move-exception v0

    .line 209586
    :try_start_5
    sget-object v1, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception in updating video uri"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209587
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 209588
    :catchall_1
    move-exception v0

    const v1, -0x1ccb7987

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;JJ)V
    .locals 13

    .prologue
    .line 209293
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209294
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 209295
    const v2, 0x7245e6a3

    invoke-static {v3, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209296
    :try_start_0
    iget-object v2, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v10

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-static/range {v3 .. v11}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/net/Uri;JJJ)V

    .line 209297
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 209298
    :try_start_1
    iget-object v2, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 209299
    iget-object v2, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Jg;

    .line 209300
    iget-object v4, v2, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {p2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 209301
    move-wide/from16 v0, p3

    iput-wide v0, v2, LX/7Jg;->f:J

    .line 209302
    const-wide/16 v4, -0x1

    cmp-long v4, p5, v4

    if-eqz v4, :cond_0

    .line 209303
    move-wide/from16 v0, p5

    iput-wide v0, v2, LX/7Jg;->d:J

    .line 209304
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209305
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209306
    const v2, -0x22910af0

    invoke-static {v3, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209307
    return-void

    .line 209308
    :cond_1
    :try_start_3
    move-wide/from16 v0, p3

    iput-wide v0, v2, LX/7Jg;->g:J

    .line 209309
    const-wide/16 v4, -0x1

    cmp-long v4, p5, v4

    if-eqz v4, :cond_0

    .line 209310
    move-wide/from16 v0, p5

    iput-wide v0, v2, LX/7Jg;->e:J

    goto :goto_0

    .line 209311
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209312
    :catch_0
    move-exception v2

    .line 209313
    :try_start_5
    sget-object v4, LX/19w;->a:Ljava/lang/String;

    const-string v5, "Exception"

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209314
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 209315
    :catchall_1
    move-exception v2

    const v4, -0xf4edac5

    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v2
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209316
    invoke-virtual {p0, p1}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209317
    sget-object v0, LX/19w;->a:Ljava/lang/String;

    const-string v1, "Video is not validated against server. Not played saved video %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209318
    :cond_0
    :goto_0
    return-void

    .line 209319
    :cond_1
    invoke-virtual {p0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209320
    if-eqz v0, :cond_0

    .line 209321
    iget-object v1, v0, LX/7Jg;->l:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, LX/7Jg;->l:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v1, v2, :cond_0

    .line 209322
    :cond_2
    iget-object v1, v0, LX/7Jg;->q:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 209323
    iget-object v1, v0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, LX/7Jg;->h:Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 209324
    :cond_3
    iget-object v1, v0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/7Jg;->h:Ljava/lang/String;

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209325
    iget-object v1, v0, LX/7Jg;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, LX/7Jg;->k:Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/StringBuilder;)V
    .locals 6

    .prologue
    .line 209128
    const-string v0, "Records in memory \n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209129
    monitor-enter p0

    .line 209130
    :try_start_0
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209131
    const-string v2, "VideoId:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209132
    iget-object v2, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209133
    const-string v2, " Uri:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209134
    iget-object v2, v0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209135
    const-string v2, " DownloadStatus:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209136
    iget-object v2, v0, LX/7Jg;->l:LX/1A0;

    invoke-virtual {v2}, LX/1A0;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209137
    const-string v2, " DownloadedBytes:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209138
    iget-wide v2, v0, LX/7Jg;->f:J

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 209139
    const-string v2, " VideoSize:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209140
    iget-wide v2, v0, LX/7Jg;->d:J

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 209141
    const-string v2, " VideoFile:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209142
    iget-object v0, v0, LX/7Jg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209143
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 209144
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209145
    const-string v0, " Records in database\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209146
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209147
    const v0, 0x3960741a

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209148
    :try_start_2
    invoke-static {v1}, LX/19x;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v0

    .line 209149
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209150
    const-string v3, "VideoId:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209151
    iget-object v3, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209152
    const-string v3, " Uri:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209153
    iget-object v3, v0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209154
    const-string v3, " DownloadStatus:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209155
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    invoke-virtual {v3}, LX/1A0;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209156
    const-string v3, " DownloadedBytes:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209157
    iget-wide v4, v0, LX/7Jg;->f:J

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 209158
    const-string v3, " VideoSize:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209159
    iget-wide v4, v0, LX/7Jg;->d:J

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 209160
    const-string v3, " VideoFile:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209161
    iget-object v3, v0, LX/7Jg;->h:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209162
    const-string v3, " AudioFile:"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209163
    iget-object v0, v0, LX/7Jg;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209164
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 209165
    :catchall_1
    move-exception v0

    const v2, -0x11b61c62

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 209166
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 209167
    const v0, 0x59083e90

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209168
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 209169
    iget-object v1, p0, LX/19w;->d:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 209170
    :cond_0
    :goto_0
    return v0

    .line 209171
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 209172
    iget-object v2, v1, LX/2fs;->c:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, LX/2fs;->c:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v2, v3, :cond_0

    iget-wide v2, v1, LX/2fs;->b:J

    iget-wide v4, v1, LX/2fs;->a:J

    invoke-direct {p0, v2, v3, v4, v5}, LX/19w;->a(JJ)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 209173
    :catch_0
    move-exception v1

    .line 209174
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "getDownloadStatus failed "

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 209175
    :try_start_0
    invoke-virtual {p0, p1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 209176
    iget-object v2, v1, LX/2fs;->c:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v2, v3, :cond_0

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 209177
    :cond_0
    :goto_0
    return v0

    .line 209178
    :catch_0
    move-exception v1

    .line 209179
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "getDownloadStatus failed "

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/2fs;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 209280
    :try_start_0
    invoke-virtual {p0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v8

    .line 209281
    if-nez v8, :cond_0

    .line 209282
    new-instance v1, LX/2fs;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    sget-object v6, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    sget-object v7, LX/2ft;->NONE:LX/2ft;

    invoke-direct/range {v1 .. v7}, LX/2fs;-><init>(JJLX/1A0;LX/2ft;)V

    .line 209283
    :goto_0
    return-object v1

    .line 209284
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v0, v8, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209285
    iget-object v0, v8, LX/7Jg;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v2, v8, LX/7Jg;->k:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209286
    :goto_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 209287
    :cond_1
    new-instance v1, LX/2fs;

    iget-wide v2, v8, LX/7Jg;->d:J

    const-wide/16 v4, 0x0

    sget-object v6, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    sget-object v7, LX/2ft;->NONE:LX/2ft;

    invoke-direct/range {v1 .. v7}, LX/2fs;-><init>(JJLX/1A0;LX/2ft;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209288
    :catch_0
    move-exception v0

    .line 209289
    sget-object v1, LX/19w;->a:Ljava/lang/String;

    const-string v2, "Exception getting download status"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209290
    new-instance v1, LX/2fs;

    sget-object v6, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    sget-object v7, LX/2ft;->NONE:LX/2ft;

    move-wide v2, v10

    move-wide v4, v10

    invoke-direct/range {v1 .. v7}, LX/2fs;-><init>(JJLX/1A0;LX/2ft;)V

    goto :goto_0

    .line 209291
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 209292
    :cond_3
    :try_start_1
    new-instance v1, LX/2fs;

    iget-wide v2, v8, LX/7Jg;->d:J

    iget-wide v4, v8, LX/7Jg;->e:J

    add-long/2addr v2, v4

    iget-wide v4, v8, LX/7Jg;->f:J

    iget-wide v6, v8, LX/7Jg;->g:J

    add-long/2addr v4, v6

    iget-object v6, v8, LX/7Jg;->l:LX/1A0;

    iget-object v7, v8, LX/7Jg;->n:LX/2ft;

    iget-boolean v8, v8, LX/7Jg;->o:Z

    invoke-direct/range {v1 .. v8}, LX/2fs;-><init>(JJLX/1A0;LX/2ft;Z)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 209180
    invoke-virtual {p0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209181
    if-nez v0, :cond_0

    .line 209182
    const/4 v0, 0x0

    .line 209183
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LX/7Jg;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized f(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 209184
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/19w;->l(Ljava/lang/String;)J

    move-result-wide v0

    .line 209185
    iget-object v2, p0, LX/19w;->d:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->l()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 209186
    const/4 v0, 0x0

    .line 209187
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 209188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 209189
    invoke-direct {p0}, LX/19w;->w()V

    .line 209190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/19w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized i(Ljava/lang/String;)LX/7Jg;
    .locals 1

    .prologue
    .line 209191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 209192
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209193
    invoke-virtual {p0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v1

    .line 209194
    if-nez v1, :cond_1

    .line 209195
    :cond_0
    :goto_0
    return v0

    .line 209196
    :cond_1
    invoke-direct {p0, v1}, LX/19w;->a(LX/7Jg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209197
    monitor-enter p0

    .line 209198
    :try_start_0
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209199
    monitor-exit p0

    .line 209200
    const/4 v0, 0x1

    goto :goto_0

    .line 209201
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 209202
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209203
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209204
    const v0, 0x2bb0afe3

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209205
    :try_start_0
    iget-object v0, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 209206
    invoke-static {v1, p1, v2, v3}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)V

    .line 209207
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 209208
    :try_start_1
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 209209
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209210
    iput-wide v2, v0, LX/7Jg;->m:J

    .line 209211
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209212
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209213
    const v0, 0x50558e20

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209214
    return-void

    .line 209215
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 209216
    :catch_0
    move-exception v0

    .line 209217
    :try_start_5
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209218
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 209219
    :catchall_1
    move-exception v0

    const v2, -0x4b6da23a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final l(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 209220
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209221
    if-nez v0, :cond_1

    .line 209222
    iget-boolean v0, p0, LX/19w;->k:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    .line 209223
    :goto_0
    return-wide v0

    .line 209224
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 209225
    :cond_1
    iget-wide v0, v0, LX/7Jg;->m:J

    .line 209226
    iget-object v2, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 209227
    sub-long v0, v2, v0

    goto :goto_0
.end method

.method public final m(Ljava/lang/String;)LX/7Jf;
    .locals 4

    .prologue
    .line 209228
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209229
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209230
    const v0, 0x70806883

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209231
    :try_start_0
    invoke-static {v1, p1}, LX/1A4;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jf;

    move-result-object v0

    .line 209232
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209233
    const v2, 0x43b628ff

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209234
    return-object v0

    .line 209235
    :catch_0
    move-exception v0

    .line 209236
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209237
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209238
    :catchall_0
    move-exception v0

    const v2, 0x60b436f8

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final m()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209118
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209119
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209120
    const v0, 0x6a5b833a

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209121
    :try_start_0
    invoke-static {v1}, LX/1A1;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v0

    .line 209122
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209123
    const v2, -0x604976db

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209124
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 209125
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 209126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    .line 209127
    :catchall_0
    move-exception v0

    const v2, -0x18af3e90

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final declared-synchronized n()J
    .locals 5

    .prologue
    .line 209239
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 209240
    :try_start_0
    iget-object v2, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209241
    iget-wide v0, v0, LX/7Jg;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 209242
    goto :goto_0

    .line 209243
    :cond_0
    monitor-exit p0

    return-wide v2

    .line 209244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n(Ljava/lang/String;)LX/7Jh;
    .locals 4

    .prologue
    .line 209245
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209246
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209247
    const v0, -0x436490fd

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209248
    :try_start_0
    invoke-static {v1, p1}, LX/1A1;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jh;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 209249
    const v2, 0x52bf4012

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209250
    return-object v0

    .line 209251
    :catch_0
    move-exception v0

    .line 209252
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209253
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209254
    :catchall_0
    move-exception v0

    const v2, 0x3c1886db

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final o(Ljava/lang/String;)LX/7Jh;
    .locals 4

    .prologue
    .line 209255
    invoke-static {p0}, LX/19w;->s(LX/19w;)V

    .line 209256
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 209257
    const v0, 0x24f46d37

    :try_start_0
    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209258
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 209259
    sget-object v2, LX/1A1;->c:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 p0, 0x0

    aput-object p1, v3, p0

    invoke-static {v0, v2, v3}, LX/1A1;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)LX/7Jh;

    move-result-object v2

    move-object v0, v2

    .line 209260
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209261
    const v2, 0x57655239

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209262
    return-object v0

    .line 209263
    :catch_0
    move-exception v0

    .line 209264
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209265
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209266
    :catchall_0
    move-exception v0

    const v2, 0x1b55f1ae

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 209267
    iget-object v0, p0, LX/19w;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 209268
    iget-wide v0, p0, LX/19w;->i:J

    return-wide v0
.end method

.method public final declared-synchronized r()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209269
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209270
    iget-object v0, p0, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 209271
    const/4 v4, 0x0

    .line 209272
    iget-boolean v5, v0, LX/7Jg;->o:Z

    if-nez v5, :cond_3

    .line 209273
    :cond_1
    :goto_1
    move v3, v4

    .line 209274
    if-eqz v3, :cond_0

    .line 209275
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 209276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 209277
    :cond_2
    monitor-exit p0

    return-object v1

    .line 209278
    :cond_3
    iget-object v5, p0, LX/19w;->j:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    iget-wide v8, v0, LX/7Jg;->p:J

    sub-long/2addr v6, v8

    .line 209279
    iget-object v5, p0, LX/19w;->d:LX/0tQ;

    invoke-virtual {v5}, LX/0tQ;->l()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    const/4 v4, 0x1

    goto :goto_1
.end method
