.class public abstract enum LX/0ci;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ci;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ci;

.field public static final enum SOFT:LX/0ci;

.field public static final enum STRONG:LX/0ci;

.field public static final enum WEAK:LX/0ci;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89211
    new-instance v0, LX/0cj;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, LX/0cj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ci;->STRONG:LX/0ci;

    .line 89212
    new-instance v0, LX/0cl;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, LX/0cl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ci;->SOFT:LX/0ci;

    .line 89213
    new-instance v0, LX/0cm;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, LX/0cm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ci;->WEAK:LX/0ci;

    .line 89214
    const/4 v0, 0x3

    new-array v0, v0, [LX/0ci;

    sget-object v1, LX/0ci;->STRONG:LX/0ci;

    aput-object v1, v0, v2

    sget-object v1, LX/0ci;->SOFT:LX/0ci;

    aput-object v1, v0, v3

    sget-object v1, LX/0ci;->WEAK:LX/0ci;

    aput-object v1, v0, v4

    sput-object v0, LX/0ci;->$VALUES:[LX/0ci;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ci;
    .locals 1

    .prologue
    .line 89208
    const-class v0, LX/0ci;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ci;

    return-object v0
.end method

.method public static values()[LX/0ci;
    .locals 1

    .prologue
    .line 89209
    sget-object v0, LX/0ci;->$VALUES:[LX/0ci;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ci;

    return-object v0
.end method


# virtual methods
.method public abstract defaultEquivalence()LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract referenceValue(LX/0d3;LX/0qF;Ljava/lang/Object;)LX/0cp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0d3",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;TV;)",
            "LX/0cp",
            "<TK;TV;>;"
        }
    .end annotation
.end method
