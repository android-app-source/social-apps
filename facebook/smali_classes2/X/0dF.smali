.class public LX/0dF;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90152
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 90153
    sget-object v0, LX/0dF;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 90154
    const-class v1, LX/0dF;

    monitor-enter v1

    .line 90155
    :try_start_0
    sget-object v0, LX/0dF;->a:Ljava/lang/String;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90156
    if-eqz v2, :cond_0

    .line 90157
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90158
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, LX/0VV;->E(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 90159
    sput-object v0, LX/0dF;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90160
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90161
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90162
    :cond_1
    sget-object v0, LX/0dF;->a:Ljava/lang/String;

    return-object v0

    .line 90163
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90164
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 90165
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/0VV;->E(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
