.class public LX/1QF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pr;


# instance fields
.field private final a:LX/1K8;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/1K8;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244425
    iput-object p1, p0, LX/1QF;->a:LX/1K8;

    .line 244426
    iput-object p2, p0, LX/1QF;->b:LX/0ad;

    .line 244427
    return-void
.end method

.method public static a(LX/0QB;)LX/1QF;
    .locals 1

    .prologue
    .line 244436
    invoke-static {p0}, LX/1QF;->b(LX/0QB;)LX/1QF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1QF;
    .locals 3

    .prologue
    .line 244434
    new-instance v2, LX/1QF;

    invoke-static {p0}, LX/1K8;->a(LX/0QB;)LX/1K8;

    move-result-object v0

    check-cast v0, LX/1K8;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/1QF;-><init>(LX/1K8;LX/0ad;)V

    .line 244435
    return-object v2
.end method


# virtual methods
.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 244437
    iget-object v0, p0, LX/1QF;->a:LX/1K8;

    invoke-virtual {v0, p1}, LX/1K8;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 244431
    iget-object v0, p0, LX/1QF;->b:LX/0ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1QF;->b:LX/0ad;

    sget-short v1, LX/1Dd;->t:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244432
    invoke-static {}, LX/26A;->a()LX/26A;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, LX/26A;->a(LX/0jW;LX/1Pr;)V

    .line 244433
    :cond_0
    iget-object v0, p0, LX/1QF;->a:LX/1K8;

    invoke-virtual {v0, p1, p2}, LX/1K8;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 244430
    iget-object v0, p0, LX/1QF;->a:LX/1K8;

    invoke-virtual {v0, p1, p2}, LX/1K8;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244428
    iget-object v0, p0, LX/1QF;->a:LX/1K8;

    invoke-virtual {v0, p1}, LX/1K8;->a(Ljava/lang/String;)V

    .line 244429
    return-void
.end method
