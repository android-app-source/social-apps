.class public abstract LX/15u;
.super LX/15v;
.source ""


# static fields
.field public static final s:Ljava/math/BigInteger;

.field public static final t:Ljava/math/BigInteger;

.field public static final u:Ljava/math/BigInteger;

.field public static final v:Ljava/math/BigInteger;

.field public static final w:Ljava/math/BigDecimal;

.field public static final x:Ljava/math/BigDecimal;

.field public static final y:Ljava/math/BigDecimal;

.field public static final z:Ljava/math/BigDecimal;


# instance fields
.field public A:I

.field public B:I

.field public C:J

.field public D:D

.field public E:Ljava/math/BigInteger;

.field public F:Ljava/math/BigDecimal;

.field public G:Z

.field public H:I

.field public I:I

.field public J:I

.field public final b:LX/12A;

.field public c:Z

.field public d:I

.field public e:I

.field public f:J

.field public g:I

.field public h:I

.field public i:J

.field public j:I

.field public k:I

.field public l:LX/15y;

.field public m:LX/15z;

.field public final n:LX/15x;

.field public o:[C

.field public p:Z

.field public q:LX/2SG;

.field public r:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 182709
    const-wide/32 v0, -0x80000000

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/15u;->s:Ljava/math/BigInteger;

    .line 182710
    const-wide/32 v0, 0x7fffffff

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/15u;->t:Ljava/math/BigInteger;

    .line 182711
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/15u;->u:Ljava/math/BigInteger;

    .line 182712
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/15u;->v:Ljava/math/BigInteger;

    .line 182713
    new-instance v0, Ljava/math/BigDecimal;

    sget-object v1, LX/15u;->u:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    sput-object v0, LX/15u;->w:Ljava/math/BigDecimal;

    .line 182714
    new-instance v0, Ljava/math/BigDecimal;

    sget-object v1, LX/15u;->v:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    sput-object v0, LX/15u;->x:Ljava/math/BigDecimal;

    .line 182715
    new-instance v0, Ljava/math/BigDecimal;

    sget-object v1, LX/15u;->s:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    sput-object v0, LX/15u;->y:Ljava/math/BigDecimal;

    .line 182716
    new-instance v0, Ljava/math/BigDecimal;

    sget-object v1, LX/15u;->t:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    sput-object v0, LX/15u;->z:Ljava/math/BigDecimal;

    return-void
.end method

.method public constructor <init>(LX/12A;I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 182596
    invoke-direct {p0}, LX/15v;-><init>()V

    .line 182597
    iput v0, p0, LX/15u;->d:I

    .line 182598
    iput v0, p0, LX/15u;->e:I

    .line 182599
    iput-wide v4, p0, LX/15u;->f:J

    .line 182600
    iput v1, p0, LX/15u;->g:I

    .line 182601
    iput v0, p0, LX/15u;->h:I

    .line 182602
    iput-wide v4, p0, LX/15u;->i:J

    .line 182603
    iput v1, p0, LX/15u;->j:I

    .line 182604
    iput v0, p0, LX/15u;->k:I

    .line 182605
    iput-object v2, p0, LX/15u;->o:[C

    .line 182606
    iput-boolean v0, p0, LX/15u;->p:Z

    .line 182607
    iput-object v2, p0, LX/15u;->q:LX/2SG;

    .line 182608
    iput v0, p0, LX/15u;->A:I

    .line 182609
    iput p2, p0, LX/15u;->a:I

    .line 182610
    iput-object p1, p0, LX/15u;->b:LX/12A;

    .line 182611
    invoke-virtual {p1}, LX/12A;->d()LX/15x;

    move-result-object v0

    iput-object v0, p0, LX/15u;->n:LX/15x;

    .line 182612
    invoke-static {}, LX/15y;->i()LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/15u;->l:LX/15y;

    .line 182613
    return-void
.end method

.method private Y()I
    .locals 1

    .prologue
    .line 182614
    iget v0, p0, LX/15u;->k:I

    .line 182615
    if-gez v0, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private Z()V
    .locals 6

    .prologue
    .line 182616
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 182617
    iget-wide v0, p0, LX/15u;->C:J

    long-to-int v0, v0

    .line 182618
    int-to-long v2, v0

    iget-wide v4, p0, LX/15u;->C:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 182619
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Numeric value ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") out of range of int"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    .line 182620
    :cond_0
    iput v0, p0, LX/15u;->B:I

    .line 182621
    :goto_0
    iget v0, p0, LX/15u;->A:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/15u;->A:I

    .line 182622
    return-void

    .line 182623
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 182624
    sget-object v0, LX/15u;->s:Ljava/math/BigInteger;

    iget-object v1, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-gtz v0, :cond_2

    sget-object v0, LX/15u;->t:Ljava/math/BigInteger;

    iget-object v1, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-gez v0, :cond_3

    .line 182625
    :cond_2
    invoke-direct {p0}, LX/15u;->ae()V

    .line 182626
    :cond_3
    iget-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    iput v0, p0, LX/15u;->B:I

    goto :goto_0

    .line 182627
    :cond_4
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 182628
    iget-wide v0, p0, LX/15u;->D:D

    const-wide/high16 v2, -0x3e20000000000000L    # -2.147483648E9

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_5

    iget-wide v0, p0, LX/15u;->D:D

    const-wide v2, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    .line 182629
    :cond_5
    invoke-direct {p0}, LX/15u;->ae()V

    .line 182630
    :cond_6
    iget-wide v0, p0, LX/15u;->D:D

    double-to-int v0, v0

    iput v0, p0, LX/15u;->B:I

    goto :goto_0

    .line 182631
    :cond_7
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_a

    .line 182632
    sget-object v0, LX/15u;->y:Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_8

    sget-object v0, LX/15u;->z:Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_9

    .line 182633
    :cond_8
    invoke-direct {p0}, LX/15u;->ae()V

    .line 182634
    :cond_9
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    iput v0, p0, LX/15u;->B:I

    goto :goto_0

    .line 182635
    :cond_a
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0
.end method

.method public static a(LX/0ln;IILjava/lang/String;)Ljava/lang/IllegalArgumentException;
    .locals 2

    .prologue
    .line 182636
    const/16 v0, 0x20

    if-gt p1, v0, :cond_1

    .line 182637
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal white space character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: can only used between units"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182638
    :goto_0
    if-eqz p3, :cond_0

    .line 182639
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182640
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 182641
    :cond_1
    invoke-virtual {p0, p1}, LX/0ln;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182642
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected padding character (\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182643
    iget-char v1, p0, LX/0ln;->b:C

    move v1, v1

    .line 182644
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: padding only legal as 3rd or 4th character"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182645
    :cond_2
    invoke-static {p1}, Ljava/lang/Character;->isDefined(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 182646
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182647
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-char v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (code 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a([CII)V
    .locals 4

    .prologue
    .line 182648
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v1

    .line 182649
    :try_start_0
    iget-boolean v0, p0, LX/15u;->G:Z

    invoke-static {p1, p2, p3, v0}, LX/16K;->a([CIIZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182650
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, LX/15u;->C:J

    .line 182651
    const/4 v0, 0x2

    iput v0, p0, LX/15u;->A:I

    .line 182652
    :goto_0
    return-void

    .line 182653
    :cond_0
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    .line 182654
    const/4 v0, 0x4

    iput v0, p0, LX/15u;->A:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182655
    :catch_0
    move-exception v0

    .line 182656
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed numeric value \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/15v;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private aa()V
    .locals 4

    .prologue
    .line 182657
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 182658
    iget v0, p0, LX/15u;->B:I

    int-to-long v0, v0

    iput-wide v0, p0, LX/15u;->C:J

    .line 182659
    :goto_0
    iget v0, p0, LX/15u;->A:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/15u;->A:I

    .line 182660
    return-void

    .line 182661
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 182662
    sget-object v0, LX/15u;->u:Ljava/math/BigInteger;

    iget-object v1, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-gtz v0, :cond_1

    sget-object v0, LX/15u;->v:Ljava/math/BigInteger;

    iget-object v1, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-gez v0, :cond_2

    .line 182663
    :cond_1
    invoke-direct {p0}, LX/15u;->af()V

    .line 182664
    :cond_2
    iget-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/15u;->C:J

    goto :goto_0

    .line 182665
    :cond_3
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 182666
    iget-wide v0, p0, LX/15u;->D:D

    const-wide/high16 v2, -0x3c20000000000000L    # -9.223372036854776E18

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_4

    iget-wide v0, p0, LX/15u;->D:D

    const-wide/high16 v2, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v0, v0, v2

    if-lez v0, :cond_5

    .line 182667
    :cond_4
    invoke-direct {p0}, LX/15u;->af()V

    .line 182668
    :cond_5
    iget-wide v0, p0, LX/15u;->D:D

    double-to-long v0, v0

    iput-wide v0, p0, LX/15u;->C:J

    goto :goto_0

    .line 182669
    :cond_6
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 182670
    sget-object v0, LX/15u;->w:Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_7

    sget-object v0, LX/15u;->x:Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_8

    .line 182671
    :cond_7
    invoke-direct {p0}, LX/15u;->af()V

    .line 182672
    :cond_8
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/15u;->C:J

    goto :goto_0

    .line 182673
    :cond_9
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 182674
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 182675
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    .line 182676
    :goto_0
    iget v0, p0, LX/15u;->A:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/15u;->A:I

    .line 182677
    return-void

    .line 182678
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 182679
    iget-wide v0, p0, LX/15u;->C:J

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    goto :goto_0

    .line 182680
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 182681
    iget v0, p0, LX/15u;->B:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    goto :goto_0

    .line 182682
    :cond_2
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 182683
    iget-wide v0, p0, LX/15u;->D:D

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    goto :goto_0

    .line 182684
    :cond_3
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 182685
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 182686
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, LX/15u;->D:D

    .line 182687
    :goto_0
    iget v0, p0, LX/15u;->A:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/15u;->A:I

    .line 182688
    return-void

    .line 182689
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 182690
    iget-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, LX/15u;->D:D

    goto :goto_0

    .line 182691
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 182692
    iget-wide v0, p0, LX/15u;->C:J

    long-to-double v0, v0

    iput-wide v0, p0, LX/15u;->D:D

    goto :goto_0

    .line 182693
    :cond_2
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 182694
    iget v0, p0, LX/15u;->B:I

    int-to-double v0, v0

    iput-wide v0, p0, LX/15u;->D:D

    goto :goto_0

    .line 182695
    :cond_3
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0
.end method

.method private ad()V
    .locals 2

    .prologue
    .line 182696
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 182697
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    .line 182698
    :goto_0
    iget v0, p0, LX/15u;->A:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/15u;->A:I

    .line 182699
    return-void

    .line 182700
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 182701
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, LX/15u;->E:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    goto :goto_0

    .line 182702
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 182703
    iget-wide v0, p0, LX/15u;->C:J

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    goto :goto_0

    .line 182704
    :cond_2
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 182705
    iget v0, p0, LX/15u;->B:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    goto :goto_0

    .line 182706
    :cond_3
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0
.end method

.method private ae()V
    .locals 2

    .prologue
    .line 182707
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Numeric value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") out of range of int (-2147483648"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - 2147483647"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182708
    return-void
.end method

.method private af()V
    .locals 2

    .prologue
    .line 182717
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Numeric value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") out of range of long (-9223372036854775808"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - 9223372036854775807"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182718
    return-void
.end method

.method private static b(LX/0ln;II)Ljava/lang/IllegalArgumentException;
    .locals 1

    .prologue
    .line 182586
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/15u;->a(LX/0ln;IILjava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    return-object v0
.end method

.method private f(I)V
    .locals 3

    .prologue
    const/16 v0, 0x10

    .line 182743
    if-ne p1, v0, :cond_0

    .line 182744
    :try_start_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->i()Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    .line 182745
    const/16 v0, 0x10

    iput v0, p0, LX/15u;->A:I

    .line 182746
    :goto_0
    return-void

    .line 182747
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->j()D

    move-result-wide v0

    iput-wide v0, p0, LX/15u;->D:D

    .line 182748
    const/16 v0, 0x8

    iput v0, p0, LX/15u;->A:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182749
    :catch_0
    move-exception v0

    .line 182750
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed numeric value \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/15v;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final A()F
    .locals 2

    .prologue
    .line 182741
    invoke-virtual {p0}, LX/15w;->B()D

    move-result-wide v0

    .line 182742
    double-to-float v0, v0

    return v0
.end method

.method public final B()D
    .locals 2

    .prologue
    .line 182735
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_1

    .line 182736
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182737
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182738
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_1

    .line 182739
    invoke-direct {p0}, LX/15u;->ac()V

    .line 182740
    :cond_1
    iget-wide v0, p0, LX/15u;->D:D

    return-wide v0
.end method

.method public final C()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 182729
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_1

    .line 182730
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182731
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182732
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_1

    .line 182733
    invoke-direct {p0}, LX/15u;->ad()V

    .line 182734
    :cond_1
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public D()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182728
    const/4 v0, 0x0

    return-object v0
.end method

.method public final K()V
    .locals 1

    .prologue
    .line 182725
    invoke-virtual {p0}, LX/15u;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182726
    invoke-virtual {p0}, LX/15v;->S()V

    .line 182727
    :cond_0
    return-void
.end method

.method public abstract L()Z
.end method

.method public abstract N()V
.end method

.method public O()V
    .locals 2

    .prologue
    .line 182719
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->a()V

    .line 182720
    iget-object v0, p0, LX/15u;->o:[C

    .line 182721
    if-eqz v0, :cond_0

    .line 182722
    const/4 v1, 0x0

    iput-object v1, p0, LX/15u;->o:[C

    .line 182723
    iget-object v1, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->c([C)V

    .line 182724
    :cond_0
    return-void
.end method

.method public final P()V
    .locals 4

    .prologue
    .line 182587
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ": expected close marker for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget-object v2, p0, LX/15u;->b:LX/12A;

    .line 182589
    iget-object v3, v2, LX/12A;->a:Ljava/lang/Object;

    move-object v2, v3

    .line 182590
    invoke-virtual {v1, v2}, LX/15y;->a(Ljava/lang/Object;)LX/28G;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 182591
    :cond_0
    return-void
.end method

.method public final Q()LX/2SG;
    .locals 1

    .prologue
    .line 182592
    iget-object v0, p0, LX/15u;->q:LX/2SG;

    if-nez v0, :cond_0

    .line 182593
    new-instance v0, LX/2SG;

    invoke-direct {v0}, LX/2SG;-><init>()V

    iput-object v0, p0, LX/15u;->q:LX/2SG;

    .line 182594
    :goto_0
    iget-object v0, p0, LX/15u;->q:LX/2SG;

    return-object v0

    .line 182595
    :cond_0
    iget-object v0, p0, LX/15u;->q:LX/2SG;

    invoke-virtual {v0}, LX/2SG;->a()V

    goto :goto_0
.end method

.method public R()C
    .locals 1

    .prologue
    .line 182516
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(LX/0ln;CI)I
    .locals 2

    .prologue
    .line 182506
    const/16 v0, 0x5c

    if-eq p2, v0, :cond_0

    .line 182507
    invoke-static {p1, p2, p3}, LX/15u;->b(LX/0ln;II)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 182508
    :cond_0
    invoke-virtual {p0}, LX/15u;->R()C

    move-result v1

    .line 182509
    const/16 v0, 0x20

    if-gt v1, v0, :cond_2

    .line 182510
    if-nez p3, :cond_2

    .line 182511
    const/4 v0, -0x1

    .line 182512
    :cond_1
    return v0

    .line 182513
    :cond_2
    invoke-virtual {p1, v1}, LX/0ln;->b(C)I

    move-result v0

    .line 182514
    if-gez v0, :cond_1

    .line 182515
    invoke-static {p1, v1, p3}, LX/15u;->b(LX/0ln;II)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method public final a(LX/0ln;II)I
    .locals 2

    .prologue
    .line 182496
    const/16 v0, 0x5c

    if-eq p2, v0, :cond_0

    .line 182497
    invoke-static {p1, p2, p3}, LX/15u;->b(LX/0ln;II)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 182498
    :cond_0
    invoke-virtual {p0}, LX/15u;->R()C

    move-result v1

    .line 182499
    const/16 v0, 0x20

    if-gt v1, v0, :cond_2

    .line 182500
    if-nez p3, :cond_2

    .line 182501
    const/4 v0, -0x1

    .line 182502
    :cond_1
    return v0

    .line 182503
    :cond_2
    invoke-virtual {p1, v1}, LX/0ln;->b(I)I

    move-result v0

    .line 182504
    if-gez v0, :cond_1

    .line 182505
    invoke-static {p1, v1, p3}, LX/15u;->b(LX/0ln;II)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/lang/String;D)LX/15z;
    .locals 2

    .prologue
    .line 182492
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0, p1}, LX/15x;->a(Ljava/lang/String;)V

    .line 182493
    iput-wide p2, p0, LX/15u;->D:D

    .line 182494
    const/16 v0, 0x8

    iput v0, p0, LX/15u;->A:I

    .line 182495
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    return-object v0
.end method

.method public final a(ZI)LX/15z;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 182486
    iput-boolean p1, p0, LX/15u;->G:Z

    .line 182487
    iput p2, p0, LX/15u;->H:I

    .line 182488
    iput v0, p0, LX/15u;->I:I

    .line 182489
    iput v0, p0, LX/15u;->J:I

    .line 182490
    iput v0, p0, LX/15u;->A:I

    .line 182491
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    return-object v0
.end method

.method public final a(ZIII)LX/15z;
    .locals 1

    .prologue
    .line 182483
    if-gtz p3, :cond_0

    if-gtz p4, :cond_0

    .line 182484
    invoke-virtual {p0, p1, p2}, LX/15u;->a(ZI)LX/15z;

    move-result-object v0

    .line 182485
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/15u;->b(ZIII)LX/15z;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IC)V
    .locals 4

    .prologue
    .line 182478
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget-object v2, p0, LX/15u;->b:LX/12A;

    .line 182479
    iget-object v3, v2, LX/12A;->a:Ljava/lang/Object;

    move-object v2, v3

    .line 182480
    invoke-virtual {v1, v2}, LX/15y;->a(Ljava/lang/Object;)LX/28G;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182481
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected close marker \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-char v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\': expected \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v2}, LX/12V;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " starting at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182482
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 182473
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected character ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in numeric value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182474
    if-eqz p2, :cond_0

    .line 182475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182476
    :cond_0
    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182477
    return-void
.end method

.method public final b(ZIII)LX/15z;
    .locals 1

    .prologue
    .line 182467
    iput-boolean p1, p0, LX/15u;->G:Z

    .line 182468
    iput p2, p0, LX/15u;->H:I

    .line 182469
    iput p3, p0, LX/15u;->I:I

    .line 182470
    iput p4, p0, LX/15u;->J:I

    .line 182471
    const/4 v0, 0x0

    iput v0, p0, LX/15u;->A:I

    .line 182472
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    return-object v0
.end method

.method public c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 182438
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_7

    .line 182439
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v1

    .line 182440
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->d()I

    move-result v0

    .line 182441
    iget v2, p0, LX/15u;->H:I

    .line 182442
    iget-boolean v3, p0, LX/15u;->G:Z

    if-eqz v3, :cond_0

    .line 182443
    add-int/lit8 v0, v0, 0x1

    .line 182444
    :cond_0
    const/16 v3, 0x9

    if-gt v2, v3, :cond_2

    .line 182445
    invoke-static {v1, v0, v2}, LX/16K;->a([CII)I

    move-result v0

    .line 182446
    iget-boolean v1, p0, LX/15u;->G:Z

    if-eqz v1, :cond_1

    neg-int v0, v0

    :cond_1
    iput v0, p0, LX/15u;->B:I

    .line 182447
    iput v4, p0, LX/15u;->A:I

    .line 182448
    :goto_0
    return-void

    .line 182449
    :cond_2
    const/16 v3, 0x12

    if-gt v2, v3, :cond_6

    .line 182450
    invoke-static {v1, v0, v2}, LX/16K;->b([CII)J

    move-result-wide v0

    .line 182451
    iget-boolean v3, p0, LX/15u;->G:Z

    if-eqz v3, :cond_3

    .line 182452
    neg-long v0, v0

    .line 182453
    :cond_3
    const/16 v3, 0xa

    if-ne v2, v3, :cond_5

    .line 182454
    iget-boolean v2, p0, LX/15u;->G:Z

    if-eqz v2, :cond_4

    .line 182455
    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    .line 182456
    long-to-int v0, v0

    iput v0, p0, LX/15u;->B:I

    .line 182457
    iput v4, p0, LX/15u;->A:I

    goto :goto_0

    .line 182458
    :cond_4
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_5

    .line 182459
    long-to-int v0, v0

    iput v0, p0, LX/15u;->B:I

    .line 182460
    iput v4, p0, LX/15u;->A:I

    goto :goto_0

    .line 182461
    :cond_5
    iput-wide v0, p0, LX/15u;->C:J

    .line 182462
    const/4 v0, 0x2

    iput v0, p0, LX/15u;->A:I

    goto :goto_0

    .line 182463
    :cond_6
    invoke-direct {p0, v1, v0, v2}, LX/15u;->a([CII)V

    goto :goto_0

    .line 182464
    :cond_7
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    if-ne v0, v1, :cond_8

    .line 182465
    invoke-direct {p0, p1}, LX/15u;->f(I)V

    goto :goto_0

    .line 182466
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not numeric, can not use numeric value accessors"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 182436
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid numeric value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182437
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 182430
    iget-boolean v0, p0, LX/15u;->c:Z

    if-nez v0, :cond_0

    .line 182431
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/15u;->c:Z

    .line 182432
    :try_start_0
    invoke-virtual {p0}, LX/15u;->N()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182433
    invoke-virtual {p0}, LX/15u;->O()V

    .line 182434
    :cond_0
    return-void

    .line 182435
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/15u;->O()V

    throw v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 182517
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_1

    .line 182518
    :cond_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 182519
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 182520
    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    .line 182521
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic j()LX/12V;
    .locals 1

    .prologue
    .line 182522
    iget-object v0, p0, LX/15u;->l:LX/15y;

    move-object v0, v0

    .line 182523
    return-object v0
.end method

.method public k()LX/28G;
    .locals 8

    .prologue
    .line 182524
    new-instance v0, LX/28G;

    iget-object v1, p0, LX/15u;->b:LX/12A;

    .line 182525
    iget-object v2, v1, LX/12A;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 182526
    iget-wide v6, p0, LX/15u;->i:J

    move-wide v2, v6

    .line 182527
    iget v4, p0, LX/15u;->j:I

    move v4, v4

    .line 182528
    invoke-direct {p0}, LX/15u;->Y()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/28G;-><init>(Ljava/lang/Object;JII)V

    return-object v0
.end method

.method public l()LX/28G;
    .locals 8

    .prologue
    .line 182529
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->h:I

    sub-int/2addr v0, v1

    add-int/lit8 v5, v0, 0x1

    .line 182530
    new-instance v0, LX/28G;

    iget-object v1, p0, LX/15u;->b:LX/12A;

    .line 182531
    iget-object v2, v1, LX/12A;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 182532
    iget-wide v2, p0, LX/15u;->f:J

    iget v4, p0, LX/15u;->d:I

    int-to-long v6, v4

    add-long/2addr v2, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v2, v6

    iget v4, p0, LX/15u;->g:I

    invoke-direct/range {v0 .. v5}, LX/28G;-><init>(Ljava/lang/Object;JII)V

    return-object v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 182533
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    .line 182534
    const/4 v0, 0x1

    .line 182535
    :goto_0
    return v0

    .line 182536
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_1

    .line 182537
    iget-boolean v0, p0, LX/15u;->p:Z

    goto :goto_0

    .line 182538
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 182539
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182540
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182541
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_4

    .line 182542
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 182543
    iget v0, p0, LX/15u;->B:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 182544
    :goto_0
    return-object v0

    .line 182545
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 182546
    iget-wide v0, p0, LX/15u;->C:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 182547
    :cond_2
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 182548
    iget-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    goto :goto_0

    .line 182549
    :cond_3
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    goto :goto_0

    .line 182550
    :cond_4
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 182551
    iget-object v0, p0, LX/15u;->F:Ljava/math/BigDecimal;

    goto :goto_0

    .line 182552
    :cond_5
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_6

    .line 182553
    invoke-static {}, LX/4pl;->b()V

    .line 182554
    :cond_6
    iget-wide v0, p0, LX/15u;->D:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method public u()LX/16L;
    .locals 2

    .prologue
    .line 182555
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182556
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182557
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_3

    .line 182558
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 182559
    sget-object v0, LX/16L;->INT:LX/16L;

    .line 182560
    :goto_0
    return-object v0

    .line 182561
    :cond_1
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 182562
    sget-object v0, LX/16L;->LONG:LX/16L;

    goto :goto_0

    .line 182563
    :cond_2
    sget-object v0, LX/16L;->BIG_INTEGER:LX/16L;

    goto :goto_0

    .line 182564
    :cond_3
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 182565
    sget-object v0, LX/16L;->BIG_DECIMAL:LX/16L;

    goto :goto_0

    .line 182566
    :cond_4
    sget-object v0, LX/16L;->DOUBLE:LX/16L;

    goto :goto_0
.end method

.method public version()LX/0ne;
    .locals 1

    .prologue
    .line 182567
    sget-object v0, LX/4ph;->VERSION:LX/0ne;

    return-object v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 182568
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 182569
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182570
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182571
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 182572
    invoke-direct {p0}, LX/15u;->Z()V

    .line 182573
    :cond_1
    iget v0, p0, LX/15u;->B:I

    return v0
.end method

.method public final y()J
    .locals 2

    .prologue
    .line 182574
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 182575
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182576
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182577
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 182578
    invoke-direct {p0}, LX/15u;->aa()V

    .line 182579
    :cond_1
    iget-wide v0, p0, LX/15u;->C:J

    return-wide v0
.end method

.method public final z()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 182580
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    .line 182581
    iget v0, p0, LX/15u;->A:I

    if-nez v0, :cond_0

    .line 182582
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/15u;->c(I)V

    .line 182583
    :cond_0
    iget v0, p0, LX/15u;->A:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    .line 182584
    invoke-direct {p0}, LX/15u;->ab()V

    .line 182585
    :cond_1
    iget-object v0, p0, LX/15u;->E:Ljava/math/BigInteger;

    return-object v0
.end method
