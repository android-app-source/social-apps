.class public LX/1W7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1VF;


# direct methods
.method public constructor <init>(LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267486
    iput-object p1, p0, LX/1W7;->a:LX/1VF;

    .line 267487
    return-void
.end method

.method public static a(LX/0QB;)LX/1W7;
    .locals 4

    .prologue
    .line 267488
    const-class v1, LX/1W7;

    monitor-enter v1

    .line 267489
    :try_start_0
    sget-object v0, LX/1W7;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267490
    sput-object v2, LX/1W7;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267491
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267492
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267493
    new-instance p0, LX/1W7;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-direct {p0, v3}, LX/1W7;-><init>(LX/1VF;)V

    .line 267494
    move-object v0, p0

    .line 267495
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267496
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1W7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267497
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267498
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
