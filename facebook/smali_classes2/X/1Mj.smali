.class public LX/1Mj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1Mj;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/1Mj;


# instance fields
.field private b:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<[",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 236004
    const-class v0, LX/1Mj;

    sput-object v0, LX/1Mj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236079
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/1Mj;->b:Ljava/lang/ref/SoftReference;

    .line 236080
    iput-object p1, p0, LX/1Mj;->c:LX/03V;

    .line 236081
    return-void
.end method

.method public static a(LX/0QB;)LX/1Mj;
    .locals 4

    .prologue
    .line 236065
    sget-object v0, LX/1Mj;->d:LX/1Mj;

    if-nez v0, :cond_1

    .line 236066
    const-class v1, LX/1Mj;

    monitor-enter v1

    .line 236067
    :try_start_0
    sget-object v0, LX/1Mj;->d:LX/1Mj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 236068
    if-eqz v2, :cond_0

    .line 236069
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 236070
    new-instance p0, LX/1Mj;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/1Mj;-><init>(LX/03V;)V

    .line 236071
    move-object v0, p0

    .line 236072
    sput-object v0, LX/1Mj;->d:LX/1Mj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236073
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 236074
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 236075
    :cond_1
    sget-object v0, LX/1Mj;->d:LX/1Mj;

    return-object v0

    .line 236076
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 236077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized d(LX/1Mj;)Ljavax/net/ssl/X509TrustManager;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 236048
    monitor-enter p0

    .line 236049
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 236050
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236051
    :goto_0
    if-nez v2, :cond_0

    move-object v0, v1

    .line 236052
    :goto_1
    monitor-exit p0

    return-object v0

    .line 236053
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 236054
    :goto_2
    :try_start_2
    sget-object v3, LX/1Mj;->a:Ljava/lang/Class;

    const-string v4, "Failed to create TrustManagerFactory"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 236055
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236056
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 236057
    :goto_3
    :try_start_3
    sget-object v3, LX/1Mj;->a:Ljava/lang/Class;

    const-string v4, "Failed to create TrustManagerFactory"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 236058
    :cond_0
    invoke-virtual {v2}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 236059
    instance-of v5, v0, Ljavax/net/ssl/X509TrustManager;

    if-eqz v5, :cond_1

    .line 236060
    check-cast v0, Ljavax/net/ssl/X509TrustManager;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 236061
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_2
    move-object v0, v1

    .line 236062
    goto :goto_1

    .line 236063
    :catch_2
    move-exception v0

    goto :goto_3

    .line 236064
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method public static declared-synchronized e(LX/1Mj;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 236025
    monitor-enter p0

    .line 236026
    :try_start_0
    const-string v0, "com.android.org.conscrypt.TrustedCertificateStore"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 236027
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-result-object v0

    .line 236028
    :goto_0
    move-object v3, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236029
    if-nez v3, :cond_0

    move-object v0, v1

    .line 236030
    :goto_1
    monitor-exit p0

    return-object v0

    .line 236031
    :cond_0
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 236032
    const-string v2, "userAliases"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 236033
    const-string v4, "getCertificate"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 236034
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 236035
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 236036
    goto :goto_1

    .line 236037
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 236038
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 236039
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v3, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 236040
    if-eqz v0, :cond_3

    .line 236041
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 236042
    :catch_0
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 236043
    goto :goto_1

    .line 236044
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236045
    :catch_1
    :try_start_3
    const-string v0, "org.apache.harmony.xnet.provider.jsse.TrustedCertificateStore"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 236046
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 236047
    :catch_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()[Ljava/security/cert/X509Certificate;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 236016
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Mj;->b:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/security/cert/X509Certificate;

    .line 236017
    if-nez v0, :cond_0

    .line 236018
    invoke-static {p0}, LX/1Mj;->d(LX/1Mj;)Ljavax/net/ssl/X509TrustManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 236019
    if-nez v0, :cond_1

    .line 236020
    const/4 v0, 0x0

    .line 236021
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 236022
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 236023
    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/1Mj;->b:Ljava/lang/ref/SoftReference;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()[[B
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 236005
    invoke-static {p0}, LX/1Mj;->e(LX/1Mj;)Ljava/util/ArrayList;

    move-result-object v3

    .line 236006
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236007
    :cond_0
    const/4 v0, 0x0

    .line 236008
    :goto_0
    return-object v0

    .line 236009
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [[B

    .line 236010
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 236011
    :try_start_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v0

    aput-object v0, v2, v1
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236012
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 236013
    :catch_0
    move-exception v0

    .line 236014
    sget-object v4, LX/1Mj;->a:Ljava/lang/Class;

    const-string v5, "Failed to encode user Root CA"

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 236015
    goto :goto_0
.end method
