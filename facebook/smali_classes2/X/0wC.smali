.class public final enum LX/0wC;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0gT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wC;",
        ">;",
        "LX/0gT;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wC;

.field public static final enum NUMBER_1:LX/0wC;

.field public static final enum NUMBER_1_5:LX/0wC;

.field public static final enum NUMBER_2:LX/0wC;

.field public static final enum NUMBER_3:LX/0wC;

.field public static final enum NUMBER_4:LX/0wC;


# instance fields
.field public final serverValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 159498
    new-instance v0, LX/0wC;

    const-string v1, "NUMBER_1"

    const-string v2, "1"

    invoke-direct {v0, v1, v3, v2}, LX/0wC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wC;->NUMBER_1:LX/0wC;

    .line 159499
    new-instance v0, LX/0wC;

    const-string v1, "NUMBER_1_5"

    const-string v2, "1.5"

    invoke-direct {v0, v1, v4, v2}, LX/0wC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wC;->NUMBER_1_5:LX/0wC;

    .line 159500
    new-instance v0, LX/0wC;

    const-string v1, "NUMBER_2"

    const-string v2, "2"

    invoke-direct {v0, v1, v5, v2}, LX/0wC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wC;->NUMBER_2:LX/0wC;

    .line 159501
    new-instance v0, LX/0wC;

    const-string v1, "NUMBER_3"

    const-string v2, "3"

    invoke-direct {v0, v1, v6, v2}, LX/0wC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wC;->NUMBER_3:LX/0wC;

    .line 159502
    new-instance v0, LX/0wC;

    const-string v1, "NUMBER_4"

    const-string v2, "4"

    invoke-direct {v0, v1, v7, v2}, LX/0wC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wC;->NUMBER_4:LX/0wC;

    .line 159503
    const/4 v0, 0x5

    new-array v0, v0, [LX/0wC;

    sget-object v1, LX/0wC;->NUMBER_1:LX/0wC;

    aput-object v1, v0, v3

    sget-object v1, LX/0wC;->NUMBER_1_5:LX/0wC;

    aput-object v1, v0, v4

    sget-object v1, LX/0wC;->NUMBER_2:LX/0wC;

    aput-object v1, v0, v5

    sget-object v1, LX/0wC;->NUMBER_3:LX/0wC;

    aput-object v1, v0, v6

    sget-object v1, LX/0wC;->NUMBER_4:LX/0wC;

    aput-object v1, v0, v7

    sput-object v0, LX/0wC;->$VALUES:[LX/0wC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159504
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 159505
    iput-object p3, p0, LX/0wC;->serverValue:Ljava/lang/String;

    .line 159506
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wC;
    .locals 1

    .prologue
    .line 159496
    const-class v0, LX/0wC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wC;

    return-object v0
.end method

.method public static values()[LX/0wC;
    .locals 1

    .prologue
    .line 159497
    sget-object v0, LX/0wC;->$VALUES:[LX/0wC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wC;

    return-object v0
.end method


# virtual methods
.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 159494
    iget-object v0, p0, LX/0wC;->serverValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 159495
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 159493
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Serialization infrastructure does not support type serialization."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159492
    iget-object v0, p0, LX/0wC;->serverValue:Ljava/lang/String;

    return-object v0
.end method
