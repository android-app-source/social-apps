.class public abstract LX/0mM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0mN;

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 132393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132394
    new-instance v0, LX/0mN;

    invoke-direct {v0}, LX/0mN;-><init>()V

    iput-object v0, p0, LX/0mM;->a:LX/0mN;

    .line 132395
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 132396
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mM;->a:LX/0mN;

    invoke-virtual {v0}, LX/0mN;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132397
    monitor-exit p0

    return-void

    .line 132398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 132399
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0mM;->c:Z

    .line 132400
    iput-object p1, p0, LX/0mM;->b:Ljava/lang/String;

    .line 132401
    iget-object v0, p0, LX/0mM;->a:LX/0mN;

    invoke-virtual {v0, p1}, LX/0mN;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132402
    monitor-exit p0

    return-void

    .line 132403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 132404
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mM;->a:LX/0mN;

    invoke-virtual {v0}, LX/0mN;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132405
    monitor-exit p0

    return-void

    .line 132406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 132407
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mM;->a:LX/0mN;

    invoke-virtual {v0}, LX/0mN;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132408
    monitor-exit p0

    return-void

    .line 132409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132410
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0mM;->c:Z

    if-nez v0, :cond_0

    .line 132411
    invoke-virtual {p0}, LX/0mM;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0mM;->b:Ljava/lang/String;

    .line 132412
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0mM;->c:Z

    .line 132413
    :cond_0
    iget-object v0, p0, LX/0mM;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 132414
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract e()Ljava/lang/String;
.end method
