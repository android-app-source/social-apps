.class public LX/1Iv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Cf;
.implements LX/0pR;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0fp;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Cf;",
        "LX/0pR;"
    }
.end annotation


# static fields
.field public static final a:I


# instance fields
.field public final b:LX/1EM;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Uh;

.field public final e:LX/0SG;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rm;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0pJ;

.field public final j:LX/0Zr;

.field public final k:LX/0r8;

.field private final l:LX/0pS;

.field private final m:LX/1Iw;

.field public final n:LX/1Ix;

.field public final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0fp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/api/feedtype/FeedType;

.field private r:LX/0rB;

.field private s:LX/0r5;

.field public t:LX/1J1;

.field public u:LX/1Iz;

.field public v:Landroid/os/HandlerThread;

.field public w:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 229481
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v2, 0x83d60

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LX/1Iv;->a:I

    return-void
.end method

.method public constructor <init>(LX/1EM;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0pJ;LX/0pV;LX/0Zr;LX/0pS;LX/1Iw;LX/1Ix;LX/0ad;)V
    .locals 6
    .param p1    # LX/1EM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1EM;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0rm;",
            ">;",
            "LX/0pJ;",
            "LX/0pV;",
            "LX/0Zr;",
            "LX/0pS;",
            "LX/1Iw;",
            "LX/1Ix;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229464
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/1Iv;->o:Ljava/util/ArrayList;

    .line 229465
    iput-object p1, p0, LX/1Iv;->b:LX/1EM;

    .line 229466
    iput-object p2, p0, LX/1Iv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 229467
    iput-object p3, p0, LX/1Iv;->d:LX/0Uh;

    .line 229468
    iput-object p4, p0, LX/1Iv;->e:LX/0SG;

    .line 229469
    iput-object p5, p0, LX/1Iv;->f:LX/0Ot;

    .line 229470
    iput-object p6, p0, LX/1Iv;->g:LX/0Ot;

    .line 229471
    iput-object p7, p0, LX/1Iv;->h:LX/0Ot;

    .line 229472
    iput-object p8, p0, LX/1Iv;->i:LX/0pJ;

    .line 229473
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1Iv;->j:LX/0Zr;

    .line 229474
    new-instance v2, LX/0r8;

    const/16 v3, 0x4e20

    invoke-virtual {p8, v3}, LX/0pJ;->j(I)I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v2, p9, v4, v5}, LX/0r8;-><init>(LX/0pV;J)V

    iput-object v2, p0, LX/1Iv;->k:LX/0r8;

    .line 229475
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1Iv;->l:LX/0pS;

    .line 229476
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1Iv;->m:LX/1Iw;

    .line 229477
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Iv;->n:LX/1Ix;

    .line 229478
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Iv;->w:LX/0ad;

    .line 229479
    invoke-direct {p0}, LX/1Iv;->v()V

    .line 229480
    return-void
.end method

.method public static u(LX/1Iv;)LX/0r5;
    .locals 3

    .prologue
    .line 229460
    iget-object v0, p0, LX/1Iv;->s:LX/0r5;

    if-nez v0, :cond_0

    .line 229461
    iget-object v0, p0, LX/1Iv;->l:LX/0pS;

    new-instance v1, LX/1Iy;

    invoke-direct {v1, p0}, LX/1Iy;-><init>(LX/1Iv;)V

    invoke-virtual {v0, v1}, LX/0pS;->a(LX/0r4;)LX/0r5;

    move-result-object v0

    iput-object v0, p0, LX/1Iv;->s:LX/0r5;

    .line 229462
    :cond_0
    iget-object v0, p0, LX/1Iv;->s:LX/0r5;

    return-object v0
.end method

.method private v()V
    .locals 4

    .prologue
    .line 229457
    iget-object v0, p0, LX/1Iv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->e:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 229458
    invoke-static {p0}, LX/1Iv;->u(LX/1Iv;)LX/0r5;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/0r5;->a(J)V

    .line 229459
    return-void
.end method

.method public static z(LX/1Iv;)Lcom/facebook/common/perftest/PerfTestConfig;
    .locals 1

    .prologue
    .line 229377
    iget-object v0, p0, LX/1Iv;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/perftest/PerfTestConfig;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)V
    .locals 4

    .prologue
    .line 229443
    iget-object v0, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    .line 229444
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 229445
    iget-object v1, p1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v1, v1

    .line 229446
    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 229447
    iget-object v0, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    if-eqz v0, :cond_3

    .line 229448
    :cond_1
    :goto_1
    return-void

    .line 229449
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 229450
    :cond_3
    iput-object p1, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    .line 229451
    iget-object v0, p0, LX/1Iv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 229452
    iget-object v2, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v2

    .line 229453
    iget-object v3, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    .line 229454
    iget-object p1, v3, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v3, p1

    .line 229455
    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 229456
    iput-object v0, p0, LX/1Iv;->r:LX/0rB;

    goto :goto_1
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 229432
    iget-object v1, p0, LX/1Iv;->w:LX/0ad;

    sget-short v2, LX/0fe;->aG:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 229433
    if-eqz v1, :cond_1

    .line 229434
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    .line 229435
    iget-object v2, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v2}, LX/0pO;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/1Iv;->i:LX/0pJ;

    invoke-virtual {v2, v1}, LX/0pJ;->b(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    move v1, v1

    .line 229436
    if-nez v1, :cond_5

    const/4 v1, 0x0

    .line 229437
    sget-object v2, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1Iv;->i:LX/0pJ;

    invoke-virtual {v2, v1}, LX/0pJ;->b(Z)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1Iv;->w:LX/0ad;

    sget-short v3, LX/0fe;->W:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    move v1, v1

    .line 229438
    if-nez v1, :cond_5

    const/4 v1, 0x0

    .line 229439
    iget-object v2, p0, LX/1Iv;->q:Lcom/facebook/api/feedtype/FeedType;

    .line 229440
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v3

    .line 229441
    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1Iv;->i:LX/0pJ;

    invoke-virtual {v2, v1}, LX/0pJ;->b(Z)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1Iv;->w:LX/0ad;

    sget-short v3, LX/0fe;->O:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x1

    :cond_4
    move v1, v1

    .line 229442
    if-eqz v1, :cond_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 229428
    invoke-virtual {p0}, LX/1Iv;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 229429
    :cond_0
    :goto_0
    return-void

    .line 229430
    :cond_1
    iget-object v0, p0, LX/1Iv;->k:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229431
    invoke-static {p0}, LX/1Iv;->u(LX/1Iv;)LX/0r5;

    move-result-object v0

    iget-object v1, p0, LX/1Iv;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iget-object v3, p0, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v3}, LX/1EM;->d()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v3}, LX/1EM;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    iget-object v4, p0, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v4}, LX/1EM;->b()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/0r5;->b(JZJ)Z

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final h()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ImprovedNewApi"
        }
    .end annotation

    .prologue
    .line 229416
    invoke-static {p0}, LX/1Iv;->u(LX/1Iv;)LX/0r5;

    move-result-object v0

    invoke-virtual {v0}, LX/0r5;->b()V

    .line 229417
    iget-object v0, p0, LX/1Iv;->u:LX/1Iz;

    if-eqz v0, :cond_0

    .line 229418
    iget-object v0, p0, LX/1Iv;->u:LX/1Iz;

    invoke-virtual {v0}, LX/0jZ;->a()V

    .line 229419
    :cond_0
    const/4 v0, 0x0

    .line 229420
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 229421
    const/4 v0, 0x1

    .line 229422
    :cond_1
    iget-object v1, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    if-eqz v1, :cond_2

    .line 229423
    if-eqz v0, :cond_3

    .line 229424
    iget-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 229425
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    .line 229426
    :cond_2
    return-void

    .line 229427
    :cond_3
    iget-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_0
.end method

.method public final j()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229415
    iget-object v0, p0, LX/1Iv;->o:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 4

    .prologue
    .line 229411
    iget-object v0, p0, LX/1Iv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0pP;->e:LX/0Tn;

    iget-object v2, p0, LX/1Iv;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 229412
    invoke-direct {p0}, LX/1Iv;->v()V

    .line 229413
    invoke-virtual {p0}, LX/1Iv;->d()V

    .line 229414
    return-void
.end method

.method public final m()V
    .locals 11
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 229389
    const-string v0, "AutoRefreshManager.loadNewDataFromNetwork"

    const v1, -0x18307631

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229390
    :try_start_0
    iget-object v0, p0, LX/1Iv;->k:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 229391
    const v0, 0x1607c1e3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229392
    :goto_0
    return-void

    .line 229393
    :cond_0
    :try_start_1
    new-instance v1, LX/1J0;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v3, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, LX/1Iv;->r:LX/0rB;

    const-string v5, "auto_refresh_new_data_fetch"

    iget-object v0, p0, LX/1Iv;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, LX/0gf;->POLLING:LX/0gf;

    invoke-direct/range {v1 .. v10}, LX/1J0;-><init>(LX/0rS;Lcom/facebook/api/feedtype/FeedType;LX/0rB;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;LX/0gf;)V

    .line 229394
    iget-object v0, p0, LX/1Iv;->p:LX/0fp;

    if-eqz v0, :cond_1

    .line 229395
    iget-object v0, p0, LX/1Iv;->p:LX/0fp;

    invoke-interface {v0}, LX/0fp;->l()V

    .line 229396
    :cond_1
    iget-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    if-nez v0, :cond_3

    .line 229397
    iget-object v0, p0, LX/1Iv;->j:LX/0Zr;

    const-string v2, "autorefresh_network_fetcher"

    sget-object v3, LX/0TP;->FOREGROUND:LX/0TP;

    invoke-virtual {v0, v2, v3}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    .line 229398
    iget-object v0, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 229399
    iget-object v0, p0, LX/1Iv;->n:LX/1Ix;

    iget-object v2, p0, LX/1Iv;->v:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    .line 229400
    iget-object v3, p0, LX/1Iv;->t:LX/1J1;

    if-nez v3, :cond_2

    .line 229401
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, LX/1Iv;->k:LX/0r8;

    iget-object v5, p0, LX/1Iv;->o:Ljava/util/ArrayList;

    new-instance v6, LX/1J2;

    invoke-direct {v6, p0}, LX/1J2;-><init>(LX/1Iv;)V

    .line 229402
    new-instance v7, LX/1J1;

    invoke-direct {v7, v3, v4, v5, v6}, LX/1J1;-><init>(Landroid/os/Looper;LX/0r8;Ljava/util/List;LX/1J2;)V

    .line 229403
    move-object v3, v7

    .line 229404
    iput-object v3, p0, LX/1Iv;->t:LX/1J1;

    .line 229405
    :cond_2
    iget-object v3, p0, LX/1Iv;->t:LX/1J1;

    move-object v3, v3

    .line 229406
    invoke-virtual {v0, v2, v3}, LX/1Ix;->a(Landroid/os/Looper;LX/1J1;)LX/1Iz;

    move-result-object v0

    iput-object v0, p0, LX/1Iv;->u:LX/1Iz;

    .line 229407
    :cond_3
    iget-object v0, p0, LX/1Iv;->k:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->d()V

    .line 229408
    iget-object v0, p0, LX/1Iv;->u:LX/1Iz;

    .line 229409
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/1Iz;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Iz;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229410
    const v0, 0x63a79854

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x21814bb8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 229380
    iget-object v1, p0, LX/1Iv;->d:LX/0Uh;

    const/16 v2, 0x414

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229381
    :cond_0
    :goto_0
    return v0

    .line 229382
    :cond_1
    iget-object v1, p0, LX/1Iv;->r:LX/0rB;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1Iv;->r:LX/0rB;

    .line 229383
    iget-boolean v2, v1, LX/0rB;->d:Z

    move v1, v2

    .line 229384
    if-eqz v1, :cond_0

    .line 229385
    iget-object v1, p0, LX/1Iv;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1Iv;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 229386
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->d:Z

    move v1, v1

    .line 229387
    if-nez v1, :cond_0

    .line 229388
    :cond_2
    invoke-virtual {p0}, LX/1Iv;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 229378
    invoke-virtual {p0}, LX/1Iv;->l()V

    .line 229379
    return-void
.end method
