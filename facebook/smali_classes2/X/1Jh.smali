.class public final LX/1Jh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/1Jf;


# direct methods
.method public constructor <init>(LX/1Jf;)V
    .locals 0

    .prologue
    .line 230320
    iput-object p1, p0, LX/1Jh;->a:LX/1Jf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 5

    .prologue
    .line 230312
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 230313
    invoke-static {p2}, LX/3BB;->getFromListViewScrollState(I)LX/3BB;

    move-result-object v0

    .line 230314
    sget-object v1, LX/3BB;->IDLE:LX/3BB;

    if-ne v0, v1, :cond_0

    .line 230315
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    invoke-interface {p1}, LX/0g8;->q()I

    move-result v1

    invoke-interface {p1}, LX/0g8;->r()I

    move-result v2

    invoke-interface {p1}, LX/0g8;->q()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, LX/1Jf;->a$redex0(LX/1Jf;II)V

    .line 230316
    :goto_0
    return-void

    .line 230317
    :cond_0
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v1, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v1, v1, LX/1Jf;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 230318
    iput-wide v2, v0, LX/1Jf;->m:J

    .line 230319
    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 5

    .prologue
    .line 230306
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, LX/1Jh;->a:LX/1Jf;

    iget-wide v2, v2, LX/1Jf;->m:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 230307
    :goto_0
    return-void

    .line 230308
    :cond_0
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    invoke-static {v0, p2, p3}, LX/1Jf;->a$redex0(LX/1Jf;II)V

    .line 230309
    iget-object v0, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v1, p0, LX/1Jh;->a:LX/1Jf;

    iget-object v1, v1, LX/1Jf;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 230310
    iput-wide v2, v0, LX/1Jf;->m:J

    .line 230311
    goto :goto_0
.end method
