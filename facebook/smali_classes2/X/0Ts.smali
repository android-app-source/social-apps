.class public LX/0Ts;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Tt;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Ts;


# instance fields
.field public final a:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63802
    iput-object p1, p0, LX/0Ts;->a:LX/0Sh;

    .line 63803
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ts;
    .locals 4

    .prologue
    .line 63804
    sget-object v0, LX/0Ts;->b:LX/0Ts;

    if-nez v0, :cond_1

    .line 63805
    const-class v1, LX/0Ts;

    monitor-enter v1

    .line 63806
    :try_start_0
    sget-object v0, LX/0Ts;->b:LX/0Ts;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 63807
    if-eqz v2, :cond_0

    .line 63808
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 63809
    new-instance p0, LX/0Ts;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/0Ts;-><init>(LX/0Sh;)V

    .line 63810
    move-object v0, p0

    .line 63811
    sput-object v0, LX/0Ts;->b:LX/0Ts;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63812
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 63813
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63814
    :cond_1
    sget-object v0, LX/0Ts;->b:LX/0Ts;

    return-object v0

    .line 63815
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 63816
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 63817
    iget-object v0, p0, LX/0Ts;->a:LX/0Sh;

    const-string v1, "Database accessed from a UI Thread."

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 63818
    return-void
.end method
