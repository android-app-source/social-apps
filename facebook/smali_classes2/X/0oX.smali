.class public LX/0oX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0jp;
.implements LX/0oY;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0oX;


# instance fields
.field private final a:LX/0oZ;

.field private final b:LX/0oI;


# direct methods
.method public constructor <init>(LX/0oZ;LX/0oI;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 141617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141618
    iput-object p1, p0, LX/0oX;->a:LX/0oZ;

    .line 141619
    iput-object p2, p0, LX/0oX;->b:LX/0oI;

    .line 141620
    return-void
.end method

.method public static a(LX/0QB;)LX/0oX;
    .locals 5

    .prologue
    .line 141621
    sget-object v0, LX/0oX;->c:LX/0oX;

    if-nez v0, :cond_1

    .line 141622
    const-class v1, LX/0oX;

    monitor-enter v1

    .line 141623
    :try_start_0
    sget-object v0, LX/0oX;->c:LX/0oX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 141624
    if-eqz v2, :cond_0

    .line 141625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 141626
    new-instance p0, LX/0oX;

    invoke-static {v0}, LX/0oZ;->b(LX/0QB;)LX/0oZ;

    move-result-object v3

    check-cast v3, LX/0oZ;

    invoke-static {v0}, LX/0oI;->a(LX/0QB;)LX/0oI;

    move-result-object v4

    check-cast v4, LX/0oI;

    invoke-direct {p0, v3, v4}, LX/0oX;-><init>(LX/0oZ;LX/0oI;)V

    .line 141627
    move-object v0, p0

    .line 141628
    sput-object v0, LX/0oX;->c:LX/0oX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 141630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 141631
    :cond_1
    sget-object v0, LX/0oX;->c:LX/0oX;

    return-object v0

    .line 141632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 141633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 141634
    iget-object v0, p0, LX/0oX;->b:LX/0oI;

    invoke-virtual {v0}, LX/0oI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141635
    iget-object v0, p0, LX/0oX;->a:LX/0oZ;

    invoke-virtual {v0, p1}, LX/0oZ;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 141636
    :goto_0
    return-object v0

    .line 141637
    :cond_0
    iget-object v0, p0, LX/0oX;->a:LX/0oZ;

    invoke-virtual {v0, p1}, LX/0oZ;->b(Landroid/content/Intent;)Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    move-result-object v1

    .line 141638
    new-instance v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-direct {v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;-><init>()V

    .line 141639
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 141640
    const-string v3, "newsfeed_fragment_builder"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 141641
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141642
    iget-object v0, p0, LX/0oX;->b:LX/0oI;

    invoke-virtual {v0}, LX/0oI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141643
    iget-object v0, p0, LX/0oX;->a:LX/0oZ;

    invoke-virtual {v0, p1}, LX/0oZ;->a(LX/0Ot;)V

    .line 141644
    :goto_0
    return-void

    .line 141645
    :cond_0
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oh;

    const-class v1, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-virtual {v0, v1}, LX/0oh;->a(Ljava/lang/Class;)LX/1Av;

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 141646
    return-void
.end method
