.class public LX/0ju;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/31a;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 124777
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/0ju;-><init>(Landroid/content/Context;I)V

    .line 124778
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 124773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124774
    new-instance v0, LX/31a;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, p2}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/31a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/0ju;->a:LX/31a;

    .line 124775
    iput p2, p0, LX/0ju;->b:I

    .line 124776
    return-void
.end method


# virtual methods
.method public final a(I)LX/0ju;
    .locals 2

    .prologue
    .line 124771
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->f:Ljava/lang/CharSequence;

    .line 124772
    return-object p0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 2

    .prologue
    .line 124768
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->k:Ljava/lang/CharSequence;

    .line 124769
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 124770
    return-object p0
.end method

.method public final a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124766
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->r:Landroid/content/DialogInterface$OnCancelListener;

    .line 124767
    return-object p0
.end method

.method public final a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124764
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->s:Landroid/content/DialogInterface$OnDismissListener;

    .line 124765
    return-object p0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)LX/0ju;
    .locals 1

    .prologue
    .line 124762
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->d:Landroid/graphics/drawable/Drawable;

    .line 124763
    return-object p0
.end method

.method public final a(Landroid/view/View;)LX/0ju;
    .locals 1

    .prologue
    .line 124700
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->g:Landroid/view/View;

    .line 124701
    return-object p0
.end method

.method public final a(Landroid/view/View;IIII)LX/0ju;
    .locals 2

    .prologue
    .line 124755
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->x:Landroid/view/View;

    .line 124756
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/31a;->D:Z

    .line 124757
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p2, v0, LX/31a;->z:I

    .line 124758
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p3, v0, LX/31a;->A:I

    .line 124759
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p4, v0, LX/31a;->B:I

    .line 124760
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p5, v0, LX/31a;->C:I

    .line 124761
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/0ju;
    .locals 1

    .prologue
    .line 124753
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->f:Ljava/lang/CharSequence;

    .line 124754
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124750
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->k:Ljava/lang/CharSequence;

    .line 124751
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 124752
    return-object p0
.end method

.method public final a(Z)LX/0ju;
    .locals 1

    .prologue
    .line 124748
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-boolean p1, v0, LX/31a;->q:Z

    .line 124749
    return-object p0
.end method

.method public final a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 2

    .prologue
    .line 124743
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->u:[Ljava/lang/CharSequence;

    .line 124744
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p3, v0, LX/31a;->w:Landroid/content/DialogInterface$OnClickListener;

    .line 124745
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p2, v0, LX/31a;->H:I

    .line 124746
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/31a;->G:Z

    .line 124747
    return-object p0
.end method

.method public final a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124740
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->u:[Ljava/lang/CharSequence;

    .line 124741
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->w:Landroid/content/DialogInterface$OnClickListener;

    .line 124742
    return-object p0
.end method

.method public a()LX/2EJ;
    .locals 3

    .prologue
    .line 124730
    new-instance v0, LX/2EJ;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    iget v2, p0, LX/0ju;->b:I

    invoke-direct {v0, v1, v2}, LX/2EJ;-><init>(Landroid/content/Context;I)V

    .line 124731
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v2, v0, LX/2EJ;->a:LX/4BW;

    invoke-virtual {v1, v2}, LX/31a;->a(LX/4BW;)V

    .line 124732
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-boolean v1, v1, LX/31a;->q:Z

    invoke-virtual {v0, v1}, LX/2EJ;->setCancelable(Z)V

    .line 124733
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-boolean v1, v1, LX/31a;->q:Z

    if-eqz v1, :cond_0

    .line 124734
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 124735
    :cond_0
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->r:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 124736
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->s:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 124737
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->t:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_1

    .line 124738
    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->t:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, LX/2EJ;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 124739
    :cond_1
    return-object v0
.end method

.method public final b(I)LX/0ju;
    .locals 2

    .prologue
    .line 124728
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->h:Ljava/lang/CharSequence;

    .line 124729
    return-object p0
.end method

.method public b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 2

    .prologue
    .line 124725
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->m:Ljava/lang/CharSequence;

    .line 124726
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 124727
    return-object p0
.end method

.method public final b(Landroid/view/View;)LX/0ju;
    .locals 2

    .prologue
    .line 124722
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->x:Landroid/view/View;

    .line 124723
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/31a;->D:Z

    .line 124724
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/0ju;
    .locals 1

    .prologue
    .line 124720
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->h:Ljava/lang/CharSequence;

    .line 124721
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124717
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->m:Ljava/lang/CharSequence;

    .line 124718
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 124719
    return-object p0
.end method

.method public final b(Z)LX/0ju;
    .locals 1

    .prologue
    .line 124715
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-boolean p1, v0, LX/31a;->M:Z

    .line 124716
    return-object p0
.end method

.method public final b()LX/2EJ;
    .locals 2

    .prologue
    .line 124712
    invoke-virtual {p0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 124713
    :try_start_0
    invoke-virtual {v0}, LX/2EJ;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124714
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final c(I)LX/0ju;
    .locals 1

    .prologue
    .line 124710
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput p1, v0, LX/31a;->c:I

    .line 124711
    return-object p0
.end method

.method public c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 2

    .prologue
    .line 124707
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iget-object v1, p0, LX/0ju;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->o:Ljava/lang/CharSequence;

    .line 124708
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->p:Landroid/content/DialogInterface$OnClickListener;

    .line 124709
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;
    .locals 1

    .prologue
    .line 124704
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p1, v0, LX/31a;->o:Ljava/lang/CharSequence;

    .line 124705
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-object p2, v0, LX/31a;->p:Landroid/content/DialogInterface$OnClickListener;

    .line 124706
    return-object p0
.end method

.method public final c(Z)LX/0ju;
    .locals 1

    .prologue
    .line 124702
    iget-object v0, p0, LX/0ju;->a:LX/31a;

    iput-boolean p1, v0, LX/31a;->P:Z

    .line 124703
    return-object p0
.end method
