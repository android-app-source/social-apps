.class public LX/0jM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0jM;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/16I;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/CSJ;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/16I;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;",
            "LX/16I;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/CSJ;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 122925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122926
    iput-object p1, p0, LX/0jM;->a:LX/0Ot;

    .line 122927
    iput-object p2, p0, LX/0jM;->b:LX/16I;

    .line 122928
    iput-object p3, p0, LX/0jM;->c:LX/0Ot;

    .line 122929
    return-void
.end method

.method public static a(LX/0QB;)LX/0jM;
    .locals 7

    .prologue
    .line 122909
    sget-object v0, LX/0jM;->d:LX/0jM;

    if-nez v0, :cond_1

    .line 122910
    const-class v1, LX/0jM;

    monitor-enter v1

    .line 122911
    :try_start_0
    sget-object v0, LX/0jM;->d:LX/0jM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 122912
    if-eqz v2, :cond_0

    .line 122913
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 122914
    new-instance v4, LX/0jM;

    const/16 v3, 0x12c1

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v3

    check-cast v3, LX/16I;

    .line 122915
    new-instance v6, LX/3AI;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v6, p0}, LX/3AI;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 122916
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v6, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v6

    move-object v6, v6

    .line 122917
    invoke-direct {v4, v5, v3, v6}, LX/0jM;-><init>(LX/0Ot;LX/16I;LX/0Ot;)V

    .line 122918
    move-object v0, v4

    .line 122919
    sput-object v0, LX/0jM;->d:LX/0jM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122920
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 122921
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122922
    :cond_1
    sget-object v0, LX/0jM;->d:LX/0jM;

    return-object v0

    .line 122923
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 122924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 122896
    iget-object v0, p0, LX/0jM;->b:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 122897
    :goto_0
    return v0

    .line 122898
    :cond_0
    iget-object v0, p0, LX/0jM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSJ;

    .line 122899
    invoke-interface {v0, p2}, LX/CSJ;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122900
    invoke-interface {v0, p2}, LX/CSJ;->b(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122901
    invoke-interface {v0, p2}, LX/CSJ;->c(Landroid/content/Intent;)LX/AEn;

    move-result-object v0

    .line 122902
    new-instance v2, LX/BEF;

    invoke-direct {v2, p1}, LX/BEF;-><init>(Landroid/content/Context;)V

    .line 122903
    iput-object v0, v2, LX/BEF;->c:LX/AEn;

    .line 122904
    iget-object v1, p0, LX/0jM;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2hU;

    const/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v1

    .line 122905
    iput-object p3, v1, LX/4nS;->i:Landroid/view/View;

    .line 122906
    invoke-virtual {v1}, LX/4nS;->a()V

    .line 122907
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 122908
    goto :goto_0
.end method
