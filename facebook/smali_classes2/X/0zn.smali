.class public LX/0zn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/CharSequence;I)Z
    .locals 2

    .prologue
    .line 168022
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 168023
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextDirection()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 168024
    sget-object v0, LX/0zo;->c:LX/0zr;

    :goto_0
    move-object v0, v0

    .line 168025
    :goto_1
    move-object v0, v0

    .line 168026
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p2}, LX/0zr;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0

    .line 168027
    :cond_0
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    sget-object v0, LX/0zo;->d:LX/0zr;

    :goto_2
    move-object v0, v0

    .line 168028
    goto :goto_1

    .line 168029
    :pswitch_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, LX/0zo;->d:LX/0zr;

    goto :goto_0

    :cond_1
    sget-object v0, LX/0zo;->c:LX/0zr;

    goto :goto_0

    .line 168030
    :pswitch_1
    sget-object v0, LX/0zo;->e:LX/0zr;

    goto :goto_0

    .line 168031
    :pswitch_2
    sget-object v0, LX/0zo;->a:LX/0zr;

    goto :goto_0

    .line 168032
    :pswitch_3
    sget-object v0, LX/0zo;->b:LX/0zr;

    goto :goto_0

    .line 168033
    :pswitch_4
    sget-object v0, LX/0zo;->f:LX/0zr;

    goto :goto_0

    :cond_2
    sget-object v0, LX/0zo;->c:LX/0zr;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
