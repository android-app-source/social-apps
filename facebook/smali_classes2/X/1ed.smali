.class public LX/1ed;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1bh;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 288670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288671
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    .line 288672
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288674
    iget-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 288673
    iget-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 288675
    if-ne p1, p0, :cond_0

    .line 288676
    const/4 v0, 0x1

    .line 288677
    :goto_0
    return v0

    .line 288678
    :cond_0
    instance-of v0, p1, LX/1ed;

    if-eqz v0, :cond_1

    .line 288679
    check-cast p1, LX/1ed;

    .line 288680
    iget-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    iget-object v1, p1, LX/1ed;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 288681
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 288668
    iget-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288669
    iget-object v0, p0, LX/1ed;->a:Ljava/lang/String;

    return-object v0
.end method
