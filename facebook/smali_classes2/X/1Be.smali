.class public LX/1Be;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile P:LX/1Be;

.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Landroid/webkit/CookieManager;

.field private B:Ljava/lang/Boolean;

.field private C:Ljava/lang/String;

.field public D:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public F:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "LX/1Bu;",
            ">;"
        }
    .end annotation
.end field

.field public G:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/Boolean;

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field

.field private O:LX/1Bh;

.field public final b:Landroid/content/Context;

.field private final c:LX/0W3;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Bn;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7hw;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/03V;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field public final h:Ljava/util/concurrent/ExecutorService;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:Landroid/os/Handler;

.field public final k:LX/0ad;

.field private final l:LX/0ka;

.field public final m:LX/0kb;

.field private final n:LX/0Uh;

.field public final o:LX/01T;

.field public final p:LX/0WV;

.field public final q:LX/1Bf;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Bj;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Bv;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7i2;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/121;

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Br;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/1Bp;

.field public final z:LX/1Bg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213639
    const-class v0, LX/1Be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Be;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/03V;LX/0W3;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ka;LX/0kb;LX/0Ot;Landroid/content/Context;LX/0Uh;Ljava/util/concurrent/ExecutorService;LX/0Wb;Landroid/os/Handler;LX/01T;LX/0WV;LX/1Bf;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1Bg;LX/121;LX/0Ot;)V
    .locals 8
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BrowserBackgroundRequestExecutor;
        .end annotation
    .end param
    .param p13    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/browser/prefetch/BrowserPrefetchHandler;
        .end annotation
    .end param
    .param p21    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Bn;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ka;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/7hw;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Wb;",
            "Landroid/os/Handler;",
            "LX/01T;",
            "LX/0WV;",
            "LX/1Bf;",
            "LX/0Ot",
            "<",
            "LX/1Bj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Bv;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7i2;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1Bg;",
            "LX/121;",
            "LX/0Ot",
            "<",
            "LX/1Br;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213641
    const/4 v2, 0x0

    iput-object v2, p0, LX/1Be;->I:Ljava/lang/Boolean;

    .line 213642
    const/16 v2, 0xa

    iput v2, p0, LX/1Be;->J:I

    .line 213643
    const/4 v2, 0x0

    iput v2, p0, LX/1Be;->K:I

    .line 213644
    const/16 v2, 0x14

    iput v2, p0, LX/1Be;->L:I

    .line 213645
    const/4 v2, 0x0

    iput v2, p0, LX/1Be;->M:I

    .line 213646
    iput-object p1, p0, LX/1Be;->d:LX/0Ot;

    .line 213647
    iput-object p2, p0, LX/1Be;->f:LX/03V;

    .line 213648
    iput-object p3, p0, LX/1Be;->c:LX/0W3;

    .line 213649
    iput-object p4, p0, LX/1Be;->k:LX/0ad;

    .line 213650
    iput-object p6, p0, LX/1Be;->l:LX/0ka;

    .line 213651
    iput-object p7, p0, LX/1Be;->m:LX/0kb;

    .line 213652
    iput-object p5, p0, LX/1Be;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 213653
    move-object/from16 v0, p8

    iput-object v0, p0, LX/1Be;->e:LX/0Ot;

    .line 213654
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1Be;->b:Landroid/content/Context;

    .line 213655
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1Be;->n:LX/0Uh;

    .line 213656
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1Be;->g:Ljava/util/concurrent/ExecutorService;

    .line 213657
    move-object/from16 v0, p12

    move-object/from16 v1, p11

    invoke-virtual {v0, v1}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v2

    iput-object v2, p0, LX/1Be;->h:Ljava/util/concurrent/ExecutorService;

    .line 213658
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Be;->j:Landroid/os/Handler;

    .line 213659
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Be;->o:LX/01T;

    .line 213660
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Be;->p:LX/0WV;

    .line 213661
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Be;->q:LX/1Bf;

    .line 213662
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Be;->r:LX/0Ot;

    .line 213663
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1Be;->s:LX/0Ot;

    .line 213664
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1Be;->t:LX/0Ot;

    .line 213665
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1Be;->u:LX/0Ot;

    .line 213666
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1Be;->v:LX/0Or;

    .line 213667
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1Be;->z:LX/1Bg;

    .line 213668
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1Be;->w:LX/121;

    .line 213669
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1Be;->x:LX/0Ot;

    .line 213670
    new-instance v2, Landroid/util/LruCache;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Landroid/util/LruCache;-><init>(I)V

    iput-object v2, p0, LX/1Be;->F:Landroid/util/LruCache;

    .line 213671
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v2, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    .line 213672
    new-instance v2, LX/1Bh;

    sget-object v3, LX/0p3;->MODERATE:LX/0p3;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/1Bh;-><init>(LX/0p3;IZZZ)V

    iput-object v2, p0, LX/1Be;->O:LX/1Bh;

    .line 213673
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 213634
    invoke-virtual {p0}, LX/1Be;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213635
    if-ne p1, v1, :cond_0

    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v1, LX/1Bi;->k:I

    iget v2, p0, LX/1Be;->L:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 213636
    :goto_0
    return v0

    .line 213637
    :cond_0
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v1, LX/1Bi;->m:I

    iget v2, p0, LX/1Be;->J:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 213638
    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v1, LX/1Bi;->j:I

    iget v2, p0, LX/1Be;->M:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v1, LX/1Bi;->l:I

    iget v2, p0, LX/1Be;->K:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Be;
    .locals 3

    .prologue
    .line 213624
    sget-object v0, LX/1Be;->P:LX/1Be;

    if-nez v0, :cond_1

    .line 213625
    const-class v1, LX/1Be;

    monitor-enter v1

    .line 213626
    :try_start_0
    sget-object v0, LX/1Be;->P:LX/1Be;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213627
    if-eqz v2, :cond_0

    .line 213628
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Be;->b(LX/0QB;)LX/1Be;

    move-result-object v0

    sput-object v0, LX/1Be;->P:LX/1Be;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213631
    :cond_1
    sget-object v0, LX/1Be;->P:LX/1Be;

    return-object v0

    .line 213632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1Be;Ljava/lang/String;ZZ)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213609
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/1Be;->E:Ljava/util/HashMap;

    .line 213610
    :goto_0
    if-nez v0, :cond_2

    .line 213611
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 213612
    iget-object v0, p0, LX/1Be;->C:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213613
    const-string v0, "User-Agent"

    iget-object v2, p0, LX/1Be;->C:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213614
    :cond_0
    :goto_1
    if-nez p3, :cond_3

    move-object v0, v1

    .line 213615
    :goto_2
    return-object v0

    .line 213616
    :cond_1
    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    goto :goto_0

    .line 213617
    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_1

    .line 213618
    :cond_3
    invoke-static {p0}, LX/1Be;->n(LX/1Be;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 213619
    iget-object v0, p0, LX/1Be;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bj;

    const/4 v2, 0x2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1Bj;->a(ILandroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 213620
    :goto_3
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 213621
    const-string v2, "Cookie"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    .line 213622
    goto :goto_2

    .line 213623
    :cond_5
    iget-object v0, p0, LX/1Be;->A:Landroid/webkit/CookieManager;

    invoke-virtual {v0, p1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static a$redex0(LX/1Be;Landroid/net/Uri;)Z
    .locals 5
    .param p0    # LX/1Be;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 213598
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 213599
    :goto_0
    return v0

    .line 213600
    :cond_1
    iget-object v0, p0, LX/1Be;->H:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Be;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    .line 213601
    goto :goto_0

    .line 213602
    :cond_3
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 213603
    iget-object v0, p0, LX/1Be;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213604
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result p0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p0, p1

    .line 213605
    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    if-eqz p0, :cond_5

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {v3, p0}, Ljava/lang/String;->codePointAt(I)I

    move-result p0

    const/16 p1, 0x2e

    if-ne p0, p1, :cond_7

    :cond_5
    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 213606
    if-eqz v0, :cond_4

    move v0, v1

    .line 213607
    goto :goto_0

    :cond_6
    move v0, v2

    .line 213608
    goto :goto_0

    :cond_7
    const/4 p0, 0x0

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/1Be;
    .locals 27

    .prologue
    .line 213596
    new-instance v2, LX/1Be;

    const/16 v3, 0x1f2

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v8

    check-cast v8, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v9

    check-cast v9, LX/0kb;

    const/16 v10, 0x181e

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1Bk;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Wb;->a(LX/0QB;)LX/0Wb;

    move-result-object v14

    check-cast v14, LX/0Wb;

    invoke-static/range {p0 .. p0}, LX/1Bl;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v15

    check-cast v15, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v16

    check-cast v16, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v17

    check-cast v17, LX/0WV;

    invoke-static/range {p0 .. p0}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v18

    check-cast v18, LX/1Bf;

    const/16 v19, 0x1ea

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x135e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1f7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x181f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x2fd

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {p0 .. p0}, LX/1Bg;->a(LX/0QB;)LX/1Bg;

    move-result-object v24

    check-cast v24, LX/1Bg;

    invoke-static/range {p0 .. p0}, LX/128;->a(LX/0QB;)LX/128;

    move-result-object v25

    check-cast v25, LX/121;

    const/16 v26, 0x1f5

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-direct/range {v2 .. v26}, LX/1Be;-><init>(LX/0Ot;LX/03V;LX/0W3;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ka;LX/0kb;LX/0Ot;Landroid/content/Context;LX/0Uh;Ljava/util/concurrent/ExecutorService;LX/0Wb;Landroid/os/Handler;LX/01T;LX/0WV;LX/1Bf;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1Bg;LX/121;LX/0Ot;)V

    .line 213597
    return-object v2
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213589
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 213590
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213591
    :goto_0
    return-object p1

    .line 213592
    :cond_0
    invoke-static {p0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 213593
    invoke-virtual {v0, p1}, Ljava/net/URI;->resolve(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 213594
    :catch_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 213595
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    .line 213588
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v1, LX/1Bi;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static m(LX/1Be;)Z
    .locals 3

    .prologue
    .line 213587
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v1, LX/1Bi;->q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static n(LX/1Be;)Z
    .locals 3

    .prologue
    .line 213586
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v1, LX/1Bm;->H:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static o(LX/1Be;)Z
    .locals 3

    .prologue
    .line 213674
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v1, LX/1Bm;->E:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static p(LX/1Be;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213392
    iget-object v0, p0, LX/1Be;->B:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 213393
    iget-object v0, p0, LX/1Be;->B:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 213394
    :cond_0
    :goto_0
    return v2

    .line 213395
    :cond_1
    iget-object v0, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    invoke-virtual {v0}, LX/1Bn;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    .line 213396
    :try_start_0
    iget-object v3, p0, LX/1Be;->b:Landroid/content/Context;

    invoke-static {v3}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 213397
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v3

    iput-object v3, p0, LX/1Be;->A:Landroid/webkit/CookieManager;

    .line 213398
    iget-object v3, p0, LX/1Be;->A:Landroid/webkit/CookieManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    .line 213399
    :cond_2
    :goto_1
    move v0, v0

    .line 213400
    if-eqz v0, :cond_7

    invoke-direct {p0}, LX/1Be;->q()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1Be;->B:Ljava/lang/Boolean;

    .line 213401
    iget-object v0, p0, LX/1Be;->B:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213402
    iget-object v0, p0, LX/1Be;->c:LX/0W3;

    sget-wide v4, LX/0X5;->aJ:J

    .line 213403
    :try_start_1
    invoke-interface {v0, v4, v5}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v3

    .line 213404
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 213405
    invoke-static {v6}, LX/16N;->a(Lorg/json/JSONArray;)Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 213406
    :goto_3
    move-object v0, v3

    .line 213407
    iput-object v0, p0, LX/1Be;->H:Ljava/util/List;

    .line 213408
    iget-object v0, p0, LX/1Be;->H:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1Be;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 213409
    :cond_3
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v3, LX/1Bm;->B:I

    const/16 v4, 0x32

    invoke-interface {v0, v3, v4}, LX/0ad;->a(II)I

    move-result v0

    .line 213410
    new-instance v3, LX/1Bo;

    invoke-direct {v3, v0}, LX/1Bo;-><init>(I)V

    invoke-static {v3}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1Be;->G:Ljava/util/Map;

    .line 213411
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_9

    .line 213412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/1Be;->b:Landroid/content/Context;

    invoke-static {v3}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, LX/1Be;->o:LX/01T;

    iget-object v5, p0, LX/1Be;->p:LX/0WV;

    invoke-static {v4, v5}, LX/1Bg;->a(LX/01T;LX/0WV;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213413
    :goto_4
    move-object v0, v0

    .line 213414
    iput-object v0, p0, LX/1Be;->C:Ljava/lang/String;

    .line 213415
    iget-object v0, p0, LX/1Be;->C:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 213416
    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    .line 213417
    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    const-string v3, "User-Agent"

    iget-object v4, p0, LX/1Be;->C:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213418
    :cond_4
    iget-object v0, p0, LX/1Be;->E:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    .line 213419
    iget-object v0, p0, LX/1Be;->E:Ljava/util/HashMap;

    const-string v3, "User-Agent"

    iget-object v4, p0, LX/1Be;->C:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213420
    :cond_5
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v3, LX/1Bi;->t:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 213421
    iget-object v0, p0, LX/1Be;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bp;

    iput-object v0, p0, LX/1Be;->y:LX/1Bp;

    .line 213422
    :goto_5
    iget-object v0, p0, LX/1Be;->y:LX/1Bp;

    new-instance v2, LX/1Bq;

    invoke-direct {v2, p0}, LX/1Bq;-><init>(LX/1Be;)V

    .line 213423
    iput-object v2, v0, LX/1Bp;->a:LX/1Bq;

    .line 213424
    invoke-direct {p0}, LX/1Be;->l()Z

    .line 213425
    invoke-static {p0}, LX/1Be;->m(LX/1Be;)Z

    .line 213426
    iget-object v0, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    invoke-virtual {v0}, LX/1Bn;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    invoke-virtual {v0}, LX/1Bn;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 213427
    iget-object v0, p0, LX/1Be;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Br;

    .line 213428
    iget-object v6, v0, LX/1Br;->g:LX/0ad;

    sget-short v7, LX/1Bm;->e:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_b

    .line 213429
    :cond_6
    :goto_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1Be;->B:Ljava/lang/Boolean;

    move v2, v1

    .line 213430
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 213431
    goto/16 :goto_2

    .line 213432
    :cond_8
    iget-object v0, p0, LX/1Be;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bp;

    iput-object v0, p0, LX/1Be;->y:LX/1Bp;

    goto :goto_5

    .line 213433
    :catch_0
    move-exception v3

    .line 213434
    sget-object v4, LX/1Be;->a:Ljava/lang/String;

    const-string v5, "Failed initialize CookieManager"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 213435
    :catch_1
    move-exception v3

    .line 213436
    const-string v6, "BrowserPrefetchConfig"

    const-string v7, "MobileConfig getBlackListDomains error"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v3, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213437
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 213438
    :cond_9
    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    const-string v3, "User-Agent"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 213439
    iget-object v0, p0, LX/1Be;->D:Ljava/util/HashMap;

    const-string v3, "User-Agent"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_4

    .line 213440
    :cond_a
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 213441
    :cond_b
    new-instance v6, Landroid/util/LruCache;

    const/16 v7, 0xa

    invoke-direct {v6, v7}, Landroid/util/LruCache;-><init>(I)V

    iput-object v6, v0, LX/1Br;->b:Landroid/util/LruCache;

    .line 213442
    iget-object v6, v0, LX/1Br;->m:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    sget-object v7, LX/03R;->YES:LX/03R;

    if-ne v6, v7, :cond_c

    .line 213443
    new-instance v6, Landroid/util/LruCache;

    const/16 v7, 0x32

    invoke-direct {v6, v7}, Landroid/util/LruCache;-><init>(I)V

    iput-object v6, v0, LX/1Br;->c:Landroid/util/LruCache;

    .line 213444
    :cond_c
    const-string v6, "<meta\\s+http-equiv\\s*=\\s*\"\\s*[c|C]ontent-[t|T]ype\\s*\"\\s+content\\s*=\\s*\"\\s*text/html;\\s*charset=(utf-8|UTF-8)\\s*\"\\s*/?>"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    iput-object v6, v0, LX/1Br;->d:Ljava/util/regex/Pattern;

    .line 213445
    const-string v6, "<meta\\s+charset\\s*=\\s*\"\\s*(utf-8|UTF-8)\\s*\"\\s*/?>"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    iput-object v6, v0, LX/1Br;->e:Ljava/util/regex/Pattern;

    .line 213446
    iget-object v6, v0, LX/1Br;->k:LX/0W3;

    sget-wide v8, LX/0X5;->aF:J

    const/4 v7, 0x0

    invoke-interface {v6, v8, v9, v7}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 213447
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 213448
    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/1Br;->f:[Ljava/lang/String;

    goto/16 :goto_6
.end method

.method private q()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213449
    :try_start_0
    iget-object v2, p0, LX/1Be;->c:LX/0W3;

    sget-wide v4, LX/0X5;->aI:J

    const/4 v7, 0x4

    .line 213450
    const-string v3, "[]"

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213451
    :try_start_1
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 213452
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eq v3, v7, :cond_1

    .line 213453
    const/4 v3, 0x0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 213454
    :goto_0
    :try_start_2
    move-object v2, v3

    .line 213455
    if-eqz v2, :cond_0

    array-length v3, v2

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 213456
    const/4 v3, 0x0

    aget v3, v2, v3

    iput v3, p0, LX/1Be;->J:I

    .line 213457
    const/4 v3, 0x1

    aget v3, v2, v3

    iput v3, p0, LX/1Be;->K:I

    .line 213458
    const/4 v3, 0x2

    aget v3, v2, v3

    iput v3, p0, LX/1Be;->L:I

    .line 213459
    const/4 v3, 0x3

    aget v2, v2, v3

    iput v2, p0, LX/1Be;->M:I

    .line 213460
    :cond_0
    iget-object v2, p0, LX/1Be;->c:LX/0W3;

    sget-wide v4, LX/0X5;->aD:J

    invoke-static {v2, v4, v5}, LX/1Bs;->a(LX/0W3;J)Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, LX/1Be;->D:Ljava/util/HashMap;

    .line 213461
    iget-object v2, p0, LX/1Be;->c:LX/0W3;

    sget-wide v4, LX/0X5;->aE:J

    invoke-static {v2, v4, v5}, LX/1Bs;->a(LX/0W3;J)Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, LX/1Be;->E:Ljava/util/HashMap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 213462
    :goto_1
    return v0

    .line 213463
    :catch_0
    move-exception v0

    .line 213464
    iget-object v2, p0, LX/1Be;->f:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/1Be;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".initializeResources.getRequestHeader"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed get request header"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 213465
    goto :goto_1

    .line 213466
    :cond_1
    :try_start_3
    const/4 v3, 0x4

    new-array v3, v3, [I

    .line 213467
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    aput v8, v3, v7

    .line 213468
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    aput v8, v3, v7

    .line 213469
    const/4 v7, 0x2

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    aput v8, v3, v7

    .line 213470
    const/4 v7, 0x3

    const/4 v8, 0x3

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v6

    aput v6, v3, v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 213471
    :catch_1
    const-string v3, "BrowserPrefetchConfig"

    const-string v6, "MobileConfig getResourceLimit error"

    invoke-static {v3, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 213472
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 213473
    iget-object v0, p0, LX/1Be;->h:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/1Be;->q:LX/1Bf;

    .line 213474
    iget-object v2, v1, LX/1Bf;->e:Ljava/lang/Runnable;

    move-object v1, v2

    .line 213475
    const v2, -0x3438ec35    # -2.6093462E7f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 213476
    return-void
.end method

.method public final a(LX/1Bt;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 213477
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 213478
    iget-boolean v3, p1, LX/1Bt;->c:Z

    .line 213479
    invoke-static {p0}, LX/1Be;->p(LX/1Be;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 213480
    if-eqz v3, :cond_0

    .line 213481
    iget-object v0, p1, LX/1Bt;->d:Ljava/lang/String;

    sget-object v1, LX/1Bu;->RESOURCE_INIT_FAIL:LX/1Bu;

    invoke-virtual {p0, v0, v1}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 213482
    :cond_0
    :goto_0
    return-void

    .line 213483
    :cond_1
    iget-object v0, p0, LX/1Be;->w:LX/121;

    sget-object v2, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v2}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213484
    if-eqz v3, :cond_0

    .line 213485
    iget-object v0, p1, LX/1Bt;->d:Ljava/lang/String;

    sget-object v1, LX/1Bu;->ZERO_RATING:LX/1Bu;

    invoke-virtual {p0, v0, v1}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto :goto_0

    .line 213486
    :cond_2
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v2, LX/1Bi;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 213487
    if-eqz v3, :cond_4

    .line 213488
    iget-object v0, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 213489
    iget-object v0, p0, LX/1Be;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bv;

    iget-object v2, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    iget-object v4, p0, LX/1Be;->O:LX/1Bh;

    invoke-virtual {v0, v2, v4, v1}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 213490
    sget-object v1, LX/1Bu;->NO_VIDEO_AUTOPLAY:LX/1Bu;

    .line 213491
    iget-object v0, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 213492
    iget-object v0, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/046;

    iget-object v0, v0, LX/046;->value:Ljava/lang/String;

    iput-object v0, v1, LX/1Bu;->extra:Ljava/lang/String;

    .line 213493
    :cond_3
    iget-object v0, p1, LX/1Bt;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto :goto_0

    .line 213494
    :cond_4
    iget-object v0, p0, LX/1Be;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bv;

    const/4 v2, 0x0

    iget-object v4, p0, LX/1Be;->O:LX/1Bh;

    invoke-virtual {v0, v2, v4}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213495
    :cond_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 213496
    :try_start_0
    iget-object v0, p1, LX/1Bt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213497
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    invoke-virtual {p0, v0, v5}, LX/1Be;->a(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_6

    .line 213498
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 213499
    :catch_0
    move-exception v0

    .line 213500
    if-eqz v3, :cond_7

    .line 213501
    iget-object v1, p1, LX/1Bt;->d:Ljava/lang/String;

    sget-object v2, LX/1Bu;->EXCEPTION:LX/1Bu;

    invoke-virtual {p0, v1, v2}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 213502
    :cond_7
    iget-object v1, p0, LX/1Be;->f:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/1Be;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".prefetch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Prefetch failed urls "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, LX/1Bt;->a:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 213503
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 213504
    move-object v0, v2

    .line 213505
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_0

    .line 213506
    :cond_8
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213507
    if-eqz v3, :cond_a

    .line 213508
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget-short v2, LX/1Bi;->b:S

    const/4 v5, 0x0

    invoke-interface {v0, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 213509
    iget-object v0, p1, LX/1Bt;->d:Ljava/lang/String;

    sget-object v1, LX/1Bu;->DRY_RUN:LX/1Bu;

    invoke-virtual {p0, v0, v1}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto/16 :goto_0

    .line 213510
    :cond_9
    iget-object v0, p1, LX/1Bt;->d:Ljava/lang/String;

    sget-object v2, LX/1Bu;->NOT_FINISH:LX/1Bu;

    invoke-virtual {p0, v0, v2}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 213511
    :cond_a
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 213512
    if-eqz v3, :cond_d

    move v2, v1

    .line 213513
    :goto_2
    if-eqz v3, :cond_b

    invoke-direct {p0}, LX/1Be;->l()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_b
    if-nez v3, :cond_e

    invoke-static {p0}, LX/1Be;->m(LX/1Be;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 213514
    :cond_c
    iget-object v0, p0, LX/1Be;->h:Ljava/util/concurrent/ExecutorService;

    move-object v1, v0

    .line 213515
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213516
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v6

    if-ge v6, v2, :cond_f

    .line 213517
    new-instance v6, LX/1Bw;

    invoke-direct {v6, p0, v0, p1}, LX/1Bw;-><init>(LX/1Be;Ljava/lang/String;LX/1Bt;)V

    move-object v6, v6

    .line 213518
    const v7, 0x6f7e4715

    invoke-static {v1, v6, v7}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v6

    .line 213519
    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 213520
    :cond_d
    iget v0, p1, LX/1Bt;->b:I

    invoke-direct {p0, v0}, LX/1Be;->a(I)I

    move-result v0

    move v2, v0

    goto :goto_2

    .line 213521
    :cond_e
    iget-object v0, p0, LX/1Be;->g:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v0

    goto :goto_3

    .line 213522
    :cond_f
    :try_start_2
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 213523
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 213524
    const-wide/16 v6, 0x4e20

    :try_start_3
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v8, 0x7766d568

    invoke-static {v0, v6, v7, v2, v8}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Bx;

    .line 213525
    if-eqz v3, :cond_10

    if-eqz v2, :cond_10

    iget-object v6, v2, LX/1Bx;->a:LX/1By;

    if-eqz v6, :cond_10

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    if-ne p2, v6, :cond_10

    iget v6, p1, LX/1Bt;->b:I

    invoke-direct {p0, v6}, LX/1Be;->a(I)I

    move-result v6

    if-lez v6, :cond_10

    iget-object v6, v2, LX/1Bx;->b:Ljava/lang/String;

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 213526
    iget-object v6, v2, LX/1Bx;->a:LX/1By;

    iget-object v2, v2, LX/1Bx;->b:Ljava/lang/String;

    .line 213527
    iget-object v7, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1Bn;

    invoke-static {v2}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v2, v8, v6}, LX/1Bn;->a(Ljava/lang/String;Ljava/lang/String;LX/1By;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    move-result-object v7

    .line 213528
    if-nez v7, :cond_15
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 213529
    :goto_6
    goto :goto_5

    .line 213530
    :catch_1
    if-eqz v3, :cond_11

    .line 213531
    :try_start_4
    sget-object v2, LX/1Bu;->TIMEOUT:LX/1Bu;

    invoke-virtual {p0, v1, v2}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 213532
    :cond_11
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 213533
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_5

    .line 213534
    :catch_2
    :try_start_5
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 213535
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_7

    .line 213536
    :catch_3
    move-exception v0

    .line 213537
    :try_start_6
    throw v0

    .line 213538
    :catch_4
    move-exception v2

    .line 213539
    invoke-static {v2}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    .line 213540
    if-eqz v3, :cond_12

    .line 213541
    sget-object v6, LX/1Bu;->EXCEPTION:LX/1Bu;

    invoke-virtual {p0, v1, v6}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 213542
    :cond_12
    iget-object v1, p0, LX/1Be;->f:LX/03V;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LX/1Be;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".prefetch"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Prefetch failed "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 213543
    iput-object v2, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 213544
    move-object v0, v0

    .line 213545
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_5

    .line 213546
    :cond_13
    invoke-static {p0}, LX/1Be;->n(LX/1Be;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1Be;->o(LX/1Be;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Be;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213547
    iget-object v0, p0, LX/1Be;->q:LX/1Bf;

    iget-object v1, p0, LX/1Be;->b:Landroid/content/Context;

    iget-object v2, p0, LX/1Be;->G:Ljava/util/Map;

    const/4 v4, 0x0

    .line 213548
    invoke-static {v1, v2, v4}, LX/048;->a(Landroid/content/Context;Ljava/util/Map;Z)V

    .line 213549
    invoke-static {v0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object v6

    .line 213550
    if-nez v6, :cond_14

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 213551
    :cond_14
    const-string v7, "EXTRA_FLUSH_COOKIES"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213552
    invoke-static {v0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v7

    invoke-static {v1, v2, v7, v6}, LX/049;->a(Landroid/content/Context;Ljava/util/Map;ZLandroid/os/Bundle;)V

    .line 213553
    iget-object v0, p0, LX/1Be;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 213554
    :cond_15
    invoke-static {p0}, LX/1Be;->m(LX/1Be;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 213555
    iget-object v8, p0, LX/1Be;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v9, Lcom/facebook/browser/prefetch/BrowserPrefetcher$6;

    invoke-direct {v9, p0, v7}, Lcom/facebook/browser/prefetch/BrowserPrefetcher$6;-><init>(LX/1Be;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V

    const v7, 0x96ec832

    invoke-static {v8, v9, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_6

    .line 213556
    :cond_16
    iget-object v8, p0, LX/1Be;->q:LX/1Bf;

    iget-object v9, p0, LX/1Be;->b:Landroid/content/Context;

    invoke-virtual {v8, v9, v7}, LX/1Bf;->a(Landroid/content/Context;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V

    goto/16 :goto_6
.end method

.method public final a(Ljava/lang/String;LX/1Bu;)V
    .locals 1

    .prologue
    .line 213557
    iget-object v0, p0, LX/1Be;->F:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213558
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 213559
    if-eqz p3, :cond_0

    const/4 v0, 0x2

    .line 213560
    :goto_0
    new-instance v2, LX/1Bt;

    new-array v3, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1, p1}, LX/1Bt;-><init>(Ljava/util/List;IZLjava/lang/String;)V

    invoke-virtual {p0, v2, p2}, LX/1Be;->a(LX/1Bt;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;)V

    .line 213561
    return-void

    :cond_0
    move v0, v1

    .line 213562
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213563
    iget-object v0, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    .line 213564
    iget-object v3, v0, LX/1Bn;->j:Ljava/util/Map;

    invoke-static {p1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    move v0, v3

    .line 213565
    if-eqz v0, :cond_0

    .line 213566
    :goto_0
    return v1

    .line 213567
    :cond_0
    if-nez p2, :cond_1

    move v1, v2

    .line 213568
    goto :goto_0

    .line 213569
    :cond_1
    iget-object v0, p0, LX/1Be;->F:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bu;

    .line 213570
    if-eqz v0, :cond_2

    invoke-static {}, LX/1Bu;->getCatchThemAllReasonInPrepare()LX/1Bu;

    move-result-object v3

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    .line 213571
    goto :goto_0

    :cond_2
    move v0, v2

    .line 213572
    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213573
    iget-object v2, p0, LX/1Be;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1C0;->a:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/1Be;->k:LX/0ad;

    sget-short v3, LX/1Bi;->s:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213574
    iget-object v0, p0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    invoke-virtual {v0}, LX/1Bn;->b()V

    .line 213575
    iget-object v0, p0, LX/1Be;->F:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 213576
    iget-object v0, p0, LX/1Be;->N:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 213577
    iput-object v1, p0, LX/1Be;->G:Ljava/util/Map;

    .line 213578
    iput-object v1, p0, LX/1Be;->C:Ljava/lang/String;

    .line 213579
    iput-object v1, p0, LX/1Be;->D:Ljava/util/HashMap;

    .line 213580
    iput-object v1, p0, LX/1Be;->E:Ljava/util/HashMap;

    .line 213581
    iput-object v1, p0, LX/1Be;->y:LX/1Bp;

    .line 213582
    iput-object v1, p0, LX/1Be;->B:Ljava/lang/Boolean;

    .line 213583
    return-void
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 213585
    iget-object v0, p0, LX/1Be;->k:LX/0ad;

    sget v1, LX/1Bi;->u:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 213584
    iget-object v0, p0, LX/1Be;->l:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Be;->m:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
