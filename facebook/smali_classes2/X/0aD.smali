.class public LX/0aD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0aD;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84384
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0aD;->a:Z

    .line 84385
    return-void
.end method

.method public static a(LX/0QB;)LX/0aD;
    .locals 3

    .prologue
    .line 84371
    sget-object v0, LX/0aD;->b:LX/0aD;

    if-nez v0, :cond_1

    .line 84372
    const-class v1, LX/0aD;

    monitor-enter v1

    .line 84373
    :try_start_0
    sget-object v0, LX/0aD;->b:LX/0aD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84374
    if-eqz v2, :cond_0

    .line 84375
    :try_start_1
    new-instance v0, LX/0aD;

    invoke-direct {v0}, LX/0aD;-><init>()V

    .line 84376
    move-object v0, v0

    .line 84377
    sput-object v0, LX/0aD;->b:LX/0aD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84378
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84379
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84380
    :cond_1
    sget-object v0, LX/0aD;->b:LX/0aD;

    return-object v0

    .line 84381
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84382
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
