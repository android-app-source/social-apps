.class public LX/0VP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/0VP;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67646
    const-class v0, LX/0VP;

    sput-object v0, LX/0VP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67648
    iput-object p1, p0, LX/0VP;->b:Landroid/content/Context;

    .line 67649
    return-void
.end method

.method public static a(LX/0QB;)LX/0VP;
    .locals 4

    .prologue
    .line 67650
    sget-object v0, LX/0VP;->c:LX/0VP;

    if-nez v0, :cond_1

    .line 67651
    const-class v1, LX/0VP;

    monitor-enter v1

    .line 67652
    :try_start_0
    sget-object v0, LX/0VP;->c:LX/0VP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67653
    if-eqz v2, :cond_0

    .line 67654
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67655
    new-instance p0, LX/0VP;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0VP;-><init>(Landroid/content/Context;)V

    .line 67656
    move-object v0, p0

    .line 67657
    sput-object v0, LX/0VP;->c:LX/0VP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67658
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67659
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67660
    :cond_1
    sget-object v0, LX/0VP;->c:LX/0VP;

    return-object v0

    .line 67661
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67662
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/io/Closeable;Z)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 67663
    :try_start_0
    invoke-static {p0, p1}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67664
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 67665
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static final a(Ljava/io/File;Ljava/io/File;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 67666
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 67667
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 67668
    :try_start_2
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v3}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 67669
    const/16 v0, 0x400

    :try_start_3
    new-array v0, v0, [B

    .line 67670
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_0

    .line 67671
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v2}, Ljava/util/zip/GZIPOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 67672
    :catchall_0
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    :goto_1
    invoke-static {v3, v7}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v3

    .line 67673
    invoke-static {v1, v7}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v1

    .line 67674
    invoke-static {v2, v7}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v2

    .line 67675
    if-eqz v3, :cond_3

    .line 67676
    throw v3

    .line 67677
    :cond_0
    :try_start_4
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->flush()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 67678
    invoke-static {v4, v6}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v0

    .line 67679
    invoke-static {v1, v6}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v1

    .line 67680
    invoke-static {v3, v6}, LX/0VP;->a(Ljava/io/Closeable;Z)Ljava/io/IOException;

    move-result-object v2

    .line 67681
    if-eqz v0, :cond_1

    .line 67682
    throw v0

    .line 67683
    :cond_1
    if-eqz v1, :cond_2

    .line 67684
    throw v1

    .line 67685
    :cond_2
    if-eqz v2, :cond_6

    .line 67686
    throw v2

    .line 67687
    :cond_3
    if-eqz v1, :cond_4

    .line 67688
    throw v1

    .line 67689
    :cond_4
    if-eqz v2, :cond_5

    .line 67690
    throw v2

    .line 67691
    :cond_5
    throw v0

    :cond_6
    return-void

    .line 67692
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67693
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67694
    iget-object v0, p0, LX/0VP;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 67695
    if-nez p1, :cond_0

    .line 67696
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/File;

    .line 67697
    :goto_0
    return-object v0

    .line 67698
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 67699
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-nez p1, :cond_2

    .line 67700
    :cond_1
    const/4 p1, 0x0

    .line 67701
    :goto_1
    move-object v0, p1

    .line 67702
    goto :goto_0

    .line 67703
    :cond_2
    new-instance p1, LX/2Hs;

    invoke-direct {p1, p0, p2}, LX/2Hs;-><init>(LX/0VP;Ljava/util/regex/Pattern;)V

    invoke-virtual {v0, p1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object p1

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 67704
    :try_start_0
    iget-object v1, p0, LX/0VP;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 67705
    :goto_0
    return-object v0

    .line 67706
    :catch_0
    move-exception v1

    .line 67707
    sget-object v2, LX/0VP;->a:Ljava/lang/Class;

    const-string v3, "Error: Failure while trying to access external files storage directroy"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
