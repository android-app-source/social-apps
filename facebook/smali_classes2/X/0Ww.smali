.class public LX/0Ww;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Wx;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/0Ww;


# instance fields
.field private b:LX/0Wx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76830
    const-class v0, LX/0Ww;

    sput-object v0, LX/0Ww;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76832
    new-instance v0, LX/0Wy;

    invoke-direct {v0}, LX/0Wy;-><init>()V

    iput-object v0, p0, LX/0Ww;->b:LX/0Wx;

    .line 76833
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ww;
    .locals 3

    .prologue
    .line 76834
    sget-object v0, LX/0Ww;->c:LX/0Ww;

    if-nez v0, :cond_1

    .line 76835
    const-class v1, LX/0Ww;

    monitor-enter v1

    .line 76836
    :try_start_0
    sget-object v0, LX/0Ww;->c:LX/0Ww;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76837
    if-eqz v2, :cond_0

    .line 76838
    :try_start_1
    new-instance v0, LX/0Ww;

    invoke-direct {v0}, LX/0Ww;-><init>()V

    .line 76839
    move-object v0, v0

    .line 76840
    sput-object v0, LX/0Ww;->c:LX/0Ww;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76841
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76842
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76843
    :cond_1
    sget-object v0, LX/0Ww;->c:LX/0Ww;

    return-object v0

    .line 76844
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76845
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0eT;
    .locals 1

    .prologue
    .line 76846
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->a()LX/0eT;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(LX/0Wx;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 76847
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0Ww;->d()LX/0Wx;

    move-result-object v2

    .line 76848
    iput-object p1, p0, LX/0Ww;->b:LX/0Wx;

    .line 76849
    instance-of v1, v2, LX/0eR;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0Ww;->b:LX/0Wx;

    instance-of v1, v1, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;

    if-eqz v1, :cond_1

    .line 76850
    move-object v0, v2

    check-cast v0, LX/0eR;

    move-object v1, v0

    .line 76851
    iget-object v0, v1, LX/0eR;->c:Ljava/util/List;

    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    move-object v6, v0

    .line 76852
    if-eqz v6, :cond_0

    .line 76853
    invoke-virtual {v6}, LX/1c9;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    invoke-virtual {v6, v5}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 76854
    iget-object v8, p0, LX/0Ww;->b:LX/0Wx;

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v8, v3, v1}, LX/0Wx;->logExposure(Ljava/lang/String;Ljava/lang/String;)V

    .line 76855
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 76856
    :cond_0
    check-cast v2, LX/0eR;

    .line 76857
    iget-object v0, v2, LX/0eR;->d:Ljava/util/List;

    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    move-object v9, v0

    .line 76858
    if-eqz v6, :cond_1

    .line 76859
    invoke-virtual {v9}, LX/1c9;->size()I

    move-result v10

    move v8, v4

    :goto_1
    if-ge v8, v10, :cond_1

    invoke-virtual {v9, v8}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/33J;

    move-object v7, v0

    .line 76860
    iget-object v1, p0, LX/0Ww;->b:LX/0Wx;

    iget-object v2, v7, LX/33J;->a:Ljava/lang/String;

    iget-object v3, v7, LX/33J;->b:Ljava/lang/String;

    iget-object v4, v7, LX/33J;->c:Ljava/lang/String;

    iget-object v5, v7, LX/33J;->d:Ljava/lang/String;

    iget-object v6, v7, LX/33J;->e:Ljava/lang/String;

    iget-object v7, v7, LX/33J;->f:Ljava/lang/String;

    invoke-interface/range {v1 .. v7}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76861
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    .line 76862
    :cond_1
    iget-object v1, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v1}, LX/0Wx;->isValid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76863
    monitor-exit p0

    return-void

    .line 76864
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 76865
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 76866
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    return-object v0
.end method

.method public final clearCurrentUserData()V
    .locals 1

    .prologue
    .line 76867
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->clearCurrentUserData()V

    .line 76868
    return-void
.end method

.method public final clearOverrides()V
    .locals 1

    .prologue
    .line 76869
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->clearOverrides()V

    .line 76870
    return-void
.end method

.method public final declared-synchronized d()LX/0Wx;
    .locals 1

    .prologue
    .line 76871
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final deleteOldUserData(I)V
    .locals 1

    .prologue
    .line 76828
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1}, LX/0Wx;->deleteOldUserData(I)V

    .line 76829
    return-void
.end method

.method public final getFrameworkStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76872
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->getFrameworkStatus()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getQEInfoFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76813
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->getQEInfoFilename()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isQEInfoAvailable()Z
    .locals 1

    .prologue
    .line 76814
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->isQEInfoAvailable()Z

    move-result v0

    return v0
.end method

.method public final isTigonServiceSet()Z
    .locals 1

    .prologue
    .line 76815
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->isTigonServiceSet()Z

    move-result v0

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .prologue
    .line 76816
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->isValid()Z

    move-result v0

    return v0
.end method

.method public final logExposure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76817
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1, p2}, LX/0Wx;->logExposure(Ljava/lang/String;Ljava/lang/String;)V

    .line 76818
    return-void
.end method

.method public final logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 76819
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76820
    return-void
.end method

.method public final refreshConfigInfos(I)Z
    .locals 1

    .prologue
    .line 76821
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1}, LX/0Wx;->refreshConfigInfos(I)Z

    move-result v0

    return v0
.end method

.method public final registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
    .locals 1

    .prologue
    .line 76822
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1}, LX/0Wx;->registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z

    move-result v0

    return v0
.end method

.method public final setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
    .locals 1

    .prologue
    .line 76823
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1, p2}, LX/0Wx;->setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V

    .line 76824
    return-void
.end method

.method public final tryUpdateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 76825
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1}, LX/0Wx;->tryUpdateConfigsSynchronously(I)Z

    move-result v0

    return v0
.end method

.method public final updateConfigs()Z
    .locals 1

    .prologue
    .line 76826
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->updateConfigs()Z

    move-result v0

    return v0
.end method

.method public final updateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 76827
    iget-object v0, p0, LX/0Ww;->b:LX/0Wx;

    invoke-interface {v0, p1}, LX/0Wx;->updateConfigsSynchronously(I)Z

    move-result v0

    return v0
.end method
