.class public LX/1L0;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fs;


# instance fields
.field private final a:LX/0bH;

.field private final b:LX/1L1;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1L2;

.field private final f:LX/1L5;

.field public g:LX/1Pq;

.field public h:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0bH;LX/1L1;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;",
            "LX/0bH;",
            "LX/1L1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 233429
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233430
    new-instance v0, LX/1L2;

    invoke-direct {v0, p0}, LX/1L2;-><init>(LX/1L0;)V

    iput-object v0, p0, LX/1L0;->e:LX/1L2;

    .line 233431
    new-instance v0, LX/1L5;

    invoke-direct {v0, p0}, LX/1L5;-><init>(LX/1L0;)V

    iput-object v0, p0, LX/1L0;->f:LX/1L5;

    .line 233432
    iput-object p3, p0, LX/1L0;->a:LX/0bH;

    .line 233433
    iput-object p4, p0, LX/1L0;->b:LX/1L1;

    .line 233434
    iput-object p2, p0, LX/1L0;->c:LX/0Ot;

    .line 233435
    iput-object p1, p0, LX/1L0;->d:LX/0Ot;

    .line 233436
    return-void
.end method

.method public static a$redex0(LX/1L0;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 233437
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 233438
    :goto_0
    return-object v0

    .line 233439
    :cond_0
    iget-object v0, p0, LX/1L0;->h:LX/1Iu;

    .line 233440
    iget-object v2, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 233441
    check-cast v0, LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 233442
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 233443
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 233444
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_1

    .line 233445
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 233446
    goto :goto_0
.end method

.method public static a$redex0(LX/1L0;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233447
    iget-object v0, p0, LX/1L0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 233448
    invoke-static {v0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 233449
    iget-object v0, p0, LX/1L0;->h:LX/1Iu;

    .line 233450
    iget-object p0, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 233451
    check-cast v0, LX/0qq;

    invoke-virtual {v0, v1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 233452
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 233453
    iput-object p3, p0, LX/1L0;->g:LX/1Pq;

    .line 233454
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 233455
    iget-object v0, p0, LX/1L0;->a:LX/0bH;

    iget-object v1, p0, LX/1L0;->f:LX/1L5;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 233456
    iget-object v0, p0, LX/1L0;->b:LX/1L1;

    iget-object v1, p0, LX/1L0;->e:LX/1L2;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 233457
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 233458
    iget-object v0, p0, LX/1L0;->a:LX/0bH;

    iget-object v1, p0, LX/1L0;->f:LX/1L5;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 233459
    iget-object v0, p0, LX/1L0;->b:LX/1L1;

    iget-object v1, p0, LX/1L0;->e:LX/1L2;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 233460
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 233461
    const/4 v0, 0x0

    iput-object v0, p0, LX/1L0;->g:LX/1Pq;

    .line 233462
    return-void
.end method
