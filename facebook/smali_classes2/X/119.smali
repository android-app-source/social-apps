.class public LX/119;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/119;


# instance fields
.field private final b:LX/117;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0SG;

.field public final e:LX/0lC;

.field public final f:LX/03V;

.field private final g:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(LX/117;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0lC;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170578
    iput-object p1, p0, LX/119;->b:LX/117;

    .line 170579
    iput-object p2, p0, LX/119;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 170580
    iput-object p3, p0, LX/119;->d:LX/0SG;

    .line 170581
    iput-object p4, p0, LX/119;->e:LX/0lC;

    .line 170582
    iput-object p5, p0, LX/119;->f:LX/03V;

    .line 170583
    iput-object p6, p0, LX/119;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 170584
    return-void
.end method

.method public static a(LX/0QB;)LX/119;
    .locals 10

    .prologue
    .line 170506
    sget-object v0, LX/119;->h:LX/119;

    if-nez v0, :cond_1

    .line 170507
    const-class v1, LX/119;

    monitor-enter v1

    .line 170508
    :try_start_0
    sget-object v0, LX/119;->h:LX/119;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 170509
    if-eqz v2, :cond_0

    .line 170510
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 170511
    new-instance v3, LX/119;

    invoke-static {v0}, LX/116;->b(LX/0QB;)LX/116;

    move-result-object v4

    check-cast v4, LX/117;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v3 .. v9}, LX/119;-><init>(LX/117;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0lC;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 170512
    move-object v0, v3

    .line 170513
    sput-object v0, LX/119;->h:LX/119;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170514
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 170515
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170516
    :cond_1
    sget-object v0, LX/119;->h:LX/119;

    return-object v0

    .line 170517
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 170518
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/119;LX/16M;LX/0lF;)Lcom/facebook/interstitial/api/FetchInterstitialResult;
    .locals 11

    .prologue
    .line 170546
    const-string v0, "InterstitialManager.deserializeInterstitialResult"

    const v1, -0x72f6f0c3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 170547
    :try_start_0
    const-string v0, "nux_id"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    .line 170548
    const-string v0, "nux_type"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170549
    const-string v1, "rank"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    .line 170550
    sget-object v3, LX/16M;->CACHE:LX/16M;

    if-ne p1, v3, :cond_1

    .line 170551
    const-string v3, "fetchTimeMs"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->c(LX/0lF;)J

    move-result-wide v4

    .line 170552
    :goto_0
    const/4 v3, 0x0

    .line 170553
    invoke-direct {p0, v2, v0}, LX/119;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 170554
    if-eqz v0, :cond_0

    .line 170555
    const-string v3, "InterstitialManager.deserializeInterstitialResult#%s#%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const v7, -0x438b6064

    invoke-static {v3, v6, v7}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 170556
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 170557
    const-string v3, "nux_data"

    iget-object v6, p0, LX/119;->e:LX/0lC;

    .line 170558
    iget-object v7, v6, LX/0lC;->_typeFactory:LX/0li;

    move-object v6, v7

    .line 170559
    invoke-virtual {v6, v0}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    const/4 v6, 0x0

    .line 170560
    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 170561
    if-nez v7, :cond_2

    .line 170562
    :goto_1
    move-object v0, v6

    .line 170563
    check-cast v0, Landroid/os/Parcelable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170564
    const v3, -0x4e9a81fb

    :try_start_2
    invoke-static {v3}, LX/02m;->a(I)V

    move-object v3, v0

    .line 170565
    :cond_0
    new-instance v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/interstitial/api/FetchInterstitialResult;-><init>(ILjava/lang/String;Landroid/os/Parcelable;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170566
    const v1, -0x553f44be

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 170567
    :cond_1
    :try_start_3
    iget-object v3, p0, LX/119;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    goto :goto_0

    .line 170568
    :catchall_0
    move-exception v0

    const v1, -0x6cf84620

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 170569
    :catchall_1
    move-exception v0

    const v1, 0x5a62679

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 170570
    :cond_2
    :try_start_4
    iget-object v8, p0, LX/119;->e:LX/0lC;

    iget-object v9, p0, LX/119;->e:LX/0lC;

    invoke-virtual {v7, v9}, LX/0lF;->a(LX/0lD;)LX/15w;

    move-result-object v7

    invoke-virtual {v8, v7, v0}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;
    :try_end_4
    .catch LX/28F; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v6

    goto :goto_1

    .line 170571
    :catch_0
    move-exception v7

    .line 170572
    iget-object v8, p0, LX/119;->f:LX/03V;

    const-string v9, "InterstitialRepository"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string p1, "Failed to parse "

    invoke-direct {v10, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string p1, " for nux_id "

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170573
    const-string v8, "InterstitialRepository"

    invoke-virtual {v7}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 170574
    :catch_1
    move-exception v7

    .line 170575
    iget-object v8, p0, LX/119;->f:LX/03V;

    const-string v9, "InterstitialRepository"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string p1, "Failed to parse "

    invoke-direct {v10, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string p1, "for nux_id "

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170576
    const-string v8, "InterstitialRepository"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 170545
    if-nez p0, :cond_0

    const-string v0, "[]"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized a(LX/0hN;)V
    .locals 2

    .prologue
    .line 170542
    const-class v1, LX/119;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/11b;->e:LX/0Tn;

    invoke-interface {p0, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170543
    monitor-exit v1

    return-void

    .line 170544
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(LX/0hN;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 2

    .prologue
    .line 170538
    const-class v1, LX/119;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, LX/11b;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0Tn;

    move-result-object v0

    .line 170539
    invoke-interface {p0, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170540
    monitor-exit v1

    return-void

    .line 170541
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(LX/0hN;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170528
    const-class v1, LX/119;

    monitor-enter v1

    .line 170529
    :try_start_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170530
    :cond_0
    const/4 v0, 0x0

    .line 170531
    :goto_0
    move-object v0, v0

    .line 170532
    invoke-static {p1}, LX/11b;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0Tn;

    move-result-object v2

    .line 170533
    if-eqz v0, :cond_1

    .line 170534
    invoke-interface {p0, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170535
    :goto_1
    monitor-exit v1

    return-void

    .line 170536
    :cond_1
    :try_start_1
    invoke-interface {p0, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 170537
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const-string v0, "~"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized a(LX/0hN;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170523
    const-class v1, LX/119;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 170524
    invoke-static {v0}, LX/11b;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 170525
    invoke-interface {p0, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170526
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170527
    :cond_0
    monitor-exit v1

    return-void
.end method

.method public static declared-synchronized a(LX/0hN;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "LX/16Q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170519
    const-class v2, LX/119;

    monitor-enter v2

    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 170520
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    invoke-virtual {v0}, LX/16Q;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/119;->a(LX/0hN;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170521
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 170522
    :cond_0
    monitor-exit v2

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 170585
    iget-object v0, p0, LX/119;->b:LX/117;

    invoke-virtual {v0, p1}, LX/117;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v0

    .line 170586
    if-nez v0, :cond_0

    .line 170587
    iget-object v0, p0, LX/119;->f:LX/03V;

    const-string v2, "InterstitialRepository"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No controller available for nux_id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and nux_type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 170588
    :goto_0
    return-object v0

    .line 170589
    :cond_0
    instance-of v2, v0, LX/13F;

    if-eqz v2, :cond_1

    .line 170590
    check-cast v0, LX/13F;

    invoke-interface {v0}, LX/13F;->d()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 170591
    goto :goto_0
.end method

.method public static declared-synchronized b(LX/0hN;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170427
    const-class v1, LX/119;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 170428
    invoke-static {v0}, LX/11b;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0Tn;

    move-result-object v0

    .line 170429
    invoke-interface {p0, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170430
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170431
    :cond_0
    monitor-exit v1

    return-void
.end method

.method public static declared-synchronized c(LX/0hN;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170432
    const-class v1, LX/119;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 170433
    invoke-static {p0, v0}, LX/119;->a(LX/0hN;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170434
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170435
    :cond_0
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170436
    sget-object v0, LX/11b;->a:LX/0Tn;

    sget-object p0, LX/11b;->d:LX/0Tn;

    invoke-static {v0, p0}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 170437
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 170438
    const/4 v1, 0x0

    .line 170439
    const-string v0, ""

    invoke-direct {p0, p2, v0}, LX/119;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 170440
    if-eqz v0, :cond_0

    .line 170441
    :try_start_0
    iget-object v2, p0, LX/119;->e:LX/0lC;

    invoke-virtual {v2, p1, v0}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v0

    .line 170442
    :goto_1
    return-object v1

    .line 170443
    :catch_0
    move-exception v0

    .line 170444
    iget-object v2, p0, LX/119;->f:LX/03V;

    const-string v3, "InterstitialRepository"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to parse data for interstitial "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/16M;LX/15w;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/16M;",
            "LX/15w;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170445
    const-string v0, "InterstitialManager#deserializeInterstitialResultList"

    const v1, 0x121283e6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 170446
    :try_start_0
    iget-object v0, p0, LX/119;->e:LX/0lC;

    invoke-virtual {p2, v0}, LX/15w;->a(LX/0lD;)V

    .line 170447
    sget-object v0, LX/119;->a:LX/266;

    if-nez v0, :cond_0

    .line 170448
    new-instance v0, LX/2az;

    invoke-direct {v0}, LX/2az;-><init>()V

    sput-object v0, LX/119;->a:LX/266;

    .line 170449
    :cond_0
    sget-object v0, LX/119;->a:LX/266;

    move-object v0, v0

    .line 170450
    invoke-virtual {p2, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 170451
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 170452
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 170453
    invoke-static {p0, p1, v0}, LX/119;->a(LX/119;LX/16M;LX/0lF;)Lcom/facebook/interstitial/api/FetchInterstitialResult;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170454
    :catchall_0
    move-exception v0

    const v1, 0x4ab2c1f0    # 5857528.0f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, -0x5f57f767

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1
.end method

.method public final declared-synchronized a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 170455
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/11b;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0Tn;

    move-result-object v0

    .line 170456
    iget-object v1, p0, LX/119;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170457
    iget-object v1, p0, LX/119;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x30017

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 170458
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 170459
    if-nez v0, :cond_1

    .line 170460
    const/4 v1, 0x0

    .line 170461
    :goto_0
    move-object v1, v1

    .line 170462
    if-eqz v0, :cond_0

    .line 170463
    iget-object v2, p0, LX/119;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x30017

    invoke-interface {v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 170464
    :cond_0
    iget-object v0, p0, LX/119;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x30017

    invoke-static {v1}, LX/119;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 170465
    iget-object v0, p0, LX/119;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x30017

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170466
    monitor-exit p0

    return-object v1

    .line 170467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170468
    :cond_1
    const-string v2, "~"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 170469
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0hN;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170470
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    .line 170471
    iget-object v2, v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-static {v2}, LX/11b;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170472
    :try_start_1
    iget-object v3, p0, LX/119;->e:LX/0lC;

    invoke-virtual {v3, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-object v3

    .line 170473
    :goto_1
    move-object v0, v3

    .line 170474
    if-eqz v0, :cond_0

    .line 170475
    invoke-interface {p1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 170476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170477
    :cond_0
    :try_start_3
    invoke-interface {p1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 170478
    :cond_1
    monitor-exit p0

    return-void

    .line 170479
    :catch_0
    move-exception v3

    .line 170480
    iget-object v4, p0, LX/119;->f:LX/03V;

    const-string v5, "InterstitialRepository"

    const-string p2, "Failed to serialize interstitial data"

    invoke-virtual {v4, v5, p2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170481
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170482
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/119;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/11b;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170483
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 170484
    if-nez v0, :cond_1

    .line 170485
    :cond_0
    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170486
    monitor-exit p0

    return-object v0

    .line 170487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170488
    :cond_1
    const-string v2, "~"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 170489
    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 170490
    invoke-static {v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a(Ljava/lang/String;)Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170491
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized d(LX/0hN;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170492
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/119;->b()Ljava/util/List;

    move-result-object v0

    .line 170493
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170494
    :cond_0
    const/4 v1, 0x0

    .line 170495
    :goto_0
    move-object v1, v1

    .line 170496
    if-eqz v1, :cond_1

    .line 170497
    sget-object v2, LX/11b;->e:LX/0Tn;

    invoke-interface {p1, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 170498
    :goto_1
    invoke-interface {v0, p2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170499
    monitor-exit p0

    return-object v0

    .line 170500
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170501
    :cond_1
    sget-object v1, LX/11b;->e:LX/0Tn;

    invoke-interface {p1, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_1

    .line 170502
    :cond_2
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v1}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 170503
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 170504
    invoke-static {v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 170505
    :cond_3
    const-string v1, "~"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v1, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
