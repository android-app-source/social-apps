.class public LX/1ar;
.super LX/1ah;
.source ""

# interfaces
.implements LX/1as;


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/1aX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 278757
    invoke-direct {p0, p1}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 278758
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ar;->a:Landroid/graphics/drawable/Drawable;

    .line 278759
    return-void
.end method


# virtual methods
.method public final a(LX/1aX;)V
    .locals 0
    .param p1    # LX/1aX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278768
    iput-object p1, p0, LX/1ar;->c:LX/1aX;

    .line 278769
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 278760
    invoke-virtual {p0}, LX/1ar;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 278761
    :cond_0
    :goto_0
    return-void

    .line 278762
    :cond_1
    iget-object v0, p0, LX/1ar;->c:LX/1aX;

    if-eqz v0, :cond_2

    .line 278763
    iget-object v0, p0, LX/1ar;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->c()V

    .line 278764
    :cond_2
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 278765
    iget-object v0, p0, LX/1ar;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 278766
    iget-object v0, p0, LX/1ar;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/1ar;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278767
    iget-object v0, p0, LX/1ar;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 278770
    const/4 v0, -0x1

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 278753
    const/4 v0, -0x1

    return v0
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 278754
    iget-object v0, p0, LX/1ar;->c:LX/1aX;

    if-eqz v0, :cond_0

    .line 278755
    iget-object v0, p0, LX/1ar;->c:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(Z)V

    .line 278756
    :cond_0
    invoke-super {p0, p1, p2}, LX/1ah;->setVisible(ZZ)Z

    move-result v0

    return v0
.end method
