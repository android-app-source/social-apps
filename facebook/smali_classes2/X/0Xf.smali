.class public LX/0Xf;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/util/concurrent/ExecutorService;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79204
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/concurrent/ExecutorService;
    .locals 3

    .prologue
    .line 79206
    sget-object v0, LX/0Xf;->a:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 79207
    const-class v1, LX/0Xf;

    monitor-enter v1

    .line 79208
    :try_start_0
    sget-object v0, LX/0Xf;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79209
    if-eqz v2, :cond_0

    .line 79210
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79211
    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object p0

    check-cast p0, LX/0TJ;

    invoke-static {p0}, LX/0Su;->b(LX/0TJ;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    move-object v0, p0

    .line 79212
    sput-object v0, LX/0Xf;->a:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79215
    :cond_1
    sget-object v0, LX/0Xf;->a:Ljava/util/concurrent/ExecutorService;

    return-object v0

    .line 79216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79205
    invoke-static {p0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v0

    check-cast v0, LX/0TJ;

    invoke-static {v0}, LX/0Su;->b(LX/0TJ;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method
