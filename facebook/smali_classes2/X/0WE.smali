.class public LX/0WE;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/content/pm/PackageInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/content/pm/PackageInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75635
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    .line 75636
    sget-object v0, LX/0WE;->a:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_1

    .line 75637
    const-class v1, LX/0WE;

    monitor-enter v1

    .line 75638
    :try_start_0
    sget-object v0, LX/0WE;->a:Landroid/content/pm/PackageInfo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75639
    if-eqz v2, :cond_0

    .line 75640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75641
    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {v3, p0}, LX/0VV;->a(Landroid/content/pm/PackageManager;Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    move-object v0, v3

    .line 75642
    sput-object v0, LX/0WE;->a:Landroid/content/pm/PackageInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75643
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75644
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75645
    :cond_1
    sget-object v0, LX/0WE;->a:Landroid/content/pm/PackageInfo;

    return-object v0

    .line 75646
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 75648
    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0, v1}, LX/0VV;->a(Landroid/content/pm/PackageManager;Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method
