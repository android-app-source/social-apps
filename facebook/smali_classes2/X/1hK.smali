.class public LX/1hK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/AnalyticsLogger;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0oz;

.field private final c:LX/0kb;

.field private final d:LX/0YR;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/0kb;LX/0YR;)V
    .locals 3

    .prologue
    .line 295756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295757
    iput-object p1, p0, LX/1hK;->a:LX/0Zb;

    .line 295758
    iput-object p2, p0, LX/1hK;->b:LX/0oz;

    .line 295759
    iput-object p3, p0, LX/1hK;->c:LX/0kb;

    .line 295760
    iput-object p4, p0, LX/1hK;->d:LX/0YR;

    .line 295761
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1hK;->e:Ljava/util/Map;

    .line 295762
    iget-object v0, p0, LX/1hK;->e:Ljava/util/Map;

    const-string v1, "http_stack"

    const-string v2, "liger"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295763
    return-void
.end method


# virtual methods
.method public final reportEvent(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295764
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 295765
    :cond_0
    :goto_0
    return-void

    .line 295766
    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 295767
    iput-object p3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 295768
    const-string v1, "http_liger"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 295769
    iget-object v1, p0, LX/1hK;->c:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 295770
    iget-object v2, p0, LX/1hK;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    .line 295771
    const-string v3, "connection_type"

    invoke-interface {p1, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295772
    const-string v3, "connection_subtype"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295773
    const-string v1, "connqual"

    iget-object v2, p0, LX/1hK;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295774
    iget-object v1, p0, LX/1hK;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 295775
    iget-object v1, p0, LX/1hK;->d:LX/0YR;

    invoke-interface {v1}, LX/0YR;->a()LX/1hM;

    move-result-object v1

    .line 295776
    if-eqz v1, :cond_2

    .line 295777
    invoke-virtual {v1}, LX/1hM;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 295778
    :cond_2
    invoke-virtual {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 295779
    iget-object v1, p0, LX/1hK;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
