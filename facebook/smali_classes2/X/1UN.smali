.class public LX/1UN;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/1UO;


# instance fields
.field private final a:I

.field public b:LX/1Fn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field private e:LX/1lD;

.field public f:LX/1DI;

.field private g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 255861
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 255862
    iput p1, p0, LX/1UN;->a:I

    .line 255863
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 255857
    iput-object v0, p0, LX/1UN;->c:Ljava/lang/String;

    .line 255858
    iput-object v0, p0, LX/1UN;->f:LX/1DI;

    .line 255859
    iput-object v0, p0, LX/1UN;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 255860
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 255847
    iget-object v0, p0, LX/1UN;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 255848
    iget-object v0, p0, LX/1UN;->e:LX/1lD;

    iget-object v1, p0, LX/1UN;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 255849
    if-nez v0, :cond_1

    .line 255850
    :cond_0
    :goto_0
    return-void

    .line 255851
    :cond_1
    sget-object v2, LX/1lt;->a:[I

    invoke-virtual {v0}, LX/1lD;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 255852
    :pswitch_0
    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0

    .line 255853
    :pswitch_1
    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0

    .line 255854
    :pswitch_2
    iget-object v2, p0, LX/1UN;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    if-eqz v2, :cond_2

    .line 255855
    iget-object v2, p0, LX/1UN;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v3, p0, LX/1UN;->f:LX/1DI;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    goto :goto_0

    .line 255856
    :cond_2
    iget-object v2, p0, LX/1UN;->c:Ljava/lang/String;

    iget-object v3, p0, LX/1UN;->f:LX/1DI;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255815
    if-nez p1, :cond_0

    .line 255816
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, LX/1UN;->a:I

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/1UN;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 255817
    iget-object v0, p0, LX/1UN;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 255818
    :goto_0
    return-object v0

    .line 255819
    :cond_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030476

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 255843
    invoke-direct {p0}, LX/1UN;->c()V

    .line 255844
    sget-object v0, LX/1lD;->LOADING:LX/1lD;

    iput-object v0, p0, LX/1UN;->e:LX/1lD;

    .line 255845
    invoke-direct {p0}, LX/1UN;->d()V

    .line 255846
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 255834
    if-nez p4, :cond_0

    .line 255835
    invoke-direct {p0}, LX/1UN;->d()V

    .line 255836
    :goto_0
    return-void

    .line 255837
    :cond_0
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 255838
    const/4 v1, 0x1

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 255839
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    .line 255840
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    mul-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 255841
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 255842
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/1DI;)V
    .locals 1

    .prologue
    .line 255828
    invoke-direct {p0}, LX/1UN;->c()V

    .line 255829
    sget-object v0, LX/1lD;->ERROR:LX/1lD;

    iput-object v0, p0, LX/1UN;->e:LX/1lD;

    .line 255830
    iput-object p1, p0, LX/1UN;->c:Ljava/lang/String;

    .line 255831
    iput-object p2, p0, LX/1UN;->f:LX/1DI;

    .line 255832
    invoke-direct {p0}, LX/1UN;->d()V

    .line 255833
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 255824
    invoke-direct {p0}, LX/1UN;->c()V

    .line 255825
    sget-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    iput-object v0, p0, LX/1UN;->e:LX/1lD;

    .line 255826
    invoke-direct {p0}, LX/1UN;->d()V

    .line 255827
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 255823
    iget-object v0, p0, LX/1UN;->b:LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255822
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 255821
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 0

    .prologue
    .line 255820
    return p1
.end method
