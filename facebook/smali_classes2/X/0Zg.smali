.class public LX/0Zg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/19P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/19P;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83757
    iput-object p1, p0, LX/0Zg;->b:LX/0Ot;

    .line 83758
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83759
    const-string v0, "video_state"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 83760
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 83761
    iget-object v0, p0, LX/0Zg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83762
    iget-object v0, p0, LX/0Zg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    invoke-virtual {v0}, LX/19P;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83763
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "exo_video_decoder="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 83764
    sget-object v2, LX/0Zg;->a:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 83765
    const-string v2, "not_supported"

    sput-object v2, LX/0Zg;->a:Ljava/lang/String;

    .line 83766
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x10

    if-lt v2, p0, :cond_1

    .line 83767
    :try_start_0
    const-string v2, "video/avc"

    const/4 p0, 0x0

    invoke-static {v2, p0}, LX/08v;->a(Ljava/lang/String;Z)LX/08w;

    move-result-object v2

    .line 83768
    if-eqz v2, :cond_2

    iget-object p0, v2, LX/08w;->a:Ljava/lang/String;

    if-eqz p0, :cond_2

    iget-object v2, v2, LX/08w;->a:Ljava/lang/String;

    :goto_0
    sput-object v2, LX/0Zg;->a:Ljava/lang/String;
    :try_end_0
    .catch LX/090; {:try_start_0 .. :try_end_0} :catch_0

    .line 83769
    :cond_1
    :goto_1
    sget-object v2, LX/0Zg;->a:Ljava/lang/String;

    move-object v2, v2

    .line 83770
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83771
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 83772
    :cond_2
    :try_start_1
    const-string v2, "unknown"
    :try_end_1
    .catch LX/090; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method
