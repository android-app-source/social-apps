.class public LX/1my;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/1mx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 314300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314301
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1my;->a:LX/01J;

    return-void
.end method


# virtual methods
.method public final a(LX/1mx;)V
    .locals 3

    .prologue
    .line 314287
    instance-of v0, p1, LX/48L;

    if-eqz v0, :cond_1

    .line 314288
    check-cast p1, LX/48L;

    .line 314289
    const/4 v0, 0x0

    .line 314290
    iget-object v1, p1, LX/48L;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move v1, v1

    .line 314291
    :goto_0
    if-ge v0, v1, :cond_0

    .line 314292
    iget-object v2, p1, LX/48L;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1mx;

    move-object v2, v2

    .line 314293
    invoke-virtual {p0, v2}, LX/1my;->a(LX/1mx;)V

    .line 314294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 314295
    :cond_0
    :goto_1
    return-void

    .line 314296
    :cond_1
    iget-object v0, p1, LX/1mx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 314297
    iget-object v1, p0, LX/1my;->a:LX/01J;

    invoke-virtual {v1, v0}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 314298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "At the moment the framework supports only one transition per key."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314299
    :cond_2
    iget-object v1, p0, LX/1my;->a:LX/01J;

    invoke-virtual {v1, v0, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
