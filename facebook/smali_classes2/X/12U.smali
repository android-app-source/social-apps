.class public LX/12U;
.super LX/12V;
.source ""


# instance fields
.field public final c:LX/12U;

.field public d:Ljava/lang/String;

.field public e:LX/12U;


# direct methods
.method private constructor <init>(ILX/12U;)V
    .locals 1

    .prologue
    .line 175460
    invoke-direct {p0}, LX/12V;-><init>()V

    .line 175461
    const/4 v0, 0x0

    iput-object v0, p0, LX/12U;->e:LX/12U;

    .line 175462
    iput p1, p0, LX/12U;->a:I

    .line 175463
    iput-object p2, p0, LX/12U;->c:LX/12U;

    .line 175464
    const/4 v0, -0x1

    iput v0, p0, LX/12U;->b:I

    .line 175465
    return-void
.end method

.method private a(I)LX/12U;
    .locals 1

    .prologue
    .line 175415
    iput p1, p0, LX/12U;->a:I

    .line 175416
    const/4 v0, -0x1

    iput v0, p0, LX/12U;->b:I

    .line 175417
    const/4 v0, 0x0

    iput-object v0, p0, LX/12U;->d:Ljava/lang/String;

    .line 175418
    return-object p0
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    const/16 v2, 0x22

    .line 175446
    iget v0, p0, LX/12V;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 175447
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175448
    iget-object v0, p0, LX/12U;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 175449
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175450
    iget-object v0, p0, LX/12U;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175451
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175452
    :goto_0
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175453
    :goto_1
    return-void

    .line 175454
    :cond_0
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 175455
    :cond_1
    iget v0, p0, LX/12V;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 175456
    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175457
    invoke-virtual {p0}, LX/12V;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 175458
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 175459
    :cond_2
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static i()LX/12U;
    .locals 3

    .prologue
    .line 175445
    new-instance v0, LX/12U;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/12U;-><init>(ILX/12U;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x4

    .line 175440
    iget v1, p0, LX/12V;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 175441
    iget-object v1, p0, LX/12U;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 175442
    :cond_0
    :goto_0
    return v0

    .line 175443
    :cond_1
    iput-object p1, p0, LX/12U;->d:Ljava/lang/String;

    .line 175444
    iget v0, p0, LX/12V;->b:I

    if-gez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic a()LX/12V;
    .locals 1

    .prologue
    .line 175466
    iget-object v0, p0, LX/12U;->c:LX/12U;

    move-object v0, v0

    .line 175467
    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175439
    iget-object v0, p0, LX/12U;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final j()LX/12U;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 175435
    iget-object v0, p0, LX/12U;->e:LX/12U;

    .line 175436
    if-nez v0, :cond_0

    .line 175437
    new-instance v0, LX/12U;

    invoke-direct {v0, v1, p0}, LX/12U;-><init>(ILX/12U;)V

    iput-object v0, p0, LX/12U;->e:LX/12U;

    .line 175438
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {v0, v1}, LX/12U;->a(I)LX/12U;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()LX/12U;
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 175431
    iget-object v0, p0, LX/12U;->e:LX/12U;

    .line 175432
    if-nez v0, :cond_0

    .line 175433
    new-instance v0, LX/12U;

    invoke-direct {v0, v1, p0}, LX/12U;-><init>(ILX/12U;)V

    iput-object v0, p0, LX/12U;->e:LX/12U;

    .line 175434
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {v0, v1}, LX/12U;->a(I)LX/12U;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()I
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 175419
    iget v3, p0, LX/12V;->a:I

    if-ne v3, v0, :cond_1

    .line 175420
    iget-object v1, p0, LX/12U;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 175421
    const/4 v0, 0x5

    .line 175422
    :goto_0
    return v0

    .line 175423
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/12U;->d:Ljava/lang/String;

    .line 175424
    iget v1, p0, LX/12V;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/12U;->b:I

    goto :goto_0

    .line 175425
    :cond_1
    iget v0, p0, LX/12V;->a:I

    if-ne v0, v2, :cond_3

    .line 175426
    iget v0, p0, LX/12V;->b:I

    .line 175427
    iget v3, p0, LX/12V;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/12U;->b:I

    .line 175428
    if-gez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 175429
    :cond_3
    iget v0, p0, LX/12V;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/12U;->b:I

    .line 175430
    iget v0, p0, LX/12V;->b:I

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175412
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 175413
    invoke-direct {p0, v0}, LX/12U;->a(Ljava/lang/StringBuilder;)V

    .line 175414
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
