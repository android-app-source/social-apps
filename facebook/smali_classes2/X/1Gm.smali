.class public LX/1Gm;
.super LX/0Vx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/1Gm;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Zm;


# direct methods
.method public constructor <init>(LX/0Zm;LX/2WA;)V
    .locals 1
    .param p2    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 226014
    invoke-direct {p0, p2}, LX/0Vx;-><init>(LX/2WA;)V

    .line 226015
    const-string v0, "total_request"

    iput-object v0, p0, LX/1Gm;->b:Ljava/lang/String;

    .line 226016
    const-string v0, "unique_request"

    iput-object v0, p0, LX/1Gm;->c:Ljava/lang/String;

    .line 226017
    const-string v0, "total_succeed"

    iput-object v0, p0, LX/1Gm;->d:Ljava/lang/String;

    .line 226018
    const-string v0, "succeed_on_first_try"

    iput-object v0, p0, LX/1Gm;->e:Ljava/lang/String;

    .line 226019
    const-string v0, "succeed_on_second_try"

    iput-object v0, p0, LX/1Gm;->f:Ljava/lang/String;

    .line 226020
    const-string v0, "succeed_on_third_try"

    iput-object v0, p0, LX/1Gm;->g:Ljava/lang/String;

    .line 226021
    const-string v0, "succeed_on_fourth_onward_try"

    iput-object v0, p0, LX/1Gm;->h:Ljava/lang/String;

    .line 226022
    const-string v0, "ignored_request_due_to_cache_failure"

    iput-object v0, p0, LX/1Gm;->i:Ljava/lang/String;

    .line 226023
    iput-object p1, p0, LX/1Gm;->k:LX/0Zm;

    .line 226024
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1Gm;->j:Ljava/util/Map;

    .line 226025
    return-void
.end method

.method public static a(LX/0QB;)LX/1Gm;
    .locals 5

    .prologue
    .line 226026
    sget-object v0, LX/1Gm;->l:LX/1Gm;

    if-nez v0, :cond_1

    .line 226027
    const-class v1, LX/1Gm;

    monitor-enter v1

    .line 226028
    :try_start_0
    sget-object v0, LX/1Gm;->l:LX/1Gm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 226029
    if-eqz v2, :cond_0

    .line 226030
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 226031
    new-instance p0, LX/1Gm;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v3

    check-cast v3, LX/0Zm;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v4

    check-cast v4, LX/2WA;

    invoke-direct {p0, v3, v4}, LX/1Gm;-><init>(LX/0Zm;LX/2WA;)V

    .line 226032
    move-object v0, p0

    .line 226033
    sput-object v0, LX/1Gm;->l:LX/1Gm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226034
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 226035
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226036
    :cond_1
    sget-object v0, LX/1Gm;->l:LX/1Gm;

    return-object v0

    .line 226037
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 226038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static f(LX/1Gm;)Z
    .locals 2

    .prologue
    .line 226039
    iget-object v0, p0, LX/1Gm;->k:LX/0Zm;

    invoke-virtual {v0}, LX/0Zm;->a()LX/17O;

    move-result-object v0

    sget-object v1, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226040
    const-string v0, "web_request_counters"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 226041
    invoke-static {p0}, LX/1Gm;->f(LX/1Gm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226042
    :goto_0
    return-void

    .line 226043
    :cond_0
    const-string v0, "total_request"

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 226044
    iget-object v0, p0, LX/1Gm;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 226045
    if-eqz v0, :cond_1

    .line 226046
    iget-object v1, p0, LX/1Gm;->j:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 226047
    :cond_1
    iget-object v0, p0, LX/1Gm;->j:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226048
    const-string v0, "unique_request"

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto :goto_0
.end method
