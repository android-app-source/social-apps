.class public final LX/1lP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/1lP;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:D

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 311861
    new-instance v1, LX/1lP;

    const-wide/16 v4, 0x0

    move-object v3, v2

    move-object v6, v2

    invoke-direct/range {v1 .. v6}, LX/1lP;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)V

    sput-object v1, LX/1lP;->a:LX/1lP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)V
    .locals 1

    .prologue
    .line 311855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311856
    iput-object p1, p0, LX/1lP;->b:Ljava/lang/String;

    .line 311857
    iput-object p2, p0, LX/1lP;->c:Ljava/lang/String;

    .line 311858
    iput-wide p3, p0, LX/1lP;->d:D

    .line 311859
    iput-object p5, p0, LX/1lP;->e:Ljava/lang/String;

    .line 311860
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)LX/1lP;
    .locals 8

    .prologue
    .line 311854
    new-instance v1, LX/1lP;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/1lP;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 311835
    if-ne p0, p1, :cond_1

    .line 311836
    :cond_0
    :goto_0
    return v1

    .line 311837
    :cond_1
    instance-of v0, p1, LX/1lP;

    if-nez v0, :cond_2

    move v1, v2

    .line 311838
    goto :goto_0

    .line 311839
    :cond_2
    check-cast p1, LX/1lP;

    .line 311840
    iget-object v0, p0, LX/1lP;->b:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p1, LX/1lP;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 311841
    :goto_1
    iget-object v3, p0, LX/1lP;->c:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, p1, LX/1lP;->c:Ljava/lang/String;

    if-nez v3, :cond_6

    move v3, v1

    .line 311842
    :goto_2
    iget-object v4, p0, LX/1lP;->e:Ljava/lang/String;

    if-nez v4, :cond_9

    iget-object v4, p1, LX/1lP;->e:Ljava/lang/String;

    if-nez v4, :cond_8

    move v4, v1

    .line 311843
    :goto_3
    iget-wide v6, p1, LX/1lP;->d:D

    iget-wide v8, p0, LX/1lP;->d:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Double;->compare(DD)I

    move-result v5

    if-nez v5, :cond_3

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    if-nez v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v0, v2

    .line 311844
    goto :goto_1

    :cond_5
    iget-object v0, p0, LX/1lP;->b:Ljava/lang/String;

    iget-object v3, p1, LX/1lP;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_6
    move v3, v2

    .line 311845
    goto :goto_2

    :cond_7
    iget-object v3, p0, LX/1lP;->c:Ljava/lang/String;

    iget-object v4, p1, LX/1lP;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_2

    :cond_8
    move v4, v2

    .line 311846
    goto :goto_3

    :cond_9
    iget-object v4, p0, LX/1lP;->e:Ljava/lang/String;

    iget-object v5, p1, LX/1lP;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_3
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 311847
    iget-object v0, p0, LX/1lP;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    .line 311848
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/1lP;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 311849
    iget-wide v2, p0, LX/1lP;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 311850
    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 311851
    return v0

    .line 311852
    :cond_0
    iget-object v0, p0, LX/1lP;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 311853
    :cond_1
    iget-object v1, p0, LX/1lP;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
