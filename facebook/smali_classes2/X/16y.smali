.class public LX/16y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 187830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187831
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    .line 187832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 187833
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187834
    :cond_0
    :goto_0
    return-object p0

    .line 187835
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 187836
    if-eqz v0, :cond_2

    .line 187837
    invoke-static {v0, p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 187838
    if-eqz v0, :cond_2

    move-object p0, v0

    .line 187839
    goto :goto_0

    .line 187840
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 187841
    if-eqz v0, :cond_3

    .line 187842
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187843
    invoke-static {v0, p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    .line 187844
    if-nez p0, :cond_0

    .line 187845
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 187846
    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 187847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 187848
    if-nez v0, :cond_0

    .line 187849
    sget-object v0, LX/16z;->g:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 187850
    :cond_0
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .locals 1

    .prologue
    .line 187852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 187853
    if-nez v0, :cond_0

    sget-object v0, LX/16z;->g:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    :cond_0
    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 187854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 187855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
