.class public LX/0mP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0mP;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 132469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132470
    iput-object p1, p0, LX/0mP;->a:LX/0Ot;

    .line 132471
    return-void
.end method

.method public static a(LX/0QB;)LX/0mP;
    .locals 4

    .prologue
    .line 132472
    sget-object v0, LX/0mP;->b:LX/0mP;

    if-nez v0, :cond_1

    .line 132473
    const-class v1, LX/0mP;

    monitor-enter v1

    .line 132474
    :try_start_0
    sget-object v0, LX/0mP;->b:LX/0mP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132475
    if-eqz v2, :cond_0

    .line 132476
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132477
    new-instance v3, LX/0mP;

    const/16 p0, 0xc1c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0mP;-><init>(LX/0Ot;)V

    .line 132478
    move-object v0, v3

    .line 132479
    sput-object v0, LX/0mP;->b:LX/0mP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132480
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132481
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132482
    :cond_1
    sget-object v0, LX/0mP;->b:LX/0mP;

    return-object v0

    .line 132483
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
