.class public LX/1am;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 278321
    ushr-int/lit8 v0, p0, 0x18

    .line 278322
    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    .line 278323
    const/4 v0, -0x1

    .line 278324
    :goto_0
    return v0

    .line 278325
    :cond_0
    if-nez v0, :cond_1

    .line 278326
    const/4 v0, -0x2

    goto :goto_0

    .line 278327
    :cond_1
    const/4 v0, -0x3

    goto :goto_0
.end method

.method public static a(II)I
    .locals 3

    .prologue
    const v2, 0xffffff

    .line 278328
    const/16 v0, 0xff

    if-ne p1, v0, :cond_0

    .line 278329
    :goto_0
    return p0

    .line 278330
    :cond_0
    if-nez p1, :cond_1

    .line 278331
    and-int/2addr p0, v2

    goto :goto_0

    .line 278332
    :cond_1
    shr-int/lit8 v0, p1, 0x7

    add-int/2addr v0, p1

    .line 278333
    ushr-int/lit8 v1, p0, 0x18

    .line 278334
    mul-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 278335
    shl-int/lit8 v0, v0, 0x18

    and-int v1, p0, v2

    or-int p0, v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 278336
    instance-of v0, p0, LX/1en;

    if-eqz v0, :cond_0

    .line 278337
    check-cast p0, LX/1en;

    invoke-interface {p0}, LX/1en;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278338
    :goto_0
    return-object v0

    .line 278339
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    .line 278340
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;LX/1al;)V
    .locals 4

    .prologue
    .line 278341
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 278342
    :cond_0
    :goto_0
    return-void

    .line 278343
    :cond_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 278344
    if-nez p0, :cond_3

    .line 278345
    :cond_2
    :goto_1
    goto :goto_0

    .line 278346
    :cond_3
    iget v0, p1, LX/1al;->a:I

    if-eq v0, v3, :cond_4

    .line 278347
    iget v0, p1, LX/1al;->a:I

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 278348
    :cond_4
    iget-boolean v0, p1, LX/1al;->b:Z

    if-eqz v0, :cond_5

    .line 278349
    iget-object v0, p1, LX/1al;->c:Landroid/graphics/ColorFilter;

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 278350
    :cond_5
    iget v0, p1, LX/1al;->d:I

    if-eq v0, v3, :cond_6

    .line 278351
    iget v0, p1, LX/1al;->d:I

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 278352
    :cond_6
    iget v0, p1, LX/1al;->e:I

    if-eq v0, v3, :cond_2

    .line 278353
    iget v0, p1, LX/1al;->e:I

    if-eqz v0, :cond_8

    :goto_3
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    goto :goto_1

    :cond_7
    move v0, v2

    .line 278354
    goto :goto_2

    :cond_8
    move v1, v2

    .line 278355
    goto :goto_3
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable$Callback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1ak;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278356
    if-eqz p0, :cond_0

    .line 278357
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 278358
    instance-of v0, p0, LX/1aj;

    if-eqz v0, :cond_0

    .line 278359
    check-cast p0, LX/1aj;

    invoke-interface {p0, p2}, LX/1aj;->a(LX/1ak;)V

    .line 278360
    :cond_0
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 278361
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    if-ne p0, p1, :cond_1

    .line 278362
    :cond_0
    :goto_0
    return-void

    .line 278363
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278364
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 278365
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getLevel()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 278366
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 278367
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method
