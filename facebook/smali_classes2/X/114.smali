.class public LX/114;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0ug;",
        "LX/0Or",
        "<",
        "LX/115;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile e:LX/114;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0W3;

.field private d:LX/115;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169787
    const-class v0, LX/114;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/114;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169784
    iput-object p1, p0, LX/114;->b:Landroid/content/Context;

    .line 169785
    iput-object p2, p0, LX/114;->c:LX/0W3;

    .line 169786
    return-void
.end method

.method public static a(LX/0QB;)LX/114;
    .locals 5

    .prologue
    .line 169770
    sget-object v0, LX/114;->e:LX/114;

    if-nez v0, :cond_1

    .line 169771
    const-class v1, LX/114;

    monitor-enter v1

    .line 169772
    :try_start_0
    sget-object v0, LX/114;->e:LX/114;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169773
    if-eqz v2, :cond_0

    .line 169774
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169775
    new-instance p0, LX/114;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/114;-><init>(Landroid/content/Context;LX/0W3;)V

    .line 169776
    move-object v0, p0

    .line 169777
    sput-object v0, LX/114;->e:LX/114;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169778
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169779
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169780
    :cond_1
    sget-object v0, LX/114;->e:LX/114;

    return-object v0

    .line 169781
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 169752
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/114;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, LX/115;->a(Landroid/content/ContentResolver;Ljava/lang/String;)LX/115;

    move-result-object v0

    iput-object v0, p0, LX/114;->d:LX/115;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169753
    :goto_0
    monitor-exit p0

    return-void

    .line 169754
    :catch_0
    move-exception v0

    .line 169755
    :goto_1
    :try_start_1
    sget-object v1, LX/114;->a:Ljava/lang/String;

    const-string v2, "Error parsing intent switch-off criteria!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169756
    new-instance v0, LX/115;

    invoke-direct {v0}, LX/115;-><init>()V

    iput-object v0, p0, LX/114;->d:LX/115;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 169757
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 169758
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 169769
    const/16 v0, 0xeb

    return v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 169765
    iget-object v0, p0, LX/114;->c:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    .line 169766
    sget-wide v2, LX/0X5;->ho:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 169767
    invoke-direct {p0, v0}, LX/114;->a(Ljava/lang/String;)V

    .line 169768
    return-void
.end method

.method public final declared-synchronized b()LX/115;
    .locals 4

    .prologue
    .line 169760
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/114;->d:LX/115;

    if-nez v0, :cond_0

    .line 169761
    iget-object v0, p0, LX/114;->c:LX/0W3;

    sget-wide v2, LX/0X5;->ho:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 169762
    invoke-direct {p0, v0}, LX/114;->a(Ljava/lang/String;)V

    .line 169763
    :cond_0
    iget-object v0, p0, LX/114;->d:LX/115;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 169764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 169759
    invoke-virtual {p0}, LX/114;->b()LX/115;

    move-result-object v0

    return-object v0
.end method
