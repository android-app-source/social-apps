.class public LX/1E1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kI;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 219058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219059
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 219060
    iput-object v0, p0, LX/1E1;->b:LX/0Ot;

    .line 219061
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 219062
    iput-object v0, p0, LX/1E1;->d:LX/0Ot;

    .line 219063
    return-void
.end method

.method public static a(LX/0QB;)LX/1E1;
    .locals 1

    .prologue
    .line 219064
    invoke-static {p0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1E1;
    .locals 5

    .prologue
    .line 219053
    new-instance v2, LX/1E1;

    invoke-direct {v2}, LX/1E1;-><init>()V

    .line 219054
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const/16 v1, 0x6a7

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v4, 0xa86

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 219055
    iput-object v0, v2, LX/1E1;->a:LX/0ad;

    iput-object v3, v2, LX/1E1;->b:LX/0Ot;

    iput-object v1, v2, LX/1E1;->c:LX/0Uh;

    iput-object v4, v2, LX/1E1;->d:LX/0Ot;

    .line 219056
    return-object v2
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 219057
    invoke-virtual {p0}, LX/1E1;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 219052
    iget-object v0, p0, LX/1E1;->a:LX/0ad;

    sget-short v1, LX/1Nu;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 219051
    iget-object v0, p0, LX/1E1;->c:LX/0Uh;

    sget v1, LX/1av;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 219050
    iget-object v0, p0, LX/1E1;->c:LX/0Uh;

    sget v2, LX/1S0;->b:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1E1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 219049
    iget-object v0, p0, LX/1E1;->a:LX/0ad;

    sget-short v1, LX/1Nu;->h:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
