.class public LX/18T;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/megaphone/api/FetchMegaphoneParams;",
        "Lcom/facebook/megaphone/api/FetchMegaphoneResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206539
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 206540
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 206541
    invoke-virtual {p3}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lC;

    .line 206542
    const-class v1, Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0, p3, v1}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 206543
    new-instance v1, Lcom/facebook/megaphone/api/FetchMegaphoneResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/facebook/megaphone/api/FetchMegaphoneResult;-><init>(Lcom/facebook/graphql/model/GraphQLMegaphone;LX/0ta;J)V

    .line 206544
    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 206545
    const/4 v0, 0x2

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 206546
    check-cast p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;

    .line 206547
    new-instance v0, LX/3TA;

    invoke-direct {v0}, LX/3TA;-><init>()V

    move-object v0, v0

    .line 206548
    const-string v1, "location"

    iget-object v2, p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "img_size"

    iget v3, p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "facepile_img_size"

    iget v3, p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 206549
    const/4 v1, 0x1

    .line 206550
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 206551
    return-object v0
.end method
