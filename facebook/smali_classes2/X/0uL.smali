.class public LX/0uL;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0uM;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0uM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156258
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0uM;
    .locals 6

    .prologue
    .line 156245
    sget-object v0, LX/0uL;->a:LX/0uM;

    if-nez v0, :cond_1

    .line 156246
    const-class v1, LX/0uL;

    monitor-enter v1

    .line 156247
    :try_start_0
    sget-object v0, LX/0uL;->a:LX/0uM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 156248
    if-eqz v2, :cond_0

    .line 156249
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 156250
    invoke-static {v0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v3

    check-cast v3, LX/0p7;

    invoke-static {v0}, LX/0p8;->a(LX/0QB;)LX/0p8;

    move-result-object v4

    check-cast v4, LX/0p8;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object p0

    check-cast p0, LX/0oz;

    invoke-static {v3, v4, v5, p0}, LX/0uJ;->a(LX/0p7;LX/0p8;LX/0kb;LX/0oz;)LX/0uM;

    move-result-object v3

    move-object v0, v3

    .line 156251
    sput-object v0, LX/0uL;->a:LX/0uM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156252
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 156253
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 156254
    :cond_1
    sget-object v0, LX/0uL;->a:LX/0uM;

    return-object v0

    .line 156255
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 156256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 156257
    invoke-static {p0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v0

    check-cast v0, LX/0p7;

    invoke-static {p0}, LX/0p8;->a(LX/0QB;)LX/0p8;

    move-result-object v1

    check-cast v1, LX/0p8;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-static {v0, v1, v2, v3}, LX/0uJ;->a(LX/0p7;LX/0p8;LX/0kb;LX/0oz;)LX/0uM;

    move-result-object v0

    return-object v0
.end method
