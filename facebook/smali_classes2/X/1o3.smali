.class public final LX/1o3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1o2;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/widget/ImageView$ScaleType;

.field public c:LX/1oA;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 318287
    invoke-static {}, LX/1o2;->q()LX/1o2;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 318288
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318289
    const-string v0, "Image"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1o2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318290
    check-cast p1, LX/1o3;

    .line 318291
    iget-object v0, p1, LX/1o3;->c:LX/1oA;

    iput-object v0, p0, LX/1o3;->c:LX/1oA;

    .line 318292
    iget-object v0, p1, LX/1o3;->d:Ljava/lang/Integer;

    iput-object v0, p0, LX/1o3;->d:Ljava/lang/Integer;

    .line 318293
    iget-object v0, p1, LX/1o3;->e:Ljava/lang/Integer;

    iput-object v0, p0, LX/1o3;->e:Ljava/lang/Integer;

    .line 318294
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318295
    if-ne p0, p1, :cond_1

    .line 318296
    :cond_0
    :goto_0
    return v0

    .line 318297
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 318298
    goto :goto_0

    .line 318299
    :cond_3
    check-cast p1, LX/1o3;

    .line 318300
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 318301
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 318302
    if-eq v2, v3, :cond_0

    .line 318303
    iget-object v2, p0, LX/1o3;->a:LX/1dc;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1o3;->a:LX/1dc;

    iget-object v3, p1, LX/1o3;->a:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 318304
    goto :goto_0

    .line 318305
    :cond_5
    iget-object v2, p1, LX/1o3;->a:LX/1dc;

    if-nez v2, :cond_4

    .line 318306
    :cond_6
    iget-object v2, p0, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    iget-object v3, p1, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView$ScaleType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 318307
    goto :goto_0

    .line 318308
    :cond_7
    iget-object v2, p1, LX/1o3;->b:Landroid/widget/ImageView$ScaleType;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 318309
    const/4 v1, 0x0

    .line 318310
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1o3;

    .line 318311
    iput-object v1, v0, LX/1o3;->c:LX/1oA;

    .line 318312
    iput-object v1, v0, LX/1o3;->d:Ljava/lang/Integer;

    .line 318313
    iput-object v1, v0, LX/1o3;->e:Ljava/lang/Integer;

    .line 318314
    return-object v0
.end method
