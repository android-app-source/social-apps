.class public LX/1Dp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/1De;

.field public b:Landroid/content/res/Resources;

.field private c:Landroid/content/res/Resources$Theme;

.field public d:LX/1Ds;

.field private final e:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 218939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218940
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/1Dp;->e:[I

    return-void
.end method


# virtual methods
.method public final a(F)I
    .locals 1

    .prologue
    .line 218941
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 218942
    mul-float/2addr v0, p1

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 218943
    int-to-float v0, p1

    invoke-virtual {p0, v0}, LX/1Dp;->a(F)I

    move-result v0

    return v0
.end method

.method public final a(II)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 218944
    iget-object v0, p0, LX/1Dp;->e:[I

    aput p1, v0, v1

    .line 218945
    iget-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, LX/1Dp;->e:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 218946
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p2}, LX/1Dp;->d(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 218947
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 218948
    invoke-virtual {p0}, LX/1Dp;->e()V

    .line 218949
    return-void
.end method

.method public final a(LX/1De;LX/1Ds;)V
    .locals 1

    .prologue
    .line 218904
    iput-object p1, p0, LX/1Dp;->a:LX/1De;

    .line 218905
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    .line 218906
    invoke-virtual {p1}, LX/1De;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iput-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    .line 218907
    iput-object p2, p0, LX/1Dp;->d:LX/1Ds;

    .line 218908
    return-void
.end method

.method public final b(F)I
    .locals 1

    .prologue
    .line 218950
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 218951
    mul-float/2addr v0, p1

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0
.end method

.method public final b(II)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 218928
    iget-object v0, p0, LX/1Dp;->e:[I

    aput p1, v0, v1

    .line 218929
    iget-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, LX/1Dp;->e:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 218930
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p2}, LX/1Dp;->e(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 218931
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 218932
    if-eqz p1, :cond_1

    .line 218933
    iget-object v0, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v0, p1}, LX/1Ds;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218934
    if-eqz v0, :cond_0

    .line 218935
    :goto_0
    return-object v0

    .line 218936
    :cond_0
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218937
    iget-object v1, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v1, p1, v0}, LX/1Ds;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 218938
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 218924
    iget-object v0, p0, LX/1Dp;->e:[I

    aput p1, v0, v1

    .line 218925
    iget-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, LX/1Dp;->e:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 218926
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p2}, LX/1Dp;->f(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 218927
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public final d(I)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 218916
    if-eqz p1, :cond_1

    .line 218917
    iget-object v0, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v0, p1}, LX/1Ds;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 218918
    if-eqz v0, :cond_0

    .line 218919
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 218920
    :goto_0
    return v0

    .line 218921
    :cond_0
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 218922
    iget-object v1, p0, LX/1Dp;->d:LX/1Ds;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/1Ds;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 218923
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)LX/1dc;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 218909
    if-nez p1, :cond_0

    .line 218910
    const/4 v0, 0x0

    .line 218911
    :goto_0
    return-object v0

    .line 218912
    :cond_0
    iget-object v0, p0, LX/1Dp;->e:[I

    aput p1, v0, v1

    .line 218913
    iget-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, LX/1Dp;->e:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 218914
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dp;->g(I)LX/1dc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 218915
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public final e(I)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218896
    if-eqz p1, :cond_1

    .line 218897
    iget-object v0, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v0, p1}, LX/1Ds;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 218898
    if-eqz v0, :cond_0

    .line 218899
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 218900
    :goto_0
    return v0

    .line 218901
    :cond_0
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 218902
    iget-object v1, p0, LX/1Dp;->d:LX/1Ds;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/1Ds;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 218903
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(II)I
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 218892
    iget-object v0, p0, LX/1Dp;->e:[I

    aput p1, v0, v1

    .line 218893
    iget-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, LX/1Dp;->e:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 218894
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 218895
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 218887
    iput-object v0, p0, LX/1Dp;->a:LX/1De;

    .line 218888
    iput-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    .line 218889
    iput-object v0, p0, LX/1Dp;->c:Landroid/content/res/Resources$Theme;

    .line 218890
    iput-object v0, p0, LX/1Dp;->d:LX/1Ds;

    .line 218891
    return-void
.end method

.method public final f(I)I
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218879
    if-eqz p1, :cond_1

    .line 218880
    iget-object v0, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v0, p1}, LX/1Ds;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 218881
    if-eqz v0, :cond_0

    .line 218882
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 218883
    :goto_0
    return v0

    .line 218884
    :cond_0
    iget-object v0, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 218885
    iget-object v1, p0, LX/1Dp;->d:LX/1Ds;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/1Ds;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 218886
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(I)LX/1dc;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218876
    if-nez p1, :cond_0

    .line 218877
    const/4 v0, 0x0

    .line 218878
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Dp;->a:LX/1De;

    invoke-static {v0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    goto :goto_0
.end method
