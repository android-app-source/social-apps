.class public final LX/0Sz;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:LX/0Sy;


# direct methods
.method public constructor <init>(LX/0Sy;)V
    .locals 0

    .prologue
    .line 62500
    iput-object p1, p0, LX/0Sz;->a:LX/0Sy;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final d(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 62501
    iget-object v0, p0, LX/0Sz;->a:LX/0Sy;

    iget-object v1, v0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62502
    :try_start_0
    iget-object v0, p0, LX/0Sz;->a:LX/0Sy;

    iget-object v0, v0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 62503
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62504
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 62505
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 62506
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "View "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " was still marked as interacting when its corresponding Activity was paused"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62507
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 62508
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62509
    iget-object v0, p0, LX/0Sz;->a:LX/0Sy;

    invoke-static {v0}, LX/0Sy;->f(LX/0Sy;)V

    .line 62510
    return-void
.end method
