.class public final LX/1Us;
.super LX/1Ur;
.source ""


# static fields
.field public static final i:LX/1Up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256848
    new-instance v0, LX/1Us;

    invoke-direct {v0}, LX/1Us;-><init>()V

    sput-object v0, LX/1Us;->i:LX/1Up;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 256849
    invoke-direct {p0}, LX/1Ur;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFFF)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 256850
    invoke-static {p7, p8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 256851
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    .line 256852
    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    .line 256853
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 256854
    add-float v0, v1, v3

    float-to-int v0, v0

    int-to-float v0, v0

    add-float v1, v2, v3

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 256855
    return-void
.end method
