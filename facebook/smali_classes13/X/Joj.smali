.class public LX/Joj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Joj;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/LinkedList",
            "<",
            "LX/DiN;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/Jot;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0W3;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jgk;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/Joi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Jot;LX/0Ot;LX/0Ot;LX/0W3;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jot;",
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "LX/Jgk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2736041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2736042
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    .line 2736043
    iput-object p1, p0, LX/Joj;->b:LX/Jot;

    .line 2736044
    iput-object p2, p0, LX/Joj;->c:LX/0Ot;

    .line 2736045
    iput-object p3, p0, LX/Joj;->d:LX/0Ot;

    .line 2736046
    iput-object p4, p0, LX/Joj;->e:LX/0W3;

    .line 2736047
    iput-object p5, p0, LX/Joj;->f:LX/0Or;

    .line 2736048
    return-void
.end method

.method public static a(LX/0QB;)LX/Joj;
    .locals 9

    .prologue
    .line 2736028
    sget-object v0, LX/Joj;->h:LX/Joj;

    if-nez v0, :cond_1

    .line 2736029
    const-class v1, LX/Joj;

    monitor-enter v1

    .line 2736030
    :try_start_0
    sget-object v0, LX/Joj;->h:LX/Joj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2736031
    if-eqz v2, :cond_0

    .line 2736032
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2736033
    new-instance v3, LX/Joj;

    invoke-static {v0}, LX/Jot;->a(LX/0QB;)LX/Jot;

    move-result-object v4

    check-cast v4, LX/Jot;

    const/16 v5, 0x140d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1430

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    const/16 v8, 0x2700

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/Joj;-><init>(LX/Jot;LX/0Ot;LX/0Ot;LX/0W3;LX/0Or;)V

    .line 2736034
    move-object v0, v3

    .line 2736035
    sput-object v0, LX/Joj;->h:LX/Joj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2736036
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2736037
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2736038
    :cond_1
    sget-object v0, LX/Joj;->h:LX/Joj;

    return-object v0

    .line 2736039
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2736040
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Joj;LX/DiN;Ljava/util/List;)Z
    .locals 13
    .param p1    # LX/DiN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DiN;",
            "Ljava/util/List",
            "<",
            "LX/DiN;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2735995
    iget-object v0, p1, LX/DiN;->g:LX/DiO;

    move-object v0, v0

    .line 2735996
    sget-object v5, LX/Jof;->a:[I

    invoke-virtual {v0}, LX/DiO;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2735997
    const/4 v5, 0x0

    :goto_0
    move v0, v5

    .line 2735998
    if-nez v0, :cond_1

    .line 2735999
    :cond_0
    :goto_1
    return v2

    .line 2736000
    :cond_1
    iget v5, p1, LX/DiN;->d:F

    move v6, v5

    .line 2736001
    iget-object v5, p0, LX/Joj;->f:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Jgk;

    .line 2736002
    invoke-virtual {v5}, LX/Jgk;->d()LX/Jgj;

    move-result-object v7

    .line 2736003
    iget-object v8, v5, LX/Jgk;->d:LX/0W3;

    iget-wide v9, v7, LX/Jgj;->confidence:J

    iget v7, v7, LX/Jgj;->defaultConfidence:F

    float-to-double v11, v7

    invoke-interface {v8, v9, v10, v11, v12}, LX/0W4;->a(JD)D

    move-result-wide v7

    double-to-float v7, v7

    move v5, v7

    .line 2736004
    cmpl-float v5, v6, v5

    if-ltz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 2736005
    if-eqz v0, :cond_0

    .line 2736006
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2736007
    invoke-static {p1, p2}, LX/Jom;->a(LX/DiN;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2736008
    iget-object v0, p1, LX/DiN;->g:LX/DiO;

    move-object v0, v0

    .line 2736009
    sget-object v1, LX/DiO;->STICKER:LX/DiO;

    if-ne v0, v1, :cond_3

    move v1, v2

    move v3, v2

    .line 2736010
    :goto_3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2736011
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DiN;

    .line 2736012
    iget-object v4, v0, LX/DiN;->g:LX/DiO;

    move-object v0, v4

    .line 2736013
    sget-object v4, LX/DiO;->STICKER:LX/DiO;

    if-ne v0, v4, :cond_2

    .line 2736014
    add-int/lit8 v3, v3, 0x1

    .line 2736015
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2736016
    :cond_2
    const/4 v0, 0x2

    if-ge v3, v0, :cond_0

    .line 2736017
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    .line 2736018
    :pswitch_0
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->ka:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto :goto_0

    .line 2736019
    :pswitch_1
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kb:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto :goto_0

    .line 2736020
    :pswitch_2
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->km:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto :goto_0

    .line 2736021
    :pswitch_3
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kc:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto :goto_0

    .line 2736022
    :pswitch_4
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kd:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    .line 2736023
    :pswitch_5
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->ke:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    .line 2736024
    :pswitch_6
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kf:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    .line 2736025
    :pswitch_7
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kg:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    .line 2736026
    :pswitch_8
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->ki:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    .line 2736027
    :pswitch_9
    iget-object v5, p0, LX/Joj;->e:LX/0W3;

    sget-wide v7, LX/0X5;->kh:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static declared-synchronized a$redex0(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    .line 2735954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2735955
    monitor-exit p0

    return-void

    .line 2735956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/LinkedList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/LinkedList",
            "<",
            "LX/DiN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2735986
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 2735987
    :goto_0
    invoke-virtual {p2}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_0

    .line 2735988
    invoke-virtual {p2}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2735989
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2735990
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735991
    if-eqz v0, :cond_1

    .line 2735992
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DiN;

    .line 2735993
    invoke-static {p0, v0}, LX/Joj;->c(LX/Joj;LX/DiN;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2735994
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static b$redex0(LX/Joj;LX/DiN;)V
    .locals 3

    .prologue
    .line 2735984
    iget-object v0, p0, LX/Joj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$4;-><init>(LX/Joj;LX/DiN;)V

    const v2, -0x16ce0e98

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2735985
    return-void
.end method

.method public static declared-synchronized c(LX/Joj;LX/DiN;)V
    .locals 3

    .prologue
    .line 2735975
    monitor-enter p0

    .line 2735976
    :try_start_0
    iget-object v0, p1, LX/DiN;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 2735977
    iget-object v1, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2735978
    if-nez v0, :cond_0

    .line 2735979
    :goto_0
    monitor-exit p0

    return-void

    .line 2735980
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_1

    .line 2735981
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2735982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2735983
    :cond_1
    :try_start_2
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/DiN;)V
    .locals 4

    .prologue
    .line 2735961
    monitor-enter p0

    .line 2735962
    :try_start_0
    iget-object v0, p1, LX/DiN;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v0

    .line 2735963
    iget-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {p0, p1, v0}, LX/Joj;->a(LX/Joj;LX/DiN;Ljava/util/List;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2735964
    :goto_0
    monitor-exit p0

    return-void

    .line 2735965
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2735966
    if-eqz v0, :cond_1

    .line 2735967
    invoke-static {p0, p1}, LX/Joj;->c(LX/Joj;LX/DiN;)V

    .line 2735968
    invoke-static {p0, p1}, LX/Joj;->b$redex0(LX/Joj;LX/DiN;)V

    .line 2735969
    :goto_2
    iget-object v0, p0, LX/Joj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$7;-><init>(LX/Joj;LX/DiN;)V

    const v2, 0x53630e40

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2735970
    goto :goto_0

    .line 2735971
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2735972
    :cond_1
    :try_start_2
    invoke-static {p0, v1}, LX/Joj;->a$redex0(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2735973
    iget-object v0, p0, LX/Joj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v2, LX/Joh;

    invoke-direct {v2, p0, v1}, LX/Joh;-><init>(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v2, v0

    .line 2735974
    new-instance v3, LX/Jog;

    invoke-direct {v3, p0, v1, p1}, LX/Jog;-><init>(LX/Joj;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/DiN;)V

    iget-object v0, p0, LX/Joj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized clearUserData()V
    .locals 3

    .prologue
    .line 2735957
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Joj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2735958
    iget-object v0, p0, LX/Joj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/omnim/directives/OmniMActionStore$1;-><init>(LX/Joj;)V

    const v2, 0x53d0310

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2735959
    monitor-exit p0

    return-void

    .line 2735960
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
