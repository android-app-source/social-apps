.class public LX/Jqf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/0SG;

.field private final c:LX/3QU;

.field public final d:LX/Jqb;

.field public final e:LX/Iuh;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/2OQ;

.field public final h:LX/0Uh;

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739983
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqf;->j:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FDs;LX/0SG;LX/3QU;LX/Jqb;LX/Iuh;LX/0Or;LX/2OQ;LX/0Uh;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p7    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/0SG;",
            "LX/3QU;",
            "LX/Jqb;",
            "LX/Iuh;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2OQ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2740045
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740046
    iput-object v0, p0, LX/Jqf;->i:LX/0Ot;

    .line 2740047
    iput-object p1, p0, LX/Jqf;->a:LX/FDs;

    .line 2740048
    iput-object p2, p0, LX/Jqf;->b:LX/0SG;

    .line 2740049
    iput-object p3, p0, LX/Jqf;->c:LX/3QU;

    .line 2740050
    iput-object p4, p0, LX/Jqf;->d:LX/Jqb;

    .line 2740051
    iput-object p5, p0, LX/Jqf;->e:LX/Iuh;

    .line 2740052
    iput-object p6, p0, LX/Jqf;->f:LX/0Or;

    .line 2740053
    iput-object p7, p0, LX/Jqf;->g:LX/2OQ;

    .line 2740054
    iput-object p8, p0, LX/Jqf;->h:LX/0Uh;

    .line 2740055
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqf;
    .locals 7

    .prologue
    .line 2740017
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740018
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740019
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740020
    if-nez v1, :cond_0

    .line 2740021
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740022
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740023
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740024
    sget-object v1, LX/Jqf;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740025
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740026
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740027
    :cond_1
    if-nez v1, :cond_4

    .line 2740028
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740029
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740030
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqf;->b(LX/0QB;)LX/Jqf;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2740031
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740032
    if-nez v1, :cond_2

    .line 2740033
    sget-object v0, LX/Jqf;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740034
    :goto_1
    if-eqz v0, :cond_3

    .line 2740035
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740036
    :goto_3
    check-cast v0, LX/Jqf;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740037
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740038
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740039
    :catchall_1
    move-exception v0

    .line 2740040
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740041
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740042
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740043
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqf;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqf;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Jqf;
    .locals 9

    .prologue
    .line 2740013
    new-instance v0, LX/Jqf;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/3QU;->b(LX/0QB;)LX/3QU;

    move-result-object v3

    check-cast v3, LX/3QU;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v4

    check-cast v4, LX/Jqb;

    invoke-static {p0}, LX/Iuh;->a(LX/0QB;)LX/Iuh;

    move-result-object v5

    check-cast v5, LX/Iuh;

    const/16 v6, 0x15e8

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/2Ok;->a(LX/0QB;)LX/2OQ;

    move-result-object v7

    check-cast v7, LX/2OQ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v0 .. v8}, LX/Jqf;-><init>(LX/FDs;LX/0SG;LX/3QU;LX/Jqb;LX/Iuh;LX/0Or;LX/2OQ;LX/0Uh;)V

    .line 2740014
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2740015
    iput-object v1, v0, LX/Jqf;->i:LX/0Ot;

    .line 2740016
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;JZ)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2740011
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/Jqf;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740012
    iget-object v0, p0, LX/Jqf;->a:LX/FDs;

    sget-object v5, LX/6jT;->a:LX/6jT;

    move-wide v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JZLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)V
    .locals 3

    .prologue
    .line 2740001
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2740002
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v0

    .line 2740003
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Jqf;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740004
    :cond_0
    :goto_0
    return-void

    .line 2740005
    :cond_1
    iget-object v0, p0, LX/Jqf;->h:LX/0Uh;

    const/16 v1, 0x1fa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2740006
    iget-object v0, p0, LX/Jqf;->e:LX/Iuh;

    invoke-virtual {v0, p1}, LX/Iuh;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 2740007
    if-eqz v0, :cond_0

    .line 2740008
    iget-object v1, p0, LX/Jqf;->d:LX/Jqb;

    .line 2740009
    iget-object v2, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v2

    .line 2740010
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2, v0}, LX/Jqb;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;LX/6k4;J)V
    .locals 3
    .param p1    # Lcom/facebook/messaging/service/model/NewMessageResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/6k4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2739995
    if-nez p1, :cond_0

    .line 2739996
    :goto_0
    return-void

    .line 2739997
    :cond_0
    if-eqz p2, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p2, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v1, v1, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v1, v0

    .line 2739998
    :goto_1
    iget-object v0, p0, LX/Jqf;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, p1, p3, p4}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2739999
    invoke-virtual {p0, p1, v1}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)V

    goto :goto_0

    .line 2740000
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)V
    .locals 6
    .param p1    # Lcom/facebook/messaging/service/model/NewMessageResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2739984
    if-nez p1, :cond_1

    .line 2739985
    :cond_0
    :goto_0
    return-void

    .line 2739986
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0

    .line 2739987
    if-eqz p2, :cond_2

    iget-object v0, p0, LX/Jqf;->h:LX/0Uh;

    const/16 v2, 0x1fa

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2739988
    iget-object v0, p0, LX/Jqf;->e:LX/Iuh;

    invoke-virtual {v0, p1}, LX/Iuh;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 2739989
    if-eqz v0, :cond_0

    .line 2739990
    :goto_1
    iget-object v2, p0, LX/Jqf;->d:LX/Jqb;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v1, v0}, LX/Jqb;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_0

    .line 2739991
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v0

    .line 2739992
    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2739993
    :goto_2
    iget-object v0, p0, LX/Jqf;->c:LX/3QU;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v4, Lcom/facebook/push/PushProperty;

    sget-object v5, LX/3B4;->MQTT:LX/3B4;

    invoke-direct {v4, v5}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;)V

    sget-object v5, LX/03R;->UNSET:LX/03R;

    invoke-virtual/range {v0 .. v5}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;LX/03R;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    goto :goto_1

    .line 2739994
    :cond_3
    sget-object v3, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    goto :goto_2
.end method
