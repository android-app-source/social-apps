.class public final LX/Jum;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/messaging/notify/NewMessageNotification;

.field public final synthetic c:LX/2HB;

.field public final synthetic d:LX/3pw;

.field public final synthetic e:LX/3pl;

.field public final synthetic f:LX/JuS;

.field public final synthetic g:LX/3Re;


# direct methods
.method public constructor <init>(LX/3Re;ZLcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;LX/3pw;LX/3pl;LX/JuS;)V
    .locals 0

    .prologue
    .line 2749436
    iput-object p1, p0, LX/Jum;->g:LX/3Re;

    iput-boolean p2, p0, LX/Jum;->a:Z

    iput-object p3, p0, LX/Jum;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iput-object p4, p0, LX/Jum;->c:LX/2HB;

    iput-object p5, p0, LX/Jum;->d:LX/3pw;

    iput-object p6, p0, LX/Jum;->e:LX/3pl;

    iput-object p7, p0, LX/Jum;->f:LX/JuS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 12
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749437
    iget-boolean v0, p0, LX/Jum;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Jum;->g:LX/3Re;

    iget-object v0, v0, LX/3Re;->u:LX/3Ri;

    const/4 v1, 0x0

    .line 2749438
    invoke-virtual {v0}, LX/3Ri;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/3Ri;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/JtV;->b:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 2749439
    if-eqz v0, :cond_1

    .line 2749440
    iget-object v0, p0, LX/Jum;->g:LX/3Re;

    iget-object v1, p0, LX/Jum;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    invoke-virtual {v0, v1, p1}, LX/3Re;->a(Lcom/facebook/messaging/notify/NewMessageNotification;Landroid/graphics/Bitmap;)V

    .line 2749441
    :cond_1
    iget-object v0, p0, LX/Jum;->g:LX/3Re;

    iget-object v1, p0, LX/Jum;->c:LX/2HB;

    iget-object v2, p0, LX/Jum;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iget-object v3, p0, LX/Jum;->d:LX/3pw;

    iget-object v4, p0, LX/Jum;->e:LX/3pl;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/3Re;->a(LX/2HB;Lcom/facebook/messaging/notify/NewMessageNotification;LX/3pw;LX/3pl;Landroid/graphics/Bitmap;)V

    .line 2749442
    iget-object v0, p0, LX/Jum;->f:LX/JuS;

    iget-object v1, p0, LX/Jum;->d:LX/3pw;

    iget-object v2, p0, LX/Jum;->e:LX/3pl;

    .line 2749443
    iget-object v6, v0, LX/JuS;->d:LX/3RG;

    iget-object v9, v0, LX/JuS;->a:LX/2HB;

    iget-object v10, v0, LX/JuS;->b:Landroid/app/PendingIntent;

    iget-object v11, v0, LX/JuS;->c:Lcom/facebook/messaging/notify/NewMessageNotification;

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v6 .. v11}, LX/3RG;->a(LX/3pw;LX/3pl;LX/2HB;Landroid/app/PendingIntent;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 2749444
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2749445
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Jum;->a(Landroid/graphics/Bitmap;)V

    .line 2749446
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2749447
    const/4 v0, 0x0

    .line 2749448
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2749449
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2749450
    :cond_0
    invoke-direct {p0, v0}, LX/Jum;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2749451
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2749452
    return-void

    .line 2749453
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
