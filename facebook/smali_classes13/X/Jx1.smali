.class public final LX/Jx1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/0bZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JxB;


# direct methods
.method public constructor <init>(LX/JxB;)V
    .locals 0

    .prologue
    .line 2752699
    iput-object p1, p0, LX/Jx1;->a:LX/JxB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 2752700
    check-cast p1, Ljava/util/List;

    .line 2752701
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Iv;

    .line 2752702
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ct;

    .line 2752703
    if-nez v0, :cond_0

    .line 2752704
    iget-object v0, p0, LX/Jx1;->a:LX/JxB;

    iget-object v0, v0, LX/JxB;->h:LX/0bW;

    const-string v2, "Null input params, updating to null"

    invoke-interface {v0, v2}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2752705
    sget-object v0, LX/2cx;->BLE:LX/2cx;

    invoke-virtual {v1, v0}, LX/2ct;->a(LX/2cx;)LX/0bZ;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2752706
    :goto_0
    return-object v0

    .line 2752707
    :cond_0
    iget-object v2, p0, LX/Jx1;->a:LX/JxB;

    iget-object v2, v2, LX/JxB;->h:LX/0bW;

    const-string v3, "Starting graphql request"

    invoke-interface {v2, v3}, LX/0bW;->a(Ljava/lang/String;)V

    .line 2752708
    new-instance v2, LX/Jwf;

    invoke-direct {v2}, LX/Jwf;-><init>()V

    move-object v2, v2

    .line 2752709
    const-string v3, "ble_params"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2752710
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2752711
    iget-object v3, p0, LX/Jx1;->a:LX/JxB;

    iget-object v3, v3, LX/JxB;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2752712
    iget-object v3, p0, LX/Jx1;->a:LX/JxB;

    iget-object v3, v3, LX/JxB;->e:LX/JwZ;

    invoke-virtual {v3, v0}, LX/JwZ;->a(LX/4Iv;)V

    .line 2752713
    iget-object v0, p0, LX/Jx1;->a:LX/JxB;

    .line 2752714
    new-instance v3, LX/Jx3;

    invoke-direct {v3, v0}, LX/Jx3;-><init>(LX/JxB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v2, v3, p0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2752715
    new-instance p0, LX/Jx7;

    invoke-direct {p0, v0}, LX/Jx7;-><init>(LX/JxB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p1

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2752716
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object p0

    .line 2752717
    new-instance p1, LX/453;

    invoke-direct {p1, p0}, LX/453;-><init>(Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v3, p1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2752718
    move-object v3, p0

    .line 2752719
    new-instance p0, LX/Jx4;

    invoke-direct {p0, v0, v1}, LX/Jx4;-><init>(LX/JxB;LX/2ct;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p1

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2752720
    goto :goto_0
.end method
