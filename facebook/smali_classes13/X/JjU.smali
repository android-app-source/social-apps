.class public LX/JjU;
.super LX/6LI;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ifg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Jk1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

.field public i:Lcom/facebook/messaging/events/banner/EventReminderMembers;

.field public j:LX/Jj4;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2727833
    const-string v0, "EventReminderNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2727834
    return-void
.end method

.method public static g(LX/JjU;)Z
    .locals 1

    .prologue
    .line 2727832
    iget-object v0, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2727807
    new-instance v0, LX/Jj4;

    iget-object v1, p0, LX/JjU;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Jj4;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JjU;->j:LX/Jj4;

    .line 2727808
    invoke-static {p0}, LX/JjU;->g(LX/JjU;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2727809
    iget-object v0, p0, LX/JjU;->j:LX/Jj4;

    .line 2727810
    :goto_0
    return-object v0

    .line 2727811
    :cond_0
    iget-object v0, p0, LX/JjU;->j:LX/Jj4;

    .line 2727812
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2727813
    iget-object v4, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727814
    iget-object v5, v4, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v4, v5

    .line 2727815
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->CALL:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v4, v5, :cond_1

    .line 2727816
    iget-object v4, p0, LX/JjU;->a:Landroid/content/Context;

    const v5, 0x7f082dd0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727817
    const-string v4, " \u22c5 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727818
    :cond_1
    iget-object v4, p0, LX/JjU;->e:LX/Jk1;

    iget-object v5, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c()J

    move-result-wide v5

    sget-object v7, LX/Jk0;->RELATIVE:LX/Jk0;

    invoke-virtual {v4, v5, v6, v7}, LX/Jk1;->a(JLX/Jk0;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727819
    iget-object v4, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727820
    iget-object v5, v4, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2727821
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2727822
    const-string v5, " \u22c5 "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727823
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727824
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 2727825
    iget-object v2, v0, LX/Jj4;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2727826
    iget-object v1, p0, LX/JjU;->c:LX/Ifg;

    iget-object v2, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v0, p0, LX/JjU;->g:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 2727827
    :goto_1
    const-string v4, "event_reminder_banner_view"

    if-nez v2, :cond_4

    const/4 v3, 0x0

    :goto_2
    invoke-static {v1, v4, v3, v0, v2}, LX/Ifg;->a(LX/Ifg;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLightweightEventType;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 2727828
    iget-object v0, p0, LX/JjU;->j:LX/Jj4;

    goto :goto_0

    .line 2727829
    :cond_3
    iget-object v0, p0, LX/JjU;->g:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_1

    .line 2727830
    :cond_4
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v3, v3

    .line 2727831
    goto :goto_2
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2727762
    invoke-static {p0}, LX/JjU;->g(LX/JjU;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2727763
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2727764
    invoke-virtual {v0, p0}, LX/6LN;->a(LX/6LI;)V

    .line 2727765
    iget-object v0, p0, LX/JjU;->j:LX/Jj4;

    if-eqz v0, :cond_1

    .line 2727766
    iget-object v0, p0, LX/JjU;->j:LX/Jj4;

    iget-object v1, p0, LX/JjU;->g:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, LX/JjU;->h:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v3, p0, LX/JjU;->i:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727767
    iput-object v2, v0, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727768
    iput-object v1, v0, LX/Jj4;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2727769
    iput-object v3, v0, LX/Jj4;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727770
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/Jj4;->setClickable(Z)V

    .line 2727771
    iget-object v4, v0, LX/Jj4;->e:Landroid/widget/LinearLayout;

    new-instance v1, LX/Jj1;

    invoke-direct {v1, v0}, LX/Jj1;-><init>(LX/Jj4;)V

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2727772
    const/4 v3, 0x0

    .line 2727773
    iget-object v4, v0, LX/Jj4;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v5, v0, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object p0, v0, LX/Jj4;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    invoke-static {v4, v5, p0}, LX/Jjj;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;Lcom/facebook/messaging/events/banner/EventReminderMembers;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, LX/Jj4;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727774
    iget-object v5, v4, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    move-object v4, v5

    .line 2727775
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2727776
    iget-object v4, v0, LX/Jj4;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727777
    iget-object v5, v4, Lcom/facebook/messaging/events/banner/EventReminderMembers;->b:LX/0Px;

    move-object v4, v5

    .line 2727778
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    .line 2727779
    invoke-virtual {v0}, LX/Jj4;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0f013c

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v5, p0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2727780
    iget-object v5, v0, LX/Jj4;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2727781
    iget-object v4, v0, LX/Jj4;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2727782
    :goto_0
    iget-object v4, v0, LX/Jj4;->l:LX/Jiz;

    invoke-virtual {v4}, LX/Jiz;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2727783
    iget-object v4, v0, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727784
    iget-object v5, v4, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    move-object v4, v5

    .line 2727785
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2727786
    iget-object v4, v0, LX/Jj4;->k:LX/4ob;

    invoke-virtual {v4}, LX/4ob;->a()Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/text/BetterTextView;

    iget-object v5, v0, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727787
    iget-object p0, v5, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    move-object v5, p0

    .line 2727788
    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2727789
    iget-object v4, v0, LX/Jj4;->k:LX/4ob;

    invoke-virtual {v4}, LX/4ob;->e()V

    .line 2727790
    :cond_0
    :goto_1
    iget-object v4, v0, LX/Jj4;->p:LX/Jjj;

    iget-object v5, v0, LX/Jj4;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object p0, v0, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v1, v0, LX/Jj4;->c:Lcom/facebook/messaging/events/banner/EventReminderMembers;

    .line 2727791
    invoke-static {v5, p0, v1}, LX/Jjj;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;Lcom/facebook/messaging/events/banner/EventReminderMembers;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v4, p0}, LX/Jjj;->a(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2727792
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-object v2, v2

    .line 2727793
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    if-eq v2, v3, :cond_6

    .line 2727794
    iget-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderMembers;->a:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-object v2, v2

    .line 2727795
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    if-eq v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v4, v2

    .line 2727796
    if-nez v4, :cond_5

    .line 2727797
    invoke-static {v0}, LX/Jj4;->e(LX/Jj4;)V

    .line 2727798
    :cond_1
    :goto_3
    return-void

    .line 2727799
    :cond_2
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2727800
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    goto :goto_3

    .line 2727801
    :cond_3
    iget-object v4, v0, LX/Jj4;->h:Lcom/facebook/widget/text/BetterTextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2727802
    :cond_4
    iget-object v4, v0, LX/Jj4;->k:LX/4ob;

    invoke-virtual {v4}, LX/4ob;->d()V

    goto :goto_1

    .line 2727803
    :cond_5
    iget-object v4, v0, LX/Jj4;->f:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2727804
    iget-object v4, v0, LX/Jj4;->e:Landroid/widget/LinearLayout;

    const v5, 0x7f02065b

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2727805
    iget-object v4, v0, LX/Jj4;->i:Lcom/facebook/widget/text/BetterTextView;

    new-instance v5, LX/Jj2;

    invoke-direct {v5, v0}, LX/Jj2;-><init>(LX/Jj4;)V

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2727806
    iget-object v4, v0, LX/Jj4;->j:Lcom/facebook/widget/text/BetterTextView;

    new-instance v5, LX/Jj3;

    invoke-direct {v5, v0}, LX/Jj3;-><init>(LX/Jj4;)V

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method
