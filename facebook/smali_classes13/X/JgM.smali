.class public LX/JgM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CKT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/CKT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2722647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722648
    iput-object p1, p0, LX/JgM;->a:LX/0Or;

    .line 2722649
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/Jfj;LX/Jfh;)LX/1a1;
    .locals 3

    .prologue
    .line 2722650
    sget-object v0, LX/JgH;->a:[I

    invoke-virtual {p2}, LX/Jfj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2722651
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2722652
    :pswitch_0
    new-instance v0, LX/JgL;

    new-instance v1, LX/JgE;

    sget-object v2, LX/JgD;->CHECKBOX:LX/JgD;

    invoke-direct {v1, p1, v2}, LX/JgE;-><init>(Landroid/view/ViewGroup;LX/JgD;)V

    iget-object v2, p0, LX/JgM;->a:LX/0Or;

    invoke-direct {v0, p0, v1, v2, p3}, LX/JgL;-><init>(LX/JgM;LX/JgE;LX/0Or;LX/Jfh;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
