.class public final LX/Jy6;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final l:Lcom/facebook/components/ComponentView;

.field public final m:LX/1De;

.field public final n:LX/JyG;

.field private final o:LX/Jy3;

.field public p:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

.field public q:I

.field public r:I

.field public s:Z


# direct methods
.method public constructor <init>(Lcom/facebook/components/ComponentView;LX/1De;LX/JyG;LX/Jy3;)V
    .locals 0

    .prologue
    .line 2754133
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2754134
    iput-object p1, p0, LX/Jy6;->l:Lcom/facebook/components/ComponentView;

    .line 2754135
    iput-object p2, p0, LX/Jy6;->m:LX/1De;

    .line 2754136
    iput-object p3, p0, LX/Jy6;->n:LX/JyG;

    .line 2754137
    iput-object p4, p0, LX/Jy6;->o:LX/Jy3;

    .line 2754138
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x22f95f46

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2754139
    iget-object v0, p0, LX/Jy6;->l:Lcom/facebook/components/ComponentView;

    if-ne p1, v0, :cond_0

    .line 2754140
    iget-object v0, p0, LX/Jy6;->o:LX/Jy3;

    iget-object v1, p0, LX/Jy6;->p:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    const-string v2, "Normal"

    iget v3, p0, LX/Jy6;->q:I

    iget v4, p0, LX/Jy6;->r:I

    iget-boolean v5, p0, LX/Jy6;->s:Z

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Jy3;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;Ljava/lang/String;IIZI)V

    .line 2754141
    :cond_0
    const v0, -0x26346045

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
