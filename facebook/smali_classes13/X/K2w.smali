.class public final LX/K2w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/K2z;


# direct methods
.method public constructor <init>(LX/K2z;)V
    .locals 0

    .prologue
    .line 2764772
    iput-object p1, p0, LX/K2w;->a:LX/K2z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2764773
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2764774
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2764775
    if-eqz p1, :cond_0

    .line 2764776
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2764777
    check-cast v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;

    invoke-virtual {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v1, p0, LX/K2w;->a:LX/K2z;

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2764778
    :goto_0
    iput-object v0, v1, LX/K2z;->m:LX/0Px;

    .line 2764779
    iget-object v0, p0, LX/K2w;->a:LX/K2z;

    invoke-static {v0}, LX/K2z;->d(LX/K2z;)V

    .line 2764780
    :cond_0
    return-void

    .line 2764781
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2764782
    goto :goto_0
.end method
