.class public LX/K0K;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K0K;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 2758245
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/K0K;-><init>(IILjava/lang/String;)V

    .line 2758246
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2758243
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/K0K;-><init>(IILjava/lang/String;II)V

    .line 2758244
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;II)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758237
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2758238
    iput p2, p0, LX/K0K;->a:I

    .line 2758239
    iput-object p3, p0, LX/K0K;->b:Ljava/lang/String;

    .line 2758240
    iput p4, p0, LX/K0K;->c:I

    .line 2758241
    iput p5, p0, LX/K0K;->d:I

    .line 2758242
    return-void
.end method

.method public static b(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2758247
    packed-switch p0, :pswitch_data_0

    .line 2758248
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid image event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2758249
    :pswitch_0
    const-string v0, "topError"

    .line 2758250
    :goto_0
    return-object v0

    .line 2758251
    :pswitch_1
    const-string v0, "topLoad"

    goto :goto_0

    .line 2758252
    :pswitch_2
    const-string v0, "topLoadEnd"

    goto :goto_0

    .line 2758253
    :pswitch_3
    const-string v0, "topLoadStart"

    goto :goto_0

    .line 2758254
    :pswitch_4
    const-string v0, "topProgress"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 2758222
    const/4 v0, 0x0

    .line 2758223
    iget-object v1, p0, LX/K0K;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    iget v1, p0, LX/K0K;->a:I

    if-ne v1, v3, :cond_3

    .line 2758224
    :cond_0
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2758225
    iget-object v1, p0, LX/K0K;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2758226
    const-string v1, "uri"

    iget-object v2, p0, LX/K0K;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2758227
    :cond_1
    iget v1, p0, LX/K0K;->a:I

    if-ne v1, v3, :cond_3

    .line 2758228
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2758229
    const-string v2, "width"

    iget v3, p0, LX/K0K;->c:I

    int-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2758230
    const-string v2, "height"

    iget v3, p0, LX/K0K;->d:I

    int-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2758231
    iget-object v2, p0, LX/K0K;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2758232
    const-string v2, "url"

    iget-object v3, p0, LX/K0K;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2758233
    :cond_2
    const-string v2, "source"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2758234
    :cond_3
    iget v1, p0, LX/5r0;->c:I

    move v1, v1

    .line 2758235
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2, v0}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2758236
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2758221
    iget v0, p0, LX/K0K;->a:I

    invoke-static {v0}, LX/K0K;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 2758220
    iget v0, p0, LX/K0K;->a:I

    int-to-short v0, v0

    return v0
.end method
