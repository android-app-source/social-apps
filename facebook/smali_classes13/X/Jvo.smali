.class public final LX/Jvo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V
    .locals 0

    .prologue
    .line 2750819
    iput-object p1, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 2750820
    iget-object v0, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->Q:LX/89f;

    iget-object v1, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->n:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->p:LX/0Px;

    invoke-interface {v0, v1, v2}, LX/89f;->a(Landroid/view/ViewGroup;LX/0Px;)V

    .line 2750821
    iget-object v0, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->L:Z

    if-nez v0, :cond_0

    .line 2750822
    iget-object v0, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 2750823
    :cond_0
    iget-object v0, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    invoke-interface {v0}, LX/89S;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2750824
    if-eqz v0, :cond_1

    .line 2750825
    new-instance v1, LX/Jvn;

    invoke-direct {v1, p0}, LX/Jvn;-><init>(LX/Jvo;)V

    iget-object v2, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->J:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2750826
    :cond_1
    iget-object v0, p0, LX/Jvo;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2750827
    return-void
.end method
