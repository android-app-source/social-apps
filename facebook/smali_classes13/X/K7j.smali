.class public final LX/K7j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

.field public final synthetic b:LX/K7k;


# direct methods
.method public constructor <init>(LX/K7k;Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V
    .locals 0

    .prologue
    .line 2773409
    iput-object p1, p0, LX/K7j;->b:LX/K7k;

    iput-object p2, p0, LX/K7j;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 2773410
    iget-object v0, p0, LX/K7j;->b:LX/K7k;

    iget-object v1, v0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->f(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;I)V

    .line 2773411
    return-void
.end method
