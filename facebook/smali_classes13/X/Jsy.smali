.class public final enum LX/Jsy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jsy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jsy;

.field public static final enum DEFAULT:LX/Jsy;

.field public static final enum DEFAULT_WITH_SENDER:LX/Jsy;

.field public static final enum IMAGE_ATTACHMENT_BOTTOM:LX/Jsy;

.field public static final enum IMAGE_ATTACHMENT_MIDDLE:LX/Jsy;

.field public static final enum ONLY_WITH_NEWER_ROW:LX/Jsy;

.field public static final enum ONLY_WITH_NEWER_ROW_WITH_SENDER:LX/Jsy;

.field public static final enum ONLY_WITH_OLDER_ROW:LX/Jsy;

.field public static final enum WITH_OLDER_AND_NEW_ROWS:LX/Jsy;


# instance fields
.field public final groupWithNewerRow:Z

.field public final groupWithOlderRow:Z

.field public final stickToTop:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2746106
    new-instance v0, LX/Jsy;

    const-string v1, "DEFAULT"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, LX/Jsy;->DEFAULT:LX/Jsy;

    .line 2746107
    new-instance v3, LX/Jsy;

    const-string v4, "DEFAULT_WITH_SENDER"

    move v5, v9

    move v6, v2

    move v7, v2

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->DEFAULT_WITH_SENDER:LX/Jsy;

    .line 2746108
    new-instance v3, LX/Jsy;

    const-string v4, "ONLY_WITH_NEWER_ROW"

    move v5, v10

    move v6, v9

    move v7, v2

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->ONLY_WITH_NEWER_ROW:LX/Jsy;

    .line 2746109
    new-instance v3, LX/Jsy;

    const-string v4, "ONLY_WITH_NEWER_ROW_WITH_SENDER"

    move v5, v11

    move v6, v9

    move v7, v2

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->ONLY_WITH_NEWER_ROW_WITH_SENDER:LX/Jsy;

    .line 2746110
    new-instance v3, LX/Jsy;

    const-string v4, "ONLY_WITH_OLDER_ROW"

    move v5, v12

    move v6, v2

    move v7, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->ONLY_WITH_OLDER_ROW:LX/Jsy;

    .line 2746111
    new-instance v3, LX/Jsy;

    const-string v4, "WITH_OLDER_AND_NEW_ROWS"

    const/4 v5, 0x5

    move v6, v9

    move v7, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->WITH_OLDER_AND_NEW_ROWS:LX/Jsy;

    .line 2746112
    new-instance v3, LX/Jsy;

    const-string v4, "IMAGE_ATTACHMENT_MIDDLE"

    const/4 v5, 0x6

    move v6, v9

    move v7, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->IMAGE_ATTACHMENT_MIDDLE:LX/Jsy;

    .line 2746113
    new-instance v3, LX/Jsy;

    const-string v4, "IMAGE_ATTACHMENT_BOTTOM"

    const/4 v5, 0x7

    move v6, v2

    move v7, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jsy;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jsy;->IMAGE_ATTACHMENT_BOTTOM:LX/Jsy;

    .line 2746114
    const/16 v0, 0x8

    new-array v0, v0, [LX/Jsy;

    sget-object v1, LX/Jsy;->DEFAULT:LX/Jsy;

    aput-object v1, v0, v2

    sget-object v1, LX/Jsy;->DEFAULT_WITH_SENDER:LX/Jsy;

    aput-object v1, v0, v9

    sget-object v1, LX/Jsy;->ONLY_WITH_NEWER_ROW:LX/Jsy;

    aput-object v1, v0, v10

    sget-object v1, LX/Jsy;->ONLY_WITH_NEWER_ROW_WITH_SENDER:LX/Jsy;

    aput-object v1, v0, v11

    sget-object v1, LX/Jsy;->ONLY_WITH_OLDER_ROW:LX/Jsy;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LX/Jsy;->WITH_OLDER_AND_NEW_ROWS:LX/Jsy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jsy;->IMAGE_ATTACHMENT_MIDDLE:LX/Jsy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Jsy;->IMAGE_ATTACHMENT_BOTTOM:LX/Jsy;

    aput-object v2, v0, v1

    sput-object v0, LX/Jsy;->$VALUES:[LX/Jsy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    .line 2746115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2746116
    iput-boolean p3, p0, LX/Jsy;->groupWithNewerRow:Z

    .line 2746117
    iput-boolean p4, p0, LX/Jsy;->groupWithOlderRow:Z

    .line 2746118
    iput-boolean p5, p0, LX/Jsy;->stickToTop:Z

    .line 2746119
    return-void
.end method

.method public static forBottomImageAttachment(Z)LX/Jsy;
    .locals 1

    .prologue
    .line 2746120
    if-eqz p0, :cond_0

    sget-object v0, LX/Jsy;->IMAGE_ATTACHMENT_MIDDLE:LX/Jsy;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Jsy;->IMAGE_ATTACHMENT_BOTTOM:LX/Jsy;

    goto :goto_0
.end method

.method public static forGrouping(ZZZ)LX/Jsy;
    .locals 1

    .prologue
    .line 2746121
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 2746122
    sget-object v0, LX/Jsy;->WITH_OLDER_AND_NEW_ROWS:LX/Jsy;

    .line 2746123
    :goto_0
    return-object v0

    .line 2746124
    :cond_0
    if-eqz p0, :cond_2

    .line 2746125
    if-eqz p2, :cond_1

    sget-object v0, LX/Jsy;->ONLY_WITH_NEWER_ROW_WITH_SENDER:LX/Jsy;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Jsy;->ONLY_WITH_NEWER_ROW:LX/Jsy;

    goto :goto_0

    .line 2746126
    :cond_2
    if-eqz p1, :cond_3

    .line 2746127
    sget-object v0, LX/Jsy;->ONLY_WITH_OLDER_ROW:LX/Jsy;

    goto :goto_0

    .line 2746128
    :cond_3
    if-eqz p2, :cond_4

    sget-object v0, LX/Jsy;->DEFAULT_WITH_SENDER:LX/Jsy;

    goto :goto_0

    :cond_4
    sget-object v0, LX/Jsy;->DEFAULT:LX/Jsy;

    goto :goto_0
.end method

.method public static forMiddleImageAttachment()LX/Jsy;
    .locals 1

    .prologue
    .line 2746129
    sget-object v0, LX/Jsy;->IMAGE_ATTACHMENT_MIDDLE:LX/Jsy;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jsy;
    .locals 1

    .prologue
    .line 2746130
    const-class v0, LX/Jsy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jsy;

    return-object v0
.end method

.method public static values()[LX/Jsy;
    .locals 1

    .prologue
    .line 2746131
    sget-object v0, LX/Jsy;->$VALUES:[LX/Jsy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jsy;

    return-object v0
.end method
