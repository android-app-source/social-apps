.class public final LX/K3z;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766259
    iput-object p1, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, LX/1OM;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;B)V
    .locals 0

    .prologue
    .line 2766260
    invoke-direct {p0, p1}, LX/K3z;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2766261
    new-instance v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v2, v2, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->t:LX/1Ad;

    invoke-direct {v0, v1, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;-><init>(Landroid/content/Context;LX/1Ad;)V

    .line 2766262
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2766263
    new-instance v1, LX/K3x;

    invoke-direct {v1, p0}, LX/K3x;-><init>(LX/K3z;)V

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2766264
    new-instance v1, LX/K3y;

    invoke-direct {v1, p0, v0}, LX/K3y;-><init>(LX/K3z;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2766265
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;

    .line 2766266
    iget-object v1, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v2, LX/K4N;->MUSIC:LX/K4N;

    if-ne v1, v2, :cond_1

    .line 2766267
    iget-object v1, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/model/Mood;

    .line 2766268
    const/4 p1, 0x0

    .line 2766269
    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2766270
    :goto_0
    iget-object v2, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v2, v2, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ah:Lcom/facebook/storyline/model/Mood;

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setSelected(Z)V

    .line 2766271
    :cond_0
    :goto_1
    return-void

    .line 2766272
    :cond_1
    iget-object v1, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v2, LX/K4N;->PHOTO:LX/K4N;

    if-ne v1, v2, :cond_0

    .line 2766273
    if-lez p2, :cond_2

    .line 2766274
    iget-object v1, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v1, v1, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyline/model/StorylinePhoto;

    .line 2766275
    const/16 p1, 0x8

    .line 2766276
    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2766277
    :goto_2
    iget-object v2, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v2, v2, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ag:Lcom/facebook/storyline/model/StorylinePhoto;

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setSelected(Z)V

    goto :goto_1

    .line 2766278
    :cond_2
    sget-object v1, LX/K4K;->EDIT_PHOTO:LX/K4K;

    const/16 p2, 0x8

    .line 2766279
    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2766280
    :goto_3
    goto :goto_1

    .line 2766281
    :cond_3
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->g:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2766282
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->i:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2766283
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Z)V

    .line 2766284
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->g:Landroid/widget/TextView;

    iget-object p1, v1, Lcom/facebook/storyline/model/Mood;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2766285
    iget-object v2, v1, Lcom/facebook/storyline/model/Mood;->icon:Lcom/facebook/storyline/model/Mood$Icon;

    iget-object v2, v2, Lcom/facebook/storyline/model/Mood$Icon;->uri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Landroid/net/Uri;)V

    .line 2766286
    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2766287
    :cond_4
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Z)V

    .line 2766288
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->g:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2766289
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->i:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2766290
    iget-object v2, v1, Lcom/facebook/storyline/model/StorylinePhoto;->e:Landroid/net/Uri;

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Landroid/net/Uri;)V

    .line 2766291
    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_2

    .line 2766292
    :cond_5
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->a(Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;Z)V

    .line 2766293
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0a00fb

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2766294
    sget-object v2, LX/K4J;->a:[I

    invoke-virtual {v1}, LX/K4K;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 2766295
    :goto_4
    invoke-virtual {v0, v1}, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    .line 2766296
    :pswitch_0
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->g:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2766297
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->i:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    .line 2766298
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const p0, 0x7f020958

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2766299
    iget-object v2, v0, Lcom/facebook/storyline/fb4a/ui/TabSectionImageView;->f:Landroid/widget/TextView;

    const p0, 0x7f083c36

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2766300
    iget-object v0, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    if-nez v0, :cond_0

    .line 2766301
    const/4 v0, 0x0

    .line 2766302
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->af:LX/K4N;

    sget-object v1, LX/K4N;->MUSIC:LX/K4N;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->X:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/K3z;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
