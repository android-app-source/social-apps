.class public final enum LX/Jqr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jqr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jqr;

.field public static final enum NOT_PASSED:LX/Jqr;

.field public static final enum THREAD_FETCHED:LX/Jqr;

.field public static final enum THREAD_NONEXISTENT:LX/Jqr;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2740502
    new-instance v0, LX/Jqr;

    const-string v1, "NOT_PASSED"

    invoke-direct {v0, v1, v2}, LX/Jqr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jqr;->NOT_PASSED:LX/Jqr;

    new-instance v0, LX/Jqr;

    const-string v1, "THREAD_FETCHED"

    invoke-direct {v0, v1, v3}, LX/Jqr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jqr;->THREAD_FETCHED:LX/Jqr;

    new-instance v0, LX/Jqr;

    const-string v1, "THREAD_NONEXISTENT"

    invoke-direct {v0, v1, v4}, LX/Jqr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jqr;->THREAD_NONEXISTENT:LX/Jqr;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Jqr;

    sget-object v1, LX/Jqr;->NOT_PASSED:LX/Jqr;

    aput-object v1, v0, v2

    sget-object v1, LX/Jqr;->THREAD_FETCHED:LX/Jqr;

    aput-object v1, v0, v3

    sget-object v1, LX/Jqr;->THREAD_NONEXISTENT:LX/Jqr;

    aput-object v1, v0, v4

    sput-object v0, LX/Jqr;->$VALUES:[LX/Jqr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2740503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jqr;
    .locals 1

    .prologue
    .line 2740504
    const-class v0, LX/Jqr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jqr;

    return-object v0
.end method

.method public static values()[LX/Jqr;
    .locals 1

    .prologue
    .line 2740505
    sget-object v0, LX/Jqr;->$VALUES:[LX/Jqr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jqr;

    return-object v0
.end method
