.class public final LX/JvE;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2749948
    const-class v1, Lcom/facebook/pages/common/deeplink/fragments/graphql/FetchPageDeeplinkTabGraphQLModels$FetchPageDeeplinkTabQueryModel;

    const v0, -0x90e9cc9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchPageDeeplinkTabQuery"

    const-string v6, "5176219b0a3e16aa950e044efe4eeaf4"

    const-string v7, "page_deeplink_tab"

    const-string v8, "10155069965546729"

    const-string v9, "10155259087926729"

    .line 2749949
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2749950
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2749951
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2749952
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2749953
    sparse-switch v0, :sswitch_data_0

    .line 2749954
    :goto_0
    return-object p1

    .line 2749955
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2749956
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2749957
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_1
        -0x12da6d78 -> :sswitch_2
        0x529f9eff -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2749958
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2749959
    :goto_1
    return v0

    .line 2749960
    :pswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2749961
    :pswitch_1
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
