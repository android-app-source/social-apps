.class public LX/K0M;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2758262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/1Up;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2758263
    const-string v0, "contain"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2758264
    sget-object v0, LX/1Up;->c:LX/1Up;

    .line 2758265
    :goto_0
    return-object v0

    .line 2758266
    :cond_0
    const-string v0, "cover"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2758267
    sget-object v0, LX/1Up;->g:LX/1Up;

    goto :goto_0

    .line 2758268
    :cond_1
    const-string v0, "stretch"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2758269
    sget-object v0, LX/1Up;->a:LX/1Up;

    goto :goto_0

    .line 2758270
    :cond_2
    const-string v0, "center"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2758271
    sget-object v0, LX/1Up;->f:LX/1Up;

    goto :goto_0

    .line 2758272
    :cond_3
    if-nez p0, :cond_4

    .line 2758273
    sget-object v0, LX/1Up;->g:LX/1Up;

    move-object v0, v0

    .line 2758274
    goto :goto_0

    .line 2758275
    :cond_4
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid resize mode: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method
