.class public final LX/K9n;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2778027
    const-class v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    const v0, 0x6eae45f9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "LiveMapVideosQuery"

    const-string v6, "6ac83737cd95704eaaedbc1c3dd33125"

    const-string v7, "live_map_videos"

    const-string v8, "10155261855061729"

    const/4 v9, 0x0

    .line 2778028
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2778029
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2778030
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2778031
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2778032
    sparse-switch v0, :sswitch_data_0

    .line 2778033
    :goto_0
    return-object p1

    .line 2778034
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2778035
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2778036
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2778037
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2778038
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2778039
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2778040
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2778041
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2778042
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2778043
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2778044
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2778045
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2778046
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2778047
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2778048
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2778049
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2778050
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2778051
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2778052
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2778053
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2778054
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2778055
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_12
        -0x680de62a -> :sswitch_8
        -0x6326fdb3 -> :sswitch_7
        -0x57984ae8 -> :sswitch_10
        -0x513764de -> :sswitch_13
        -0x4496acc9 -> :sswitch_9
        -0x41a91745 -> :sswitch_e
        -0x41143822 -> :sswitch_3
        -0x3c54de38 -> :sswitch_c
        -0x3b85b241 -> :sswitch_15
        -0x25a646c8 -> :sswitch_4
        -0x1b87b280 -> :sswitch_6
        -0x15db59af -> :sswitch_14
        -0x12efdeb3 -> :sswitch_a
        -0x3e446ed -> :sswitch_5
        -0x12603b3 -> :sswitch_11
        0x6234bbb -> :sswitch_1
        0xa1fa812 -> :sswitch_2
        0x214100e0 -> :sswitch_b
        0x291d8de0 -> :sswitch_f
        0x51b3404b -> :sswitch_0
        0x73a026b5 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2778056
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2778057
    :goto_1
    return v0

    .line 2778058
    :sswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "12"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "19"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "21"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "16"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 2778059
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778060
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778061
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778062
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778063
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778064
    :pswitch_5
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2778065
    :pswitch_6
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2778066
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x33 -> :sswitch_0
        0x34 -> :sswitch_1
        0x621 -> :sswitch_2
        0x625 -> :sswitch_7
        0x626 -> :sswitch_3
        0x628 -> :sswitch_4
        0x63e -> :sswitch_5
        0x63f -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
