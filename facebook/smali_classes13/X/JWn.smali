.class public LX/JWn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JWo;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JWn",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JWo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703517
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2703518
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JWn;->b:LX/0Zi;

    .line 2703519
    iput-object p1, p0, LX/JWn;->a:LX/0Ot;

    .line 2703520
    return-void
.end method

.method public static a(LX/0QB;)LX/JWn;
    .locals 4

    .prologue
    .line 2703506
    const-class v1, LX/JWn;

    monitor-enter v1

    .line 2703507
    :try_start_0
    sget-object v0, LX/JWn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703508
    sput-object v2, LX/JWn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703509
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703510
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703511
    new-instance v3, LX/JWn;

    const/16 p0, 0x20d5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JWn;-><init>(LX/0Ot;)V

    .line 2703512
    move-object v0, v3

    .line 2703513
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703514
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703515
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2703471
    check-cast p2, LX/JWm;

    .line 2703472
    iget-object v0, p0, LX/JWn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JWo;

    iget-object v2, p2, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v4, p2, LX/JWm;->c:LX/1Pc;

    iget-object v5, p2, LX/JWm;->d:LX/3mj;

    move-object v1, p1

    const/4 v9, 0x0

    .line 2703473
    new-instance v8, LX/JWN;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->g()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v6, v9}, LX/JWN;-><init>(Ljava/lang/String;Z)V

    move-object v6, v4

    .line 2703474
    check-cast v6, LX/1Pr;

    .line 2703475
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 2703476
    check-cast v7, LX/0jW;

    invoke-interface {v6, v8, v7}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    .line 2703477
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b1bba

    invoke-interface {v6, v7}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f020a3d

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    const/16 v7, 0x8

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0a00d5

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/JWo;->b:LX/JWd;

    const/4 v8, 0x0

    .line 2703478
    new-instance v9, LX/JWc;

    invoke-direct {v9, v7}, LX/JWc;-><init>(LX/JWd;)V

    .line 2703479
    iget-object v10, v7, LX/JWd;->b:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/JWb;

    .line 2703480
    if-nez v10, :cond_0

    .line 2703481
    new-instance v10, LX/JWb;

    invoke-direct {v10, v7}, LX/JWb;-><init>(LX/JWd;)V

    .line 2703482
    :cond_0
    invoke-static {v10, v1, v8, v8, v9}, LX/JWb;->a$redex0(LX/JWb;LX/1De;IILX/JWc;)V

    .line 2703483
    move-object v9, v10

    .line 2703484
    move-object v8, v9

    .line 2703485
    move-object v7, v8

    .line 2703486
    iget-object v8, v7, LX/JWb;->a:LX/JWc;

    iput-object v3, v8, LX/JWc;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 2703487
    iget-object v8, v7, LX/JWb;->e:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 2703488
    move-object v7, v7

    .line 2703489
    iget-object v8, v7, LX/JWb;->a:LX/JWc;

    iput-boolean p0, v8, LX/JWc;->b:Z

    .line 2703490
    iget-object v8, v7, LX/JWb;->e:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 2703491
    move-object v7, v7

    .line 2703492
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a009e

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b0060

    invoke-interface {v7, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    invoke-interface {v7, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p2

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    const/high16 v5, 0x3f800000    # 1.0f

    .line 2703493
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 2703494
    :try_start_0
    iget-object v2, v6, LX/JWo;->c:LX/0lB;

    invoke-virtual {v2, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2703495
    const-string v3, "ego_id"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2703496
    :goto_0
    move-object v1, v1

    .line 2703497
    invoke-static {v7}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    iget-object v0, v6, LX/JWo;->a:LX/JWR;

    invoke-virtual {v0, v7}, LX/JWR;->c(LX/1De;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/JWP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/JWP;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0, v10}, LX/JWP;->a(LX/1Pc;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/JWP;->b(Ljava/lang/String;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0, v11}, LX/JWP;->a(LX/3mj;)LX/JWP;

    move-result-object v3

    if-eqz p0, :cond_2

    sget-object v0, LX/JWT;->UNDO:LX/JWT;

    :goto_1
    invoke-virtual {v3, v0}, LX/JWP;->a(LX/JWT;)LX/JWP;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2703498
    if-nez p0, :cond_1

    .line 2703499
    invoke-static {v7}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a009e

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Di;->j(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b0063

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v3, v6, LX/JWo;->a:LX/JWR;

    invoke-virtual {v3, v7}, LX/JWR;->c(LX/1De;)LX/JWP;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/JWP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JWP;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/JWP;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)LX/JWP;

    move-result-object v3

    invoke-virtual {v3, v10}, LX/JWP;->a(LX/1Pc;)LX/JWP;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/JWP;->b(Ljava/lang/String;)LX/JWP;

    move-result-object v1

    invoke-virtual {v1, v11}, LX/JWP;->a(LX/3mj;)LX/JWP;

    move-result-object v1

    sget-object v3, LX/JWT;->REMOVE:LX/JWT;

    invoke-virtual {v1, v3}, LX/JWP;->a(LX/JWT;)LX/JWP;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2703500
    :cond_1
    move-object v6, v0

    .line 2703501
    invoke-interface {p2, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2703502
    return-object v0

    .line 2703503
    :cond_2
    sget-object v0, LX/JWT;->ADD_FRIEND:LX/JWT;

    goto :goto_1

    .line 2703504
    :cond_3
    :try_start_1
    const-string v3, "ego_id"

    invoke-virtual {v2, v3}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto/16 :goto_0

    .line 2703505
    :catch_0
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2703469
    invoke-static {}, LX/1dS;->b()V

    .line 2703470
    const/4 v0, 0x0

    return-object v0
.end method
