.class public final LX/JtK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/stickers/model/Sticker;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;)V
    .locals 0

    .prologue
    .line 2746567
    iput-object p1, p0, LX/JtK;->a:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746568
    sget-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v1, "Fetched for sticker info failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746569
    iget-object v0, p0, LX/JtK;->a:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;

    iget-object v0, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746570
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2746571
    check-cast p1, Lcom/facebook/stickers/model/Sticker;

    .line 2746572
    iget-object v0, p0, LX/JtK;->a:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;

    iget-object v0, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x5cd80ca9

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746573
    return-void
.end method
