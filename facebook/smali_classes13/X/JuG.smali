.class public LX/JuG;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/JuE;",
        ">;",
        "Lcom/facebook/notes/view/block/TimestampAndPrivacyBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/11R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2748646
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2748647
    const-class v0, LX/JuG;

    invoke-static {v0, p0}, LX/JuG;->a(Ljava/lang/Class;LX/02k;)V

    .line 2748648
    iget-object v0, p0, LX/JuG;->b:LX/Ck0;

    invoke-virtual {v0, p1}, LX/Ck0;->a(Landroid/view/View;)V

    .line 2748649
    const v0, 0x7f0d1dd1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JuG;->c:Landroid/widget/TextView;

    .line 2748650
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JuG;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11R;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object v1, p1, LX/JuG;->a:LX/11R;

    iput-object p0, p1, LX/JuG;->b:LX/Ck0;

    return-void
.end method
