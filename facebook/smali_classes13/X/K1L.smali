.class public LX/K1L;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K1L;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 2761728
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2761729
    iput p2, p0, LX/K1L;->a:I

    .line 2761730
    iput p3, p0, LX/K1L;->b:I

    .line 2761731
    return-void
.end method

.method private j()LX/5pH;
    .locals 4

    .prologue
    .line 2761732
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2761733
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2761734
    const-string v2, "end"

    iget v3, p0, LX/K1L;->b:I

    invoke-interface {v1, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761735
    const-string v2, "start"

    iget v3, p0, LX/K1L;->a:I

    invoke-interface {v1, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2761736
    const-string v2, "selection"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2761737
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2761738
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2761739
    invoke-virtual {p0}, LX/K1L;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K1L;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2761740
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2761741
    const-string v0, "topSelectionChange"

    return-object v0
.end method
