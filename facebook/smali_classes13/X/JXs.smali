.class public final LX/JXs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/04D;

.field private final e:LX/2oL;

.field private final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:LX/2yJ;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;LX/2oL;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;LX/2yJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/04D;",
            "LX/2oL;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "Ljava/lang/String;",
            "LX/2yJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2705458
    iput-object p1, p0, LX/JXs;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2705459
    iput-object p2, p0, LX/JXs;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705460
    iput-object p3, p0, LX/JXs;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705461
    iput-object p4, p0, LX/JXs;->d:LX/04D;

    .line 2705462
    iput-object p5, p0, LX/JXs;->e:LX/2oL;

    .line 2705463
    iput-object p6, p0, LX/JXs;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2705464
    iput-object p7, p0, LX/JXs;->g:Ljava/lang/String;

    .line 2705465
    iput-object p8, p0, LX/JXs;->h:LX/2yJ;

    .line 2705466
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3e5418fb

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2705417
    iget-object v0, p0, LX/JXs;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->b:LX/13l;

    sget-object v1, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 2705418
    iget-object v0, p0, LX/JXs;->e:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    invoke-virtual {v0}, LX/2oO;->a()V

    .line 2705419
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2705420
    if-nez v0, :cond_0

    .line 2705421
    const v0, 0x92adad

    invoke-static {v2, v2, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2705422
    :goto_0
    return-void

    .line 2705423
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2705424
    iget-object v2, p0, LX/JXs;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->d:LX/17Y;

    iget-object v3, p0, LX/JXs;->g:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2705425
    if-nez v1, :cond_1

    .line 2705426
    const v0, -0x6b28c609

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0

    .line 2705427
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2705428
    iget-object v2, p0, LX/JXs;->h:LX/2yJ;

    iget-object v3, p0, LX/JXs;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, LX/2yJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 2705429
    if-eqz v2, :cond_2

    .line 2705430
    const-string v3, "tracking_codes"

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705431
    :cond_2
    invoke-interface {v0}, LX/0f8;->o()LX/0hE;

    move-result-object v0

    check-cast v0, LX/FAe;

    .line 2705432
    invoke-virtual {v0, v1}, LX/FAe;->setArguments(Landroid/os/Bundle;)V

    .line 2705433
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2705434
    iget-object v1, p0, LX/JXs;->h:LX/2yJ;

    invoke-virtual {v1}, LX/2yJ;->a()LX/2yN;

    move-result-object v1

    iget-object v2, p0, LX/JXs;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v1, v2, p1}, LX/2yN;->a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2705435
    iget-object v2, p0, LX/JXs;->a:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->e:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2705436
    invoke-virtual {v0}, LX/FAe;->k()V

    .line 2705437
    invoke-virtual {v0}, LX/FAe;->d()V

    .line 2705438
    new-instance v1, LX/JXu;

    iget-object v2, p0, LX/JXs;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, v2}, LX/JXu;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 2705439
    iput-object v1, v0, LX/FAe;->e:LX/394;

    .line 2705440
    check-cast p1, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2705441
    iget-object v1, p0, LX/JXs;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getTransitionNode()LX/3FT;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getSeekPosition()I

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getLastStartPosition()I

    move-result v4

    iget-object v5, p0, LX/JXs;->d:LX/04D;

    iget-object v6, p0, LX/JXs;->e:LX/2oL;

    invoke-virtual {v6}, LX/2oL;->c()LX/04g;

    move-result-object v6

    .line 2705442
    iget-object p1, v0, LX/FAe;->e:LX/394;

    move-object v8, v1

    move-object v9, v2

    move v10, v3

    move v11, v4

    move-object v12, v5

    move-object p0, v6

    .line 2705443
    new-instance v1, LX/Gp3;

    const/16 v2, 0x77

    sget v3, LX/CIY;->a:I

    invoke-direct {v1, v2, v3}, LX/Gp3;-><init>(II)V

    .line 2705444
    iput-object v8, v1, LX/Gp3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705445
    iput-object v9, v1, LX/Gp3;->b:LX/3FT;

    .line 2705446
    iput v10, v1, LX/Gp3;->c:I

    .line 2705447
    iput v11, v1, LX/Gp3;->d:I

    .line 2705448
    iput-object v12, v1, LX/Gp3;->e:LX/04D;

    .line 2705449
    iput-object p0, v1, LX/Gp3;->f:LX/04g;

    .line 2705450
    iput-object p1, v1, LX/Gp3;->g:LX/394;

    .line 2705451
    invoke-virtual {v1}, LX/Gp3;->c()LX/Gp4;

    move-result-object v1

    move-object v8, v1

    .line 2705452
    iget-object v9, v0, LX/FAe;->d:LX/FAa;

    .line 2705453
    iput-object v8, v9, LX/FAa;->aG:LX/Clr;

    .line 2705454
    iget-object v8, v0, LX/FAe;->d:LX/FAa;

    const/4 v9, 0x1

    .line 2705455
    iput-boolean v9, v8, LX/FAa;->aH:Z

    .line 2705456
    iget-object v8, v0, LX/FAe;->g:LX/Gn8;

    invoke-virtual {v8}, LX/Gn8;->a()V

    .line 2705457
    const v0, 0x883f732

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
