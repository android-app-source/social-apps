.class public final LX/JvP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jb3;

.field public final synthetic b:LX/JvQ;


# direct methods
.method public constructor <init>(LX/JvQ;LX/Jb3;)V
    .locals 0

    .prologue
    .line 2750382
    iput-object p1, p0, LX/JvP;->b:LX/JvQ;

    iput-object p2, p0, LX/JvP;->a:LX/Jb3;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2750383
    iget-object v0, p0, LX/JvP;->b:LX/JvQ;

    iget-object v0, v0, LX/JvQ;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-object v2, p0, LX/JvP;->b:LX/JvQ;

    iget-wide v2, v2, LX/JvQ;->f:J

    const-string v4, "fetch_fail"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2750384
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2750385
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2750386
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2750387
    :goto_0
    if-nez v0, :cond_1

    .line 2750388
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2750389
    invoke-static {}, LX/JvZ;->a()LX/JvZ;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v6, v0

    .line 2750390
    :goto_1
    iget-object v0, p0, LX/JvP;->b:LX/JvQ;

    iget-object v0, v0, LX/JvQ;->d:LX/0if;

    sget-object v1, LX/0ig;->aq:LX/0ih;

    iget-object v2, p0, LX/JvP;->b:LX/JvQ;

    iget-wide v2, v2, LX/JvQ;->f:J

    const-string v4, "fetch_succeed"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "list_size:"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 2750391
    iget-object v0, p0, LX/JvP;->b:LX/JvQ;

    iget-object v0, v0, LX/JvQ;->e:Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;

    .line 2750392
    iget-object v1, v0, Lcom/facebook/pages/common/voiceswitcher/fragment/PageVoiceSwitcherFragment;->i:LX/JvL;

    .line 2750393
    iput-object v6, v1, LX/JvL;->c:Ljava/util/List;

    .line 2750394
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2750395
    return-void

    .line 2750396
    :cond_0
    iget-object v0, p0, LX/JvP;->a:LX/Jb3;

    invoke-interface {v0, p1}, LX/Jb3;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v6, v0

    goto :goto_1
.end method
