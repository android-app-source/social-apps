.class public final LX/K0X;
.super LX/9na;
.source ""

# interfaces
.implements LX/5rD;


# instance fields
.field private final a:LX/5qp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2758882
    invoke-direct {p0, p1}, LX/9na;-><init>(Landroid/content/Context;)V

    .line 2758883
    new-instance v0, LX/5qp;

    invoke-direct {v0, p0}, LX/5qp;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, LX/K0X;->a:LX/5qp;

    .line 2758884
    return-void
.end method

.method private c()LX/5s9;
    .locals 2

    .prologue
    .line 2758878
    invoke-virtual {p0}, LX/K0X;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2758879
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2758880
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2758881
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 2758876
    iget-object v0, p0, LX/K0X;->a:LX/5qp;

    invoke-direct {p0}, LX/K0X;->c()LX/5s9;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/5qp;->a(Landroid/view/MotionEvent;LX/5s9;)V

    .line 2758877
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2758866
    iget-object v0, p0, LX/K0X;->a:LX/5qp;

    invoke-direct {p0}, LX/K0X;->c()LX/5s9;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/5qp;->b(Landroid/view/MotionEvent;LX/5s9;)V

    .line 2758867
    invoke-super {p0, p1}, LX/9na;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x22ddf4d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2758872
    invoke-super {p0, p1, p2, p3, p4}, LX/9na;->onSizeChanged(IIII)V

    .line 2758873
    invoke-virtual {p0}, LX/K0X;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2758874
    invoke-virtual {p0}, LX/K0X;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    new-instance v2, Lcom/facebook/react/views/modal/ReactModalHostView$DialogRootViewGroup$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/facebook/react/views/modal/ReactModalHostView$DialogRootViewGroup$1;-><init>(LX/K0X;II)V

    invoke-virtual {v0, v2}, LX/5pX;->b(Ljava/lang/Runnable;)V

    .line 2758875
    :cond_0
    const/16 v0, 0x2d

    const v2, 0x3c253663

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x5b0004f0

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2758869
    iget-object v1, p0, LX/K0X;->a:LX/5qp;

    invoke-direct {p0}, LX/K0X;->c()LX/5s9;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/5qp;->b(Landroid/view/MotionEvent;LX/5s9;)V

    .line 2758870
    invoke-super {p0, p1}, LX/9na;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2758871
    const v1, 0x550f5ab7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v4
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 2758868
    return-void
.end method
