.class public final LX/JpZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0TF",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

.field private final b:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;LX/0Vd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Vd",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2737670
    iput-object p1, p0, LX/JpZ;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737671
    iput-object p2, p0, LX/JpZ;->b:LX/0Vd;

    .line 2737672
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2737673
    iget-object v0, p0, LX/JpZ;->b:LX/0Vd;

    invoke-virtual {v0, p1}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    .line 2737674
    iget-object v0, p0, LX/JpZ;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V

    .line 2737675
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 2737676
    iget-object v0, p0, LX/JpZ;->b:LX/0Vd;

    invoke-virtual {v0, p1}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    .line 2737677
    iget-object v0, p0, LX/JpZ;->a:Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;->b(Lcom/facebook/messaging/sms/migration/ContactMatchingOperationLogic;)V

    .line 2737678
    return-void
.end method
