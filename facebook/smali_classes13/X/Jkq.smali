.class public LX/Jkq;
.super Landroid/graphics/drawable/Drawable;
.source ""


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field public b:Lcom/facebook/user/tiles/UserTileDrawableController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:I

.field public d:LX/Jkp;

.field private final e:Landroid/graphics/Paint;

.field public f:I

.field public g:I

.field public h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2730348
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, LX/Jkq;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2730349
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2730350
    sget-object v0, LX/Jkp;->VISIBLE:LX/Jkp;

    iput-object v0, p0, LX/Jkq;->d:LX/Jkp;

    .line 2730351
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Jkq;->e:Landroid/graphics/Paint;

    .line 2730352
    return-void
.end method

.method public static k(LX/Jkq;)F
    .locals 4

    .prologue
    .line 2730353
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LX/Jkq;->h:J

    sub-long/2addr v0, v2

    .line 2730354
    long-to-float v0, v0

    const/high16 v1, 0x44160000    # 600.0f

    div-float/2addr v0, v1

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    .line 2730355
    sget-object v1, LX/Jkq;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const v5, 0x3f4ccccd    # 0.8f

    .line 2730356
    iget-object v0, p0, LX/Jkq;->d:LX/Jkp;

    sget-object v1, LX/Jkp;->GONE:LX/Jkp;

    if-ne v0, v1, :cond_1

    .line 2730357
    :cond_0
    :goto_0
    return-void

    .line 2730358
    :cond_1
    invoke-virtual {p0}, LX/Jkq;->e()F

    move-result v1

    .line 2730359
    invoke-virtual {p0}, LX/Jkq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2730360
    iget-object v0, p0, LX/Jkq;->d:LX/Jkp;

    sget-object v2, LX/Jkp;->ENTERING:LX/Jkp;

    if-ne v0, v2, :cond_3

    iget v0, p0, LX/Jkq;->f:I

    .line 2730361
    :goto_1
    iget-object v2, p0, LX/Jkq;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2730362
    iget v0, p0, LX/Jkq;->c:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v2, p0, LX/Jkq;->c:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, LX/Jkq;->c:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget-object v4, p0, LX/Jkq;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2730363
    :cond_2
    cmpl-float v0, v1, v5

    if-lez v0, :cond_0

    .line 2730364
    iget-object v0, p0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 2730365
    iget-object v2, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v2

    .line 2730366
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v5, v2}, LX/0yq;->c(FFF)F

    move-result v1

    .line 2730367
    const/4 v2, 0x0

    const/16 v3, 0xff

    .line 2730368
    sub-int v4, v3, v2

    int-to-float v4, v4

    mul-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/2addr v4, v2

    move v1, v4

    .line 2730369
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2730370
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, LX/Jkq;->e()F

    move-result v2

    sub-float/2addr v1, v2

    iget v2, p0, LX/Jkq;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 2730371
    iget v2, p0, LX/Jkq;->c:I

    sub-int/2addr v2, v1

    iget v3, p0, LX/Jkq;->c:I

    sub-int/2addr v3, v1

    invoke-virtual {p0, v1, v1, v2, v3}, LX/Jkq;->setBounds(IIII)V

    .line 2730372
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2730373
    :cond_3
    iget v0, p0, LX/Jkq;->g:I

    goto :goto_1
.end method

.method public final e()F
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2730374
    sget-object v1, LX/Jko;->a:[I

    iget-object v2, p0, LX/Jkq;->d:LX/Jkp;

    invoke-virtual {v2}, LX/Jkp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2730375
    :goto_0
    return v0

    .line 2730376
    :pswitch_0
    invoke-static {p0}, LX/Jkq;->k(LX/Jkq;)F

    move-result v0

    goto :goto_0

    .line 2730377
    :pswitch_1
    invoke-static {p0}, LX/Jkq;->k(LX/Jkq;)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_0

    .line 2730378
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 2730379
    sget-object v0, LX/Jko;->a:[I

    iget-object v1, p0, LX/Jkq;->d:LX/Jkp;

    invoke-virtual {v1}, LX/Jkp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2730380
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2730381
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2730382
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2730383
    iget-object v0, p0, LX/Jkq;->b:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 2730384
    iget-object p0, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, p0

    .line 2730385
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2730386
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 2730387
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 2730388
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
