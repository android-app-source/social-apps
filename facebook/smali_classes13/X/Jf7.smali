.class public LX/Jf7;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U",
        "<",
        "LX/Jf4;",
        ">;>;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Jex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jex",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/Jez;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721507
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2721508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jf7;->a:Ljava/util/ArrayList;

    .line 2721509
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2721510
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2721506
    iget-object v0, p0, LX/Jf7;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2721502
    sget-object v0, LX/Dfa;->V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM:LX/Dfa;

    invoke-virtual {v0}, LX/Dfa;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2721503
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030af8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Jf4;

    .line 2721504
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1

    .line 2721505
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;)V
    .locals 2

    .prologue
    .line 2721497
    check-cast p1, LX/62U;

    .line 2721498
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2721499
    iget-object v1, p0, LX/Jf7;->d:LX/Jez;

    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    check-cast v0, LX/Jf4;

    invoke-virtual {v0}, LX/Jf4;->getInboxUnitItem()Lcom/facebook/messaging/inbox2/items/InboxUnitItem;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Jez;->b(Lcom/facebook/messaging/inbox2/items/InboxUnitItem;)V

    .line 2721500
    :cond_0
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2721501
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2721489
    check-cast p1, LX/62U;

    .line 2721490
    iget-object v0, p0, LX/Jf7;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;

    .line 2721491
    iget-object v1, p1, LX/62U;->l:Landroid/view/View;

    check-cast v1, LX/Jf4;

    .line 2721492
    new-instance v2, LX/Jf6;

    invoke-direct {v2, p0, v0, p1}, LX/Jf6;-><init>(LX/Jf7;Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;LX/62U;)V

    invoke-virtual {v1, v2}, LX/Jf4;->setListener(LX/Jf6;)V

    .line 2721493
    iget-object v2, p0, LX/Jf7;->c:LX/0gc;

    invoke-virtual {v1, v2}, LX/Jf4;->setFragmentManager(LX/0gc;)V

    .line 2721494
    invoke-virtual {v1, v0}, LX/Jf4;->a(Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;)V

    .line 2721495
    iget-object v1, p0, LX/Jf7;->d:LX/Jez;

    invoke-virtual {v1, v0}, LX/Jez;->a(Lcom/facebook/messaging/inbox2/items/InboxUnitItem;)V

    .line 2721496
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2721487
    iget-object v0, p0, LX/Jf7;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->b()LX/Dfa;

    move-result-object v0

    invoke-virtual {v0}, LX/Dfa;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2721488
    iget-object v0, p0, LX/Jf7;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
