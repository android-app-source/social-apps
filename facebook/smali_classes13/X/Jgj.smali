.class public final enum LX/Jgj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jgj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jgj;

.field public static final enum AGGRESSIVE:LX/Jgj;

.field public static final enum BALANCED:LX/Jgj;

.field public static final enum CONSERVATIVE:LX/Jgj;


# instance fields
.field public final confidence:J

.field public final defaultConfidence:F

.field public final drawableId:I

.field public final modeValue:I

.field public final optionStringId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 2723746
    new-instance v0, LX/Jgj;

    const-string v1, "CONSERVATIVE"

    const v3, 0x7f080575

    const v5, 0x7f020fe4

    sget-wide v6, LX/0X5;->jY:J

    const v8, 0x3f666666    # 0.9f

    move v4, v2

    invoke-direct/range {v0 .. v8}, LX/Jgj;-><init>(Ljava/lang/String;IIIIJF)V

    sput-object v0, LX/Jgj;->CONSERVATIVE:LX/Jgj;

    .line 2723747
    new-instance v4, LX/Jgj;

    const-string v5, "BALANCED"

    const v7, 0x7f080576

    const v9, 0x7f020fe7

    sget-wide v10, LX/0X5;->jZ:J

    const v12, 0x3f333333    # 0.7f

    move v6, v13

    move v8, v13

    invoke-direct/range {v4 .. v12}, LX/Jgj;-><init>(Ljava/lang/String;IIIIJF)V

    sput-object v4, LX/Jgj;->BALANCED:LX/Jgj;

    .line 2723748
    new-instance v4, LX/Jgj;

    const-string v5, "AGGRESSIVE"

    const v7, 0x7f080577

    const v9, 0x7f020fe5

    sget-wide v10, LX/0X5;->jZ:J

    const/high16 v12, 0x3f000000    # 0.5f

    move v6, v14

    move v8, v14

    invoke-direct/range {v4 .. v12}, LX/Jgj;-><init>(Ljava/lang/String;IIIIJF)V

    sput-object v4, LX/Jgj;->AGGRESSIVE:LX/Jgj;

    .line 2723749
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jgj;

    sget-object v1, LX/Jgj;->CONSERVATIVE:LX/Jgj;

    aput-object v1, v0, v2

    sget-object v1, LX/Jgj;->BALANCED:LX/Jgj;

    aput-object v1, v0, v13

    sget-object v1, LX/Jgj;->AGGRESSIVE:LX/Jgj;

    aput-object v1, v0, v14

    sput-object v0, LX/Jgj;->$VALUES:[LX/Jgj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIJF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIJF)V"
        }
    .end annotation

    .prologue
    .line 2723739
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2723740
    iput p3, p0, LX/Jgj;->optionStringId:I

    .line 2723741
    iput p4, p0, LX/Jgj;->modeValue:I

    .line 2723742
    iput p5, p0, LX/Jgj;->drawableId:I

    .line 2723743
    iput-wide p6, p0, LX/Jgj;->confidence:J

    .line 2723744
    iput p8, p0, LX/Jgj;->defaultConfidence:F

    .line 2723745
    return-void
.end method

.method public static getSuggestionModeByModeValue(I)LX/Jgj;
    .locals 5

    .prologue
    .line 2723734
    invoke-static {}, LX/Jgj;->values()[LX/Jgj;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2723735
    iget v4, v0, LX/Jgj;->modeValue:I

    if-ne p0, v4, :cond_0

    .line 2723736
    :goto_1
    return-object v0

    .line 2723737
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2723738
    :cond_1
    sget-object v0, LX/Jgj;->BALANCED:LX/Jgj;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jgj;
    .locals 1

    .prologue
    .line 2723732
    const-class v0, LX/Jgj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jgj;

    return-object v0
.end method

.method public static values()[LX/Jgj;
    .locals 1

    .prologue
    .line 2723733
    sget-object v0, LX/Jgj;->$VALUES:[LX/Jgj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jgj;

    return-object v0
.end method
