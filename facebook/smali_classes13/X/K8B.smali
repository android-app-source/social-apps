.class public LX/K8B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;
.implements LX/CGs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ya;",
        "LX/CGs",
        "<",
        "LX/0zO",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/8bK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:J

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2773661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773662
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, LX/K8B;->e:J

    .line 2773663
    const-class v0, LX/K8B;

    invoke-static {v0, p0, p1}, LX/K8B;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2773664
    iput-object p1, p0, LX/K8B;->c:Landroid/content/Context;

    .line 2773665
    iput-object p2, p0, LX/K8B;->d:Ljava/util/List;

    .line 2773666
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/K8B;

    invoke-static {p0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v0

    check-cast v0, LX/8bK;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object v0, p1, LX/K8B;->a:LX/8bK;

    iput-object p0, p1, LX/K8B;->b:LX/0hB;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2773667
    new-instance v0, LX/K8N;

    invoke-direct {v0}, LX/K8N;-><init>()V

    move-object v0, v0

    .line 2773668
    const-string v1, "ids"

    iget-object v2, p0, LX/K8B;->d:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "deviceWidth"

    iget-object v3, p0, LX/K8B;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "deviceHeight"

    iget-object v3, p0, LX/K8B;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "playlist_scrubbing"

    const-string v3, "MPEG_DASH"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "preferred_quality"

    const-string v3, "HD"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "preferred_scrubbing"

    const-string v3, "MPEG_DASH"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    sget-object v3, LX/Cj6;->b:LX/0wC;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "video_cover_image_width"

    iget-object v3, p0, LX/K8B;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "video_cover_image_height"

    iget-object v3, p0, LX/K8B;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2773669
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    iget-wide v2, p0, LX/K8B;->e:J

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2773670
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2773671
    move-object v0, v0

    .line 2773672
    sget-object v1, LX/Cj6;->d:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/Cj6;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2773673
    return-object v0
.end method
