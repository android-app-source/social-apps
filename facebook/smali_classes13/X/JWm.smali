.class public final LX/JWm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JWn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

.field public c:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/3mj;

.field public final synthetic e:LX/JWn;


# direct methods
.method public constructor <init>(LX/JWn;)V
    .locals 1

    .prologue
    .line 2703444
    iput-object p1, p0, LX/JWm;->e:LX/JWn;

    .line 2703445
    move-object v0, p1

    .line 2703446
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2703447
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2703448
    const-string v0, "FutureFriendingPageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2703449
    if-ne p0, p1, :cond_1

    .line 2703450
    :cond_0
    :goto_0
    return v0

    .line 2703451
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2703452
    goto :goto_0

    .line 2703453
    :cond_3
    check-cast p1, LX/JWm;

    .line 2703454
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2703455
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2703456
    if-eq v2, v3, :cond_0

    .line 2703457
    iget-object v2, p0, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2703458
    goto :goto_0

    .line 2703459
    :cond_5
    iget-object v2, p1, LX/JWm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2703460
    :cond_6
    iget-object v2, p0, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v3, p1, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2703461
    goto :goto_0

    .line 2703462
    :cond_8
    iget-object v2, p1, LX/JWm;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    if-nez v2, :cond_7

    .line 2703463
    :cond_9
    iget-object v2, p0, LX/JWm;->c:LX/1Pc;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JWm;->c:LX/1Pc;

    iget-object v3, p1, LX/JWm;->c:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2703464
    goto :goto_0

    .line 2703465
    :cond_b
    iget-object v2, p1, LX/JWm;->c:LX/1Pc;

    if-nez v2, :cond_a

    .line 2703466
    :cond_c
    iget-object v2, p0, LX/JWm;->d:LX/3mj;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JWm;->d:LX/3mj;

    iget-object v3, p1, LX/JWm;->d:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2703467
    goto :goto_0

    .line 2703468
    :cond_d
    iget-object v2, p1, LX/JWm;->d:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
