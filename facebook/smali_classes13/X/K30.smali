.class public LX/K30;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/K2z;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2764875
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2764876
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/slideshow/SlideshowEditFragment;LX/0Px;Lcom/facebook/ipc/composer/model/ComposerSlideshowData;Ljava/lang/String;LX/0gc;)LX/K2z;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/slideshow/SlideshowEditFragment;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Lcom/facebook/ipc/composer/model/ComposerSlideshowData;",
            "Ljava/lang/String;",
            "LX/0gc;",
            ")",
            "LX/K2z;"
        }
    .end annotation

    .prologue
    .line 2764877
    new-instance v0, LX/K2z;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    const-class v1, LX/K3Q;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/K3Q;

    const-class v1, LX/K3L;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/K3L;

    const-class v1, LX/K3X;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/K3X;

    const-class v1, LX/K32;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/K32;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/Executor;

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v12}, LX/K2z;-><init>(Lcom/facebook/slideshow/SlideshowEditFragment;LX/0Px;Lcom/facebook/ipc/composer/model/ComposerSlideshowData;Ljava/lang/String;LX/0gc;LX/0ad;LX/K3Q;LX/K3L;LX/K3X;LX/K32;LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 2764878
    return-object v0
.end method
