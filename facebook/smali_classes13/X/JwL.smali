.class public final LX/JwL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/Cdn;",
        "LX/0bZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JwS;


# direct methods
.method public constructor <init>(LX/JwS;)V
    .locals 0

    .prologue
    .line 2751799
    iput-object p1, p0, LX/JwL;->a:LX/JwS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2751793
    check-cast p1, LX/Cdn;

    .line 2751794
    iget-object v0, p0, LX/JwL;->a:LX/JwS;

    iget-object v0, v0, LX/JwS;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JxB;

    .line 2751795
    iget-object v1, v0, LX/JxB;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Jx2;

    invoke-direct {v2, v0, p1}, LX/Jx2;-><init>(LX/JxB;LX/Cdn;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2751796
    new-instance v2, LX/Jx0;

    invoke-direct {v2, v0}, LX/Jx0;-><init>(LX/JxB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2751797
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 p0, 0x0

    aput-object v1, v2, p0

    const/4 p0, 0x1

    iget-object v1, v0, LX/JxB;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v1, v2, p0

    invoke-static {v2}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/Jx1;

    invoke-direct {v2, v0}, LX/Jx1;-><init>(LX/JxB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2751798
    return-object v0
.end method
