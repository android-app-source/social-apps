.class public final LX/Jj1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jj4;


# direct methods
.method public constructor <init>(LX/Jj4;)V
    .locals 0

    .prologue
    .line 2727217
    iput-object p1, p0, LX/Jj1;->a:LX/Jj4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5dedb884

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2727206
    iget-object v0, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v0, v0, LX/Jj4;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifg;

    iget-object v2, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v2, v2, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v3, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v3, v3, LX/Jj4;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2727207
    const-string p1, "event_reminder_banner_tapped"

    if-nez v2, :cond_0

    const/4 v5, 0x0

    :goto_0
    invoke-static {v0, p1, v5, v3, v2}, LX/Ifg;->a(LX/Ifg;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLightweightEventType;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 2727208
    iget-object v0, p0, LX/Jj1;->a:LX/Jj4;

    invoke-virtual {v0}, LX/Jj4;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v2, v2, LX/Jj4;->a:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v3, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v3, v3, LX/Jj4;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2727209
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727210
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727211
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727212
    new-instance v5, Landroid/content/Intent;

    const-class p1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {v5, v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p1, "thread_event_reminder_model_extra"

    invoke-virtual {v5, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    const-string p1, "thread_key_extra"

    invoke-virtual {v5, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    move-object v2, v5

    .line 2727213
    iget-object v0, p0, LX/Jj1;->a:LX/Jj4;

    iget-object v0, v0, LX/Jj4;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Jj1;->a:LX/Jj4;

    invoke-virtual {v3}, LX/Jj4;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2727214
    const v0, 0x3cde00ba    # 0.027099956f

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2727215
    :cond_0
    iget-object v5, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v5, v5

    .line 2727216
    goto :goto_0
.end method
