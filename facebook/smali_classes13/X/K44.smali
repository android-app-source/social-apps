.class public final LX/K44;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766377
    iput-object p1, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;B)V
    .locals 0

    .prologue
    .line 2766367
    invoke-direct {p0, p1}, LX/K44;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 2766368
    iget-object v0, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p1}, LX/K4o;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2766369
    iget-object v0, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p2, p3}, LX/K4o;->a(II)V

    .line 2766370
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 2766371
    iget-object v0, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    if-eqz v0, :cond_0

    .line 2766372
    iget-object v0, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->f()V

    .line 2766373
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 2766374
    iget-object v0, p0, LX/K44;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0, p2, p3}, LX/K4o;->a(II)V

    .line 2766375
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 2766376
    return-void
.end method
