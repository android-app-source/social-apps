.class public LX/K4c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:[F

.field public static final b:[S

.field private static final c:LX/K4y;


# instance fields
.field public final d:Lcom/facebook/storyline/renderer/TextureLoader;

.field public e:LX/K57;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/K57;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768786
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, LX/K4c;->a:[F

    .line 2768787
    const/4 v0, 0x6

    new-array v0, v0, [S

    fill-array-data v0, :array_1

    sput-object v0, LX/K4c;->b:[S

    .line 2768788
    new-instance v0, LX/K4y;

    invoke-direct {v0}, LX/K4y;-><init>()V

    sput-object v0, LX/K4c;->c:LX/K4y;

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
    .end array-data

    .line 2768789
    :array_1
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x0s
        0x2s
        0x3s
    .end array-data
.end method

.method public constructor <init>(Lcom/facebook/storyline/renderer/TextureLoader;)V
    .locals 0

    .prologue
    .line 2768790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2768791
    iput-object p1, p0, LX/K4c;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    .line 2768792
    return-void
.end method

.method private a(LX/K50;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/K50;",
            "Ljava/util/List",
            "<",
            "LX/K54;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x1406

    const/16 v4, 0x14

    const/4 v3, 0x0

    .line 2768793
    new-instance v0, LX/K4y;

    .line 2768794
    iget-object v1, p1, LX/K4z;->a:LX/K4y;

    move-object v1, v1

    .line 2768795
    invoke-direct {v0, v1}, LX/K4y;-><init>(LX/K4y;)V

    .line 2768796
    const/16 v12, 0x10

    const/4 v5, 0x1

    const/4 v11, 0x4

    const/4 v1, 0x0

    .line 2768797
    new-array v8, v12, [F

    move v7, v1

    .line 2768798
    :goto_0
    if-ge v7, v11, :cond_1

    move v6, v1

    .line 2768799
    :goto_1
    if-ge v6, v11, :cond_0

    .line 2768800
    mul-int/lit8 v9, v7, 0x4

    add-int/2addr v9, v6

    invoke-static {v0, v7, v6}, LX/K4y;->b(LX/K4y;II)F

    move-result v10

    aput v10, v8, v9

    .line 2768801
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2768802
    :cond_0
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 2768803
    :cond_1
    iget-object v6, v0, LX/K4y;->a:[F

    aget v6, v6, v1

    aget v7, v8, v1

    mul-float/2addr v6, v7

    iget-object v7, v0, LX/K4y;->a:[F

    aget v7, v7, v11

    aget v9, v8, v5

    mul-float/2addr v7, v9

    add-float/2addr v6, v7

    iget-object v7, v0, LX/K4y;->a:[F

    const/16 v9, 0x8

    aget v7, v7, v9

    const/4 v9, 0x2

    aget v9, v8, v9

    mul-float/2addr v7, v9

    add-float/2addr v6, v7

    iget-object v7, v0, LX/K4y;->a:[F

    const/16 v9, 0xc

    aget v7, v7, v9

    const/4 v9, 0x3

    aget v9, v8, v9

    mul-float/2addr v7, v9

    add-float/2addr v6, v7

    .line 2768804
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const v9, 0x358637bd    # 1.0E-6f

    cmpg-float v7, v7, v9

    if-gez v7, :cond_c

    .line 2768805
    :goto_2
    new-instance v8, LX/K4y;

    invoke-direct {v8}, LX/K4y;-><init>()V

    .line 2768806
    iget-object v1, p1, LX/K50;->a:LX/K4y;

    move-object v1, v1

    .line 2768807
    invoke-virtual {v8, v1, v0}, LX/K4y;->a(LX/K4y;LX/K4y;)V

    .line 2768808
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/K54;

    .line 2768809
    iget v0, v6, LX/K54;->n:I

    move v0, v0

    .line 2768810
    iget v1, p1, LX/K50;->h:I

    move v1, v1

    .line 2768811
    shr-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 2768812
    iget-boolean v0, v6, LX/K54;->j:Z

    move v0, v0

    .line 2768813
    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthMask(Z)V

    .line 2768814
    iget-object v0, v6, LX/K54;->b:LX/K4v;

    move-object v0, v0

    .line 2768815
    const/16 p2, 0xbe2

    const/16 v12, 0x303

    const/4 v11, 0x0

    const v10, 0x8006

    const/4 v7, 0x1

    .line 2768816
    sget-object v1, LX/K4w;->a:[I

    invoke-virtual {v0}, LX/K4v;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_0

    .line 2768817
    invoke-static {v7, v11}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768818
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    .line 2768819
    :goto_4
    sget-object v1, LX/K4v;->NORMAL:LX/K4v;

    if-ne v0, v1, :cond_e

    .line 2768820
    invoke-static {p2}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 2768821
    :goto_5
    iget-object v0, v6, LX/K54;->c:LX/K5C;

    move-object v0, v0

    .line 2768822
    const/16 v1, 0xb71

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 2768823
    sget-object v1, LX/K4w;->b:[I

    invoke-virtual {v0}, LX/K5C;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_1

    .line 2768824
    const/16 v1, 0x201

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 2768825
    :goto_6
    iget v0, v6, LX/K54;->g:I

    move v0, v0

    .line 2768826
    if-lez v0, :cond_5

    .line 2768827
    iget v0, v6, LX/K54;->g:I

    move v7, v0

    .line 2768828
    const-string v0, "aPosition"

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v5

    .line 2768829
    const-string v0, "aTextureCoord"

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    .line 2768830
    const-string v0, "uMVPMatrix"

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    move v11, v0

    move v0, v5

    move v5, v7

    move v7, v1

    move v1, v11

    .line 2768831
    :goto_7
    iput v5, v6, LX/K54;->g:I

    .line 2768832
    invoke-static {v5}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 2768833
    new-instance v5, LX/K4y;

    invoke-direct {v5}, LX/K4y;-><init>()V

    .line 2768834
    iget-object v10, v6, LX/K54;->a:LX/K4y;

    move-object v10, v10

    .line 2768835
    invoke-virtual {v5, v8, v10}, LX/K4y;->a(LX/K4y;LX/K4y;)V

    .line 2768836
    const/4 v10, 0x1

    .line 2768837
    iget-object v11, v5, LX/K4y;->a:[F

    move-object v5, v11

    .line 2768838
    invoke-static {v1, v10, v3, v5, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 2768839
    invoke-static {v6}, LX/K4c;->a(LX/K54;)V

    .line 2768840
    invoke-static {p0, v6}, LX/K4c;->b(LX/K4c;LX/K54;)V

    .line 2768841
    const v5, 0x8892

    .line 2768842
    iget v1, v6, LX/K54;->d:I

    move v1, v1

    .line 2768843
    if-lez v1, :cond_8

    .line 2768844
    iget v1, v6, LX/K54;->d:I

    move v1, v1

    .line 2768845
    :goto_8
    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 2768846
    if-ltz v0, :cond_3

    .line 2768847
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 2768848
    const/4 v1, 0x3

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 2768849
    :cond_3
    if-ltz v7, :cond_4

    .line 2768850
    invoke-static {v7}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 2768851
    const/4 v1, 0x2

    const/16 v5, 0xc

    move v0, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 2768852
    :cond_4
    const v1, 0x8893

    .line 2768853
    iget v0, v6, LX/K54;->e:I

    move v0, v0

    .line 2768854
    if-lez v0, :cond_9

    .line 2768855
    iget v0, v6, LX/K54;->e:I

    move v0, v0

    .line 2768856
    :goto_9
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 2768857
    const/4 v1, 0x4

    .line 2768858
    iget v0, v6, LX/K54;->f:I

    move v0, v0

    .line 2768859
    if-lez v0, :cond_a

    .line 2768860
    iget v0, v6, LX/K54;->f:I

    move v0, v0

    .line 2768861
    :goto_a
    const/16 v5, 0x1403

    invoke-static {v1, v0, v5, v3}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    goto/16 :goto_3

    .line 2768862
    :cond_5
    iget-object v0, p0, LX/K4c;->g:LX/0P1;

    .line 2768863
    iget-object v1, v6, LX/K54;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2768864
    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2768865
    if-eqz v0, :cond_6

    .line 2768866
    iput-object v0, v6, LX/K54;->h:Ljava/lang/String;

    .line 2768867
    :cond_6
    iget-object v0, p0, LX/K4c;->g:LX/0P1;

    .line 2768868
    iget-object v1, v6, LX/K54;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2768869
    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2768870
    if-eqz v0, :cond_7

    .line 2768871
    iput-object v0, v6, LX/K54;->i:Ljava/lang/String;

    .line 2768872
    :cond_7
    iget-object v0, v6, LX/K54;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2768873
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2768874
    iget-object v0, v6, LX/K54;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2768875
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2768876
    iget-object v0, p0, LX/K4c;->e:LX/K57;

    .line 2768877
    :goto_b
    move-object v0, v0

    .line 2768878
    iget v1, v0, LX/K57;->f:I

    move v7, v1

    .line 2768879
    iget v1, v0, LX/K57;->c:I

    move v5, v1

    .line 2768880
    iget v1, v0, LX/K57;->d:I

    move v1, v1

    .line 2768881
    iget v10, v0, LX/K57;->e:I

    move v0, v10

    .line 2768882
    move v11, v0

    move v0, v5

    move v5, v7

    move v7, v1

    move v1, v11

    goto/16 :goto_7

    .line 2768883
    :cond_8
    iget v1, p0, LX/K4c;->i:I

    goto :goto_8

    .line 2768884
    :cond_9
    iget v0, p0, LX/K4c;->j:I

    goto :goto_9

    .line 2768885
    :cond_a
    sget-object v0, LX/K4c;->b:[S

    array-length v0, v0

    goto :goto_a

    .line 2768886
    :cond_b
    return-void

    .line 2768887
    :cond_c
    const/high16 v7, 0x3f800000    # 1.0f

    div-float v6, v7, v6

    .line 2768888
    :goto_c
    if-ge v1, v12, :cond_d

    .line 2768889
    iget-object v7, v0, LX/K4y;->a:[F

    aget v9, v8, v1

    mul-float/2addr v9, v6

    aput v9, v7, v1

    .line 2768890
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 2768891
    :cond_d
    goto/16 :goto_2

    .line 2768892
    :pswitch_0
    const/16 v1, 0x302

    invoke-static {v1, v12}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768893
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768894
    :pswitch_1
    const/16 v1, 0x306

    invoke-static {v1, v11}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768895
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768896
    :pswitch_2
    const/16 v1, 0x301

    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768897
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768898
    :pswitch_3
    invoke-static {v7, v7}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768899
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768900
    :pswitch_4
    invoke-static {v7, v7}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768901
    const v1, 0x800a

    invoke-static {v1}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768902
    :pswitch_5
    invoke-static {v7, v7}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768903
    const v1, 0x800b

    invoke-static {v1}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768904
    :pswitch_6
    invoke-static {v7, v12}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 2768905
    invoke-static {v10}, Landroid/opengl/GLES20;->glBlendEquation(I)V

    goto/16 :goto_4

    .line 2768906
    :cond_e
    invoke-static {p2}, Landroid/opengl/GLES20;->glEnable(I)V

    goto/16 :goto_5

    .line 2768907
    :pswitch_7
    const/16 v1, 0x203

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768908
    :pswitch_8
    const/16 v1, 0x204

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768909
    :pswitch_9
    const/16 v1, 0x206

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768910
    :pswitch_a
    const/16 v1, 0x202

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768911
    :pswitch_b
    const/16 v1, 0x205

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768912
    :pswitch_c
    const/16 v1, 0x207

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768913
    :pswitch_d
    const/16 v1, 0x200

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto/16 :goto_6

    .line 2768914
    :cond_f
    iget-object v0, p0, LX/K4c;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K57;

    .line 2768915
    iget-object v5, v0, LX/K57;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2768916
    iget-object v7, v6, LX/K54;->h:Ljava/lang/String;

    move-object v7, v7

    .line 2768917
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2768918
    iget-object v5, v0, LX/K57;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2768919
    iget-object v7, v6, LX/K54;->i:Ljava/lang/String;

    move-object v7, v7

    .line 2768920
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    goto/16 :goto_b

    .line 2768921
    :cond_11
    new-instance v0, LX/K57;

    invoke-direct {v0}, LX/K57;-><init>()V

    .line 2768922
    iget-object v1, v6, LX/K54;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2768923
    iput-object v1, v0, LX/K57;->a:Ljava/lang/String;

    .line 2768924
    iget-object v1, v6, LX/K54;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2768925
    iput-object v1, v0, LX/K57;->b:Ljava/lang/String;

    .line 2768926
    invoke-static {v0}, LX/K4x;->a(LX/K57;)V

    .line 2768927
    iget-object v1, p0, LX/K4c;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static a(LX/K54;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2768928
    iget-object v0, p0, LX/K54;->k:Ljava/util/Map;

    move-object v1, v0

    .line 2768929
    if-nez v1, :cond_1

    .line 2768930
    :cond_0
    return-void

    .line 2768931
    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2768932
    iget v3, p0, LX/K54;->g:I

    move v3, v3

    .line 2768933
    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v3

    .line 2768934
    if-ltz v3, :cond_2

    .line 2768935
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 2768936
    array-length v4, v0

    sparse-switch v4, :sswitch_data_0

    .line 2768937
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uniform length is illegal"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2768938
    :sswitch_0
    invoke-static {v3, v6, v0, v5}, Landroid/opengl/GLES20;->glUniform1fv(II[FI)V

    goto :goto_0

    .line 2768939
    :sswitch_1
    invoke-static {v3, v6, v0, v5}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    goto :goto_0

    .line 2768940
    :sswitch_2
    invoke-static {v3, v6, v0, v5}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    goto :goto_0

    .line 2768941
    :sswitch_3
    invoke-static {v3, v6, v0, v5}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    goto :goto_0

    .line 2768942
    :sswitch_4
    invoke-static {v3, v6, v5, v0, v5}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    goto :goto_0

    .line 2768943
    :sswitch_5
    invoke-static {v3, v6, v5, v0, v5}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x9 -> :sswitch_4
        0x10 -> :sswitch_5
    .end sparse-switch
.end method

.method private static b(LX/K4c;LX/K54;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2768944
    iget-object v0, p1, LX/K54;->o:LX/K59;

    move-object v0, v0

    .line 2768945
    if-eqz v0, :cond_2

    .line 2768946
    const/4 v5, 0x1

    .line 2768947
    iget-object v0, p0, LX/K4c;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    .line 2768948
    iget-object v1, p1, LX/K54;->o:LX/K59;

    move-object v1, v1

    .line 2768949
    const p0, 0x812f

    const/16 v8, 0x2601

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xde1

    .line 2768950
    iget-object v2, v0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 2768951
    if-eqz v2, :cond_7

    .line 2768952
    :goto_0
    move-object v1, v2

    .line 2768953
    const v2, 0x84c0

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2768954
    iget v3, p1, LX/K54;->g:I

    move v3, v3

    .line 2768955
    const-string v4, "sTexture0"

    invoke-static {v2, v0, v3, v4}, LX/K4x;->a(IIILjava/lang/String;)V

    .line 2768956
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [F

    .line 2768957
    iget v1, p1, LX/K54;->g:I

    move v1, v1

    .line 2768958
    const-string v2, "sTexture0_dim"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    .line 2768959
    if-ltz v1, :cond_0

    .line 2768960
    const/4 v2, 0x0

    invoke-static {v1, v5, v0, v2}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    .line 2768961
    :cond_0
    iget v1, p1, LX/K54;->g:I

    move v1, v1

    .line 2768962
    const-string v2, "uTextLayout"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    .line 2768963
    if-ltz v1, :cond_1

    .line 2768964
    const/4 v2, 0x2

    invoke-static {v1, v5, v0, v2}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    .line 2768965
    :cond_1
    return-void

    .line 2768966
    :cond_2
    iget-object v0, p1, LX/K54;->l:Ljava/util/List;

    move-object v6, v0

    .line 2768967
    if-eqz v6, :cond_1

    move v2, v3

    .line 2768968
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2768969
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2768970
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 2768971
    const-string v4, "moodAsset:"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2768972
    iget-object v4, p0, LX/K4c;->h:LX/0P1;

    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2768973
    :cond_3
    iget-object v4, p0, LX/K4c;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    .line 2768974
    iget-object v5, v4, Lcom/facebook/storyline/renderer/TextureLoader;->c:LX/K4g;

    invoke-virtual {v5}, LX/K4g;->a()V

    .line 2768975
    iget-object v5, v4, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/K4t;

    .line 2768976
    if-eqz v5, :cond_4

    iget-object v7, v5, LX/K4t;->a:LX/K5A;

    if-nez v7, :cond_8

    :cond_4
    const/4 v5, 0x0

    :goto_2
    move-object v7, v5

    .line 2768977
    if-eqz v7, :cond_6

    .line 2768978
    const v4, 0x84c0

    .line 2768979
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const/4 v5, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_5
    move v1, v5

    :goto_3
    packed-switch v1, :pswitch_data_1

    .line 2768980
    :goto_4
    iget v5, v7, LX/K5A;->b:I

    .line 2768981
    iget v1, p1, LX/K54;->g:I

    move v8, v1

    .line 2768982
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v5, v8, v1}, LX/K4x;->a(IIILjava/lang/String;)V

    .line 2768983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2768984
    iget v1, p1, LX/K54;->g:I

    move v1, v1

    .line 2768985
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 2768986
    if-ltz v0, :cond_6

    .line 2768987
    iget v1, v7, LX/K5A;->c:I

    int-to-float v1, v1

    iget v4, v7, LX/K5A;->d:I

    int-to-float v4, v4

    invoke-static {v0, v1, v4}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 2768988
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 2768989
    :pswitch_0
    const-string v8, "sTexture1"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    goto :goto_3

    :pswitch_1
    const-string v8, "sTexture2"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    goto :goto_3

    :pswitch_2
    const-string v8, "sTexture3"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x2

    goto :goto_3

    :pswitch_3
    const-string v8, "sTexture4"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x3

    goto :goto_3

    :pswitch_4
    const-string v8, "sTexture5"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x4

    goto :goto_3

    .line 2768990
    :pswitch_5
    const v1, 0x84c1

    move v4, v1

    .line 2768991
    goto :goto_4

    .line 2768992
    :pswitch_6
    const v1, 0x84c2

    move v4, v1

    .line 2768993
    goto :goto_4

    .line 2768994
    :pswitch_7
    const v1, 0x84c3

    move v4, v1

    .line 2768995
    goto :goto_4

    .line 2768996
    :pswitch_8
    const v1, 0x84c4

    move v4, v1

    .line 2768997
    goto :goto_4

    .line 2768998
    :pswitch_9
    const v1, 0x84c5

    move v4, v1

    goto/16 :goto_4

    .line 2768999
    :cond_7
    new-array v3, v4, [I

    .line 2769000
    invoke-static {v4, v3, v7}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 2769001
    aget v2, v3, v7

    invoke-static {v6, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 2769002
    const/16 v2, 0x2801

    invoke-static {v6, v2, v8}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769003
    const/16 v2, 0x2800

    invoke-static {v6, v2, v8}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769004
    const/16 v2, 0x2802

    invoke-static {v6, v2, p0}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769005
    const/16 v2, 0x2803

    invoke-static {v6, v2, p0}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 2769006
    invoke-static {v1}, LX/K56;->b(LX/K59;)Landroid/util/Pair;

    move-result-object v4

    .line 2769007
    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v6, v7, v2, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 2769008
    invoke-static {v6, v7}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 2769009
    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 2769010
    aget v2, v3, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 2769011
    iget-object v3, v0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_8
    iget-object v5, v5, LX/K4t;->a:LX/K5A;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x62d51009
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/K51;II)V
    .locals 8

    .prologue
    .line 2769012
    iget-object v0, p1, LX/K51;->a:Ljava/util/List;

    move-object v0, v0

    .line 2769013
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2769014
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2769015
    iget-object v0, p1, LX/K51;->b:LX/K4z;

    move-object v0, v0

    .line 2769016
    sget-object v2, LX/K4c;->c:LX/K4y;

    invoke-virtual {v0, v1, v2}, LX/K4z;->a(Ljava/util/List;LX/K4y;)V

    .line 2769017
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2769018
    iget-object v0, p1, LX/K51;->a:Ljava/util/List;

    move-object v0, v0

    .line 2769019
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K50;

    .line 2769020
    iget-object v3, v0, LX/K50;->b:LX/K5B;

    move-object v3, v3

    .line 2769021
    iget v4, v3, LX/K5B;->a:F

    move v4, v4

    .line 2769022
    int-to-float v5, p2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 2769023
    iget v5, v3, LX/K5B;->b:F

    move v5, v5

    .line 2769024
    int-to-float v6, p3

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 2769025
    iget v6, v3, LX/K5B;->c:F

    move v6, v6

    .line 2769026
    int-to-float v7, p2

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 2769027
    iget v7, v3, LX/K5B;->d:F

    move v7, v7

    .line 2769028
    int-to-float p1, p3

    mul-float/2addr v7, p1

    float-to-int v7, v7

    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 2769029
    iget v4, v3, LX/K5B;->a:F

    move v4, v4

    .line 2769030
    int-to-float v5, p2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 2769031
    iget v5, v3, LX/K5B;->b:F

    move v5, v5

    .line 2769032
    int-to-float v6, p3

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 2769033
    iget v6, v3, LX/K5B;->c:F

    move v6, v6

    .line 2769034
    int-to-float v7, p2

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 2769035
    iget v7, v3, LX/K5B;->d:F

    move v3, v7

    .line 2769036
    int-to-float v7, p3

    mul-float/2addr v3, v7

    float-to-int v3, v3

    invoke-static {v4, v5, v6, v3}, Landroid/opengl/GLES20;->glScissor(IIII)V

    .line 2769037
    const/16 v3, 0xc11

    invoke-static {v3}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 2769038
    iget-object v3, v0, LX/K50;->d:LX/K5B;

    move-object v3, v3

    .line 2769039
    iget v4, v3, LX/K5B;->a:F

    move v4, v4

    .line 2769040
    iget v5, v3, LX/K5B;->b:F

    move v5, v5

    .line 2769041
    iget v6, v3, LX/K5B;->c:F

    move v6, v6

    .line 2769042
    iget v7, v3, LX/K5B;->d:F

    move v3, v7

    .line 2769043
    invoke-static {v4, v5, v6, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 2769044
    iget v3, v0, LX/K50;->e:F

    move v3, v3

    .line 2769045
    invoke-static {v3}, Landroid/opengl/GLES20;->glClearDepthf(F)V

    .line 2769046
    const/4 v3, 0x0

    .line 2769047
    iget-boolean v4, v0, LX/K50;->g:Z

    move v4, v4

    .line 2769048
    if-eqz v4, :cond_0

    .line 2769049
    const/16 v3, 0x4000

    .line 2769050
    :cond_0
    iget-boolean v4, v0, LX/K50;->f:Z

    move v4, v4

    .line 2769051
    if-eqz v4, :cond_1

    .line 2769052
    or-int/lit16 v3, v3, 0x100

    .line 2769053
    :cond_1
    invoke-static {v3}, Landroid/opengl/GLES20;->glClear(I)V

    .line 2769054
    iget-object v3, v0, LX/K50;->c:LX/K5B;

    move-object v3, v3

    .line 2769055
    iget v4, v3, LX/K5B;->a:F

    move v4, v4

    .line 2769056
    int-to-float v5, p2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 2769057
    iget v5, v3, LX/K5B;->b:F

    move v5, v5

    .line 2769058
    int-to-float v6, p3

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 2769059
    iget v6, v3, LX/K5B;->c:F

    move v6, v6

    .line 2769060
    int-to-float v7, p2

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 2769061
    iget v7, v3, LX/K5B;->d:F

    move v3, v7

    .line 2769062
    int-to-float v7, p3

    mul-float/2addr v3, v7

    float-to-int v3, v3

    invoke-static {v4, v5, v6, v3}, Landroid/opengl/GLES20;->glScissor(IIII)V

    .line 2769063
    invoke-direct {p0, v0, v1}, LX/K4c;->a(LX/K50;Ljava/util/List;)V

    goto/16 :goto_0

    .line 2769064
    :cond_2
    return-void
.end method
