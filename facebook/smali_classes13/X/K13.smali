.class public abstract LX/K13;
.super Landroid/text/style/ReplacementSpan;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2760945
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    return-void
.end method

.method public static a(Landroid/text/Spannable;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2760939
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v2, LX/K13;

    invoke-interface {p0, v1, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760940
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2760941
    invoke-virtual {v3}, LX/K13;->d()V

    .line 2760942
    invoke-virtual {v3, p1}, LX/K13;->a(Landroid/widget/TextView;)V

    .line 2760943
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760944
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(Landroid/widget/TextView;)V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method
