.class public LX/KA9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:Landroid/content/pm/PackageManager;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/KA8;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/KAA;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "LX/KA8;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/KAA;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/wearlistener/WearNodeListener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2778979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2778980
    iput-object p1, p0, LX/KA9;->a:Landroid/content/pm/PackageManager;

    .line 2778981
    iput-object p2, p0, LX/KA9;->b:Ljava/lang/String;

    .line 2778982
    iput-object p3, p0, LX/KA9;->c:Ljava/util/Set;

    .line 2778983
    iput-object p4, p0, LX/KA9;->d:Ljava/util/Set;

    .line 2778984
    iput-object p5, p0, LX/KA9;->e:Ljava/util/Set;

    .line 2778985
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2778986
    iget-object v0, p0, LX/KA9;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KA9;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/KA9;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2778987
    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    .line 2778988
    :goto_1
    iget-object v2, p0, LX/KA9;->a:Landroid/content/pm/PackageManager;

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, LX/KA9;->b:Ljava/lang/String;

    const-class v5, Lcom/facebook/wearlistener/DataLayerListenerService;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 2778989
    return-void

    .line 2778990
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2778991
    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method
