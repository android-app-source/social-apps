.class public LX/Jrt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Jrt;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2744900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2744901
    return-void
.end method

.method public static a(LX/0QB;)LX/Jrt;
    .locals 5

    .prologue
    .line 2744902
    sget-object v0, LX/Jrt;->c:LX/Jrt;

    if-nez v0, :cond_1

    .line 2744903
    const-class v1, LX/Jrt;

    monitor-enter v1

    .line 2744904
    :try_start_0
    sget-object v0, LX/Jrt;->c:LX/Jrt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2744905
    if-eqz v2, :cond_0

    .line 2744906
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2744907
    new-instance p0, LX/Jrt;

    invoke-direct {p0}, LX/Jrt;-><init>()V

    .line 2744908
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    .line 2744909
    iput-object v3, p0, LX/Jrt;->a:LX/0Uh;

    iput-object v4, p0, LX/Jrt;->b:LX/0ad;

    .line 2744910
    move-object v0, p0

    .line 2744911
    sput-object v0, LX/Jrt;->c:LX/Jrt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2744912
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2744913
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2744914
    :cond_1
    sget-object v0, LX/Jrt;->c:LX/Jrt;

    return-object v0

    .line 2744915
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2744916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2744917
    sget-object v0, LX/0c1;->Off:LX/0c1;

    .line 2744918
    iget-object v3, p0, LX/Jrt;->b:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-short v5, LX/Jrs;->a:S

    const/4 v6, 0x0

    invoke-interface {v3, v4, v0, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    move v0, v3

    .line 2744919
    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
