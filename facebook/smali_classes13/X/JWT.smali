.class public final enum LX/JWT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JWT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JWT;

.field public static final enum ADD_FRIEND:LX/JWT;

.field public static final enum REMOVE:LX/JWT;

.field public static final enum UNDO:LX/JWT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2702998
    new-instance v0, LX/JWT;

    const-string v1, "ADD_FRIEND"

    invoke-direct {v0, v1, v2}, LX/JWT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JWT;->ADD_FRIEND:LX/JWT;

    .line 2702999
    new-instance v0, LX/JWT;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3}, LX/JWT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JWT;->REMOVE:LX/JWT;

    .line 2703000
    new-instance v0, LX/JWT;

    const-string v1, "UNDO"

    invoke-direct {v0, v1, v4}, LX/JWT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JWT;->UNDO:LX/JWT;

    .line 2703001
    const/4 v0, 0x3

    new-array v0, v0, [LX/JWT;

    sget-object v1, LX/JWT;->ADD_FRIEND:LX/JWT;

    aput-object v1, v0, v2

    sget-object v1, LX/JWT;->REMOVE:LX/JWT;

    aput-object v1, v0, v3

    sget-object v1, LX/JWT;->UNDO:LX/JWT;

    aput-object v1, v0, v4

    sput-object v0, LX/JWT;->$VALUES:[LX/JWT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2703002
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JWT;
    .locals 1

    .prologue
    .line 2703003
    const-class v0, LX/JWT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JWT;

    return-object v0
.end method

.method public static values()[LX/JWT;
    .locals 1

    .prologue
    .line 2703004
    sget-object v0, LX/JWT;->$VALUES:[LX/JWT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JWT;

    return-object v0
.end method
