.class public LX/Jso;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Jsn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Jmz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2745794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745795
    return-void
.end method

.method public static a(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V
    .locals 1

    .prologue
    .line 2745796
    invoke-interface {p1}, LX/5St;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2745797
    invoke-interface {p1}, LX/5St;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 2745798
    return-void

    .line 2745799
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745800
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2745801
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2745802
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2745803
    :goto_0
    return-void

    .line 2745804
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Jso;
    .locals 7

    .prologue
    .line 2745805
    new-instance v2, LX/Jso;

    invoke-direct {v2}, LX/Jso;-><init>()V

    .line 2745806
    new-instance v0, LX/Jsn;

    invoke-direct {v0}, LX/Jsn;-><init>()V

    .line 2745807
    const/16 v1, 0x27b3

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v3, 0x27b2

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x27b4

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v5

    .line 2745808
    iput-object v1, v0, LX/Jsn;->a:LX/0Ot;

    iput-object v3, v0, LX/Jsn;->b:LX/0Ot;

    iput-object v4, v0, LX/Jsn;->c:LX/0Ot;

    iput-object v5, v0, LX/Jsn;->d:LX/0Ot;

    .line 2745809
    move-object v0, v0

    .line 2745810
    check-cast v0, LX/Jsn;

    .line 2745811
    new-instance v1, LX/Jmz;

    invoke-direct {v1}, LX/Jmz;-><init>()V

    .line 2745812
    const/16 v3, 0x27b3

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x27b2

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x27b4

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v6

    .line 2745813
    iput-object v3, v1, LX/Jmz;->a:LX/0Ot;

    iput-object v4, v1, LX/Jmz;->b:LX/0Ot;

    iput-object v5, v1, LX/Jmz;->c:LX/0Ot;

    iput-object v6, v1, LX/Jmz;->d:LX/0Ot;

    .line 2745814
    move-object v1, v1

    .line 2745815
    check-cast v1, LX/Jmz;

    .line 2745816
    iput-object v0, v2, LX/Jso;->a:LX/Jsn;

    iput-object v1, v2, LX/Jso;->b:LX/Jmz;

    .line 2745817
    return-object v2
.end method

.method public static b(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;",
            ">;",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2745818
    iget-object v0, p1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2745819
    invoke-virtual {p0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    iget-object v1, p1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    iget-object v2, p1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->a(Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V

    .line 2745820
    invoke-virtual {p0}, LX/4ob;->e()V

    .line 2745821
    :goto_0
    return-void

    .line 2745822
    :cond_0
    invoke-virtual {p0}, LX/4ob;->d()V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/widget/text/BetterTextView;LX/5St;)V
    .locals 1

    .prologue
    .line 2745823
    invoke-interface {p1}, LX/5St;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/Jso;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2745824
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/text/BetterTextView;Landroid/widget/ImageView;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V
    .locals 1

    .prologue
    .line 2745825
    iget-object v0, p0, LX/Jso;->a:LX/Jsn;

    .line 2745826
    iget-object p0, p3, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->k:Ljava/lang/String;

    invoke-virtual {p1, p0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2745827
    invoke-static {v0, p3}, LX/Jsn;->a(LX/Jsn;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2745828
    invoke-static {v0, p3}, LX/Jsn;->b(LX/Jsn;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)V

    .line 2745829
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2745830
    :goto_0
    return-void

    .line 2745831
    :cond_0
    const/16 p0, 0x8

    invoke-virtual {p2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/4ob;Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2745832
    iget-object v0, p2, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->k:Ljava/lang/String;

    .line 2745833
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2745834
    invoke-virtual {p1}, LX/4ob;->d()V

    .line 2745835
    const/4 p2, 0x0

    .line 2745836
    :goto_0
    move v0, p2

    .line 2745837
    move v0, v0

    .line 2745838
    return v0

    .line 2745839
    :cond_0
    invoke-virtual {p1}, LX/4ob;->e()V

    .line 2745840
    const/4 p2, 0x1

    goto :goto_0
.end method
