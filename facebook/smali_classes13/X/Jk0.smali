.class public final enum LX/Jk0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jk0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jk0;

.field public static final enum ABSOLUTE:LX/Jk0;

.field public static final enum RELATIVE:LX/Jk0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2728672
    new-instance v0, LX/Jk0;

    const-string v1, "ABSOLUTE"

    invoke-direct {v0, v1, v2}, LX/Jk0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk0;->ABSOLUTE:LX/Jk0;

    .line 2728673
    new-instance v0, LX/Jk0;

    const-string v1, "RELATIVE"

    invoke-direct {v0, v1, v3}, LX/Jk0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jk0;->RELATIVE:LX/Jk0;

    .line 2728674
    const/4 v0, 0x2

    new-array v0, v0, [LX/Jk0;

    sget-object v1, LX/Jk0;->ABSOLUTE:LX/Jk0;

    aput-object v1, v0, v2

    sget-object v1, LX/Jk0;->RELATIVE:LX/Jk0;

    aput-object v1, v0, v3

    sput-object v0, LX/Jk0;->$VALUES:[LX/Jk0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2728669
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jk0;
    .locals 1

    .prologue
    .line 2728670
    const-class v0, LX/Jk0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jk0;

    return-object v0
.end method

.method public static values()[LX/Jk0;
    .locals 1

    .prologue
    .line 2728671
    sget-object v0, LX/Jk0;->$VALUES:[LX/Jk0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jk0;

    return-object v0
.end method
