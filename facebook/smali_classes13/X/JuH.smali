.class public LX/JuH;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/JuF;",
        ">;",
        "Lcom/facebook/notes/view/block/UFIBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:LX/CnR;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2748651
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2748652
    const-class v0, LX/JuH;

    invoke-static {v0, p0}, LX/JuH;->a(Ljava/lang/Class;LX/02k;)V

    .line 2748653
    check-cast p1, LX/CnR;

    iput-object p1, p0, LX/JuH;->b:LX/CnR;

    .line 2748654
    iget-object v0, p0, LX/JuH;->b:LX/CnR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CnR;->setIsOverlay(Z)V

    .line 2748655
    iget-object v0, p0, LX/JuH;->a:LX/Ck0;

    iget-object v1, p0, LX/JuH;->b:LX/CnR;

    invoke-virtual {v0, v1}, LX/Ck0;->a(Landroid/view/View;)V

    .line 2748656
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JuH;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, LX/JuH;->a:LX/Ck0;

    return-void
.end method
