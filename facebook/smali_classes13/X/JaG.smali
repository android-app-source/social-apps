.class public LX/JaG;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field private final a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/ImageButton;

.field public g:LX/JaB;

.field private h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field private i:Ljava/lang/Runnable;

.field private j:Z

.field public k:LX/JaC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2709681
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2709682
    const-class v0, LX/JaG;

    invoke-static {v0, p0}, LX/JaG;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2709683
    const v0, 0x7f0306fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2709684
    const v0, 0x7f0d12b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/JaG;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2709685
    const v0, 0x7f0d12b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JaG;->b:Landroid/widget/TextView;

    .line 2709686
    const v0, 0x7f0d12b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JaG;->c:Landroid/widget/TextView;

    .line 2709687
    const v0, 0x7f0d12b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JaG;->d:Landroid/widget/TextView;

    .line 2709688
    const v0, 0x7f0d12b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JaG;->e:Landroid/widget/TextView;

    .line 2709689
    const v0, 0x7f0d12b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LX/JaG;->f:Landroid/widget/ImageButton;

    .line 2709690
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JaG;->j:Z

    .line 2709691
    return-void
.end method

.method public static a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V
    .locals 1

    .prologue
    .line 2709692
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2709693
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2709694
    :goto_0
    return-void

    .line 2709695
    :cond_1
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2709696
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/JaG;

    const-class v1, LX/JaC;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/JaC;

    const-class p0, LX/13A;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/13A;

    iput-object v1, p1, LX/JaG;->k:LX/JaC;

    iput-object v2, p1, LX/JaG;->l:LX/13A;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2709626
    iget-object v0, p0, LX/JaG;->i:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2709627
    iget-object v0, p0, LX/JaG;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2709628
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JaG;->j:Z

    .line 2709629
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/JaG;->setVisibility(I)V

    .line 2709630
    return-void
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2709637
    iget-object v0, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 2709638
    iget-boolean v0, p0, LX/JaG;->j:Z

    if-eqz v0, :cond_0

    .line 2709639
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/JaG;->setVisibility(I)V

    .line 2709640
    :cond_0
    :goto_0
    return-void

    .line 2709641
    :cond_1
    iput-object p1, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2709642
    iget-object v0, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v1

    .line 2709643
    if-nez v1, :cond_2

    .line 2709644
    invoke-virtual {p0}, LX/JaG;->a()V

    goto :goto_0

    .line 2709645
    :cond_2
    new-instance v2, LX/Ja9;

    invoke-direct {v2}, LX/Ja9;-><init>()V

    .line 2709646
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    .line 2709647
    iput-object v0, v2, LX/Ja9;->a:Ljava/lang/String;

    .line 2709648
    iget-object v0, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    const-string v3, "user_name"

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2709649
    iget-object v0, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    const-string v3, "user_name"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2709650
    iput-object v0, v2, LX/Ja9;->c:Ljava/lang/String;

    .line 2709651
    :cond_3
    sget-object v0, LX/76S;->ANY:LX/76S;

    invoke-static {v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 2709652
    if-eqz v0, :cond_5

    .line 2709653
    iget-object v3, p0, LX/JaG;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v4, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2709654
    iget-object v3, p0, LX/JaG;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2709655
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    .line 2709656
    iput-object v0, v2, LX/Ja9;->b:Ljava/lang/String;

    .line 2709657
    :goto_1
    iget-object v0, p0, LX/JaG;->b:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2709658
    iget-object v0, p0, LX/JaG;->c:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2709659
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2709660
    iget-object v3, p0, LX/JaG;->d:Landroid/widget/TextView;

    new-instance v4, LX/JaD;

    invoke-direct {v4, p0}, LX/JaD;-><init>(LX/JaG;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2709661
    iget-object v3, p0, LX/JaG;->d:Landroid/widget/TextView;

    invoke-static {v3, v0}, LX/JaG;->a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    .line 2709662
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2709663
    iget-object v3, p0, LX/JaG;->e:Landroid/widget/TextView;

    new-instance v4, LX/JaE;

    invoke-direct {v4, p0}, LX/JaE;-><init>(LX/JaG;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2709664
    iget-object v3, p0, LX/JaG;->e:Landroid/widget/TextView;

    invoke-static {v3, v0}, LX/JaG;->a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    .line 2709665
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2709666
    if-nez v0, :cond_6

    .line 2709667
    iget-object v3, p0, LX/JaG;->f:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2709668
    :goto_2
    iget-object v0, p0, LX/JaG;->l:LX/13A;

    iget-object v3, p0, LX/JaG;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v3, p2, v1, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v0

    .line 2709669
    iget-object v1, p0, LX/JaG;->k:LX/JaC;

    iget-object v3, p0, LX/JaG;->i:Ljava/lang/Runnable;

    invoke-virtual {v1, p0, v2, v0, v3}, LX/JaC;->a(LX/JaG;LX/Ja9;LX/78A;Ljava/lang/Runnable;)LX/JaB;

    move-result-object v0

    iput-object v0, p0, LX/JaG;->g:LX/JaB;

    .line 2709670
    iget-object v0, p0, LX/JaG;->g:LX/JaB;

    .line 2709671
    iget-object v1, v0, LX/JaB;->g:LX/Ja9;

    .line 2709672
    iget-object v2, v1, LX/Ja9;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2709673
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, LX/JaB;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_7

    .line 2709674
    :cond_4
    :goto_3
    iget-object v0, p0, LX/JaG;->g:LX/JaB;

    invoke-virtual {v0}, LX/76U;->a()V

    .line 2709675
    iput-boolean v5, p0, LX/JaG;->j:Z

    .line 2709676
    invoke-virtual {p0, v5}, LX/JaG;->setVisibility(I)V

    goto/16 :goto_0

    .line 2709677
    :cond_5
    iget-object v0, p0, LX/JaG;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_1

    .line 2709678
    :cond_6
    iget-object v3, p0, LX/JaG;->f:Landroid/widget/ImageButton;

    new-instance v4, LX/JaF;

    invoke-direct {v4, p0}, LX/JaF;-><init>(LX/JaG;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2709679
    :cond_7
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "user_id"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/JaB;->h:Ljava/lang/String;

    .line 2709680
    iget-object v1, v0, LX/JaB;->d:LX/2do;

    iget-object v2, v0, LX/JaB;->b:LX/2Ip;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_3
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2709633
    iget-boolean v0, p0, LX/JaG;->j:Z

    if-eqz v0, :cond_0

    .line 2709634
    invoke-virtual {p0, v1, v1}, LX/JaG;->setMeasuredDimension(II)V

    .line 2709635
    :goto_0
    return-void

    .line 2709636
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2709631
    iput-object p1, p0, LX/JaG;->i:Ljava/lang/Runnable;

    .line 2709632
    return-void
.end method
