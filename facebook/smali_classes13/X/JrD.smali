.class public LX/JrD;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/2N4;

.field private final b:LX/FDs;

.field private final c:LX/Jqb;

.field private final d:LX/Jrc;

.field private final e:LX/3QL;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742433
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrD;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2N4;LX/FDs;LX/Jqb;LX/Jrc;LX/3QL;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2742434
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2742435
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742436
    iput-object v0, p0, LX/JrD;->f:LX/0Ot;

    .line 2742437
    iput-object p1, p0, LX/JrD;->a:LX/2N4;

    .line 2742438
    iput-object p2, p0, LX/JrD;->b:LX/FDs;

    .line 2742439
    iput-object p3, p0, LX/JrD;->c:LX/Jqb;

    .line 2742440
    iput-object p4, p0, LX/JrD;->d:LX/Jrc;

    .line 2742441
    iput-object p5, p0, LX/JrD;->e:LX/3QL;

    .line 2742442
    return-void
.end method

.method public static a(LX/0QB;)LX/JrD;
    .locals 13

    .prologue
    .line 2742400
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742401
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742402
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742403
    if-nez v1, :cond_0

    .line 2742404
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742405
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742406
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742407
    sget-object v1, LX/JrD;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742408
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742409
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742410
    :cond_1
    if-nez v1, :cond_4

    .line 2742411
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742412
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742413
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742414
    new-instance v7, LX/JrD;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v8

    check-cast v8, LX/2N4;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v9

    check-cast v9, LX/FDs;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v10

    check-cast v10, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v11

    check-cast v11, LX/Jrc;

    invoke-static {v0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v12

    check-cast v12, LX/3QL;

    invoke-direct/range {v7 .. v12}, LX/JrD;-><init>(LX/2N4;LX/FDs;LX/Jqb;LX/Jrc;LX/3QL;)V

    .line 2742415
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2742416
    iput-object v8, v7, LX/JrD;->f:LX/0Ot;

    .line 2742417
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742418
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742419
    if-nez v1, :cond_2

    .line 2742420
    sget-object v0, LX/JrD;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742421
    :goto_1
    if-eqz v0, :cond_3

    .line 2742422
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742423
    :goto_3
    check-cast v0, LX/JrD;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742424
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742425
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742426
    :catchall_1
    move-exception v0

    .line 2742427
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742428
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742429
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742430
    :cond_2
    :try_start_8
    sget-object v0, LX/JrD;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrD;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742431
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742432
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742387
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->s()LX/6kH;

    move-result-object v0

    .line 2742388
    iget-object v1, p0, LX/JrD;->d:LX/Jrc;

    iget-object v2, v0, LX/6kH;->threadKey:LX/6l9;

    invoke-virtual {v1, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2742389
    iget-object v1, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v1, v3, :cond_1

    .line 2742390
    iget-wide v4, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    .line 2742391
    :goto_0
    iget-object v1, p0, LX/JrD;->b:LX/FDs;

    iget-object v4, v0, LX/6kH;->actionTimestampMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v0, LX/6kH;->watermarkTimestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-wide v8, p2, LX/7GJ;->b:J

    invoke-virtual/range {v1 .. v9}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;JJJ)V

    .line 2742392
    iget-object v0, p0, LX/JrD;->a:LX/2N4;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2742393
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742394
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2742395
    if-eqz v1, :cond_0

    .line 2742396
    const-string v3, "threadSummary"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742397
    const-string v1, "fetchTimeMs"

    iget-wide v4, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2742398
    :cond_0
    return-object v2

    .line 2742399
    :cond_1
    iget-object v1, v0, LX/6kH;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742381
    const-string v0, "threadSummary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742382
    if-nez v0, :cond_0

    .line 2742383
    :goto_0
    return-void

    .line 2742384
    :cond_0
    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->s()LX/6kH;

    move-result-object v1

    iget-object v1, v1, LX/6kH;->actionTimestampMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2742385
    iget-object v1, p0, LX/JrD;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const-string v4, "fetchTimeMs"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/2Oe;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742386
    iget-object v1, p0, LX/JrD;->c:LX/Jqb;

    invoke-virtual {v1, v0, v2, v3}, LX/Jqb;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2742377
    check-cast p1, LX/6kW;

    .line 2742378
    invoke-virtual {p1}, LX/6kW;->s()LX/6kH;

    move-result-object v0

    .line 2742379
    iget-object v1, p0, LX/JrD;->d:LX/Jrc;

    iget-object v0, v0, LX/6kH;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742380
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
