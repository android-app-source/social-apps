.class public final enum LX/K90;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K90;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K90;

.field public static final enum ALL_CAPS:LX/K90;

.field public static final enum NONE:LX/K90;

.field public static final enum REPLACE_PUBLISHER_NAME:LX/K90;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2776536
    new-instance v0, LX/K90;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/K90;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K90;->NONE:LX/K90;

    .line 2776537
    new-instance v0, LX/K90;

    const-string v1, "ALL_CAPS"

    invoke-direct {v0, v1, v3}, LX/K90;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K90;->ALL_CAPS:LX/K90;

    .line 2776538
    new-instance v0, LX/K90;

    const-string v1, "REPLACE_PUBLISHER_NAME"

    invoke-direct {v0, v1, v4}, LX/K90;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    .line 2776539
    const/4 v0, 0x3

    new-array v0, v0, [LX/K90;

    sget-object v1, LX/K90;->NONE:LX/K90;

    aput-object v1, v0, v2

    sget-object v1, LX/K90;->ALL_CAPS:LX/K90;

    aput-object v1, v0, v3

    sget-object v1, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    aput-object v1, v0, v4

    sput-object v0, LX/K90;->$VALUES:[LX/K90;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2776540
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K90;
    .locals 1

    .prologue
    .line 2776541
    const-class v0, LX/K90;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K90;

    return-object v0
.end method

.method public static values()[LX/K90;
    .locals 1

    .prologue
    .line 2776542
    sget-object v0, LX/K90;->$VALUES:[LX/K90;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K90;

    return-object v0
.end method
