.class public LX/Jmh;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2733212
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2733213
    invoke-direct {p0}, LX/Jmh;->a()V

    .line 2733214
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733218
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2733219
    invoke-direct {p0}, LX/Jmh;->a()V

    .line 2733220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733215
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2733216
    invoke-direct {p0}, LX/Jmh;->a()V

    .line 2733217
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2733207
    const-class v0, LX/Jmh;

    invoke-static {v0, p0}, LX/Jmh;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2733208
    const v0, 0x7f0308fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2733209
    invoke-virtual {p0}, LX/Jmh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0278

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Jmh;->c:I

    .line 2733210
    invoke-direct {p0}, LX/Jmh;->b()V

    .line 2733211
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Jmh;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Jmh;

    new-instance p1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    invoke-direct {p1}, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;-><init>()V

    invoke-static {v0}, LX/FOK;->b(LX/0QB;)LX/FOK;

    move-result-object v1

    check-cast v1, LX/FOK;

    invoke-static {v0}, LX/FKD;->b(LX/0QB;)LX/FKD;

    move-result-object v2

    check-cast v2, LX/FKD;

    iput-object v1, p1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->e:LX/FOK;

    iput-object v2, p1, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->f:LX/FKD;

    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    iput-object v0, p0, LX/Jmh;->a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2733203
    const v0, 0x7f0d171d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, LX/Jmh;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2733204
    iget-object v0, p0, LX/Jmh;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/Jmh;->a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2733205
    iget-object v0, p0, LX/Jmh;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/Jmg;

    invoke-direct {v1, p0}, LX/Jmg;-><init>(LX/Jmh;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2733206
    return-void
.end method


# virtual methods
.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2733202
    iget-object v0, p0, LX/Jmh;->b:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2733201
    iget-object v0, p0, LX/Jmh;->a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    return-object v0
.end method

.method public setData(LX/Jme;)V
    .locals 1

    .prologue
    .line 2733195
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2733196
    iget-object v0, p0, LX/Jmh;->a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    .line 2733197
    iget-object p0, v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    if-ne p0, p1, :cond_0

    .line 2733198
    :goto_0
    return-void

    .line 2733199
    :cond_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Jme;

    iput-object p0, v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->g:LX/Jme;

    .line 2733200
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setInboxRoomSuggestionListener(LX/Jmf;)V
    .locals 1
    .param p1    # LX/Jmf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2733192
    iget-object v0, p0, LX/Jmh;->a:Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;

    .line 2733193
    iput-object p1, v0, Lcom/facebook/messaging/inbox2/roomsuggestions/InboxRoomSuggestionAdapter;->h:LX/Jmf;

    .line 2733194
    return-void
.end method
