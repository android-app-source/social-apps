.class public final LX/JWA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/JWD;


# direct methods
.method public constructor <init>(LX/JWD;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 2702495
    iput-object p1, p0, LX/JWA;->c:LX/JWD;

    iput-object p2, p0, LX/JWA;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iput-object p3, p0, LX/JWA;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2702496
    iget-object v0, p0, LX/JWA;->c:LX/JWD;

    iget-object v1, p0, LX/JWA;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JWA;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 2702497
    new-instance v3, LX/85H;

    invoke-direct {v3}, LX/85H;-><init>()V

    move-object v3, v3

    .line 2702498
    const-string v4, "node_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "after_cursor"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/85H;

    .line 2702499
    iget-object v4, v0, LX/JWD;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2702500
    new-instance v4, LX/JWC;

    invoke-direct {v4, v0}, LX/JWC;-><init>(LX/JWD;)V

    iget-object p0, v0, LX/JWD;->c:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2702501
    return-object v0
.end method
