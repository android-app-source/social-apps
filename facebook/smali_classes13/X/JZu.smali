.class public LX/JZu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JZs;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/spec/FriendRequestActionAcceptedComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2709304
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JZu;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/components/spec/FriendRequestActionAcceptedComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2709280
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2709281
    iput-object p1, p0, LX/JZu;->b:LX/0Ot;

    .line 2709282
    return-void
.end method

.method public static a(LX/0QB;)LX/JZu;
    .locals 4

    .prologue
    .line 2709293
    const-class v1, LX/JZu;

    monitor-enter v1

    .line 2709294
    :try_start_0
    sget-object v0, LX/JZu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2709295
    sput-object v2, LX/JZu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2709296
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2709297
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2709298
    new-instance v3, LX/JZu;

    const/16 p0, 0x2253

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JZu;-><init>(LX/0Ot;)V

    .line 2709299
    move-object v0, v3

    .line 2709300
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2709301
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709302
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2709303
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2709285
    check-cast p2, LX/JZt;

    .line 2709286
    iget-object v0, p0, LX/JZu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/components/spec/FriendRequestActionAcceptedComponentSpec;

    iget-object v1, p2, LX/JZt;->a:Ljava/lang/String;

    iget-object v2, p2, LX/JZt;->b:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2709287
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    .line 2709288
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v5, p0}, LX/1up;->c(F)LX/1up;

    move-result-object p0

    iget-object v5, v0, Lcom/facebook/friending/components/spec/FriendRequestActionAcceptedComponentSpec;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-virtual {v5, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v5

    sget-object p2, Lcom/facebook/friending/components/spec/FriendRequestActionAcceptedComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v5

    invoke-virtual {v5}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const p0, 0x7f0b2629

    invoke-interface {v5, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const/4 p0, 0x2

    const p2, 0x7f0b0064

    invoke-interface {v5, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 p0, 0x5

    const p2, 0x7f0b0064

    invoke-interface {v5, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 2709289
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 2709290
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b02aa

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a0281

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v5, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v5

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const p0, 0x7f0b02ab

    invoke-interface {v5, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 2709291
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a009a

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v6, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->d(Z)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2709292
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2709283
    invoke-static {}, LX/1dS;->b()V

    .line 2709284
    const/4 v0, 0x0

    return-object v0
.end method
