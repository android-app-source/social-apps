.class public final enum LX/Jlu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jlu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jlu;

.field public static final enum NO_DATA:LX/Jlu;

.field public static final enum STALE_DATA_NEEDS_FULL_SERVER_FETCH:LX/Jlu;

.field public static final enum STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

.field public static final enum UP_TO_DATE:LX/Jlu;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2731936
    new-instance v0, LX/Jlu;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v2}, LX/Jlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlu;->NO_DATA:LX/Jlu;

    .line 2731937
    new-instance v0, LX/Jlu;

    const-string v1, "STALE_DATA_NEEDS_FULL_SERVER_FETCH"

    invoke-direct {v0, v1, v3}, LX/Jlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlu;->STALE_DATA_NEEDS_FULL_SERVER_FETCH:LX/Jlu;

    .line 2731938
    new-instance v0, LX/Jlu;

    const-string v1, "STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH"

    invoke-direct {v0, v1, v4}, LX/Jlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlu;->STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

    .line 2731939
    new-instance v0, LX/Jlu;

    const-string v1, "UP_TO_DATE"

    invoke-direct {v0, v1, v5}, LX/Jlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlu;->UP_TO_DATE:LX/Jlu;

    .line 2731940
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jlu;

    sget-object v1, LX/Jlu;->NO_DATA:LX/Jlu;

    aput-object v1, v0, v2

    sget-object v1, LX/Jlu;->STALE_DATA_NEEDS_FULL_SERVER_FETCH:LX/Jlu;

    aput-object v1, v0, v3

    sget-object v1, LX/Jlu;->STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH:LX/Jlu;

    aput-object v1, v0, v4

    sget-object v1, LX/Jlu;->UP_TO_DATE:LX/Jlu;

    aput-object v1, v0, v5

    sput-object v0, LX/Jlu;->$VALUES:[LX/Jlu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2731941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jlu;
    .locals 1

    .prologue
    .line 2731942
    const-class v0, LX/Jlu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jlu;

    return-object v0
.end method

.method public static values()[LX/Jlu;
    .locals 1

    .prologue
    .line 2731943
    sget-object v0, LX/Jlu;->$VALUES:[LX/Jlu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jlu;

    return-object v0
.end method
