.class public final LX/Jpn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Jpb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V
    .locals 0

    .prologue
    .line 2737990
    iput-object p1, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2737991
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->p(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2737992
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2737993
    check-cast p1, LX/Jpb;

    .line 2737994
    iget-object v0, p1, LX/Jpb;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2737995
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->p(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2737996
    :goto_0
    return-void

    .line 2737997
    :cond_0
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->i:LX/3Ne;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    iget-object v2, p1, LX/Jpb;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    iget-object v2, p1, LX/Jpb;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ne;->a(LX/0Px;)V

    .line 2737998
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    const v1, -0x70c5bccf

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2737999
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    sget-object v1, LX/Jpg;->MATCHED:LX/Jpg;

    .line 2738000
    iput-object v1, v0, LX/Jph;->a:LX/Jpg;

    .line 2738001
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->l(Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;)V

    .line 2738002
    iget-object v0, p0, LX/Jpn;->a:Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    .line 2738003
    iget-object v1, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->b:LX/67a;

    invoke-virtual {v1}, LX/67a;->f()LX/3u1;

    move-result-object v1

    .line 2738004
    iget-object v2, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->n:LX/Jph;

    iget-object p0, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738005
    iget p1, p0, LX/Jpe;->g:I

    move p0, p1

    .line 2738006
    iget-object p1, v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;->m:LX/Jpe;

    .line 2738007
    iget-object v0, p1, LX/Jpe;->d:LX/0Px;

    move-object p1, v0

    .line 2738008
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    invoke-virtual {v2, p0, p1}, LX/Jph;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3u1;->a(Ljava/lang/CharSequence;)V

    .line 2738009
    goto :goto_0
.end method
