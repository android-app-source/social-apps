.class public final LX/JZw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JZx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2709342
    invoke-static {}, LX/JZx;->q()LX/JZx;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2709343
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2709344
    const-string v0, "FriendRequestActionDeletedComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2709345
    if-ne p0, p1, :cond_1

    .line 2709346
    :cond_0
    :goto_0
    return v0

    .line 2709347
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2709348
    goto :goto_0

    .line 2709349
    :cond_3
    check-cast p1, LX/JZw;

    .line 2709350
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2709351
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2709352
    if-eq v2, v3, :cond_0

    .line 2709353
    iget-object v2, p0, LX/JZw;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JZw;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JZw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2709354
    goto :goto_0

    .line 2709355
    :cond_4
    iget-object v2, p1, LX/JZw;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
