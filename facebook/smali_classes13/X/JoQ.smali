.class public final LX/JoQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;)V
    .locals 0

    .prologue
    .line 2735593
    iput-object p1, p0, LX/JoQ;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 2735586
    iget-object v0, p0, LX/JoQ;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->q:LX/JoR;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735587
    iget-object v0, p0, LX/JoQ;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;->m:LX/JdU;

    .line 2735588
    iget-object v1, v0, LX/JdU;->a:LX/0Zb;

    const-string p1, "message_block_select_cancel_from_blocked_warning_alert"

    const/4 p2, 0x0

    invoke-interface {v1, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2735589
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2735590
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2735591
    :cond_0
    iget-object v0, p0, LX/JoQ;->a:Lcom/facebook/messaging/mutators/AskToOpenThreadDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2735592
    return-void
.end method
