.class public LX/JbA;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:I

.field private f:LX/JbC;

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field private h:LX/Jb8;

.field private i:I

.field private j:LX/Jb7;

.field private k:LX/Jb7;

.field private l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2716590
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2716591
    iput-boolean v0, p0, LX/JbA;->c:Z

    .line 2716592
    iput-boolean v0, p0, LX/JbA;->d:Z

    .line 2716593
    const/16 v0, 0xc

    iput v0, p0, LX/JbA;->e:I

    .line 2716594
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/JbA;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2716595
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JbA;->a(Landroid/util/AttributeSet;)V

    .line 2716596
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2716468
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2716469
    iput-boolean v0, p0, LX/JbA;->c:Z

    .line 2716470
    iput-boolean v0, p0, LX/JbA;->d:Z

    .line 2716471
    const/16 v0, 0xc

    iput v0, p0, LX/JbA;->e:I

    .line 2716472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/JbA;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2716473
    invoke-direct {p0, p2}, LX/JbA;->a(Landroid/util/AttributeSet;)V

    .line 2716474
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2716583
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2716584
    iput-boolean v0, p0, LX/JbA;->c:Z

    .line 2716585
    iput-boolean v0, p0, LX/JbA;->d:Z

    .line 2716586
    const/16 v0, 0xc

    iput v0, p0, LX/JbA;->e:I

    .line 2716587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/JbA;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2716588
    invoke-direct {p0, p2}, LX/JbA;->a(Landroid/util/AttributeSet;)V

    .line 2716589
    return-void
.end method

.method private a()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v13, -0x2

    const/4 v4, 0x0

    .line 2716505
    invoke-virtual {p0}, LX/JbA;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, LX/JbA;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/JbA;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    .line 2716506
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v0

    .line 2716507
    iget v2, p0, LX/JbA;->i:I

    add-int/2addr v2, v0

    .line 2716508
    div-int v0, v1, v2

    .line 2716509
    mul-int/2addr v2, v0

    iget v3, p0, LX/JbA;->i:I

    add-int/2addr v2, v3

    if-gt v2, v1, :cond_13

    .line 2716510
    add-int/lit8 v0, v0, 0x1

    move v1, v0

    .line 2716511
    :goto_0
    iget-object v0, p0, LX/JbA;->f:LX/JbC;

    if-eqz v0, :cond_12

    iget-object v0, p0, LX/JbA;->f:LX/JbC;

    invoke-interface {v0}, LX/JbC;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_12

    const/4 v0, 0x2

    if-le v1, v0, :cond_12

    .line 2716512
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2716513
    iget-object v0, p0, LX/JbA;->f:LX/JbC;

    invoke-interface {v0}, LX/JbC;->b()LX/0Px;

    move-result-object v7

    move v3, v4

    move-object v2, v5

    .line 2716514
    :goto_1
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 2716515
    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JbB;

    invoke-interface {v0}, LX/JbB;->a()LX/DUV;

    move-result-object v0

    .line 2716516
    invoke-interface {v0}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/JbA;->b:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2716517
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v2

    .line 2716518
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_1

    .line 2716519
    :cond_1
    if-eqz v2, :cond_2

    .line 2716520
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2716521
    :cond_2
    iget-object v0, p0, LX/JbA;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v2, :cond_7

    move v2, v6

    .line 2716522
    :goto_2
    iget v0, p0, LX/JbA;->e:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2716523
    if-eqz v2, :cond_3

    iget-boolean v1, p0, LX/JbA;->c:Z

    if-eqz v1, :cond_3

    .line 2716524
    add-int/lit8 v0, v0, -0x1

    .line 2716525
    :cond_3
    iget-object v1, p0, LX/JbA;->f:LX/JbC;

    invoke-interface {v1}, LX/JbC;->a()I

    move-result v1

    if-le v1, v0, :cond_8

    move v7, v6

    .line 2716526
    :goto_3
    if-eqz v7, :cond_4

    iget-boolean v1, p0, LX/JbA;->d:Z

    if-eqz v1, :cond_4

    .line 2716527
    add-int/lit8 v0, v0, -0x1

    .line 2716528
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 2716529
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    if-eqz v0, :cond_5

    .line 2716530
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    invoke-virtual {p0, v0}, LX/JbA;->removeView(Landroid/view/View;)V

    .line 2716531
    :cond_5
    iget-object v0, p0, LX/JbA;->k:LX/Jb7;

    if-eqz v0, :cond_6

    .line 2716532
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object v1, p0, LX/JbA;->k:LX/Jb7;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeView(Landroid/view/View;)V

    .line 2716533
    :cond_6
    :goto_4
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v0

    if-le v0, v9, :cond_9

    .line 2716534
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object v1, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeViewAt(I)V

    goto :goto_4

    :cond_7
    move v2, v4

    .line 2716535
    goto :goto_2

    :cond_8
    move v7, v4

    .line 2716536
    goto :goto_3

    :cond_9
    move v6, v4

    .line 2716537
    :goto_5
    if-ge v6, v9, :cond_e

    .line 2716538
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DUV;

    .line 2716539
    iget-object v1, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v1

    if-le v1, v6, :cond_a

    .line 2716540
    iget-object v1, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1, v6}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    .line 2716541
    :goto_6
    invoke-interface {v0}, LX/DUV;->k()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_b

    .line 2716542
    invoke-interface {v0}, LX/DUV;->k()LX/1vs;

    move-result-object v3

    iget-object v10, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2716543
    invoke-virtual {v10, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2716544
    :goto_7
    iget-boolean v10, p0, LX/JbA;->n:Z

    if-eqz v10, :cond_d

    .line 2716545
    invoke-interface {v0}, LX/DUV;->j()LX/1vs;

    move-result-object v10

    iget v10, v10, LX/1vs;->b:I

    if-eqz v10, :cond_c

    .line 2716546
    invoke-interface {v0}, LX/DUV;->j()LX/1vs;

    move-result-object v0

    iget-object v10, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2716547
    invoke-virtual {v10, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    .line 2716548
    :goto_8
    invoke-virtual {v1, v3, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/String;Z)V

    .line 2716549
    :goto_9
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 2716550
    :cond_a
    iget v3, p0, LX/JbA;->i:I

    .line 2716551
    invoke-virtual {p0}, LX/JbA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v10, 0x7f0a0842

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 2716552
    invoke-virtual {p0}, LX/JbA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v11, 0x7f0b1f89

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 2716553
    new-instance v1, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    invoke-virtual {p0}, LX/JbA;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v1, v12, v10, v11}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;-><init>(Landroid/content/Context;II)V

    .line 2716554
    invoke-virtual {v1, v3}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->setPogSize(I)V

    .line 2716555
    iget-object v3, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v3, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_6

    :cond_b
    move-object v3, v5

    .line 2716556
    goto :goto_7

    :cond_c
    move v0, v4

    .line 2716557
    goto :goto_8

    .line 2716558
    :cond_d
    iget-boolean v0, p0, LX/JbA;->n:Z

    invoke-virtual {v1, v3, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/String;Z)V

    goto :goto_9

    .line 2716559
    :cond_e
    if-eqz v7, :cond_10

    iget-boolean v0, p0, LX/JbA;->d:Z

    if-eqz v0, :cond_10

    .line 2716560
    iget-object v0, p0, LX/JbA;->k:LX/Jb7;

    if-nez v0, :cond_f

    .line 2716561
    new-instance v0, LX/Jb7;

    invoke-virtual {p0}, LX/JbA;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/JbA;->h:LX/Jb8;

    invoke-direct {v0, v1, v3}, LX/Jb7;-><init>(Landroid/content/Context;LX/Jb8;)V

    iput-object v0, p0, LX/JbA;->k:LX/Jb7;

    .line 2716562
    :cond_f
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object v1, p0, LX/JbA;->k:LX/Jb7;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2716563
    iget-object v0, p0, LX/JbA;->k:LX/Jb7;

    iget-object v1, p0, LX/JbA;->f:LX/JbC;

    invoke-interface {v1}, LX/JbC;->a()I

    move-result v1

    sub-int/2addr v1, v9

    invoke-virtual {v0, v1}, LX/Jb7;->setTypeShowCount(I)V

    .line 2716564
    :cond_10
    if-eqz v2, :cond_12

    iget-boolean v0, p0, LX/JbA;->c:Z

    if-eqz v0, :cond_12

    .line 2716565
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    if-nez v0, :cond_11

    .line 2716566
    new-instance v0, LX/Jb7;

    invoke-virtual {p0}, LX/JbA;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JbA;->h:LX/Jb8;

    invoke-direct {v0, v1, v2}, LX/Jb7;-><init>(Landroid/content/Context;LX/Jb8;)V

    iput-object v0, p0, LX/JbA;->j:LX/Jb7;

    .line 2716567
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    const v1, 0x7f0d0102

    invoke-virtual {v0, v1}, LX/Jb7;->setId(I)V

    .line 2716568
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v13, v13}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2716569
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2716570
    iget-object v1, p0, LX/JbA;->j:LX/Jb7;

    invoke-virtual {v1, v0}, LX/Jb7;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2716571
    :cond_11
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    invoke-virtual {p0, v0}, LX/JbA;->addView(Landroid/view/View;)V

    .line 2716572
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    iget-object v1, p0, LX/JbA;->a:Landroid/content/res/Resources;

    const v2, 0x7f081b71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Jb7;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2716573
    iget-object v0, p0, LX/JbA;->j:LX/Jb7;

    .line 2716574
    iget-object v1, v0, LX/Jb7;->i:Landroid/graphics/Paint;

    iget-object v2, v0, LX/Jb7;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2716575
    iget-object v1, v0, LX/Jb7;->i:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2716576
    iget-object v1, v0, LX/Jb7;->a:Landroid/content/res/Resources;

    const v2, 0x7f020ec6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2716577
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    iget-object v3, v0, LX/Jb7;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a05f0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2716578
    iget-object v2, v0, LX/Jb7;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2716579
    new-instance v1, LX/Jb5;

    invoke-direct {v1, v0}, LX/Jb5;-><init>(LX/Jb7;)V

    invoke-virtual {v0, v1}, LX/Jb7;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2716580
    invoke-virtual {v0}, LX/Jb7;->invalidate()V

    .line 2716581
    :cond_12
    iget-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget v1, p0, LX/JbA;->m:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2716582
    return-void

    :cond_13
    move v1, v0

    goto/16 :goto_0
.end method

.method private static a(LX/JbA;Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2716504
    iput-object p1, p0, LX/JbA;->a:Landroid/content/res/Resources;

    iput-object p2, p0, LX/JbA;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2716485
    const-class v0, LX/JbA;

    invoke-static {v0, p0}, LX/JbA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2716486
    invoke-virtual {p0, v2}, LX/JbA;->setWillNotDraw(Z)V

    .line 2716487
    const v0, 0x7f030abb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2716488
    const v0, 0x7f0d1b70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2716489
    iget-object v0, p0, LX/JbA;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JbA;->i:I

    .line 2716490
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JbA;->n:Z

    .line 2716491
    iget-object v0, p0, LX/JbA;->a:Landroid/content/res/Resources;

    const v1, 0x7f081b70

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/JbA;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2716492
    new-instance v0, LX/Jb9;

    invoke-direct {v0, p0}, LX/Jb9;-><init>(LX/JbA;)V

    invoke-virtual {p0, v0}, LX/JbA;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2716493
    if-eqz p1, :cond_0

    .line 2716494
    invoke-virtual {p0}, LX/JbA;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->MemberBarAttrs:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2716495
    if-eqz v0, :cond_0

    .line 2716496
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/JbA;->d:Z

    .line 2716497
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/JbA;->c:Z

    .line 2716498
    const/16 v1, 0x2

    iget v2, p0, LX/JbA;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/JbA;->e:I

    .line 2716499
    iget-object v1, p0, LX/JbA;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b11f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2716500
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/JbA;->m:I

    .line 2716501
    iget-object v1, p0, LX/JbA;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget v2, p0, LX/JbA;->m:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2716502
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2716503
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JbA;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/JbA;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v0, v1}, LX/JbA;->a(LX/JbA;Landroid/content/res/Resources;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2716481
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 2716482
    invoke-direct {p0}, LX/JbA;->a()V

    .line 2716483
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 2716484
    return-void
.end method

.method public setCircleSize(I)V
    .locals 0

    .prologue
    .line 2716479
    iput p1, p0, LX/JbA;->i:I

    .line 2716480
    return-void
.end method

.method public setPaddingBetweenPogs(I)V
    .locals 0

    .prologue
    .line 2716477
    iput p1, p0, LX/JbA;->m:I

    .line 2716478
    return-void
.end method

.method public setShowPresenceIconState(Z)V
    .locals 0

    .prologue
    .line 2716475
    iput-boolean p1, p0, LX/JbA;->n:Z

    .line 2716476
    return-void
.end method
