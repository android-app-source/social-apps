.class public final LX/JtH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<[B>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/messaging/attachments/ImageAttachmentData;

.field public final synthetic c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/messaging/attachments/ImageAttachmentData;)V
    .locals 0

    .prologue
    .line 2746542
    iput-object p1, p0, LX/JtH;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iput-object p2, p0, LX/JtH;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/JtH;->b:Lcom/facebook/messaging/attachments/ImageAttachmentData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746534
    sget-object v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch photo image(s)"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746535
    iget-object v0, p0, LX/JtH;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746536
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746537
    check-cast p1, [B

    .line 2746538
    if-nez p1, :cond_0

    .line 2746539
    iget-object v0, p0, LX/JtH;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x152c218e

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746540
    :goto_0
    return-void

    .line 2746541
    :cond_0
    iget-object v0, p0, LX/JtH;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/JtH;->c:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v2, p0, LX/JtH;->b:Lcom/facebook/messaging/attachments/ImageAttachmentData;

    iget-object v2, v2, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;[BLjava/lang/String;)LX/JtM;

    move-result-object v1

    const v2, 0x15c83551

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
