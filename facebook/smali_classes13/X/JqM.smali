.class public LX/JqM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JqM;


# instance fields
.field private final a:LX/2Sz;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Sz;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Sz;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738821
    iput-object p1, p0, LX/JqM;->a:LX/2Sz;

    .line 2738822
    iput-object p2, p0, LX/JqM;->b:LX/0Or;

    .line 2738823
    iput-object p3, p0, LX/JqM;->c:LX/0Or;

    .line 2738824
    return-void
.end method

.method public static a(LX/0QB;)LX/JqM;
    .locals 6

    .prologue
    .line 2738825
    sget-object v0, LX/JqM;->d:LX/JqM;

    if-nez v0, :cond_1

    .line 2738826
    const-class v1, LX/JqM;

    monitor-enter v1

    .line 2738827
    :try_start_0
    sget-object v0, LX/JqM;->d:LX/JqM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2738828
    if-eqz v2, :cond_0

    .line 2738829
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2738830
    new-instance v4, LX/JqM;

    invoke-static {v0}, LX/2Sz;->a(LX/0QB;)LX/2Sz;

    move-result-object v3

    check-cast v3, LX/2Sz;

    const/16 v5, 0x19e

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x2744

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/JqM;-><init>(LX/2Sz;LX/0Or;LX/0Or;)V

    .line 2738831
    move-object v0, v4

    .line 2738832
    sput-object v0, LX/JqM;->d:LX/JqM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2738833
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2738834
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2738835
    :cond_1
    sget-object v0, LX/JqM;->d:LX/JqM;

    return-object v0

    .line 2738836
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2738837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
