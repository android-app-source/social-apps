.class public final LX/Juv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/4ok;

.field public final synthetic b:Z

.field public final synthetic c:LX/0Tn;

.field public final synthetic d:LX/Jv4;


# direct methods
.method public constructor <init>(LX/Jv4;LX/4ok;ZLX/0Tn;)V
    .locals 0

    .prologue
    .line 2749555
    iput-object p1, p0, LX/Juv;->d:LX/Jv4;

    iput-object p2, p0, LX/Juv;->a:LX/4ok;

    iput-boolean p3, p0, LX/Juv;->b:Z

    iput-object p4, p0, LX/Juv;->c:LX/0Tn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2749556
    iget-object v0, p0, LX/Juv;->a:LX/4ok;

    if-eqz v0, :cond_0

    .line 2749557
    iget-object v3, p0, LX/Juv;->a:LX/4ok;

    iget-boolean v0, p0, LX/Juv;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/4ok;->setChecked(Z)V

    .line 2749558
    iget-object v0, p0, LX/Juv;->a:LX/4ok;

    invoke-virtual {v0}, LX/4ok;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/Juv;->d:LX/Jv4;

    iget-object v3, v3, LX/Jv4;->g:LX/0Tn;

    invoke-virtual {v3}, LX/0To;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2749559
    iget-object v0, p0, LX/Juv;->d:LX/Jv4;

    iget-boolean v3, p0, LX/Juv;->b:Z

    invoke-static {v0, v3}, LX/Jv4;->b$redex0(LX/Jv4;Z)V

    .line 2749560
    :cond_0
    iget-object v0, p0, LX/Juv;->d:LX/Jv4;

    iget-object v0, v0, LX/Jv4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v3, p0, LX/Juv;->c:LX/0Tn;

    iget-boolean v4, p0, LX/Juv;->b:Z

    if-nez v4, :cond_2

    :goto_1
    invoke-interface {v0, v3, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2749561
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2749562
    return-void

    :cond_1
    move v0, v2

    .line 2749563
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2749564
    goto :goto_1
.end method
