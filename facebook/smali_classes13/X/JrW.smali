.class public LX/JrW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final r:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Uo;

.field public final b:LX/0Uh;

.field private final c:LX/0Uh;

.field private final d:LX/JqK;

.field public final e:LX/JqR;

.field private final f:LX/2Sz;

.field private final g:LX/JrV;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/6cy;

.field public final k:LX/Jrg;

.field public final l:Ljava/util/concurrent/ScheduledExecutorService;

.field private final m:LX/JqM;

.field private final n:LX/2Sv;

.field private final o:LX/03V;

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/Future;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743615
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrW;->r:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Uo;LX/0Uh;LX/0Uh;LX/JqK;LX/JqR;LX/2Sz;LX/JrV;LX/0Or;LX/0Or;LX/6cy;LX/Jrg;Ljava/util/concurrent/ScheduledExecutorService;LX/JqM;LX/2Sv;LX/03V;)V
    .locals 2
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/MessagesSyncApiVersion;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperStoreManager;",
            "LX/JqK;",
            "LX/JqR;",
            "LX/2Sz;",
            "LX/JrV;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/6cy;",
            "LX/Jrg;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/messaging/sync/push/SyncOperationContextSupplier;",
            "Lcom/facebook/sync/SyncContextChecker;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2743596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2743597
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/JrW;->p:LX/0Ot;

    .line 2743598
    invoke-static {}, LX/0Vg;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/JrW;->q:Ljava/util/concurrent/Future;

    .line 2743599
    iput-object p1, p0, LX/JrW;->a:LX/0Uo;

    .line 2743600
    iput-object p2, p0, LX/JrW;->b:LX/0Uh;

    .line 2743601
    iput-object p3, p0, LX/JrW;->c:LX/0Uh;

    .line 2743602
    iput-object p4, p0, LX/JrW;->d:LX/JqK;

    .line 2743603
    iput-object p5, p0, LX/JrW;->e:LX/JqR;

    .line 2743604
    iput-object p6, p0, LX/JrW;->f:LX/2Sz;

    .line 2743605
    iput-object p7, p0, LX/JrW;->g:LX/JrV;

    .line 2743606
    iput-object p8, p0, LX/JrW;->h:LX/0Or;

    .line 2743607
    iput-object p9, p0, LX/JrW;->i:LX/0Or;

    .line 2743608
    iput-object p10, p0, LX/JrW;->j:LX/6cy;

    .line 2743609
    iput-object p11, p0, LX/JrW;->k:LX/Jrg;

    .line 2743610
    iput-object p12, p0, LX/JrW;->l:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2743611
    iput-object p13, p0, LX/JrW;->m:LX/JqM;

    .line 2743612
    move-object/from16 v0, p14

    iput-object v0, p0, LX/JrW;->n:LX/2Sv;

    .line 2743613
    move-object/from16 v0, p15

    iput-object v0, p0, LX/JrW;->o:LX/03V;

    .line 2743614
    return-void
.end method

.method public static a(LX/0QB;)LX/JrW;
    .locals 7

    .prologue
    .line 2743568
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743569
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743570
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743571
    if-nez v1, :cond_0

    .line 2743572
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743573
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743574
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743575
    sget-object v1, LX/JrW;->r:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743576
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743577
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743578
    :cond_1
    if-nez v1, :cond_4

    .line 2743579
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743580
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743581
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrW;->b(LX/0QB;)LX/JrW;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2743582
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743583
    if-nez v1, :cond_2

    .line 2743584
    sget-object v0, LX/JrW;->r:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrW;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743585
    :goto_1
    if-eqz v0, :cond_3

    .line 2743586
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743587
    :goto_3
    check-cast v0, LX/JrW;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743588
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743589
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743590
    :catchall_1
    move-exception v0

    .line 2743591
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743592
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743593
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743594
    :cond_2
    :try_start_8
    sget-object v0, LX/JrW;->r:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrW;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2743616
    invoke-static {p1}, LX/2Sz;->a(LX/1qK;)Ljava/io/Serializable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/6l3;

    .line 2743617
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2743618
    const-string v2, "fbTraceNode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbtrace/FbTraceNode;

    move-object v2, v0

    .line 2743619
    iget-object v0, v1, LX/6l3;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kW;

    .line 2743620
    iget v4, v0, LX/6kT;->setField_:I

    move v4, v4

    .line 2743621
    const/16 v5, 0x12

    if-ne v4, v5, :cond_0

    .line 2743622
    invoke-virtual {v0}, LX/6kW;->r()LX/6jo;

    move-result-object v0

    .line 2743623
    iget-object v4, v0, LX/6jo;->messageId:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v0, v0, LX/6jo;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 2743624
    iget-object v0, p0, LX/JrW;->e:LX/JqR;

    iget-object v1, v1, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/sync/analytics/FullRefreshReason;->a(J)Lcom/facebook/sync/analytics/FullRefreshReason;

    move-result-object v1

    .line 2743625
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2743626
    invoke-virtual {v0, v1, v2}, LX/JqR;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2743627
    :goto_0
    return-object v0

    .line 2743628
    :cond_1
    iget-object v0, p0, LX/JrW;->q:Ljava/util/concurrent/Future;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2743629
    const/4 v0, 0x0

    .line 2743630
    iget-object v3, p0, LX/JrW;->b:LX/0Uh;

    const/16 v4, 0x115

    invoke-virtual {v3, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2743631
    iget-object v3, p0, LX/JrW;->a:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/JrW;->k:LX/Jrg;

    invoke-virtual {v3}, LX/Jrg;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    .line 2743632
    :cond_2
    move v3, v0

    .line 2743633
    :try_start_0
    iget-object v0, p0, LX/JrW;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqe;

    invoke-virtual {v0, v1, v2}, LX/Jqe;->a(LX/6l3;Lcom/facebook/fbtrace/FbTraceNode;)V

    .line 2743634
    if-eqz v3, :cond_3

    .line 2743635
    iget-object v0, p0, LX/JrW;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2743636
    iget-object v0, p0, LX/JrW;->k:LX/Jrg;

    const-string v2, "first_delta_seq_id"

    iget-object v3, v1, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "num_deltas"

    iget-object v5, v1, LX/6l3;->deltas:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/Jrg;->a(Ljava/util/Map;)V

    .line 2743637
    :cond_3
    iget-object v6, p0, LX/JrW;->b:LX/0Uh;

    const/16 v7, 0x116

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2743638
    iget-object v6, p0, LX/JrW;->l:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/messaging/sync/service/MessagesSyncServiceHandler$1;

    invoke-direct {v7, p0}, Lcom/facebook/messaging/sync/service/MessagesSyncServiceHandler$1;-><init>(LX/JrW;)V

    const-wide/16 v8, 0x1

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, LX/JrW;->q:Ljava/util/concurrent/Future;

    .line 2743639
    :cond_4
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2743640
    goto :goto_0

    .line 2743641
    :catch_0
    move-exception v5

    .line 2743642
    iget-object v0, p0, LX/JrW;->g:LX/JrV;

    iget-object v2, p0, LX/JrW;->m:LX/JqM;

    .line 2743643
    iget-object v6, v1, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-nez v6, :cond_5

    .line 2743644
    iget-object v6, v2, LX/JqM;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2743645
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2743646
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 2743647
    :goto_1
    move-object v1, v6

    .line 2743648
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JrW;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/JrW;->d:LX/JqK;

    .line 2743649
    iget-object v4, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 2743650
    invoke-virtual/range {v0 .. v5}, LX/7Gb;->a(Ljava/lang/String;ILX/7Fx;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/Exception;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v6, v1, LX/6l3;->queueEntityId:Ljava/lang/Long;

    goto :goto_1
.end method

.method private static a(LX/JrW;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JrW;",
            "LX/0Ot",
            "<",
            "LX/Jqe;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2743595
    iput-object p1, p0, LX/JrW;->p:LX/0Ot;

    return-void
.end method

.method private static b(LX/0QB;)LX/JrW;
    .locals 17

    .prologue
    .line 2743565
    new-instance v1, LX/JrW;

    invoke-static/range {p0 .. p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v2

    check-cast v2, LX/0Uo;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/JqK;->a(LX/0QB;)LX/JqK;

    move-result-object v5

    check-cast v5, LX/JqK;

    invoke-static/range {p0 .. p0}, LX/JqR;->a(LX/0QB;)LX/JqR;

    move-result-object v6

    check-cast v6, LX/JqR;

    invoke-static/range {p0 .. p0}, LX/2Sz;->a(LX/0QB;)LX/2Sz;

    move-result-object v7

    check-cast v7, LX/2Sz;

    invoke-static/range {p0 .. p0}, LX/JrV;->a(LX/0QB;)LX/JrV;

    move-result-object v8

    check-cast v8, LX/JrV;

    const/16 v9, 0x14ea

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x15d4

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v11

    check-cast v11, LX/6cy;

    invoke-static/range {p0 .. p0}, LX/Jrg;->a(LX/0QB;)LX/Jrg;

    move-result-object v12

    check-cast v12, LX/Jrg;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/JqM;->a(LX/0QB;)LX/JqM;

    move-result-object v14

    check-cast v14, LX/JqM;

    invoke-static/range {p0 .. p0}, LX/2Sv;->a(LX/0QB;)LX/2Sv;

    move-result-object v15

    check-cast v15, LX/2Sv;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v16

    check-cast v16, LX/03V;

    invoke-direct/range {v1 .. v16}, LX/JrW;-><init>(LX/0Uo;LX/0Uh;LX/0Uh;LX/JqK;LX/JqR;LX/2Sz;LX/JrV;LX/0Or;LX/0Or;LX/6cy;LX/Jrg;Ljava/util/concurrent/ScheduledExecutorService;LX/JqM;LX/2Sv;LX/03V;)V

    .line 2743566
    const/16 v2, 0x29c8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1, v2}, LX/JrW;->a(LX/JrW;LX/0Ot;)V

    .line 2743567
    return-object v1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2743531
    iget-object v0, p0, LX/JrW;->c:LX/0Uh;

    invoke-virtual {v0}, LX/0Uh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2743532
    const-string v0, "MessagesSyncServiceHandler"

    const-string v1, "GKs not initialized yet. Ignore operation %s"

    new-array v2, v3, [Ljava/lang/Object;

    .line 2743533
    iget-object v3, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 2743534
    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2743535
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2743536
    :goto_0
    return-object v0

    .line 2743537
    :cond_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 2743538
    iget-object v0, p0, LX/JrW;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2743539
    const-string v0, "MessagesSyncServiceHandler"

    const-string v2, "Sync protocol disabled. Ignore operation %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2743540
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2743541
    :cond_1
    iget-object v0, p0, LX/JrW;->n:LX/2Sv;

    invoke-virtual {v0}, LX/2Sv;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2743542
    iget-object v0, p0, LX/JrW;->o:LX/03V;

    const-string v2, "MessagesSyncServiceHandler"

    const-string v3, "%s called without a valid logged in user. CallerContext=%s"

    .line 2743543
    iget-object v4, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 2743544
    invoke-static {v3, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743545
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2743546
    :cond_2
    const-string v0, "ensure_sync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2743547
    invoke-static {p1}, LX/2Sz;->d(LX/1qK;)LX/7G7;

    move-result-object v1

    .line 2743548
    iget-object v2, p0, LX/JrW;->e:LX/JqR;

    iget-object v0, p0, LX/JrW;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, LX/JrW;->d:LX/JqK;

    .line 2743549
    iget-object v4, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 2743550
    invoke-virtual {v2, v0, v3, v1, v4}, LX/7G8;->a(ILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2743551
    :cond_3
    const-string v0, "force_full_refresh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2743552
    invoke-static {p1}, LX/2Sz;->c(LX/1qK;)Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;

    move-result-object v0

    .line 2743553
    iget-object v1, p0, LX/JrW;->j:LX/6cy;

    sget-object v2, LX/6cx;->k:LX/2bA;

    invoke-virtual {v1, v2}, LX/48u;->a(LX/0To;)Ljava/lang/String;

    move-result-object v1

    .line 2743554
    iget-object v2, v0, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;->b:Ljava/lang/String;

    invoke-static {v2, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2743555
    iget-object v1, p0, LX/JrW;->e:LX/JqR;

    iget-object v0, v0, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;->a:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 2743556
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2743557
    invoke-virtual {v1, v0, v2}, LX/JqR;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2743558
    :goto_1
    move-object v0, v0

    .line 2743559
    goto/16 :goto_0

    .line 2743560
    :cond_4
    const-string v0, "deltas"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2743561
    invoke-direct {p0, p1}, LX/JrW;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2743562
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743563
    :cond_6
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2743564
    goto :goto_1
.end method
