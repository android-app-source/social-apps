.class public final LX/K2t;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/slideshow/SlideshowEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/SlideshowEditActivity;)V
    .locals 0

    .prologue
    .line 2764693
    iput-object p1, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 5

    .prologue
    .line 2764687
    iget-object v0, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v0, v0, Lcom/facebook/slideshow/SlideshowEditActivity;->s:LX/3l1;

    iget-object v1, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v1, v1, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v1}, LX/K2z;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3l1;->a(I)V

    .line 2764688
    iget-object v0, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v0, v0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2764689
    iget-object v1, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v1, v1, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v1}, LX/K2z;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2764690
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2764691
    iget-object v1, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v1, v1, Lcom/facebook/slideshow/SlideshowEditActivity;->t:LX/1Kf;

    iget-object v2, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    iget-object v2, v2, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v3, 0x3e8

    iget-object v4, p0, LX/K2t;->a:Lcom/facebook/slideshow/SlideshowEditActivity;

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2764692
    return-void
.end method
