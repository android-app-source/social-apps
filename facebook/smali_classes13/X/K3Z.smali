.class public LX/K3Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/0W3;

.field private final d:LX/1Mj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2765853
    const-class v0, LX/K3Z;

    sput-object v0, LX/K3Z;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0W3;LX/1Mj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2765854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2765855
    iput-object p1, p0, LX/K3Z;->b:LX/0Zb;

    .line 2765856
    iput-object p2, p0, LX/K3Z;->c:LX/0W3;

    .line 2765857
    iput-object p3, p0, LX/K3Z;->d:LX/1Mj;

    .line 2765858
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 14

    .prologue
    .line 2765859
    iget-object v0, p0, LX/K3Z;->c:LX/0W3;

    sget-wide v2, LX/0X5;->hg:J

    const/4 v1, 0x1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v4

    .line 2765860
    iget-object v0, p0, LX/K3Z;->c:LX/0W3;

    sget-wide v2, LX/0X5;->hh:J

    const/4 v1, 0x1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v6

    .line 2765861
    if-nez v4, :cond_0

    if-nez v6, :cond_0

    .line 2765862
    const/4 v0, 0x1

    .line 2765863
    :goto_0
    return v0

    .line 2765864
    :cond_0
    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    .line 2765865
    if-eqz v4, :cond_b

    .line 2765866
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-object v4, p0, LX/K3Z;->c:LX/0W3;

    sget-wide v8, LX/0X5;->he:J

    const-wide/32 v10, 0x186a0

    invoke-interface {v4, v8, v9, v10, v11}, LX/0W4;->a(JJ)J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v2, v4

    move-wide v4, v2

    .line 2765867
    :goto_1
    if-eqz v6, :cond_a

    .line 2765868
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-object v2, p0, LX/K3Z;->c:LX/0W3;

    sget-wide v6, LX/0X5;->hf:J

    const-wide/16 v8, 0x3e8

    invoke-interface {v2, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    move-wide v2, v0

    .line 2765869
    :goto_2
    :try_start_0
    const-string v0, "SHA1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 2765870
    iget-object v0, p0, LX/K3Z;->d:LX/1Mj;

    invoke-virtual {v0}, LX/1Mj;->a()[Ljava/security/cert/X509Certificate;

    move-result-object v7

    .line 2765871
    if-nez v7, :cond_1

    .line 2765872
    sget-object v0, LX/K3Z;->a:Ljava/lang/Class;

    const-string v1, "Could not get root certificates"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2765873
    const/4 v0, 0x0

    goto :goto_0

    .line 2765874
    :catch_0
    move-exception v0

    .line 2765875
    sget-object v1, LX/K3Z;->a:Ljava/lang/Class;

    const-string v2, "Device can\'t hash with SHA1"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2765876
    const/4 v0, 0x0

    goto :goto_0

    .line 2765877
    :cond_1
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2765878
    array-length v8, v7

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v8, :cond_3

    aget-object v0, v7, v1

    .line 2765879
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v10

    cmpg-double v9, v10, v4

    if-gez v9, :cond_2

    .line 2765880
    :try_start_1
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "rootca_sample"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2765881
    const-string v10, "rootca_event"

    .line 2765882
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2765883
    const-string v10, "weight"

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double/2addr v12, v4

    double-to-long v12, v12

    invoke-virtual {v9, v10, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2765884
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v0

    .line 2765885
    const-string v10, "hash"

    invoke-virtual {v6, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2765886
    const-string v10, "cert"

    const/4 v11, 0x0

    invoke-static {v0, v11}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2765887
    iget-object v0, p0, LX/K3Z;->b:LX/0Zb;

    invoke-interface {v0, v9}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2765888
    :cond_2
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2765889
    :catch_1
    move-exception v0

    .line 2765890
    sget-object v9, LX/K3Z;->a:Ljava/lang/Class;

    const-string v10, "Failed to encode certificate"

    invoke-static {v9, v10, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 2765891
    :cond_3
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    const/4 v0, 0x1

    .line 2765892
    :goto_5
    if-eqz v0, :cond_9

    .line 2765893
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 2765894
    iget-object v0, p0, LX/K3Z;->d:LX/1Mj;

    invoke-virtual {v0}, LX/1Mj;->b()[[B

    move-result-object v1

    .line 2765895
    if-nez v1, :cond_5

    .line 2765896
    sget-object v0, LX/K3Z;->a:Ljava/lang/Class;

    const-string v1, "Could not get user certificates"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2765897
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2765898
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 2765899
    :cond_5
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2765900
    array-length v3, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v3, :cond_6

    aget-object v8, v1, v0

    .line 2765901
    invoke-virtual {v6, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v8

    .line 2765902
    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2765903
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2765904
    :cond_6
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 2765905
    array-length v8, v7

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v8, :cond_8

    aget-object v0, v7, v1

    .line 2765906
    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-virtual {v9}, LX/0mC;->c()LX/0m9;

    move-result-object v9

    .line 2765907
    :try_start_2
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v0

    .line 2765908
    if-eqz v6, :cond_7

    .line 2765909
    invoke-virtual {v6, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 2765910
    const-string v10, "hash"

    const/4 v11, 0x0

    invoke-static {v0, v11}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2765911
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 2765912
    const-string v10, "userCert"

    invoke-virtual {v9, v10, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;
    :try_end_2
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2765913
    :cond_7
    :goto_8
    invoke-virtual {v3, v9}, LX/162;->a(LX/0lF;)LX/162;

    .line 2765914
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 2765915
    :catch_2
    move-exception v0

    .line 2765916
    sget-object v10, LX/K3Z;->a:Ljava/lang/Class;

    const-string v11, "Failed to encode certificate"

    invoke-static {v10, v11, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 2765917
    :cond_8
    iget-object v0, p0, LX/K3Z;->b:LX/0Zb;

    const-string v1, "rootca_reporter"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2765918
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2765919
    const-string v1, "rootca_event"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2765920
    const-string v1, "weight"

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    div-double v4, v6, v4

    double-to-long v4, v4

    invoke-virtual {v0, v1, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2765921
    const-string v1, "list"

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2765922
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2765923
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_a
    move-wide v2, v0

    goto/16 :goto_2

    :cond_b
    move-wide v4, v2

    goto/16 :goto_1
.end method
