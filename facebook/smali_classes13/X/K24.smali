.class public LX/K24;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2763085
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2763086
    return-void
.end method

.method public static a(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)LX/K20;
    .locals 9
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2sP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)",
            "Lcom/facebook/richdocument/optional/OptionalComposer;"
        }
    .end annotation

    .prologue
    .line 2763082
    new-instance v0, LX/K20;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/K20;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-object v0
.end method

.method public static a(LX/1Ay;)LX/K25;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 2763087
    new-instance v0, LX/K25;

    invoke-direct {v0, p0}, LX/K25;-><init>(LX/1Ay;)V

    return-object v0
.end method

.method public static a(LX/0Ot;)LX/K27;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/K2D;",
            ">;)",
            "Lcom/facebook/richdocument/optional/OptionalPageLiker;"
        }
    .end annotation

    .prologue
    .line 2763083
    new-instance v0, LX/K27;

    invoke-direct {v0, p0}, LX/K27;-><init>(LX/0Ot;)V

    return-object v0
.end method

.method public static a(LX/0Ot;LX/0Ot;)LX/K29;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;"
        }
    .end annotation

    .prologue
    .line 2763084
    new-instance v0, LX/K29;

    invoke-direct {v0, p0, p1}, LX/K29;-><init>(LX/0Ot;LX/0Ot;)V

    return-object v0
.end method

.method public static b(LX/0Ot;)LX/K2B;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)",
            "Lcom/facebook/richdocument/optional/OptionalUFI;"
        }
    .end annotation

    .prologue
    .line 2763079
    new-instance v0, LX/K2B;

    invoke-direct {v0, p0}, LX/K2B;-><init>(LX/0Ot;)V

    return-object v0
.end method

.method public static c(LX/0Ot;)LX/K22;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;)",
            "Lcom/facebook/richdocument/optional/OptionalFriending;"
        }
    .end annotation

    .prologue
    .line 2763080
    new-instance v0, LX/K22;

    invoke-direct {v0, p0}, LX/K22;-><init>(LX/0Ot;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2763081
    return-void
.end method
