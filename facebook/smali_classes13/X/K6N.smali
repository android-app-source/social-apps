.class public final LX/K6N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5m;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardVideo;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V
    .locals 0

    .prologue
    .line 2771429
    iput-object p1, p0, LX/K6N;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardVideo;B)V
    .locals 0

    .prologue
    .line 2771430
    invoke-direct {p0, p1}, LX/K6N;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771431
    iget-object v0, p0, LX/K6N;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setTitleFont(Landroid/graphics/Typeface;)V

    .line 2771432
    return-void
.end method

.method public final b(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 2771433
    iget-object v0, p0, LX/K6N;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionFont(Landroid/graphics/Typeface;)V

    .line 2771434
    return-void
.end method
