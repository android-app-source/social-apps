.class public final LX/K0m;
.super Landroid/view/ViewGroup;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2759546
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2759547
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2759548
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2759549
    invoke-virtual {p0}, LX/K0m;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2759550
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/K0m;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2759551
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/K0m;->setMeasuredDimension(II)V

    .line 2759552
    :goto_0
    return-void

    .line 2759553
    :cond_0
    const-string v0, "RecyclableWrapperView measured but no view attached"

    invoke-static {v0}, LX/0nE;->a(Ljava/lang/String;)Ljava/lang/AssertionError;

    goto :goto_0
.end method
