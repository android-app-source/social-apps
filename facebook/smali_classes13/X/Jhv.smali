.class public LX/Jhv;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6lJ;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/JnW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2725593
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2725594
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2725595
    iput-object v0, p0, LX/Jhv;->a:LX/0Ot;

    .line 2725596
    const-class v0, LX/Jhv;

    invoke-static {v0, p0}, LX/Jhv;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725597
    const v0, 0x7f030369

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2725598
    const v0, 0x7f0d0b28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    iput-object v0, p0, LX/Jhv;->d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    .line 2725599
    iget-object v0, p0, LX/Jhv;->d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    const p1, 0x7f020fd5

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setIcon(I)V

    .line 2725600
    iget-object v0, p0, LX/Jhv;->d:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    const p1, 0x7f08051d

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setText(I)V

    .line 2725601
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/Jhv;

    const/16 v1, 0x2a41

    invoke-static {v2, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2}, LX/JnW;->a(LX/0QB;)LX/JnW;

    move-result-object v2

    check-cast v2, LX/JnW;

    iput-object p0, p1, LX/Jhv;->a:LX/0Ot;

    iput-object v1, p1, LX/Jhv;->b:Ljava/util/concurrent/Executor;

    iput-object v2, p1, LX/Jhv;->c:LX/JnW;

    return-void
.end method
