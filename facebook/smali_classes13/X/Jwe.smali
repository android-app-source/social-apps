.class public LX/Jwe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/bluetooth/BluetoothAdapter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/1SQ;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:Landroid/os/Handler;

.field private f:Landroid/content/BroadcastReceiver;

.field private g:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0YZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;LX/1SQ;Lcom/facebook/content/SecureContextHelper;Landroid/os/Handler;)V
    .locals 1
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2751955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2751956
    iput-object v0, p0, LX/Jwe;->f:Landroid/content/BroadcastReceiver;

    .line 2751957
    iput-object v0, p0, LX/Jwe;->g:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2751958
    new-instance v0, LX/Jwd;

    invoke-direct {v0, p0}, LX/Jwd;-><init>(LX/Jwe;)V

    iput-object v0, p0, LX/Jwe;->h:LX/0YZ;

    .line 2751959
    iput-object p1, p0, LX/Jwe;->a:Landroid/content/Context;

    .line 2751960
    iput-object p2, p0, LX/Jwe;->b:Landroid/bluetooth/BluetoothAdapter;

    .line 2751961
    iput-object p3, p0, LX/Jwe;->c:LX/1SQ;

    .line 2751962
    iput-object p4, p0, LX/Jwe;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2751963
    iput-object p5, p0, LX/Jwe;->e:Landroid/os/Handler;

    .line 2751964
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/Jwe;Z)V
    .locals 5

    .prologue
    .line 2751965
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jwe;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 2751966
    iget-object v0, p0, LX/Jwe;->a:Landroid/content/Context;

    iget-object v1, p0, LX/Jwe;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2751967
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jwe;->f:Landroid/content/BroadcastReceiver;

    .line 2751968
    :cond_0
    iget-object v0, p0, LX/Jwe;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/placetips/pulsarcore/bluetooth/BluetoothTogglerImpl$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/placetips/pulsarcore/bluetooth/BluetoothTogglerImpl$2;-><init>(LX/Jwe;Z)V

    const-wide/16 v2, 0x3e8

    const v4, 0x691311bb

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2751969
    monitor-exit p0

    return-void

    .line 2751970
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/Jwe;Z)V
    .locals 3

    .prologue
    .line 2751971
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jwe;->g:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    .line 2751972
    iget-object v0, p0, LX/Jwe;->g:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x358d8c8f

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2751973
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jwe;->g:Lcom/google/common/util/concurrent/SettableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2751974
    :cond_0
    monitor-exit p0

    return-void

    .line 2751975
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
