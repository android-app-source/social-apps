.class public final LX/Jnr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Jns;

.field private final b:Lcom/facebook/messaging/montage/composer/art/EffectItemView;


# direct methods
.method public constructor <init>(LX/Jns;Lcom/facebook/messaging/montage/composer/art/EffectItemView;)V
    .locals 0

    .prologue
    .line 2734545
    iput-object p1, p0, LX/Jnr;->a:LX/Jns;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734546
    iput-object p2, p0, LX/Jnr;->b:Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    .line 2734547
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/montage/model/art/EffectItem;)V
    .locals 2

    .prologue
    .line 2734548
    iget-object v0, p0, LX/Jnr;->a:LX/Jns;

    iget-object v0, v0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    invoke-virtual {v0, p1}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v0

    .line 2734549
    iget-object v1, p0, LX/Jnr;->b:Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a(LX/Jnw;)V

    .line 2734550
    return-void
.end method

.method public final b(Lcom/facebook/messaging/montage/model/art/EffectItem;)V
    .locals 2

    .prologue
    .line 2734551
    iget-object v0, p0, LX/Jnr;->a:LX/Jns;

    iget-object v0, v0, LX/Jns;->a:Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtPickerPreviewListView;->b:LX/Jnx;

    invoke-virtual {v0, p1}, LX/Jnx;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v0

    .line 2734552
    iget-object v1, p0, LX/Jnr;->b:Lcom/facebook/messaging/montage/composer/art/EffectItemView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/montage/composer/art/EffectItemView;->a(LX/Jnw;)V

    .line 2734553
    return-void
.end method
