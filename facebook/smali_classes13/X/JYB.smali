.class public LX/JYB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JY7;


# direct methods
.method public constructor <init>(LX/JY7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2705939
    iput-object p1, p0, LX/JYB;->a:LX/JY7;

    .line 2705940
    return-void
.end method

.method public static a(LX/0QB;)LX/JYB;
    .locals 4

    .prologue
    .line 2705941
    const-class v1, LX/JYB;

    monitor-enter v1

    .line 2705942
    :try_start_0
    sget-object v0, LX/JYB;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705943
    sput-object v2, LX/JYB;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705946
    new-instance p0, LX/JYB;

    invoke-static {v0}, LX/JY7;->a(LX/0QB;)LX/JY7;

    move-result-object v3

    check-cast v3, LX/JY7;

    invoke-direct {p0, v3}, LX/JYB;-><init>(LX/JY7;)V

    .line 2705947
    move-object v0, p0

    .line 2705948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
