.class public final LX/Jzb;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/5pC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/5pW;


# direct methods
.method private constructor <init>(LX/5pX;ILjava/lang/String;Ljava/lang/String;LX/5pC;LX/5pW;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2756347
    invoke-direct {p0, p1}, LX/5p8;-><init>(LX/5pX;)V

    .line 2756348
    iput-object p1, p0, LX/Jzb;->a:Landroid/content/Context;

    .line 2756349
    iput p2, p0, LX/Jzb;->b:I

    .line 2756350
    iput-object p3, p0, LX/Jzb;->c:Ljava/lang/String;

    .line 2756351
    iput-object p4, p0, LX/Jzb;->d:Ljava/lang/String;

    .line 2756352
    iput-object p5, p0, LX/Jzb;->e:LX/5pC;

    .line 2756353
    iput-object p6, p0, LX/Jzb;->f:LX/5pW;

    .line 2756354
    return-void
.end method

.method public synthetic constructor <init>(LX/5pX;ILjava/lang/String;Ljava/lang/String;LX/5pC;LX/5pW;B)V
    .locals 0

    .prologue
    .line 2756355
    invoke-direct/range {p0 .. p6}, LX/Jzb;-><init>(LX/5pX;ILjava/lang/String;Ljava/lang/String;LX/5pC;LX/5pW;)V

    return-void
.end method

.method private varargs a()V
    .locals 8

    .prologue
    .line 2756356
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "1"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2756357
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2756358
    iget-object v0, p0, LX/Jzb;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2756359
    const-string v0, " AND datetaken < ?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756360
    iget-object v0, p0, LX/Jzb;->c:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2756361
    :cond_0
    iget-object v0, p0, LX/Jzb;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2756362
    const-string v0, " AND bucket_display_name = ?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756363
    iget-object v0, p0, LX/Jzb;->d:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2756364
    :cond_1
    iget-object v0, p0, LX/Jzb;->e:LX/5pC;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Jzb;->e:LX/5pC;

    invoke-interface {v0}, LX/5pC;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 2756365
    const-string v0, " AND mime_type IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756366
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/Jzb;->e:LX/5pC;

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2756367
    const-string v1, "?,"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756368
    iget-object v1, p0, LX/Jzb;->e:LX/5pC;

    invoke-interface {v1, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2756369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2756370
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const-string v2, ")"

    invoke-virtual {v3, v0, v1, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756371
    :cond_3
    new-instance v6, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v6}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2756372
    iget-object v0, p0, LX/Jzb;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2756373
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/Jzf;->b:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "datetaken DESC, date_modified DESC LIMIT "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, LX/Jzb;->b:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2756374
    if-nez v1, :cond_4

    .line 2756375
    iget-object v0, p0, LX/Jzb;->f:LX/5pW;

    const-string v1, "E_UNABLE_TO_LOAD"

    const-string v2, "Could not get photos"

    invoke-interface {v0, v1, v2}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2756376
    :goto_1
    return-void

    .line 2756377
    :cond_4
    :try_start_1
    iget v2, p0, LX/Jzb;->b:I

    invoke-static {v0, v1, v6, v2}, LX/Jzf;->b(Landroid/content/ContentResolver;Landroid/database/Cursor;LX/5pH;I)V

    .line 2756378
    iget v0, p0, LX/Jzb;->b:I

    invoke-static {v1, v6, v0}, LX/Jzf;->b(Landroid/database/Cursor;LX/5pH;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2756379
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2756380
    iget-object v0, p0, LX/Jzb;->f:LX/5pW;

    invoke-interface {v0, v6}, LX/5pW;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 2756381
    :catch_0
    iget-object v0, p0, LX/Jzb;->f:LX/5pW;

    const-string v1, "E_UNABLE_TO_LOAD_PERMISSION"

    const-string v2, "Could not get photos: need READ_EXTERNAL_STORAGE permission"

    invoke-interface {v0, v1, v2}, LX/5pW;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2756382
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2756383
    iget-object v1, p0, LX/Jzb;->f:LX/5pW;

    invoke-interface {v1, v6}, LX/5pW;->a(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2756384
    invoke-direct {p0}, LX/Jzb;->a()V

    return-void
.end method
