.class public final LX/Jnb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jnc;

.field public final synthetic b:Z

.field public final synthetic c:LX/Jnd;


# direct methods
.method public constructor <init>(LX/Jnd;LX/Jnc;Z)V
    .locals 0

    .prologue
    .line 2734198
    iput-object p1, p0, LX/Jnb;->c:LX/Jnd;

    iput-object p2, p0, LX/Jnb;->a:LX/Jnc;

    iput-boolean p3, p0, LX/Jnb;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2734199
    new-instance v0, LX/Jna;

    invoke-direct {v0, p0}, LX/Jna;-><init>(LX/Jnb;)V

    .line 2734200
    packed-switch p2, :pswitch_data_0

    .line 2734201
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected button clicked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734202
    :pswitch_0
    iget-object v1, p0, LX/Jnb;->c:LX/Jnd;

    iget-object v1, v1, LX/Jnd;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->a(ZLX/Jna;)V

    .line 2734203
    :goto_0
    :pswitch_1
    return-void

    .line 2734204
    :pswitch_2
    iget-object v1, p0, LX/Jnb;->c:LX/Jnd;

    iget-object v1, v1, LX/Jnd;->c:Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/messaging/montage/audience/data/MontageAudienceModeHelper;->a(ZLX/Jna;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
