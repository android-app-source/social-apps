.class public final LX/JyI;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JyJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

.field public b:I

.field public final synthetic c:LX/JyJ;


# direct methods
.method public constructor <init>(LX/JyJ;)V
    .locals 1

    .prologue
    .line 2754550
    iput-object p1, p0, LX/JyI;->c:LX/JyJ;

    .line 2754551
    move-object v0, p1

    .line 2754552
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2754553
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2754554
    const-string v0, "DiscoverySubtitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2754555
    if-ne p0, p1, :cond_1

    .line 2754556
    :cond_0
    :goto_0
    return v0

    .line 2754557
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2754558
    goto :goto_0

    .line 2754559
    :cond_3
    check-cast p1, LX/JyI;

    .line 2754560
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2754561
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2754562
    if-eq v2, v3, :cond_0

    .line 2754563
    iget-object v2, p0, LX/JyI;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JyI;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    iget-object v3, p1, LX/JyI;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2754564
    goto :goto_0

    .line 2754565
    :cond_5
    iget-object v2, p1, LX/JyI;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-nez v2, :cond_4

    .line 2754566
    :cond_6
    iget v2, p0, LX/JyI;->b:I

    iget v3, p1, LX/JyI;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2754567
    goto :goto_0
.end method
