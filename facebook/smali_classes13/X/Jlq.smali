.class public LX/Jlq;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2731889
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2731890
    return-void
.end method

.method public static a(LX/Jln;LX/Jlx;LX/0SG;Ljava/util/concurrent/Executor;)LX/Jld;
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 2731891
    new-instance v0, LX/Jld;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jld;-><init>(LX/Jln;LX/Jlx;LX/0SG;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(LX/Jlx;LX/0Xl;)LX/Jln;
    .locals 1
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 2731892
    new-instance v0, LX/Jln;

    invoke-direct {v0, p0, p1}, LX/Jln;-><init>(LX/Jlx;LX/0Xl;)V

    return-object v0
.end method

.method public static b(LX/Jln;LX/Jlx;LX/0SG;Ljava/util/concurrent/Executor;)LX/Jld;
    .locals 1
    .param p0    # LX/Jln;
        .annotation runtime Lcom/facebook/messaging/inbox2/data/common/MessengerMediaService;
        .end annotation
    .end param
    .param p1    # LX/Jlx;
        .annotation runtime Lcom/facebook/messaging/inbox2/data/common/MessengerMediaService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/inbox2/data/common/MessengerMediaService;
    .end annotation

    .prologue
    .line 2731893
    new-instance v0, LX/Jld;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Jld;-><init>(LX/Jln;LX/Jlx;LX/0SG;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static b(LX/Jlx;LX/0Xl;)LX/Jln;
    .locals 1
    .param p0    # LX/Jlx;
        .annotation runtime Lcom/facebook/messaging/inbox2/data/common/MessengerMediaService;
        .end annotation
    .end param
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/inbox2/data/common/MessengerMediaService;
    .end annotation

    .prologue
    .line 2731894
    new-instance v0, LX/Jln;

    invoke-direct {v0, p0, p1}, LX/Jln;-><init>(LX/Jlx;LX/0Xl;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2731895
    return-void
.end method
