.class public final enum LX/Jwp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jwp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jwp;

.field public static final enum FBLE:LX/Jwp;

.field public static final enum PULSAR:LX/Jwp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2752574
    new-instance v0, LX/Jwp;

    const-string v1, "PULSAR"

    invoke-direct {v0, v1, v2}, LX/Jwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jwp;->PULSAR:LX/Jwp;

    .line 2752575
    new-instance v0, LX/Jwp;

    const-string v1, "FBLE"

    invoke-direct {v0, v1, v3}, LX/Jwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jwp;->FBLE:LX/Jwp;

    .line 2752576
    const/4 v0, 0x2

    new-array v0, v0, [LX/Jwp;

    sget-object v1, LX/Jwp;->PULSAR:LX/Jwp;

    aput-object v1, v0, v2

    sget-object v1, LX/Jwp;->FBLE:LX/Jwp;

    aput-object v1, v0, v3

    sput-object v0, LX/Jwp;->$VALUES:[LX/Jwp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2752578
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jwp;
    .locals 1

    .prologue
    .line 2752579
    const-class v0, LX/Jwp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jwp;

    return-object v0
.end method

.method public static values()[LX/Jwp;
    .locals 1

    .prologue
    .line 2752577
    sget-object v0, LX/Jwp;->$VALUES:[LX/Jwp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jwp;

    return-object v0
.end method
