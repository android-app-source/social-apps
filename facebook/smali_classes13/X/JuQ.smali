.class public final LX/JuQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:LX/2HB;

.field public final synthetic b:Lcom/facebook/messaging/notify/NewMessageNotification;

.field public final synthetic c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic d:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;LX/2HB;Lcom/facebook/messaging/notify/NewMessageNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 2748737
    iput-object p1, p0, LX/JuQ;->d:LX/3RG;

    iput-object p2, p0, LX/JuQ;->a:LX/2HB;

    iput-object p3, p0, LX/JuQ;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iput-object p4, p0, LX/JuQ;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0x2739

    .line 2748738
    if-eqz p1, :cond_0

    .line 2748739
    iget-object v0, p0, LX/JuQ;->a:LX/2HB;

    .line 2748740
    iput-object p1, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748741
    :cond_0
    iget-object v0, p0, LX/JuQ;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->m:LX/2bC;

    iget-object v1, p0, LX/JuQ;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v2, p0, LX/JuQ;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iget-object v2, v2, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v2, v2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/JuQ;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    iget-object v3, v3, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v3, v3, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2748742
    iget-object v0, p0, LX/JuQ;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->d:LX/3RK;

    iget-object v1, p0, LX/JuQ;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 2748743
    iget-object v0, p0, LX/JuQ;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->d:LX/3RK;

    iget-object v1, p0, LX/JuQ;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JuQ;->a:LX/2HB;

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v5, v2}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748744
    iget-object v0, p0, LX/JuQ;->b:Lcom/facebook/messaging/notify/NewMessageNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2748745
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2748746
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuQ;->a(Landroid/graphics/Bitmap;)V

    .line 2748747
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2748748
    const/4 v0, 0x0

    .line 2748749
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2748750
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2748751
    :cond_0
    invoke-direct {p0, v0}, LX/JuQ;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748752
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2748753
    return-void

    .line 2748754
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
