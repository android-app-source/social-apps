.class public final LX/Je3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jdw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jdw",
        "<",
        "Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Jdn;

.field public final synthetic c:LX/JeB;


# direct methods
.method public constructor <init>(LX/JeB;Landroid/content/Context;LX/Jdn;)V
    .locals 0

    .prologue
    .line 2720287
    iput-object p1, p0, LX/Je3;->c:LX/JeB;

    iput-object p2, p0, LX/Je3;->a:Landroid/content/Context;

    iput-object p3, p0, LX/Je3;->b:LX/Jdn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JeZ;LX/Jek;)V
    .locals 6

    .prologue
    .line 2720288
    check-cast p2, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 2720289
    check-cast p1, LX/Jea;

    .line 2720290
    iget-object v0, p1, LX/Jea;->a:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 2720291
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->P()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2720292
    iget-object v1, p0, LX/Je3;->a:Landroid/content/Context;

    const v2, 0x7f082b1f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Ljava/lang/String;)V

    .line 2720293
    iget-object v1, p0, LX/Je3;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082b46    # 1.809997E38f

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, LX/JeB;->b(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b(Ljava/lang/String;)V

    .line 2720294
    :goto_0
    new-instance v1, LX/Je2;

    invoke-direct {v1, p0, v0}, LX/Je2;-><init>(LX/Je3;Lcom/facebook/user/model/User;)V

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Landroid/view/View$OnClickListener;)V

    .line 2720295
    return-void

    .line 2720296
    :cond_0
    iget-object v1, p0, LX/Je3;->a:Landroid/content/Context;

    const v2, 0x7f082b1d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Ljava/lang/String;)V

    .line 2720297
    iget-object v1, p0, LX/Je3;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082b45    # 1.8099968E38f

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, LX/JeB;->b(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
