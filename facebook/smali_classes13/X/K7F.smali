.class public final LX/K7F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/tarot/data/FeedbackData;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:LX/K73;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2772880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2772881
    iput-object p1, p0, LX/K7F;->a:Ljava/lang/String;

    .line 2772882
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/tarot/data/TarotCardEndCardData;
    .locals 8

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 2772883
    new-instance v0, Lcom/facebook/tarot/data/TarotCardEndCardData;

    iget-object v1, p0, LX/K7F;->a:Ljava/lang/String;

    new-instance v2, Lcom/facebook/tarot/data/BackgroundImageData;

    iget-object v3, p0, LX/K7F;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/tarot/data/BackgroundImageData;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/facebook/tarot/data/DescriptionData;

    iget-object v4, p0, LX/K7F;->c:Ljava/lang/String;

    iget-object v5, p0, LX/K7F;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6, v6}, Lcom/facebook/tarot/data/DescriptionData;-><init>(Ljava/lang/String;Ljava/lang/String;FF)V

    iget-object v4, p0, LX/K7F;->e:Lcom/facebook/tarot/data/FeedbackData;

    iget-object v5, p0, LX/K7F;->f:Ljava/lang/String;

    iget-object v6, p0, LX/K7F;->g:Ljava/lang/String;

    iget-object v7, p0, LX/K7F;->h:LX/K73;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/tarot/data/TarotCardEndCardData;-><init>(Ljava/lang/String;Lcom/facebook/tarot/data/BackgroundImageData;Lcom/facebook/tarot/data/DescriptionData;Lcom/facebook/tarot/data/FeedbackData;Ljava/lang/String;Ljava/lang/String;LX/K73;)V

    return-object v0
.end method
