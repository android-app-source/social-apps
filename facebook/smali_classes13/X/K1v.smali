.class public final LX/K1v;
.super LX/Chm;
.source ""


# instance fields
.field public final synthetic a:LX/K1w;


# direct methods
.method public constructor <init>(LX/K1w;)V
    .locals 0

    .prologue
    .line 2762896
    iput-object p1, p0, LX/K1v;->a:LX/K1w;

    invoke-direct {p0}, LX/Chm;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2762897
    check-cast p1, LX/CiX;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2762898
    iget-object v0, p1, LX/CiX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2762899
    iget-object v1, p0, LX/K1v;->a:LX/K1w;

    .line 2762900
    iget-object v4, v1, LX/Cos;->a:LX/Ctg;

    move-object v4, v4

    .line 2762901
    move-object v1, v4

    .line 2762902
    if-eq v0, v1, :cond_1

    .line 2762903
    :cond_0
    :goto_0
    return-void

    .line 2762904
    :cond_1
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    iget-boolean v0, v0, LX/K1w;->q:Z

    if-nez v0, :cond_2

    .line 2762905
    iget-object v0, p1, LX/CiX;->b:LX/Cqw;

    move-object v1, v0

    .line 2762906
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    .line 2762907
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v4

    move-object v0, v4

    .line 2762908
    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    .line 2762909
    iget-object v4, v1, LX/Cqw;->f:LX/Cqt;

    move-object v4, v4

    .line 2762910
    iget-object v5, v1, LX/Cqw;->e:LX/Cqu;

    move-object v1, v5

    .line 2762911
    sget-object v5, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v1, v5, :cond_3

    move v1, v2

    .line 2762912
    :goto_1
    iget-object v5, p0, LX/K1v;->a:LX/K1w;

    iget-object v5, v5, LX/K1w;->d:LX/CuL;

    .line 2762913
    iput-object v4, v5, LX/CuL;->g:LX/Cqt;

    .line 2762914
    invoke-virtual {v4}, LX/Cqt;->isLandscape()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2762915
    sget-object v1, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v4, v1, :cond_4

    sget-object v1, LX/7Cm;->RENDER_AXIS_ROTATE_90_RIGHT:LX/7Cm;

    :goto_2
    invoke-virtual {v0, v1}, LX/8wv;->setRotatedRenderAxis(LX/7Cm;)V

    .line 2762916
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    iget-object v0, v0, LX/K1w;->d:LX/CuL;

    invoke-virtual {v0, v2}, LX/CuL;->a(Z)V

    .line 2762917
    :goto_3
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    .line 2762918
    iput-boolean v2, v0, LX/K1w;->q:Z

    .line 2762919
    :cond_2
    iget-boolean v0, p1, LX/CiX;->d:Z

    move v0, v0

    .line 2762920
    if-eqz v0, :cond_0

    .line 2762921
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    .line 2762922
    iput-boolean v3, v0, LX/K1w;->q:Z

    .line 2762923
    goto :goto_0

    :cond_3
    move v1, v3

    .line 2762924
    goto :goto_1

    .line 2762925
    :cond_4
    sget-object v1, LX/7Cm;->RENDER_AXIS_ROTATE_90_LEFT:LX/7Cm;

    goto :goto_2

    .line 2762926
    :cond_5
    sget-object v4, LX/7Cm;->RENDER_AXIS_ROTATE_0_PORTRAIT:LX/7Cm;

    invoke-virtual {v0, v4}, LX/8wv;->setRotatedRenderAxis(LX/7Cm;)V

    .line 2762927
    iget-object v0, p0, LX/K1v;->a:LX/K1w;

    iget-object v0, v0, LX/K1w;->d:LX/CuL;

    invoke-virtual {v0, v1}, LX/CuL;->a(Z)V

    goto :goto_3
.end method
