.class public LX/K8I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/K8I;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/0tX;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2773764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773765
    iput-object p1, p0, LX/K8I;->a:Ljava/util/concurrent/ExecutorService;

    .line 2773766
    iput-object p2, p0, LX/K8I;->b:LX/0tX;

    .line 2773767
    return-void
.end method

.method public static a(LX/0QB;)LX/K8I;
    .locals 5

    .prologue
    .line 2773768
    sget-object v0, LX/K8I;->c:LX/K8I;

    if-nez v0, :cond_1

    .line 2773769
    const-class v1, LX/K8I;

    monitor-enter v1

    .line 2773770
    :try_start_0
    sget-object v0, LX/K8I;->c:LX/K8I;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2773771
    if-eqz v2, :cond_0

    .line 2773772
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2773773
    new-instance p0, LX/K8I;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/K8I;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;)V

    .line 2773774
    move-object v0, p0

    .line 2773775
    sput-object v0, LX/K8I;->c:LX/K8I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2773776
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2773777
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2773778
    :cond_1
    sget-object v0, LX/K8I;->c:LX/K8I;

    return-object v0

    .line 2773779
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2773780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
