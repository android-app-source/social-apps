.class public LX/Jbv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fjq;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2c4;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2c4;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/notification/settings/IsGlobalNotificationPreferenceEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2c4;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717315
    iput-object p1, p0, LX/Jbv;->a:Landroid/content/Context;

    .line 2717316
    iput-object p2, p0, LX/Jbv;->b:LX/2c4;

    .line 2717317
    iput-object p3, p0, LX/Jbv;->d:LX/0Or;

    .line 2717318
    iput-object p4, p0, LX/Jbv;->c:LX/0Or;

    .line 2717319
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2717320
    iget-object v0, p0, LX/Jbv;->b:LX/2c4;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->AUTO_UPDATE_AVAILABLE:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v1}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 2717321
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2717322
    const-string v0, "app_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2717323
    if-nez v0, :cond_0

    .line 2717324
    const-string v0, ""

    .line 2717325
    :cond_0
    iget-object v1, p0, LX/Jbv;->a:Landroid/content/Context;

    const v2, 0x7f0832d7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2717326
    if-nez v1, :cond_1

    .line 2717327
    const-string v1, ""

    .line 2717328
    :cond_1
    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2717329
    iget-object v1, p0, LX/Jbv;->a:Landroid/content/Context;

    const v3, 0x7f0832d8

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2717330
    if-nez v1, :cond_2

    .line 2717331
    const-string v1, ""

    .line 2717332
    :cond_2
    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2717333
    iget-object v1, p0, LX/Jbv;->a:Landroid/content/Context;

    const v4, 0x7f0832d9

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2717334
    if-nez v1, :cond_3

    .line 2717335
    const-string v1, ""

    .line 2717336
    :cond_3
    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2717337
    iget-object v0, p0, LX/Jbv;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAa;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/BAa;->a(J)LX/BAa;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    const v1, 0x7f0218e4

    invoke-virtual {v0, v1}, LX/BAa;->a(I)LX/BAa;

    move-result-object v2

    .line 2717338
    iget-object v0, p0, LX/Jbv;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2717339
    invoke-virtual {v2}, LX/BAa;->a()LX/BAa;

    move-result-object v0

    invoke-virtual {v0}, LX/BAa;->c()LX/BAa;

    move-result-object v0

    invoke-virtual {v0}, LX/BAa;->b()LX/BAa;

    .line 2717340
    :cond_4
    new-instance v5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    .line 2717341
    iget-object v0, p0, LX/Jbv;->b:LX/2c4;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->AUTO_UPDATE_AVAILABLE:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v4, LX/8D4;->ACTIVITY:LX/8D4;

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2717342
    return-void
.end method
