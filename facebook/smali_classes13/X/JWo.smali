.class public LX/JWo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/JWR;

.field public final b:LX/JWd;

.field public final c:LX/0lB;


# direct methods
.method public constructor <init>(LX/JWR;LX/JWd;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703522
    iput-object p1, p0, LX/JWo;->a:LX/JWR;

    .line 2703523
    iput-object p2, p0, LX/JWo;->b:LX/JWd;

    .line 2703524
    iput-object p3, p0, LX/JWo;->c:LX/0lB;

    .line 2703525
    return-void
.end method

.method public static a(LX/0QB;)LX/JWo;
    .locals 6

    .prologue
    .line 2703526
    const-class v1, LX/JWo;

    monitor-enter v1

    .line 2703527
    :try_start_0
    sget-object v0, LX/JWo;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703528
    sput-object v2, LX/JWo;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703529
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703530
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703531
    new-instance p0, LX/JWo;

    invoke-static {v0}, LX/JWR;->a(LX/0QB;)LX/JWR;

    move-result-object v3

    check-cast v3, LX/JWR;

    invoke-static {v0}, LX/JWd;->a(LX/0QB;)LX/JWd;

    move-result-object v4

    check-cast v4, LX/JWd;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-direct {p0, v3, v4, v5}, LX/JWo;-><init>(LX/JWR;LX/JWd;LX/0lB;)V

    .line 2703532
    move-object v0, p0

    .line 2703533
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703534
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703535
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
