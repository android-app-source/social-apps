.class public LX/JW7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JW8;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JW7",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JW8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702378
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2702379
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JW7;->b:LX/0Zi;

    .line 2702380
    iput-object p1, p0, LX/JW7;->a:LX/0Ot;

    .line 2702381
    return-void
.end method

.method public static a(LX/0QB;)LX/JW7;
    .locals 4

    .prologue
    .line 2702382
    const-class v1, LX/JW7;

    monitor-enter v1

    .line 2702383
    :try_start_0
    sget-object v0, LX/JW7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702384
    sput-object v2, LX/JW7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702385
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702386
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702387
    new-instance v3, LX/JW7;

    const/16 p0, 0x20c0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JW7;-><init>(LX/0Ot;)V

    .line 2702388
    move-object v0, v3

    .line 2702389
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702390
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JW7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702391
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702392
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2702393
    check-cast p2, LX/JW6;

    .line 2702394
    iget-object v0, p0, LX/JW7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JW8;

    iget-object v1, p2, LX/JW6;->a:LX/1Pn;

    iget-object v2, p2, LX/JW6;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    const/high16 p0, 0x3f800000    # 1.0f

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 2702395
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0b1057

    invoke-interface {v3, v8, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    iget-object v5, v0, LX/JW8;->a:LX/5JV;

    invoke-virtual {v5, p1}, LX/5JV;->c(LX/1De;)LX/5JT;

    move-result-object v5

    const v6, 0x7f0a00e6

    invoke-virtual {v5, v6}, LX/5JT;->j(I)LX/5JT;

    move-result-object v5

    const v6, 0x7f0214f6

    invoke-virtual {v5, v6}, LX/5JT;->h(I)LX/5JT;

    move-result-object v5

    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    .line 2702396
    iget-object v7, v5, LX/5JT;->a:LX/5JU;

    iput-object v6, v7, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    .line 2702397
    move-object v5, v5

    .line 2702398
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b105e

    invoke-interface {v5, v8, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b105d

    invoke-interface {v5, p2, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    const v7, 0x7f0b105d

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1059

    invoke-interface {v5, v9, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v7, 0x7f0b1059

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    .line 2702399
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2702400
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v6, 0x7f0b0050

    invoke-virtual {v3, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v6, 0x7f0a00e6

    invoke-virtual {v3, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v6, 0x7f0b1063

    invoke-virtual {v3, v6}, LX/1ne;->s(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JW8;->b:LX/1vb;

    invoke-virtual {v4, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v4

    sget-object v5, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v4, v5}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v4

    check-cast v1, LX/1Pk;

    invoke-interface {v1}, LX/1Pk;->e()LX/1SX;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2702401
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2702402
    invoke-static {}, LX/1dS;->b()V

    .line 2702403
    const/4 v0, 0x0

    return-object v0
.end method
