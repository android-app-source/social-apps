.class public final LX/K8C;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/K8F;

.field public final synthetic c:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic d:LX/K8G;


# direct methods
.method public constructor <init>(LX/K8G;Ljava/util/List;LX/K8F;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2773674
    iput-object p1, p0, LX/K8C;->d:LX/K8G;

    iput-object p2, p0, LX/K8C;->a:Ljava/util/List;

    iput-object p3, p0, LX/K8C;->b:LX/K8F;

    iput-object p4, p0, LX/K8C;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2773675
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/K8C;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2773676
    iget-object v1, p0, LX/K8C;->d:LX/K8G;

    iget-object v1, v1, LX/K8G;->h:LX/0QI;

    iget-object v2, p0, LX/K8C;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 2773677
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2773678
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2773679
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2773680
    if-eqz p1, :cond_0

    .line 2773681
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2773682
    if-nez v0, :cond_1

    .line 2773683
    :cond_0
    return-void

    .line 2773684
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2773685
    check-cast v0, Ljava/util/List;

    .line 2773686
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 2773687
    iget-object v1, p0, LX/K8C;->d:LX/K8G;

    iget-object v1, v1, LX/K8G;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Yg;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-result-object v2

    invoke-static {v4, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8Yg;->a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2773688
    iget-object v2, p0, LX/K8C;->b:LX/K8F;

    .line 2773689
    iput-object v1, v2, LX/K8F;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2773690
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2773691
    iget-object v2, p0, LX/K8C;->d:LX/K8G;

    iget-object v2, v2, LX/K8G;->c:LX/8bW;

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->l()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/K8C;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v2

    .line 2773692
    iget-object v4, p0, LX/K8C;->b:LX/K8F;

    .line 2773693
    iput-object v2, v4, LX/K8F;->a:LX/1ca;

    .line 2773694
    iget-object v5, v4, LX/K8F;->a:LX/1ca;

    if-eqz v5, :cond_2

    .line 2773695
    iget-object v5, v4, LX/K8F;->a:LX/1ca;

    new-instance v6, LX/8bV;

    invoke-direct {v6}, LX/8bV;-><init>()V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p1

    invoke-interface {v5, v6, p1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2773696
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2773697
    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 2773698
    if-nez v6, :cond_4

    move-object v2, v4

    .line 2773699
    :goto_1
    move-object v4, v2

    .line 2773700
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 2773701
    iget-object v2, p0, LX/K8C;->d:LX/K8G;

    iget-object v6, v2, LX/K8G;->c:LX/8bW;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v2, v1}, LX/8bW;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2773702
    :cond_3
    iget-object v1, p0, LX/K8C;->b:LX/K8F;

    .line 2773703
    iget-object v2, v1, LX/K8F;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2773704
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    .line 2773705
    :cond_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v7, :cond_7

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;

    .line 2773706
    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestCardsModel;->p()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;

    move-result-object v8

    .line 2773707
    if-eqz v8, :cond_5

    .line 2773708
    invoke-virtual {v8}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->t()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v8}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->t()Ljava/lang/String;

    move-result-object v2

    .line 2773709
    :goto_4
    if-eqz v2, :cond_5

    .line 2773710
    new-instance p1, Landroid/util/Pair;

    invoke-virtual {v8}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p1, v2, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2773711
    :cond_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 2773712
    :cond_6
    invoke-virtual {v8}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotVideoCardModel$VideoModel;->s()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_7
    move-object v2, v4

    .line 2773713
    goto :goto_1
.end method
