.class public final LX/JhB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JhC;


# direct methods
.method public constructor <init>(LX/JhC;)V
    .locals 0

    .prologue
    .line 2724197
    iput-object p1, p0, LX/JhB;->a:LX/JhC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x27b7b547

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2724198
    iget-object v1, p0, LX/JhB;->a:LX/JhC;

    iget-object v1, v1, LX/JhC;->d:LX/JhD;

    .line 2724199
    iget-object v3, v1, LX/JhD;->b:LX/Jh4;

    if-eqz v3, :cond_0

    .line 2724200
    iget-object v3, v1, LX/JhD;->b:LX/Jh4;

    iget-object v4, v1, LX/JhD;->a:Lcom/facebook/user/model/User;

    .line 2724201
    iget-object v5, v3, LX/Jh4;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2724202
    sget-object p0, LX/01T;->MESSENGER:LX/01T;

    iget-object p1, v5, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->o:LX/01T;

    invoke-virtual {p0, p1}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2724203
    invoke-static {v5, v4}, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->b$redex0(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;Lcom/facebook/user/model/User;)V

    .line 2724204
    :cond_0
    :goto_0
    const v1, 0x5b5122fb

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2724205
    :cond_1
    new-instance p0, LX/Jh2;

    invoke-direct {p0, v5, v4}, LX/Jh2;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;Lcom/facebook/user/model/User;)V

    .line 2724206
    new-instance p1, LX/Jh3;

    invoke-direct {p1, v5}, LX/Jh3;-><init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 2724207
    new-instance v1, LX/31Y;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0802e7

    invoke-virtual {v5, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v3, 0x7f080020

    invoke-virtual {v5, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v3, 0x7f080021

    invoke-virtual {v5, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    move-result-object p0

    iput-object p0, v5, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->n:LX/2EJ;

    goto :goto_0
.end method
