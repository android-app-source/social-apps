.class public LX/JeX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JeV;


# instance fields
.field private a:LX/CKT;

.field private b:LX/CK7;


# direct methods
.method public constructor <init>(LX/CKT;LX/CK7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720837
    iput-object p1, p0, LX/JeX;->a:LX/CKT;

    .line 2720838
    iput-object p2, p0, LX/JeX;->b:LX/CK7;

    .line 2720839
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2720840
    iget-object v0, p0, LX/JeX;->a:LX/CKT;

    .line 2720841
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/CKT;->a(Ljava/lang/String;LX/CKR;)V

    .line 2720842
    iget-object v0, p0, LX/JeX;->b:LX/CK7;

    .line 2720843
    iget-object v1, v0, LX/CK7;->a:LX/0Zb;

    const-string v2, "messenger_content_subscription_station_on_subscribe"

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2720844
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2720845
    :goto_0
    return-void

    .line 2720846
    :cond_0
    const-string v2, "station_id"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2720847
    iget-object v0, p0, LX/JeX;->a:LX/CKT;

    .line 2720848
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/CKT;->b(Ljava/lang/String;LX/CKS;)V

    .line 2720849
    iget-object v0, p0, LX/JeX;->b:LX/CK7;

    .line 2720850
    iget-object v1, v0, LX/CK7;->a:LX/0Zb;

    const-string v2, "messenger_content_subscription_station_on_unsubscribe"

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2720851
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2720852
    :goto_0
    return-void

    .line 2720853
    :cond_0
    const-string v2, "station_id"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_0
.end method
