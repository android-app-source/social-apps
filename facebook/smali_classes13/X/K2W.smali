.class public LX/K2W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Dy;


# instance fields
.field private final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;)V
    .locals 0

    .prologue
    .line 2764207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764208
    iput-object p1, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    .line 2764209
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2764197
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->ee_()I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2764206
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->e()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2764205
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->b()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2764204
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->a()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2764203
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->c()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2764202
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->d()I

    move-result v0

    return v0
.end method

.method public final g()D
    .locals 2

    .prologue
    .line 2764201
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->ef_()D

    move-result-wide v0

    return-wide v0
.end method

.method public final h()D
    .locals 2

    .prologue
    .line 2764200
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->j()D

    move-result-wide v0

    return-wide v0
.end method

.method public final i()D
    .locals 2

    .prologue
    .line 2764199
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->k()D

    move-result-wide v0

    return-wide v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 2764198
    iget-object v0, p0, LX/K2W;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$SphericalMetadataModel;->l()D

    move-result-wide v0

    return-wide v0
.end method
