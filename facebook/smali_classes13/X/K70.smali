.class public final enum LX/K70;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K70;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K70;

.field public static final enum SILENTLY_WATCHING_MOVE_EVENTS:LX/K70;

.field public static final enum WAITING_FOR_CONSIDERABLE_MOVE:LX/K70;

.field public static final enum WAITING_FOR_DOWN:LX/K70;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2772407
    new-instance v0, LX/K70;

    const-string v1, "WAITING_FOR_DOWN"

    invoke-direct {v0, v1, v2}, LX/K70;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    .line 2772408
    new-instance v0, LX/K70;

    const-string v1, "WAITING_FOR_CONSIDERABLE_MOVE"

    invoke-direct {v0, v1, v3}, LX/K70;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K70;->WAITING_FOR_CONSIDERABLE_MOVE:LX/K70;

    .line 2772409
    new-instance v0, LX/K70;

    const-string v1, "SILENTLY_WATCHING_MOVE_EVENTS"

    invoke-direct {v0, v1, v4}, LX/K70;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/K70;->SILENTLY_WATCHING_MOVE_EVENTS:LX/K70;

    .line 2772410
    const/4 v0, 0x3

    new-array v0, v0, [LX/K70;

    sget-object v1, LX/K70;->WAITING_FOR_DOWN:LX/K70;

    aput-object v1, v0, v2

    sget-object v1, LX/K70;->WAITING_FOR_CONSIDERABLE_MOVE:LX/K70;

    aput-object v1, v0, v3

    sget-object v1, LX/K70;->SILENTLY_WATCHING_MOVE_EVENTS:LX/K70;

    aput-object v1, v0, v4

    sput-object v0, LX/K70;->$VALUES:[LX/K70;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2772412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K70;
    .locals 1

    .prologue
    .line 2772413
    const-class v0, LX/K70;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K70;

    return-object v0
.end method

.method public static values()[LX/K70;
    .locals 1

    .prologue
    .line 2772411
    sget-object v0, LX/K70;->$VALUES:[LX/K70;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K70;

    return-object v0
.end method
