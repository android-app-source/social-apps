.class public final LX/Jgy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Md;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V
    .locals 0

    .prologue
    .line 2723903
    iput-object p1, p0, LX/Jgy;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/3OQ;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2723904
    instance-of v0, p1, Lcom/facebook/user/model/User;

    if-eqz v0, :cond_2

    .line 2723905
    check-cast p1, Lcom/facebook/user/model/User;

    .line 2723906
    const/4 v0, -0x1

    move v0, v0

    .line 2723907
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, LX/Jgy;->a:Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 2723908
    iget-object v2, v1, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->s:LX/Jgt;

    invoke-virtual {v2}, LX/Jgt;->d()I

    move-result v2

    move v1, v2

    .line 2723909
    if-ge v1, v0, :cond_1

    :cond_0
    new-instance v0, LX/Jgn;

    invoke-direct {v0, p1}, LX/Jgn;-><init>(Lcom/facebook/user/model/User;)V

    .line 2723910
    :goto_0
    new-instance v1, LX/Jgw;

    invoke-direct {v1, p0}, LX/Jgw;-><init>(LX/Jgy;)V

    .line 2723911
    iput-object v1, v0, LX/Jgn;->b:LX/Jgw;

    .line 2723912
    :goto_1
    return-object v0

    .line 2723913
    :cond_1
    new-instance v0, LX/JhQ;

    invoke-direct {v0, p1}, LX/JhQ;-><init>(Lcom/facebook/user/model/User;)V

    goto :goto_0

    .line 2723914
    :cond_2
    instance-of v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_3

    .line 2723915
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2723916
    new-instance v0, LX/Jgq;

    invoke-direct {v0, p1}, LX/Jgq;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2723917
    new-instance v1, LX/Jgx;

    invoke-direct {v1, p0}, LX/Jgx;-><init>(LX/Jgy;)V

    .line 2723918
    iput-object v1, v0, LX/Jgq;->b:LX/Jgx;

    .line 2723919
    goto :goto_1

    .line 2723920
    :cond_3
    sget-object v0, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;->b:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected rowData of type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2723921
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
