.class public LX/Jer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jer;


# instance fields
.field public a:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Jet;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/Jes;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2721123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2721124
    new-instance v0, LX/2sn;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    const-string v2, "db_messages_json.txt"

    const-string v3, "cache_messages_json.txt"

    invoke-direct {v0, v1, v2, v3}, LX/2sn;-><init>(LX/6ek;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ui_messages_json.txt"

    .line 2721125
    iput-object v1, v0, LX/2sn;->d:Ljava/lang/String;

    .line 2721126
    move-object v0, v0

    .line 2721127
    const/4 v1, 0x5

    .line 2721128
    if-ltz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2721129
    iput v1, v0, LX/2sn;->e:I

    .line 2721130
    move-object v0, v0

    .line 2721131
    const/16 v1, 0xa

    .line 2721132
    if-ltz v1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2721133
    iput v1, v0, LX/2sn;->f:I

    .line 2721134
    move-object v0, v0

    .line 2721135
    new-instance v1, LX/Jes;

    invoke-direct {v1, v0}, LX/Jes;-><init>(LX/2sn;)V

    move-object v0, v1

    .line 2721136
    iput-object v0, p0, LX/Jer;->d:LX/Jes;

    .line 2721137
    return-void

    .line 2721138
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2721139
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/Jer;
    .locals 6

    .prologue
    .line 2721140
    sget-object v0, LX/Jer;->e:LX/Jer;

    if-nez v0, :cond_1

    .line 2721141
    const-class v1, LX/Jer;

    monitor-enter v1

    .line 2721142
    :try_start_0
    sget-object v0, LX/Jer;->e:LX/Jer;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2721143
    if-eqz v2, :cond_0

    .line 2721144
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2721145
    new-instance p0, LX/Jer;

    invoke-direct {p0}, LX/Jer;-><init>()V

    .line 2721146
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/Jet;->a(LX/0QB;)LX/Jet;

    move-result-object v5

    check-cast v5, LX/Jet;

    .line 2721147
    iput-object v3, p0, LX/Jer;->b:LX/03V;

    iput-object v4, p0, LX/Jer;->a:LX/0W3;

    iput-object v5, p0, LX/Jer;->c:LX/Jet;

    .line 2721148
    move-object v0, p0

    .line 2721149
    sput-object v0, LX/Jer;->e:LX/Jer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721150
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2721151
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2721152
    :cond_1
    sget-object v0, LX/Jer;->e:LX/Jer;

    return-object v0

    .line 2721153
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2721154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721155
    :try_start_0
    iget-object v0, p0, LX/Jer;->c:LX/Jet;

    iget-object v1, p0, LX/Jer;->d:LX/Jes;

    .line 2721156
    invoke-static {v0, v1}, LX/Jet;->a(LX/Jet;LX/Jes;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2721157
    iget-object v3, v1, LX/Jes;->b:Ljava/lang/String;

    invoke-static {p1, v3, v2}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v2

    .line 2721158
    invoke-static {v0, v1}, LX/Jet;->b(LX/Jet;LX/Jes;)Lorg/json/JSONObject;

    move-result-object v3

    .line 2721159
    iget-object v4, v1, LX/Jes;->c:Ljava/lang/String;

    invoke-static {p1, v4, v3}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v3

    .line 2721160
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2721161
    iget-object v5, v1, LX/Jes;->b:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721162
    iget-object v2, v1, LX/Jes;->c:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721163
    invoke-static {v1}, LX/Jes;->a$redex0(LX/Jes;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2721164
    invoke-static {v0}, LX/Jet;->a(LX/Jet;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2721165
    iget-object v3, v1, LX/Jes;->d:Ljava/lang/String;

    invoke-static {p1, v3, v2}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v2

    .line 2721166
    iget-object v3, v1, LX/Jes;->d:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721167
    :cond_0
    move-object v0, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2721168
    :goto_0
    return-object v0

    .line 2721169
    :catch_0
    move-exception v0

    .line 2721170
    iget-object v1, p0, LX/Jer;->b:LX/03V;

    const-string v2, "ThreadedMessagesExtraFileProvider"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2721171
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721172
    :try_start_0
    iget-object v0, p0, LX/Jer;->c:LX/Jet;

    iget-object v1, p0, LX/Jer;->d:LX/Jes;

    const/4 p0, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2721173
    invoke-static {v0, v1}, LX/Jet;->a(LX/Jet;LX/Jes;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2721174
    iget-object v3, v1, LX/Jes;->b:Ljava/lang/String;

    invoke-static {p1, v3, v2}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v2

    .line 2721175
    new-instance v3, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    iget-object v4, v1, LX/Jes;->b:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "text/plain"

    invoke-direct {v3, v4, v2, v5}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2721176
    invoke-static {v0, v1}, LX/Jet;->b(LX/Jet;LX/Jes;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2721177
    iget-object v4, v1, LX/Jes;->c:Ljava/lang/String;

    invoke-static {p1, v4, v2}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v2

    .line 2721178
    new-instance v4, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    iget-object v5, v1, LX/Jes;->c:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "text/plain"

    invoke-direct {v4, v5, v2, v6}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2721179
    invoke-static {v1}, LX/Jes;->a$redex0(LX/Jes;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2721180
    invoke-static {v0}, LX/Jet;->a(LX/Jet;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2721181
    iget-object v5, v1, LX/Jes;->d:Ljava/lang/String;

    invoke-static {p1, v5, v2}, LX/Jet;->a(Ljava/io/File;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/net/Uri;

    move-result-object v2

    .line 2721182
    new-instance v5, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    iget-object v6, v1, LX/Jes;->d:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v7, "text/plain"

    invoke-direct {v5, v6, v2, v7}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2721183
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    aput-object v3, v2, v8

    aput-object v4, v2, v9

    aput-object v5, v2, p0

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2721184
    :goto_0
    move-object v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2721185
    return-object v0

    .line 2721186
    :catch_0
    move-exception v0

    .line 2721187
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to prepare recent messages for writing"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    new-array v2, p0, [Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    aput-object v3, v2, v8

    aput-object v4, v2, v9

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 2721188
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 2721189
    iget-object v0, p0, LX/Jer;->a:LX/0W3;

    sget-wide v2, LX/0X5;->aT:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
