.class public LX/JqS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public final a:LX/JqN;

.field public final b:LX/28W;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2739070
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JqS;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/JqN;LX/28W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2739071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2739072
    iput-object p1, p0, LX/JqS;->a:LX/JqN;

    .line 2739073
    iput-object p2, p0, LX/JqS;->b:LX/28W;

    .line 2739074
    return-void
.end method

.method public static a(LX/0QB;)LX/JqS;
    .locals 11

    .prologue
    .line 2739075
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2739076
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2739077
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2739078
    if-nez v1, :cond_0

    .line 2739079
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739080
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2739081
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2739082
    sget-object v1, LX/JqS;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2739083
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2739084
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2739085
    :cond_1
    if-nez v1, :cond_4

    .line 2739086
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2739087
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2739088
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2739089
    new-instance v8, LX/JqS;

    .line 2739090
    new-instance v10, LX/JqN;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-static {v0}, LX/2ac;->b(LX/0QB;)LX/2ac;

    move-result-object v7

    check-cast v7, LX/2ac;

    invoke-static {v0}, LX/3gD;->b(LX/0QB;)LX/3gD;

    move-result-object v9

    check-cast v9, LX/3gD;

    const/16 p0, 0x2a65

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v10, v1, v7, v9, p0}, LX/JqN;-><init>(LX/0WJ;LX/2ac;LX/3gD;LX/0Ot;)V

    .line 2739091
    move-object v1, v10

    .line 2739092
    check-cast v1, LX/JqN;

    invoke-static {v0}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v7

    check-cast v7, LX/28W;

    invoke-direct {v8, v1, v7}, LX/JqS;-><init>(LX/JqN;LX/28W;)V

    .line 2739093
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2739094
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2739095
    if-nez v1, :cond_2

    .line 2739096
    sget-object v0, LX/JqS;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqS;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2739097
    :goto_1
    if-eqz v0, :cond_3

    .line 2739098
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739099
    :goto_3
    check-cast v0, LX/JqS;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2739100
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2739101
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2739102
    :catchall_1
    move-exception v0

    .line 2739103
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2739104
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2739105
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2739106
    :cond_2
    :try_start_8
    sget-object v0, LX/JqS;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqS;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
