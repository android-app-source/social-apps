.class public final LX/K5O;
.super LX/K5N;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;)V
    .locals 0

    .prologue
    .line 2770348
    iput-object p1, p0, LX/K5O;->a:Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;

    invoke-direct {p0}, LX/K5N;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 2770349
    check-cast p1, LX/K89;

    .line 2770350
    iget-object v0, p0, LX/K5O;->a:Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;

    iget-object v1, p1, LX/K89;->a:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;

    const/4 v3, 0x0

    .line 2770351
    iget-object v2, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2770352
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2770353
    :cond_0
    :goto_0
    return-void

    .line 2770354
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2770355
    invoke-virtual {v1}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel;->a()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;

    move-result-object v2

    .line 2770356
    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->j()LX/0Px;

    move-result-object v6

    .line 2770357
    invoke-virtual {v2}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotViewerInfoModel$TarotViewerInfoTarotViewerInfoModel;->a()LX/0Px;

    move-result-object v7

    .line 2770358
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2770359
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p0

    move v4, v3

    :goto_1
    if-ge v4, p0, :cond_2

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    .line 2770360
    new-instance p1, LX/K7K;

    invoke-direct {p1, v5, v2}, LX/K7K;-><init>(Landroid/content/Context;Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;)V

    .line 2770361
    invoke-interface {v8, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2770362
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2770363
    :cond_2
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v3, v4, :cond_3

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    .line 2770364
    new-instance v7, LX/K7K;

    invoke-direct {v7, v5, v2}, LX/K7K;-><init>(Landroid/content/Context;Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;)V

    .line 2770365
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2770366
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2770367
    :cond_3
    iget-object v2, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v2, :cond_4

    invoke-static {v8}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2770368
    :cond_4
    :goto_3
    goto :goto_0

    .line 2770369
    :cond_5
    iget-object v2, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/1P1;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2770370
    new-instance v2, LX/K9E;

    iget-object v3, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v4, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->c:LX/K91;

    invoke-direct {v2, v3, v8, v4}, LX/K9E;-><init>(Landroid/support/v7/widget/RecyclerView;Ljava/util/List;LX/K91;)V

    .line 2770371
    iget-object v3, v0, Lcom/facebook/tarot/TarotSubscriptionsManagerFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_3
.end method
