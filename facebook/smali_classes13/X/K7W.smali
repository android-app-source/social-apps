.class public final LX/K7W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:F

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:F

.field public final synthetic i:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;)V
    .locals 2

    .prologue
    .line 2773239
    iput-object p1, p0, LX/K7W;->i:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773240
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, LX/K7W;->a:F

    .line 2773241
    const/high16 v0, 0x437f0000    # 255.0f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/K7W;->b:I

    .line 2773242
    const/high16 v0, 0x42ff0000    # 127.5f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/K7W;->c:I

    .line 2773243
    invoke-virtual {p1}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/K7W;->h:F

    .line 2773244
    return-void
.end method

.method private b()F
    .locals 2

    .prologue
    .line 2773236
    iget v0, p0, LX/K7W;->d:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x0

    .line 2773237
    :goto_0
    iget-object v1, p0, LX/K7W;->i:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    invoke-virtual {v1}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    iget-object v1, p0, LX/K7W;->i:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    invoke-virtual {v1}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/K7W;->i:Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;

    invoke-virtual {v1}, Lcom/facebook/tarot/drawer/TarotHeaderProgressBar;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, LX/K7W;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0

    .line 2773238
    :cond_0
    iget v0, p0, LX/K7W;->d:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2773252
    iget v0, p0, LX/K7W;->e:I

    if-gt p1, v0, :cond_0

    .line 2773253
    iget v0, p0, LX/K7W;->b:I

    .line 2773254
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/K7W;->c:I

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2773250
    invoke-direct {p0}, LX/K7W;->b()F

    move-result v0

    iput v0, p0, LX/K7W;->g:F

    .line 2773251
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2773245
    iput p1, p0, LX/K7W;->d:I

    .line 2773246
    iput p2, p0, LX/K7W;->e:I

    .line 2773247
    const/4 v0, 0x0

    iput v0, p0, LX/K7W;->f:F

    .line 2773248
    invoke-direct {p0}, LX/K7W;->b()F

    move-result v0

    iput v0, p0, LX/K7W;->g:F

    .line 2773249
    return-void
.end method
