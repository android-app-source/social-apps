.class public LX/Jca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jca;


# instance fields
.field public final a:LX/0Zb;

.field private final b:Landroid/content/Context;

.field private final c:LX/03V;

.field public final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/content/Context;LX/03V;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2718064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2718065
    iput-object p1, p0, LX/Jca;->a:LX/0Zb;

    .line 2718066
    iput-object p2, p0, LX/Jca;->b:Landroid/content/Context;

    .line 2718067
    iput-object p3, p0, LX/Jca;->c:LX/03V;

    .line 2718068
    iput-object p4, p0, LX/Jca;->d:LX/0W3;

    .line 2718069
    return-void
.end method

.method public static a(LX/0QB;)LX/Jca;
    .locals 7

    .prologue
    .line 2717990
    sget-object v0, LX/Jca;->e:LX/Jca;

    if-nez v0, :cond_1

    .line 2717991
    const-class v1, LX/Jca;

    monitor-enter v1

    .line 2717992
    :try_start_0
    sget-object v0, LX/Jca;->e:LX/Jca;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2717993
    if-eqz v2, :cond_0

    .line 2717994
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2717995
    new-instance p0, LX/Jca;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Jca;-><init>(LX/0Zb;Landroid/content/Context;LX/03V;LX/0W3;)V

    .line 2717996
    move-object v0, p0

    .line 2717997
    sput-object v0, LX/Jca;->e:LX/Jca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717998
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2717999
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2718000
    :cond_1
    sget-object v0, LX/Jca;->e:LX/Jca;

    return-object v0

    .line 2718001
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2718002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Jca;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2718062
    iget-object v0, p0, LX/Jca;->c:LX/03V;

    const-string v1, "MalwareScanner"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718063
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 12

    .prologue
    .line 2718003
    iget-object v0, p0, LX/Jca;->b:Landroid/content/Context;

    .line 2718004
    const/4 v1, 0x0

    .line 2718005
    const/16 v2, 0x1040

    .line 2718006
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 2718007
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    move-object v1, v3
    :try_end_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2718008
    :goto_0
    move-object v1, v1

    .line 2718009
    if-eqz v1, :cond_9

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 2718010
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 2718011
    const/4 v6, 0x0

    .line 2718012
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "android_malware_scanner_send_installed_packages_event"

    invoke-direct {v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2718013
    const-string v5, "name"

    iget-object v8, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v5, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718014
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 2718015
    if-eqz v5, :cond_0

    .line 2718016
    const-string v8, "size"

    new-instance v9, Ljava/io/File;

    iget-object v10, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718017
    const-string v8, "target_sdk_version"

    iget v9, v5, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718018
    :cond_0
    const-string v8, "version_name"

    iget-object v9, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718019
    const-string v8, "version_code"

    iget v9, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718020
    :try_start_1
    iget-object v8, p0, LX/Jca;->d:LX/0W3;

    sget-wide v9, LX/0X5;->fz:J

    const/4 v11, 0x0

    invoke-interface {v8, v9, v10, v11}, LX/0W4;->a(JZ)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2718021
    const-string v8, "md5_checksum_of_apk"

    invoke-static {v5}, LX/Jcb;->a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718022
    :cond_1
    iget-object v8, p0, LX/Jca;->d:LX/0W3;

    sget-wide v9, LX/0X5;->fA:J

    const/4 v11, 0x0

    invoke-interface {v8, v9, v10, v11}, LX/0W4;->a(JZ)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2718023
    const-string v8, "sha1_checksum_of_apk"

    invoke-static {v5}, LX/Jcb;->b(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2718024
    :cond_2
    :goto_2
    const-string v5, "install_date"

    iget-wide v9, v3, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-virtual {v7, v5, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718025
    const-string v5, "last_update_time"

    iget-wide v9, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v7, v5, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718026
    iget-object v8, v3, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 2718027
    if-eqz v8, :cond_4

    array-length v5, v8

    if-eqz v5, :cond_4

    .line 2718028
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 2718029
    array-length v10, v8

    move v5, v6

    :goto_3
    if-ge v5, v10, :cond_3

    aget-object v11, v8, v5

    .line 2718030
    invoke-virtual {v9, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2718031
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 2718032
    :catch_0
    move-exception v5

    .line 2718033
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Exception getting checksum of apk: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    goto :goto_2

    .line 2718034
    :catch_1
    move-exception v5

    .line 2718035
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Exception getting checksum of apk: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    goto :goto_2

    .line 2718036
    :cond_3
    const-string v5, "requested_permissions"

    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718037
    :cond_4
    iget-object v8, v3, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    .line 2718038
    if-eqz v8, :cond_6

    array-length v5, v8

    if-eqz v5, :cond_6

    .line 2718039
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 2718040
    array-length v10, v8

    move v5, v6

    :goto_4
    if-ge v5, v10, :cond_5

    aget-object v11, v8, v5

    .line 2718041
    iget-object v11, v11, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2718042
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 2718043
    :cond_5
    const-string v5, "permissions"

    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718044
    :cond_6
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v5, :cond_8

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v5, v5

    if-eqz v5, :cond_8

    .line 2718045
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 2718046
    iget-object v9, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v10, v9

    move v5, v6

    :goto_5
    if-ge v5, v10, :cond_7

    aget-object v6, v9, v5

    .line 2718047
    invoke-virtual {v6}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2718048
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 2718049
    :cond_7
    const-string v5, "signature"

    invoke-virtual {v8}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2718050
    :cond_8
    iget-object v5, p0, LX/Jca;->a:LX/0Zb;

    invoke-interface {v5, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2718051
    goto/16 :goto_1

    .line 2718052
    :cond_9
    const/4 v0, 0x1

    return v0

    .line 2718053
    :catch_2
    move-exception v2

    .line 2718054
    invoke-virtual {v2}, Landroid/os/TransactionTooLargeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    .line 2718055
    const/16 v2, 0x1040

    :try_start_2
    invoke-static {v0, v2}, LX/Jcc;->b(Landroid/content/Context;I)Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_5

    move-result-object v1

    goto/16 :goto_0

    .line 2718056
    :catch_3
    move-exception v2

    .line 2718057
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2718058
    :catch_4
    move-exception v2

    .line 2718059
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2718060
    :catch_5
    move-exception v2

    .line 2718061
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/Jca;->a(LX/Jca;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
