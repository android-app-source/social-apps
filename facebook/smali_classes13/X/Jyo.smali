.class public LX/Jyo;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;)V
    .locals 0

    .prologue
    .line 2755450
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2755451
    iput-object p1, p0, LX/Jyo;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    .line 2755452
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2755472
    new-instance v0, LX/Jym;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Jym;-><init>(Landroid/content/Context;)V

    .line 2755473
    new-instance v1, LX/Jyn;

    invoke-direct {v1, v0}, LX/Jyn;-><init>(LX/Jym;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2755454
    iget-object v0, p0, LX/Jyo;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;

    .line 2755455
    iget-object v1, p0, LX/Jyo;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v1}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2755456
    check-cast p1, LX/Jyn;

    .line 2755457
    iget-object v2, p1, LX/Jyn;->l:LX/Jym;

    move-object v2, v2

    .line 2755458
    if-nez v0, :cond_0

    .line 2755459
    :goto_0
    return-void

    .line 2755460
    :cond_0
    iget-object v3, v2, LX/Jym;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v4, v2, LX/Jym;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    move-result-object p0

    invoke-static {p0}, LX/JEN;->a(Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;)I

    move-result p0

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2755461
    iget-object v3, v2, LX/Jym;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2755462
    iget-object v3, v2, LX/Jym;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2755463
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2755464
    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel$SummaryInfoModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 2755465
    if-eqz v4, :cond_1

    .line 2755466
    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2755467
    const-string p0, "  "

    invoke-virtual {v3, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2755468
    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2755469
    new-instance p0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v2}, LX/Jym;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00d2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-direct {p0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 p1, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 p2, 0x12

    invoke-virtual {v3, p0, p1, v4, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2755470
    :cond_1
    iget-object v4, v2, LX/Jym;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2755471
    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2755453
    iget-object v0, p0, LX/Jyo;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
