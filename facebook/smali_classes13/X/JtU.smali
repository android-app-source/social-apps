.class public LX/JtU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile g:LX/JtU;


# instance fields
.field public b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/JtG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/JtZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2746854
    const-class v0, LX/JtU;

    sput-object v0, LX/JtU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2746786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746787
    return-void
.end method

.method public static a(LX/0QB;)LX/JtU;
    .locals 10

    .prologue
    .line 2746830
    sget-object v0, LX/JtU;->g:LX/JtU;

    if-nez v0, :cond_1

    .line 2746831
    const-class v1, LX/JtU;

    monitor-enter v1

    .line 2746832
    :try_start_0
    sget-object v0, LX/JtU;->g:LX/JtU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2746833
    if-eqz v2, :cond_0

    .line 2746834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2746835
    new-instance v3, LX/JtU;

    invoke-direct {v3}, LX/JtU;-><init>()V

    .line 2746836
    new-instance v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    invoke-direct {v4}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;-><init>()V

    .line 2746837
    const/16 v5, 0x1204

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/8jI;->a(LX/0QB;)LX/8jI;

    move-result-object v6

    check-cast v6, LX/8jI;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v8

    check-cast v8, LX/1HI;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object p0

    check-cast p0, LX/1FZ;

    .line 2746838
    iput-object v5, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->c:LX/0Or;

    iput-object v6, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->d:LX/8jI;

    iput-object v7, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->f:LX/1HI;

    iput-object v9, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->g:Ljava/util/concurrent/ExecutorService;

    iput-object p0, v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->h:LX/1FZ;

    .line 2746839
    move-object v4, v4

    .line 2746840
    check-cast v4, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    .line 2746841
    new-instance v8, LX/JtG;

    invoke-direct {v8}, LX/JtG;-><init>()V

    .line 2746842
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/JtZ;->a(LX/0QB;)LX/JtZ;

    move-result-object v7

    check-cast v7, LX/JtZ;

    .line 2746843
    iput-object v5, v8, LX/JtG;->b:LX/0SG;

    iput-object v6, v8, LX/JtG;->c:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v8, LX/JtG;->d:LX/JtZ;

    .line 2746844
    move-object v5, v8

    .line 2746845
    check-cast v5, LX/JtG;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/JtZ;->a(LX/0QB;)LX/JtZ;

    move-result-object v8

    check-cast v8, LX/JtZ;

    .line 2746846
    iput-object v4, v3, LX/JtU;->b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iput-object v5, v3, LX/JtU;->c:LX/JtG;

    iput-object v6, v3, LX/JtU;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v3, LX/JtU;->e:LX/0tX;

    iput-object v8, v3, LX/JtU;->f:LX/JtZ;

    .line 2746847
    move-object v0, v3

    .line 2746848
    sput-object v0, LX/JtU;->g:LX/JtU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746849
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2746850
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2746851
    :cond_1
    sget-object v0, LX/JtU;->g:LX/JtU;

    return-object v0

    .line 2746852
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2746853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/JtU;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/JtT;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2746799
    const-string v0, "/mru_stickers"

    move-object v0, v0

    .line 2746800
    invoke-static {v0}, LX/KAx;->a(Ljava/lang/String;)LX/KAx;

    move-result-object v5

    .line 2746801
    iget-object v0, v5, LX/KAx;->b:LX/KAr;

    move-object v6, v0

    .line 2746802
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 2746803
    new-array v8, v7, [Ljava/lang/String;

    move v4, v2

    move v1, v2

    .line 2746804
    :goto_0
    if-ge v4, v7, :cond_1

    .line 2746805
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JtT;

    .line 2746806
    iget-object v9, v0, LX/JtT;->b:[Lcom/google/android/gms/wearable/Asset;

    move v3, v1

    move v1, v2

    .line 2746807
    :goto_1
    array-length v10, v9

    if-ge v1, v10, :cond_0

    .line 2746808
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "frame_"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aget-object v11, v9, v1

    invoke-virtual {v6, v10, v11}, LX/KAr;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V

    .line 2746809
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2746810
    :cond_0
    iget-object v0, v0, LX/JtT;->a:Ljava/lang/String;

    aput-object v0, v8, v4

    .line 2746811
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v1, v3

    goto :goto_0

    .line 2746812
    :cond_1
    const-string v0, "sticker_ids"

    invoke-virtual {v6, v0, v8}, LX/KAr;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2746813
    invoke-static {v8}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 2746814
    invoke-virtual {v5}, LX/KAx;->c()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v0

    .line 2746815
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataRequest;->g()Lcom/google/android/gms/wearable/PutDataRequest;

    .line 2746816
    iget-object v1, p0, LX/JtU;->f:LX/JtZ;

    invoke-virtual {v1}, LX/JtZ;->a()LX/2wX;

    move-result-object v1

    .line 2746817
    :try_start_0
    invoke-virtual {v1}, LX/2wX;->f()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v2

    .line 2746818
    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2746819
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v2, "Connection to Google API failed"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2746820
    invoke-virtual {v1}, LX/2wX;->g()V

    .line 2746821
    :goto_2
    return-void

    .line 2746822
    :cond_2
    :try_start_1
    sget-object v2, LX/KB1;->a:LX/KAl;

    invoke-interface {v2, v1, v0}, LX/KAl;->a(LX/2wX;Lcom/google/android/gms/wearable/PutDataRequest;)LX/2wg;

    move-result-object v0

    .line 2746823
    invoke-virtual {v0}, LX/2wg;->a()LX/2NW;

    move-result-object v0

    check-cast v0, LX/KAj;

    .line 2746824
    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2746825
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v2, "updateRecentlyUsedStickerDataItem: putDataItem() failure"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2746826
    :cond_3
    invoke-virtual {v1}, LX/2wX;->g()V

    goto :goto_2

    .line 2746827
    :catch_0
    move-exception v0

    .line 2746828
    :try_start_2
    sget-object v2, LX/JtU;->a:Ljava/lang/Class;

    const-string v3, "updateRecentlyUsedStickerDataItem"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2746829
    invoke-virtual {v1}, LX/2wX;->g()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2wX;->g()V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/JtS;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/JtT;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2746788
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 2746789
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2746790
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JtS;

    .line 2746791
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2746792
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2746793
    iget-object v4, p0, LX/JtU;->c:LX/JtG;

    .line 2746794
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v5

    .line 2746795
    iget-object v6, v4, LX/JtG;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;

    invoke-direct {v7, v4, v0, v5}, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$1;-><init>(LX/JtG;LX/JtS;Lcom/google/common/util/concurrent/SettableFuture;)V

    const p1, -0x37bd1148

    invoke-static {v6, v7, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2746796
    move-object v4, v5

    .line 2746797
    new-instance v5, LX/JtR;

    invoke-direct {v5, p0, v3, v0}, LX/JtR;-><init>(LX/JtU;Lcom/google/common/util/concurrent/SettableFuture;LX/JtS;)V

    iget-object v0, p0, LX/JtU;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2746798
    :cond_0
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
