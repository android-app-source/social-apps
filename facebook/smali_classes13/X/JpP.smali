.class public final LX/JpP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Do1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Do1",
        "<",
        "LX/JpS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6ek;

.field public final synthetic b:LX/JpT;


# direct methods
.method public constructor <init>(LX/JpT;LX/6ek;)V
    .locals 0

    .prologue
    .line 2737352
    iput-object p1, p0, LX/JpP;->b:LX/JpT;

    iput-object p2, p0, LX/JpP;->a:LX/6ek;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;LX/0Rf;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/JpS;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 2737353
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2737354
    new-instance v0, LX/Dnz;

    invoke-direct {v0, p2}, LX/Dnz;-><init>(Ljava/util/Collection;)V

    throw v0

    .line 2737355
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2737356
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2737357
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2737358
    const-wide/high16 v0, -0x8000000000000000L

    .line 2737359
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2737360
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2737361
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737362
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737363
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    invoke-static {v6, v1}, LX/JpT;->b(Ljava/util/HashMap;LX/0Px;)V

    .line 2737364
    iget-wide v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 2737365
    goto :goto_0

    .line 2737366
    :cond_1
    sget-object v0, LX/JpS;->FACEBOOK:LX/JpS;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2737367
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2737368
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2737369
    const/4 v7, 0x0

    invoke-direct {v0, v1, v7}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2737370
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v0

    invoke-static {v5}, Lcom/facebook/fbservice/results/DataFetchDisposition;->buildCombinedDisposition(Ljava/util/List;)Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v1

    .line 2737371
    iput-object v1, v0, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2737372
    move-object v0, v0

    .line 2737373
    iget-object v1, p0, LX/JpP;->a:LX/6ek;

    .line 2737374
    iput-object v1, v0, LX/6iK;->b:LX/6ek;

    .line 2737375
    move-object v0, v0

    .line 2737376
    invoke-static {v4}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v1

    .line 2737377
    iput-object v1, v0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2737378
    move-object v0, v0

    .line 2737379
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2737380
    iput-object v1, v0, LX/6iK;->d:Ljava/util/List;

    .line 2737381
    move-object v0, v0

    .line 2737382
    iput-wide v2, v0, LX/6iK;->j:J

    .line 2737383
    move-object v0, v0

    .line 2737384
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
