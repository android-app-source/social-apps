.class public LX/JzS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5sA;


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Jyw;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Jz0;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Jyw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/react/animated/EventAnimationDriver;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/5rN;

.field private g:I


# direct methods
.method public constructor <init>(LX/5rQ;)V
    .locals 2

    .prologue
    .line 2756176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756177
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    .line 2756178
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    .line 2756179
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    .line 2756180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/JzS;->d:Ljava/util/Map;

    .line 2756181
    const/4 v0, 0x0

    iput v0, p0, LX/JzS;->g:I

    .line 2756182
    iget-object v0, p1, LX/5rQ;->c:LX/5rN;

    move-object v0, v0

    .line 2756183
    iput-object v0, p0, LX/JzS;->f:LX/5rN;

    .line 2756184
    iget-object v0, p1, LX/5rQ;->a:LX/5s9;

    move-object v0, v0

    .line 2756185
    invoke-virtual {v0, p0}, LX/5s9;->a(LX/5sA;)V

    .line 2756186
    invoke-virtual {p1}, LX/5p5;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "customDirectEventTypes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2756187
    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LX/JzS;->e:Ljava/util/Map;

    .line 2756188
    return-void
.end method


# virtual methods
.method public final a(I)LX/Jyw;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2756175
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    return-object v0
.end method

.method public final a(ID)V
    .locals 4

    .prologue
    .line 2756169
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756170
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2756171
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v1, v0

    .line 2756172
    check-cast v1, LX/Jyx;

    iput-wide p2, v1, LX/Jyx;->e:D

    .line 2756173
    iget-object v1, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756174
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 2756160
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756161
    if-nez v0, :cond_0

    .line 2756162
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756163
    :cond_0
    iget-object v1, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jyw;

    .line 2756164
    if-nez v1, :cond_1

    .line 2756165
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756166
    :cond_1
    invoke-virtual {v0, v1}, LX/Jyw;->a(LX/Jyw;)V

    .line 2756167
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756168
    return-void
.end method

.method public final a(IILX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 4

    .prologue
    .line 2756142
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756143
    if-nez v0, :cond_0

    .line 2756144
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756145
    :cond_0
    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2756146
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node should be of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/Jyx;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756147
    :cond_1
    const-string v1, "type"

    invoke-interface {p3, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2756148
    const-string v2, "frames"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2756149
    new-instance v1, LX/Jz4;

    invoke-direct {v1, p3}, LX/Jz4;-><init>(LX/5pG;)V

    .line 2756150
    :goto_0
    iput p1, v1, LX/Jz0;->d:I

    .line 2756151
    iput-object p4, v1, LX/Jz0;->c:Lcom/facebook/react/bridge/Callback;

    .line 2756152
    check-cast v0, LX/Jyx;

    iput-object v0, v1, LX/Jz0;->b:LX/Jyx;

    .line 2756153
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756154
    return-void

    .line 2756155
    :cond_2
    const-string v2, "spring"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2756156
    new-instance v1, LX/JzV;

    invoke-direct {v1, p3}, LX/JzV;-><init>(LX/5pG;)V

    goto :goto_0

    .line 2756157
    :cond_3
    const-string v2, "decay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2756158
    new-instance v1, LX/Jz1;

    invoke-direct {v1, p3}, LX/Jz1;-><init>(LX/5pG;)V

    goto :goto_0

    .line 2756159
    :cond_4
    new-instance v0, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported animation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ILX/5pG;)V
    .locals 4

    .prologue
    .line 2756114
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2756115
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756116
    :cond_0
    const-string v0, "type"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2756117
    const-string v1, "style"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2756118
    new-instance v0, LX/JzW;

    invoke-direct {v0, p2, p0}, LX/JzW;-><init>(LX/5pG;LX/JzS;)V

    .line 2756119
    :goto_0
    iput p1, v0, LX/Jyw;->d:I

    .line 2756120
    iget-object v1, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756121
    iget-object v1, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756122
    return-void

    .line 2756123
    :cond_1
    const-string v1, "value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2756124
    new-instance v0, LX/Jyx;

    invoke-direct {v0, p2}, LX/Jyx;-><init>(LX/5pG;)V

    goto :goto_0

    .line 2756125
    :cond_2
    const-string v1, "props"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2756126
    new-instance v0, LX/JzT;

    invoke-direct {v0, p2, p0}, LX/JzT;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756127
    :cond_3
    const-string v1, "interpolation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2756128
    new-instance v0, LX/Jz5;

    invoke-direct {v0, p2}, LX/Jz5;-><init>(LX/5pG;)V

    goto :goto_0

    .line 2756129
    :cond_4
    const-string v1, "addition"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2756130
    new-instance v0, LX/Jyy;

    invoke-direct {v0, p2, p0}, LX/Jyy;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756131
    :cond_5
    const-string v1, "division"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2756132
    new-instance v0, LX/Jz3;

    invoke-direct {v0, p2, p0}, LX/Jz3;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756133
    :cond_6
    const-string v1, "multiplication"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2756134
    new-instance v0, LX/Jz7;

    invoke-direct {v0, p2, p0}, LX/Jz7;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756135
    :cond_7
    const-string v1, "modulus"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2756136
    new-instance v0, LX/Jz6;

    invoke-direct {v0, p2, p0}, LX/Jz6;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756137
    :cond_8
    const-string v1, "diffclamp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2756138
    new-instance v0, LX/Jz2;

    invoke-direct {v0, p2, p0}, LX/Jz2;-><init>(LX/5pG;LX/JzS;)V

    goto :goto_0

    .line 2756139
    :cond_9
    const-string v1, "transform"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2756140
    new-instance v0, LX/Jza;

    invoke-direct {v0, p2, p0}, LX/Jza;-><init>(LX/5pG;LX/JzS;)V

    goto/16 :goto_0

    .line 2756141
    :cond_a
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported node type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(ILX/Jyz;)V
    .locals 3

    .prologue
    .line 2756108
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756109
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2756110
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756111
    :cond_1
    check-cast v0, LX/Jyx;

    .line 2756112
    iput-object p2, v0, LX/Jyx;->g:LX/Jyz;

    .line 2756113
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2756106
    iget-object v0, p0, LX/JzS;->d:Ljava/util/Map;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2756107
    return-void
.end method

.method public final a(ILjava/lang/String;LX/5pG;)V
    .locals 5

    .prologue
    .line 2756092
    const-string v0, "animatedValueTag"

    invoke-interface {p3, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2756093
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756094
    if-nez v0, :cond_0

    .line 2756095
    new-instance v0, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Animated node with tag "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756096
    :cond_0
    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2756097
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node connected to event should beof type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/Jyx;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756098
    :cond_1
    const-string v1, "nativeEventPath"

    invoke-interface {p3, v1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v2

    .line 2756099
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, LX/5pC;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2756100
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, LX/5pC;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 2756101
    invoke-interface {v2, v1}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2756102
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2756103
    :cond_2
    new-instance v1, Lcom/facebook/react/animated/EventAnimationDriver;

    check-cast v0, LX/Jyx;

    invoke-direct {v1, v3, v0}, Lcom/facebook/react/animated/EventAnimationDriver;-><init>(Ljava/util/List;LX/Jyx;)V

    .line 2756104
    iget-object v0, p0, LX/JzS;->d:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2756105
    return-void
.end method

.method public final a(J)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2756012
    invoke-static {}, LX/5pe;->b()V

    .line 2756013
    iget v0, p0, LX/JzS;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JzS;->g:I

    .line 2756014
    iget v0, p0, LX/JzS;->g:I

    if-nez v0, :cond_0

    .line 2756015
    iget v0, p0, LX/JzS;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JzS;->g:I

    .line 2756016
    :cond_0
    new-instance v8, Ljava/util/ArrayDeque;

    invoke-direct {v8}, Ljava/util/ArrayDeque;-><init>()V

    move v1, v2

    move v3, v2

    .line 2756017
    :goto_0
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2756018
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756019
    iget v4, v0, LX/Jyw;->c:I

    iget v6, p0, LX/JzS;->g:I

    if-eq v4, v6, :cond_1

    .line 2756020
    iget v4, p0, LX/JzS;->g:I

    iput v4, v0, LX/Jyw;->c:I

    .line 2756021
    add-int/lit8 v3, v3, 0x1

    .line 2756022
    invoke-interface {v8, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756023
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    move v4, v3

    move v3, v2

    .line 2756024
    :goto_1
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 2756025
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jz0;

    .line 2756026
    invoke-virtual {v0, p1, p2}, LX/Jz0;->a(J)V

    .line 2756027
    iget-object v6, v0, LX/Jz0;->b:LX/Jyx;

    .line 2756028
    iget v7, v6, LX/Jyw;->c:I

    iget v9, p0, LX/JzS;->g:I

    if-eq v7, v9, :cond_3

    .line 2756029
    iget v7, p0, LX/JzS;->g:I

    iput v7, v6, LX/Jyw;->c:I

    .line 2756030
    add-int/lit8 v4, v4, 0x1

    .line 2756031
    invoke-interface {v8, v6}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756032
    :cond_3
    iget-boolean v0, v0, LX/Jz0;->a:Z

    if-eqz v0, :cond_4

    move v3, v5

    .line 2756033
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    move v6, v4

    :cond_6
    move v4, v6

    .line 2756034
    :cond_7
    invoke-interface {v8}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2756035
    invoke-interface {v8}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/Jyw;

    .line 2756036
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    if-eqz v0, :cond_5

    move v6, v4

    move v4, v2

    .line 2756037
    :goto_2
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 2756038
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756039
    iget v7, v0, LX/Jyw;->b:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v0, LX/Jyw;->b:I

    .line 2756040
    iget v7, v0, LX/Jyw;->c:I

    iget v9, p0, LX/JzS;->g:I

    if-eq v7, v9, :cond_8

    .line 2756041
    iget v7, p0, LX/JzS;->g:I

    iput v7, v0, LX/Jyw;->c:I

    .line 2756042
    add-int/lit8 v6, v6, 0x1

    .line 2756043
    invoke-interface {v8, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756044
    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2756045
    :cond_9
    iget v0, p0, LX/JzS;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JzS;->g:I

    .line 2756046
    iget v0, p0, LX/JzS;->g:I

    if-nez v0, :cond_a

    .line 2756047
    iget v0, p0, LX/JzS;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JzS;->g:I

    :cond_a
    move v1, v2

    move v6, v2

    .line 2756048
    :goto_3
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 2756049
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756050
    iget v7, v0, LX/Jyw;->b:I

    if-nez v7, :cond_b

    iget v7, v0, LX/Jyw;->c:I

    iget v9, p0, LX/JzS;->g:I

    if-eq v7, v9, :cond_b

    .line 2756051
    iget v7, p0, LX/JzS;->g:I

    iput v7, v0, LX/Jyw;->c:I

    .line 2756052
    add-int/lit8 v6, v6, 0x1

    .line 2756053
    invoke-interface {v8, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756054
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_c
    move v1, v2

    .line 2756055
    :goto_4
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 2756056
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jz0;

    .line 2756057
    iget-object v0, v0, LX/Jz0;->b:LX/Jyx;

    .line 2756058
    iget v7, v0, LX/Jyw;->b:I

    if-nez v7, :cond_d

    iget v7, v0, LX/Jyw;->c:I

    iget v9, p0, LX/JzS;->g:I

    if-eq v7, v9, :cond_d

    .line 2756059
    iget v7, p0, LX/JzS;->g:I

    iput v7, v0, LX/Jyw;->c:I

    .line 2756060
    add-int/lit8 v6, v6, 0x1

    .line 2756061
    invoke-interface {v8, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756062
    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_e
    move v7, v6

    :cond_f
    move v6, v7

    .line 2756063
    :cond_10
    invoke-interface {v8}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 2756064
    invoke-interface {v8}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jyw;

    .line 2756065
    invoke-virtual {v1}, LX/Jyw;->a()V

    .line 2756066
    instance-of v0, v1, LX/JzT;

    if-eqz v0, :cond_11

    move-object v0, v1

    .line 2756067
    check-cast v0, LX/JzT;

    iget-object v7, p0, LX/JzS;->f:LX/5rN;

    invoke-virtual {v0, v7}, LX/JzT;->a(LX/5rN;)V

    .line 2756068
    :cond_11
    instance-of v0, v1, LX/Jyx;

    if-eqz v0, :cond_12

    move-object v0, v1

    .line 2756069
    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->e()V

    .line 2756070
    :cond_12
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    if-eqz v0, :cond_e

    move v7, v6

    move v6, v2

    .line 2756071
    :goto_5
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_f

    .line 2756072
    iget-object v0, v1, LX/Jyw;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756073
    iget v9, v0, LX/Jyw;->b:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v0, LX/Jyw;->b:I

    .line 2756074
    iget v9, v0, LX/Jyw;->c:I

    iget v10, p0, LX/JzS;->g:I

    if-eq v9, v10, :cond_13

    iget v9, v0, LX/Jyw;->b:I

    if-nez v9, :cond_13

    .line 2756075
    iget v9, p0, LX/JzS;->g:I

    iput v9, v0, LX/Jyw;->c:I

    .line 2756076
    add-int/lit8 v7, v7, 0x1

    .line 2756077
    invoke-interface {v8, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2756078
    :cond_13
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 2756079
    :cond_14
    if-eq v4, v6, :cond_15

    .line 2756080
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Looks like animated nodes graph has cycles, there are "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but toposort visited only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2756081
    :cond_15
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2756082
    if-eqz v3, :cond_17

    .line 2756083
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_6
    if-ltz v1, :cond_17

    .line 2756084
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jz0;

    .line 2756085
    iget-boolean v3, v0, LX/Jz0;->a:Z

    if-eqz v3, :cond_16

    .line 2756086
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2756087
    const-string v4, "finished"

    invoke-interface {v3, v4, v5}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2756088
    iget-object v0, v0, LX/Jz0;->c:Lcom/facebook/react/bridge/Callback;

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v3, v4, v2

    invoke-interface {v0, v4}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2756089
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 2756090
    :cond_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_6

    .line 2756091
    :cond_17
    return-void
.end method

.method public final a(LX/5r0;)V
    .locals 4

    .prologue
    .line 2755934
    invoke-static {}, LX/5pe;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2755935
    :cond_0
    :goto_0
    return-void

    .line 2755936
    :cond_1
    iget-object v0, p0, LX/JzS;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2755937
    invoke-virtual {p1}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    .line 2755938
    iget-object v0, p0, LX/JzS;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2755939
    if-eqz v0, :cond_2

    .line 2755940
    const-string v1, "registrationName"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2755941
    :goto_1
    iget-object v1, p0, LX/JzS;->d:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2755942
    iget v3, p1, LX/5r0;->c:I

    move v3, v3

    .line 2755943
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/animated/EventAnimationDriver;

    .line 2755944
    if-eqz v0, :cond_0

    .line 2755945
    invoke-virtual {p1, v0}, LX/5r0;->a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V

    .line 2755946
    iget-object v1, p0, LX/JzS;->c:Landroid/util/SparseArray;

    iget-object v2, v0, Lcom/facebook/react/animated/EventAnimationDriver;->mValueNode:LX/Jyx;

    iget v2, v2, LX/Jyw;->d:I

    iget-object v0, v0, Lcom/facebook/react/animated/EventAnimationDriver;->mValueNode:LX/Jyx;

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2756011
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2756008
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2756009
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2756010
    return-void
.end method

.method public final b(ID)V
    .locals 4

    .prologue
    .line 2756002
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2756003
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2756004
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v1, v0

    .line 2756005
    check-cast v1, LX/Jyx;

    iput-wide p2, v1, LX/Jyx;->f:D

    .line 2756006
    iget-object v1, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756007
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    .line 2755993
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755994
    if-nez v0, :cond_0

    .line 2755995
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755996
    :cond_0
    iget-object v1, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jyw;

    .line 2755997
    if-nez v1, :cond_1

    .line 2755998
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755999
    :cond_1
    invoke-virtual {v0, v1}, LX/Jyw;->b(LX/Jyw;)V

    .line 2756000
    iget-object v0, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2756001
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2755987
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755988
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2755989
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755990
    :cond_1
    check-cast v0, LX/Jyx;

    const/4 v1, 0x0

    .line 2755991
    iput-object v1, v0, LX/Jyx;->g:LX/Jyz;

    .line 2755992
    return-void
.end method

.method public final c(II)V
    .locals 4

    .prologue
    .line 2755976
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755977
    if-nez v0, :cond_0

    .line 2755978
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755979
    :cond_0
    instance-of v1, v0, LX/JzT;

    if-nez v1, :cond_1

    .line 2755980
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node connected to view should beof type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/JzT;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v1, v0

    .line 2755981
    check-cast v1, LX/JzT;

    .line 2755982
    iget v2, v1, LX/JzT;->e:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 2755983
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already attached to a view"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755984
    :cond_2
    iput p2, v1, LX/JzT;->e:I

    .line 2755985
    iget-object v1, p0, LX/JzS;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2755986
    return-void
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 2755971
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755972
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2755973
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755974
    :cond_1
    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->c()V

    .line 2755975
    return-void
.end method

.method public final d(II)V
    .locals 3

    .prologue
    .line 2755961
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755962
    if-nez v0, :cond_0

    .line 2755963
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755964
    :cond_0
    instance-of v1, v0, LX/JzT;

    if-nez v1, :cond_1

    .line 2755965
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node connected to view should beof type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/JzT;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755966
    :cond_1
    check-cast v0, LX/JzT;

    .line 2755967
    iget v1, v0, LX/JzT;->e:I

    if-eq v1, p2, :cond_2

    .line 2755968
    new-instance v0, LX/5pA;

    const-string v1, "Attempting to disconnect view that has not been connected with the given animated node"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755969
    :cond_2
    const/4 v1, -0x1

    iput v1, v0, LX/JzT;->e:I

    .line 2755970
    return-void
.end method

.method public final e(I)V
    .locals 3

    .prologue
    .line 2755956
    iget-object v0, p0, LX/JzS;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jyw;

    .line 2755957
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-nez v1, :cond_1

    .line 2755958
    :cond_0
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animated node with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exists or is not a \'value\' node"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755959
    :cond_1
    check-cast v0, LX/Jyx;

    invoke-virtual {v0}, LX/Jyx;->d()V

    .line 2755960
    return-void
.end method

.method public final f(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2755947
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2755948
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jz0;

    .line 2755949
    iget v3, v0, LX/Jz0;->d:I

    if-ne v3, p1, :cond_1

    .line 2755950
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2755951
    const-string v4, "finished"

    invoke-interface {v3, v4, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2755952
    iget-object v0, v0, LX/Jz0;->c:Lcom/facebook/react/bridge/Callback;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v2

    invoke-interface {v0, v4}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2755953
    iget-object v0, p0, LX/JzS;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 2755954
    :cond_0
    return-void

    .line 2755955
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
