.class public LX/Jah;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715670
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Jah;->a:Z

    .line 2715671
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2715672
    iget-boolean v0, p0, LX/Jah;->a:Z

    if-eqz v0, :cond_0

    .line 2715673
    new-instance v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;-><init>()V

    .line 2715674
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2715675
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;-><init>()V

    goto :goto_0
.end method
