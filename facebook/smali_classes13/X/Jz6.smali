.class public LX/Jz6;
.super LX/Jyx;
.source ""


# instance fields
.field private final g:LX/JzS;

.field private final h:I

.field private final i:I


# direct methods
.method public constructor <init>(LX/5pG;LX/JzS;)V
    .locals 1

    .prologue
    .line 2755772
    invoke-direct {p0}, LX/Jyx;-><init>()V

    .line 2755773
    iput-object p2, p0, LX/Jz6;->g:LX/JzS;

    .line 2755774
    const-string v0, "input"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Jz6;->h:I

    .line 2755775
    const-string v0, "modulus"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Jz6;->i:I

    .line 2755776
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2755777
    iget-object v0, p0, LX/Jz6;->g:LX/JzS;

    iget v1, p0, LX/Jz6;->h:I

    invoke-virtual {v0, v1}, LX/JzS;->a(I)LX/Jyw;

    move-result-object v0

    .line 2755778
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Jyx;

    if-eqz v1, :cond_0

    .line 2755779
    check-cast v0, LX/Jyx;

    iget-wide v0, v0, LX/Jyx;->e:D

    iget v2, p0, LX/Jz6;->i:I

    int-to-double v2, v2

    rem-double/2addr v0, v2

    iput-wide v0, p0, LX/Jz6;->e:D

    return-void

    .line 2755780
    :cond_0
    new-instance v0, LX/5p9;

    const-string v1, "Illegal node ID set as an input for Animated.modulus node"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0
.end method
