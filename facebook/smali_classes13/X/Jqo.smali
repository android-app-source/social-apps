.class public LX/Jqo;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/2N4;

.field private final c:LX/Jqb;

.field private final d:LX/0SG;

.field private final e:LX/6cw;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740496
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqo;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/Jrc;LX/2N4;LX/Jqb;LX/6cw;LX/0SG;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jrc;",
            "LX/2N4;",
            "LX/Jqb;",
            "LX/6cw;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2740487
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2740488
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2740489
    iput-object v0, p0, LX/Jqo;->f:LX/0Ot;

    .line 2740490
    iput-object p2, p0, LX/Jqo;->a:LX/Jrc;

    .line 2740491
    iput-object p3, p0, LX/Jqo;->b:LX/2N4;

    .line 2740492
    iput-object p4, p0, LX/Jqo;->c:LX/Jqb;

    .line 2740493
    iput-object p5, p0, LX/Jqo;->e:LX/6cw;

    .line 2740494
    iput-object p6, p0, LX/Jqo;->d:LX/0SG;

    .line 2740495
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqo;
    .locals 14

    .prologue
    .line 2740425
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740426
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740427
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740428
    if-nez v1, :cond_0

    .line 2740429
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740430
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740431
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740432
    sget-object v1, LX/Jqo;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740433
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740434
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740435
    :cond_1
    if-nez v1, :cond_4

    .line 2740436
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740437
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740438
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2740439
    new-instance v7, LX/Jqo;

    const/16 v8, 0x35bd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v9

    check-cast v9, LX/Jrc;

    invoke-static {v0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v10

    check-cast v10, LX/2N4;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v11

    check-cast v11, LX/Jqb;

    invoke-static {v0}, LX/6cw;->a(LX/0QB;)LX/6cw;

    move-result-object v12

    check-cast v12, LX/6cw;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v7 .. v13}, LX/Jqo;-><init>(LX/0Ot;LX/Jrc;LX/2N4;LX/Jqb;LX/6cw;LX/0SG;)V

    .line 2740440
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2740441
    iput-object v8, v7, LX/Jqo;->f:LX/0Ot;

    .line 2740442
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2740443
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740444
    if-nez v1, :cond_2

    .line 2740445
    sget-object v0, LX/Jqo;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqo;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740446
    :goto_1
    if-eqz v0, :cond_3

    .line 2740447
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740448
    :goto_3
    check-cast v0, LX/Jqo;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740449
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740450
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740451
    :catchall_1
    move-exception v0

    .line 2740452
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740453
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740454
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740455
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqo;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqo;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2740485
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740486
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2740465
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2740466
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->H()LX/6jj;

    move-result-object v0

    .line 2740467
    iget-object v2, v0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/6jj;->action:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 2740468
    :goto_0
    return-object v0

    .line 2740469
    :cond_1
    iget-object v2, p0, LX/Jqo;->a:LX/Jrc;

    iget-object v3, v0, LX/6jj;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2740470
    iget-object v3, p0, LX/Jqo;->b:LX/2N4;

    invoke-virtual {v3, v2, v8}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v3

    .line 2740471
    iget-object v3, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740472
    if-nez v3, :cond_2

    move-object v0, v1

    .line 2740473
    goto :goto_0

    .line 2740474
    :cond_2
    iget-object v3, v0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    .line 2740475
    iget-object v4, v0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_4

    .line 2740476
    iget-object v4, p0, LX/Jqo;->e:LX/6cw;

    iget-object v0, v0, LX/6jj;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v2, v3, v6, v7}, LX/6cw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;J)V

    .line 2740477
    :goto_1
    iget-object v0, p0, LX/Jqo;->b:LX/2N4;

    invoke-virtual {v0, v2, v8}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2740478
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740479
    if-eqz v0, :cond_3

    .line 2740480
    const-string v2, "approval_queue_thread_summary"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    move-object v0, v1

    .line 2740481
    goto :goto_0

    .line 2740482
    :cond_4
    iget-object v4, v0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 2740483
    iget-object v0, p0, LX/Jqo;->e:LX/6cw;

    invoke-virtual {v0, v2, v3}, LX/6cw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;)V

    goto :goto_1

    .line 2740484
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inappropriate DeltaApprovalQueueAction: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740459
    const-string v0, "approval_queue_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2740460
    if-eqz v0, :cond_0

    .line 2740461
    iget-object v1, p0, LX/Jqo;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/Jqo;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740462
    iget-object v1, p0, LX/Jqo;->c:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2740463
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2740464
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2740456
    check-cast p1, LX/6kW;

    .line 2740457
    invoke-virtual {p1}, LX/6kW;->H()LX/6jj;

    move-result-object v0

    .line 2740458
    iget-object v1, p0, LX/Jqo;->a:LX/Jrc;

    iget-object v0, v0, LX/6jj;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
