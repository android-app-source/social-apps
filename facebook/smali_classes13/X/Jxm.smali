.class public final LX/Jxm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

.field public final synthetic b:LX/Jxn;


# direct methods
.method public constructor <init>(LX/Jxn;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)V
    .locals 0

    .prologue
    .line 2753502
    iput-object p1, p0, LX/Jxm;->b:LX/Jxn;

    iput-object p2, p0, LX/Jxm;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2753503
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 2753504
    iget-object v0, p0, LX/Jxm;->b:LX/Jxn;

    iget-object v0, v0, LX/Jxn;->b:LX/03V;

    const-string v1, "discovery_curation"

    const-string v2, "Could not fetch tags to refresh"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2753505
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2753506
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2753507
    iget-object v0, p0, LX/Jxm;->b:LX/Jxn;

    iget-object v0, v0, LX/Jxn;->e:LX/Eny;

    iget-object v1, p0, LX/Jxm;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/Eny;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2753508
    return-void
.end method
