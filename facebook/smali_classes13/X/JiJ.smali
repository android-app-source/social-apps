.class public LX/JiJ;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/94q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/94j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ien;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jq0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Jq2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/94z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final l:LX/0Yb;

.field public final m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

.field private final n:Landroid/widget/LinearLayout;

.field private final o:Landroid/widget/ProgressBar;

.field private final p:Landroid/widget/RelativeLayout;

.field public final q:Lcom/facebook/widget/text/BetterTextView;

.field private final r:Landroid/widget/Button;

.field public final s:Lcom/facebook/widget/text/BetterTextView;

.field public t:LX/JiI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2725778
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/JiJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725779
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2725780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2725781
    const-class v0, LX/JiJ;

    invoke-static {v0, p0}, LX/JiJ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2725782
    iget-object v0, p0, LX/JiJ;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.contacts.CONTACTS_UPLOAD_STATE_CHANGED"

    new-instance v2, LX/JiB;

    invoke-direct {v2, p0}, LX/JiB;-><init>(LX/JiJ;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "contacts_upload_permission_granted"

    new-instance v2, LX/JiA;

    invoke-direct {v2, p0}, LX/JiA;-><init>(LX/JiJ;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "contacts_upload_permission_denied_never_ask"

    new-instance v2, LX/Ji9;

    invoke-direct {v2, p0}, LX/Ji9;-><init>(LX/JiJ;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/JiJ;->l:LX/0Yb;

    .line 2725783
    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b078c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v3, v0, v3, v3}, LX/JiJ;->setPadding(IIII)V

    .line 2725784
    const v0, 0x7f030cee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2725785
    const v0, 0x7f0d2041

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    iput-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    .line 2725786
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082de3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setNegativeButtonContentDescription(Ljava/lang/String;)V

    .line 2725787
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082de0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setTitle(Ljava/lang/String;)V

    .line 2725788
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082de2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setTextContentDescription(Ljava/lang/String;)V

    .line 2725789
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082de5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setPositiveButtonText(Ljava/lang/String;)V

    .line 2725790
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    new-instance v1, LX/JiC;

    invoke-direct {v1, p0, p1}, LX/JiC;-><init>(LX/JiJ;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725791
    iget-object v0, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    new-instance v1, LX/JiD;

    invoke-direct {v1, p0}, LX/JiD;-><init>(LX/JiJ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725792
    const v0, 0x7f0d2042

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/JiJ;->n:Landroid/widget/LinearLayout;

    .line 2725793
    const v0, 0x7f0d2044

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    .line 2725794
    iget-object v0, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2725795
    const v0, 0x7f0d2045

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/JiJ;->p:Landroid/widget/RelativeLayout;

    .line 2725796
    const v0, 0x7f0d2047

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/JiJ;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2725797
    const v0, 0x7f0d2048

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/JiJ;->r:Landroid/widget/Button;

    .line 2725798
    iget-object v0, p0, LX/JiJ;->r:Landroid/widget/Button;

    new-instance v1, LX/JiE;

    invoke-direct {v1, p0}, LX/JiE;-><init>(LX/JiJ;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2725799
    const v0, 0x7f0d2049

    invoke-virtual {p0, v0}, LX/JiJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/JiJ;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 2725800
    return-void
.end method

.method public static a(LX/JiJ;Lcom/facebook/widget/text/BetterTextView;Landroid/text/style/ClickableSpan;IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2725801
    new-instance v0, LX/47x;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2725802
    invoke-virtual {v0, p3}, LX/47x;->a(I)LX/47x;

    .line 2725803
    invoke-virtual {p0}, LX/JiJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v0, p5, v1, p2, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2725804
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2725805
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725806
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, LX/JiJ;

    invoke-static {p0}, LX/94q;->a(LX/0QB;)LX/94q;

    move-result-object v2

    check-cast v2, LX/94q;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v5, 0x97

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/94j;->b(LX/0QB;)LX/94j;

    move-result-object v6

    check-cast v6, LX/94j;

    invoke-static {p0}, LX/Ien;->b(LX/0QB;)LX/Ien;

    move-result-object v7

    check-cast v7, LX/Ien;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    const/16 v9, 0x455

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x29a5

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    new-instance v0, LX/Jq2;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-direct {v0, p1}, LX/Jq2;-><init>(LX/0Uh;)V

    move-object p0, v0

    check-cast p0, LX/Jq2;

    iput-object v2, v1, LX/JiJ;->a:LX/94q;

    iput-object v3, v1, LX/JiJ;->b:LX/0Xl;

    iput-object v4, v1, LX/JiJ;->c:LX/0Zb;

    iput-object v5, v1, LX/JiJ;->d:LX/0Ot;

    iput-object v6, v1, LX/JiJ;->e:LX/94j;

    iput-object v7, v1, LX/JiJ;->f:LX/Ien;

    iput-object v8, v1, LX/JiJ;->g:LX/1Ml;

    iput-object v9, v1, LX/JiJ;->h:LX/0Ot;

    iput-object v10, v1, LX/JiJ;->i:LX/0Ot;

    iput-object p0, v1, LX/JiJ;->j:LX/Jq2;

    return-void
.end method

.method public static a$redex0(LX/JiJ;Lcom/facebook/contacts/upload/ContactsUploadState;)V
    .locals 4

    .prologue
    .line 2725807
    iget v0, p1, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    move v0, v0

    .line 2725808
    iget v1, p1, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    move v1, v1

    .line 2725809
    if-gtz v0, :cond_0

    .line 2725810
    iget-object v0, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2725811
    :goto_0
    invoke-direct {p0}, LX/JiJ;->i()V

    .line 2725812
    return-void

    .line 2725813
    :cond_0
    iget-object v2, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2725814
    iget-object v2, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2725815
    iget-object v0, p0, LX/JiJ;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/JiJ;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2725816
    iget-object v0, p0, LX/JiJ;->c:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2725817
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2725818
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2725819
    :cond_0
    return-void
.end method

.method public static g$redex0(LX/JiJ;)V
    .locals 2

    .prologue
    .line 2725820
    iget-object v0, p0, LX/JiJ;->a:LX/94q;

    sget-object v1, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->SHOW:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    invoke-virtual {v0, v1}, LX/94q;->a(Lcom/facebook/contacts/upload/ContactsUploadVisibility;)LX/1ML;

    .line 2725821
    invoke-direct {p0}, LX/JiJ;->i()V

    .line 2725822
    return-void
.end method

.method private i()V
    .locals 13

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 2725823
    iget-object v2, p0, LX/JiJ;->a:LX/94q;

    invoke-virtual {v2}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v2

    .line 2725824
    iget-object v3, v2, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    move-object v3, v3

    .line 2725825
    iget-object v4, p0, LX/JiJ;->k:LX/94z;

    if-ne v3, v4, :cond_0

    .line 2725826
    :goto_0
    return-void

    .line 2725827
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 2725828
    iget-object v5, p0, LX/JiJ;->k:LX/94z;

    if-eqz v5, :cond_1

    .line 2725829
    const-string v5, "source_module"

    iget-object v6, p0, LX/JiJ;->k:LX/94z;

    invoke-virtual {v6}, LX/94z;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2725830
    :cond_1
    const-string v5, "dest_module"

    invoke-virtual {v3}, LX/94z;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2725831
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    const/4 v8, 0x0

    .line 2725832
    iget-object v7, p0, LX/JiJ;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0gh;

    const-string v9, "neue_nux"

    const-string v11, "neue"

    move-object v10, v8

    move-object v12, v4

    invoke-virtual/range {v7 .. v12}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2725833
    iput-object v3, p0, LX/JiJ;->k:LX/94z;

    .line 2725834
    sget-object v4, LX/Ji8;->a:[I

    invoke-virtual {v3}, LX/94z;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    move v2, v0

    move v3, v0

    move v4, v0

    .line 2725835
    :goto_1
    invoke-virtual {p0, v1}, LX/JiJ;->setVisibility(I)V

    .line 2725836
    iget-object v1, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    invoke-virtual {v1, v4}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->setVisibility(I)V

    .line 2725837
    iget-object v1, p0, LX/JiJ;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2725838
    iget-object v1, p0, LX/JiJ;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2725839
    iget-object v1, p0, LX/JiJ;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_0
    move v2, v0

    move v3, v0

    move v4, v1

    .line 2725840
    goto :goto_1

    :pswitch_1
    move v2, v0

    move v3, v1

    move v4, v0

    .line 2725841
    goto :goto_1

    :pswitch_2
    move v2, v1

    move v3, v0

    move v4, v0

    .line 2725842
    goto :goto_1

    .line 2725843
    :pswitch_3
    iget v3, v2, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v2, v3

    .line 2725844
    if-nez v2, :cond_2

    .line 2725845
    new-instance v3, LX/JiH;

    invoke-direct {v3, p0}, LX/JiH;-><init>(LX/JiJ;)V

    .line 2725846
    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082df4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2725847
    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f082df5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2725848
    new-instance v6, LX/47x;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v6, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2725849
    invoke-virtual {v6, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2725850
    const-string v4, "[[okay]]"

    const/16 v2, 0x21

    invoke-virtual {v6, v4, v5, v3, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2725851
    invoke-virtual {v6}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    move-object v3, v3

    .line 2725852
    :goto_2
    iget-object v4, p0, LX/JiJ;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2725853
    iget-object v4, p0, LX/JiJ;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2725854
    move v2, v0

    move v3, v0

    move v4, v0

    move v0, v1

    goto :goto_1

    .line 2725855
    :cond_2
    new-instance v3, LX/Ji7;

    invoke-direct {v3, p0}, LX/Ji7;-><init>(LX/JiJ;)V

    .line 2725856
    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f013e

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2725857
    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f082df3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2725858
    new-instance v6, LX/47x;

    invoke-virtual {p0}, LX/JiJ;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2725859
    invoke-virtual {v6, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2725860
    const-string v4, "[[view]]"

    const/16 v7, 0x21

    invoke-virtual {v6, v4, v5, v3, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2725861
    invoke-virtual {v6}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    move-object v3, v3

    .line 2725862
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static m(LX/JiJ;)V
    .locals 1

    .prologue
    .line 2725863
    iget-object v0, p0, LX/JiJ;->a:LX/94q;

    invoke-virtual {v0}, LX/94q;->a()V

    .line 2725864
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/JiJ;->setVisibility(I)V

    .line 2725865
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xdcf5fbe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2725866
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2725867
    new-instance v4, LX/JiF;

    invoke-direct {v4, p0}, LX/JiF;-><init>(LX/JiJ;)V

    .line 2725868
    iget-object v5, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    .line 2725869
    iget-object v6, v5, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->b:Lcom/facebook/widget/text/BetterTextView;

    move-object v5, v6

    .line 2725870
    const v9, 0x7f082de1

    const v10, 0x7f082de4

    const-string v11, "[[learn_more_link]]"

    move-object v6, p0

    move-object v7, v5

    move-object v8, v4

    invoke-static/range {v6 .. v11}, LX/JiJ;->a(LX/JiJ;Lcom/facebook/widget/text/BetterTextView;Landroid/text/style/ClickableSpan;IILjava/lang/String;)V

    .line 2725871
    new-instance v4, LX/JiG;

    invoke-direct {v4, p0}, LX/JiG;-><init>(LX/JiJ;)V

    .line 2725872
    iget-object v5, p0, LX/JiJ;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2725873
    const v9, 0x7f082de8

    const v10, 0x7f082dea

    const-string v11, "[[not_now_link]]"

    move-object v6, p0

    move-object v7, v5

    move-object v8, v4

    invoke-static/range {v6 .. v11}, LX/JiJ;->a(LX/JiJ;Lcom/facebook/widget/text/BetterTextView;Landroid/text/style/ClickableSpan;IILjava/lang/String;)V

    .line 2725874
    iget-object v1, p0, LX/JiJ;->t:LX/JiI;

    if-eqz v1, :cond_0

    .line 2725875
    iget-object v1, p0, LX/JiJ;->m:Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;

    iget-object v2, p0, LX/JiJ;->t:LX/JiI;

    invoke-interface {v2}, LX/JiI;->a()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/contacts/picker/ContactPickerSectionUpsellView;->a(Z)V

    .line 2725876
    :cond_0
    iget-object v1, p0, LX/JiJ;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 2725877
    iget-object v1, p0, LX/JiJ;->a:LX/94q;

    invoke-virtual {v1}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v1

    invoke-static {p0, v1}, LX/JiJ;->a$redex0(LX/JiJ;Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 2725878
    const/16 v1, 0x2d

    const v2, -0xebfdd7e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5e786a39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2725879
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2725880
    iget-object v1, p0, LX/JiJ;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2725881
    const/16 v1, 0x2d

    const v2, -0x7502124e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2725882
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2725883
    invoke-virtual {p0}, LX/JiJ;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2725884
    invoke-virtual {p0, v2, v2}, LX/JiJ;->setMeasuredDimension(II)V

    .line 2725885
    :cond_0
    return-void
.end method
