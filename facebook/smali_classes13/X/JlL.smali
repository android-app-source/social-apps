.class public LX/JlL;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/user/tiles/UserTileView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2731003
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/JlL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2731004
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2731005
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JlL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2731006
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2731007
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2731008
    invoke-direct {p0}, LX/JlL;->a()V

    .line 2731009
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2731010
    const v0, 0x7f03021d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2731011
    const v0, 0x7f0d0840

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/JlL;->a:Lcom/facebook/user/tiles/UserTileView;

    .line 2731012
    const v0, 0x7f0d0841

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JlL;->b:Landroid/widget/TextView;

    .line 2731013
    const v0, 0x7f0d0842

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JlL;->c:Landroid/widget/TextView;

    .line 2731014
    return-void
.end method
