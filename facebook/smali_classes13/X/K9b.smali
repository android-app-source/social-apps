.class public final LX/K9b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/K9d;


# direct methods
.method public constructor <init>(LX/K9d;I)V
    .locals 0

    .prologue
    .line 2777640
    iput-object p1, p0, LX/K9b;->b:LX/K9d;

    iput p2, p0, LX/K9b;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 4

    .prologue
    .line 2777641
    iget-object v0, p0, LX/K9b;->b:LX/K9d;

    iget-object v0, v0, LX/K9d;->i:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 2777642
    iget-object v0, p0, LX/K9b;->b:LX/K9d;

    iget-object v0, v0, LX/K9d;->i:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 2777643
    iget-object v0, p0, LX/K9b;->b:LX/K9d;

    iget v1, p0, LX/K9b;->a:I

    iget-object v2, p0, LX/K9b;->b:LX/K9d;

    iget-object v2, v2, LX/K9d;->c:Ljava/text/DateFormat;

    iget-object v3, p0, LX/K9b;->b:LX/K9d;

    iget-object v3, v3, LX/K9d;->i:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/K9d;->a(ILjava/lang/String;)V

    .line 2777644
    return-void
.end method
