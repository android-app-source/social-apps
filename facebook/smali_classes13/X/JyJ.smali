.class public LX/JyJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JyH;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2754594
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JyJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2754591
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2754592
    iput-object p1, p0, LX/JyJ;->b:LX/0Ot;

    .line 2754593
    return-void
.end method

.method public static a(LX/0QB;)LX/JyJ;
    .locals 4

    .prologue
    .line 2754595
    const-class v1, LX/JyJ;

    monitor-enter v1

    .line 2754596
    :try_start_0
    sget-object v0, LX/JyJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754597
    sput-object v2, LX/JyJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754598
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754599
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754600
    new-instance v3, LX/JyJ;

    const/16 p0, 0x300d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JyJ;-><init>(LX/0Ot;)V

    .line 2754601
    move-object v0, v3

    .line 2754602
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754603
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JyJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754604
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754605
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2754578
    check-cast p2, LX/JyI;

    .line 2754579
    iget-object v0, p0, LX/JyJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;

    iget-object v1, p2, LX/JyI;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    iget v2, p2, LX/JyI;->b:I

    const/4 p2, 0x1

    .line 2754580
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 2754581
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$FbglyphImageModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$FbglyphImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketCoreFieldsModel$FbglyphImageModel;->a()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 2754582
    :goto_0
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2754583
    iget-object v3, v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v4}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object v4, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    .line 2754584
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/profile/discovery/components/DiscoverySubtitleComponentSpec;->d:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {v3, v4}, LX/1up;->a(Landroid/graphics/ColorFilter;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b26dc

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b26dc

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    const p0, 0x7f0b26da

    invoke-interface {v3, v4, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b26db

    invoke-interface {v3, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754585
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 2754586
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2754587
    const/4 v4, 0x0

    const p0, 0x7f0e0123

    invoke-static {p1, v4, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->c(F)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754588
    :cond_1
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2754589
    return-object v0

    .line 2754590
    :cond_2
    const/4 v3, 0x0

    move-object v4, v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2754576
    invoke-static {}, LX/1dS;->b()V

    .line 2754577
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/JyH;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2754568
    new-instance v1, LX/JyI;

    invoke-direct {v1, p0}, LX/JyI;-><init>(LX/JyJ;)V

    .line 2754569
    sget-object v2, LX/JyJ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JyH;

    .line 2754570
    if-nez v2, :cond_0

    .line 2754571
    new-instance v2, LX/JyH;

    invoke-direct {v2}, LX/JyH;-><init>()V

    .line 2754572
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JyH;->a$redex0(LX/JyH;LX/1De;IILX/JyI;)V

    .line 2754573
    move-object v1, v2

    .line 2754574
    move-object v0, v1

    .line 2754575
    return-object v0
.end method
