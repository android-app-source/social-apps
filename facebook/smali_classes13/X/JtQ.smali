.class public final LX/JtQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/JtM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JtR;


# direct methods
.method public constructor <init>(LX/JtR;)V
    .locals 0

    .prologue
    .line 2746720
    iput-object p1, p0, LX/JtQ;->a:LX/JtR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746721
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "Media Data fetch failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746722
    iget-object v0, p0, LX/JtQ;->a:LX/JtR;

    iget-object v0, v0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746723
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746724
    check-cast p1, LX/JtM;

    .line 2746725
    if-nez p1, :cond_0

    .line 2746726
    iget-object v0, p0, LX/JtQ;->a:LX/JtR;

    iget-object v0, v0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0xa41971f

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746727
    :goto_0
    return-void

    .line 2746728
    :cond_0
    iget-object v0, p0, LX/JtQ;->a:LX/JtR;

    iget-object v0, v0, LX/JtR;->c:LX/JtU;

    iget-object v0, v0, LX/JtU;->c:LX/JtG;

    iget-object v1, p0, LX/JtQ;->a:LX/JtR;

    iget-object v1, v1, LX/JtR;->b:LX/JtS;

    iget-object v1, v1, LX/JtS;->c:LX/Jt0;

    .line 2746729
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2746730
    iget-object v3, v0, LX/JtG;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;

    invoke-direct {v4, v0, p1, v1, v2}, Lcom/facebook/messengerwear/support/MessengerWearMediaAssets$2;-><init>(LX/JtG;LX/JtM;LX/Jt0;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v5, -0x6dfab595

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2746731
    move-object v0, v2

    .line 2746732
    new-instance v1, LX/JtP;

    invoke-direct {v1, p0}, LX/JtP;-><init>(LX/JtQ;)V

    iget-object v2, p0, LX/JtQ;->a:LX/JtR;

    iget-object v2, v2, LX/JtR;->c:LX/JtU;

    iget-object v2, v2, LX/JtU;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
