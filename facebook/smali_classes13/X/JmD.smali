.class public LX/JmD;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2732532
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2732533
    invoke-direct {p0}, LX/JmD;->a()V

    .line 2732534
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2732529
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2732530
    invoke-direct {p0}, LX/JmD;->a()V

    .line 2732531
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2732535
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2732536
    invoke-direct {p0}, LX/JmD;->a()V

    .line 2732537
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2732525
    const v0, 0x7f0308ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2732526
    const v0, 0x7f0d170e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JmD;->a:Landroid/widget/TextView;

    .line 2732527
    const v0, 0x7f0d170f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JmD;->b:Landroid/widget/TextView;

    .line 2732528
    return-void
.end method


# virtual methods
.method public setData(LX/JmC;)V
    .locals 2

    .prologue
    .line 2732522
    iget-object v0, p0, LX/JmD;->a:Landroid/widget/TextView;

    iget-object v1, p1, LX/JmC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2732523
    iget-object v0, p0, LX/JmD;->b:Landroid/widget/TextView;

    iget-object v1, p1, LX/JmC;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2732524
    return-void
.end method
