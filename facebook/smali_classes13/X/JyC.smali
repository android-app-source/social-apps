.class public final LX/JyC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JyD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

.field public b:Landroid/view/View$OnClickListener;

.field public final synthetic c:LX/JyD;


# direct methods
.method public constructor <init>(LX/JyD;)V
    .locals 1

    .prologue
    .line 2754302
    iput-object p1, p0, LX/JyC;->c:LX/JyD;

    .line 2754303
    move-object v0, p1

    .line 2754304
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2754305
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2754287
    const-string v0, "DiscoveryExpandedUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2754288
    if-ne p0, p1, :cond_1

    .line 2754289
    :cond_0
    :goto_0
    return v0

    .line 2754290
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2754291
    goto :goto_0

    .line 2754292
    :cond_3
    check-cast p1, LX/JyC;

    .line 2754293
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2754294
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2754295
    if-eq v2, v3, :cond_0

    .line 2754296
    iget-object v2, p0, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    iget-object v3, p1, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2754297
    goto :goto_0

    .line 2754298
    :cond_5
    iget-object v2, p1, LX/JyC;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-nez v2, :cond_4

    .line 2754299
    :cond_6
    iget-object v2, p0, LX/JyC;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JyC;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/JyC;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2754300
    goto :goto_0

    .line 2754301
    :cond_7
    iget-object v2, p1, LX/JyC;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
