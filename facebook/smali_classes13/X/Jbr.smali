.class public LX/Jbr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Jbr;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/17X;

.field private final c:LX/3Q1;

.field private final d:LX/2aN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17X;LX/3Q1;LX/2aN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717182
    iput-object p1, p0, LX/Jbr;->a:Landroid/content/Context;

    .line 2717183
    iput-object p2, p0, LX/Jbr;->b:LX/17X;

    .line 2717184
    iput-object p3, p0, LX/Jbr;->c:LX/3Q1;

    .line 2717185
    iput-object p4, p0, LX/Jbr;->d:LX/2aN;

    .line 2717186
    return-void
.end method

.method public static a(LX/0QB;)LX/Jbr;
    .locals 7

    .prologue
    .line 2717187
    sget-object v0, LX/Jbr;->e:LX/Jbr;

    if-nez v0, :cond_1

    .line 2717188
    const-class v1, LX/Jbr;

    monitor-enter v1

    .line 2717189
    :try_start_0
    sget-object v0, LX/Jbr;->e:LX/Jbr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2717190
    if-eqz v2, :cond_0

    .line 2717191
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2717192
    new-instance p0, LX/Jbr;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17X;

    invoke-static {v0}, LX/3Q1;->b(LX/0QB;)LX/3Q1;

    move-result-object v5

    check-cast v5, LX/3Q1;

    invoke-static {v0}, LX/2aN;->b(LX/0QB;)LX/2aN;

    move-result-object v6

    check-cast v6, LX/2aN;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Jbr;-><init>(Landroid/content/Context;LX/17X;LX/3Q1;LX/2aN;)V

    .line 2717193
    move-object v0, p0

    .line 2717194
    sput-object v0, LX/Jbr;->e:LX/Jbr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717195
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2717196
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2717197
    :cond_1
    sget-object v0, LX/Jbr;->e:LX/Jbr;

    return-object v0

    .line 2717198
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2717199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 7

    .prologue
    .line 2717200
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v6, v0

    .line 2717201
    const/4 v0, 0x0

    .line 2717202
    iget-object v1, p0, LX/Jbr;->c:LX/3Q1;

    invoke-virtual {v1}, LX/3Q1;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2717203
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v1

    .line 2717204
    if-eqz v1, :cond_0

    .line 2717205
    iget-object v0, p0, LX/Jbr;->b:LX/17X;

    iget-object v2, p0, LX/Jbr;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->dm:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_login_approval_notification_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DIRECT_TO_DENY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_COMPONENT_TYPE"

    sget-object v2, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    :cond_0
    move-object v2, v0

    .line 2717206
    :goto_0
    iget-object v0, p0, LX/Jbr;->d:LX/2aN;

    .line 2717207
    iget-object v1, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v1, v1

    .line 2717208
    invoke-virtual {v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v3, LX/8D4;->ACTIVITY:LX/8D4;

    sget-object v4, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    .line 2717209
    iget v5, p1, LX/Drw;->a:I

    move v5, v5

    .line 2717210
    invoke-virtual/range {v0 .. v5}, LX/2aN;->b(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Landroid/content/Intent;LX/8D4;LX/3B2;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2717211
    new-instance v1, LX/3pX;

    const v2, 0x7f020819

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0

    .line 2717212
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->HREF:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2717213
    const/4 v1, 0x0

    .line 2717214
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2717215
    iget-object v1, p0, LX/Jbr;->b:LX/17X;

    iget-object v2, p0, LX/Jbr;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2717216
    :cond_2
    if-nez v1, :cond_3

    .line 2717217
    iget-object v1, p0, LX/Jbr;->b:LX/17X;

    iget-object v2, p0, LX/Jbr;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->dd:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2717218
    :cond_3
    move-object v0, v1

    .line 2717219
    const-string v1, "EXTRA_COMPONENT_TYPE"

    sget-object v2, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2717220
    const/4 v0, 0x0

    return v0
.end method
