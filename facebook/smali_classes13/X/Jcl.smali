.class public final LX/Jcl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3x2;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

.field public final synthetic b:LX/Jcm;


# direct methods
.method public constructor <init>(LX/Jcm;Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 0

    .prologue
    .line 2718306
    iput-object p1, p0, LX/Jcl;->b:LX/Jcm;

    iput-object p2, p0, LX/Jcl;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2718307
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d31f8    # 1.876806E38f

    if-ne v0, v1, :cond_1

    .line 2718308
    iget-object v0, p0, LX/Jcl;->b:LX/Jcm;

    iget-object v0, v0, LX/Jcm;->a:LX/Jcn;

    iget-object v0, v0, LX/Jcn;->p:LX/Jcq;

    iget-object v1, p0, LX/Jcl;->a:Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 2718309
    iget-object v2, v0, LX/Jcq;->l:LX/JdA;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2718310
    iget-object v2, v0, LX/Jcq;->l:LX/JdA;

    .line 2718311
    iget-object v3, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iget-object v4, v2, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    invoke-static {v4}, Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;->r(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2718312
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 2718313
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2718314
    :cond_2
    iget-object v3, v2, LX/JdA;->a:Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;

    const/4 p1, 0x1

    .line 2718315
    new-instance v4, LX/31Y;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v4, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    const p0, 0x7f083b46

    invoke-virtual {v4, p0}, LX/0ju;->a(I)LX/0ju;

    move-result-object v4

    const p0, 0x7f083b47

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v2, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    aput-object v2, p1, v0

    invoke-virtual {v3, p0, p1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    const p0, 0x7f083b48

    new-instance p1, LX/JdD;

    invoke-direct {p1, v3, v1}, LX/JdD;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    const p0, 0x7f080017

    new-instance p1, LX/JdC;

    invoke-direct {p1, v3}, LX/JdC;-><init>(Lcom/facebook/messaging/accountswitch/SwitchAccountsFragment;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    .line 2718316
    goto :goto_0
.end method
