.class public final LX/Jdx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jdw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jdw",
        "<",
        "Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JeB;


# direct methods
.method public constructor <init>(LX/JeB;)V
    .locals 0

    .prologue
    .line 2720234
    iput-object p1, p0, LX/Jdx;->a:LX/JeB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JeZ;LX/Jek;)V
    .locals 4

    .prologue
    .line 2720224
    check-cast p2, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;

    .line 2720225
    check-cast p1, LX/Jef;

    .line 2720226
    iget-object v0, p1, LX/Jef;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    move-object v0, v0

    .line 2720227
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2720228
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a(Landroid/net/Uri;)V

    .line 2720229
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a(Ljava/lang/String;)V

    .line 2720230
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->b(Ljava/lang/String;)V

    .line 2720231
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->n()Z

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Jdx;->a:LX/JeB;

    iget-boolean v0, v0, LX/JeB;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Jdx;->a:LX/JeB;

    iget-object v0, v0, LX/JeB;->d:LX/JeY;

    :goto_0
    new-instance v3, LX/Jdu;

    invoke-direct {v3, p0, p1}, LX/Jdu;-><init>(LX/Jdx;LX/Jef;)V

    invoke-virtual {p2, v1, v2, v0, v3}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a(ZLjava/lang/String;LX/JeV;LX/Jdt;)V

    .line 2720232
    return-void

    .line 2720233
    :cond_1
    iget-object v0, p0, LX/Jdx;->a:LX/JeB;

    iget-object v0, v0, LX/JeB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JeV;

    goto :goto_0
.end method
