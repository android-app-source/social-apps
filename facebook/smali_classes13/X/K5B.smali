.class public LX/K5B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2769954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769955
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .prologue
    .line 2769956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2769957
    iput p1, p0, LX/K5B;->a:F

    .line 2769958
    iput p2, p0, LX/K5B;->b:F

    .line 2769959
    iput p3, p0, LX/K5B;->c:F

    .line 2769960
    iput p4, p0, LX/K5B;->d:F

    .line 2769961
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2769962
    const-string v0, "(%f, %f, %f, %f)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/K5B;->a:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/K5B;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, LX/K5B;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, LX/K5B;->d:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
