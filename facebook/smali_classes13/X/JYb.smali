.class public LX/JYb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYc;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYb",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706635
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706636
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYb;->b:LX/0Zi;

    .line 2706637
    iput-object p1, p0, LX/JYb;->a:LX/0Ot;

    .line 2706638
    return-void
.end method

.method public static a(LX/0QB;)LX/JYb;
    .locals 4

    .prologue
    .line 2706639
    const-class v1, LX/JYb;

    monitor-enter v1

    .line 2706640
    :try_start_0
    sget-object v0, LX/JYb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706641
    sput-object v2, LX/JYb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706644
    new-instance v3, LX/JYb;

    const/16 p0, 0x217d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYb;-><init>(LX/0Ot;)V

    .line 2706645
    move-object v0, v3

    .line 2706646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2706650
    check-cast p2, LX/JYa;

    .line 2706651
    iget-object v0, p0, LX/JYb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JYc;

    iget-object v1, p2, LX/JYa;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    const/4 p2, 0x7

    const/4 p0, 0x1

    const/4 v8, 0x2

    .line 2706652
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2706653
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706654
    invoke-static {v2}, LX/JYy;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    .line 2706655
    invoke-static {v2}, LX/JYc;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v5

    .line 2706656
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b25dc

    invoke-interface {v6, p2, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b25db

    invoke-interface {v6, v8, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b25db

    invoke-interface {v7, v8, v9}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v8, 0x7f0b0052

    invoke-virtual {v3, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    sget-object v8, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v8}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    const v8, 0x7f0a00d1

    invoke-virtual {v3, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    .line 2706657
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2706658
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/JYy;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/JYc;->b:LX/8ye;

    invoke-virtual {v3, p1}, LX/8ye;->c(LX/1De;)LX/8yc;

    move-result-object v3

    invoke-static {p1, v2}, LX/JYc;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1dc;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/8yc;->a(LX/1dc;)LX/8yc;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/8yc;->i(I)LX/8yc;

    move-result-object v3

    const/16 v5, 0x64

    invoke-virtual {v3, v5}, LX/8yc;->h(I)LX/8yc;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b25d9

    invoke-interface {v3, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0b25d8

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-interface {v3, v5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    :goto_0
    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {v0, v1}, LX/JYc;->a(LX/JYc;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v4, v0, LX/JYc;->c:LX/2g9;

    invoke-virtual {v4, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v4

    const/16 v5, 0x101

    invoke-virtual {v4, v5}, LX/2gA;->h(I)LX/2gA;

    move-result-object v4

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2706659
    const v4, 0xc026d4

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2706660
    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    :cond_0
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2706661
    return-object v0

    :cond_1
    move-object v3, v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2706662
    invoke-static {}, LX/1dS;->b()V

    .line 2706663
    iget v0, p1, LX/1dQ;->b:I

    .line 2706664
    sparse-switch v0, :sswitch_data_0

    .line 2706665
    :goto_0
    return-object v2

    .line 2706666
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2706667
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2706668
    check-cast v1, LX/JYa;

    .line 2706669
    iget-object v3, p0, LX/JYb;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JYc;

    iget-object v4, v1, LX/JYa;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706670
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2706671
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706672
    iget-object p1, v3, LX/JYc;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {}, LX/BOe;->b()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, p2, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2706673
    iget-object p1, v3, LX/JYc;->a:LX/0Zb;

    invoke-static {v5}, LX/JYy;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/BOe;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {p1, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2706674
    goto :goto_0

    .line 2706675
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2706676
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2706677
    check-cast v1, LX/JYa;

    .line 2706678
    iget-object v3, p0, LX/JYb;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JYc;

    iget-object v4, v1, LX/JYa;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2706679
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2706680
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2706681
    invoke-static {v3, v4}, LX/JYc;->a(LX/JYc;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2706682
    :goto_1
    goto :goto_0

    .line 2706683
    :cond_0
    invoke-static {v5}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p1, "attachment_type"

    const-string p0, "native"

    invoke-virtual {v5, p1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2706684
    iget-object v5, v3, LX/JYc;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v5, p0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0xc026d4 -> :sswitch_1
        0xc04802 -> :sswitch_0
    .end sparse-switch
.end method
