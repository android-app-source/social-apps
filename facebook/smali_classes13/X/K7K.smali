.class public LX/K7K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/K8f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;)V
    .locals 1

    .prologue
    .line 2773062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773063
    const-class v0, LX/K7K;

    invoke-static {v0, p0, p1}, LX/K7K;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2773064
    iput-object p2, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    .line 2773065
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/K7K;

    invoke-static {p0}, LX/K8f;->b(LX/0QB;)LX/K8f;

    move-result-object p0

    check-cast p0, LX/K8f;

    iput-object p0, p1, LX/K7K;->a:LX/K8f;

    return-void
.end method

.method public static d(LX/K7K;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773066
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2773067
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2773068
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/K7K;->b:Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel;->m()Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotSubscriptionsGraphQlModels$TarotPublisherSubscriptionInfoFieldsModel$TarotPublisherInfoModel;->a()Z

    move-result v0

    goto :goto_0
.end method
