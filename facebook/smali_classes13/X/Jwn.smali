.class public final LX/Jwn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2752544
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2752545
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2752546
    :goto_0
    return v1

    .line 2752547
    :cond_0
    const-string v6, "rssi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2752548
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2752549
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2752550
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2752551
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2752552
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2752553
    const-string v6, "pulsar"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2752554
    invoke-static {p0, p1}, LX/Jwm;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2752555
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2752556
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2752557
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2752558
    if-eqz v0, :cond_4

    .line 2752559
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 2752560
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2752561
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2752562
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2752563
    if-eqz v0, :cond_0

    .line 2752564
    const-string v1, "pulsar"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752565
    invoke-static {p0, v0, p2, p3}, LX/Jwm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2752566
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2752567
    if-eqz v0, :cond_1

    .line 2752568
    const-string v1, "rssi"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2752569
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2752570
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2752571
    return-void
.end method
