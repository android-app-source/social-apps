.class public final LX/Jx3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;",
        ">;",
        "LX/JxA;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JxB;


# direct methods
.method public constructor <init>(LX/JxB;)V
    .locals 0

    .prologue
    .line 2752802
    iput-object p1, p0, LX/Jx3;->a:LX/JxB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2752803
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2752804
    if-nez p1, :cond_0

    .line 2752805
    new-instance v0, LX/CeF;

    const-string v1, "input is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2752806
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2752807
    if-nez v0, :cond_1

    .line 2752808
    new-instance v0, LX/CeF;

    const-string v1, "input.getResult() is null"

    invoke-direct {v0, v1}, LX/CeF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2752809
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2752810
    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;

    invoke-virtual {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->k()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    .line 2752811
    iget-object v1, p0, LX/Jx3;->a:LX/JxB;

    .line 2752812
    iget-object v2, v1, LX/JxB;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Jx5;

    invoke-direct {v3, v1}, LX/Jx5;-><init>(LX/JxB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2752813
    iget-object v2, v1, LX/JxB;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Jx6;

    invoke-direct {v3, v1, v0}, LX/Jx6;-><init>(LX/JxB;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2752814
    invoke-static {v0}, LX/2dT;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2752815
    new-instance v0, Ljava/lang/IllegalAccessException;

    const-string v1, "Gravity no longer enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2752816
    :cond_2
    iget-object v0, p0, LX/Jx3;->a:LX/JxB;

    iget-object v1, v0, LX/JxB;->h:LX/0bW;

    const-string v2, "Got pulsar_scan result code: %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2752817
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2752818
    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;

    invoke-virtual {v0}, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLPulsarScanQueryResultCode;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2752819
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2752820
    check-cast v0, Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;

    invoke-static {v0}, LX/JxA;->b(Lcom/facebook/placetips/pulsarcore/graphql/PulsarScanQueryModels$SimplePulsarScanQueryModel;)LX/JxA;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
