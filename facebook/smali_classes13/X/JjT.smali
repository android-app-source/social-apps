.class public LX/JjT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/JjG;

.field public final c:LX/0tX;

.field public final d:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0SG;LX/JjG;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2727748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727749
    iput-object p1, p0, LX/JjT;->a:LX/0SG;

    .line 2727750
    iput-object p2, p0, LX/JjT;->b:LX/JjG;

    .line 2727751
    iput-object p3, p0, LX/JjT;->c:LX/0tX;

    .line 2727752
    iput-object p4, p0, LX/JjT;->d:LX/1Ck;

    .line 2727753
    return-void
.end method

.method public static a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gf;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/LightweightEventType;
        .end annotation
    .end param

    .prologue
    .line 2727737
    new-instance v6, LX/4Gf;

    invoke-direct {v6}, LX/4Gf;-><init>()V

    .line 2727738
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v7

    .line 2727739
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v8

    iget-object v1, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v4, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    iget v5, p4, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/JjT;->a(LX/JjT;Ljava/lang/String;JLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2727740
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    .line 2727741
    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2727742
    const-string v0, "context"

    invoke-virtual {v6, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2727743
    const-string v0, "thread_id"

    invoke-virtual {v6, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727744
    const-string v0, "event_type"

    invoke-virtual {v6, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727745
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2727746
    const-string v1, "event_time"

    invoke-virtual {v6, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2727747
    return-object v6
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gi;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Calendar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727721
    new-instance v6, LX/4Gi;

    invoke-direct {v6}, LX/4Gi;-><init>()V

    .line 2727722
    const-string v0, "event_id"

    invoke-virtual {v6, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727723
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v7

    .line 2727724
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v8

    iget-object v1, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    if-nez p3, :cond_3

    const-wide/16 v2, 0x0

    :goto_0
    iget-object v4, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    iget v5, p5, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/JjT;->a(LX/JjT;Ljava/lang/String;JLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2727725
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    .line 2727726
    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2727727
    const-string v0, "context"

    invoke-virtual {v6, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2727728
    if-eqz p3, :cond_0

    .line 2727729
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2727730
    const-string v1, "event_time"

    invoke-virtual {v6, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2727731
    :cond_0
    if-eqz p2, :cond_1

    .line 2727732
    const-string v0, "title"

    invoke-virtual {v6, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727733
    :cond_1
    if-eqz p4, :cond_2

    .line 2727734
    const-string v0, "location_name"

    invoke-virtual {v6, v0, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727735
    :cond_2
    return-object v6

    .line 2727736
    :cond_3
    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    goto :goto_0
.end method

.method private static a(LX/JjT;Ljava/lang/String;JLjava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p0    # LX/JjT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727710
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2727711
    const-string v1, "conversation_size"

    invoke-virtual {v0, v1, p5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2727712
    if-eqz p1, :cond_0

    .line 2727713
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2727714
    :cond_0
    if-eqz p4, :cond_1

    .line 2727715
    const-string v1, "triggered_word"

    invoke-virtual {v0, v1, p4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2727716
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-lez v1, :cond_2

    .line 2727717
    const-string v1, "time_until_reminder"

    .line 2727718
    const-wide/16 v4, 0x0

    iget-object v6, p0, LX/JjT;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v6, p2, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    move-wide v2, v4

    .line 2727719
    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2727720
    :cond_2
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;LX/Jj7;)V
    .locals 5
    .param p0    # LX/JjT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Calendar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/messaging/events/banner/EventReminderParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2727754
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2727755
    :goto_0
    return-void

    .line 2727756
    :cond_0
    invoke-direct/range {p0 .. p5}, LX/JjT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gi;

    move-result-object v0

    .line 2727757
    new-instance v1, LX/Jjn;

    invoke-direct {v1}, LX/Jjn;-><init>()V

    move-object v1, v1

    .line 2727758
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Jjn;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2727759
    iget-object v1, p0, LX/JjT;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2727760
    new-instance v1, LX/JjQ;

    invoke-direct {v1, p0, p6, p1}, LX/JjQ;-><init>(LX/JjT;LX/Jj7;Ljava/lang/String;)V

    .line 2727761
    iget-object v2, p0, LX/JjT;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-updateEvent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/399;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/LightweightEventGuestStatus;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/events/banner/EventReminderParams;",
            ")",
            "LX/399",
            "<",
            "Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventRsvpModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2727700
    new-instance v6, LX/4Gh;

    invoke-direct {v6}, LX/4Gh;-><init>()V

    .line 2727701
    const-string v0, "event_id"

    invoke-virtual {v6, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727702
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v7

    .line 2727703
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v8

    iget-object v1, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    iget-wide v2, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    iget-object v4, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    iget v5, p3, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/JjT;->a(LX/JjT;Ljava/lang/String;JLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2727704
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    .line 2727705
    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2727706
    const-string v0, "context"

    invoke-virtual {v6, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2727707
    const-string v0, "guest_list_state"

    invoke-virtual {v6, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727708
    new-instance v0, LX/Jjm;

    invoke-direct {v0}, LX/Jjm;-><init>()V

    move-object v0, v0

    .line 2727709
    const-string v1, "input"

    invoke-virtual {v0, v1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Jjm;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/JjT;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/4Gg;
    .locals 9

    .prologue
    .line 2727692
    new-instance v6, LX/4Gg;

    invoke-direct {v6}, LX/4Gg;-><init>()V

    .line 2727693
    const-string v0, "event_id"

    invoke-virtual {v6, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2727694
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v7

    .line 2727695
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    iget-object v1, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    move-result-object v8

    iget-object v1, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->g:Ljava/lang/String;

    iget-wide v2, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->c:J

    iget-object v4, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->j:Ljava/lang/String;

    iget v5, p2, Lcom/facebook/messaging/events/banner/EventReminderParams;->b:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/JjT;->a(LX/JjT;Ljava/lang/String;JLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2727696
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    .line 2727697
    invoke-static {v7, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2727698
    const-string v0, "context"

    invoke-virtual {v6, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2727699
    return-object v6
.end method

.method public static b(LX/0QB;)LX/JjT;
    .locals 5

    .prologue
    .line 2727682
    new-instance v4, LX/JjT;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    .line 2727683
    new-instance v2, LX/JjG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v1}, LX/JjG;-><init>(LX/03V;)V

    .line 2727684
    move-object v1, v2

    .line 2727685
    check-cast v1, LX/JjG;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/JjT;-><init>(LX/0SG;LX/JjG;LX/0tX;LX/1Ck;)V

    .line 2727686
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/LightweightEventGuestStatus;
        .end annotation
    .end param

    .prologue
    .line 2727687
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DECLINED"

    if-eq p2, v0, :cond_1

    const-string v0, "GOING"

    if-eq p2, v0, :cond_1

    .line 2727688
    :cond_0
    :goto_0
    return-void

    .line 2727689
    :cond_1
    iget-object v0, p0, LX/JjT;->c:LX/0tX;

    invoke-direct {p0, p1, p2, p3}, LX/JjT;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2727690
    new-instance v1, LX/JjR;

    invoke-direct {v1, p0}, LX/JjR;-><init>(LX/JjT;)V

    .line 2727691
    iget-object v2, p0, LX/JjT;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-rsvpEvent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
