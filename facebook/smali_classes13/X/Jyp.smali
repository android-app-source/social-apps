.class public LX/Jyp;
.super LX/AOu;
.source ""


# instance fields
.field public a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2755474
    invoke-direct {p0, p1}, LX/AOu;-><init>(LX/0Sh;)V

    .line 2755475
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2755476
    iget-object v0, p0, LX/Jyp;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2755477
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2755478
    iget-object v0, p0, LX/Jyp;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2755479
    new-instance v1, LX/Jyh;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Jyh;-><init>(Landroid/content/Context;)V

    .line 2755480
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2755481
    iget-object v0, p0, LX/Jyp;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;->j()Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    move-result-object v2

    iget-object v0, p0, LX/Jyp;->a:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2755482
    iput-object v2, v1, LX/Jyh;->g:Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;

    .line 2755483
    new-instance v3, LX/Jyg;

    invoke-direct {v3, v1}, LX/Jyg;-><init>(LX/Jyh;)V

    iput-object v3, v1, LX/Jyh;->e:Landroid/view/View$OnClickListener;

    .line 2755484
    iget-object v3, v1, LX/Jyh;->e:Landroid/view/View$OnClickListener;

    .line 2755485
    iget-object v4, v1, LX/Jyh;->c:Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;

    const v5, 0x7f0209e1

    const v6, 0x7f083c23

    const v7, 0x7f0208ec

    move-object v8, v0

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Lcom/facebook/profile/insight/view/ProfileInsightCardTitleView;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2755486
    new-instance v4, LX/Jyo;

    invoke-direct {v4, v2}, LX/Jyo;-><init>(Lcom/facebook/profile/insight/protocol/FetchProfileInsightsGraphQLModels$FetchProfileInsightModel$ProfileInsightsModel$SummaryModel;)V

    .line 2755487
    new-instance v5, LX/1P0;

    invoke-virtual {v1}, LX/Jyh;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2755488
    new-instance v6, LX/62X;

    invoke-virtual {v1}, LX/Jyh;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a00fb

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    const/4 v8, 0x2

    invoke-direct {v6, v7, v8}, LX/62X;-><init>(II)V

    .line 2755489
    const/4 v7, 0x1

    .line 2755490
    iput-boolean v7, v6, LX/62X;->e:Z

    .line 2755491
    iget-object v7, v1, LX/Jyh;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v7, v6}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2755492
    iget-object v6, v1, LX/Jyh;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2755493
    iget-object v5, v1, LX/Jyh;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2755494
    invoke-static {v1}, LX/Jyh;->a(LX/Jyh;)V

    .line 2755495
    return-object v1
.end method

.method public final c(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2755496
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2755497
    return-void
.end method
