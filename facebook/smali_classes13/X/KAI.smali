.class public LX/KAI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/KAG;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/KAJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2779180
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/KAI;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/KAJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779181
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2779182
    iput-object p1, p0, LX/KAI;->b:LX/0Ot;

    .line 2779183
    return-void
.end method

.method public static a(LX/0QB;)LX/KAI;
    .locals 4

    .prologue
    .line 2779184
    const-class v1, LX/KAI;

    monitor-enter v1

    .line 2779185
    :try_start_0
    sget-object v0, LX/KAI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779186
    sput-object v2, LX/KAI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779187
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779188
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779189
    new-instance v3, LX/KAI;

    const/16 p0, 0x38ba

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/KAI;-><init>(LX/0Ot;)V

    .line 2779190
    move-object v0, v3

    .line 2779191
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779192
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/KAI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779193
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2779195
    iget-object v0, p0, LX/KAI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p2, 0x7

    const/4 p0, 0x2

    const/4 v5, 0x0

    .line 2779196
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f0b2712

    invoke-interface {v1, v2, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f0206b1

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2712

    invoke-interface {v2, p2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b2713

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e0c71

    invoke-static {p1, v5, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    const v4, 0x7f083c48

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e0123

    invoke-static {p1, v5, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    const v4, 0x7f083c49

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2779197
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a009f

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b2711

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b2712

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 2779198
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2712

    invoke-interface {v2, p2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/KAR;->c(LX/1De;)LX/KAP;

    move-result-object v3

    .line 2779199
    const v4, -0x8b37486

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2779200
    invoke-virtual {v3, v4}, LX/KAP;->a(LX/1dQ;)LX/KAP;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/KAM;->c(LX/1De;)LX/KAK;

    move-result-object v3

    .line 2779201
    const v4, -0x8b36ed5

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2779202
    invoke-virtual {v3, v4}, LX/KAK;->a(LX/1dQ;)LX/KAK;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2779203
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2779204
    invoke-static {}, LX/1dS;->b()V

    .line 2779205
    iget v0, p1, LX/1dQ;->b:I

    .line 2779206
    sparse-switch v0, :sswitch_data_0

    .line 2779207
    :goto_0
    return-object v2

    .line 2779208
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2779209
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2779210
    check-cast v1, LX/KAH;

    .line 2779211
    iget-object v3, p0, LX/KAI;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAJ;

    iget-object v4, v1, LX/KAH;->a:Ljava/lang/String;

    .line 2779212
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2779213
    const-string p1, "ref"

    const-string p2, "FEED_END_STATE"

    invoke-virtual {v5, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779214
    iget-object p1, v3, LX/KAJ;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->N:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p1, p2, p0, v5, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2779215
    iget-object v5, v3, LX/KAJ;->d:LX/0gh;

    const-string p1, "tap_end_of_list_button"

    invoke-virtual {v5, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2779216
    iget-object v5, v3, LX/KAJ;->a:LX/KAO;

    const-string p1, "feed_end_find_groups_components"

    const-string p2, "create_group"

    invoke-virtual {v5, v4, p1, p2}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779217
    goto :goto_0

    .line 2779218
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2779219
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2779220
    check-cast v1, LX/KAH;

    .line 2779221
    iget-object v3, p0, LX/KAI;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/KAJ;

    iget-object v4, v1, LX/KAH;->a:Ljava/lang/String;

    .line 2779222
    iget-boolean v5, v3, LX/KAJ;->c:Z

    if-eqz v5, :cond_0

    .line 2779223
    iget-object v5, v3, LX/KAJ;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->ad:Ljava/lang/String;

    invoke-interface {v5, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2779224
    :goto_1
    iget-object v5, v3, LX/KAJ;->d:LX/0gh;

    const-string p0, "tap_end_of_list_button"

    invoke-virtual {v5, p0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2779225
    iget-object v5, v3, LX/KAJ;->a:LX/KAO;

    const-string p0, "feed_end_find_groups_components"

    const-string v1, "join_groups"

    invoke-virtual {v5, v4, p0, v1}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779226
    goto :goto_0

    .line 2779227
    :cond_0
    iget-object v5, v3, LX/KAJ;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->E:Ljava/lang/String;

    invoke-interface {v5, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x8b37486 -> :sswitch_1
        -0x8b36ed5 -> :sswitch_0
    .end sparse-switch
.end method
