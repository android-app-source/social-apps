.class public final LX/K9h;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/livemap/LiveMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/livemap/LiveMapFragment;)V
    .locals 0

    .prologue
    .line 2777701
    iput-object p1, p0, LX/K9h;->a:Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2777702
    iget-object v0, p0, LX/K9h;->a:Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-static {v0, p1}, Lcom/facebook/video/livemap/LiveMapFragment;->a$redex0(Lcom/facebook/video/livemap/LiveMapFragment;Ljava/lang/Throwable;)V

    .line 2777703
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2777704
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2777705
    if-eqz p1, :cond_0

    .line 2777706
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2777707
    if-eqz v0, :cond_0

    .line 2777708
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2777709
    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2777710
    :cond_0
    iget-object v0, p0, LX/K9h;->a:Lcom/facebook/video/livemap/LiveMapFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/video/livemap/LiveMapFragment;->a$redex0(Lcom/facebook/video/livemap/LiveMapFragment;Ljava/lang/Throwable;)V

    .line 2777711
    :goto_0
    return-void

    .line 2777712
    :cond_1
    iget-object v1, p0, LX/K9h;->a:Lcom/facebook/video/livemap/LiveMapFragment;

    .line 2777713
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2777714
    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    .line 2777715
    invoke-static {v1}, Lcom/facebook/video/livemap/LiveMapFragment;->b(Lcom/facebook/video/livemap/LiveMapFragment;)V

    .line 2777716
    iget-object p0, v1, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance p1, LX/K9i;

    invoke-direct {p1, v1, v0}, LX/K9i;-><init>(Lcom/facebook/video/livemap/LiveMapFragment;Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;)V

    invoke-virtual {p0, p1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2777717
    goto :goto_0
.end method
