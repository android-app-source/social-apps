.class public LX/JZL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/2yS;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:LX/JZQ;

.field private final e:LX/K91;

.field private final f:LX/11S;


# direct methods
.method public constructor <init>(LX/2yS;LX/JZQ;LX/0Or;LX/11S;LX/K91;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2yS;",
            "LX/JZQ;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/11S;",
            "LX/K91;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2708403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2708404
    iput-object p1, p0, LX/JZL;->a:LX/2yS;

    .line 2708405
    iput-object p2, p0, LX/JZL;->d:LX/JZQ;

    .line 2708406
    iput-object p3, p0, LX/JZL;->b:LX/0Or;

    .line 2708407
    iput-object p5, p0, LX/JZL;->e:LX/K91;

    .line 2708408
    iput-object p4, p0, LX/JZL;->f:LX/11S;

    .line 2708409
    iget-object v0, p0, LX/JZL;->e:LX/K91;

    invoke-virtual {v0}, LX/K91;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JZL;->c:Ljava/lang/String;

    .line 2708410
    return-void
.end method

.method public static a(LX/0QB;)LX/JZL;
    .locals 9

    .prologue
    .line 2708411
    const-class v1, LX/JZL;

    monitor-enter v1

    .line 2708412
    :try_start_0
    sget-object v0, LX/JZL;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2708413
    sput-object v2, LX/JZL;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2708414
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2708415
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2708416
    new-instance v3, LX/JZL;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v4

    check-cast v4, LX/2yS;

    invoke-static {v0}, LX/JZQ;->a(LX/0QB;)LX/JZQ;

    move-result-object v5

    check-cast v5, LX/JZQ;

    const/16 v6, 0x509

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v7

    check-cast v7, LX/11S;

    invoke-static {v0}, LX/K91;->a(LX/0QB;)LX/K91;

    move-result-object v8

    check-cast v8, LX/K91;

    invoke-direct/range {v3 .. v8}, LX/JZL;-><init>(LX/2yS;LX/JZQ;LX/0Or;LX/11S;LX/K91;)V

    .line 2708417
    move-object v0, v3

    .line 2708418
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2708419
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JZL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2708420
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2708421
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIILandroid/view/View$OnClickListener;Landroid/graphics/Typeface;Lcom/facebook/graphql/model/GraphQLTarotDigest;Ljava/lang/Boolean;)LX/1Dg;
    .locals 10
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # J
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/graphql/model/GraphQLTarotDigest;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # Ljava/lang/Boolean;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2708422
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0b2603

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    iget-object v4, p0, LX/JZL;->f:LX/11S;

    sget-object v5, LX/1lB;->MONTH_DAY_LONG_STYLE:LX/1lB;

    const-wide/16 v6, 0x3e8

    mul-long v6, v6, p6

    invoke-interface {v4, v5, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b2604

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b2604

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    iget-object v4, p0, LX/JZL;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b2604

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2708423
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    iget-object v2, p0, LX/JZL;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, p3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    sget-object v4, LX/1Up;->b:LX/1Up;

    invoke-virtual {v2, v4}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    .line 2708424
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x80

    const/16 v6, 0xff

    const/16 v7, 0xff

    const/16 v8, 0xff

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-interface {v4, v5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x64

    invoke-interface {v4, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    .line 2708425
    mul-int/lit8 v5, p8, 0x7

    div-int/lit8 v5, v5, 0x8

    .line 2708426
    mul-int/lit8 v6, p9, 0x17

    div-int/lit8 v6, v6, 0x64

    .line 2708427
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b2602

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v5}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v5

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x0

    const v6, 0x7f0b2603

    invoke-interface {v2, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x1

    const v6, 0x7f0b2608

    invoke-interface {v2, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x3

    const v6, 0x7f0b2605

    invoke-interface {v2, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    const v7, 0x7f0b2603

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const/4 v5, -0x1

    invoke-virtual {v2, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b2606

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    if-nez p11, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v5, v2}, LX/1ne;->j(F)LX/1ne;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v2

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v5, 0x0

    const v6, 0x7f0b2603

    invoke-interface {v2, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v5, 0x3

    const v6, 0x7f0b2605

    invoke-interface {v2, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v5, 0x1

    const v6, 0x7f0b2605

    invoke-interface {v2, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v3, p0, LX/JZL;->a:LX/2yS;

    invoke-virtual {v3, p1}, LX/2yS;->c(LX/1De;)LX/AE2;

    move-result-object v3

    iget-object v4, p0, LX/JZL;->e:LX/K91;

    invoke-virtual {v4}, LX/K91;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/AE2;->a(Ljava/lang/CharSequence;)LX/AE2;

    move-result-object v3

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    const/16 v7, 0xff

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, LX/AE2;->h(I)LX/AE2;

    move-result-object v3

    const v4, 0x7f0200fb

    invoke-virtual {v3, v4}, LX/AE2;->j(I)LX/AE2;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/AE2;->c(F)LX/AE2;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, LX/AE2;->a(Landroid/view/View$OnClickListener;)LX/AE2;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    const v5, 0x7f0b2605

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    .line 2708428
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 v4, 0x80

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-interface {v2, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v2

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v4

    .line 2708429
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    iget-object v2, p0, LX/JZL;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, p4}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    sget-object v5, LX/1Up;->g:LX/1Up;

    invoke-virtual {v2, v5}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    .line 2708430
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    move/from16 v0, p9

    invoke-interface {v5, v0}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v5

    invoke-virtual/range {p13 .. p13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v2, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    :goto_1
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    return-object v2

    .line 2708431
    :cond_0
    const v2, 0x3f99999a    # 1.2f

    goto/16 :goto_0

    .line 2708432
    :cond_1
    iget-object v2, p0, LX/JZL;->d:LX/JZQ;

    invoke-virtual {v2, p1}, LX/JZQ;->c(LX/1De;)LX/JZO;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/JZO;->a(Lcom/facebook/graphql/model/GraphQLTarotDigest;)LX/JZO;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v2, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_1
.end method
