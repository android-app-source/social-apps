.class public LX/JYl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYj;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2707127
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JYl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707128
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2707129
    iput-object p1, p0, LX/JYl;->b:LX/0Ot;

    .line 2707130
    return-void
.end method

.method public static a(LX/0QB;)LX/JYl;
    .locals 4

    .prologue
    .line 2707131
    const-class v1, LX/JYl;

    monitor-enter v1

    .line 2707132
    :try_start_0
    sget-object v0, LX/JYl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707133
    sput-object v2, LX/JYl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707134
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707135
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707136
    new-instance v3, LX/JYl;

    const/16 p0, 0x2186

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYl;-><init>(LX/0Ot;)V

    .line 2707137
    move-object v0, v3

    .line 2707138
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707139
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707140
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2707142
    check-cast p2, LX/JYk;

    .line 2707143
    iget-object v0, p0, LX/JYl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;

    iget-boolean v2, p2, LX/JYk;->a:Z

    iget-object v3, p2, LX/JYk;->b:Landroid/net/Uri;

    iget-object v4, p2, LX/JYk;->c:Ljava/lang/String;

    iget-object v5, p2, LX/JYk;->d:Ljava/lang/CharSequence;

    move-object v1, p1

    const/16 p0, 0x8

    const/4 v8, 0x1

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 2707144
    if-eqz v2, :cond_0

    const v6, 0x7f0a0098

    .line 2707145
    :goto_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    if-eqz v2, :cond_1

    move v7, v8

    :goto_1
    invoke-interface {v10, v7}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0, v9}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v7

    const v10, 0x7f0b25fe

    invoke-interface {v7, p0, v10}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const/4 v10, 0x2

    invoke-interface {v7, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v10, 0x3

    invoke-interface {v7, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    iget-object v10, v0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->b:LX/1nu;

    invoke-virtual {v10, v1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v10

    sget-object p0, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v10

    invoke-virtual {v10, p2}, LX/1nw;->c(F)LX/1nw;

    move-result-object v10

    const p0, 0x7f020bc5

    invoke-virtual {v10, p0}, LX/1nw;->l(I)LX/1nw;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const p0, 0x7f0b25ff

    invoke-interface {v10, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v10

    const/4 p0, 0x5

    const p1, 0x7f0b25fe

    invoke-interface {v10, p0, p1}, LX/1Di;->g(II)LX/1Di;

    move-result-object v10

    .line 2707146
    const p0, 0x3245d19

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2707147
    invoke-interface {v10, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v10

    invoke-interface {v7, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b0054

    invoke-static {v1, v4, v9, v6, v2}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;IIZ)LX/1ne;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-static {v1, v5, v9, v6, v2}, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;IIZ)LX/1ne;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2707148
    return-object v0

    .line 2707149
    :cond_0
    const v6, 0x7f0a00ab

    goto/16 :goto_0

    :cond_1
    move v7, v9

    .line 2707150
    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2707151
    invoke-static {}, LX/1dS;->b()V

    .line 2707152
    iget v0, p1, LX/1dQ;->b:I

    .line 2707153
    packed-switch v0, :pswitch_data_0

    .line 2707154
    :goto_0
    return-object v2

    .line 2707155
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2707156
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2707157
    check-cast v1, LX/JYk;

    .line 2707158
    iget-object p1, p0, LX/JYl;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;

    iget-object p2, v1, LX/JYk;->e:Ljava/lang/String;

    .line 2707159
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2707160
    iget-object p0, p1, Lcom/facebook/feedplugins/socialgood/FundraiserAttachmentHeaderContentComponentSpec;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2707161
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3245d19
        :pswitch_0
    .end packed-switch
.end method
