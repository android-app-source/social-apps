.class public LX/K5i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/tarot/cards/TarotCardEndCard;

.field public b:LX/K73;

.field public c:LX/K6F;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;LX/K73;)V
    .locals 0

    .prologue
    .line 2770627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770628
    iput-object p1, p0, LX/K5i;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    .line 2770629
    iput-object p2, p0, LX/K5i;->b:LX/K73;

    .line 2770630
    iget-object p1, p0, LX/K5i;->b:LX/K73;

    sget-object p2, LX/K73;->NONE:LX/K73;

    if-eq p1, p2, :cond_0

    iget-object p1, p0, LX/K5i;->b:LX/K73;

    sget-object p2, LX/K73;->FOLLOWING_AND_SUBSCRIBED:LX/K73;

    if-ne p1, p2, :cond_2

    .line 2770631
    :cond_0
    sget-object p1, LX/K6F;->ALL_CAUGHT_UP:LX/K6F;

    iput-object p1, p0, LX/K5i;->c:LX/K6F;

    .line 2770632
    :cond_1
    :goto_0
    invoke-static {p0}, LX/K5i;->b(LX/K5i;)V

    .line 2770633
    return-void

    .line 2770634
    :cond_2
    iget-object p1, p0, LX/K5i;->b:LX/K73;

    sget-object p2, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    if-ne p1, p2, :cond_3

    .line 2770635
    sget-object p1, LX/K6F;->NOT_FOLLOWING_INITIALLY:LX/K6F;

    iput-object p1, p0, LX/K5i;->c:LX/K6F;

    goto :goto_0

    .line 2770636
    :cond_3
    iget-object p1, p0, LX/K5i;->b:LX/K73;

    sget-object p2, LX/K73;->FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    if-ne p1, p2, :cond_1

    .line 2770637
    sget-object p1, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED:LX/K6F;

    iput-object p1, p0, LX/K5i;->c:LX/K6F;

    goto :goto_0
.end method

.method public static b(LX/K5i;)V
    .locals 2

    .prologue
    .line 2770638
    iget-object v0, p0, LX/K5i;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, p0, LX/K5i;->c:LX/K6F;

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/cards/TarotCardEndCard;->a(LX/K6F;)V

    .line 2770639
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2770640
    if-eqz p1, :cond_2

    .line 2770641
    iget-object v0, p0, LX/K5i;->b:LX/K73;

    sget-object v1, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    if-ne v0, v1, :cond_1

    .line 2770642
    sget-object v0, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED:LX/K6F;

    iput-object v0, p0, LX/K5i;->c:LX/K6F;

    .line 2770643
    :cond_0
    :goto_0
    invoke-static {p0}, LX/K5i;->b(LX/K5i;)V

    .line 2770644
    return-void

    .line 2770645
    :cond_1
    iget-object v0, p0, LX/K5i;->b:LX/K73;

    sget-object v1, LX/K73;->FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    if-ne v0, v1, :cond_0

    .line 2770646
    sget-object v0, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_SUBSCRIBED:LX/K6F;

    iput-object v0, p0, LX/K5i;->c:LX/K6F;

    goto :goto_0

    .line 2770647
    :cond_2
    iget-object v0, p0, LX/K5i;->b:LX/K73;

    sget-object v1, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    if-ne v0, v1, :cond_3

    .line 2770648
    sget-object v0, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_UNSUBSCRIBED:LX/K6F;

    iput-object v0, p0, LX/K5i;->c:LX/K6F;

    goto :goto_0

    .line 2770649
    :cond_3
    sget-object v0, LX/K6F;->FOLLOWING_INITIALLY_BUT_JUST_UNSUBSCRIBED:LX/K6F;

    iput-object v0, p0, LX/K5i;->c:LX/K6F;

    goto :goto_0
.end method
