.class public LX/JW4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/17W;

.field public final b:LX/JVx;

.field private final c:LX/JVy;


# direct methods
.method public constructor <init>(LX/17W;LX/JVx;LX/JVy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2702322
    iput-object p1, p0, LX/JW4;->a:LX/17W;

    .line 2702323
    iput-object p2, p0, LX/JW4;->b:LX/JVx;

    .line 2702324
    iput-object p3, p0, LX/JW4;->c:LX/JVy;

    .line 2702325
    return-void
.end method

.method public static a(LX/0QB;)LX/JW4;
    .locals 6

    .prologue
    .line 2702326
    const-class v1, LX/JW4;

    monitor-enter v1

    .line 2702327
    :try_start_0
    sget-object v0, LX/JW4;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702328
    sput-object v2, LX/JW4;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702329
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702330
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702331
    new-instance p0, LX/JW4;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/JVx;->a(LX/0QB;)LX/JVx;

    move-result-object v4

    check-cast v4, LX/JVx;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v5

    check-cast v5, LX/JVy;

    invoke-direct {p0, v3, v4, v5}, LX/JW4;-><init>(LX/17W;LX/JVx;LX/JVy;)V

    .line 2702332
    move-object v0, p0

    .line 2702333
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702334
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JW4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702335
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702336
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
