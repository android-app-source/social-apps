.class public LX/Jxx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2753872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2753873
    return-void
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/Integer;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "create"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 2753874
    invoke-static {p0}, LX/Jxx;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2753875
    new-instance v0, LX/JyL;

    invoke-direct {v0}, LX/JyL;-><init>()V

    move-object v0, v0

    .line 2753876
    const-string v1, "referral_type"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2753877
    iput-object v1, v0, LX/JyL;->e:Ljava/lang/String;

    .line 2753878
    move-object v0, v0

    .line 2753879
    const-string v1, "referral_id"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2753880
    iput-object v1, v0, LX/JyL;->d:Ljava/lang/String;

    .line 2753881
    move-object v0, v0

    .line 2753882
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2753883
    iput-object v1, v0, LX/JyL;->c:Ljava/lang/String;

    .line 2753884
    move-object v0, v0

    .line 2753885
    :goto_0
    invoke-static {p1, v0}, LX/Jxx;->a(Ljava/lang/Integer;LX/JyL;)V

    .line 2753886
    new-instance v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    invoke-direct {v1, v0}, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;-><init>(LX/JyL;)V

    move-object v0, v1

    .line 2753887
    return-object v0

    .line 2753888
    :cond_0
    invoke-static {p0}, LX/Jxx;->b(Landroid/content/Intent;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    move-result-object v0

    .line 2753889
    new-instance v1, LX/JyL;

    invoke-direct {v1, v0}, LX/JyL;-><init>(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V

    move-object v0, v1

    .line 2753890
    goto :goto_0
.end method

.method private static a(Ljava/lang/Integer;LX/JyL;)V
    .locals 3

    .prologue
    .line 2753891
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2753892
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected sessionType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2753893
    :pswitch_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2753894
    iput-object v0, p1, LX/JyL;->b:Ljava/lang/String;

    .line 2753895
    :goto_0
    return-void

    .line 2753896
    :pswitch_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2753897
    iput-object v0, p1, LX/JyL;->a:Ljava/lang/String;

    .line 2753898
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2753899
    invoke-static {p0}, LX/Jxx;->b(Landroid/content/Intent;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    move-result-object v0

    .line 2753900
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Intent;)Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2753901
    const-string v0, "discovery_curation_logging_data"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    return-object v0
.end method
