.class public final enum LX/Jdk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jdk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jdk;

.field public static final enum MESSAGES:LX/Jdk;

.field public static final enum PROMOTION_MESSAGES:LX/Jdk;

.field public static final enum SUBSCRIPTION_MESSAGES:LX/Jdk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2719922
    new-instance v0, LX/Jdk;

    const-string v1, "MESSAGES"

    invoke-direct {v0, v1, v2}, LX/Jdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdk;->MESSAGES:LX/Jdk;

    .line 2719923
    new-instance v0, LX/Jdk;

    const-string v1, "PROMOTION_MESSAGES"

    invoke-direct {v0, v1, v3}, LX/Jdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdk;->PROMOTION_MESSAGES:LX/Jdk;

    .line 2719924
    new-instance v0, LX/Jdk;

    const-string v1, "SUBSCRIPTION_MESSAGES"

    invoke-direct {v0, v1, v4}, LX/Jdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jdk;->SUBSCRIPTION_MESSAGES:LX/Jdk;

    .line 2719925
    const/4 v0, 0x3

    new-array v0, v0, [LX/Jdk;

    sget-object v1, LX/Jdk;->MESSAGES:LX/Jdk;

    aput-object v1, v0, v2

    sget-object v1, LX/Jdk;->PROMOTION_MESSAGES:LX/Jdk;

    aput-object v1, v0, v3

    sget-object v1, LX/Jdk;->SUBSCRIPTION_MESSAGES:LX/Jdk;

    aput-object v1, v0, v4

    sput-object v0, LX/Jdk;->$VALUES:[LX/Jdk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2719926
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jdk;
    .locals 1

    .prologue
    .line 2719927
    const-class v0, LX/Jdk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jdk;

    return-object v0
.end method

.method public static values()[LX/Jdk;
    .locals 1

    .prologue
    .line 2719928
    sget-object v0, LX/Jdk;->$VALUES:[LX/Jdk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jdk;

    return-object v0
.end method
