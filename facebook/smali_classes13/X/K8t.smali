.class public LX/K8t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field public final i:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776340
    const-string v0, "nav_from_digest_id"

    iput-object v0, p0, LX/K8t;->a:Ljava/lang/String;

    .line 2776341
    const-string v0, "nav_to_digest_id"

    iput-object v0, p0, LX/K8t;->b:Ljava/lang/String;

    .line 2776342
    const-string v0, "nav_from_card_deck_index"

    iput-object v0, p0, LX/K8t;->c:Ljava/lang/String;

    .line 2776343
    const-string v0, "nav_to_card_deck_index"

    iput-object v0, p0, LX/K8t;->d:Ljava/lang/String;

    .line 2776344
    const-string v0, "nav_from_card_deck_id"

    iput-object v0, p0, LX/K8t;->e:Ljava/lang/String;

    .line 2776345
    const-string v0, "nav_to_card_deck_id"

    iput-object v0, p0, LX/K8t;->f:Ljava/lang/String;

    .line 2776346
    const-string v0, "nav_from_pager_index"

    iput-object v0, p0, LX/K8t;->g:Ljava/lang/String;

    .line 2776347
    const-string v0, "nav_to_pager_index"

    iput-object v0, p0, LX/K8t;->h:Ljava/lang/String;

    .line 2776348
    iput-object p1, p0, LX/K8t;->i:LX/0if;

    .line 2776349
    return-void
.end method

.method public static a(LX/0QB;)LX/K8t;
    .locals 4

    .prologue
    .line 2776328
    const-class v1, LX/K8t;

    monitor-enter v1

    .line 2776329
    :try_start_0
    sget-object v0, LX/K8t;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2776330
    sput-object v2, LX/K8t;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2776331
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2776332
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2776333
    new-instance p0, LX/K8t;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/K8t;-><init>(LX/0if;)V

    .line 2776334
    move-object v0, p0

    .line 2776335
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2776336
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/K8t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2776337
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2776338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
