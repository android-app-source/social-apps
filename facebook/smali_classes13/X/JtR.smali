.class public final LX/JtR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<[",
        "Lcom/google/android/gms/wearable/Asset;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/JtS;

.field public final synthetic c:LX/JtU;


# direct methods
.method public constructor <init>(LX/JtU;Lcom/google/common/util/concurrent/SettableFuture;LX/JtS;)V
    .locals 0

    .prologue
    .line 2746773
    iput-object p1, p0, LX/JtR;->c:LX/JtU;

    iput-object p2, p0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/JtR;->b:LX/JtS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2746733
    sget-object v0, LX/JtU;->a:Ljava/lang/Class;

    const-string v1, "Media Data check in Cache failed with error"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2746734
    iget-object v0, p0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2746735
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2746736
    check-cast p1, [Lcom/google/android/gms/wearable/Asset;

    .line 2746737
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2746738
    if-eqz p1, :cond_1

    .line 2746739
    iget-object v0, p0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/JtT;

    iget-object v2, p0, LX/JtR;->b:LX/JtS;

    iget-object v2, v2, LX/JtS;->b:Ljava/lang/String;

    iget-object v3, p0, LX/JtR;->b:LX/JtS;

    iget-object v3, v3, LX/JtS;->c:LX/Jt0;

    invoke-direct {v1, v2, p1, v3}, LX/JtT;-><init>(Ljava/lang/String;[Lcom/google/android/gms/wearable/Asset;LX/Jt0;)V

    const v2, -0x197eac97

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2746740
    :goto_1
    return-void

    .line 2746741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2746742
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/JtR;->b:LX/JtS;

    iget-object v0, v0, LX/JtS;->c:LX/Jt0;

    sget-object v1, LX/Jt0;->STICKER:LX/Jt0;

    if-ne v0, v1, :cond_2

    .line 2746743
    iget-object v0, p0, LX/JtR;->c:LX/JtU;

    iget-object v0, v0, LX/JtU;->b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v1, p0, LX/JtR;->b:LX/JtS;

    iget-object v1, v1, LX/JtS;->b:Ljava/lang/String;

    .line 2746744
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2746745
    iget-object v3, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3dt;

    invoke-virtual {v3, v1}, LX/3dt;->d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v3

    .line 2746746
    if-eqz v3, :cond_3

    .line 2746747
    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2746748
    :goto_2
    move-object v3, v3

    .line 2746749
    new-instance v4, LX/JtJ;

    invoke-direct {v4, v0, v2, v1}, LX/JtJ;-><init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2746750
    move-object v0, v2

    .line 2746751
    :goto_3
    new-instance v1, LX/JtQ;

    invoke-direct {v1, p0}, LX/JtQ;-><init>(LX/JtR;)V

    iget-object v2, p0, LX/JtR;->c:LX/JtU;

    iget-object v2, v2, LX/JtU;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2746752
    :catch_0
    move-exception v0

    .line 2746753
    iget-object v1, p0, LX/JtR;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_1

    .line 2746754
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/JtR;->c:LX/JtU;

    iget-object v0, v0, LX/JtU;->b:Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;

    iget-object v1, p0, LX/JtR;->b:LX/JtS;

    iget-object v1, v1, LX/JtS;->a:Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 2746755
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2746756
    const/16 v5, 0x1e0

    .line 2746757
    iget v3, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    iget v4, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 p1, 0x1

    .line 2746758
    int-to-float v6, v5

    int-to-float v7, v4

    div-float/2addr v6, v7

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 2746759
    int-to-float v7, v5

    int-to-float v8, v3

    div-float/2addr v7, v8

    invoke-static {v7, v9}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 2746760
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 2746761
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    int-to-float v9, v3

    mul-float/2addr v9, v6

    float-to-int v9, v9

    invoke-static {v9, p1}, Ljava/lang/Math;->max(II)I

    move-result v9

    aput v9, v7, v8

    int-to-float v8, v4

    mul-float/2addr v6, v8

    float-to-int v6, v6

    invoke-static {v6, p1}, Ljava/lang/Math;->max(II)I

    move-result v6

    aput v6, v7, p1

    move-object v3, v7

    .line 2746762
    iget-object v4, v1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v4, v4, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 2746763
    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    new-instance v5, LX/1o9;

    const/4 v6, 0x0

    aget v6, v3, v6

    const/4 v7, 0x1

    aget v3, v3, v7

    invoke-direct {v5, v6, v3}, LX/1o9;-><init>(II)V

    .line 2746764
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2746765
    move-object v3, v4

    .line 2746766
    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v3

    move-object v3, v3

    .line 2746767
    invoke-static {v0, v3}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->a$redex0(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2746768
    new-instance v4, LX/JtH;

    invoke-direct {v4, v0, v2, v1}, LX/JtH;-><init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/messaging/attachments/ImageAttachmentData;)V

    iget-object v5, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2746769
    move-object v0, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2746770
    goto :goto_3

    .line 2746771
    :cond_3
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2746772
    iget-object v4, v0, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;

    invoke-direct {v5, v0, v1, v3}, Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher$3;-><init>(Lcom/facebook/messengerwear/support/MessengerWearMediaFetcher;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    const p1, 0xfd02529

    invoke-static {v4, v5, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_2
.end method
