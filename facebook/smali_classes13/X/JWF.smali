.class public final LX/JWF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JWG;

.field public final synthetic b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;LX/JWG;)V
    .locals 0

    .prologue
    .line 2702606
    iput-object p1, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iput-object p2, p0, LX/JWF;->a:LX/JWG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x7bf991dd

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2702607
    iget-object v1, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iget-object v2, p0, LX/JWF;->a:LX/JWG;

    .line 2702608
    iget-object v3, v2, LX/JWG;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    iget-object v4, v2, LX/JWG;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-static {v3, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-static {v3}, LX/17Q;->f(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2702609
    iget-object v4, v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->c:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2702610
    iget-object v1, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->g:LX/2dj;

    iget-object v2, p0, LX/JWF;->a:LX/JWG;

    iget-object v2, v2, LX/JWG;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2dj;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2702611
    iget-object v1, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->h:LX/2do;

    new-instance v2, LX/2iF;

    iget-object v3, p0, LX/JWF;->a:LX/JWG;

    iget-object v3, v3, LX/JWG;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/2iF;-><init>(J)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2702612
    iget-object v1, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->e:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v2, p0, LX/JWF;->a:LX/JWG;

    iget-object v2, v2, LX/JWG;->a:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    iget-object v3, p0, LX/JWF;->a:LX/JWG;

    iget-object v3, v3, LX/JWG;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-result-object v1

    .line 2702613
    if-eqz v1, :cond_0

    .line 2702614
    iget-object v2, p0, LX/JWF;->b:Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/pymi/rows/FutureFriendingBlacklistPartDefinition;->f:LX/JWD;

    invoke-virtual {v2, v1}, LX/JWD;->c(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    .line 2702615
    :cond_0
    const v1, 0x7e482ddf

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
