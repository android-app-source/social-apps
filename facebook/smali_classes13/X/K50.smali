.class public LX/K50;
.super LX/K4z;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/K4z;",
        "Ljava/lang/Comparable",
        "<",
        "LX/K50;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/K4y;

.field public b:LX/K5B;

.field public c:LX/K5B;

.field public d:LX/K5B;

.field public e:F

.field public f:Z

.field public g:Z

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 2769754
    invoke-direct {p0}, LX/K4z;-><init>()V

    .line 2769755
    new-instance v0, LX/K4y;

    invoke-direct {v0}, LX/K4y;-><init>()V

    iput-object v0, p0, LX/K50;->a:LX/K4y;

    .line 2769756
    new-instance v0, LX/K5B;

    invoke-direct {v0, v2, v2, v1, v1}, LX/K5B;-><init>(FFFF)V

    iput-object v0, p0, LX/K50;->b:LX/K5B;

    .line 2769757
    new-instance v0, LX/K5B;

    invoke-direct {v0, v2, v2, v1, v1}, LX/K5B;-><init>(FFFF)V

    iput-object v0, p0, LX/K50;->c:LX/K5B;

    .line 2769758
    new-instance v0, LX/K5B;

    invoke-direct {v0, v1, v1, v1, v1}, LX/K5B;-><init>(FFFF)V

    iput-object v0, p0, LX/K50;->d:LX/K5B;

    .line 2769759
    iput v1, p0, LX/K50;->e:F

    .line 2769760
    iput-boolean v3, p0, LX/K50;->f:Z

    .line 2769761
    iput-boolean v3, p0, LX/K50;->g:Z

    .line 2769762
    const/4 v0, 0x0

    iput v0, p0, LX/K50;->h:I

    .line 2769763
    return-void
.end method


# virtual methods
.method public final a(LX/K5B;)V
    .locals 1

    .prologue
    .line 2769764
    iget-object v0, p0, LX/K50;->d:LX/K5B;

    .line 2769765
    iget p0, p1, LX/K5B;->a:F

    iput p0, v0, LX/K5B;->a:F

    .line 2769766
    iget p0, p1, LX/K5B;->b:F

    iput p0, v0, LX/K5B;->b:F

    .line 2769767
    iget p0, p1, LX/K5B;->c:F

    iput p0, v0, LX/K5B;->c:F

    .line 2769768
    iget p0, p1, LX/K5B;->d:F

    iput p0, v0, LX/K5B;->d:F

    .line 2769769
    return-void
.end method

.method public final a(Ljava/util/List;LX/K4y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/K54;",
            ">;",
            "LX/K4y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2769753
    return-void
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2769749
    check-cast p1, LX/K50;

    .line 2769750
    iget v0, p0, LX/K50;->h:I

    .line 2769751
    iget v1, p1, LX/K50;->h:I

    move v1, v1

    .line 2769752
    sub-int/2addr v0, v1

    return v0
.end method
