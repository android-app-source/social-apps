.class public final LX/Jdy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

.field public final synthetic b:LX/Jdz;


# direct methods
.method public constructor <init>(LX/Jdz;Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;)V
    .locals 0

    .prologue
    .line 2720235
    iput-object p1, p0, LX/Jdy;->b:LX/Jdz;

    iput-object p2, p0, LX/Jdy;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3480befe

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2720236
    iget-object v0, p0, LX/Jdy;->b:LX/Jdz;

    iget-object v0, v0, LX/Jdz;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, LX/Jdy;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Jdy;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->k()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    const/4 p1, 0x0

    .line 2720237
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2720238
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2720239
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2720240
    const-string v6, "arg_station_id"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720241
    const-string v6, "arg_station_name"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720242
    const-string v6, "ManageSubstationsFragment"

    invoke-static {v0, v6, v5}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    move-object v2, v5

    .line 2720243
    iget-object v0, p0, LX/Jdy;->b:LX/Jdz;

    iget-object v0, v0, LX/Jdz;->b:LX/JeB;

    iget-object v0, v0, LX/JeB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Jdy;->b:LX/Jdz;

    iget-object v3, v3, LX/Jdz;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2720244
    const v0, -0x74d41fce

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move v5, p1

    .line 2720245
    goto :goto_0

    :cond_1
    move v6, p1

    .line 2720246
    goto :goto_1
.end method
