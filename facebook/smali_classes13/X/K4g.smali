.class public LX/K4g;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/K4c;

.field private final c:LX/K3f;

.field public final d:Lcom/facebook/storyline/renderer/TextureLoader;

.field public final e:Landroid/os/Handler;

.field public final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public g:Landroid/opengl/EGLDisplay;

.field public h:Landroid/opengl/EGLContext;

.field public i:Landroid/opengl/EGLSurface;

.field public j:Landroid/opengl/EGLConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2769080
    const-class v0, LX/K4g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;LX/K4n;Landroid/os/Handler;LX/K4u;LX/K3f;)V
    .locals 2
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/K4n;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2769081
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2769082
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    .line 2769083
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    .line 2769084
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    .line 2769085
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/K4g;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2769086
    new-instance p1, Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-static {p4}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v0

    check-cast v0, LX/1HI;

    const-class v1, Landroid/content/Context;

    invoke-interface {p4, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {p1, p0, p2, v0, v1}, Lcom/facebook/storyline/renderer/TextureLoader;-><init>(LX/K4g;LX/K4n;LX/1HI;Landroid/content/Context;)V

    .line 2769087
    move-object v0, p1

    .line 2769088
    iput-object v0, p0, LX/K4g;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    .line 2769089
    new-instance v0, LX/K4c;

    iget-object v1, p0, LX/K4g;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    invoke-direct {v0, v1}, LX/K4c;-><init>(Lcom/facebook/storyline/renderer/TextureLoader;)V

    iput-object v0, p0, LX/K4g;->b:LX/K4c;

    .line 2769090
    iput-object p5, p0, LX/K4g;->c:LX/K3f;

    .line 2769091
    iput-object p3, p0, LX/K4g;->e:Landroid/os/Handler;

    .line 2769092
    return-void
.end method

.method public static a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/opengl/EGLObjectHandle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2769093
    if-nez p0, :cond_0

    .line 2769094
    const/4 v0, 0x0

    .line 2769095
    :goto_0
    return-object v0

    .line 2769096
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 2769097
    invoke-virtual {p0}, Landroid/opengl/EGLObjectHandle;->getNativeHandle()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2769098
    :cond_1
    invoke-virtual {p0}, Landroid/opengl/EGLObjectHandle;->getHandle()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/graphics/SurfaceTexture;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2769099
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    .line 2769100
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "eglGetDisplay, handle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v1}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769101
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_3

    move v0, v5

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Get EGL display failed, handle: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v3}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2769102
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    .line 2769103
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 2769104
    iget-object v1, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0, v2, v0, v5}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2769105
    const-string v0, "eglInitialize"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769106
    :cond_0
    const/16 v0, 0xf

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 2769107
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 2769108
    new-array v6, v5, [I

    .line 2769109
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2769110
    const-string v0, "eglChooseConfig"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769111
    :cond_1
    aget-object v0, v3, v2

    iput-object v0, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    .line 2769112
    iget-object v0, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    .line 2769113
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 2769114
    iget-object v1, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v1, v3, v4, v0, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    .line 2769115
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "eglCreateContext, handle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v1}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769116
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v0, v1, :cond_4

    move v0, v5

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Create EGL context failed, handle: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v3}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2769117
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    .line 2769118
    new-array v0, v5, [I

    const/16 v1, 0x3038

    aput v1, v0, v2

    .line 2769119
    iget-object v1, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    invoke-static {v1, v3, p1, v0, v2}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    .line 2769120
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "eglCreateWindowSurface, handle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v1}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769121
    iget-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v1, :cond_5

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Create EGL surface failed, handle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v1}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2769122
    iget-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    .line 2769123
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    iget-object v2, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    iget-object v3, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2769124
    const-string v0, "eglMakeCurrent"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769125
    :cond_2
    iget-object v0, p0, LX/K4g;->b:LX/K4c;

    .line 2769126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/K4c;->f:Ljava/util/List;

    .line 2769127
    new-instance v1, LX/K57;

    invoke-direct {v1}, LX/K57;-><init>()V

    iput-object v1, v0, LX/K4c;->e:LX/K57;

    .line 2769128
    iget-object v1, v0, LX/K4c;->e:LX/K57;

    const-string v2, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = aTextureCoord;\n}"

    .line 2769129
    iput-object v2, v1, LX/K57;->a:Ljava/lang/String;

    .line 2769130
    iget-object v1, v0, LX/K4c;->e:LX/K57;

    const-string v2, "precision mediump float;\nvarying vec2 vTextureCoord;\nuniform sampler2D sTexture0;\nuniform vec4 uColorTweaks;\nvoid main() {\n  vec4 avg = vec4(0.0, 0.0, 0.0, 0.0);\n  avg = texture2D(sTexture0, vTextureCoord.xy);\n  avg.xyz *= uColorTweaks.xyz;\n  gl_FragColor = avg;\n}"

    .line 2769131
    iput-object v2, v1, LX/K57;->b:Ljava/lang/String;

    .line 2769132
    iget-object v1, v0, LX/K4c;->e:LX/K57;

    invoke-static {v1}, LX/K4x;->a(LX/K57;)V

    .line 2769133
    const p0, 0x88e4

    const v7, 0x8893

    const v6, 0x8892

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2769134
    new-array v1, v5, [I

    .line 2769135
    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 2769136
    aget v2, v1, v4

    iput v2, v0, LX/K4c;->i:I

    .line 2769137
    sget-object v2, LX/K4c;->a:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    .line 2769138
    sget-object v3, LX/K4c;->a:[F

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 2769139
    iget v3, v0, LX/K4c;->i:I

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 2769140
    sget-object v3, LX/K4c;->a:[F

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v6, v3, v2, p0}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 2769141
    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 2769142
    aget v1, v1, v4

    iput v1, v0, LX/K4c;->j:I

    .line 2769143
    sget-object v1, LX/K4c;->b:[S

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    .line 2769144
    sget-object v2, LX/K4c;->b:[S

    invoke-virtual {v1, v2}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 2769145
    iget v2, v0, LX/K4c;->j:I

    invoke-static {v7, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 2769146
    sget-object v2, LX/K4c;->b:[S

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v7, v2, v1, p0}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 2769147
    return-void

    :cond_3
    move v0, v2

    .line 2769148
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 2769149
    goto/16 :goto_1

    :cond_5
    move v5, v2

    .line 2769150
    goto/16 :goto_2

    nop

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x8
        0x3026
        0x0
        0x3038
    .end array-data

    .line 2769151
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method


# virtual methods
.method public final a(LX/K4d;Ljava/lang/Object;)Landroid/os/Message;
    .locals 1

    .prologue
    .line 2769152
    invoke-virtual {p1}, LX/K4d;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, LX/K4g;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2769153
    const/4 v0, 0x0

    .line 2769154
    invoke-virtual {p0}, LX/K4g;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2769155
    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2769156
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/K4d;)Z
    .locals 1

    .prologue
    .line 2769157
    invoke-virtual {p1}, LX/K4d;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, LX/K4g;->sendEmptyMessage(I)Z

    move-result v0

    return v0
.end method

.method public final b(LX/K4d;)V
    .locals 1

    .prologue
    .line 2769158
    invoke-virtual {p1}, LX/K4d;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, LX/K4g;->removeMessages(I)V

    .line 2769159
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 2769160
    sget-object v0, LX/K4f;->a:[I

    invoke-static {}, LX/K4d;->values()[LX/K4d;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/K4d;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2769161
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2769162
    :pswitch_0
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-ne v0, v2, :cond_1

    move v0, v9

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2769163
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    invoke-direct {p0, v0}, LX/K4g;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2769164
    iget-object v0, p0, LX/K4g;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2769165
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2769166
    goto :goto_0

    .line 2769167
    :pswitch_1
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v0, v2, :cond_5

    :goto_2
    invoke-static {v9}, LX/0PB;->checkState(Z)V

    .line 2769168
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 2769169
    iget-object v2, p0, LX/K4g;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/util/Set;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2769170
    iget-object v3, v2, Lcom/facebook/storyline/renderer/TextureLoader;->c:LX/K4g;

    invoke-virtual {v3}, LX/K4g;->a()V

    .line 2769171
    iget-object v3, v2, Lcom/facebook/storyline/renderer/TextureLoader;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2769172
    iput-object v0, v2, Lcom/facebook/storyline/renderer/TextureLoader;->j:Ljava/lang/String;

    .line 2769173
    new-instance v4, Ljava/util/HashSet;

    iget-object v3, v2, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2769174
    invoke-static {v4, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v3

    invoke-virtual {v3}, LX/0Ro;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2769175
    invoke-static {v2, v3}, Lcom/facebook/storyline/renderer/TextureLoader;->c(Lcom/facebook/storyline/renderer/TextureLoader;Ljava/lang/String;)V

    goto :goto_4

    .line 2769176
    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    .line 2769177
    :cond_3
    invoke-static {v1, v4}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v3

    invoke-virtual {v3}, LX/0Ro;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2769178
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2769179
    iget-object v5, v2, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    move v5, v6

    :goto_6
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2769180
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v5

    new-instance v8, LX/1o9;

    iget v1, v2, Lcom/facebook/storyline/renderer/TextureLoader;->i:I

    iget v0, v2, Lcom/facebook/storyline/renderer/TextureLoader;->i:I

    invoke-direct {v8, v1, v0}, LX/1o9;-><init>(II)V

    .line 2769181
    iput-object v8, v5, LX/1bX;->c:LX/1o9;

    .line 2769182
    move-object v5, v5

    .line 2769183
    invoke-virtual {v5, v6}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v5

    invoke-virtual {v5}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2769184
    iget-object v6, v2, Lcom/facebook/storyline/renderer/TextureLoader;->e:LX/1HI;

    sget-object v8, Lcom/facebook/storyline/renderer/TextureLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v5, v8}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v5

    .line 2769185
    new-instance v6, LX/K4s;

    invoke-direct {v6, v2, v3}, LX/K4s;-><init>(Lcom/facebook/storyline/renderer/TextureLoader;Ljava/lang/String;)V

    iget-object v8, v2, Lcom/facebook/storyline/renderer/TextureLoader;->h:Ljava/util/concurrent/Executor;

    invoke-interface {v5, v6, v8}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2769186
    iget-object v6, v2, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    new-instance v8, LX/K4t;

    invoke-direct {v8, v5}, LX/K4t;-><init>(LX/1ca;)V

    invoke-interface {v6, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769187
    iget v5, v2, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/facebook/storyline/renderer/TextureLoader;->k:I

    .line 2769188
    goto :goto_5

    .line 2769189
    :cond_4
    invoke-static {v2}, Lcom/facebook/storyline/renderer/TextureLoader;->c$redex0(Lcom/facebook/storyline/renderer/TextureLoader;)V

    .line 2769190
    goto/16 :goto_1

    :cond_5
    move v9, v1

    .line 2769191
    goto/16 :goto_2

    .line 2769192
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 2769193
    iget-object v2, p0, LX/K4g;->b:LX/K4c;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/0P1;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/0P1;

    .line 2769194
    if-nez v1, :cond_6

    .line 2769195
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2769196
    :cond_6
    iput-object v1, v2, LX/K4c;->g:LX/0P1;

    .line 2769197
    if-nez v0, :cond_7

    .line 2769198
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v0, v1

    .line 2769199
    :cond_7
    iput-object v0, v2, LX/K4c;->h:LX/0P1;

    .line 2769200
    goto/16 :goto_1

    .line 2769201
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/5pG;

    .line 2769202
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 2769203
    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 2769204
    iget-object v3, p0, LX/K4g;->b:LX/K4c;

    invoke-static {v0}, LX/K4Y;->a(LX/5pG;)LX/K51;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, LX/K4c;->a(LX/K51;II)V

    .line 2769205
    iget-object v0, p0, LX/K4g;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2769206
    :cond_8
    :goto_7
    goto/16 :goto_1

    .line 2769207
    :pswitch_4
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v0, v1, :cond_0

    .line 2769208
    iget-object v0, p0, LX/K4g;->d:Lcom/facebook/storyline/renderer/TextureLoader;

    const/4 v3, 0x0

    .line 2769209
    iget-object v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->c:LX/K4g;

    invoke-virtual {v1}, LX/K4g;->a()V

    .line 2769210
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->j:Ljava/lang/String;

    .line 2769211
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, v0, Lcom/facebook/storyline/renderer/TextureLoader;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2769212
    invoke-static {v0, v1}, Lcom/facebook/storyline/renderer/TextureLoader;->c(Lcom/facebook/storyline/renderer/TextureLoader;Ljava/lang/String;)V

    goto :goto_8

    .line 2769213
    :cond_9
    iget-object v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v5, v1, [I

    .line 2769214
    iget-object v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 2769215
    add-int/lit8 v4, v2, 0x1

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v5, v2

    move v2, v4

    .line 2769216
    goto :goto_9

    .line 2769217
    :cond_a
    array-length v1, v5

    invoke-static {v1, v5, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 2769218
    iget-object v1, v0, Lcom/facebook/storyline/renderer/TextureLoader;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2769219
    iget-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    iget-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    iget-object v0, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    invoke-static {v0}, LX/K4g;->a(Landroid/opengl/EGLObjectHandle;)Ljava/lang/String;

    .line 2769220
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 2769221
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 2769222
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 2769223
    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    .line 2769224
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 2769225
    const-string v0, "terminateEgl"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 2769226
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    .line 2769227
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    .line 2769228
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    .line 2769229
    const/4 v0, 0x0

    iput-object v0, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    .line 2769230
    goto/16 :goto_1

    .line 2769231
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 2769232
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/K3g;

    .line 2769233
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, LX/K3c;

    .line 2769234
    new-instance v3, LX/K4e;

    invoke-direct {v3, p0}, LX/K4e;-><init>(LX/K4g;)V

    .line 2769235
    iget-object v0, p0, LX/K4g;->c:LX/K3f;

    iget-object v4, p0, LX/K4g;->b:LX/K4c;

    iget-object v5, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v6, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    iget-object v7, p0, LX/K4g;->h:Landroid/opengl/EGLContext;

    iget-object v8, p0, LX/K4g;->j:Landroid/opengl/EGLConfig;

    invoke-virtual/range {v0 .. v8}, LX/K3f;->a(LX/K3g;LX/K3c;LX/K4e;LX/K4c;Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;Landroid/opengl/EGLConfig;)Z

    move-result v0

    .line 2769236
    iget-object v2, p0, LX/K4g;->e:Landroid/os/Handler;

    iget-object v3, p0, LX/K4g;->e:Landroid/os/Handler;

    if-eqz v0, :cond_b

    :goto_a
    invoke-virtual {v3, v9, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a

    .line 2769237
    :pswitch_6
    invoke-virtual {p0}, LX/K4g;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_1

    :cond_c
    move v5, v7

    .line 2769238
    goto/16 :goto_6

    .line 2769239
    :cond_d
    iget-object v0, p0, LX/K4g;->g:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/K4g;->i:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2769240
    const-string v0, "eglSwapBuffers"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
