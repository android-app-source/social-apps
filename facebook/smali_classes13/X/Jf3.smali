.class public final enum LX/Jf3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jf3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jf3;

.field public static final enum SINGLE_IMAGE:LX/Jf3;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2721436
    new-instance v0, LX/Jf3;

    const-string v1, "SINGLE_IMAGE"

    invoke-direct {v0, v1, v2}, LX/Jf3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jf3;->SINGLE_IMAGE:LX/Jf3;

    .line 2721437
    const/4 v0, 0x1

    new-array v0, v0, [LX/Jf3;

    sget-object v1, LX/Jf3;->SINGLE_IMAGE:LX/Jf3;

    aput-object v1, v0, v2

    sput-object v0, LX/Jf3;->$VALUES:[LX/Jf3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2721435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jf3;
    .locals 1

    .prologue
    .line 2721434
    const-class v0, LX/Jf3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jf3;

    return-object v0
.end method

.method public static values()[LX/Jf3;
    .locals 1

    .prologue
    .line 2721433
    sget-object v0, LX/Jf3;->$VALUES:[LX/Jf3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jf3;

    return-object v0
.end method
