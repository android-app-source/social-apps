.class public final LX/Jsx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:LX/Jsz;

.field private g:LX/Jsy;

.field public h:Lcom/facebook/messengerwear/shared/Message$Attachment;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Jsz;)V
    .locals 1

    .prologue
    .line 2746039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2746040
    sget-object v0, LX/Jsy;->DEFAULT:LX/Jsy;

    iput-object v0, p0, LX/Jsx;->g:LX/Jsy;

    .line 2746041
    iput-object p1, p0, LX/Jsx;->f:LX/Jsz;

    .line 2746042
    return-void
.end method


# virtual methods
.method public final a(J)LX/Jsx;
    .locals 1

    .prologue
    .line 2746037
    iput-wide p1, p0, LX/Jsx;->a:J

    .line 2746038
    return-object p0
.end method

.method public final a(LX/Jsy;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746035
    iput-object p1, p0, LX/Jsx;->g:LX/Jsy;

    .line 2746036
    return-object p0
.end method

.method public final a(Lcom/facebook/messengerwear/shared/Message$Attachment;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746033
    iput-object p1, p0, LX/Jsx;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    .line 2746034
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746031
    iput-object p1, p0, LX/Jsx;->b:Ljava/lang/String;

    .line 2746032
    return-object p0
.end method

.method public final a(Z)LX/Jsx;
    .locals 0

    .prologue
    .line 2746022
    iput-boolean p1, p0, LX/Jsx;->d:Z

    .line 2746023
    return-object p0
.end method

.method public final a()Lcom/facebook/messengerwear/shared/Message;
    .locals 12

    .prologue
    .line 2746030
    new-instance v1, Lcom/facebook/messengerwear/shared/Message;

    iget-wide v2, p0, LX/Jsx;->a:J

    iget-object v4, p0, LX/Jsx;->b:Ljava/lang/String;

    iget-object v5, p0, LX/Jsx;->c:Ljava/lang/String;

    iget-boolean v6, p0, LX/Jsx;->d:Z

    iget-object v7, p0, LX/Jsx;->e:Ljava/lang/String;

    iget-object v8, p0, LX/Jsx;->f:LX/Jsz;

    iget-object v9, p0, LX/Jsx;->g:LX/Jsy;

    iget-object v10, p0, LX/Jsx;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    iget-object v11, p0, LX/Jsx;->i:Ljava/lang/String;

    invoke-direct/range {v1 .. v11}, Lcom/facebook/messengerwear/shared/Message;-><init>(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;LX/Jsz;LX/Jsy;Lcom/facebook/messengerwear/shared/Message$Attachment;Ljava/lang/String;)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746028
    iput-object p1, p0, LX/Jsx;->c:Ljava/lang/String;

    .line 2746029
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746026
    iput-object p1, p0, LX/Jsx;->e:Ljava/lang/String;

    .line 2746027
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/Jsx;
    .locals 0

    .prologue
    .line 2746024
    iput-object p1, p0, LX/Jsx;->i:Ljava/lang/String;

    .line 2746025
    return-object p0
.end method
