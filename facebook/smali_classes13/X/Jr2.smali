.class public LX/Jr2;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDI;

.field private final b:LX/FDq;

.field private final c:LX/FDs;

.field private final d:LX/Jqb;

.field private final e:LX/Jrc;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741403
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jr2;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDI;LX/FDq;LX/FDs;LX/Jqb;LX/Jrc;)V
    .locals 1
    .param p1    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2741404
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2741405
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741406
    iput-object v0, p0, LX/Jr2;->f:LX/0Ot;

    .line 2741407
    iput-object p1, p0, LX/Jr2;->a:LX/FDI;

    .line 2741408
    iput-object p2, p0, LX/Jr2;->b:LX/FDq;

    .line 2741409
    iput-object p3, p0, LX/Jr2;->c:LX/FDs;

    .line 2741410
    iput-object p4, p0, LX/Jr2;->d:LX/Jqb;

    .line 2741411
    iput-object p5, p0, LX/Jr2;->e:LX/Jrc;

    .line 2741412
    return-void
.end method

.method public static a(LX/0QB;)LX/Jr2;
    .locals 13

    .prologue
    .line 2741413
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741414
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741415
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741416
    if-nez v1, :cond_0

    .line 2741417
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741418
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741419
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741420
    sget-object v1, LX/Jr2;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741421
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741422
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741423
    :cond_1
    if-nez v1, :cond_4

    .line 2741424
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741425
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741426
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741427
    new-instance v7, LX/Jr2;

    invoke-static {v0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v8

    check-cast v8, LX/FDI;

    invoke-static {v0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v9

    check-cast v9, LX/FDq;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v10

    check-cast v10, LX/FDs;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v11

    check-cast v11, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v12

    check-cast v12, LX/Jrc;

    invoke-direct/range {v7 .. v12}, LX/Jr2;-><init>(LX/FDI;LX/FDq;LX/FDs;LX/Jqb;LX/Jrc;)V

    .line 2741428
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2741429
    iput-object v8, v7, LX/Jr2;->f:LX/0Ot;

    .line 2741430
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741431
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741432
    if-nez v1, :cond_2

    .line 2741433
    sget-object v0, LX/Jr2;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr2;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741434
    :goto_1
    if-eqz v0, :cond_3

    .line 2741435
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741436
    :goto_3
    check-cast v0, LX/Jr2;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741437
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741438
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741439
    :catchall_1
    move-exception v0

    .line 2741440
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741441
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741442
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741443
    :cond_2
    :try_start_8
    sget-object v0, LX/Jr2;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jr2;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741444
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741445
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2741446
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->f()LX/6jw;

    move-result-object v3

    .line 2741447
    iget-object v0, p0, LX/Jr2;->e:LX/Jrc;

    iget-object v1, v3, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741448
    iget-object v6, p0, LX/Jr2;->c:LX/FDs;

    new-instance v7, LX/6ie;

    invoke-direct {v7}, LX/6ie;-><init>()V

    sget-object v8, LX/6iW;->READ:LX/6iW;

    .line 2741449
    iput-object v8, v7, LX/6ie;->a:LX/6iW;

    .line 2741450
    move-object v7, v7

    .line 2741451
    new-instance v8, LX/6ia;

    invoke-direct {v8}, LX/6ia;-><init>()V

    .line 2741452
    iput-object v0, v8, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741453
    move-object v0, v8

    .line 2741454
    iput-boolean v2, v0, LX/6ia;->b:Z

    .line 2741455
    move-object v0, v0

    .line 2741456
    iget-wide v8, p2, LX/7GJ;->b:J

    .line 2741457
    iput-wide v8, v0, LX/6ia;->d:J

    .line 2741458
    move-object v0, v0

    .line 2741459
    invoke-virtual {v0}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    move-result-object v0

    invoke-virtual {v0}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    .line 2741460
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2741461
    :cond_0
    iget-object v0, v3, LX/6jw;->folders:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2741462
    iget-object v0, v3, LX/6jw;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2741463
    invoke-static {v0}, LX/Jrc;->a(I)LX/6ek;

    move-result-object v0

    .line 2741464
    iget-object v3, p0, LX/Jr2;->b:LX/FDq;

    const-wide/16 v4, -0x1

    const/4 v6, -0x1

    invoke-virtual {v3, v0, v4, v5, v6}, LX/FDq;->a(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 2741465
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741466
    iget-object v4, p0, LX/Jr2;->c:LX/FDs;

    new-instance v5, LX/6ie;

    invoke-direct {v5}, LX/6ie;-><init>()V

    sget-object v6, LX/6iW;->READ:LX/6iW;

    .line 2741467
    iput-object v6, v5, LX/6ie;->a:LX/6iW;

    .line 2741468
    move-object v5, v5

    .line 2741469
    new-instance v6, LX/6ia;

    invoke-direct {v6}, LX/6ia;-><init>()V

    .line 2741470
    iput-object v0, v6, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741471
    move-object v0, v6

    .line 2741472
    iput-boolean v2, v0, LX/6ia;->b:Z

    .line 2741473
    move-object v0, v0

    .line 2741474
    iget-wide v6, p2, LX/7GJ;->b:J

    .line 2741475
    iput-wide v6, v0, LX/6ia;->d:J

    .line 2741476
    move-object v0, v0

    .line 2741477
    invoke-virtual {v0}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    move-result-object v0

    invoke-virtual {v0}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    goto :goto_1

    .line 2741478
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2741479
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->f()LX/6jw;

    move-result-object v4

    .line 2741480
    iget-object v0, p0, LX/Jr2;->e:LX/Jrc;

    iget-object v1, v4, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741481
    iget-object v1, p0, LX/Jr2;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741482
    iget-object v1, p0, LX/Jr2;->d:LX/Jqb;

    invoke-virtual {v1, v0}, LX/Jqb;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741483
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2741484
    :cond_0
    iget-object v0, v4, LX/6jw;->folders:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2741485
    iget-object v0, v4, LX/6jw;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2741486
    invoke-static {v0}, LX/Jrc;->a(I)LX/6ek;

    move-result-object v0

    .line 2741487
    iget-object v1, p0, LX/Jr2;->a:LX/FDI;

    invoke-virtual {v1, v0}, LX/FDI;->a(LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2741488
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2741489
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v5, v1

    .line 2741490
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741491
    iget-object v1, p0, LX/Jr2;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v7}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741492
    iget-object v1, p0, LX/Jr2;->d:LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v0}, LX/Jqb;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2741493
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2741494
    :cond_2
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741495
    check-cast p1, LX/6kW;

    .line 2741496
    invoke-virtual {p1}, LX/6kW;->f()LX/6jw;

    move-result-object v0

    .line 2741497
    iget-object v1, p0, LX/Jr2;->e:LX/Jrc;

    iget-object v0, v0, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Jrc;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    .line 2741498
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
