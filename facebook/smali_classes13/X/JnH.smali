.class public LX/JnH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/messaging/send/trigger/NavigationTrigger;


# instance fields
.field public c:LX/Jkm;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public final e:LX/0gc;

.field public final f:Landroid/view/View;

.field public final g:LX/JnE;

.field public final h:LX/3Ec;

.field public final i:LX/3Rl;

.field public final j:LX/FFh;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/It1;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2733908
    const-class v0, LX/JnH;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/JnH;->a:Ljava/lang/String;

    .line 2733909
    const-string v0, "lightweight_action_dialog_selector"

    invoke-static {v0}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a(Ljava/lang/String;)Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    move-result-object v0

    sput-object v0, LX/JnH;->b:Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    return-void
.end method

.method public constructor <init>(LX/JnE;LX/3Ec;LX/3Rl;LX/FFh;LX/0Ot;LX/0Ot;LX/0gc;Landroid/view/View;LX/Jkm;)V
    .locals 1
    .param p7    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/Jkm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JnE;",
            "LX/3Ec;",
            "LX/3Rl;",
            "LX/FFh;",
            "LX/0Ot",
            "<",
            "LX/It1;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            ">;",
            "LX/0gc;",
            "Landroid/view/View;",
            "Lcom/facebook/messaging/lightweightactions/ui/LightweightActionItemDialogSelector$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2733910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2733911
    iput-object p1, p0, LX/JnH;->g:LX/JnE;

    .line 2733912
    iput-object p2, p0, LX/JnH;->h:LX/3Ec;

    .line 2733913
    iput-object p3, p0, LX/JnH;->i:LX/3Rl;

    .line 2733914
    iput-object p5, p0, LX/JnH;->k:LX/0Ot;

    .line 2733915
    iput-object p6, p0, LX/JnH;->l:LX/0Ot;

    .line 2733916
    iput-object p7, p0, LX/JnH;->e:LX/0gc;

    .line 2733917
    iput-object p8, p0, LX/JnH;->f:Landroid/view/View;

    .line 2733918
    iput-object p4, p0, LX/JnH;->j:LX/FFh;

    .line 2733919
    iget-object v0, p0, LX/JnH;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/JnH;->d:Landroid/content/Context;

    .line 2733920
    iput-object p9, p0, LX/JnH;->c:LX/Jkm;

    .line 2733921
    return-void
.end method

.method public static a$redex0(LX/JnH;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FFq;LX/9VZ;)V
    .locals 4

    .prologue
    .line 2733922
    sget-object v0, LX/FFq;->OTHERS:LX/FFq;

    if-ne p2, v0, :cond_0

    .line 2733923
    :goto_0
    return-void

    .line 2733924
    :cond_0
    iget-object v0, p0, LX/JnH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It1;

    invoke-virtual {v0, p1, p2}, LX/It1;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FFq;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2733925
    iget-object v0, p0, LX/JnH;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageManager;

    const-string v2, "lightweight_action_dialog_selector"

    sget-object v3, LX/JnH;->b:Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;LX/9VZ;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2733926
    iget-object v0, p0, LX/JnH;->j:LX/FFh;

    const-string v1, "lightweight_action_dialog_selector"

    const/4 v2, 0x0

    .line 2733927
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "lwa_sent"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2733928
    iput-object v1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2733929
    move-object v3, v3

    .line 2733930
    const-string p0, "lwa_type"

    invoke-virtual {p2}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "is_reciprocation"

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "trigger"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2733931
    iget-object p0, v0, LX/FFh;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2733932
    goto :goto_0
.end method
