.class public final LX/K98;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

.field public final synthetic b:Z

.field public final synthetic c:LX/K7K;

.field public final synthetic d:LX/K9E;


# direct methods
.method public constructor <init>(LX/K9E;Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;ZLX/K7K;)V
    .locals 0

    .prologue
    .line 2776735
    iput-object p1, p0, LX/K98;->d:LX/K9E;

    iput-object p2, p0, LX/K98;->a:Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    iput-boolean p3, p0, LX/K98;->b:Z

    iput-object p4, p0, LX/K98;->c:LX/K7K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 2776736
    iget-object v0, p0, LX/K98;->a:Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setSwitchStateChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2776737
    iget-object v1, p0, LX/K98;->a:Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    iget-boolean v0, p0, LX/K98;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setSwitchEnabled(Z)V

    .line 2776738
    iget-object v0, p0, LX/K98;->a:Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;

    new-instance v1, LX/K97;

    invoke-direct {v1, p0}, LX/K97;-><init>(LX/K98;)V

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/view/SubscriptionsManagerPublisherRowView;->setSwitchStateChangedListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2776739
    return-void

    .line 2776740
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
