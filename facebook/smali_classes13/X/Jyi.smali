.class public final LX/Jyi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/insight/view/ProfileInsightFragment;)V
    .locals 0

    .prologue
    .line 2755348
    iput-object p1, p0, LX/Jyi;->a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 3

    .prologue
    .line 2755349
    iget-object v0, p0, LX/Jyi;->a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    iget-object v0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->b:LX/0Zb;

    const-string v1, "profile_insight_click_action"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2755350
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2755351
    const-string v0, "session_id"

    iget-object v2, p0, LX/Jyi;->a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    iget-object v2, v2, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2755352
    const-string v2, "profile_id"

    iget-object v0, p0, LX/Jyi;->a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    iget-object v0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2755353
    iget-object v0, p0, LX/Jyi;->a:Lcom/facebook/profile/insight/view/ProfileInsightFragment;

    iget-object v0, v0, Lcom/facebook/profile/insight/view/ProfileInsightFragment;->a:LX/Jyp;

    invoke-virtual {v0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2755354
    const-string v2, "click_on_tab"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2755355
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2755356
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2755357
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2755358
    return-void
.end method
