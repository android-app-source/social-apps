.class public LX/Jej;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/63a;


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2720883
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2720884
    invoke-direct {p0}, LX/Jej;->a()V

    .line 2720885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2720886
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2720887
    invoke-direct {p0}, LX/Jej;->a()V

    .line 2720888
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2720889
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2720890
    invoke-direct {p0}, LX/Jej;->a()V

    .line 2720891
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2720892
    const v0, 0x7f030a6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2720893
    const v0, 0x7f0d1a89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Jej;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2720894
    return-void
.end method


# virtual methods
.method public setButtonTintColor(I)V
    .locals 1

    .prologue
    .line 2720895
    iget-object v0, p0, LX/Jej;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2720896
    return-void
.end method
