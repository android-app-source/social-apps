.class public LX/Jfs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/support/v7/widget/RecyclerView;

.field public final c:Landroid/widget/ProgressBar;

.field public final d:Landroid/support/v7/widget/SearchView;

.field public final e:Landroid/view/MenuItem;

.field private final f:LX/0gc;

.field private final g:Ljava/lang/String;

.field public final h:LX/Jfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jfy",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final i:LX/Jg3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/messaging/business/subscription/manage/common/loader/SubscriptionManageSearchLoader",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final j:LX/Jfk;

.field public final k:LX/Jfu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jfu",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final l:Landroid/view/inputmethod/InputMethodManager;

.field private final m:LX/Jfp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jfp",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final n:LX/Jfp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Jfp",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Jfl;Landroid/view/inputmethod/InputMethodManager;LX/Jfu;LX/Jfy;LX/Jg3;LX/JgM;Landroid/support/v7/widget/RecyclerView;Landroid/widget/ProgressBar;Landroid/view/MenuItem;Ljava/lang/String;LX/0gc;)V
    .locals 2
    .param p4    # LX/Jfu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Jfy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Jg3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/JgM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/widget/ProgressBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Landroid/view/MenuItem;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/Jfl;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/Jfu",
            "<TT;>;",
            "LX/Jfy",
            "<TT;>;",
            "Lcom/facebook/messaging/business/subscription/manage/common/loader/SubscriptionManageSearchLoader",
            "<TT;>;",
            "Lcom/facebook/messaging/business/subscription/manage/common/views/SubscriptionManageAdapterViewFactory;",
            "Landroid/support/v7/widget/RecyclerView;",
            "Landroid/widget/ProgressBar;",
            "Landroid/view/MenuItem;",
            "Ljava/lang/String;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2722302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2722303
    new-instance v0, LX/Jfq;

    invoke-direct {v0, p0}, LX/Jfq;-><init>(LX/Jfs;)V

    iput-object v0, p0, LX/Jfs;->m:LX/Jfp;

    .line 2722304
    new-instance v0, LX/Jfr;

    invoke-direct {v0, p0}, LX/Jfr;-><init>(LX/Jfs;)V

    iput-object v0, p0, LX/Jfs;->n:LX/Jfp;

    .line 2722305
    iput-object p1, p0, LX/Jfs;->a:Landroid/content/Context;

    .line 2722306
    iput-object p8, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    .line 2722307
    iput-object p9, p0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    .line 2722308
    if-eqz p10, :cond_0

    .line 2722309
    invoke-interface {p10}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, LX/Jfs;->d:Landroid/support/v7/widget/SearchView;

    .line 2722310
    iput-object p10, p0, LX/Jfs;->e:Landroid/view/MenuItem;

    .line 2722311
    :goto_0
    iput-object p11, p0, LX/Jfs;->g:Ljava/lang/String;

    .line 2722312
    iput-object p4, p0, LX/Jfs;->k:LX/Jfu;

    .line 2722313
    iput-object p5, p0, LX/Jfs;->h:LX/Jfy;

    .line 2722314
    iput-object p6, p0, LX/Jfs;->i:LX/Jg3;

    .line 2722315
    iput-object p12, p0, LX/Jfs;->f:LX/0gc;

    .line 2722316
    iget-object v0, p0, LX/Jfs;->f:LX/0gc;

    .line 2722317
    new-instance v1, LX/Jfk;

    invoke-direct {v1, p7, v0}, LX/Jfk;-><init>(LX/JgM;LX/0gc;)V

    .line 2722318
    move-object v0, v1

    .line 2722319
    iput-object v0, p0, LX/Jfs;->j:LX/Jfk;

    .line 2722320
    iput-object p3, p0, LX/Jfs;->l:Landroid/view/inputmethod/InputMethodManager;

    .line 2722321
    iget-object v0, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-direct {v1, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2722322
    iget-object v0, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/Jfs;->j:LX/Jfk;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2722323
    iget-object v0, p0, LX/Jfs;->d:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_1

    .line 2722324
    :goto_1
    invoke-static {p0}, LX/Jfs;->f$redex0(LX/Jfs;)V

    .line 2722325
    invoke-static {p0}, LX/Jfs;->b(LX/Jfs;)V

    .line 2722326
    return-void

    .line 2722327
    :cond_0
    iput-object v1, p0, LX/Jfs;->d:Landroid/support/v7/widget/SearchView;

    .line 2722328
    iput-object v1, p0, LX/Jfs;->e:Landroid/view/MenuItem;

    goto :goto_0

    .line 2722329
    :cond_1
    iget-object v0, p0, LX/Jfs;->d:Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, LX/Jfs;->a:Landroid/content/Context;

    const p1, 0x7f082b12

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2722330
    iget-object v0, p0, LX/Jfs;->d:Landroid/support/v7/widget/SearchView;

    new-instance v1, LX/Jfm;

    invoke-direct {v1, p0}, LX/Jfm;-><init>(LX/Jfs;)V

    .line 2722331
    iput-object v1, v0, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2722332
    iget-object v0, p0, LX/Jfs;->e:Landroid/view/MenuItem;

    new-instance v1, LX/Jfn;

    invoke-direct {v1, p0}, LX/Jfn;-><init>(LX/Jfs;)V

    invoke-static {v0, v1}, LX/3rl;->a(Landroid/view/MenuItem;LX/3rk;)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public static a$redex0(LX/Jfs;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x4

    .line 2722333
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2722334
    iget-object v0, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2722335
    :cond_0
    :goto_0
    return-void

    .line 2722336
    :cond_1
    iget-object v0, p0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722337
    iget-object v0, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2722338
    iget-object v0, p0, LX/Jfs;->i:LX/Jg3;

    if-eqz v0, :cond_0

    .line 2722339
    iget-object v0, p0, LX/Jfs;->i:LX/Jg3;

    iget-object v1, p0, LX/Jfs;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Jfs;->n:LX/Jfp;

    .line 2722340
    if-nez v1, :cond_2

    .line 2722341
    :goto_1
    goto :goto_0

    .line 2722342
    :cond_2
    new-instance v3, LX/CL3;

    invoke-direct {v3}, LX/CL3;-><init>()V

    move-object v3, v3

    .line 2722343
    const-string v4, "station_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "search_str"

    invoke-virtual {v4, v5, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2722344
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x78

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2722345
    iget-object v4, v0, LX/Jg3;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2722346
    iget-object v4, v0, LX/Jg3;->b:LX/1Ck;

    const-string v5, "load_search"

    .line 2722347
    new-instance v6, LX/Jg2;

    invoke-direct {v6, v0, v2}, LX/Jg2;-><init>(LX/Jg3;LX/Jfp;)V

    move-object v6, v6

    .line 2722348
    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1
.end method

.method public static b(LX/Jfs;)V
    .locals 3

    .prologue
    .line 2722349
    invoke-direct {p0}, LX/Jfs;->c()V

    .line 2722350
    iget-object v0, p0, LX/Jfs;->h:LX/Jfy;

    iget-object v1, p0, LX/Jfs;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Jfs;->m:LX/Jfp;

    invoke-interface {v0, v1, v2}, LX/Jfy;->a(Ljava/lang/String;LX/Jfp;)V

    .line 2722351
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2722352
    iget-object v0, p0, LX/Jfs;->j:LX/Jfk;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_0

    .line 2722353
    iget-object v0, p0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722354
    :goto_0
    return-void

    .line 2722355
    :cond_0
    iget-object v0, p0, LX/Jfs;->j:LX/Jfk;

    .line 2722356
    iget-object v1, v0, LX/Jfk;->c:Ljava/util/List;

    new-instance p0, LX/Jg5;

    invoke-direct {p0}, LX/Jg5;-><init>()V

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2722357
    iget-object v1, v0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2722358
    invoke-virtual {v0, v1}, LX/1OM;->j_(I)V

    .line 2722359
    goto :goto_0
.end method

.method public static d$redex0(LX/Jfs;)V
    .locals 3

    .prologue
    .line 2722360
    iget-object v0, p0, LX/Jfs;->j:LX/Jfk;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_0

    .line 2722361
    iget-object v0, p0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722362
    :goto_0
    return-void

    .line 2722363
    :cond_0
    iget-object v0, p0, LX/Jfs;->j:LX/Jfk;

    .line 2722364
    iget-object v1, v0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2722365
    if-ltz v1, :cond_1

    iget-object v2, v0, LX/Jfk;->c:Ljava/util/List;

    iget-object p0, v0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/Jg5;

    if-eqz v2, :cond_1

    .line 2722366
    iget-object v2, v0, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2722367
    invoke-virtual {v0, v1}, LX/1OM;->d(I)V

    .line 2722368
    :cond_1
    goto :goto_0
.end method

.method public static f$redex0(LX/Jfs;)V
    .locals 2

    .prologue
    .line 2722369
    iget-object v0, p0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/Jfo;

    invoke-direct {v1, p0}, LX/Jfo;-><init>(LX/Jfs;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2722370
    return-void
.end method
