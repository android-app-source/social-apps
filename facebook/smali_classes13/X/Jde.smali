.class public final LX/Jde;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/Jdk;

.field public final synthetic b:Lcom/facebook/widget/BetterSwitch;

.field public final synthetic c:Landroid/widget/ProgressBar;

.field public final synthetic d:LX/Jdl;


# direct methods
.method public constructor <init>(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 2719823
    iput-object p1, p0, LX/Jde;->d:LX/Jdl;

    iput-object p2, p0, LX/Jde;->a:LX/Jdk;

    iput-object p3, p0, LX/Jde;->b:Lcom/facebook/widget/BetterSwitch;

    iput-object p4, p0, LX/Jde;->c:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 8

    .prologue
    .line 2719824
    if-eqz p2, :cond_1

    .line 2719825
    iget-object v0, p0, LX/Jde;->d:LX/Jdl;

    iget-object v1, p0, LX/Jde;->a:LX/Jdk;

    iget-object v2, p0, LX/Jde;->b:Lcom/facebook/widget/BetterSwitch;

    iget-object v3, p0, LX/Jde;->c:Landroid/widget/ProgressBar;

    .line 2719826
    sget-object v4, LX/Jdk;->MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_3

    .line 2719827
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719828
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719829
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->e(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/2h0;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Ljava/lang/String;LX/2h0;)V

    .line 2719830
    iget-object v4, v0, LX/Jdl;->k:LX/JdU;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719831
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719832
    invoke-virtual {v4, v5}, LX/JdU;->b(Ljava/lang/String;)V

    .line 2719833
    invoke-static {v0, v2}, LX/Jdl;->a$redex0(LX/Jdl;Lcom/facebook/widget/BetterSwitch;)V

    .line 2719834
    :cond_0
    :goto_0
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->g(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    .line 2719835
    :goto_1
    return-void

    .line 2719836
    :cond_1
    iget-object v0, p0, LX/Jde;->d:LX/Jdl;

    iget-object v1, p0, LX/Jde;->a:LX/Jdk;

    iget-object v2, p0, LX/Jde;->b:Lcom/facebook/widget/BetterSwitch;

    iget-object v3, p0, LX/Jde;->c:Landroid/widget/ProgressBar;

    .line 2719837
    sget-object v4, LX/Jdk;->MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_5

    .line 2719838
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719839
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719840
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->e(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/2h0;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/messaging/blocking/BlockingUtils;->b(Ljava/lang/String;LX/2h0;)V

    .line 2719841
    iget-object v4, v0, LX/Jdl;->k:LX/JdU;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719842
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719843
    invoke-virtual {v4, v5}, LX/JdU;->c(Ljava/lang/String;)V

    .line 2719844
    invoke-static {v0, v2}, LX/Jdl;->a$redex0(LX/Jdl;Lcom/facebook/widget/BetterSwitch;)V

    .line 2719845
    :cond_2
    :goto_2
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->g(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)V

    .line 2719846
    goto :goto_1

    .line 2719847
    :cond_3
    sget-object v4, LX/Jdk;->PROMOTION_MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_4

    .line 2719848
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719849
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719850
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->d(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/Jdf;

    move-result-object v6

    .line 2719851
    new-instance p0, LX/4H5;

    invoke-direct {p0}, LX/4H5;-><init>()V

    .line 2719852
    invoke-virtual {p0, v5}, LX/4H5;->b(Ljava/lang/String;)LX/4H5;

    .line 2719853
    const-string p1, "block_promotion"

    invoke-virtual {p0, p1}, LX/4H5;->a(Ljava/lang/String;)LX/4H5;

    .line 2719854
    invoke-static {v4, p0, v6}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/messaging/blocking/BlockingUtils;LX/4H5;LX/Jdf;)V

    .line 2719855
    iget-object v4, v0, LX/Jdl;->l:LX/JfM;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719856
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719857
    iget-object v6, v4, LX/JfM;->a:LX/0Zb;

    const-string p0, "promotion_message_block_on"

    invoke-static {p0}, LX/JfM;->c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "page_id"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v6, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2719858
    goto :goto_0

    .line 2719859
    :cond_4
    sget-object v4, LX/Jdk;->SUBSCRIPTION_MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_0

    .line 2719860
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719861
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719862
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->d(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/Jdf;

    move-result-object v6

    .line 2719863
    iget-object p0, v4, Lcom/facebook/messaging/blocking/BlockingUtils;->h:LX/CKT;

    const-string p1, "messenger_user_control"

    new-instance p2, LX/JdV;

    invoke-direct {p2, v4, v6}, LX/JdV;-><init>(Lcom/facebook/messaging/blocking/BlockingUtils;LX/Jdf;)V

    invoke-virtual {p0, p1, v5, p2}, LX/CKT;->a(Ljava/lang/String;Ljava/lang/String;LX/CKR;)V

    .line 2719864
    goto/16 :goto_0

    .line 2719865
    :cond_5
    sget-object v4, LX/Jdk;->PROMOTION_MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_6

    .line 2719866
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719867
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719868
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->d(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/Jdf;

    move-result-object v6

    .line 2719869
    new-instance v7, LX/4H5;

    invoke-direct {v7}, LX/4H5;-><init>()V

    .line 2719870
    invoke-virtual {v7, v5}, LX/4H5;->b(Ljava/lang/String;)LX/4H5;

    .line 2719871
    const-string p0, "unblock_promotion"

    invoke-virtual {v7, p0}, LX/4H5;->a(Ljava/lang/String;)LX/4H5;

    .line 2719872
    invoke-static {v4, v7, v6}, Lcom/facebook/messaging/blocking/BlockingUtils;->a(Lcom/facebook/messaging/blocking/BlockingUtils;LX/4H5;LX/Jdf;)V

    .line 2719873
    iget-object v4, v0, LX/Jdl;->l:LX/JfM;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719874
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719875
    iget-object v6, v4, LX/JfM;->a:LX/0Zb;

    const-string v7, "promotion_message_block_off"

    invoke-static {v7}, LX/JfM;->c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_id"

    invoke-virtual {v7, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2719876
    goto/16 :goto_2

    .line 2719877
    :cond_6
    sget-object v4, LX/Jdk;->SUBSCRIPTION_MESSAGES:LX/Jdk;

    if-ne v1, v4, :cond_2

    .line 2719878
    iget-object v4, v0, LX/Jdl;->j:Lcom/facebook/messaging/blocking/BlockingUtils;

    iget-object v5, v0, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719879
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2719880
    invoke-static {v0, v1, v2, v3}, LX/Jdl;->d(LX/Jdl;LX/Jdk;Lcom/facebook/widget/BetterSwitch;Landroid/widget/ProgressBar;)LX/Jdf;

    move-result-object v6

    .line 2719881
    iget-object v7, v4, Lcom/facebook/messaging/blocking/BlockingUtils;->h:LX/CKT;

    new-instance p0, LX/JdW;

    invoke-direct {p0, v4, v6}, LX/JdW;-><init>(Lcom/facebook/messaging/blocking/BlockingUtils;LX/Jdf;)V

    .line 2719882
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2719883
    iget-object p1, v7, LX/CKT;->a:LX/03V;

    const-string p2, "BusinessSubscriptionMutationHelper"

    const-string v4, "Unsubscribe page id is null"

    invoke-virtual {p1, p2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719884
    :goto_3
    goto/16 :goto_2

    .line 2719885
    :cond_7
    new-instance p1, LX/4H7;

    invoke-direct {p1}, LX/4H7;-><init>()V

    .line 2719886
    const-string p2, "page_id"

    invoke-virtual {p1, p2, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719887
    new-instance p2, LX/CK9;

    invoke-direct {p2}, LX/CK9;-><init>()V

    move-object p2, p2

    .line 2719888
    const-string v4, "input"

    invoke-virtual {p2, v4, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2719889
    iget-object p1, v7, LX/CKT;->b:LX/0tX;

    invoke-static {p2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    iput-object p1, v7, LX/CKT;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2719890
    iget-object p1, v7, LX/CKT;->d:LX/1Ck;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v4, "subscribe_to_page"

    invoke-direct {p2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iget-object v4, v7, LX/CKT;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v6, LX/CKO;

    invoke-direct {v6, v7, p0}, LX/CKO;-><init>(LX/CKT;LX/CKS;)V

    invoke-virtual {p1, p2, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_3
.end method
