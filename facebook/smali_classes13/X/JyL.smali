.class public final LX/JyL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2754629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754630
    const-string v0, ""

    iput-object v0, p0, LX/JyL;->c:Ljava/lang/String;

    .line 2754631
    return-void
.end method

.method public constructor <init>(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V
    .locals 1

    .prologue
    .line 2754632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2754633
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2754634
    instance-of v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    if-eqz v0, :cond_0

    .line 2754635
    check-cast p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2754636
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    iput-object v0, p0, LX/JyL;->a:Ljava/lang/String;

    .line 2754637
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    iput-object v0, p0, LX/JyL;->b:Ljava/lang/String;

    .line 2754638
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    iput-object v0, p0, LX/JyL;->c:Ljava/lang/String;

    .line 2754639
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    iput-object v0, p0, LX/JyL;->d:Ljava/lang/String;

    .line 2754640
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    iput-object v0, p0, LX/JyL;->e:Ljava/lang/String;

    .line 2754641
    :goto_0
    return-void

    .line 2754642
    :cond_0
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2754643
    iput-object v0, p0, LX/JyL;->a:Ljava/lang/String;

    .line 2754644
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2754645
    iput-object v0, p0, LX/JyL;->b:Ljava/lang/String;

    .line 2754646
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2754647
    iput-object v0, p0, LX/JyL;->c:Ljava/lang/String;

    .line 2754648
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2754649
    iput-object v0, p0, LX/JyL;->d:Ljava/lang/String;

    .line 2754650
    iget-object v0, p1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2754651
    iput-object v0, p0, LX/JyL;->e:Ljava/lang/String;

    goto :goto_0
.end method
