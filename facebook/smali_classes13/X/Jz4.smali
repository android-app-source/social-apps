.class public LX/Jz4;
.super LX/Jz0;
.source ""


# instance fields
.field private e:J

.field private final f:[D

.field private final g:D

.field private h:D


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 6

    .prologue
    .line 2755707
    invoke-direct {p0}, LX/Jz0;-><init>()V

    .line 2755708
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Jz4;->e:J

    .line 2755709
    const-string v0, "frames"

    invoke-interface {p1, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v1

    .line 2755710
    invoke-interface {v1}, LX/5pC;->size()I

    move-result v2

    .line 2755711
    new-array v0, v2, [D

    iput-object v0, p0, LX/Jz4;->f:[D

    .line 2755712
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2755713
    iget-object v3, p0, LX/Jz4;->f:[D

    invoke-interface {v1, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 2755714
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2755715
    :cond_0
    const-string v0, "toValue"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jz4;->g:D

    .line 2755716
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    .line 2755717
    iget-wide v0, p0, LX/Jz4;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2755718
    iput-wide p1, p0, LX/Jz4;->e:J

    .line 2755719
    iget-object v0, p0, LX/Jz0;->b:LX/Jyx;

    iget-wide v0, v0, LX/Jyx;->e:D

    iput-wide v0, p0, LX/Jz4;->h:D

    .line 2755720
    :cond_0
    iget-wide v0, p0, LX/Jz4;->e:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 2755721
    long-to-double v0, v0

    const-wide v2, 0x4030aaaaaaaaaaabL    # 16.666666666666668

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 2755722
    if-gez v0, :cond_1

    .line 2755723
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Calculated frame index should never be lower than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2755724
    :cond_1
    iget-boolean v1, p0, LX/Jz0;->a:Z

    if-eqz v1, :cond_2

    .line 2755725
    :goto_0
    return-void

    .line 2755726
    :cond_2
    iget-object v1, p0, LX/Jz4;->f:[D

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_3

    .line 2755727
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Jz4;->a:Z

    .line 2755728
    iget-wide v0, p0, LX/Jz4;->g:D

    .line 2755729
    :goto_1
    iget-object v2, p0, LX/Jz0;->b:LX/Jyx;

    iput-wide v0, v2, LX/Jyx;->e:D

    goto :goto_0

    .line 2755730
    :cond_3
    iget-wide v2, p0, LX/Jz4;->h:D

    iget-object v1, p0, LX/Jz4;->f:[D

    aget-wide v0, v1, v0

    iget-wide v4, p0, LX/Jz4;->g:D

    iget-wide v6, p0, LX/Jz4;->h:D

    sub-double/2addr v4, v6

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    goto :goto_1
.end method
