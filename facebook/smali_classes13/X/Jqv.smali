.class public LX/Jqv;
.super LX/Jqi;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqi",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field private final a:LX/Jrc;

.field private final b:LX/FDI;

.field private final c:LX/13Q;

.field public final d:LX/2N4;

.field private final e:Ljava/lang/String;

.field private final f:LX/Jqf;

.field private final g:LX/2Og;

.field private final h:LX/3QK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2740945
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqv;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Jrc;LX/FDI;LX/13Q;LX/2N4;Ljava/lang/String;LX/Jqf;LX/2Og;LX/3QK;)V
    .locals 0
    .param p2    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2740886
    invoke-direct {p0}, LX/Jqi;-><init>()V

    .line 2740887
    iput-object p1, p0, LX/Jqv;->a:LX/Jrc;

    .line 2740888
    iput-object p2, p0, LX/Jqv;->b:LX/FDI;

    .line 2740889
    iput-object p3, p0, LX/Jqv;->c:LX/13Q;

    .line 2740890
    iput-object p4, p0, LX/Jqv;->d:LX/2N4;

    .line 2740891
    iput-object p5, p0, LX/Jqv;->e:Ljava/lang/String;

    .line 2740892
    iput-object p6, p0, LX/Jqv;->f:LX/Jqf;

    .line 2740893
    iput-object p7, p0, LX/Jqv;->g:LX/2Og;

    .line 2740894
    iput-object p8, p0, LX/Jqv;->h:LX/3QK;

    .line 2740895
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqv;
    .locals 7

    .prologue
    .line 2740918
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2740919
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2740920
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2740921
    if-nez v1, :cond_0

    .line 2740922
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2740923
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2740924
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2740925
    sget-object v1, LX/Jqv;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2740926
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2740927
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2740928
    :cond_1
    if-nez v1, :cond_4

    .line 2740929
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2740930
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2740931
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Jqv;->b(LX/0QB;)LX/Jqv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2740932
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2740933
    if-nez v1, :cond_2

    .line 2740934
    sget-object v0, LX/Jqv;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2740935
    :goto_1
    if-eqz v0, :cond_3

    .line 2740936
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740937
    :goto_3
    check-cast v0, LX/Jqv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2740938
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2740939
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2740940
    :catchall_1
    move-exception v0

    .line 2740941
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2740942
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2740943
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2740944
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqv;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/6jo;)Z
    .locals 1

    .prologue
    .line 2740917
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2740911
    invoke-static {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v1

    .line 2740912
    iget-object v2, p0, LX/Jqv;->b:LX/FDI;

    invoke-virtual {v2, v1}, LX/FDI;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2740913
    if-eqz v2, :cond_1

    .line 2740914
    :cond_0
    :goto_0
    return v0

    .line 2740915
    :cond_1
    iget-object v2, p0, LX/Jqv;->d:LX/2N4;

    invoke-virtual {v2, v1, v0}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    .line 2740916
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v2, v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v2, :cond_2

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/Jqv;
    .locals 9

    .prologue
    .line 2740909
    new-instance v0, LX/Jqv;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v1

    check-cast v1, LX/Jrc;

    invoke-static {p0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v2

    check-cast v2, LX/FDI;

    invoke-static {p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v3

    check-cast v3, LX/13Q;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v4

    check-cast v4, LX/2N4;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0}, LX/Jqf;->a(LX/0QB;)LX/Jqf;

    move-result-object v6

    check-cast v6, LX/Jqf;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v7

    check-cast v7, LX/2Og;

    invoke-static {p0}, LX/3QK;->a(LX/0QB;)LX/3QK;

    move-result-object v8

    check-cast v8, LX/3QK;

    invoke-direct/range {v0 .. v8}, LX/Jqv;-><init>(LX/Jrc;LX/FDI;LX/13Q;LX/2N4;Ljava/lang/String;LX/Jqf;LX/2Og;LX/3QK;)V

    .line 2740910
    return-object v0
.end method

.method private e(LX/6kW;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2740896
    invoke-virtual {p1}, LX/6kW;->r()LX/6jo;

    move-result-object v2

    .line 2740897
    iget-object v3, v2, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iget-object v4, v2, LX/6jo;->isLazy:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2740898
    :cond_0
    :goto_0
    return v0

    .line 2740899
    :cond_1
    invoke-static {v2}, LX/Jqv;->a(LX/6jo;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2740900
    iget-object v0, v2, LX/6jo;->messageId:Ljava/lang/String;

    .line 2740901
    iget-object v1, p0, LX/Jqv;->d:LX/2N4;

    invoke-virtual {v1, v0}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2740902
    goto :goto_0

    .line 2740903
    :cond_2
    iget-object v3, v2, LX/6jo;->threadKey:LX/6l9;

    if-eqz v3, :cond_4

    .line 2740904
    iget-object v3, p0, LX/Jqv;->a:LX/Jrc;

    iget-object v2, v2, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v3, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    invoke-direct {p0, v2}, LX/Jqv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2740905
    iget-object v1, p0, LX/Jqv;->c:LX/13Q;

    const-string v2, "lazy_dff_fetching_thread"

    invoke-virtual {v1, v2}, LX/13Q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2740906
    :cond_3
    iget-object v0, p0, LX/Jqv;->c:LX/13Q;

    const-string v2, "lazy_dff_not_fetching_thread"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2740907
    goto :goto_0

    :cond_4
    move v0, v1

    .line 2740908
    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 3

    .prologue
    .line 2740946
    check-cast p1, LX/6kW;

    .line 2740947
    invoke-direct {p0, p1}, LX/Jqv;->e(LX/6kW;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2740948
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740949
    :goto_0
    return-object v0

    .line 2740950
    :cond_0
    invoke-virtual {p1}, LX/6kW;->r()LX/6jo;

    move-result-object v0

    .line 2740951
    iget-object v1, p0, LX/Jqv;->a:LX/Jrc;

    iget-object v2, v0, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v1, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2740952
    invoke-static {v0}, LX/Jqv;->a(LX/6jo;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, LX/Jqv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740953
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2740954
    goto :goto_0

    .line 2740955
    :cond_1
    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2740852
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2740853
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->r()LX/6jo;

    move-result-object v1

    .line 2740854
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v1, LX/6jo;->isLazy:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2740855
    iget-object v0, p0, LX/Jqv;->a:LX/Jrc;

    iget-object v2, v1, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v0, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2740856
    const/4 v0, 0x0

    .line 2740857
    iget-object v3, v1, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 2740858
    iget-object v0, p0, LX/Jqv;->g:LX/2Og;

    iget-object v3, v1, LX/6jo;->messageId:Ljava/lang/String;

    .line 2740859
    if-eqz v2, :cond_0

    if-nez v3, :cond_4

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v0, p1

    .line 2740860
    if-nez v0, :cond_1

    .line 2740861
    iget-object v0, p0, LX/Jqv;->d:LX/2N4;

    iget-object v2, v1, LX/6jo;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2740862
    :cond_1
    iget-object v2, p0, LX/Jqv;->f:LX/Jqf;

    .line 2740863
    if-eqz v0, :cond_2

    iget-object v4, v2, LX/Jqf;->h:LX/0Uh;

    const/16 v5, 0x1fb

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2740864
    new-instance v5, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v6, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v8, 0x0

    iget-object v4, v2, LX/Jqf;->g:LX/2OQ;

    iget-object v7, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v7}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v9

    const-wide/16 v10, 0x0

    move-object v7, v0

    invoke-direct/range {v5 .. v11}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2740865
    iget-object v4, v2, LX/Jqf;->e:LX/Iuh;

    invoke-virtual {v4, v5}, LX/Iuh;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v4

    .line 2740866
    if-nez v4, :cond_5

    .line 2740867
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Jqv;->h:LX/3QK;

    const-string v2, "DFF"

    iget-object v1, v1, LX/6jo;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/3QK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2740868
    :cond_3
    return-void

    :cond_4
    invoke-static {v0, v2}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/2OQ;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    goto :goto_0

    .line 2740869
    :cond_5
    iget-object v5, v2, LX/Jqf;->d:LX/Jqb;

    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5, v6, v4}, LX/Jqb;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2740870
    check-cast p1, LX/6kW;

    .line 2740871
    invoke-virtual {p1}, LX/6kW;->r()LX/6jo;

    move-result-object v0

    .line 2740872
    iget-object v1, p0, LX/Jqv;->a:LX/Jrc;

    iget-object v0, v0, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2740873
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)LX/0P1;
    .locals 3

    .prologue
    .line 2740874
    check-cast p1, LX/6kW;

    .line 2740875
    invoke-virtual {p1}, LX/6kW;->r()LX/6jo;

    move-result-object v0

    .line 2740876
    iget-object v1, v0, LX/6jo;->messageId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2740877
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2740878
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/Jqv;->a:LX/Jrc;

    iget-object v2, v0, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v1, v2}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    iget-object v0, v0, LX/6jo;->messageId:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2740879
    check-cast p1, LX/6kW;

    .line 2740880
    invoke-virtual {p1}, LX/6kW;->r()LX/6jo;

    move-result-object v0

    .line 2740881
    iget-object v0, v0, LX/6jo;->threadKey:LX/6l9;

    iget-object v0, v0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    .line 2740882
    iget-object v1, p0, LX/Jqv;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Jqv;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2740883
    const/4 v0, 0x1

    .line 2740884
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2740885
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/Jqv;->e(LX/6kW;)Z

    move-result v0

    return v0
.end method
