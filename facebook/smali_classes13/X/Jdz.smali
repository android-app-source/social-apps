.class public final LX/Jdz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jdw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jdw",
        "<",
        "Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:LX/JeB;


# direct methods
.method public constructor <init>(LX/JeB;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2720247
    iput-object p1, p0, LX/Jdz;->b:LX/JeB;

    iput-object p2, p0, LX/Jdz;->a:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JeZ;LX/Jek;)V
    .locals 2

    .prologue
    .line 2720248
    check-cast p2, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;

    .line 2720249
    check-cast p1, LX/Jeg;

    .line 2720250
    iget-object v0, p1, LX/Jeg;->a:Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    move-object v0, v0

    .line 2720251
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2720252
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Landroid/net/Uri;)V

    .line 2720253
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Ljava/lang/String;)V

    .line 2720254
    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b(Ljava/lang/String;)V

    .line 2720255
    new-instance v1, LX/Jdy;

    invoke-direct {v1, p0, v0}, LX/Jdy;-><init>(LX/Jdz;Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;)V

    invoke-virtual {p2, v1}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->a(Landroid/view/View$OnClickListener;)V

    .line 2720256
    iget-object v0, p0, LX/Jdz;->b:LX/JeB;

    iget-object v0, v0, LX/JeB;->e:LX/JeN;

    invoke-virtual {v0}, LX/JeN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2720257
    iget-object v0, p2, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;->b:LX/JgA;

    .line 2720258
    iget-object v1, v0, LX/JgA;->e:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2720259
    :cond_1
    return-void
.end method
