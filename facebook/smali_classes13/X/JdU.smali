.class public LX/JdU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2719620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2719621
    iput-object p1, p0, LX/JdU;->a:LX/0Zb;

    .line 2719622
    return-void
.end method

.method public static a(LX/JdU;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2719623
    const-string v0, "target"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2719624
    iget-object v0, p0, LX/JdU;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2719625
    return-void
.end method

.method public static b(LX/0QB;)LX/JdU;
    .locals 2

    .prologue
    .line 2719626
    new-instance v1, LX/JdU;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/JdU;-><init>(LX/0Zb;)V

    .line 2719627
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2719628
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "message_block_toggle_block_messages_off"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2719629
    invoke-static {p0, v0, p1}, LX/JdU;->a(LX/JdU;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2719630
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2719631
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "message_block_toggle_block_messages_on"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2719632
    invoke-static {p0, v0, p1}, LX/JdU;->a(LX/JdU;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2719633
    return-void
.end method
