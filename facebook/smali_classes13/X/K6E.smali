.class public final LX/K6E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5Z;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardEndCard;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V
    .locals 0

    .prologue
    .line 2771206
    iput-object p1, p0, LX/K6E;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;B)V
    .locals 0

    .prologue
    .line 2771207
    invoke-direct {p0, p1}, LX/K6E;-><init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V

    return-void
.end method


# virtual methods
.method public final a(LX/ClW;LX/162;LX/CnN;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2771208
    if-eqz p1, :cond_0

    .line 2771209
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2771210
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2771211
    :goto_0
    if-eqz v0, :cond_1

    .line 2771212
    iget-object v0, p0, LX/K6E;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/BaseTarotCard;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->a(LX/ClW;LX/162;LX/CnN;)V

    .line 2771213
    iget-object v0, p0, LX/K6E;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/BaseTarotCard;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->setVisibility(I)V

    .line 2771214
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2771215
    goto :goto_0

    .line 2771216
    :cond_1
    iget-object v0, p0, LX/K6E;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/BaseTarotCard;->b:Lcom/facebook/tarot/cards/elements/FooterFeedbackView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/tarot/cards/elements/FooterFeedbackView;->setVisibility(I)V

    goto :goto_1
.end method
