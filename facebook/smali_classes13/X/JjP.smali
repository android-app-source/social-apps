.class public final LX/JjP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventDeleteModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JjT;


# direct methods
.method public constructor <init>(LX/JjT;)V
    .locals 0

    .prologue
    .line 2727655
    iput-object p1, p0, LX/JjP;->a:LX/JjT;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2727656
    iget-object v0, p0, LX/JjP;->a:LX/JjT;

    iget-object v0, v0, LX/JjT;->b:LX/JjG;

    const-string v1, "EventReminderMutator"

    const-string v2, "Failed to delete an event reminder."

    invoke-virtual {v0, v1, v2, p1}, LX/JjG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2727657
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2727658
    return-void
.end method
