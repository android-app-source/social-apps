.class public final LX/K69;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K68;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardEndCard;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V
    .locals 0

    .prologue
    .line 2771176
    iput-object p1, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 2771177
    if-eqz p1, :cond_0

    .line 2771178
    iget-object v0, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->e:LX/K8f;

    iget-object v1, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/K8f;->a(Ljava/lang/String;)V

    .line 2771179
    :goto_0
    iget-object v0, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->v:LX/K5i;

    .line 2771180
    if-eqz p1, :cond_1

    sget-object v1, LX/K6F;->NOT_FOLLOWING_INITIALLY_NOW_FOLLOWING_SUBSCRIBED:LX/K6F;

    :goto_1
    iput-object v1, v0, LX/K5i;->c:LX/K6F;

    .line 2771181
    invoke-static {v0}, LX/K5i;->b(LX/K5i;)V

    .line 2771182
    return-void

    .line 2771183
    :cond_0
    iget-object v0, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->f:LX/2dj;

    iget-object v1, p0, LX/K69;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v1, Lcom/facebook/tarot/cards/TarotCardEndCard;->w:Ljava/lang/String;

    const-string v2, "TAROT_STORY_SUBSCRIPTION"

    invoke-virtual {v0, v1, v2}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2771184
    :cond_1
    sget-object v1, LX/K6F;->NOT_FOLLOWING_INITIALLY:LX/K6F;

    goto :goto_1
.end method
