.class public final LX/JuY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/MissedCallNotification;

.field public final synthetic b:J

.field public final synthetic c:I

.field public final synthetic d:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/MissedCallNotification;JI)V
    .locals 1

    .prologue
    .line 2748916
    iput-object p1, p0, LX/JuY;->d:LX/3RG;

    iput-object p2, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iput-wide p3, p0, LX/JuY;->b:J

    iput p5, p0, LX/JuY;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 9
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 2748917
    iget-object v0, p0, LX/JuY;->d:LX/3RG;

    iget-object v1, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, p0, LX/JuY;->b:J

    iget-object v4, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-wide v4, v4, Lcom/facebook/messaging/notify/MissedCallNotification;->d:J

    iget-object v6, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v6, v6, Lcom/facebook/messaging/notify/MissedCallNotification;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/3RG;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2748918
    iget-object v0, p0, LX/JuY;->d:LX/3RG;

    iget-wide v2, p0, LX/JuY;->b:J

    iget-object v4, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v4, v4, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, LX/3RG;->a(JLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2748919
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v3, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->a:Ljava/lang/String;

    .line 2748920
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v4, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->b:Ljava/lang/String;

    .line 2748921
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v0, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    sget-object v5, LX/Di7;->CONFERENCE_ON_GOING:LX/Di7;

    if-ne v0, v5, :cond_5

    .line 2748922
    iget-object v0, p0, LX/JuY;->d:LX/3RG;

    iget-object v5, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v5, v5, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v6, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v6, v6, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    invoke-virtual {v0, v5, v7, v8, v6}, LX/3RG;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748923
    if-eqz v0, :cond_5

    .line 2748924
    :goto_0
    new-instance v5, LX/2HB;

    iget-object v6, p0, LX/JuY;->d:LX/3RG;

    iget-object v6, v6, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v5, v6}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget v4, p0, LX/JuY;->c:I

    invoke-virtual {v3, v4}, LX/2HB;->a(I)LX/2HB;

    move-result-object v3

    .line 2748925
    iput-object v0, v3, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2748926
    move-object v3, v3

    .line 2748927
    invoke-virtual {v3, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v2

    const/4 v3, 0x2

    .line 2748928
    iput v3, v2, LX/2HB;->j:I

    .line 2748929
    move-object v2, v2

    .line 2748930
    iget-object v3, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-wide v4, v3, Lcom/facebook/messaging/notify/MissedCallNotification;->d:J

    invoke-virtual {v2, v4, v5}, LX/2HB;->a(J)LX/2HB;

    move-result-object v2

    iget-object v3, p0, LX/JuY;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0301

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2748931
    iput v3, v2, LX/2HB;->y:I

    .line 2748932
    move-object v2, v2

    .line 2748933
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v2

    .line 2748934
    if-eqz p1, :cond_0

    .line 2748935
    iput-object p1, v2, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 2748936
    :cond_0
    iget-object v3, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v3, v3, Lcom/facebook/messaging/notify/MissedCallNotification;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/JuY;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->z:LX/00H;

    .line 2748937
    iget-object v4, v3, LX/00H;->j:LX/01T;

    move-object v3, v4

    .line 2748938
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    if-ne v3, v4, :cond_1

    .line 2748939
    invoke-static {}, LX/10A;->a()I

    move-result v3

    iget-object v4, p0, LX/JuY;->d:LX/3RG;

    iget-object v4, v4, LX/3RG;->b:Landroid/content/Context;

    const v5, 0x7f08079b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2748940
    iget-object v1, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    sget-object v3, LX/Di7;->CONFERENCE_ON_GOING:LX/Di7;

    if-ne v1, v3, :cond_2

    .line 2748941
    const v1, 0x7f021ab9

    iget-object v3, p0, LX/JuY;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    const v4, 0x7f080721

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2748942
    :cond_1
    :goto_1
    iget-object v0, p0, LX/JuY;->d:LX/3RG;

    iget-object v0, v0, LX/3RG;->f:LX/3RQ;

    iget-object v1, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/MissedCallNotification;->g:LX/Dhq;

    iget-object v3, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v3, v3, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, v1, v8, v3}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2748943
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v0, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    sget-object v1, LX/Di7;->P2P:LX/Di7;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v0, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->c:Ljava/lang/String;

    .line 2748944
    :goto_2
    iget-object v1, p0, LX/JuY;->d:LX/3RG;

    .line 2748945
    invoke-static {v0}, LX/0db;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    .line 2748946
    iget-object v4, v1, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    .line 2748947
    invoke-interface {v4, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2748948
    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2748949
    iget-object v1, p0, LX/JuY;->d:LX/3RG;

    iget-object v1, v1, LX/3RG;->d:LX/3RK;

    const/16 v3, 0x271a

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2748950
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    invoke-virtual {v0}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 2748951
    return-void

    .line 2748952
    :cond_2
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    .line 2748953
    iget-object v1, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    sget-object v3, LX/Di7;->CONFERENCE_ON_GOING:LX/Di7;

    if-eq v1, v3, :cond_3

    iget-object v1, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->h:LX/Di7;

    sget-object v3, LX/Di7;->CONFERENCE_ENDED:LX/Di7;

    if-ne v1, v3, :cond_6

    :cond_3
    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 2748954
    if-nez v0, :cond_1

    .line 2748955
    iget-object v0, p0, LX/JuY;->d:LX/3RG;

    iget-wide v4, p0, LX/JuY;->b:J

    iget-object v1, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v1, v1, Lcom/facebook/messaging/notify/MissedCallNotification;->f:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v7, v1}, LX/3RG;->a(JZLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2748956
    const v1, 0x7f021ab9

    iget-object v3, p0, LX/JuY;->d:LX/3RG;

    iget-object v3, v3, LX/3RG;->b:Landroid/content/Context;

    const v4, 0x7f080751

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    goto :goto_1

    .line 2748957
    :cond_4
    iget-object v0, p0, LX/JuY;->a:Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v0, v0, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2748958
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuY;->a(Landroid/graphics/Bitmap;)V

    .line 2748959
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2748960
    const/4 v0, 0x0

    .line 2748961
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2748962
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2748963
    :cond_0
    invoke-direct {p0, v0}, LX/JuY;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748964
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2748965
    return-void

    .line 2748966
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
