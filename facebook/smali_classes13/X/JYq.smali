.class public final LX/JYq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2707374
    iput-object p1, p0, LX/JYq;->c:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iput-object p2, p0, LX/JYq;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/JYq;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5646158

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2707375
    iget-object v1, p0, LX/JYq;->c:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->f:LX/0Zb;

    iget-object v2, p0, LX/JYq;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/JYy;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    .line 2707376
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "tapped_donate_button"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "fundraiser_share_attachment"

    .line 2707377
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2707378
    move-object v3, v3

    .line 2707379
    const-string v4, "fundraiser_campaign_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object v2, v3

    .line 2707380
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2707381
    iget-object v1, p0, LX/JYq;->c:Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->e:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/JYq;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2707382
    const v1, 0x17cb30f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
