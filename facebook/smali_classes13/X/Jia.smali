.class public LX/Jia;
.super LX/3LH;
.source ""

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements LX/2ht;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/3LB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:[Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field public k:LX/JiI;

.field public l:LX/Ism;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2726343
    const-class v0, LX/Jia;

    sput-object v0, LX/Jia;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2726285
    invoke-direct {p0}, LX/3LH;-><init>()V

    .line 2726286
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2726287
    iput-object v0, p0, LX/Jia;->d:LX/0Px;

    .line 2726288
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jia;->e:Ljava/util/HashMap;

    .line 2726289
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Jia;->f:Ljava/util/HashMap;

    .line 2726290
    const/4 v0, -0x1

    iput v0, p0, LX/Jia;->h:I

    .line 2726291
    const/4 v0, 0x0

    iput v0, p0, LX/Jia;->j:I

    .line 2726292
    iput-object p1, p0, LX/Jia;->c:Landroid/content/Context;

    .line 2726293
    return-void
.end method

.method private e(Landroid/view/View;)Landroid/view/View;
    .locals 9

    .prologue
    .line 2726328
    check-cast p1, LX/Ife;

    .line 2726329
    if-nez p1, :cond_0

    .line 2726330
    new-instance p1, LX/Ife;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p1, v0}, LX/Ife;-><init>(Landroid/content/Context;)V

    .line 2726331
    :cond_0
    new-instance v0, LX/JiY;

    invoke-direct {v0, p0}, LX/JiY;-><init>(LX/Jia;)V

    const/4 v2, 0x0

    .line 2726332
    iput-object v0, p1, LX/Ife;->n:LX/JiY;

    .line 2726333
    iget-object v1, p1, LX/Ife;->c:LX/Ieu;

    const-string v3, "PEOPLE_TAB"

    invoke-virtual {v1, v3}, LX/Ieu;->a(Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    move-result-object v3

    .line 2726334
    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2726335
    iget-object v1, p1, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    if-eqz v1, :cond_2

    iget-object v1, p1, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    if-eq v1, v3, :cond_1

    iget-object v1, p1, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-wide v5, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    iget-wide v7, v3, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    cmp-long v1, v5, v7

    if-ltz v1, :cond_2

    :cond_1
    iget-object v1, p1, LX/Ife;->c:LX/Ieu;

    invoke-virtual {v1}, LX/Ieu;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2726336
    :cond_2
    invoke-static {p1, v3}, LX/Ife;->setData(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V

    .line 2726337
    iget-object v1, p1, LX/Ife;->c:LX/Ieu;

    invoke-virtual {v1, v2}, LX/Ieu;->a(Z)V

    .line 2726338
    :cond_3
    iget-object v0, p0, LX/Jia;->m:LX/0gc;

    .line 2726339
    iget-object v1, p1, LX/Ife;->a:LX/Ies;

    new-instance v2, LX/Ifb;

    invoke-direct {v2, p1, v0}, LX/Ifb;-><init>(LX/Ife;LX/0gc;)V

    .line 2726340
    iput-object v2, v1, LX/Ies;->d:LX/IfB;

    .line 2726341
    return-object p1

    :cond_4
    move v1, v2

    .line 2726342
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2726311
    iput-object p1, p0, LX/Jia;->d:LX/0Px;

    .line 2726312
    const/4 v0, 0x0

    .line 2726313
    iget-object v1, p0, LX/Jia;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 2726314
    iget-object v1, p0, LX/Jia;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 2726315
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2726316
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2726317
    instance-of v5, v0, LX/3Ov;

    if-eqz v5, :cond_0

    .line 2726318
    check-cast v0, LX/3Ov;

    .line 2726319
    invoke-interface {v0}, LX/3Ov;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2726320
    iget-object v0, p0, LX/Jia;->e:Ljava/util/HashMap;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2726321
    :cond_0
    iget-object v0, p0, LX/Jia;->f:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2726322
    add-int/lit8 v2, v2, 0x1

    .line 2726323
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2726324
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, LX/Jia;->g:[Ljava/lang/String;

    .line 2726325
    iget-object v0, p0, LX/Jia;->g:[Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2726326
    const v0, 0x107acc68

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2726327
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2726310
    const/4 v0, 0x0

    return v0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2726302
    invoke-virtual {p0, p1}, LX/Jia;->getSectionForPosition(I)I

    move-result v1

    .line 2726303
    invoke-virtual {p0, v1}, LX/Jia;->getPositionForSection(I)I

    move-result v1

    .line 2726304
    invoke-virtual {p0, v1}, LX/Jia;->getItemViewType(I)I

    move-result v2

    .line 2726305
    sget-object v3, LX/JiZ;->CONTACT_UPLOAD_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->CHAT_AVAILABILITY_TOGGLE_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->INVITE_FRIENDS_UPSELL_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->INVITE_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->NEW_GROUPS_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->SMS_TAKEOVER_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->SMS_BRIDGE_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->ADD_CONTACTS_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v3, LX/JiZ;->CYMK_ROW:LX/JiZ;

    invoke-virtual {v3}, LX/JiZ;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 2726306
    :cond_0
    :goto_0
    return-object v0

    .line 2726307
    :cond_1
    iget v3, p0, LX/Jia;->h:I

    if-eq v3, v2, :cond_2

    move-object p2, v0

    .line 2726308
    :cond_2
    iput v2, p0, LX/Jia;->h:I

    .line 2726309
    invoke-virtual {p0, v1, p2, p3}, LX/Jia;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2726301
    iget v0, p0, LX/Jia;->j:I

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2726300
    iget v0, p0, LX/Jia;->i:I

    return v0
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2726295
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/Jia;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 2726296
    :cond_0
    :goto_0
    return v0

    .line 2726297
    :cond_1
    invoke-virtual {p0, p1}, LX/Jia;->getSectionForPosition(I)I

    move-result v1

    .line 2726298
    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, LX/Jia;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 2726299
    invoke-virtual {p0, p1}, LX/Jia;->getSectionForPosition(I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/Jia;->getPositionForSection(I)I

    move-result v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2726294
    iget-object v0, p0, LX/Jia;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2726344
    iget-object v0, p0, LX/Jia;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2726055
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    .line 2726056
    iget-object v0, p0, LX/Jia;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2726057
    instance-of v1, v0, LX/3OO;

    if-eqz v1, :cond_0

    .line 2726058
    sget-object v0, LX/JiZ;->CONTACT_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    .line 2726059
    :goto_0
    return v0

    .line 2726060
    :cond_0
    instance-of v1, v0, LX/DAb;

    if-eqz v1, :cond_1

    .line 2726061
    sget-object v0, LX/JiZ;->SECTION_SPLITTER:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726062
    :cond_1
    instance-of v1, v0, LX/DAa;

    if-eqz v1, :cond_2

    .line 2726063
    sget-object v0, LX/JiZ;->SECTION_HEADER:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726064
    :cond_2
    instance-of v1, v0, LX/3Ou;

    if-eqz v1, :cond_3

    .line 2726065
    sget-object v0, LX/JiZ;->FAVORITES_HEADER:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726066
    :cond_3
    instance-of v1, v0, LX/Ji6;

    if-eqz v1, :cond_4

    .line 2726067
    sget-object v0, LX/JiZ;->CONTACT_UPLOAD_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726068
    :cond_4
    instance-of v1, v0, LX/JiW;

    if-eqz v1, :cond_5

    .line 2726069
    sget-object v0, LX/JiZ;->CHAT_AVAILABILITY_TOGGLE_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726070
    :cond_5
    instance-of v1, v0, LX/JiO;

    if-eqz v1, :cond_6

    .line 2726071
    sget-object v0, LX/JiZ;->INVITE_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726072
    :cond_6
    instance-of v1, v0, LX/JiK;

    if-eqz v1, :cond_7

    .line 2726073
    sget-object v0, LX/JiZ;->INVITE_FRIENDS_UPSELL_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726074
    :cond_7
    instance-of v1, v0, LX/Jhz;

    if-eqz v1, :cond_8

    .line 2726075
    sget-object v0, LX/JiZ;->NEW_GROUPS_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726076
    :cond_8
    instance-of v1, v0, LX/JiQ;

    if-eqz v1, :cond_9

    .line 2726077
    sget-object v0, LX/JiZ;->SMS_TAKEOVER_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726078
    :cond_9
    instance-of v1, v0, LX/JhU;

    if-eqz v1, :cond_a

    .line 2726079
    sget-object v0, LX/JiZ;->H_SCROLL_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726080
    :cond_a
    instance-of v1, v0, LX/Jii;

    if-eqz v1, :cond_b

    .line 2726081
    sget-object v0, LX/JiZ;->IMAGE_CODE_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2726082
    :cond_b
    instance-of v1, v0, LX/JhT;

    if-eqz v1, :cond_c

    .line 2726083
    sget-object v0, LX/JiZ;->CYMK_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 2726084
    :cond_c
    instance-of v1, v0, LX/Jht;

    if-eqz v1, :cond_d

    .line 2726085
    sget-object v0, LX/JiZ;->MESSAGE_REQUESTS_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 2726086
    :cond_d
    instance-of v1, v0, LX/Jhx;

    if-eqz v1, :cond_e

    .line 2726087
    sget-object v0, LX/JiZ;->MESSENGER_CONTACTS_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 2726088
    :cond_e
    instance-of v1, v0, LX/JiP;

    if-eqz v1, :cond_f

    .line 2726089
    sget-object v0, LX/JiZ;->SMS_BRIDGE_PERMANENT_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 2726090
    :cond_f
    instance-of v1, v0, LX/Ji4;

    if-eqz v1, :cond_10

    .line 2726091
    sget-object v0, LX/JiZ;->ADD_CONTACTS_ROW:LX/JiZ;

    invoke-virtual {v0}, LX/JiZ;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 2726092
    :cond_10
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final getPositionForSection(I)I
    .locals 2

    .prologue
    .line 2726093
    iget-object v0, p0, LX/Jia;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2726094
    iget-object v0, p0, LX/Jia;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2726095
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Jia;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 2726096
    iget-object v0, p0, LX/Jia;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2726097
    iget-object v0, p0, LX/Jia;->g:[Ljava/lang/String;

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2726098
    iget-object v0, p0, LX/Jia;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 2726099
    instance-of v1, v0, LX/3OO;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 2726100
    check-cast v1, LX/3OO;

    if-lez p1, :cond_2

    iget-object v2, p0, LX/Jia;->d:LX/0Px;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3OQ;

    :goto_0
    const/4 v3, 0x0

    .line 2726101
    iget-object p1, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    if-eqz p1, :cond_0

    iget-object p1, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result p1

    if-eqz p1, :cond_0

    instance-of p1, v2, LX/3OO;

    if-nez p1, :cond_26

    .line 2726102
    :cond_0
    iput-boolean v3, v1, LX/3OO;->K:Z

    .line 2726103
    :goto_1
    check-cast v0, LX/3OO;

    .line 2726104
    check-cast p2, LX/Jho;

    .line 2726105
    if-nez p2, :cond_1

    .line 2726106
    new-instance p2, LX/Jho;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/Jho;-><init>(Landroid/content/Context;)V

    .line 2726107
    :cond_1
    invoke-virtual {p2, v0}, LX/Jho;->setContactRow(LX/3OO;)V

    .line 2726108
    move-object v0, p2

    .line 2726109
    :goto_2
    return-object v0

    .line 2726110
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2726111
    :cond_3
    instance-of v1, v0, LX/DAb;

    if-eqz v1, :cond_5

    .line 2726112
    check-cast p2, LX/JiR;

    .line 2726113
    if-nez p2, :cond_4

    .line 2726114
    new-instance p2, LX/JiR;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/JiR;-><init>(Landroid/content/Context;)V

    .line 2726115
    :cond_4
    move-object v0, p2

    .line 2726116
    goto :goto_2

    .line 2726117
    :cond_5
    instance-of v1, v0, LX/DAa;

    if-eqz v1, :cond_8

    .line 2726118
    iget-object v2, p0, LX/Jia;->a:LX/3LB;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v1, v0

    check-cast v1, LX/DAa;

    .line 2726119
    iget-object v4, v1, LX/DAa;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2726120
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 2726121
    :cond_6
    :goto_3
    check-cast v0, LX/DAa;

    .line 2726122
    check-cast p2, LX/Js7;

    .line 2726123
    if-nez p2, :cond_7

    .line 2726124
    new-instance p2, LX/Js7;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/Js7;-><init>(Landroid/content/Context;)V

    .line 2726125
    :cond_7
    iget-object v1, v0, LX/DAa;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2726126
    invoke-virtual {p2, v1}, LX/Js7;->setText(Ljava/lang/String;)V

    .line 2726127
    move-object v0, p2

    .line 2726128
    goto :goto_2

    .line 2726129
    :cond_8
    instance-of v1, v0, LX/3Ou;

    if-eqz v1, :cond_a

    .line 2726130
    check-cast v0, LX/3Ou;

    .line 2726131
    check-cast p2, LX/Js7;

    .line 2726132
    if-nez p2, :cond_9

    .line 2726133
    new-instance p2, LX/Js7;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/Js7;-><init>(Landroid/content/Context;)V

    .line 2726134
    :cond_9
    iget-object v1, v0, LX/3Ou;->a:Ljava/lang/String;

    invoke-virtual {p2, v1}, LX/Js7;->setText(Ljava/lang/String;)V

    .line 2726135
    iget-object v1, v0, LX/3Ou;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, LX/Js7;->setActionButtonText(Ljava/lang/String;)V

    .line 2726136
    iget-object v1, v0, LX/3Ou;->c:Landroid/view/View$OnClickListener;

    .line 2726137
    iput-object v1, p2, LX/Js7;->c:Landroid/view/View$OnClickListener;

    .line 2726138
    move-object v0, p2

    .line 2726139
    goto :goto_2

    .line 2726140
    :cond_a
    instance-of v1, v0, LX/Ji6;

    if-eqz v1, :cond_c

    .line 2726141
    check-cast p2, LX/JiJ;

    .line 2726142
    if-nez p2, :cond_b

    .line 2726143
    new-instance p2, LX/JiJ;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/JiJ;-><init>(Landroid/content/Context;)V

    .line 2726144
    iget-object v0, p0, LX/Jia;->k:LX/JiI;

    .line 2726145
    iput-object v0, p2, LX/JiJ;->t:LX/JiI;

    .line 2726146
    :cond_b
    move-object v0, p2

    .line 2726147
    goto :goto_2

    .line 2726148
    :cond_c
    instance-of v1, v0, LX/JiW;

    if-eqz v1, :cond_e

    .line 2726149
    check-cast v0, LX/JiW;

    .line 2726150
    check-cast p2, LX/JiV;

    .line 2726151
    if-nez p2, :cond_d

    .line 2726152
    new-instance p2, LX/JiV;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiV;-><init>(Landroid/content/Context;)V

    .line 2726153
    :cond_d
    iput-object v0, p2, LX/JiV;->e:LX/JiW;

    .line 2726154
    iget-object v1, p2, LX/JiV;->e:LX/JiW;

    .line 2726155
    iget-object p0, v1, LX/JiW;->a:Lcom/facebook/user/model/User;

    move-object v1, p0

    .line 2726156
    iget-object p0, p2, LX/JiV;->b:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2726157
    iget-object p0, p2, LX/JiV;->a:LX/FJw;

    invoke-virtual {p0, v1}, LX/FJw;->b(Lcom/facebook/user/model/User;)LX/8ue;

    move-result-object p0

    .line 2726158
    iget-object v0, p2, LX/JiV;->c:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v1, p0}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2726159
    iget-object v1, p2, LX/JiV;->e:LX/JiW;

    .line 2726160
    iget-boolean p0, v1, LX/JiW;->b:Z

    move v1, p0

    .line 2726161
    iget-object p0, p2, LX/JiV;->f:Landroid/widget/CompoundButton;

    invoke-virtual {p0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2726162
    iget-object p0, p2, LX/JiV;->d:Lcom/facebook/messaging/presence/PresenceIndicatorView;

    if-eqz v1, :cond_29

    const/4 v1, 0x0

    :goto_4
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/presence/PresenceIndicatorView;->setVisibility(I)V

    .line 2726163
    move-object v0, p2

    .line 2726164
    goto/16 :goto_2

    .line 2726165
    :cond_e
    instance-of v1, v0, LX/JiK;

    if-eqz v1, :cond_10

    .line 2726166
    check-cast p2, LX/JiN;

    .line 2726167
    if-nez p2, :cond_f

    .line 2726168
    new-instance p2, LX/JiN;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/JiN;-><init>(Landroid/content/Context;)V

    .line 2726169
    :cond_f
    move-object v0, p2

    .line 2726170
    goto/16 :goto_2

    .line 2726171
    :cond_10
    instance-of v1, v0, LX/JiO;

    if-eqz v1, :cond_12

    .line 2726172
    check-cast v0, LX/JiO;

    .line 2726173
    check-cast p2, LX/JiS;

    .line 2726174
    if-nez p2, :cond_11

    .line 2726175
    new-instance p2, LX/JiS;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiS;-><init>(Landroid/content/Context;)V

    .line 2726176
    :cond_11
    iget v1, v0, LX/Ji3;->a:I

    invoke-virtual {p2, v1}, LX/JiS;->setTextResource(I)V

    .line 2726177
    iget v1, v0, LX/Ji3;->b:I

    invoke-virtual {p2, v1}, LX/JiS;->setIconResource(I)V

    .line 2726178
    move-object v0, p2

    .line 2726179
    goto/16 :goto_2

    .line 2726180
    :cond_12
    instance-of v1, v0, LX/Ji4;

    if-eqz v1, :cond_14

    .line 2726181
    check-cast v0, LX/Ji4;

    .line 2726182
    check-cast p2, LX/JiS;

    .line 2726183
    if-nez p2, :cond_13

    .line 2726184
    new-instance p2, LX/JiS;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiS;-><init>(Landroid/content/Context;)V

    .line 2726185
    :cond_13
    iget v1, v0, LX/Ji3;->a:I

    invoke-virtual {p2, v1}, LX/JiS;->setTextResource(I)V

    .line 2726186
    iget v1, v0, LX/Ji3;->b:I

    invoke-virtual {p2, v1}, LX/JiS;->setIconResource(I)V

    .line 2726187
    move-object v0, p2

    .line 2726188
    goto/16 :goto_2

    .line 2726189
    :cond_14
    instance-of v1, v0, LX/Jhz;

    if-eqz v1, :cond_16

    .line 2726190
    check-cast p2, LX/Ji0;

    .line 2726191
    if-nez p2, :cond_15

    .line 2726192
    new-instance p2, LX/Ji0;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Ji0;-><init>(Landroid/content/Context;)V

    .line 2726193
    :cond_15
    move-object v0, p2

    .line 2726194
    goto/16 :goto_2

    .line 2726195
    :cond_16
    instance-of v1, v0, LX/JiQ;

    if-eqz v1, :cond_18

    .line 2726196
    check-cast v0, LX/JiQ;

    .line 2726197
    check-cast p2, LX/JiS;

    .line 2726198
    if-nez p2, :cond_17

    .line 2726199
    new-instance p2, LX/JiS;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiS;-><init>(Landroid/content/Context;)V

    .line 2726200
    :cond_17
    iget v1, v0, LX/Ji3;->a:I

    invoke-virtual {p2, v1}, LX/JiS;->setTextResource(I)V

    .line 2726201
    iget v1, v0, LX/Ji3;->b:I

    invoke-virtual {p2, v1}, LX/JiS;->setIconResource(I)V

    .line 2726202
    move-object v0, p2

    .line 2726203
    goto/16 :goto_2

    .line 2726204
    :cond_18
    instance-of v1, v0, LX/JhU;

    if-eqz v1, :cond_1c

    .line 2726205
    check-cast v0, LX/JhU;

    .line 2726206
    check-cast p2, LX/Isi;

    .line 2726207
    if-nez p2, :cond_19

    .line 2726208
    new-instance p2, LX/Isi;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/Isi;-><init>(Landroid/content/Context;)V

    .line 2726209
    :cond_19
    iget-boolean v1, v0, LX/JhU;->a:Z

    .line 2726210
    invoke-virtual {p2}, LX/Isi;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2726211
    iput-boolean v1, p2, LX/Isi;->c:Z

    .line 2726212
    iget-object v3, p2, LX/Isi;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget-boolean v4, p2, LX/Isi;->c:Z

    if-eqz v4, :cond_2a

    const v4, 0x7f0b03c0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    :goto_5
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2726213
    iget-object v2, p2, LX/Isi;->a:LX/Isl;

    .line 2726214
    iput-boolean v1, v2, LX/Isl;->c:Z

    .line 2726215
    iget-object v1, v0, LX/JhU;->b:LX/0Px;

    move-object v1, v1

    .line 2726216
    iget-object v2, p2, LX/Isi;->a:LX/Isl;

    .line 2726217
    iput-object v1, v2, LX/Isl;->e:LX/0Px;

    .line 2726218
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2726219
    iget-object v1, p0, LX/Jia;->l:LX/Ism;

    .line 2726220
    iget-object v2, p2, LX/Isi;->a:LX/Isl;

    .line 2726221
    iput-object v1, v2, LX/Isl;->d:LX/Ism;

    .line 2726222
    iget-object v1, v0, LX/JhU;->b:LX/0Px;

    move-object v1, v1

    .line 2726223
    const/4 v3, 0x0

    .line 2726224
    if-eqz v1, :cond_1a

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2b

    :cond_1a
    move v2, v3

    .line 2726225
    :goto_6
    move v1, v2

    .line 2726226
    if-eqz v1, :cond_1b

    .line 2726227
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2726228
    new-instance v2, LX/JiX;

    invoke-direct {v2, p0, v1, v0}, LX/JiX;-><init>(LX/Jia;Ljava/util/Set;LX/JhU;)V

    .line 2726229
    iget-object v1, p2, LX/Isi;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2726230
    :cond_1b
    move-object v0, p2

    .line 2726231
    goto/16 :goto_2

    .line 2726232
    :cond_1c
    instance-of v1, v0, LX/Jii;

    if-eqz v1, :cond_1e

    .line 2726233
    check-cast v0, LX/Jii;

    .line 2726234
    check-cast p2, LX/JiS;

    .line 2726235
    if-nez p2, :cond_1d

    .line 2726236
    new-instance p2, LX/JiS;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiS;-><init>(Landroid/content/Context;)V

    .line 2726237
    :cond_1d
    iget v1, v0, LX/Ji3;->a:I

    invoke-virtual {p2, v1}, LX/JiS;->setTextResource(I)V

    .line 2726238
    iget v1, v0, LX/Ji3;->b:I

    invoke-virtual {p2, v1}, LX/JiS;->setIconResource(I)V

    .line 2726239
    move-object v0, p2

    .line 2726240
    goto/16 :goto_2

    .line 2726241
    :cond_1e
    instance-of v1, v0, LX/JhT;

    if-eqz v1, :cond_1f

    .line 2726242
    invoke-direct {p0, p2}, LX/Jia;->e(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_2

    .line 2726243
    :cond_1f
    instance-of v1, v0, LX/Jht;

    if-eqz v1, :cond_21

    .line 2726244
    check-cast p2, LX/Jhv;

    .line 2726245
    if-nez p2, :cond_20

    .line 2726246
    new-instance p2, LX/Jhv;

    iget-object v0, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jhv;-><init>(Landroid/content/Context;)V

    .line 2726247
    :cond_20
    iget-object v0, p2, LX/Jhv;->c:LX/JnW;

    invoke-virtual {v0}, LX/JnW;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2726248
    new-instance v1, LX/Jhu;

    invoke-direct {v1, p2}, LX/Jhu;-><init>(LX/Jhv;)V

    iget-object p0, p2, LX/Jhv;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2726249
    move-object v0, p2

    .line 2726250
    goto/16 :goto_2

    .line 2726251
    :cond_21
    instance-of v1, v0, LX/Jhx;

    if-eqz v1, :cond_23

    .line 2726252
    check-cast v0, LX/Jhx;

    .line 2726253
    check-cast p2, LX/Jhy;

    .line 2726254
    if-nez p2, :cond_22

    .line 2726255
    new-instance p2, LX/Jhy;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/Jhy;-><init>(Landroid/content/Context;)V

    .line 2726256
    :cond_22
    iget v1, v0, LX/Jhx;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2726257
    iget-object p0, p2, LX/Jhy;->a:Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerCustomListItem;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2726258
    move-object v0, p2

    .line 2726259
    goto/16 :goto_2

    .line 2726260
    :cond_23
    instance-of v1, v0, LX/JiP;

    if-eqz v1, :cond_25

    .line 2726261
    check-cast v0, LX/JiP;

    .line 2726262
    check-cast p2, LX/JiS;

    .line 2726263
    if-nez p2, :cond_24

    .line 2726264
    new-instance p2, LX/JiS;

    iget-object v1, p0, LX/Jia;->c:Landroid/content/Context;

    invoke-direct {p2, v1}, LX/JiS;-><init>(Landroid/content/Context;)V

    .line 2726265
    :cond_24
    iget v1, v0, LX/Ji3;->a:I

    invoke-virtual {p2, v1}, LX/JiS;->setTextResource(I)V

    .line 2726266
    iget v1, v0, LX/Ji3;->b:I

    invoke-virtual {p2, v1}, LX/JiS;->setIconResource(I)V

    .line 2726267
    move-object v0, p2

    .line 2726268
    goto/16 :goto_2

    .line 2726269
    :cond_25
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2726270
    :cond_26
    check-cast v2, LX/3OO;

    .line 2726271
    iget-object p1, v2, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object p1, p1

    .line 2726272
    if-eqz p1, :cond_27

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result p3

    if-eqz p3, :cond_27

    iget-object p3, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    invoke-virtual {p3}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_27

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object p1

    iget-object p3, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    invoke-virtual {p3}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_27

    const/4 v3, 0x1

    :cond_27
    iput-boolean v3, v1, LX/3OO;->K:Z

    goto/16 :goto_1

    .line 2726273
    :cond_28
    iget-object v4, v1, LX/DAa;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2726274
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0802d7

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2726275
    iget-object v4, v2, LX/3LB;->c:Landroid/os/Handler;

    new-instance p1, Lcom/facebook/messaging/contacts/picker/util/MessagingContactLoggingHelper$1;

    invoke-direct {p1, v2}, Lcom/facebook/messaging/contacts/picker/util/MessagingContactLoggingHelper$1;-><init>(LX/3LB;)V

    const p3, 0x280b2f0e

    invoke-static {v4, p1, p3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_3

    .line 2726276
    :cond_29
    const/16 v1, 0x8

    goto/16 :goto_4

    .line 2726277
    :cond_2a
    const v4, 0x7f0b03c1

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto/16 :goto_5

    .line 2726278
    :cond_2b
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ish;

    .line 2726279
    iget-object v4, v2, LX/Ish;->c:LX/Isg;

    move-object v2, v4

    .line 2726280
    sget-object v4, LX/Isg;->BOTS:LX/Isg;

    if-eq v2, v4, :cond_2c

    sget-object v4, LX/Isg;->DIRECT_M:LX/Isg;

    if-ne v2, v4, :cond_2d

    :cond_2c
    const/4 v2, 0x1

    goto/16 :goto_6

    :cond_2d
    move v2, v3

    goto/16 :goto_6
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2726281
    invoke-static {}, LX/JiZ;->values()[LX/JiZ;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2726282
    invoke-virtual {p0, p1}, LX/Jia;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2726283
    instance-of v1, v0, LX/3OO;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/JiO;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/Jhz;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/JiQ;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/Jii;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/Jht;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/Jhx;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/JiP;

    if-nez v1, :cond_0

    instance-of v0, v0, LX/Ji4;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2726284
    const/4 v0, 0x0

    return v0
.end method
