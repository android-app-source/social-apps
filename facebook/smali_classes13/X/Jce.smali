.class public final LX/Jce;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Jcf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2718158
    invoke-static {}, LX/Jcf;->q()LX/Jcf;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2718159
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Jce;->a:Z

    .line 2718160
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2718177
    const-string v0, "MapComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/Jcf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2718178
    check-cast p1, LX/Jce;

    .line 2718179
    iget-object v0, p1, LX/Jce;->b:LX/1dc;

    iput-object v0, p0, LX/Jce;->b:LX/1dc;

    .line 2718180
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2718164
    if-ne p0, p1, :cond_1

    .line 2718165
    :cond_0
    :goto_0
    return v0

    .line 2718166
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2718167
    goto :goto_0

    .line 2718168
    :cond_3
    check-cast p1, LX/Jce;

    .line 2718169
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2718170
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2718171
    if-eq v2, v3, :cond_0

    .line 2718172
    iget-boolean v2, p0, LX/Jce;->a:Z

    iget-boolean v3, p1, LX/Jce;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2718173
    goto :goto_0

    .line 2718174
    :cond_4
    iget-object v2, p0, LX/Jce;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Jce;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v3, p1, LX/Jce;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2718175
    goto :goto_0

    .line 2718176
    :cond_5
    iget-object v2, p1, LX/Jce;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2718161
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Jce;

    .line 2718162
    const/4 v1, 0x0

    iput-object v1, v0, LX/Jce;->b:LX/1dc;

    .line 2718163
    return-object v0
.end method
