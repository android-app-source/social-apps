.class public LX/Jxf;
.super LX/0gG;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Jxl;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:LX/JxW;

.field public d:LX/JxV;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2753291
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2753292
    iput-object p1, p0, LX/Jxf;->b:Landroid/content/Context;

    .line 2753293
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Jxf;->a:Ljava/util/List;

    .line 2753294
    invoke-static {}, LX/Jxk;->values()[LX/Jxk;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2753295
    iget-object v4, p0, LX/Jxf;->a:Ljava/util/List;

    new-instance p1, LX/Jxl;

    invoke-direct {p1, v3}, LX/Jxl;-><init>(LX/Jxk;)V

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2753296
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2753297
    :cond_0
    return-void
.end method

.method public static a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V
    .locals 6

    .prologue
    .line 2753269
    const v0, 0x7f0d2854

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2753270
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2753271
    const v0, 0x7f0d2856

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2753272
    invoke-virtual {v0, p3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2753273
    invoke-virtual {v0, p4}, Landroid/view/ViewStub;->setInflatedId(I)V

    .line 2753274
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2753275
    const v0, 0x7f0d2852

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2753276
    const v0, 0x7f0d2855

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/fbui/glyph/GlyphButton;

    move-object v0, p0

    move v2, p3

    move-object v3, p5

    .line 2753277
    iget-object p0, v3, LX/Jxl;->b:LX/Jxk;

    move-object p0, p0

    .line 2753278
    sget-object p1, LX/Jxk;->RELEVANT:LX/Jxk;

    if-eq p0, p1, :cond_3

    .line 2753279
    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2753280
    :goto_0
    new-instance p0, LX/Jxa;

    invoke-direct {p0, v0}, LX/Jxa;-><init>(LX/Jxf;)V

    invoke-virtual {v4, p0}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2753281
    new-instance p0, LX/Jxb;

    invoke-direct {p0, v0}, LX/Jxb;-><init>(LX/Jxf;)V

    invoke-virtual {v5, p0}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2753282
    const p0, 0x7f0314a1

    if-eq v2, p0, :cond_0

    const p0, 0x7f03066e

    if-ne v2, p0, :cond_2

    .line 2753283
    :cond_0
    check-cast v1, Landroid/widget/RadioGroup;

    .line 2753284
    new-instance p0, LX/Jxc;

    invoke-direct {p0, v0, v3, v5}, LX/Jxc;-><init>(LX/Jxf;LX/Jxl;Lcom/facebook/fbui/glyph/GlyphButton;)V

    invoke-virtual {v1, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2753285
    :cond_1
    :goto_1
    return-void

    .line 2753286
    :cond_2
    const p0, 0x7f030599

    if-ne v2, p0, :cond_1

    .line 2753287
    const p0, 0x7f0d0f61

    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/fig/textinput/FigEditText;

    .line 2753288
    const p1, 0x7f0d0f62

    invoke-virtual {v1, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    .line 2753289
    new-instance p2, LX/Jxd;

    invoke-direct {p2, v0, v3, p0}, LX/Jxd;-><init>(LX/Jxf;LX/Jxl;Lcom/facebook/fig/textinput/FigEditText;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2753290
    :cond_3
    const/4 p0, 0x4

    invoke-virtual {v4, p0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2753246
    iget-object v0, p0, LX/Jxf;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jxl;

    .line 2753247
    const/4 v7, 0x0

    .line 2753248
    iget-object v1, p0, LX/Jxf;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2753249
    const v2, 0x7f0310f7

    invoke-virtual {v1, v2, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 2753250
    const v1, 0x7f0d2853

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2753251
    iget-object v3, p0, LX/Jxf;->b:Landroid/content/Context;

    const v4, 0x7f083c12

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    iget-object v7, p0, LX/Jxf;->a:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2753252
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2753253
    iget-object v1, v0, LX/Jxl;->b:LX/Jxk;

    move-object v1, v1

    .line 2753254
    sget-object v3, LX/Jxe;->a:[I

    invoke-virtual {v1}, LX/Jxk;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 2753255
    :goto_0
    move-object v0, v2

    .line 2753256
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2753257
    return-object v0

    .line 2753258
    :pswitch_0
    const v3, 0x7f083c09

    const v4, 0x7f0314a1

    const v5, 0x7f0d2edb

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753259
    :pswitch_1
    const v3, 0x7f083c0a

    const v4, 0x7f0314a1

    const v5, 0x7f0d2edb

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753260
    :pswitch_2
    const v3, 0x7f083c0b

    const v4, 0x7f0314a1

    const v5, 0x7f0d2edb

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753261
    :pswitch_3
    const v3, 0x7f083c0c

    const v4, 0x7f0314a1

    const v5, 0x7f0d2edb

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753262
    :pswitch_4
    const v3, 0x7f083c0d

    const v4, 0x7f0314a1

    const v5, 0x7f0d2edb

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753263
    :pswitch_5
    const v3, 0x7f083c0e

    const v4, 0x7f03066e

    const v5, 0x7f0d11b0

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    .line 2753264
    :pswitch_6
    const v3, 0x7f083c0f

    const v4, 0x7f030599

    const v5, 0x7f0d0f60

    move-object v1, p0

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/Jxf;->a(LX/Jxf;Landroid/view/ViewGroup;IIILX/Jxl;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2753267
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2753268
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2753266
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2753265
    iget-object v0, p0, LX/Jxf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
