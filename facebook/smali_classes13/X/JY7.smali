.class public LX/JY7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:LX/3mF;

.field private final b:Ljava/util/concurrent/Executor;

.field public final c:LX/3mG;

.field public final d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final e:LX/17Q;

.field public final f:LX/0Zb;

.field public final g:LX/1nA;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:LX/17Y;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3mF;Ljava/util/concurrent/Executor;LX/3mG;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/17Q;LX/0Zb;LX/1nA;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3mF;",
            "Ljava/util/concurrent/Executor;",
            "LX/3mG;",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/1nA;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2705813
    iput-object p1, p0, LX/JY7;->a:LX/3mF;

    .line 2705814
    iput-object p2, p0, LX/JY7;->b:Ljava/util/concurrent/Executor;

    .line 2705815
    iput-object p3, p0, LX/JY7;->c:LX/3mG;

    .line 2705816
    iput-object p4, p0, LX/JY7;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2705817
    iput-object p5, p0, LX/JY7;->e:LX/17Q;

    .line 2705818
    iput-object p6, p0, LX/JY7;->f:LX/0Zb;

    .line 2705819
    iput-object p7, p0, LX/JY7;->g:LX/1nA;

    .line 2705820
    iput-object p9, p0, LX/JY7;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2705821
    iput-object p8, p0, LX/JY7;->i:LX/17Y;

    .line 2705822
    iput-object p10, p0, LX/JY7;->j:LX/0Ot;

    .line 2705823
    iput-object p11, p0, LX/JY7;->k:LX/0Ot;

    .line 2705824
    return-void
.end method

.method public static a(LX/0QB;)LX/JY7;
    .locals 15

    .prologue
    .line 2705825
    const-class v1, LX/JY7;

    monitor-enter v1

    .line 2705826
    :try_start_0
    sget-object v0, LX/JY7;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705827
    sput-object v2, LX/JY7;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705828
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705829
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705830
    new-instance v3, LX/JY7;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v4

    check-cast v4, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/3mG;->b(LX/0QB;)LX/3mG;

    move-result-object v6

    check-cast v6, LX/3mG;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v7

    check-cast v7, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v10

    check-cast v10, LX/1nA;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v11

    check-cast v11, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    const/16 v13, 0x2eb

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1032

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/JY7;-><init>(LX/3mF;Ljava/util/concurrent/Executor;LX/3mG;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/17Q;LX/0Zb;LX/1nA;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;)V

    .line 2705831
    move-object v0, v3

    .line 2705832
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JY7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/JY7;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2705836
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    invoke-static {p3, v0}, LX/17Q;->c(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2705837
    iget-object v1, p0, LX/JY7;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2705838
    return-void
.end method


# virtual methods
.method public final a(LX/JY6;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;LX/1Pq;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JY6;",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2705839
    iget-boolean v0, p1, LX/JY6;->a:Z

    move v1, v0

    .line 2705840
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/JY7;->a:LX/3mF;

    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sgny"

    const-string v4, "ALLOW_READD"

    invoke-virtual {v0, v2, v3, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2705841
    :goto_0
    if-nez v1, :cond_0

    .line 2705842
    const-string v2, "sgny_join"

    invoke-static {p0, p3, p2, v2}, LX/JY7;->a(LX/JY7;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Ljava/lang/String;)V

    .line 2705843
    :cond_0
    new-instance v5, LX/JY3;

    move-object v6, p0

    move-object v7, p1

    move v8, v1

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v5 .. v10}, LX/JY3;-><init>(LX/JY7;LX/JY6;ZLcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;LX/1Pq;)V

    move-object v1, v5

    .line 2705844
    iget-object v2, p0, LX/JY7;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2705845
    return-void

    .line 2705846
    :cond_1
    iget-object v0, p0, LX/JY7;->a:LX/3mF;

    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sgny"

    invoke-virtual {v0, v2, v3}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
