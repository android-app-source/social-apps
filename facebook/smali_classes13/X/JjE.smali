.class public final LX/JjE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbEditText;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;Lcom/facebook/resources/ui/FbEditText;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2727436
    iput-object p1, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iput-object p2, p0, LX/JjE;->a:Lcom/facebook/resources/ui/FbEditText;

    iput-object p3, p0, LX/JjE;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12

    .prologue
    .line 2727437
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v1, p0, LX/JjE;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2727438
    iput-object v1, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    .line 2727439
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->u:LX/JjZ;

    if-eqz v0, :cond_0

    .line 2727440
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->u:LX/JjZ;

    iget-object v1, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    .line 2727441
    iget-object v2, v0, LX/JjZ;->a:LX/Jja;

    iget-object v2, v2, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    .line 2727442
    iput-object v1, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->D:Ljava/lang/String;

    .line 2727443
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2727444
    iget-object v2, v0, LX/JjZ;->a:LX/Jja;

    iget-object v2, v2, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    iget-object v3, v0, LX/JjZ;->a:LX/Jja;

    iget-object v3, v3, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082db9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setPlaceholderText(Ljava/lang/String;)V

    .line 2727445
    :cond_0
    :goto_0
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    .line 2727446
    :goto_1
    iget-object v1, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2727447
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JjT;

    iget-object v1, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->q:Ljava/lang/String;

    iget-object v2, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->t:Ljava/lang/String;

    iget-object v3, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v3, v3, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->p:Lcom/facebook/messaging/events/banner/EventReminderParams;

    new-instance v4, LX/JjD;

    invoke-direct {v4, p0}, LX/JjD;-><init>(LX/JjE;)V

    const/4 v8, 0x0

    .line 2727448
    move-object v5, v0

    move-object v6, v1

    move-object v7, v2

    move-object v9, v8

    move-object v10, v3

    move-object v11, v4

    invoke-static/range {v5 .. v11}, LX/JjT;->a(LX/JjT;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Lcom/facebook/messaging/events/banner/EventReminderParams;LX/Jj7;)V

    .line 2727449
    :cond_1
    return-void

    .line 2727450
    :cond_2
    iget-object v0, p0, LX/JjE;->c:Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/events/banner/EventReminderEditTitleDialogFragment;->s:Ljava/lang/String;

    goto :goto_1

    .line 2727451
    :cond_3
    iget-object v2, v0, LX/JjZ;->a:LX/Jja;

    iget-object v2, v2, LX/Jja;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->w:Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/events/banner/EventReminderSettingsRow;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
