.class public final LX/KAD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/KAE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/KAO;

.field public b:Ljava/lang/String;

.field public final synthetic c:LX/KAE;


# direct methods
.method public constructor <init>(LX/KAE;)V
    .locals 1

    .prologue
    .line 2779016
    iput-object p1, p0, LX/KAD;->c:LX/KAE;

    .line 2779017
    move-object v0, p1

    .line 2779018
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2779019
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2779020
    const-string v0, "EmptyFeedFindGroupsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2779021
    if-ne p0, p1, :cond_1

    .line 2779022
    :cond_0
    :goto_0
    return v0

    .line 2779023
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2779024
    goto :goto_0

    .line 2779025
    :cond_3
    check-cast p1, LX/KAD;

    .line 2779026
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2779027
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2779028
    if-eq v2, v3, :cond_0

    .line 2779029
    iget-object v2, p0, LX/KAD;->a:LX/KAO;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/KAD;->a:LX/KAO;

    iget-object v3, p1, LX/KAD;->a:LX/KAO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2779030
    goto :goto_0

    .line 2779031
    :cond_5
    iget-object v2, p1, LX/KAD;->a:LX/KAO;

    if-nez v2, :cond_4

    .line 2779032
    :cond_6
    iget-object v2, p0, LX/KAD;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/KAD;->b:Ljava/lang/String;

    iget-object v3, p1, LX/KAD;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2779033
    goto :goto_0

    .line 2779034
    :cond_7
    iget-object v2, p1, LX/KAD;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
