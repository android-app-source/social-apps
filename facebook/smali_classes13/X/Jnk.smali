.class public final LX/Jnk;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/Sticker;

.field public final synthetic b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/art/ArtItemView;Lcom/facebook/stickers/model/Sticker;)V
    .locals 0

    .prologue
    .line 2734377
    iput-object p1, p0, LX/Jnk;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iput-object p2, p0, LX/Jnk;->a:Lcom/facebook/stickers/model/Sticker;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2734374
    iget-object v0, p0, LX/Jnk;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734375
    :goto_0
    return-void

    .line 2734376
    :cond_0
    iget-object v0, p0, LX/Jnk;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->c()V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2734369
    invoke-direct {p0}, LX/Jnk;->b()V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2734370
    iget-object v0, p0, LX/Jnk;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    invoke-virtual {v0}, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734371
    :goto_0
    return-void

    .line 2734372
    :cond_0
    sget-object v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->a:Ljava/lang/String;

    const-string v1, "Failed to load sticker: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Jnk;->a:Lcom/facebook/stickers/model/Sticker;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2734373
    iget-object v0, p0, LX/Jnk;->b:Lcom/facebook/messaging/montage/composer/art/ArtItemView;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/art/ArtItemView;->f:LX/IqH;

    invoke-virtual {v0}, LX/IqH;->c()V

    goto :goto_0
.end method
