.class public LX/Jf9;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/Jey;


# instance fields
.field private a:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/Jf7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2721541
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721542
    invoke-direct {p0}, LX/Jf9;->a()V

    .line 2721543
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721538
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721539
    invoke-direct {p0}, LX/Jf9;->a()V

    .line 2721540
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2721518
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2721519
    invoke-direct {p0}, LX/Jf9;->a()V

    .line 2721520
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2721534
    const-class v0, LX/Jf9;

    invoke-static {v0, p0}, LX/Jf9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2721535
    const v0, 0x7f030af6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2721536
    invoke-direct {p0}, LX/Jf9;->b()V

    .line 2721537
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Jf9;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Jf9;

    new-instance p1, LX/Jf7;

    invoke-direct {p1}, LX/Jf7;-><init>()V

    invoke-static {v0}, LX/Jez;->a(LX/0QB;)LX/Jez;

    move-result-object v1

    check-cast v1, LX/Jez;

    iput-object v1, p1, LX/Jf7;->d:LX/Jez;

    move-object v0, p1

    check-cast v0, LX/Jf7;

    iput-object v0, p0, LX/Jf9;->b:LX/Jf7;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2721530
    const v0, 0x7f0d1bba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, LX/Jf9;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2721531
    iget-object v0, p0, LX/Jf9;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/Jf9;->b:LX/Jf7;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2721532
    iget-object v0, p0, LX/Jf9;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/Jf8;

    invoke-direct {v1, p0}, LX/Jf8;-><init>(LX/Jf9;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2721533
    return-void
.end method


# virtual methods
.method public getInboxUnitItem()Lcom/facebook/messaging/inbox2/items/InboxUnitItem;
    .locals 1

    .prologue
    .line 2721529
    iget-object v0, p0, LX/Jf9;->a:Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdUnit;

    return-object v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2721528
    iget-object v0, p0, LX/Jf9;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2721527
    iget-object v0, p0, LX/Jf9;->b:LX/Jf7;

    return-object v0
.end method

.method public setFragmentManager(LX/0gc;)V
    .locals 1

    .prologue
    .line 2721524
    iget-object v0, p0, LX/Jf9;->b:LX/Jf7;

    .line 2721525
    iput-object p1, v0, LX/Jf7;->c:LX/0gc;

    .line 2721526
    return-void
.end method

.method public setListener(LX/Jex;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jex",
            "<",
            "Lcom/facebook/messaging/business/inboxads/hscroll/MessengerInboxHScrollAdItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2721521
    iget-object v0, p0, LX/Jf9;->b:LX/Jf7;

    .line 2721522
    iput-object p1, v0, LX/Jf7;->b:LX/Jex;

    .line 2721523
    return-void
.end method
