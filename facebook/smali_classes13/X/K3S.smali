.class public final LX/K3S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;)V
    .locals 0

    .prologue
    .line 2765742
    iput-object p1, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 2765743
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2765744
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-boolean v0, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->h:Z

    if-nez v0, :cond_0

    .line 2765745
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2765746
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2765747
    :pswitch_0
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    const-wide v2, 0x3fee666666666666L    # 0.95

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 2765748
    :pswitch_1
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-boolean v0, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->g:Z

    if-eqz v0, :cond_1

    .line 2765749
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->callOnClick()Z

    .line 2765750
    :cond_1
    iget-object v0, p0, LX/K3S;->a:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-object v0, v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
