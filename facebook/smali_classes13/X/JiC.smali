.class public final LX/JiC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/JiJ;


# direct methods
.method public constructor <init>(LX/JiJ;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2725733
    iput-object p1, p0, LX/JiC;->b:LX/JiJ;

    iput-object p2, p0, LX/JiC;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x474e8f32

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2725734
    iget-object v0, p0, LX/JiC;->b:LX/JiJ;

    const-string v2, "contact_upload_upsell_start"

    invoke-static {v0, v2}, LX/JiJ;->a$redex0(LX/JiJ;Ljava/lang/String;)V

    .line 2725735
    iget-object v0, p0, LX/JiC;->b:LX/JiJ;

    iget-object v0, v0, LX/JiJ;->j:LX/Jq2;

    .line 2725736
    iget-object v2, v0, LX/Jq2;->a:LX/0Uh;

    const/16 v3, 0x1e1

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 2725737
    if-eqz v0, :cond_0

    .line 2725738
    iget-object v0, p0, LX/JiC;->b:LX/JiJ;

    iget-object v0, v0, LX/JiJ;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jq0;

    const-string v2, "people_tab_upload_upsell"

    .line 2725739
    iget-object v3, v0, LX/Jq0;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "migrator_nux_open"

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "source"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2725740
    iget-object v0, p0, LX/JiC;->b:LX/JiJ;

    iget-object v0, v0, LX/JiJ;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    sget-object v2, LX/Jpv;->NUX_UPLOAD_FLOW:LX/Jpv;

    iget-object v3, p0, LX/JiC;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/Jpv;->generateIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, LX/JiC;->a:Landroid/content/Context;

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2725741
    :cond_0
    const v0, 0x5294ff2a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void
.end method
