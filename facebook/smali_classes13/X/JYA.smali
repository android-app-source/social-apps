.class public LX/JYA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYB;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYA",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705933
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2705934
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYA;->b:LX/0Zi;

    .line 2705935
    iput-object p1, p0, LX/JYA;->a:LX/0Ot;

    .line 2705936
    return-void
.end method

.method public static a(LX/0QB;)LX/JYA;
    .locals 4

    .prologue
    .line 2705922
    const-class v1, LX/JYA;

    monitor-enter v1

    .line 2705923
    :try_start_0
    sget-object v0, LX/JYA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2705924
    sput-object v2, LX/JYA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2705925
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2705926
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2705927
    new-instance v3, LX/JYA;

    const/16 p0, 0x2170

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYA;-><init>(LX/0Ot;)V

    .line 2705928
    move-object v0, v3

    .line 2705929
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2705930
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705931
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2705932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2705937
    const v0, -0x2cbba0f6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2705891
    check-cast p2, LX/JY8;

    .line 2705892
    iget-object v0, p0, LX/JYA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JY8;->a:LX/JY4;

    iget-object v1, p2, LX/JY8;->b:LX/JY6;

    const/4 p2, 0x3

    const/4 p0, 0x0

    const/4 v6, 0x2

    .line 2705893
    iget-object v2, v0, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    .line 2705894
    const/4 v3, 0x0

    .line 2705895
    invoke-static {v2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    .line 2705896
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v4, v5, :cond_0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v4, v5, :cond_1

    .line 2705897
    :cond_0
    const/4 v3, 0x1

    .line 2705898
    :cond_1
    iget-boolean v4, v1, LX/JY6;->a:Z

    move v4, v4

    .line 2705899
    if-eqz v4, :cond_5

    .line 2705900
    if-eqz v3, :cond_4

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2705901
    :goto_0
    move-object v2, v3

    .line 2705902
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v3, :cond_3

    .line 2705903
    :cond_2
    const v3, 0x7f020a52

    .line 2705904
    const v2, 0x7f081b98

    .line 2705905
    :goto_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0937

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0936

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v3

    .line 2705906
    const v4, -0x2cbba0f6

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2705907
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, p0, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2705908
    return-object v0

    .line 2705909
    :cond_3
    const v3, 0x7f020ad5

    .line 2705910
    const v2, 0x7f081b99

    goto :goto_1

    .line 2705911
    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0

    .line 2705912
    :cond_5
    if-eqz v3, :cond_6

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2705913
    invoke-static {}, LX/1dS;->b()V

    .line 2705914
    iget v0, p1, LX/1dQ;->b:I

    .line 2705915
    packed-switch v0, :pswitch_data_0

    .line 2705916
    :goto_0
    return-object v1

    .line 2705917
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2705918
    check-cast v0, LX/JY8;

    .line 2705919
    iget-object v2, p0, LX/JYA;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JYB;

    iget-object v3, v0, LX/JY8;->a:LX/JY4;

    iget-object v4, v0, LX/JY8;->b:LX/JY6;

    iget-object p1, v0, LX/JY8;->c:LX/1Pq;

    .line 2705920
    iget-object p2, v2, LX/JYB;->a:LX/JY7;

    iget-object p0, v3, LX/JY4;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    iget-object v0, v3, LX/JY4;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {p2, v4, p0, v0, p1}, LX/JY7;->a(LX/JY6;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;LX/1Pq;)V

    .line 2705921
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2cbba0f6
        :pswitch_0
    .end packed-switch
.end method
