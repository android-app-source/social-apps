.class public LX/Jzi;
.super Landroid/app/DialogFragment;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# instance fields
.field public a:Landroid/app/DatePickerDialog$OnDateSetListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/content/DialogInterface$OnDismissListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2756571
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 2756572
    return-void
.end method

.method public static a(Landroid/os/Bundle;Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;)Landroid/app/Dialog;
    .locals 13
    .param p2    # Landroid/app/DatePickerDialog$OnDateSetListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v12, 0xd

    const/16 v11, 0xc

    const/16 v10, 0xb

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2756573
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 2756574
    if-eqz p0, :cond_0

    const-string v0, "date"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756575
    const-string v0, "date"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2756576
    :cond_0
    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 2756577
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 2756578
    const/4 v0, 0x5

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 2756579
    sget-object v0, LX/Jzl;->DEFAULT:LX/Jzl;

    .line 2756580
    if-eqz p0, :cond_1

    const-string v1, "mode"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2756581
    const-string v0, "mode"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Jzl;->valueOf(Ljava/lang/String;)LX/Jzl;

    move-result-object v0

    .line 2756582
    :cond_1
    const/4 v1, 0x0

    .line 2756583
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_3

    .line 2756584
    sget-object v2, LX/Jzh;->a:[I

    invoke-virtual {v0}, LX/Jzl;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2756585
    :goto_0
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    .line 2756586
    if-eqz p0, :cond_4

    const-string v2, "minDate"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2756587
    const-string v2, "minDate"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2756588
    invoke-virtual {v7, v10, v9}, Ljava/util/Calendar;->set(II)V

    .line 2756589
    invoke-virtual {v7, v11, v9}, Ljava/util/Calendar;->set(II)V

    .line 2756590
    invoke-virtual {v7, v12, v9}, Ljava/util/Calendar;->set(II)V

    .line 2756591
    const/16 v2, 0xe

    invoke-virtual {v7, v2, v9}, Ljava/util/Calendar;->set(II)V

    .line 2756592
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 2756593
    :goto_1
    if-eqz p0, :cond_2

    const-string v2, "maxDate"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2756594
    const-string v2, "maxDate"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2756595
    const/16 v2, 0x17

    invoke-virtual {v7, v10, v2}, Ljava/util/Calendar;->set(II)V

    .line 2756596
    const/16 v2, 0x3b

    invoke-virtual {v7, v11, v2}, Ljava/util/Calendar;->set(II)V

    .line 2756597
    const/16 v2, 0x3b

    invoke-virtual {v7, v12, v2}, Ljava/util/Calendar;->set(II)V

    .line 2756598
    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v7, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 2756599
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 2756600
    :cond_2
    return-object v1

    .line 2756601
    :pswitch_0
    new-instance v0, LX/Jzm;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "CalendarDatePickerDialog"

    const-string v3, "style"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v2, v3, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, LX/Jzm;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    move-object v1, v0

    .line 2756602
    goto :goto_0

    .line 2756603
    :pswitch_1
    new-instance v0, LX/Jzm;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "SpinnerDatePickerDialog"

    const-string v3, "style"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v2, v3, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, LX/Jzm;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    move-object v1, v0

    .line 2756604
    goto/16 :goto_0

    .line 2756605
    :pswitch_2
    new-instance v1, LX/Jzm;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/Jzm;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    goto/16 :goto_0

    .line 2756606
    :cond_3
    new-instance v1, LX/Jzm;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/Jzm;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2756607
    sget-object v2, LX/Jzh;->a:[I

    invoke-virtual {v0}, LX/Jzl;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 2756608
    :pswitch_3
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 2756609
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    goto/16 :goto_0

    .line 2756610
    :pswitch_4
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    goto/16 :goto_0

    .line 2756611
    :cond_4
    const-wide v2, -0x20251fe2401L

    invoke-virtual {v0, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2756612
    invoke-virtual {p0}, LX/Jzi;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 2756613
    invoke-virtual {p0}, LX/Jzi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, LX/Jzi;->a:Landroid/app/DatePickerDialog$OnDateSetListener;

    invoke-static {v0, v1, v2}, LX/Jzi;->a(Landroid/os/Bundle;Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2756614
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2756615
    iget-object v0, p0, LX/Jzi;->b:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 2756616
    iget-object v0, p0, LX/Jzi;->b:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2756617
    :cond_0
    return-void
.end method
