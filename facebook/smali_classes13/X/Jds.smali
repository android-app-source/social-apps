.class public LX/Jds;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/JeN;


# direct methods
.method public constructor <init>(LX/JeN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720167
    iput-object p1, p0, LX/Jds;->a:LX/JeN;

    .line 2720168
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 3

    .prologue
    .line 2720169
    iget-object v0, p0, LX/Jds;->a:LX/JeN;

    .line 2720170
    iget-object v1, v0, LX/JeN;->a:LX/0Uh;

    const/16 v2, 0x18f

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2720171
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;Z)LX/0Px;
    .locals 7
    .param p2    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/model/User;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;",
            "Z)",
            "LX/0Px",
            "<",
            "LX/JeZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2720172
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2720173
    iget-boolean v1, p1, Lcom/facebook/user/model/User;->J:Z

    move v1, v1

    .line 2720174
    if-eqz v1, :cond_0

    .line 2720175
    new-instance v1, LX/Jei;

    invoke-direct {v1, p1}, LX/Jei;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720176
    new-instance v1, LX/Jec;

    invoke-direct {v1}, LX/Jec;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720177
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2720178
    :goto_0
    return-object v0

    .line 2720179
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->P()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2720180
    new-instance v1, LX/Jea;

    invoke-direct {v1, p1}, LX/Jea;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720181
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2720182
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2720183
    invoke-virtual {p3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$ContentSubscriptionTopicsQueryModel;->j()LX/0Px;

    move-result-object v1

    const/4 v4, 0x0

    .line 2720184
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2720185
    if-nez v1, :cond_6

    move-object v2, v6

    .line 2720186
    :goto_1
    move-object v1, v2

    .line 2720187
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2720188
    new-instance v2, LX/Jeb;

    invoke-direct {v2}, LX/Jeb;-><init>()V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720189
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2720190
    if-nez p4, :cond_5

    invoke-direct {p0, p2}, LX/Jds;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2720191
    new-instance v1, LX/Jed;

    invoke-direct {v1}, LX/Jed;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720192
    :cond_2
    :goto_2
    if-nez p4, :cond_4

    .line 2720193
    invoke-direct {p0, p2}, LX/Jds;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2720194
    new-instance v1, LX/Jee;

    invoke-direct {v1, p2}, LX/Jee;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720195
    new-instance v1, LX/Jec;

    invoke-direct {v1}, LX/Jec;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720196
    :cond_3
    new-instance v1, LX/Jea;

    invoke-direct {v1, p1}, LX/Jea;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720197
    new-instance v1, LX/Jec;

    invoke-direct {v1}, LX/Jec;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2720198
    :cond_4
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2720199
    :cond_5
    new-instance v1, LX/Jec;

    invoke-direct {v1}, LX/Jec;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    :cond_6
    move v3, v4

    .line 2720200
    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_a

    .line 2720201
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    .line 2720202
    invoke-virtual {v2}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->l()Z

    move-result v5

    if-nez v5, :cond_9

    .line 2720203
    if-nez v3, :cond_8

    const/4 v5, 0x1

    .line 2720204
    :goto_4
    new-instance p3, LX/Jef;

    invoke-direct {p3, v2, v5}, LX/Jef;-><init>(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;Z)V

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2720205
    :cond_7
    :goto_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    :cond_8
    move v5, v4

    .line 2720206
    goto :goto_4

    .line 2720207
    :cond_9
    iget-object v5, p0, LX/Jds;->a:LX/JeN;

    invoke-virtual {v5}, LX/JeN;->a()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2720208
    new-instance v5, LX/Jeg;

    invoke-direct {v5, v2}, LX/Jeg;-><init>(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_a
    move-object v2, v6

    .line 2720209
    goto/16 :goto_1
.end method
