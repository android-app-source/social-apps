.class public final LX/Jjc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;)V
    .locals 0

    .prologue
    .line 2727983
    iput-object p1, p0, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x6b413eb4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2727984
    iget-object v1, p0, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v1, v1, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->s:Lcom/facebook/messaging/events/banner/EventReminderParams;

    invoke-static {v1}, Lcom/facebook/messaging/events/banner/EventReminderParams;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)LX/JjW;

    move-result-object v1

    iget-object v2, p0, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->p:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2727985
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2727986
    iput-object v2, v1, LX/JjW;->h:Ljava/lang/String;

    .line 2727987
    move-object v1, v1

    .line 2727988
    iget-object v2, p0, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    iget-object v2, v2, Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;->C:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 2727989
    iput-wide v2, v1, LX/JjW;->c:J

    .line 2727990
    move-object v1, v1

    .line 2727991
    invoke-virtual {v1}, LX/JjW;->a()Lcom/facebook/messaging/events/banner/EventReminderParams;

    move-result-object v1

    .line 2727992
    invoke-static {v1}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->a(Lcom/facebook/messaging/events/banner/EventReminderParams;)Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;

    move-result-object v1

    .line 2727993
    new-instance v2, LX/Jjb;

    invoke-direct {v2, p0}, LX/Jjb;-><init>(LX/Jjc;)V

    .line 2727994
    iput-object v2, v1, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->v:LX/Jjb;

    .line 2727995
    iget-object v2, p0, LX/Jjc;->a:Lcom/facebook/messaging/events/banner/EventReminderSettingsActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "edit_event_reminder_time"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/messaging/events/banner/EventReminderEditTimeDialogFragment;->b(LX/0gc;Ljava/lang/String;)V

    .line 2727996
    const v1, 0x491a841b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
