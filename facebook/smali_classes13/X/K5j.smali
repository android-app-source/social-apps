.class public LX/K5j;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2770665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z
    .locals 2

    .prologue
    .line 2770650
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2770651
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1OS;

    if-nez v0, :cond_2

    .line 2770652
    const/4 v0, 0x0

    .line 2770653
    :goto_0
    move v0, v0

    .line 2770654
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 2770655
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2770656
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1OS;

    .line 2770657
    invoke-interface {v0}, LX/1OS;->n()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2770658
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2770659
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    instance-of v1, v1, LX/1OS;

    if-nez v1, :cond_2

    .line 2770660
    const/4 v1, 0x0

    .line 2770661
    :goto_0
    move v1, v1

    .line 2770662
    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    .line 2770663
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/1OS;

    .line 2770664
    invoke-interface {v1}, LX/1OS;->I()I

    move-result v1

    goto :goto_0
.end method
