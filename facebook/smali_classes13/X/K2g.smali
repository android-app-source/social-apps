.class public final LX/K2g;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/optional/impl/UFIViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V
    .locals 0

    .prologue
    .line 2764264
    iput-object p1, p0, LX/K2g;->a:Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2764265
    iget-object v1, p0, LX/K2g;->a:Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    .line 2764266
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2764267
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setFeedback(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2764268
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2764269
    iget-object v0, p0, LX/K2g;->a:Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->h:LX/03V;

    sget-object v1, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->m:Ljava/lang/String;

    const-string v2, "Fetching UFI failed"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2764270
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2764271
    move-object v1, v1

    .line 2764272
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2764273
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2764274
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/K2g;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
