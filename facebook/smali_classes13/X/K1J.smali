.class public final LX/K1J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K1I;


# instance fields
.field public final synthetic a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

.field private b:LX/K19;

.field private c:LX/5s9;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/K19;)V
    .locals 2

    .prologue
    .line 2761698
    iput-object p1, p0, LX/K1J;->a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2761699
    iput-object p2, p0, LX/K1J;->b:LX/K19;

    .line 2761700
    invoke-virtual {p2}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2761701
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2761702
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2761703
    iput-object v0, p0, LX/K1J;->c:LX/5s9;

    .line 2761704
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 2761693
    iget v0, p0, LX/K1J;->d:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/K1J;->e:I

    if-eq v0, p2, :cond_1

    .line 2761694
    :cond_0
    iget-object v0, p0, LX/K1J;->c:LX/5s9;

    new-instance v1, LX/K1L;

    iget-object v2, p0, LX/K1J;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getId()I

    move-result v2

    invoke-direct {v1, v2, p1, p2}, LX/K1L;-><init>(III)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2761695
    iput p1, p0, LX/K1J;->d:I

    .line 2761696
    iput p2, p0, LX/K1J;->e:I

    .line 2761697
    :cond_1
    return-void
.end method
