.class public LX/K1l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/K1l;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762500
    return-void
.end method

.method public static a(LX/0QB;)LX/K1l;
    .locals 3

    .prologue
    .line 2762487
    sget-object v0, LX/K1l;->a:LX/K1l;

    if-nez v0, :cond_1

    .line 2762488
    const-class v1, LX/K1l;

    monitor-enter v1

    .line 2762489
    :try_start_0
    sget-object v0, LX/K1l;->a:LX/K1l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2762490
    if-eqz v2, :cond_0

    .line 2762491
    :try_start_1
    new-instance v0, LX/K1l;

    invoke-direct {v0}, LX/K1l;-><init>()V

    .line 2762492
    move-object v0, v0

    .line 2762493
    sput-object v0, LX/K1l;->a:LX/K1l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2762494
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2762495
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2762496
    :cond_1
    sget-object v0, LX/K1l;->a:LX/K1l;

    return-object v0

    .line 2762497
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2762498
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(LX/0ed;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 2762479
    iget-object v0, p0, LX/0ed;->a:Landroid/content/Context;

    move-object v0, v0

    .line 2762480
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 2762481
    invoke-virtual {p0}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v1

    .line 2762482
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "strings/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".fbstr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2762483
    :goto_0
    move-object v0, v2

    .line 2762484
    if-nez v0, :cond_0

    .line 2762485
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load language asset for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2762486
    :cond_0
    return-object v0

    :catch_0
    const/4 v2, 0x0

    goto :goto_0
.end method
