.class public LX/Jnx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Jnx;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jnz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AMO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jo3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DhA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2734698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734699
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734700
    iput-object v0, p0, LX/Jnx;->a:LX/0Ot;

    .line 2734701
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734702
    iput-object v0, p0, LX/Jnx;->b:LX/0Ot;

    .line 2734703
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734704
    iput-object v0, p0, LX/Jnx;->c:LX/0Ot;

    .line 2734705
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734706
    iput-object v0, p0, LX/Jnx;->d:LX/0Ot;

    .line 2734707
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734708
    iput-object v0, p0, LX/Jnx;->e:LX/0Ot;

    .line 2734709
    return-void
.end method

.method public static a(LX/0QB;)LX/Jnx;
    .locals 8

    .prologue
    .line 2734710
    sget-object v0, LX/Jnx;->f:LX/Jnx;

    if-nez v0, :cond_1

    .line 2734711
    const-class v1, LX/Jnx;

    monitor-enter v1

    .line 2734712
    :try_start_0
    sget-object v0, LX/Jnx;->f:LX/Jnx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2734713
    if-eqz v2, :cond_0

    .line 2734714
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2734715
    new-instance v3, LX/Jnx;

    invoke-direct {v3}, LX/Jnx;-><init>()V

    .line 2734716
    const/16 v4, 0x2835

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x187f

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2836

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2837

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x2838

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2734717
    iput-object v4, v3, LX/Jnx;->a:LX/0Ot;

    iput-object v5, v3, LX/Jnx;->b:LX/0Ot;

    iput-object v6, v3, LX/Jnx;->c:LX/0Ot;

    iput-object v7, v3, LX/Jnx;->d:LX/0Ot;

    iput-object p0, v3, LX/Jnx;->e:LX/0Ot;

    .line 2734718
    move-object v0, v3

    .line 2734719
    sput-object v0, LX/Jnx;->f:LX/Jnx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2734720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2734721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2734722
    :cond_1
    sget-object v0, LX/Jnx;->f:LX/Jnx;

    return-object v0

    .line 2734723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2734724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;
    .locals 2

    .prologue
    .line 2734725
    monitor-enter p0

    .line 2734726
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2734727
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2734728
    sget-object v0, LX/Jnw;->COMPLETED:LX/Jnw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734729
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2734730
    :cond_0
    :try_start_1
    sget-object v0, LX/Jnv;->a:[I

    .line 2734731
    iget-object v1, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->e:LX/Dhg;

    move-object v1, v1

    .line 2734732
    invoke-virtual {v1}, LX/Dhg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2734733
    sget-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;

    goto :goto_0

    .line 2734734
    :pswitch_0
    iget-object v0, p0, LX/Jnx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jnz;

    invoke-virtual {v0, p1}, LX/Jnz;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v0

    goto :goto_0

    .line 2734735
    :pswitch_1
    iget-object v0, p0, LX/Jnx;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/montage/composer/art/ParticleAssetManager;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;

    move-result-object v0

    goto :goto_0

    .line 2734736
    :pswitch_2
    iget-object v0, p0, LX/Jnx;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jo3;

    invoke-virtual {v0, p1}, LX/Jo3;->a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2734737
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
