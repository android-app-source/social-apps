.class public final LX/Jdh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jdl;


# direct methods
.method public constructor <init>(LX/Jdl;)V
    .locals 0

    .prologue
    .line 2719911
    iput-object p1, p0, LX/Jdh;->a:LX/Jdl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x537d588f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2719905
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2719906
    const-string v2, "https://m.facebook.com/privacy/touch/block/confirm?bid=%s"

    iget-object v3, p0, LX/Jdh;->a:LX/Jdl;

    iget-object v3, v3, LX/Jdl;->b:Lcom/facebook/user/model/User;

    .line 2719907
    iget-object v5, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v5

    .line 2719908
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2719909
    iget-object v2, p0, LX/Jdh;->a:LX/Jdl;

    iget-object v2, v2, LX/Jdl;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2719910
    const v1, -0x6aeb3a94

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
