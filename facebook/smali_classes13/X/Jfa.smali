.class public final LX/Jfa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/review/graphql/ReviewMutationFragmentsModels$MessengerPlatformBotReviewMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jfe;

.field public final synthetic b:LX/Jfc;


# direct methods
.method public constructor <init>(LX/Jfc;LX/Jfe;)V
    .locals 0

    .prologue
    .line 2722088
    iput-object p1, p0, LX/Jfa;->b:LX/Jfc;

    iput-object p2, p0, LX/Jfa;->a:LX/Jfe;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2722089
    iget-object v0, p0, LX/Jfa;->a:LX/Jfe;

    .line 2722090
    iget-object v1, v0, LX/Jfe;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-static {v1}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722091
    iget-object v1, v0, LX/Jfe;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->b:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    .line 2722092
    iget-object v0, p0, LX/Jfa;->b:LX/Jfc;

    iget-object v0, v0, LX/Jfc;->a:LX/03V;

    const-string v1, "ReviewTaskManager"

    const-string v2, "Messenger platform bot review mutation fails."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722093
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2722094
    iget-object v0, p0, LX/Jfa;->a:LX/Jfe;

    .line 2722095
    iget-object p0, v0, LX/Jfe;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-static {p0}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722096
    iget-object p0, v0, LX/Jfe;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 2722097
    return-void
.end method
