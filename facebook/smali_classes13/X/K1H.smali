.class public final LX/K1H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K15;


# instance fields
.field public final synthetic a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

.field private b:LX/K19;

.field private c:LX/5s9;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/K19;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2761674
    iput-object p1, p0, LX/K1H;->a:Lcom/facebook/react/views/textinput/ReactTextInputManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2761675
    iput v0, p0, LX/K1H;->d:I

    .line 2761676
    iput v0, p0, LX/K1H;->e:I

    .line 2761677
    iput-object p2, p0, LX/K1H;->b:LX/K19;

    .line 2761678
    invoke-virtual {p2}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2761679
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2761680
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2761681
    iput-object v0, p0, LX/K1H;->c:LX/5s9;

    .line 2761682
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2761683
    iget-object v0, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v0}, LX/K19;->getWidth()I

    move-result v1

    .line 2761684
    iget-object v0, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v0}, LX/K19;->getHeight()I

    move-result v0

    .line 2761685
    iget-object v2, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2761686
    iget-object v0, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v0}, LX/K19;->getCompoundPaddingLeft()I

    move-result v0

    iget-object v1, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v1}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v1}, LX/K19;->getCompoundPaddingRight()I

    move-result v1

    add-int/2addr v1, v0

    .line 2761687
    iget-object v0, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v0}, LX/K19;->getCompoundPaddingTop()I

    move-result v0

    iget-object v2, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v2}, LX/K19;->getCompoundPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 2761688
    :cond_0
    iget v2, p0, LX/K1H;->d:I

    if-ne v1, v2, :cond_1

    iget v2, p0, LX/K1H;->e:I

    if-eq v0, v2, :cond_2

    .line 2761689
    :cond_1
    iput v0, p0, LX/K1H;->e:I

    .line 2761690
    iput v1, p0, LX/K1H;->d:I

    .line 2761691
    iget-object v2, p0, LX/K1H;->c:LX/5s9;

    new-instance v3, LX/K16;

    iget-object v4, p0, LX/K1H;->b:LX/K19;

    invoke-virtual {v4}, LX/K19;->getId()I

    move-result v4

    int-to-float v1, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    invoke-direct {v3, v4, v1, v0}, LX/K16;-><init>(IFF)V

    invoke-virtual {v2, v3}, LX/5s9;->a(LX/5r0;)V

    .line 2761692
    :cond_2
    return-void
.end method
