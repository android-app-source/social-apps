.class public final LX/KAL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/KAM;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2779298
    invoke-static {}, LX/KAM;->q()LX/KAM;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2779299
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2779300
    const-string v0, "FindGroupsCreateButton"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2779301
    if-ne p0, p1, :cond_1

    .line 2779302
    :cond_0
    :goto_0
    return v0

    .line 2779303
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2779304
    goto :goto_0

    .line 2779305
    :cond_3
    check-cast p1, LX/KAL;

    .line 2779306
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2779307
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2779308
    if-eq v2, v3, :cond_0

    .line 2779309
    iget-object v2, p0, LX/KAL;->a:LX/1dQ;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/KAL;->a:LX/1dQ;

    iget-object v3, p1, LX/KAL;->a:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2779310
    goto :goto_0

    .line 2779311
    :cond_4
    iget-object v2, p1, LX/KAL;->a:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
