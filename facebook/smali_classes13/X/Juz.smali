.class public final LX/Juz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/Jv4;


# direct methods
.method public constructor <init>(LX/Jv4;)V
    .locals 0

    .prologue
    .line 2749585
    iput-object p1, p0, LX/Juz;->a:LX/Jv4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2749586
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2749587
    if-eqz v0, :cond_0

    .line 2749588
    iget-object v0, p0, LX/Juz;->a:LX/Jv4;

    iget-object v1, p0, LX/Juz;->a:LX/Jv4;

    iget-object v1, v1, LX/Jv4;->m:LX/4ok;

    invoke-static {v0, v2, v1}, LX/Jv4;->b$redex0(LX/Jv4;ZLX/4ok;)V

    .line 2749589
    iget-object v0, p0, LX/Juz;->a:LX/Jv4;

    iget-object v0, v0, LX/Jv4;->m:LX/4ok;

    invoke-virtual {v0, v2}, LX/4ok;->setChecked(Z)V

    .line 2749590
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2749591
    :cond_0
    iget-object v0, p0, LX/Juz;->a:LX/Jv4;

    const/4 v1, 0x0

    .line 2749592
    iget-object v2, v0, LX/Jv4;->o:LX/8DZ;

    .line 2749593
    iget-boolean v3, v2, LX/8DZ;->d:Z

    move v2, v3

    .line 2749594
    if-eqz v2, :cond_1

    .line 2749595
    iget-object v2, v0, LX/Jv4;->m:LX/4ok;

    invoke-static {v0, v1, v2}, LX/Jv4;->b$redex0(LX/Jv4;ZLX/4ok;)V

    .line 2749596
    const/4 v1, 0x1

    .line 2749597
    :goto_1
    move v0, v1

    .line 2749598
    goto :goto_0

    .line 2749599
    :cond_1
    const/4 p2, 0x0

    .line 2749600
    new-instance v2, LX/31Y;

    iget-object v3, v0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2749601
    const v3, 0x7f083afc

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    .line 2749602
    iget-object v3, v0, LX/Jv4;->b:Landroid/content/Context;

    const v4, 0x7f083afd

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    iget-object p1, v0, LX/Jv4;->u:Ljava/lang/String;

    aput-object p1, p0, p2

    invoke-virtual {v3, v4, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2749603
    const v3, 0x7f083af5

    new-instance v4, LX/Jv0;

    invoke-direct {v4, v0}, LX/Jv0;-><init>(LX/Jv4;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749604
    const/high16 v3, 0x1040000

    new-instance v4, LX/Jv1;

    invoke-direct {v4, v0}, LX/Jv1;-><init>(LX/Jv4;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749605
    invoke-virtual {v2, p2}, LX/0ju;->a(Z)LX/0ju;

    .line 2749606
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 2749607
    goto :goto_1
.end method
