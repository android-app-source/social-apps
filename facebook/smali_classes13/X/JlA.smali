.class public final LX/JlA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;)V
    .locals 0

    .prologue
    .line 2730874
    iput-object p1, p0, LX/JlA;->a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2730875
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 2730876
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2730877
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2730878
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v4, v2

    .line 2730879
    iget-object v0, p0, LX/JlA;->a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->aw:LX/0Tn;

    const-wide/16 v6, -0x1

    invoke-interface {v0, v2, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 2730880
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2730881
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2730882
    iget-wide v8, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_2

    .line 2730883
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/JlA;->a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jni;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 2730884
    iget-object v8, v1, LX/Jni;->c:LX/Dhj;

    .line 2730885
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v9

    move-object v9, v9

    .line 2730886
    invoke-virtual {v8, v9}, LX/Dhj;->a(Ljava/lang/String;)Z

    move-result v8

    move v0, v8

    .line 2730887
    if-nez v0, :cond_2

    .line 2730888
    :cond_0
    add-int/lit8 v0, v2, 0x1

    .line 2730889
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 2730890
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
