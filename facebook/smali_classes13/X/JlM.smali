.class public LX/JlM;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/JlI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

.field private d:LX/1P1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2731031
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2731032
    invoke-direct {p0}, LX/JlM;->a()V

    .line 2731033
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2731028
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2731029
    invoke-direct {p0}, LX/JlM;->a()V

    .line 2731030
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2731043
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2731044
    invoke-direct {p0}, LX/JlM;->a()V

    .line 2731045
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2731034
    const-class v0, LX/JlM;

    invoke-static {v0, p0}, LX/JlM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2731035
    const v0, 0x7f0308eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2731036
    const v0, 0x7f0d170c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2731037
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, LX/JlM;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2731038
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/JlM;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JlM;->d:LX/1P1;

    .line 2731039
    iget-object v0, p0, LX/JlM;->d:LX/1P1;

    invoke-virtual {v0, v2}, LX/1P1;->b(I)V

    .line 2731040
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JlM;->d:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2731041
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JlM;->a:LX/JlI;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2731042
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/JlM;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/JlM;

    new-instance p1, LX/JlI;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-direct {p1, v1}, LX/JlI;-><init>(Landroid/view/LayoutInflater;)V

    move-object v0, p1

    check-cast v0, LX/JlI;

    iput-object v0, p0, LX/JlM;->a:LX/JlI;

    return-void
.end method


# virtual methods
.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2731016
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2731015
    iget-object v0, p0, LX/JlM;->a:LX/JlI;

    return-object v0
.end method

.method public setData(Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;)V
    .locals 2

    .prologue
    .line 2731017
    iput-object p1, p0, LX/JlM;->c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    .line 2731018
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/JlM;->d:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2731019
    iget-object v0, p0, LX/JlM;->a:LX/JlI;

    iget-object v1, p0, LX/JlM;->c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    .line 2731020
    iput-object v1, v0, LX/JlI;->c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    .line 2731021
    iget-object p1, v0, LX/JlI;->c:Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;

    iget-object p1, p1, Lcom/facebook/messaging/inbox2/bymm/InboxBYMMViewData;->a:LX/0Px;

    iput-object p1, v0, LX/JlI;->e:LX/0Px;

    .line 2731022
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2731023
    iget-object v0, p0, LX/JlM;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->requestLayout()V

    .line 2731024
    return-void
.end method

.method public setListener(LX/JlK;)V
    .locals 1

    .prologue
    .line 2731025
    iget-object v0, p0, LX/JlM;->a:LX/JlI;

    .line 2731026
    iput-object p1, v0, LX/JlI;->d:LX/JlK;

    .line 2731027
    return-void
.end method
