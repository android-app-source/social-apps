.class public LX/Jrg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Jet;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2744395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2744396
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2744397
    iput-object v0, p0, LX/Jrg;->a:LX/0Ot;

    .line 2744398
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2744399
    iput-object v0, p0, LX/Jrg;->b:LX/0Ot;

    .line 2744400
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2744401
    iput-object v0, p0, LX/Jrg;->c:LX/0Ot;

    .line 2744402
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2744403
    iput-object v0, p0, LX/Jrg;->d:LX/0Ot;

    .line 2744404
    return-void
.end method

.method private static a(LX/0Xu;)LX/0Xu;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)",
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2744390
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v2

    .line 2744391
    invoke-interface {p0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2744392
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2744393
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2744394
    :cond_0
    return-object v2
.end method

.method private static a(LX/4z1;LX/4z1;)LX/0Xu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)",
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2744383
    new-instance v0, LX/Jrf;

    invoke-direct {v0, p1}, LX/Jrf;-><init>(LX/0Xu;)V

    .line 2744384
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2744385
    instance-of v1, p0, LX/4xk;

    if-eqz v1, :cond_0

    check-cast p0, LX/4xk;

    .line 2744386
    invoke-interface {p0}, LX/4xi;->c()LX/0Rl;

    move-result-object v1

    invoke-static {v1, v0}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    .line 2744387
    new-instance v2, LX/4xk;

    invoke-virtual {p0}, LX/4xk;->d()LX/0vX;

    move-result-object p1

    invoke-direct {v2, p1, v1}, LX/4xk;-><init>(LX/0vX;LX/0Rl;)V

    move-object v1, v2

    .line 2744388
    :goto_0
    move-object v0, v1

    .line 2744389
    return-object v0

    :cond_0
    new-instance v2, LX/4xk;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vX;

    invoke-direct {v2, v1, v0}, LX/4xk;-><init>(LX/0vX;LX/0Rl;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Jrg;
    .locals 6

    .prologue
    .line 2744378
    new-instance v0, LX/Jrg;

    invoke-direct {v0}, LX/Jrg;-><init>()V

    .line 2744379
    const/16 v1, 0xbc

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xd18

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xafd

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2744

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/Jet;->a(LX/0QB;)LX/Jet;

    move-result-object v5

    check-cast v5, LX/Jet;

    .line 2744380
    iput-object v1, v0, LX/Jrg;->a:LX/0Ot;

    iput-object v2, v0, LX/Jrg;->b:LX/0Ot;

    iput-object v3, v0, LX/Jrg;->c:LX/0Ot;

    iput-object v4, v0, LX/Jrg;->d:LX/0Ot;

    iput-object v5, v0, LX/Jrg;->e:LX/Jet;

    .line 2744381
    move-object v0, v0

    .line 2744382
    return-object v0
.end method

.method private static declared-synchronized c(LX/Jrg;)LX/4z1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2744377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jrg;->e:LX/Jet;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, LX/Jet;->a(LX/6ek;II)LX/4z1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/Jrg;)LX/4z1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2744342
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jrg;->e:LX/Jet;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, LX/Jet;->b(LX/6ek;II)LX/4z1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2744363
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Jrg;->c(LX/Jrg;)LX/4z1;

    move-result-object v0

    .line 2744364
    invoke-static {p0}, LX/Jrg;->d(LX/Jrg;)LX/4z1;

    move-result-object v1

    .line 2744365
    invoke-static {v1, v0}, LX/Jrg;->a(LX/4z1;LX/4z1;)LX/0Xu;

    move-result-object v2

    .line 2744366
    invoke-static {v0, v1}, LX/Jrg;->a(LX/4z1;LX/4z1;)LX/0Xu;

    move-result-object v3

    .line 2744367
    invoke-interface {v2}, LX/0Xu;->n()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, LX/0Xu;->n()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 2744368
    :goto_0
    monitor-exit p0

    return-void

    .line 2744369
    :cond_0
    :try_start_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "android_messenger_threads_inconsistent"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2744370
    const-string v5, "recent_db_messages"

    invoke-static {v0}, LX/Jrg;->a(LX/0Xu;)LX/0Xu;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2744371
    const-string v0, "recent_cache_messages"

    invoke-static {v1}, LX/Jrg;->a(LX/0Xu;)LX/0Xu;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2744372
    const-string v0, "messages_not_in_db"

    invoke-static {v2}, LX/Jrg;->a(LX/0Xu;)LX/0Xu;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2744373
    const-string v0, "messages_not_in_cache"

    invoke-static {v3}, LX/Jrg;->a(LX/0Xu;)LX/0Xu;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2744374
    invoke-virtual {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2744375
    iget-object v0, p0, LX/Jrg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2744376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2744343
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Jrg;->c(LX/Jrg;)LX/4z1;

    move-result-object v5

    .line 2744344
    invoke-static {p0}, LX/Jrg;->d(LX/Jrg;)LX/4z1;

    move-result-object v6

    .line 2744345
    invoke-virtual {v5}, LX/0Xt;->q()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2744346
    invoke-virtual {v5, v0}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 2744347
    array-length v4, v1

    const-class v8, [Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v1, v4, v8}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/facebook/messaging/model/messages/Message;

    .line 2744348
    array-length v4, v1

    const/4 v8, 0x2

    if-le v4, v8, :cond_0

    .line 2744349
    invoke-virtual {v6, v0}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 2744350
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    array-length v8, v1

    if-lt v4, v8, :cond_0

    .line 2744351
    const/4 v4, 0x0

    aget-object v8, v1, v4

    .line 2744352
    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v9, v1, v4

    .line 2744353
    iget-object v1, v8, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    sget-object v4, LX/6f2;->MQTT:LX/6f2;

    if-ne v1, v4, :cond_0

    iget-object v1, v9, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    sget-object v4, LX/6f2;->MQTT:LX/6f2;

    if-ne v1, v4, :cond_0

    .line 2744354
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v1, v2

    move v4, v2

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2744355
    if-eqz v0, :cond_1

    .line 2744356
    iget-object v11, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v12, v8, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v11, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v1, v3

    .line 2744357
    :cond_2
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v11, v9, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0, v11}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    .line 2744358
    :goto_1
    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    :cond_3
    move v4, v0

    .line 2744359
    goto :goto_0

    :cond_4
    move v13, v1

    move v1, v0

    move v0, v13

    .line 2744360
    :goto_2
    if-eqz v0, :cond_5

    if-nez v1, :cond_0

    :cond_5
    move v0, v2

    .line 2744361
    :goto_3
    monitor-exit p0

    return v0

    :cond_6
    move v0, v3

    goto :goto_3

    .line 2744362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move v0, v4

    goto :goto_1

    :cond_8
    move v0, v1

    move v1, v4

    goto :goto_2
.end method
