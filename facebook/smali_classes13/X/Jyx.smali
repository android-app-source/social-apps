.class public LX/Jyx;
.super LX/Jyw;
.source ""


# instance fields
.field public e:D

.field public f:D

.field public g:LX/Jyz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2755620
    invoke-direct {p0}, LX/Jyw;-><init>()V

    .line 2755621
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, LX/Jyx;->e:D

    .line 2755622
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Jyx;->f:D

    .line 2755623
    return-void
.end method

.method public constructor <init>(LX/5pG;)V
    .locals 2

    .prologue
    .line 2755614
    invoke-direct {p0}, LX/Jyw;-><init>()V

    .line 2755615
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, LX/Jyx;->e:D

    .line 2755616
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Jyx;->f:D

    .line 2755617
    const-string v0, "value"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jyx;->e:D

    .line 2755618
    const-string v0, "offset"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/Jyx;->f:D

    .line 2755619
    return-void
.end method


# virtual methods
.method public final b()D
    .locals 4

    .prologue
    .line 2755624
    iget-wide v0, p0, LX/Jyx;->f:D

    iget-wide v2, p0, LX/Jyx;->e:D

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2755611
    iget-wide v0, p0, LX/Jyx;->e:D

    iget-wide v2, p0, LX/Jyx;->f:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/Jyx;->e:D

    .line 2755612
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Jyx;->f:D

    .line 2755613
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 2755605
    iget-wide v0, p0, LX/Jyx;->f:D

    iget-wide v2, p0, LX/Jyx;->e:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/Jyx;->f:D

    .line 2755606
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Jyx;->e:D

    .line 2755607
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 2755608
    iget-object v0, p0, LX/Jyx;->g:LX/Jyz;

    if-nez v0, :cond_0

    .line 2755609
    :goto_0
    return-void

    .line 2755610
    :cond_0
    iget-object v0, p0, LX/Jyx;->g:LX/Jyz;

    iget-wide v2, p0, LX/Jyx;->e:D

    invoke-interface {v0, v2, v3}, LX/Jyz;->a(D)V

    goto :goto_0
.end method
