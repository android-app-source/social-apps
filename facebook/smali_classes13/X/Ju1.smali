.class public final LX/Ju1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 2748281
    const/16 v20, 0x0

    .line 2748282
    const/16 v19, 0x0

    .line 2748283
    const/16 v18, 0x0

    .line 2748284
    const/16 v17, 0x0

    .line 2748285
    const/16 v16, 0x0

    .line 2748286
    const/4 v15, 0x0

    .line 2748287
    const/4 v14, 0x0

    .line 2748288
    const/4 v13, 0x0

    .line 2748289
    const/4 v12, 0x0

    .line 2748290
    const/4 v11, 0x0

    .line 2748291
    const/4 v10, 0x0

    .line 2748292
    const/4 v9, 0x0

    .line 2748293
    const/4 v8, 0x0

    .line 2748294
    const-wide/16 v6, 0x0

    .line 2748295
    const-wide/16 v4, 0x0

    .line 2748296
    const/4 v3, 0x0

    .line 2748297
    const/4 v2, 0x0

    .line 2748298
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_13

    .line 2748299
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2748300
    const/4 v2, 0x0

    .line 2748301
    :goto_0
    return v2

    .line 2748302
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 2748303
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2748304
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2748305
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2748306
    const-string v6, "copyright"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2748307
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 2748308
    :cond_1
    const-string v6, "credits"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2748309
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 2748310
    :cond_2
    const-string v6, "document_authors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2748311
    invoke-static/range {p0 .. p1}, LX/Jtz;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 2748312
    :cond_3
    const-string v6, "document_body_elements"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2748313
    invoke-static/range {p0 .. p1}, LX/Ju0;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 2748314
    :cond_4
    const-string v6, "document_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2748315
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 2748316
    :cond_5
    const-string v6, "document_kicker"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2748317
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2748318
    :cond_6
    const-string v6, "document_owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2748319
    invoke-static/range {p0 .. p1}, LX/8ZM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2748320
    :cond_7
    const-string v6, "document_subtitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2748321
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2748322
    :cond_8
    const-string v6, "document_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2748323
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2748324
    :cond_9
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2748325
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2748326
    :cond_a
    const-string v6, "feedback_options"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2748327
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2748328
    :cond_b
    const-string v6, "format_version"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2748329
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2748330
    :cond_c
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2748331
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 2748332
    :cond_d
    const-string v6, "modified_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2748333
    const/4 v2, 0x1

    .line 2748334
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2748335
    :cond_e
    const-string v6, "publish_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2748336
    const/4 v2, 0x1

    .line 2748337
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto/16 :goto_1

    .line 2748338
    :cond_f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2748339
    :cond_10
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2748340
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748341
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748342
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748343
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748344
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748345
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748346
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748347
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2748348
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2748349
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2748350
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2748351
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2748352
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2748353
    if-eqz v3, :cond_11

    .line 2748354
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2748355
    :cond_11
    if-eqz v8, :cond_12

    .line 2748356
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2748357
    :cond_12
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_13
    move/from16 v21, v18

    move/from16 v22, v19

    move/from16 v23, v20

    move/from16 v18, v15

    move/from16 v19, v16

    move/from16 v20, v17

    move v15, v12

    move/from16 v16, v13

    move/from16 v17, v14

    move v14, v11

    move v12, v9

    move v13, v10

    move v9, v8

    move-wide v10, v4

    move v8, v2

    move-wide v4, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v3, 0xb

    const/16 v2, 0xa

    const-wide/16 v4, 0x0

    .line 2748358
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2748359
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748360
    if-eqz v0, :cond_0

    .line 2748361
    const-string v1, "copyright"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748362
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748363
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748364
    if-eqz v0, :cond_1

    .line 2748365
    const-string v1, "credits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748366
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748367
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748368
    if-eqz v0, :cond_2

    .line 2748369
    const-string v1, "document_authors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748370
    invoke-static {p0, v0, p2, p3}, LX/Jtz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748371
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748372
    if-eqz v0, :cond_3

    .line 2748373
    const-string v1, "document_body_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748374
    invoke-static {p0, v0, p2, p3}, LX/Ju0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748375
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748376
    if-eqz v0, :cond_4

    .line 2748377
    const-string v1, "document_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748378
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748379
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748380
    if-eqz v0, :cond_5

    .line 2748381
    const-string v1, "document_kicker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748382
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748383
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748384
    if-eqz v0, :cond_6

    .line 2748385
    const-string v1, "document_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748386
    invoke-static {p0, v0, p2, p3}, LX/8ZM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748387
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748388
    if-eqz v0, :cond_7

    .line 2748389
    const-string v1, "document_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748390
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748391
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748392
    if-eqz v0, :cond_8

    .line 2748393
    const-string v1, "document_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748394
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748395
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748396
    if-eqz v0, :cond_9

    .line 2748397
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748398
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748399
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2748400
    if-eqz v0, :cond_a

    .line 2748401
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748402
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748403
    :cond_a
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2748404
    if-eqz v0, :cond_b

    .line 2748405
    const-string v0, "format_version"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748406
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748407
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2748408
    if-eqz v0, :cond_c

    .line 2748409
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748410
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2748411
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2748412
    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    .line 2748413
    const-string v2, "modified_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748414
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2748415
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2748416
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 2748417
    const-string v2, "publish_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748418
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2748419
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2748420
    return-void
.end method
