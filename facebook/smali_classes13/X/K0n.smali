.class public final LX/K0n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/K0l;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/K0l;)V
    .locals 0

    .prologue
    .line 2759557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2759558
    iput-object p1, p0, LX/K0n;->a:LX/K0l;

    .line 2759559
    return-void
.end method

.method public synthetic constructor <init>(LX/K0l;B)V
    .locals 0

    .prologue
    .line 2759560
    invoke-direct {p0, p1}, LX/K0n;-><init>(LX/K0l;)V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2759561
    iget v1, p0, LX/K0n;->b:I

    if-eq v1, p1, :cond_2

    .line 2759562
    iget v1, p0, LX/K0n;->b:I

    if-ge v1, p1, :cond_3

    .line 2759563
    iget v1, p0, LX/K0n;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2759564
    iget v1, p0, LX/K0n;->c:I

    .line 2759565
    iget v0, p0, LX/K0n;->b:I

    :goto_0
    move v2, v0

    .line 2759566
    :goto_1
    if-ge v2, p1, :cond_1

    .line 2759567
    iget-object v0, p0, LX/K0n;->a:LX/K0l;

    iget-object v0, v0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v1, v0

    .line 2759568
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v1, v0

    .line 2759569
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2759570
    :goto_2
    iput p1, p0, LX/K0n;->b:I

    .line 2759571
    iput v0, p0, LX/K0n;->c:I

    .line 2759572
    :cond_2
    iget v0, p0, LX/K0n;->c:I

    return v0

    .line 2759573
    :cond_3
    iget v1, p0, LX/K0n;->b:I

    sub-int/2addr v1, p1

    if-ge p1, v1, :cond_5

    move v2, v0

    move v1, v0

    .line 2759574
    :goto_3
    if-ge v2, p1, :cond_4

    .line 2759575
    iget-object v0, p0, LX/K0n;->a:LX/K0l;

    iget-object v0, v0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v1, v0

    .line 2759576
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2

    .line 2759577
    :cond_5
    iget v1, p0, LX/K0n;->c:I

    .line 2759578
    iget v0, p0, LX/K0n;->b:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_4
    if-lt v2, p1, :cond_6

    .line 2759579
    iget-object v0, p0, LX/K0n;->a:LX/K0l;

    iget-object v0, v0, LX/K0l;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v1, v0

    .line 2759580
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 2759554
    iget v0, p0, LX/K0n;->b:I

    if-ge p1, v0, :cond_0

    .line 2759555
    iget v0, p0, LX/K0n;->c:I

    sub-int/2addr v0, p2

    add-int/2addr v0, p3

    iput v0, p0, LX/K0n;->c:I

    .line 2759556
    :cond_0
    return-void
.end method
