.class public LX/K11;
.super Landroid/widget/TextView;
.source ""

# interfaces
.implements LX/5r9;


# static fields
.field private static final a:Landroid/view/ViewGroup$LayoutParams;


# instance fields
.field private b:Z

.field private c:I

.field private d:I

.field private e:Z

.field private f:F

.field private g:I

.field private h:I

.field public i:Landroid/text/TextUtils$TruncateAt;

.field private j:LX/9nY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2760533
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    sput-object v0, LX/K11;->a:Landroid/view/ViewGroup$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2760534
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2760535
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, LX/K11;->f:F

    .line 2760536
    const/4 v0, 0x0

    iput v0, p0, LX/K11;->g:I

    .line 2760537
    const v0, 0x7fffffff

    iput v0, p0, LX/K11;->h:I

    .line 2760538
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    iput-object v0, p0, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    .line 2760539
    invoke-virtual {p0}, LX/K11;->getGravity()I

    move-result v0

    const v1, 0x800007

    and-int/2addr v0, v1

    iput v0, p0, LX/K11;->c:I

    .line 2760540
    invoke-virtual {p0}, LX/K11;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x70

    iput v0, p0, LX/K11;->d:I

    .line 2760541
    return-void
.end method

.method private getOrCreateReactViewBackground()LX/9nY;
    .locals 5

    .prologue
    .line 2760542
    iget-object v0, p0, LX/K11;->j:LX/9nY;

    if-nez v0, :cond_0

    .line 2760543
    new-instance v0, LX/9nY;

    invoke-direct {v0}, LX/9nY;-><init>()V

    iput-object v0, p0, LX/K11;->j:LX/9nY;

    .line 2760544
    invoke-virtual {p0}, LX/K11;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2760545
    const/4 v1, 0x0

    invoke-super {p0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2760546
    if-nez v0, :cond_1

    .line 2760547
    iget-object v0, p0, LX/K11;->j:LX/9nY;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2760548
    :cond_0
    :goto_0
    iget-object v0, p0, LX/K11;->j:LX/9nY;

    return-object v0

    .line 2760549
    :cond_1
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    iget-object v4, p0, LX/K11;->j:LX/9nY;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2760550
    invoke-super {p0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)I
    .locals 9

    .prologue
    .line 2760551
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760552
    invoke-virtual {p0}, LX/K11;->getId()I

    move-result v4

    .line 2760553
    float-to-int v1, p1

    .line 2760554
    float-to-int v2, p2

    .line 2760555
    invoke-virtual {p0}, LX/K11;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 2760556
    if-nez v3, :cond_1

    .line 2760557
    :cond_0
    return v4

    .line 2760558
    :cond_1
    invoke-virtual {v3, v2}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v2

    .line 2760559
    invoke-virtual {v3, v2}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v5

    float-to-int v5, v5

    .line 2760560
    invoke-virtual {v3, v2}, Landroid/text/Layout;->getLineRight(I)F

    move-result v6

    float-to-int v6, v6

    .line 2760561
    if-lt v1, v5, :cond_0

    if-gt v1, v6, :cond_0

    .line 2760562
    int-to-float v1, v1

    invoke-virtual {v3, v2, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v5

    .line 2760563
    const-class v1, LX/K0x;

    invoke-interface {v0, v5, v5, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/K0x;

    .line 2760564
    if-eqz v1, :cond_0

    .line 2760565
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    .line 2760566
    const/4 v2, 0x0

    :goto_0
    array-length v6, v1

    if-ge v2, v6, :cond_0

    .line 2760567
    aget-object v6, v1, v2

    invoke-interface {v0, v6}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 2760568
    aget-object v7, v1, v2

    invoke-interface {v0, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 2760569
    if-le v7, v5, :cond_2

    sub-int v8, v7, v6

    if-gt v8, v3, :cond_2

    .line 2760570
    aget-object v3, v1, v2

    .line 2760571
    iget v4, v3, LX/K0x;->a:I

    move v4, v4

    .line 2760572
    sub-int v3, v7, v6

    .line 2760573
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2760574
    iget v0, p0, LX/K11;->h:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 2760575
    :goto_0
    invoke-virtual {p0, v0}, LX/K11;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2760576
    return-void

    .line 2760577
    :cond_0
    iget-object v0, p0, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    goto :goto_0
.end method

.method public final a(FI)V
    .locals 1

    .prologue
    .line 2760599
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(FI)V

    .line 2760600
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 2760578
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/9nY;->a(IF)V

    .line 2760579
    return-void
.end method

.method public final a(IFF)V
    .locals 1

    .prologue
    .line 2760580
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/9nY;->a(IFF)V

    .line 2760581
    return-void
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2760582
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 2760583
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760584
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760585
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 2760586
    invoke-virtual {v3}, LX/K13;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 2760587
    invoke-virtual {p0}, LX/K11;->invalidate()V

    .line 2760588
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760589
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2760590
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, -0x6bd143f9

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2760591
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    .line 2760592
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2760593
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760594
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, LX/K13;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760595
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 2760596
    invoke-virtual {v4}, LX/K13;->d()V

    .line 2760597
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760598
    :cond_0
    const v0, 0x1c3b802

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, 0x1956a6ad

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2760517
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 2760518
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2760519
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760520
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, LX/K13;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760521
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 2760522
    invoke-virtual {v4}, LX/K13;->b()V

    .line 2760523
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760524
    :cond_0
    const v0, 0x5d52111d

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2760525
    invoke-super {p0}, Landroid/widget/TextView;->onFinishTemporaryDetach()V

    .line 2760526
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2760527
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760528
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760529
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2760530
    invoke-virtual {v3}, LX/K13;->e()V

    .line 2760531
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760532
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2760458
    invoke-super {p0}, Landroid/widget/TextView;->onStartTemporaryDetach()V

    .line 2760459
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 2760460
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760461
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760462
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2760463
    invoke-virtual {v3}, LX/K13;->c()V

    .line 2760464
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760465
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 2760466
    if-nez p1, :cond_0

    iget-object v0, p0, LX/K11;->j:LX/9nY;

    if-eqz v0, :cond_1

    .line 2760467
    :cond_0
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(I)V

    .line 2760468
    :cond_1
    return-void
.end method

.method public setBorderRadius(F)V
    .locals 1

    .prologue
    .line 2760469
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(F)V

    .line 2760470
    return-void
.end method

.method public setBorderStyle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2760471
    invoke-direct {p0}, LX/K11;->getOrCreateReactViewBackground()LX/9nY;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9nY;->a(Ljava/lang/String;)V

    .line 2760472
    return-void
.end method

.method public setEllipsizeLocation(Landroid/text/TextUtils$TruncateAt;)V
    .locals 0

    .prologue
    .line 2760473
    iput-object p1, p0, LX/K11;->i:Landroid/text/TextUtils$TruncateAt;

    .line 2760474
    return-void
.end method

.method public setGravityHorizontal(I)V
    .locals 2

    .prologue
    .line 2760475
    if-nez p1, :cond_0

    .line 2760476
    iget p1, p0, LX/K11;->c:I

    .line 2760477
    :cond_0
    invoke-virtual {p0}, LX/K11;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, -0x8

    const v1, -0x800008

    and-int/2addr v0, v1

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/K11;->setGravity(I)V

    .line 2760478
    return-void
.end method

.method public setGravityVertical(I)V
    .locals 1

    .prologue
    .line 2760479
    if-nez p1, :cond_0

    .line 2760480
    iget p1, p0, LX/K11;->d:I

    .line 2760481
    :cond_0
    invoke-virtual {p0}, LX/K11;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, -0x71

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/K11;->setGravity(I)V

    .line 2760482
    return-void
.end method

.method public setNumberOfLines(I)V
    .locals 1

    .prologue
    .line 2760483
    if-nez p1, :cond_0

    const p1, 0x7fffffff

    :cond_0
    iput p1, p0, LX/K11;->h:I

    .line 2760484
    iget v0, p0, LX/K11;->h:I

    invoke-virtual {p0, v0}, LX/K11;->setMaxLines(I)V

    .line 2760485
    return-void
.end method

.method public setText(LX/K10;)V
    .locals 6

    .prologue
    .line 2760486
    iget-boolean v0, p1, LX/K10;->c:Z

    move v0, v0

    .line 2760487
    iput-boolean v0, p0, LX/K11;->b:Z

    .line 2760488
    invoke-virtual {p0}, LX/K11;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2760489
    sget-object v0, LX/K11;->a:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0}, LX/K11;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2760490
    :cond_0
    iget-object v0, p1, LX/K10;->a:Landroid/text/Spannable;

    move-object v0, v0

    .line 2760491
    invoke-virtual {p0, v0}, LX/K11;->setText(Ljava/lang/CharSequence;)V

    .line 2760492
    iget v0, p1, LX/K10;->d:F

    move v0, v0

    .line 2760493
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 2760494
    iget v1, p1, LX/K10;->e:F

    move v1, v1

    .line 2760495
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 2760496
    iget v2, p1, LX/K10;->f:F

    move v2, v2

    .line 2760497
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 2760498
    iget v3, p1, LX/K10;->g:F

    move v3, v3

    .line 2760499
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, LX/K11;->setPadding(IIII)V

    .line 2760500
    iget v0, p1, LX/K10;->h:I

    move v0, v0

    .line 2760501
    iget v1, p0, LX/K11;->g:I

    if-eq v1, v0, :cond_1

    .line 2760502
    iput v0, p0, LX/K11;->g:I

    .line 2760503
    :cond_1
    iget v0, p0, LX/K11;->g:I

    invoke-virtual {p0, v0}, LX/K11;->setGravityHorizontal(I)V

    .line 2760504
    return-void
.end method

.method public setTextIsSelectable(Z)V
    .locals 0

    .prologue
    .line 2760505
    iput-boolean p1, p0, LX/K11;->e:Z

    .line 2760506
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 2760507
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2760508
    iget-boolean v0, p0, LX/K11;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 2760509
    invoke-virtual {p0}, LX/K11;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2760510
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, LX/K13;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K13;

    .line 2760511
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 2760512
    invoke-virtual {v3}, LX/K13;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 2760513
    const/4 v0, 0x1

    .line 2760514
    :goto_1
    return v0

    .line 2760515
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2760516
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_1
.end method
