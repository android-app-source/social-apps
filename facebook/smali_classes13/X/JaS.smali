.class public LX/JaS;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JaS;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715496
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2715497
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2715498
    const-string v1, "extra_parent_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2715499
    sget-object v1, LX/0ax;->R:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v3, LX/0cQ;->GROUPS_GRID_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2715500
    return-void
.end method

.method public static a(LX/0QB;)LX/JaS;
    .locals 3

    .prologue
    .line 2715501
    sget-object v0, LX/JaS;->a:LX/JaS;

    if-nez v0, :cond_1

    .line 2715502
    const-class v1, LX/JaS;

    monitor-enter v1

    .line 2715503
    :try_start_0
    sget-object v0, LX/JaS;->a:LX/JaS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2715504
    if-eqz v2, :cond_0

    .line 2715505
    :try_start_1
    new-instance v0, LX/JaS;

    invoke-direct {v0}, LX/JaS;-><init>()V

    .line 2715506
    move-object v0, v0

    .line 2715507
    sput-object v0, LX/JaS;->a:LX/JaS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2715508
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2715509
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2715510
    :cond_1
    sget-object v0, LX/JaS;->a:LX/JaS;

    return-object v0

    .line 2715511
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2715512
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
