.class public final LX/K8b;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 41

    .prologue
    .line 2775438
    const/16 v34, 0x0

    .line 2775439
    const/16 v33, 0x0

    .line 2775440
    const/16 v32, 0x0

    .line 2775441
    const/16 v31, 0x0

    .line 2775442
    const/16 v30, 0x0

    .line 2775443
    const/16 v29, 0x0

    .line 2775444
    const/16 v28, 0x0

    .line 2775445
    const/16 v27, 0x0

    .line 2775446
    const/16 v26, 0x0

    .line 2775447
    const/16 v25, 0x0

    .line 2775448
    const/16 v24, 0x0

    .line 2775449
    const/16 v23, 0x0

    .line 2775450
    const/16 v22, 0x0

    .line 2775451
    const-wide/16 v20, 0x0

    .line 2775452
    const-wide/16 v18, 0x0

    .line 2775453
    const/16 v17, 0x0

    .line 2775454
    const/16 v16, 0x0

    .line 2775455
    const/4 v15, 0x0

    .line 2775456
    const/4 v14, 0x0

    .line 2775457
    const/4 v13, 0x0

    .line 2775458
    const/4 v12, 0x0

    .line 2775459
    const/4 v11, 0x0

    .line 2775460
    const/4 v10, 0x0

    .line 2775461
    const/4 v9, 0x0

    .line 2775462
    const/4 v8, 0x0

    .line 2775463
    const/4 v7, 0x0

    .line 2775464
    const/4 v6, 0x0

    .line 2775465
    const/4 v5, 0x0

    .line 2775466
    const/4 v4, 0x0

    .line 2775467
    const/4 v3, 0x0

    .line 2775468
    const/4 v2, 0x0

    .line 2775469
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_21

    .line 2775470
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2775471
    const/4 v2, 0x0

    .line 2775472
    :goto_0
    return v2

    .line 2775473
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v36, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v36

    if-eq v2, v0, :cond_16

    .line 2775474
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2775475
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2775476
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2775477
    const-string v36, "height"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 2775478
    const/4 v2, 0x1

    .line 2775479
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v35, v14

    move v14, v2

    goto :goto_1

    .line 2775480
    :cond_1
    const-string v36, "id"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 2775481
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto :goto_1

    .line 2775482
    :cond_2
    const-string v36, "initial_view_heading_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 2775483
    const/4 v2, 0x1

    .line 2775484
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v33, v13

    move v13, v2

    goto :goto_1

    .line 2775485
    :cond_3
    const-string v36, "initial_view_pitch_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 2775486
    const/4 v2, 0x1

    .line 2775487
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v32, v12

    move v12, v2

    goto :goto_1

    .line 2775488
    :cond_4
    const-string v36, "initial_view_roll_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 2775489
    const/4 v2, 0x1

    .line 2775490
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v31, v11

    move v11, v2

    goto/16 :goto_1

    .line 2775491
    :cond_5
    const-string v36, "is_spherical"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 2775492
    const/4 v2, 0x1

    .line 2775493
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v30, v7

    move v7, v2

    goto/16 :goto_1

    .line 2775494
    :cond_6
    const-string v36, "message"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_7

    .line 2775495
    invoke-static/range {p0 .. p1}, LX/8ZU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 2775496
    :cond_7
    const-string v36, "playable_duration_in_ms"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 2775497
    const/4 v2, 0x1

    .line 2775498
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v28, v6

    move v6, v2

    goto/16 :goto_1

    .line 2775499
    :cond_8
    const-string v36, "playable_url"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_9

    .line 2775500
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 2775501
    :cond_9
    const-string v36, "playable_url_hd"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 2775502
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 2775503
    :cond_a
    const-string v36, "playable_url_preferred"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 2775504
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 2775505
    :cond_b
    const-string v36, "playlist"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 2775506
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 2775507
    :cond_c
    const-string v36, "projection_type"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_d

    .line 2775508
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 2775509
    :cond_d
    const-string v36, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 2775510
    const/4 v2, 0x1

    .line 2775511
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2775512
    :cond_e
    const-string v36, "sphericalInlineAspectRatio"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_f

    .line 2775513
    const/4 v2, 0x1

    .line 2775514
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v22

    move v10, v2

    goto/16 :goto_1

    .line 2775515
    :cond_f
    const-string v36, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 2775516
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2775517
    :cond_10
    const-string v36, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 2775518
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2775519
    :cond_11
    const-string v36, "sphericalPlaylist"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 2775520
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2775521
    :cond_12
    const-string v36, "sphericalPreferredFov"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 2775522
    const/4 v2, 0x1

    .line 2775523
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v17, v9

    move v9, v2

    goto/16 :goto_1

    .line 2775524
    :cond_13
    const-string v36, "video_preview_image"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 2775525
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2775526
    :cond_14
    const-string v36, "width"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2775527
    const/4 v2, 0x1

    .line 2775528
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move v15, v8

    move v8, v2

    goto/16 :goto_1

    .line 2775529
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2775530
    :cond_16
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2775531
    if-eqz v14, :cond_17

    .line 2775532
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 2775533
    :cond_17
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775534
    if-eqz v13, :cond_18

    .line 2775535
    const/4 v2, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 2775536
    :cond_18
    if-eqz v12, :cond_19

    .line 2775537
    const/4 v2, 0x3

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 2775538
    :cond_19
    if-eqz v11, :cond_1a

    .line 2775539
    const/4 v2, 0x4

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 2775540
    :cond_1a
    if-eqz v7, :cond_1b

    .line 2775541
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2775542
    :cond_1b
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775543
    if-eqz v6, :cond_1c

    .line 2775544
    const/4 v2, 0x7

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 2775545
    :cond_1c
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775546
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775547
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775548
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775549
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775550
    if-eqz v3, :cond_1d

    .line 2775551
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2775552
    :cond_1d
    if-eqz v10, :cond_1e

    .line 2775553
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2775554
    :cond_1e
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775555
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775556
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775557
    if-eqz v9, :cond_1f

    .line 2775558
    const/16 v2, 0x12

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2775559
    :cond_1f
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2775560
    if-eqz v8, :cond_20

    .line 2775561
    const/16 v2, 0x14

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 2775562
    :cond_20
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_21
    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v38, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v39, v20

    move/from16 v21, v22

    move/from16 v20, v17

    move/from16 v17, v14

    move-wide/from16 v22, v18

    move/from16 v19, v38

    move v14, v11

    move/from16 v18, v15

    move v11, v8

    move v15, v12

    move v12, v9

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v39

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2775563
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2775564
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775565
    if-eqz v0, :cond_0

    .line 2775566
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775567
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775568
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775569
    if-eqz v0, :cond_1

    .line 2775570
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775572
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775573
    if-eqz v0, :cond_2

    .line 2775574
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775575
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775576
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775577
    if-eqz v0, :cond_3

    .line 2775578
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775579
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775580
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775581
    if-eqz v0, :cond_4

    .line 2775582
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775583
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775584
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2775585
    if-eqz v0, :cond_5

    .line 2775586
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775587
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2775588
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2775589
    if-eqz v0, :cond_6

    .line 2775590
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775591
    invoke-static {p0, v0, p2}, LX/8ZU;->a(LX/15i;ILX/0nX;)V

    .line 2775592
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775593
    if-eqz v0, :cond_7

    .line 2775594
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775595
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775596
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775597
    if-eqz v0, :cond_8

    .line 2775598
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775599
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775600
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775601
    if-eqz v0, :cond_9

    .line 2775602
    const-string v1, "playable_url_hd"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775603
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775604
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775605
    if-eqz v0, :cond_a

    .line 2775606
    const-string v1, "playable_url_preferred"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775607
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775608
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775609
    if-eqz v0, :cond_b

    .line 2775610
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775611
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775612
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775613
    if-eqz v0, :cond_c

    .line 2775614
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775616
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2775617
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_d

    .line 2775618
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775619
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2775620
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2775621
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_e

    .line 2775622
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775623
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2775624
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775625
    if-eqz v0, :cond_f

    .line 2775626
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775627
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775628
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775629
    if-eqz v0, :cond_10

    .line 2775630
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775631
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775632
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2775633
    if-eqz v0, :cond_11

    .line 2775634
    const-string v1, "sphericalPlaylist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775635
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2775636
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775637
    if-eqz v0, :cond_12

    .line 2775638
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775639
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775640
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2775641
    if-eqz v0, :cond_13

    .line 2775642
    const-string v1, "video_preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775643
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2775644
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2775645
    if-eqz v0, :cond_14

    .line 2775646
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2775647
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2775648
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2775649
    return-void
.end method
