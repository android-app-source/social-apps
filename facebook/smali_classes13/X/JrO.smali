.class public LX/JrO;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/Jrb;

.field private final c:LX/Jqb;

.field private final d:LX/Jrc;

.field private final e:LX/0SG;

.field private final f:LX/Jqf;

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2743261
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrO;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0SG;LX/0Ot;LX/Jqf;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/Jqf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2743251
    invoke-direct {p0, p6}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2743252
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2743253
    iput-object v0, p0, LX/JrO;->g:LX/0Ot;

    .line 2743254
    iput-object p1, p0, LX/JrO;->a:LX/FDs;

    .line 2743255
    iput-object p2, p0, LX/JrO;->b:LX/Jrb;

    .line 2743256
    iput-object p3, p0, LX/JrO;->c:LX/Jqb;

    .line 2743257
    iput-object p5, p0, LX/JrO;->e:LX/0SG;

    .line 2743258
    iput-object p4, p0, LX/JrO;->d:LX/Jrc;

    .line 2743259
    iput-object p7, p0, LX/JrO;->f:LX/Jqf;

    .line 2743260
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2743248
    invoke-virtual {p1}, LX/6kW;->k()LX/6kR;

    move-result-object v0

    .line 2743249
    iget-object v1, p0, LX/JrO;->d:LX/Jrc;

    iget-object v0, v0, LX/6kR;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2743250
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrO;
    .locals 15

    .prologue
    .line 2743217
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2743218
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2743219
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2743220
    if-nez v1, :cond_0

    .line 2743221
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2743222
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2743223
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2743224
    sget-object v1, LX/JrO;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2743225
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2743226
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2743227
    :cond_1
    if-nez v1, :cond_4

    .line 2743228
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2743229
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2743230
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2743231
    new-instance v7, LX/JrO;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v8

    check-cast v8, LX/FDs;

    invoke-static {v0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v9

    check-cast v9, LX/Jrb;

    invoke-static {v0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v10

    check-cast v10, LX/Jqb;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v11

    check-cast v11, LX/Jrc;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0x35bd

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, LX/Jqf;->a(LX/0QB;)LX/Jqf;

    move-result-object v14

    check-cast v14, LX/Jqf;

    invoke-direct/range {v7 .. v14}, LX/JrO;-><init>(LX/FDs;LX/Jrb;LX/Jqb;LX/Jrc;LX/0SG;LX/0Ot;LX/Jqf;)V

    .line 2743232
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2743233
    iput-object v8, v7, LX/JrO;->g:LX/0Ot;

    .line 2743234
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2743235
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2743236
    if-nez v1, :cond_2

    .line 2743237
    sget-object v0, LX/JrO;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrO;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2743238
    :goto_1
    if-eqz v0, :cond_3

    .line 2743239
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743240
    :goto_3
    check-cast v0, LX/JrO;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2743241
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2743242
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2743243
    :catchall_1
    move-exception v0

    .line 2743244
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2743245
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2743246
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2743247
    :cond_2
    :try_start_8
    sget-object v0, LX/JrO;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrO;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kR;J)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2743170
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/JrO;->b:LX/Jrb;

    .line 2743171
    iget-object v3, p2, LX/6kR;->messageMetadata:LX/6kn;

    invoke-static {v0, v3, p1}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v3

    sget-object v5, LX/2uW;->SET_NAME:LX/2uW;

    .line 2743172
    iput-object v5, v3, LX/6f7;->l:LX/2uW;

    .line 2743173
    move-object v3, v3

    .line 2743174
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2743175
    iget-object v5, v0, LX/Jrb;->f:LX/3QS;

    sget-object v6, LX/6fK;->SYNC_PROTOCOL_THREAD_NAME_DELTA:LX/6fK;

    invoke-virtual {v5, v6, v3}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2743176
    move-object v3, v3

    .line 2743177
    iget-object v0, p0, LX/JrO;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2743178
    iget-object v0, p0, LX/JrO;->a:LX/FDs;

    invoke-virtual {v0, v1, p3, p4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2743179
    iget-object v1, p0, LX/JrO;->a:LX/FDs;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p2, LX/6kR;->name:Ljava/lang/String;

    const/4 p1, 0x0

    .line 2743180
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2743181
    const-string v4, "thread_key"

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743182
    const-string v4, "name"

    invoke-virtual {v5, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743183
    iget-object v4, v1, LX/FDs;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6dQ;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2743184
    const-string v6, "threads"

    const-string v7, "thread_key=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v8, p1

    invoke-virtual {v4, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2743185
    iget-object v4, v1, LX/FDs;->d:LX/2N4;

    invoke-virtual {v4, v2, p1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v5, v4

    .line 2743186
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2743187
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2743188
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v3

    .line 2743189
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v4, v4

    .line 2743190
    iget-wide v9, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 2743191
    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2743215
    check-cast p1, LX/6kW;

    .line 2743216
    invoke-direct {p0, p1}, LX/JrO;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2743205
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->k()LX/6kR;

    move-result-object v0

    .line 2743206
    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-direct {p0, p1, v0, v2, v3}, LX/JrO;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6kR;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v1

    .line 2743207
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2743208
    const-string v3, "newMessageResult"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2743209
    iget-object v3, v0, LX/6kR;->messageMetadata:LX/6kn;

    .line 2743210
    if-eqz v3, :cond_0

    .line 2743211
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v3, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v4, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2743212
    if-eqz v3, :cond_0

    iget-object v0, v0, LX/6kR;->name:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2743213
    iget-object v0, p0, LX/JrO;->f:LX/Jqf;

    invoke-virtual {v0, v1}, LX/Jqf;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2743214
    :cond_0
    return-object v2
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2743193
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2743194
    if-eqz v0, :cond_0

    .line 2743195
    iget-object v1, p0, LX/JrO;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v2, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)V

    .line 2743196
    iget-object v1, p0, LX/JrO;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2743197
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2743198
    iget-wide v6, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 2743199
    invoke-virtual {v1, v2, v4, v5}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2743200
    iget-object v1, p0, LX/JrO;->c:LX/Jqb;

    .line 2743201
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2743202
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2743203
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2743204
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2743192
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrO;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
