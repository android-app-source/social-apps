.class public final LX/JjS;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventCreateModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jj7;

.field public final synthetic b:LX/JjT;


# direct methods
.method public constructor <init>(LX/JjT;LX/Jj7;)V
    .locals 0

    .prologue
    .line 2727671
    iput-object p1, p0, LX/JjS;->b:LX/JjT;

    iput-object p2, p0, LX/JjS;->a:LX/Jj7;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2727672
    iget-object v0, p0, LX/JjS;->b:LX/JjT;

    iget-object v0, v0, LX/JjT;->b:LX/JjG;

    const-string v1, "EventReminderMutator"

    const-string v2, "Failed to create an event reminder."

    invoke-virtual {v0, v1, v2, p1}, LX/JjG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2727673
    iget-object v0, p0, LX/JjS;->a:LX/Jj7;

    if-eqz v0, :cond_0

    .line 2727674
    iget-object v0, p0, LX/JjS;->a:LX/Jj7;

    invoke-interface {v0}, LX/Jj7;->a()V

    .line 2727675
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2727676
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2727677
    iget-object v0, p0, LX/JjS;->a:LX/Jj7;

    if-eqz v0, :cond_0

    .line 2727678
    iget-object v1, p0, LX/JjS;->a:LX/Jj7;

    .line 2727679
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2727680
    check-cast v0, Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventCreateModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventCreateModel;->a()Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventCreateModel$EventReminderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/events/graphql/EventRemindersMutationModels$LightweightEventCreateModel$EventReminderModel;->j()Ljava/lang/String;

    invoke-interface {v1}, LX/Jj7;->b()V

    .line 2727681
    :cond_0
    return-void
.end method
