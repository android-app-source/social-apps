.class public final LX/K9w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2778796
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2778797
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2778798
    :goto_0
    return v1

    .line 2778799
    :cond_0
    const-string v11, "viewer_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2778800
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    .line 2778801
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 2778802
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2778803
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2778804
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2778805
    const-string v11, "location"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2778806
    invoke-static {p0, p1}, LX/K9r;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2778807
    :cond_2
    const-string v11, "preview_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2778808
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2778809
    :cond_3
    const-string v11, "profile_picture"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2778810
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2778811
    :cond_4
    const-string v11, "publisher_category"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2778812
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2778813
    :cond_5
    const-string v11, "video"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2778814
    invoke-static {p0, p1}, LX/K9u;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2778815
    :cond_6
    const-string v11, "viewer_locations"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2778816
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2778817
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_7

    .line 2778818
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_7

    .line 2778819
    invoke-static {p0, p1}, LX/K9v;->b(LX/15w;LX/186;)I

    move-result v10

    .line 2778820
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2778821
    :cond_7
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2778822
    goto/16 :goto_1

    .line 2778823
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2778824
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2778825
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2778826
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2778827
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2778828
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2778829
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2778830
    if-eqz v0, :cond_a

    .line 2778831
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2778832
    :cond_a
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2778833
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2778741
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2778742
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2778743
    if-eqz v0, :cond_2

    .line 2778744
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778745
    const-wide/16 v7, 0x0

    .line 2778746
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2778747
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 2778748
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_0

    .line 2778749
    const-string v5, "latitude"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778750
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 2778751
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 2778752
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_1

    .line 2778753
    const-string v5, "longitude"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778754
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 2778755
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2778756
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778757
    if-eqz v0, :cond_3

    .line 2778758
    const-string v1, "preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778759
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778760
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778761
    if-eqz v0, :cond_4

    .line 2778762
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778763
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778764
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778765
    if-eqz v0, :cond_5

    .line 2778766
    const-string v1, "publisher_category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778767
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2778768
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2778769
    if-eqz v0, :cond_6

    .line 2778770
    const-string v1, "video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778771
    invoke-static {p0, v0, p2, p3}, LX/K9u;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2778772
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2778773
    if-eqz v0, :cond_7

    .line 2778774
    const-string v1, "viewer_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778775
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2778776
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2778777
    if-eqz v0, :cond_b

    .line 2778778
    const-string v1, "viewer_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778779
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2778780
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_a

    .line 2778781
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v4

    const-wide/16 v9, 0x0

    .line 2778782
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2778783
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v9, v10}, LX/15i;->a(IID)D

    move-result-wide v5

    .line 2778784
    cmpl-double v7, v5, v9

    if-eqz v7, :cond_8

    .line 2778785
    const-string v7, "latitude"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778786
    invoke-virtual {p2, v5, v6}, LX/0nX;->a(D)V

    .line 2778787
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5, v9, v10}, LX/15i;->a(IID)D

    move-result-wide v5

    .line 2778788
    cmpl-double v7, v5, v9

    if-eqz v7, :cond_9

    .line 2778789
    const-string v7, "longitude"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2778790
    invoke-virtual {p2, v5, v6}, LX/0nX;->a(D)V

    .line 2778791
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2778792
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2778793
    :cond_a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2778794
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2778795
    return-void
.end method
