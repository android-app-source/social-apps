.class public LX/K0t;
.super Landroid/text/style/MetricAffectingSpan;
.source ""


# instance fields
.field private final a:Landroid/content/res/AssetManager;

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/lang/String;Landroid/content/res/AssetManager;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2759853
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 2759854
    iput p1, p0, LX/K0t;->b:I

    .line 2759855
    iput p2, p0, LX/K0t;->c:I

    .line 2759856
    iput-object p3, p0, LX/K0t;->d:Ljava/lang/String;

    .line 2759857
    iput-object p4, p0, LX/K0t;->a:Landroid/content/res/AssetManager;

    .line 2759858
    return-void
.end method

.method private static a(Landroid/graphics/Paint;IILjava/lang/String;Landroid/content/res/AssetManager;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 2759863
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 2759864
    if-nez v0, :cond_5

    move v3, v1

    .line 2759865
    :goto_0
    if-eq p2, v2, :cond_0

    and-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_1

    if-ne p2, v5, :cond_1

    :cond_0
    move v1, v2

    .line 2759866
    :cond_1
    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    and-int/lit8 v2, v3, 0x2

    if-eqz v2, :cond_3

    if-ne p1, v5, :cond_3

    .line 2759867
    :cond_2
    or-int/lit8 v1, v1, 0x2

    .line 2759868
    :cond_3
    if-eqz p3, :cond_6

    .line 2759869
    invoke-static {}, LX/K0w;->a()LX/K0w;

    move-result-object v0

    invoke-virtual {v0, p3, v1, p4}, LX/K0w;->a(Ljava/lang/String;ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2759870
    :cond_4
    :goto_1
    if-eqz v0, :cond_7

    .line 2759871
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2759872
    :goto_2
    return-void

    .line 2759873
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v3

    goto :goto_0

    .line 2759874
    :cond_6
    if-eqz v0, :cond_4

    .line 2759875
    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1

    .line 2759876
    :cond_7
    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_2
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    .line 2759861
    iget v0, p0, LX/K0t;->b:I

    iget v1, p0, LX/K0t;->c:I

    iget-object v2, p0, LX/K0t;->d:Ljava/lang/String;

    iget-object v3, p0, LX/K0t;->a:Landroid/content/res/AssetManager;

    invoke-static {p1, v0, v1, v2, v3}, LX/K0t;->a(Landroid/graphics/Paint;IILjava/lang/String;Landroid/content/res/AssetManager;)V

    .line 2759862
    return-void
.end method

.method public final updateMeasureState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    .line 2759859
    iget v0, p0, LX/K0t;->b:I

    iget v1, p0, LX/K0t;->c:I

    iget-object v2, p0, LX/K0t;->d:Ljava/lang/String;

    iget-object v3, p0, LX/K0t;->a:Landroid/content/res/AssetManager;

    invoke-static {p1, v0, v1, v2, v3}, LX/K0t;->a(Landroid/graphics/Paint;IILjava/lang/String;Landroid/content/res/AssetManager;)V

    .line 2759860
    return-void
.end method
