.class public final LX/Jw2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3FB;

.field public final synthetic b:Lcom/facebook/location/ImmutableLocation;

.field public final synthetic c:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic d:LX/3FA;


# direct methods
.method public constructor <init>(LX/3FA;LX/3FB;Lcom/facebook/location/ImmutableLocation;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 2751336
    iput-object p1, p0, LX/Jw2;->d:LX/3FA;

    iput-object p2, p0, LX/Jw2;->a:LX/3FB;

    iput-object p3, p0, LX/Jw2;->b:Lcom/facebook/location/ImmutableLocation;

    iput-object p4, p0, LX/Jw2;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2751337
    iget-object v0, p0, LX/Jw2;->a:LX/3FB;

    sget-object v1, LX/3FC;->END_PAGE_LOOKUP_FAIL:LX/3FC;

    .line 2751338
    iget-object v2, v0, LX/3FB;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2cw;

    sget-object v3, LX/2cx;->GPS:LX/2cx;

    invoke-virtual {v2, v1, v3, p1}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/Throwable;)V

    .line 2751339
    iget-object v0, p0, LX/Jw2;->d:LX/3FA;

    iget-object v1, p0, LX/Jw2;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0, v1}, LX/3FA;->a$redex0(LX/3FA;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2751340
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2751341
    check-cast p1, Ljava/util/List;

    .line 2751342
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ct;

    .line 2751343
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 2751344
    iget-object v3, p0, LX/Jw2;->a:LX/3FB;

    iget-object v1, p0, LX/Jw2;->d:LX/3FA;

    iget-object v1, v1, LX/3FA;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jvw;

    invoke-virtual {v1, v2}, LX/Jvw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2751345
    iget-object v5, v3, LX/3FB;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2cw;

    sget-object v6, LX/2cx;->GPS:LX/2cx;

    const/4 v10, 0x0

    .line 2751346
    iget-object v7, v5, LX/2cw;->c:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    invoke-static {v5, v6, v10, v7, v8}, LX/2cw;->a(LX/2cw;LX/2cx;ZJ)LX/3FD;

    move-result-object v7

    iput-object v1, v7, LX/3FD;->c:Ljava/lang/String;

    .line 2751347
    iget-object v7, v5, LX/2cw;->d:LX/0bW;

    const-string v8, "Updated logging id for %s to %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v10

    const/4 v10, 0x1

    aput-object v1, v9, v10

    invoke-interface {v7, v8, v9}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2751348
    iget-object v1, p0, LX/Jw2;->a:LX/3FB;

    .line 2751349
    iget-object v3, v1, LX/3FB;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2cw;

    sget-object v4, LX/2cx;->GPS:LX/2cx;

    invoke-virtual {v3, v4}, LX/2cw;->a(LX/2cx;)V

    .line 2751350
    iget-object v1, p0, LX/Jw2;->d:LX/3FA;

    iget-object v3, p0, LX/Jw2;->b:Lcom/facebook/location/ImmutableLocation;

    iget-object v4, p0, LX/Jw2;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2751351
    iget-object v5, v1, LX/3FA;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ck;

    sget-object v6, LX/3FG;->HANDLE_LOCATION_RESULT:LX/3FG;

    new-instance v7, LX/Jw3;

    invoke-direct {v7, v1, v3, v2, v0}, LX/Jw3;-><init>(LX/3FA;Lcom/facebook/location/ImmutableLocation;Ljava/lang/Object;LX/2ct;)V

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2751352
    return-void
.end method
