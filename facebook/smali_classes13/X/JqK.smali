.class public LX/JqK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Fx;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7Fx",
        "<",
        "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final g:Ljava/lang/Object;


# instance fields
.field public final b:LX/6cy;

.field private final c:LX/6dQ;

.field private final d:LX/Jqc;

.field private final e:Ljava/lang/Boolean;

.field private final f:LX/7Gg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2738803
    const-class v0, LX/JqK;

    sput-object v0, LX/JqK;->a:Ljava/lang/Class;

    .line 2738804
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JqK;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6cy;LX/6dQ;LX/Jqc;Ljava/lang/Boolean;LX/7Gg;)V
    .locals 0
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/AreSyncPerBatchTransactionsEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2738796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738797
    iput-object p1, p0, LX/JqK;->b:LX/6cy;

    .line 2738798
    iput-object p2, p0, LX/JqK;->c:LX/6dQ;

    .line 2738799
    iput-object p3, p0, LX/JqK;->d:LX/Jqc;

    .line 2738800
    iput-object p4, p0, LX/JqK;->e:Ljava/lang/Boolean;

    .line 2738801
    iput-object p5, p0, LX/JqK;->f:LX/7Gg;

    .line 2738802
    return-void
.end method

.method public static a(LX/0QB;)LX/JqK;
    .locals 14

    .prologue
    .line 2738765
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2738766
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2738767
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2738768
    if-nez v1, :cond_0

    .line 2738769
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2738770
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2738771
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2738772
    sget-object v1, LX/JqK;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2738773
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2738774
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2738775
    :cond_1
    if-nez v1, :cond_4

    .line 2738776
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2738777
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2738778
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2738779
    new-instance v7, LX/JqK;

    invoke-static {v0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v8

    check-cast v8, LX/6cy;

    invoke-static {v0}, LX/6dQ;->a(LX/0QB;)LX/6dQ;

    move-result-object v9

    check-cast v9, LX/6dQ;

    invoke-static {v0}, LX/Jqc;->a(LX/0QB;)LX/Jqc;

    move-result-object v10

    check-cast v10, LX/Jqc;

    .line 2738780
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    const/16 v12, 0x568

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, LX/0Uh;->a(IZ)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object v11, v11

    .line 2738781
    check-cast v11, Ljava/lang/Boolean;

    invoke-static {v0}, LX/7Gg;->a(LX/0QB;)LX/7Gg;

    move-result-object v12

    check-cast v12, LX/7Gg;

    invoke-direct/range {v7 .. v12}, LX/JqK;-><init>(LX/6cy;LX/6dQ;LX/Jqc;Ljava/lang/Boolean;LX/7Gg;)V

    .line 2738782
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2738783
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2738784
    if-nez v1, :cond_2

    .line 2738785
    sget-object v0, LX/JqK;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2738786
    :goto_1
    if-eqz v0, :cond_3

    .line 2738787
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2738788
    :goto_3
    check-cast v0, LX/JqK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2738789
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2738790
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2738791
    :catchall_1
    move-exception v0

    .line 2738792
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2738793
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2738794
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2738795
    :cond_2
    :try_start_8
    sget-object v0, LX/JqK;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JqK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private b(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2738756
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    .line 2738757
    iget v1, v0, LX/6kT;->setField_:I

    move v1, v1

    .line 2738758
    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, LX/6kW;->c()LX/6k5;

    move-result-object v1

    iget-object v1, v1, LX/6k5;->numNoOps:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/6kW;->c()LX/6k5;

    move-result-object v1

    iget-object v1, v1, LX/6k5;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 2738759
    iget-object v1, p0, LX/JqK;->b:LX/6cy;

    sget-object v2, LX/6cx;->l:LX/2bA;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v0}, LX/6kW;->c()LX/6k5;

    move-result-object v0

    iget-object v0, v0, LX/6k5;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, LX/48u;->b(LX/0To;J)V

    .line 2738760
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2738761
    :goto_0
    return-object v0

    .line 2738762
    :cond_0
    iget-object v1, p0, LX/JqK;->d:LX/Jqc;

    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v1, v0}, LX/Jqc;->a(LX/6kW;)LX/Jqi;

    move-result-object v0

    .line 2738763
    invoke-virtual {v0, p1, p2}, LX/Jqi;->a(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v0

    .line 2738764
    iget-object v1, p0, LX/JqK;->b:LX/6cy;

    sget-object v2, LX/6cx;->l:LX/2bA;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v1, v2, v4, v5}, LX/48u;->b(LX/0To;J)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 2738755
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->l:LX/2bA;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/List;)LX/0P1;
    .locals 12

    .prologue
    .line 2738701
    check-cast p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    .line 2738702
    iget-object v0, p0, LX/JqK;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2738703
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2738704
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2738705
    iget-object v0, p0, LX/JqK;->c:LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2738706
    invoke-virtual {p0}, LX/JqK;->a()J

    move-result-wide v4

    .line 2738707
    const v0, 0x1f7bad15

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2738708
    const/4 v0, 0x0

    .line 2738709
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    .line 2738710
    invoke-virtual {p0}, LX/JqK;->a()J

    move-result-wide v8

    .line 2738711
    iget-wide v10, v0, LX/7GJ;->b:J

    invoke-static {v10, v11, v8, v9}, LX/7Gg;->a(JJ)LX/7Gf;

    move-result-object v7

    .line 2738712
    sget-object v8, LX/7Gf;->EXPECTED:LX/7Gf;

    if-eq v7, v8, :cond_2

    .line 2738713
    if-nez v1, :cond_1

    const/4 v1, -0x1

    move v2, v1

    .line 2738714
    :goto_1
    sget-object v6, LX/JqK;->a:Ljava/lang/Class;

    const-string v7, "Unexpected delta! type %d , previous type: %d, firstSeqId: %d, currentSeqId: %d"

    const/4 v1, 0x4

    new-array v8, v1, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v1, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    .line 2738715
    iget v10, v1, LX/6kT;->setField_:I

    move v1, v10

    .line 2738716
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v9

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x3

    iget-wide v10, v0, LX/7GJ;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-static {v6, v7, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2738717
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to process the whole batch of deltas at once."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2738718
    :catch_0
    move-exception v0

    .line 2738719
    :try_start_1
    sget-object v1, LX/JqK;->a:Ljava/lang/Class;

    const-string v2, "Failed batch processing deltas"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2738720
    iget-object v1, p0, LX/JqK;->b:LX/6cy;

    sget-object v2, LX/6cx;->l:LX/2bA;

    invoke-virtual {v1, v2, v4, v5}, LX/48u;->b(LX/0To;J)V

    .line 2738721
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2738722
    :catchall_0
    move-exception v0

    const v1, -0x69e2ba7a

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 2738723
    :cond_1
    :try_start_2
    iget-object v1, v1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    .line 2738724
    iget v2, v1, LX/6kT;->setField_:I

    move v1, v2

    .line 2738725
    move v2, v1

    goto :goto_1

    .line 2738726
    :cond_2
    invoke-direct {p0, p1, v0}, LX/JqK;->b(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v1

    .line 2738727
    if-eqz v1, :cond_3

    .line 2738728
    iget-wide v8, v0, LX/7GJ;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-object v1, v0

    goto/16 :goto_0

    .line 2738729
    :cond_3
    sget-object v7, LX/JqK;->a:Ljava/lang/Class;

    const-string v8, "Bundle from handleDeltaInternal is null. delta type: %d, seqId: %d"

    const/4 v1, 0x2

    new-array v9, v1, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v1, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    .line 2738730
    iget v11, v1, LX/6kT;->setField_:I

    move v1, v11

    .line 2738731
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v1, 0x1

    iget-wide v10, v0, LX/7GJ;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v7, v8, v9}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    .line 2738732
    goto/16 :goto_0

    .line 2738733
    :cond_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2738734
    invoke-virtual {p0}, LX/JqK;->a()J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2738735
    const v0, 0x90fb00d

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2738736
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/7GJ;)Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 2738742
    check-cast p1, Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;

    .line 2738743
    iget-object v0, p0, LX/JqK;->c:LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2738744
    const v0, 0x5ebff557

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2738745
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/JqK;->b(Lcom/facebook/messaging/sync/delta/PrefetchedSyncData;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v0

    .line 2738746
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2738747
    const v1, -0x7abe4292

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2738748
    return-object v0

    .line 2738749
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2738750
    :try_start_1
    sget-object v3, LX/JqK;->a:Ljava/lang/Class;

    const-string v4, "Error applying delta type %d seqid %d error: %s"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    .line 2738751
    iget v7, v0, LX/6kT;->setField_:I

    move v0, v7

    .line 2738752
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-wide v6, p2, LX/7GJ;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2738753
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2738754
    :catchall_0
    move-exception v0

    const v1, 0x5f1c4de6

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2738805
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->l:LX/2bA;

    invoke-virtual {v0, v1, p1, p2}, LX/48u;->b(LX/0To;J)V

    .line 2738806
    return-void
.end method

.method public final a(ZLcom/facebook/sync/analytics/FullRefreshReason;)V
    .locals 3

    .prologue
    .line 2738739
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->n:LX/2bA;

    invoke-virtual {v0, v1, p1}, LX/48u;->b(LX/0To;Z)V

    .line 2738740
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->o:LX/2bA;

    invoke-virtual {p2}, Lcom/facebook/sync/analytics/FullRefreshReason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/48u;->b(LX/0To;Ljava/lang/String;)V

    .line 2738741
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 2738737
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->m:LX/2bA;

    invoke-virtual {v0, v1, p1, p2}, LX/48u;->b(LX/0To;J)V

    .line 2738738
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2738700
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->n:LX/2bA;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/48u;->a(LX/0To;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Lcom/facebook/sync/analytics/FullRefreshReason;
    .locals 3

    .prologue
    .line 2738695
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->o:LX/2bA;

    const-string v2, ""

    .line 2738696
    invoke-virtual {v0, v1}, LX/48u;->a(LX/0To;)Ljava/lang/String;

    move-result-object p0

    .line 2738697
    if-nez p0, :cond_0

    .line 2738698
    :goto_0
    move-object v0, v2

    .line 2738699
    invoke-static {v0}, Lcom/facebook/sync/analytics/FullRefreshReason;->a(Ljava/lang/String;)Lcom/facebook/sync/analytics/FullRefreshReason;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p0

    goto :goto_0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 2738694
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->m:LX/2bA;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2738693
    iget-object v0, p0, LX/JqK;->b:LX/6cy;

    sget-object v1, LX/6cx;->k:LX/2bA;

    invoke-virtual {v0, v1}, LX/48u;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2738692
    iget-object v0, p0, LX/JqK;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
