.class public LX/JYo;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYo",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2707219
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2707220
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYo;->b:LX/0Zi;

    .line 2707221
    iput-object p1, p0, LX/JYo;->a:LX/0Ot;

    .line 2707222
    return-void
.end method

.method public static a(LX/0QB;)LX/JYo;
    .locals 4

    .prologue
    .line 2707278
    const-class v1, LX/JYo;

    monitor-enter v1

    .line 2707279
    :try_start_0
    sget-object v0, LX/JYo;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2707280
    sput-object v2, LX/JYo;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2707281
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707282
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2707283
    new-instance v3, LX/JYo;

    const/16 p0, 0x2189

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYo;-><init>(LX/0Ot;)V

    .line 2707284
    move-object v0, v3

    .line 2707285
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2707286
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2707287
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2707288
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2707277
    const v0, 0x2407017f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2707255
    check-cast p2, LX/JYn;

    .line 2707256
    iget-object v0, p0, LX/JYo;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    iget-object v1, p2, LX/JYn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x3

    const/4 p0, 0x1

    const/4 v10, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 2707257
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2707258
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707259
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->mx()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2707260
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->mw()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object v4, v4

    .line 2707261
    invoke-static {v4}, LX/1eD;->e(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/8sH;

    move-result-object v4

    .line 2707262
    iget-object v5, v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->f:LX/1Uf;

    invoke-virtual {v5, v4}, LX/1Uf;->a(LX/8sH;)Landroid/text/Spannable;

    move-result-object v4

    .line 2707263
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLCharity;->k()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2707264
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 v6, 0x8

    const v7, 0x7f0b010f

    invoke-interface {v2, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    iget-object v6, v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->c:LX/2g9;

    invoke-virtual {v6, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v6

    const/16 v7, 0x101

    invoke-virtual {v6, v7}, LX/2gA;->h(I)LX/2gA;

    move-result-object v6

    const v7, 0x7f083a93

    invoke-virtual {v6, v7}, LX/2gA;->i(I)LX/2gA;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    .line 2707265
    const v7, 0x2407017f

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2707266
    invoke-interface {v6, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v2, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2707267
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v7, 0x6

    const v8, 0x7f0b25fa

    invoke-interface {v2, v7, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v7

    .line 2707268
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2707269
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2707270
    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2707271
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    .line 2707272
    const v3, 0x2407d7b6

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2707273
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    iget-object v2, v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v5}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v5, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v4, 0x7f0b25f9

    invoke-interface {v2, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2707274
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2707275
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2707276
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2707223
    invoke-static {}, LX/1dS;->b()V

    .line 2707224
    iget v0, p1, LX/1dQ;->b:I

    .line 2707225
    sparse-switch v0, :sswitch_data_0

    .line 2707226
    :goto_0
    return-object v2

    .line 2707227
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2707228
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2707229
    check-cast v1, LX/JYn;

    .line 2707230
    iget-object v3, p0, LX/JYo;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    iget-object v4, v1, LX/JYn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707231
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2707232
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707233
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCharity;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 2707234
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCharity;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object p1

    .line 2707235
    :goto_1
    move-object p1, p1

    .line 2707236
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 2707237
    iget-object p2, v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->e:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2707238
    iget-object p1, v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->b:LX/0Zb;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    .line 2707239
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "fundraiser_for_story_charity_click"

    invoke-direct {p2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "fundraiser_for_story"

    .line 2707240
    iput-object p0, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2707241
    move-object p2, p2

    .line 2707242
    const-string p0, "fundraiser_campaign_id"

    invoke-virtual {p2, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    move-object v5, p2

    .line 2707243
    invoke-interface {p1, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2707244
    :cond_0
    goto :goto_0

    .line 2707245
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2707246
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2707247
    check-cast v1, LX/JYn;

    .line 2707248
    iget-object v3, p0, LX/JYo;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;

    iget-object v4, v1, LX/JYn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2707249
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2707250
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2707251
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mz()Ljava/lang/String;

    move-result-object p0

    .line 2707252
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    const-string v1, "attachment_type"

    const-string v4, "native"

    invoke-virtual {p0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    move-object v5, p0

    .line 2707253
    iget-object p0, v3, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentBodyComponentSpec;->e:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2707254
    goto/16 :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2407017f -> :sswitch_1
        0x2407d7b6 -> :sswitch_0
    .end sparse-switch
.end method
