.class public LX/Jwx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/UUID;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2752647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2752648
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2752649
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jwx;->a:Ljava/util/UUID;

    .line 2752650
    :goto_0
    return-void

    .line 2752651
    :cond_0
    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, LX/Jwx;->a:Ljava/util/UUID;

    goto :goto_0
.end method

.method public static a(LX/Jwx;LX/Jwt;)Lcom/facebook/placetips/bootstrap/data/PulsarRecord;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v7, 0x16

    const/16 v4, 0x14

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v6, 0x1

    .line 2752652
    iget v0, p1, LX/Jwt;->a:I

    move v0, v0

    .line 2752653
    const/16 v1, 0x1a

    if-ne v0, v1, :cond_0

    .line 2752654
    iget-byte v0, p1, LX/Jwt;->b:B

    move v0, v0

    .line 2752655
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2752656
    :cond_0
    const/4 v0, 0x0

    .line 2752657
    :goto_0
    return-object v0

    .line 2752658
    :cond_1
    iget-object v0, p1, LX/Jwt;->c:[B

    move-object v0, v0

    .line 2752659
    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 2752660
    iget-object v1, p1, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752661
    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    .line 2752662
    iget-object v1, p1, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752663
    invoke-static {v1, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    .line 2752664
    iget-object v1, p1, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752665
    invoke-static {v1, v4, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    .line 2752666
    iget-object v1, p1, LX/Jwt;->c:[B

    move-object v1, v1

    .line 2752667
    const/16 v5, 0x18

    invoke-static {v1, v7, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    .line 2752668
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v6, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    .line 2752669
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v6, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    .line 2752670
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2752671
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v10

    .line 2752672
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v8

    .line 2752673
    new-instance v12, Ljava/util/UUID;

    invoke-direct {v12, v10, v11, v8, v9}, Ljava/util/UUID;-><init>(JJ)V

    move-object v3, v12

    .line 2752674
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v6, v4}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v4

    .line 2752675
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v6, v5}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v5

    .line 2752676
    iget-object v0, p0, LX/Jwx;->a:Ljava/util/UUID;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Jwx;->a:Ljava/util/UUID;

    invoke-virtual {v0, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2752677
    const/4 v0, 0x0

    goto :goto_0

    .line 2752678
    :cond_2
    new-instance v0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;-><init>(IILjava/util/UUID;II)V

    goto :goto_0
.end method
