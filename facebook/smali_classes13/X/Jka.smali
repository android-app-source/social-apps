.class public LX/Jka;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Lcom/facebook/user/model/UserKey;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JkZ;)V
    .locals 1

    .prologue
    .line 2730101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730102
    iget-object v0, p1, LX/JkZ;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 2730103
    iput-object v0, p0, LX/Jka;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730104
    iget-object v0, p1, LX/JkZ;->b:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 2730105
    iput-object v0, p0, LX/Jka;->b:Lcom/facebook/user/model/UserKey;

    .line 2730106
    iget-object v0, p1, LX/JkZ;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2730107
    iput-object v0, p0, LX/Jka;->c:Ljava/lang/String;

    .line 2730108
    iget-object v0, p1, LX/JkZ;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2730109
    iput-object v0, p0, LX/Jka;->d:Ljava/lang/String;

    .line 2730110
    iget-object v0, p1, LX/JkZ;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2730111
    iput-object v0, p0, LX/Jka;->e:Ljava/lang/String;

    .line 2730112
    iget-object v0, p1, LX/JkZ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2730113
    iput-object v0, p0, LX/Jka;->f:Ljava/lang/String;

    .line 2730114
    iget-object v0, p1, LX/JkZ;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2730115
    iput-object v0, p0, LX/Jka;->g:Ljava/lang/String;

    .line 2730116
    iget-object v0, p0, LX/Jka;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2730117
    return-void

    .line 2730118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
