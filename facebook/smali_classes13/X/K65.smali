.class public final LX/K65;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardEndCard;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardEndCard;)V
    .locals 0

    .prologue
    .line 2771159
    iput-object p1, p0, LX/K65;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2771160
    iget-object v0, p0, LX/K65;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->v:LX/K5i;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2771161
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2771162
    if-eqz v0, :cond_0

    .line 2771163
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2771164
    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2771165
    iget-object v0, p0, LX/K65;->a:Lcom/facebook/tarot/cards/TarotCardEndCard;

    iget-object v1, v0, Lcom/facebook/tarot/cards/TarotCardEndCard;->v:LX/K5i;

    .line 2771166
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2771167
    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->a()Z

    move-result v0

    invoke-virtual {v1, v0}, LX/K5i;->a(Z)V

    .line 2771168
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2771169
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2771170
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/K65;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
