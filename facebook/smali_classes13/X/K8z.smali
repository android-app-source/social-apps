.class public final LX/K8z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/K91;

.field private final e:C

.field private final f:I

.field private final g:LX/K90;


# direct methods
.method public constructor <init>(LX/K91;CI)V
    .locals 1

    .prologue
    .line 2776507
    sget-object v0, LX/K90;->NONE:LX/K90;

    invoke-direct {p0, p1, p2, p3, v0}, LX/K8z;-><init>(LX/K91;CILX/K90;)V

    .line 2776508
    return-void
.end method

.method public constructor <init>(LX/K91;CILX/K90;)V
    .locals 0

    .prologue
    .line 2776509
    iput-object p1, p0, LX/K8z;->d:LX/K91;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776510
    iput-char p2, p0, LX/K8z;->e:C

    .line 2776511
    iput p3, p0, LX/K8z;->f:I

    .line 2776512
    iput-object p4, p0, LX/K8z;->g:LX/K90;

    .line 2776513
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2776514
    iget-object v0, p0, LX/K8z;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2776515
    iget-object v0, p0, LX/K8z;->b:Ljava/lang/String;

    .line 2776516
    :goto_0
    return-object v0

    .line 2776517
    :cond_0
    iget-object v0, p0, LX/K8z;->d:LX/K91;

    iget-object v0, v0, LX/K91;->b:LX/0Vt;

    iget v1, p0, LX/K8z;->f:I

    invoke-virtual {v0, v1}, LX/0Vt;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K8z;->a:Ljava/lang/String;

    .line 2776518
    iget-object v0, p0, LX/K8z;->d:LX/K91;

    iget-object v0, v0, LX/K91;->a:LX/0ad;

    iget-char v1, p0, LX/K8z;->e:C

    iget-object v2, p0, LX/K8z;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K8z;->b:Ljava/lang/String;

    .line 2776519
    iget-object v0, p0, LX/K8z;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2776520
    iget-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2776521
    iget-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    .line 2776522
    :goto_0
    return-object v0

    .line 2776523
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0}, LX/K8z;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 2776524
    iget-object v1, p0, LX/K8z;->g:LX/K90;

    sget-object v2, LX/K90;->NONE:LX/K90;

    if-ne v1, v2, :cond_2

    .line 2776525
    iput-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    .line 2776526
    :cond_1
    :goto_1
    iget-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    goto :goto_0

    .line 2776527
    :cond_2
    iget-object v1, p0, LX/K8z;->g:LX/K90;

    sget-object v2, LX/K90;->ALL_CAPS:LX/K90;

    if-ne v1, v2, :cond_3

    .line 2776528
    iget-object v1, p0, LX/K8z;->d:LX/K91;

    iget-object v1, v1, LX/K91;->c:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    goto :goto_1

    .line 2776529
    :cond_3
    iget-object v1, p0, LX/K8z;->g:LX/K90;

    sget-object v2, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    if-ne v1, v2, :cond_1

    .line 2776530
    const-string v1, "[Publisher]"

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/K8z;->c:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2776531
    iget-object v0, p0, LX/K8z;->g:LX/K90;

    sget-object v1, LX/K90;->REPLACE_PUBLISHER_NAME:LX/K90;

    if-ne v0, v1, :cond_0

    .line 2776532
    iget-object v0, p0, LX/K8z;->d:LX/K91;

    iget-object v0, v0, LX/K91;->d:LX/03V;

    const-string v1, "TarotStrings"

    const-string v2, "No publisher name requested"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2776533
    invoke-direct {p0}, LX/K8z;->b()Ljava/lang/String;

    move-result-object v0

    .line 2776534
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/K8z;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2776535
    invoke-direct {p0, p1}, LX/K8z;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
