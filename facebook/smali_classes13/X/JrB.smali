.class public LX/JrB;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final l:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/2N4;

.field private final c:LX/Jrb;

.field private final d:LX/Jqb;

.field private final e:LX/Jrc;

.field private final f:LX/FO4;

.field private final g:LX/FDI;

.field private final h:LX/0SG;

.field private final i:Ljava/lang/String;

.field public final j:LX/Do7;

.field public k:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742305
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrB;->l:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FDs;LX/2N4;LX/Jrb;LX/Jqb;LX/Jrc;LX/FO4;LX/0SG;LX/0Ot;LX/FDI;Ljava/lang/String;LX/Do7;)V
    .locals 1
    .param p9    # LX/FDI;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/2N4;",
            "LX/Jrb;",
            "LX/Jqb;",
            "LX/Jrc;",
            "LX/FO4;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;",
            "LX/FDI;",
            "Ljava/lang/String;",
            "LX/Do7;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2742291
    invoke-direct {p0, p8}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742292
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742293
    iput-object v0, p0, LX/JrB;->k:LX/0Ot;

    .line 2742294
    iput-object p1, p0, LX/JrB;->a:LX/FDs;

    .line 2742295
    iput-object p2, p0, LX/JrB;->b:LX/2N4;

    .line 2742296
    iput-object p3, p0, LX/JrB;->c:LX/Jrb;

    .line 2742297
    iput-object p4, p0, LX/JrB;->d:LX/Jqb;

    .line 2742298
    iput-object p5, p0, LX/JrB;->e:LX/Jrc;

    .line 2742299
    iput-object p6, p0, LX/JrB;->f:LX/FO4;

    .line 2742300
    iput-object p9, p0, LX/JrB;->g:LX/FDI;

    .line 2742301
    iput-object p7, p0, LX/JrB;->h:LX/0SG;

    .line 2742302
    iput-object p10, p0, LX/JrB;->i:Ljava/lang/String;

    .line 2742303
    iput-object p11, p0, LX/JrB;->j:LX/Do7;

    .line 2742304
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742288
    invoke-virtual {p1}, LX/6kW;->i()LX/6kD;

    move-result-object v0

    .line 2742289
    iget-object v1, p0, LX/JrB;->e:LX/Jrc;

    iget-object v0, v0, LX/6kD;->messageMetadata:LX/6kn;

    iget-object v0, v0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742290
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrB;
    .locals 7

    .prologue
    .line 2742261
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742262
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742263
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742264
    if-nez v1, :cond_0

    .line 2742265
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742266
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742267
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742268
    sget-object v1, LX/JrB;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742269
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742270
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742271
    :cond_1
    if-nez v1, :cond_4

    .line 2742272
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742273
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742274
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JrB;->b(LX/0QB;)LX/JrB;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2742275
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742276
    if-nez v1, :cond_2

    .line 2742277
    sget-object v0, LX/JrB;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrB;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742278
    :goto_1
    if-eqz v0, :cond_3

    .line 2742279
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742280
    :goto_3
    check-cast v0, LX/JrB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742281
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742282
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742283
    :catchall_1
    move-exception v0

    .line 2742284
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742285
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742286
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742287
    :cond_2
    :try_start_8
    sget-object v0, LX/JrB;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrB;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/6kD;Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2742220
    iget-object v0, p1, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-static {v0}, LX/Jra;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2742221
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2742222
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2742223
    new-instance v5, LX/6fz;

    invoke-direct {v5}, LX/6fz;-><init>()V

    .line 2742224
    iput-object v1, v5, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2742225
    move-object v1, v5

    .line 2742226
    invoke-virtual {v1}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v1

    .line 2742227
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2742228
    :cond_0
    move-object v1, v2

    .line 2742229
    iget-object v2, p1, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-static {v2}, LX/Jra;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 2742230
    iget-object v3, p0, LX/JrB;->a:LX/FDs;

    .line 2742231
    iget-object v5, v3, LX/FDs;->f:LX/3N0;

    invoke-virtual {v5, v2}, LX/3N0;->a(Ljava/util/List;)V

    .line 2742232
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2742233
    iget-object v5, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2742234
    new-instance v7, LX/0UE;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v7, v5}, LX/0UE;-><init>(I)V

    .line 2742235
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2742236
    invoke-virtual {v5}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2742237
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2742238
    invoke-virtual {v5}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2742239
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2742240
    :cond_3
    move-object v5, v6

    .line 2742241
    iget-object v6, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v5, v6}, LX/FDs;->a(LX/FDs;Ljava/util/List;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742242
    iget-object v5, v3, LX/FDs;->d:LX/2N4;

    iget-object v6, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v8, v5

    .line 2742243
    iget-object v1, p0, LX/JrB;->c:LX/Jrb;

    .line 2742244
    iget-object v2, p1, LX/6kD;->messageMetadata:LX/6kn;

    invoke-static {v1, v2, v8}, LX/Jrb;->a(LX/Jrb;LX/6kn;Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6f7;

    move-result-object v2

    sget-object v3, LX/2uW;->ADD_MEMBERS:LX/2uW;

    .line 2742245
    iput-object v3, v2, LX/6f7;->l:LX/2uW;

    .line 2742246
    move-object v2, v2

    .line 2742247
    iput-object v0, v2, LX/6f7;->m:Ljava/util/List;

    .line 2742248
    move-object v2, v2

    .line 2742249
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2742250
    iget-object v3, v1, LX/Jrb;->f:LX/3QS;

    sget-object v5, LX/6fK;->SYNC_PROTOCOL_PARTICIPANTS_ADDED_DELTA:LX/6fK;

    invoke-virtual {v3, v5, v2}, LX/3QS;->a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2742251
    move-object v3, v2

    .line 2742252
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, LX/JrB;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742253
    iget-object v0, p0, LX/JrB;->j:LX/Do7;

    invoke-virtual {v0}, LX/Do7;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/JrB;->a:LX/FDs;

    iget-object v2, p1, LX/6kD;->messageMetadata:LX/6kn;

    invoke-static {v2}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v2

    invoke-virtual {v0, v1, p3, p4, v2}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2742254
    :goto_3
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742255
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2742256
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v3

    .line 2742257
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v4, v4

    .line 2742258
    iget-wide v9, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 2742259
    move-object v5, v8

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    return-object v1

    .line 2742260
    :cond_4
    iget-object v0, p0, LX/JrB;->a:LX/FDs;

    invoke-virtual {v0, v1, p3, p4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/JrB;
    .locals 12

    .prologue
    .line 2742306
    new-instance v0, LX/JrB;

    invoke-static {p0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v2

    check-cast v2, LX/2N4;

    invoke-static {p0}, LX/Jrb;->a(LX/0QB;)LX/Jrb;

    move-result-object v3

    check-cast v3, LX/Jrb;

    invoke-static {p0}, LX/Jqb;->a(LX/0QB;)LX/Jqb;

    move-result-object v4

    check-cast v4, LX/Jqb;

    invoke-static {p0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v5

    check-cast v5, LX/Jrc;

    invoke-static {p0}, LX/FO4;->a(LX/0QB;)LX/FO4;

    move-result-object v6

    check-cast v6, LX/FO4;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const/16 v8, 0x35bd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/FDK;->a(LX/0QB;)LX/FDI;

    move-result-object v9

    check-cast v9, LX/FDI;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {p0}, LX/Do7;->a(LX/0QB;)LX/Do7;

    move-result-object v11

    check-cast v11, LX/Do7;

    invoke-direct/range {v0 .. v11}, LX/JrB;-><init>(LX/FDs;LX/2N4;LX/Jrb;LX/Jqb;LX/Jrc;LX/FO4;LX/0SG;LX/0Ot;LX/FDI;Ljava/lang/String;LX/Do7;)V

    .line 2742307
    const/16 v1, 0xce5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742308
    iput-object v1, v0, LX/JrB;->k:LX/0Ot;

    .line 2742309
    return-object v0
.end method

.method private b(LX/6kW;)Z
    .locals 6

    .prologue
    .line 2742215
    invoke-virtual {p1}, LX/6kW;->i()LX/6kD;

    move-result-object v0

    iget-object v0, v0, LX/6kD;->addedParticipants:Ljava/util/List;

    .line 2742216
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 2742217
    iget-object v2, v0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/JrB;->i:Ljava/lang/String;

    iget-object v0, v0, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2742218
    const/4 v0, 0x1

    .line 2742219
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742209
    check-cast p1, LX/6kW;

    .line 2742210
    invoke-direct {p0, p1}, LX/JrB;->b(LX/6kW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2742211
    invoke-direct {p0, p1}, LX/JrB;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    .line 2742212
    :goto_0
    return-object v0

    .line 2742213
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742214
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742196
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->i()LX/6kD;

    move-result-object v1

    .line 2742197
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-direct {p0, v0}, LX/JrB;->b(LX/6kW;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2742198
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2742199
    :cond_0
    :goto_0
    return-object v0

    .line 2742200
    :cond_1
    iget-object v0, p0, LX/JrB;->b:LX/2N4;

    iget-object v2, p0, LX/JrB;->e:LX/Jrc;

    iget-object v3, v1, LX/6kD;->messageMetadata:LX/6kn;

    iget-object v3, v3, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v2, v3}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742201
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2742202
    if-eqz v2, :cond_0

    .line 2742203
    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-direct {p0, v1, v2, v4, v5}, LX/JrB;->a(LX/6kD;Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v1

    .line 2742204
    const-string v2, "newMessageResult"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2742205
    if-eqz v1, :cond_0

    .line 2742206
    const-string v2, "threadSummary"

    .line 2742207
    iget-object v3, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, v3

    .line 2742208
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742176
    const-string v0, "newMessageResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2742177
    if-eqz v0, :cond_1

    .line 2742178
    iget-object v1, p0, LX/JrB;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-wide v4, p2, LX/7GJ;->b:J

    iget-object v2, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v2, LX/6kW;

    invoke-virtual {v2}, LX/6kW;->i()LX/6kD;

    move-result-object v2

    iget-object v2, v2, LX/6kD;->messageMetadata:LX/6kn;

    .line 2742179
    iget-object v3, p0, LX/JrB;->j:LX/Do7;

    invoke-virtual {v3}, LX/Do7;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, LX/JrZ;->a(LX/6kn;)LX/6jT;

    move-result-object v3

    :goto_0
    move-object v2, v3

    .line 2742180
    invoke-virtual {v1, v0, v4, v5, v2}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)V

    .line 2742181
    iget-object v1, p0, LX/JrB;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2742182
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2742183
    iget-wide v6, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 2742184
    invoke-virtual {v1, v2, v4, v5}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742185
    iget-object v1, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/6kW;

    invoke-virtual {v1}, LX/6kW;->i()LX/6kD;

    move-result-object v1

    .line 2742186
    iget-object v1, v1, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-static {v1}, LX/Jra;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2742187
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2742188
    iget-object v2, p0, LX/JrB;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v1}, LX/2Oe;->a(Lcom/facebook/user/model/User;)V

    goto :goto_1

    .line 2742189
    :cond_0
    iget-object v1, p0, LX/JrB;->d:LX/Jqb;

    .line 2742190
    iget-object v2, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v2

    .line 2742191
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742192
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742193
    :cond_1
    return-void

    :cond_2
    sget-object v3, LX/6jT;->a:LX/6jT;

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742195
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrB;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2742194
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrB;->b(LX/6kW;)Z

    move-result v0

    return v0
.end method
