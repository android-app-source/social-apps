.class public LX/JY2;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/JY2;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/03V;

.field public final e:LX/189;

.field private final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/JY1;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0sa;

.field public final h:LX/0bH;


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;LX/0Ot;LX/0sa;LX/0bH;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/189;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0sa;",
            "LX/0bH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2705777
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 2705778
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/JY2;->a:Ljava/util/Set;

    .line 2705779
    iput-object p1, p0, LX/JY2;->d:LX/03V;

    .line 2705780
    iput-object p2, p0, LX/JY2;->b:LX/0tX;

    .line 2705781
    iput-object p3, p0, LX/JY2;->c:Ljava/util/concurrent/ExecutorService;

    .line 2705782
    iput-object p4, p0, LX/JY2;->e:LX/189;

    .line 2705783
    iput-object p5, p0, LX/JY2;->f:LX/1Ck;

    .line 2705784
    iput-object p7, p0, LX/JY2;->g:LX/0sa;

    .line 2705785
    iput-object p8, p0, LX/JY2;->h:LX/0bH;

    .line 2705786
    return-void
.end method

.method public static a(LX/0QB;)LX/JY2;
    .locals 12

    .prologue
    .line 2705764
    sget-object v0, LX/JY2;->i:LX/JY2;

    if-nez v0, :cond_1

    .line 2705765
    const-class v1, LX/JY2;

    monitor-enter v1

    .line 2705766
    :try_start_0
    sget-object v0, LX/JY2;->i:LX/JY2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2705767
    if-eqz v2, :cond_0

    .line 2705768
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2705769
    new-instance v3, LX/JY2;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    const/16 v9, 0x474

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v10

    check-cast v10, LX/0sa;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v11

    check-cast v11, LX/0bH;

    invoke-direct/range {v3 .. v11}, LX/JY2;-><init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;LX/0Ot;LX/0sa;LX/0bH;)V

    .line 2705770
    move-object v0, v3

    .line 2705771
    sput-object v0, LX/JY2;->i:LX/JY2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2705772
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2705773
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2705774
    :cond_1
    sget-object v0, LX/JY2;->i:LX/JY2;

    return-object v0

    .line 2705775
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2705776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)Z
    .locals 2

    .prologue
    .line 2705762
    invoke-static {p0}, LX/JY2;->c(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2705763
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 2705761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;I)Z
    .locals 4

    .prologue
    .line 2705756
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2705757
    const/4 v2, 0x1

    .line 2705758
    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 2705759
    :cond_0
    move v1, v2

    .line 2705760
    iget-object v2, p0, LX/JY2;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sub-int/2addr v0, v1

    if-lt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 2705755
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-static {p1}, LX/JY2;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 2705748
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/JY2;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)V
    .locals 5

    .prologue
    .line 2705750
    invoke-static {p1}, LX/JY2;->c(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2705751
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2705752
    :cond_0
    :goto_0
    return-void

    .line 2705753
    :cond_1
    iget-object v1, p0, LX/JY2;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2705754
    iget-object v1, p0, LX/JY2;->f:LX/1Ck;

    sget-object v2, LX/JY1;->SGNY_FETCH_MORE:LX/JY1;

    new-instance v3, LX/JXy;

    invoke-direct {v3, p0, p1, v0}, LX/JXy;-><init>(LX/JY2;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    new-instance v4, LX/JXz;

    invoke-direct {v4, p0, p1, v0}, LX/JXz;-><init>(LX/JY2;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 2705749
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {p0, p1}, LX/JY2;->b(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)V

    return-void
.end method
