.class public final LX/Jhj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jho;


# direct methods
.method public constructor <init>(LX/Jho;)V
    .locals 0

    .prologue
    .line 2724870
    iput-object p1, p0, LX/Jhj;->a:LX/Jho;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, 0x7714a5d7

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2724871
    iget-object v1, p0, LX/Jhj;->a:LX/Jho;

    invoke-static {v1}, LX/Jho;->G(LX/Jho;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2724872
    iget-object v1, p0, LX/Jhj;->a:LX/Jho;

    iget-object v1, v1, LX/Jho;->O:LX/3OO;

    .line 2724873
    iget-object v2, v1, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v1, v2

    .line 2724874
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2724875
    iget-object v1, p0, LX/Jhj;->a:LX/Jho;

    iget-object v2, p0, LX/Jhj;->a:LX/Jho;

    iget-object v2, v2, LX/Jho;->O:LX/3OO;

    .line 2724876
    iget-object v3, v2, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v2, v3

    .line 2724877
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v2

    .line 2724878
    iget-object v3, v2, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2724879
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2724880
    :cond_0
    :goto_0
    const v1, -0x309c5445

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2724881
    :cond_1
    iget-object v1, p0, LX/Jhj;->a:LX/Jho;

    iget-object v1, v1, LX/Jho;->k:LX/FCl;

    invoke-virtual {v1}, LX/FCl;->a()Z

    .line 2724882
    iget-object v1, p0, LX/Jhj;->a:LX/Jho;

    iget-object v1, v1, LX/Jho;->b:LX/3A0;

    iget-object v2, p0, LX/Jhj;->a:LX/Jho;

    invoke-virtual {v2}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Jhj;->a:LX/Jho;

    iget-object v3, v3, LX/Jho;->O:LX/3OO;

    .line 2724883
    iget-object v6, v3, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v3, v6

    .line 2724884
    iget-object v6, v3, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v6

    .line 2724885
    iget-object v6, p0, LX/Jhj;->a:LX/Jho;

    iget-object v6, v6, LX/Jho;->O:LX/3OO;

    .line 2724886
    iget-object v7, v6, LX/3OO;->F:Ljava/lang/String;

    move-object v7, v7

    .line 2724887
    const-wide/16 v8, 0x0

    move-object v6, v5

    invoke-virtual/range {v1 .. v9}, LX/3A0;->a(Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)LX/EFc;

    goto :goto_0

    .line 2724888
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CALL"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2724889
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tel:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2724890
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2724891
    :try_start_0
    iget-object v4, v1, LX/Jho;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/Jho;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2724892
    :catch_0
    move-exception v3

    .line 2724893
    sget-object v4, LX/Jho;->a:Ljava/lang/String;

    const-string v5, "Failed to open dialer for number %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v4, v3, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
