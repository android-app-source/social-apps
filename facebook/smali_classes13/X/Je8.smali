.class public final LX/Je8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jdw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jdw",
        "<",
        "Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/JeB;


# direct methods
.method public constructor <init>(LX/JeB;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2720338
    iput-object p1, p0, LX/Je8;->b:LX/JeB;

    iput-object p2, p0, LX/Je8;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JeZ;LX/Jek;)V
    .locals 5

    .prologue
    .line 2720339
    check-cast p2, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;

    const/4 v4, 0x0

    .line 2720340
    iget-object v0, p0, LX/Je8;->a:Landroid/content/Context;

    const v1, 0x7f082b4f    # 1.8099988E38f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a(Ljava/lang/String;)V

    .line 2720341
    check-cast p1, LX/Jee;

    .line 2720342
    iget-object v0, p1, LX/Jee;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 2720343
    iget-object v1, p0, LX/Je8;->b:LX/JeB;

    iget-object v1, v1, LX/JeB;->h:LX/FJW;

    invoke-interface {v1}, LX/FJW;->b()LX/2EJ;

    move-result-object v1

    .line 2720344
    new-instance v2, LX/Je6;

    invoke-direct {v2, p0, p2}, LX/Je6;-><init>(LX/Je8;Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2720345
    iget-object v2, p0, LX/Je8;->b:LX/JeB;

    .line 2720346
    iget-object v3, v2, LX/JeB;->i:LX/297;

    invoke-virtual {v3, v0}, LX/297;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v3

    move v2, v3

    .line 2720347
    new-instance v3, LX/Je7;

    invoke-direct {v3, p0, v0, v1}, LX/Je7;-><init>(LX/Je8;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/app/Dialog;)V

    invoke-virtual {p2, v2, v4, v4, v3}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;->a(ZLjava/lang/String;LX/JeV;LX/Jdt;)V

    .line 2720348
    return-void
.end method
