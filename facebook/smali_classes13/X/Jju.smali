.class public LX/Jju;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iu3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2728623
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENT_REMINDER_ENTRY_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Jju;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2728648
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 2728649
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728650
    iput-object v0, p0, LX/Jju;->b:LX/0Ot;

    .line 2728651
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2728647
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 2728646
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2728626
    if-eqz p2, :cond_0

    instance-of v0, p2, LX/Jjs;

    if-nez v0, :cond_1

    .line 2728627
    :cond_0
    :goto_0
    return-void

    .line 2728628
    :cond_1
    check-cast p2, LX/Jjs;

    .line 2728629
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2728630
    iget-object v0, p2, LX/Jjs;->c:LX/Jjt;

    move-object v0, v0

    .line 2728631
    if-eqz v0, :cond_0

    goto :goto_0

    .line 2728632
    :cond_2
    iget-object v0, p0, LX/Jju;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iu3;

    .line 2728633
    iget-object v1, p2, LX/Jjs;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2728634
    const/4 v2, 0x0

    .line 2728635
    iget-object v3, p2, LX/Jjs;->a:Landroid/os/Bundle;

    move-object v3, v3

    .line 2728636
    const-class v4, Landroid/app/Activity;

    invoke-static {p1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 2728637
    if-eqz v4, :cond_3

    .line 2728638
    const p0, 0x7f040081

    const p2, 0x7f04008d

    invoke-virtual {v4, p0, p2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2728639
    :cond_3
    invoke-static {v0, v1}, LX/Iu3;->a(LX/Iu3;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v4

    .line 2728640
    const-string p0, "trigger"

    invoke-virtual {v4, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2728641
    const-string p0, "prefer_chat_if_possible"

    const/4 p2, 0x0

    invoke-virtual {v4, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2728642
    if-eqz v3, :cond_4

    .line 2728643
    invoke-virtual {v4, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2728644
    :cond_4
    iget-object p0, v0, LX/Iu3;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2728645
    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2728625
    const-string v0, "4355"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2728624
    sget-object v0, LX/Jju;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
