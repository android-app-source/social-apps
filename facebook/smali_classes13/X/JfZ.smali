.class public final LX/JfZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jff;

.field public final synthetic b:LX/Jfc;


# direct methods
.method public constructor <init>(LX/Jfc;LX/Jff;)V
    .locals 0

    .prologue
    .line 2722062
    iput-object p1, p0, LX/JfZ;->b:LX/Jfc;

    iput-object p2, p0, LX/JfZ;->a:LX/Jff;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2722063
    iget-object v0, p0, LX/JfZ;->a:LX/Jff;

    .line 2722064
    iget-object v1, v0, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-static {v1}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d$redex0(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722065
    iget-object v1, v0, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->m:LX/IZ9;

    if-eqz v1, :cond_0

    .line 2722066
    iget-object v1, v0, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->m:LX/IZ9;

    invoke-interface {v1}, LX/IZ9;->b()V

    .line 2722067
    :cond_0
    iget-object v0, p0, LX/JfZ;->b:LX/Jfc;

    iget-object v0, v0, LX/Jfc;->a:LX/03V;

    const-string v1, "ReviewTaskManager"

    const-string v2, "Messenger platform bot review graphql query fails"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722068
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2722069
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2722070
    iget-object v1, p0, LX/JfZ;->a:LX/Jff;

    .line 2722071
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2722072
    check-cast v0, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel;

    const/4 p1, 0x0

    const/4 p0, 0x1

    .line 2722073
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d$redex0(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    .line 2722074
    if-nez v0, :cond_1

    .line 2722075
    :cond_0
    :goto_0
    return-void

    .line 2722076
    :cond_1
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2722077
    iput-object v3, v2, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    .line 2722078
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    iget-object v3, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    const v4, 0x7f083bf3

    new-array v5, p0, [Ljava/lang/Object;

    iget-object v6, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v6, v6, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    aput-object v6, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2722079
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    const v4, 0x7f083bf6

    new-array v5, p0, [Ljava/lang/Object;

    iget-object v6, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v6, v6, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k:Ljava/lang/String;

    aput-object v6, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2722080
    invoke-virtual {v0}, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel;->k()Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel$ViewerMessengerPlatformBotReviewModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2722081
    invoke-virtual {v0}, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel;->k()Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel$ViewerMessengerPlatformBotReviewModel;

    move-result-object v2

    .line 2722082
    iget-object v3, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->d:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel$ViewerMessengerPlatformBotReviewModel;->j()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->setRating(I)V

    .line 2722083
    iget-object v3, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/review/graphql/ReviewQueryFragmentsModels$MessengerPlatformBotReviewQueryModel$ViewerMessengerPlatformBotReviewModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2722084
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->i:Landroid/view/MenuItem;

    iget-object v3, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    const v4, 0x7f083bf5

    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2722085
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    .line 2722086
    iput-boolean p0, v2, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->l:Z

    .line 2722087
    iget-object v2, v1, LX/Jff;->a:Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;->k(Lcom/facebook/messaging/business/review/view/ReviewUpdateFragment;)V

    goto :goto_0
.end method
