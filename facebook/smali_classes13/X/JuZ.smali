.class public final LX/JuZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FJf;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/notify/IncomingCallNotification;

.field public final synthetic b:J

.field public final synthetic c:LX/3RG;


# direct methods
.method public constructor <init>(LX/3RG;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V
    .locals 1

    .prologue
    .line 2748967
    iput-object p1, p0, LX/JuZ;->c:LX/3RG;

    iput-object p2, p0, LX/JuZ;->a:Lcom/facebook/messaging/notify/IncomingCallNotification;

    iput-wide p3, p0, LX/JuZ;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2748968
    iget-object v0, p0, LX/JuZ;->c:LX/3RG;

    iget-object v1, p0, LX/JuZ;->a:Lcom/facebook/messaging/notify/IncomingCallNotification;

    iget-wide v2, p0, LX/JuZ;->b:J

    invoke-static {v0, p1, v1, v2, v3}, LX/3RG;->a$redex0(LX/3RG;Landroid/graphics/Bitmap;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V

    .line 2748969
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2748970
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JuZ;->a(Landroid/graphics/Bitmap;)V

    .line 2748971
    return-void
.end method

.method public final a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2748972
    const/4 v0, 0x0

    .line 2748973
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-eqz v1, :cond_0

    .line 2748974
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2748975
    :cond_0
    invoke-direct {p0, v0}, LX/JuZ;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748976
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 2748977
    return-void

    .line 2748978
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    throw v0
.end method
