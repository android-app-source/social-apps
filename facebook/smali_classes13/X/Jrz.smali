.class public final enum LX/Jrz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jrz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jrz;

.field public static final enum DEFAULT:LX/Jrz;

.field public static final enum DEFAULT_WITH_DIVIDER:LX/Jrz;

.field public static final enum ONLY_WITH_NEWER_ROW:LX/Jrz;

.field public static final enum ONLY_WITH_NEWER_ROW_WITH_DIVIDER:LX/Jrz;

.field public static final enum ONLY_WITH_OLDER_ROW:LX/Jrz;

.field public static final enum ONLY_WITH_OLDER_ROW_WITH_DIVIDER:LX/Jrz;

.field public static final enum WITH_OLDER_AND_NEW_ROWS:LX/Jrz;

.field public static final enum WITH_OLDER_AND_NEW_ROWS_AND_DIVIDER:LX/Jrz;


# instance fields
.field public final groupWithNewerRow:Z

.field public final groupWithOlderRow:Z

.field public final hasTimestapDividerAbove:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2744948
    new-instance v0, LX/Jrz;

    const-string v1, "DEFAULT"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, LX/Jrz;->DEFAULT:LX/Jrz;

    .line 2744949
    new-instance v3, LX/Jrz;

    const-string v4, "DEFAULT_WITH_DIVIDER"

    move v5, v9

    move v6, v2

    move v7, v2

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->DEFAULT_WITH_DIVIDER:LX/Jrz;

    .line 2744950
    new-instance v3, LX/Jrz;

    const-string v4, "ONLY_WITH_NEWER_ROW"

    move v5, v10

    move v6, v9

    move v7, v2

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->ONLY_WITH_NEWER_ROW:LX/Jrz;

    .line 2744951
    new-instance v3, LX/Jrz;

    const-string v4, "ONLY_WITH_NEWER_ROW_WITH_DIVIDER"

    move v5, v11

    move v6, v9

    move v7, v2

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->ONLY_WITH_NEWER_ROW_WITH_DIVIDER:LX/Jrz;

    .line 2744952
    new-instance v3, LX/Jrz;

    const-string v4, "ONLY_WITH_OLDER_ROW"

    move v5, v12

    move v6, v2

    move v7, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->ONLY_WITH_OLDER_ROW:LX/Jrz;

    .line 2744953
    new-instance v3, LX/Jrz;

    const-string v4, "ONLY_WITH_OLDER_ROW_WITH_DIVIDER"

    const/4 v5, 0x5

    move v6, v2

    move v7, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->ONLY_WITH_OLDER_ROW_WITH_DIVIDER:LX/Jrz;

    .line 2744954
    new-instance v3, LX/Jrz;

    const-string v4, "WITH_OLDER_AND_NEW_ROWS"

    const/4 v5, 0x6

    move v6, v9

    move v7, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS:LX/Jrz;

    .line 2744955
    new-instance v3, LX/Jrz;

    const-string v4, "WITH_OLDER_AND_NEW_ROWS_AND_DIVIDER"

    const/4 v5, 0x7

    move v6, v9

    move v7, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/Jrz;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS_AND_DIVIDER:LX/Jrz;

    .line 2744956
    const/16 v0, 0x8

    new-array v0, v0, [LX/Jrz;

    sget-object v1, LX/Jrz;->DEFAULT:LX/Jrz;

    aput-object v1, v0, v2

    sget-object v1, LX/Jrz;->DEFAULT_WITH_DIVIDER:LX/Jrz;

    aput-object v1, v0, v9

    sget-object v1, LX/Jrz;->ONLY_WITH_NEWER_ROW:LX/Jrz;

    aput-object v1, v0, v10

    sget-object v1, LX/Jrz;->ONLY_WITH_NEWER_ROW_WITH_DIVIDER:LX/Jrz;

    aput-object v1, v0, v11

    sget-object v1, LX/Jrz;->ONLY_WITH_OLDER_ROW:LX/Jrz;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LX/Jrz;->ONLY_WITH_OLDER_ROW_WITH_DIVIDER:LX/Jrz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS:LX/Jrz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS_AND_DIVIDER:LX/Jrz;

    aput-object v2, v0, v1

    sput-object v0, LX/Jrz;->$VALUES:[LX/Jrz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    .line 2744957
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2744958
    iput-boolean p3, p0, LX/Jrz;->groupWithNewerRow:Z

    .line 2744959
    iput-boolean p4, p0, LX/Jrz;->groupWithOlderRow:Z

    .line 2744960
    iput-boolean p5, p0, LX/Jrz;->hasTimestapDividerAbove:Z

    .line 2744961
    return-void
.end method

.method public static forGrouping(ZZZ)LX/Jrz;
    .locals 1

    .prologue
    .line 2744939
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 2744940
    if-eqz p2, :cond_0

    sget-object v0, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS_AND_DIVIDER:LX/Jrz;

    .line 2744941
    :goto_0
    return-object v0

    .line 2744942
    :cond_0
    sget-object v0, LX/Jrz;->WITH_OLDER_AND_NEW_ROWS:LX/Jrz;

    goto :goto_0

    .line 2744943
    :cond_1
    if-eqz p0, :cond_3

    .line 2744944
    if-eqz p2, :cond_2

    sget-object v0, LX/Jrz;->ONLY_WITH_NEWER_ROW_WITH_DIVIDER:LX/Jrz;

    goto :goto_0

    :cond_2
    sget-object v0, LX/Jrz;->ONLY_WITH_NEWER_ROW:LX/Jrz;

    goto :goto_0

    .line 2744945
    :cond_3
    if-eqz p1, :cond_5

    .line 2744946
    if-eqz p2, :cond_4

    sget-object v0, LX/Jrz;->ONLY_WITH_OLDER_ROW_WITH_DIVIDER:LX/Jrz;

    goto :goto_0

    :cond_4
    sget-object v0, LX/Jrz;->ONLY_WITH_OLDER_ROW:LX/Jrz;

    goto :goto_0

    .line 2744947
    :cond_5
    if-eqz p2, :cond_6

    sget-object v0, LX/Jrz;->DEFAULT_WITH_DIVIDER:LX/Jrz;

    goto :goto_0

    :cond_6
    sget-object v0, LX/Jrz;->DEFAULT:LX/Jrz;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jrz;
    .locals 1

    .prologue
    .line 2744937
    const-class v0, LX/Jrz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jrz;

    return-object v0
.end method

.method public static values()[LX/Jrz;
    .locals 1

    .prologue
    .line 2744938
    sget-object v0, LX/Jrz;->$VALUES:[LX/Jrz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jrz;

    return-object v0
.end method
