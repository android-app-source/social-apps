.class public LX/JXP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/LinearLayout;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/view/View;

.field public h:Landroid/widget/TextView;

.field public i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2704493
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2704494
    const p1, 0x7f0311cd

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2704495
    const p1, 0x7f0d29cc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/JXP;->b:Landroid/view/View;

    .line 2704496
    const p1, 0x7f0d29cf

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/JXP;->c:Landroid/widget/TextView;

    .line 2704497
    const p1, 0x7f0d29d0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/JXP;->d:Landroid/widget/TextView;

    .line 2704498
    const p1, 0x7f0d29d4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    .line 2704499
    const p1, 0x7f0d29d5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/JXP;->f:Landroid/widget/TextView;

    .line 2704500
    const p1, 0x7f0d29d6

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/JXP;->g:Landroid/view/View;

    .line 2704501
    const p1, 0x7f0d29d7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/JXP;->h:Landroid/widget/TextView;

    .line 2704502
    const p1, 0x7f0d29d3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/JXP;->e:Landroid/widget/TextView;

    .line 2704503
    const p1, 0x7f0d29d2

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    iput-object p1, p0, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    .line 2704504
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2704488
    iget-object v0, p0, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setVisibility(I)V

    .line 2704489
    iget-object v0, p0, LX/JXP;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2704490
    iget-object v0, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2704491
    iget-object v0, p0, LX/JXP;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2704492
    return-void
.end method

.method public setCallToActionViewListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2704486
    iget-object v0, p0, LX/JXP;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704487
    return-void
.end method

.method public setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2704473
    iget-object v0, p0, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704474
    return-void
.end method

.method public setQuestionHint(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2704484
    iget-object v0, p0, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setQuestionHint(Ljava/lang/String;)V

    .line 2704485
    return-void
.end method

.method public setVoteButtonActive(Z)V
    .locals 2

    .prologue
    .line 2704481
    iget-object v1, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2704482
    return-void

    .line 2704483
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVoteButtonEnabled(Z)V
    .locals 2

    .prologue
    .line 2704477
    iget-object v0, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2704478
    iget-object v1, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 2704479
    return-void

    .line 2704480
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method

.method public setVoteButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2704475
    iget-object v0, p0, LX/JXP;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2704476
    return-void
.end method
