.class public final LX/Jxc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/Jxl;

.field public final synthetic b:Lcom/facebook/fbui/glyph/GlyphButton;

.field public final synthetic c:LX/Jxf;


# direct methods
.method public constructor <init>(LX/Jxf;LX/Jxl;Lcom/facebook/fbui/glyph/GlyphButton;)V
    .locals 0

    .prologue
    .line 2753228
    iput-object p1, p0, LX/Jxc;->c:LX/Jxf;

    iput-object p2, p0, LX/Jxc;->a:LX/Jxl;

    iput-object p3, p0, LX/Jxc;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3

    .prologue
    .line 2753229
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2753230
    iget-object v1, p0, LX/Jxc;->a:LX/Jxl;

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2753231
    iput v0, v1, LX/Jxl;->c:I

    .line 2753232
    iget-object v1, p0, LX/Jxc;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v2, p0, LX/Jxc;->a:LX/Jxl;

    .line 2753233
    iget v0, v2, LX/Jxl;->c:I

    move v0, v0

    .line 2753234
    if-eqz v0, :cond_0

    .line 2753235
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2753236
    :goto_0
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2753237
    :goto_1
    iget-object v0, p0, LX/Jxc;->c:LX/Jxf;

    iget-object v0, v0, LX/Jxf;->c:LX/JxW;

    invoke-virtual {v0}, LX/JxW;->a()V

    .line 2753238
    return-void

    :catch_0
    goto :goto_1

    .line 2753239
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method
