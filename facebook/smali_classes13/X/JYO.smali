.class public LX/JYO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JYS;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JYO",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JYS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2706212
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2706213
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JYO;->b:LX/0Zi;

    .line 2706214
    iput-object p1, p0, LX/JYO;->a:LX/0Ot;

    .line 2706215
    return-void
.end method

.method public static a(LX/0QB;)LX/JYO;
    .locals 4

    .prologue
    .line 2706216
    const-class v1, LX/JYO;

    monitor-enter v1

    .line 2706217
    :try_start_0
    sget-object v0, LX/JYO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2706218
    sput-object v2, LX/JYO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2706219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2706220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2706221
    new-instance v3, LX/JYO;

    const/16 p0, 0x2176

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JYO;-><init>(LX/0Ot;)V

    .line 2706222
    move-object v0, v3

    .line 2706223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2706224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JYO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2706226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2706227
    check-cast p2, LX/JYM;

    .line 2706228
    iget-object v0, p0, LX/JYO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JYS;

    iget-object v1, p2, LX/JYM;->a:LX/1Pq;

    iget-object v2, p2, LX/JYM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v3, p2, LX/JYM;->c:I

    .line 2706229
    new-instance v5, LX/JYR;

    invoke-direct {v5, v0, v2}, LX/JYR;-><init>(LX/JYS;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2706230
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v6

    .line 2706231
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2706232
    check-cast v4, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v4

    .line 2706233
    iput-object v4, v6, LX/3mP;->d:LX/25L;

    .line 2706234
    move-object v6, v6

    .line 2706235
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2706236
    check-cast v4, LX/0jW;

    .line 2706237
    iput-object v4, v6, LX/3mP;->e:LX/0jW;

    .line 2706238
    move-object v4, v6

    .line 2706239
    const/16 v6, 0x8

    .line 2706240
    iput v6, v4, LX/3mP;->b:I

    .line 2706241
    move-object v4, v4

    .line 2706242
    iput-object v5, v4, LX/3mP;->g:LX/25K;

    .line 2706243
    move-object v4, v4

    .line 2706244
    invoke-virtual {v4}, LX/3mP;->a()LX/25M;

    move-result-object v10

    .line 2706245
    iget-object v4, v0, LX/JYS;->c:LX/JYQ;

    invoke-static {v0, v2, p1, v3}, LX/JYS;->a(LX/JYS;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;I)LX/0Px;

    move-result-object v6

    new-instance v7, LX/JYG;

    .line 2706246
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2706247
    check-cast v5, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iget-object v8, v0, LX/JYS;->d:LX/JY2;

    invoke-direct {v7, v5, v8}, LX/JYG;-><init>(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;LX/JY2;)V

    move-object v5, p1

    move-object v8, v1

    move-object v9, v2

    invoke-virtual/range {v4 .. v10}, LX/JYQ;->a(Landroid/content/Context;LX/0Px;LX/3mU;Ljava/lang/Object;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)LX/JYP;

    move-result-object v4

    .line 2706248
    iget-object v5, v0, LX/JYS;->b:LX/3mL;

    invoke-virtual {v5, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const/4 v6, 0x2

    invoke-interface {v4, v5, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2706249
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2706250
    invoke-static {}, LX/1dS;->b()V

    .line 2706251
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2706252
    check-cast p1, LX/JYM;

    .line 2706253
    check-cast p2, LX/JYM;

    .line 2706254
    iget v0, p1, LX/JYM;->c:I

    iput v0, p2, LX/JYM;->c:I

    .line 2706255
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2706256
    check-cast p2, LX/JYM;

    .line 2706257
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2706258
    iget-object v0, p0, LX/JYO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2706259
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2706260
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2706261
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2706262
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/JYM;->c:I

    .line 2706263
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2706264
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2706265
    const/4 v0, 0x1

    return v0
.end method
