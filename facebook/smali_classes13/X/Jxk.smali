.class public final enum LX/Jxk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jxk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jxk;

.field public static final enum ENTERTAINING:LX/Jxk;

.field public static final enum MAIN:LX/Jxk;

.field public static final enum MISLEADING:LX/Jxk;

.field public static final enum OFFENSIVE:LX/Jxk;

.field public static final enum RELEVANT:LX/Jxk;

.field public static final enum USEFUL:LX/Jxk;

.field public static final enum WHY:LX/Jxk;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2753343
    new-instance v0, LX/Jxk;

    const-string v1, "RELEVANT"

    invoke-direct {v0, v1, v3}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->RELEVANT:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "USEFUL"

    invoke-direct {v0, v1, v4}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->USEFUL:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "ENTERTAINING"

    invoke-direct {v0, v1, v5}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->ENTERTAINING:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "OFFENSIVE"

    invoke-direct {v0, v1, v6}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->OFFENSIVE:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "MISLEADING"

    invoke-direct {v0, v1, v7}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->MISLEADING:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "MAIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->MAIN:LX/Jxk;

    new-instance v0, LX/Jxk;

    const-string v1, "WHY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Jxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jxk;->WHY:LX/Jxk;

    .line 2753344
    const/4 v0, 0x7

    new-array v0, v0, [LX/Jxk;

    sget-object v1, LX/Jxk;->RELEVANT:LX/Jxk;

    aput-object v1, v0, v3

    sget-object v1, LX/Jxk;->USEFUL:LX/Jxk;

    aput-object v1, v0, v4

    sget-object v1, LX/Jxk;->ENTERTAINING:LX/Jxk;

    aput-object v1, v0, v5

    sget-object v1, LX/Jxk;->OFFENSIVE:LX/Jxk;

    aput-object v1, v0, v6

    sget-object v1, LX/Jxk;->MISLEADING:LX/Jxk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Jxk;->MAIN:LX/Jxk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Jxk;->WHY:LX/Jxk;

    aput-object v2, v0, v1

    sput-object v0, LX/Jxk;->$VALUES:[LX/Jxk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2753345
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jxk;
    .locals 1

    .prologue
    .line 2753346
    const-class v0, LX/Jxk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jxk;

    return-object v0
.end method

.method public static values()[LX/Jxk;
    .locals 1

    .prologue
    .line 2753347
    sget-object v0, LX/Jxk;->$VALUES:[LX/Jxk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jxk;

    return-object v0
.end method
