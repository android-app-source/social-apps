.class public final LX/Jd2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2UV;


# direct methods
.method public constructor <init>(LX/2UV;)V
    .locals 0

    .prologue
    .line 2718772
    iput-object p1, p0, LX/Jd2;->a:LX/2UV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2718773
    iget-object v0, p0, LX/Jd2;->a:LX/2UV;

    iget-object v0, v0, LX/2UV;->f:LX/2UX;

    const-string v1, "mswitchaccounts_unseen_fetch_failure"

    invoke-virtual {v0, v1}, LX/2UX;->a(Ljava/lang/String;)V

    .line 2718774
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2718775
    iget-object v0, p0, LX/Jd2;->a:LX/2UV;

    iget-object v0, v0, LX/2UV;->f:LX/2UX;

    const-string v1, "mswitchaccounts_unseen_fetch_success"

    invoke-virtual {v0, v1}, LX/2UX;->a(Ljava/lang/String;)V

    .line 2718776
    iget-object v0, p0, LX/Jd2;->a:LX/2UV;

    iget-object v0, v0, LX/2UV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Vv;->l:LX/0Tn;

    iget-object v2, p0, LX/Jd2;->a:LX/2UV;

    iget-object v2, v2, LX/2UV;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2718777
    return-void
.end method
