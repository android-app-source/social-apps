.class public LX/JZa;
.super LX/Bst;
.source ""


# instance fields
.field private c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2708798
    invoke-direct {p0, p1}, LX/Bst;-><init>(Landroid/content/Context;)V

    .line 2708799
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/JZa;->c:Landroid/graphics/Paint;

    .line 2708800
    iget-object v0, p0, LX/JZa;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JZa;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2708801
    iget-object v0, p0, LX/JZa;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2708802
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2708803
    invoke-super {p0, p1}, LX/Bst;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2708804
    invoke-virtual {p0}, LX/JZa;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 2708805
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, LX/JZa;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/JZa;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2708806
    return-void
.end method
