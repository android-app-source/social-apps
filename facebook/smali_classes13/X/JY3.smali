.class public final LX/JY3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JY6;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

.field public final synthetic d:LX/1Pq;

.field public final synthetic e:LX/JY7;


# direct methods
.method public constructor <init>(LX/JY7;LX/JY6;ZLcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2705787
    iput-object p1, p0, LX/JY3;->e:LX/JY7;

    iput-object p2, p0, LX/JY3;->a:LX/JY6;

    iput-boolean p3, p0, LX/JY3;->b:Z

    iput-object p4, p0, LX/JY3;->c:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iput-object p5, p0, LX/JY3;->d:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2705788
    iget-object v0, p0, LX/JY3;->e:LX/JY7;

    iget-object v0, v0, LX/JY7;->c:LX/3mG;

    invoke-virtual {v0, p1}, LX/3mG;->a(Ljava/lang/Throwable;)V

    .line 2705789
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2705790
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2705791
    iget-object v3, p0, LX/JY3;->a:LX/JY6;

    iget-boolean v0, p0, LX/JY3;->b:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 2705792
    :goto_0
    iput-boolean v0, v3, LX/JY6;->a:Z

    .line 2705793
    iget-object v0, p0, LX/JY3;->c:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iget-object v3, p0, LX/JY3;->c:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v0, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 2705794
    iget-object v0, p0, LX/JY3;->d:LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, LX/JY3;->c:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2705795
    return-void

    :cond_0
    move v0, v2

    .line 2705796
    goto :goto_0
.end method
