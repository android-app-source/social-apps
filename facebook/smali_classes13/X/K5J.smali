.class public final LX/K5J;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/0lF;

.field public final synthetic e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

.field public final synthetic f:LX/0ew;

.field public final synthetic g:LX/K5K;


# direct methods
.method public constructor <init>(LX/K5K;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/0lF;Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;LX/0ew;)V
    .locals 0

    .prologue
    .line 2770229
    iput-object p1, p0, LX/K5J;->g:LX/K5K;

    iput-object p2, p0, LX/K5J;->a:Ljava/lang/String;

    iput-object p3, p0, LX/K5J;->b:Ljava/util/List;

    iput-object p4, p0, LX/K5J;->c:Ljava/lang/String;

    iput-object p5, p0, LX/K5J;->d:LX/0lF;

    iput-object p6, p0, LX/K5J;->e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    iput-object p7, p0, LX/K5J;->f:LX/0ew;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2770230
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2770231
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2770232
    if-eqz p1, :cond_0

    .line 2770233
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2770234
    if-eqz v0, :cond_0

    .line 2770235
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2770236
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2770237
    :cond_0
    :goto_0
    return-void

    .line 2770238
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2770239
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2770240
    move-object v2, v0

    check-cast v2, Ljava/util/List;

    .line 2770241
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    move v1, v3

    .line 2770242
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2770243
    new-instance v4, LX/K5t;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-direct {v4, v0}, LX/K5t;-><init>(Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;)V

    .line 2770244
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2770245
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2770246
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2770247
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_7

    :cond_3
    sget-object v8, LX/K73;->NONE:LX/K73;

    .line 2770248
    :goto_2
    new-instance v0, LX/K79;

    iget-object v1, p0, LX/K5J;->a:Ljava/lang/String;

    const/4 v3, 0x0

    .line 2770249
    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_4
    move v4, v3

    .line 2770250
    :cond_5
    :goto_3
    move v2, v4

    .line 2770251
    int-to-long v2, v2

    iget-object v4, p0, LX/K5J;->b:Ljava/util/List;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iget-object v6, p0, LX/K5J;->c:Ljava/lang/String;

    iget-object v7, p0, LX/K5J;->d:LX/0lF;

    invoke-direct/range {v0 .. v8}, LX/K79;-><init>(Ljava/lang/String;JLjava/util/List;LX/0Px;Ljava/lang/String;LX/0lF;LX/K73;)V

    .line 2770252
    iget-object v1, p0, LX/K5J;->e:Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2770253
    iput-object v0, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->G:LX/K79;

    .line 2770254
    iget-object v2, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    if-eqz v2, :cond_6

    .line 2770255
    iget-object v2, v1, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;->o:LX/K6a;

    .line 2770256
    iput-object v0, v2, LX/K6a;->h:LX/K79;

    .line 2770257
    :cond_6
    iget-object v1, p0, LX/K5J;->g:LX/K5K;

    iget-object v1, v1, LX/K5K;->f:LX/K7A;

    .line 2770258
    iput-object v0, v1, LX/K7A;->a:LX/K79;

    .line 2770259
    iget-object v0, p0, LX/K5J;->g:LX/K5K;

    iget-object v0, v0, LX/K5K;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;

    invoke-direct {v1, p0, v9}, Lcom/facebook/tarot/TarotExternalIntentHandler$1$1;-><init>(LX/K5J;Ljava/util/List;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2770260
    :cond_7
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2770261
    if-nez v0, :cond_b

    .line 2770262
    sget-object v3, LX/K73;->NONE:LX/K73;

    .line 2770263
    :goto_4
    move-object v3, v3

    .line 2770264
    move-object v8, v3

    .line 2770265
    goto :goto_2

    .line 2770266
    :cond_8
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :cond_9
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;

    .line 2770267
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 2770268
    invoke-virtual {v3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->n()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/2addr v3, v4

    .line 2770269
    add-int/lit8 v3, v3, 0x1

    move v4, v3

    .line 2770270
    goto :goto_5

    .line 2770271
    :cond_a
    if-lez v4, :cond_5

    .line 2770272
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2770273
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    .line 2770274
    invoke-virtual {v0}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel;->o()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel;->n()Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/tarot/graphql/TarotFragmentsQueryModels$FBTarotDigestModel$DigestOwnerModel$TarotPublisherInfoModel;->a()Z

    move-result v4

    .line 2770275
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v3, v1, :cond_c

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v1, :cond_d

    .line 2770276
    :cond_c
    sget-object v3, LX/K73;->NOT_FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    goto :goto_4

    .line 2770277
    :cond_d
    if-nez v4, :cond_e

    .line 2770278
    sget-object v3, LX/K73;->FOLLOWING_NOT_SUBSCRIBED:LX/K73;

    goto :goto_4

    .line 2770279
    :cond_e
    sget-object v3, LX/K73;->FOLLOWING_AND_SUBSCRIBED:LX/K73;

    goto :goto_4
.end method
