.class public final LX/Jze;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/Uri;

.field public final c:LX/5pW;

.field private final d:LX/Jzc;


# direct methods
.method public constructor <init>(LX/5pX;Landroid/net/Uri;LX/Jzc;LX/5pW;)V
    .locals 0

    .prologue
    .line 2756394
    invoke-direct {p0, p1}, LX/5p8;-><init>(LX/5pX;)V

    .line 2756395
    iput-object p1, p0, LX/Jze;->a:Landroid/content/Context;

    .line 2756396
    iput-object p2, p0, LX/Jze;->b:Landroid/net/Uri;

    .line 2756397
    iput-object p4, p0, LX/Jze;->c:LX/5pW;

    .line 2756398
    iput-object p3, p0, LX/Jze;->d:LX/Jzc;

    .line 2756399
    return-void
.end method

.method private varargs a()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2756400
    new-instance v8, Ljava/io/File;

    iget-object v0, p0, LX/Jze;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2756401
    :try_start_0
    iget-object v0, p0, LX/Jze;->d:LX/Jzc;

    sget-object v1, LX/Jzc;->PHOTO:LX/Jzc;

    if-ne v0, v1, :cond_1

    sget-object v0, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v7, v0

    .line 2756402
    :goto_0
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 2756403
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2756404
    iget-object v0, p0, LX/Jze;->c:LX/5pW;

    const-string v1, "E_UNABLE_TO_LOAD"

    const-string v3, "External media storage directory not available"

    invoke-interface {v0, v1, v3}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2756405
    :cond_0
    :goto_1
    return-void

    .line 2756406
    :cond_1
    sget-object v0, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    .line 2756407
    :cond_2
    new-instance v5, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v7, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2756408
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2756409
    const/16 v0, 0x2e

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_3

    .line 2756410
    const/4 v0, 0x0

    const/16 v1, 0x2e

    invoke-virtual {v3, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2756411
    const/16 v0, 0x2e

    invoke-virtual {v3, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move v3, v4

    move-object v6, v5

    .line 2756412
    :goto_2
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2756413
    new-instance v5, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v7, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move v3, v4

    move-object v6, v5

    goto :goto_2

    .line 2756414
    :cond_3
    const-string v0, ""

    move-object v1, v3

    move-object v6, v5

    move v3, v4

    goto :goto_2

    .line 2756415
    :cond_4
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2756416
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2756417
    const-wide/16 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 2756418
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 2756419
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 2756420
    iget-object v2, p0, LX/Jze;->a:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x0

    new-instance v5, LX/Jzd;

    invoke-direct {v5, p0}, LX/Jzd;-><init>(LX/Jze;)V

    invoke-static {v2, v3, v4, v5}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2756421
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2756422
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2756423
    :cond_5
    :goto_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2756424
    :try_start_4
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 2756425
    :catch_0
    move-exception v0

    .line 2756426
    const-string v1, "React"

    const-string v2, "Could not close output channel"

    invoke-static {v1, v2, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2756427
    :catch_1
    move-exception v1

    .line 2756428
    const-string v2, "React"

    const-string v3, "Could not close input channel"

    invoke-static {v2, v3, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2756429
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 2756430
    :goto_4
    :try_start_5
    iget-object v3, p0, LX/Jze;->c:LX/5pW;

    invoke-interface {v3, v0}, LX/5pW;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 2756431
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2756432
    :try_start_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 2756433
    :cond_6
    :goto_5
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756434
    :try_start_7
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_1

    .line 2756435
    :catch_3
    move-exception v0

    .line 2756436
    const-string v1, "React"

    const-string v2, "Could not close output channel"

    invoke-static {v1, v2, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2756437
    :catch_4
    move-exception v0

    .line 2756438
    const-string v2, "React"

    const-string v3, "Could not close input channel"

    invoke-static {v2, v3, v0}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 2756439
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2756440
    :try_start_8
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 2756441
    :cond_7
    :goto_7
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2756442
    :try_start_9
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 2756443
    :cond_8
    :goto_8
    throw v0

    .line 2756444
    :catch_5
    move-exception v1

    .line 2756445
    const-string v3, "React"

    const-string v4, "Could not close input channel"

    invoke-static {v3, v4, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 2756446
    :catch_6
    move-exception v1

    .line 2756447
    const-string v2, "React"

    const-string v3, "Could not close output channel"

    invoke-static {v2, v3, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 2756448
    :catchall_1
    move-exception v0

    goto :goto_6

    :catchall_2
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v9, v1

    move-object v1, v2

    move-object v2, v9

    goto :goto_6

    .line 2756449
    :catch_7
    move-exception v0

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    goto :goto_4

    :catch_8
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_4
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2756450
    invoke-direct {p0}, LX/Jze;->a()V

    return-void
.end method
