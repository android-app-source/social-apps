.class public final LX/K1f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesInterfaces$MediaFetchFromReactionStory;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2762395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2762396
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2762397
    if-eqz p1, :cond_0

    .line 2762398
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2762399
    if-nez v0, :cond_1

    .line 2762400
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2762401
    :goto_0
    return-object v0

    .line 2762402
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2762403
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;

    .line 2762404
    if-nez v0, :cond_3

    .line 2762405
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2762406
    :goto_1
    move-object v1, v1

    .line 2762407
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2762408
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;

    const/4 v2, 0x0

    .line 2762409
    if-nez v0, :cond_8

    .line 2762410
    :cond_2
    :goto_2
    move-object v0, v2

    .line 2762411
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2762412
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v1

    .line 2762413
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2762414
    :cond_4
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2762415
    goto :goto_1

    .line 2762416
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2762417
    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_7

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;

    .line 2762418
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kD;

    move-result-object p0

    if-eqz p0, :cond_6

    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kD;

    move-result-object p0

    invoke-interface {p0}, LX/5kD;->e()LX/1Fb;

    move-result-object p0

    if-eqz p0, :cond_6

    .line 2762419
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kD;

    move-result-object v1

    invoke-static {v1}, LX/8I6;->a(LX/5kD;)LX/8IH;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(LX/8IH;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2762420
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2762421
    :cond_7
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_1

    .line 2762422
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v3

    .line 2762423
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2762424
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->b()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v2

    .line 2762425
    new-instance v3, LX/17L;

    invoke-direct {v3}, LX/17L;-><init>()V

    invoke-interface {v2}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v4

    .line 2762426
    iput-object v4, v3, LX/17L;->f:Ljava/lang/String;

    .line 2762427
    move-object v3, v3

    .line 2762428
    invoke-interface {v2}, LX/0us;->a()Ljava/lang/String;

    move-result-object v4

    .line 2762429
    iput-object v4, v3, LX/17L;->c:Ljava/lang/String;

    .line 2762430
    move-object v3, v3

    .line 2762431
    invoke-interface {v2}, LX/0us;->c()Z

    move-result v4

    .line 2762432
    iput-boolean v4, v3, LX/17L;->e:Z

    .line 2762433
    move-object v3, v3

    .line 2762434
    invoke-interface {v2}, LX/0us;->b()Z

    move-result v2

    .line 2762435
    iput-boolean v2, v3, LX/17L;->d:Z

    .line 2762436
    move-object v2, v3

    .line 2762437
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_2
.end method
