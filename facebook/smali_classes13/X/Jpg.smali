.class public final enum LX/Jpg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jpg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jpg;

.field public static final enum LOCAL:LX/Jpg;

.field public static final enum MATCHED:LX/Jpg;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2737903
    new-instance v0, LX/Jpg;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2}, LX/Jpg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpg;->LOCAL:LX/Jpg;

    .line 2737904
    new-instance v0, LX/Jpg;

    const-string v1, "MATCHED"

    invoke-direct {v0, v1, v3}, LX/Jpg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jpg;->MATCHED:LX/Jpg;

    .line 2737905
    const/4 v0, 0x2

    new-array v0, v0, [LX/Jpg;

    sget-object v1, LX/Jpg;->LOCAL:LX/Jpg;

    aput-object v1, v0, v2

    sget-object v1, LX/Jpg;->MATCHED:LX/Jpg;

    aput-object v1, v0, v3

    sput-object v0, LX/Jpg;->$VALUES:[LX/Jpg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2737902
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jpg;
    .locals 1

    .prologue
    .line 2737901
    const-class v0, LX/Jpg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jpg;

    return-object v0
.end method

.method public static values()[LX/Jpg;
    .locals 1

    .prologue
    .line 2737900
    sget-object v0, LX/Jpg;->$VALUES:[LX/Jpg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jpg;

    return-object v0
.end method
