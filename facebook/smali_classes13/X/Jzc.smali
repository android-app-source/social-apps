.class public final enum LX/Jzc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jzc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jzc;

.field public static final enum PHOTO:LX/Jzc;

.field public static final enum VIDEO:LX/Jzc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2756385
    new-instance v0, LX/Jzc;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/Jzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jzc;->PHOTO:LX/Jzc;

    new-instance v0, LX/Jzc;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/Jzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jzc;->VIDEO:LX/Jzc;

    const/4 v0, 0x2

    new-array v0, v0, [LX/Jzc;

    sget-object v1, LX/Jzc;->PHOTO:LX/Jzc;

    aput-object v1, v0, v2

    sget-object v1, LX/Jzc;->VIDEO:LX/Jzc;

    aput-object v1, v0, v3

    sput-object v0, LX/Jzc;->$VALUES:[LX/Jzc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2756386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jzc;
    .locals 1

    .prologue
    .line 2756387
    const-class v0, LX/Jzc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jzc;

    return-object v0
.end method

.method public static values()[LX/Jzc;
    .locals 1

    .prologue
    .line 2756388
    sget-object v0, LX/Jzc;->$VALUES:[LX/Jzc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jzc;

    return-object v0
.end method
