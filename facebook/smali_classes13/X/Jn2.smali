.class public final LX/Jn2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jn1;


# instance fields
.field public final synthetic a:LX/Jn3;


# direct methods
.method public constructor <init>(LX/Jn3;)V
    .locals 0

    .prologue
    .line 2733600
    iput-object p1, p0, LX/Jn2;->a:LX/Jn3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2733601
    iget-object v0, p0, LX/Jn2;->a:LX/Jn3;

    .line 2733602
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    .line 2733603
    if-eqz v1, :cond_0

    .line 2733604
    iget-object v2, v0, LX/Jn3;->c:Ljava/util/Set;

    iget-object p0, v1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733605
    iget-object p1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2733606
    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2733607
    :cond_0
    invoke-static {v0, v1}, LX/Jn3;->c(LX/Jn3;Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)Ljava/lang/Integer;

    move-result-object v2

    .line 2733608
    iget-object p0, v0, LX/Jn3;->d:LX/JnA;

    if-eqz p0, :cond_1

    if-eqz v2, :cond_1

    .line 2733609
    iget-object p0, v0, LX/Jn3;->d:LX/JnA;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/JnA;->a(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;I)V

    .line 2733610
    :cond_1
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2733611
    iget-object v0, p0, LX/Jn2;->a:LX/Jn3;

    .line 2733612
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    .line 2733613
    if-nez v1, :cond_0

    .line 2733614
    :goto_0
    return-void

    .line 2733615
    :cond_0
    invoke-static {v0, v1}, LX/Jn3;->c(LX/Jn3;Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;)Ljava/lang/Integer;

    move-result-object v2

    .line 2733616
    iget-object v3, v0, LX/Jn3;->d:LX/JnA;

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 2733617
    iget-object v3, v0, LX/Jn3;->d:LX/JnA;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v1, v2}, LX/JnA;->b(Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;I)V

    .line 2733618
    :cond_1
    const/4 v2, 0x0

    .line 2733619
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2733620
    iget-object v3, v0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    move v3, v2

    :goto_1
    if-ge v4, v6, :cond_3

    iget-object v2, v0, LX/Jn3;->b:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;

    .line 2733621
    iget-object v7, v2, Lcom/facebook/messaging/invites/inbox2/InboxInviteFbFriendsUserItem;->a:Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;

    move-object v7, v7

    .line 2733622
    iget-object v7, v7, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733623
    iget-object p0, v7, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, p0

    .line 2733624
    iget-object p0, v1, Lcom/facebook/messaging/invites/inbox2/FbFriendsSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2733625
    iget-object p1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2733626
    invoke-static {v7, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2733627
    invoke-virtual {v0, v3}, LX/1OM;->d(I)V

    move v2, v3

    .line 2733628
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_1

    .line 2733629
    :cond_2
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2733630
    add-int/lit8 v2, v3, 0x1

    goto :goto_2

    .line 2733631
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v0, LX/Jn3;->b:LX/0Px;

    .line 2733632
    goto :goto_0
.end method
