.class public LX/JrF;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kU;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jrc;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDs;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742579
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrF;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742565
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742566
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742567
    iput-object v0, p0, LX/JrF;->a:LX/0Ot;

    .line 2742568
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742569
    iput-object v0, p0, LX/JrF;->b:LX/0Ot;

    .line 2742570
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742571
    iput-object v0, p0, LX/JrF;->c:LX/0Ot;

    .line 2742572
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742573
    iput-object v0, p0, LX/JrF;->d:LX/0Ot;

    .line 2742574
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742575
    iput-object v0, p0, LX/JrF;->e:LX/0Ot;

    .line 2742576
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742577
    iput-object v0, p0, LX/JrF;->f:LX/0Ot;

    .line 2742578
    return-void
.end method

.method public static a(LX/0QB;)LX/JrF;
    .locals 12

    .prologue
    .line 2742505
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742506
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742507
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742508
    if-nez v1, :cond_0

    .line 2742509
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742510
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742511
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742512
    sget-object v1, LX/JrF;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742513
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742514
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742515
    :cond_1
    if-nez v1, :cond_4

    .line 2742516
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742517
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742518
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742519
    new-instance v1, LX/JrF;

    const/16 v7, 0x35bd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct {v1, v7}, LX/JrF;-><init>(LX/0Ot;)V

    .line 2742520
    const/16 v7, 0xce5

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29fd

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x29c4

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xd18

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2739

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0x2e3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2742521
    iput-object v7, v1, LX/JrF;->a:LX/0Ot;

    iput-object v8, v1, LX/JrF;->b:LX/0Ot;

    iput-object v9, v1, LX/JrF;->c:LX/0Ot;

    iput-object v10, v1, LX/JrF;->d:LX/0Ot;

    iput-object v11, v1, LX/JrF;->e:LX/0Ot;

    iput-object p0, v1, LX/JrF;->f:LX/0Ot;

    .line 2742522
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742523
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742524
    if-nez v1, :cond_2

    .line 2742525
    sget-object v0, LX/JrF;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrF;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742526
    :goto_1
    if-eqz v0, :cond_3

    .line 2742527
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742528
    :goto_3
    check-cast v0, LX/JrF;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742529
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742530
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742531
    :catchall_1
    move-exception v0

    .line 2742532
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742533
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742534
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742535
    :cond_2
    :try_start_8
    sget-object v0, LX/JrF;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrF;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742563
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742564
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2742545
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2742546
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->f()LX/6kJ;

    move-result-object v5

    .line 2742547
    iget-object v0, p0, LX/JrF;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    iget-object v1, p0, LX/JrF;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jrc;

    iget-object v6, v5, LX/6kJ;->threadKey:LX/6l9;

    invoke-virtual {v1, v6}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2742548
    iget-object v6, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742549
    if-nez v6, :cond_0

    move-object v0, v4

    .line 2742550
    :goto_0
    return-object v0

    .line 2742551
    :cond_0
    iget-object v0, p0, LX/JrF;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    iget-object v1, v5, LX/6kJ;->roomDiscoverableMode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_2

    :goto_1
    iget-object v1, p0, LX/JrF;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v8

    .line 2742552
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v1

    iget-object v3, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {v1, v3}, LX/6fm;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6fm;

    move-result-object v1

    .line 2742553
    iput-boolean v2, v1, LX/6fm;->c:Z

    .line 2742554
    move-object v1, v1

    .line 2742555
    invoke-virtual {v1}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v1

    .line 2742556
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    move-result-object v1

    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2742557
    invoke-static {v0, v1, v8, v9}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742558
    iget-object v3, v0, LX/FDs;->d:LX/2N4;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v1

    .line 2742559
    if-eqz v0, :cond_1

    .line 2742560
    const-string v1, "is_discoverable_thread_summary"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    move-object v0, v4

    .line 2742561
    goto :goto_0

    :cond_2
    move v2, v3

    .line 2742562
    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742539
    const-string v0, "is_discoverable_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2742540
    if-eqz v0, :cond_0

    .line 2742541
    iget-object v1, p0, LX/JrF;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/JrF;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2742542
    iget-object v1, p0, LX/JrF;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2742543
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2742544
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2742536
    check-cast p1, LX/6kU;

    .line 2742537
    invoke-virtual {p1}, LX/6kU;->f()LX/6kJ;

    move-result-object v1

    .line 2742538
    iget-object v0, p0, LX/JrF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jrc;

    iget-object v1, v1, LX/6kJ;->threadKey:LX/6l9;

    invoke-virtual {v0, v1}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
