.class public LX/JvV;
.super LX/1a1;
.source ""


# instance fields
.field public final l:Lcom/facebook/fig/listitem/FigListItem;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2750422
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2750423
    check-cast p1, Lcom/facebook/fig/listitem/FigListItem;

    iput-object p1, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2750424
    iget-object v0, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    .line 2750425
    iget-object v0, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 2750426
    iget-object v0, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2750427
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2750428
    iget-object v0, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2750429
    iget-object v0, p0, LX/JvV;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2750430
    return-void
.end method
