.class public final LX/Jx2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/Jwa;",
        "LX/Jx9;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cdn;

.field public final synthetic b:LX/JxB;


# direct methods
.method public constructor <init>(LX/JxB;LX/Cdn;)V
    .locals 0

    .prologue
    .line 2752721
    iput-object p1, p0, LX/Jx2;->b:LX/JxB;

    iput-object p2, p0, LX/Jx2;->a:LX/Cdn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2752722
    check-cast p1, LX/Jwa;

    const/4 v0, 0x0

    .line 2752723
    new-instance v2, LX/Jx9;

    invoke-direct {v2}, LX/Jx9;-><init>()V

    .line 2752724
    iget-object v1, p0, LX/Jx2;->b:LX/JxB;

    iget-object v1, v1, LX/JxB;->f:LX/Jws;

    iget-object v3, p1, LX/Jwa;->b:Ljava/lang/String;

    .line 2752725
    new-instance v7, LX/Jwr;

    .line 2752726
    new-instance v4, LX/Jwu;

    invoke-direct {v4}, LX/Jwu;-><init>()V

    .line 2752727
    move-object v4, v4

    .line 2752728
    move-object v4, v4

    .line 2752729
    check-cast v4, LX/Jwu;

    const-class v5, LX/Jwy;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Jwy;

    .line 2752730
    new-instance v6, LX/Jww;

    invoke-direct {v6}, LX/Jww;-><init>()V

    .line 2752731
    move-object v6, v6

    .line 2752732
    move-object v6, v6

    .line 2752733
    check-cast v6, LX/Jww;

    invoke-direct {v7, v4, v5, v6, v3}, LX/Jwr;-><init>(LX/Jwu;LX/Jwy;LX/Jww;Ljava/lang/String;)V

    .line 2752734
    move-object v3, v7

    .line 2752735
    iget-object v1, p0, LX/Jx2;->a:LX/Cdn;

    .line 2752736
    iget-object v4, v1, LX/Cdn;->a:LX/0Px;

    move-object v4, v4

    .line 2752737
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_4

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cdm;

    .line 2752738
    iget-object v6, v0, LX/Cdm;->a:LX/Cdl;

    move-object v6, v6

    .line 2752739
    iget-object v7, v6, LX/Cdl;->b:[B

    move-object v7, v7

    .line 2752740
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v9

    .line 2752741
    const/4 v8, 0x0

    .line 2752742
    :goto_1
    array-length v10, v7

    if-ge v8, v10, :cond_0

    .line 2752743
    aget-byte v10, v7, v8

    .line 2752744
    add-int/lit8 v8, v8, 0x1

    .line 2752745
    if-lez v10, :cond_0

    array-length v11, v7

    if-ge v8, v11, :cond_0

    .line 2752746
    aget-byte v11, v7, v8

    .line 2752747
    add-int/lit8 p1, v8, 0x1

    .line 2752748
    if-eqz v11, :cond_0

    array-length v8, v7

    if-ge p1, v8, :cond_0

    .line 2752749
    add-int v8, p1, v10

    add-int/lit8 v8, v8, -0x1

    .line 2752750
    array-length v6, v7

    if-gt v8, v6, :cond_0

    .line 2752751
    invoke-static {v7, p1, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    .line 2752752
    new-instance v6, LX/Jwt;

    invoke-direct {v6, v10, v11, p1}, LX/Jwt;-><init>(IB[B)V

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2752753
    :cond_0
    move-object v7, v9

    .line 2752754
    invoke-static {v7}, LX/Jww;->a(Ljava/util/List;)LX/Jwv;

    move-result-object v8

    .line 2752755
    if-eqz v8, :cond_5

    .line 2752756
    new-instance v7, LX/Jwq;

    invoke-direct {v7, v8}, LX/Jwq;-><init>(LX/Jwv;)V

    .line 2752757
    :goto_2
    move-object v6, v7

    .line 2752758
    if-eqz v6, :cond_1

    .line 2752759
    iget-object v7, v0, LX/Cdm;->b:Ljava/util/List;

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v7, v7

    .line 2752760
    sget-object v0, LX/Jx8;->a:[I

    .line 2752761
    iget-object v8, v6, LX/Jwq;->a:LX/Jwp;

    move-object v8, v8

    .line 2752762
    invoke-virtual {v8}, LX/Jwp;->ordinal()I

    move-result v8

    aget v0, v0, v8

    packed-switch v0, :pswitch_data_0

    .line 2752763
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2752764
    :pswitch_0
    iget-object v0, v6, LX/Jwq;->b:LX/0am;

    move-object v0, v0

    .line 2752765
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    .line 2752766
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 2752767
    new-instance v10, LX/4Iu;

    invoke-direct {v10}, LX/4Iu;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2752768
    const-string v7, "uuid"

    invoke-virtual {v10, v7, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2752769
    move-object v10, v10

    .line 2752770
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->b()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 2752771
    const-string v7, "major"

    invoke-virtual {v10, v7, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2752772
    move-object v10, v10

    .line 2752773
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->c()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 2752774
    const-string v7, "minor"

    invoke-virtual {v10, v7, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2752775
    move-object v10, v10

    .line 2752776
    const-string v8, "rssi"

    invoke-virtual {v10, v8, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2752777
    move-object v6, v10

    .line 2752778
    iget-object v10, v2, LX/Jx9;->a:Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2752779
    goto :goto_4

    .line 2752780
    :cond_2
    goto :goto_3

    .line 2752781
    :pswitch_1
    iget-object v0, v6, LX/Jwq;->c:LX/0am;

    move-object v0, v0

    .line 2752782
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jwv;

    .line 2752783
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 2752784
    new-instance v10, LX/4It;

    invoke-direct {v10}, LX/4It;-><init>()V

    .line 2752785
    iget-object v8, v0, LX/Jwv;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2752786
    const-string v7, "payload"

    invoke-virtual {v10, v7, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2752787
    move-object v10, v10

    .line 2752788
    const-string v8, "rssi"

    invoke-virtual {v10, v8, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2752789
    move-object v6, v10

    .line 2752790
    iget-object v10, v2, LX/Jx9;->b:Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2752791
    goto :goto_5

    .line 2752792
    :cond_3
    goto/16 :goto_3

    .line 2752793
    :cond_4
    return-object v2

    .line 2752794
    :cond_5
    iget-object v8, v3, LX/Jwr;->b:LX/Jwx;

    .line 2752795
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/Jwt;

    .line 2752796
    invoke-static {v8, v9}, LX/Jwx;->a(LX/Jwx;LX/Jwt;)Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    move-result-object v9

    .line 2752797
    if-eqz v9, :cond_6

    .line 2752798
    :goto_6
    move-object v8, v9

    .line 2752799
    if-eqz v8, :cond_7

    .line 2752800
    new-instance v7, LX/Jwq;

    invoke-direct {v7, v8}, LX/Jwq;-><init>(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;)V

    goto/16 :goto_2

    .line 2752801
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v9, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
