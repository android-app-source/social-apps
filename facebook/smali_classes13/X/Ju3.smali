.class public final LX/Ju3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2748439
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2748440
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2748441
    :goto_0
    return v1

    .line 2748442
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2748443
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2748444
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2748445
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2748446
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2748447
    const-string v4, "focus"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2748448
    invoke-static {p0, p1}, LX/Ju2;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2748449
    :cond_2
    const-string v4, "photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2748450
    invoke-static {p0, p1}, LX/8ZQ;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2748451
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2748452
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2748453
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2748454
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2748455
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2748456
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748457
    if-eqz v0, :cond_2

    .line 2748458
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748459
    const-wide/16 v6, 0x0

    .line 2748460
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2748461
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2748462
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_0

    .line 2748463
    const-string v4, "x"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748464
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 2748465
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2748466
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 2748467
    const-string v4, "y"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748468
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 2748469
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2748470
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2748471
    if-eqz v0, :cond_3

    .line 2748472
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2748473
    invoke-static {p0, v0, p2, p3}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2748474
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2748475
    return-void
.end method
