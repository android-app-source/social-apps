.class public final LX/KBt;
.super LX/KBr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/KBr",
        "<",
        "LX/KAf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/2wh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wh",
            "<",
            "LX/KAf;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, LX/KBr;-><init>(LX/2wh;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/GetCapabilityResponse;)V
    .locals 4

    new-instance v0, LX/KCJ;

    iget v1, p1, Lcom/google/android/gms/wearable/internal/GetCapabilityResponse;->b:I

    invoke-static {v1}, LX/KBn;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    new-instance v2, LX/KCI;

    iget-object v3, p1, Lcom/google/android/gms/wearable/internal/GetCapabilityResponse;->c:Lcom/google/android/gms/wearable/internal/CapabilityInfoParcelable;

    invoke-direct {v2, v3}, LX/KCI;-><init>(LX/KAh;)V

    invoke-direct {v0, v1, v2}, LX/KCJ;-><init>(Lcom/google/android/gms/common/api/Status;LX/KAh;)V

    invoke-virtual {p0, v0}, LX/KBr;->a(Ljava/lang/Object;)V

    return-void
.end method
