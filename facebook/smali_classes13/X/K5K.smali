.class public LX/K5K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/K8G;

.field public final c:LX/0Sh;

.field private final d:LX/0So;

.field private final e:LX/0lB;

.field public final f:LX/K7A;

.field private g:J


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/K8G;LX/0Sh;LX/0So;LX/0lB;LX/K7A;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2770286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2770287
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/K5K;->g:J

    .line 2770288
    iput-object p1, p0, LX/K5K;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2770289
    iput-object p2, p0, LX/K5K;->b:LX/K8G;

    .line 2770290
    iput-object p3, p0, LX/K5K;->c:LX/0Sh;

    .line 2770291
    iput-object p4, p0, LX/K5K;->d:LX/0So;

    .line 2770292
    iput-object p5, p0, LX/K5K;->e:LX/0lB;

    .line 2770293
    iput-object p6, p0, LX/K5K;->f:LX/K7A;

    .line 2770294
    return-void
.end method

.method public static a(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2770283
    if-nez p0, :cond_0

    .line 2770284
    const/4 v0, 0x0

    .line 2770285
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "tarot_story_ids"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 11
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 2770295
    const/4 v1, 0x0

    .line 2770296
    if-eqz p1, :cond_0

    if-nez p2, :cond_6

    .line 2770297
    :cond_0
    :goto_0
    move v1, v1

    .line 2770298
    if-nez v1, :cond_2

    .line 2770299
    :cond_1
    :goto_1
    return v0

    .line 2770300
    :cond_2
    iget-object v1, p0, LX/K5K;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/K5K;->g:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x5dc

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    move v0, v8

    .line 2770301
    goto :goto_1

    .line 2770302
    :cond_3
    const-class v1, LX/0ew;

    invoke-static {p2, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ew;

    .line 2770303
    if-eqz v7, :cond_1

    invoke-interface {v7}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2770304
    invoke-interface {v7}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 2770305
    const-string v2, "chromeless:content:fragment:tag"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2770306
    instance-of v3, v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    if-eqz v3, :cond_7

    .line 2770307
    check-cast v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    .line 2770308
    :goto_2
    move-object v2, v2

    .line 2770309
    if-nez v2, :cond_4

    .line 2770310
    new-instance v2, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;

    invoke-direct {v2}, Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;-><init>()V

    .line 2770311
    :cond_4
    move-object v6, v2

    .line 2770312
    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2770313
    invoke-interface {v7}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 2770314
    const/4 v2, 0x1

    invoke-static {v6, v1, v2}, Lcom/facebook/widget/popover/PopoverFragment;->a(Lcom/facebook/widget/popover/PopoverFragment;LX/0gc;Z)V

    .line 2770315
    :cond_5
    invoke-static {p1}, LX/K5K;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v3

    .line 2770316
    if-nez p1, :cond_8

    .line 2770317
    const/4 v1, 0x0

    .line 2770318
    :goto_3
    move-object v4, v1

    .line 2770319
    invoke-static {p2, v3, v4, v0}, LX/K8G;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Z)LX/K8B;

    move-result-object v9

    .line 2770320
    const-string v0, "tracking_codes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2770321
    iget-object v1, p0, LX/K5K;->e:LX/0lB;

    invoke-static {v1, v0}, LX/K5I;->a(LX/0lC;Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2770322
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2770323
    iget-object v0, p0, LX/K5K;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/K5K;->g:J

    .line 2770324
    iget-object v10, p0, LX/K5K;->b:LX/K8G;

    new-instance v0, LX/K5J;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/K5J;-><init>(LX/K5K;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/0lF;Lcom/facebook/tarot/carousel/TarotCarouselDialogFragment;LX/0ew;)V

    invoke-virtual {v10, v9, v0}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    .line 2770325
    invoke-interface {v7}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    move v0, v8

    .line 2770326
    goto/16 :goto_1

    .line 2770327
    :cond_6
    invoke-static {p1}, LX/K5K;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    .line 2770328
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    :cond_8
    const-string v1, "tarot_click_source"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 2770282
    invoke-direct {p0, p1, p3}, LX/K5K;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 1

    .prologue
    .line 2770281
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/K5K;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 2770280
    invoke-direct {p0, p1, p2}, LX/K5K;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
