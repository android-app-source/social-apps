.class public final LX/JyF;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JyG;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

.field public b:Landroid/view/View$OnClickListener;

.field public final synthetic c:LX/JyG;


# direct methods
.method public constructor <init>(LX/JyG;)V
    .locals 1

    .prologue
    .line 2754418
    iput-object p1, p0, LX/JyF;->c:LX/JyG;

    .line 2754419
    move-object v0, p1

    .line 2754420
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2754421
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2754422
    const-string v0, "DiscoveryListItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2754423
    if-ne p0, p1, :cond_1

    .line 2754424
    :cond_0
    :goto_0
    return v0

    .line 2754425
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2754426
    goto :goto_0

    .line 2754427
    :cond_3
    check-cast p1, LX/JyF;

    .line 2754428
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2754429
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2754430
    if-eq v2, v3, :cond_0

    .line 2754431
    iget-object v2, p0, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    iget-object v3, p1, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2754432
    goto :goto_0

    .line 2754433
    :cond_5
    iget-object v2, p1, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    if-nez v2, :cond_4

    .line 2754434
    :cond_6
    iget-object v2, p0, LX/JyF;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JyF;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/JyF;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2754435
    goto :goto_0

    .line 2754436
    :cond_7
    iget-object v2, p1, LX/JyF;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
