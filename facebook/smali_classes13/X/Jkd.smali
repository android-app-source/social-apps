.class public final LX/Jkd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jkc;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;)V
    .locals 0

    .prologue
    .line 2730130
    iput-object p1, p0, LX/Jkd;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2730131
    iget-object v0, p0, LX/Jkd;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;

    invoke-static {v0}, LX/3pR;->a(Landroid/app/Activity;)V

    .line 2730132
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4

    .prologue
    .line 2730133
    iget-object v0, p0, LX/Jkd;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->p:LX/Iu3;

    const-string v1, "lightweight_action_see_thread"

    .line 2730134
    const/4 v2, 0x0

    .line 2730135
    iget-object v3, v0, LX/Iu3;->a:Landroid/content/Context;

    const-class p0, Landroid/app/Service;

    invoke-static {v3, p0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2730136
    if-eqz v3, :cond_1

    .line 2730137
    iget-object v3, v0, LX/Iu3;->d:LX/3RA;

    .line 2730138
    sget-object p0, LX/3RB;->d:Ljava/lang/String;

    invoke-static {v3, p0}, LX/3RA;->b(LX/3RA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2730139
    sget-object v0, LX/FDW;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2730140
    sget-object v0, LX/3RB;->n:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2730141
    if-nez v2, :cond_0

    sget-object v2, LX/FOD;->OTHER:LX/FOD;

    .line 2730142
    :cond_0
    const-string v0, "extra_thread_view_source"

    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2730143
    invoke-static {v3, p0}, LX/3RA;->a$redex0(LX/3RA;Landroid/content/Intent;)V

    .line 2730144
    :goto_1
    return-void

    .line 2730145
    :cond_1
    invoke-static {v0, p1}, LX/Iu3;->a(LX/Iu3;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v3

    .line 2730146
    const-string p0, "prefer_chat_if_possible"

    const/4 v1, 0x0

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2730147
    if-nez v2, :cond_3

    .line 2730148
    const-string p0, "extra_thread_view_source"

    sget-object v1, LX/FOD;->OTHER:LX/FOD;

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2730149
    :goto_2
    iget-object p0, v0, LX/Iu3;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, v0, LX/Iu3;->a:Landroid/content/Context;

    invoke-interface {p0, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2730150
    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2730151
    :cond_3
    const-string p0, "extra_thread_view_source"

    invoke-virtual {v3, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final a(Lcom/facebook/user/model/User;)V
    .locals 4

    .prologue
    .line 2730152
    iget-object v0, p0, LX/Jkd;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowActivity;->p:LX/Iu3;

    .line 2730153
    const/4 p0, 0x1

    .line 2730154
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2730155
    iget-object v1, v0, LX/Iu3;->e:LX/3Ec;

    .line 2730156
    iget-object v2, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2730157
    invoke-virtual {v1, v2}, LX/3Ec;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2730158
    iget-object v2, v0, LX/Iu3;->c:LX/3GK;

    invoke-interface {v2, v1}, LX/3GK;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v1

    .line 2730159
    :goto_0
    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2730160
    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2730161
    const-string v1, "focus_compose"

    invoke-virtual {v2, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2730162
    const-string v1, "show_composer"

    invoke-virtual {v2, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2730163
    const-string v1, "modify_backstack_override"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2730164
    move-object v1, v2

    .line 2730165
    const-string v2, "prefer_chat_if_possible"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2730166
    iget-object v2, v0, LX/Iu3;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/Iu3;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2730167
    return-void

    .line 2730168
    :cond_0
    iget-object v1, v0, LX/Iu3;->c:LX/3GK;

    .line 2730169
    iget-object v2, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2730170
    invoke-interface {v1, v2}, LX/3GK;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method
