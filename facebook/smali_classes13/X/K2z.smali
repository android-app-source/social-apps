.class public LX/K2z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/slideshow/SlideshowEditFragment;

.field private final e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

.field private final f:LX/0ad;

.field private final g:LX/K3L;

.field private final h:LX/K3X;

.field private final i:LX/0tX;

.field private final j:Ljava/util/concurrent/Executor;

.field private k:LX/K3K;

.field private l:LX/K3W;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLInterfaces$FBApplicationWithMoodsFragment$MovieFactoryConfig$Moods;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/slideshow/SlideshowEditFragment;LX/0Px;Lcom/facebook/ipc/composer/model/ComposerSlideshowData;Ljava/lang/String;LX/0gc;LX/0ad;LX/K3Q;LX/K3L;LX/K3X;LX/K32;LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1    # Lcom/facebook/slideshow/SlideshowEditFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/slideshow/SlideshowEditFragment;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Lcom/facebook/ipc/composer/model/ComposerSlideshowData;",
            "Ljava/lang/String;",
            "LX/0gc;",
            "LX/0ad;",
            "LX/K3Q;",
            "LX/K3L;",
            "LX/K3X;",
            "LX/K32;",
            "LX/0tX;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2764833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764834
    iput-object p1, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764835
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764836
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    move-object v0, v1

    .line 2764837
    invoke-virtual {p7, v0, p4}, LX/K3Q;->a(Lcom/facebook/slideshow/ui/PlayableSlideshowView;Ljava/lang/String;)Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    move-result-object v0

    iput-object v0, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764838
    iput-object p6, p0, LX/K2z;->f:LX/0ad;

    .line 2764839
    iput-object p4, p0, LX/K2z;->c:Ljava/lang/String;

    .line 2764840
    iput-object p2, p0, LX/K2z;->a:LX/0Px;

    .line 2764841
    iput-object p3, p0, LX/K2z;->b:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2764842
    iput-object p8, p0, LX/K2z;->g:LX/K3L;

    .line 2764843
    iput-object p9, p0, LX/K2z;->h:LX/K3X;

    .line 2764844
    iput-object p11, p0, LX/K2z;->i:LX/0tX;

    .line 2764845
    iput-object p12, p0, LX/K2z;->j:Ljava/util/concurrent/Executor;

    .line 2764846
    iget-object v0, p0, LX/K2z;->f:LX/0ad;

    sget-short v1, LX/K33;->a:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2764847
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764848
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    move-object v0, v1

    .line 2764849
    invoke-virtual {v0, v2}, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->setVisibility(I)V

    .line 2764850
    iget-object v0, p0, LX/K2z;->g:LX/K3L;

    iget-object v1, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764851
    iget-object v2, v1, Lcom/facebook/slideshow/SlideshowEditFragment;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    move-object v1, v2

    .line 2764852
    iget-object v2, p0, LX/K2z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/K3L;->a(Lcom/facebook/slideshow/ui/DragSortThumbnailListView;Ljava/lang/String;)LX/K3K;

    move-result-object v0

    iput-object v0, p0, LX/K2z;->k:LX/K3K;

    .line 2764853
    iget-object v0, p0, LX/K2z;->k:LX/K3K;

    iget-object v1, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764854
    iput-object v1, v0, LX/K3K;->e:LX/K3I;

    .line 2764855
    iget-object v0, p0, LX/K2z;->a:LX/0Px;

    invoke-virtual {p0, v0}, LX/K2z;->a(LX/0Px;)V

    .line 2764856
    :goto_0
    return-void

    .line 2764857
    :cond_0
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764858
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->d:Lcom/facebook/widget/CustomViewPager;

    move-object v0, v1

    .line 2764859
    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 2764860
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764861
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->c:Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;

    move-object v0, v1

    .line 2764862
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;->setVisibility(I)V

    .line 2764863
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764864
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->d:Lcom/facebook/widget/CustomViewPager;

    move-object v0, v1

    .line 2764865
    invoke-static {}, LX/K2y;->values()[LX/K2y;

    move-result-object v1

    .line 2764866
    new-instance p1, LX/K31;

    const-class v2, Landroid/content/Context;

    invoke-interface {p10, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {p1, v1, p0, v2}, LX/K31;-><init>([LX/K2y;LX/K2z;Landroid/content/Context;)V

    .line 2764867
    move-object v1, p1

    .line 2764868
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2764869
    iget-object v0, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764870
    iget-object v1, v0, Lcom/facebook/slideshow/SlideshowEditFragment;->c:Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;

    move-object v0, v1

    .line 2764871
    iget-object v1, p0, LX/K2z;->d:Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764872
    iget-object v2, v1, Lcom/facebook/slideshow/SlideshowEditFragment;->d:Lcom/facebook/widget/CustomViewPager;

    move-object v1, v2

    .line 2764873
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2764874
    invoke-direct {p0}, LX/K2z;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2764793
    new-instance v0, LX/K34;

    invoke-direct {v0}, LX/K34;-><init>()V

    move-object v0, v0

    .line 2764794
    const-string v1, "appID"

    const-string v2, "1176968752345840"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2764795
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x93a80

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2764796
    iget-object v1, p0, LX/K2z;->i:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2764797
    new-instance v1, LX/K2w;

    invoke-direct {v1, p0}, LX/K2w;-><init>(LX/K2z;)V

    iget-object v2, p0, LX/K2z;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2764798
    return-void
.end method

.method public static d(LX/K2z;)V
    .locals 5

    .prologue
    .line 2764799
    iget-object v0, p0, LX/K2z;->m:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K2z;->l:LX/K3W;

    if-eqz v0, :cond_0

    .line 2764800
    iget-object v0, p0, LX/K2z;->b:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    if-nez v0, :cond_1

    .line 2764801
    iget-object v0, p0, LX/K2z;->l:LX/K3W;

    iget-object v1, p0, LX/K2z;->m:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/K3W;->a(LX/0Px;Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V

    .line 2764802
    :cond_0
    :goto_0
    return-void

    .line 2764803
    :cond_1
    iget-object v0, p0, LX/K2z;->b:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->getMoodId()Ljava/lang/String;

    move-result-object v2

    .line 2764804
    iget-object v0, p0, LX/K2z;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/K2z;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2764805
    invoke-virtual {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2764806
    iget-object v1, p0, LX/K2z;->l:LX/K3W;

    iget-object v2, p0, LX/K2z;->m:LX/0Px;

    invoke-virtual {v1, v2, v0}, LX/K3W;->a(LX/0Px;Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V

    goto :goto_0

    .line 2764807
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2764808
    iget-object v0, p0, LX/K2z;->k:LX/K3K;

    .line 2764809
    iget-object p0, v0, LX/K3K;->a:LX/0Px;

    move-object v0, p0

    .line 2764810
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2764811
    iget-object v0, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->a(LX/0Px;Z)V

    .line 2764812
    iget-object v0, p0, LX/K2z;->k:LX/K3K;

    invoke-virtual {v0, p1}, LX/K3K;->a(LX/0Px;)V

    .line 2764813
    return-void
.end method

.method public final a(Landroid/view/View;LX/K2y;)V
    .locals 4

    .prologue
    .line 2764814
    sget-object v0, LX/K2x;->a:[I

    invoke-virtual {p2}, LX/K2y;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2764815
    :goto_0
    return-void

    .line 2764816
    :pswitch_0
    iget-object v1, p0, LX/K2z;->g:LX/K3L;

    const v0, 0x7f0d2ca4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    iget-object v2, p0, LX/K2z;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/K3L;->a(Lcom/facebook/slideshow/ui/DragSortThumbnailListView;Ljava/lang/String;)LX/K3K;

    move-result-object v0

    iput-object v0, p0, LX/K2z;->k:LX/K3K;

    .line 2764817
    iget-object v0, p0, LX/K2z;->k:LX/K3K;

    iget-object v1, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764818
    iput-object v1, v0, LX/K3K;->e:LX/K3I;

    .line 2764819
    iget-object v0, p0, LX/K2z;->a:LX/0Px;

    invoke-virtual {p0, v0}, LX/K2z;->a(LX/0Px;)V

    goto :goto_0

    .line 2764820
    :pswitch_1
    iget-object v1, p0, LX/K2z;->h:LX/K3X;

    const v0, 0x7f0d2d2c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2764821
    new-instance p1, LX/K3W;

    .line 2764822
    new-instance p2, Lcom/facebook/slideshow/ui/SoundListAdapter;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p2, v2, v3}, Lcom/facebook/slideshow/ui/SoundListAdapter;-><init>(Landroid/content/Context;LX/23P;)V

    .line 2764823
    move-object v2, p2

    .line 2764824
    check-cast v2, Lcom/facebook/slideshow/ui/SoundListAdapter;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p1, v0, v2, v3}, LX/K3W;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/slideshow/ui/SoundListAdapter;Landroid/content/Context;)V

    .line 2764825
    move-object v0, p1

    .line 2764826
    iput-object v0, p0, LX/K2z;->l:LX/K3W;

    .line 2764827
    iget-object v0, p0, LX/K2z;->l:LX/K3W;

    iget-object v1, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764828
    iput-object v1, v0, LX/K3W;->c:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764829
    invoke-static {p0}, LX/K2z;->d(LX/K2z;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2764830
    iget-object v0, p0, LX/K2z;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    .line 2764831
    iget-object p0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->i:Ljava/lang/String;

    move-object v0, p0

    .line 2764832
    return-object v0
.end method
