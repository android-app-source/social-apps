.class public final LX/KAM;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/KAM;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/KAK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/KAN;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2779325
    const/4 v0, 0x0

    sput-object v0, LX/KAM;->a:LX/KAM;

    .line 2779326
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/KAM;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2779322
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2779323
    new-instance v0, LX/KAN;

    invoke-direct {v0}, LX/KAN;-><init>()V

    iput-object v0, p0, LX/KAM;->c:LX/KAN;

    .line 2779324
    return-void
.end method

.method public static c(LX/1De;)LX/KAK;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2779327
    new-instance v1, LX/KAL;

    invoke-direct {v1}, LX/KAL;-><init>()V

    .line 2779328
    sget-object v2, LX/KAM;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/KAK;

    .line 2779329
    if-nez v2, :cond_0

    .line 2779330
    new-instance v2, LX/KAK;

    invoke-direct {v2}, LX/KAK;-><init>()V

    .line 2779331
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/KAK;->a$redex0(LX/KAK;LX/1De;IILX/KAL;)V

    .line 2779332
    move-object v1, v2

    .line 2779333
    move-object v0, v1

    .line 2779334
    return-object v0
.end method

.method public static declared-synchronized q()LX/KAM;
    .locals 2

    .prologue
    .line 2779318
    const-class v1, LX/KAM;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/KAM;->a:LX/KAM;

    if-nez v0, :cond_0

    .line 2779319
    new-instance v0, LX/KAM;

    invoke-direct {v0}, LX/KAM;-><init>()V

    sput-object v0, LX/KAM;->a:LX/KAM;

    .line 2779320
    :cond_0
    sget-object v0, LX/KAM;->a:LX/KAM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2779321
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2779312
    check-cast p2, LX/KAL;

    .line 2779313
    iget-object v0, p2, LX/KAL;->a:LX/1dQ;

    .line 2779314
    const/4 v1, 0x0

    const p0, 0x7f0e0c6f

    invoke-static {p1, v1, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    const p0, 0x7f083c45

    invoke-virtual {v1, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p0, 0x7f0b270d

    invoke-interface {v1, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const p0, 0x7f0b270e

    invoke-interface {v1, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const/4 p0, 0x6

    const p2, 0x7f0b270f

    invoke-interface {v1, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2779315
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2779316
    invoke-static {}, LX/1dS;->b()V

    .line 2779317
    const/4 v0, 0x0

    return-object v0
.end method
