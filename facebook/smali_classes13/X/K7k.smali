.class public final LX/K7k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

.field private final b:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V
    .locals 2

    .prologue
    .line 2773412
    iput-object p1, p0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2773413
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    .line 2773414
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2773415
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/K7j;

    invoke-direct {v1, p0, p1}, LX/K7j;-><init>(LX/K7k;Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2773416
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 2773417
    if-ne p1, p2, :cond_1

    .line 2773418
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2773419
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2773420
    :cond_0
    :goto_0
    return-void

    .line 2773421
    :cond_1
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-virtual {v1}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    .line 2773422
    iget-object v1, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2773423
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2773424
    iget-object v0, p0, LX/K7k;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2773425
    iget-object v0, p0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-static {v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getTopMargin(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/K7k;->a(II)V

    .line 2773426
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2773427
    iget-object v0, p0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    invoke-static {v0}, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->getTopMargin(Lcom/facebook/tarot/drawer/TarotPublisherDrawer;)I

    move-result v0

    iget-object v1, p0, LX/K7k;->a:Lcom/facebook/tarot/drawer/TarotPublisherDrawer;

    iget v1, v1, Lcom/facebook/tarot/drawer/TarotPublisherDrawer;->j:I

    neg-int v1, v1

    invoke-direct {p0, v0, v1}, LX/K7k;->a(II)V

    .line 2773428
    return-void
.end method
