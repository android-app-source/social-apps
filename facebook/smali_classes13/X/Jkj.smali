.class public final LX/Jkj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;)V
    .locals 0

    .prologue
    .line 2730226
    iput-object p1, p0, LX/Jkj;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;)V
    .locals 1

    .prologue
    .line 2730227
    iget-object v0, p0, LX/Jkj;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->l:LX/Jkc;

    if-eqz v0, :cond_0

    .line 2730228
    iget-object v0, p0, LX/Jkj;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->l:LX/Jkc;

    invoke-interface {v0, p1}, LX/Jkc;->a(Lcom/facebook/user/model/User;)V

    .line 2730229
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/user/model/User;)V
    .locals 12

    .prologue
    .line 2730230
    iget-object v0, p0, LX/Jkj;->a:Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/activenow/ActiveNowFragment;->b()LX/JnH;

    move-result-object v0

    sget-object v1, LX/9VZ;->ACTIVE_NOW_LIGHTWEIGHT_ACTIONS:LX/9VZ;

    .line 2730231
    iget-object v2, v0, LX/JnH;->d:Landroid/content/Context;

    .line 2730232
    new-instance v4, LX/6e5;

    invoke-direct {v4}, LX/6e5;-><init>()V

    .line 2730233
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v3

    .line 2730234
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2730235
    iput-object v3, v4, LX/6e5;->b:Ljava/lang/String;

    .line 2730236
    :cond_0
    iget-object v3, v0, LX/JnH;->g:LX/JnE;

    .line 2730237
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2730238
    sget-object v5, LX/FFk;->a:LX/0Px;

    move-object v8, v5

    .line 2730239
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v9, :cond_3

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FFq;

    .line 2730240
    new-instance v10, Landroid/text/SpannableStringBuilder;

    invoke-direct {v10}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2730241
    iget-object v11, v5, LX/FFq;->initialEmoji:Ljava/lang/String;

    .line 2730242
    invoke-static {v11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2730243
    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v11

    const-string p0, " "

    invoke-virtual {v11, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2730244
    :cond_1
    iget v11, v5, LX/FFq;->actionNameResId:I

    invoke-virtual {v2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2730245
    invoke-static {v11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2730246
    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2730247
    :cond_2
    move-object v10, v10

    .line 2730248
    iget-object v11, v3, LX/JnE;->a:LX/1zC;

    invoke-virtual {v11, v10}, LX/1zC;->a(Landroid/text/Editable;)Z

    .line 2730249
    new-instance v11, LX/6e3;

    invoke-direct {v11}, LX/6e3;-><init>()V

    .line 2730250
    iput-object v10, v11, LX/6e3;->d:Ljava/lang/CharSequence;

    .line 2730251
    move-object v10, v11

    .line 2730252
    invoke-virtual {v5}, LX/FFq;->ordinal()I

    move-result v5

    .line 2730253
    iput v5, v10, LX/6e3;->a:I

    .line 2730254
    move-object v5, v10

    .line 2730255
    invoke-virtual {v5}, LX/6e3;->g()Lcom/facebook/messaging/dialog/MenuDialogItem;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2730256
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 2730257
    :cond_3
    move-object v3, v7

    .line 2730258
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/dialog/MenuDialogItem;

    .line 2730259
    invoke-virtual {v4, v3}, LX/6e5;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;)LX/6e5;

    goto :goto_1

    .line 2730260
    :cond_4
    invoke-virtual {v4}, LX/6e5;->e()Lcom/facebook/messaging/dialog/MenuDialogParams;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/messaging/dialog/MenuDialogFragment;->a(Lcom/facebook/messaging/dialog/MenuDialogParams;)Lcom/facebook/messaging/dialog/MenuDialogFragment;

    move-result-object v3

    .line 2730261
    new-instance v4, LX/JnF;

    invoke-direct {v4, v0, p1, v1}, LX/JnF;-><init>(LX/JnH;Lcom/facebook/user/model/User;LX/9VZ;)V

    .line 2730262
    iput-object v4, v3, Lcom/facebook/messaging/dialog/MenuDialogFragment;->n:LX/6e1;

    .line 2730263
    move-object v2, v3

    .line 2730264
    iget-object v3, v0, LX/JnH;->e:LX/0gc;

    sget-object v4, LX/JnH;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2730265
    iget-object v2, v0, LX/JnH;->j:LX/FFh;

    invoke-virtual {v1}, LX/9VZ;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2730266
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "lwa_modal"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "entry_point"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2730267
    iget-object v5, v2, LX/FFh;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2730268
    return-void
.end method
