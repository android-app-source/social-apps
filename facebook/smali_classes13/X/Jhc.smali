.class public LX/Jhc;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/DAU;

.field public b:LX/3Ky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FJv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/EFW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/rtc/annotations/IsRtcVideoConferencingEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/FCl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final l:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field public final m:I

.field private final n:Lcom/facebook/messaging/ui/name/ThreadNameView;

.field private final o:Lcom/facebook/widget/tiles/ThreadTileView;

.field public final p:Landroid/widget/ImageView;

.field public q:Landroid/widget/CheckBox;

.field public r:Landroid/view/ViewStub;

.field public s:Landroid/view/View;

.field private t:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterButton;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/resources/ui/FbTextView;

.field private z:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2724615
    const-class v0, LX/Jhc;

    sput-object v0, LX/Jhc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2724616
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Jhc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2724617
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2724618
    const v0, 0x7f0104c4

    invoke-direct {p0, p1, p2, v0}, LX/Jhc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724619
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    .line 2724620
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2724621
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2724622
    iput-object v0, p0, LX/Jhc;->h:LX/0Ot;

    .line 2724623
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2724624
    iput-object v0, p0, LX/Jhc;->i:LX/0Ot;

    .line 2724625
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2724626
    iput-object v0, p0, LX/Jhc;->k:LX/0Ot;

    .line 2724627
    const v0, 0x7f030cdc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2724628
    const v0, 0x7f0d005d

    invoke-virtual {p0, v0}, LX/Jhc;->setId(I)V

    .line 2724629
    const v0, 0x7f0d0a22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2724630
    iget-object v0, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0}, LX/2Wj;->getTextColor()I

    move-result v0

    iput v0, p0, LX/Jhc;->m:I

    .line 2724631
    const v0, 0x7f0d1575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/name/ThreadNameView;

    iput-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    .line 2724632
    const v0, 0x7f0d1e98    # 1.8758E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/Jhc;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2724633
    const v0, 0x7f0d037e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Jhc;->p:Landroid/widget/ImageView;

    .line 2724634
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/Jhc;->q:Landroid/widget/CheckBox;

    .line 2724635
    const v0, 0x7f0d2005

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Jhc;->r:Landroid/view/ViewStub;

    .line 2724636
    const v0, 0x7f0d2000

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->t:LX/4ob;

    .line 2724637
    const v0, 0x7f0d2002

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->u:LX/4ob;

    .line 2724638
    const v0, 0x7f0d1fff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->v:LX/4ob;

    .line 2724639
    const v0, 0x7f0d2004

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->w:LX/4ob;

    .line 2724640
    const v0, 0x7f0d2003

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->x:LX/4ob;

    .line 2724641
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, LX/Jhc;

    invoke-static {p3}, LX/3Ky;->a(LX/0QB;)LX/3Ky;

    move-result-object v3

    check-cast v3, LX/3Ky;

    invoke-static {p3}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v4

    check-cast v4, LX/FJv;

    invoke-static {p3}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p3}, LX/EFW;->b(LX/0QB;)LX/EFW;

    move-result-object v7

    check-cast v7, LX/EFW;

    const/16 v8, 0x1560

    invoke-static {p3, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x1644

    invoke-static {p3, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p1, 0x1430

    invoke-static {p3, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {p3}, LX/FCl;->a(LX/0QB;)LX/FCl;

    move-result-object p2

    check-cast p2, LX/FCl;

    const/16 v0, 0x110e

    invoke-static {p3, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v3, v2, LX/Jhc;->b:LX/3Ky;

    iput-object v4, v2, LX/Jhc;->c:LX/FJv;

    iput-object v5, v2, LX/Jhc;->d:LX/0SG;

    iput-object v6, v2, LX/Jhc;->e:LX/0ad;

    iput-object v7, v2, LX/Jhc;->f:LX/EFW;

    iput-object v8, v2, LX/Jhc;->g:LX/0Or;

    iput-object v9, v2, LX/Jhc;->h:LX/0Ot;

    iput-object p1, v2, LX/Jhc;->i:LX/0Ot;

    iput-object p2, v2, LX/Jhc;->j:LX/FCl;

    iput-object p3, v2, LX/Jhc;->k:LX/0Ot;

    .line 2724642
    return-void
.end method

.method public static a(LX/Jhc;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2724643
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724644
    iget-object v1, v0, LX/DAU;->b:LX/3OI;

    move-object v0, v1

    .line 2724645
    sget-object v1, LX/DAT;->AGGREGATE_CALL_DETAILS:LX/DAT;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2724646
    if-nez v0, :cond_2

    .line 2724647
    iget-object v0, p0, LX/Jhc;->v:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2724648
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724649
    iget-object v1, v0, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v1

    .line 2724650
    iget-object v1, p0, LX/Jhc;->b:LX/3Ky;

    invoke-virtual {v1, v0}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v1

    .line 2724651
    iget-object v2, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v2, v1}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724652
    iget-object v2, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v2, v5}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    .line 2724653
    iget-object v2, p0, LX/Jhc;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v3, p0, LX/Jhc;->c:LX/FJv;

    invoke-virtual {v3, v0}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2724654
    iget-object v2, p0, LX/Jhc;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v2, v5}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2724655
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2724656
    iget-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v1}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 2724657
    iget-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v5}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    .line 2724658
    :goto_1
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724659
    iget-object v1, v0, LX/DAU;->d:LX/DAS;

    move-object v0, v1

    .line 2724660
    if-eqz v0, :cond_4

    .line 2724661
    iget-object v1, p0, LX/Jhc;->p:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2724662
    iget-object v1, p0, LX/Jhc;->p:Landroid/widget/ImageView;

    new-instance v2, LX/JhY;

    invoke-direct {v2, p0, v0}, LX/JhY;-><init>(LX/Jhc;LX/DAS;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724663
    :goto_2
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724664
    iget-boolean v1, v0, LX/DAU;->i:Z

    move v0, v1

    .line 2724665
    if-eqz v0, :cond_5

    .line 2724666
    iget-object v0, p0, LX/Jhc;->q:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2724667
    iget-object v0, p0, LX/Jhc;->q:Landroid/widget/CheckBox;

    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2724668
    :goto_3
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v0}, LX/3OP;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2724669
    iget-object v0, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {p0}, LX/Jhc;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2724670
    :goto_4
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724671
    iget-boolean v1, v0, LX/DAU;->h:Z

    move v0, v1

    .line 2724672
    if-nez v0, :cond_7

    .line 2724673
    iget-object v0, p0, LX/Jhc;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2724674
    iget-object v0, p0, LX/Jhc;->s:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2724675
    :cond_0
    :goto_5
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2724676
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    if-nez v0, :cond_9

    .line 2724677
    :goto_6
    invoke-direct {p0}, LX/Jhc;->f()V

    .line 2724678
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724679
    iget-boolean v1, v0, LX/DAU;->m:Z

    move v0, v1

    .line 2724680
    if-eqz v0, :cond_12

    .line 2724681
    iget-object v0, p0, LX/Jhc;->w:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;

    .line 2724682
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setVisibility(I)V

    .line 2724683
    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    .line 2724684
    iget-object v2, v1, LX/DAU;->e:LX/Ddk;

    move-object v1, v2

    .line 2724685
    iput-object v1, v0, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->a:LX/Ddk;

    .line 2724686
    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contacts/picker/ContactPickerListUndoButton;->setRow(LX/3OP;)V

    .line 2724687
    :goto_7
    return-void

    .line 2724688
    :cond_1
    iget-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2724689
    :cond_2
    iget-object v0, p0, LX/Jhc;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2724690
    iget-object v0, p0, LX/Jhc;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2724691
    iget-object v0, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    .line 2724692
    iget-object v0, p0, LX/Jhc;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2724693
    iget-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    .line 2724694
    iget-object v0, p0, LX/Jhc;->v:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2724695
    const v0, 0x7f0d1ff5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724696
    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v1}, LX/DAU;->p()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724697
    const v1, 0x7f0a0300

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2724698
    const v0, 0x7f0d1ff6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2724699
    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    .line 2724700
    iget-object v2, v1, LX/DAU;->p:Ljava/lang/String;

    move-object v1, v2

    .line 2724701
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2724702
    const v1, 0x7f0a0300

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 2724703
    goto :goto_7

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2724704
    :cond_4
    iget-object v0, p0, LX/Jhc;->p:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2724705
    :cond_5
    iget-object v0, p0, LX/Jhc;->q:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_3

    .line 2724706
    :cond_6
    iget-object v0, p0, LX/Jhc;->l:Lcom/facebook/messaging/ui/name/ThreadNameView;

    iget v1, p0, LX/Jhc;->m:I

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    goto/16 :goto_4

    .line 2724707
    :cond_7
    iget-object v0, p0, LX/Jhc;->s:Landroid/view/View;

    if-nez v0, :cond_8

    .line 2724708
    iget-object v0, p0, LX/Jhc;->r:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->s:Landroid/view/View;

    .line 2724709
    :cond_8
    iget-object v0, p0, LX/Jhc;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 2724710
    :cond_9
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724711
    iget-boolean v1, v0, LX/DAU;->j:Z

    move v0, v1

    .line 2724712
    if-eqz v0, :cond_a

    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724713
    iget-object v1, v0, LX/DAU;->n:Ljava/lang/String;

    move-object v0, v1

    .line 2724714
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724715
    iget-boolean v1, v0, LX/DAU;->k:Z

    move v0, v1

    .line 2724716
    if-nez v0, :cond_b

    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724717
    iget-object v1, v0, LX/DAU;->b:LX/3OI;

    move-object v0, v1

    .line 2724718
    sget-object v1, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    if-ne v0, v1, :cond_11

    :cond_b
    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 2724719
    if-nez v0, :cond_c

    .line 2724720
    iget-object v0, p0, LX/Jhc;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto/16 :goto_6

    .line 2724721
    :cond_c
    iget-object v0, p0, LX/Jhc;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2724722
    const v0, 0x7f0d201a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2724723
    const v1, 0x7f0d201b

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2724724
    const v2, 0x7f0d203e

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2724725
    iget-object v3, p0, LX/Jhc;->A:LX/DAU;

    .line 2724726
    iget-object v6, v3, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v3, v6

    .line 2724727
    if-eqz v3, :cond_d

    iget-object v3, p0, LX/Jhc;->A:LX/DAU;

    .line 2724728
    iget-object v6, v3, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v3, v6

    .line 2724729
    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    if-eqz v3, :cond_d

    iget-object v3, p0, LX/Jhc;->A:LX/DAU;

    .line 2724730
    iget-object v6, v3, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v3, v6

    .line 2724731
    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a()Z

    move-result v3

    if-eqz v3, :cond_d

    const/4 v3, 0x1

    .line 2724732
    :goto_9
    iget-object v6, p0, LX/Jhc;->A:LX/DAU;

    .line 2724733
    iget-boolean v7, v6, LX/DAU;->j:Z

    move v6, v7

    .line 2724734
    if-eqz v6, :cond_e

    iget-object v6, p0, LX/Jhc;->A:LX/DAU;

    .line 2724735
    iget-object v7, v6, LX/DAU;->n:Ljava/lang/String;

    move-object v6, v7

    .line 2724736
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 2724737
    iget-object v6, p0, LX/Jhc;->f:LX/EFW;

    invoke-virtual {v6, v3}, LX/EFW;->a(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2724738
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2724739
    new-instance v6, LX/Jha;

    invoke-direct {v6, p0, v3}, LX/Jha;-><init>(LX/Jhc;Z)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724740
    :goto_a
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724741
    iget-boolean v6, v0, LX/DAU;->k:Z

    move v0, v6

    .line 2724742
    if-eqz v0, :cond_f

    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724743
    iget-object v6, v0, LX/DAU;->o:Ljava/lang/String;

    move-object v0, v6

    .line 2724744
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, LX/Jhc;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2724745
    iget-object v0, p0, LX/Jhc;->f:LX/EFW;

    invoke-virtual {v0, v3}, LX/EFW;->b(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2724746
    new-instance v0, LX/Jhb;

    invoke-direct {v0, p0, v3}, LX/Jhb;-><init>(LX/Jhc;Z)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724747
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2724748
    :goto_b
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724749
    iget-object v1, v0, LX/DAU;->b:LX/3OI;

    move-object v0, v1

    .line 2724750
    sget-object v1, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    if-ne v0, v1, :cond_10

    :goto_c
    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_d
    move v3, v4

    .line 2724751
    goto :goto_9

    .line 2724752
    :cond_e
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_a

    .line 2724753
    :cond_f
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_b

    :cond_10
    move v4, v5

    .line 2724754
    goto :goto_c

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 2724755
    :cond_12
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724756
    iget-boolean v1, v0, LX/DAU;->l:Z

    move v0, v1

    .line 2724757
    if-eqz v0, :cond_14

    .line 2724758
    iget-object v0, p0, LX/Jhc;->x:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2724759
    iget-object v0, p0, LX/Jhc;->x:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    .line 2724760
    iget-boolean v2, v1, LX/DAU;->g:Z

    move v1, v2

    .line 2724761
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2724762
    iget-object v0, p0, LX/Jhc;->x:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    .line 2724763
    iget-boolean v2, v1, LX/DAU;->g:Z

    move v1, v2

    .line 2724764
    if-eqz v1, :cond_13

    invoke-virtual {p0}, LX/Jhc;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08030c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_d
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2724765
    iget-object v0, p0, LX/Jhc;->x:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2724766
    new-instance v1, LX/JhZ;

    invoke-direct {v1, p0, p0}, LX/JhZ;-><init>(LX/Jhc;LX/Jhc;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2724767
    goto/16 :goto_7

    .line 2724768
    :cond_13
    invoke-virtual {p0}, LX/Jhc;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08043f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_d

    .line 2724769
    :cond_14
    iget-object v0, p0, LX/Jhc;->w:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2724770
    iget-object v0, p0, LX/Jhc;->x:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto/16 :goto_7
.end method

.method private f()V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x1

    const/16 v5, 0x8

    const/4 v8, 0x0

    .line 2724771
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724772
    iget-object v1, v0, LX/DAU;->b:LX/3OI;

    move-object v0, v1

    .line 2724773
    sget-object v1, LX/DAT;->CALL_LOG:LX/DAT;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    .line 2724774
    iget-object v1, v0, LX/DAU;->b:LX/3OI;

    move-object v0, v1

    .line 2724775
    sget-object v1, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    if-eq v0, v1, :cond_1

    .line 2724776
    iget-object v0, p0, LX/Jhc;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2724777
    :cond_0
    :goto_0
    return-void

    .line 2724778
    :cond_1
    iget-object v0, p0, LX/Jhc;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2724779
    const v0, 0x7f0d2001

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Jhc;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 2724780
    iget-object v0, p0, LX/Jhc;->y:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    .line 2724781
    iget-object v2, v1, LX/DAU;->p:Ljava/lang/String;

    move-object v1, v2

    .line 2724782
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2724783
    iget-object v0, p0, LX/Jhc;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2724784
    iget-object v0, p0, LX/Jhc;->n:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-virtual {v0, v5}, Lcom/facebook/messaging/ui/name/ThreadNameView;->setVisibility(I)V

    .line 2724785
    const v0, 0x7f0d203d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724786
    const v1, 0x7f0d203c

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724787
    const v2, 0x7f0d203b

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724788
    const v3, 0x7f0d203a

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2724789
    const v4, 0x7f0d2039

    invoke-virtual {p0, v4}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 2724790
    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724791
    invoke-virtual {v1, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724792
    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724793
    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724794
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2724795
    iget-object v5, p0, LX/Jhc;->A:LX/DAU;

    .line 2724796
    iget-object v6, v5, LX/DAU;->b:LX/3OI;

    move-object v5, v6

    .line 2724797
    sget-object v6, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    if-ne v5, v6, :cond_2

    .line 2724798
    iget-object v0, p0, LX/Jhc;->z:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 2724799
    iget-object v0, p0, LX/Jhc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/contacts/picker/ContactPickerListGroupItem$2;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/contacts/picker/ContactPickerListGroupItem$2;-><init>(LX/Jhc;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/Jhc;->z:Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_0

    .line 2724800
    :cond_2
    iget-object v5, p0, LX/Jhc;->z:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v5, :cond_3

    .line 2724801
    iget-object v5, p0, LX/Jhc;->z:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v5, v9}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2724802
    const/4 v5, 0x0

    iput-object v5, p0, LX/Jhc;->z:Ljava/util/concurrent/ScheduledFuture;

    .line 2724803
    :cond_3
    iget-object v5, p0, LX/Jhc;->A:LX/DAU;

    .line 2724804
    iget v6, v5, LX/DAU;->u:I

    move v5, v6

    .line 2724805
    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2724806
    packed-switch v6, :pswitch_data_0

    .line 2724807
    :goto_1
    if-ne v6, v10, :cond_4

    .line 2724808
    invoke-virtual {v4, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2724809
    :pswitch_0
    iget-object v5, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v5}, LX/DAU;->p()LX/0Px;

    move-result-object v5

    const/4 v7, 0x2

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724810
    const v5, 0x7f0a0300

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2724811
    invoke-virtual {v2, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724812
    :pswitch_1
    iget-object v2, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v2}, LX/DAU;->p()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724813
    const v2, 0x7f0a0300

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2724814
    invoke-virtual {v1, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2724815
    :pswitch_2
    iget-object v1, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v1}, LX/DAU;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724816
    const v1, 0x7f0a0300

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2724817
    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_1

    .line 2724818
    :cond_4
    const/4 v0, 0x4

    if-ne v6, v0, :cond_0

    .line 2724819
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    invoke-virtual {v0}, LX/DAU;->p()LX/0Px;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2724820
    const v0, 0x7f0a0300

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2724821
    invoke-virtual {v3, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getContactRow()LX/DAU;
    .locals 1

    .prologue
    .line 2724822
    iget-object v0, p0, LX/Jhc;->A:LX/DAU;

    return-object v0
.end method
