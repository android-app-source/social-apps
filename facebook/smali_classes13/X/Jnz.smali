.class public LX/Jnz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Jnw;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AMO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2734750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2734751
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Jnz;->a:Ljava/util/Map;

    .line 2734752
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2734753
    iput-object v0, p0, LX/Jnz;->b:LX/0Ot;

    .line 2734754
    return-void
.end method

.method private static c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V
    .locals 2

    .prologue
    .line 2734755
    iget-object v0, p0, Lcom/facebook/messaging/montage/model/art/EffectItem;->j:LX/0Px;

    move-object v0, v0

    .line 2734756
    if-nez v0, :cond_0

    .line 2734757
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The effect is not a mask."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2734758
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/montage/model/art/EffectItem;)LX/Jnw;
    .locals 2

    .prologue
    .line 2734759
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/Jnz;->c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734760
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2734761
    iget-object v1, p0, LX/Jnz;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2734762
    iget-object v1, p0, LX/Jnz;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jnw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734763
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2734764
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Jnz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    .line 2734765
    iget-object v1, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->j:LX/0Px;

    move-object v1, v1

    .line 2734766
    invoke-virtual {v0, v1}, LX/AMO;->c(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2734767
    sget-object v0, LX/Jnw;->COMPLETED:LX/Jnw;

    goto :goto_0

    .line 2734768
    :cond_1
    sget-object v0, LX/Jnw;->NOT_STARTED:LX/Jnw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2734769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/montage/model/art/EffectItem;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/montage/model/art/EffectItem;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2734770
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/Jnz;->c(Lcom/facebook/messaging/montage/model/art/EffectItem;)V

    .line 2734771
    iget-object v0, p1, Lcom/facebook/messaging/montage/model/art/BaseItem;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2734772
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 2734773
    iget-object v0, p0, LX/Jnz;->a:Ljava/util/Map;

    sget-object v3, LX/Jnw;->IN_PROGRESS:LX/Jnw;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2734774
    iget-object v0, p0, LX/Jnz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    .line 2734775
    iget-object v3, p1, Lcom/facebook/messaging/montage/model/art/EffectItem;->j:LX/0Px;

    move-object v3, v3

    .line 2734776
    new-instance v4, LX/Jny;

    invoke-direct {v4, p0, v1, v2}, LX/Jny;-><init>(LX/Jnz;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v0, v3, v4}, LX/AMO;->a(LX/0Px;LX/AMN;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2734777
    monitor-exit p0

    return-object v2

    .line 2734778
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
