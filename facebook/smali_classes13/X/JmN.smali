.class public LX/JmN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2732668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/0P1;)Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;",
            "Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;)",
            "Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2732669
    invoke-virtual {p1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v1

    .line 2732670
    if-nez v1, :cond_1

    .line 2732671
    :cond_0
    :goto_0
    return-object v0

    .line 2732672
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->F()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2732673
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->F()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;

    move-result-object v0

    invoke-static {v0}, LX/JmN;->a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;)Lcom/facebook/user/model/User;

    move-result-object v0

    const/4 v9, 0x0

    .line 2732674
    new-instance v4, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    sget-object v7, LX/JmJ;->USER:LX/JmJ;

    move-object v5, p0

    move-object v6, p1

    move-object v8, v0

    move-object v10, v9

    move-object v11, v9

    invoke-direct/range {v4 .. v11}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/JmJ;Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/8ue;)V

    move-object v0, v4

    .line 2732675
    goto :goto_0

    .line 2732676
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->z()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2732677
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->z()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;

    move-result-object v0

    .line 2732678
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    .line 2732679
    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2732680
    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 2732681
    iput-object v2, v1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2732682
    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel$PageModel;->m()Z

    move-result v2

    .line 2732683
    iput-boolean v2, v1, LX/0XI;->z:Z

    .line 2732684
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    move-object v0, v1

    .line 2732685
    const/4 v8, 0x0

    .line 2732686
    new-instance v4, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    sget-object v7, LX/JmJ;->PAGE:LX/JmJ;

    move-object v5, p0

    move-object v6, p1

    move-object v9, v0

    move-object v10, v8

    move-object v11, v8

    invoke-direct/range {v4 .. v11}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/JmJ;Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/8ue;)V

    move-object v0, v4

    .line 2732687
    goto :goto_0

    .line 2732688
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->D()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 2732689
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->D()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/JmN;->a(LX/15i;I)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2732690
    invoke-virtual {p2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2732691
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732692
    const/4 v8, 0x0

    .line 2732693
    new-instance v4, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;

    sget-object v7, LX/JmJ;->GROUP:LX/JmJ;

    move-object v5, p0

    move-object v6, p1

    move-object v9, v8

    move-object v10, v0

    move-object v11, v8

    invoke-direct/range {v4 .. v11}, Lcom/facebook/messaging/inbox2/horizontaltiles/HorizontalTileInboxItem;-><init>(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;LX/JmJ;Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/8ue;)V

    move-object v0, v4

    .line 2732694
    goto/16 :goto_0
.end method

.method public static a(LX/15i;I)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2732695
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    const v1, -0x2717cc77

    invoke-static {p0, v0, v1}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 2732696
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p0, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2732697
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p0, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;)Lcom/facebook/user/model/User;
    .locals 14

    .prologue
    .line 2732698
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    .line 2732699
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2732700
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 2732701
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->m_()Ljava/lang/String;

    move-result-object v6

    .line 2732702
    if-eqz v6, :cond_1

    .line 2732703
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v5, v2

    move-object v4, v3

    :goto_0
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;

    .line 2732704
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v9

    .line 2732705
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v10

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->a()I

    move-result v11

    add-int/2addr v10, v11

    .line 2732706
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v11

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v11, v12, :cond_0

    .line 2732707
    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object v13, v3

    move-object v3, v2

    move-object v2, v13

    .line 2732708
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v3

    move-object v3, v2

    goto :goto_0

    .line 2732709
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v2

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v2, v11, :cond_3

    .line 2732710
    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object v3, v4

    goto :goto_1

    :cond_1
    move-object v4, v3

    .line 2732711
    :cond_2
    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-direct {v2, v4, v3, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 2732712
    iput-object v1, v0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2732713
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;->k()Z

    move-result v1

    .line 2732714
    iput-boolean v1, v0, LX/0XI;->z:Z

    .line 2732715
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel;->l()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel$MessengerContactModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$PeopleYouMayMessageUserInfoModel$MessengerContactModel;->a()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v1

    invoke-static {v1}, LX/6Ok;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/0XK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0XI;->a(LX/0XK;)LX/0XI;

    .line 2732716
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v2, v3

    move-object v3, v4

    goto :goto_1
.end method
