.class public LX/K0R;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2758652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2758653
    return-void
.end method

.method public static a(IILjava/util/List;)LX/K0Q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/9nR;",
            ">;)",
            "LX/K0Q;"
        }
    .end annotation

    .prologue
    .line 2758675
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p0, p1, p2, v0, v1}, LX/K0R;->a(IILjava/util/List;D)LX/K0Q;

    move-result-object v0

    return-object v0
.end method

.method public static a(IILjava/util/List;D)LX/K0Q;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/9nR;",
            ">;D)",
            "LX/K0Q;"
        }
    .end annotation

    .prologue
    .line 2758654
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2758655
    new-instance v2, LX/K0Q;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/K0Q;-><init>(LX/9nR;LX/9nR;)V

    .line 2758656
    :goto_0
    return-object v2

    .line 2758657
    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 2758658
    new-instance v3, LX/K0Q;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9nR;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, LX/K0Q;-><init>(LX/9nR;LX/9nR;)V

    move-object v2, v3

    goto :goto_0

    .line 2758659
    :cond_1
    if-lez p0, :cond_2

    if-gtz p1, :cond_3

    .line 2758660
    :cond_2
    new-instance v2, LX/K0Q;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/K0Q;-><init>(LX/9nR;LX/9nR;)V

    goto :goto_0

    .line 2758661
    :cond_3
    invoke-static {}, LX/1FE;->a()LX/1FE;

    move-result-object v2

    invoke-virtual {v2}, LX/1FE;->h()LX/1HI;

    move-result-object v11

    .line 2758662
    const/4 v7, 0x0

    .line 2758663
    const/4 v6, 0x0

    .line 2758664
    mul-int v2, p0, p1

    int-to-double v2, v2

    mul-double v12, v2, p3

    .line 2758665
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2758666
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2758667
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-wide v8, v2

    move-object v10, v7

    move-object v3, v6

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9nR;

    .line 2758668
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2}, LX/9nR;->c()D

    move-result-wide v16

    div-double v16, v16, v12

    sub-double v6, v6, v16

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    .line 2758669
    cmpg-double v15, v6, v8

    if-gez v15, :cond_4

    move-wide v8, v6

    move-object v10, v2

    .line 2758670
    :cond_4
    cmpg-double v15, v6, v4

    if-gez v15, :cond_8

    invoke-virtual {v2}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v11, v15}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v15

    if-nez v15, :cond_5

    invoke-virtual {v2}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v11, v15}, LX/1HI;->c(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_8

    :cond_5
    move-object v4, v2

    move-wide v2, v6

    :goto_2
    move-wide/from16 v18, v2

    move-object v3, v4

    move-wide/from16 v4, v18

    .line 2758671
    goto :goto_1

    .line 2758672
    :cond_6
    if-eqz v3, :cond_7

    if-eqz v10, :cond_7

    invoke-virtual {v3}, LX/9nR;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, LX/9nR;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2758673
    const/4 v3, 0x0

    .line 2758674
    :cond_7
    new-instance v2, LX/K0Q;

    invoke-direct {v2, v10, v3}, LX/K0Q;-><init>(LX/9nR;LX/9nR;)V

    goto/16 :goto_0

    :cond_8
    move-wide/from16 v18, v4

    move-object v4, v3

    move-wide/from16 v2, v18

    goto :goto_2
.end method
