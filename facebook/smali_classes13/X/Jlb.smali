.class public final enum LX/Jlb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jlb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jlb;

.field public static final enum DEFAULT:LX/Jlb;

.field public static final enum DEFAULT_WITH_TOP_UNITS_FIRST:LX/Jlb;

.field public static final enum FORCE_SERVER_FETCH:LX/Jlb;

.field public static final enum TOP_UNITS_PRELOAD_ONLY:LX/Jlb;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2731639
    new-instance v0, LX/Jlb;

    const-string v1, "FORCE_SERVER_FETCH"

    invoke-direct {v0, v1, v2}, LX/Jlb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlb;->FORCE_SERVER_FETCH:LX/Jlb;

    .line 2731640
    new-instance v0, LX/Jlb;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/Jlb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlb;->DEFAULT:LX/Jlb;

    .line 2731641
    new-instance v0, LX/Jlb;

    const-string v1, "DEFAULT_WITH_TOP_UNITS_FIRST"

    invoke-direct {v0, v1, v4}, LX/Jlb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlb;->DEFAULT_WITH_TOP_UNITS_FIRST:LX/Jlb;

    .line 2731642
    new-instance v0, LX/Jlb;

    const-string v1, "TOP_UNITS_PRELOAD_ONLY"

    invoke-direct {v0, v1, v5}, LX/Jlb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Jlb;->TOP_UNITS_PRELOAD_ONLY:LX/Jlb;

    .line 2731643
    const/4 v0, 0x4

    new-array v0, v0, [LX/Jlb;

    sget-object v1, LX/Jlb;->FORCE_SERVER_FETCH:LX/Jlb;

    aput-object v1, v0, v2

    sget-object v1, LX/Jlb;->DEFAULT:LX/Jlb;

    aput-object v1, v0, v3

    sget-object v1, LX/Jlb;->DEFAULT_WITH_TOP_UNITS_FIRST:LX/Jlb;

    aput-object v1, v0, v4

    sget-object v1, LX/Jlb;->TOP_UNITS_PRELOAD_ONLY:LX/Jlb;

    aput-object v1, v0, v5

    sput-object v0, LX/Jlb;->$VALUES:[LX/Jlb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2731644
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jlb;
    .locals 1

    .prologue
    .line 2731645
    const-class v0, LX/Jlb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jlb;

    return-object v0
.end method

.method public static values()[LX/Jlb;
    .locals 1

    .prologue
    .line 2731646
    sget-object v0, LX/Jlb;->$VALUES:[LX/Jlb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jlb;

    return-object v0
.end method
