.class public final LX/Jv3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Tn;

.field public final synthetic b:Z

.field public final synthetic c:LX/4ok;

.field public final synthetic d:LX/Jv4;


# direct methods
.method public constructor <init>(LX/Jv4;LX/0Tn;ZLX/4ok;)V
    .locals 0

    .prologue
    .line 2749626
    iput-object p1, p0, LX/Jv3;->d:LX/Jv4;

    iput-object p2, p0, LX/Jv3;->a:LX/0Tn;

    iput-boolean p3, p0, LX/Jv3;->b:Z

    iput-object p4, p0, LX/Jv3;->c:LX/4ok;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2749627
    iget-object v0, p0, LX/Jv3;->c:LX/4ok;

    if-eqz v0, :cond_0

    .line 2749628
    iget-object v0, p0, LX/Jv3;->d:LX/Jv4;

    iget-object v1, p0, LX/Jv3;->a:LX/0Tn;

    iget-boolean v2, p0, LX/Jv3;->b:Z

    iget-object v3, p0, LX/Jv3;->c:LX/4ok;

    .line 2749629
    new-instance v4, LX/31Y;

    iget-object v5, v0, LX/Jv4;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2749630
    const v5, 0x7f083aee

    invoke-virtual {v4, v5}, LX/0ju;->a(I)LX/0ju;

    .line 2749631
    const v5, 0x7f083aef

    invoke-virtual {v4, v5}, LX/0ju;->b(I)LX/0ju;

    .line 2749632
    const v5, 0x7f083af0

    new-instance v6, LX/Juu;

    invoke-direct {v6, v0, v1, v2, v3}, LX/Juu;-><init>(LX/Jv4;LX/0Tn;ZLX/4ok;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749633
    const/high16 v5, 0x1040000

    new-instance v6, LX/Juv;

    invoke-direct {v6, v0, v3, v2, v1}, LX/Juv;-><init>(LX/Jv4;LX/4ok;ZLX/0Tn;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2749634
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0ju;->a(Z)LX/0ju;

    .line 2749635
    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2749636
    :cond_0
    iget-object v0, p0, LX/Jv3;->d:LX/Jv4;

    iget-object v0, v0, LX/Jv4;->d:LX/03V;

    sget-object v1, LX/Jv4;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to persist setting:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Jv3;->a:LX/0Tn;

    invoke-virtual {v3}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2749637
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2749638
    iget-object v0, p0, LX/Jv3;->a:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    iget-boolean v0, p0, LX/Jv3;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 2749639
    return-void
.end method
