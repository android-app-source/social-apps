.class public final LX/K6H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5l;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardImage;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardImage;)V
    .locals 0

    .prologue
    .line 2771364
    iput-object p1, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardImage;B)V
    .locals 0

    .prologue
    .line 2771363
    invoke-direct {p0, p1}, LX/K6H;-><init>(Lcom/facebook/tarot/cards/TarotCardImage;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2771360
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a()V

    .line 2771361
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->d:LX/K6S;

    invoke-virtual {v0, v1, v1}, LX/K6S;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771362
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 2771352
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setHeadlineLineHeightMultiplier(F)V

    .line 2771353
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2771358
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setHeadlineText(Ljava/lang/String;)V

    .line 2771359
    return-void
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 2771356
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionLineHeightMultiplier(F)V

    .line 2771357
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2771354
    iget-object v0, p0, LX/K6H;->a:Lcom/facebook/tarot/cards/TarotCardImage;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardImage;->g:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionText(Ljava/lang/String;)V

    .line 2771355
    return-void
.end method
