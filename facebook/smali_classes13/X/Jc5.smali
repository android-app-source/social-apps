.class public final LX/Jc5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/4ok;

.field public final synthetic b:Lcom/facebook/katana/settings/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/settings/activity/SettingsActivity;LX/4ok;)V
    .locals 0

    .prologue
    .line 2717393
    iput-object p1, p0, LX/Jc5;->b:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iput-object p2, p0, LX/Jc5;->a:LX/4ok;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2717394
    iget-object v0, p0, LX/Jc5;->b:Lcom/facebook/katana/settings/activity/SettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/settings/activity/SettingsActivity;->y:LX/A6X;

    iget-object v1, p0, LX/Jc5;->a:LX/4ok;

    invoke-virtual {v1}, LX/4ok;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, p2}, LX/A6X;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 2717395
    const/4 v0, 0x1

    return v0
.end method
