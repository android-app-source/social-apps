.class public LX/Jqx;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kU;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jrc;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jqb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDs;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2741004
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Jqx;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741005
    invoke-direct {p0, p1}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2741006
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741007
    iput-object v0, p0, LX/Jqx;->a:LX/0Ot;

    .line 2741008
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741009
    iput-object v0, p0, LX/Jqx;->b:LX/0Ot;

    .line 2741010
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741011
    iput-object v0, p0, LX/Jqx;->c:LX/0Ot;

    .line 2741012
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741013
    iput-object v0, p0, LX/Jqx;->d:LX/0Ot;

    .line 2741014
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741015
    iput-object v0, p0, LX/Jqx;->e:LX/0Ot;

    .line 2741016
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2741017
    iput-object v0, p0, LX/Jqx;->f:LX/0Ot;

    .line 2741018
    return-void
.end method

.method public static a(LX/0QB;)LX/Jqx;
    .locals 12

    .prologue
    .line 2741019
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2741020
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2741021
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2741022
    if-nez v1, :cond_0

    .line 2741023
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2741024
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2741025
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2741026
    sget-object v1, LX/Jqx;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2741027
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2741028
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2741029
    :cond_1
    if-nez v1, :cond_4

    .line 2741030
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2741031
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2741032
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2741033
    new-instance v1, LX/Jqx;

    const/16 v7, 0x35bd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct {v1, v7}, LX/Jqx;-><init>(LX/0Ot;)V

    .line 2741034
    const/16 v7, 0xce5

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29fd

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x29c4

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xd18

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2739

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0x2e3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2741035
    iput-object v7, v1, LX/Jqx;->a:LX/0Ot;

    iput-object v8, v1, LX/Jqx;->b:LX/0Ot;

    iput-object v9, v1, LX/Jqx;->c:LX/0Ot;

    iput-object v10, v1, LX/Jqx;->d:LX/0Ot;

    iput-object v11, v1, LX/Jqx;->e:LX/0Ot;

    iput-object p0, v1, LX/Jqx;->f:LX/0Ot;

    .line 2741036
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2741037
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2741038
    if-nez v1, :cond_2

    .line 2741039
    sget-object v0, LX/Jqx;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqx;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2741040
    :goto_1
    if-eqz v0, :cond_3

    .line 2741041
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741042
    :goto_3
    check-cast v0, LX/Jqx;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2741043
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2741044
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2741045
    :catchall_1
    move-exception v0

    .line 2741046
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2741047
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2741048
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2741049
    :cond_2
    :try_start_8
    sget-object v0, LX/Jqx;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jqx;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2741050
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2741051
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2741052
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2741053
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kU;

    invoke-virtual {v0}, LX/6kU;->c()LX/6jr;

    move-result-object v3

    .line 2741054
    iget-object v0, p0, LX/Jqx;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    iget-object v1, p0, LX/Jqx;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jrc;

    iget-object v4, v3, LX/6jr;->threadKey:LX/6l9;

    invoke-virtual {v1, v4}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2741055
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741056
    if-nez v4, :cond_0

    move-object v0, v2

    .line 2741057
    :goto_0
    return-object v0

    .line 2741058
    :cond_0
    iget-object v3, v3, LX/6jr;->threadDescription:Ljava/lang/String;

    .line 2741059
    iget-object v0, p0, LX/Jqx;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    iget-object v1, p0, LX/Jqx;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    .line 2741060
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v1

    iget-object v5, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {v1, v5}, LX/6fm;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6fm;

    move-result-object v1

    .line 2741061
    iput-object v3, v1, LX/6fm;->f:Ljava/lang/String;

    .line 2741062
    move-object v1, v1

    .line 2741063
    invoke-virtual {v1}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v1

    .line 2741064
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    move-result-object v1

    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2741065
    invoke-static {v0, v1, v6, v7}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741066
    iget-object v5, v0, LX/FDs;->d:LX/2N4;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 p0, 0x0

    invoke-virtual {v5, v1, p0}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v1

    .line 2741067
    if-eqz v0, :cond_1

    .line 2741068
    const-string v1, "group_description_thread_summary"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    move-object v0, v2

    .line 2741069
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2741070
    const-string v0, "group_description_thread_summary"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2741071
    if-eqz v0, :cond_0

    .line 2741072
    iget-object v1, p0, LX/Jqx;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v2, p0, LX/Jqx;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Oe;->c(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2741073
    iget-object v1, p0, LX/Jqx;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jqb;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2741074
    invoke-static {v1, v0}, LX/Jqb;->e(LX/Jqb;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    .line 2741075
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2741076
    check-cast p1, LX/6kU;

    .line 2741077
    invoke-virtual {p1}, LX/6kU;->c()LX/6jr;

    move-result-object v1

    .line 2741078
    iget-object v0, p0, LX/Jqx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jrc;

    iget-object v1, v1, LX/6jr;->threadKey:LX/6l9;

    invoke-virtual {v0, v1}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
