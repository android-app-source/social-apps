.class public LX/JyG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JyE;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2754437
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JyG;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2754438
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2754439
    iput-object p1, p0, LX/JyG;->b:LX/0Ot;

    .line 2754440
    return-void
.end method

.method public static a(LX/0QB;)LX/JyG;
    .locals 4

    .prologue
    .line 2754441
    const-class v1, LX/JyG;

    monitor-enter v1

    .line 2754442
    :try_start_0
    sget-object v0, LX/JyG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2754443
    sput-object v2, LX/JyG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2754444
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754445
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2754446
    new-instance v3, LX/JyG;

    const/16 p0, 0x300b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JyG;-><init>(LX/0Ot;)V

    .line 2754447
    move-object v0, v3

    .line 2754448
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2754449
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JyG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2754450
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2754451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2754452
    const v0, 0xdce64a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2754453
    check-cast p2, LX/JyF;

    .line 2754454
    iget-object v0, p0, LX/JyG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;

    iget-object v1, p2, LX/JyF;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;

    .line 2754455
    iget-object v2, v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    .line 2754456
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    const v5, 0x7f0b26d9    # 1.849644E38f

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    .line 2754457
    const v4, 0xdce64a

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2754458
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    .line 2754459
    invoke-static {v0, p1, v1, v2}, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->a(Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;LX/1De;Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;LX/1Ad;)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x2

    .line 2754460
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v3

    .line 2754461
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2754462
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lb_()Ljava/lang/String;

    move-result-object v5

    .line 2754463
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2754464
    const/4 v6, 0x0

    const p0, 0x7f0e011e

    invoke-static {p1, v6, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2754465
    :cond_0
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->e:LX/JyJ;

    invoke-virtual {v4, p1}, LX/JyJ;->c(LX/1De;)LX/JyH;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/JyH;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;)LX/JyH;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/JyH;->h(I)LX/JyH;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 2754466
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2754467
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    .line 2754468
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLModels$ProfileDiscoveryBucketFieldsModel;->lc_()I

    move-result v4

    if-lez v4, :cond_1

    .line 2754469
    iget-object v4, v0, Lcom/facebook/profile/discovery/components/DiscoveryListItemComponentSpec;->d:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v5, 0x7f0a008f

    invoke-virtual {v4, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    const v5, 0x7f02071f

    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    .line 2754470
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    const v6, 0x7f0b26d9    # 1.849644E38f

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2754471
    :cond_1
    move-object v3, v3

    .line 2754472
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2754473
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2754474
    invoke-static {}, LX/1dS;->b()V

    .line 2754475
    iget v0, p1, LX/1dQ;->b:I

    .line 2754476
    packed-switch v0, :pswitch_data_0

    .line 2754477
    :goto_0
    return-object v2

    .line 2754478
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2754479
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2754480
    check-cast v1, LX/JyF;

    .line 2754481
    iget-object p1, p0, LX/JyG;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/JyF;->b:Landroid/view/View$OnClickListener;

    .line 2754482
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2754483
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xdce64a
        :pswitch_0
    .end packed-switch
.end method
