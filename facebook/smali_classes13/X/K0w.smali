.class public LX/K0w;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static c:LX/K0w;


# instance fields
.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/K0v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2759920
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v3

    const-string v1, "_bold"

    aput-object v1, v0, v4

    const-string v1, "_italic"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "_bold_italic"

    aput-object v2, v0, v1

    sput-object v0, LX/K0w;->a:[Ljava/lang/String;

    .line 2759921
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, ".ttf"

    aput-object v1, v0, v3

    const-string v1, ".otf"

    aput-object v1, v0, v4

    sput-object v0, LX/K0w;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2759897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2759898
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K0w;->d:Ljava/util/Map;

    .line 2759899
    return-void
.end method

.method public static a()LX/K0w;
    .locals 1

    .prologue
    .line 2759900
    sget-object v0, LX/K0w;->c:LX/K0w;

    if-nez v0, :cond_0

    .line 2759901
    new-instance v0, LX/K0w;

    invoke-direct {v0}, LX/K0w;-><init>()V

    sput-object v0, LX/K0w;->c:LX/K0w;

    .line 2759902
    :cond_0
    sget-object v0, LX/K0w;->c:LX/K0w;

    return-object v0
.end method

.method private static b(Ljava/lang/String;ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2759903
    sget-object v0, LX/K0w;->a:[Ljava/lang/String;

    aget-object v1, v0, p1

    .line 2759904
    sget-object v2, LX/K0w;->b:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2759905
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fonts/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2759906
    :try_start_0
    invoke-static {p2, v4}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2759907
    :goto_1
    return-object v0

    .line 2759908
    :catch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2759909
    :cond_0
    invoke-static {p0, p1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2759910
    iget-object v0, p0, LX/K0w;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K0v;

    .line 2759911
    if-nez v0, :cond_0

    .line 2759912
    new-instance v0, LX/K0v;

    invoke-direct {v0}, LX/K0v;-><init>()V

    .line 2759913
    iget-object v1, p0, LX/K0w;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759914
    :cond_0
    invoke-virtual {v0, p2}, LX/K0v;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 2759915
    if-nez v1, :cond_1

    .line 2759916
    invoke-static {p1, p2, p3}, LX/K0w;->b(Ljava/lang/String;ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 2759917
    if-eqz v1, :cond_1

    .line 2759918
    invoke-virtual {v0, p2, v1}, LX/K0v;->a(ILandroid/graphics/Typeface;)V

    :cond_1
    move-object v0, v1

    .line 2759919
    return-object v0
.end method
