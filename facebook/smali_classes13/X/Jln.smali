.class public LX/Jln;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/Jlx;

.field private final c:LX/0Xl;

.field private final d:LX/0Yb;

.field private final e:LX/Jlg;

.field public f:LX/Jll;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public g:LX/Jlv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2731726
    const-class v0, LX/Jln;

    sput-object v0, LX/Jln;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Jlx;LX/0Xl;)V
    .locals 3
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param

    .prologue
    .line 2731727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2731728
    new-instance v0, LX/Jlg;

    invoke-direct {v0, p0}, LX/Jlg;-><init>(LX/Jln;)V

    iput-object v0, p0, LX/Jln;->e:LX/Jlg;

    .line 2731729
    iput-object p1, p0, LX/Jln;->b:LX/Jlx;

    .line 2731730
    iput-object p2, p0, LX/Jln;->c:LX/0Xl;

    .line 2731731
    new-instance v0, LX/Jlh;

    invoke-direct {v0, p0}, LX/Jlh;-><init>(LX/Jln;)V

    .line 2731732
    iget-object v1, p0, LX/Jln;->c:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/Jln;->d:LX/0Yb;

    .line 2731733
    return-void
.end method

.method private static a(LX/JlW;LX/JlW;)LX/Jlm;
    .locals 3
    .param p0    # LX/JlW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2731734
    iget-object v0, p1, LX/JlW;->b:LX/JlY;

    sget-object v1, LX/JlY;->TOP:LX/JlY;

    if-ne v0, v1, :cond_0

    .line 2731735
    sget-object v0, LX/Jlm;->START_NEW_INDEPENDENT_FETCH:LX/Jlm;

    .line 2731736
    :goto_0
    return-object v0

    .line 2731737
    :cond_0
    if-eqz p0, :cond_1

    iget-object v0, p0, LX/JlW;->a:LX/0rS;

    .line 2731738
    :goto_1
    sget-object v1, LX/Jlj;->b:[I

    iget-object v2, p1, LX/JlW;->a:LX/0rS;

    invoke-virtual {v2}, LX/0rS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2731739
    sget-object v0, LX/Jlm;->START_NEW_INDEPENDENT_FETCH:LX/Jlm;

    goto :goto_0

    .line 2731740
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2731741
    :pswitch_0
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v0, v1, :cond_2

    .line 2731742
    sget-object v0, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    goto :goto_0

    .line 2731743
    :cond_2
    sget-object v0, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    goto :goto_0

    .line 2731744
    :pswitch_1
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-ne v0, v1, :cond_4

    .line 2731745
    :cond_3
    sget-object v0, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    goto :goto_0

    .line 2731746
    :cond_4
    sget-object v0, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    goto :goto_0

    .line 2731747
    :pswitch_2
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v0, v1, :cond_5

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v0, v1, :cond_5

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-ne v0, v1, :cond_6

    .line 2731748
    :cond_5
    sget-object v0, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    goto :goto_0

    .line 2731749
    :cond_6
    sget-object v0, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    goto :goto_0

    .line 2731750
    :pswitch_3
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v0, v1, :cond_7

    .line 2731751
    sget-object v0, LX/Jlm;->USE_CURRENT_FETCH:LX/Jlm;

    goto :goto_0

    .line 2731752
    :cond_7
    if-nez v0, :cond_8

    .line 2731753
    sget-object v0, LX/Jlm;->START_NEW_FETCH_AND_TRACK_RESULT:LX/Jlm;

    goto :goto_0

    .line 2731754
    :cond_8
    sget-object v0, LX/Jlm;->START_NEW_INDEPENDENT_FETCH:LX/Jlm;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static declared-synchronized a(LX/Jln;Z)V
    .locals 1

    .prologue
    .line 2731755
    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, LX/Jln;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2731756
    iget-object v0, p0, LX/Jln;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731757
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2731758
    :cond_1
    if-nez p1, :cond_0

    :try_start_1
    iget-object v0, p0, LX/Jln;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2731759
    iget-object v0, p0, LX/Jln;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2731760
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/Jln;Ljava/util/Set;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)Z"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2731761
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731762
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    .line 2731763
    iget-object v1, v0, LX/Jlv;->b:LX/JlX;

    move-object v0, v1

    .line 2731764
    iget-object v5, v0, LX/JlX;->d:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JlS;

    .line 2731765
    iget-object v1, v0, LX/JlS;->a:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->MESSAGE_THREADS:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    if-ne v1, v3, :cond_1

    .line 2731766
    iget-object v1, v0, LX/JlS;->c:Ljava/lang/Object;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2731767
    iget-object v0, v0, LX/JlS;->c:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    .line 2731768
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;

    .line 2731769
    iget-object p0, v1, Lcom/facebook/messaging/inbox2/items/InboxUnitThreadItem;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, p0

    .line 2731770
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2731771
    const/4 v0, 0x1

    .line 2731772
    :goto_2
    return v0

    .line 2731773
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2731774
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2731775
    goto :goto_2
.end method

.method public static declared-synchronized a$redex0(LX/Jln;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2731776
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2731777
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2731778
    :cond_1
    const/4 v2, 0x0

    .line 2731779
    :try_start_1
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2731780
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2731781
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-static {p0, v0}, LX/Jln;->a(LX/Jln;Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    move v1, v0

    .line 2731782
    :cond_2
    :goto_2
    if-eqz v1, :cond_0

    .line 2731783
    invoke-static {p0}, LX/Jln;->b(LX/Jln;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2731784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2731785
    :cond_3
    :try_start_2
    const-string v0, "multiple_thread_keys"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2731786
    const-string v0, "multiple_thread_keys"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2731787
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    invoke-static {p0, v0}, LX/Jln;->a(LX/Jln;Ljava/util/Set;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private declared-synchronized b(LX/JlW;)V
    .locals 4

    .prologue
    .line 2731788
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jln;->f:LX/Jll;

    if-eqz v0, :cond_0

    .line 2731789
    iget-object v0, p0, LX/Jln;->f:LX/Jll;

    .line 2731790
    iget-object v1, v0, LX/Jll;->c:LX/0Ve;

    invoke-interface {v1}, LX/0Ve;->dispose()V

    .line 2731791
    const/4 v0, 0x0

    iput-object v0, p0, LX/Jln;->f:LX/Jll;

    .line 2731792
    :cond_0
    iget-object v0, p0, LX/Jln;->b:LX/Jlx;

    invoke-virtual {v0, p1}, LX/Jlx;->a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2731793
    new-instance v1, LX/Jli;

    invoke-direct {v1, p0}, LX/Jli;-><init>(LX/Jln;)V

    .line 2731794
    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2731795
    new-instance v2, LX/Jll;

    invoke-direct {v2, p1, v0, v1}, LX/Jll;-><init>(LX/JlW;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v2, p0, LX/Jln;->f:LX/Jll;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731796
    monitor-exit p0

    return-void

    .line 2731797
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/Jln;)V
    .locals 2

    .prologue
    .line 2731798
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/Jln;->h:Z

    .line 2731799
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/Jln;->b$redex0(LX/Jln;Z)V

    .line 2731800
    iget-object v0, p0, LX/Jln;->c:LX/0Xl;

    sget-object v1, LX/0aY;->G:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731801
    monitor-exit p0

    return-void

    .line 2731802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/Jln;Z)V
    .locals 2

    .prologue
    .line 2731803
    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    if-eqz v0, :cond_1

    .line 2731804
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    iget-object v1, p0, LX/Jln;->e:LX/Jlg;

    invoke-virtual {v0, v1}, LX/Jlv;->a(LX/Jlg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731805
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2731806
    :cond_1
    if-nez p1, :cond_0

    :try_start_1
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    if-eqz v0, :cond_0

    .line 2731807
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Jlv;->a(LX/Jlg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2731808
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/Jlk;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2731809
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Jln;->g:LX/Jlv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 2731810
    const/4 v0, 0x0

    .line 2731811
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, LX/Jlk;

    iget-object v1, p0, LX/Jln;->g:LX/Jlv;

    iget-boolean v2, p0, LX/Jln;->h:Z

    invoke-direct {v0, v1, v2}, LX/Jlk;-><init>(LX/Jlv;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2731812
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JlW;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/inbox2/data/common/InboxUnitResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2731813
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/Jln;->f:LX/Jll;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Jln;->f:LX/Jll;

    iget-object v2, v2, LX/Jll;->a:LX/JlW;

    .line 2731814
    :goto_0
    invoke-static {v2, p1}, LX/Jln;->a(LX/JlW;LX/JlW;)LX/Jlm;

    move-result-object v2

    .line 2731815
    sget-object v3, LX/Jlj;->a:[I

    invoke-virtual {v2}, LX/Jlm;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2731816
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2731817
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2731818
    :pswitch_0
    :try_start_1
    iget-object v2, p0, LX/Jln;->f:LX/Jll;

    if-eqz v2, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2731819
    iget-object v0, p0, LX/Jln;->f:LX/Jll;

    iget-object v0, v0, LX/Jll;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2731820
    :goto_2
    monitor-exit p0

    return-object v0

    :cond_1
    move v0, v1

    .line 2731821
    goto :goto_1

    .line 2731822
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/Jln;->b:LX/Jlx;

    invoke-virtual {v0, p1}, LX/Jlx;->a(LX/JlW;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 2731823
    :pswitch_2
    invoke-direct {p0, p1}, LX/Jln;->b(LX/JlW;)V

    .line 2731824
    iget-object v2, p0, LX/Jln;->f:LX/Jll;

    if-eqz v2, :cond_2

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2731825
    iget-object v0, p0, LX/Jln;->f:LX/Jll;

    iget-object v0, v0, LX/Jll;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 2731826
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
