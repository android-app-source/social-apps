.class public final LX/Jl9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Integer;",
        ">;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;)V
    .locals 0

    .prologue
    .line 2730864
    iput-object p1, p0, LX/Jl9;->a:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2730865
    check-cast p1, Ljava/util/List;

    const/4 v0, 0x0

    .line 2730866
    if-nez p1, :cond_0

    .line 2730867
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2730868
    :goto_0
    return-object v0

    .line 2730869
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2730870
    if-eqz v0, :cond_2

    .line 2730871
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 2730872
    goto :goto_1

    .line 2730873
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method
