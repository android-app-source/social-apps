.class public LX/JkU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final k:Ljava/lang/Object;


# instance fields
.field private final a:LX/0SG;

.field private final b:Ljava/util/Random;

.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

.field public final e:LX/3RQ;

.field private final f:LX/0Sh;

.field private final g:LX/297;

.field public final h:LX/2Og;

.field public final i:LX/3RK;

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2729944
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JkU;->k:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/0SG;Ljava/util/Random;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Landroid/content/Context;LX/3RQ;LX/0Sh;LX/297;LX/2Og;)V
    .locals 1
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2729945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2729946
    iput-object p1, p0, LX/JkU;->a:LX/0SG;

    .line 2729947
    iput-object p2, p0, LX/JkU;->b:Ljava/util/Random;

    .line 2729948
    iput-object p3, p0, LX/JkU;->d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    .line 2729949
    iput-object p4, p0, LX/JkU;->c:Landroid/content/Context;

    .line 2729950
    iput-object p5, p0, LX/JkU;->e:LX/3RQ;

    .line 2729951
    iput-object p6, p0, LX/JkU;->f:LX/0Sh;

    .line 2729952
    iput-object p7, p0, LX/JkU;->g:LX/297;

    .line 2729953
    iput-object p8, p0, LX/JkU;->h:LX/2Og;

    .line 2729954
    iget-object v0, p0, LX/JkU;->c:Landroid/content/Context;

    invoke-static {v0}, LX/3RK;->a(Landroid/content/Context;)LX/3RK;

    move-result-object v0

    iput-object v0, p0, LX/JkU;->i:LX/3RK;

    .line 2729955
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    .line 2729956
    return-void
.end method

.method public static a(LX/0QB;)LX/JkU;
    .locals 7

    .prologue
    .line 2729957
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2729958
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2729959
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2729960
    if-nez v1, :cond_0

    .line 2729961
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2729962
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2729963
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2729964
    sget-object v1, LX/JkU;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2729965
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2729966
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2729967
    :cond_1
    if-nez v1, :cond_4

    .line 2729968
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2729969
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2729970
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/JkU;->b(LX/0QB;)LX/JkU;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2729971
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2729972
    if-nez v1, :cond_2

    .line 2729973
    sget-object v0, LX/JkU;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JkU;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2729974
    :goto_1
    if-eqz v0, :cond_3

    .line 2729975
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2729976
    :goto_3
    check-cast v0, LX/JkU;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2729977
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2729978
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2729979
    :catchall_1
    move-exception v0

    .line 2729980
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2729981
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2729982
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2729983
    :cond_2
    :try_start_8
    sget-object v0, LX/JkU;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JkU;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2729984
    iget-object v0, p0, LX/JkU;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 2729985
    iget-object v1, p0, LX/JkU;->c:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v0, p1, v2}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Z)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2729986
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/3RH;->ad:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "user_id"

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "approve_request"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2729987
    invoke-static {p0, v0}, LX/JkU;->a(LX/JkU;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 3

    .prologue
    .line 2729988
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/JkU;->i:LX/3RK;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x272f

    invoke-virtual {v0, v1, v2}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 2729989
    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2729990
    monitor-exit p0

    return-void

    .line 2729991
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 10

    .prologue
    .line 2729992
    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 2729993
    :cond_0
    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2729994
    :goto_0
    return-void

    .line 2729995
    :cond_1
    const-wide/16 v4, 0x0

    .line 2729996
    const/4 v2, 0x0

    .line 2729997
    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2729998
    iget-object v1, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2729999
    if-eqz v2, :cond_2

    cmp-long v1, v4, v6

    if-lez v1, :cond_4

    :cond_2
    move-wide v2, v6

    :goto_2
    move-wide v4, v2

    move-object v2, v0

    .line 2730000
    goto :goto_1

    .line 2730001
    :cond_3
    invoke-static {p0, v2}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    :cond_4
    move-object v0, v2

    move-wide v2, v4

    goto :goto_2
.end method

.method private static b(LX/0QB;)LX/JkU;
    .locals 9

    .prologue
    .line 2730002
    new-instance v0, LX/JkU;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v2

    check-cast v2, Ljava/util/Random;

    invoke-static {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/3RQ;->a(LX/0QB;)LX/3RQ;

    move-result-object v5

    check-cast v5, LX/3RQ;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v7

    check-cast v7, LX/297;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v8

    check-cast v8, LX/2Og;

    invoke-direct/range {v0 .. v8}, LX/JkU;-><init>(LX/0SG;Ljava/util/Random;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Landroid/content/Context;LX/3RQ;LX/0Sh;LX/297;LX/2Og;)V

    .line 2730003
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 2730004
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/JkU;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730005
    invoke-static {p0, v0}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2730006
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2730007
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/notify/JoinRequestNotification;ILandroid/app/PendingIntent;)V
    .locals 5

    .prologue
    .line 2730008
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/JkU;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2730009
    iget-object v0, p1, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2730010
    iget-object v1, p0, LX/JkU;->g:LX/297;

    invoke-virtual {v1, v0}, LX/297;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 2730011
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2730012
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/JkU;->j:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2730013
    iget-object v1, p1, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, LX/JkU;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, v1, v2, v3}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2730014
    iget-object v1, p0, LX/JkU;->d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    .line 2730015
    new-instance v2, LX/JkS;

    invoke-direct {v2, p0, p1, p3, p2}, LX/JkS;-><init>(LX/JkU;Lcom/facebook/messaging/notify/JoinRequestNotification;Landroid/app/PendingIntent;I)V

    move-object v2, v2

    .line 2730016
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2730017
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2730018
    :cond_2
    :try_start_2
    iget-object v0, p1, Lcom/facebook/messaging/notify/JoinRequestNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, p0, LX/JkU;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, v0, v2, v3}, LX/JkU;->a(LX/JkU;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2730019
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, p1, Lcom/facebook/messaging/notify/JoinRequestNotification;->d:Lcom/facebook/user/model/UserKey;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2730020
    iget-object v1, p0, LX/JkU;->d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;

    move-result-object v0

    .line 2730021
    if-eqz v0, :cond_0

    .line 2730022
    new-instance v1, LX/JkT;

    invoke-direct {v1, p0, p1, p3, p2}, LX/JkT;-><init>(LX/JkU;Lcom/facebook/messaging/notify/JoinRequestNotification;Landroid/app/PendingIntent;I)V

    move-object v1, v1

    .line 2730023
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
