.class public LX/Jag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/DN3;


# direct methods
.method public constructor <init>(LX/DN3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2715657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2715658
    iput-object p1, p0, LX/Jag;->a:LX/DN3;

    .line 2715659
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2715660
    iget-object v0, p0, LX/Jag;->a:LX/DN3;

    invoke-virtual {v0}, LX/DN3;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2715661
    new-instance v1, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;

    invoke-direct {v1}, Lcom/facebook/groups/fb4a/groupshub/fragment/FB4AGroupsHubFragment;-><init>()V

    .line 2715662
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2715663
    :goto_0
    const-string v2, "groups_hub_tab"

    const-string v3, "groups_hub_discover"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2715664
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 2715665
    :goto_1
    return-object v0

    .line 2715666
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 2715667
    :cond_1
    new-instance v0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;-><init>()V

    .line 2715668
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1
.end method
