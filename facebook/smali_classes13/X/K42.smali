.class public final LX/K42;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V
    .locals 0

    .prologue
    .line 2766318
    iput-object p1, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;B)V
    .locals 0

    .prologue
    .line 2766319
    invoke-direct {p0, p1}, LX/K42;-><init>(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2766320
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->K:Lcom/facebook/storyline/ui/StorylineSeekBar;

    invoke-virtual {v0, p1}, Lcom/facebook/storyline/ui/StorylineSeekBar;->setProgress(F)V

    .line 2766321
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2766322
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->K:Lcom/facebook/storyline/ui/StorylineSeekBar;

    invoke-virtual {v0, p1}, Lcom/facebook/storyline/ui/StorylineSeekBar;->setDuration(I)V

    .line 2766323
    return-void
.end method

.method public final a(LX/K3g;)V
    .locals 9
    .param p1    # LX/K3g;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2766324
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2766325
    if-nez p1, :cond_0

    .line 2766326
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    .line 2766327
    iget-object v1, v0, LX/D1k;->a:LX/0if;

    sget-object v2, LX/0ig;->V:LX/0ih;

    const-string v3, "export_video_failed"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2766328
    sget-object v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->p:Ljava/lang/String;

    const-string v1, "Failed to export storyline video"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2766329
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    const v1, 0x7f083c34    # 1.810876E38f

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2766330
    :goto_0
    return-void

    .line 2766331
    :cond_0
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->y:LX/D1k;

    iget v1, p1, LX/K3g;->d:I

    iget v2, p1, LX/K3g;->e:I

    iget v3, p1, LX/K3g;->f:I

    .line 2766332
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string v5, "video_width"

    invoke-virtual {v4, v5, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    const-string v5, "video_height"

    invoke-virtual {v4, v5, v2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    const-string v5, "video_duration"

    invoke-virtual {v4, v5, v3}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    .line 2766333
    iget-object v5, v0, LX/D1k;->a:LX/0if;

    sget-object v6, LX/0ig;->V:LX/0ih;

    const-string v7, "export_video_succeeded"

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2766334
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    .line 2766335
    invoke-static {v0, p1}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->a$redex0(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;LX/K3g;)V

    .line 2766336
    goto :goto_0
.end method

.method public final a(LX/K4l;LX/K4l;)V
    .locals 6

    .prologue
    const/16 v3, 0x80

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2766337
    sget-object v0, LX/K4l;->PLAYING:LX/K4l;

    if-ne p1, v0, :cond_2

    .line 2766338
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    .line 2766339
    :goto_0
    sget-object v0, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-eq p1, v0, :cond_3

    sget-object v0, LX/K4l;->PLAYING:LX/K4l;

    if-eq p1, v0, :cond_3

    move v0, v1

    .line 2766340
    :goto_1
    iget-object v3, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v5, v3, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->J:Lcom/facebook/drawee/view/DraweeView;

    if-eqz v0, :cond_4

    move v3, v2

    :goto_2
    invoke-virtual {v5, v3}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 2766341
    iget-object v3, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v3, v3, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->L:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    move v4, v2

    :cond_0
    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2766342
    sget-object v0, LX/K4l;->PLAYING:LX/K4l;

    if-ne p1, v0, :cond_5

    move v0, v1

    .line 2766343
    :goto_3
    iget-object v3, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v3, v3, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    invoke-virtual {v3, v0}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setIsPlaying(Z)V

    .line 2766344
    if-eqz v0, :cond_6

    .line 2766345
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setButtonVisibility(Z)V

    .line 2766346
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->b(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;Lcom/facebook/storyline/model/StorylinePhoto;)V

    .line 2766347
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->O:Lcom/facebook/storyline/ui/StorylineAudioIndicator;

    invoke-virtual {v0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->a()V

    .line 2766348
    :goto_4
    sget-object v0, LX/K4l;->INITIALIZED:LX/K4l;

    if-ne p2, v0, :cond_1

    sget-object v0, LX/K4l;->READY_TO_PLAY:LX/K4l;

    if-ne p1, v0, :cond_1

    .line 2766349
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-boolean v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->ac:Z

    if-eqz v0, :cond_7

    .line 2766350
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    invoke-virtual {v0}, LX/K4o;->a()V

    .line 2766351
    :cond_1
    :goto_5
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-static {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s(Lcom/facebook/storyline/fb4a/activity/StorylineActivity;)V

    .line 2766352
    return-void

    .line 2766353
    :cond_2
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    invoke-virtual {v0}, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2766354
    goto :goto_1

    :cond_4
    move v3, v4

    .line 2766355
    goto :goto_2

    :cond_5
    move v0, v2

    .line 2766356
    goto :goto_3

    .line 2766357
    :cond_6
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->O:Lcom/facebook/storyline/ui/StorylineAudioIndicator;

    invoke-virtual {v0}, Lcom/facebook/storyline/ui/StorylineAudioIndicator;->b()V

    goto :goto_4

    .line 2766358
    :cond_7
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->s:LX/K4o;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/K4o;->a(F)V

    .line 2766359
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->N:Lcom/facebook/storyline/ui/StorylinePlayButtonView;

    invoke-virtual {v0, v1}, Lcom/facebook/storyline/ui/StorylinePlayButtonView;->setButtonVisibility(Z)V

    goto :goto_5
.end method

.method public final b(F)V
    .locals 2

    .prologue
    .line 2766360
    iget-object v0, p0, LX/K42;->a:Lcom/facebook/storyline/fb4a/activity/StorylineActivity;

    iget-object v0, v0, Lcom/facebook/storyline/fb4a/activity/StorylineActivity;->P:LX/4BY;

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, LX/4BY;->b(I)V

    .line 2766361
    return-void
.end method
