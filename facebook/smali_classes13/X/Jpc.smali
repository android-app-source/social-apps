.class public LX/Jpc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0gc;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/JqE;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0gc;Landroid/content/res/Resources;LX/JqE;)V
    .locals 0
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2737811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2737812
    iput-object p1, p0, LX/Jpc;->a:Landroid/content/Context;

    .line 2737813
    iput-object p2, p0, LX/Jpc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2737814
    iput-object p3, p0, LX/Jpc;->c:LX/0gc;

    .line 2737815
    iput-object p4, p0, LX/Jpc;->d:Landroid/content/res/Resources;

    .line 2737816
    iput-object p5, p0, LX/Jpc;->e:LX/JqE;

    .line 2737817
    return-void
.end method

.method public static a(LX/Jpc;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 2

    .prologue
    .line 2737818
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/Jpc;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f082ea8

    invoke-virtual {v0, v1, p2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2737819
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2737820
    iget-object v0, p0, LX/Jpc;->c:LX/0gc;

    const-string v1, "sms_contact_picker_progress_dialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2737821
    if-nez v0, :cond_0

    .line 2737822
    iget-object v0, p0, LX/Jpc;->d:Landroid/content/res/Resources;

    const v1, 0x7f082ea3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2737823
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2737824
    iget-object v1, p0, LX/Jpc;->c:LX/0gc;

    const-string v2, "sms_contact_picker_progress_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2737825
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2737826
    iget-object v0, p0, LX/Jpc;->c:LX/0gc;

    const-string v1, "sms_contact_picker_progress_dialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2737827
    if-eqz v0, :cond_0

    .line 2737828
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2737829
    :cond_0
    return-void
.end method
