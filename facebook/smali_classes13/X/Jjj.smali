.class public LX/Jjj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:LX/Ifg;

.field private final b:LX/JjT;

.field private final c:LX/Jk1;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Ou;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DdT;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dde;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ifg;LX/JjT;LX/Jk1;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ifg;",
            "LX/JjT;",
            "LX/Jk1;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Ou;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DdT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2728250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2728251
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728252
    iput-object v0, p0, LX/Jjj;->g:LX/0Ot;

    .line 2728253
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728254
    iput-object v0, p0, LX/Jjj;->h:LX/0Ot;

    .line 2728255
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728256
    iput-object v0, p0, LX/Jjj;->i:LX/0Ot;

    .line 2728257
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728258
    iput-object v0, p0, LX/Jjj;->j:LX/0Ot;

    .line 2728259
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728260
    iput-object v0, p0, LX/Jjj;->k:LX/0Ot;

    .line 2728261
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728262
    iput-object v0, p0, LX/Jjj;->m:LX/0Ot;

    .line 2728263
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2728264
    iput-object v0, p0, LX/Jjj;->n:LX/0Ot;

    .line 2728265
    iput-object p1, p0, LX/Jjj;->a:LX/Ifg;

    .line 2728266
    iput-object p2, p0, LX/Jjj;->b:LX/JjT;

    .line 2728267
    iput-object p3, p0, LX/Jjj;->c:LX/Jk1;

    .line 2728268
    iput-object p4, p0, LX/Jjj;->d:LX/0Or;

    .line 2728269
    iput-object p5, p0, LX/Jjj;->e:LX/0Or;

    .line 2728270
    iput-object p6, p0, LX/Jjj;->f:LX/0Or;

    .line 2728271
    return-void
.end method

.method public static a(LX/0QB;)LX/Jjj;
    .locals 12

    .prologue
    .line 2728237
    const-class v1, LX/Jjj;

    monitor-enter v1

    .line 2728238
    :try_start_0
    sget-object v0, LX/Jjj;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2728239
    sput-object v2, LX/Jjj;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2728240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2728241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2728242
    new-instance v3, LX/Jjj;

    invoke-static {v0}, LX/Ifg;->b(LX/0QB;)LX/Ifg;

    move-result-object v4

    check-cast v4, LX/Ifg;

    invoke-static {v0}, LX/JjT;->b(LX/0QB;)LX/JjT;

    move-result-object v5

    check-cast v5, LX/JjT;

    invoke-static {v0}, LX/Jk1;->b(LX/0QB;)LX/Jk1;

    move-result-object v6

    check-cast v6, LX/Jk1;

    const/16 v7, 0xce8

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xcee

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x26ef

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/Jjj;-><init>(LX/Ifg;LX/JjT;LX/Jk1;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2728243
    const/16 v4, 0xd2b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2778

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2779

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x12af

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xbca

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v9

    check-cast v9, LX/3kp;

    const/16 v10, 0x26ff

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1032

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 2728244
    iput-object v4, v3, LX/Jjj;->g:LX/0Ot;

    iput-object v5, v3, LX/Jjj;->h:LX/0Ot;

    iput-object v6, v3, LX/Jjj;->i:LX/0Ot;

    iput-object v7, v3, LX/Jjj;->j:LX/0Ot;

    iput-object v8, v3, LX/Jjj;->k:LX/0Ot;

    iput-object v9, v3, LX/Jjj;->l:LX/3kp;

    iput-object v10, v3, LX/Jjj;->m:LX/0Ot;

    iput-object v11, v3, LX/Jjj;->n:LX/0Ot;

    .line 2728245
    move-object v0, v3

    .line 2728246
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2728247
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Jjj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2728248
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2728249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;Lcom/facebook/messaging/events/banner/EventReminderMembers;)Z
    .locals 1
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/messaging/model/threads/ThreadEventReminder;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/events/banner/EventReminderMembers;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2728234
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2728235
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    move v0, v0

    .line 2728236
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 2728229
    iget-object v0, p0, LX/Jjj;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CX;

    invoke-static {p1}, LX/4mm;->a(Landroid/content/Context;)LX/4mn;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/4mn;->a(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    new-instance v2, LX/Jji;

    invoke-direct {v2, p0}, LX/Jji;-><init>(LX/Jjj;)V

    .line 2728230
    iput-object v2, v1, LX/4mn;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 2728231
    move-object v1, v1

    .line 2728232
    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2728233
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)Z
    .locals 4

    .prologue
    .line 2728228
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c()J

    move-result-wide v2

    iget-object v0, p0, LX/Jjj;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
