.class public LX/JcM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/11H;

.field public final b:LX/JcT;


# direct methods
.method public constructor <init>(LX/11H;LX/JcT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2717759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2717760
    iput-object p1, p0, LX/JcM;->a:LX/11H;

    .line 2717761
    iput-object p2, p0, LX/JcM;->b:LX/JcT;

    .line 2717762
    return-void
.end method

.method public static a(LX/0QB;)LX/JcM;
    .locals 13

    .prologue
    .line 2717763
    const-class v1, LX/JcM;

    monitor-enter v1

    .line 2717764
    :try_start_0
    sget-object v0, LX/JcM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2717765
    sput-object v2, LX/JcM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2717766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2717767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2717768
    new-instance v5, LX/JcM;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    .line 2717769
    new-instance v6, LX/JcT;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v7

    check-cast v7, LX/0dC;

    const/16 v8, 0x330

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x331

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/1Mk;->a(LX/0QB;)LX/1Mk;

    move-result-object v10

    check-cast v10, LX/1Mk;

    invoke-static {v0}, LX/2s9;->a(LX/0QB;)LX/2s9;

    move-result-object v11

    check-cast v11, LX/2s9;

    invoke-static {v0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v12

    check-cast v12, LX/0yD;

    invoke-direct/range {v6 .. v12}, LX/JcT;-><init>(LX/0dC;LX/0Or;LX/0Or;LX/1Mk;LX/2s9;LX/0yD;)V

    .line 2717770
    move-object v4, v6

    .line 2717771
    check-cast v4, LX/JcT;

    invoke-direct {v5, v3, v4}, LX/JcM;-><init>(LX/11H;LX/JcT;)V

    .line 2717772
    move-object v0, v5

    .line 2717773
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2717774
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JcM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717775
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2717776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2717777
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2717778
    const-string v1, "update_foreground_location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2717779
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2717780
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 2717781
    iget-object v1, p0, LX/JcM;->a:LX/11H;

    iget-object v2, p0, LX/JcM;->b:LX/JcT;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717782
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2717783
    move-object v0, v0

    .line 2717784
    return-object v0

    .line 2717785
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected operation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
