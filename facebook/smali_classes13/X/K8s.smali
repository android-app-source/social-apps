.class public LX/K8s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/K8s;


# instance fields
.field public final a:LX/13u;

.field public final b:LX/0Zb;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/K79;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/13u;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2776279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2776280
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/K8s;->d:Ljava/util/Stack;

    .line 2776281
    iput-object p2, p0, LX/K8s;->a:LX/13u;

    .line 2776282
    iput-object p1, p0, LX/K8s;->b:LX/0Zb;

    .line 2776283
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/K8s;->c:Ljava/util/Map;

    .line 2776284
    return-void
.end method

.method public static a(LX/0QB;)LX/K8s;
    .locals 5

    .prologue
    .line 2776285
    sget-object v0, LX/K8s;->e:LX/K8s;

    if-nez v0, :cond_1

    .line 2776286
    const-class v1, LX/K8s;

    monitor-enter v1

    .line 2776287
    :try_start_0
    sget-object v0, LX/K8s;->e:LX/K8s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2776288
    if-eqz v2, :cond_0

    .line 2776289
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2776290
    new-instance p0, LX/K8s;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/13u;->a(LX/0QB;)LX/13u;

    move-result-object v4

    check-cast v4, LX/13u;

    invoke-direct {p0, v3, v4}, LX/K8s;-><init>(LX/0Zb;LX/13u;)V

    .line 2776291
    move-object v0, p0

    .line 2776292
    sput-object v0, LX/K8s;->e:LX/K8s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2776293
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2776294
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2776295
    :cond_1
    sget-object v0, LX/K8s;->e:LX/K8s;

    return-object v0

    .line 2776296
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2776297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2776298
    if-nez p3, :cond_0

    .line 2776299
    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 2776300
    :cond_0
    iget-object v0, p0, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K79;

    .line 2776301
    if-eqz v0, :cond_1

    .line 2776302
    invoke-virtual {v0, p3}, LX/K79;->a(Ljava/util/Map;)V

    .line 2776303
    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tarot_digest"

    .line 2776304
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2776305
    move-object v0, v0

    .line 2776306
    invoke-virtual {v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2776307
    iget-object v1, p0, LX/K8s;->a:LX/13u;

    invoke-virtual {v1, v0}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2776308
    iget-object v1, p0, LX/K8s;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2776309
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2776310
    iget-object v0, p0, LX/K8s;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K79;

    .line 2776311
    if-eqz v0, :cond_0

    .line 2776312
    const-string v1, "digest_id"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "page_id"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2776313
    iput-object v1, v0, LX/K79;->h:Ljava/lang/String;

    .line 2776314
    iput-object v2, v0, LX/K79;->i:Ljava/lang/String;

    .line 2776315
    :cond_0
    const-string v0, "tarot_open_card"

    invoke-static {p0, v0, p1, p2}, LX/K8s;->a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2776316
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;LX/K8u;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "LX/K8u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2776317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2776318
    if-eqz p2, :cond_0

    .line 2776319
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2776320
    :cond_0
    const-string v1, "tarot_origin"

    iget-object v2, p3, LX/K8u;->mName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776321
    const-string v1, "tarot_share_digest"

    invoke-static {p0, v1, p1, v0}, LX/K8s;->a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2776322
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2776323
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2776324
    if-eqz p2, :cond_0

    .line 2776325
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2776326
    :cond_0
    const-string v1, "tarot_share_tarot_card"

    invoke-static {p0, v1, p1, v0}, LX/K8s;->a(LX/K8s;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2776327
    return-void
.end method
