.class public final LX/JXy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/JY2;


# direct methods
.method public constructor <init>(LX/JY2;Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 2705588
    iput-object p1, p0, LX/JXy;->c:LX/JY2;

    iput-object p2, p0, LX/JXy;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iput-object p3, p0, LX/JXy;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2705589
    iget-object v0, p0, LX/JXy;->c:LX/JY2;

    iget-object v1, p0, LX/JXy;->a:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JXy;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 2705590
    iget-object v4, v0, LX/JY2;->b:LX/0tX;

    .line 2705591
    new-instance v3, LX/81F;

    invoke-direct {v3}, LX/81F;-><init>()V

    move-object v3, v3

    .line 2705592
    const-string v5, "node_id"

    invoke-virtual {v3, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "sgny_cover_photo_width_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    mul-int/lit8 p0, p0, 0x3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "sgny_facepile_size_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "sgny_size_param"

    iget-object p0, v0, LX/JY2;->g:LX/0sa;

    invoke-virtual {p0}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "after_param"

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "sgny_facepile_count_param"

    const/4 p0, 0x3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/81F;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2705593
    new-instance v4, LX/JY0;

    invoke-direct {v4, v0}, LX/JY0;-><init>(LX/JY2;)V

    iget-object v5, v0, LX/JY2;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2705594
    return-object v0
.end method
