.class public LX/K4a;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "ReactNarrativeEngineNativeModule"
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/K4Z;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2768780
    const-class v0, LX/K4a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/K4a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/5pY;LX/K4Z;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2768781
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2768782
    iput-object p2, p0, LX/K4a;->b:LX/K4Z;

    .line 2768783
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2768765
    const-string v0, "StorylineNarrativeEngine"

    return-object v0
.end method

.method public storylineNarrativeEngineReceiveAnimationDescriptor(LX/5pG;)V
    .locals 10
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2768766
    iget-object v0, p0, LX/K4a;->b:LX/K4Z;

    .line 2768767
    iget-object v1, v0, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K4V;

    .line 2768768
    const-string v3, "animationDescriptorID"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2768769
    iget-object v3, v1, LX/K4V;->a:LX/K4W;

    iget-object v3, v3, LX/K4W;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2768770
    :goto_1
    goto :goto_0

    .line 2768771
    :cond_0
    return-void

    .line 2768772
    :cond_1
    const-string v3, "isFirstInBatch"

    invoke-interface {p1, v3}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 2768773
    const-string v3, "isLastInBatch"

    invoke-interface {p1, v3}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 2768774
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2768775
    const-string v3, "animationDescriptor"

    invoke-interface {p1, v3}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v4

    .line 2768776
    const/4 v3, 0x0

    :goto_2
    invoke-interface {v4}, LX/5pC;->size()I

    move-result v9

    if-ge v3, v9, :cond_2

    .line 2768777
    invoke-interface {v4, v3}, LX/5pC;->a(I)LX/5pG;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2768778
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2768779
    :cond_2
    iget-object v3, v1, LX/K4V;->a:LX/K4W;

    iget-object v9, v3, LX/K4W;->d:LX/0Sh;

    new-instance v3, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$4;-><init>(LX/K4V;Ljava/lang/String;ZZLjava/util/List;)V

    invoke-virtual {v9, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public storylineNarrativeEngineReceiveAnimationDuration(F)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2768740
    iget-object v0, p0, LX/K4a;->b:LX/K4Z;

    .line 2768741
    iget-object v1, v0, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K4V;

    .line 2768742
    iget-object p0, v1, LX/K4V;->a:LX/K4W;

    iget-object p0, p0, LX/K4W;->d:LX/0Sh;

    new-instance v0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$3;

    invoke-direct {v0, v1, p1}, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$3;-><init>(LX/K4V;F)V

    invoke-virtual {p0, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2768743
    goto :goto_0

    .line 2768744
    :cond_0
    return-void
.end method

.method public storylineNarrativeEngineReceiveAssetMetadata(LX/5pC;)V
    .locals 9
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2768745
    iget-object v0, p0, LX/K4a;->b:LX/K4Z;

    .line 2768746
    iget-object v1, v0, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K4V;

    .line 2768747
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2768748
    const/4 v3, 0x0

    :goto_1
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 2768749
    invoke-interface {p1, v3}, LX/5pC;->a(I)LX/5pG;

    move-result-object v5

    .line 2768750
    const-string v6, "assetType"

    invoke-interface {v5, v6}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "photo"

    const-string v7, "assetType"

    invoke-interface {v5, v7}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2768751
    :cond_0
    const/4 v6, 0x0

    .line 2768752
    :goto_2
    move-object v6, v6

    .line 2768753
    if-eqz v6, :cond_1

    .line 2768754
    const-string v7, "startTimeInAnimation"

    invoke-interface {v5, v7}, LX/5pG;->getDouble(Ljava/lang/String;)D

    .line 2768755
    const-string v7, "startTimeInAnimation"

    invoke-interface {v5, v7}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2768756
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2768757
    :cond_2
    iget-object v3, v1, LX/K4V;->a:LX/K4W;

    iget-object v3, v3, LX/K4W;->d:LX/0Sh;

    new-instance v5, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$1;

    invoke-direct {v5, v1, v4}, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$1;-><init>(LX/K4V;Ljava/util/Map;)V

    invoke-virtual {v3, v5}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2768758
    goto :goto_0

    .line 2768759
    :cond_3
    return-void

    :cond_4
    const-string v6, "id"

    invoke-interface {v5, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public storylineNarrativeEngineReceiveShaderMap(LX/5pG;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2768760
    iget-object v0, p0, LX/K4a;->b:LX/K4Z;

    .line 2768761
    iget-object v1, v0, LX/K4Z;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/K4V;

    .line 2768762
    iget-object p0, v1, LX/K4V;->a:LX/K4W;

    iget-object p0, p0, LX/K4W;->d:LX/0Sh;

    new-instance v0, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;

    invoke-direct {v0, v1, p1}, Lcom/facebook/storyline/narrativeengine/ReactNarrativeEngine$ReactNarrativeEngineCallbacksImpl$2;-><init>(LX/K4V;LX/5pG;)V

    invoke-virtual {p0, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2768763
    goto :goto_0

    .line 2768764
    :cond_0
    return-void
.end method
