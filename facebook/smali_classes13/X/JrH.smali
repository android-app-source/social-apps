.class public LX/JrH;
.super LX/Jqj;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Jqj",
        "<",
        "LX/6kW;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDs;

.field private final b:LX/Jrc;

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2742723
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JrH;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDs;LX/Jrc;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDs;",
            "LX/Jrc;",
            "LX/0Ot",
            "<",
            "LX/7G0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742717
    invoke-direct {p0, p3}, LX/Jqj;-><init>(LX/0Ot;)V

    .line 2742718
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2742719
    iput-object v0, p0, LX/JrH;->c:LX/0Ot;

    .line 2742720
    iput-object p1, p0, LX/JrH;->a:LX/FDs;

    .line 2742721
    iput-object p2, p0, LX/JrH;->b:LX/Jrc;

    .line 2742722
    return-void
.end method

.method private a(LX/6kW;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kW;",
            ")",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2742714
    invoke-virtual {p1}, LX/6kW;->n()LX/6kM;

    move-result-object v0

    .line 2742715
    iget-object v1, p0, LX/JrH;->b:LX/Jrc;

    iget-object v0, v0, LX/6kM;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742716
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JrH;
    .locals 9

    .prologue
    .line 2742683
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2742684
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2742685
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2742686
    if-nez v1, :cond_0

    .line 2742687
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2742688
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2742689
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2742690
    sget-object v1, LX/JrH;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2742691
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2742692
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2742693
    :cond_1
    if-nez v1, :cond_4

    .line 2742694
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2742695
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2742696
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2742697
    new-instance v8, LX/JrH;

    invoke-static {v0}, LX/FDs;->a(LX/0QB;)LX/FDs;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-static {v0}, LX/Jrc;->a(LX/0QB;)LX/Jrc;

    move-result-object v7

    check-cast v7, LX/Jrc;

    const/16 p0, 0x35bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v8, v1, v7, p0}, LX/JrH;-><init>(LX/FDs;LX/Jrc;LX/0Ot;)V

    .line 2742698
    const/16 v1, 0xce5

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2742699
    iput-object v1, v8, LX/JrH;->c:LX/0Ot;

    .line 2742700
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2742701
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2742702
    if-nez v1, :cond_2

    .line 2742703
    sget-object v0, LX/JrH;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrH;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2742704
    :goto_1
    if-eqz v0, :cond_3

    .line 2742705
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742706
    :goto_3
    check-cast v0, LX/JrH;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2742707
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2742708
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2742709
    :catchall_1
    move-exception v0

    .line 2742710
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2742711
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2742712
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2742713
    :cond_2
    :try_start_8
    sget-object v0, LX/JrH;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JrH;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Rf;
    .locals 2

    .prologue
    .line 2742724
    check-cast p1, LX/6kW;

    .line 2742725
    invoke-virtual {p1}, LX/6kW;->n()LX/6kM;

    move-result-object v0

    .line 2742726
    iget-object v0, v0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, LX/JrH;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2742727
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2742728
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/7GJ;)Landroid/os/Bundle;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2742678
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->n()LX/6kM;

    move-result-object v0

    .line 2742679
    iget-object v1, v0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2742680
    iget-object v1, p0, LX/JrH;->b:LX/Jrc;

    iget-object v0, v0, LX/6kM;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2742681
    iget-object v1, p0, LX/JrH;->a:LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742682
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/6kW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2742673
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/6kW;

    invoke-virtual {v0}, LX/6kW;->n()LX/6kM;

    move-result-object v0

    .line 2742674
    iget-object v1, v0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2742675
    iget-object v1, p0, LX/JrH;->b:LX/Jrc;

    iget-object v0, v0, LX/6kM;->threadKey:LX/6l9;

    invoke-virtual {v1, v0}, LX/Jrc;->a(LX/6l9;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2742676
    iget-object v0, p0, LX/JrH;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v0, v2, v1}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2742677
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 2742672
    check-cast p1, LX/6kW;

    invoke-direct {p0, p1}, LX/JrH;->a(LX/6kW;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
