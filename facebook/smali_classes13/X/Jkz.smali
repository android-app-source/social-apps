.class public LX/Jkz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/3Kl;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/presence/PresenceManager;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field public j:LX/3Lb;

.field private k:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "Ljava/lang/Void;",
            "LX/3MZ;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0dN;

.field private m:LX/3Mg;

.field public n:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private o:Z

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public r:I

.field public s:Z

.field public t:J

.field public u:Z

.field public v:Z

.field public w:Z

.field public final x:LX/0qK;

.field public y:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2730757
    const-class v0, LX/Jkz;

    sput-object v0, LX/Jkz;->a:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/3Kl;LX/0SG;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2730742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2730743
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2730744
    iput-object v0, p0, LX/Jkz;->d:LX/0Ot;

    .line 2730745
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2730746
    iput-object v0, p0, LX/Jkz;->f:LX/0Ot;

    .line 2730747
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2730748
    iput-object v0, p0, LX/Jkz;->g:LX/0Ot;

    .line 2730749
    new-instance v0, LX/Jkw;

    invoke-direct {v0, p0}, LX/Jkw;-><init>(LX/Jkz;)V

    iput-object v0, p0, LX/Jkz;->l:LX/0dN;

    .line 2730750
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Jkz;->t:J

    .line 2730751
    iput-object p1, p0, LX/Jkz;->b:LX/3Kl;

    .line 2730752
    new-instance v0, LX/Jkx;

    invoke-direct {v0, p0}, LX/Jkx;-><init>(LX/Jkz;)V

    iput-object v0, p0, LX/Jkz;->m:LX/3Mg;

    .line 2730753
    iget-object v0, p0, LX/Jkz;->b:LX/3Kl;

    invoke-virtual {v0}, LX/3Kl;->b()LX/3Lb;

    move-result-object v0

    iput-object v0, p0, LX/Jkz;->j:LX/3Lb;

    .line 2730754
    new-instance v0, LX/Jky;

    invoke-direct {v0, p0}, LX/Jky;-><init>(LX/Jkz;)V

    iput-object v0, p0, LX/Jkz;->k:LX/3Mb;

    .line 2730755
    new-instance v0, LX/0qK;

    const/4 v1, 0x2

    const-wide/32 v2, 0xea60

    invoke-direct {v0, p2, v1, v2, v3}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v0, p0, LX/Jkz;->x:LX/0qK;

    .line 2730756
    return-void
.end method

.method public static a(LX/Jkz;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2730732
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    invoke-virtual {v0}, LX/2CH;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2730733
    const-string v0, "mobile availability disabled"

    invoke-direct {p0, v1, v0}, LX/Jkz;->a(ZLjava/lang/String;)V

    .line 2730734
    :cond_0
    :goto_0
    return-void

    .line 2730735
    :cond_1
    iget-boolean v0, p0, LX/Jkz;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/Jkz;->i:Z

    if-nez v0, :cond_3

    .line 2730736
    :cond_2
    const-string v0, "not visible"

    invoke-direct {p0, v1, v0}, LX/Jkz;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 2730737
    :cond_3
    const/4 v0, 0x1

    const-string v1, "viewport"

    invoke-direct {p0, v0, v1}, LX/Jkz;->a(ZLjava/lang/String;)V

    .line 2730738
    iget-boolean v0, p0, LX/Jkz;->o:Z

    if-eqz v0, :cond_0

    .line 2730739
    iget-object v2, p0, LX/Jkz;->n:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_4

    .line 2730740
    :goto_1
    goto :goto_0

    .line 2730741
    :cond_4
    iget-object v2, p0, LX/Jkz;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$4;

    invoke-direct {v3, p0}, Lcom/facebook/messaging/inbox2/activenow/InboxActiveNowController$4;-><init>(LX/Jkz;)V

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, p0, LX/Jkz;->n:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1
.end method

.method private a(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2730708
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2730709
    iget-boolean v0, p0, LX/Jkz;->o:Z

    if-ne v0, p1, :cond_0

    .line 2730710
    :goto_0
    return-void

    .line 2730711
    :cond_0
    iput-boolean p1, p0, LX/Jkz;->o:Z

    .line 2730712
    iget-boolean v0, p0, LX/Jkz;->o:Z

    if-eqz v0, :cond_1

    .line 2730713
    iget-object v0, p0, LX/Jkz;->j:LX/3Lb;

    iget-object v1, p0, LX/Jkz;->k:LX/3Mb;

    .line 2730714
    iput-object v1, v0, LX/3Lb;->B:LX/3Mb;

    .line 2730715
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    iget-object v1, p0, LX/Jkz;->m:LX/3Mg;

    invoke-virtual {v0, v1}, LX/2CH;->a(LX/3Mg;)V

    .line 2730716
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    invoke-virtual {v0, p0}, LX/2CH;->a(Ljava/lang/Object;)V

    .line 2730717
    invoke-static {p0}, LX/Jkz;->e(LX/Jkz;)V

    goto :goto_0

    .line 2730718
    :cond_1
    iget-object v0, p0, LX/Jkz;->j:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->b()V

    .line 2730719
    iput-boolean v2, p0, LX/Jkz;->s:Z

    .line 2730720
    iget-object v0, p0, LX/Jkz;->j:LX/3Lb;

    .line 2730721
    iput-object v3, v0, LX/3Lb;->B:LX/3Mb;

    .line 2730722
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    iget-object v1, p0, LX/Jkz;->m:LX/3Mg;

    invoke-virtual {v0, v1}, LX/2CH;->b(LX/3Mg;)V

    .line 2730723
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    invoke-virtual {v0, p0}, LX/2CH;->b(Ljava/lang/Object;)V

    .line 2730724
    iget-object v0, p0, LX/Jkz;->n:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2730725
    iput-object v3, p0, LX/Jkz;->n:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static e(LX/Jkz;)V
    .locals 2

    .prologue
    .line 2730726
    iget-boolean v0, p0, LX/Jkz;->s:Z

    if-eqz v0, :cond_1

    .line 2730727
    :cond_0
    :goto_0
    return-void

    .line 2730728
    :cond_1
    iget-object v0, p0, LX/Jkz;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    .line 2730729
    iget-object v1, v0, LX/2CH;->K:LX/2AL;

    move-object v0, v1

    .line 2730730
    sget-object v1, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    if-ne v0, v1, :cond_0

    .line 2730731
    iget-object v0, p0, LX/Jkz;->j:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V

    goto :goto_0
.end method
