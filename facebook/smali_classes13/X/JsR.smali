.class public LX/JsR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2RJ;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/2RJ;Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2745502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745503
    iput-object p1, p0, LX/JsR;->a:LX/2RJ;

    .line 2745504
    iput-object p2, p0, LX/JsR;->b:Landroid/content/Context;

    .line 2745505
    return-void
.end method

.method public static a(LX/JsR;Ljava/lang/String;LX/4ob;ZZI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;ZZI)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2745506
    if-nez p4, :cond_1

    iget-object v0, p0, LX/JsR;->a:LX/2RJ;

    invoke-virtual {v0}, LX/2RJ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2745507
    :goto_0
    if-eqz v0, :cond_2

    .line 2745508
    invoke-virtual {p2}, LX/4ob;->d()V

    .line 2745509
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2745510
    goto :goto_0

    .line 2745511
    :cond_2
    invoke-virtual {p2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2745512
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2745513
    if-nez p4, :cond_3

    .line 2745514
    iget-object v3, p0, LX/JsR;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f60

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2745515
    :cond_3
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2745516
    if-eqz p3, :cond_4

    .line 2745517
    iget-object v3, p0, LX/JsR;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f61

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2745518
    iget-object v3, p0, LX/JsR;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f5f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2745519
    iget-object v3, p0, LX/JsR;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080f61

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2745520
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2745521
    iget-object v2, p0, LX/JsR;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, p5}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2745522
    const v2, 0x7f0e0539

    if-ne p5, v2, :cond_5

    .line 2745523
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setIncludeFontPadding(Z)V

    .line 2745524
    :cond_5
    invoke-virtual {p2}, LX/4ob;->e()V

    goto :goto_1
.end method
