.class public LX/KAJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/KAO;

.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:Z

.field public final d:LX/0gh;


# direct methods
.method public constructor <init>(LX/KAO;Lcom/facebook/intent/feed/IFeedIntentBuilder;Ljava/lang/Boolean;LX/0gh;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groupstab/annotations/ShouldShowWorkDiscoveryGroupTab;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2779228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2779229
    iput-object p1, p0, LX/KAJ;->a:LX/KAO;

    .line 2779230
    iput-object p2, p0, LX/KAJ;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2779231
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/KAJ;->c:Z

    .line 2779232
    iput-object p4, p0, LX/KAJ;->d:LX/0gh;

    .line 2779233
    return-void
.end method

.method public static a(LX/0QB;)LX/KAJ;
    .locals 7

    .prologue
    .line 2779234
    const-class v1, LX/KAJ;

    monitor-enter v1

    .line 2779235
    :try_start_0
    sget-object v0, LX/KAJ;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2779236
    sput-object v2, LX/KAJ;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2779237
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779238
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2779239
    new-instance p0, LX/KAJ;

    invoke-static {v0}, LX/KAO;->b(LX/0QB;)LX/KAO;

    move-result-object v3

    check-cast v3, LX/KAO;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/Hf6;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v6

    check-cast v6, LX/0gh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/KAJ;-><init>(LX/KAO;Lcom/facebook/intent/feed/IFeedIntentBuilder;Ljava/lang/Boolean;LX/0gh;)V

    .line 2779240
    move-object v0, p0

    .line 2779241
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2779242
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/KAJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2779243
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2779244
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
