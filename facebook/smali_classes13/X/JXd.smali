.class public final LX/JXd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Oj;


# instance fields
.field public a:Landroid/view/View;

.field public b:LX/JXf;

.field public c:Ljava/util/concurrent/ScheduledFuture;

.field private final d:Landroid/graphics/Rect;

.field public final e:Ljava/lang/Runnable;

.field public final f:Ljava/lang/Runnable;

.field public final g:Ljava/util/concurrent/ScheduledExecutorService;

.field public final h:I

.field public final i:I

.field private final j:F

.field public final k:Ljava/lang/String;

.field public final l:LX/0Zb;

.field public m:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;IIFLjava/lang/String;LX/0Zb;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "IIF",
            "Ljava/lang/String;",
            "LX/0Zb;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2705025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2705026
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/JXd;->d:Landroid/graphics/Rect;

    .line 2705027
    iput-object p1, p0, LX/JXd;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2705028
    iput p2, p0, LX/JXd;->h:I

    .line 2705029
    iput p3, p0, LX/JXd;->i:I

    .line 2705030
    iput p4, p0, LX/JXd;->j:F

    .line 2705031
    iput-object p5, p0, LX/JXd;->k:Ljava/lang/String;

    .line 2705032
    iput-object p6, p0, LX/JXd;->l:LX/0Zb;

    .line 2705033
    iput-object p7, p0, LX/JXd;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2705034
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$1;-><init>(LX/JXd;)V

    iput-object v0, p0, LX/JXd;->e:Ljava/lang/Runnable;

    .line 2705035
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition$ScrollListener$2;-><init>(LX/JXd;)V

    iput-object v0, p0, LX/JXd;->f:Ljava/lang/Runnable;

    .line 2705036
    return-void
.end method

.method public static b(LX/JXd;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2705037
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2705038
    :cond_0
    :goto_0
    return v0

    .line 2705039
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2705040
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2705041
    iget-object v1, p0, LX/JXd;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    .line 2705042
    if-eqz v1, :cond_0

    .line 2705043
    iget-object v1, p0, LX/JXd;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, LX/JXd;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v1, v2

    .line 2705044
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    .line 2705045
    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 2705046
    iget v2, p0, LX/JXd;->j:F

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 2705047
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2705048
    iget-object v0, p0, LX/JXd;->a:Landroid/view/View;

    invoke-static {p0, v0}, LX/JXd;->b(LX/JXd;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/JXd;->b:LX/JXf;

    .line 2705049
    iget-boolean v1, v0, LX/JXf;->c:Z

    move v0, v1

    .line 2705050
    if-eqz v0, :cond_1

    .line 2705051
    iget-object v1, p0, LX/JXd;->g:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/JXd;->e:Ljava/lang/Runnable;

    iget v3, p0, LX/JXd;->i:I

    int-to-long v3, v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    .line 2705052
    :cond_0
    :goto_0
    return-void

    .line 2705053
    :cond_1
    iget-object v0, p0, LX/JXd;->a:Landroid/view/View;

    invoke-static {p0, v0}, LX/JXd;->b(LX/JXd;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2705054
    invoke-virtual {p0}, LX/JXd;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2705055
    iget-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2705056
    iget-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2705057
    iget-object v0, p0, LX/JXd;->a:Landroid/view/View;

    check-cast v0, LX/JXT;

    invoke-interface {v0}, LX/JXT;->h()V

    .line 2705058
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/JXd;->c:Ljava/util/concurrent/ScheduledFuture;

    .line 2705059
    return-void
.end method
