.class public final LX/K6M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/K5l;


# instance fields
.field public final synthetic a:Lcom/facebook/tarot/cards/TarotCardVideo;


# direct methods
.method public constructor <init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V
    .locals 0

    .prologue
    .line 2771425
    iput-object p1, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/tarot/cards/TarotCardVideo;B)V
    .locals 0

    .prologue
    .line 2771428
    invoke-direct {p0, p1}, LX/K6M;-><init>(Lcom/facebook/tarot/cards/TarotCardVideo;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2771422
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->a()V

    .line 2771423
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->f:LX/K6S;

    invoke-virtual {v0, v1, v1}, LX/K6S;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771424
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 2771426
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setHeadlineLineHeightMultiplier(F)V

    .line 2771427
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2771420
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setHeadlineText(Ljava/lang/String;)V

    .line 2771421
    return-void
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 2771418
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionLineHeightMultiplier(F)V

    .line 2771419
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2771416
    iget-object v0, p0, LX/K6M;->a:Lcom/facebook/tarot/cards/TarotCardVideo;

    iget-object v0, v0, Lcom/facebook/tarot/cards/TarotCardVideo;->h:Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;

    invoke-virtual {v0, p1}, Lcom/facebook/tarot/cards/elements/HeadlineAndBodyView;->setDescriptionText(Ljava/lang/String;)V

    .line 2771417
    return-void
.end method
