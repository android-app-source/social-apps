.class public final LX/Jha;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Jhc;


# direct methods
.method public constructor <init>(LX/Jhc;Z)V
    .locals 0

    .prologue
    .line 2724583
    iput-object p1, p0, LX/Jha;->b:LX/Jhc;

    iput-boolean p2, p0, LX/Jha;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7e3125e6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2724584
    iget-object v0, p0, LX/Jha;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->j:LX/FCl;

    invoke-virtual {v0}, LX/FCl;->a()Z

    .line 2724585
    iget-boolean v0, p0, LX/Jha;->a:Z

    if-eqz v0, :cond_0

    .line 2724586
    iget-object v0, p0, LX/Jha;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3A0;

    iget-object v1, p0, LX/Jha;->b:LX/Jhc;

    iget-object v1, v1, LX/Jhc;->A:LX/DAU;

    .line 2724587
    iget-object v2, v1, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, v2

    .line 2724588
    iget-object v2, p0, LX/Jha;->b:LX/Jhc;

    iget-object v2, v2, LX/Jhc;->A:LX/DAU;

    .line 2724589
    iget-object v4, v2, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v4

    .line 2724590
    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    iget-object v4, p0, LX/Jha;->b:LX/Jhc;

    iget-object v4, v4, LX/Jhc;->A:LX/DAU;

    .line 2724591
    iget-object v5, v4, LX/DAU;->n:Ljava/lang/String;

    move-object v4, v5

    .line 2724592
    iget-object v5, p0, LX/Jha;->b:LX/Jhc;

    invoke-virtual {v5}, LX/Jhc;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    .line 2724593
    :goto_0
    const v0, -0x21c35a53

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2724594
    :cond_0
    iget-object v0, p0, LX/Jha;->b:LX/Jhc;

    iget-object v0, v0, LX/Jhc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3A0;

    iget-object v1, p0, LX/Jha;->b:LX/Jhc;

    invoke-virtual {v1}, LX/Jhc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Jha;->b:LX/Jhc;

    iget-object v2, v2, LX/Jhc;->A:LX/DAU;

    .line 2724595
    iget-object v4, v2, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v4

    .line 2724596
    iget-object v4, p0, LX/Jha;->b:LX/Jhc;

    iget-object v4, v4, LX/Jhc;->A:LX/DAU;

    .line 2724597
    iget-object v5, v4, LX/DAU;->n:Ljava/lang/String;

    move-object v4, v5

    .line 2724598
    invoke-virtual {v0, v1, v2, v3, v4}, LX/3A0;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V

    goto :goto_0
.end method
