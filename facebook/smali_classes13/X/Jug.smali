.class public LX/Jug;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3S2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2749360
    const-string v0, "MessagesSystemTrayLogService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2749361
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/Jug;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/3S2;->a(LX/0QB;)LX/3S2;

    move-result-object v1

    check-cast v1, LX/3S2;

    iput-object v0, p0, LX/Jug;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p0, LX/Jug;->b:LX/3S2;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x6b50d1f0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2749362
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2749363
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2749364
    const-string v0, "event_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zJ;

    .line 2749365
    sget-object v3, LX/3zJ;->DISMISS_FROM_TRAY:LX/3zJ;

    if-ne v0, v3, :cond_1

    .line 2749366
    iget-object v3, p0, LX/Jug;->b:LX/3S2;

    const-string v0, "notif_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "event_params"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2749367
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "messaging_notification_dismiss_from_tray"

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2749368
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/String;

    invoke-static {v0, p1}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object p0

    .line 2749369
    const-string p1, "notif_type"

    invoke-interface {p0, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2749370
    invoke-virtual {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2749371
    invoke-static {v3, v2}, LX/3S2;->a(LX/3S2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2749372
    :cond_0
    :goto_0
    const v0, 0x567ae5e4

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 2749373
    :cond_1
    sget-object v3, LX/3zJ;->CLICK_FROM_TRAY:LX/3zJ;

    if-ne v0, v3, :cond_0

    .line 2749374
    iget-object v3, p0, LX/Jug;->b:LX/3S2;

    const-string v0, "notif_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "event_params"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v3, v4, v0}, LX/3S2;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2749375
    const-string v0, "redirect_intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2749376
    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2749377
    iget-object v2, p0, LX/Jug;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x50fd76ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2749378
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2749379
    invoke-static {p0, p0}, LX/Jug;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2749380
    const/16 v1, 0x25

    const v2, 0x13ff6954

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
