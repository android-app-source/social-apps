.class public final LX/K1Z;
.super Landroid/webkit/WebView;
.source ""

# interfaces
.implements LX/5pQ;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(LX/5rJ;)V
    .locals 1

    .prologue
    .line 2762210
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 2762211
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1Z;->b:Z

    .line 2762212
    return-void
.end method

.method public static f(LX/K1Z;)V
    .locals 1

    .prologue
    .line 2762207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/K1Z;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2762208
    invoke-virtual {p0}, LX/K1Z;->destroy()V

    .line 2762209
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2762205
    new-instance v0, LX/K1e;

    invoke-virtual {p0}, LX/K1Z;->getId()I

    move-result v1

    invoke-direct {v0, v1, p1}, LX/K1e;-><init>(ILjava/lang/String;)V

    invoke-static {p0, v0}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;LX/5r0;)V

    .line 2762206
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2762202
    invoke-virtual {p0}, LX/K1Z;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getJavaScriptEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K1Z;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/K1Z;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2762203
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript:(function() {\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/K1Z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";\n})();"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/K1Z;->loadUrl(Ljava/lang/String;)V

    .line 2762204
    :cond_0
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2762213
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2762201
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2762199
    invoke-static {p0}, LX/K1Z;->f(LX/K1Z;)V

    .line 2762200
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2762193
    iget-boolean v0, p0, LX/K1Z;->b:Z

    if-eqz v0, :cond_1

    .line 2762194
    sget-boolean v0, LX/0AN;->a:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2762195
    const-string v0, "String(window.postMessage) === String(Object.hasOwnProperty).replace(\'hasOwnProperty\', \'postMessage\')"

    .line 2762196
    new-instance v1, LX/K1X;

    invoke-direct {v1, p0}, LX/K1X;-><init>(LX/K1Z;)V

    invoke-virtual {p0, v0, v1}, LX/K1Z;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 2762197
    :cond_0
    const-string v0, "javascript:(window.originalPostMessage = window.postMessage,window.postMessage = function(data) {__REACT_WEB_VIEW_BRIDGE.postMessage(String(data));})"

    invoke-virtual {p0, v0}, LX/K1Z;->loadUrl(Ljava/lang/String;)V

    .line 2762198
    :cond_1
    return-void
.end method

.method public setInjectedJavaScript(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2762191
    iput-object p1, p0, LX/K1Z;->a:Ljava/lang/String;

    .line 2762192
    return-void
.end method

.method public setMessagingEnabled(Z)V
    .locals 2

    .prologue
    .line 2762184
    iget-boolean v0, p0, LX/K1Z;->b:Z

    if-ne v0, p1, :cond_0

    .line 2762185
    :goto_0
    return-void

    .line 2762186
    :cond_0
    iput-boolean p1, p0, LX/K1Z;->b:Z

    .line 2762187
    if-eqz p1, :cond_1

    .line 2762188
    new-instance v0, LX/K1Y;

    invoke-direct {v0, p0, p0}, LX/K1Y;-><init>(LX/K1Z;LX/K1Z;)V

    const-string v1, "__REACT_WEB_VIEW_BRIDGE"

    invoke-virtual {p0, v0, v1}, LX/K1Z;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2762189
    invoke-virtual {p0}, LX/K1Z;->d()V

    goto :goto_0

    .line 2762190
    :cond_1
    const-string v0, "__REACT_WEB_VIEW_BRIDGE"

    invoke-virtual {p0, v0}, LX/K1Z;->removeJavascriptInterface(Ljava/lang/String;)V

    goto :goto_0
.end method
