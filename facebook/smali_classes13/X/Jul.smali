.class public LX/Jul;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2749425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2749426
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Jul;->a:Ljava/util/LinkedList;

    .line 2749427
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2749428
    iget-object v0, p0, LX/Jul;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2749429
    invoke-virtual {p0}, LX/Jul;->c()V

    .line 2749430
    :cond_0
    iget-object v0, p0, LX/Jul;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2749431
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iput-wide v0, p0, LX/Jul;->b:J

    .line 2749432
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2749433
    iget-object v0, p0, LX/Jul;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2749434
    iget-object v0, p0, LX/Jul;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 2749435
    return-void
.end method
