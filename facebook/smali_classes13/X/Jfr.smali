.class public final LX/Jfr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Jfp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Jfp",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jfs;


# direct methods
.method public constructor <init>(LX/Jfs;)V
    .locals 0

    .prologue
    .line 2722290
    iput-object p1, p0, LX/Jfr;->a:LX/Jfs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2722291
    iget-object v0, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v0, v0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722292
    iget-object v0, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v0, v0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2722293
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2722294
    iget-object v0, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v0, v0, LX/Jfs;->k:LX/Jfu;

    invoke-interface {v0, p1}, LX/Jfu;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2722295
    iget-object v1, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v1, v1, LX/Jfs;->j:LX/Jfk;

    .line 2722296
    iget-object p1, v1, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 2722297
    iget-object p1, v1, LX/Jfk;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2722298
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2722299
    iget-object v0, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v0, v0, LX/Jfs;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2722300
    iget-object v0, p0, LX/Jfr;->a:LX/Jfs;

    iget-object v0, v0, LX/Jfs;->b:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2722301
    return-void
.end method
