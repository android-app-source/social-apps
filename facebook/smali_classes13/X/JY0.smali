.class public final LX/JY0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JY2;


# direct methods
.method public constructor <init>(LX/JY2;)V
    .locals 0

    .prologue
    .line 2705632
    iput-object p1, p0, LX/JY0;->a:LX/JY2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2705633
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2705634
    if-nez p1, :cond_0

    .line 2705635
    const/4 v0, 0x0

    .line 2705636
    :goto_0
    return-object v0

    .line 2705637
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2705638
    check-cast v0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    move-result-object v0

    .line 2705639
    if-nez v0, :cond_1

    .line 2705640
    const/4 v1, 0x0

    .line 2705641
    :goto_1
    move-object v0, v1

    .line 2705642
    goto :goto_0

    .line 2705643
    :cond_1
    new-instance v3, LX/4Yc;

    invoke-direct {v3}, LX/4Yc;-><init>()V

    .line 2705644
    invoke-virtual {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2705645
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2705646
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 2705647
    invoke-virtual {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;

    .line 2705648
    if-nez v1, :cond_4

    .line 2705649
    const/4 v5, 0x0

    .line 2705650
    :goto_3
    move-object v1, v5

    .line 2705651
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2705652
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2705653
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2705654
    iput-object v1, v3, LX/4Yc;->b:LX/0Px;

    .line 2705655
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;->a()LX/0us;

    move-result-object v1

    .line 2705656
    if-nez v1, :cond_d

    .line 2705657
    const/4 v2, 0x0

    .line 2705658
    :goto_4
    move-object v1, v2

    .line 2705659
    iput-object v1, v3, LX/4Yc;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2705660
    invoke-virtual {v3}, LX/4Yc;->a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v1

    goto :goto_1

    .line 2705661
    :cond_4
    new-instance v5, LX/4Yd;

    invoke-direct {v5}, LX/4Yd;-><init>()V

    .line 2705662
    invoke-virtual {v1}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v6

    .line 2705663
    if-nez v6, :cond_5

    .line 2705664
    const/4 v7, 0x0

    .line 2705665
    :goto_5
    move-object v6, v7

    .line 2705666
    iput-object v6, v5, LX/4Yd;->b:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 2705667
    invoke-virtual {v1}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2705668
    iput-object v6, v5, LX/4Yd;->c:Ljava/lang/String;

    .line 2705669
    new-instance v6, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;-><init>(LX/4Yd;)V

    .line 2705670
    move-object v5, v6

    .line 2705671
    goto :goto_3

    .line 2705672
    :cond_5
    new-instance v7, LX/4Wm;

    invoke-direct {v7}, LX/4Wm;-><init>()V

    .line 2705673
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2705674
    if-nez v8, :cond_6

    .line 2705675
    const/4 v10, 0x0

    .line 2705676
    :goto_6
    move-object v8, v10

    .line 2705677
    iput-object v8, v7, LX/4Wm;->v:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 2705678
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->c()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v8

    .line 2705679
    if-nez v8, :cond_8

    .line 2705680
    const/4 v9, 0x0

    .line 2705681
    :goto_7
    move-object v8, v9

    .line 2705682
    iput-object v8, v7, LX/4Wm;->J:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 2705683
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->d()I

    move-result v8

    .line 2705684
    iput v8, v7, LX/4Wm;->K:I

    .line 2705685
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v8

    .line 2705686
    iput-object v8, v7, LX/4Wm;->R:Ljava/lang/String;

    .line 2705687
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->eA_()Ljava/lang/String;

    move-result-object v8

    .line 2705688
    iput-object v8, v7, LX/4Wm;->ac:Ljava/lang/String;

    .line 2705689
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->eB_()LX/1Fb;

    move-result-object v8

    invoke-static {v8}, LX/JXw;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    .line 2705690
    iput-object v8, v7, LX/4Wm;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2705691
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v8

    .line 2705692
    iput-object v8, v7, LX/4Wm;->bk:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2705693
    invoke-virtual {v6}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k()LX/174;

    move-result-object v8

    .line 2705694
    if-nez v8, :cond_c

    .line 2705695
    const/4 v9, 0x0

    .line 2705696
    :goto_8
    move-object v8, v9

    .line 2705697
    iput-object v8, v7, LX/4Wm;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2705698
    invoke-virtual {v7}, LX/4Wm;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    goto :goto_5

    .line 2705699
    :cond_6
    new-instance v11, LX/4WQ;

    invoke-direct {v11}, LX/4WQ;-><init>()V

    .line 2705700
    const/4 v10, 0x0

    const-class v12, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v9, v8, v10, v12}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 2705701
    if-nez v10, :cond_7

    .line 2705702
    const/4 v12, 0x0

    .line 2705703
    :goto_9
    move-object v10, v12

    .line 2705704
    iput-object v10, v11, LX/4WQ;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2705705
    invoke-virtual {v11}, LX/4WQ;->a()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v10

    goto :goto_6

    .line 2705706
    :cond_7
    new-instance v12, LX/4Xy;

    invoke-direct {v12}, LX/4Xy;-><init>()V

    .line 2705707
    invoke-virtual {v10}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/JXw;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 2705708
    iput-object v9, v12, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2705709
    invoke-virtual {v12}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v12

    goto :goto_9

    .line 2705710
    :cond_8
    new-instance v11, LX/4Wo;

    invoke-direct {v11}, LX/4Wo;-><init>()V

    .line 2705711
    invoke-virtual {v8}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->a()I

    move-result v9

    .line 2705712
    iput v9, v11, LX/4Wo;->b:I

    .line 2705713
    invoke-virtual {v8}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 2705714
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 2705715
    const/4 v9, 0x0

    move v10, v9

    :goto_a
    invoke-virtual {v8}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_9

    .line 2705716
    invoke-virtual {v8}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;

    .line 2705717
    if-nez v9, :cond_b

    .line 2705718
    const/4 p0, 0x0

    .line 2705719
    :goto_b
    move-object v9, p0

    .line 2705720
    invoke-virtual {v12, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2705721
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_a

    .line 2705722
    :cond_9
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 2705723
    iput-object v9, v11, LX/4Wo;->d:LX/0Px;

    .line 2705724
    :cond_a
    invoke-virtual {v11}, LX/4Wo;->a()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v9

    goto/16 :goto_7

    .line 2705725
    :cond_b
    new-instance p0, LX/33O;

    invoke-direct {p0}, LX/33O;-><init>()V

    .line 2705726
    invoke-virtual {v9}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->a()LX/1Fb;

    move-result-object p1

    invoke-static {p1}, LX/JXw;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    .line 2705727
    iput-object p1, p0, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2705728
    invoke-virtual {p0}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object p0

    goto :goto_b

    .line 2705729
    :cond_c
    new-instance v9, LX/173;

    invoke-direct {v9}, LX/173;-><init>()V

    .line 2705730
    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v10

    .line 2705731
    iput-object v10, v9, LX/173;->f:Ljava/lang/String;

    .line 2705732
    invoke-virtual {v9}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    goto/16 :goto_8

    .line 2705733
    :cond_d
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2705734
    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v4

    .line 2705735
    iput-object v4, v2, LX/17L;->c:Ljava/lang/String;

    .line 2705736
    invoke-interface {v1}, LX/0us;->b()Z

    move-result v4

    .line 2705737
    iput-boolean v4, v2, LX/17L;->d:Z

    .line 2705738
    invoke-interface {v1}, LX/0us;->c()Z

    move-result v4

    .line 2705739
    iput-boolean v4, v2, LX/17L;->e:Z

    .line 2705740
    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v4

    .line 2705741
    iput-object v4, v2, LX/17L;->f:Ljava/lang/String;

    .line 2705742
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_4
.end method
