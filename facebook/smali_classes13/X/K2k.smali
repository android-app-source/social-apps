.class public final LX/K2k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/82h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/richdocument/optional/impl/UFIViewImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V
    .locals 1

    .prologue
    .line 2764296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2764297
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/K2k;->a:Ljava/lang/ref/WeakReference;

    .line 2764298
    return-void
.end method

.method private a(LX/82h;)V
    .locals 2

    .prologue
    .line 2764299
    iget-object v0, p0, LX/K2k;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    .line 2764300
    if-nez v0, :cond_0

    .line 2764301
    :goto_0
    return-void

    .line 2764302
    :cond_0
    iget-object v1, p1, LX/82f;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v1, v1

    .line 2764303
    iput-object v1, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764304
    const/4 v1, 0x1

    .line 2764305
    iput-boolean v1, v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->z:Z

    .line 2764306
    invoke-virtual {v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2764307
    check-cast p1, LX/82h;

    invoke-direct {p0, p1}, LX/K2k;->a(LX/82h;)V

    return-void
.end method
