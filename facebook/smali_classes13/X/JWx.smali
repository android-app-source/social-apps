.class public final LX/JWx;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JWy;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JWy;


# direct methods
.method public constructor <init>(LX/JWy;)V
    .locals 1

    .prologue
    .line 2703710
    iput-object p1, p0, LX/JWx;->c:LX/JWy;

    .line 2703711
    move-object v0, p1

    .line 2703712
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2703713
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2703714
    const-string v0, "PysfHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2703715
    if-ne p0, p1, :cond_1

    .line 2703716
    :cond_0
    :goto_0
    return v0

    .line 2703717
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2703718
    goto :goto_0

    .line 2703719
    :cond_3
    check-cast p1, LX/JWx;

    .line 2703720
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2703721
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2703722
    if-eq v2, v3, :cond_0

    .line 2703723
    iget-object v2, p0, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2703724
    goto :goto_0

    .line 2703725
    :cond_5
    iget-object v2, p1, LX/JWx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2703726
    :cond_6
    iget-object v2, p0, LX/JWx;->b:LX/1Pb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JWx;->b:LX/1Pb;

    iget-object v3, p1, LX/JWx;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2703727
    goto :goto_0

    .line 2703728
    :cond_7
    iget-object v2, p1, LX/JWx;->b:LX/1Pb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
