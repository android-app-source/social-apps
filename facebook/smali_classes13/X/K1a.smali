.class public final LX/K1a;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2762256
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2762257
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1a;->a:Z

    return-void
.end method

.method private a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2762254
    new-instance v0, LX/K1c;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getId()I

    move-result v1

    invoke-direct {p0, p1, p2}, LX/K1a;->b(Landroid/webkit/WebView;Ljava/lang/String;)LX/5pH;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/K1c;-><init>(ILX/5pH;)V

    invoke-static {p1, v0}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;LX/5r0;)V

    .line 2762255
    return-void
.end method

.method private b(Landroid/webkit/WebView;Ljava/lang/String;)LX/5pH;
    .locals 4

    .prologue
    .line 2762245
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2762246
    const-string v0, "target"

    invoke-virtual {p1}, Landroid/webkit/WebView;->getId()I

    move-result v2

    int-to-double v2, v2

    invoke-interface {v1, v0, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2762247
    const-string v0, "url"

    invoke-interface {v1, v0, p2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2762248
    const-string v2, "loading"

    iget-boolean v0, p0, LX/K1a;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/webkit/WebView;->getProgress()I

    move-result v0

    const/16 v3, 0x64

    if-eq v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2762249
    const-string v0, "title"

    invoke-virtual {p1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2762250
    const-string v0, "canGoBack"

    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2762251
    const-string v0, "canGoForward"

    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoForward()Z

    move-result v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2762252
    return-object v1

    .line 2762253
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2762242
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    .line 2762243
    new-instance v0, LX/K1d;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getId()I

    move-result v1

    invoke-direct {p0, p1, p2}, LX/K1a;->b(Landroid/webkit/WebView;Ljava/lang/String;)LX/5pH;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/K1d;-><init>(ILX/5pH;)V

    invoke-static {p1, v0}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;LX/5r0;)V

    .line 2762244
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2762235
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2762236
    iget-boolean v0, p0, LX/K1a;->a:Z

    if-nez v0, :cond_0

    move-object v0, p1

    .line 2762237
    check-cast v0, LX/K1Z;

    .line 2762238
    invoke-virtual {v0}, LX/K1Z;->b()V

    .line 2762239
    invoke-virtual {v0}, LX/K1Z;->d()V

    .line 2762240
    invoke-direct {p0, p1, p2}, LX/K1a;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2762241
    :cond_0
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 2762231
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 2762232
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/K1a;->a:Z

    .line 2762233
    new-instance v0, LX/K1d;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getId()I

    move-result v1

    invoke-direct {p0, p1, p2}, LX/K1a;->b(Landroid/webkit/WebView;Ljava/lang/String;)LX/5pH;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/K1d;-><init>(ILX/5pH;)V

    invoke-static {p1, v0}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;LX/5r0;)V

    .line 2762234
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2762223
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2762224
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K1a;->a:Z

    .line 2762225
    invoke-direct {p0, p1, p4}, LX/K1a;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2762226
    invoke-direct {p0, p1, p4}, LX/K1a;->b(Landroid/webkit/WebView;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    .line 2762227
    const-string v1, "code"

    int-to-double v2, p2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2762228
    const-string v1, "description"

    invoke-interface {v0, v1, p3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2762229
    new-instance v1, LX/K1b;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getId()I

    move-result v2

    invoke-direct {v1, v2, v0}, LX/K1b;-><init>(ILX/5pH;)V

    invoke-static {p1, v1}, Lcom/facebook/react/views/webview/ReactWebViewManager;->b(Landroid/webkit/WebView;LX/5r0;)V

    .line 2762230
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2762214
    const-string v0, "http://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "file://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2762215
    :cond_0
    const/4 v0, 0x0

    .line 2762216
    :goto_0
    return v0

    .line 2762217
    :cond_1
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2762218
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2762219
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2762220
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2762221
    :catch_0
    move-exception v0

    .line 2762222
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activity not found to handle uri scheme for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
