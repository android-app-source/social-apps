.class public LX/Js7;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Landroid/widget/Button;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2745103
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2745104
    const v0, 0x7f030c75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2745105
    const v0, 0x7f0d1e96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/Js7;->a:Landroid/widget/Button;

    .line 2745106
    const v0, 0x7f0d1e95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Js7;->b:Landroid/widget/TextView;

    .line 2745107
    iget-object v0, p0, LX/Js7;->a:Landroid/widget/Button;

    new-instance p1, LX/Js6;

    invoke-direct {p1, p0}, LX/Js6;-><init>(LX/Js7;)V

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2745108
    iget-object v0, p0, LX/Js7;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getCurrentTextColor()I

    move-result v0

    iput v0, p0, LX/Js7;->d:I

    .line 2745109
    return-void
.end method


# virtual methods
.method public setActionButtonText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2745110
    iget-object v0, p0, LX/Js7;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2745111
    iget-object v1, p0, LX/Js7;->a:Landroid/widget/Button;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2745112
    return-void

    .line 2745113
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionButtonTextColor(I)V
    .locals 1

    .prologue
    .line 2745114
    iget-object v0, p0, LX/Js7;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextColor(I)V

    .line 2745115
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2745116
    iget-object v0, p0, LX/Js7;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2745117
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 2745118
    const/4 v0, 0x0

    return v0
.end method
