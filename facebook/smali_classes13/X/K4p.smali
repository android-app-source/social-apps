.class public final enum LX/K4p;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/K4p;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/K4p;

.field public static final enum FB4A:LX/K4p;

.field public static final enum MOMENTS:LX/K4p;


# instance fields
.field private final mReactNativeLaunchSurface:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2769512
    new-instance v0, LX/K4p;

    const-string v1, "FB4A"

    const-string v2, "wilde_composer"

    invoke-direct {v0, v1, v3, v2}, LX/K4p;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/K4p;->FB4A:LX/K4p;

    .line 2769513
    new-instance v0, LX/K4p;

    const-string v1, "MOMENTS"

    const-string v2, "folder_cover"

    invoke-direct {v0, v1, v4, v2}, LX/K4p;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/K4p;->MOMENTS:LX/K4p;

    .line 2769514
    const/4 v0, 0x2

    new-array v0, v0, [LX/K4p;

    sget-object v1, LX/K4p;->FB4A:LX/K4p;

    aput-object v1, v0, v3

    sget-object v1, LX/K4p;->MOMENTS:LX/K4p;

    aput-object v1, v0, v4

    sput-object v0, LX/K4p;->$VALUES:[LX/K4p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2769515
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2769516
    iput-object p3, p0, LX/K4p;->mReactNativeLaunchSurface:Ljava/lang/String;

    .line 2769517
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/K4p;
    .locals 1

    .prologue
    .line 2769518
    const-class v0, LX/K4p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/K4p;

    return-object v0
.end method

.method public static values()[LX/K4p;
    .locals 1

    .prologue
    .line 2769519
    sget-object v0, LX/K4p;->$VALUES:[LX/K4p;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/K4p;

    return-object v0
.end method


# virtual methods
.method public final getReactNativeLaunchSurface()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2769520
    iget-object v0, p0, LX/K4p;->mReactNativeLaunchSurface:Ljava/lang/String;

    return-object v0
.end method
