.class public LX/K0d;
.super Landroid/widget/Spinner;
.source ""


# instance fields
.field private a:I

.field public b:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:LX/K0c;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2759181
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 2759182
    const/4 v0, 0x0

    iput v0, p0, LX/K0d;->a:I

    .line 2759183
    new-instance v0, Lcom/facebook/react/views/picker/ReactPicker$1;

    invoke-direct {v0, p0}, Lcom/facebook/react/views/picker/ReactPicker$1;-><init>(LX/K0d;)V

    iput-object v0, p0, LX/K0d;->f:Ljava/lang/Runnable;

    .line 2759184
    iput p2, p0, LX/K0d;->a:I

    .line 2759185
    return-void
.end method

.method private setSelectionWithSuppressEvent(I)V
    .locals 1

    .prologue
    .line 2759177
    invoke-virtual {p0}, LX/K0d;->getSelectedItemPosition()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 2759178
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0d;->c:Z

    .line 2759179
    invoke-virtual {p0, p1}, LX/K0d;->setSelection(I)V

    .line 2759180
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2759173
    iget-object v0, p0, LX/K0d;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2759174
    iget-object v0, p0, LX/K0d;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, LX/K0d;->setSelectionWithSuppressEvent(I)V

    .line 2759175
    const/4 v0, 0x0

    iput-object v0, p0, LX/K0d;->e:Ljava/lang/Integer;

    .line 2759176
    :cond_0
    return-void
.end method

.method public getMode()I
    .locals 1
    .annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2759158
    iget v0, p0, LX/K0d;->a:I

    return v0
.end method

.method public getOnSelectListener()LX/K0c;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2759172
    iget-object v0, p0, LX/K0d;->d:LX/K0c;

    return-object v0
.end method

.method public getPrimaryColor()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2759171
    iget-object v0, p0, LX/K0d;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 2759168
    invoke-super {p0}, Landroid/widget/Spinner;->requestLayout()V

    .line 2759169
    iget-object v0, p0, LX/K0d;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/K0d;->post(Ljava/lang/Runnable;)Z

    .line 2759170
    return-void
.end method

.method public setOnSelectListener(LX/K0c;)V
    .locals 1
    .param p1    # LX/K0c;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2759163
    invoke-virtual {p0}, LX/K0d;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2759164
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/K0d;->c:Z

    .line 2759165
    new-instance v0, LX/K0b;

    invoke-direct {v0, p0}, LX/K0b;-><init>(LX/K0d;)V

    invoke-virtual {p0, v0}, LX/K0d;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2759166
    :cond_0
    iput-object p1, p0, LX/K0d;->d:LX/K0c;

    .line 2759167
    return-void
.end method

.method public setPrimaryColor(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2759161
    iput-object p1, p0, LX/K0d;->b:Ljava/lang/Integer;

    .line 2759162
    return-void
.end method

.method public setStagedSelection(I)V
    .locals 1

    .prologue
    .line 2759159
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/K0d;->e:Ljava/lang/Integer;

    .line 2759160
    return-void
.end method
