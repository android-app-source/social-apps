.class public LX/Jop;
.super LX/1qS;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Jop;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/Jos;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2736220
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "messaging_omni_m"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 2736221
    return-void
.end method

.method public static a(LX/0QB;)LX/Jop;
    .locals 7

    .prologue
    .line 2736201
    sget-object v0, LX/Jop;->a:LX/Jop;

    if-nez v0, :cond_1

    .line 2736202
    const-class v1, LX/Jop;

    monitor-enter v1

    .line 2736203
    :try_start_0
    sget-object v0, LX/Jop;->a:LX/Jop;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2736204
    if-eqz v2, :cond_0

    .line 2736205
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2736206
    new-instance p0, LX/Jop;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    .line 2736207
    new-instance v6, LX/Jos;

    invoke-direct {v6}, LX/Jos;-><init>()V

    .line 2736208
    move-object v6, v6

    .line 2736209
    move-object v6, v6

    .line 2736210
    check-cast v6, LX/Jos;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Jop;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/Jos;)V

    .line 2736211
    move-object v0, p0

    .line 2736212
    sput-object v0, LX/Jop;->a:LX/Jop;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2736213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2736214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2736215
    :cond_1
    sget-object v0, LX/Jop;->a:LX/Jop;

    return-object v0

    .line 2736216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2736217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2736218
    invoke-virtual {p0}, LX/0Tr;->f()V

    .line 2736219
    return-void
.end method
