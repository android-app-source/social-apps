.class public final LX/Jy0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public final synthetic b:Z

.field public final synthetic c:LX/Jy3;


# direct methods
.method public constructor <init>(LX/Jy3;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V
    .locals 0

    .prologue
    .line 2753972
    iput-object p1, p0, LX/Jy0;->c:LX/Jy3;

    iput-object p2, p0, LX/Jy0;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-boolean p3, p0, LX/Jy0;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryGraphQLInterfaces$ProfileDiscoverySectionFields;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2753973
    iget-object v0, p0, LX/Jy0;->c:LX/Jy3;

    const/4 v1, 0x0

    .line 2753974
    iput-object v1, v0, LX/Jy3;->n:LX/1Mv;

    .line 2753975
    iget-object v0, p0, LX/Jy0;->c:LX/Jy3;

    iget-object v1, p0, LX/Jy0;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-boolean v2, p0, LX/Jy0;->b:Z

    .line 2753976
    invoke-static {v0, p1, v1, v2}, LX/Jy3;->a$redex0(LX/Jy3;LX/0Px;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    .line 2753977
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2753978
    iget-object v0, p0, LX/Jy0;->c:LX/Jy3;

    const/4 v1, 0x0

    .line 2753979
    iput-object v1, v0, LX/Jy3;->n:LX/1Mv;

    .line 2753980
    iget-object v0, p0, LX/Jy0;->c:LX/Jy3;

    iget-object v1, p0, LX/Jy0;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-boolean v2, p0, LX/Jy0;->b:Z

    .line 2753981
    invoke-static {v0, p1, v1, v2}, LX/Jy3;->a$redex0(LX/Jy3;Ljava/lang/Throwable;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Z)V

    .line 2753982
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2753983
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/Jy0;->a(LX/0Px;)V

    return-void
.end method
