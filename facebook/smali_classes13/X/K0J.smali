.class public LX/K0J;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/K0J;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 2758207
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2758208
    iput p2, p0, LX/K0J;->a:I

    .line 2758209
    return-void
.end method

.method private k()LX/5pH;
    .locals 4

    .prologue
    .line 2758210
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2758211
    const-string v1, "drawerState"

    .line 2758212
    iget v2, p0, LX/K0J;->a:I

    move v2, v2

    .line 2758213
    int-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2758214
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2758215
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2758216
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/K0J;->k()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2758217
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2758218
    const-string v0, "topDrawerStateChanged"

    return-object v0
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 2758219
    const/4 v0, 0x0

    return v0
.end method
