.class public LX/JWe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2703293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2703294
    iput-object p1, p0, LX/JWe;->a:LX/1vg;

    .line 2703295
    return-void
.end method

.method public static a(LX/JWe;LX/1De;I)LX/1Di;
    .locals 3

    .prologue
    .line 2703296
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    iget-object v1, p0, LX/JWe;->a:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1bbc

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1bbc

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b1bbd

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f02032c

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JWe;
    .locals 4

    .prologue
    .line 2703297
    const-class v1, LX/JWe;

    monitor-enter v1

    .line 2703298
    :try_start_0
    sget-object v0, LX/JWe;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2703299
    sput-object v2, LX/JWe;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2703300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2703302
    new-instance p0, LX/JWe;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/JWe;-><init>(LX/1vg;)V

    .line 2703303
    move-object v0, p0

    .line 2703304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2703305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JWe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2703306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2703307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
