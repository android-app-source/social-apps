.class public final enum LX/Jpv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Jpv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Jpv;

.field public static final enum CONTACT_PICKER_DIALOG_NUX_FLOW:LX/Jpv;

.field public static final enum CONTACT_PICKER_FLOW:LX/Jpv;

.field public static final enum CONTACT_PICKER_NO_NUX_FLOW:LX/Jpv;

.field public static final enum NUX_UPLOAD_FLOW:LX/Jpv;

.field public static final enum NUX_UPLOAD_WITH_CONTACT_MATCHING_FLOW:LX/Jpv;


# instance fields
.field private final mFlowIdentifier:Ljava/lang/String;

.field private final mFlowName:Ljava/lang/String;

.field private final mFlowSteps:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2738330
    new-instance v0, LX/Jpv;

    const-string v1, "NUX_UPLOAD_FLOW"

    const-string v3, "Contact Log/Sync Only (No Picker)"

    const-string v4, "upload_flow"

    const-class v5, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Jpv;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V

    sput-object v0, LX/Jpv;->NUX_UPLOAD_FLOW:LX/Jpv;

    .line 2738331
    new-instance v3, LX/Jpv;

    const-string v4, "CONTACT_PICKER_FLOW"

    const-string v6, "Flow B (Picker -> Log/Sync)"

    const-string v7, "contact_picker"

    const-class v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    const-class v1, Lcom/facebook/messaging/sms/migration/SMSUploadFragment;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/Jpv;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V

    sput-object v3, LX/Jpv;->CONTACT_PICKER_FLOW:LX/Jpv;

    .line 2738332
    new-instance v3, LX/Jpv;

    const-string v4, "CONTACT_PICKER_NO_NUX_FLOW"

    const-string v6, "Picker Only (No Contact Log/Sync)"

    const-string v7, "contact_picker_no_nux"

    const-class v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/Jpv;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V

    sput-object v3, LX/Jpv;->CONTACT_PICKER_NO_NUX_FLOW:LX/Jpv;

    .line 2738333
    new-instance v3, LX/Jpv;

    const-string v4, "CONTACT_PICKER_DIALOG_NUX_FLOW"

    const-string v6, "Flow B\' (Picker -> Log/Sync Dialog)"

    const-string v7, "contact_picker_dialog_nux"

    const-class v0, Lcom/facebook/messaging/sms/migration/SMSContactPickerWithUploadDialogFragment;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    move v5, v11

    invoke-direct/range {v3 .. v8}, LX/Jpv;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V

    sput-object v3, LX/Jpv;->CONTACT_PICKER_DIALOG_NUX_FLOW:LX/Jpv;

    .line 2738334
    new-instance v3, LX/Jpv;

    const-string v4, "NUX_UPLOAD_WITH_CONTACT_MATCHING_FLOW"

    const-string v6, "Flow A (Log/Sync -> Picker)"

    const-string v7, "nux_upload_with_contact_matching_flow"

    const-class v0, Lcom/facebook/messaging/sms/migration/SMSUploadAndMatchFragment;

    const-class v1, Lcom/facebook/messaging/sms/migration/SMSContactPickerFragment;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    move v5, v12

    invoke-direct/range {v3 .. v8}, LX/Jpv;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V

    sput-object v3, LX/Jpv;->NUX_UPLOAD_WITH_CONTACT_MATCHING_FLOW:LX/Jpv;

    .line 2738335
    const/4 v0, 0x5

    new-array v0, v0, [LX/Jpv;

    sget-object v1, LX/Jpv;->NUX_UPLOAD_FLOW:LX/Jpv;

    aput-object v1, v0, v2

    sget-object v1, LX/Jpv;->CONTACT_PICKER_FLOW:LX/Jpv;

    aput-object v1, v0, v9

    sget-object v1, LX/Jpv;->CONTACT_PICKER_NO_NUX_FLOW:LX/Jpv;

    aput-object v1, v0, v10

    sget-object v1, LX/Jpv;->CONTACT_PICKER_DIALOG_NUX_FLOW:LX/Jpv;

    aput-object v1, v0, v11

    sget-object v1, LX/Jpv;->NUX_UPLOAD_WITH_CONTACT_MATCHING_FLOW:LX/Jpv;

    aput-object v1, v0, v12

    sput-object v0, LX/Jpv;->$VALUES:[LX/Jpv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2738325
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2738326
    iput-object p3, p0, LX/Jpv;->mFlowName:Ljava/lang/String;

    .line 2738327
    iput-object p4, p0, LX/Jpv;->mFlowIdentifier:Ljava/lang/String;

    .line 2738328
    iput-object p5, p0, LX/Jpv;->mFlowSteps:LX/0Px;

    .line 2738329
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Jpv;
    .locals 5

    .prologue
    .line 2738308
    if-eqz p0, :cond_1

    .line 2738309
    invoke-static {}, LX/Jpv;->values()[LX/Jpv;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2738310
    iget-object v4, v0, LX/Jpv;->mFlowIdentifier:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2738311
    :goto_1
    return-object v0

    .line 2738312
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2738313
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Jpv;
    .locals 1

    .prologue
    .line 2738324
    const-class v0, LX/Jpv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Jpv;

    return-object v0
.end method

.method public static values()[LX/Jpv;
    .locals 1

    .prologue
    .line 2738323
    sget-object v0, LX/Jpv;->$VALUES:[LX/Jpv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Jpv;

    return-object v0
.end method


# virtual methods
.method public final generateIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2738320
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/sms/migration/SMSContactsMigratorActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2738321
    const-string v1, "migration_flow"

    iget-object v2, p0, LX/Jpv;->mFlowIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2738322
    return-object v0
.end method

.method public final getFirstStep()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2738319
    iget-object v0, p0, LX/Jpv;->mFlowSteps:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2738318
    iget-object v0, p0, LX/Jpv;->mFlowName:Ljava/lang/String;

    return-object v0
.end method

.method public final getNextStep(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;)",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2738314
    iget-object v0, p0, LX/Jpv;->mFlowSteps:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2738315
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/Jpv;->mFlowSteps:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 2738316
    :cond_0
    const/4 v0, 0x0

    .line 2738317
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/Jpv;->mFlowSteps:LX/0Px;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    goto :goto_0
.end method
