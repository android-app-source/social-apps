.class public LX/JeB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JeX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/JeW;

.field public final d:LX/JeY;

.field public final e:LX/JeN;

.field public final f:LX/JfY;

.field private final g:LX/Jdn;

.field public final h:LX/FJW;

.field public final i:LX/297;

.field public j:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/JeW;LX/JeY;LX/JeN;LX/JfY;LX/FJW;LX/297;LX/Jdn;)V
    .locals 1
    .param p9    # LX/Jdn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/JeX;",
            ">;",
            "LX/JeW;",
            "LX/JeY;",
            "LX/JeN;",
            "LX/JfY;",
            "LX/FJW;",
            "LX/297;",
            "Lcom/facebook/messaging/blocking/ManageMessagesAdapterViewFactory$ManageMessagesRowBindableOptimisticCallback;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2720360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2720361
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JeB;->j:Z

    .line 2720362
    iput-object p1, p0, LX/JeB;->a:LX/0Ot;

    .line 2720363
    iput-object p2, p0, LX/JeB;->b:LX/0Or;

    .line 2720364
    iput-object p3, p0, LX/JeB;->c:LX/JeW;

    .line 2720365
    iput-object p4, p0, LX/JeB;->d:LX/JeY;

    .line 2720366
    iput-object p5, p0, LX/JeB;->e:LX/JeN;

    .line 2720367
    iput-object p7, p0, LX/JeB;->h:LX/FJW;

    .line 2720368
    iput-object p8, p0, LX/JeB;->i:LX/297;

    .line 2720369
    iput-object p6, p0, LX/JeB;->f:LX/JfY;

    .line 2720370
    iput-object p9, p0, LX/JeB;->g:LX/Jdn;

    .line 2720371
    return-void
.end method

.method public static b(Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;Z)Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;
    .locals 10

    .prologue
    .line 2720372
    new-instance v0, LX/CKp;

    invoke-direct {v0}, LX/CKp;-><init>()V

    .line 2720373
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CKp;->a:Ljava/lang/String;

    .line 2720374
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CKp;->b:Ljava/lang/String;

    .line 2720375
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/CKp;->c:Z

    .line 2720376
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CKp;->d:Ljava/lang/String;

    .line 2720377
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/CKp;->e:Z

    .line 2720378
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CKp;->f:Ljava/lang/String;

    .line 2720379
    move-object v0, v0

    .line 2720380
    iput-boolean p1, v0, LX/CKp;->e:Z

    .line 2720381
    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 2720382
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 2720383
    iget-object v2, v0, LX/CKp;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2720384
    iget-object v4, v0, LX/CKp;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2720385
    iget-object v6, v0, LX/CKp;->d:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2720386
    iget-object v7, v0, LX/CKp;->f:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2720387
    const/4 v8, 0x6

    invoke-virtual {v1, v8}, LX/186;->c(I)V

    .line 2720388
    invoke-virtual {v1, v9, v2}, LX/186;->b(II)V

    .line 2720389
    invoke-virtual {v1, v5, v4}, LX/186;->b(II)V

    .line 2720390
    const/4 v2, 0x2

    iget-boolean v4, v0, LX/CKp;->c:Z

    invoke-virtual {v1, v2, v4}, LX/186;->a(IZ)V

    .line 2720391
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 2720392
    const/4 v2, 0x4

    iget-boolean v4, v0, LX/CKp;->e:Z

    invoke-virtual {v1, v2, v4}, LX/186;->a(IZ)V

    .line 2720393
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 2720394
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 2720395
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 2720396
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2720397
    invoke-virtual {v2, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2720398
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2720399
    new-instance v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-direct {v2, v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;-><init>(LX/15i;)V

    .line 2720400
    move-object v0, v2

    .line 2720401
    return-object v0
.end method

.method public static b(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2720402
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/Jdp;)LX/Jek;
    .locals 4

    .prologue
    .line 2720403
    sget-object v0, LX/Jdv;->a:[I

    invoke-virtual {p2}, LX/Jdp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2720404
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2720405
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2720406
    new-instance v1, LX/Jem;

    new-instance v2, LX/JgB;

    invoke-direct {v2, p1}, LX/JgB;-><init>(Landroid/view/ViewGroup;)V

    new-instance p2, LX/Je0;

    invoke-direct {p2, p0, v0}, LX/Je0;-><init>(LX/JeB;Landroid/content/Context;)V

    invoke-direct {v1, v2, p2}, LX/Jem;-><init>(LX/JgB;LX/Jdw;)V

    move-object v0, v1

    .line 2720407
    :goto_0
    return-object v0

    .line 2720408
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2720409
    new-instance v1, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;

    new-instance v2, LX/JgE;

    sget-object p2, LX/JgD;->SWITCH:LX/JgD;

    invoke-direct {v2, p1, p2}, LX/JgE;-><init>(Landroid/view/ViewGroup;LX/JgD;)V

    new-instance p2, LX/Je8;

    invoke-direct {p2, p0, v0}, LX/Je8;-><init>(LX/JeB;Landroid/content/Context;)V

    invoke-direct {v1, v2, p2}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;-><init>(LX/JgE;LX/Jdw;)V

    move-object v0, v1

    .line 2720410
    goto :goto_0

    .line 2720411
    :pswitch_2
    new-instance v0, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;

    new-instance v1, LX/JgE;

    sget-object v2, LX/JgD;->CHECKBOX:LX/JgD;

    invoke-direct {v1, p1, v2}, LX/JgE;-><init>(Landroid/view/ViewGroup;LX/JgD;)V

    new-instance v2, LX/Jdx;

    invoke-direct {v2, p0}, LX/Jdx;-><init>(LX/JeB;)V

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/blocking/view/ManageMessagesToggleRowBindable;-><init>(LX/JgE;LX/Jdw;)V

    move-object v0, v0

    .line 2720412
    goto :goto_0

    .line 2720413
    :pswitch_3
    new-instance v0, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;

    new-instance v1, LX/JgA;

    invoke-direct {v1, p1}, LX/JgA;-><init>(Landroid/view/ViewGroup;)V

    new-instance v2, LX/Jdz;

    invoke-direct {v2, p0, p1}, LX/Jdz;-><init>(LX/JeB;Landroid/view/ViewGroup;)V

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;-><init>(LX/JgA;LX/Jdw;)V

    move-object v0, v0

    .line 2720414
    goto :goto_0

    .line 2720415
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2720416
    new-instance v1, LX/Jem;

    new-instance v2, LX/JgB;

    invoke-direct {v2, p1}, LX/JgB;-><init>(Landroid/view/ViewGroup;)V

    new-instance p2, LX/Je1;

    invoke-direct {p2, p0, v0}, LX/Je1;-><init>(LX/JeB;Landroid/content/Context;)V

    invoke-direct {v1, v2, p2}, LX/Jem;-><init>(LX/JgB;LX/Jdw;)V

    move-object v0, v1

    .line 2720417
    goto :goto_0

    .line 2720418
    :pswitch_5
    iget-object v0, p0, LX/JeB;->g:LX/Jdn;

    .line 2720419
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2720420
    new-instance v2, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;

    new-instance v3, LX/JgA;

    invoke-direct {v3, p1}, LX/JgA;-><init>(Landroid/view/ViewGroup;)V

    new-instance p2, LX/Je3;

    invoke-direct {p2, p0, v1, v0}, LX/Je3;-><init>(LX/JeB;Landroid/content/Context;LX/Jdn;)V

    invoke-direct {v2, v3, p2}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;-><init>(LX/JgA;LX/Jdw;)V

    move-object v0, v2

    .line 2720421
    goto :goto_0

    .line 2720422
    :pswitch_6
    iget-object v0, p0, LX/JeB;->g:LX/Jdn;

    .line 2720423
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2720424
    new-instance v2, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;

    new-instance v3, LX/JgA;

    invoke-direct {v3, p1}, LX/JgA;-><init>(Landroid/view/ViewGroup;)V

    new-instance p2, LX/Je5;

    invoke-direct {p2, p0, v1, v0}, LX/Je5;-><init>(LX/JeB;Landroid/content/Context;LX/Jdn;)V

    invoke-direct {v2, v3, p2}, Lcom/facebook/messaging/blocking/view/ManageMessagesTextRowBindable;-><init>(LX/JgA;LX/Jdw;)V

    move-object v0, v2

    .line 2720425
    goto :goto_0

    .line 2720426
    :pswitch_7
    new-instance v0, LX/Jel;

    new-instance v1, LX/Jg9;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/Jg9;-><init>(Landroid/view/ViewGroup;Ljava/lang/Integer;)V

    invoke-direct {v0, v1}, LX/Jel;-><init>(LX/Jg9;)V

    move-object v0, v0

    .line 2720427
    goto/16 :goto_0

    .line 2720428
    :pswitch_8
    new-instance v0, LX/Jel;

    new-instance v1, LX/Jg9;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/Jg9;-><init>(Landroid/view/ViewGroup;Ljava/lang/Integer;)V

    invoke-direct {v0, v1}, LX/Jel;-><init>(LX/Jg9;)V

    move-object v0, v0

    .line 2720429
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
